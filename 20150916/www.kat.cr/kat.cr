<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-bca1102.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-bca1102.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-bca1102.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-bca1102.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kat.cr/sc-bca1102.js');

        sc('setHost', 'a.kat.cr');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: 'bca1102',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-bca1102.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<div id="_60318cd4e8d28f6fb76fe34e9bd9c498"></div>
<div id="_39ecb76dd457e5ac33776fdf11500d56"></div>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka">e</i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka">g</i></button></div>
			</form>
		</div>
        <div id="_277923e5f9d753c5b0630c28e641790c"></div>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080p/" class="tag6">1080p</a>
	<a href="/search/2014/" class="tag1">2014</a>
	<a href="/search/2015/" class="tag10">2015</a>
	<a href="/search/3d/" class="tag3">3d</a>
	<a href="/search/android/" class="tag2">android</a>
	<a href="/search/ant%20man/" class="tag2">ant man</a>
	<a href="/search/apk/" class="tag2">apk</a>
	<a href="/search/avengers/" class="tag1">avengers</a>
	<a href="/search/discography/" class="tag3">discography</a>
	<a href="/search/fear%20the%20walking%20dead/" class="tag7">fear the walking dead</a>
	<a href="/search/fear%20the%20walking%20dead/" class="tag1">fear the walking dead</a>
	<a href="/search/fear%20the%20walking%20dead%20s01e03/" class="tag4">fear the walking dead s01e03</a>
	<a href="/search/french/" class="tag3">french</a>
	<a href="/search/hindi/" class="tag10">hindi</a>
	<a href="/search/hindi%202015/" class="tag5">hindi 2015</a>
	<a href="/search/inside%20out/" class="tag2">inside out</a>
	<a href="/search/ita/" class="tag2">ita</a>
	<a href="/search/kat%20ph%20com/" class="tag5">kat ph com</a>
	<a href="/search/mad%20max/" class="tag6">mad max</a>
	<a href="/search/malayalam/" class="tag3">malayalam</a>
	<a href="/search/metal%20gear%20solid%20v/" class="tag2">metal gear solid v</a>
	<a href="/search/mia%20khalifa/" class="tag1">mia khalifa</a>
	<a href="/search/minions/" class="tag2">minions</a>
	<a href="/search/movies/" class="tag2">movies</a>
	<a href="/search/mr%20robot/" class="tag2">mr robot</a>
	<a href="/search/narcos/" class="tag2">narcos</a>
	<a href="/search/nezu/" class="tag10">nezu</a>
	<a href="/search/nl/" class="tag2">nl</a>
	<a href="/search/pixels/" class="tag2">pixels</a>
	<a href="/search/ray%20donovan/" class="tag2">ray donovan</a>
	<a href="/search/rick%20and%20morty/" class="tag3">rick and morty</a>
	<a href="/search/rick%20and%20morty%20s02e07/" class="tag2">rick and morty s02e07</a>
	<a href="/search/ripsalot/" class="tag5">ripsalot</a>
	<a href="/search/southpaw/" class="tag1">southpaw</a>
	<a href="/search/star%20wars/" class="tag2">star wars</a>
	<a href="/search/straight%20outta%20compton/" class="tag2">straight outta compton</a>
	<a href="/search/tamil/" class="tag4">tamil</a>
	<a href="/search/telugu/" class="tag3">telugu</a>
	<a href="/search/telugu%202015/" class="tag2">telugu 2015</a>
	<a href="/search/the%20strain/" class="tag3">the strain</a>
	<a href="/search/the%20strain%20s02e10/" class="tag2">the strain s02e10</a>
	<a href="/search/the%20visit/" class="tag2">the visit</a>
	<a href="/search/the%20walking%20dead/" class="tag2">the walking dead</a>
	<a href="/search/under%20the%20dome/" class="tag2">under the dome</a>
	<a href="/search/windows%2010/" class="tag1">windows 10</a>
	<a href="/search/yify/" class="tag10">yify</a>
	<a href="/search/yify%201080p/" class="tag4">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag5">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11247152,0" class="icommentjs icon16" href="/forbidden-kingdom-2015-dvdrip-x264-ac3-5-1-ddr-t11247152.html#comment"> <em class="iconvalue">68</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/forbidden-kingdom-2015-dvdrip-x264-ac3-5-1-ddr-t11247152.html" class="cellMainLink">Forbidden Kingdom (2015) DvDRip x264 AC3 5.1 [DDR]</a></div>
			</td>
			<td class="nobr center" data-sort="1582007539">1.47 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="12 Sep 2015, 21:20">3&nbsp;days</span></td>
			<td class="green center">6244</td>
			<td class="red lasttd center">6036</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11245376,0" class="icommentjs icon16" href="/monster-hunt-2015-hd720p-x264-aac-mandarin-chs-eng-mp4ba-t11245376.html#comment"> <em class="iconvalue">75</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/monster-hunt-2015-hd720p-x264-aac-mandarin-chs-eng-mp4ba-t11245376.html" class="cellMainLink">Monster Hunt 2015 HD720P X264 AAC Mandarin CHS-ENG Mp4Ba</a></div>
			</td>
			<td class="nobr center" data-sort="1783627898">1.66 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span title="12 Sep 2015, 12:05">3&nbsp;days</span></td>
			<td class="green center">4010</td>
			<td class="red lasttd center">6811</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11245585,0" class="icommentjs icon16" href="/swat-unit-887-2015-dvdrip-xvid-etrg-t11245585.html#comment"> <em class="iconvalue">41</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/swat-unit-887-2015-dvdrip-xvid-etrg-t11245585.html" class="cellMainLink">SWAT Unit 887 2015 DVDRip XviD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="739772779">705.5 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="12 Sep 2015, 12:55">3&nbsp;days</span></td>
			<td class="green center">4698</td>
			<td class="red lasttd center">2815</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11249471,0" class="icommentjs icon16" href="/alien-anthology-special-edition-1080p-bluray-ac3-x264-etrg-t11249471.html#comment"> <em class="iconvalue">43</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/alien-anthology-special-edition-1080p-bluray-ac3-x264-etrg-t11249471.html" class="cellMainLink">Alien Anthology Special Edition 1080p BluRay AC3 x264-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="16479432645">15.35 <span>GB</span></td>
			<td class="center">52</td>
			<td class="center"><span title="13 Sep 2015, 08:13">2&nbsp;days</span></td>
			<td class="green center">838</td>
			<td class="red lasttd center">3494</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11250041,0" class="icommentjs icon16" href="/blood-moon-2014-hdrip-xvid-ac3-evo-t11250041.html#comment"> <em class="iconvalue">23</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/blood-moon-2014-hdrip-xvid-ac3-evo-t11250041.html" class="cellMainLink">Blood Moon 2014 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1473321875">1.37 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="13 Sep 2015, 11:17">2&nbsp;days</span></td>
			<td class="green center">2230</td>
			<td class="red lasttd center">2097</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11255369,0" class="icommentjs icon16" href="/spooks-the-greater-good-2015-hdrip-xvid-ac3-etrg-t11255369.html#comment"> <em class="iconvalue">24</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/spooks-the-greater-good-2015-hdrip-xvid-ac3-etrg-t11255369.html" class="cellMainLink">Spooks The Greater Good 2015 HDRip XViD AC3-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="1485852543">1.38 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="14 Sep 2015, 07:24">1&nbsp;day</span></td>
			<td class="green center">1959</td>
			<td class="red lasttd center">1884</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11243617,0" class="icommentjs icon16" href="/manglehorn-2014-720p-brrip-x264-yify-t11243617.html#comment"> <em class="iconvalue">27</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/manglehorn-2014-720p-brrip-x264-yify-t11243617.html" class="cellMainLink">Manglehorn (2014) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="790650039">754.02 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="12 Sep 2015, 05:15">3&nbsp;days</span></td>
			<td class="green center">1978</td>
			<td class="red lasttd center">1405</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11255096,0" class="icommentjs icon16" href="/jian-bing-man-2015-hd1080p-x264-aac-mandarin-chs-eng-mp4ba-t11255096.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/jian-bing-man-2015-hd1080p-x264-aac-mandarin-chs-eng-mp4ba-t11255096.html" class="cellMainLink">Jian Bing Man 2015 HD1080P X264 AAC Mandarin CHS-ENG Mp4Ba</a></div>
			</td>
			<td class="nobr center" data-sort="2548646759">2.37 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span title="14 Sep 2015, 06:08">1&nbsp;day</span></td>
			<td class="green center">896</td>
			<td class="red lasttd center">2487</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11255823,0" class="icommentjs icon16" href="/anti-social-2015-hdrip-xvid-ac3-etrg-t11255823.html#comment"> <em class="iconvalue">19</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/anti-social-2015-hdrip-xvid-ac3-etrg-t11255823.html" class="cellMainLink">Anti-Social 2015 HDRip XViD AC3-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="1502316833">1.4 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="14 Sep 2015, 09:43">1&nbsp;day</span></td>
			<td class="green center">1179</td>
			<td class="red lasttd center">1211</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11249322,0" class="icommentjs icon16" href="/mr-right-2015-hdrip-xvid-ac3-evo-t11249322.html#comment"> <em class="iconvalue">19</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/mr-right-2015-hdrip-xvid-ac3-evo-t11249322.html" class="cellMainLink">Mr Right 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1512300882">1.41 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="13 Sep 2015, 07:30">2&nbsp;days</span></td>
			<td class="green center">972</td>
			<td class="red lasttd center">1327</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11257357,0" class="icommentjs icon16" href="/ghost-squad-2015-hdrip-xvid-ac3-evo-t11257357.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ghost-squad-2015-hdrip-xvid-ac3-evo-t11257357.html" class="cellMainLink">Ghost Squad 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1484807342">1.38 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="14 Sep 2015, 16:03">1&nbsp;day</span></td>
			<td class="green center">796</td>
			<td class="red lasttd center">1352</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11252539,0" class="icommentjs icon16" href="/lucky-number-2015-hdrip-xvid-ac3-evo-t11252539.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/lucky-number-2015-hdrip-xvid-ac3-evo-t11252539.html" class="cellMainLink">Lucky Number 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1504182793">1.4 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="13 Sep 2015, 20:17">2&nbsp;days</span></td>
			<td class="green center">1055</td>
			<td class="red lasttd center">944</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11251792,0" class="icommentjs icon16" href="/detective-k-secret-of-the-lost-island-2015-bluray-720p-dts-x264-epic-t11251792.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/detective-k-secret-of-the-lost-island-2015-bluray-720p-dts-x264-epic-t11251792.html" class="cellMainLink">Detective K Secret of the Lost Island 2015 BluRay 720p DTS x264-EPiC</a></div>
			</td>
			<td class="nobr center" data-sort="5247549614">4.89 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="13 Sep 2015, 17:39">2&nbsp;days</span></td>
			<td class="green center">862</td>
			<td class="red lasttd center">912</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11255109,0" class="icommentjs icon16" href="/fantastic-four-2015-hd720p-x264-aac-english-chs-eng-mp4ba-t11255109.html#comment"> <em class="iconvalue">19</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fantastic-four-2015-hd720p-x264-aac-english-chs-eng-mp4ba-t11255109.html" class="cellMainLink">Fantastic Four 2015 HD720P X264 AAC English CHS-ENG Mp4Ba</a></div>
			</td>
			<td class="nobr center" data-sort="2224029818">2.07 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span title="14 Sep 2015, 06:12">1&nbsp;day</span></td>
			<td class="green center">493</td>
			<td class="red lasttd center">958</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11253102,0" class="icommentjs icon16" href="/hero-2015-1cd-desiscr-rip-x264-ddr-t11253102.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/hero-2015-1cd-desiscr-rip-x264-ddr-t11253102.html" class="cellMainLink">HERO (2015) 1CD DesiSCR Rip x264 [DDR]</a></div>
			</td>
			<td class="nobr center" data-sort="819137170">781.19 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="13 Sep 2015, 23:35">2&nbsp;days</span></td>
			<td class="green center">314</td>
			<td class="red lasttd center">948</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11254409,0" class="icommentjs icon16" href="/the-strain-s02e10-hdtv-x264-batv-ettv-t11254409.html#comment"> <em class="iconvalue">79</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-strain-s02e10-hdtv-x264-batv-ettv-t11254409.html" class="cellMainLink">The Strain S02E10 HDTV x264-BATV[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="276785046">263.96 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="14 Sep 2015, 03:17">1&nbsp;day</span></td>
			<td class="green center">7623</td>
			<td class="red lasttd center">1019</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11253928,0" class="icommentjs icon16" href="/ray-donovan-s03e10-hdtv-x264-lol-ettv-t11253928.html#comment"> <em class="iconvalue">72</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ray-donovan-s03e10-hdtv-x264-lol-ettv-t11253928.html" class="cellMainLink">Ray Donovan S03E10 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="323062416">308.1 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="14 Sep 2015, 01:59">1&nbsp;day</span></td>
			<td class="green center">4457</td>
			<td class="red lasttd center">1537</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11243089,0" class="icommentjs icon16" href="/z-nation-s02e01-hdtv-x264-killers-ettv-t11243089.html#comment"> <em class="iconvalue">125</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/z-nation-s02e01-hdtv-x264-killers-ettv-t11243089.html" class="cellMainLink">Z Nation S02E01 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="477606441">455.48 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="12 Sep 2015, 03:10">3&nbsp;days</span></td>
			<td class="green center">2667</td>
			<td class="red lasttd center">964</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11243553,0" class="icommentjs icon16" href="/hand-of-god-s01-web-dl-xvid-fum-ettv-t11243553.html#comment"> <em class="iconvalue">35</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/hand-of-god-s01-web-dl-xvid-fum-ettv-t11243553.html" class="cellMainLink">Hand of God S01 WEB-DL XviD-FUM[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="4623584535">4.31 <span>GB</span></td>
			<td class="center">11</td>
			<td class="center"><span title="12 Sep 2015, 05:05">3&nbsp;days</span></td>
			<td class="green center">1707</td>
			<td class="red lasttd center">1084</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11242601,0" class="icommentjs icon16" href="/continuum-s04e02-hdtv-x264-killers-ettv-t11242601.html#comment"> <em class="iconvalue">62</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/continuum-s04e02-hdtv-x264-killers-ettv-t11242601.html" class="cellMainLink">Continuum S04E02 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="360064685">343.38 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="12 Sep 2015, 02:12">3&nbsp;days</span></td>
			<td class="green center">1859</td>
			<td class="red lasttd center">535</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11254319,0" class="icommentjs icon16" href="/fear-the-walking-dead-s01e03-720p-mhd-dailyflix-xvid-avi-t11254319.html#comment"> <em class="iconvalue">17</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fear-the-walking-dead-s01e03-720p-mhd-dailyflix-xvid-avi-t11254319.html" class="cellMainLink">Fear The Walking Dead S01E03 720p mHD DailyFliX XviD avi</a></div>
			</td>
			<td class="nobr center" data-sort="706128270">673.42 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="14 Sep 2015, 02:48">1&nbsp;day</span></td>
			<td class="green center">1596</td>
			<td class="red lasttd center">730</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11249856,0" class="icommentjs icon16" href="/floyd-mayweather-jr-vs-andre-berto-full-event-hdtv-x264-fight-bb-t11249856.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/floyd-mayweather-jr-vs-andre-berto-full-event-hdtv-x264-fight-bb-t11249856.html" class="cellMainLink">Floyd Mayweather Jr vs Andre Berto Full Event HDTV x264 Fight-BB</a></div>
			</td>
			<td class="nobr center" data-sort="3217801564">3 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="13 Sep 2015, 10:22">2&nbsp;days</span></td>
			<td class="green center">654</td>
			<td class="red lasttd center">436</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11259676,0" class="icommentjs icon16" href="/awkward-s05e03-hdtv-x264-2hd-ettv-t11259676.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/awkward-s05e03-hdtv-x264-2hd-ettv-t11259676.html" class="cellMainLink">Awkward S05E03 HDTV x264-2HD[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="182055733">173.62 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="15 Sep 2015, 01:37">21&nbsp;hours</span></td>
			<td class="green center">420</td>
			<td class="red lasttd center">464</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11259764,0" class="icommentjs icon16" href="/faking-it-s02e13-hdtv-x264-killers-ettv-t11259764.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/faking-it-s02e13-hdtv-x264-killers-ettv-t11259764.html" class="cellMainLink">Faking It S02E13 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="163566301">155.99 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="15 Sep 2015, 02:11">21&nbsp;hours</span></td>
			<td class="green center">387</td>
			<td class="red lasttd center">321</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11259634,0" class="icommentjs icon16" href="/switched-at-birth-s04e14-hdtv-x264-killers-ettv-t11259634.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/switched-at-birth-s04e14-hdtv-x264-killers-ettv-t11259634.html" class="cellMainLink">Switched at Birth S04E14 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="337521669">321.89 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="15 Sep 2015, 01:20">22&nbsp;hours</span></td>
			<td class="green center">267</td>
			<td class="red lasttd center">390</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11248593,0" class="icommentjs icon16" href="/ë°¤ì-ê±·ë-ì ë¹-scholar-who-walks-the-night-720p-hdtv-x264-aac-sodihd-complete-english-subtitles-added-to-description-t11248593.html#comment"> <em class="iconvalue">42</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ë°¤ì-ê±·ë-ì ë¹-scholar-who-walks-the-night-720p-hdtv-x264-aac-sodihd-complete-english-subtitles-added-to-description-t11248593.html" class="cellMainLink">ë°¤ì ê±·ë ì ë¹ (Scholar Who Walks the Night) 720p HDTV x264 AAC-SODiHD Complete (English subtitles added to description)</a></div>
			</td>
			<td class="nobr center" data-sort="20566125630">19.15 <span>GB</span></td>
			<td class="center">20</td>
			<td class="center"><span title="13 Sep 2015, 03:48">2&nbsp;days</span></td>
			<td class="green center">212</td>
			<td class="red lasttd center">421</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11259781,0" class="icommentjs icon16" href="/chasing-life-s02e11-hdtv-x264-killers-ettv-t11259781.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/chasing-life-s02e11-hdtv-x264-killers-ettv-t11259781.html" class="cellMainLink">Chasing Life S02E11 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="338192295">322.53 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="15 Sep 2015, 02:17">21&nbsp;hours</span></td>
			<td class="green center">257</td>
			<td class="red lasttd center">357</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11260243,0" class="icommentjs icon16" href="/wwe-raw-2015-09-14-hdtv-x264-ebi-sparrow-t11260243.html#comment"> <em class="iconvalue">41</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/wwe-raw-2015-09-14-hdtv-x264-ebi-sparrow-t11260243.html" class="cellMainLink">WWE RAW 2015 09 14 HDTV x264-Ebi -={SPARROW}=-</a></div>
			</td>
			<td class="nobr center" data-sort="1499242348">1.4 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="15 Sep 2015, 04:42">18&nbsp;hours</span></td>
			<td class="green center">20</td>
			<td class="red lasttd center">418</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11253809,0" class="icommentjs icon16" href="/eu-a-patroa-e-as-crianÃ§as-5Âª-temporada-2005-dublado-720p-by-luanharper-tpf-t11253809.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/eu-a-patroa-e-as-crianÃ§as-5Âª-temporada-2005-dublado-720p-by-luanharper-tpf-t11253809.html" class="cellMainLink">Eu, a Patroa e as CrianÃ§as - 5Âª Temporada (2005) - Dublado 720p (By-LuanHarper TPF)</a></div>
			</td>
			<td class="nobr center" data-sort="4908386967">4.57 <span>GB</span></td>
			<td class="center">29</td>
			<td class="center"><span title="14 Sep 2015, 01:21">1&nbsp;day</span></td>
			<td class="green center">43</td>
			<td class="red lasttd center">351</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11260035,0" class="icommentjs icon16" href="/wwe-monday-night-raw-hdtv-2015-09-14-720p-avchd-sc-sdh-t11260035.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/wwe-monday-night-raw-hdtv-2015-09-14-720p-avchd-sc-sdh-t11260035.html" class="cellMainLink">WWE Monday Night Raw HDTV 2015-09-14 720p AVCHD-SC-SDH</a></div>
			</td>
			<td class="nobr center" data-sort="6176271768">5.75 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="15 Sep 2015, 03:51">19&nbsp;hours</span></td>
			<td class="green center">8</td>
			<td class="red lasttd center">322</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11246418,0" class="icommentjs icon16" href="/billboard-top-100-singles-80s-1980-1989-mp3-vbr-h4ckus-glodls-t11246418.html#comment"> <em class="iconvalue">21</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/billboard-top-100-singles-80s-1980-1989-mp3-vbr-h4ckus-glodls-t11246418.html" class="cellMainLink">Billboard - Top 100 Singles 80s (1980 - 1989) [MP3-VBR] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="5149102873">4.8 <span>GB</span></td>
			<td class="center">1006</td>
			<td class="center"><span title="12 Sep 2015, 17:05">3&nbsp;days</span></td>
			<td class="green center">406</td>
			<td class="red lasttd center">581</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11248757,0" class="icommentjs icon16" href="/guns-n-roses-the-ultimate-best-of-2-cd-2015-320ak-t11248757.html#comment"> <em class="iconvalue">15</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/guns-n-roses-the-ultimate-best-of-2-cd-2015-320ak-t11248757.html" class="cellMainLink">Guns N&#039; Roses - The Ultimate Best Of_...(2-CD) 2015 320ak</a></div>
			</td>
			<td class="nobr center" data-sort="513284411">489.51 <span>MB</span></td>
			<td class="center">41</td>
			<td class="center"><span title="13 Sep 2015, 04:39">2&nbsp;days</span></td>
			<td class="green center">489</td>
			<td class="red lasttd center">188</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11243199,0" class="icommentjs icon16" href="/slayer-repentless-limited-box-set-2-cd-2015-320ak-t11243199.html#comment"> <em class="iconvalue">18</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/slayer-repentless-limited-box-set-2-cd-2015-320ak-t11243199.html" class="cellMainLink">Slayer - Repentless [Limited Box Set 2-CD] (2015)320ak</a></div>
			</td>
			<td class="nobr center" data-sort="267267350">254.89 <span>MB</span></td>
			<td class="center">32</td>
			<td class="center"><span title="12 Sep 2015, 03:31">3&nbsp;days</span></td>
			<td class="green center">386</td>
			<td class="red lasttd center">63</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11251907,0" class="icommentjs icon16" href="/va-i-love-disco-80s-number-1-2015-mp3-320-kbps-t11251907.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-i-love-disco-80s-number-1-2015-mp3-320-kbps-t11251907.html" class="cellMainLink">VA - I Love Disco 80s Number 1 (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="583707837">556.67 <span>MB</span></td>
			<td class="center">76</td>
			<td class="center"><span title="13 Sep 2015, 17:54">2&nbsp;days</span></td>
			<td class="green center">195</td>
			<td class="red lasttd center">81</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11256273,0" class="icommentjs icon16" href="/mac-miller-go-od-am-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11256273.html#comment"> <em class="iconvalue">15</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/mac-miller-go-od-am-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11256273.html" class="cellMainLink">Mac Miller - GO:OD AM (2015) [MP3-320Kbps] [CBR] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="169993562">162.12 <span>MB</span></td>
			<td class="center">19</td>
			<td class="center"><span title="14 Sep 2015, 11:46">1&nbsp;day</span></td>
			<td class="green center">214</td>
			<td class="red lasttd center">58</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11247102,0" class="icommentjs icon16" href="/deep-tech-prog-electro-house-nudisco-techno-beatport-top-100-august-2015-2015-mp3-320-kbps-edm-rg-t11247102.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/deep-tech-prog-electro-house-nudisco-techno-beatport-top-100-august-2015-2015-mp3-320-kbps-edm-rg-t11247102.html" class="cellMainLink">(Deep/Tech/Prog/Electro House, NuDisco, Techno) Beatport Top 100 August 2015 (2015) MP3, 320 Kbps [EDM RG]</a></div>
			</td>
			<td class="nobr center" data-sort="1464747780">1.36 <span>GB</span></td>
			<td class="center">101</td>
			<td class="center"><span title="12 Sep 2015, 20:58">3&nbsp;days</span></td>
			<td class="green center">115</td>
			<td class="red lasttd center">56</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11251843,0" class="icommentjs icon16" href="/va-101-psy-trance-goa-hits-2015-mp3-320-kbps-t11251843.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-101-psy-trance-goa-hits-2015-mp3-320-kbps-t11251843.html" class="cellMainLink">VA - 101 Psy-Trance Goa Hits (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="1806108307">1.68 <span>GB</span></td>
			<td class="center">102</td>
			<td class="center"><span title="13 Sep 2015, 17:45">2&nbsp;days</span></td>
			<td class="green center">71</td>
			<td class="red lasttd center">60</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11244013,0" class="icommentjs icon16" href="/va-moon-flower-deep-compilation-house-2015-mp3-320-kbps-t11244013.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-moon-flower-deep-compilation-house-2015-mp3-320-kbps-t11244013.html" class="cellMainLink">VA - Moon Flower: Deep Compilation House (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="1596162712">1.49 <span>GB</span></td>
			<td class="center">101</td>
			<td class="center"><span title="12 Sep 2015, 06:46">3&nbsp;days</span></td>
			<td class="green center">73</td>
			<td class="red lasttd center">46</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11256224,0" class="icommentjs icon16" href="/tazzz-ayaan-rap-artist-hit-songs-collection-mp3-320kbps-groo-t11256224.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/tazzz-ayaan-rap-artist-hit-songs-collection-mp3-320kbps-groo-t11256224.html" class="cellMainLink">TaZzZ Ayaan (Rap Artist) Hit Songs Collection Mp3 320Kbps Groo</a></div>
			</td>
			<td class="nobr center" data-sort="148102628">141.24 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span title="14 Sep 2015, 11:30">1&nbsp;day</span></td>
			<td class="green center">44</td>
			<td class="red lasttd center">53</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11260094,0" class="icommentjs icon16" href="/howard-stern-show-wrap-up-09-14-15-t11260094.html#comment"> <em class="iconvalue">14</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/howard-stern-show-wrap-up-09-14-15-t11260094.html" class="cellMainLink">Howard Stern Show + Wrap Up 09-14-15</a></div>
			</td>
			<td class="nobr center" data-sort="198954087">189.74 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="15 Sep 2015, 04:05">19&nbsp;hours</span></td>
			<td class="green center">2</td>
			<td class="red lasttd center">75</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11257534,0" class="icommentjs icon16" href="/beatport-singles-14-09-2015-edm-electro-house-future-house-hardstyle-trance-progressive-house-t11257534.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/beatport-singles-14-09-2015-edm-electro-house-future-house-hardstyle-trance-progressive-house-t11257534.html" class="cellMainLink">Beatport Singles - 14.09.2015 (EDM,Electro House,Future House,Hardstyle,Trance,Progressive House)</a></div>
			</td>
			<td class="nobr center" data-sort="1177405460">1.1 <span>GB</span></td>
			<td class="center">99</td>
			<td class="center"><span title="14 Sep 2015, 16:54">1&nbsp;day</span></td>
			<td class="green center">25</td>
			<td class="red lasttd center">25</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11246422,0" class="icommentjs icon16" href="/bring-me-the-horizon-bmth-discography-2004-2015-mp3-320kbps-sn3h1t87-glodls-t11246422.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bring-me-the-horizon-bmth-discography-2004-2015-mp3-320kbps-sn3h1t87-glodls-t11246422.html" class="cellMainLink">Bring Me The Horizon [BMTH] - Discography [2004-2015] [MP3-320Kbps] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="2163856355">2.02 <span>GB</span></td>
			<td class="center">329</td>
			<td class="center"><span title="12 Sep 2015, 17:06">3&nbsp;days</span></td>
			<td class="green center">11</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/jawani-phir-nahi-ani-2015-pakistani-movie-full-album-mp3-320kbps-groo-t11259933.html" class="cellMainLink">Jawani Phir Nahi Ani (2015) Pakistani Movie Full Album Mp3 320Kbps Groo</a></div>
			</td>
			<td class="nobr center" data-sort="58169993">55.48 <span>MB</span></td>
			<td class="center">8</td>
			<td class="center"><span title="15 Sep 2015, 03:06">20&nbsp;hours</span></td>
			<td class="green center">2</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11258655,0" class="icommentjs icon16" href="/nu-jazz-fox-capture-plan-discography-2012-2015-jamal-the-moroccan-t11258655.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/nu-jazz-fox-capture-plan-discography-2012-2015-jamal-the-moroccan-t11258655.html" class="cellMainLink">[Nu Jazz] Fox Capture Plan - Discography 2012-2015 (Jamal The Moroccan)</a></div>
			</td>
			<td class="nobr center" data-sort="518935564">494.9 <span>MB</span></td>
			<td class="center">58</td>
			<td class="center"><span title="14 Sep 2015, 20:23">1&nbsp;day</span></td>
			<td class="green center">1</td>
			<td class="red lasttd center">25</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11257050,0" class="icommentjs icon16" href="/req-dmc-best-of-bootlegs-cut-up-s-collection-21-from-bjthedj-t11257050.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/req-dmc-best-of-bootlegs-cut-up-s-collection-21-from-bjthedj-t11257050.html" class="cellMainLink">Req. DMC Best of Bootlegs Cut-Up&#039;s Collection #21 from BJtheDJ</a></div>
			</td>
			<td class="nobr center" data-sort="198772313">189.56 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center"><span title="14 Sep 2015, 14:50">1&nbsp;day</span></td>
			<td class="green center">2</td>
			<td class="red lasttd center">22</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11251108,0" class="icommentjs icon16" href="/mad-max-v-1-0-1-1-3-dlc-2015-pc-repack-Ð¾Ñ-seyter-t11251108.html#comment"> <em class="iconvalue">253</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/mad-max-v-1-0-1-1-3-dlc-2015-pc-repack-Ð¾Ñ-seyter-t11251108.html" class="cellMainLink">Mad Max [v 1.0.1.1 + 3 DLC] (2015) PC | RePack Ð¾Ñ SEYTER</a></div>
			</td>
			<td class="nobr center" data-sort="4583950024">4.27 <span>GB</span></td>
			<td class="center">8</td>
			<td class="center"><span title="13 Sep 2015, 15:30">2&nbsp;days</span></td>
			<td class="green center">1156</td>
			<td class="red lasttd center">816</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11252878,0" class="icommentjs icon16" href="/mad-max-ripper-special-edition-2015-elamigos-v1-0-1-1-dlcs-3dm-v2-crack-3dm-v3-crack-available-t11252878.html#comment"> <em class="iconvalue">160</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/mad-max-ripper-special-edition-2015-elamigos-v1-0-1-1-dlcs-3dm-v2-crack-3dm-v3-crack-available-t11252878.html" class="cellMainLink">Mad Max Ripper Special Edition (2015) Elamigos (v1.0.1.1 + DLCs) [+ 3DM v2 crack] [3DM V3 crack available]</a></div>
			</td>
			<td class="nobr center" data-sort="7215603475">6.72 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="13 Sep 2015, 22:17">2&nbsp;days</span></td>
			<td class="green center">204</td>
			<td class="red lasttd center">365</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11246456,0" class="icommentjs icon16" href="/the-incredible-adventures-of-van-helsing-ii-complete-pack-gog-t11246456.html#comment"> <em class="iconvalue">13</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/the-incredible-adventures-of-van-helsing-ii-complete-pack-gog-t11246456.html" class="cellMainLink">The Incredible Adventures of Van Helsing II - Complete Pack (GOG)</a></div>
			</td>
			<td class="nobr center" data-sort="13175285474">12.27 <span>GB</span></td>
			<td class="center">9</td>
			<td class="center"><span title="12 Sep 2015, 17:18">3&nbsp;days</span></td>
			<td class="green center">82</td>
			<td class="red lasttd center">227</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11243586,0" class="icommentjs icon16" href="/nevertales-4-legends-ce-2015-pc-final-t11243586.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/nevertales-4-legends-ce-2015-pc-final-t11243586.html" class="cellMainLink">Nevertales 4: Legends CE (2015) PC [FINAL]</a></div>
			</td>
			<td class="nobr center" data-sort="814067441">776.36 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="12 Sep 2015, 05:10">3&nbsp;days</span></td>
			<td class="green center">166</td>
			<td class="red lasttd center">31</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11249770,0" class="icommentjs icon16" href="/ark-survival-evolved-v206-2-rldgames-t11249770.html#comment"> <em class="iconvalue">27</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/ark-survival-evolved-v206-2-rldgames-t11249770.html" class="cellMainLink">ARK Survival Evolved v206.2-RLDGAMES</a></div>
			</td>
			<td class="nobr center" data-sort="7655627169">7.13 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="13 Sep 2015, 09:55">2&nbsp;days</span></td>
			<td class="green center">73</td>
			<td class="red lasttd center">120</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11249519,0" class="icommentjs icon16" href="/euro-truck-simulator-2-v-1-20-1s-27-dlc-2013-pc-steam-rip-Ð¾Ñ-r-g-origins-t11249519.html#comment"> <em class="iconvalue">15</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/euro-truck-simulator-2-v-1-20-1s-27-dlc-2013-pc-steam-rip-Ð¾Ñ-r-g-origins-t11249519.html" class="cellMainLink">Euro Truck Simulator 2 [v 1.20.1s + 27 DLC] (2013) PC | Steam-Rip Ð¾Ñ R.G. Origins</a></div>
			</td>
			<td class="nobr center" data-sort="3423687629">3.19 <span>GB</span></td>
			<td class="center">43</td>
			<td class="center"><span title="13 Sep 2015, 08:32">2&nbsp;days</span></td>
			<td class="green center">123</td>
			<td class="red lasttd center">48</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11255418,0" class="icommentjs icon16" href="/mad-max-update-0-incl-dlc-and-crack-v3-3dm-t11255418.html#comment"> <em class="iconvalue">88</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/mad-max-update-0-incl-dlc-and-crack-v3-3dm-t11255418.html" class="cellMainLink">Mad Max Update 0 Incl DLC and Crack v3-3DM</a></div>
			</td>
			<td class="nobr center" data-sort="257415497">245.49 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="14 Sep 2015, 07:38">1&nbsp;day</span></td>
			<td class="green center">151</td>
			<td class="red lasttd center">18</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11250413,0" class="icommentjs icon16" href="/mass-effect-trilogy-corepack-t11250413.html#comment"> <em class="iconvalue">42</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/mass-effect-trilogy-corepack-t11250413.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/mass-effect-trilogy-corepack-t11250413.html" class="cellMainLink">Mass Effect Trilogy | CorePack</a></div>
			</td>
			<td class="nobr center" data-sort="21272702976">19.81 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="13 Sep 2015, 13:12">2&nbsp;days</span></td>
			<td class="green center">4</td>
			<td class="red lasttd center">154</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11258782,0" class="icommentjs icon16" href="/dragon-age-inquisition-all-dlc-t11258782.html#comment"> <em class="iconvalue">36</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/dragon-age-inquisition-all-dlc-t11258782.html" class="cellMainLink">Dragon Age Inquisition All DLC</a></div>
			</td>
			<td class="nobr center" data-sort="14209069550">13.23 <span>GB</span></td>
			<td class="center">663</td>
			<td class="center"><span title="14 Sep 2015, 21:21">1&nbsp;day</span></td>
			<td class="green center">12</td>
			<td class="red lasttd center">41</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11247580,0" class="icommentjs icon16" href="/destiny-the-taken-king-legendary-edition-xbox360-imars-t11247580.html#comment"> <em class="iconvalue">20</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/destiny-the-taken-king-legendary-edition-xbox360-imars-t11247580.html" class="cellMainLink">Destiny The Taken King Legendary Edition XBOX360-iMARS</a></div>
			</td>
			<td class="nobr center" data-sort="7913498539">7.37 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="12 Sep 2015, 23:58">2&nbsp;days</span></td>
			<td class="green center">14</td>
			<td class="red lasttd center">28</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11245775,0" class="icommentjs icon16" href="/subnautica-beta-2287-t11245775.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/subnautica-beta-2287-t11245775.html" class="cellMainLink">Subnautica - Beta 2287</a></div>
			</td>
			<td class="nobr center" data-sort="3587182781">3.34 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="12 Sep 2015, 13:54">3&nbsp;days</span></td>
			<td class="green center">25</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11259512,0" class="icommentjs icon16" href="/fault-milestone-two-side-above-visual-novel-english-japanese-t11259512.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/fault-milestone-two-side-above-visual-novel-english-japanese-t11259512.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/fault-milestone-two-side-above-visual-novel-english-japanese-t11259512.html" class="cellMainLink">Fault Milestone Two Side:Above [Visual Novel; English, Japanese]</a></div>
			</td>
			<td class="nobr center" data-sort="1654450869">1.54 <span>GB</span></td>
			<td class="center">862</td>
			<td class="center"><span title="15 Sep 2015, 00:21">23&nbsp;hours</span></td>
			<td class="green center">15</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11258285,0" class="icommentjs icon16" href="/farm-expert-2016-fruit-company-postmortem-t11258285.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/farm-expert-2016-fruit-company-postmortem-t11258285.html" class="cellMainLink">Farm Expert 2016 Fruit Company-POSTMORTEM</a></div>
			</td>
			<td class="nobr center" data-sort="5145821440">4.79 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="14 Sep 2015, 18:45">1&nbsp;day</span></td>
			<td class="green center">8</td>
			<td class="red lasttd center">20</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11251024,0" class="icommentjs icon16" href="/heavy-fire-afganistan-game-reload-game-double-pack-pc-games-by-mastiff-1-4-players-ctshon-ctrc-t11251024.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/heavy-fire-afganistan-game-reload-game-double-pack-pc-games-by-mastiff-1-4-players-ctshon-ctrc-t11251024.html" class="cellMainLink">Heavy Fire Afganistan Game-Reload Game(Double Pack PC Games by Mastiff 1-4 Players){CtShoN}[CTRC]</a></div>
			</td>
			<td class="nobr center" data-sort="2591834026">2.41 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="13 Sep 2015, 15:18">2&nbsp;days</span></td>
			<td class="green center">9</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11259770,0" class="icommentjs icon16" href="/unrest-special-edition-gog-t11259770.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/unrest-special-edition-gog-t11259770.html" class="cellMainLink">Unrest: Special Edition (GOG)</a></div>
			</td>
			<td class="nobr center" data-sort="2412214380">2.25 <span>GB</span></td>
			<td class="center">11</td>
			<td class="center"><span title="15 Sep 2015, 02:13">21&nbsp;hours</span></td>
			<td class="green center">6</td>
			<td class="red lasttd center">14</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11252624,0" class="icommentjs icon16" href="/microsoft-windows-10-pro-full-x86-sep-2015-techtools-t11252624.html#comment"> <em class="iconvalue">123</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/microsoft-windows-10-pro-full-x86-sep-2015-techtools-t11252624.html" class="cellMainLink">Microsoft Windows 10 PRO FULL (x86) Sep 2015 [TechTools]</a></div>
			</td>
			<td class="nobr center" data-sort="3622361785">3.37 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="13 Sep 2015, 20:53">2&nbsp;days</span></td>
			<td class="green center">807</td>
			<td class="red lasttd center">1281</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11245481,0" class="icommentjs icon16" href="/microsoft-office-2013-sp1-x64-pro-plus-vl-multi-15-generation2-t11245481.html#comment"> <em class="iconvalue">19</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/microsoft-office-2013-sp1-x64-pro-plus-vl-multi-15-generation2-t11245481.html" class="cellMainLink">Microsoft Office 2013 SP1 X64 Pro Plus VL MULTi-15 {Generation2}</a></div>
			</td>
			<td class="nobr center" data-sort="3939252362">3.67 <span>GB</span></td>
			<td class="center">18</td>
			<td class="center"><span title="12 Sep 2015, 12:38">3&nbsp;days</span></td>
			<td class="green center">148</td>
			<td class="red lasttd center">208</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11245069,0" class="icommentjs icon16" href="/collection-of-programs-portableapps-v-12-1-multilanguage-appzdam-t11245069.html#comment"> <em class="iconvalue">16</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/collection-of-programs-portableapps-v-12-1-multilanguage-appzdam-t11245069.html" class="cellMainLink">Collection of programs PortableApps v.12.1 Multilanguage - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="6471215087">6.03 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="12 Sep 2015, 10:53">3&nbsp;days</span></td>
			<td class="green center">151</td>
			<td class="red lasttd center">182</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11245222,0" class="icommentjs icon16" href="/apple-final-cut-pro-x-10-2-2-mac-os-x-coque599-t11245222.html#comment"> <em class="iconvalue">27</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/apple-final-cut-pro-x-10-2-2-mac-os-x-coque599-t11245222.html" class="cellMainLink">Apple Final Cut Pro X 10.2.2 [Mac Os X] [coque599]</a></div>
			</td>
			<td class="nobr center" data-sort="2837576464">2.64 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="12 Sep 2015, 11:30">3&nbsp;days</span></td>
			<td class="green center">155</td>
			<td class="red lasttd center">75</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11249649,0" class="icommentjs icon16" href="/windows-10-pro-x64-office15-en-us-sep-2015-generation2-t11249649.html#comment"> <em class="iconvalue">23</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-10-pro-x64-office15-en-us-sep-2015-generation2-t11249649.html" class="cellMainLink">Windows 10 Pro X64 Office15 en-US Sep 2015 {Generation2}</a></div>
			</td>
			<td class="nobr center" data-sort="4975743000">4.63 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="13 Sep 2015, 09:15">2&nbsp;days</span></td>
			<td class="green center">68</td>
			<td class="red lasttd center">151</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11252997,0" class="icommentjs icon16" href="/windows-password-rescuer-personal-6-0-0-1-x32-x64-eng-portable-t11252997.html#comment"> <em class="iconvalue">16</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-password-rescuer-personal-6-0-0-1-x32-x64-eng-portable-t11252997.html" class="cellMainLink">Windows Password Rescuer Personal 6.0.0.1 (x32/x64)[ENG][Portable]</a></div>
			</td>
			<td class="nobr center" data-sort="32207052">30.72 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="13 Sep 2015, 23:03">2&nbsp;days</span></td>
			<td class="green center">123</td>
			<td class="red lasttd center">25</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11247056,0" class="icommentjs icon16" href="/windows-xp-professional-sp3-x86-black-edition-2015-9-12-t11247056.html#comment"> <em class="iconvalue">21</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-xp-professional-sp3-x86-black-edition-2015-9-12-t11247056.html" class="cellMainLink">Windows XP Professional SP3 x86 - Black Edition 2015.9.12</a></div>
			</td>
			<td class="nobr center" data-sort="717225984">684 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="12 Sep 2015, 20:34">3&nbsp;days</span></td>
			<td class="green center">96</td>
			<td class="red lasttd center">33</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11252627,0" class="icommentjs icon16" href="/creativemarket-137771-mega-bundle-25-best-selling-products-fluck3r-t11252627.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/creativemarket-137771-mega-bundle-25-best-selling-products-fluck3r-t11252627.html" class="cellMainLink">[Creativemarket][137771] Mega Bundle - 25 Best Selling Products [Fluck3r]</a></div>
			</td>
			<td class="nobr center" data-sort="1545044618">1.44 <span>GB</span></td>
			<td class="center">25</td>
			<td class="center"><span title="13 Sep 2015, 20:55">2&nbsp;days</span></td>
			<td class="green center">60</td>
			<td class="red lasttd center">57</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11254863,0" class="icommentjs icon16" href="/tubemate-2-2-6-642-apk-adfree-material-design-md-mod-osmdroid-youtube-downloader-t11254863.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/tubemate-2-2-6-642-apk-adfree-material-design-md-mod-osmdroid-youtube-downloader-t11254863.html" class="cellMainLink">TubeMate 2.2.6.642 apk AdFree Material Design MD Mod {OsmDroid} | YouTube Downloader</a></div>
			</td>
			<td class="nobr center" data-sort="2533433">2.42 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="14 Sep 2015, 05:20">1&nbsp;day</span></td>
			<td class="green center">97</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11245859,0" class="icommentjs icon16" href="/windows-8-1-pro-vl-update-3-x86-en-us-esd-sept2015-pre-activated-team-os-t11245859.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-8-1-pro-vl-update-3-x86-en-us-esd-sept2015-pre-activated-team-os-t11245859.html" class="cellMainLink">Windows 8.1 Pro Vl Update 3 x86 En-Us ESD Sept2015 Pre-activated-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="3175408440">2.96 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span title="12 Sep 2015, 14:18">3&nbsp;days</span></td>
			<td class="green center">32</td>
			<td class="red lasttd center">62</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11245009,0" class="icommentjs icon16" href="/native-instruments-reaktor-6-os-x-pitchshifter-dada-t11245009.html#comment"> <em class="iconvalue">23</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/native-instruments-reaktor-6-os-x-pitchshifter-dada-t11245009.html" class="cellMainLink">Native Instruments - Reaktor 6 OS X [PiTcHsHiFTeR][dada]</a></div>
			</td>
			<td class="nobr center" data-sort="813630586">775.94 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="12 Sep 2015, 10:37">3&nbsp;days</span></td>
			<td class="green center">70</td>
			<td class="red lasttd center">11</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11259475,0" class="icommentjs icon16" href="/internet-ability-pro-v1-51-18-incl-keygen-r2r-deepstatus-t11259475.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/internet-ability-pro-v1-51-18-incl-keygen-r2r-deepstatus-t11259475.html" class="cellMainLink">Internet ABILITY Pro v1.51.18 Incl.Keygen-R2R [deepstatus]</a></div>
			</td>
			<td class="nobr center" data-sort="1533835761">1.43 <span>GB</span></td>
			<td class="center">18</td>
			<td class="center"><span title="15 Sep 2015, 00:11">23&nbsp;hours</span></td>
			<td class="green center">9</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11257520,0" class="icommentjs icon16" href="/windows-10-pro-home-insider-preview-build-10537-x64-en-us-t11257520.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-10-pro-home-insider-preview-build-10537-x64-en-us-t11257520.html" class="cellMainLink">Windows 10 Pro &amp; Home | Insider Preview | Build 10537 | x64 | en-us</a></div>
			</td>
			<td class="nobr center" data-sort="4395991040">4.09 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="14 Sep 2015, 16:49">1&nbsp;day</span></td>
			<td class="green center">14</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-server-2016-technical-preview-3-build-10537-x64-en-us-t11258742.html" class="cellMainLink">Windows Server 2016 | Technical Preview 3 | Build 10537 | x64 | en-us</a></div>
			</td>
			<td class="nobr center" data-sort="3733389312">3.48 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="14 Sep 2015, 21:00">1&nbsp;day</span></td>
			<td class="green center">7</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/aurora-blu-ray-media-player-2-18-4-x32-x64-multi-crack-t11260107.html" class="cellMainLink">Aurora Blu-ray Media Player 2.18.4 (x32/x64)[Multi][Crack]</a></div>
			</td>
			<td class="nobr center" data-sort="35355127">33.72 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="15 Sep 2015, 04:11">19&nbsp;hours</span></td>
			<td class="green center">2</td>
			<td class="red lasttd center">7</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11255359,0" class="icommentjs icon16" href="/annon-dragon-ball-super-10-french-subbed-mp4-t11255359.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/annon-dragon-ball-super-10-french-subbed-mp4-t11255359.html" class="cellMainLink">[ANNON] Dragon Ball Super - 10 [French Subbed].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="310643082">296.25 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="14 Sep 2015, 07:22">1&nbsp;day</span></td>
			<td class="green center">1626</td>
			<td class="red lasttd center">86</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11251237,0" class="icommentjs icon16" href="/animerg-dragon-ball-super-10-720p-mkv-t11251237.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-dragon-ball-super-10-720p-mkv-t11251237.html" class="cellMainLink">[AnimeRG] Dragon Ball Super - 10 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="375403390">358.01 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="13 Sep 2015, 15:49">2&nbsp;days</span></td>
			<td class="green center">731</td>
			<td class="red lasttd center">106</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/kaerizaki-fansub-one-piece-709-french-subbed-hd-1280x720-mp4-t11255356.html" class="cellMainLink">[Kaerizaki-Fansub]One Piece 709 [French Subbed][HD_1280x720].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="294807868">281.15 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="14 Sep 2015, 07:22">1&nbsp;day</span></td>
			<td class="green center">722</td>
			<td class="red lasttd center">49</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-to-love-ru-trouble-darkness-2nd-10-raw-bs11-1280x720-x264-aac-mp4-t11258603.html" class="cellMainLink">[Leopard-Raws] To Love-Ru - Trouble - Darkness 2nd - 10 RAW (BS11 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="526801427">502.4 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="14 Sep 2015, 20:00">1&nbsp;day</span></td>
			<td class="green center">514</td>
			<td class="red lasttd center">131</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-shimoneta-to-iu-gainen-ga-sonzai-shinai-taikutsu-na-sekai-11-raw-atx-1280x720-x264-aac-mp4-t11257343.html" class="cellMainLink">[Leopard-Raws] Shimoneta to Iu Gainen ga Sonzai Shinai Taikutsu na Sekai - 11 RAW (ATX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="371603829">354.39 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="14 Sep 2015, 16:00">1&nbsp;day</span></td>
			<td class="green center">391</td>
			<td class="red lasttd center">78</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-dia-no-ace-second-season-24-raw-tx-1280x720-x264-aac-mp4-t11258359.html" class="cellMainLink">[Leopard-Raws] Dia no Ace - Second Season - 24 RAW (TX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="375307037">357.92 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="14 Sep 2015, 19:10">1&nbsp;day</span></td>
			<td class="green center">285</td>
			<td class="red lasttd center">58</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-non-non-biyori-repeat-11-raw-tx-1280x720-x264-aac-mp4-t11260026.html" class="cellMainLink">[Leopard-Raws] Non Non Biyori - Repeat - 11 RAW (TX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="284652339">271.47 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="15 Sep 2015, 03:50">19&nbsp;hours</span></td>
			<td class="green center">162</td>
			<td class="red lasttd center">139</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-sore-ga-seiyuu-11-720p-mkv-t11263621.html" class="cellMainLink">[HorribleSubs] Sore ga Seiyuu! - 11 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="434032453">413.93 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="15 Sep 2015, 20:40">2&nbsp;hours</span></td>
			<td class="green center">23</td>
			<td class="red lasttd center">255</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-akagami-no-shirayukihime-11-raw-tva-1280x720-x264-aac-mp4-t11260050.html" class="cellMainLink">[Leopard-Raws] Akagami no Shirayukihime - 11 RAW (TVA 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="358966289">342.34 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="15 Sep 2015, 03:55">19&nbsp;hours</span></td>
			<td class="green center">114</td>
			<td class="red lasttd center">112</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11249735,0" class="icommentjs icon16" href="/dragon-ball-super-episode-010-english-subbed-720p-arizone-t11249735.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dragon-ball-super-episode-010-english-subbed-720p-arizone-t11249735.html" class="cellMainLink">DRAGON BALL SUPER Episode - 010 [ENGLISH SUBBED] 720p ~ARIZONE</a></div>
			</td>
			<td class="nobr center" data-sort="182805722">174.34 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="13 Sep 2015, 09:45">2&nbsp;days</span></td>
			<td class="green center">172</td>
			<td class="red lasttd center">53</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-jitsu-wa-watashi-wa-11-raw-tx-1280x720-x264-aac-mp4-t11260027.html" class="cellMainLink">[Leopard-Raws] Jitsu wa Watashi wa - 11 RAW (TX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="270903317">258.35 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="15 Sep 2015, 03:50">19&nbsp;hours</span></td>
			<td class="green center">110</td>
			<td class="red lasttd center">103</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11262166,0" class="icommentjs icon16" href="/vivid-non-non-biyori-repeat-11-204fd015-mkv-t11262166.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/vivid-non-non-biyori-repeat-11-204fd015-mkv-t11262166.html" class="cellMainLink">[Vivid] Non Non Biyori Repeat - 11 [204FD015].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="336298372">320.72 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="15 Sep 2015, 14:00">9&nbsp;hours</span></td>
			<td class="green center">28</td>
			<td class="red lasttd center">114</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11250018,0" class="icommentjs icon16" href="/animerg-tokyo-ravens-bd-8-bit-x264-720p-ac3-dual-audio-xcelent-t11250018.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-tokyo-ravens-bd-8-bit-x264-720p-ac3-dual-audio-xcelent-t11250018.html" class="cellMainLink">[AnimeRG] Tokyo Ravens BD (8-bit x264 720p AC3) [Dual-Audio] [Xcelent]</a></div>
			</td>
			<td class="nobr center" data-sort="11766137549">10.96 <span>GB</span></td>
			<td class="center">24</td>
			<td class="center"><span title="13 Sep 2015, 11:12">2&nbsp;days</span></td>
			<td class="green center">28</td>
			<td class="red lasttd center">87</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11253785,0" class="icommentjs icon16" href="/fate-stay-night-unlimited-blade-works-dub-00-12-1080p-bd-x264-dushikushi-t11253785.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fate-stay-night-unlimited-blade-works-dub-00-12-1080p-bd-x264-dushikushi-t11253785.html" class="cellMainLink">Fate/Stay Night - Unlimited Blade Works (Dub) 00-12 1080p BD x264 - DushiKushi</a></div>
			</td>
			<td class="nobr center" data-sort="5154426604">4.8 <span>GB</span></td>
			<td class="center">13</td>
			<td class="center"><span title="14 Sep 2015, 01:09">1&nbsp;day</span></td>
			<td class="green center">51</td>
			<td class="red lasttd center">60</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11252980,0" class="icommentjs icon16" href="/welcome-to-the-space-show-2010-1080p-jpn-5-1-eng-5-1-blu-ray-t11252980.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/welcome-to-the-space-show-2010-1080p-jpn-5-1-eng-5-1-blu-ray-t11252980.html" class="cellMainLink">Welcome to the Space Show (2010) 1080p [Jpn 5.1 &amp; Eng 5.1] Blu-ray</a></div>
			</td>
			<td class="nobr center" data-sort="3650818557">3.4 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="13 Sep 2015, 22:52">2&nbsp;days</span></td>
			<td class="green center">38</td>
			<td class="red lasttd center">50</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11249330,0" class="icommentjs icon16" href="/assorted-magazines-bundle-september-13-2015-true-pdf-t11249330.html#comment"> <em class="iconvalue">13</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/assorted-magazines-bundle-september-13-2015-true-pdf-t11249330.html" class="cellMainLink">Assorted Magazines Bundle - September 13 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="403819210">385.11 <span>MB</span></td>
			<td class="center">29</td>
			<td class="center"><span title="13 Sep 2015, 07:31">2&nbsp;days</span></td>
			<td class="green center">397</td>
			<td class="red lasttd center">211</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11249333,0" class="icommentjs icon16" href="/computer-gadget-gamer-mags-sept-13-2015-true-pdf-t11249333.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/computer-gadget-gamer-mags-sept-13-2015-true-pdf-t11249333.html" class="cellMainLink">Computer Gadget &amp; Gamer Mags - Sept 13 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="267580375">255.18 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span title="13 Sep 2015, 07:32">2&nbsp;days</span></td>
			<td class="green center">337</td>
			<td class="red lasttd center">138</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11249338,0" class="icommentjs icon16" href="/photography-magazines-september-13-2015-true-pdf-t11249338.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/photography-magazines-september-13-2015-true-pdf-t11249338.html" class="cellMainLink">Photography Magazines - September 13 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="170316071">162.43 <span>MB</span></td>
			<td class="center">11</td>
			<td class="center"><span title="13 Sep 2015, 07:33">2&nbsp;days</span></td>
			<td class="green center">333</td>
			<td class="red lasttd center">83</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11253279,0" class="icommentjs icon16" href="/athletic-sports-magazines-september-14-2015-true-pdf-t11253279.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/athletic-sports-magazines-september-14-2015-true-pdf-t11253279.html" class="cellMainLink">Athletic &amp; Sports Magazines - September 14 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="221567177">211.3 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span title="13 Sep 2015, 23:49">1&nbsp;day</span></td>
			<td class="green center">165</td>
			<td class="red lasttd center">67</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11249354,0" class="icommentjs icon16" href="/womens-magazines-bundle-sptemeber-11-2015-true-pdf-t11249354.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/womens-magazines-bundle-sptemeber-11-2015-true-pdf-t11249354.html" class="cellMainLink">Womens Magazines Bundle - Sptemeber 11 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="375700650">358.3 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span title="13 Sep 2015, 07:37">2&nbsp;days</span></td>
			<td class="green center">162</td>
			<td class="red lasttd center">68</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11252914,0" class="icommentjs icon16" href="/tabloid-magazines-bundle-september-14-2015-true-pdf-t11252914.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/tabloid-magazines-bundle-september-14-2015-true-pdf-t11252914.html" class="cellMainLink">Tabloid Magazines Bundle - September 14 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="159454933">152.07 <span>MB</span></td>
			<td class="center">13</td>
			<td class="center"><span title="13 Sep 2015, 22:27">2&nbsp;days</span></td>
			<td class="green center">142</td>
			<td class="red lasttd center">52</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11258565,0" class="icommentjs icon16" href="/graphic-design-the-new-basics-2nd-edition-revised-and-expanded-2015-pdf-epub-gooner-t11258565.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/graphic-design-the-new-basics-2nd-edition-revised-and-expanded-2015-pdf-epub-gooner-t11258565.html" class="cellMainLink">Graphic Design - The New Basics 2nd Edition, Revised and Expanded (2015) (Pdf &amp; Epub) Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="361385279">344.64 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="14 Sep 2015, 19:48">1&nbsp;day</span></td>
			<td class="green center">121</td>
			<td class="red lasttd center">36</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11247455,0" class="icommentjs icon16" href="/hercules-collection-1965-2012-nem-t11247455.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/hercules-collection-1965-2012-nem-t11247455.html" class="cellMainLink">Hercules Collection (1965-2012) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="2187153194">2.04 <span>GB</span></td>
			<td class="center">94</td>
			<td class="center"><span title="12 Sep 2015, 23:10">3&nbsp;days</span></td>
			<td class="green center">99</td>
			<td class="red lasttd center">44</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11257309,0" class="icommentjs icon16" href="/top-20-usa-today-s-best-selling-books-09-06-15-epub-mobi-t11257309.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/top-20-usa-today-s-best-selling-books-09-06-15-epub-mobi-t11257309.html" class="cellMainLink">TOP 20 USA TODAY&#039;s Best-Selling Books 09-06-15 [.epub, .mobi]</a></div>
			</td>
			<td class="nobr center" data-sort="56649426">54.03 <span>MB</span></td>
			<td class="center">43</td>
			<td class="center"><span title="14 Sep 2015, 15:48">1&nbsp;day</span></td>
			<td class="green center">106</td>
			<td class="red lasttd center">37</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11255800,0" class="icommentjs icon16" href="/nations-the-long-history-and-deep-roots-of-political-ethnicity-and-nationalism-true-pdf-prg-pdf-t11255800.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/nations-the-long-history-and-deep-roots-of-political-ethnicity-and-nationalism-true-pdf-prg-pdf-t11255800.html" class="cellMainLink">Nations The Long History and Deep Roots of Political Ethnicity and Nationalism True PDF {PRG}.pdf</a></div>
			</td>
			<td class="nobr center" data-sort="3760382">3.59 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="14 Sep 2015, 09:35">1&nbsp;day</span></td>
			<td class="green center">0</td>
			<td class="red lasttd center">128</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11255814,0" class="icommentjs icon16" href="/encyclopedia-of-philosophers-on-religion-true-pdf-prg-pdf-t11255814.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/encyclopedia-of-philosophers-on-religion-true-pdf-prg-pdf-t11255814.html" class="cellMainLink">Encyclopedia of Philosophers on Religion True PDF {PRG}.pdf</a></div>
			</td>
			<td class="nobr center" data-sort="3733965">3.56 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="14 Sep 2015, 09:40">1&nbsp;day</span></td>
			<td class="green center">1</td>
			<td class="red lasttd center">120</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11255770,0" class="icommentjs icon16" href="/the-aims-of-education-routledge-international-studies-in-the-philosophy-of-education-1st-edition-true-pdf-prg-pdf-t11255770.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-aims-of-education-routledge-international-studies-in-the-philosophy-of-education-1st-edition-true-pdf-prg-pdf-t11255770.html" class="cellMainLink">The Aims of Education (Routledge International Studies in the Philosophy of Education) 1st Edition True PDF {PRG}.pdf</a></div>
			</td>
			<td class="nobr center" data-sort="4973199">4.74 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="14 Sep 2015, 09:28">1&nbsp;day</span></td>
			<td class="green center">1</td>
			<td class="red lasttd center">110</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11255824,0" class="icommentjs icon16" href="/configurational-forces-thermomechanics-physics-mathematics-and-numerics-modern-mechanics-and-mathematics-1st-edition-true-pdf-prg-pdf-t11255824.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/configurational-forces-thermomechanics-physics-mathematics-and-numerics-modern-mechanics-and-mathematics-1st-edition-true-pdf-prg-pdf-t11255824.html" class="cellMainLink">Configurational Forces Thermomechanics, Physics, Mathematics, and Numerics (Modern Mechanics and Mathematics) 1st Edition True PDF {PRG}.pdf</a></div>
			</td>
			<td class="nobr center" data-sort="8893969">8.48 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="14 Sep 2015, 09:43">1&nbsp;day</span></td>
			<td class="green center">0</td>
			<td class="red lasttd center">103</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11260319,0" class="icommentjs icon16" href="/30-books-by-maggie-shayne-t11260319.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/30-books-by-maggie-shayne-t11260319.html" class="cellMainLink">30 Books by Maggie Shayne</a></div>
			</td>
			<td class="nobr center" data-sort="37045322">35.33 <span>MB</span></td>
			<td class="center">44</td>
			<td class="center"><span title="15 Sep 2015, 05:09">18&nbsp;hours</span></td>
			<td class="green center">0</td>
			<td class="red lasttd center">82</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/squadron-supreme-001-012-2008-2009-digital-anpymgold-empire-nem-t11259994.html" class="cellMainLink">Squadron Supreme (001-012) (2008-2009) (Digital) (AnPymGold-Empire) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="244367270">233.05 <span>MB</span></td>
			<td class="center">12</td>
			<td class="center"><span title="15 Sep 2015, 03:40">19&nbsp;hours</span></td>
			<td class="green center">2</td>
			<td class="red lasttd center">31</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11245384,0" class="icommentjs icon16" href="/santana-santana-iii-2014-24-96-hd-flac-t11245384.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/santana-santana-iii-2014-24-96-hd-flac-t11245384.html" class="cellMainLink">Santana - Santana III (2014) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="947965948">904.05 <span>MB</span></td>
			<td class="center">24</td>
			<td class="center"><span title="12 Sep 2015, 12:08">3&nbsp;days</span></td>
			<td class="green center">133</td>
			<td class="red lasttd center">48</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11258276,0" class="icommentjs icon16" href="/top-100-60-s-rock-albums-by-ultimateclassicrock-cd-s-76-100-flac-t11258276.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/top-100-60-s-rock-albums-by-ultimateclassicrock-cd-s-76-100-flac-t11258276.html" class="cellMainLink">Top 100 &#039;60&#039;s Rock Albums by UltimateClassicRock - CD&#039;s 76-100 [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="7812764049">7.28 <span>GB</span></td>
			<td class="center">564</td>
			<td class="center"><span title="14 Sep 2015, 18:40">1&nbsp;day</span></td>
			<td class="green center">2</td>
			<td class="red lasttd center">154</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11249947,0" class="icommentjs icon16" href="/the-band-rock-of-ages-2015-24-192-hd-flac-t11249947.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-band-rock-of-ages-2015-24-192-hd-flac-t11249947.html" class="cellMainLink">The Band - Rock of Ages (2015) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="3406490411">3.17 <span>GB</span></td>
			<td class="center">41</td>
			<td class="center"><span title="13 Sep 2015, 10:58">2&nbsp;days</span></td>
			<td class="green center">109</td>
			<td class="red lasttd center">45</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11249069,0" class="icommentjs icon16" href="/black-sabbath-studio-albums-japan-remastered-1970-2013-flac-ikar911-t11249069.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/black-sabbath-studio-albums-japan-remastered-1970-2013-flac-ikar911-t11249069.html" class="cellMainLink">Black Sabbath - Studio Albums [JAPAN Remastered] 1970-2013 FLAC ikar911</a></div>
			</td>
			<td class="nobr center" data-sort="9620713345">8.96 <span>GB</span></td>
			<td class="center">286</td>
			<td class="center"><span title="13 Sep 2015, 06:03">2&nbsp;days</span></td>
			<td class="green center">56</td>
			<td class="red lasttd center">85</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11256165,0" class="icommentjs icon16" href="/keith-jarrett-mysteries-2015-24-192-hd-flac-t11256165.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/keith-jarrett-mysteries-2015-24-192-hd-flac-t11256165.html" class="cellMainLink">Keith Jarrett - Mysteries (2015) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1752749838">1.63 <span>GB</span></td>
			<td class="center">14</td>
			<td class="center"><span title="14 Sep 2015, 11:12">1&nbsp;day</span></td>
			<td class="green center">112</td>
			<td class="red lasttd center">25</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11245716,0" class="icommentjs icon16" href="/chick-corea-bela-fleck-two-2015-flac-24-92-t11245716.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/chick-corea-bela-fleck-two-2015-flac-24-92-t11245716.html" class="cellMainLink">Chick Corea &amp; Bela Fleck - Two (2015) FLAC 24-92</a></div>
			</td>
			<td class="nobr center" data-sort="2182639745">2.03 <span>GB</span></td>
			<td class="center">21</td>
			<td class="center"><span title="12 Sep 2015, 13:33">3&nbsp;days</span></td>
			<td class="green center">99</td>
			<td class="red lasttd center">35</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11249034,0" class="icommentjs icon16" href="/va-my-favourite-hits-of-1979-15cd-2015-flac-Ð¾Ñ-don-music-t11249034.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-my-favourite-hits-of-1979-15cd-2015-flac-Ð¾Ñ-don-music-t11249034.html" class="cellMainLink">VA - My Favourite Hits of 1979 [15CD] (2015) FLAC Ð¾Ñ DON Music</a></div>
			</td>
			<td class="nobr center" data-sort="7899166461">7.36 <span>GB</span></td>
			<td class="center">253</td>
			<td class="center"><span title="13 Sep 2015, 05:58">2&nbsp;days</span></td>
			<td class="green center">47</td>
			<td class="red lasttd center">83</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11246055,0" class="icommentjs icon16" href="/linda-ronstadt-hasten-down-the-wind-2014-24-192-hd-flac-t11246055.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/linda-ronstadt-hasten-down-the-wind-2014-24-192-hd-flac-t11246055.html" class="cellMainLink">Linda Ronstadt - Hasten Down The Wind (2014) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1630198546">1.52 <span>GB</span></td>
			<td class="center">30</td>
			<td class="center"><span title="12 Sep 2015, 15:09">3&nbsp;days</span></td>
			<td class="green center">98</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11252346,0" class="icommentjs icon16" href="/thelonious-monk-genius-of-modern-music-vol-1-2013-24-192-hd-flac-t11252346.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/thelonious-monk-genius-of-modern-music-vol-1-2013-24-192-hd-flac-t11252346.html" class="cellMainLink">Thelonious Monk - Genius of Modern Music Vol.1 (2013) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="821477404">783.42 <span>MB</span></td>
			<td class="center">30</td>
			<td class="center"><span title="13 Sep 2015, 19:21">2&nbsp;days</span></td>
			<td class="green center">86</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11257048,0" class="icommentjs icon16" href="/maria-callas-verdi-puccini-opera-pack-vinyl-yeraycito-master-series-t11257048.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/maria-callas-verdi-puccini-opera-pack-vinyl-yeraycito-master-series-t11257048.html" class="cellMainLink">Maria Callas (Verdi-Puccini) - Opera Pack (VINYL) YERAYCITO MASTER SERIES</a></div>
			</td>
			<td class="nobr center" data-sort="24755554990">23.06 <span>GB</span></td>
			<td class="center">525</td>
			<td class="center"><span title="14 Sep 2015, 14:50">1&nbsp;day</span></td>
			<td class="green center">1</td>
			<td class="red lasttd center">88</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-prog-magazine-compilations-62cd-2009-2015-flac-Ð¾Ñ-new-team-t11249061.html" class="cellMainLink">VA - PROG Magazine Compilations [62CD] (2009-2015) FLAC Ð¾Ñ New-Team</a></div>
			</td>
			<td class="nobr center" data-sort="27692890210">25.79 <span>GB</span></td>
			<td class="center">1125</td>
			<td class="center"><span title="13 Sep 2015, 06:02">2&nbsp;days</span></td>
			<td class="green center">14</td>
			<td class="red lasttd center">44</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11252343,0" class="icommentjs icon16" href="/reggae-steel-pulse-rastafari-centennial-live-in-paris-1992-flac-jamal-the-moroccan-t11252343.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/reggae-steel-pulse-rastafari-centennial-live-in-paris-1992-flac-jamal-the-moroccan-t11252343.html" class="cellMainLink">[Reggae] Steel Pulse - Rastafari Centennial - Live In Paris 1992 FLAC (Jamal The Moroccan)</a></div>
			</td>
			<td class="nobr center" data-sort="508800831">485.23 <span>MB</span></td>
			<td class="center">23</td>
			<td class="center"><span title="13 Sep 2015, 19:21">2&nbsp;days</span></td>
			<td class="green center">38</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11258809,0" class="icommentjs icon16" href="/country-folk-gretchen-peters-blackbirds-2015-flac-jamal-the-moroccan-t11258809.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/country-folk-gretchen-peters-blackbirds-2015-flac-jamal-the-moroccan-t11258809.html" class="cellMainLink">[Country, Folk] Gretchen Peters - Blackbirds 2015 FLAC (Jamal The Moroccan)</a></div>
			</td>
			<td class="nobr center" data-sort="450599700">429.73 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span title="14 Sep 2015, 21:40">1&nbsp;day</span></td>
			<td class="green center">1</td>
			<td class="red lasttd center">37</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11259536,0" class="icommentjs icon16" href="/phish-2015-09-05-dick-s-day-2-flac-t11259536.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/phish-2015-09-05-dick-s-day-2-flac-t11259536.html" class="cellMainLink">PHiSH 2015-09-05 Dick&#039;s Day 2 [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1211843282">1.13 <span>GB</span></td>
			<td class="center">23</td>
			<td class="center"><span title="15 Sep 2015, 00:29">23&nbsp;hours</span></td>
			<td class="green center">1</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/lauryn-hill-the-miseducation-of-lauryn-hill-flac-1998-t11259899.html" class="cellMainLink">Lauryn Hill - The Miseducation of Lauryn Hill (FLAC) - 1998</a></div>
			</td>
			<td class="nobr center" data-sort="484919838">462.46 <span>MB</span></td>
			<td class="center">32</td>
			<td class="center"><span title="15 Sep 2015, 02:57">20&nbsp;hours</span></td>
			<td class="green center">7</td>
			<td class="red lasttd center">14</td>
        </tr>
			</table>


	<script type="text/javascript">
		if ($.cookie('kat_settings[fpw_column]') != null && $.cookie('kat_settings[fpw_order]')) {
			var colIndex = $.cookie('kat_settings[fpw_column]')
			var ascend = $.cookie('kat_settings[fpw_order]');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			sortTables(colIndex, ascend);
		}
		$('.frontPageWidget .firstr th .fpw-sort').click(function() {
			var colIndex = $(this).parent().index();
			var ascend = !$(this).hasClass('sortedAsc')?1:0;
			$('.fpw-sort').removeClass('sortedAsc sortedDesc');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			updateFPWsettings(colIndex, ascend);
			sortTables(colIndex, ascend);
		});
		function sortTables(colIndex, ascend) {	
			$('.frontPageWidget').each(function() {
				var data = [];
				$('tr:not(.firstr)', $(this)).each(function() {
					var name = $('td:first .cellMainLink', $(this)).text();
					var id = $('td:first .cellMainLink', $(this)).attr('href').match(/.*-t(\d+)\.html/)[1];
					var fileSize = $('td:eq(1)', $(this)).attr('data-sort');
					data.push({
						'col0':name, 'col1':padNum(fileSize), 'col2':padNum($('td:eq(2)',$(this)).text()), 'col3':padNum(id), 'col4':padNum($('td:eq(4)',$(this)).text()), 'col5':padNum($('td:eq(5)',$(this)).text()), 'html':$(this).html()
					});
				});
				data.sort(function(a, b) {
					var x = a['col'+colIndex].toLowerCase(); var y = b['col'+colIndex].toLowerCase();
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
				if (!ascend||ascend=='0') data.reverse();
				$(this).find('tr:not(.firstr)').remove();
				for (var i=0;i<data.length;i++) {
					$(this).append('<tr class="'+(i+1%2==0?'even':'odd')+'">'+data[i].html+'</tr>');
				}
			});
		}
		function padNum(val) {
			val = '0000000000000000'+val;
			return val.substring(val.length-15);
		}
		function updateFPWsettings(t, o) {
			$.cookie('kat_settings[fpw_column]', t, { expires: 365, path: '/' });
			$.cookie('kat_settings[fpw_order]', o, { expires: 365, path: '/' });
		}
	</script>
		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
    
            
        <div id="_119b0a17fab5493361a252d04bf527db"></div>
    
                
    	    <div class="advertising">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <div id="_7063408f1c01d50e0dc2d833186ce962"></div>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/kickass-anime-community-v-6/?unread=16900522">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Kickass Anime Community V.6!
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/Harem_King/">Harem_King</a></span></span> <span title="15 Sep 2015, 23:35">1&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/adopt-uploader-program-v11-all-users-help/?unread=16900520">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				**Adopt an uploader Program v11-- For all Users to Help**
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_verified"><a class="plain" href="/user/Joel/">Joel</a></span></span> <span title="15 Sep 2015, 23:35">2&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/all-karaoke-requests-here-v2/?unread=16900517">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				All Karaoke Requests Here V2
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/MFord76/">MFord76</a></span></span> <span title="15 Sep 2015, 23:34">2&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/what-are-you-listening-right-now-v4-thread-108003/?unread=16900515">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What are you listening to right now? V4
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/MasterTovic/">MasterTovic</a></span></span> <span title="15 Sep 2015, 23:34">2&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/relaxin-v-2/?unread=16900514">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				ReLaXin !! V.2
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/MasterTovic/">MasterTovic</a></span></span> <span title="15 Sep 2015, 23:34">3&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/please-request-ebooks-and-audio-books-here-v11/?unread=16900513">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Please request ebooks and audio books here. V11
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/Nymphee/">Nymphee</a></span></span> <span title="15 Sep 2015, 23:33">3&nbsp;min.&nbsp;ago</span></span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/summer-updates-september-1/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Summer updates [September, 1]
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="01 Sep 2015, 16:13">2&nbsp;weeks&nbsp;ago</span></span>
	</li>
	<li>
		<a href="/blog/post/kickasstorrents-is-moving-to-kat-cr-domain/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				KickassTorrents is moving to kat.cr domain
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="24 Apr 2015, 10:13">4&nbsp;months&nbsp;ago</span></span>
	</li>
	<li>
		<a href="/blog/post/happy-torrents-day-2015/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Happy Torrents Day 2015!
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="30 Mar 2015, 10:06">5&nbsp;months&nbsp;ago</span></span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/SirSeedsAlot/post/fire-in-the-hole-burning-animals/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Fire in the Hole: Burning Animals</p></a><span class="explanation">by <a class="plain aclColor_3" href="/user/SirSeedsAlot/">SirSeedsAlot</a> <span title="15 Sep 2015, 22:46">50&nbsp;min.&nbsp;ago</span></span></li>
	<li><a href="/blog/magicpotions/post/thus-time-flies-by-watertiger/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Thus time flies by watertiger</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/magicpotions/">magicpotions</a> <span title="15 Sep 2015, 21:39">1&nbsp;hour&nbsp;ago</span></span></li>
	<li><a href="/blog/QueenSuccubus/post/one-side-of-life/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> One Side Of Life</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/QueenSuccubus/">QueenSuccubus</a> <span title="15 Sep 2015, 19:41">3&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/OptimusPr1me/post/2015-formula-1-singapore-airlines-singapore-grand-prix/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> 2015 FORMULA 1 SINGAPORE AIRLINES SINGAPORE GRAND PRIX</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/OptimusPr1me/">OptimusPr1me</a> <span title="15 Sep 2015, 12:51">10&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/olderthangod/post/chapter-2-of-7/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Chapter 2 of 7</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/olderthangod/">olderthangod</a> <span title="15 Sep 2015, 00:32">23&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/stuffytick9/post/the-biggest-lesson-i-have-learned-blog-hosted-for-audiobookbrn/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> The biggest lesson I have learned, Blog hosted for AudioBookBrn</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/stuffytick9/">stuffytick9</a> <span title="14 Sep 2015, 16:33">yesterday</span></span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/tatty18/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				tatty18
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/benfica/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				benfica
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/dancing%20stars%20s21e01/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				dancing stars s21e01
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/granny%20720p/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				granny 720p
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/discografia%20forro/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				discografia forro
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/dublado/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				dublado
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/alien%3A%20isolation/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Alien: Isolation
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/conquest/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				conquest
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/new%20girl%20complete%203/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				new girl complete 3
			</p>
		</a>
				<span class="explanation">1&nbsp;sec.&nbsp;ago</span>
	</li>

	<li>
		<a href="/search/bladerunner/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				bladerunner
			</p>
		</a>
				<span class="explanation">1&nbsp;sec.&nbsp;ago</span>
	</li>

	<li>
		<a href="/search/from%20dusk%20till%20dawn/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				from dusk till dawn
			</p>
		</a>
				<span class="explanation">1&nbsp;sec.&nbsp;ago</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		<li>
			<a data-nop href="http://www.torrentreactor.net/" target="_blank" rel="external">
				<span class="itorreact thirdPartIcons"></span>TorrentReactor
			</a>
		</li>
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a onclick="setLanguage('bsc', '.kat.cr');return false;" class="plain">Bosnian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a onclick="setLanguage('ko', '.kat.cr');return false;" class="plain">Korean</a></li>
                                <li><a onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
		<li><a href="/welcome/">welcome</a></li>
		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <div id="_673e31f53f8166159b8e996c4124765b"></div>
        <div id="_e7050fb15fd39b3e4e99a5be4a57b6ea"></div>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script><script>
 sc('addGlobal', 'pagetype', 'front');
 sc('addSlot', '_60318cd4e8d28f6fb76fe34e9bd9c498');
sc('addSlot', '_39ecb76dd457e5ac33776fdf11500d56');
sc('addSlot', '_277923e5f9d753c5b0630c28e641790c');
sc('addSlot', '_119b0a17fab5493361a252d04bf527db');
sc('addSlot', '_7063408f1c01d50e0dc2d833186ce962', { 'searchQuery': '' });
sc('addSlot', '_673e31f53f8166159b8e996c4124765b');
sc('addSlot', '_e7050fb15fd39b3e4e99a5be4a57b6ea');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>

</body>
</html>
