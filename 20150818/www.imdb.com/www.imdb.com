



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>

                    
                    
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "598-9567037-7671748";
                var ue_id = "06M5SJ7P421ZFCXESVK9";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        
    
        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />
    
    <meta property='fb:app_id' content='115109575169727' />
    
    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="06M5SJ7P421ZFCXESVK9" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1e-c3-2xl-i-2856b181.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-4179705684._CB314325221_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-485555676._CB318510215_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-247421025._CB318510254_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-3978168775._CB318510288_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-1541130848._CB318510255_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
(function(){var d=function(o){return Object.prototype.toString.call(o)==="[object Array]";},g=function(q,p){var o;for(o=0;o<q.length;o++){if(o in q){p.call(null,q[o],o);}}},h=[],k,b,l=false,n=false,f=function(){var o=[],p=[],q={};g(h,function(s){var r="";g(s.dartsite.split("/"),function(t){if(t!==""){if(t in q){}else{q[t]=o.length;o.push(t);}r+="/"+q[t];}});p.push(r);});return{iu_parts:o,enc_prev_ius:p};},c=function(){var o=[];g(h,function(q){var p=[];g(q.sizes,function(r){p.push(r.join("x"));});o.push(p.join("|"));});return o;},m=function(){var o=[];g(h,function(p){o.push(a(p.targeting));});return o.join("|");},a=function(r,o){var q,p,s=[];for(q in r){p=[];for(j=0;j<r[q].length;j++){p.push(encodeURIComponent(r[q][j]));}if(o){s.push(q+"="+encodeURIComponent(p.join(",")));}else{s.push(q+"="+p.join(","));}}return s.join("&");},e=function(){var o=+new Date();if(n){return;}if(!this.readyState||"loaded"===this.readyState){n=true;if(l){imdbads.cmd.push(function(){for(i=0;i<h.length;i++){generic.monitoring.record_metric(h[i].name+".fail",csm.duration(o));}});}}};window.tinygpt={define_slot:function(r,q,o,p){h.push({dartsite:r.replace(/\/$/,""),sizes:q,name:o,targeting:p});},set_targeting:function(o){k=o;},callback:function(q){var r,p={},t,o,s=+new Date();l=false;for(r=0;r<h.length;r++){t=h[r].dartsite;o=h[r].name;if(q[r][t]){p[o]=q[r][t];}else{window.console&&console.error&&console.error("Unable to correlate GPT response for "+o);}}imdbads.cmd.push(function(){for(r=0;r<h.length;r++){ad_utils.slot_events.trigger(h[r].name,"request",{timestamp:b});ad_utils.slot_events.trigger(h[r].name,"tagdeliver",{timestamp:s});}ad_utils.gpt.handle_response(p);});},send:function(){var r=[],q=function(s,t){if(d(t)){t=t.join(",");}if(t){r.push(s+"="+encodeURIComponent(""+t));}},o,p;if(h.length===0){tinygpt.callback({});return;}q("gdfp_req","1");q("correlator",Math.floor(4503599627370496*Math.random()));q("output","json_html");q("callback","tinygpt.callback");q("impl","fifs");q("json_a","1");result=f();q("iu_parts",result.iu_parts);q("enc_prev_ius",result.enc_prev_ius);q("prev_iu_szs",c());q("prev_scp",m());q("cust_params",a(k,true));o=document.createElement("script");p=document.getElementsByTagName("script")[0];o.async=true;o.type="text/javascript";o.src="http://pubads.g.doubleclick.net/gampad/ads?"+r.join("&");o.id="tinygpt";o.onload=o.onerror=o.onreadystatechange=e;l=true;p.parentNode.insertBefore(o,p);b=+new Date();}};})();</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['e'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['2'],
'u': ['358576858078'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-3480509068._CB314075722_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"08c50efaf26bba998016a21b23ab7c1665a35653",
"2015-08-17T23%3A52%3A41GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 25639;
generic.days_to_midnight = 0.2967476844787598;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-1582251138._CB317085808_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'e']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm === undefined || window.generic === undefined || window.consoleLog === undefined)) {
                    if (console !== undefined && console.log !== undefined) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=e;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=358576858078;ord=358576858078?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=e;bpx=2;c=0;s=3075;s=32;ord=358576858078?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=e;bpx=2;c=0;s=3075;s=32;ord=358576858078?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
onsubmit="(new Image()).src='/rg/SEARCH-BOX/HEADER/images/b.gif?link=/find';"
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/imdbpicks/monthly-tv-picks/?ref_=nv_tp_tvpicks_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/search/title?count=100&title_type=feature,tv_series,tv_movie&explore=title_type,genres,year,countries&ref_=nv_ch_mm_1"
>Popular Movies & TV</a></li>
                        <li><a href="/chart/?ref_=nv_ch_cht_2"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_3"
>Oscar Winners</a></li>
                        <li><a href="/chart/top?ref_=nv_ch_250_4"
>Top 250 Movies</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_5"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/imdbpicks/monthly-tv-picks/?ref_=nv_tvv_picks_1"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_2"
>On Tonight</a></li>
                            <li><a href="/chart/toptv?ref_=nv_tvv_250_3"
>Top 250 TV</a></li>
                        <li><a href="/list/ls074093523/?ref_=nv_tvv_wn_4"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_5"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/academymuseum/?ref_=nv_sf_am_1"
>Academy Museum</a></li>
                        <li><a href="/imdbpicks/?ref_=nv_sf_pks_2"
>IMDb Picks</a></li>
                        <li><a href="/best-of/?ref_=nv_sf_bo_3"
>Best of 2014</a></li>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_4"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/whattowatch/?ref_=nv_sf_wtw_5"
>What to Watch</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=08-17&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_2"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_3"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_4"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_5"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_6"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_7"
>Road to the Emmys</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_8"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_9"
>Toronto Film Festival</a></li>
                        <li><a href="/awards-central/?ref_=nv_ev_all_10"
>More Popular Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni58909240/?ref_=nv_nw_tn_1"
> Emile Hirsch Gets 15 Days In Jail Over Paramount Exec Assault At Sundance â Update
</a><br />
                        <span class="time">4 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni58908338/?ref_=nv_nw_tn_2"
> Tracy Morgan, Amy Schumer, Miley Cyrus Set to Host 'Saturday Night Live'
</a><br />
                        <span class="time">7 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni58908296/?ref_=nv_nw_tn_3"
> Who should return to host Oscars? Neil Patrick Harris, Ellen DeGeneres, Chris Rock ...
</a><br />
                        <span class="time">7 hours ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/games/guess?ref_=nv_cm_qz_3"
>Quiz Game</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_4"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5> 
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0053125/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTMyMTYyNjIzMF5BMl5BanBnXkFtZTcwODE2ODUwNA@@._V1._SY335_CR30,20,410,315_CT10_.jpg",
            titleYears : "1959",
            rank : 67,
                    headline : "North by Northwest"
    },
    nameAd : {
            clickThru : "/name/nm0147147/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTM1NDAwODcxMl5BMl5BanBnXkFtZTcwMjM4NTIwNw@@._V1._SX250_CR0,0,250,315_BR-10_.jpg",
            rank : 10,
            headline : "Henry Cavill"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYvzXEavk1qOZpko8rsdEb01ZVxTSyNeqW0E6Q-o62GGkbE4LzdMB23LCK8X830M88cdA-Sp47J%0D%0ALjfw_jUPS_rEeDbSSbiMZULPCM0np5BeahgwMJEy_HySuaJ1z0oE66UQjkVoIQ-G-IJAihev_c2U%0D%0Ai-rgod6CYmxADwh4NsOx0K1_lNPHd73NDOb3Uc8EwNwSHT_98B8wD0fR5S_-OyxdzQ%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYnjNvAFb58LOTZT6yPv_4CR9I8BGZ0heBPT4WIOc8acyha8AHDoPBsYPmnDW3LvhQAirTD3ESU%0D%0AwF13QiGvwSFiz58iqrv4ePuKoPO1qdJ_TnAX-2WsGSyJ2PrEOkl81Cm6Y_3wabl31XpMTmFy6lwP%0D%0ACaolY1wWOoPN0Z0cm8Sqd2SLZACicNwL6chDMGvoUCKw%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYnRYE5xdg9gRNnq2StBqjRWtCei1v56FC31Imwu2jsHRCdxuKjC-UEa97UMhvR0lP5sPXvGKMj%0D%0AO0Gr5ZkCqMOimnDn8UIl6IiJjgfafRqEseu9Tr9ZEG7GLHvQbI2NsjllcvKrDgFM7uY_beUFWJT6%0D%0AvjjG4DpVMHCZAorCyjLrI1UO5pqm6Fge7_GV2rMHQex65mDXqAVJUzVEZstOR_hmcA%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=358576858078;ord=358576858078?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>
                    <div id="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=358576858078;ord=358576858078?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	

                    
                    

<div id="content-2-wide">
    <div id="main">
                    
        <div class="heroWidget">
        <span class="ab_widget"
        >
            <div class="ab_hero">
<div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3218846489?ref_=hm_hp_i_1" data-video="vi3218846489" data-source="bylist" data-id="ls074935490" data-rid="06M5SJ7P421ZFCXESVK9" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="Skylar Astin and other 2015 Teen Choice Nominees discuss their first IMDb credits." alt="Skylar Astin and other 2015 Teen Choice Nominees discuss their first IMDb credits." src="http://ia.media-imdb.com/images/M/MV5BMjI2MzA3MzI1NV5BMl5BanBnXkFtZTgwMjkxNzc1NjE@._V1_SX201_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI2MzA3MzI1NV5BMl5BanBnXkFtZTgwMjkxNzc1NjE@._V1_SX201_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Skylar Astin and other 2015 Teen Choice Nominees discuss their first IMDb credits." title="Skylar Astin and other 2015 Teen Choice Nominees discuss their first IMDb credits." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Skylar Astin and other 2015 Teen Choice Nominees discuss their first IMDb credits." title="Skylar Astin and other 2015 Teen Choice Nominees discuss their first IMDb credits." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt4940624/?ref_=hm_hp_cap_pri_1" > Teen Choice Nominees </a> </div> </div> <div class="secondary ellipsis"> "My First IMDb Credit" </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1490858777?ref_=hm_hp_i_2" data-video="vi1490858777" data-source="bylist" data-id="ls002922459" data-rid="06M5SJ7P421ZFCXESVK9" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="Set in 1950s New York, a department-store clerk who dreams of a better life falls for an older, married woman." alt="Set in 1950s New York, a department-store clerk who dreams of a better life falls for an older, married woman." src="http://ia.media-imdb.com/images/M/MV5BMTkwMjIzOTIyNl5BMl5BanBnXkFtZTgwNzA4Nzc2NTE@._V1_SX201_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTkwMjIzOTIyNl5BMl5BanBnXkFtZTgwNzA4Nzc2NTE@._V1_SX201_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Set in 1950s New York, a department-store clerk who dreams of a better life falls for an older, married woman." title="Set in 1950s New York, a department-store clerk who dreams of a better life falls for an older, married woman." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Set in 1950s New York, a department-store clerk who dreams of a better life falls for an older, married woman." title="Set in 1950s New York, a department-store clerk who dreams of a better life falls for an older, married woman." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2402927/?ref_=hm_hp_cap_pri_2" > Carol </a> </div> </div> <div class="secondary ellipsis"> Trailer #1 </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2682041113?ref_=hm_hp_i_3" data-video="vi2682041113" data-source="bylist" data-id="ls056131825" data-rid="06M5SJ7P421ZFCXESVK9" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="Police detective Allison McLean finds her work and family life shaken up after her brother is sent to prison and she becomes guardian to his two teenagers." alt="Police detective Allison McLean finds her work and family life shaken up after her brother is sent to prison and she becomes guardian to his two teenagers." src="http://ia.media-imdb.com/images/M/MV5BMTUxMzI4NTMyMF5BMl5BanBnXkFtZTgwMDY1MDg1NjE@._V1_SY298_CR13,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUxMzI4NTMyMF5BMl5BanBnXkFtZTgwMDY1MDg1NjE@._V1_SY298_CR13,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Police detective Allison McLean finds her work and family life shaken up after her brother is sent to prison and she becomes guardian to his two teenagers." title="Police detective Allison McLean finds her work and family life shaken up after her brother is sent to prison and she becomes guardian to his two teenagers." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Police detective Allison McLean finds her work and family life shaken up after her brother is sent to prison and she becomes guardian to his two teenagers." title="Police detective Allison McLean finds her work and family life shaken up after her brother is sent to prison and she becomes guardian to his two teenagers." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt4671682/?ref_=hm_hp_cap_pri_3" > "Ties That Bind" </a> </div> </div> <div class="secondary ellipsis"> Series Trailer </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/trailers?ref_=hm_hp_sm" class="position_bottom supplemental" > Browse more trailers </a> </p>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="http://www.imdb.com/family-entertainment-guide/?ref_=hm_feg_hm_hd" > <h3>Family Entertainment Guide: Something for Everyone</h3> </a> </span> </span> <p class="blurb">Check out IMDb's comprehensive <a href="http://www.imdb.com/family-entertainment-guide/?ref_=hm_feg_hm_lk1">Family Entertainment Guide</a>, brought to you by Honda Pilot, where you'll get recommendations for movies and TV series for every age and every viewing platform. From the latest releases to childhood classics to streaming TV shows, there's definitely something for the entire family.</p> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/family-entertainment-guide/most-awesome-family-movies-to-stream?ref_=hm_feg_hm_i_1" > <img itemprop="image" class="pri_image" title="Harry Potter and the Half-Blood Prince (2009)" alt="Harry Potter and the Half-Blood Prince (2009)" src="http://ia.media-imdb.com/images/M/MV5BMTY0NTU3NzM1MV5BMl5BanBnXkFtZTcwNTIyODMyNw@@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY0NTU3NzM1MV5BMl5BanBnXkFtZTcwNTIyODMyNw@@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="http://www.imdb.com/family-entertainment-guide/most-awesome-family-movies-to-stream?ref_=hm_feg_hm_cap_pri_1" > Top 20 Most Awesome Streaming Family Movies </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/family-entertainment-guide/galleries/favorite-family-films-rm3398290432?ref_=hm_feg_hm_i_2" > <img itemprop="image" class="pri_image" title="E.T. the Extra-Terrestrial (1982)" alt="E.T. the Extra-Terrestrial (1982)" src="http://ia.media-imdb.com/images/M/MV5BNjU5MTg4MDUyNF5BMl5BanBnXkFtZTgwNTA3MTg5MTE@._V1_SY201_CR49,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjU5MTg4MDUyNF5BMl5BanBnXkFtZTgwNTA3MTg5MTE@._V1_SY201_CR49,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="http://www.imdb.com/family-entertainment-guide/galleries/favorite-family-films-rm3398290432?ref_=hm_feg_hm_cap_pri_2" > Photos We Love From Our Favorite Family Films </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/family-entertainment-guide/best-live-action-kids-movies/?ref_=hm_feg_hm_i_3" > <img itemprop="image" class="pri_image" title="The Goonies (1985)" alt="The Goonies (1985)" src="http://ia.media-imdb.com/images/M/MV5BMTI5Njg1NDIxMF5BMl5BanBnXkFtZTcwNzQ4NDIwNA@@._V1_SY201_CR48,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTI5Njg1NDIxMF5BMl5BanBnXkFtZTcwNzQ4NDIwNA@@._V1_SY201_CR48,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="http://www.imdb.com/family-entertainment-guide/best-live-action-kids-movies/?ref_=hm_feg_hm_cap_pri_3" > 15 Best Live Action Kids' Movies </a> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="http://www.imdb.com/family-entertainment-guide/?ref_=hm_feg_hm_sm" class="position_bottom supplemental" > Visit our Family Entertainment Guide </a> </p>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <h3>IMDb on the Scene: 2015 Teen Choice Awards</h3> </span> </span> <p class="blurb">We hit the blue carpet at the 2015 Teen Choice Awards to talk to the casts of <a href="http://www.imdb.com/title/tt3566726/?ref_=nv_sr_1">"Jane the Virgin,"</a> <a href="http://www.imdb.com/title/tt1578873/?ref_=nv_sr_1">"Pretty Little Liars,"</a> <a href="http://www.imdb.com/title/tt3107288/?ref_=nv_sr_1">"The Flash,"</a> and many more. The stars dropped juicy TV scoop and shared stories about their first IMDb credits and some hilarious secrets to the onscreen kiss. <a href="http://www.imdb.com/awards-central/video/teen-choice/?ref_=nv_sr_1">Watch all our coverage here.</a></p> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2480648985?ref_=hm_acd_tc_hm_i_1" data-video="vi2480648985" data-source="bylist" data-id="ls074935490" data-rid="06M5SJ7P421ZFCXESVK9" data-type="playlist" class="video-colorbox" data-refsuffix="hm_acd_tc_hm" data-ref="hm_acd_tc_hm_i_1"> <img itemprop="image" class="pri_image" title="IMDb on the Scene (2015-)" alt="IMDb on the Scene (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMTU4MjAxMDc3Nl5BMl5BanBnXkFtZTgwNzU3Njc1NjE@._V1._CR410,0,700,1080_SY172_SX116_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU4MjAxMDc3Nl5BMl5BanBnXkFtZTgwNzU3Njc1NjE@._V1._CR410,0,700,1080_SY172_SX116_AL_UY344_UX232_AL_.jpg" /> <img alt="IMDb on the Scene (2015-)" title="IMDb on the Scene (2015-)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="IMDb on the Scene (2015-)" title="IMDb on the Scene (2015-)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/awards-central/video/teen-choice?ref_=hm_acd_tc_hm_cap_pri_1" > Victoria Justice </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2312876825?ref_=hm_acd_tc_hm_i_2" data-video="vi2312876825" data-source="bylist" data-id="ls074935490" data-rid="06M5SJ7P421ZFCXESVK9" data-type="playlist" class="video-colorbox" data-refsuffix="hm_acd_tc_hm" data-ref="hm_acd_tc_hm_i_2"> <img itemprop="image" class="pri_image" title="IMDb on the Scene (2015-)" alt="IMDb on the Scene (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMzQwMDc5MzQzN15BMl5BanBnXkFtZTgwMDQ4NTc1NjE@._V1._CR380,0,700,1080_SY172_SX116_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzQwMDc5MzQzN15BMl5BanBnXkFtZTgwMDQ4NTc1NjE@._V1._CR380,0,700,1080_SY172_SX116_AL_UY344_UX232_AL_.jpg" /> <img alt="IMDb on the Scene (2015-)" title="IMDb on the Scene (2015-)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="IMDb on the Scene (2015-)" title="IMDb on the Scene (2015-)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/awards-central/video/teen-choice?ref_=hm_acd_tc_hm_cap_pri_2" > Candice Patton </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2145104665?ref_=hm_acd_tc_hm_i_3" data-video="vi2145104665" data-source="bylist" data-id="ls074935490" data-rid="06M5SJ7P421ZFCXESVK9" data-type="playlist" class="video-colorbox" data-refsuffix="hm_acd_tc_hm" data-ref="hm_acd_tc_hm_i_3"> <img itemprop="image" class="pri_image" title="IMDb on the Scene (2015-)" alt="IMDb on the Scene (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMTQ4MTk0NzAzMl5BMl5BanBnXkFtZTgwOTQ1NTc1NjE@._V1._CR380,0,700,1080_SY172_SX116_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ4MTk0NzAzMl5BMl5BanBnXkFtZTgwOTQ1NTc1NjE@._V1._CR380,0,700,1080_SY172_SX116_AL_UY344_UX232_AL_.jpg" /> <img alt="IMDb on the Scene (2015-)" title="IMDb on the Scene (2015-)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="IMDb on the Scene (2015-)" title="IMDb on the Scene (2015-)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/awards-central/video/teen-choice?ref_=hm_acd_tc_hm_cap_pri_3" > Justin Baldoni </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2329654041?ref_=hm_acd_tc_hm_i_4" data-video="vi2329654041" data-source="bylist" data-id="ls074935490" data-rid="06M5SJ7P421ZFCXESVK9" data-type="playlist" class="video-colorbox" data-refsuffix="hm_acd_tc_hm" data-ref="hm_acd_tc_hm_i_4"> <img itemprop="image" class="pri_image" title="IMDb on the Scene (2015-)" alt="IMDb on the Scene (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMjA0MjUyNjU3MF5BMl5BanBnXkFtZTgwMDQzNjc1NjE@._V1._CR400,0,700,1080_SY172_SX116_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA0MjUyNjU3MF5BMl5BanBnXkFtZTgwMDQzNjc1NjE@._V1._CR400,0,700,1080_SY172_SX116_AL_UY344_UX232_AL_.jpg" /> <img alt="IMDb on the Scene (2015-)" title="IMDb on the Scene (2015-)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="IMDb on the Scene (2015-)" title="IMDb on the Scene (2015-)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/awards-central/video/teen-choice?ref_=hm_acd_tc_hm_cap_pri_4" > Nominees on Favorite Movies </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1725674265?ref_=hm_acd_tc_hm_i_5" data-video="vi1725674265" data-source="bylist" data-id="ls074935490" data-rid="06M5SJ7P421ZFCXESVK9" data-type="playlist" class="video-colorbox" data-refsuffix="hm_acd_tc_hm" data-ref="hm_acd_tc_hm_i_5"> <img itemprop="image" class="pri_image" title="IMDb on the Scene (2015-)" alt="IMDb on the Scene (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMjE5NTkxNTc0MV5BMl5BanBnXkFtZTgwOTMzNjc1NjE@._V1._CR380,0,700,1080_SY172_SX116_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE5NTkxNTc0MV5BMl5BanBnXkFtZTgwOTMzNjc1NjE@._V1._CR380,0,700,1080_SY172_SX116_AL_UY344_UX232_AL_.jpg" /> <img alt="IMDb on the Scene (2015-)" title="IMDb on the Scene (2015-)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="IMDb on the Scene (2015-)" title="IMDb on the Scene (2015-)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/awards-central/video/teen-choice?ref_=hm_acd_tc_hm_cap_pri_5" > Beau Mirchoff </a> </div> </div> </div> </div> </div> </div> </div>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58909240?ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTY1NDM1OTQ4OV5BMl5BanBnXkFtZTgwNjE2NjYxNDE@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58909240?ref_=hm_nw_tp1"
class="headlines" >Emile Hirsch Gets 15 Days In Jail Over Paramount Exec Assault At Sundance â Update</a>
    <div class="infobar">
            <span class="text-muted">4 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?ref_=hm_nw_tp1_src"
>Deadline</a></span>
    </div>
                                </div>
<p>Updated with more from the hearing:Â FacingÂ 5-years behind bars forÂ felony and misdemeanor charges resulting from his violent and drunken January 25 nightclub attack on Paramount Digital VPÂ Daniele Bernfeld, <a href="/name/nm0386472?ref_=hm_nw_tp1_lk1">Emile Hirsch</a> instead began serving 15 days in jail starting today. With 90 days probation, ...                                        <span class="nobr"><a href="/news/ni58909240?ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58908338?ref_=hm_nw_tp2"
class="headlines" >Tracy Morgan, Amy Schumer, Miley Cyrus Set to Host 'Saturday Night Live'</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000001?ref_=hm_nw_tp2_src"
>The Hollywood Reporter</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58908296?ref_=hm_nw_tp3"
class="headlines" >Who should return to host Oscars? Neil Patrick Harris, Ellen DeGeneres, Chris Rock ...</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000143?ref_=hm_nw_tp3_src"
>Gold Derby</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58908668?ref_=hm_nw_tp4"
class="headlines" >X-Men: Apocalypse Will Include Yet Another Classic Mutant Villain</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000098?ref_=hm_nw_tp4_src"
>cinemablend.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58908217?ref_=hm_nw_tp5"
class="headlines" >Lionsgate Looking to Expand Local Remakes</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?ref_=hm_nw_tp5_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58908668?ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BNzExNTg1NDQ5Nl5BMl5BanBnXkFtZTgwNjg5NjcyNjE@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58908668?ref_=hm_nw_mv1"
class="headlines" >X-Men: Apocalypse Will Include Yet Another Classic Mutant Villain</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000098?ref_=hm_nw_mv1_src"
>cinemablend.com</a></span>
    </div>
                                </div>
<p><a href="/title/tt3385516?ref_=hm_nw_mv1_lk1">X-Men: Apocalypse</a> may have a lot of heroes in its cast, but there will be plenty of villains giving them trouble. As if the eponymous antagonist wasn.t bad enough, he will also be assisted by his Four Horsemen (Magneto, Storm, Psylocke and Angel), and William Stryker will also be returning to cause...                                        <span class="nobr"><a href="/news/ni58908668?ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58908296?ref_=hm_nw_mv2"
class="headlines" >Who should return to host Oscars? Neil Patrick Harris, Ellen DeGeneres, Chris Rock ...</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000143?ref_=hm_nw_mv2_src"
>Gold Derby</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58907901?ref_=hm_nw_mv3"
class="headlines" >Nascar Teams With Eugenio Derbez for Comedy Film</a>
    <div class="infobar">
            <span class="text-muted">10 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?ref_=hm_nw_mv3_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58908824?ref_=hm_nw_mv4"
class="headlines" >Takashi Miike's 'Yakuza Apocalypse' Gets Us Release</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000353?ref_=hm_nw_mv4_src"
>Thompson on Hollywood</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58908217?ref_=hm_nw_mv5"
class="headlines" >Lionsgate Looking to Expand Local Remakes</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?ref_=hm_nw_mv5_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58908753?ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BODE3Nzg1NzUyNl5BMl5BanBnXkFtZTcwNTI2ODgyNQ@@._V1_SY150_CR1,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58908753?ref_=hm_nw_tv1"
class="headlines" >Jerry Bruckheimer Drama âHomeâ Lands Pilot Order at TNT</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?ref_=hm_nw_tv1_src"
>The Wrap</a></span>
    </div>
                                </div>
<p>TNT has ordered a pilot for â<a href="/name/nm0000988?ref_=hm_nw_tv1_lk1">Jerry Bruckheimer</a>â dramatic thriller âHome.â The storyÂ delves into the secrets lingering behind the faÃ§ade of a seemingly idyllic suburban family. Here is the pilot description: Rose Altman couldnât be happier: An accomplished designer with a successful business, the ...                                        <span class="nobr"><a href="/news/ni58908753?ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58908810?ref_=hm_nw_tv2"
class="headlines" >Selena Gomez Joins 'The Voice'</a>
    <div class="infobar">
            <span class="text-muted">6 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000936?ref_=hm_nw_tv2_src"
>Access Hollywood</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58908313?ref_=hm_nw_tv3"
class="headlines" >Noah Emmerich of âThe Americansâ to Guest Star on Showtimeâs âBillionsâ</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?ref_=hm_nw_tv3_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58908338?ref_=hm_nw_tv4"
class="headlines" >Tracy Morgan, Amy Schumer, Miley Cyrus Set to Host 'Saturday Night Live'</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000001?ref_=hm_nw_tv4_src"
>The Hollywood Reporter</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58907950?ref_=hm_nw_tv5"
class="headlines" >Top 5 TV: 'Mr. Robot' Fries Our Circuits & HBO Shows Us a 'Hero'</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0044855?ref_=hm_nw_tv5_src"
>Rollingstone.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58907984?ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTM5NjA2NTYwNF5BMl5BanBnXkFtZTYwNDc4OTM0._V1_SY150_CR4,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58907984?ref_=hm_nw_cel1"
class="headlines" >Kathie Lee Gifford Returns to Today After Frank Gifford's Death, Shares Stories of Her Late Husband's Faith</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?ref_=hm_nw_cel1_src"
>E! Online</a></span>
    </div>
                                </div>
<p><a href="/name/nm0317537?ref_=hm_nw_cel1_lk1">Kathie Lee Gifford</a> is finding strength in the Lord. Following the Aug. 9 death of her husband, <a href="/name/nm0317520?ref_=hm_nw_cel1_lk2">Frank Gifford</a>, <a href="/name/nm0317537?ref_=hm_nw_cel1_lk3">Kathie Lee</a> returned to NBC's Today Monday after a weeklong absence, joining <a href="/name/nm1938316?ref_=hm_nw_cel1_lk4">Hoda Kotb</a> to co-host the show's fourth hour. After thanking Hoda and acknowledging the birth of <a href="/name/nm1679606?ref_=hm_nw_cel1_lk5">Jenna Bush Hager</a>'...                                        <span class="nobr"><a href="/news/ni58907984?ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58907935?ref_=hm_nw_cel2"
class="headlines" >5 Biggest Revelations From Stephen Colbertâs GQ Cover: Why He Retired Alter Ego, Surviving Deaths of Father, 2 Brothers</a>
    <div class="infobar">
            <span class="text-muted">9 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?ref_=hm_nw_cel2_src"
>The Wrap</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58907986?ref_=hm_nw_cel3"
class="headlines" >Lauren Conrad to Make Her Collection Debut at New York Fashion Week This SeptemberâSee the Dreamy First Photo!</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?ref_=hm_nw_cel3_src"
>E! Online</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58908427?ref_=hm_nw_cel4"
class="headlines" >Cue the '90s Flashbacks! The Casts of "Empire Records" and "Can't Hardly Wait" Reunited This Weekend</a>
    <div class="infobar">
            <span class="text-muted">6 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004699?ref_=hm_nw_cel4_src"
>TooFab</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni56283754?ref_=hm_nw_cel5"
class="headlines" >Over 250 of the Wildest Celebrity Halloween Costumes !</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000088?ref_=hm_nw_cel5_src"
>Popsugar.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/gallery/rg2465176320?ref_=hm_snp_pwl_hd" > <h3>IMDb Snapshot - Photos We Love</h3> </a> </span> </span> <p class="blurb">Here's a look back at some of our favorite event photos, still images, and posters released between August 9 - August 15.</p> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm2658593024/rg2465176320?ref_=hm_snp_pwl_i_1" > <img itemprop="image" class="pri_image" title="The Hateful Eight (2015)" alt="The Hateful Eight (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTc0MjgzMDY0N15BMl5BanBnXkFtZTgwODk4MDM1NjE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc0MjgzMDY0N15BMl5BanBnXkFtZTgwODk4MDM1NjE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1966991616/rg2465176320?ref_=hm_snp_pwl_i_2" > <img itemprop="image" class="pri_image" title="Gigantic (2018)" alt="Gigantic (2018)" src="http://ia.media-imdb.com/images/M/MV5BMTgyMTY4NjI3M15BMl5BanBnXkFtZTgwODUxOTU1NjE@._V1_SY201_CR55,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTgyMTY4NjI3M15BMl5BanBnXkFtZTgwODUxOTU1NjE@._V1_SY201_CR55,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3258509568/rg2465176320?ref_=hm_snp_pwl_i_3" > <img itemprop="image" class="pri_image" title="Sicario (2015)" alt="Sicario (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTg3MzgyNTE4Ml5BMl5BanBnXkFtZTgwNDkwNTU1NjE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg3MzgyNTE4Ml5BMl5BanBnXkFtZTgwNDkwNTU1NjE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/gallery/rg2465176320?ref_=hm_snp_pwl_sm" class="position_bottom supplemental" > See full gallery </a> </p>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
                <div class="ab_borntoday">
<span class="ninja_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=8-17&ref_=hm_brn_hd" > <h3>Born Today</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0000134?ref_=hm_brn_i_1" > <img itemprop="image" class="pri_image" title="Robert De Niro" alt="Robert De Niro" src="http://ia.media-imdb.com/images/M/MV5BMjAwNDU3MzcyOV5BMl5BanBnXkFtZTcwMjc0MTIxMw@@._V1_SY172_CR7,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjAwNDU3MzcyOV5BMl5BanBnXkFtZTcwMjc0MTIxMw@@._V1_SY172_CR7,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0000134?ref_=hm_brn_cap_pri_lk1_1">Robert De Niro</a> (72) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm3920288?ref_=hm_brn_i_2" > <img itemprop="image" class="pri_image" title="Taissa Farmiga" alt="Taissa Farmiga" src="http://ia.media-imdb.com/images/M/MV5BMTgzNTE5NTk1Nl5BMl5BanBnXkFtZTcwMjg0MjcyNw@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTgzNTE5NTk1Nl5BMl5BanBnXkFtZTcwMjg0MjcyNw@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm3920288?ref_=hm_brn_cap_pri_lk1_2">Taissa Farmiga</a> (21) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0000576?ref_=hm_brn_i_3" > <img itemprop="image" class="pri_image" title="Sean Penn" alt="Sean Penn" src="http://ia.media-imdb.com/images/M/MV5BMTc1NjMzMjY3NF5BMl5BanBnXkFtZTcwMzkxNjQzMg@@._V1_SY172_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc1NjMzMjY3NF5BMl5BanBnXkFtZTcwMzkxNjQzMg@@._V1_SY172_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0000576?ref_=hm_brn_cap_pri_lk1_3">Sean Penn</a> (55) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0567031?ref_=hm_brn_i_4" > <img itemprop="image" class="pri_image" title="Helen McCrory" alt="Helen McCrory" src="http://ia.media-imdb.com/images/M/MV5BMTcwOTMwNjEwOV5BMl5BanBnXkFtZTcwMTUwMjc4Mg@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTcwOTMwNjEwOV5BMl5BanBnXkFtZTcwMTUwMjc4Mg@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0567031?ref_=hm_brn_cap_pri_lk1_4">Helen McCrory</a> (47) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm2581521?ref_=hm_brn_i_5" > <img itemprop="image" class="pri_image" title="Austin Butler" alt="Austin Butler" src="http://ia.media-imdb.com/images/M/MV5BMjI2MDcwODc2NV5BMl5BanBnXkFtZTcwOTg2NzcwOQ@@._V1_SY172_CR6,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI2MDcwODc2NV5BMl5BanBnXkFtZTcwOTg2NzcwOQ@@._V1_SY172_CR6,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm2581521?ref_=hm_brn_cap_pri_lk1_5">Austin Butler</a> (24) </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/search/name?refine=birth_monthday&birth_monthday=8-17&ref_=hm_brn_sm" class="position_bottom supplemental" > See all birthdays </a> </p>        </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/gallery/rg3939146496?ref_=hm_ph_tv_da_hd" > <h3>TV Spotlight: "Downton Abbey"</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3826313472/rg3939146496?ref_=hm_ph_tv_da_i_1" > <img itemprop="image" class="pri_image" title="Downton Abbey (2010-)" alt="Downton Abbey (2010-)" src="http://ia.media-imdb.com/images/M/MV5BNTU0NTk1ODY2MF5BMl5BanBnXkFtZTgwMzQ3NTc1NjE@._V1_SY148_CR39,0,148,148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNTU0NTk1ODY2MF5BMl5BanBnXkFtZTgwMzQ3NTc1NjE@._V1_SY148_CR39,0,148,148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3759204608/rg3939146496?ref_=hm_ph_tv_da_i_2" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTc3OTgyNjkyNV5BMl5BanBnXkFtZTgwNzQ3NTc1NjE@._V1._CR442,0,1365,1365_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc3OTgyNjkyNV5BMl5BanBnXkFtZTgwNzQ3NTc1NjE@._V1._CR442,0,1365,1365_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3859867904/rg3939146496?ref_=hm_ph_tv_da_i_3" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjAxODE1MzEyNV5BMl5BanBnXkFtZTgwMTQ3NTc1NjE@._V1._CR0,0,1533,1533_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjAxODE1MzEyNV5BMl5BanBnXkFtZTgwMTQ3NTc1NjE@._V1._CR0,0,1533,1533_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm4010862848/rg3939146496?ref_=hm_ph_tv_da_i_4" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjE5NzEwOTUzMF5BMl5BanBnXkFtZTgwODQ3NTc1NjE@._V1._CR0,0,1338,1338_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE5NzEwOTUzMF5BMl5BanBnXkFtZTgwODQ3NTc1NjE@._V1._CR0,0,1338,1338_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">The BBC has released the first photos from the sixth and final season of "<a href="/title/tt1606375/?ref_=hm_ph_tv_da_lk1">Downton Abbey</a>." The final season will premiere in this UK in September, but Americans will have to wait until January 2016.</p> <p class="seemore"> <a href="/gallery/rg3939146496?ref_=hm_ph_tv_da_sm" class="position_bottom supplemental" > See full photo gallery </a> </p>    </div>

        </span>
        </div>
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/title/tt2091478/?ref_=hm_if_air_hd" > <h3>Indie Focus: <i>Air</i> - "Testing the Tank"</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm97643264/tt2091478?ref_=hm_if_air_i_1" > <img itemprop="image" class="pri_image" title="Air (2015)" alt="Air (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjE2NjcwMjA2OF5BMl5BanBnXkFtZTgwODA5NTkxNjE@._V1_SX170_CR0,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE2NjcwMjA2OF5BMl5BanBnXkFtZTgwODA5NTkxNjE@._V1_SX170_CR0,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1406579481?ref_=hm_if_air_i_2" data-video="vi1406579481" data-rid="06M5SJ7P421ZFCXESVK9" data-type="single" class="video-colorbox" data-refsuffix="hm_if_air" data-ref="hm_if_air_i_2"> <img itemprop="image" class="pri_image" title="Air (2015)" alt="Air (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjMyMjk2ODA5OF5BMl5BanBnXkFtZTgwMjM5NTkxNjE@._V1_SX444_CR0,0,444,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjMyMjk2ODA5OF5BMl5BanBnXkFtZTgwMjM5NTkxNjE@._V1_SX444_CR0,0,444,250_AL_UY500_UX888_AL_.jpg" /> <img alt="Air (2015)" title="Air (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Air (2015)" title="Air (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">In the near future, breathable air is nonexistent and two engineers (<a href="/name/nm0005342?ref_=hm_if_air_lk1">Norman Reedus</a> and <a href="/name/nm0005023/?ref_=hm_if_air_lk2">Djimon Hounsou</a>) tasked with guarding the last hope for mankind struggle to preserve their own lives while administering to their vital task at hand.</p> <p class="seemore"> <a href="/title/tt2091478/?ref_=hm_if_air_sm" class="position_bottom supplemental" > Learn more about <i>Air</i> </a> </p>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_trivia">
<span class="ninja_header"> <span class="oneline"> <a href="/title/tt2381249/trivia?item=tr2274305&ref_=hm_trv_hd" > <h3>Did You Know?</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt2381249?ref_=hm_trv_i_1" > <img itemprop="image" class="pri_image" title="Mission: Impossible - Rogue Nation (2015)" alt="Mission: Impossible - Rogue Nation (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTQ1NDI2MzU2MF5BMl5BanBnXkFtZTgwNTExNTU5NDE@._V1_SX89_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ1NDI2MzU2MF5BMl5BanBnXkFtZTgwNTExNTU5NDE@._V1_SX89_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" > <strong class="text-large"><a href="/title/tt2381249?ref_=hm_trv_lk1">Mission: Impossible - Rogue Nation</a></strong> <p class="blurb"><a href="/name/nm0000129?ref_=hm_trv_lk1">Tom Cruise</a> and <a href="/name/nm0000609?ref_=hm_trv_lk2">Ving Rhames</a> are the only actors to appear in all five films in the franchise.</p></div> </div> </div> <p class="seemore"> <a href="/title/tt2381249/trivia?item=tr2274305&ref_=hm_trv_sm" class="position_bottom supplemental" > See more trivia </a> </p>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;J_6IK_XGEbU&quot;}"
        >
            <div class="ab_poll poll">
<span class="ninja_header"> <span class="oneline"> <a href="/poll/J_6IK_XGEbU/?ref_=hm_poll_hd" > <h3>Poll: Best Movie to Have Been the IMDb Top 250 #1</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?ref_=hm_poll_mp">More Polls</a></h4> </span> </span> <p class="blurb">Only five movies have ever topped the IMDb Top 250. Which of them is the best? <a href="http://www.imdb.com/board/bd0000088/nest/230445462/?ref_=hm_poll_lk1">Discuss here</a> after voting.</p> <p class="seemore"> <a href="/poll/J_6IK_XGEbU/?ref_=hm_poll_sm" class="position_blurb supplemental" > Vote now </a> </p> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/J_6IK_XGEbU/?ref_=hm_poll_i_1" > <img itemprop="image" class="pri_image" title="The Shawshank Redemption (1994)" alt="The Shawshank Redemption (1994)" src="http://ia.media-imdb.com/images/M/MV5BODU4MjU4NjIwNl5BMl5BanBnXkFtZTgwMDU2MjEyMDE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BODU4MjU4NjIwNl5BMl5BanBnXkFtZTgwMDU2MjEyMDE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/J_6IK_XGEbU/?ref_=hm_poll_i_2" > <img itemprop="image" class="pri_image" title="El padrino (1972)" alt="El padrino (1972)" src="http://ia.media-imdb.com/images/M/MV5BMjEyMjcyNDI4MF5BMl5BanBnXkFtZTcwMDA5Mzg3OA@@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjEyMjcyNDI4MF5BMl5BanBnXkFtZTcwMDA5Mzg3OA@@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/J_6IK_XGEbU/?ref_=hm_poll_i_3" > <img itemprop="image" class="pri_image" title="The Dark Knight (2008)" alt="The Dark Knight (2008)" src="http://ia.media-imdb.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/J_6IK_XGEbU/?ref_=hm_poll_i_4" > <img itemprop="image" class="pri_image" title="Star Wars: Episode IV - A New Hope (1977)" alt="Star Wars: Episode IV - A New Hope (1977)" src="http://ia.media-imdb.com/images/M/MV5BMTU4NTczODkwM15BMl5BanBnXkFtZTcwMzEyMTIyMw@@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU4NTczODkwM15BMl5BanBnXkFtZTcwMzEyMTIyMw@@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/J_6IK_XGEbU/?ref_=hm_poll_i_5" > <img itemprop="image" class="pri_image" title="The Lord of the Rings: The Fellowship of the Ring (2001)" alt="The Lord of the Rings: The Fellowship of the Ring (2001)" src="http://ia.media-imdb.com/images/M/MV5BNTEyMjAwMDU1OV5BMl5BanBnXkFtZTcwNDQyNTkxMw@@._V1_SY207_CR1,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNTEyMjAwMDU1OV5BMl5BanBnXkFtZTcwNDQyNTkxMw@@._V1_SY207_CR1,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
    </div>
    <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,11x1;p=tr;fv=1;ab=e;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=358576858078;ord=358576858078?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,11x1;p=tr;fv=1;ab=e;bpx=2;c=0;s=3075;s=32;ord=358576858078?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,11x1;p=tr;fv=1;ab=e;bpx=2;c=0;s=3075;s=32;ord=358576858078?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	
                    
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/movies-in-theaters/?ref_=hm_otw_hd" > <h3>Opening This Week</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2752772"></div> <div class="title"> <a href="/title/tt2752772?ref_=hm_otw_t0"> Sinister 2 </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3316948"></div> <div class="title"> <a href="/title/tt3316948?ref_=hm_otw_t1"> American Ultra </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2679042"></div> <div class="title"> <a href="/title/tt2679042?ref_=hm_otw_t2"> Hitman: Agent 47 </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4270516"></div> <div class="title"> <a href="/title/tt4270516?ref_=hm_otw_t3"> Grandma </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3799372"></div> <div class="title"> <a href="/title/tt3799372?ref_=hm_otw_t4"> 6 Years </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3704416"></div> <div class="title"> <a href="/title/tt3704416?ref_=hm_otw_t5"> Digging for Fire </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1767372"></div> <div class="title"> <a href="/title/tt1767372?ref_=hm_otw_t6"> Broadway Therapy </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3062976"></div> <div class="title"> <a href="/title/tt3062976?ref_=hm_otw_t7"> Learning to Drive </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <div><a href="/movies-in-theaters/?ref_=hm_otw_sm" ><p class="seemore position_bottom">See more opening this week</p></a></div>
        </span>
        </div>
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
            <div class="ab_ninja showtimesMessagingWidget">
<p class="blurb"><a href="/showtimes?ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p>    </div>

        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/chart/?ref_=hm_cht_hd" > <h3>Now Playing (Box Office)</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1398426"></div> <div class="title"> <a href="/title/tt1398426?ref_=hm_cht_t0"> Straight Outta Compton </a> <span class="secondary-text">$60.2M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2381249"></div> <div class="title"> <a href="/title/tt2381249?ref_=hm_cht_t1"> Mission: Impossible - Rogue Nation </a> <span class="secondary-text">$17.2M</span> </div> <div class="action"> <a href="/showtimes/title/tt2381249?ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1638355"></div> <div class="title"> <a href="/title/tt1638355?ref_=hm_cht_t2"> The Man from U.N.C.L.E. </a> <span class="secondary-text">$13.4M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1502712"></div> <div class="title"> <a href="/title/tt1502712?ref_=hm_cht_t3"> Fantastic Four </a> <span class="secondary-text">$8.2M</span> </div> <div class="action"> <a href="/showtimes/title/tt1502712?ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4178092"></div> <div class="title"> <a href="/title/tt4178092?ref_=hm_cht_t4"> The Gift </a> <span class="secondary-text">$6.5M</span> </div> </div> </div> </div> </div> <div><a href="/chart/?ref_=hm_cht_sm" ><p class="seemore position_bottom">See more box office results</p></a></div>
        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/movies-coming-soon/?ref_=hm_cs_hd" > <h3>Coming Soon</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3742378"></div> <div class="title"> <a href="/title/tt3742378?ref_=hm_cs_t0"> Une seconde mÃ¨re </a> <span class="secondary-text"></span> </div> <div class="action"> <a href="/showtimes/title/tt3742378?ref_=hm_cs_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3787590"></div> <div class="title"> <a href="/title/tt3787590?ref_=hm_cs_t1"> We Are Your Friends </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1781922"></div> <div class="title"> <a href="/title/tt1781922?ref_=hm_cs_t2"> No Escape </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1598642"></div> <div class="title"> <a href="/title/tt1598642?ref_=hm_cs_t3"> Z for Zachariah </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3832914"></div> <div class="title"> <a href="/title/tt3832914?ref_=hm_cs_t4"> War Room </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <div><a href="/movies-coming-soon/?ref_=hm_cs_sm" ><p class="seemore position_bottom">See more coming soon</p></a></div>
        </span>
        </div>
                    
                    
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
            <div class="ab_twitter">
<span class="widget_header"> <h3>Follow Us On Twitter</h3> </span> <div class="widget_content"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3776329187._CB305760180_.html#{"screen-name":"IMDb","height":"500px","width":"300px"}'></iframe> </div> </div>    </div>

        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
            <div class="ab_facebook">
<span class="widget_header"> <h3>Find Us On Facebook</h3> </span> <div class="widget_content"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
        <div class="aux-content-widget-2 sticky-widget">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/imdbpicks/?ref_=hm_pks_hd" > <h3>IMDb Picks: August</h3> </a> </span> </span> <p class="blurb">Visit our IMDb Picks section to see our recommendations of movies and TV shows coming out in August.</p> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/?ref_=hm_pks_i_1" > <img itemprop="image" class="pri_image" title="The Man from U.N.C.L.E. (2015)" alt="The Man from U.N.C.L.E. (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTYwMzUxNjI5N15BMl5BanBnXkFtZTgwNjE3NjkzNjE@._V1_SY525_CR44,0,700,525_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTYwMzUxNjI5N15BMl5BanBnXkFtZTgwNjE3NjkzNjE@._V1_SY525_CR44,0,700,525_AL_UY1050_UX1400_AL_.jpg" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/imdbpicks/?ref_=hm_pks_cap_pri_1" > The Man from U.N.C.L.E. </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/imdbpicks/?ref_=hm_pks_sm" class="position_bottom supplemental" > Visit the IMDb Picks section </a> </p>    </div>

        </span>
        </div>
    </div>
</div>

                    
                    


                   <br class="clear" />
                </div>





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYqGvyETBanqdcvXZeQxSMkLH_MjudpUJeGacxGNNt0M0NQI6-fTwL9bDl-0cIJC4bbpRGfTNGD%0D%0AYwV4zpwlawRIJZD6Ah6tZT6GOOlmp-yn4SH5T_QgbTE7jJ2KkDO55ZdMtpVINQdxUWeb2GK14Owu%0D%0AAPoRB9fVTmp0DmKfhTqdWOIXEbhRvXvQ5JqHxkKQnlaDXkyZqHdXmsMkaedqF_1oaX4UkLN2GIcZ%0D%0ADSZnBZz-M28%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYuLQnwjCyhk45pFk6vaLGvSWTyWzym4k2WuevXqCMjsNWbH7GyVBgv3d3yu9wUYF3O08FkGRoj%0D%0AeU_M-vmHBH4Dw5WZEmgyhQyMrgp411jRAW-Wq1hg8Q3_D4tiys-FFGiK87Gn8i3p1wT5hYzw4QwS%0D%0A-EEl6oNX8A0Ete4eMNgbH4aYXt1qlpWtNQyv2FdXEsUsvxdRJ8ty-lPDCG5-TrHI5Q%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYkdrjWPLbXRZjj_s8gGuRkxxSKTcsooz2y3MaZ4HuUnCeFUZpH0yTloUmgYuEy_CPEOTvNvvnp%0D%0A7Lz1nQX251HZm5g-C8HgtM-bsJ4_DwWZXVqCMNnz6MU_Qv3AzniMe97hZPKOdc_MxwAQpFdesdgo%0D%0AOM9aWn7E88IdvOSP2JxKvbDIyBUp1twXHfSMv5eb3xOOBFaCBbH3ZIF2-i1-ZXDvHHrIo14GjADQ%0D%0Afp7me2LTICftIlKW5zSquxGhS7nXO-A8I5DqtNBUcrTim4yh2GZLaLo3i5I4M6iHV8FXQUnMAHW_%0D%0A_fASTq4tMBpqgF-S-0npM8anHIeYL47QBmvZT4X-SroCKctdPM3BsYO1fOaDjZkswxFbdL9UuOC8%0D%0AS4LGeLlybT4npXcsYkJNPKAvOxKM5w%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYiHLZJ4AJpAPO_g99vvOpJetpxUwCiL_Rur66COLbZT3NHk5nwxEE301c0rBnpppWF-0nerXdH%0D%0A4kIo-bJ3NoFDCUf6hzvgF3XbafcaHKdU0ns4RGf7reogn14CMoKr9TFSoZVMGVG_qOqfBmgLizP0%0D%0Ayv_fLJp0MdKWy61zfFXtsq9ipuplOLE-68Zri4HnELuQfQEXDyWhdBQb_1wqpgRlJg%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYmmedMfnNKe8tLUeS_Sr-hGf3Vtn0UfkVTIJlXx_3H2gnas8DOB1kYKcG8Ujk1Rq-hIGq3OFO_%0D%0A-H9_YIaBJM28WJxY5b9DQFMgik0HcFQKLLRRAkpRgQpAMH23uYCGpLvJ8BB4DQxzL1X_0PnfuVvN%0D%0AfITRHcSsfFWTePZoY2oEJFb5lKj-XzNbrHBWbe0OLfsk%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYl60taE6uMnOfq-KT5OBTQ6wsuZNJtTDx8u1VuyI0ne6CmTLGTzVy1I_y7a5ABFM5Oa6TPDr-H%0D%0A2J161ZsqAdGfo3d0BjvYLOtz_sn-IdHCWorZ3AtdvIv45YtMe6nsh_AwzDF8fcL8-cbbR2SfT6UU%0D%0AbIZiWA7NqMT0JkjFX2JS7ybZhjiL-nm0srzqqN4zDzUbvUDNjt59RYFhYXaVB56Tnw%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top 250</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Top Movies</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li>    <a href="/offsite/?page-action=ft_ad&token=BCYuROhxBa7rI-ZkuECYwcdbcjwgb-_ql8SDf-VBSbJSHXMw2wz2NBj19w30SJxEzngO_M-zLuFq%0D%0AaD9i0S9sNQ9cnr0P167kCje2XDQDLMiS6txOlgaFhJNU-E8TEeuNyTJtxNfNCAGjhkf2n5q0bJj1%0D%0AR72knFFuv6f0wixuigpfocVgsvAURFZDGJYw-GUroMk7ZtORwB2PbPhxRu0FwIGb1plhawo9ySy6%0D%0AiFgHuGGL-VahIs_2dskabNLCoCa9bhV29r-ZNI0OSeOvfO7aKm5ruw%0D%0A&ref_=ft_ad"
itemprop='url'>Advertising</a>
</li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYuD6Bbxc5GL3oY6aUtbXtM-o-a1_NtVTrUycghaS1y6A8REenRwcVAsF6CLzCQVTYTEZxXdXQd%0D%0Ax72EahFGBtRXpsrh-wbU14doB6vDsYZ4XSd0VB1RIDKFQw4BSGTUQ_5_4DB73oB-ghDFUpsiI0Xj%0D%0ARTrsQqVmAsW_NgPRxDI7uSuUKiytFSI79aOZJOgv6KcYsJm5JrMFCfo4QLabs_SAOA%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYudhHXWD_i0YJNH3TQjYqktioSINxgo1So_ZQ0enPXwRV4fTZdPsf9jnEGYUMN5IeDZ1_qy1NQ%0D%0A_ywteiq2I6_fu2vMU21Ls2YBNM-GI7pvxIDLf2j-5m_saH9LQugWoYAWnQkNGn8a7OnATO3cXipy%0D%0AbByfNKH6z5zXMDKE1pp-EmN88jVhgfzEr4GMSgcMKmZ4%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYgeN-cYpuS0wu3C_xdnTEjWX6WSY_mrv7yEAorx8WTYEpvTZhs9xxTlFIVarR5aAW-KiD8zXNz%0D%0A896-gJ-ih6cux68SRqb7ubhz9g_iHFJiG-fetlIZ08ALM5feH6IiVZD4oH4v3aSh9TEUovkcMEy7%0D%0AjJ6LtBNHP3vq0kWiW69S4QwWkhi1Npajx25qYDIYW2lozI9vWopb9Urag-FX7KgTCY_lSWj3POSq%0D%0AkNZ6xjztO5A9GgY3WayES6eSZSRoTl5s%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/show_article?conditions';"
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2015
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/';"
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYo1CZ_tT9JPdTMp78Q4t7OL9tFrjzVt6Nzd1xTMuCQ9ph6pbBzBFV6ngpYfI9r_YEB1gdsTBYv%0D%0A-ufvLyL1VL4eb5CHxQK55eIsC3urxxoGtXM4UVdA1Nhp2o9pZgGgwqK664OQe9HSz58dQwU_urky%0D%0AfZ8S0kjZQ6UDLwSz9_t1H3eSm6sII18KdX4nHjCj-CxRrg8Rw_QiFGBcrXUxAIjO1LCG2RADQhGq%0D%0AbyCmFnlYqNo%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Instant Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYmWIoYkx-W1Tu5nV21P-hB78R8TT5dFOg1pxHfWX_DKUN5V8EDTi6V-dsPDjr9Y8kECQLIlIM3%0D%0Al2cQx55QBLTNdN-6h-WGJnqSxR5rlehaGoq5gjyaHypBPQfGFhGkUKRLWwvlwCViHhvpc-UY4EFz%0D%0APSzNNtqDvfJlIHEdCTlFzGIKBr1LfOPDkNvhEPNkZVYMWQGDLCtwQCG3fshMwutUDwk1b927DWSv%0D%0AXqCfPded_a8%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Instant Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYr6tPoTDjJ-kWpbvVcsB4Fjfw4FoLX7fnLW46Vkfhy1oHPCEjHFsDFMuvgZuxt7rIBIcj_B5gK%0D%0Aolr03OCcmoNM2umeoJL178MMApbIyFj9YxV2PwOmNv4W1Z2FnJRcCdK6isXgddlMbAVuLvKkABG2%0D%0A3Kz80aSeDW3KYw6HMIEhnYyG_6L1YKfFRsoOAim7tN7Elby8Bq6Ntxi1bzTA-_AnVQ3dtzS2KBzd%0D%0A2FuPcpQmFT4%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYqmNkBSc7SI-VsWc824ut2O7HRCGorqogxhCSgB1_3v5BavnuWxLL4fhd4y9CtyS25r6Cn3_ee%0D%0ApVn8cjYoUg5pp4rC7MoYIybPQ0qnbUTUxRrMNTPdht8ZzbOCRojlL9zYDc2GZomqybsMDKNhDjjj%0D%0AwVmRcFCIs92BolieJwvQi2nrB00oNtR6PZHAAemYKWuLwc_vps4sTPomJrY2Hwz10TabMuTyFdSj%0D%0AJIHbgCYrI3k%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYgQUouu9SzhlUhWJWj0orjidLhr1GJ05sdWsXTquB1zZ5rTFw1hvbtpj08FOD6s4kUmzvMaDYy%0D%0AZpT8LOO6I-V4n67CnVnHvdWgDTZmL65wiqf-u8KThid9rSWAjPCGkop6H8SQN3I9vhNW9NVnn8bY%0D%0Asblayg_YDcmMHq2Ye3lCQKldDuLi7gSETP7DzMqdvp99BBo4YAJd92q8fkrSxNzs-8i3sBYiWFXZ%0D%0AsIdo9AfP-vA%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYpG7IppnixE6i79QAqpt00eBdnfSR0ZO-ikHcT9GJK_7YxOraEY9xxckwoVyqbdE51sugpsP1n%0D%0AE7F5AZ0P_Xx2GMxI0BOKHMyd5SXGSV3J9PYAQA6unjz4a2CHeziUSR0IukRzRvbtN5V7iWsFuvLE%0D%0ApO4-fFlPm6JoYzn2Hodw6BWZtChSk4eX-IkFVlDI6H5ThDwLWyEBWCbIE17tc5k0stTTg42kg-2I%0D%0AXYA9fkE7HmOHCjhZx6DyjCA5i7tulfAg%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYt5jEA0o9ttB0xKi58j2e4p8vPtV-a8Ojg45MKiW9nYOCqkLo_VkfnfwHXzfcqdRxhyvctdVfV%0D%0AI8Gq-vl3mMioyjAJwwXu481Rze3mQG1fdYg4m2Ztgnu_2gqEezORLpRBZvaez0iuN7R_2oqjYdyo%0D%0AgrRtLc0LJByUn8ihDLRx5jE%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYuunV-OvgY1iwILZG0HB7HaQssxvY_ftH26VKXI9Wm7GhUeA5sHNXfwNvLV5ibDCtz4MfZHZHr%0D%0Aa0Ek4PsZQi8g54T6WZu780m02fqzHftOy93vgGaFqYQTyD0VbHmN8-OleqBV-PlFiwfQblN0jksF%0D%0A8CYaMliYk1fEhxiv9-ccxiM%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-2808948949._CB312481908_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-1753959650._CB315300322_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-3627981178._CB318528484_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-2430112694._CB318528507_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=0101b0b0f82b60aba7d062a4a48fbf063149bb5ddab3e1ccab200dec6a3c01d48d50",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=358576858078"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-232398347._CB349580400_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=358576858078&ord=358576858078";
    },"unable to request AAN pixel");
</script>

<div id="flashContent" style="width: 0px; height: 0px; overflow:hidden;">
    <script type="text/javascript">
        if (generic && generic.monitoring.record_metric && flashAdUtils.canPlayFlash) {
            var flashLoaded = 0;
            function swfLoaded() {
                flashLoaded = 1;
                generic.monitoring.record_metric('ads_flash_did_play', 1, true);
            }
            window.onload = function() {
                // Flash was not loaded
                if (flashLoaded === 0) {
                    // Flash could be loaded but wasn't
                    if (flashAdUtils.canPlayFlash()) {
                        generic.monitoring.record_metric('ads_flash_can_but_did_not_play', 0, true);
                    } else {
                        generic.monitoring.record_metric('ads_flash_cannot_play', 0, true);
                    }
                }
            }
            /**
             * There is a rare possibility of window.onload getting called before swfLoaded.
             * Firing this metric to keep track of such calls.
             */
             generic.monitoring.record_metric('ads_flash_page_loaded', 1, true);
         }
    </script>
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="1x1" align="middle">
        <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
        <param name="quality" value="high" />
        <param name="play" value="true" />
        <param name="loop" value="true" />
        <param name="wmode" value="transparent" />
        <param name="scale" value="showall" />
        <param name="menu" value="true" />
        <param name="devicefont" value="false" />
        <param name="salign" value="" />
        <param name="allowScriptAccess" value="always" />

        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" width="1" height="1">
            <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
            <param name="quality" value="high" />
            <param name="play" value="true" />
            <param name="loop" value="true" />
            <param name="wmode" value="transparent" />
            <param name="scale" value="showall" />
            <param name="menu" value="true" />
            <param name="devicefont" value="false" />
            <param name="salign" value="" />
            <param name="allowScriptAccess" value="always" />
        </object>
        <!--<![endif]-->
    </object>
</div>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="647"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
