<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trending  stories on Indian Lifestyle, Culture, Relationships, Food, Travel, Entertainment, News & New Technology News - Indiatimes.com</title>

<meta name="keywords" content="" />
<meta name="description" content="Indiatimes.com brings you the news, articles, stories and videos on entertainment, latest lifestyle, culture & new technologies emerging worldwide." />



		

<link rel="image_src" href="http://media.indiatimes.in/resources/images/fbimage.png" />
<meta property="og:title" content="" />    
<meta property="og:url" content="http://www.indiatimes.com" />
<meta property="og:image" content="http://media.indiatimes.in/resources/images/fbimage.png" />

<meta property="og:site_name" content="indiatimes.com" />
<meta property="fb:app_id" content="117787264903013" />
<meta property="og:description" content="" />
<meta name="google-site-verification" content="ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw" />
<meta name="msvalidate.01" content="BAB71AAED7CCC01DB106A58E99625EB4" />
<link href="https://plus.google.com/+indiatimes/posts" rel="publisher" />

<meta property="og:type" content="website" />
    
<!--<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />-->

<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">    <link rel="shortcut icon" href="http://media.indiatimes.in/resources/images/favicon.ico?v=25" type="image/x-icon"><link rel="stylesheet" href="http://www.indiatimes.com/fonts/fonts.min.css?v=100.6" media="screen"><link rel="stylesheet" href="http://media.indiatimes.in/resources/css/site.css?v=100.6" media="screen"><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery-1.11.1.min.js?v=100.6"></script><script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/handlebars-v1.3.0.js?v=100.6"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.inview.min.js?v=100.6"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.preload.min.js?v=100.6"></script><script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script><!-- Facebook Pixel Code --><script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');fbq('init', '853339751421540');fbq('track', 'PageView');</script><noscript><img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=853339751421540&ev=PageView&noscript=1"/></noscript><!-- End Facebook Pixel Code -->           <!--  Analytics Code Begin -->    <!-- Begin comScore Tag -->    <script>        var _comscore = _comscore || [];        _comscore.push({ c1: "2", c2: "6036484" });        (function() {            var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";            el.parentNode.insertBefore(s, el);        })();    </script>    <noscript>    <img src="http://b.scorecardresearch.com/p?c1=2&c2=6036484&cv=2.0&cj=1" />    </noscript>    <!-- End comScore Tag -->    <!-- Begin Google Analytics Tag -->    <script>        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');        ga('create', 'UA-198011-6', 'indiatimes.com');            ga('require', 'displayfeatures');        ga('send', 'pageview');    </script>    <!-- End Google Analytics Tag -->    <!--  Analytics Code Begin --><!--adcode common script--><script type='text/javascript' src="http://ad.crwdcntrl.net/5/c=2800/pe=y/var=_ccaud"></script><script type="text/javascript">    var googletag = googletag || {};    googletag.cmd = googletag.cmd || [];    (function() {        var gads = document.createElement("script");        gads.async = true;        gads.type = "text/javascript";        var useSSL = "https:" == document.location.protocol;        gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";        var node =document.getElementsByTagName("script")[0];        node.parentNode.insertBefore(gads, node);    })();</script> <script type="text/javascript">        var nid='';        var cid='';        var sid='';        var brand_name='';                                                             </script>    <script>
    var contType = "";
    </script>
        <script type="text/javascript" src="http://media.indiatimes.in/resources/js/banner/home.js?v=100.6"></script>
            
<!--[if lt IE 9]>
  <script type="text/javascript" src="http://media.indiatimes.in/js/html5.js?v1.1"></script>
  <![endif]-->
<script>
var isDetailPage = 0</script>
</head>
<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];   
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=117787264903013&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script> 	
	
<div id="wrap" class="cf"><!--wrap start-->
    <div class='darkDisabledBg' id='disabledBg'></div>
<header>
    <div class="header-inner">
        <div class="comm-header ">
            <nav>
                <div class='forTop'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>

                    <div class="lnks">
                                                        <a href="http://www.indiatimes.com/news/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','News');">News</a> 
                                                               <a href="http://www.indiatimes.com/lifestyle/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Lifestyle');">Lifestyle</a> 
                                                               <a href="http://www.indiatimes.com/entertainment/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Entertainment');">Entertainment</a> 
                                                               <a href="http://www.indiatimes.com/health/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Health');">Health</a> 
                                                               <a href="http://www.indiatimes.com/videocafe/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Videos');">Videos</a> 
                                                                                          <a href='http://www.indiatimes.com/trending'  target="_blank" class="#fffff" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Trending');">Trending</a> 
                                                </div>
                </div>
                <div class='onScrolled'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>
                    <a href="javascript:;" class="menu rc sprite" id="headerMenu">Menu</a>
                    <div class="has-tag">
                                           </div>
                </div>

                                
            </nav>
			                 
	    <div id="sticker" style="display:none;"><span class="readTitle">
				</span></div>
                    <div class="socls fr share">
            </div>
                        
            
			 <div class="social fr">
                            <a title="facebook" target="_blank"  class="sprite fb" href="https://www.facebook.com/indiatimes">facebook</a>
                            <a title="twitter" target="_blank"  class="sprite twt" href="https://twitter.com/indiatimes">twitter</a>
                            <a title="search"  class="sprite serach" onclick="$('body').showSearch();" href="javascript:void(0);">search</a>
                        </div>
        </div>
    </div>
</header>
<div class="clr"></div>
<div id="overlay" class="animated bounceOut"><!--overlay start-->
    <div class="search_conts"><!--search-cont start-->
        <form onsubmit="return false;" class="serach-form">
            <input type="text" onkeyup="search_data();" class="input" onfocus="if(this.value=='Search'){this.value=''}" onblur="if(this.value==''){this.value='Search'};" value="Search" name="q" id="q" autocomplete="off">
            <input type="button" class="sprite src-btn" onclick="search_data();">
            <a class="sprite sclose" id="close" href="javascript:void(0);">X</a>
        </form>
        <div class="scroll-panes" id="scroll-panes">
            <div class="clr"></div>
            <div class="res-msg" id="res-msg"></div>
            <div class="search-list cf" id="results"></div>
            <div class="clr"></div>
            <div class="loadmore" id="progressBar" style="display: none;">Loading......</div>
            <a class="loadmore" style="display:none;" onclick="load_search_data(); " id="load_more_button" href="javascript:void(0);">Load more</a>
        </div>
    </div>
</div>
<div class="blk">
    <div  id="leftMenu" class="leftMenu accordion inner">
        <dl >
            <dt data-color="red-bg" class=""><a href="http://www.indiatimes.com">Home</a></dt>
            <dd> </dd>
                            <dt  data-color="blue-bg" ><a href="http://www.indiatimes.com/news/">News</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/news/india/'>India</a>
                                     
                            <a href='http://www.indiatimes.com/news/world/'>World</a>
                                     
                            <a href='http://www.indiatimes.com/news/sports/'>Sports</a>
                                     
                            <a href='http://www.indiatimes.com/news/weird/'>Weird</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="purple-bg" ><a href="http://www.indiatimes.com/lifestyle/">Lifestyle</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/lifestyle/self/'>Self</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/style/'>Style</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                     
                            <a href='http://www.indiatimes.com/culture/who-we-are/'>Who we are</a>
                                     
                            <a href='http://www.indiatimes.com/culture/travel/'>Travel</a>
                                     
                            <a href='http://www.indiatimes.com/culture/food/'>Food</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="red-bg" ><a href="http://www.indiatimes.com/entertainment/">Entertainment</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/entertainment/bollywood/'>Bollywood</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/celebs/'>Celebscoop</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/hollywood/'>Hollywood</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="green-bg" ><a href="http://www.indiatimes.com/health/">Health</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                     
                            <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                     
                            <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                     
                            <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                     
                            <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="orange-bg" ><a href="http://www.indiatimes.com/videocafe/">Videos</a>
                                </dt>
                				<dt data-color="" class=""><a href="http://www.indiatimes.com/trending/">Trending</a>
                           </dt>
            <dt class="pink-bg">
            <div class="follow">Follow indiatimes </div>
            <div class="follow_cont"> 
                <a href="https://www.facebook.com/indiatimes" class="fbleft sprite"> facebook</a> 
                <a href="https://twitter.com/indiatimes" class="twtleft sprite"> twitter</a> 
            </div>
            </dt>

            <dd>&nbsp;</dd>
            <div class="clr"></div>
        </dl>
    </div>
</div>


<div class="clr"></div>
<div class="dummy-cont">&nbsp;</div>
<div class="clr"></div>


<div id="pushdown">
    <div class="pull-ad"><!--pull-ad start-->   
	
                <script type="text/javascript"> 
                    showHeaderCode();
                </script>
	    </div><!--pull-ad end-->
</div>
    <!--testing 15-11-22 23:50:03-->  

    <section id="hp_block_1" class="container cf" style="border: 0px solid red;"><!--container start-->
                

        <div class="news-panel cf"><!--news-panel start-->
            <h2>Big news</h2>

            <div class="feature-list cf"><!--feature-list start-->	
                                    <figure>
                        <div class="hash-tag">
                             7 hours ago                         </div>
						 						          
                        <a href="http://www.indiatimes.com/news/india/this-female-traffic-cop-from-mangalore-is-filling-potholes-just-so-that-you-can-have-a-smoother-ride-247593.html" class=" tint" title="This Female Traffic Cop From Mangalore Is Filling Potholes Just So That You Can Have A Smoother Ride">
                            <img  class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Nov/trafficcopmangalore_1448189633_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/trafficcopmangalore_1448189633_236x111.jpg"  border="0" alt="This Female Traffic Cop From Mangalore Is Filling Potholes Just So That You Can Have A Smoother Ride"/></a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/this-female-traffic-cop-from-mangalore-is-filling-potholes-just-so-that-you-can-have-a-smoother-ride-247593.html" title="This Female Traffic Cop From Mangalore Is Filling Potholes Just So That You Can Have A Smoother Ride">
                            This Female Traffic Cop From Mangalore Is Filling Potholes Just So That You Can Have A Smoother Ride                        </a>
                    </figcaption>
                            </div><!--feature-list end-->

                            <div class="news-panel-list cf" id="column1_0"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            2 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/sports/no-cricket-no-problem-india-still-thrash-pakistan-this-time-in-asian-hockey-247599.html" title="No Cricket No Problem! India Still Thrash Pakistan, This Time In Asian Hockey" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Nov/indiapakjuniorhockey_1448208184_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/indiapakjuniorhockey_1448208184_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/no-cricket-no-problem-india-still-thrash-pakistan-this-time-in-asian-hockey-247599.html" title="No Cricket No Problem! India Still Thrash Pakistan, This Time In Asian Hockey">
                            No Cricket No Problem! India Still Thrash Pakistan, This Time In Asian Hockey                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_1"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            4 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/world/two-men-live-to-tell-the-tale-of-surviving-both-the-paris-attacks-and-9-11_-247598.html" title="Two Men Live To Tell The Tale Of Surviving Both The Paris Attacks And 9/11" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Nov/pariswtc_1448197165_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/pariswtc_1448197165_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/two-men-live-to-tell-the-tale-of-surviving-both-the-paris-attacks-and-9-11_-247598.html" title="Two Men Live To Tell The Tale Of Surviving Both The Paris Attacks And 9/11">
                            Two Men Live To Tell The Tale Of Surviving Both The Paris Attacks And 9/11                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_2"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            5 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/world/shocking-new-video-emerges-showing-isis-training-toddlers-to-behead-247595.html" title="Shocking New Video Emerges Showing ISIS Training Toddlers To Behead" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Nov/capture-card_1448191019_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/capture-card_1448191019_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/shocking-new-video-emerges-showing-isis-training-toddlers-to-behead-247595.html" title="Shocking New Video Emerges Showing ISIS Training Toddlers To Behead">
                            Shocking New Video Emerges Showing ISIS Training Toddlers To Behead                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_3"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            5 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/world/malala-yousafzai-says-she-s-a-fan-of-sachin-tendulkar-s-straight-drive-and-relates-to-indian-culture-247596.html" title="Malala Yousafzai Says She's A Fan Of Sachin Tendulkar's Straight Drive And Relates To Indian Culture" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Nov/malalasachin_1448195019_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/malalasachin_1448195019_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/malala-yousafzai-says-she-s-a-fan-of-sachin-tendulkar-s-straight-drive-and-relates-to-indian-culture-247596.html" title="Malala Yousafzai Says She's A Fan Of Sachin Tendulkar's Straight Drive And Relates To Indian Culture">
                            Malala Yousafzai Says She's A Fan Of Sachin Tendulkar's Straight Drive And Relates To Indian Culture                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                    </div><!--news-panel end-->

        <div class="life-panel cf" id="middleBlock1"><!--life-panel start-->
            <h2>lifestyle</h2>

                                <div class="life-panel-list cf"><!--life-panel-list start-->
                        <figure><span class="label-tag spon">Partner</span>
                            
                               
                            <a href="http://www.indiatimes.com/entertainment/bollywood/here-s-what-your-favourite-bollywood-celebs-wished-for-this-diwali-247474.html" class=" tint" title="" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/entertainment/bollywood/here-s-what-your-favourite-bollywood-celebs-wished-for-this-diwali-247474.html');">
                                <img  class="greyBg23 lazy23"  src="http://media.indiatimes.in/media/content/2015/Nov/card_1447932032_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/card_1447932032_502x234.jpg"  border="0" alt="Hereâs What Your Favourite Bollywood Celebs Wished For This Diwali!" class="img-responsive"/>
                            </a>
                        </figure>
                        <figcaption>
                            <a href="http://www.indiatimes.com/entertainment/bollywood/here-s-what-your-favourite-bollywood-celebs-wished-for-this-diwali-247474.html" title="Hereâs What Your Favourite Bollywood Celebs Wished For This Diwali!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/entertainment/bollywood/here-s-what-your-favourite-bollywood-celebs-wished-for-this-diwali-247474.html');">Hereâs What Your Favourite Bollywood Celebs Wished For This Diwali!</a>
                        </figcaption>
                    </div><!--life-panel-list end-->
                

                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                   
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/lifestyle/self/how-to-seduce-and-get-each-zodiac-sign-to-go-on-a-date-with-you-247492.html" class="tint" title="How To Seduce And Get Each Zodiac Sign To Go On A Date With You" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/self/how-to-seduce-and-get-each-zodiac-sign-to-go-on-a-date-with-you-247492.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Nov/card_1448001198_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/card_1448001198_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/how-to-seduce-and-get-each-zodiac-sign-to-go-on-a-date-with-you-247492.html" title="How To Seduce And Get Each Zodiac Sign To Go On A Date With You" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/self/how-to-seduce-and-get-each-zodiac-sign-to-go-on-a-date-with-you-247492.html');">
                            How To Seduce And Get Each Zodiac Sign To Go On A Date With You                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/get-a-full-ab-workout-done-in-just-4-minutes-yes-really-247500.html'>video</a>                           
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/healthyliving/get-a-full-ab-workout-done-in-just-4-minutes-yes-really-247500.html" class="tint" title="Get A Full Ab Workout Done In Just 4 Minutes! Yes, Really!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/health/healthyliving/get-a-full-ab-workout-done-in-just-4-minutes-yes-really-247500.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/videocafe/2015/Nov/card-1_1447932222_502x234.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Nov/card-1_1447932222_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/get-a-full-ab-workout-done-in-just-4-minutes-yes-really-247500.html" title="Get A Full Ab Workout Done In Just 4 Minutes! Yes, Really!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/health/healthyliving/get-a-full-ab-workout-done-in-just-4-minutes-yes-really-247500.html');">
                            Get A Full Ab Workout Done In Just 4 Minutes! Yes, Really!                        </a>
                    </figcaption>
                </div>
                        
        </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_0">
                    <span class="strip skyblue-bg"></span>                    <figure>

                                                <a href="http://www.indiatimes.com/entertainment/matrix-star-keanu-reeves-s-heart-wrenching-note-about-life-will-inspire-you-to-never-give-up-247597.html" class="tint" title="Matrix Star Keanu Reeves' Heart-Wrenching Note About Life Will Inspire You To Never Give Up!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Nov/download_1448194822_1448194828_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/download_1448194822_1448194828_218x102.jpg" border="0" alt="Matrix Star Keanu Reeves' Heart-Wrenching Note About Life Will Inspire You To Never Give Up!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/matrix-star-keanu-reeves-s-heart-wrenching-note-about-life-will-inspire-you-to-never-give-up-247597.html" title="Matrix Star Keanu Reeves' Heart-Wrenching Note About Life Will Inspire You To Never Give Up!">
                            Matrix Star Keanu Reeves' Heart-Wrenching Note About Life Will Inspire You To Never Give Up!                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_1">
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/he-decided-to-help-this-pretty-woman-with-a-flat-tyre-and-learnt-a-lesson-he-ll-never-forget-247583.html'>video</a>                        <a href="http://www.indiatimes.com/videocafe/he-decided-to-help-this-pretty-woman-with-a-flat-tyre-and-learnt-a-lesson-he-ll-never-forget-247583.html" class="tint" title="He Decided To Help This Pretty Woman With A Flat Tyre And Learnt A Lesson He'll Never Forget!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/videocafe/2015/Nov/help_card_1448178961_218x102.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Nov/help_card_1448178961_218x102.jpg" border="0" alt="He Decided To Help This Pretty Woman With A Flat Tyre And Learnt A Lesson He'll Never Forget!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/he-decided-to-help-this-pretty-woman-with-a-flat-tyre-and-learnt-a-lesson-he-ll-never-forget-247583.html" title="He Decided To Help This Pretty Woman With A Flat Tyre And Learnt A Lesson He'll Never Forget!">
                            He Decided To Help This Pretty Woman With A Flat Tyre And Learnt A Lesson He'll Never Forget!                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_2">
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-is-what-happens-when-you-question-medical-students-general-knowledge-247577.html'>video</a>                        <a href="http://www.indiatimes.com/videocafe/this-is-what-happens-when-you-question-medical-students-general-knowledge-247577.html" class="tint" title="This Is What Happens When You Question Medical Students' General Knowledge!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/videocafe/2015/Nov/medicalstudetns_card_1448173900_218x102.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Nov/medicalstudetns_card_1448173900_218x102.jpg" border="0" alt="This Is What Happens When You Question Medical Students' General Knowledge!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/this-is-what-happens-when-you-question-medical-students-general-knowledge-247577.html" title="This Is What Happens When You Question Medical Students' General Knowledge!">
                            This Is What Happens When You Question Medical Students' General Knowledge!                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_3">
                                        <figure>

                                                <a href="http://www.indiatimes.com/health/tips-tricks/4-life-saving-self-defence-techniques-every-women-needs-to-know-247530.html" class="tint" title="4 Life-Saving Self-Defence Techniques Every Women Needs To Know">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Nov/card_1448009294_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/card_1448009294_218x102.jpg" border="0" alt="4 Life-Saving Self-Defence Techniques Every Women Needs To Know"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/tips-tricks/4-life-saving-self-defence-techniques-every-women-needs-to-know-247530.html" title="4 Life-Saving Self-Defence Techniques Every Women Needs To Know">
                            4 Life-Saving Self-Defence Techniques Every Women Needs To Know                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_4">
                                        <figure>

                                                <a href="http://www.indiatimes.com/entertainment/salman-khan-turns-down-deepika-padukone-s-marriage-proposal-could-it-get-any-worse-247594.html" class="tint" title="Salman Khan Turns Down Deepika Padukone's Marriage Proposal! Could It Get Any Worse?">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Nov/deepika-salman_1448191918_1448191927_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/deepika-salman_1448191918_1448191927_218x102.jpg" border="0" alt="Salman Khan Turns Down Deepika Padukone's Marriage Proposal! Could It Get Any Worse?"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/salman-khan-turns-down-deepika-padukone-s-marriage-proposal-could-it-get-any-worse-247594.html" title="Salman Khan Turns Down Deepika Padukone's Marriage Proposal! Could It Get Any Worse?">
                            Salman Khan Turns Down Deepika Padukone's Marriage Proposal! Could It Get Any Worse?                        </a>
                    </figcaption>
                </div>
                        </div><!--trending-panel end-->
    </section><!--container end-->

<!-- -------------------    end Block 1  ------------------------------------- -->
<section class="big-ads" id="ad1"> 
<div id="bigAd1_slot"></div>
    <script>        
        showBigAD1('bigAd1_slot');
    </script> 
</section>
<!-- -------------------    start Block 2  ------------------------------------- -->

    <section id="hp_block_2" class="container cf" style="border: 0px solid red;"><!--container start-->
        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
                            <div class="news-panel-list cf" id="column1_4"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            6 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/meet-a-farmer-from-bihar-who-grew-an-astonishing-22-4-tonnes-of-rice-on-one-hectar-of-land-247592.html" title="Meet A Farmer From Bihar Who Grew An Astonishing 22.4 Tonnes Of Rice On One Hectar of Land" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/sumantkumar_1448189582_1448189638_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/meet-a-farmer-from-bihar-who-grew-an-astonishing-22-4-tonnes-of-rice-on-one-hectar-of-land-247592.html" title="Meet A Farmer From Bihar Who Grew An Astonishing 22.4 Tonnes Of Rice On One Hectar of Land">
                            Meet A Farmer From Bihar Who Grew An Astonishing 22.4 Tonnes Of Rice On One Hectar of Land                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_5"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            7 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/world/this-18yo-won-rs-1-65-cr-for-explaining-einstein-s-theory-of-relativity-in-the-simplest-way-possible-247590.html" title="This 18YO Won Rs 1.65 Cr For Explaining Einstein's Theory Of Relativity In The Simplest Way Possible" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/theory_1448185874_1448185890_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/world/this-18yo-won-rs-1-65-cr-for-explaining-einstein-s-theory-of-relativity-in-the-simplest-way-possible-247590.html" title="This 18YO Won Rs 1.65 Cr For Explaining Einstein's Theory Of Relativity In The Simplest Way Possible">
                            This 18YO Won Rs 1.65 Cr For Explaining Einstein's Theory Of Relativity In The Simplest Way Possible                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_6"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            7 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/delhiites-may-soon-experience-less-call-drops-as-telcos-add-over-2-000-new-mobile-towers-247588.html" title="Delhiites May Soon Experience Less Call Drops As Telcos Add Over 2,000 New Mobile Towers!" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/mobile-card_1448184898_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/delhiites-may-soon-experience-less-call-drops-as-telcos-add-over-2-000-new-mobile-towers-247588.html" title="Delhiites May Soon Experience Less Call Drops As Telcos Add Over 2,000 New Mobile Towers!">
                            Delhiites May Soon Experience Less Call Drops As Telcos Add Over 2,000 New Mobile Towers!                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_7"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            7 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/here-s-a-humanitarian-organisation-which-is-providing-flood-relief-in-chennai-and-langar-to-syrian-refugees-247587.html" title="Here's A Humanitarian Organisation Which Is Providing Flood Relief In Chennai, And Langar To Syrian Refugees" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/flood_1448184480_1448184492_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/here-s-a-humanitarian-organisation-which-is-providing-flood-relief-in-chennai-and-langar-to-syrian-refugees-247587.html" title="Here's A Humanitarian Organisation Which Is Providing Flood Relief In Chennai, And Langar To Syrian Refugees">
                            Here's A Humanitarian Organisation Which Is Providing Flood Relief In Chennai, And Langar To Syrian Refugees                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_8"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            7 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/more-trouble-for-vijay-mallya-lenders-to-auction-kingfisher-s-movable-assets-247585.html" title="More Trouble For Vijay Mallya, Lenders To Auction Kingfisherâs Movable Assets" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/mallya-card_1448179966_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/more-trouble-for-vijay-mallya-lenders-to-auction-kingfisher-s-movable-assets-247585.html" title="More Trouble For Vijay Mallya, Lenders To Auction Kingfisherâs Movable Assets">
                            More Trouble For Vijay Mallya, Lenders To Auction Kingfisherâs Movable Assets                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/matrix-star-keanu-reeves-s-heart-wrenching-note-about-life-will-inspire-you-to-never-give-up-247597.html" class="tint" title="Matrix Star Keanu Reeves' Heart-Wrenching Note About Life Will Inspire You To Never Give Up!">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/download_1448194822_1448194828_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/matrix-star-keanu-reeves-s-heart-wrenching-note-about-life-will-inspire-you-to-never-give-up-247597.html" title="Matrix Star Keanu Reeves' Heart-Wrenching Note About Life Will Inspire You To Never Give Up!">
                            Matrix Star Keanu Reeves' Heart-Wrenching Note About Life Will Inspire You To Never Give Up!                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/culture/travel/11-of-the-longest-non-stop-flights-in-the-world-247446.html" class="tint" title="11 Of The Longest Non-Stop Flights In The World">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/dubpam_1447837812_1447837823_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/travel/11-of-the-longest-non-stop-flights-in-the-world-247446.html" title="11 Of The Longest Non-Stop Flights In The World">
                            11 Of The Longest Non-Stop Flights In The World                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/salman-khan-turns-down-deepika-padukone-s-marriage-proposal-could-it-get-any-worse-247594.html" class="tint" title="Salman Khan Turns Down Deepika Padukone's Marriage Proposal! Could It Get Any Worse?">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/deepika-salman_1448191918_1448191927_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/salman-khan-turns-down-deepika-padukone-s-marriage-proposal-could-it-get-any-worse-247594.html" title="Salman Khan Turns Down Deepika Padukone's Marriage Proposal! Could It Get Any Worse?">
                            Salman Khan Turns Down Deepika Padukone's Marriage Proposal! Could It Get Any Worse?                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_5"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/culture/who-we-are/8-heartwarming-stories-pakistani-citizens-have-had-to-share-about-india-247283.html" class="tint" title="8 Heartwarming Stories Pakistani Citizens Have Had To Share About India">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1447419551_218x102.jpg" border="0" alt="8 Heartwarming Stories Pakistani Citizens Have Had To Share About India"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/8-heartwarming-stories-pakistani-citizens-have-had-to-share-about-india-247283.html" title="8 Heartwarming Stories Pakistani Citizens Have Had To Share About India">
                            8 Heartwarming Stories Pakistani Citizens Have Had To Share About India                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_6"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/lifestyle/technology/this-social-networking-app-designed-by-iitians-is-helping-neighbours-connect-and-stay-safe-247580.html" class="tint" title="This Social Networking App Designed By IITians Is Helping Neighbours Connect And Stay Safe">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/ggk_1448176880_1448176907_218x102.jpg" border="0" alt="This Social Networking App Designed By IITians Is Helping Neighbours Connect And Stay Safe"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/technology/this-social-networking-app-designed-by-iitians-is-helping-neighbours-connect-and-stay-safe-247580.html" title="This Social Networking App Designed By IITians Is Helping Neighbours Connect And Stay Safe">
                            This Social Networking App Designed By IITians Is Helping Neighbours Connect And Stay Safe                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_7"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/health/healthyliving/11-simple-but-powerful-remedies-to-stop-hair-loss-in-men-247447.html" class="tint" title="11 Simple But Powerful Remedies To Stop Hair Loss In Men">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/hair-loss-card_1448002568_218x102.jpg" border="0" alt="11 Simple But Powerful Remedies To Stop Hair Loss In Men"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/11-simple-but-powerful-remedies-to-stop-hair-loss-in-men-247447.html" title="11 Simple But Powerful Remedies To Stop Hair Loss In Men">
                            11 Simple But Powerful Remedies To Stop Hair Loss In Men                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_8"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/culture/travel/11-of-the-longest-non-stop-flights-in-the-world-247446.html" class="tint" title="11 Of The Longest Non-Stop Flights In The World">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/dubpam_1447837812_1447837823_218x102.jpg" border="0" alt="11 Of The Longest Non-Stop Flights In The World"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/travel/11-of-the-longest-non-stop-flights-in-the-world-247446.html" title="11 Of The Longest Non-Stop Flights In The World">
                            11 Of The Longest Non-Stop Flights In The World                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_9"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/lifestyle/self/how-to-seduce-and-get-each-zodiac-sign-to-go-on-a-date-with-you-247492.html" class="tint" title="How To Seduce And Get Each Zodiac Sign To Go On A Date With You">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1448001198_218x102.jpg" border="0" alt="How To Seduce And Get Each Zodiac Sign To Go On A Date With You"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/how-to-seduce-and-get-each-zodiac-sign-to-go-on-a-date-with-you-247492.html" title="How To Seduce And Get Each Zodiac Sign To Go On A Date With You">
                            How To Seduce And Get Each Zodiac Sign To Go On A Date With You                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div>

    </section><!--container end-->

<!--------------------------- end Block2 ------------------------------------------>
<section class="big-ads" id="ad2"> 
<div id="bigAd2_slot"></div>
</section>
<!--------------------------- start Block3 ------------------------------------------>

    <section id="hp_block_3" class="container cf"><!--container start-->

        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
          
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_9">
                    <figure>
                        <div class="hash-tag">
                            8 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/world/this-mythical-looking-creature-is-very-real-and-is-called-the-blue-dragon-247582.html" title="This Mythical Looking Creature Is Very Real And Is Called The Blue Dragon" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/bluedragon_1448178596_1448178619_236x111.jpg" border="0" alt="This Mythical Looking Creature Is Very Real And Is Called The Blue Dragon"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/this-mythical-looking-creature-is-very-real-and-is-called-the-blue-dragon-247582.html" title="This Mythical Looking Creature Is Very Real And Is Called The Blue Dragon">
                            This Mythical Looking Creature Is Very Real And Is Called The Blue Dragon                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_10">
                    <figure>
                        <div class="hash-tag">
                            8 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/weird/43-of-the-most-adorable-pictures-of-animals-taken-for-the-2015-comedy-wildlife-photography-awards-247236.html" title="43 Of The Most Adorable Pictures Of Animals Taken For The 2015 Comedy Wildlife Photography Awards" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/ape_1447322825_1447322834_236x111.jpg" border="0" alt="43 Of The Most Adorable Pictures Of Animals Taken For The 2015 Comedy Wildlife Photography Awards"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/weird/43-of-the-most-adorable-pictures-of-animals-taken-for-the-2015-comedy-wildlife-photography-awards-247236.html" title="43 Of The Most Adorable Pictures Of Animals Taken For The 2015 Comedy Wildlife Photography Awards">
                            43 Of The Most Adorable Pictures Of Animals Taken For The 2015 Comedy Wildlife Photography Awards                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_11">
                    <figure>
                        <div class="hash-tag">
                            8 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/a-woman-has-complete-right-over-her-property-even-after-separation-from-husband-says-supreme-court-247579.html" title="Wife Has Complete Right Over Her Property Even After Separation From Husband Says Supreme Court" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/dowry-money-card_1448176650_236x111.jpg" border="0" alt="Wife Has Complete Right Over Her Property Even After Separation From Husband Says Supreme Court"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/a-woman-has-complete-right-over-her-property-even-after-separation-from-husband-says-supreme-court-247579.html" title="Wife Has Complete Right Over Her Property Even After Separation From Husband Says Supreme Court">
                            Wife Has Complete Right Over Her Property Even After Separation From Husband Says Supreme Court                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_12">
                    <figure>
                        <div class="hash-tag">
                            9 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/hindustan-is-for-hindus-says-assam-governor-pb-acharya-247586.html" title="'Hindustan Is For Hindus' Says Assam Governor PB Acharya" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/pb-acharya-addresses-media-card_1448181319_236x111.jpg" border="0" alt="'Hindustan Is For Hindus' Says Assam Governor PB Acharya"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/hindustan-is-for-hindus-says-assam-governor-pb-acharya-247586.html" title="'Hindustan Is For Hindus' Says Assam Governor PB Acharya">
                            'Hindustan Is For Hindus' Says Assam Governor PB Acharya                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_13">
                    <figure>
                        <div class="hash-tag">
                            11 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/the-humble-one-rupee-note-is-being-sold-online-for-1000-times-its-value-247575.html" title="The Humble One Rupee Note Is Being Sold Online For 1000 Times Its Value!" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/onerupee_1448173115_1448173132_236x111.jpg" border="0" alt="The Humble One Rupee Note Is Being Sold Online For 1000 Times Its Value!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/the-humble-one-rupee-note-is-being-sold-online-for-1000-times-its-value-247575.html" title="The Humble One Rupee Note Is Being Sold Online For 1000 Times Its Value!">
                            The Humble One Rupee Note Is Being Sold Online For 1000 Times Its Value!                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/bajrangi-bhaijaan-s-munni-pulled-off-a-madhuri-dixit-for-salman-uncle-it-s-totally-adorbs-247578.html" class="tint" title="Bajrangi Bhaijaan's Munni Pulled Off A 'Madhuri Dixit' For Salman Uncle & It's Totally Adorbs!">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/bm1_1448176311_1448176319_502x234.jpg" border="0" alt="http://www.indiatimes.com/entertainment/bajrangi-bhaijaan-s-munni-pulled-off-a-madhuri-dixit-for-salman-uncle-it-s-totally-adorbs-247578.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bajrangi-bhaijaan-s-munni-pulled-off-a-madhuri-dixit-for-salman-uncle-it-s-totally-adorbs-247578.html" title="Bajrangi Bhaijaan's Munni Pulled Off A 'Madhuri Dixit' For Salman Uncle & It's Totally Adorbs!">
                            Bajrangi Bhaijaan's Munni Pulled Off A 'Madhuri Dixit' For Salman Uncle & It's Totally Adorbs!                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/after-kaho-na-pyaar-hai-hrithik-roshan-rakesh-roshan-to-join-hands-for-a-romantic-film-247589.html" class="tint" title="After Kaho Na Pyaar Hai, Hrithik Roshan & Rakesh Roshan To Join Hands For A Romantic Film!">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/hh_1448185990_1448186003_502x234.jpg" border="0" alt="http://www.indiatimes.com/entertainment/after-kaho-na-pyaar-hai-hrithik-roshan-rakesh-roshan-to-join-hands-for-a-romantic-film-247589.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/after-kaho-na-pyaar-hai-hrithik-roshan-rakesh-roshan-to-join-hands-for-a-romantic-film-247589.html" title="After Kaho Na Pyaar Hai, Hrithik Roshan & Rakesh Roshan To Join Hands For A Romantic Film!">
                            After Kaho Na Pyaar Hai, Hrithik Roshan & Rakesh Roshan To Join Hands For A Romantic Film!                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/healthyliving/here-s-how-you-can-manage-weight-loss-according-to-your-different-body-type-247526.html" class="tint" title="Here's How You Can Manage Weight Loss According To Your Different Body Type">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card-3_1448011823_502x234.jpg" border="0" alt="http://www.indiatimes.com/health/healthyliving/here-s-how-you-can-manage-weight-loss-according-to-your-different-body-type-247526.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/here-s-how-you-can-manage-weight-loss-according-to-your-different-body-type-247526.html" title="Here's How You Can Manage Weight Loss According To Your Different Body Type">
                            Here's How You Can Manage Weight Loss According To Your Different Body Type                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_10"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/priyanka-chopra-broke-down-on-the-sets-of-bajirao-mastani-almost-quit-the-film-247576.html" class="tint" title="Priyanka Chopra Broke Down On The Sets Of Bajirao Mastani & Almost Quit The Film!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/pc_1448173395_1448173404_218x102.jpg" border="0" alt="Priyanka Chopra Broke Down On The Sets Of Bajirao Mastani & Almost Quit The Film!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/priyanka-chopra-broke-down-on-the-sets-of-bajirao-mastani-almost-quit-the-film-247576.html" title="Priyanka Chopra Broke Down On The Sets Of Bajirao Mastani & Almost Quit The Film!">
                            Priyanka Chopra Broke Down On The Sets Of Bajirao Mastani & Almost Quit The Film!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_11"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/a-selfie-with-kareena-kapoor-lands-chhattisgarh-cm-in-major-trouble-faces-flak-247572.html" class="tint" title="A Selfie With Kareena Kapoor Lands Chhattisgarh CM In Major Trouble, Faces Flak!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/kareena_1448170924_1448170935_218x102.jpg" border="0" alt="A Selfie With Kareena Kapoor Lands Chhattisgarh CM In Major Trouble, Faces Flak!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/a-selfie-with-kareena-kapoor-lands-chhattisgarh-cm-in-major-trouble-faces-flak-247572.html" title="A Selfie With Kareena Kapoor Lands Chhattisgarh CM In Major Trouble, Faces Flak!">
                            A Selfie With Kareena Kapoor Lands Chhattisgarh CM In Major Trouble, Faces Flak!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_12"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/there-has-to-be-a-lakshman-rekha-says-censor-board-chief-pahlaj-nihalani-on-censorship-in-films-247584.html" class="tint" title="There Has To Be A 'Lakshman Rekha', Says Censor Board Chief Pahlaj Nihalani On Censorship In Films">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/censor_1448183646_1448183652_218x102.jpg" border="0" alt="There Has To Be A 'Lakshman Rekha', Says Censor Board Chief Pahlaj Nihalani On Censorship In Films"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/there-has-to-be-a-lakshman-rekha-says-censor-board-chief-pahlaj-nihalani-on-censorship-in-films-247584.html" title="There Has To Be A 'Lakshman Rekha', Says Censor Board Chief Pahlaj Nihalani On Censorship In Films">
                            There Has To Be A 'Lakshman Rekha', Says Censor Board Chief Pahlaj Nihalani On Censorship In Films                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_13"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/culture/who-we-are/9-people-who-quit-their-high-paying-jobs-and-took-the-road-less-traveled-247506.html" class="tint" title="9 People Who Quit Their High-Paying Jobs And Took The Road Less Traveled">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/changing-life_1447933496_218x102.jpg" border="0" alt="9 People Who Quit Their High-Paying Jobs And Took The Road Less Traveled"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/9-people-who-quit-their-high-paying-jobs-and-took-the-road-less-traveled-247506.html" title="9 People Who Quit Their High-Paying Jobs And Took The Road Less Traveled">
                            9 People Who Quit Their High-Paying Jobs And Took The Road Less Traveled                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_14"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/lifestyle/self/11-things-your-mom-felt-and-never-shared-with-you-247442.html" class="tint" title="11 Things Your Mom Felt And Never Shared With You">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/k3g1_1447833379_218x102.jpg" border="0" alt="11 Things Your Mom Felt And Never Shared With You"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/11-things-your-mom-felt-and-never-shared-with-you-247442.html" title="11 Things Your Mom Felt And Never Shared With You">
                            11 Things Your Mom Felt And Never Shared With You                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div><!--trending-panel end-->

    </section><!--container end-->

<!------------------------------- end Block 3 ---------------------------------------->
<section class="big-ads">
<div id="bigAd3_slot"></div>
</section>

<!------------------------------- start Block4  -------------------------------------->

    <section class="container cf" id="container4"><!--container start-->
        <div class="news-panel cf ">
            <h2>news</h2>

                            <div class="news-panel-list cf" id="column1_14">
                    <figure>
                        <div class="hash-tag">
                            11 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/sports/it-s-official-manchester-united-boss-wants-cristiano-ronaldo-back-to-stay-in-premier-league-title-race-247581.html" title="It's Official! Manchester United Boss Wants Cristiano Ronaldo Back, To Stay In Premier League Title Race" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/vangaalronaldo_1448176416_236x111.jpg" border="0" alt="It's Official! Manchester United Boss Wants Cristiano Ronaldo Back, To Stay In Premier League Title Race"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/it-s-official-manchester-united-boss-wants-cristiano-ronaldo-back-to-stay-in-premier-league-title-race-247581.html" title="It's Official! Manchester United Boss Wants Cristiano Ronaldo Back, To Stay In Premier League Title Race">
                            It's Official! Manchester United Boss Wants Cristiano Ronaldo Back, To Stay In Premier League Title Race                        </a>
                    </figcaption> 
                </div>
                <div class='container1'>                <div class="news-panel-list cf" id="column1_15">
                    <figure>
                        <div class="hash-tag">
                            11 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/beti-bachao-this-pune-doctor-doesn-t-charge-a-penny-if-the-newborn-is-a-baby-girl-247574.html" title="Beti Bachao! This Pune Doctor Doesn't Charge A Penny If The Newborn Is A Baby Girl!" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/36129565-card_1448172592_236x111.jpg" border="0" alt="Beti Bachao! This Pune Doctor Doesn't Charge A Penny If The Newborn Is A Baby Girl!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/beti-bachao-this-pune-doctor-doesn-t-charge-a-penny-if-the-newborn-is-a-baby-girl-247574.html" title="Beti Bachao! This Pune Doctor Doesn't Charge A Penny If The Newborn Is A Baby Girl!">
                            Beti Bachao! This Pune Doctor Doesn't Charge A Penny If The Newborn Is A Baby Girl!                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_16">
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/india-is-4th-most-vacation-deprived-nation-in-the-world-well-we-are-not-surprised-247573.html" title="India Is 4th Most Vacation-Deprived Nation In The World! Well, We Are Not Surprised" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/o-holiday-airport-facebook_1448171108_1448171117_236x111.jpg" border="0" alt="India Is 4th Most Vacation-Deprived Nation In The World! Well, We Are Not Surprised"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/india-is-4th-most-vacation-deprived-nation-in-the-world-well-we-are-not-surprised-247573.html" title="India Is 4th Most Vacation-Deprived Nation In The World! Well, We Are Not Surprised">
                            India Is 4th Most Vacation-Deprived Nation In The World! Well, We Are Not Surprised                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_17">
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/science-is-my-religion-says-monk-who-won-the-biggest-maths-prize-in-the-country-247571.html" title="Science Is My Religion Says Monk Who Won The Biggest Maths Prize In The Country" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/maharaj-card_1448169401_236x111.jpg" border="0" alt="Science Is My Religion Says Monk Who Won The Biggest Maths Prize In The Country"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/science-is-my-religion-says-monk-who-won-the-biggest-maths-prize-in-the-country-247571.html" title="Science Is My Religion Says Monk Who Won The Biggest Maths Prize In The Country">
                            Science Is My Religion Says Monk Who Won The Biggest Maths Prize In The Country                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_18">
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/french-president-francois-hollande-to-be-the-chief-guest-at-2016-republic-day-parade-247570.html" title="French President Francois Hollande To Be The Chief Guest At 2016 Republic Day Parade" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/hollande_1448169249_1448169261_236x111.jpg" border="0" alt="French President Francois Hollande To Be The Chief Guest At 2016 Republic Day Parade"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/french-president-francois-hollande-to-be-the-chief-guest-at-2016-republic-day-parade-247570.html" title="French President Francois Hollande To Be The Chief Guest At 2016 Republic Day Parade">
                            French President Francois Hollande To Be The Chief Guest At 2016 Republic Day Parade                        </a>
                    </figcaption> 
                </div>
                 
        </div>
    </div><!--news-panel end-->

    <div class="life-panel cf container2" id="b4c2"><!--life-panel start-->
        <h2>lifestyle</h2>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-is-what-happens-when-you-question-medical-students-general-knowledge-247577.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-is-what-happens-when-you-question-medical-students-general-knowledge-247577.html" class="tint" title="This Is What Happens When You Question Medical Students' General Knowledge!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/medicalstudetns_card_1448173900_502x234.jpg" border="0" alt="This Is What Happens When You Question Medical Students' General Knowledge!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-is-what-happens-when-you-question-medical-students-general-knowledge-247577.html" title="This Is What Happens When You Question Medical Students' General Knowledge!">
                        This Is What Happens When You Question Medical Students' General Knowledge!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/he-decided-to-help-this-pretty-woman-with-a-flat-tyre-and-learnt-a-lesson-he-ll-never-forget-247583.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/he-decided-to-help-this-pretty-woman-with-a-flat-tyre-and-learnt-a-lesson-he-ll-never-forget-247583.html" class="tint" title="He Decided To Help This Pretty Woman With A Flat Tyre And Learnt A Lesson He'll Never Forget!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/help_card_1448178961_502x234.jpg" border="0" alt="He Decided To Help This Pretty Woman With A Flat Tyre And Learnt A Lesson He'll Never Forget!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/he-decided-to-help-this-pretty-woman-with-a-flat-tyre-and-learnt-a-lesson-he-ll-never-forget-247583.html" title="He Decided To Help This Pretty Woman With A Flat Tyre And Learnt A Lesson He'll Never Forget!">
                        He Decided To Help This Pretty Woman With A Flat Tyre And Learnt A Lesson He'll Never Forget!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/there-has-to-be-a-lakshman-rekha-says-censor-board-chief-pahlaj-nihalani-on-censorship-in-films-247584.html" class="tint" title="There Has To Be A 'Lakshman Rekha', Says Censor Board Chief Pahlaj Nihalani On Censorship In Films">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/censor_1448183646_1448183652_502x234.jpg" border="0" alt="There Has To Be A 'Lakshman Rekha', Says Censor Board Chief Pahlaj Nihalani On Censorship In Films" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/there-has-to-be-a-lakshman-rekha-says-censor-board-chief-pahlaj-nihalani-on-censorship-in-films-247584.html" title="There Has To Be A 'Lakshman Rekha', Says Censor Board Chief Pahlaj Nihalani On Censorship In Films">
                        There Has To Be A 'Lakshman Rekha', Says Censor Board Chief Pahlaj Nihalani On Censorship In Films                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/this-social-networking-app-designed-by-iitians-is-helping-neighbours-connect-and-stay-safe-247580.html" class="tint" title="This Social Networking App Designed By IITians Is Helping Neighbours Connect And Stay Safe">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/ggk_1448176880_1448176907_502x234.jpg" border="0" alt="This Social Networking App Designed By IITians Is Helping Neighbours Connect And Stay Safe" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/this-social-networking-app-designed-by-iitians-is-helping-neighbours-connect-and-stay-safe-247580.html" title="This Social Networking App Designed By IITians Is Helping Neighbours Connect And Stay Safe">
                        This Social Networking App Designed By IITians Is Helping Neighbours Connect And Stay Safe                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/4-life-saving-self-defence-techniques-every-women-needs-to-know-247530.html" class="tint" title="4 Life-Saving Self-Defence Techniques Every Women Needs To Know">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1448009294_502x234.jpg" border="0" alt="4 Life-Saving Self-Defence Techniques Every Women Needs To Know" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/4-life-saving-self-defence-techniques-every-women-needs-to-know-247530.html" title="4 Life-Saving Self-Defence Techniques Every Women Needs To Know">
                        4 Life-Saving Self-Defence Techniques Every Women Needs To Know                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/priyanka-chopra-broke-down-on-the-sets-of-bajirao-mastani-almost-quit-the-film-247576.html" class="tint" title="Priyanka Chopra Broke Down On The Sets Of Bajirao Mastani & Almost Quit The Film!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/pc_1448173395_1448173404_502x234.jpg" border="0" alt="Priyanka Chopra Broke Down On The Sets Of Bajirao Mastani & Almost Quit The Film!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/priyanka-chopra-broke-down-on-the-sets-of-bajirao-mastani-almost-quit-the-film-247576.html" title="Priyanka Chopra Broke Down On The Sets Of Bajirao Mastani & Almost Quit The Film!">
                        Priyanka Chopra Broke Down On The Sets Of Bajirao Mastani & Almost Quit The Film!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/8-heartwarming-stories-pakistani-citizens-have-had-to-share-about-india-247283.html" class="tint" title="8 Heartwarming Stories Pakistani Citizens Have Had To Share About India">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1447419551_502x234.jpg" border="0" alt="8 Heartwarming Stories Pakistani Citizens Have Had To Share About India" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/8-heartwarming-stories-pakistani-citizens-have-had-to-share-about-india-247283.html" title="8 Heartwarming Stories Pakistani Citizens Have Had To Share About India">
                        8 Heartwarming Stories Pakistani Citizens Have Had To Share About India                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/a-selfie-with-kareena-kapoor-lands-chhattisgarh-cm-in-major-trouble-faces-flak-247572.html" class="tint" title="A Selfie With Kareena Kapoor Lands Chhattisgarh CM In Major Trouble, Faces Flak!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/kareena_1448170924_1448170935_502x234.jpg" border="0" alt="A Selfie With Kareena Kapoor Lands Chhattisgarh CM In Major Trouble, Faces Flak!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/a-selfie-with-kareena-kapoor-lands-chhattisgarh-cm-in-major-trouble-faces-flak-247572.html" title="A Selfie With Kareena Kapoor Lands Chhattisgarh CM In Major Trouble, Faces Flak!">
                        A Selfie With Kareena Kapoor Lands Chhattisgarh CM In Major Trouble, Faces Flak!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/11-simple-but-powerful-remedies-to-stop-hair-loss-in-men-247447.html" class="tint" title="11 Simple But Powerful Remedies To Stop Hair Loss In Men">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/hair-loss-card_1448002568_502x234.jpg" border="0" alt="11 Simple But Powerful Remedies To Stop Hair Loss In Men" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/11-simple-but-powerful-remedies-to-stop-hair-loss-in-men-247447.html" title="11 Simple But Powerful Remedies To Stop Hair Loss In Men">
                        11 Simple But Powerful Remedies To Stop Hair Loss In Men                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/9-things-you-ll-relate-to-if-you-were-raised-by-really-artsy-parents-247453.html" class="tint" title="9 Things You'll Relate To If You Were Raised By Really Artsy Parents">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cp_1447842144_502x234.jpg" border="0" alt="9 Things You'll Relate To If You Were Raised By Really Artsy Parents" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/9-things-you-ll-relate-to-if-you-were-raised-by-really-artsy-parents-247453.html" title="9 Things You'll Relate To If You Were Raised By Really Artsy Parents">
                        9 Things You'll Relate To If You Were Raised By Really Artsy Parents                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/6-things-you-need-to-know-about-priya-malik-the-next-wild-card-entry-on-bigg-boss-247564.html" class="tint" title="6 Things You Need To Know About Priya Malik, The Next Wild Card Entry On Bigg Boss">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1448101746_1448101752_502x234.jpg" border="0" alt="6 Things You Need To Know About Priya Malik, The Next Wild Card Entry On Bigg Boss" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/6-things-you-need-to-know-about-priya-malik-the-next-wild-card-entry-on-bigg-boss-247564.html" title="6 Things You Need To Know About Priya Malik, The Next Wild Card Entry On Bigg Boss">
                        6 Things You Need To Know About Priya Malik, The Next Wild Card Entry On Bigg Boss                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/an-apology-to-our-readers-247567.html" class="tint" title="An Apology To Our Readers">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card-12_1448108431_1448108440_502x234.jpg" border="0" alt="An Apology To Our Readers" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/an-apology-to-our-readers-247567.html" title="An Apology To Our Readers">
                        An Apology To Our Readers                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/9-people-who-quit-their-high-paying-jobs-and-took-the-road-less-traveled-247506.html" class="tint" title="9 People Who Quit Their High-Paying Jobs And Took The Road Less Traveled">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/changing-life_1447933496_502x234.jpg" border="0" alt="9 People Who Quit Their High-Paying Jobs And Took The Road Less Traveled" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/9-people-who-quit-their-high-paying-jobs-and-took-the-road-less-traveled-247506.html" title="9 People Who Quit Their High-Paying Jobs And Took The Road Less Traveled">
                        9 People Who Quit Their High-Paying Jobs And Took The Road Less Traveled                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/meet-vineet-vincent-india-s-best-beatboxer-he-can-give-any-drummer-a-run-for-their-money-246949.html" class="tint" title="Meet Vineet Vincent, India's Best Beatboxer. He Can Give Any Drummer A Run For Their Money">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/vineeth_1446613711_1446613722_502x234.jpg" border="0" alt="Meet Vineet Vincent, India's Best Beatboxer. He Can Give Any Drummer A Run For Their Money" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/meet-vineet-vincent-india-s-best-beatboxer-he-can-give-any-drummer-a-run-for-their-money-246949.html" title="Meet Vineet Vincent, India's Best Beatboxer. He Can Give Any Drummer A Run For Their Money">
                        Meet Vineet Vincent, India's Best Beatboxer. He Can Give Any Drummer A Run For Their Money                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/priyanka-chopra-might-play-astronaut-kalpana-chawla-in-a-new-biopic-247557.html" class="tint" title="Priyanka Chopra Might Play Astronaut Kalpana Chawla In A New Biopic">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/dcr_1448094742_1448094749_502x234.jpg" border="0" alt="Priyanka Chopra Might Play Astronaut Kalpana Chawla In A New Biopic" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/priyanka-chopra-might-play-astronaut-kalpana-chawla-in-a-new-biopic-247557.html" title="Priyanka Chopra Might Play Astronaut Kalpana Chawla In A New Biopic">
                        Priyanka Chopra Might Play Astronaut Kalpana Chawla In A New Biopic                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/ranveer-singh-did-not-charge-a-rupee-for-bajirao-mastani-instead-he-has-a-share-in-the-film-s-profits-247556.html" class="tint" title="Ranveer Singh Did Not Charge A Rupee For 'Bajirao Mastani'. Instead, He Has A Share In The Film's Profits">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/br_1448092100_1448092105_502x234.jpg" border="0" alt="Ranveer Singh Did Not Charge A Rupee For 'Bajirao Mastani'. Instead, He Has A Share In The Film's Profits" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/ranveer-singh-did-not-charge-a-rupee-for-bajirao-mastani-instead-he-has-a-share-in-the-film-s-profits-247556.html" title="Ranveer Singh Did Not Charge A Rupee For 'Bajirao Mastani'. Instead, He Has A Share In The Film's Profits">
                        Ranveer Singh Did Not Charge A Rupee For 'Bajirao Mastani'. Instead, He Has A Share In The Film's Profits                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/11-things-your-mom-felt-and-never-shared-with-you-247442.html" class="tint" title="11 Things Your Mom Felt And Never Shared With You">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/k3g1_1447833379_502x234.jpg" border="0" alt="11 Things Your Mom Felt And Never Shared With You" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/11-things-your-mom-felt-and-never-shared-with-you-247442.html" title="11 Things Your Mom Felt And Never Shared With You">
                        11 Things Your Mom Felt And Never Shared With You                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/the-trailer-of-bajirao-mastani-is-here-and-it-will-teleport-you-to-an-era-of-love-war-and-royalty-247547.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/the-trailer-of-bajirao-mastani-is-here-and-it-will-teleport-you-to-an-era-of-love-war-and-royalty-247547.html" class="tint" title="The Trailer Of 'Bajirao Mastani' Is Here And It Will Teleport You To An Era Of Love, War And Royalty">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/crd_1448044507_1448044512_502x234.jpg" border="0" alt="The Trailer Of 'Bajirao Mastani' Is Here And It Will Teleport You To An Era Of Love, War And Royalty" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/the-trailer-of-bajirao-mastani-is-here-and-it-will-teleport-you-to-an-era-of-love-war-and-royalty-247547.html" title="The Trailer Of 'Bajirao Mastani' Is Here And It Will Teleport You To An Era Of Love, War And Royalty">
                        The Trailer Of 'Bajirao Mastani' Is Here And It Will Teleport You To An Era Of Love, War And Royalty                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/recipes/13-mint-recipes-that-great-for-digestion-and-good-health-247435.html" class="tint" title="13 Mint Recipes That Great For Digestion And Good Health">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card-mint_1447833703_502x234.jpg" border="0" alt="13 Mint Recipes That Great For Digestion And Good Health" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/recipes/13-mint-recipes-that-great-for-digestion-and-good-health-247435.html" title="13 Mint Recipes That Great For Digestion And Good Health">
                        13 Mint Recipes That Great For Digestion And Good Health                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/18-pictures-that-depict-the-beautiful-aishwarya-rai-s-life-and-her-milestones-247529.html" class="tint" title="18 Pictures That Depict The Beautiful Aishwarya Rai's Life And Her Milestones">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1448023126_1448023133_502x234.jpg" border="0" alt="18 Pictures That Depict The Beautiful Aishwarya Rai's Life And Her Milestones" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/18-pictures-that-depict-the-beautiful-aishwarya-rai-s-life-and-her-milestones-247529.html" title="18 Pictures That Depict The Beautiful Aishwarya Rai's Life And Her Milestones">
                        18 Pictures That Depict The Beautiful Aishwarya Rai's Life And Her Milestones                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/7-people-narrate-their-most-embarrassing-puberty-stories-247378.html" class="tint" title="7 People Narrate Their Most Embarrassing Puberty Stories">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/puberty-story-card_1447747582_502x234.jpg" border="0" alt="7 People Narrate Their Most Embarrassing Puberty Stories" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/7-people-narrate-their-most-embarrassing-puberty-stories-247378.html" title="7 People Narrate Their Most Embarrassing Puberty Stories">
                        7 People Narrate Their Most Embarrassing Puberty Stories                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/shraddha-kapoor-s-travel-diaries-from-shillong-and-kerala-show-you-just-how-beautiful-india-is-247536.html" class="tint" title="Shraddha Kapoor's Travel Diaries From Shillong And Kerala Show You Just How Beautiful India Is!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/screenshot_5_1448012535_1448012543_502x234.jpg" border="0" alt="Shraddha Kapoor's Travel Diaries From Shillong And Kerala Show You Just How Beautiful India Is!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/shraddha-kapoor-s-travel-diaries-from-shillong-and-kerala-show-you-just-how-beautiful-india-is-247536.html" title="Shraddha Kapoor's Travel Diaries From Shillong And Kerala Show You Just How Beautiful India Is!">
                        Shraddha Kapoor's Travel Diaries From Shillong And Kerala Show You Just How Beautiful India Is!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/10-good-bad-and-ugly-snippets-from-the-life-and-times-of-tipu-sultan-247532.html" class="tint" title="10 Good, Bad And Ugly Snippets From The Life And Times Of Tipu Sultan">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1448010598_502x234.jpg" border="0" alt="10 Good, Bad And Ugly Snippets From The Life And Times Of Tipu Sultan" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/10-good-bad-and-ugly-snippets-from-the-life-and-times-of-tipu-sultan-247532.html" title="10 Good, Bad And Ugly Snippets From The Life And Times Of Tipu Sultan">
                        10 Good, Bad And Ugly Snippets From The Life And Times Of Tipu Sultan                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/more-people-are-terrified-of-having-unprotected-sex-than-dying-in-a-car-crash-247405.html" class="tint" title="More People Are Terrified Of Having Unprotected Sex Than Dying In A Car Crash!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/_63832137_jeans think_1447756237_1447756241_502x234.jpg" border="0" alt="More People Are Terrified Of Having Unprotected Sex Than Dying In A Car Crash!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/more-people-are-terrified-of-having-unprotected-sex-than-dying-in-a-car-crash-247405.html" title="More People Are Terrified Of Having Unprotected Sex Than Dying In A Car Crash!">
                        More People Are Terrified Of Having Unprotected Sex Than Dying In A Car Crash!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/remember-item-queen-of-the-80s-kalpana-iyer-here-s-what-she-s-up-to-these-days-247527.html" class="tint" title="Remember Item Queen Of The 80s, Kalpana Iyer? Here's What She's Up To These Days">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1448008651_1448008661_502x234.jpg" border="0" alt="Remember Item Queen Of The 80s, Kalpana Iyer? Here's What She's Up To These Days" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/remember-item-queen-of-the-80s-kalpana-iyer-here-s-what-she-s-up-to-these-days-247527.html" title="Remember Item Queen Of The 80s, Kalpana Iyer? Here's What She's Up To These Days">
                        Remember Item Queen Of The 80s, Kalpana Iyer? Here's What She's Up To These Days                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/this-couple-make-the-most-of-their-long-distance-relationship-by-posting-fantastic-collages-across-cities-247479.html" class="tint" title="This Couple Make The Most Of Their Long Distance Relationship By Posting Fantastic Collages Across Cities">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cp_1447918006_502x234.jpg" border="0" alt="This Couple Make The Most Of Their Long Distance Relationship By Posting Fantastic Collages Across Cities" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/this-couple-make-the-most-of-their-long-distance-relationship-by-posting-fantastic-collages-across-cities-247479.html" title="This Couple Make The Most Of Their Long Distance Relationship By Posting Fantastic Collages Across Cities">
                        This Couple Make The Most Of Their Long Distance Relationship By Posting Fantastic Collages Across Cities                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/5-types-of-pain-you-shouldn-t-try-to-treat-at-home-247491.html" class="tint" title="5 Types Of Pain You Shouldnât Try To Treat At Home">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cover_1447924339_502x234.jpg" border="0" alt="5 Types Of Pain You Shouldnât Try To Treat At Home" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/5-types-of-pain-you-shouldn-t-try-to-treat-at-home-247491.html" title="5 Types Of Pain You Shouldnât Try To Treat At Home">
                        5 Types Of Pain You Shouldnât Try To Treat At Home                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/watch-this-day-21-years-ago-aishwarya-rai-was-crowned-the-most-beautiful-woman-in-the-world-247525.html" class="tint" title="WATCH: 21 Years Ago, Aishwarya Rai Was Crowned The Most Beautiful Woman In The World">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1448004346_502x234.jpg" border="0" alt="WATCH: 21 Years Ago, Aishwarya Rai Was Crowned The Most Beautiful Woman In The World" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/watch-this-day-21-years-ago-aishwarya-rai-was-crowned-the-most-beautiful-woman-in-the-world-247525.html" title="WATCH: 21 Years Ago, Aishwarya Rai Was Crowned The Most Beautiful Woman In The World">
                        WATCH: 21 Years Ago, Aishwarya Rai Was Crowned The Most Beautiful Woman In The World                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/ranbir-kapoor-croons-for-the-tamasha-crew-at-the-film-s-wrap-up-party-in-corsica-247521.html" class="tint" title="Ranbir Kapoor Croons For The 'Tamasha' Crew At The Film's Wrap-Up Party In Corsica!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1448000688_1448000695_502x234.jpg" border="0" alt="Ranbir Kapoor Croons For The 'Tamasha' Crew At The Film's Wrap-Up Party In Corsica!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/ranbir-kapoor-croons-for-the-tamasha-crew-at-the-film-s-wrap-up-party-in-corsica-247521.html" title="Ranbir Kapoor Croons For The 'Tamasha' Crew At The Film's Wrap-Up Party In Corsica!">
                        Ranbir Kapoor Croons For The 'Tamasha' Crew At The Film's Wrap-Up Party In Corsica!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/19-awkward-jokes-you-should-crack-on-a-boring-day-at-work-247452.html" class="tint" title="19 Awkward Jokes You Should Crack On A Boring Day At Work">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1447846941_502x234.jpg" border="0" alt="19 Awkward Jokes You Should Crack On A Boring Day At Work" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/19-awkward-jokes-you-should-crack-on-a-boring-day-at-work-247452.html" title="19 Awkward Jokes You Should Crack On A Boring Day At Work">
                        19 Awkward Jokes You Should Crack On A Boring Day At Work                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/watch-rajiv-gandhi-justify-the-1984-sikh-riots-in-this-31-year-old-video-that-just-surfaced-247519.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/watch-rajiv-gandhi-justify-the-1984-sikh-riots-in-this-31-year-old-video-that-just-surfaced-247519.html" class="tint" title="Watch Rajiv Gandhi Justify The 1984 Sikh Riots In This 31-Year-Old Video That Just Surfaced">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/gandhi5_1447998184_502x234.jpg" border="0" alt="Watch Rajiv Gandhi Justify The 1984 Sikh Riots In This 31-Year-Old Video That Just Surfaced" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/watch-rajiv-gandhi-justify-the-1984-sikh-riots-in-this-31-year-old-video-that-just-surfaced-247519.html" title="Watch Rajiv Gandhi Justify The 1984 Sikh Riots In This 31-Year-Old Video That Just Surfaced">
                        Watch Rajiv Gandhi Justify The 1984 Sikh Riots In This 31-Year-Old Video That Just Surfaced                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/11-things-you-can-do-if-you-eat-something-spicy-or-if-chilli-gets-into-your-eyes-247483.html" class="tint" title="11 Things You Can Do If You Eat Something Spicy, Or If Chilli Gets Into Your Eyes!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1447919370_502x234.jpg" border="0" alt="11 Things You Can Do If You Eat Something Spicy, Or If Chilli Gets Into Your Eyes!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/11-things-you-can-do-if-you-eat-something-spicy-or-if-chilli-gets-into-your-eyes-247483.html" title="11 Things You Can Do If You Eat Something Spicy, Or If Chilli Gets Into Your Eyes!">
                        11 Things You Can Do If You Eat Something Spicy, Or If Chilli Gets Into Your Eyes!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/9-evil-things-your-elder-sibling-did-to-you-when-you-were-a-kid-that-are-hilarious-today-247428.html" class="tint" title="9 Evil Things Your Elder Sibling Did To You When You Were A Kid That Are Hilarious Today">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1447839425_502x234.jpg" border="0" alt="9 Evil Things Your Elder Sibling Did To You When You Were A Kid That Are Hilarious Today" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/9-evil-things-your-elder-sibling-did-to-you-when-you-were-a-kid-that-are-hilarious-today-247428.html" title="9 Evil Things Your Elder Sibling Did To You When You Were A Kid That Are Hilarious Today">
                        9 Evil Things Your Elder Sibling Did To You When You Were A Kid That Are Hilarious Today                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/smartphones-should-come-with-a-bed-mode-for-uninterrupted-sleep-says-this-professor-247415.html" class="tint" title="Smartphones Should Come With A 'Bed Mode' For Uninterrupted Sleep Says This Professor">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card-fgfg_1447760616_502x234.jpg" border="0" alt="Smartphones Should Come With A 'Bed Mode' For Uninterrupted Sleep Says This Professor" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/smartphones-should-come-with-a-bed-mode-for-uninterrupted-sleep-says-this-professor-247415.html" title="Smartphones Should Come With A 'Bed Mode' For Uninterrupted Sleep Says This Professor">
                        Smartphones Should Come With A 'Bed Mode' For Uninterrupted Sleep Says This Professor                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/hollywood/get-ready-to-see-alexandra-daddario-dwayne-johnson-and-zac-efron-sizzle-in-the-new-baywatch-movie-247507.html" class="tint" title="Get Ready To See Alexandra Daddario, Dwayne Johnson And Zac Efron Sizzle In The New Baywatch Movie!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1447935640_1447935644_502x234.jpg" border="0" alt="Get Ready To See Alexandra Daddario, Dwayne Johnson And Zac Efron Sizzle In The New Baywatch Movie!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/hollywood/get-ready-to-see-alexandra-daddario-dwayne-johnson-and-zac-efron-sizzle-in-the-new-baywatch-movie-247507.html" title="Get Ready To See Alexandra Daddario, Dwayne Johnson And Zac Efron Sizzle In The New Baywatch Movie!">
                        Get Ready To See Alexandra Daddario, Dwayne Johnson And Zac Efron Sizzle In The New Baywatch Movie!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/travel/10-things-the-world-needs-to-learn-from-japan-and-its-people-246954.html" class="tint" title="10 Things The World Needs To Learn From Japan And Its People">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1446812634_502x234.jpg" border="0" alt="10 Things The World Needs To Learn From Japan And Its People" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/travel/10-things-the-world-needs-to-learn-from-japan-and-its-people-246954.html" title="10 Things The World Needs To Learn From Japan And Its People">
                        10 Things The World Needs To Learn From Japan And Its People                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-appalling-video-shows-a-woman-s-ordeal-of-finding-a-public-toilet-in-a-city-247494.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-appalling-video-shows-a-woman-s-ordeal-of-finding-a-public-toilet-in-a-city-247494.html" class="tint" title="This Appalling Video Shows A Woman's Ordeal Of Finding A Public Toilet In A City!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/worldtoiletday_card1_1447929103_502x234.jpg" border="0" alt="This Appalling Video Shows A Woman's Ordeal Of Finding A Public Toilet In A City!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-appalling-video-shows-a-woman-s-ordeal-of-finding-a-public-toilet-in-a-city-247494.html" title="This Appalling Video Shows A Woman's Ordeal Of Finding A Public Toilet In A City!">
                        This Appalling Video Shows A Woman's Ordeal Of Finding A Public Toilet In A City!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/an-open-letter-from-bajirao-s-kin-to-sanjay-leela-bhansali-questions-if-he-did-any-research-at-all-before-making-the-film-247496.html" class="tint" title="An Open Letter From Bajirao's Kin To Sanjay Leela Bhansali Questions If He Did Any Research At All!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/626030_1447932257_1447932270_502x234.jpg" border="0" alt="An Open Letter From Bajirao's Kin To Sanjay Leela Bhansali Questions If He Did Any Research At All!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/an-open-letter-from-bajirao-s-kin-to-sanjay-leela-bhansali-questions-if-he-did-any-research-at-all-before-making-the-film-247496.html" title="An Open Letter From Bajirao's Kin To Sanjay Leela Bhansali Questions If He Did Any Research At All!">
                        An Open Letter From Bajirao's Kin To Sanjay Leela Bhansali Questions If He Did Any Research At All!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/6-superheroes-are-celebrating-movember-and-they-look-absolutely-dashing-247498.html" class="tint" title="6 Superheroes Are Celebrating Movember And They Look Absolutely Dashing!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/movember_1447931118_1447931133_502x234.jpg" border="0" alt="6 Superheroes Are Celebrating Movember And They Look Absolutely Dashing!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/6-superheroes-are-celebrating-movember-and-they-look-absolutely-dashing-247498.html" title="6 Superheroes Are Celebrating Movember And They Look Absolutely Dashing!">
                        6 Superheroes Are Celebrating Movember And They Look Absolutely Dashing!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/bajrangi-bhaijaan-could-have-made-better-money-if-more-people-had-watched-it-in-theaters-247489.html" class="tint" title="'Bajrangi Bhaijaan' Could Have Made Better Money If More People Had Watched It In Theaters">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1447927688_502x234.jpg" border="0" alt="'Bajrangi Bhaijaan' Could Have Made Better Money If More People Had Watched It In Theaters" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/bajrangi-bhaijaan-could-have-made-better-money-if-more-people-had-watched-it-in-theaters-247489.html" title="'Bajrangi Bhaijaan' Could Have Made Better Money If More People Had Watched It In Theaters">
                        'Bajrangi Bhaijaan' Could Have Made Better Money If More People Had Watched It In Theaters                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/from-beating-aishwarya-in-a-beauty-pageant-to-being-a-single-mother-sushmita-sen-is-an-inspiration-for-all-247476.html" class="tint" title="From Beating Aishwarya In A Beauty Pageant To Being A Single Mother, Sushmita Sen Is An Inspiration For All!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/sen_1447925781_1447925787_502x234.jpg" border="0" alt="From Beating Aishwarya In A Beauty Pageant To Being A Single Mother, Sushmita Sen Is An Inspiration For All!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/from-beating-aishwarya-in-a-beauty-pageant-to-being-a-single-mother-sushmita-sen-is-an-inspiration-for-all-247476.html" title="From Beating Aishwarya In A Beauty Pageant To Being A Single Mother, Sushmita Sen Is An Inspiration For All!">
                        From Beating Aishwarya In A Beauty Pageant To Being A Single Mother, Sushmita Sen Is An Inspiration For All!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/7-reasons-why-antibiotic-drug-resistance-is-becoming-a-major-problem-around-the-world-247407.html" class="tint" title="7 Reasons Why Antibiotic Drug Resistance Is Becoming A Major Problem Around The World">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card-10_1447756699_502x234.jpg" border="0" alt="7 Reasons Why Antibiotic Drug Resistance Is Becoming A Major Problem Around The World" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/7-reasons-why-antibiotic-drug-resistance-is-becoming-a-major-problem-around-the-world-247407.html" title="7 Reasons Why Antibiotic Drug Resistance Is Becoming A Major Problem Around The World">
                        7 Reasons Why Antibiotic Drug Resistance Is Becoming A Major Problem Around The World                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/hollywood/oops-daniel-craig-s-spectre-fails-to-impress-ex-james-bond-pierce-brosnan-247488.html" class="tint" title="Oops! Daniel Craig's 'Spectre' Fails To Impress Ex James Bond Pierce Brosnan!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1447922513_1447922516_502x234.jpg" border="0" alt="Oops! Daniel Craig's 'Spectre' Fails To Impress Ex James Bond Pierce Brosnan!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/hollywood/oops-daniel-craig-s-spectre-fails-to-impress-ex-james-bond-pierce-brosnan-247488.html" title="Oops! Daniel Craig's 'Spectre' Fails To Impress Ex James Bond Pierce Brosnan!">
                        Oops! Daniel Craig's 'Spectre' Fails To Impress Ex James Bond Pierce Brosnan!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/keith-to-return-in-bb-house-this-week-giving-him-company-will-be-a-third-wild-card-entrant-247484.html" class="tint" title="Keith To Return In BB House This Week & Giving Him Company Will Be A Third Wild Card Entrant!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/keithstory_647_111515022709_1447919828_1447919843_502x234.jpg" border="0" alt="Keith To Return In BB House This Week & Giving Him Company Will Be A Third Wild Card Entrant!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/keith-to-return-in-bb-house-this-week-giving-him-company-will-be-a-third-wild-card-entrant-247484.html" title="Keith To Return In BB House This Week & Giving Him Company Will Be A Third Wild Card Entrant!">
                        Keith To Return In BB House This Week & Giving Him Company Will Be A Third Wild Card Entrant!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/if-you-re-a-woman-who-drives-this-mensday-blog-will-make-you-feel-pretty-damn-terrible-247485.html" class="tint" title="If You're A Woman Who Drives This #MensDay Blog Will Make You Feel Pretty Damn Terrible!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/women-who-drive502_1447923193_502x234.jpg" border="0" alt="If You're A Woman Who Drives This #MensDay Blog Will Make You Feel Pretty Damn Terrible!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/if-you-re-a-woman-who-drives-this-mensday-blog-will-make-you-feel-pretty-damn-terrible-247485.html" title="If You're A Woman Who Drives This #MensDay Blog Will Make You Feel Pretty Damn Terrible!">
                        If You're A Woman Who Drives This #MensDay Blog Will Make You Feel Pretty Damn Terrible!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/eating-too-many-raisins-can-damage-your-teeth-247403.html" class="tint" title="Eating Too Many Raisins Can Damage Your Teeth">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/kishmish_1447754862_1447754867_502x234.jpg" border="0" alt="Eating Too Many Raisins Can Damage Your Teeth" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/eating-too-many-raisins-can-damage-your-teeth-247403.html" title="Eating Too Many Raisins Can Damage Your Teeth">
                        Eating Too Many Raisins Can Damage Your Teeth                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/sania-mirza-asha-bhonsle-and-five-more-indians-make-it-to-bbc-list-of-100-inspiring-women-247482.html" class="tint" title="Sania Mirza, Asha Bhonsle And Five More Indians Make It To BBC List Of 100 Inspiring Women!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1447918922_1447918931_502x234.jpg" border="0" alt="Sania Mirza, Asha Bhonsle And Five More Indians Make It To BBC List Of 100 Inspiring Women!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/sania-mirza-asha-bhonsle-and-five-more-indians-make-it-to-bbc-list-of-100-inspiring-women-247482.html" title="Sania Mirza, Asha Bhonsle And Five More Indians Make It To BBC List Of 100 Inspiring Women!">
                        Sania Mirza, Asha Bhonsle And Five More Indians Make It To BBC List Of 100 Inspiring Women!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/these-pakistani-muslims-tell-you-why-they-won-t-apologize-for-the-paris-attacks-247475.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/these-pakistani-muslims-tell-you-why-they-won-t-apologize-for-the-paris-attacks-247475.html" class="tint" title="These Pakistani Muslims Tell You Why They Won't 'Apologize' For The Paris Attacks!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/pakguys_card_1447915070_502x234.jpg" border="0" alt="These Pakistani Muslims Tell You Why They Won't 'Apologize' For The Paris Attacks!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/these-pakistani-muslims-tell-you-why-they-won-t-apologize-for-the-paris-attacks-247475.html" title="These Pakistani Muslims Tell You Why They Won't 'Apologize' For The Paris Attacks!">
                        These Pakistani Muslims Tell You Why They Won't 'Apologize' For The Paris Attacks!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/22-bollywood-movies-which-showed-us-different-shades-of-love-through-love-triangles-247448.html" class="tint" title="22 Bollywood Movies Which Showed Us Different Shades Of Love Through Love Triangles!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1447839753_1447839758_502x234.jpg" border="0" alt="22 Bollywood Movies Which Showed Us Different Shades Of Love Through Love Triangles!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/22-bollywood-movies-which-showed-us-different-shades-of-love-through-love-triangles-247448.html" title="22 Bollywood Movies Which Showed Us Different Shades Of Love Through Love Triangles!">
                        22 Bollywood Movies Which Showed Us Different Shades Of Love Through Love Triangles!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/18-really-tricky-questions-that-have-been-asked-at-apple-interviews-247409.html" class="tint" title="18 Really Tricky Questions That Have Been Asked At Apple Interviews">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1447757552_502x234.jpg" border="0" alt="18 Really Tricky Questions That Have Been Asked At Apple Interviews" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/18-really-tricky-questions-that-have-been-asked-at-apple-interviews-247409.html" title="18 Really Tricky Questions That Have Been Asked At Apple Interviews">
                        18 Really Tricky Questions That Have Been Asked At Apple Interviews                    </a>
                </figcaption>
            </div>
        
        <script id="HpMiddleBlock_tpl" type="text/x-handlebars-template">
            {{#if MoreData}}
            {{#each MoreData}} 
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>


                    <a  class='video-btn sprite' href="{{guid}}" style="display:{{card_id}}"> </a>

    				{{#if label_name.length}}
                    <a href="{{guid}}" class="{{label_name}} sticker">&nbsp;</a>
    						 {{/if}} 
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="{{guid}}" title="{{carousal_headline}}" class="tint">
                        <img class="greyBg lazy" data-original="{{thumbnail}}" border="0" alt="{{carousal_headline}}" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="{{guid}}" title="{{carousal_headline}}">
                        {{carousal_headline}}
                    </a>
                </figcaption>
            </div>  
            {{/each}} 
            {{/if}}

            </script>

        </div><!--life-panel end-->

        <div class="trending-panel cf "><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_15"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/bollywood/priyanka-chopra-might-play-astronaut-kalpana-chawla-in-a-new-biopic-247557.html" class="tint" title="Priyanka Chopra Might Play Astronaut Kalpana Chawla In A New Biopic">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/dcr_1448094742_1448094749_218x102.jpg" border="0" alt="Priyanka Chopra Might Play Astronaut Kalpana Chawla In A New Biopic"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/priyanka-chopra-might-play-astronaut-kalpana-chawla-in-a-new-biopic-247557.html" title="Priyanka Chopra Might Play Astronaut Kalpana Chawla In A New Biopic">
                            Priyanka Chopra Might Play Astronaut Kalpana Chawla In A New Biopic                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                <div class='container3'>                <div class="trending-panel-list cf" id="column3_16"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/celebs/6-things-you-need-to-know-about-priya-malik-the-next-wild-card-entry-on-bigg-boss-247564.html" class="tint" title="6 Things You Need To Know About Priya Malik, The Next Wild Card Entry On Bigg Boss">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1448101746_1448101752_218x102.jpg" border="0" alt="6 Things You Need To Know About Priya Malik, The Next Wild Card Entry On Bigg Boss"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/6-things-you-need-to-know-about-priya-malik-the-next-wild-card-entry-on-bigg-boss-247564.html" title="6 Things You Need To Know About Priya Malik, The Next Wild Card Entry On Bigg Boss">
                            6 Things You Need To Know About Priya Malik, The Next Wild Card Entry On Bigg Boss                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_17"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/culture/who-we-are/meet-vineet-vincent-india-s-best-beatboxer-he-can-give-any-drummer-a-run-for-their-money-246949.html" class="tint" title="Meet Vineet Vincent, India's Best Beatboxer. He Can Give Any Drummer A Run For Their Money">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/vineeth_1446613711_1446613722_218x102.jpg" border="0" alt="Meet Vineet Vincent, India's Best Beatboxer. He Can Give Any Drummer A Run For Their Money"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/meet-vineet-vincent-india-s-best-beatboxer-he-can-give-any-drummer-a-run-for-their-money-246949.html" title="Meet Vineet Vincent, India's Best Beatboxer. He Can Give Any Drummer A Run For Their Money">
                            Meet Vineet Vincent, India's Best Beatboxer. He Can Give Any Drummer A Run For Their Money                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_18"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/bollywood/ranveer-singh-did-not-charge-a-rupee-for-bajirao-mastani-instead-he-has-a-share-in-the-film-s-profits-247556.html" class="tint" title="Ranveer Singh Did Not Charge A Rupee For 'Bajirao Mastani'. Instead, He Has A Share In The Film's Profits">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/br_1448092100_1448092105_218x102.jpg" border="0" alt="Ranveer Singh Did Not Charge A Rupee For 'Bajirao Mastani'. Instead, He Has A Share In The Film's Profits"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/ranveer-singh-did-not-charge-a-rupee-for-bajirao-mastani-instead-he-has-a-share-in-the-film-s-profits-247556.html" title="Ranveer Singh Did Not Charge A Rupee For 'Bajirao Mastani'. Instead, He Has A Share In The Film's Profits">
                            Ranveer Singh Did Not Charge A Rupee For 'Bajirao Mastani'. Instead, He Has A Share In The Film's Profits                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_19"><!--trending-panel-list start-->
                                        <figure>
                                
                        <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/the-trailer-of-bajirao-mastani-is-here-and-it-will-teleport-you-to-an-era-of-love-war-and-royalty-247547.html'>video</a>					
                        <a href="http://www.indiatimes.com/entertainment/bollywood/the-trailer-of-bajirao-mastani-is-here-and-it-will-teleport-you-to-an-era-of-love-war-and-royalty-247547.html" class="tint" title="The Trailer Of 'Bajirao Mastani' Is Here And It Will Teleport You To An Era Of Love, War And Royalty">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/crd_1448044507_1448044512_218x102.jpg" border="0" alt="The Trailer Of 'Bajirao Mastani' Is Here And It Will Teleport You To An Era Of Love, War And Royalty"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/the-trailer-of-bajirao-mastani-is-here-and-it-will-teleport-you-to-an-era-of-love-war-and-royalty-247547.html" title="The Trailer Of 'Bajirao Mastani' Is Here And It Will Teleport You To An Era Of Love, War And Royalty">
                            The Trailer Of 'Bajirao Mastani' Is Here And It Will Teleport You To An Era Of Love, War And Royalty                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                
        </div>

    </div><!--trending-panel end-->

    </section>
    <section class="container cf" id="loader" style="display: none;border: 0px solid red;">
    <div class="news-panel cf ">&nbsp;</div>
    <div class="life-panel cf" style="text-align: center;"><img src="http://media.indiatimes.in/resources/images/loading.gif" border="0" /></div>
    <div class="trending-panel cf ">&nbsp;</div>
</section>
<section class="big-ads remove-fixed-home" id="adfooter">
<div id="badRos_slot"></div> 
</section>   

<!------------------------ end Block 4------------------------------------------------>
<!--container end-->

<script type="text/javascript">
   
    $('#bigAd1_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    showBigAD2('bigAd2_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible     
       
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#bigAd2_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    showBigAD3('bigAd3_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible

    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible  
      
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#bigAd3_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    BADros('badRos_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});

$(document).ready(function() {
	/* spotlight onload tracking homepage */
	//console.log("homepage");
			ga('send', 'event', 'OnLoad Partner Stories', '247474', 'homepage', {'nonInteraction': 1});
	           
});

$(document).ready(function(){
        var trigger_depth={b2:99, b3:499, b4:1199};
        var is_trigger_active={b2:true, b3:true, b4:true};
        var call_on_scroll=true;
        
            $(window).on("scroll",function(){
            if((!(is_trigger_active.b2)&&!(is_trigger_active.b3)&&!(is_trigger_active.b4))==false)
            {
                callPreload(is_trigger_active,trigger_depth);
                
            }
    });
});

function callPreload(active,depth)
{
    var scroll_top = $(window).scrollTop();
    if((active.b2)&&(scroll_top > depth.b2))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Nov/sumantkumar_1448189582_1448189638_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/theory_1448185874_1448185890_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/mobile-card_1448184898_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/flood_1448184480_1448184492_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/mallya-card_1448179966_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/download_1448194822_1448194828_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/dubpam_1447837812_1447837823_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/deepika-salman_1448191918_1448191927_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1447419551_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/ggk_1448176880_1448176907_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/hair-loss-card_1448002568_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/dubpam_1447837812_1447837823_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1448001198_218x102.jpg");
        active.b2=false;
    }
    if((active.b3)&&(scroll_top > depth.b3))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Nov/bluedragon_1448178596_1448178619_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/ape_1447322825_1447322834_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/dowry-money-card_1448176650_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/pb-acharya-addresses-media-card_1448181319_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/onerupee_1448173115_1448173132_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/bm1_1448176311_1448176319_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/hh_1448185990_1448186003_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card-3_1448011823_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/pc_1448173395_1448173404_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/kareena_1448170924_1448170935_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/censor_1448183646_1448183652_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/changing-life_1447933496_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/k3g1_1447833379_218x102.jpg");
        active.b3=false;
    }
    
    if((active.b4)&&(scroll_top > depth.b4))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Nov/vangaalronaldo_1448176416_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/36129565-card_1448172592_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/o-holiday-airport-facebook_1448171108_1448171117_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/maharaj-card_1448169401_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/hollande_1448169249_1448169261_236x111.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/medicalstudetns_card_1448173900_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/help_card_1448178961_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/censor_1448183646_1448183652_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/ggk_1448176880_1448176907_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1448009294_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/pc_1448173395_1448173404_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1447419551_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/kareena_1448170924_1448170935_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/hair-loss-card_1448002568_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/cp_1447842144_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1448101746_1448101752_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card-12_1448108431_1448108440_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/changing-life_1447933496_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/vineeth_1446613711_1446613722_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/dcr_1448094742_1448094749_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/br_1448092100_1448092105_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/k3g1_1447833379_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/crd_1448044507_1448044512_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card-mint_1447833703_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1448023126_1448023133_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/puberty-story-card_1447747582_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/screenshot_5_1448012535_1448012543_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1448010598_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/_63832137_jeans think_1447756237_1447756241_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1448008651_1448008661_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/cp_1447918006_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/cover_1447924339_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1448004346_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1448000688_1448000695_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1447846941_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/gandhi5_1447998184_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1447919370_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1447839425_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card-fgfg_1447760616_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1447935640_1447935644_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1446812634_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/worldtoiletday_card1_1447929103_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/626030_1447932257_1447932270_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/movember_1447931118_1447931133_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1447927688_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/sen_1447925781_1447925787_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card-10_1447756699_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1447922513_1447922516_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/keithstory_647_111515022709_1447919828_1447919843_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/women-who-drive502_1447923193_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/kishmish_1447754862_1447754867_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1447918922_1447918931_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/pakguys_card_1447915070_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1447839753_1447839758_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1447757552_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/dcr_1448094742_1448094749_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1448101746_1448101752_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/vineeth_1446613711_1446613722_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/br_1448092100_1448092105_218x102.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/crd_1448044507_1448044512_218x102.jpg");
        active.b4=false;
    }
}
</script>    <div class="clr"></div>
      
	
                 <script> 
				                    showFooterCode();
					            </script>
	           <style>
.bottom-strip{background:#ebebeb;display:block;padding:15px 0;overflow:hidden;}.bottom-strip-box{margin:0 auto;display:table;max-width:410px;width:100%;}.square-logo{width:66px;height:64px;float:left;margin-right:10px;}.bottom-strip-content{float:left;margin-top:14px;}.bottom-strip-content span{display:block;color:#737272;font:12px/1 "RobotoRegular", Arial, Helvetica, sans-serif;margin-bottom:2px;}.bottom-strip-content a{color:#1b85dd;font:24px/1 "RobotoRegular", Arial, Helvetica, sans-serif;text-decoration:none;}
</style>
<div class="last-container">
    <!--<br>--> 
    <!--social-panel start-->
    <div class="social-panel red-bg">
        <div class="soc-inner"><!--soc-inner start-->
            <p class="soc-title">Be a Part of The New & Next</p>
            <div class="soc-count"><!--soc-count start-->
                <a href="https://www.facebook.com/indiatimes" target="_blank" title="facebook" class="sprite soc-fb"></a>
                <p>
                    3,932,098<span class="black"> FRIENDS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://twitter.com/indiatimes" target="_blank" title="twitter" class="sprite soc-twt"></a>
                <p>10343  <span class="black">FOLLOWERS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://plus.google.com/+indiatimes" target="_blank" title="gplus" class="sprite soc-gplus"></a>
                <p>104,163 <span class="black">MEMBERS</span></p>
            </div>
            <div class="soc-count"><!--soc-count start-->
                <a href="javascript:void(0);" title="E-mail" class="sprite soc-mail" id="subscribers_id"></a>
                <p>50,700 <span class="black">SUBSCRIBERS</span></p>
            </div>
            <!--soc-count end-->
        </div>
        <!--soc-inner end-->
    </div>
    <!--social-panel end-->
    <div class="clr"></div>

    <footer>
        <!-- social container start-->
        <div class="socical">
            <div class="containers">
                <div class="fl left_cont"> Get your weekly dose of virality sent to your inbox! </div>
                <div class="fr right_cont">

                    <input type="text" value="Please Enter Email Address" class="Email" name="UserEmail" id="UserEmail" defaultValue="Please Enter Email Address"/>
                    <input type="submit" id="btn-submit" value="SIGN UP" name="btn-submit" class="Subscribe">
                    <span id="subsEmal" style='color: red; padding-left: 66px; text-align: left;'></span>
                </div>

            </div>
        </div><!-- social containers end-->
        <div class="footer"><!-- footer grey start-->
            <div class="sub_container">
                <!-- container start-->
                <div class="cont_new">
                    <h2>Our Channels</h2>
                                                <a href='http://www.indiatimes.com/news/' class="blue size">News</a> 
                                                    <a href='http://www.indiatimes.com/lifestyle/' class="purple size">Lifestyle</a> 
                                                    <a href='http://www.indiatimes.com/entertainment/' class="red size">Entertainment</a> 
                                                    <a href='http://www.indiatimes.com/health/' class="green size">Health</a> 
                                                    <a href='http://www.indiatimes.com/videocafe/' class="orange size">Videos</a> 
                        					<a href='http://www.indiatimes.com/trending' class="size" style="color:white">Trending</a> 
                </div><!-- container closed-->

                <div class="cont"> <!-- container start-->
                    <h2>Sections</h2>
                                            <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/news/india/'>india</a>
                                             
                                    <a href='http://www.indiatimes.com/news/world/'>world</a>
                                             
                                    <a href='http://www.indiatimes.com/news/sports/'>sports</a>
                                             
                                    <a href='http://www.indiatimes.com/news/weird/'>weird</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/lifestyle/self/'>self</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/style/'>style</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/who-we-are/'>who we are</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/travel/'>travel</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/food/'>food</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/entertainment/bollywood/'>bollywood</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/celebs/'>celebscoop</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/hollywood/'>hollywood</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                             
                                    <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                             
                                    <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                             
                                    <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                             
                                    <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                
                             </div>                        <div class="sub_link">
                             </div>                    <div class="sub_link"><a href="http://www.indiatimes.com/photogallery">Photogallery</a></div>
                </div><!-- container closed-->
                <div class="cont_new"> <!-- container start-->
                    <h2>Indiatimes Lifestyle Network</h2>

                    <div class="sub_link">                             <a href="http://www.mensxp.com"   target="_blank" >MENS XP</a>
                                                    <a href="http://www.idiva.com"   target="_blank" >iDiva</a>
                                                    <a href="http://luxpresso.com"   target="_blank" >Luxury</a>
                                                    <a href="http://in.askmen.com/"   target="_blank" >Ask Men</a>
                                                    <a href="http://www.pursuitist.in/"   target="_blank" >Pursuitist</a>
                        </div>
                </div><!-- container closed-->
                <div class="cont"> <!-- container start-->
                    <h2>Other Sites</h2>
                    <div class="sub_link"> 
                        
                            <a href="http://www.cricbuzz.com"  target="_blank">CricBuzz</a> 
                        
                            <a href="http://www.happytrips.com/"  target="_blank">Happy Trips</a> 
                        
                            <a href="http://www.businessinsider.in/"  target="_blank">Business Insider</a> 
                        
                            <a href="http://www.zoomtv.com"  target="_blank">ZoomTv</a> 
                        
                            <a href="http://www.gizmodo.in/"  target="_blank">Gizmodo</a> 
                        
                            <a href="http://www.lifehacker.co.in/"  target="_blank">Lifehacker</a> 
                        
                            <a href="http://in.ign.com/"  target="_blank">IGN</a> 
                        
                            <a href="http://boxtv.com"  target="_blank">BoxTV</a> 
                        
                            <a href="http://www.gaana.com"  target="_blank">Gaana</a> 
                        
                            <a href="http://shopping.indiatimes.com"  target="_blank">Online Shopping</a> 
                        
                            <a href="http://www.in.techradar.com/"  target="_blank">Techradar</a> 
                        
                            <a href="http://timesdeal.com"  target="_blank">Daily Deals</a> 
                        
                            <a href="http://www.gitanjalishop.com/"  target="_blank">Gitanjali Shop</a> 
                        
                            <a href="http://www.satvikshop.com/"  target="_blank">Satvik Shop</a> 
                        
                            <a href="http://whatshot.in"  target="_blank">WHAT'S HOT</a> 
                        
                            <a href="http://zigwheels.com"  target="_blank">ZigWheels</a> 
                        
                            <a href="http://filmipop.com"  target="_blank">FILMIPOP</a> 
                        
                            <a href="http://www.filmfare.com/"  target="_blank">Filmfare</a> 
                        
                            <a href="http://www.femina.in"  target="_blank">Femina</a> 
                        
                            <a href="http://mobile.indiatimes.com"  target="_blank">Mobile</a> 
                        
                            <a href="http://www.greetzap.com"  target="_blank">Greetings</a> 
                        
                            <a href="http://www.follo.co.in"  target="_blank">Follo</a> 
                        
                            <a href="http://timesjobs.com"  target="_blank">Jobs</a> 
                        
                            <a href="http://magicbricks.com"  target="_blank">Property</a> 
                        
                            <a href="http://www.astrospeak.com"  target="_blank">Astrology</a> 
                        
                            <a href="http://speakingtree.in"  target="_blank">Speaking Tree</a> 
                        
                            <a href="http://simplymarry.com"  target="_blank">Matrimonial</a> 
                        
                            <a href="http://itimes.com"  target="_blank">iTimes</a> 
                        
                            <a href="http://www.ads2book.com/"  target="_blank">Ads2book</a> 
                        
                            <a href="http://www.tcnext.com/"  target="_blank">TcNext</a> 
                        
                            <a href="http://www.romedynow.com"  target="_blank">Romedy Now</a> 
                                            </div>
                </div><!-- container closed-->
                <div class="cont border_none"><!-- container start-->
                    <h2>About us</h2>
                    <div class="sub_link">
                                                    <a href='http://www.indiatimes.com/privacypolicy' >Privacy Policy</a> 
                                                    <a href='http://www.indiatimes.com/sitemap.html' >sitemap</a> 
                                                    <a href='http://www.indiatimes.com/termsandcondition' >Terms & CONDITIONS</a> 
                                                    <a href='http://www.indiatimes.com/seoarchive' >ARCHIVES</a> 
                                                    <a href='http://www.indiatimes.com/contactus' >Contact us</a> 
                                                    <a href='http://www.indiatimes.com/aboutus' >ABOUT INDIATIMES</a> 
                                            </div>
                </div><!-- container closed-->
            </div><!-- footer grey closed-->
        </div><!-- footer black closed-->

        <div class="black_footer"><!-- footer black start-->
            <div class="containers">
                <p><a href="javascript:void(0)" class="copyrt">Copyright Â© 2015 Times Internet Limited. Powered by Indiatimes Lifestyle Network. All rights reserved</a></p>
                <span><a href="http://www.indiatimes.com/seoarchive/">Archive</a> | <a href="http://www.indiatimes.com/sitemap.html">Sitemap</a> | <a href="http://www.indiatimes.com/aboutus/">About Us</a> | <a href="http://www.indiatimes.com/contactus/">Contact Us</a> | <a href="http://www.indiatimes.com/privacypolicy/">Privacy Policy</a> | <a target="_blank" href="http://m.indiatimes.com">Indiatimes Mobile</a> | <a href="http://www.indiatimes.com/termsandcondition/">Terms of Use and Grievance Redressal Policy </a> </span> 
            </div>
        </div><!-- footer black end-->
    </footer>
</div>
<script>
    $( document ).ready(function() {
        $("#subscribers_id").click(function(){
            $("#UserEmail").focus();
        });
    });
    
</script>

    <script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.lazyload.min.js?v=100.6" charset="utf-8"></script>
<script defer type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.easing.1.3.min.js?v=100.6"></script>

<input type="hidden" name="currentId" id="currentId" value="">
    <div class="animate_cont dawn floating delay5">
        <span class="animate_arrow sprite_image"></span>
    </div>



<script type="text/javascript" src="http://media.indiatimes.in/resources/js/main.js?v=100.6"></script>    
<script type="text/javascript" src="http://media.indiatimes.in/resources/js/common.js?v=100.6"></script>
<script  defer type="text/javascript" src="http://media.indiatimes.in/resources/js/jquery-scrolltofixed.js?v=100.6"></script>

<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://cdn.mouseflow.com/projects/1e238441-c30e-4d1b-8edd-c0f14d8fa6b7.js' type='text/javascript'%3E%3C/script%3E"));</script>
<!-- mouse flow code ends-->
<script  src="http://tags.crwdcntrl.net/c/2818/cc.js?ns=_cc2818" id="LOTCC_2818"></script> 

<script> 
    _cc2818.bcp(); 
</script>

	 <p id="back-top" style="display:none;"><a href="#top"><span></span></a> </p>
</div><!--wrap end-->


</body>
</html>