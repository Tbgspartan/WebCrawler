<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="auto">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-261c451.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-261c451.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-261c451.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-261c451.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kickass.to/sc-261c451.js');

        sc('setHost', 'a.kickass.to');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: '261c451',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-261c451.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<span  data-sc-slot="_60318cd4e8d28f6fb76fe34e9bd9c498"></span>
<span  data-sc-slot="_39ecb76dd457e5ac33776fdf11500d56"></span>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka ka-settings"></i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka ka-search"></i></button></div>
			</form>
		</div>
        <span  data-sc-slot="_277923e5f9d753c5b0630c28e641790c"></span>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080p/" class="tag6">1080p</a>
	<a href="/search/2014/" class="tag2">2014</a>
	<a href="/search/2015/" class="tag10">2015</a>
	<a href="/search/3d/" class="tag3">3d</a>
	<a href="/search/adele/" class="tag4">adele</a>
	<a href="/search/adele%2025/" class="tag2">adele 25</a>
	<a href="/search/android/" class="tag2">android</a>
	<a href="/search/ant%20man/" class="tag2">ant man</a>
	<a href="/search/christmas/" class="tag3">christmas</a>
	<a href="/search/discography/" class="tag3">discography</a>
	<a href="/search/doctor%20who/" class="tag2">doctor who</a>
	<a href="/search/dual%20audio%20hindi/" class="tag2">dual audio hindi</a>
	<a href="/search/fallout%204/" class="tag2">fallout 4</a>
	<a href="/search/french/" class="tag3">french</a>
	<a href="/search/grimm/" class="tag2">grimm</a>
	<a href="/search/hindi/" class="tag8">hindi</a>
	<a href="/search/hindi%202015/" class="tag6">hindi 2015</a>
	<a href="/search/hotel%20transylvania%202/" class="tag2">hotel transylvania 2</a>
	<a href="/search/hunger%20games/" class="tag2">hunger games</a>
	<a href="/search/ita/" class="tag2">ita</a>
	<a href="/search/jessica%20jones/" class="tag10">jessica jones</a>
	<a href="/search/jessica%20jones%20s01e07/" class="tag2">jessica jones s01e07</a>
	<a href="/search/jessica%20jones%20s01e08/" class="tag2">jessica jones s01e08</a>
	<a href="/search/limitless/" class="tag2">limitless</a>
	<a href="/search/malayalam/" class="tag3">malayalam</a>
	<a href="/search/marvels%20jessica%20jones/" class="tag2">marvels jessica jones</a>
	<a href="/search/nezu/" class="tag9">nezu</a>
	<a href="/search/nl/" class="tag2">nl</a>
	<a href="/search/prem%20ratan%20dhan%20payo/" class="tag2">prem ratan dhan payo</a>
	<a href="/search/pyaar%20ka%20punchnama%202/" class="tag3">pyaar ka punchnama 2</a>
	<a href="/search/ripsalot/" class="tag4">ripsalot</a>
	<a href="/search/south%20park/" class="tag1">south park</a>
	<a href="/search/spectre/" class="tag4">spectre</a>
	<a href="/search/star%20wars/" class="tag3">star wars</a>
	<a href="/search/supergirl/" class="tag3">supergirl</a>
	<a href="/search/tamil/" class="tag4">tamil</a>
	<a href="/search/telugu/" class="tag4">telugu</a>
	<a href="/search/telugu%202015/" class="tag2">telugu 2015</a>
	<a href="/search/the%20blacklist/" class="tag2">the blacklist</a>
	<a href="/search/the%20hunger%20games/" class="tag2">the hunger games</a>
	<a href="/search/the%20man%20in%20the%20high%20castle/" class="tag4">the man in the high castle</a>
	<a href="/search/the%20martian/" class="tag2">the martian</a>
	<a href="/search/the%20walking%20dead/" class="tag6">the walking dead</a>
	<a href="/search/walking%20dead/" class="tag2">walking dead</a>
	<a href="/search/yify/" class="tag10">yify</a>
	<a href="/search/yify%201080p/" class="tag3">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag4">yify 720p</a>
	<a href="/search/z%20nation/" class="tag2">z nation</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11617288,0" class="icommentjs kaButton smallButton rightButton" href="/maze-runner-the-scorch-trials-2015-1080p-bluray-h264-aac-rarbg-t11617288.html#comment">76 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/maze-runner-the-scorch-trials-2015-1080p-bluray-h264-aac-rarbg-t11617288.html" class="cellMainLink">Maze Runner The Scorch Trials 2015 1080p BluRay H264 AAC-RARBG</a></div>
			</td>
			<td class="nobr center" data-sort="2705663063">2.52 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T11:11:41+00:00">19 Nov 2015, 11:11:41</span></td>
			<td class="green center">11591</td>
			<td class="red lasttd center">16012</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11622514,0" class="icommentjs kaButton smallButton rightButton" href="/criminal-activities-2015-hdrip-xvid-etrg-t11622514.html#comment">70 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/criminal-activities-2015-hdrip-xvid-etrg-t11622514.html" class="cellMainLink">Criminal Activities 2015 HDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="743710811">709.26 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T07:50:22+00:00">20 Nov 2015, 07:50:22</span></td>
			<td class="green center">10171</td>
			<td class="red lasttd center">6487</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11623742,0" class="icommentjs kaButton smallButton rightButton" href="/halo-the-fall-of-reach-2015-dvdrip-xvid-evo-t11623742.html#comment">67 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/halo-the-fall-of-reach-2015-dvdrip-xvid-evo-t11623742.html" class="cellMainLink">Halo The Fall of Reach 2015 DVDRip XviD-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="764068476">728.67 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T13:34:53+00:00">20 Nov 2015, 13:34:53</span></td>
			<td class="green center">8423</td>
			<td class="red lasttd center">5685</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11626416,0" class="icommentjs kaButton smallButton rightButton" href="/ant-man-2015-1080p-bluray-x264-dts-jyk-t11626416.html#comment">54 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ant-man-2015-1080p-bluray-x264-dts-jyk-t11626416.html" class="cellMainLink">Ant-Man 2015 1080p BluRay x264 DTS-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="3168707577">2.95 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T00:04:25+00:00">21 Nov 2015, 00:04:25</span></td>
			<td class="green center">6654</td>
			<td class="red lasttd center">6627</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11618780,0" class="icommentjs kaButton smallButton rightButton" href="/the-little-prince-2015-hc-hdrip-xvid-etrg-t11618780.html#comment">96 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-little-prince-2015-hc-hdrip-xvid-etrg-t11618780.html" class="cellMainLink">The Little Prince 2015 HC HDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="743304933">708.87 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T17:38:32+00:00">19 Nov 2015, 17:38:32</span></td>
			<td class="green center">7284</td>
			<td class="red lasttd center">4485</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11627471,0" class="icommentjs kaButton smallButton rightButton" href="/007-spectre-2015-english-720p-hd-ts-x264-1gb-zippymoviez-exclusive-t11627471.html#comment">44 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/007-spectre-2015-english-720p-hd-ts-x264-1gb-zippymoviez-exclusive-t11627471.html" class="cellMainLink">007 Spectre (2015) English 720p HD TS x264 1GB ZippyMovieZ ExCluSivE</a></div>
			</td>
			<td class="nobr center" data-sort="1071136492">1021.52 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T04:34:37+00:00">21 Nov 2015, 04:34:37</span></td>
			<td class="green center">6255</td>
			<td class="red lasttd center">6174</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11618096,0" class="icommentjs kaButton smallButton rightButton" href="/the-hunger-games-trilogy-2012-2014-imax-edition-1080p-bluray-x264-anoxmous-t11618096.html#comment">67 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-hunger-games-trilogy-2012-2014-imax-edition-1080p-bluray-x264-anoxmous-t11618096.html" class="cellMainLink">The Hunger Games Trilogy 2012-2014 IMAX Edition 1080p BluRay x264 anoXmous</a></div>
			</td>
			<td class="nobr center" data-sort="8800595987">8.2 <span>GB</span></td>
			<td class="center">10</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T14:43:58+00:00">19 Nov 2015, 14:43:58</span></td>
			<td class="green center">4373</td>
			<td class="red lasttd center">9255</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11625667,0" class="icommentjs kaButton smallButton rightButton" href="/close-range-2015-dvdrip-xvid-evo-t11625667.html#comment">27 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/close-range-2015-dvdrip-xvid-evo-t11625667.html" class="cellMainLink">Close Range 2015 DVDRip XviD-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1170418183">1.09 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T21:09:30+00:00">20 Nov 2015, 21:09:30</span></td>
			<td class="green center">5538</td>
			<td class="red lasttd center">5737</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11618076,0" class="icommentjs kaButton smallButton rightButton" href="/hitman-agent-47-2015-1080p-bluray-x264-dts-jyk-t11618076.html#comment">85 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/hitman-agent-47-2015-1080p-bluray-x264-dts-jyk-t11618076.html" class="cellMainLink">Hitman Agent 47 2015 1080p BluRay x264 DTS-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="2601285061">2.42 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T14:37:16+00:00">19 Nov 2015, 14:37:16</span></td>
			<td class="green center">6217</td>
			<td class="red lasttd center">4299</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11616778,0" class="icommentjs kaButton smallButton rightButton" href="/everest-2015-hdrip-xvid-etrg-t11616778.html#comment">84 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/everest-2015-hdrip-xvid-etrg-t11616778.html" class="cellMainLink">Everest 2015 HDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="741556249">707.2 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T08:38:29+00:00">19 Nov 2015, 08:38:29</span></td>
			<td class="green center">4511</td>
			<td class="red lasttd center">2724</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/el-corredor-del-laberinto-las-pruebas-spanish-espaÃ±ol-hdrip-xvid-elitetorrent-t11621490.html" class="cellMainLink">El corredor del laberinto: Las pruebas SPANiSH ESPAÃ±OL HDRip XviD-ELiTETORRENT</a></div>
			</td>
			<td class="nobr center" data-sort="2140909568">1.99 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T04:18:55+00:00">20 Nov 2015, 04:18:55</span></td>
			<td class="green center">4109</td>
			<td class="red lasttd center">1909</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11627512,0" class="icommentjs kaButton smallButton rightButton" href="/the-assassin-2015-chinese-1080p-bluray-x264-dts-jyk-t11627512.html#comment">17 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-assassin-2015-chinese-1080p-bluray-x264-dts-jyk-t11627512.html" class="cellMainLink">The Assassin 2015 CHINESE 1080p BluRay x264 DTS-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="2853132395">2.66 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T04:45:56+00:00">21 Nov 2015, 04:45:56</span></td>
			<td class="green center">2273</td>
			<td class="red lasttd center">1690</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11618944,0" class="icommentjs kaButton smallButton rightButton" href="/ant-man-2015-truefrench-720p-bluray-x264-liberty-mkv-t11618944.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ant-man-2015-truefrench-720p-bluray-x264-liberty-mkv-t11618944.html" class="cellMainLink">Ant-Man 2015 TRUEFRENCH 720p BluRay x264-LiBERTY mkv</a></div>
			</td>
			<td class="nobr center" data-sort="4880166750">4.55 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T18:14:24+00:00">19 Nov 2015, 18:14:24</span></td>
			<td class="green center">2226</td>
			<td class="red lasttd center">1448</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11624074,0" class="icommentjs kaButton smallButton rightButton" href="/love-2015-webrip-720p-dual-audio-rus-eng-t11624074.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/love-2015-webrip-720p-dual-audio-rus-eng-t11624074.html" class="cellMainLink">Love (2015) WEBRip 720p | [DUAL AUDIO] | [RUS-ENG]</a></div>
			</td>
			<td class="nobr center" data-sort="3947545198">3.68 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T14:53:01+00:00">20 Nov 2015, 14:53:01</span></td>
			<td class="green center">1593</td>
			<td class="red lasttd center">908</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11618758,0" class="icommentjs kaButton smallButton rightButton" href="/saul-journey-to-damascus-2015-hdrip-xvid-ac3-evo-t11618758.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/saul-journey-to-damascus-2015-hdrip-xvid-ac3-evo-t11618758.html" class="cellMainLink">Saul Journey to Damascus 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1484196320">1.38 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T17:33:36+00:00">19 Nov 2015, 17:33:36</span></td>
			<td class="green center">1111</td>
			<td class="red lasttd center">1547</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11626370,0" class="icommentjs kaButton smallButton rightButton" href="/marvels-jessica-jones-s01e01-webrip-xvid-fum-ettv-t11626370.html#comment">97 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/marvels-jessica-jones-s01e01-webrip-xvid-fum-ettv-t11626370.html" class="cellMainLink">Marvels Jessica Jones S01E01 WEBRip XviD-FUM[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="448530580">427.75 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T23:56:18+00:00">20 Nov 2015, 23:56:18</span></td>
			<td class="green center">13726</td>
			<td class="red lasttd center">4145</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11615295,0" class="icommentjs kaButton smallButton rightButton" href="/arrow-s04e07-hdtv-x264-lol-ettv-t11615295.html#comment">240 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/arrow-s04e07-hdtv-x264-lol-ettv-t11615295.html" class="cellMainLink">Arrow S04E07 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="288086757">274.74 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T02:01:15+00:00">19 Nov 2015, 02:01:15</span></td>
			<td class="green center">14953</td>
			<td class="red lasttd center">1677</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11620919,0" class="icommentjs kaButton smallButton rightButton" href="/the-big-bang-theory-s09e09-hdtv-x264-lol-rartv-t11620919.html#comment">126 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-big-bang-theory-s09e09-hdtv-x264-lol-rartv-t11620919.html" class="cellMainLink">The Big Bang Theory S09E09 HDTV x264-LOL[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="151778343">144.75 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T01:29:03+00:00">20 Nov 2015, 01:29:03</span></td>
			<td class="green center">14193</td>
			<td class="red lasttd center">1171</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11621449,0" class="icommentjs kaButton smallButton rightButton" href="/how-to-get-away-with-murder-s02e09-hdtv-x264-lol-ettv-t11621449.html#comment">95 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/how-to-get-away-with-murder-s02e09-hdtv-x264-lol-ettv-t11621449.html" class="cellMainLink">How to Get Away with Murder S02E09 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="272038106">259.44 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T04:09:24+00:00">20 Nov 2015, 04:09:24</span></td>
			<td class="green center">10623</td>
			<td class="red lasttd center">1357</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11621258,0" class="icommentjs kaButton smallButton rightButton" href="/the-originals-s03e07-hdtv-x264-lol-ettv-t11621258.html#comment">80 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-originals-s03e07-hdtv-x264-lol-ettv-t11621258.html" class="cellMainLink">The Originals S03E07 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="210844835">201.08 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T03:02:17+00:00">20 Nov 2015, 03:02:17</span></td>
			<td class="green center">8204</td>
			<td class="red lasttd center">1080</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11621195,0" class="icommentjs kaButton smallButton rightButton" href="/heroes-reborn-s01e10-hdtv-x264-fum-ettv-t11621195.html#comment">86 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/heroes-reborn-s01e10-hdtv-x264-fum-ettv-t11621195.html" class="cellMainLink">Heroes Reborn S01E10 HDTV x264-FUM[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="329946981">314.66 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T02:49:22+00:00">20 Nov 2015, 02:49:22</span></td>
			<td class="green center">8021</td>
			<td class="red lasttd center">1018</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11621050,0" class="icommentjs kaButton smallButton rightButton" href="/the-vampire-diaries-s07e07-hdtv-x264-lol-ettv-t11621050.html#comment">95 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-vampire-diaries-s07e07-hdtv-x264-lol-ettv-t11621050.html" class="cellMainLink">The Vampire Diaries S07E07 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="205956831">196.42 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T02:05:20+00:00">20 Nov 2015, 02:05:20</span></td>
			<td class="green center">7630</td>
			<td class="red lasttd center">951</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11628425,0" class="icommentjs kaButton smallButton rightButton" href="/ash-vs-evil-dead-s01e04-webrip-x264-fum-ettv-t11628425.html#comment">62 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ash-vs-evil-dead-s01e04-webrip-x264-fum-ettv-t11628425.html" class="cellMainLink">Ash vs Evil Dead S01E04 WEBRip x264-FUM[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="287570315">274.25 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T08:38:24+00:00">21 Nov 2015, 08:38:24</span></td>
			<td class="green center">6277</td>
			<td class="red lasttd center">2042</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11615449,0" class="icommentjs kaButton smallButton rightButton" href="/modern-family-s07e07-internal-hdtv-x264-batv-ettv-t11615449.html#comment">65 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/modern-family-s07e07-internal-hdtv-x264-batv-ettv-t11615449.html" class="cellMainLink">Modern Family S07E07 INTERNAL HDTV x264-BATV[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="203617364">194.18 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T02:48:48+00:00">19 Nov 2015, 02:48:48</span></td>
			<td class="green center">6896</td>
			<td class="red lasttd center">533</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11615497,0" class="icommentjs kaButton smallButton rightButton" href="/empire-2015-s02e08-hdtv-x264-killers-ettv-t11615497.html#comment">39 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/empire-2015-s02e08-hdtv-x264-killers-ettv-t11615497.html" class="cellMainLink">Empire 2015 S02E08 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="391501063">373.36 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T02:57:16+00:00">19 Nov 2015, 02:57:16</span></td>
			<td class="green center">6622</td>
			<td class="red lasttd center">878</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11615514,0" class="icommentjs kaButton smallButton rightButton" href="/supernatural-s11e07-hdtv-x264-lol-ettv-t11615514.html#comment">121 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/supernatural-s11e07-hdtv-x264-lol-ettv-t11615514.html" class="cellMainLink">Supernatural S11E07 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="235002447">224.12 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T03:00:17+00:00">19 Nov 2015, 03:00:17</span></td>
			<td class="green center">6671</td>
			<td class="red lasttd center">576</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11620852,0" class="icommentjs kaButton smallButton rightButton" href="/greys-anatomy-s12e08-hdtv-x264-killers-ettv-t11620852.html#comment">33 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/greys-anatomy-s12e08-hdtv-x264-killers-ettv-t11620852.html" class="cellMainLink">Greys Anatomy S12E08 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="263539096">251.33 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T01:12:18+00:00">20 Nov 2015, 01:12:18</span></td>
			<td class="green center">6142</td>
			<td class="red lasttd center">627</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11621547,0" class="icommentjs kaButton smallButton rightButton" href="/scandal-us-s05e09-hdtv-x264-fleet-rartv-t11621547.html#comment">29 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/scandal-us-s05e09-hdtv-x264-fleet-rartv-t11621547.html" class="cellMainLink">Scandal US S05E09 HDTV x264-FLEET[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="298320256">284.5 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T04:35:03+00:00">20 Nov 2015, 04:35:03</span></td>
			<td class="green center">4549</td>
			<td class="red lasttd center">1416</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11627210,0" class="icommentjs kaButton smallButton rightButton" href="/grimm-s05e04-hdtv-x264-fleet-rartv-t11627210.html#comment">57 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/grimm-s05e04-hdtv-x264-fleet-rartv-t11627210.html" class="cellMainLink">Grimm S05E04 HDTV x264-FLEET[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="293084978">279.51 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T03:01:03+00:00">21 Nov 2015, 03:01:03</span></td>
			<td class="green center">4251</td>
			<td class="red lasttd center">1007</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11631521,0" class="icommentjs kaButton smallButton rightButton" href="/doctor-who-2005-s09e10-hdtv-x264-tla-ettv-t11631521.html#comment">28 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/doctor-who-2005-s09e10-hdtv-x264-tla-ettv-t11631521.html" class="cellMainLink">Doctor Who 2005 S09E10 HDTV x264-TLA[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="316559871">301.9 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T21:09:29+00:00">21 Nov 2015, 21:09:29</span></td>
			<td class="green center">3754</td>
			<td class="red lasttd center">840</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11624516,0" class="icommentjs kaButton smallButton rightButton" href="/adele-25-target-exclusive-deluxe-edition-2015-320-kbps-tx-t11624516.html#comment">109 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/adele-25-target-exclusive-deluxe-edition-2015-320-kbps-tx-t11624516.html" class="cellMainLink">Adele - 25 (Target Exclusive Deluxe Edition) - 2015 [320 kbps] - TX</a></div>
			</td>
			<td class="nobr center" data-sort="155360416">148.16 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T16:16:44+00:00">20 Nov 2015, 16:16:44</span></td>
			<td class="green center">8382</td>
			<td class="red lasttd center">2319</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11617527,0" class="icommentjs kaButton smallButton rightButton" href="/enya-dark-sky-island-deluxe-edition-2015-mp3-320kbps-h4ckus-glodls-t11617527.html#comment">47 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/enya-dark-sky-island-deluxe-edition-2015-mp3-320kbps-h4ckus-glodls-t11617527.html" class="cellMainLink">Enya - Dark Sky Island [Deluxe Edition] [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="131837945">125.73 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T12:18:35+00:00">19 Nov 2015, 12:18:35</span></td>
			<td class="green center">1515</td>
			<td class="red lasttd center">146</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11617592,0" class="icommentjs kaButton smallButton rightButton" href="/jadakiss-top-5-dead-or-alive-2015-mp3-320kbps-h4ckus-glodls-t11617592.html#comment">17 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/jadakiss-top-5-dead-or-alive-2015-mp3-320kbps-h4ckus-glodls-t11617592.html" class="cellMainLink">Jadakiss - Top 5 Dead Or Alive [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="147711429">140.87 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T12:36:33+00:00">19 Nov 2015, 12:36:33</span></td>
			<td class="green center">1192</td>
			<td class="red lasttd center">264</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11621807,0" class="icommentjs kaButton smallButton rightButton" href="/now-that-s-what-i-call-the-80-s-2015-mp3-320kbps-h4ckus-glodls-t11621807.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/now-that-s-what-i-call-the-80-s-2015-mp3-320kbps-h4ckus-glodls-t11621807.html" class="cellMainLink">Now That&#039;s What I Call The 80&#039;s [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="564050902">537.92 <span>MB</span></td>
			<td class="center">68</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T05:32:49+00:00">20 Nov 2015, 05:32:49</span></td>
			<td class="green center">1010</td>
			<td class="red lasttd center">389</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11628234,0" class="icommentjs kaButton smallButton rightButton" href="/the-official-uk-top-40-singles-chart-20th-november-2015-mp3-320kbps-h4ckus-glodls-t11628234.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-official-uk-top-40-singles-chart-20th-november-2015-mp3-320kbps-h4ckus-glodls-t11628234.html" class="cellMainLink">The Official UK Top 40 Singles Chart (20th November 2015) [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="361861661">345.1 <span>MB</span></td>
			<td class="center">44</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T07:47:04+00:00">21 Nov 2015, 07:47:04</span></td>
			<td class="green center">919</td>
			<td class="red lasttd center">511</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11621819,0" class="icommentjs kaButton smallButton rightButton" href="/mp3-new-releases-2015-week-46-amazeballz-glodls-t11621819.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/mp3-new-releases-2015-week-46-amazeballz-glodls-t11621819.html" class="cellMainLink">MP3 NEW RELEASES 2015 WEEK 46 - AMAZEBALLZ [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="4643543713">4.32 <span>GB</span></td>
			<td class="center">622</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T05:35:23+00:00">20 Nov 2015, 05:35:23</span></td>
			<td class="green center">731</td>
			<td class="red lasttd center">715</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11623617,0" class="icommentjs kaButton smallButton rightButton" href="/va-the-official-uk-top-40-singles-chart-13th-november-2015-mp3-320kbps-h4ckus-glodls-t11623617.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-the-official-uk-top-40-singles-chart-13th-november-2015-mp3-320kbps-h4ckus-glodls-t11623617.html" class="cellMainLink">VA - The Official UK Top 40 Singles Chart (13th November 2015) [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="362051892">345.28 <span>MB</span></td>
			<td class="center">44</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T12:55:54+00:00">20 Nov 2015, 12:55:54</span></td>
			<td class="green center">751</td>
			<td class="red lasttd center">264</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11628904,0" class="icommentjs kaButton smallButton rightButton" href="/diskoteka-2015-dance-club-vol-145-Ð¾Ñ-nnnb-t11628904.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/diskoteka-2015-dance-club-vol-145-Ð¾Ñ-nnnb-t11628904.html" class="cellMainLink">DISKOTEKA 2015 Dance Club Vol. 145 Ð¾Ñ NNNB</a></div>
			</td>
			<td class="nobr center" data-sort="1598082818">1.49 <span>GB</span></td>
			<td class="center">134</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T10:34:44+00:00">21 Nov 2015, 10:34:44</span></td>
			<td class="green center">731</td>
			<td class="red lasttd center">185</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11628387,0" class="icommentjs kaButton smallButton rightButton" href="/madonna-iconic-2015-mp3-320kbps-h4ckus-glodls-t11628387.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/madonna-iconic-2015-mp3-320kbps-h4ckus-glodls-t11628387.html" class="cellMainLink">Madonna - Iconic [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="108018676">103.01 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T08:30:14+00:00">21 Nov 2015, 08:30:14</span></td>
			<td class="green center">631</td>
			<td class="red lasttd center">146</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11622691,0" class="icommentjs kaButton smallButton rightButton" href="/tracy-chapman-greatest-hits-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11622691.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/tracy-chapman-greatest-hits-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11622691.html" class="cellMainLink">Tracy Chapman - Greatest Hits [2015] [MP3-320Kbps] [CBR] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="187302050">178.63 <span>MB</span></td>
			<td class="center">21</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T08:44:28+00:00">20 Nov 2015, 08:44:28</span></td>
			<td class="green center">617</td>
			<td class="red lasttd center">151</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11617485,0" class="icommentjs kaButton smallButton rightButton" href="/tech-n9ne-strangeulation-vol-ii-deluxe-edition-2015-mp3-320kbps-h4ckus-glodls-t11617485.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/tech-n9ne-strangeulation-vol-ii-deluxe-edition-2015-mp3-320kbps-h4ckus-glodls-t11617485.html" class="cellMainLink">Tech N9ne - Strangeulation, Vol. II [Deluxe Edition] [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="192193839">183.29 <span>MB</span></td>
			<td class="center">27</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T12:04:57+00:00">19 Nov 2015, 12:04:57</span></td>
			<td class="green center">473</td>
			<td class="red lasttd center">98</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11613829,0" class="icommentjs kaButton smallButton rightButton" href="/erasure-always-the-very-best-of-erasure-3cd-deluxe-version-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11613829.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/erasure-always-the-very-best-of-erasure-3cd-deluxe-version-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11613829.html" class="cellMainLink">Erasure - Always The Very Best of Erasure [3CD Deluxe Version] (2015) [MP3-320Kbps] [CBR] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="530048720">505.49 <span>MB</span></td>
			<td class="center">45</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-18T18:49:31+00:00">18 Nov 2015, 18:49:31</span></td>
			<td class="green center">382</td>
			<td class="red lasttd center">79</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11628293,0" class="icommentjs kaButton smallButton rightButton" href="/carly-simon-songs-from-the-trees-a-musical-memoir-collection-2015-mp3-vbr-h4ckus-glodls-t11628293.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/carly-simon-songs-from-the-trees-a-musical-memoir-collection-2015-mp3-vbr-h4ckus-glodls-t11628293.html" class="cellMainLink">Carly Simon - Songs From The Trees (A Musical Memoir Collection) [2015] [MP3-VBR] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="199585160">190.34 <span>MB</span></td>
			<td class="center">35</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T08:04:50+00:00">21 Nov 2015, 08:04:50</span></td>
			<td class="green center">162</td>
			<td class="red lasttd center">90</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11618136,0" class="icommentjs kaButton smallButton rightButton" href="/roger-waters-the-wall-2-cd-2015-320ak-t11618136.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/roger-waters-the-wall-2-cd-2015-320ak-t11618136.html" class="cellMainLink">Roger Waters - The Wall (2 CD) (2015)320ak</a></div>
			</td>
			<td class="nobr center" data-sort="254251947">242.47 <span>MB</span></td>
			<td class="center">30</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T14:59:53+00:00">19 Nov 2015, 14:59:53</span></td>
			<td class="green center">194</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11624121,0" class="icommentjs kaButton smallButton rightButton" href="/marco-borsato-evenwicht-2015-320kbps-rapflame-t11624121.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/marco-borsato-evenwicht-2015-320kbps-rapflame-t11624121.html" class="cellMainLink">Marco Borsato - Evenwicht (2015) {320Kbps} [Rapflame]</a></div>
			</td>
			<td class="nobr center" data-sort="103822590">99.01 <span>MB</span></td>
			<td class="center">13</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T15:03:50+00:00">20 Nov 2015, 15:03:50</span></td>
			<td class="green center">176</td>
			<td class="red lasttd center">23</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11619602,0" class="icommentjs kaButton smallButton rightButton" href="/far-cry-4-gold-edition-v1-10-all-dlcs-true-multi16-fitgirl-repack-selective-download-from-10-9-gb-t11619602.html#comment">140 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/far-cry-4-gold-edition-v1-10-all-dlcs-true-multi16-fitgirl-repack-selective-download-from-10-9-gb-t11619602.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/far-cry-4-gold-edition-v1-10-all-dlcs-true-multi16-fitgirl-repack-selective-download-from-10-9-gb-t11619602.html" class="cellMainLink">Far Cry 4: Gold Edition (v1.10 + All DLCs, True MULTI16) [FitGirl Repack, Selective Download - from 10.9 GB]</a></div>
			</td>
			<td class="nobr center" data-sort="16485159294">15.35 <span>GB</span></td>
			<td class="center">29</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T20:54:32+00:00">19 Nov 2015, 20:54:32</span></td>
			<td class="green center">221</td>
			<td class="red lasttd center">1026</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11615646,0" class="icommentjs kaButton smallButton rightButton" href="/assassin-s-creed-syndicate-black-box-t11615646.html#comment">34 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/assassin-s-creed-syndicate-black-box-t11615646.html" class="cellMainLink">Assassin&#039;s Creed Syndicate-Black Box</a></div>
			</td>
			<td class="nobr center" data-sort="22431887182">20.89 <span>GB</span></td>
			<td class="center">11</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T03:42:03+00:00">19 Nov 2015, 03:42:03</span></td>
			<td class="green center">156</td>
			<td class="red lasttd center">1024</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11613526,0" class="icommentjs kaButton smallButton rightButton" href="/hard-west-gog-t11613526.html#comment">33 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/hard-west-gog-t11613526.html" class="cellMainLink">Hard West-GOG</a></div>
			</td>
			<td class="nobr center" data-sort="3265995173">3.04 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-18T17:51:07+00:00">18 Nov 2015, 17:51:07</span></td>
			<td class="green center">486</td>
			<td class="red lasttd center">180</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11619510,0" class="icommentjs kaButton smallButton rightButton" href="/mordheim-city-of-the-damned-codex-t11619510.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/mordheim-city-of-the-damned-codex-t11619510.html" class="cellMainLink">Mordheim City of the Damned-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="8196951494">7.63 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T20:30:29+00:00">19 Nov 2015, 20:30:29</span></td>
			<td class="green center">301</td>
			<td class="red lasttd center">338</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/call-of-duty-black-ops-3-update-2-2015-pc-repack-by-r-g-games-t11633395.html" class="cellMainLink">Call of Duty: Black Ops 3 [Update 2] (2015) PC | RePack by R.G. Games</a></div>
			</td>
			<td class="nobr center" data-sort="46353354834">43.17 <span>GB</span></td>
			<td class="center">16</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T06:54:01+00:00">22 Nov 2015, 06:54:01</span></td>
			<td class="green center">90</td>
			<td class="red lasttd center">524</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11620138,0" class="icommentjs kaButton smallButton rightButton" href="/endless-legend-the-lost-tales-proper-codex-t11620138.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/endless-legend-the-lost-tales-proper-codex-t11620138.html" class="cellMainLink">Endless Legend The Lost Tales PROPER-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="4566314311">4.25 <span>GB</span></td>
			<td class="center">94</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T00:01:45+00:00">20 Nov 2015, 00:01:45</span></td>
			<td class="green center">225</td>
			<td class="red lasttd center">151</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11624099,0" class="icommentjs kaButton smallButton rightButton" href="/thea-the-awakening-codex-t11624099.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/thea-the-awakening-codex-t11624099.html" class="cellMainLink">Thea.The.Awakening-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="466621888">445.01 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T14:58:46+00:00">20 Nov 2015, 14:58:46</span></td>
			<td class="green center">227</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11616665,0" class="icommentjs kaButton smallButton rightButton" href="/dark-parables-10-goldilocks-and-the-fallen-star-collector-s-edition-asg-t11616665.html#comment">20 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/dark-parables-10-goldilocks-and-the-fallen-star-collector-s-edition-asg-t11616665.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/dark-parables-10-goldilocks-and-the-fallen-star-collector-s-edition-asg-t11616665.html" class="cellMainLink">Dark Parables 10 - Goldilocks and the Fallen Star Collector&#039;s Edition [ASG]</a></div>
			</td>
			<td class="nobr center" data-sort="899092924">857.44 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T08:01:47+00:00">19 Nov 2015, 08:01:47</span></td>
			<td class="green center">212</td>
			<td class="red lasttd center">31</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11625307,0" class="icommentjs kaButton smallButton rightButton" href="/vendetta-curse-of-ravens-cry-codex-t11625307.html#comment">16 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/vendetta-curse-of-ravens-cry-codex-t11625307.html" class="cellMainLink">Vendetta Curse of Ravens Cry-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="18779608621">17.49 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T19:22:06+00:00">20 Nov 2015, 19:22:06</span></td>
			<td class="green center">75</td>
			<td class="red lasttd center">287</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11628586,0" class="icommentjs kaButton smallButton rightButton" href="/grand-ages-medieval-r-g-catalyst-t11628586.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/grand-ages-medieval-r-g-catalyst-t11628586.html" class="cellMainLink">Grand Ages Medieval [R.G.Catalyst]</a></div>
			</td>
			<td class="nobr center" data-sort="3744130645">3.49 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T09:17:30+00:00">21 Nov 2015, 09:17:30</span></td>
			<td class="green center">148</td>
			<td class="red lasttd center">83</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11628532,0" class="icommentjs kaButton smallButton rightButton" href="/pes-2016-pro-evolution-soccer-2016-v-1-02-01-2015-pc-repack-by-mizantrop1337-t11628532.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/pes-2016-pro-evolution-soccer-2016-v-1-02-01-2015-pc-repack-by-mizantrop1337-t11628532.html" class="cellMainLink">PES 2016 / Pro Evolution Soccer 2016 [v 1.02.01] (2015) PC | RePack by Mizantrop1337</a></div>
			</td>
			<td class="nobr center" data-sort="3726656538">3.47 <span>GB</span></td>
			<td class="center">19</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T09:06:18+00:00">21 Nov 2015, 09:06:18</span></td>
			<td class="green center">104</td>
			<td class="red lasttd center">156</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11623708,0" class="icommentjs kaButton smallButton rightButton" href="/the-binding-of-isaac-afterbirth-update-7-t11623708.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/the-binding-of-isaac-afterbirth-update-7-t11623708.html" class="cellMainLink">The Binding of Isaac: Afterbirth (Update 7)</a></div>
			</td>
			<td class="nobr center" data-sort="470282870">448.5 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T13:21:18+00:00">20 Nov 2015, 13:21:18</span></td>
			<td class="green center">136</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11619076,0" class="icommentjs kaButton smallButton rightButton" href="/minecraft-pocket-edition-full-v0-13-0-no-mod-mod-android-4-0-apk-android-2-3-apk-xpoz-t11619076.html#comment">12 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/minecraft-pocket-edition-full-v0-13-0-no-mod-mod-android-4-0-apk-android-2-3-apk-xpoz-t11619076.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/minecraft-pocket-edition-full-v0-13-0-no-mod-mod-android-4-0-apk-android-2-3-apk-xpoz-t11619076.html" class="cellMainLink">Minecraft-Pocket Edition(Full) v0.13.0 [No Mod+Mod]+[(Android 4.0+Apk)+(Android 2.3+Apk)]-XpoZ</a></div>
			</td>
			<td class="nobr center" data-sort="56625863">54 <span>MB</span></td>
			<td class="center">7</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T18:45:13+00:00">19 Nov 2015, 18:45:13</span></td>
			<td class="green center">133</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11619850,0" class="icommentjs kaButton smallButton rightButton" href="/club-manager-2016-reloaded-t11619850.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/club-manager-2016-reloaded-t11619850.html" class="cellMainLink">Club.Manager.2016-RELOADED</a></div>
			</td>
			<td class="nobr center" data-sort="864033325">824.01 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T22:11:30+00:00">19 Nov 2015, 22:11:30</span></td>
			<td class="green center">111</td>
			<td class="red lasttd center">28</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11628980,0" class="icommentjs kaButton smallButton rightButton" href="/surface-8-return-to-another-world-collector-s-edition-asg-t11628980.html#comment">5 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/surface-8-return-to-another-world-collector-s-edition-asg-t11628980.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/surface-8-return-to-another-world-collector-s-edition-asg-t11628980.html" class="cellMainLink">Surface - 8 Return to Another World Collector&#039;s Edition [ASG]</a></div>
			</td>
			<td class="nobr center" data-sort="845557363">806.39 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T10:49:38+00:00">21 Nov 2015, 10:49:38</span></td>
			<td class="green center">93</td>
			<td class="red lasttd center">52</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11631848,0" class="icommentjs kaButton smallButton rightButton" href="/bittorrent-pro-7-9-5-build-41163-stable-crack-zip-t11631848.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/bittorrent-pro-7-9-5-build-41163-stable-crack-zip-t11631848.html" class="cellMainLink">BitTorrent Pro 7.9.5 Build 41163 Stable + Crack.zip</a></div>
			</td>
			<td class="nobr center" data-sort="3529172">3.37 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T23:06:36+00:00">21 Nov 2015, 23:06:36</span></td>
			<td class="green center">480</td>
			<td class="red lasttd center">314</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11623119,0" class="icommentjs kaButton smallButton rightButton" href="/utorrent-pro-3-4-5-build-41372-crack-final-nov2015-seven7i-t11623119.html#comment">83 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/utorrent-pro-3-4-5-build-41372-crack-final-nov2015-seven7i-t11623119.html" class="cellMainLink">uTorrent Pro 3.4.5 Build 41372 + Crack Final Nov2015 Seven7i</a></div>
			</td>
			<td class="nobr center" data-sort="109100359">104.05 <span>MB</span></td>
			<td class="center">420</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T10:34:35+00:00">20 Nov 2015, 10:34:35</span></td>
			<td class="green center">570</td>
			<td class="red lasttd center">123</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11618446,0" class="icommentjs kaButton smallButton rightButton" href="/iobit-advanced-system-care-pro-9-0-3-1077-setup-serial-core-x-t11618446.html#comment">52 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/iobit-advanced-system-care-pro-9-0-3-1077-setup-serial-core-x-t11618446.html" class="cellMainLink">IObit Advanced System Care Pro 9.0.3.1077 Setup + Serial - {Core-X}</a></div>
			</td>
			<td class="nobr center" data-sort="39605168">37.77 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T16:06:44+00:00">19 Nov 2015, 16:06:44</span></td>
			<td class="green center">315</td>
			<td class="red lasttd center">41</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11618489,0" class="icommentjs kaButton smallButton rightButton" href="/windows-7-blue-core-64bit-axeswy-tomecar-team-os-t11618489.html#comment">34 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-7-blue-core-64bit-axeswy-tomecar-team-os-t11618489.html" class="cellMainLink">Windows 7 Blue Core (64bit) Axeswy &amp; Tomecar-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="3296354406">3.07 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T16:17:05+00:00">19 Nov 2015, 16:17:05</span></td>
			<td class="green center">189</td>
			<td class="red lasttd center">200</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11625738,0" class="icommentjs kaButton smallButton rightButton" href="/windows-7-ultimate-sp1-x64-en-us-esd-nov2015-pre-activation-team-os-t11625738.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-7-ultimate-sp1-x64-en-us-esd-nov2015-pre-activation-team-os-t11625738.html" class="cellMainLink">Windows 7 Ultimate Sp1 x64 En-Us ESD Nov2015 Pre-Activation=-{TEAM OS}=</a></div>
			</td>
			<td class="nobr center" data-sort="3333114041">3.1 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T21:30:02+00:00">20 Nov 2015, 21:30:02</span></td>
			<td class="green center">124</td>
			<td class="red lasttd center">176</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11629776,0" class="icommentjs kaButton smallButton rightButton" href="/youtube-downloader-ytd-pro-5-0-0-crack-karanpc-t11629776.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/youtube-downloader-ytd-pro-5-0-0-crack-karanpc-t11629776.html" class="cellMainLink">YouTube Downloader (YTD) Pro 5.0.0 + Crack [KaranPC]</a></div>
			</td>
			<td class="nobr center" data-sort="12214080">11.65 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T13:44:11+00:00">21 Nov 2015, 13:44:11</span></td>
			<td class="green center">190</td>
			<td class="red lasttd center">34</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11628277,0" class="icommentjs kaButton smallButton rightButton" href="/xilisoft-video-converter-ultimate-7-8-12-build-20151119-multilanguage-serial-at-team-t11628277.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/xilisoft-video-converter-ultimate-7-8-12-build-20151119-multilanguage-serial-at-team-t11628277.html" class="cellMainLink">Xilisoft Video Converter Ultimate 7.8.12 Build 20151119 [Multilanguage] [Serial] [AT-TEAM]</a></div>
			</td>
			<td class="nobr center" data-sort="36303348">34.62 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T07:58:44+00:00">21 Nov 2015, 07:58:44</span></td>
			<td class="green center">190</td>
			<td class="red lasttd center">32</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11613669,0" class="icommentjs kaButton smallButton rightButton" href="/iobit-driver-booster-pro-3-1-0-332-t11613669.html#comment">21 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/iobit-driver-booster-pro-3-1-0-332-t11613669.html" class="cellMainLink">IObit Driver Booster Pro 3.1.0.332</a></div>
			</td>
			<td class="nobr center" data-sort="14791467">14.11 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-18T18:15:00+00:00">18 Nov 2015, 18:15:00</span></td>
			<td class="green center">195</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11621605,0" class="icommentjs kaButton smallButton rightButton" href="/microsoft-office-2010-professional-plus-visio-pro-project-pro-14-0-7162-5000-sp2-repack-by-kpojiuk-team-os-t11621605.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/microsoft-office-2010-professional-plus-visio-pro-project-pro-14-0-7162-5000-sp2-repack-by-kpojiuk-team-os-t11621605.html" class="cellMainLink">Microsoft Office 2010 Professional Plus + Visio Pro + Project Pro 14.0.7162.5000 SP2 RePack by KpoJIuK-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="4894499758">4.56 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T04:54:28+00:00">20 Nov 2015, 04:54:28</span></td>
			<td class="green center">71</td>
			<td class="red lasttd center">132</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11629352,0" class="icommentjs kaButton smallButton rightButton" href="/latest-utorrentpro-3-4-5-build-41372-stable-crack-s0ft4pc-t11629352.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/latest-utorrentpro-3-4-5-build-41372-stable-crack-s0ft4pc-t11629352.html" class="cellMainLink">Latest uTorrentPro 3.4.5 build 41372 Stable + Crack [S0ft4PC]</a></div>
			</td>
			<td class="nobr center" data-sort="5503138">5.25 <span>MB</span></td>
			<td class="center">8</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T12:13:13+00:00">21 Nov 2015, 12:13:13</span></td>
			<td class="green center">119</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11616217,0" class="icommentjs kaButton smallButton rightButton" href="/vso-convertxtovideo-ultimate-v1-6-0-42-multilingual-pl-t11616217.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/vso-convertxtovideo-ultimate-v1-6-0-42-multilingual-pl-t11616217.html" class="cellMainLink">VSO ConvertXtoVideo Ultimate v1.6.0.42 [Multilingual][PL]</a></div>
			</td>
			<td class="nobr center" data-sort="37973554">36.21 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T06:21:26+00:00">19 Nov 2015, 06:21:26</span></td>
			<td class="green center">118</td>
			<td class="red lasttd center">6</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11623979,0" class="icommentjs kaButton smallButton rightButton" href="/skype-7-14-32-106-2015-Ð Ð¡-repack-portable-by-d-akov-t11623979.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/skype-7-14-32-106-2015-Ð Ð¡-repack-portable-by-d-akov-t11623979.html" class="cellMainLink">Skype 7.14.32.106 (2015) Ð Ð¡ | RePack &amp; Portable by D!akov</a></div>
			</td>
			<td class="nobr center" data-sort="52538464">50.1 <span>MB</span></td>
			<td class="center">7</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T14:32:40+00:00">20 Nov 2015, 14:32:40</span></td>
			<td class="green center">94</td>
			<td class="red lasttd center">3</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11624279,0" class="icommentjs kaButton smallButton rightButton" href="/native-instruments-flesh-v1-0-0-os-x-dada-t11624279.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/native-instruments-flesh-v1-0-0-os-x-dada-t11624279.html" class="cellMainLink">Native Instruments - Flesh v1.0.0 OS X [dada]</a></div>
			</td>
			<td class="nobr center" data-sort="278267211">265.38 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T15:40:48+00:00">20 Nov 2015, 15:40:48</span></td>
			<td class="green center">69</td>
			<td class="red lasttd center">7</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/adobe-photoshop-lightroom-6-3-final-x64-2015-Ð Ð¡-repack-portable-by-d-akov-t11629845.html" class="cellMainLink">Adobe Photoshop Lightroom 6.3 Final [x64] (2015) Ð Ð¡ | RePack &amp; Portable by D!akov</a></div>
			</td>
			<td class="nobr center" data-sort="792272320">755.57 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T13:58:43+00:00">21 Nov 2015, 13:58:43</span></td>
			<td class="green center">50</td>
			<td class="red lasttd center">40</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/rarmaradio-pro-2-70-1-2015-pc-repack-by-d-akov-t11630667.html" class="cellMainLink">RarmaRadio Pro 2.70.1 (2015) PC | RePack by D!akov</a></div>
			</td>
			<td class="nobr center" data-sort="8379484">7.99 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T16:43:09+00:00">21 Nov 2015, 16:43:09</span></td>
			<td class="green center">62</td>
			<td class="red lasttd center">2</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11630938,0" class="icommentjs kaButton smallButton rightButton" href="/leopard-raws-owarimonogatari-08-raw-gyt-1280x720-x264-aac-mp4-t11630938.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-owarimonogatari-08-raw-gyt-1280x720-x264-aac-mp4-t11630938.html" class="cellMainLink">[Leopard-Raws] Owarimonogatari - 08 RAW (GYT 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="326876634">311.73 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T18:05:02+00:00">21 Nov 2015, 18:05:02</span></td>
			<td class="green center">962</td>
			<td class="red lasttd center">261</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/kaerizaki-fansub-one-piece-719-vostfr-hd-1280x720-mp4-t11635048.html" class="cellMainLink">[Kaerizaki-Fansub] One Piece 719 [VOSTFR][HD 1280x720].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="294342292">280.71 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T13:34:40+00:00">22 Nov 2015, 13:34:40</span></td>
			<td class="green center">757</td>
			<td class="red lasttd center">534</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ohys-raws-valkyrie-drive-mermaid-07-at-x-1280x720-x264-aac-mp4-t11634764.html" class="cellMainLink">[Ohys-Raws] Valkyrie Drive Mermaid - 07 (AT-X 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="374355637">357.01 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T12:15:27+00:00">22 Nov 2015, 12:15:27</span></td>
			<td class="green center">902</td>
			<td class="red lasttd center">178</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11632279,0" class="icommentjs kaButton smallButton rightButton" href="/ohys-raws-gakusen-toshi-asterisk-08-animax-1280x720-x264-aac-mp4-t11632279.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ohys-raws-gakusen-toshi-asterisk-08-animax-1280x720-x264-aac-mp4-t11632279.html" class="cellMainLink">[Ohys-Raws] Gakusen Toshi Asterisk - 08 (ANIMAX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="429831404">409.92 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T01:57:39+00:00">22 Nov 2015, 01:57:39</span></td>
			<td class="green center">918</td>
			<td class="red lasttd center">105</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11631228,0" class="icommentjs kaButton smallButton rightButton" href="/horriblesubs-rakudai-kishi-no-cavalry-08-720p-mkv-t11631228.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-rakudai-kishi-no-cavalry-08-720p-mkv-t11631228.html" class="cellMainLink">[HorribleSubs] Rakudai Kishi no Cavalry - 08 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="306539294">292.34 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T19:30:02+00:00">21 Nov 2015, 19:30:02</span></td>
			<td class="green center">878</td>
			<td class="red lasttd center">122</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ohys-raws-utawarerumono-itsuwari-no-kamen-08-mx-1280x720-x264-aac-mp4-t11634757.html" class="cellMainLink">[Ohys-Raws] Utawarerumono - Itsuwari no Kamen - 08 (MX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="305198045">291.06 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T12:14:34+00:00">22 Nov 2015, 12:14:34</span></td>
			<td class="green center">541</td>
			<td class="red lasttd center">66</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11626590,0" class="icommentjs kaButton smallButton rightButton" href="/animerg-digimon-adventure-tri-ep-01-04-720p-eng-subbed-scavvykid-t11626590.html#comment">16 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-digimon-adventure-tri-ep-01-04-720p-eng-subbed-scavvykid-t11626590.html" class="cellMainLink">[AnimeRG] Digimon Adventure Tri EP 01 - 04 [720p] [Eng Subbed] [ScavvyKiD]</a></div>
			</td>
			<td class="nobr center" data-sort="516065157">492.16 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T00:47:35+00:00">21 Nov 2015, 00:47:35</span></td>
			<td class="green center">420</td>
			<td class="red lasttd center">191</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ohys-raws-gochuumon-wa-usagi-desuka-2-07-at-x-1280x720-x264-aac-mp4-t11632280.html" class="cellMainLink">[Ohys-Raws] Gochuumon wa Usagi Desuka 2 - 07 (AT-X 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="363103311">346.28 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T01:58:02+00:00">22 Nov 2015, 01:58:02</span></td>
			<td class="green center">416</td>
			<td class="red lasttd center">43</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ohys-raws-soukyuu-no-fafner-exodus-2-08-21-tbs-1280x720-x264-aac-mp4-t11632293.html" class="cellMainLink">[Ohys-Raws] Soukyuu no Fafner - Exodus 2 - 08(21) (TBS 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="566152013">539.92 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T02:01:57+00:00">22 Nov 2015, 02:01:57</span></td>
			<td class="green center">373</td>
			<td class="red lasttd center">45</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11617496,0" class="icommentjs kaButton smallButton rightButton" href="/naruto-shippuden-episode-438-english-subbed-480p-arizone-t11617496.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/naruto-shippuden-episode-438-english-subbed-480p-arizone-t11617496.html" class="cellMainLink">Naruto Shippuden Episode 438 [English Subbed] 480p ~ARIZONE</a></div>
			</td>
			<td class="nobr center" data-sort="77952743">74.34 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T12:08:00+00:00">19 Nov 2015, 12:08:00</span></td>
			<td class="green center">252</td>
			<td class="red lasttd center">73</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-k-return-of-kings-08-raw-tbs-1280x720-x264-aac-mp4-t11627761.html" class="cellMainLink">[Leopard-Raws] K - Return of Kings - 08 RAW (TBS 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="583713059">556.67 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T05:55:02+00:00">21 Nov 2015, 05:55:02</span></td>
			<td class="green center">207</td>
			<td class="red lasttd center">53</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11617930,0" class="icommentjs kaButton smallButton rightButton" href="/animerg-death-note-complete-dual-720p-khatake2-t11617930.html#comment">12 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/animerg-death-note-complete-dual-720p-khatake2-t11617930.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-death-note-complete-dual-720p-khatake2-t11617930.html" class="cellMainLink">[AnimeRG] Death Note [Complete - Dual] [720p] [khatake2]</a></div>
			</td>
			<td class="nobr center" data-sort="7745258588">7.21 <span>GB</span></td>
			<td class="center">37</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T14:11:28+00:00">19 Nov 2015, 14:11:28</span></td>
			<td class="green center">84</td>
			<td class="red lasttd center">253</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-garo-guren-no-tsuki-07-raw-tx-1280x720-x264-aac-mp4-t11627762.html" class="cellMainLink">[Leopard-Raws] Garo - Guren no Tsuki - 07 RAW (TX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="567122872">540.85 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T05:55:02+00:00">21 Nov 2015, 05:55:02</span></td>
			<td class="green center">139</td>
			<td class="red lasttd center">68</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11632812,0" class="icommentjs kaButton smallButton rightButton" href="/one-piece-719-480p-engsub-iorchid-mkv-t11632812.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/one-piece-719-480p-engsub-iorchid-mkv-t11632812.html" class="cellMainLink">One Piece - 719 [480p][EngSub][iORcHiD].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="99323234">94.72 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T04:54:04+00:00">22 Nov 2015, 04:54:04</span></td>
			<td class="green center">127</td>
			<td class="red lasttd center">88</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ohys-raws-kekkai-sensen-v3-bd-1280x720-x264-aac-t11632284.html" class="cellMainLink">[Ohys-Raws] Kekkai Sensen v3 (BD 1280x720 x264 AAC)</a></div>
			</td>
			<td class="nobr center" data-sort="8508262900">7.92 <span>GB</span></td>
			<td class="center">13</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T01:59:17+00:00">22 Nov 2015, 01:59:17</span></td>
			<td class="green center">96</td>
			<td class="red lasttd center">147</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11614020,0" class="icommentjs kaButton smallButton rightButton" href="/marvel-week-11-18-2015-nem-t11614020.html#comment">51 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/marvel-week-11-18-2015-nem-t11614020.html" class="cellMainLink">Marvel Week+ (11-18-2015) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="848404642">809.1 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-18T19:22:06+00:00">18 Nov 2015, 19:22:06</span></td>
			<td class="green center">683</td>
			<td class="red lasttd center">268</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11630837,0" class="icommentjs kaButton smallButton rightButton" href="/the-python-book-the-ultimate-guide-to-coding-with-python-2015-pdf-gooner-t11630837.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-python-book-the-ultimate-guide-to-coding-with-python-2015-pdf-gooner-t11630837.html" class="cellMainLink">The Python Book - The Ultimate Guide to Coding with Python (2015).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="40829997">38.94 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T17:25:17+00:00">21 Nov 2015, 17:25:17</span></td>
			<td class="green center">435</td>
			<td class="red lasttd center">80</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11616119,0" class="icommentjs kaButton smallButton rightButton" href="/assorted-magazines-bundle-november-19-2015-true-pdf-t11616119.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/assorted-magazines-bundle-november-19-2015-true-pdf-t11616119.html" class="cellMainLink">Assorted Magazines Bundle - November 19 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="437328942">417.07 <span>MB</span></td>
			<td class="center">26</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T05:55:23+00:00">19 Nov 2015, 05:55:23</span></td>
			<td class="green center">335</td>
			<td class="red lasttd center">260</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11624826,0" class="icommentjs kaButton smallButton rightButton" href="/art-nude-photography-explained-how-to-photograph-and-understand-great-art-nude-images-2015-ertb-t11624826.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/art-nude-photography-explained-how-to-photograph-and-understand-great-art-nude-images-2015-ertb-t11624826.html" class="cellMainLink">Art Nude Photography Explained - How to Photograph and Understand Great Art Nude Images 2015 {{ERTB}}</a></div>
			</td>
			<td class="nobr center" data-sort="15511394">14.79 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T17:14:18+00:00">20 Nov 2015, 17:14:18</span></td>
			<td class="green center">408</td>
			<td class="red lasttd center">84</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11630853,0" class="icommentjs kaButton smallButton rightButton" href="/the-new-york-times-best-sellers-november-29-2015-fiction-non-fiction-t11630853.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-new-york-times-best-sellers-november-29-2015-fiction-non-fiction-t11630853.html" class="cellMainLink">The New York Times Best Sellers - November 29, 2015 [Fiction / Non-Fiction]</a></div>
			</td>
			<td class="nobr center" data-sort="219992242">209.8 <span>MB</span></td>
			<td class="center">23</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T17:30:15+00:00">21 Nov 2015, 17:30:15</span></td>
			<td class="green center">354</td>
			<td class="red lasttd center">153</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11615976,0" class="icommentjs kaButton smallButton rightButton" href="/home-garden-magazines-november-19-2015-true-pdf-t11615976.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/home-garden-magazines-november-19-2015-true-pdf-t11615976.html" class="cellMainLink">Home &amp; Garden Magazines - November 19 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="326901608">311.76 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T05:22:05+00:00">19 Nov 2015, 05:22:05</span></td>
			<td class="green center">327</td>
			<td class="red lasttd center">87</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11615946,0" class="icommentjs kaButton smallButton rightButton" href="/automobile-magazines-bundle-november-19-2015-true-pdf-t11615946.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/automobile-magazines-bundle-november-19-2015-true-pdf-t11615946.html" class="cellMainLink">Automobile Magazines Bundle - November 19 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="239982508">228.87 <span>MB</span></td>
			<td class="center">14</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T05:15:19+00:00">19 Nov 2015, 05:15:19</span></td>
			<td class="green center">292</td>
			<td class="red lasttd center">84</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11623750,0" class="icommentjs kaButton smallButton rightButton" href="/herbal-kitchen-the-50-easy-to-find-herbs-and-over-250-recipes-to-bring-lasting-health-to-you-and-your-family-t11623750.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/herbal-kitchen-the-50-easy-to-find-herbs-and-over-250-recipes-to-bring-lasting-health-to-you-and-your-family-t11623750.html" class="cellMainLink">Herbal Kitchen, The 50 Easy-to-Find Herbs and Over 250 Recipes to Bring Lasting Health to You and Your Family</a></div>
			</td>
			<td class="nobr center" data-sort="5181997">4.94 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T13:36:39+00:00">20 Nov 2015, 13:36:39</span></td>
			<td class="green center">287</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11626030,0" class="icommentjs kaButton smallButton rightButton" href="/the-economist-21-november-27-november-2015-true-pdf-w-cover-no-watermarks-t11626030.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-economist-21-november-27-november-2015-true-pdf-w-cover-no-watermarks-t11626030.html" class="cellMainLink">The Economist - 21 November - 27 November 2015 (True PDF w/ cover), NO Watermarks)</a></div>
			</td>
			<td class="nobr center" data-sort="62073897">59.2 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T22:45:54+00:00">20 Nov 2015, 22:45:54</span></td>
			<td class="green center">214</td>
			<td class="red lasttd center">44</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11621409,0" class="icommentjs kaButton smallButton rightButton" href="/train-magazines-bundle-november-21-20105-true-pdf-t11621409.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/train-magazines-bundle-november-21-20105-true-pdf-t11621409.html" class="cellMainLink">Train Magazines Bundle - November 21 20105 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="225886672">215.42 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T03:55:01+00:00">20 Nov 2015, 03:55:01</span></td>
			<td class="green center">156</td>
			<td class="red lasttd center">31</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11620104,0" class="icommentjs kaButton smallButton rightButton" href="/image-week-11-18-2015-nem-t11620104.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/image-week-11-18-2015-nem-t11620104.html" class="cellMainLink">Image Week (11-18-2015) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="694093639">661.94 <span>MB</span></td>
			<td class="center">13</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T23:55:19+00:00">19 Nov 2015, 23:55:19</span></td>
			<td class="green center">116</td>
			<td class="red lasttd center">44</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11614041,0" class="icommentjs kaButton smallButton rightButton" href="/huck-001-2015-digital-zone-empire-cbr-t11614041.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/huck-001-2015-digital-zone-empire-cbr-t11614041.html" class="cellMainLink">Huck 001 (2015) (Digital) (Zone-Empire).cbr</a></div>
			</td>
			<td class="nobr center" data-sort="41052255">39.15 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-18T19:24:46+00:00">18 Nov 2015, 19:24:46</span></td>
			<td class="green center">136</td>
			<td class="red lasttd center">3</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11614015,0" class="icommentjs kaButton smallButton rightButton" href="/ms-marvel-001-2016-digital-zone-empire-cbr-t11614015.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/ms-marvel-001-2016-digital-zone-empire-cbr-t11614015.html" class="cellMainLink">Ms. Marvel 001 (2016) (Digital) (Zone-Empire).cbr</a></div>
			</td>
			<td class="nobr center" data-sort="84730402">80.81 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-18T19:21:23+00:00">18 Nov 2015, 19:21:23</span></td>
			<td class="green center">129</td>
			<td class="red lasttd center">6</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11631546,0" class="icommentjs kaButton smallButton rightButton" href="/new-york-times-bestsellers-december-06-2015-fiction-non-fiction-t11631546.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/new-york-times-bestsellers-december-06-2015-fiction-non-fiction-t11631546.html" class="cellMainLink">New York Times Bestsellers (December 06, 2015) (Fiction + Non Fiction)</a></div>
			</td>
			<td class="nobr center" data-sort="270985182">258.43 <span>MB</span></td>
			<td class="center">31</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T21:19:03+00:00">21 Nov 2015, 21:19:03</span></td>
			<td class="green center">99</td>
			<td class="red lasttd center">40</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-real-life-mba-your-no-bs-guide-to-winning-the-game-building-a-team-and-growing-your-career-2015-epub-gooner-t11631131.html" class="cellMainLink">The Real-Life MBA - Your No-BS Guide to Winning the Game, Building a Team and Growing Your Career (2015).epub Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="131017281">124.95 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T19:05:37+00:00">21 Nov 2015, 19:05:37</span></td>
			<td class="green center">96</td>
			<td class="red lasttd center">37</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11618861,0" class="icommentjs kaButton smallButton rightButton" href="/enya-dark-sky-island-deluxe-edition-2015-cd-flac-sn3h1t87-glodls-t11618861.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/enya-dark-sky-island-deluxe-edition-2015-cd-flac-sn3h1t87-glodls-t11618861.html" class="cellMainLink">Enya - Dark Sky Island (Deluxe Edition) [2015] [CD-FLAC] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="425953573">406.22 <span>MB</span></td>
			<td class="center">7</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T17:57:42+00:00">19 Nov 2015, 17:57:42</span></td>
			<td class="green center">263</td>
			<td class="red lasttd center">38</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11631235,0" class="icommentjs kaButton smallButton rightButton" href="/va-the-greatest-easy-listening-2-cd-2004-flac-tfm-t11631235.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-the-greatest-easy-listening-2-cd-2004-flac-tfm-t11631235.html" class="cellMainLink">VA - The Greatest Easy Listening - 2-CD - (2004) - [FLAC] - [TFM]</a></div>
			</td>
			<td class="nobr center" data-sort="575486337">548.83 <span>MB</span></td>
			<td class="center">46</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T19:31:31+00:00">21 Nov 2015, 19:31:31</span></td>
			<td class="green center">203</td>
			<td class="red lasttd center">95</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11622659,0" class="icommentjs kaButton smallButton rightButton" href="/roger-waters-roger-waters-the-wall-2015-ost-flac-sn3h1t87-glodls-t11622659.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/roger-waters-roger-waters-the-wall-2015-ost-flac-sn3h1t87-glodls-t11622659.html" class="cellMainLink">Roger Waters - Roger Waters the Wall (2015) [OST-FLAC] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="647889885">617.88 <span>MB</span></td>
			<td class="center">34</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T08:35:59+00:00">20 Nov 2015, 08:35:59</span></td>
			<td class="green center">180</td>
			<td class="red lasttd center">28</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11628207,0" class="icommentjs kaButton smallButton rightButton" href="/grateful-dead-fare-thee-well-celebrating-50-years-of-grateful-dead-12cd-set-2015-flac-t11628207.html#comment">21 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/grateful-dead-fare-thee-well-celebrating-50-years-of-grateful-dead-12cd-set-2015-flac-t11628207.html" class="cellMainLink">Grateful Dead â Fare Thee Well: Celebrating 50 Years of Grateful Dead, 12CD Set (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="4771094383">4.44 <span>GB</span></td>
			<td class="center">77</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T07:40:30+00:00">21 Nov 2015, 07:40:30</span></td>
			<td class="green center">142</td>
			<td class="red lasttd center">99</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11614488,0" class="icommentjs kaButton smallButton rightButton" href="/mozart-string-quartets-nos-k-465-k-499-k-590-budapest-quartet-t11614488.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/mozart-string-quartets-nos-k-465-k-499-k-590-budapest-quartet-t11614488.html" class="cellMainLink">Mozart - String Quartets Nos. K. 465 , K. 499 &amp; K. 590 - Budapest Quartet</a></div>
			</td>
			<td class="nobr center" data-sort="253361597">241.62 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-18T20:55:19+00:00">18 Nov 2015, 20:55:19</span></td>
			<td class="green center">173</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11623087,0" class="icommentjs kaButton smallButton rightButton" href="/tchaikovsky-swan-lake-boston-symphony-orchestra-seiji-ozawa-t11623087.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/tchaikovsky-swan-lake-boston-symphony-orchestra-seiji-ozawa-t11623087.html" class="cellMainLink">Tchaikovsky - Swan Lake - Boston Symphony Orchestra, Seiji Ozawa</a></div>
			</td>
			<td class="nobr center" data-sort="644554955">614.7 <span>MB</span></td>
			<td class="center">9</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T10:27:39+00:00">20 Nov 2015, 10:27:39</span></td>
			<td class="green center">148</td>
			<td class="red lasttd center">52</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11615015,0" class="icommentjs kaButton smallButton rightButton" href="/yes-fragile-remixed-by-steven-wilson-2015-flac-beolab1700-t11615015.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/yes-fragile-remixed-by-steven-wilson-2015-flac-beolab1700-t11615015.html" class="cellMainLink">Yes - Fragile [Remixed by Steven Wilson] (2015) FLAC Beolab1700</a></div>
			</td>
			<td class="nobr center" data-sort="381845558">364.16 <span>MB</span></td>
			<td class="center">31</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T00:03:53+00:00">19 Nov 2015, 00:03:53</span></td>
			<td class="green center">146</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11625051,0" class="icommentjs kaButton smallButton rightButton" href="/garbage-garbage-20th-anniversary-deluxe-2015-24-96-hd-flac-t11625051.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/garbage-garbage-20th-anniversary-deluxe-2015-24-96-hd-flac-t11625051.html" class="cellMainLink">Garbage - Garbage (20th Anniversary Deluxe 2015) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1976648470">1.84 <span>GB</span></td>
			<td class="center">48</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T18:13:12+00:00">20 Nov 2015, 18:13:12</span></td>
			<td class="green center">109</td>
			<td class="red lasttd center">81</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11629034,0" class="icommentjs kaButton smallButton rightButton" href="/the-who-odds-sods-2015-24-96-hd-flac-t11629034.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-who-odds-sods-2015-24-96-hd-flac-t11629034.html" class="cellMainLink">The Who - Odds &amp; Sods (2015) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1601948235">1.49 <span>GB</span></td>
			<td class="center">39</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T11:00:19+00:00">21 Nov 2015, 11:00:19</span></td>
			<td class="green center">122</td>
			<td class="red lasttd center">42</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11625645,0" class="icommentjs kaButton smallButton rightButton" href="/va-a-world-of-favourite-classics-the-nation-s-favourites-3-cd-box-set-flac-t11625645.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-a-world-of-favourite-classics-the-nation-s-favourites-3-cd-box-set-flac-t11625645.html" class="cellMainLink">VA - A World of Favourite Classics - The Nation&#039;s Favourites 3 CD Box Set [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1012197899">965.31 <span>MB</span></td>
			<td class="center">30</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T21:03:09+00:00">20 Nov 2015, 21:03:09</span></td>
			<td class="green center">105</td>
			<td class="red lasttd center">59</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11618147,0" class="icommentjs kaButton smallButton rightButton" href="/miles-davis-aura-jazz-fusion-1989-flac-lossless-t11618147.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/miles-davis-aura-jazz-fusion-1989-flac-lossless-t11618147.html" class="cellMainLink">Miles Davis Aura - Jazz Fusion 1989 [FLAC-Lossless]</a></div>
			</td>
			<td class="nobr center" data-sort="351456862">335.18 <span>MB</span></td>
			<td class="center">21</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T15:01:58+00:00">19 Nov 2015, 15:01:58</span></td>
			<td class="green center">115</td>
			<td class="red lasttd center">18</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11623376,0" class="icommentjs kaButton smallButton rightButton" href="/the-who-face-dances-2015-24-96-hd-flac-t11623376.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-who-face-dances-2015-24-96-hd-flac-t11623376.html" class="cellMainLink">The Who - Face Dances (2015) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1451231253">1.35 <span>GB</span></td>
			<td class="center">32</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-20T11:54:56+00:00">20 Nov 2015, 11:54:56</span></td>
			<td class="green center">106</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11617122,0" class="icommentjs kaButton smallButton rightButton" href="/aretha-franklin-let-me-in-your-life-2012-24-192-hd-flac-t11617122.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/aretha-franklin-let-me-in-your-life-2012-24-192-hd-flac-t11617122.html" class="cellMainLink">Aretha Franklin - Let Me In Your Life (2012) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1727330300">1.61 <span>GB</span></td>
			<td class="center">29</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-19T10:12:59+00:00">19 Nov 2015, 10:12:59</span></td>
			<td class="green center">75</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11631664,0" class="icommentjs kaButton smallButton rightButton" href="/various-artists-deuses-da-guitarra-flac-tnt-t11631664.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/various-artists-deuses-da-guitarra-flac-tnt-t11631664.html" class="cellMainLink">Various Artists - Deuses da Guitarra [Flac][TNT]</a></div>
			</td>
			<td class="nobr center" data-sort="375590284">358.19 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T22:02:40+00:00">21 Nov 2015, 22:02:40</span></td>
			<td class="green center">70</td>
			<td class="red lasttd center">31</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/enya-studio-discography-1987-2015-flac-pirate-shovon-t11632261.html" class="cellMainLink">Enya - Studio Discography [1987 - 2015] [FLAC] [Pirate Shovon]</a></div>
			</td>
			<td class="nobr center" data-sort="2497053676">2.33 <span>GB</span></td>
			<td class="center">138</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T01:50:11+00:00">22 Nov 2015, 01:50:11</span></td>
			<td class="green center">48</td>
			<td class="red lasttd center">49</td>
        </tr>
			</table>


	<script type="text/javascript">
		if ($.cookie('kat_settings[fpw_column]') != null && $.cookie('kat_settings[fpw_order]')) {
			var colIndex = $.cookie('kat_settings[fpw_column]')
			var ascend = $.cookie('kat_settings[fpw_order]');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			sortTables(colIndex, ascend);
		}
		$('.frontPageWidget .firstr th .fpw-sort').click(function() {
			var colIndex = $(this).parent().index();
			var ascend = !$(this).hasClass('sortedAsc')?1:0;
			$('.fpw-sort').removeClass('sortedAsc sortedDesc');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			updateFPWsettings(colIndex, ascend);
			sortTables(colIndex, ascend);
		});
		function sortTables(colIndex, ascend) {	
			$('.frontPageWidget').each(function() {
				var data = [];
				$('tr:not(.firstr)', $(this)).each(function() {
					var name = $('td:first .cellMainLink', $(this)).text();
					var id = $('td:first .cellMainLink', $(this)).attr('href').match(/.*-t(\d+)\.html/)[1];
					var fileSize = $('td:eq(1)', $(this)).attr('data-sort');
					data.push({
						'col0':name, 'col1':padNum(fileSize), 'col2':padNum($('td:eq(2)',$(this)).text()), 'col3':padNum(id), 'col4':padNum($('td:eq(4)',$(this)).text()), 'col5':padNum($('td:eq(5)',$(this)).text()), 'html':$(this).html()
					});
				});
				data.sort(function(a, b) {
					var x = a['col'+colIndex].toLowerCase(); var y = b['col'+colIndex].toLowerCase();
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
				if (!ascend||ascend=='0') data.reverse();
				$(this).find('tr:not(.firstr)').remove();
				for (var i=0;i<data.length;i++) {
					$(this).append('<tr class="'+((i+1)%2==0?'even':'odd')+'">'+data[i].html+'</tr>');
				}
			});
		}
		function padNum(val) {
			val = '0000000000000000'+val;
			return val.substring(val.length-15);
		}
		function updateFPWsettings(t, o) {
			$.cookie('kat_settings[fpw_column]', t, { expires: 365, path: '/' });
			$.cookie('kat_settings[fpw_order]', o, { expires: 365, path: '/' });
		}
	</script>
		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
    
            
        <span  data-sc-slot="_119b0a17fab5493361a252d04bf527db"></span>
    
                
    	    <div class="spareBlock">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <span  data-sc-slot="_7063408f1c01d50e0dc2d833186ce962" data-sc-params="{ 'searchQuery': '' }"></span>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/hi-everebody-introducing-mysef/?unread=17128131">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Hi to everebody, Introducing mysef
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_2"><a class="plain" href="/user/CarolDanvers/">CarolDanvers</a></span></span> <time class="timeago" datetime="2015-11-22T17:54:19+00:00">22 Nov 2015, 17:54</time></span>
	</li>
		<li>
		<a href="/community/show/romance-romantic-comedy-anime-suggestions/?unread=17128118">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Romance/Romantic Comedy anime suggestions.
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/Harem_King/">Harem_King</a></span></span> <time class="timeago" datetime="2015-11-22T17:50:54+00:00">22 Nov 2015, 17:50</time></span>
	</li>
		<li>
		<a href="/community/show/which-one-should-i-download-solved/?unread=17128108">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Which one should I download? [Solved]
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/Proper0/">Proper0</a></span></span> <time class="timeago" datetime="2015-11-22T17:48:14+00:00">22 Nov 2015, 17:48</time></span>
	</li>
		<li>
		<a href="/community/show/what-are-you-listening-right-now-v5-thread-116026/?unread=17128106">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What are you listening to right now? V5
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/Kingjulian11/">Kingjulian11</a></span></span> <time class="timeago" datetime="2015-11-22T17:48:10+00:00">22 Nov 2015, 17:48</time></span>
	</li>
		<li>
		<a href="/community/show/book-yaang3rs-bya-academic-it-engineering-fiction-non-fictio-thread-103734/?unread=17128102">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				BÃÃK Ð¯ANGÎRS[BÐ¯] : Academic, IT, Engineering, Fiction &amp; Non-Fiction Releases
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_2"><a class="plain" href="/user/ira-1969/">ira-1969</a></span></span> <time class="timeago" datetime="2015-11-22T17:47:35+00:00">22 Nov 2015, 17:47</time></span>
	</li>
		<li>
		<a href="/community/show/kickass-anime-community-v-6/?unread=17128100">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Kickass Anime Community V.6!
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/Harem_King/">Harem_King</a></span></span> <time class="timeago" datetime="2015-11-22T17:46:59+00:00">22 Nov 2015, 17:46</time></span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/new-site-rules/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				New site Rules
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-15T14:18:43+00:00">15 Oct 2015, 14:18</time></span>
	</li>
	<li>
		<a href="/blog/post/look-mama-i-m-popular/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Look, mama, I&#039;m popular!
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-05T17:42:40+00:00">05 Oct 2015, 17:42</time></span>
	</li>
	<li>
		<a href="/blog/post/summer-updates-september-1/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Summer updates [September, 1]
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-09-01T16:13:36+00:00">01 Sep 2015, 16:13</time></span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/TheDels/post/youtube-is-finally-taking-a-stand-against-bs-copyright-strikes/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Youtube is finally taking a stand against BS Copyright Strikes</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/TheDels/">TheDels</a> <time class="timeago" datetime="2015-11-22T12:55:54+00:00">22 Nov 2015, 12:55</time></span></li>
	<li><a href="/blog/Dr.Sality/post/winged-seed-carried-by-winds/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Winged Seed carried by winds</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/Dr.Sality/">Dr.Sality</a> <time class="timeago" datetime="2015-11-22T09:01:13+00:00">22 Nov 2015, 09:01</time></span></li>
	<li><a href="/blog/Ajay.Ghale/post/sri-lanka-becomes-first-country-to-get-universal-internet-with-project-google-loon/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Sri Lanka Becomes First Country to Get Universal Internet With Project Google Loon.</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/Ajay.Ghale/">Ajay.Ghale</a> <time class="timeago" datetime="2015-11-22T04:21:36+00:00">22 Nov 2015, 04:21</time></span></li>
	<li><a href="/blog/zotobom/post/more-bad-news-regarding-pc/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> More Bad News Regarding PC</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/zotobom/">zotobom</a> <time class="timeago" datetime="2015-11-21T19:26:58+00:00">21 Nov 2015, 19:26</time></span></li>
	<li><a href="/blog/Dude./post/extraÃ±o-mÃºsica-parte-uno/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> ExtraÃ±o MÃºsica: Parte uno</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/Dude./">Dude.</a> <time class="timeago" datetime="2015-11-21T18:20:08+00:00">21 Nov 2015, 18:20</time></span></li>
	<li><a href="/blog/rabinsxp/post/saaya-sometimes-the-truth-is-invisible-like-the-unborn-fetus-inside-the-womb/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Saaya - Sometimes the truth is invisible like the unborn fetus inside the womb.</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/rabinsxp/">rabinsxp</a> <time class="timeago" datetime="2015-11-21T17:20:05+00:00">21 Nov 2015, 17:20</time></span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/belen%20moreno/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				belen moreno
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/ellie%20roe/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				ellie roe
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/song%20of%20the%20sea%202014/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				song of the sea 2014
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/windows%207%20wifi/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				windows 7 wifi
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/owarimonogatari/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Owarimonogatari
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/%22ios%209%22/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				&quot;ios 9&quot;
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/silvia%20saint/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				silvia saint
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/missing%20at%2017/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				missing at 17
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/no.%202%20discography/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				No. 2 discography
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/castle.2009.s08e06/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Castle.2009.S08E06
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/5%20to%207/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				5 to 7
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a href="#" onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a href="#" onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a href="#" onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a href="#" onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a href="#" onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a href="#" onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a href="#" onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a href="#" onclick="setLanguage('bsc', '.kat.cr');return false;" class="plain">Bosnian-Cyrillic</a></li>
                                <li><a href="#" onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a href="#" onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a href="#" onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a href="#" onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a href="#" onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a href="#" onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a href="#" onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a href="#" onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a href="#" onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a href="#" onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a href="#" onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a href="#" onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a href="#" onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a href="#" onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a href="#" onclick="setLanguage('he', '.kat.cr');return false;" class="plain">Hebrew</a></li>
                                <li><a href="#" onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a href="#" onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a href="#" onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a href="#" onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a href="#" onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a href="#" onclick="setLanguage('ko', '.kat.cr');return false;" class="plain">Korean</a></li>
                                <li><a href="#" onclick="setLanguage('lv', '.kat.cr');return false;" class="plain">Latvian</a></li>
                                <li><a href="#" onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a href="#" onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a href="#" onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a href="#" onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a href="#" onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a href="#" onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a href="#" onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a href="#" onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a href="#" onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a href="#" onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a href="#" onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a href="#" onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a href="#" onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a href="#" onclick="setLanguage('si', '.kat.cr');return false;" class="plain">Sinhala</a></li>
                                <li><a href="#" onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a href="#" onclick="setLanguage('sl', '.kat.cr');return false;" class="plain">Slovenian</a></li>
                                <li><a href="#" onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a href="#" onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a href="#" onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a href="#" onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a href="#" onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a href="#" onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a href="#" onclick="setLanguage('ur', '.kat.cr');return false;" class="plain">Urdu</a></li>
                                <li><a href="#" onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" data-nop href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
        		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
        <li><a href="https://kastatus.com">KAT status</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <span  data-sc-slot="_673e31f53f8166159b8e996c4124765b"></span>
        <span  data-sc-slot="_e7050fb15fd39b3e4e99a5be4a57b6ea"></span>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script>
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "Organization",
"name": "KickassTorrents",
"url": "https://kat.cr",
"logo": "https://kat.cr/content/images/kickasslogo.png",
"sameAs": [
    "https://www.facebook.com/Official.KAT.Fanclub",
    "https://twitter.com/kickasstorrents"
]
}
</script>
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "WebSite",
"url": "https://kat.cr",
"potentialAction": {
    "@type": "SearchAction",
    "target": "https://kat.cr/usearch/{q}/",
    "query-input": {
        "@type": "PropertyValueSpecification",
        "valueRequired": true,
        "valueName": "q"
    }
}
}
</script>
<script>
 sc('addGlobal', 'pagetype', 'front');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>

</body>
</html>
