



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

    
    
    

    
    
    
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "092-9652532-3281897";
                var ue_id = "1WEN9SK6MFY3MWSSPKCR";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        

        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />

    <meta property='fb:app_id' content='115109575169727' />

    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="1WEN9SK6MFY3MWSSPKCR" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1a-c3-2xl-i-d0bd6f2e.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-1965580546._CB290605844_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-3118624260._CB293333852_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-2454701167._CB293329573_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-1180111305._CB293333875_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-520887519._CB293333839_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
(function(){var d=function(o){return Object.prototype.toString.call(o)==="[object Array]";},g=function(q,p){var o;for(o=0;o<q.length;o++){if(o in q){p.call(null,q[o],o);}}},h=[],k,b,l=false,n=false,f=function(){var o=[],p=[],q={};g(h,function(s){var r="";g(s.dartsite.split("/"),function(t){if(t!==""){if(t in q){}else{q[t]=o.length;o.push(t);}r+="/"+q[t];}});p.push(r);});return{iu_parts:o,enc_prev_ius:p};},c=function(){var o=[];g(h,function(q){var p=[];g(q.sizes,function(r){p.push(r.join("x"));});o.push(p.join("|"));});return o;},m=function(){var o=[];g(h,function(p){o.push(a(p.targeting));});return o.join("|");},a=function(r,o){var q,p,s=[];for(q in r){p=[];for(j=0;j<r[q].length;j++){p.push(encodeURIComponent(r[q][j]));}if(o){s.push(q+"="+encodeURIComponent(p.join(",")));}else{s.push(q+"="+p.join(","));}}return s.join("&");},e=function(){var o=+new Date();if(n){return;}if(!this.readyState||"loaded"===this.readyState){n=true;if(l){imdbads.cmd.push(function(){for(i=0;i<h.length;i++){generic.monitoring.record_metric(h[i].name+".fail",csm.duration(o));}});}}};window.tinygpt={define_slot:function(r,q,o,p){h.push({dartsite:r.replace(/\/$/,""),sizes:q,name:o,targeting:p});},set_targeting:function(o){k=o;},callback:function(q){var r,p={},t,o,s=+new Date();l=false;for(r=0;r<h.length;r++){t=h[r].dartsite;o=h[r].name;if(q[r][t]){p[o]=q[r][t];}else{window.console&&console.error&&console.error("Unable to correlate GPT response for "+o);}}imdbads.cmd.push(function(){for(r=0;r<h.length;r++){ad_utils.slot_events.trigger(h[r].name,"request",{timestamp:b});ad_utils.slot_events.trigger(h[r].name,"tagdeliver",{timestamp:s});}ad_utils.gpt.handle_response(p);});},send:function(){var r=[],q=function(s,t){if(d(t)){t=t.join(",");}if(t){r.push(s+"="+encodeURIComponent(""+t));}},o,p;if(h.length===0){tinygpt.callback({});return;}q("gdfp_req","1");q("correlator",Math.floor(4503599627370496*Math.random()));q("output","json_html");q("callback","tinygpt.callback");q("impl","fifs");q("json_a","1");result=f();q("iu_parts",result.iu_parts);q("enc_prev_ius",result.enc_prev_ius);q("prev_iu_szs",c());q("prev_scp",m());q("cust_params",a(k,true));o=document.createElement("script");p=document.getElementsByTagName("script")[0];o.async=true;o.type="text/javascript";o.src="http://pubads.g.doubleclick.net/gampad/ads?"+r.join("&");o.id="tinygpt";o.onload=o.onerror=o.onreadystatechange=e;l=true;p.parentNode.insertBefore(o,p);b=+new Date();}};})();</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[300,600],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['e'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['2'],
'u': ['410009526473'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-tarnhelm-2214936824._CB289610996_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"e34f19622a4258306e70f0209b939febf3634527",
"2015-11-02T18%3A53%3A34GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 47186;
generic.days_to_midnight = 0.5461342334747314;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-1582251138._CB317085808_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'e']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm == null || window.generic == null || window.consoleLog == null)) {
                    if (window.console && console.log) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=e;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=410009526473;ord=410009526473?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=e;bpx=2;c=0;s=3075;s=32;ord=410009526473?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=e;bpx=2;c=0;s=3075;s=32;ord=410009526473?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
onsubmit="(new Image()).src='/rg/SEARCH-BOX/HEADER/images/b.gif?link=/find';"
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/chart/toptv/?ref_=nv_tp_tv250_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                        <li><a href="/chart/top?ref_=nv_mv_250_6"
>Top Rated Movies</a></li>
                            <li><a href="/chart/moviemeter?ref_=nv_mv_mpm_7"
>Most Popular Movies</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/chart/?ref_=nv_ch_cht_1"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_2"
>Oscar Winners</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_3"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/falltv/?ref_=nv_tvv_fall_1"
>Fall TV</a></li>
                        <li><a href="/list/ls074418362/?ref_=nv_tvv_picks_2"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_3"
>On Tonight</a></li>
                        <li><a href="/chart/toptv/?ref_=nv_tvv_250_4"
>Top Rated TV Shows</a></li>
                            <li><a href="/chart/tvmeter?ref_=nv_tvv_mptv_5"
>Most Popular TV Shows</a></li>
                        <li><a href="/list/ls074093523/?ref_=nv_tvv_wn_6"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_7"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_2"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/whattowatch/?ref_=nv_sf_wtw_3"
>What to Watch</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=11-02&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_2"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_3"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_4"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_5"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_6"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_7"
>Emmy Awards</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_8"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_9"
>Toronto Film Festival</a></li>
                        <li><a href="/awards-central/?ref_=nv_ev_all_10"
>More Popular Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni59146647/?ref_=nv_nw_tn_1"
> New 'Star Trek' TV Series To Launch In 2017, 'Star Trek 4' Slated For 2019
</a><br />
                        <span class="time">2 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59145638/?ref_=nv_nw_tn_2"
> Hollywood Film Awards: Will Smith, Johnny Depp Help Kick Off the Season
</a><br />
                        <span class="time">12 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59145328/?ref_=nv_nw_tn_3"
> Fred Thompson, 'Law & Order' Actor and Former Senator, Dies at 73
</a><br />
                        <span class="time">19 hours ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/games/guess?ref_=nv_cm_qz_3"
>Quiz Game</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_4"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5>
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0027977/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMjA0NTI5MDE1NV5BMl5BanBnXkFtZTcwMDg3NzE5Ng@@._V1._SY375_CR45,60,410,315_CT10_.jpg",
            titleYears : "1936",
            rank : 39,
                    headline : "Modern Times"
    },
    nameAd : {
            clickThru : "/name/nm0000204/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMjI2NDQxNzUwM15BMl5BanBnXkFtZTcwNDg0MzcyNw@@._V1._SX250_CR0,0,250,315_.jpg",
            rank : 81,
            headline : "Natalie Portman"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYvqVV_tDgLo10frlAYnDU3OwlByIJMhBidvp8w_DUvsm0DBLoIFZka-VnGx5pxfntCKxycbLHK%0D%0AgJohzfeSuylohcyCYh7gTWi1TmBB3zvw6528KmY1o4jvBY1HzU4JBVGXzkneshtzHD3rIfvt1pZf%0D%0AFs7iBb9TzU0QxSbCfyINfq77soIASJiItwbrEwN9m-JJQQu8PuiRN3nia5E9bGiG4g%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYnHf-hTzA42CNXNda2nawdCtVNv9kyE-BnbN1RmhAF2vwDG_VzBYbdhMh0TgDII2iOMZpWEkyK%0D%0AGjs5ygTXWBBKi3X4rYZwJX53JfypNvk4JwVY9tQ_ySezpZmC9N4X_EGgAOczxNkgvldo8qNCb363%0D%0AnnzDfOwEA--koTAPZY1bzRcz5HoGqeetIm0c7B_30qJl%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYveILWDXwr0rY1HT9xqtPMP4dglLlDtFNPkvowSUDJBO5qzRh3Aza9Jio_DN4SVbm18h_FrK95%0D%0AgAakjRgX43qbNZkIJRqIZl9NWMNFtdBECkkf_o2I6ujl4a0Ag_z3iKgxnHJFFatCIRwwAkLcp2Hp%0D%0AglrEGpcXXE07Km521r0kyRU3zj_SEU1wpsRtOXRld_vBhwovKS2E42CZhe0mMRpk7w%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=410009526473;ord=410009526473?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>

    <div id="pagecontent">
    <div class="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=410009526473;ord=410009526473?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	
    </div>
    <div id="top-slot-wrapper" class="pagecontent">

    
    
    

    
    
    
    </div>
    <div class="pagecontent">
        <div id="content-2-wide">
            <div id="main">

    
    
        <a name="slot_hero"></a>
        <div class="heroWidget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','HeroWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_hero">
<div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi850375193?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265501102&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265501102&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi850375193" data-source="bylist" data-id="ls056131825" data-rid="1WEN9SK6MFY3MWSSPKCR" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="After a supernatural event at his church a preacher enlists the help of a vampire to find God." alt="After a supernatural event at his church a preacher enlists the help of a vampire to find God." src="http://ia.media-imdb.com/images/M/MV5BMTYxODQ0MTA5MF5BMl5BanBnXkFtZTgwODM5MTY3NjE@._V1_SY298_CR2,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTYxODQ0MTA5MF5BMl5BanBnXkFtZTgwODM5MTY3NjE@._V1_SY298_CR2,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="After a supernatural event at his church a preacher enlists the help of a vampire to find God." title="After a supernatural event at his church a preacher enlists the help of a vampire to find God." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="After a supernatural event at his church a preacher enlists the help of a vampire to find God." title="After a supernatural event at his church a preacher enlists the help of a vampire to find God." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt5016504/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265501102&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265501102&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > "Preacher" </a> </div> </div> <div class="secondary ellipsis"> Trailer #1 </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3148788249?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265501102&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265501102&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi3148788249" data-source="bylist" data-id="ls002653141" data-rid="1WEN9SK6MFY3MWSSPKCR" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="Based on the 1820 event, a whaling ship is preyed upon by a sperm whale, stranding its crew at sea for 90 days, thousands of miles from home." alt="Based on the 1820 event, a whaling ship is preyed upon by a sperm whale, stranding its crew at sea for 90 days, thousands of miles from home." src="http://ia.media-imdb.com/images/M/MV5BMjA5NzUwODExM15BMl5BanBnXkFtZTgwNjM0MzE4NjE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA5NzUwODExM15BMl5BanBnXkFtZTgwNjM0MzE4NjE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Based on the 1820 event, a whaling ship is preyed upon by a sperm whale, stranding its crew at sea for 90 days, thousands of miles from home." title="Based on the 1820 event, a whaling ship is preyed upon by a sperm whale, stranding its crew at sea for 90 days, thousands of miles from home." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Based on the 1820 event, a whaling ship is preyed upon by a sperm whale, stranding its crew at sea for 90 days, thousands of miles from home." title="Based on the 1820 event, a whaling ship is preyed upon by a sperm whale, stranding its crew at sea for 90 days, thousands of miles from home." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt1390411/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265501102&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265501102&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > In the Heart of the Sea </a> </div> </div> <div class="secondary ellipsis"> Theatrical Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi782807577?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265501102&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265501102&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi782807577" data-source="bylist" data-id="ls002252034" data-rid="1WEN9SK6MFY3MWSSPKCR" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="Through a series of misunderstandings, Alvin, Simon and Theodore come to believe that Dave is going to propose to his new girlfriend in Miami...and dump them. They have three days to get to him and stop the proposal, saving themselves not only from losing Dave but possibly from gaining a terrible stepbrother." alt="Through a series of misunderstandings, Alvin, Simon and Theodore come to believe that Dave is going to propose to his new girlfriend in Miami...and dump them. They have three days to get to him and stop the proposal, saving themselves not only from losing Dave but possibly from gaining a terrible stepbrother." src="http://ia.media-imdb.com/images/M/MV5BMTcyNDY0MjY2M15BMl5BanBnXkFtZTgwMTU3Mjk4NjE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTcyNDY0MjY2M15BMl5BanBnXkFtZTgwMTU3Mjk4NjE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Through a series of misunderstandings, Alvin, Simon and Theodore come to believe that Dave is going to propose to his new girlfriend in Miami...and dump them. They have three days to get to him and stop the proposal, saving themselves not only from losing Dave but possibly from gaining a terrible stepbrother." title="Through a series of misunderstandings, Alvin, Simon and Theodore come to believe that Dave is going to propose to his new girlfriend in Miami...and dump them. They have three days to get to him and stop the proposal, saving themselves not only from losing Dave but possibly from gaining a terrible stepbrother." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Through a series of misunderstandings, Alvin, Simon and Theodore come to believe that Dave is going to propose to his new girlfriend in Miami...and dump them. They have three days to get to him and stop the proposal, saving themselves not only from losing Dave but possibly from gaining a terrible stepbrother." title="Through a series of misunderstandings, Alvin, Simon and Theodore come to believe that Dave is going to propose to his new girlfriend in Miami...and dump them. They have three days to get to him and stop the proposal, saving themselves not only from losing Dave but possibly from gaining a terrible stepbrother." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2974918/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265501102&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265501102&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > Alvin and the Chipmunks: The Road Chip </a> </div> </div> <div class="secondary ellipsis"> Trailer #2 </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/trailers?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265501102&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265501102&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Browse more trailers </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','HeroWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-2"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <h3>2015 Hollywood Film Awards</h3> </span> </span> <p class="blurb">The stars stepped out Sunday, Nov. 1, in Los Angeles to kick off awards season with the 19th Annual Hollywood Film Awards. Check out show photos, press and backstage shots, and looks on the red carpet.</p> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm2067784192/rg248355584?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265756362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_awa_hfa_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265756362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Amy Schumer" alt="Amy Schumer" src="http://ia.media-imdb.com/images/M/MV5BMTkyODU4NTM3NF5BMl5BanBnXkFtZTgwNjM3NjcxNzE@._V1_SY201_CR24,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTkyODU4NTM3NF5BMl5BanBnXkFtZTgwNjM3NjcxNzE@._V1_SY201_CR24,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/gallery/rg248355584?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265756362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_awa_hfa_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265756362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > Photos From the Show </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3460293120/rg13474560?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265756362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_awa_hfa_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265756362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Will Smith" alt="Will Smith" src="http://ia.media-imdb.com/images/M/MV5BMjEyNDE0Nzc4NF5BMl5BanBnXkFtZTgwMzIwNzcxNzE@._V1_SY201_CR34,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjEyNDE0Nzc4NF5BMl5BanBnXkFtZTgwMzIwNzcxNzE@._V1_SY201_CR34,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/gallery/rg13474560?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265756362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_awa_hfa_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265756362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > Press and Backstage Shots </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm37675520/rg265132800?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265756362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_awa_hfa_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265756362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Carey Mulligan" alt="Carey Mulligan" src="http://ia.media-imdb.com/images/M/MV5BODAxOTk0NDEyNV5BMl5BanBnXkFtZTgwOTkyNjcxNzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BODAxOTk0NDEyNV5BMl5BanBnXkFtZTgwOTkyNjcxNzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/gallery/rg265132800?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265756362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_awa_hfa_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265756362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > Looks on the Red Carpet </a> </div> </div> </div> </div> </div> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-3"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NewsDeskWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59146647?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTY5MTIxNjkxOF5BMl5BanBnXkFtZTYwNTkyOTE2._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59146647?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1"
class="headlines" >New 'Star Trek' TV Series To Launch In 2017, 'Star Trek 4' Slated For 2019</a>
    <div class="infobar">
            <span class="text-muted">2 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0032055?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_src"
>Indiewire Television</a></span>
    </div>
                                </div>
<p>While one franchise is going to take you to a place a long time ago, in a galaxy, far, far away, another is getting ready to boldly go where no man has gone before. Next summer, <a href="/title/tt2660888?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk1">"Star Trek Beyond"</a> will bring the revived franchise back to the big screen, but the Gene Rodenberry created series is ...                                        <span class="nobr"><a href="/news/ni59146647?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59145638?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2"
class="headlines" >Hollywood Film Awards: Will Smith, Johnny Depp Help Kick Off the Season</a>
    <div class="infobar">
            <span class="text-muted">12 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000001?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2_src"
>The Hollywood Reporter</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59145328?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3"
class="headlines" >Fred Thompson, 'Law & Order' Actor and Former Senator, Dies at 73</a>
    <div class="infobar">
            <span class="text-muted">19 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59144916?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4"
class="headlines" >Box Office: 'Our Brand Is Crisis,' 'Burnt,' 'Scouts Guide' All Bomb</a>
    <div class="infobar">
            <span class="text-muted">1 November 2015 4:46 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59144939?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5"
class="headlines" >Oscar mystery: Is 'The Martian' the next 'Gravity'?</a>
    <div class="infobar">
            <span class="text-muted">1 November 2015 5:12 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000143?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5_src"
>Gold Derby</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59145638?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTM0ODU5Nzk2OV5BMl5BanBnXkFtZTcwMzI2ODgyNQ@@._V1_SY150_CR2,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59145638?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1"
class="headlines" >Hollywood Film Awards: Will Smith, Johnny Depp Help Kick Off the Season</a>
    <div class="infobar">
            <span class="text-muted">12 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000001?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_src"
>The Hollywood Reporter</a></span>
    </div>
                                </div>
<p> "If you told me a year ago that I would be hosting the <a href="/title/tt4196864?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk1">Hollywood Film Awards</a>, I would ask you 'What are the <a href="/title/tt4196864?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk2">Hollywood Film Awards</a>?' " That is how host <a href="/name/nm0179479?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk3">James Corden</a> kicked of the evening's festivities at the 19th annualÂ HFAs.Â  The event is marketed as being "The Official Launch of the Awards Season...                                        <span class="nobr"><a href="/news/ni59145638?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59144939?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2"
class="headlines" >Oscar mystery: Is 'The Martian' the next 'Gravity'?</a>
    <div class="infobar">
            <span class="text-muted">1 November 2015 5:12 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000143?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2_src"
>Gold Derby</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59144916?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3"
class="headlines" >Box Office: 'Our Brand Is Crisis,' 'Burnt,' 'Scouts Guide' All Bomb</a>
    <div class="infobar">
            <span class="text-muted">1 November 2015 4:46 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59145064?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4"
class="headlines" >Arthouse Audit: Gaspar Noe's 'Love' Conquers All at a Scary Halloween Box Office</a>
    <div class="infobar">
            <span class="text-muted">1 November 2015 7:17 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000353?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4_src"
>Thompson on Hollywood</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59145086?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5"
class="headlines" >âSpectreâ Smashes Records With $80.4M Opening; Overtakes âSkyfallâ</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5_src"
>Deadline</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59146647?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTY5MTIxNjkxOF5BMl5BanBnXkFtZTYwNTkyOTE2._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59146647?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1"
class="headlines" >New 'Star Trek' TV Series To Launch In 2017, 'Star Trek 4' Slated For 2019</a>
    <div class="infobar">
            <span class="text-muted">2 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0032055?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_src"
>Indiewire Television</a></span>
    </div>
                                </div>
<p>While one franchise is going to take you to a place a long time ago, in a galaxy, far, far away, another is getting ready to boldly go where no man has gone before. Next summer, <a href="/title/tt2660888?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk1">"Star Trek Beyond"</a> will bring the revived franchise back to the big screen, but the Gene Rodenberry created series is ...                                        <span class="nobr"><a href="/news/ni59146647?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59145328?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2"
class="headlines" >Fred Thompson, 'Law & Order' Actor and Former Senator, Dies at 73</a>
    <div class="infobar">
            <span class="text-muted">19 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59144902?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3"
class="headlines" >âSupergirl': Mehcad Brooks and Jeremy Jordan on Season 1, Jimmy Olsen, and More</a>
    <div class="infobar">
            <span class="text-muted">1 November 2015 4:26 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000152?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3_src"
>Collider.com</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59146824?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4"
class="headlines" >TBS Gives Series Order to Mystery Comedy âSearch Partyâ</a>
    <div class="infobar">
            <span class="text-muted">58 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59144587?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5"
class="headlines" >Donald Trump Swaps Insults With âBoringâ John Oliver for Calling Him âOpen Bookâ</a>
    <div class="infobar">
            <span class="text-muted">1 November 2015 1:44 AM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5_src"
>The Wrap</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59146754?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTY2NTEwNTEwMV5BMl5BanBnXkFtZTcwNDA1NDQ2Mw@@._V1_SY150_CR5,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59146754?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1"
class="headlines" >Bill Cosby & Marty Singer To Give Depositions In Janice Dickinson Defamation Case</a>
    <div class="infobar">
            <span class="text-muted">2 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004913?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_src"
>Deadline TV</a></span>
    </div>
                                </div>
<p>By order of a state judge today, the comedian and his former lawyer will have to face lawyers for another woman alleging that heÂ sexually assaulted her. In a brief hearing Monday in L.A. Superior Court, attorneys for <a href="/name/nm0001070?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk1">Bill Cosby</a> were told by Judge Debra Weintraub that discovery and depositions will ...                                        <span class="nobr"><a href="/news/ni59146754?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59145068?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2"
class="headlines" >Heidi Klum as Jessica Rabbit Wins Halloween (But These Celebrities Didn't Do So Badly, Either)</a>
    <div class="infobar">
            <span class="text-muted">1 November 2015 7:27 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2_src"
>Vulture</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59144796?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3"
class="headlines" >Kate Winslet says children being harmed by social media</a>
    <div class="infobar">
            <span class="text-muted">1 November 2015 1:13 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000680?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3_src"
>The Guardian - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59144969?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4"
class="headlines" >Jamie Chung and Bryan Greenberg Are Married - See Her Gorgeous Wedding Dress!</a>
    <div class="infobar">
            <span class="text-muted">1 November 2015 5:50 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000088?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4_src"
>Popsugar.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59145121?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5"
class="headlines" >LeBron James Was Prince at a Halloween Party and Performed a Concert, Singing "Purple Rain" and More Songs</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5_src"
>E! Online</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NewsDeskWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-4"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <h3>IMDb Snapshot</h3> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/falltv/galleries/ash-vs-evil-dead-rm2840781312?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261238042&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261238042&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Ash vs Evil Dead (2015-)" alt="Ash vs Evil Dead (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMTk0MDA1ODAyNF5BMl5BanBnXkFtZTgwNTYxMzYxNzE@._V1_SY201_CR25,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk0MDA1ODAyNF5BMl5BanBnXkFtZTgwNTYxMzYxNzE@._V1_SY201_CR25,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/falltv/galleries/ash-vs-evil-dead-rm2840781312?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261238042&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261238042&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > "Ash vs. Evil Dead" - Season One Photos </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm2907955712/rg63806208?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261238042&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261238042&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="River Phoenix" alt="River Phoenix" src="http://ia.media-imdb.com/images/M/MV5BMTg0Mjg1NzYzOF5BMl5BanBnXkFtZTgwMDI3MzYxNzE@._V1_SY201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg0Mjg1NzYzOF5BMl5BanBnXkFtZTgwMDI3MzYxNzE@._V1_SY201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm2907955712/rg63806208?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261238042&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261238042&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > Remembering River Phoenix </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/falltv/galleries/scream-queens-rm1649467904?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261238042&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261238042&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Scream Queens (2015-)" alt="Scream Queens (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMTY3MDMyNjUwMF5BMl5BanBnXkFtZTgwMDA3MTYxNzE@._V1_SY201_CR44,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY3MDMyNjUwMF5BMl5BanBnXkFtZTgwMDA3MTYxNzE@._V1_SY201_CR44,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/falltv/galleries/scream-queens-rm1649467904?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261238042&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261238042&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > "Scream Queens" - New Photos </a> </div> </div> </div> </div> </div> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-5"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BornTodayWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
                <div class="ab_borntoday">
<span class="ninja_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=11-2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Born Today</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0410622?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Katharine Isabelle" alt="Katharine Isabelle" src="http://ia.media-imdb.com/images/M/MV5BMjIyNzEyODQ1M15BMl5BanBnXkFtZTgwMTI0MDc0MTE@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjIyNzEyODQ1M15BMl5BanBnXkFtZTgwMTI0MDc0MTE@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0410622?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_1">Katharine Isabelle</a> (34) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0001710?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="David Schwimmer" alt="David Schwimmer" src="http://ia.media-imdb.com/images/M/MV5BMjExOTQxMTg1Ml5BMl5BanBnXkFtZTcwMDUzMzQ2MQ@@._V1_SY172_CR5,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjExOTQxMTg1Ml5BMl5BanBnXkFtZTcwMDUzMzQ2MQ@@._V1_SY172_CR5,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0001710?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_2">David Schwimmer</a> (49) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0451321?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Shah Rukh Khan" alt="Shah Rukh Khan" src="http://ia.media-imdb.com/images/M/MV5BMTQxMjg4Mzk1Nl5BMl5BanBnXkFtZTcwMzQyMTUxNw@@._V1_SY172_CR1,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQxMjg4Mzk1Nl5BMl5BanBnXkFtZTcwMzQyMTUxNw@@._V1_SY172_CR1,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0451321?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_3">Shah Rukh Khan</a> (50) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0694619?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Stefanie Powers" alt="Stefanie Powers" src="http://ia.media-imdb.com/images/M/MV5BMjE0NTMyODE3Nl5BMl5BanBnXkFtZTYwMzY5NTQ2._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE0NTMyODE3Nl5BMl5BanBnXkFtZTYwMzY5NTQ2._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0694619?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_4">Stefanie Powers</a> (73) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0629653?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Marisol Nichols" alt="Marisol Nichols" src="http://ia.media-imdb.com/images/M/MV5BMTgyNTA0ODk5Ml5BMl5BanBnXkFtZTgwNjAyMTI3NjE@._V1_SY172_CR10,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTgyNTA0ODk5Ml5BMl5BanBnXkFtZTgwNjAyMTI3NjE@._V1_SY172_CR10,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0629653?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_5">Marisol Nichols</a> (42) </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/search/name?refine=birth_monthday&birth_monthday=11-2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See all birthdays </a> </p>        </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','BornTodayWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-7"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/imdbpicks/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265484042&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265484042&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <h3>IMDb Picks: November</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265484042&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265484042&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Hunger Games: Mockingjay - Part 2" alt="The Hunger Games: Mockingjay - Part 2" src="http://ia.media-imdb.com/images/M/MV5BNjQzNDI2NTU1Ml5BMl5BanBnXkFtZTgwNTAyMDQ5NjE@._V1_SX148_CR0,0,148,219_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjQzNDI2NTU1Ml5BMl5BanBnXkFtZTgwNTAyMDQ5NjE@._V1_SX148_CR0,0,148,219_AL_UY438_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265484042&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265484042&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="&quot;W/ Bob & David&quot;" alt="&quot;W/ Bob & David&quot;" src="http://ia.media-imdb.com/images/M/MV5BNTIzOTA4MDk0Nl5BMl5BanBnXkFtZTgwMTQ5NjAxNzE@._V1_SX148_CR0,0,148,219_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNTIzOTA4MDk0Nl5BMl5BanBnXkFtZTgwMTQ5NjAxNzE@._V1_SX148_CR0,0,148,219_AL_UY438_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265484042&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265484042&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="&quot;Into the Badlands&quot;" alt="&quot;Into the Badlands&quot;" src="http://ia.media-imdb.com/images/M/MV5BMTg0NjkxOTY2M15BMl5BanBnXkFtZTgwODc0NTE0NjE@._V1_SX148_CR0,0,148,219_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg0NjkxOTY2M15BMl5BanBnXkFtZTgwODc0NTE0NjE@._V1_SX148_CR0,0,148,219_AL_UY438_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265484042&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265484042&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Good Dinosaur" alt="The Good Dinosaur" src="http://ia.media-imdb.com/images/M/MV5BMTc5MTg2NjQ4MV5BMl5BanBnXkFtZTgwNzcxOTY5NjE@._V1_SX148_CR0,0,148,219_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc5MTg2NjQ4MV5BMl5BanBnXkFtZTgwNzcxOTY5NjE@._V1_SX148_CR0,0,148,219_AL_UY438_UX296_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">The holidays are upon us. That means it's the time of year to catch Oscar hopefuls and other big movies in theaters. 'Tis the season to get hooked on TV shows to keep you company through the winter, too. Our IMDb Picks section spotlights some of the movies and TV shows we're excited about this November, and we hope you'll find some favorites inside.</p> <p class="seemore"> <a href="/imdbpicks/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265484042&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265484042&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Visit the IMDb Picks section </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-8"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <h3>Indie Focus: Watch a Clip From New Film 'Trumbo'</h3> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt3203606/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265712162&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265712162&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Trumbo (2015)" alt="Trumbo (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjM1MDc2OTQ3NV5BMl5BanBnXkFtZTgwNzQ0NjQ1NjE@._V1_SX170_CR0,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjM1MDc2OTQ3NV5BMl5BanBnXkFtZTgwNzQ0NjQ1NjE@._V1_SX170_CR0,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi900641305?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265712162&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265712162&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi900641305" data-rid="1WEN9SK6MFY3MWSSPKCR" data-type="single" class="video-colorbox" data-refsuffix="hm_if" data-ref="hm_if_i_2"> <img itemprop="image" class="pri_image" title="Trumbo (2015)" alt="Trumbo (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjI4MzU2NzY0N15BMl5BanBnXkFtZTgwMTc0NTcxNzE@._V1_SX444_CR0,0,444,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI4MzU2NzY0N15BMl5BanBnXkFtZTgwMTc0NTcxNzE@._V1_SX444_CR0,0,444,250_AL_UY500_UX888_AL_.jpg" /> <img alt="Trumbo (2015)" title="Trumbo (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Trumbo (2015)" title="Trumbo (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb"><a href="/name/nm0186505/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265712162&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_lk1">Bryan Cranston</a> stars as blacklisted screenwriter Dalton Trumbo in the new film <a href="/title/tt3203606/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265712162&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_lk2"><i>Trumbo</i></a>. In this clip, Trumbo's friend Arlen Hird <a href="/name/nm0127373/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265712162&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_lk3"> (Louis C.K.)</a> points out the gap between Trumbo's radical beliefs and his lavish lifestyle.</p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-13"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','RecsWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','RecsWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-19"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TriviaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_trivia">
<span class="ninja_header"> <span class="oneline"> <a href="/title/tt2268016/trivia?item=tr2462088&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Did You Know?</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt2268016?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Magic Mike XXL (2015)" alt="Magic Mike XXL (2015)" src="http://ia.media-imdb.com/images/M/MV5BNDMyODU3ODk3Ml5BMl5BanBnXkFtZTgwNDc1ODkwNjE@._V1_SY132_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNDMyODU3ODk3Ml5BMl5BanBnXkFtZTgwNDc1ODkwNjE@._V1_SY132_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" > <strong class="text-large"><a href="/title/tt2268016?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Magic Mike XXL</a></strong> <p class="blurb">Cody Horn is not returning to reprise her role as Brooke.</p></div> </div> </div> <p class="seemore"> <a href="/title/tt2268016/trivia?item=tr2462088&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See more trivia </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','TriviaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-25"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','PollWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;znUStWxtXa0&quot;}">
            <div class="ab_poll poll">
<span class="ninja_header"> <span class="oneline"> <a href="/poll/znUStWxtXa0/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265486362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265486362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Poll: Who would you want as your TV Mom?</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265486362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk">More Polls</a></h4> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/znUStWxtXa0/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265486362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265486362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="THAT '70s SHOW: Kitty Forman (Debra Jo Rupp) on season eight of THAT '70s SHOW airing Wednesdays (8:00-8:30 PM ET/PT) on FOX." alt="THAT '70s SHOW: Kitty Forman (Debra Jo Rupp) on season eight of THAT '70s SHOW airing Wednesdays (8:00-8:30 PM ET/PT) on FOX." src="http://ia.media-imdb.com/images/M/MV5BMTE5OTgyMzU0MV5BMl5BanBnXkFtZTYwMzg3OTI3._V1_SY207_CR16,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTE5OTgyMzU0MV5BMl5BanBnXkFtZTYwMzg3OTI3._V1_SY207_CR16,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/znUStWxtXa0/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265486362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265486362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Patricia Heaton in The Middle (2009)" alt="Still of Patricia Heaton in The Middle (2009)" src="http://ia.media-imdb.com/images/M/MV5BMTI5OTE3NTg5M15BMl5BanBnXkFtZTcwOTU5NTY5Mg@@._V1_SY207_CR7,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTI5OTE3NTg5M15BMl5BanBnXkFtZTcwOTU5NTY5Mg@@._V1_SY207_CR7,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/znUStWxtXa0/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265486362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265486362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="&quot;Home Improvement&quot; Patricia Richardson" alt="&quot;Home Improvement&quot; Patricia Richardson" src="http://ia.media-imdb.com/images/M/MV5BMTA3MTUzMTgyNjJeQTJeQWpwZ15BbWU2MDgxNzMzNg@@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTA3MTUzMTgyNjJeQTJeQWpwZ15BbWU2MDgxNzMzNg@@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/znUStWxtXa0/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265486362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265486362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="That '70s Show (1998)" alt="That '70s Show (1998)" src="http://ia.media-imdb.com/images/M/MV5BMTcwNTQwODc4OF5BMl5BanBnXkFtZTgwNDEyNTU2MjE@._V1_SY207_CR68,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTcwNTQwODc4OF5BMl5BanBnXkFtZTgwNDEyNTU2MjE@._V1_SY207_CR68,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/znUStWxtXa0/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265486362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265486362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Lauren Graham in Gilmore Girls (2000)" alt="Still of Lauren Graham in Gilmore Girls (2000)" src="http://ia.media-imdb.com/images/M/MV5BMjExMTM0MjY0OV5BMl5BanBnXkFtZTYwNTY1OTA3._V1_SY207_CR85,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjExMTM0MjY0OV5BMl5BanBnXkFtZTYwNTY1OTA3._V1_SY207_CR85,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">Which of these TV moms would you choose to be your mom? Discuss <a href="http://www.imdb.com/board/bd0000088/nest/249481368?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265486362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk1">here</a> after voting.</p> <p class="seemore"> <a href="/poll/znUStWxtXa0/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265486362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2265486362&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Vote now </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','PollWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    
            </div>
            <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,300x600,11x1;p=tr;fv=1;ab=e;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=410009526473;ord=410009526473?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=e;bpx=2;c=0;s=3075;s=32;ord=410009526473?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=e;bpx=2;c=0;s=3075;s=32;ord=410009526473?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	

    
    
    

    
    
    

    
    
    
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	

    
    
        <a name="slot_right-4"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja showtimesMessagingWidget">
<p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2006955922&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-6"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BoxOfficeListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <a href="/chart/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Now Playing (Box Office)</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3659388"></div> <div class="title"> <a href="/title/tt3659388?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t0"> The Martian </a> <span class="secondary-text">$11.4M</span> </div> <div class="action"> <a href="/showtimes/title/tt3659388?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1051904"></div> <div class="title"> <a href="/title/tt1051904?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t1"> Goosebumps </a> <span class="secondary-text">$10.2M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3682448"></div> <div class="title"> <a href="/title/tt3682448?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t2"> Bridge of Spies </a> <span class="secondary-text">$8.1M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2510894"></div> <div class="title"> <a href="/title/tt2510894?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t3"> Hotel Transylvania 2 </a> <span class="secondary-text">$5.8M</span> </div> <div class="action"> <a href="/showtimes/title/tt2510894?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2503944"></div> <div class="title"> <a href="/title/tt2503944?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t4"> Burnt </a> <span class="secondary-text">$5.0M</span> </div> </div> </div> </div> </div> <div><a href="/chart/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more box office results</p></a></div>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','BoxOfficeListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-8"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Coming Soon</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3707106"></div> <div class="title"> <a href="/title/tt3707106?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t0"> By the Sea </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2006295"></div> <div class="title"> <a href="/title/tt2006295?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t1"> The 33 </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2279339"></div> <div class="title"> <a href="/title/tt2279339?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t2"> Love the Coopers </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3595298"></div> <div class="title"> <a href="/title/tt3595298?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t3"> Prem Ratan Dhan Payo </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4288636"></div> <div class="title"> <a href="/title/tt4288636?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t4"> James White </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <div><a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more coming soon</p></a></div>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-12"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TwitterWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_twitter">
<span class="widget_header"> <h3>Follow Us On Twitter</h3> </span> <div class="widget_content"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3776329187._CB305760180_.html#{"screen-name":"IMDb","height":"500px","width":"300px"}'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','TwitterWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-14"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','FacebookWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_facebook">
<span class="widget_header"> <h3>Find Us On Facebook</h3> </span> <div class="widget_content"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','FacebookWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-30"></a>
        <div class="aux-content-widget-2 sticky-widget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="http://www.imdb.com/video/imdb/vi2642129689/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261200402&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_bnd_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261200402&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <h3>10 Bond Trivia Facts</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2642129689?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261200402&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_bnd_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261200402&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi2642129689" data-rid="1WEN9SK6MFY3MWSSPKCR" data-type="single" class="video-colorbox" data-refsuffix="hm_bnd" data-ref="hm_bnd_i_1"> <img itemprop="image" class="pri_image" title="Spectre (2015)" alt="Spectre (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTc0Nzg5NTQ4NV5BMl5BanBnXkFtZTgwODc4NzI0NDE@._V1_SX700_CR0,0,700,393_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc0Nzg5NTQ4NV5BMl5BanBnXkFtZTgwODc4NzI0NDE@._V1_SX700_CR0,0,700,393_AL_UY786_UX1400_AL_.jpg" /> <img alt="Spectre (2015)" title="Spectre (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Spectre (2015)" title="Spectre (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">Count down 10 things only real James Bond fans know about James Bond.</p> <p class="seemore"> <a href="http://www.imdb.com/video/imdb/vi2642129689/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261200402&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_bnd_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2261200402&pf_rd_r=1WEN9SK6MFY3MWSSPKCR&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Watch now </a> </p>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    
            </div>
        </div>
        

    
    
    

    
    
    
        
    
        <br class="clear" />
    </div>
</div>
                





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYm5oXrBp0GlJEDu6G7T1aISIjHHkJLqSeFrl0HILbX76dGSK1NwdqPSXwUCsnE6N3QnhRmO9nw%0D%0AUlMzmazZTMSe0WIA0fzzPhz1ZoJCjx_yBPZKtpO1aL3yZ07YTw3qbwejayEGgc6Y5bJXyMbPH5Z7%0D%0A3lxUqmj_ba3vI0m434XdQIy5Iq6nyJ-tgFl3BQRZbFxLvM0vQDudkV9kFr_C5wAdWFlelLxEJnuo%0D%0AE0x4mEP1kOc%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYsUjlM7OVQHCNmAknMGvBO94zAQy7NTYEXDWP6AlnWBZGOIBG1qNkNtybz2cQRw9CcQ4AWDMB1%0D%0ADWeRtZE1AuOHX0Z7CbGTcWoPSbERNWBhtgsprL1DvDIIYE8kFf4yUL3H0OjxvRNcUaG5Vexgs5w2%0D%0AfBc_pgWjnifXZ0eZeMyMc5iTxYEQphdREuw81EOeY1IS2t28-svJMuDvyiUADfg82w%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYr_q2kSibgQxFKlO-lP9xvQjCeyy01OsZRHtCbuUf8ELbKRI5sBSVM39BZuKY05IpoEbJxWdTY%0D%0A-tDUZrwUoNjVKaEaDANGMhbCrZ1wJn5BBnriKc3HChLZVI08WV35EjjrheyjXgOHtkzej4RK6Mc6%0D%0AVNKo3RerX1D-5RJjo5yBGKRD_MbwZvv153r8X3r3xuBQau1jLRG-XL7PYMhttlye0t58VnGdZFLY%0D%0ATno_i_ymm_TLFkmKYuwBGvkGs58iR8Wv8YLNNGOELCBaCSptfShH5pXljgg_GN00kV9azI2VzjD6%0D%0AXtvG4CNfdXnoJX1aYN9IZj1COpBQcMY64_WJIKJDc-jGUb_2QPu65E1fejXgrw2DfMgM9dWuFZc8%0D%0AUJRi7sweHfghAJqpmr_nMkYKqFeJCw%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYq4ELZVnIaC6zZvM6C1YTTFWZS6BxxhNLJVQQ05vY-XlAZIgZrBVfdvoNS0waxx_dcO3l00EGI%0D%0AnTndqAiKQpn_e_9QOQjG7cDXpdZY5H_gUQYLBXls2R8Ak8JJMge0EEWPVPg6W8jJepOiE_27pgRN%0D%0AjtkzyhO14othAc8-fdCOuOLCoidkn8gwsbM6eqP65kM5PLBOSimqN9NpD-aJQDbxww%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYng8N_KCmGDcls0IYJIxjb-kQek12llMmUvikZDTjHLsYEFUpVQ-QDZtRXr6OfrW99ss7Bturp%0D%0Asap5EhCUeTqj18jEOdi8iTpF1Y6yvszH7fwB2bWf1oyrCxJ3mybUOZwO12DL5gOAN6O4oERNcMNC%0D%0A2uTbrj4q6x1alpbsaHUpbXwDuH4cXAqgnEkx-NzuBxsm%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYtnf0f57mkhs784i97V9-J4x7-KxOSPRtRuOQS-1OB7S1iYqt_RtwT_7c3GoiVp4XofTtM3QCh%0D%0A1rp2JBxtvXI30g75VXZYbkhF39ScPIgpD85ccH9jhJ7LSnDAWtz0lBWPT4hRCGbThfQhuqiu1eDC%0D%0AAKKV-0kzXEY8Y55TW_MciSayvzj7voaLxDDYCJ7RqUFxdyXjdYqCGd5vY3BnUbzWmQ%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top 250</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Top Movies</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li>    <a href="/offsite/?page-action=ft_ad&token=BCYhQBhyL5s--y8yE9-Exw4rVZSZ1ATdO0Fs524PDyCkZoYkJdkOV7xFxkgbcyJiSap1djbYd2uZ%0D%0A0QJNkC2EYXG6iO6kMZ-4PWlsirfHnKDaBI2tt2tORzMF03XW7J2Thbd6DcIaMs1mu3k2T9361ib6%0D%0AhK4jcEkiLbIn0bZ5u7y--J9prdgR4f75TIK9CGvCH1JMJ5gnH797CGiitry46pDIRkmtl26C346L%0D%0AKaK-4yVHYCdWAsjuvZ-bYoTqIjup49h1F4NyWV4IxZr2mYFrh97xhQ%0D%0A&ref_=ft_ad"
itemprop='url'>Advertising</a>
</li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYqGzEpQsKnthWtlqiZlXVnJvNDdLxgIhsQCeF18PRzI-DcMsBcDHEBJF_WfdCmWlLDBvRkMdSb%0D%0A2cTinTIdn5THAVVBskrjo0PB7cVoO7tBWmhMjL2hYVtSkYNSx4aY8ehJU-l4Q8x7suoh7UmwmhXs%0D%0A5jyuFf4hjh__NgYAz6Bo9k_8njNbsQ7K6uD5Ir-nVRnuEDx8YlWPfb0Kx3e8FptiaQ%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYvN0CJ7kgo5enznPKdqfctKKnMaDAkVJz0q4AwBtgibbI2qKNDK_tBb9EcSnQ5hH9tPEeNmWAW%0D%0AG6PEBSXbdcHMulzzrTWr6NWy_1dxvv1IzaK0OxKN_H-K5QYa-MrzN0H_c5fsWdR6BK-D0OLUNmzy%0D%0ASUADPfnYFK96fE_QhDrhT9bByWvJEt9X41QdrL9EYQdx%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYjJZSOCKjJ0wkQQX-HYlkckg85cDDRvio_As-Tyb1ktvTjlmau7d2LZvuuOHyFe1L6UxBeKfUZ%0D%0A0Ofad0IYW8Pve3NP7U6L3YZweILyV0vg-5pDv0Em5tIIvWX90E7-tYb_ZjZHUpOLNSatdd8MLC8O%0D%0Axn36L3SZ0X35cpgdlPdA0-qrV88lC82tvvXmwOWncWjM_tAUMFMa80vp3kSjVEKT9t22H6ZqQW24%0D%0AQGhnfoh1jRTo_VdY8XrQXW73DHiW7IQi%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/show_article?conditions';"
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2015
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/';"
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYntaY52J3cd0V3rjz_KWGFP8jKPD7i0_HZyegigzoH-c-RnErX6Q-UvEjfZCfr0CK5A-XWJKQj%0D%0ACZh0Z1eVuklpv6iLOtdT2KHLpSqYwGj6-6ePgHTHap9FfkoX4OYY3drH_t1hVqFlI7dqWR5SF_nl%0D%0Aum72m4b1NZUjk_QHdJ1bgRixMNsMVJmg-HsYP6Omw_Zv46zkE6QGECHXQMLdT2rorlHuePYSkl80%0D%0AajGT9yrYzdU%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYn8Lyk-c5A40CvZskQhxD7Aa-uC2aqlJ7xabiskVqAImyOs4Lfjo-Q7v3rnl_fMX_oPeQR1lVf%0D%0AVkP_N5Si0yMO5aSbI-lYkuDiigomF4Dg6V_jxD6Hullamat2UMe2t88kv5t-swxE4TEzSuEqzgKG%0D%0ARCQArwhW5vTD0ug8e-gtSGB82r5-Czi25D1uUXgHb4i_2UZRsnbcznUH1tfLCo9AstfXUQTR_9Vr%0D%0A3NEIZyrDfEg%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYmsdr9rCTFcQS-rmmucXoYjuM6fnlkvDf0849FjexDjbvWHQPnvtkfMXmYpbrx5SRWOGpI5nkJ%0D%0AcqaFTEWriMpvD1Q9Qx7hY9cWCQdNOHU9RlqyAT4YaYmMv_gl1fb160-TqOqAKbIKKsAH3G6QKlsf%0D%0A_C_QRraNNA6lhO7dbXcpvjqBLz2Ts6Jo0SDK8-_LRN4K0oHJ7hqGUCgaKwBAvwSFxFJERP1RDv93%0D%0A9Q_Wy4Umxao%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYmGpqwFKtBK185iQ_POS1YjF8CV9Y715ti2Bfg1IX0xVxc8nSoUM05gRP57GfwHW-eRXqV-qww%0D%0AfTPL2_5CV77hdx2LqKe_eP7Ms1Wr9hGUeLfE3mMcpBJqdmPU-GqQC5CJSlrWfCplcEH1A0n3EPAA%0D%0AUB9DMTljaPb39r4nEvi1iuLImpROyQ1Mdrd38SiAFmjX0ynm1SQU-TJru2BUTZIsDjcQJlAcUFyO%0D%0ArF3Td7mEHDk%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYtcBAhocrYo2dRAc2fkZEVz7kEWUEWL5i9uxK2ztt40IiMAMrTZW9wE46_u9wLQwyMbySSAzqU%0D%0Ag7xionwoXciMnMOogNsH8H-yUOqgbx9wkbKUAWQ9VPrYGlKjkr6z70Hzx4IApVzXENFY_wfYh-xF%0D%0AX3JyWv7pg8gNpm2n0kQTosHaZzJNtr1ZztRsJNfqy8GfZVtcs_KUIn6yQZNOVLIklcM5I8fN9H6b%0D%0A6RfBOMBRsrk%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYvZnd2YQBLN6fj39t0gvBwhszl59Mh6ZcX43xZJp_ZBtIhITTVrFTxaPqxS4To5xuaT8yDA6Ij%0D%0AaKtEfILjVHp2gkzo6Ont2Mz1kIW3X849B4Er7AspZD_mHBBNuptec3fdYN77bWDcWz5lGuLi9SVw%0D%0AMHM4anVwRsQU0wuW_RdnNLB_OuGkoU2xPsxkGlPCzYh-TQjtXcx_G0im6F4JVbwsAEQcMgCE5vTD%0D%0AOxymN5rHZ-MIWb4MfSar5jrDbBZPggH4%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYpdTpWpZ2ha4VBko7-93NWgxcBP1tQ4yrdymTx8kbETH73XYqmB9VW2HgNUlPu3gSwLYwcemuj%0D%0AinVFlpky7XCGbneK6E-8K9SfsicgVrHzoouR-5memWYUOtf8y_kFGQHhqO1TqK2KmYQ2gcHmR9uo%0D%0AcsL_lX-5ySfH9RAMXbCLZXY%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYk5Rm39pFWDJgoz-3TbXmLSYs-L-F_swXkWGQM6A5S5UktXSlN_cJQs4GR_ArlIboYo_uiew3L%0D%0Aiub-NVPmnIt2XuJkfV1xrGAUKwcQHCoK30GMYzkr5cojmLREw6Ijl8Z7CuYppIoOztNpygvgRFLe%0D%0A9Kugm5gZbbWqLrruMroAqLU%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/common-1062213378._CB290996593_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-1776171563._CB291276595_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-1238345358._CB290967079_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-3920146857._CB292796746_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-1294823147._CB290767939_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=0101007a1658f9b6069a25eed2448efcb2a44df387638e649978830f2d42d5ac9d35",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=410009526473"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-232398347._CB349580400_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=410009526473&ord=410009526473";
    },"unable to request AAN pixel");
</script>

<div id="flashContent" style="width: 0px; height: 0px; overflow:hidden;">
    <script type="text/javascript">
        if (generic && generic.monitoring.record_metric && flashAdUtils.canPlayFlash) {
            var flashLoaded = 0;
            function swfLoaded() {
                flashLoaded = 1;
                generic.monitoring.record_metric('ads_flash_did_play', 1, true);
            }
            window.onload = function() {
                // Flash was not loaded
                if (flashLoaded === 0) {
                    // Flash could be loaded but wasn't
                    if (flashAdUtils.canPlayFlash()) {
                        generic.monitoring.record_metric('ads_flash_can_but_did_not_play', 0, true);
                    } else {
                        generic.monitoring.record_metric('ads_flash_cannot_play', 0, true);
                    }
                }
            }
            /**
             * There is a rare possibility of window.onload getting called before swfLoaded.
             * Firing this metric to keep track of such calls.
             */
             generic.monitoring.record_metric('ads_flash_page_loaded', 1, true);
         }
    </script>
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="1x1" align="middle">
        <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
        <param name="quality" value="high" />
        <param name="play" value="true" />
        <param name="loop" value="true" />
        <param name="wmode" value="transparent" />
        <param name="scale" value="showall" />
        <param name="menu" value="true" />
        <param name="devicefont" value="false" />
        <param name="salign" value="" />
        <param name="allowScriptAccess" value="always" />

        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" width="1" height="1">
            <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
            <param name="quality" value="high" />
            <param name="play" value="true" />
            <param name="loop" value="true" />
            <param name="wmode" value="transparent" />
            <param name="scale" value="showall" />
            <param name="menu" value="true" />
            <param name="devicefont" value="false" />
            <param name="salign" value="" />
            <param name="allowScriptAccess" value="always" />
        </object>
        <!--<![endif]-->
    </object>
</div>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="377"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
