



<!DOCTYPE html>

<!--[if IE]><![endif]--><html><head><meta charset="utf-8" /><meta http-equiv="Content-Type" content="text/html;charset=UTF-8" /><title>globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento</title><meta name="google-site-verification" content="BKmmuVQac1JM6sKlj3IoXQvffyIRJvJfbicMouA2a88" /><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.5" /><meta property="og:title" content="globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento"/><meta property="og:type" content="website"/><meta property="og:url" content="http://www.globo.com/"/><meta property="og:image" content="http://s.glbimg.com/en/ho/static/globocom2012/img/fb_marca.png"/><meta property="og:site_name" content="globo.com"/><meta property="og:description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta property="fb:page_id" content="224969370851736" /><meta property='busca:title' content='Globo.com' /><meta property='busca:species' content='Home' /><meta property='busca:issued' content='07/08/2015 22:05:26' /><meta property='busca:modified' content='07/08/2015 22:05:26' /><meta property='busca:publisher' content='www.globo.com' /><meta name="description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta name="keywords" content="NotÃ­cias, Entretenimento, Esporte, Tecnologia, Portal, ConteÃºdo, Rede Globo, TV Globo, VÃ­deos, TelevisÃ£o"/><meta name="application-name" content="Globo.com"/><meta name="msapplication-TileColor" content="#0669DE"/><meta name="msapplication-TileImage" content="http://s.glbimg.com/en/ho/static/globocom2012/img/globo-win-tile.png"/><link rel="canonical" href="http://www.globo.com/" /><link rel="shortcut icon" href="http://s.glbimg.com/en/ho/static/globocom2012/img/favicon.png" type="image/ico" /><link rel="apple-touch-icon-precomposed" sizes="57x57" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone.png" /><link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-ipad.png" /><link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone-retina.png" /><link rel="stylesheet" href="http://s.glbimg.com/en/ho/static/CACHE/css/3bf94536106e.css" type="text/css" media="screen" /><!--[if lt IE 10]><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/d783f8b1c48f.js"></script><![endif]--><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/12ba0da8a716.js"></script></head><!--[if lt IE 7 ]><body class="ie ie6"><![endif]--><!--[if IE 7 ]><body class="ie ie7"><![endif]--><!--[if IE 8 ]><body class="ie ie8"><![endif]--><!--[if IE 9 ]><body class="ie ie9gt ie9"><![endif]--><!--[if gt IE 9 ]><body class="ie ie9gt ie10gt"><![endif]--><!--[if !IE]><!--><body><!--<![endif]--><div id="home-push-menu"><div class="home-push-menu-container"><div class="home-push-menu-initial"><div class="home-push-menu-header">NAVEGUE</div><ul class="barra-itens" data-analytics-context="Link Produto"><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://g1.globo.com" accesskey="n" class="barra-item-g1 link-produto analytics-area analytics-id-T">g1</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://globoesporte.globo.com" accesskey="e" class="barra-item-globoesporte link-produto analytics-area analytics-id-T">globoesporte</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://gshow.globo.com" accesskey="t" class="barra-item-gshow link-produto analytics-area analytics-id-T">gshow</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://famosos.globo.com" accesskey="t" class="barra-item-famosos-etc link-produto analytics-area analytics-id-T">famosos &amp; etc</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://techtudo.com.br" accesskey="b" class="barra-item-tech link-produto analytics-area analytics-id-T">tecnologia</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://video.globo.com" accesskey="v" class="barra-item-videos link-produto analytics-area analytics-id-T">vÃ­deos</a></li><li><a href="#" id="home-push-menu-show-email">e-mail</a><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="2.423,0 0,2.399 5.153,7.5 0,12.602 2.423,15 10,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-right.png"></image></svg></span></li><li><a href="https://meuperfil.globo.com/">central globo.com</a></li></ul></div><div class="home-push-menu-email"><ul><li><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="7.577,0 10,2.399 4.847,7.5 10,12.602 7.577,15 0,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-left.png"></image></svg></span><a id="home-push-menu-hide-email" href="#">e-mail</a></li><li><a href="https://login.globo.com/login/1948">globomail free</a></li><li><a href="https://login.globo.com/login/1">globomail pro</a></li></ul></div></div></div><div id="home-pagecontent" class=" home-menu-rendered" style=""><script type="text/javascript">
function comScore(C){var A="comScore",j=document,y=j.location,B="",z="undefined",x=2048,D,v,i,w,c="characterSet",l="defaultCharset",u=(typeof encodeURIComponent!=z?encodeURIComponent:escape);if(j.cookie.indexOf(A+"=")!=-1){i=j.cookie.split(";");for(w=0,y=i.length;w<y;w++){var d=i[w].indexOf(A+"=");if(d!=-1){B="&"+unescape(i[w].substring(d+A.length+1))}}}C=C+"&ns__t="+(new Date().getTime());C=C+"&ns_c="+(j[c]?j[c]:(j[l]?j[l]:""))+"&c8="+u(j.title)+B+"&c7="+u(y&&y.href?y.href:j.URL)+"&c9="+u(j.referrer);if(C.length>x&&C.indexOf("&")>0){D=C.substr(0,x-8).lastIndexOf("&");C=(C.substring(0,D)+"&ns_cut="+u(C.substring(D+1))).substr(0,x)}if(j.images){v=new Image();if(typeof ns_p==z){ns_p=v}v.src=C}else{j.write('<p><img src="'+C+'" height="1" width="1" alt="*"/></p>')}}comScore("http"+(document.location.href.charAt(4)=="s"?"s://sb":"://b")+".scorecardresearch.com/p?c1=2&c2=6035227");</script><noscript><p><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6035227&amp;c4=http%3A%2F%2Fwww.globo.com%2Findex.html" height="1" width="1" alt="*"/></p></noscript><script>
libby.loadScript('http://b.scorecardresearch.com/c2/6035227/cs.js');</script><script type="text/javascript">
var utag_data = {"structure_tree": "[\"globocom\"]", "ad_site_page": "globo.com/globo.com/home", "page_name": "index"};</script><script type="text/javascript">
(function (a,b,c,d) {a = '//tags.globo.com/utag/globo/home/prod/utag.js';b=document;c = 'script';d=b.createElement(c);d.src=a;d.type = 'text/java' + c;d.async=true;a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);})();
</script><header><div id="base-container-width-element" class="container"><div class="header-mobile-top analytics-area analytics-id-H"><a id="open-menu" class="open-menu" href="#"><svg version="1.1" class="burger-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19px" height="13px" viewBox="0 0 19 13" enable-background="new 0 0 19 13" xml:space="preserve"><g><path fill="#1063E0" d="M17.493,5H1.485C0.665,5,0,5.665,0,6.485C0,7.305,0.665,7.97,1.485,7.97h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,5.665,18.313,5,17.493,5z M1.485,2.971h16.008c0.82,0,1.486-0.666,1.486-1.486C18.979,0.666,18.313,0,17.493,0H1.485C0.665,0,0,0.666,0,1.484C0,2.305,0.665,2.971,1.485,2.971z M17.493,10H1.485C0.665,10,0,10.666,0,11.485c0,0.82,0.665,1.485,1.485,1.485h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,10.666,18.313,10,17.493,10z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/menu-button.png"></image></svg></a><a class="logo-globo analytics-area analytics-id-T" href="http://globo.com" title="globo.com"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 98 22" enable-background="new 0 0 98 22" xml:space="preserve"><g><path fill="#1063E0" d="M92.982,5.033c-1.115,0-2.549,0.601-3.459,1.625c-0.865-1.069-2.026-1.625-3.392-1.625c-1.205,0-2.252,0.489-3.049,1.336c-0.114-0.757-0.661-1.203-1.502-1.203c-0.957,0-1.639,0.712-1.639,1.781v8.551c0,1.069,0.682,1.781,1.639,1.781s1.638-0.712,1.638-1.781V9.909c0-1.292,0.592-2.205,1.936-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.682,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.909c0-1.292,0.728-2.205,1.934-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.683,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.352C97.466,6.547,95.645,5.033,92.982,5.033z M10.036,5.166c-0.91,0-1.434,0.49-1.639,1.336c-0.614-0.98-1.889-1.469-3.004-1.469C2.093,5.033,0,8.039,0,11.268c0,3.184,1.616,6.146,5.052,6.146c1.502,0,2.617-0.535,3.299-1.515h0.045v0.757c0,1.625-1.183,2.538-2.776,2.538c-1.889,0-3.049-1.001-4.097-1.001c-0.637,0-1.319,0.8-1.319,1.403C0.205,21.354,4.028,22,5.371,22c3.732,0,6.304-1.938,6.304-5.791V6.947C11.674,5.878,10.991,5.166,10.036,5.166z M5.826,14.474c-1.707,0-2.549-1.559-2.549-3.273c0-1.492,0.842-3.229,2.549-3.229c1.797,0,2.571,1.781,2.571,3.339C8.397,12.871,7.578,14.474,5.826,14.474z M14.318,0c-0.956,0-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781s1.639-0.712,1.639-1.781V1.782C15.957,0.712,15.274,0,14.318,0z M22.479,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169c3.527,0,5.872-2.829,5.872-6.169C28.35,7.994,25.938,5.033,22.479,5.033z M22.479,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.797-3.273,2.594-3.273c1.798,0,2.594,1.737,2.594,3.273C25.073,12.782,24.254,14.474,22.479,14.474z M35.371,5.033c-1.138,0-2.253,0.378-3.118,1.113V1.782C32.253,0.712,31.57,0,30.614,0s-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781c0.911,0,1.434-0.49,1.639-1.336c0.614,0.979,1.889,1.471,3.004,1.471c3.3,0,5.393-3.008,5.393-6.036C40.65,8.261,38.897,5.033,35.371,5.033z M34.825,14.474c-1.798,0-2.572-1.781-2.572-3.34c0-1.559,0.819-3.162,2.572-3.162c1.707,0,2.548,1.759,2.548,3.273C37.373,12.737,36.531,14.474,34.825,14.474z M46.863,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169s5.872-2.829,5.872-6.169C52.735,7.994,50.322,5.033,46.863,5.033z M46.863,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273s2.594,1.737,2.594,3.273C49.458,12.782,48.638,14.474,46.863,14.474z M55.425,13.939c-0.978,0-1.774,0.779-1.774,1.736s0.796,1.738,1.774,1.738c0.979,0,1.775-0.781,1.775-1.738S56.404,13.939,55.425,13.939z M66.583,13.805c-0.386,0-1.616,0.669-2.436,0.669c-1.774,0-2.753-1.648-2.753-3.229c0-1.648,0.888-3.273,2.753-3.273c1.048,0,1.776,0.623,2.594,0.623c0.775,0,1.253-0.868,1.253-1.536c0-1.56-2.709-2.026-4.006-2.026c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.343,6.169,5.871,6.169c1.184,0,4.006-0.558,4.006-2.206C67.994,14.451,67.355,13.805,66.583,13.805z M73.446,5.033c-3.459,0-5.872,2.961-5.872,6.212c0,3.34,2.344,6.169,5.872,6.169c3.527,0,5.871-2.829,5.871-6.169C79.317,7.994,76.904,5.033,73.446,5.033z M73.446,14.474c-1.774,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273c1.797,0,2.594,1.737,2.594,3.273C76.04,12.782,75.221,14.474,73.446,14.474z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/logo.png"></image></svg></a><form id="search-form" action="http://www.globo.com/busca/" method="get"><div id="search-container" class="search-container"><span><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="12px" height="12px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><g><path fill="#999999" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/search-small.png"></image></svg></span><div class="search-internal-container"><input type="text" id="search-globo" name="q" placeholder="encontre na globo.com"></div><a id="close-search" class="close-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="11px" height="11px" viewBox="0 0 11 11" enable-background="new 0 0 11 11" xml:space="preserve"><g><path fill="#666766" d="M6.884,5.501c1.955-1.955,3.733-3.732,3.809-3.808c0.388-0.388,0.388-1.015,0-1.403 c-0.387-0.386-1.015-0.386-1.402,0C9.215,0.365,7.437,2.144,5.482,4.099c-1.947-1.947-3.714-3.714-3.79-3.79 c-0.387-0.388-1.015-0.388-1.402,0c-0.387,0.387-0.387,1.016,0,1.401c0.076,0.076,1.842,1.843,3.79,3.791 C2.135,7.446,0.372,9.209,0.297,9.285c-0.387,0.387-0.387,1.015,0,1.401c0.387,0.389,1.015,0.389,1.402,0 c0.075-0.075,1.838-1.838,3.784-3.783c1.953,1.952,3.727,3.727,3.802,3.802c0.387,0.387,1.015,0.387,1.402,0 c0.387-0.387,0.387-1.015,0-1.402C10.611,9.228,8.837,7.454,6.884,5.501z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/close.png"></image></svg></a></div><a id="open-search" class="open-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><path fill="#1063e0" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path><image src="http://s.glbimg.com/gl/ba/img/common/search.png"></image></svg></a></form></div><h1 class="analytics-area analytics-id-H"><a class="logo-topo analytics-area analytics-id-T" href="/" title="globo.com">
                globo.com
            </a></h1><div class="complementos analytics-area analytics-id-T"><div id="busca-padrao"><form action="http://www.globo.com/busca/" method="get" accept-charset="utf-8"><fieldset><legend>buscar</legend><label for="busca-campo" class="label-for-search">buscar</label><input id="busca-campo" type="text" name="q" autocomplete="off" accesskey="s" lang="pt-BR" x-webkit-speech speech /><button type="submit">buscar</button></fieldset></form></div><div id="libby-box-previsao-tempo" class="analytics-area analytics-id-L"></div></div><nav><div class="spacer"></div><script type='text/javascript'> window.SETTINGS=window.SETTINGS || {};window.SETTINGS.optimizelyId = "2223881511";window.OptimizelyAbCallback.includeABScript(SETTINGS.optimizelyId); </script>



<ul id="home-menu" class="nested widget-menu">
    
        <li class="g1 analytics-product analytics-multi-product">
            <a href="http://g1.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">g1</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://g1.globo.com/" data-menu-id="g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">g1</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="jornais-revistas-e-radio">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">jornais, revistas e rÃ¡dio</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="servicos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">serviÃ§os</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-g1">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-g1" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/economia/">
                                                <span class="titulo">Economia</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/carros/">
                                                <span class="titulo">Carros</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/">
                                                <span class="titulo">G1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/politica/">
                                                <span class="titulo">PolÃ­tica</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/jornal-hoje/">
                                                <span class="titulo">Jornal Hoje</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/mundo/">
                                                <span class="titulo">Mundo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/concursos-e-emprego/">
                                                <span class="titulo">Concursos e Empregos</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/">
                                                <span class="titulo">Jornal Extra</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/">
                                                <span class="titulo">Jornal O Globo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/jornal-nacional/">
                                                <span class="titulo">Jornal Nacional</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="g1">
                                        <div class="submenu-title">g1</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Carros</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/concursos-e-emprego/">
                                                    <span class="titulo">Concursos e Empregos</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/">
                                                    <span class="titulo">Economia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/educacao/">
                                                    <span class="titulo">EducaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/imposto-de-renda/2015/index.html">
                                                    <span class="titulo">Imposto de Renda 2015</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/mundo/">
                                                    <span class="titulo">Mundo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/planeta-bizarro/">
                                                    <span class="titulo">Planeta Bizarro</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/politica/">
                                                    <span class="titulo">PolÃ­tica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/">
                                                    <span class="titulo">Pop &amp; Arte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/tecnologia/">
                                                    <span class="titulo">Tecnologia &amp; Games</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bom-dia-brasil/">
                                                    <span class="titulo">Bom dia Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://fantastico.globo.com/">
                                                    <span class="titulo">FantÃ¡stico</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/agronegocios/">
                                                    <span class="titulo">Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/globo-news/">
                                                    <span class="titulo">GloboNews</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/hora1">
                                                    <span class="titulo">Hora 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-da-globo/">
                                                    <span class="titulo">Jornal da Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-hoje/">
                                                    <span class="titulo">Jornal Hoje</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-nacional/">
                                                    <span class="titulo">Jornal Nacional</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="jornais-revistas-e-radio">
                                        <div class="submenu-title">jornais, revistas e rÃ¡dio</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/">
                                                    <span class="titulo">Jornal Extra</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/">
                                                    <span class="titulo">Jornal O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://cbn.globoradio.globo.com/home/HOME.htm">
                                                    <span class="titulo">RÃ¡dio CBN</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaautoesporte.globo.com/">
                                                    <span class="titulo">Revista Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaepoca.globo.com/">
                                                    <span class="titulo">Revista Ãpoca</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagalileu.globo.com/">
                                                    <span class="titulo">Revista Galileu</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagloborural.globo.com/">
                                                    <span class="titulo">Revista Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radioglobo.globoradio.globo.com/">
                                                    <span class="titulo">RÃ¡dio Globo</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="servicos">
                                        <div class="submenu-title">serviÃ§os</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/agenda.html">
                                                    <span class="titulo">Agenda de Shows</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/">
                                                    <span class="titulo">Conversor de Moedas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">
                                                    <span class="titulo">CotaÃ§Ãµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/">
                                                    <span class="titulo">Enem e Vestibular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/loteria/">
                                                    <span class="titulo">Loterias</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/previsao-do-tempo.html">
                                                    <span class="titulo">PrevisÃ£o do tempo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/tabela-fipe/index.html">
                                                    <span class="titulo">Tabela FIPE</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/telecurso/">
                                                    <span class="titulo">Telecurso</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/radar-g1/platb/">
                                                    <span class="titulo">TrÃ¢nsito</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.techtudo.com.br/velocimetro.html">
                                                    <span class="titulo">VelocÃ­metro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-g1"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="globoesporte analytics-product analytics-multi-product">
            <a href="http://globoesporte.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">globoesporte</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-globoesporte">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/" data-menu-id="globoesportecom">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globoesporte.com</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://sportv.globo.com/" data-menu-id="sportv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sportv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/futebol/" data-menu-id="futebol">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">futebol</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="mais-esportes">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mais esportes</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-globoesporte">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-globoesporte" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie B</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/cartola-fc/">
                                                <span class="titulo">Cartola FC</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                <span class="titulo">Futebol Internacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                <span class="titulo">FÃ³rmula 1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/">
                                                <span class="titulo">Globo Esporte</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://sportv.globo.com/site/combate/">
                                                <span class="titulo">MMA</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/">
                                                <span class="titulo">Futebol Nacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/volei/">
                                                <span class="titulo">VÃ´lei</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/tenis/">
                                                <span class="titulo">TÃªnis</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="globoesportecom">
                                        <div class="submenu-title">globoesporte.com</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://futpedia.globo.com/">
                                                    <span class="titulo">FutpÃ©dia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/cartola-fc/">
                                                    <span class="titulo">Cartola FC</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="sportv">
                                        <div class="submenu-title">sportv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/bolao/">
                                                    <span class="titulo">BolÃ£o</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/eventos/combate/">
                                                    <span class="titulo">Combate</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/corujao-do-esporte/">
                                                    <span class="titulo">CorujÃ£o do Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/esporte-espetacular/">
                                                    <span class="titulo">Esporte Espetacular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/">
                                                    <span class="titulo">Globo Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://app.globoesporte.globo.com/tv/planeta-extremo/">
                                                    <span class="titulo">Planeta Extremo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sociopremiere.globo.com/">
                                                    <span class="titulo">Premiere</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/">
                                                    <span class="titulo">SporTV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/lutas/tuf-brasil/">
                                                    <span class="titulo">The Ultimate Fighter Brasil</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="futebol">
                                        <div class="submenu-title">futebol</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie B</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-c/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie C</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-america/">
                                                    <span class="titulo">Copa AmÃ©rica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-do-brasil/">
                                                    <span class="titulo">Copa do Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                    <span class="titulo">Futebol Internacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol Nacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">
                                                    <span class="titulo">Liga dos CampeÃµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/selecao-brasileira/">
                                                    <span class="titulo">SeleÃ§Ã£o Brasileira</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="mais-esportes">
                                        <div class="submenu-title">mais esportes</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/atletismo/">
                                                    <span class="titulo">Atletismo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/basquete/">
                                                    <span class="titulo">Basquete</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eu-atleta/">
                                                    <span class="titulo">Eu Atleta</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                    <span class="titulo">FÃ³rmula 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eventos/futsal/">
                                                    <span class="titulo">Futsal</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/combate/">
                                                    <span class="titulo">MMA</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/natacao/">
                                                    <span class="titulo">NataÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/stock-car/">
                                                    <span class="titulo">StockCar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/tenis/">
                                                    <span class="titulo">TÃªnis</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/volei/">
                                                    <span class="titulo">VÃ´lei</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-globoesporte"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="gshow analytics-product analytics-multi-product">
            <a href="http://gshow.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">gshow</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/" data-menu-id="gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gshow</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="programas">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">programas</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="series">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sÃ©ries</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/programas/webseries/" data-menu-id="webseries">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">websÃ©ries</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com/" data-menu-id="rede-globo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">rede globo</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-gshow">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-gshow" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/mais-voce/">
                                                <span class="titulo">Mais VocÃª</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                <span class="titulo">VÃ­deo Show</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                <span class="titulo">Encontro com FÃ¡tima</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/">
                                                <span class="titulo">Gshow</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://redeglobo.globo.com/programacao.html">
                                                <span class="titulo">ProgramaÃ§Ã£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/programa-do-jo/">
                                                <span class="titulo">Programa do JÃ´</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://redeglobo.globo.com/">
                                                <span class="titulo">Rede globo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/receitas/">
                                                <span class="titulo">Receitas.com</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                <span class="titulo">CaldeirÃ£o do Huck</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="gshow">
                                        <div class="submenu-title">gshow</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Bastidores/">
                                                    <span class="titulo">Bastidores</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/como-fazer/">
                                                    <span class="titulo">Como Fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo">
                                                    <span class="titulo">Estilo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/especial-blog/gshow-troll/1.html">
                                                    <span class="titulo">Gshow Troll</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/participe/">
                                                    <span class="titulo">Participe</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas-da-Ana-Maria/">
                                                    <span class="titulo">Receitas da Ana Maria</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/tv/plantao/">
                                                    <span class="titulo">TV</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="programas">
                                        <div class="submenu-title">programas</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://gshow.globo.com/programas/altas-horas/">
                                                    <span class="titulo">Altas Horas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                    <span class="titulo">CaldeirÃ£o do Huck</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                    <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/e-de-casa/">
                                                    <span class="titulo">Ã de Casa</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                    <span class="titulo">Encontro com FÃ¡tima</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/esquenta/">
                                                    <span class="titulo">Esquenta</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/estrelas/index.html">
                                                    <span class="titulo">Estrelas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/">
                                                    <span class="titulo">Mais VocÃª</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/programa-do-jo/">
                                                    <span class="titulo">Programa do JÃ´</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                    <span class="titulo">VÃ­deo Show</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="series">
                                        <div class="submenu-title">sÃ©ries</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/amorteamo/">
                                                    <span class="titulo">Amorteamo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/chapa-quente/">
                                                    <span class="titulo">Chapa Quente</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/dupla-identidade/index.html">
                                                    <span class="titulo">Dupla Identidade</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/felizes-para-sempre/index.html">
                                                    <span class="titulo">Felizes para sempre?</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/os-experientes/">
                                                    <span class="titulo">Os Experientes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/pe-na-cova/index.html">
                                                    <span class="titulo">PÃ© na Cova</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/especial-blog/ta-no-ar-a-tv-na-tv/1.html">
                                                    <span class="titulo">TÃ¡ no ar: a TV na TV </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/tapas-e-beijos/">
                                                    <span class="titulo">Tapas &amp; Beijos</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/zorra/">
                                                    <span class="titulo">Zorra</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="webseries">
                                        <div class="submenu-title">websÃ©ries</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/como-fazer/no-ar.html">
                                                    <span class="titulo">Como fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/dulce-delight/">
                                                    <span class="titulo">Dulce Delight</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/look-do-dia-com-tia-suelly/no-ar.html">
                                                    <span class="titulo">Look do Dia com Tia Suelly</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/marrom-combina-com-tudo/no-ar.html">
                                                    <span class="titulo">Marrom Combina com Tudo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/morri-na-tv/no-ar.html">
                                                    <span class="titulo">Morri na TV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/sebastiana-quebra-galho/no-ar.html">
                                                    <span class="titulo">Sebastiana Quebra-Galho</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/sete-vidas-casos-reais/">
                                                    <span class="titulo">Sete Vidas: casos reais</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/o-incrivel-superonix/no-ar.html">
                                                    <span class="titulo">SuperÃnix</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/tome-prumo/">
                                                    <span class="titulo">Tome Prumo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/vlog-da-valeska/no-ar.html">
                                                    <span class="titulo">Vlog da Valeska</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="rede-globo">
                                        <div class="submenu-title">rede globo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/programacao.html">
                                                    <span class="titulo">ProgramaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-gshow"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="famosos-etc analytics-product analytics-multi-product">
            <a href="http://famosos.globo.com" class="analytics-area analytics-id-T">
                <span class="titulo">famosos &amp; etc</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-famosos-etc">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://famosos.globo.com/" data-menu-id="famosos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">famosos</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="moda-estilo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">moda & estilo</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="bem-estar-saude">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">bem-estar & saÃºde</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="casa-decoracao">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">casa & decoraÃ§Ã£o</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="gastronomia">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gastronomia</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="musica-cinema-e-teatro">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mÃºsica, cinema e teatro</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="canais-de-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">canais de tv</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-famosos-etc">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-famosos-etc" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://ego.globo.com/">
                                                <span class="titulo">Ego</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://paparazzo.globo.com/">
                                                <span class="titulo">Paparazzo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://revistaquem.globo.com/">
                                                <span class="titulo">Quem</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/famosos/">
                                                <span class="titulo">Retratos da Vida</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/cultura/kogut/">
                                                <span class="titulo">Patricia Kogut</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://famosos.globo.com/">
                                                <span class="titulo">Famosos</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://revistacasaejardim.globo.com/">
                                                <span class="titulo">Revista Casa e Jardim</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="famosos">
                                        <div class="submenu-title">famosos</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/">
                                                    <span class="titulo">Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://paparazzo.globo.com/">
                                                    <span class="titulo">Paparazzo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/cultura/kogut/">
                                                    <span class="titulo">Patricia Kogut</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaquem.globo.com/">
                                                    <span class="titulo">Quem</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/famosos/">
                                                    <span class="titulo">Retratos da Vida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="moda-estilo">
                                        <div class="submenu-title">moda &amp; estilo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/moda/index.html">
                                                    <span class="titulo">Moda no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ela.oglobo.globo.com/">
                                                    <span class="titulo">Ela no O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo/">
                                                    <span class="titulo">Estilo no Gshow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaglamour.globo.com/">
                                                    <span class="titulo">Revista Glamour</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gq.globo.com/">
                                                    <span class="titulo">Revista GQ Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistamarieclaire.globo.com/">
                                                    <span class="titulo">Revista Marie Claire </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/moda/">
                                                    <span class="titulo">Moda no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://vogue.globo.com/">
                                                    <span class="titulo">Revista Vogue</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="bem-estar-saude">
                                        <div class="submenu-title">bem-estar &amp; saÃºde</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/beleza/index.html">
                                                    <span class="titulo">Beleza no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/beleza/">
                                                    <span class="titulo">Beleza no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.com.br/bemestar">
                                                    <span class="titulo">Bem Estar no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar no G1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/maes-e-filhos/">
                                                    <span class="titulo">MÃ£es no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/">
                                                    <span class="titulo">Meus 5 Minutos </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://horoscopo.ego.globo.com/">
                                                    <span class="titulo">HorÃ³scopo no Ego</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="casa-decoracao">
                                        <div class="submenu-title">casa &amp; decoraÃ§Ã£o</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/">
                                                    <span class="titulo">Revista Casa e Jardim</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/casa-e-decoracao/">
                                                    <span class="titulo">Casa no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://casavogue.globo.com/">
                                                    <span class="titulo">Casa Vogue </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revista.zapimoveis.com.br/?utm_source=globo.com-etc&amp;utm_medium=botao-casa-decoracao&amp;utm_campaign=Revista-Home">
                                                    <span class="titulo">Revista do Zap</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="gastronomia">
                                        <div class="submenu-title">gastronomia</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/receitas/">
                                                    <span class="titulo">Receitas no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas/index.html">
                                                    <span class="titulo">Receitas no Mais VocÃª </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/blogs/Colheradas/">
                                                    <span class="titulo">Colheradas </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/Casa-e-Comida/">
                                                    <span class="titulo">Casa e Comida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="musica-cinema-e-teatro">
                                        <div class="submenu-title">mÃºsica, cinema e teatro</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/cinema/">
                                                    <span class="titulo">G1 Cinema</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/">
                                                    <span class="titulo">G1 MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">Gshow MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://musica.com.br/">
                                                    <span class="titulo">MÃºsica.com.br</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radiobeat.com.br/">
                                                    <span class="titulo">RADIOBEAT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.agentesevenoteatro.com.br/">
                                                    <span class="titulo">Teatro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="canais-de-tv">
                                        <div class="submenu-title">canais de tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/">
                                                    <span class="titulo">Rede Globo </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalbrasil.globo.com/">
                                                    <span class="titulo">Canal Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://mundogloob.globo.com/">
                                                    <span class="titulo">Gloob</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/">
                                                    <span class="titulo">GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://megapix.globo.com/">
                                                    <span class="titulo">Megapix</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://multishow.globo.com/">
                                                    <span class="titulo">Multishow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://telecine.globo.com/">
                                                    <span class="titulo">Telecine</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://uc.globo.com/">
                                                    <span class="titulo">Universal Channel</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalviva.globo.com/">
                                                    <span class="titulo">Viva</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://canaloff.globo.com">
                                                    <span class="titulo">Off</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-famosos-etc"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="tecnologia analytics-product analytics-multi-product">
            <a href="http://www.techtudo.com.br/" class="analytics-area analytics-id-T">
                <span class="titulo">tecnologia</span>

        
        
            </a>
        </li>
        
    
        <li class="videos analytics-product analytics-multi-product">
            <a href="http://globotv.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">vÃ­deos</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                            
                                <li class="has-link ">
                                    <a href="http://globotv.globo.com/" data-menu-id="globotv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globo.tv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link ">
                                    <a href="http://globosatplay.globo.com/" data-menu-id="globosat-play">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globosat play</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-videos">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
</ul>
<!-- gerado em: 2015-08-0722:02:40Z --><div id="assinante-menu"></div></nav></div><div class="header-bottom"></div></header><script>
libby.loadScript('http://s.glbimg.com/en/ho/static/etc/busca/js/jquery.buscaPadrao.v2.compressed.js', function(){var searchArea=document.getElementById('busca-padrao'),
placeholderValue = 'encontre na globo.com';if(typeof searchWidget !== "undefined" && searchWidget){searchWidget(searchArea, {suggestionsEnabled: true,
baseSearchUrl: 'http://www.globo.com',
addClass:'home-search',
placeholder: placeholderValue,
buscaHome: true,
qtd_min_letras_pro_suggest: 2,
qtd_min_letras_pra_busca: 2,
addStyleSheet: ''});}
searchArea.style.display = "block";if(searchArea && searchArea.querySelectorAll){var inputElement=searchArea.querySelectorAll('input')[0];inputElement.onwebkitspeechchange=inputElement.onspeechchange=function (ev) {inputElement.value=inputElement.value.replace(placeholderValue, '');inputElement.form.submit();};}});</script><div id="x60" class="opec-area opec-mobile opec-x60 grid-12"><div id="banner_mobile_topo" class="tag-manager-publicidade-container"></div></div><div id="urgente" class="container urgente analytics-area analytics-id-U "><div class="grid-base super-wide"><div class="glb-box-eventos-maker"><div class="glb-container" style="border-color: #FF7400;"><a class="content-box-evento" href="http://globoesporte.globo.com/cartola-fc/"><span class="logo" style="background: url(http://s2.glbimg.com/J7EmQ8qEpKZNWcwDO_oDRzAgYzs=/0x0:100x75/100x75/s.glbimg.com/en/ho/f/original/2015/05/18/sprite.png) 0 0 no-repeat; width: 100px; height: 75px;"></span><span class="title-parent"><span class="logo-title" style="color: #FF7400;">cartola fc</span><span class="title" data-hover-color="#FF7400">Ainda dÃ¡ tempo de escolher os jogadores e montar seu time para a prÃ³xima rodada</span></span><span class="subtitle" style="color: #FF7400;">mercado estÃ¡ aberto<span class="arrow"> âº</span></span></a></div></div></div></div><div id="bloco-principal" class="container first-scroll-container clearfix"><div class="grid-base wide analytics-area analytics-id-A"><div class="grid-base narrow pull-left primeira-area"><div class="destaque-principal-vertical-foto-topo destaque destaque-primeiro-scroll principal glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/economia/noticia/2015/08/bc-perde-r-57-bilhoes-com-swaps-ate-julho-e-nao-contem-alta-do-dolar.html" class=" " title="BC perde R$ 57 bi em 7 meses e nÃ£o contÃ©m alta do dÃ³lar"><div class="conteudo"><h2>BC perde R$ 57 bi em 7 meses e nÃ£o contÃ©m alta do dÃ³lar</h2></div></a></div></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/pr/parana/noticia/2015/08/prisoes-de-irmao-de-dirceu-e-mais-2-sao-prorrogadas-na-lava-jato.html" class=" " title="IrmÃ£o de Dirceu recebia R$ 30 mil mensais de lobista, segundo Moro"><div class="conteudo"><h2>IrmÃ£o de Dirceu recebia R$ 30 mil mensais de lobista, segundo Moro</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Delator diz ter repassado R$ 765 mil a ex-Eletronuclear" href="http://g1.globo.com/politica/blog/matheus-leitao/post/delator-afirma-ter-repassado-r-765-mil-para-empresa-de-almirante-suspeito-na-lava-jato.html">Delator diz ter repassado R$ 765 mil a ex-Eletronuclear</a></div></li></ul></div><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/sp/ribeirao-preto-franca/noticia/2015/08/grande-coincidencia-diz-delegado-sobre-libertacao-de-37-presos-em-sp.html" class="foto " title="Grupo liberta 37 presos, e polÃ­cia vÃª &#39;coincidÃªncia&#39; (ReproduÃ§Ã£o/TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/ECxXUnApbbHZsI-YB27PoYeC0Bc=/filters:quality(10):strip_icc()/s2.glbimg.com/9-DiWD6oG-kqXw38g6FK4xS6GDs=/0x115:300x251/155x70/s.glbimg.com/jo/g1/f/original/2015/08/07/carroforte.jpg" alt="Grupo liberta 37 presos, e polÃ­cia vÃª &#39;coincidÃªncia&#39; (ReproduÃ§Ã£o/TV Globo)" title="Grupo liberta 37 presos, e polÃ­cia vÃª &#39;coincidÃªncia&#39; (ReproduÃ§Ã£o/TV Globo)"
         data-original-image="s2.glbimg.com/9-DiWD6oG-kqXw38g6FK4xS6GDs=/0x115:300x251/155x70/s.glbimg.com/jo/g1/f/original/2015/08/07/carroforte.jpg" data-url-smart_horizontal="7Tl_mVYuwYX6BFc6OB92N_fIphE=/90x56/smart/filters:strip_icc()/" data-url-smart="7Tl_mVYuwYX6BFc6OB92N_fIphE=/90x56/smart/filters:strip_icc()/" data-url-feature="7Tl_mVYuwYX6BFc6OB92N_fIphE=/90x56/smart/filters:strip_icc()/" data-url-tablet="SB__8qqy2hZ6ZrCal4pYjPEj45I=/160xorig/smart/filters:strip_icc()/" data-url-desktop="hJngpvNe8EVkd8nE3mrAGkdQOv8=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Grupo liberta 37 presos, e polÃ­cia vÃª &#39;coincidÃªncia&#39;</h2></div></a></div></div><div class="ultimo-destaque"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/times/palmeiras/noticia/2015/08/valdivia-vai-justica-para-que-google-exclua-da-web-fotos-comprometedoras.html" class="foto " title="Valdivia tenta na JustiÃ§a excluir fotos com outra (Cesar Greco/Ag Palmeiras)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/eZ_glupoimrQDOInBcCE9NSyl7U=/filters:quality(10):strip_icc()/s2.glbimg.com/0wo-NmFWsyiys6ZFgjzATs_t-tY=/62x133:750x443/155x70/s.glbimg.com/es/ge/f/original/2015/07/07/19319965239_9b29c65d2a_o_1.jpg" alt="Valdivia tenta na JustiÃ§a excluir fotos com outra (Cesar Greco/Ag Palmeiras)" title="Valdivia tenta na JustiÃ§a excluir fotos com outra (Cesar Greco/Ag Palmeiras)"
         data-original-image="s2.glbimg.com/0wo-NmFWsyiys6ZFgjzATs_t-tY=/62x133:750x443/155x70/s.glbimg.com/es/ge/f/original/2015/07/07/19319965239_9b29c65d2a_o_1.jpg" data-url-smart_horizontal="aQE2JqvTyqMcdlOmJjkDVIckJTI=/90x56/smart/filters:strip_icc()/" data-url-smart="aQE2JqvTyqMcdlOmJjkDVIckJTI=/90x56/smart/filters:strip_icc()/" data-url-feature="aQE2JqvTyqMcdlOmJjkDVIckJTI=/90x56/smart/filters:strip_icc()/" data-url-tablet="NmAInvTfQrc5FM_ETbZyKo_ybSM=/160xorig/smart/filters:strip_icc()/" data-url-desktop="AP0ZOAZwLPPPs9j9g6ZrEn1WeAA=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Valdivia tenta na JustiÃ§a excluir  fotos com outra</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/times/internacional/noticia/2015/08/aguirre-nao-ve-razoes-claras-para-demissao-so-quem-ganha-e-bom.html" class="foto " title="Aguirre diz nÃ£o ver razÃµes para saÃ­da: &#39;Surpreso&#39; (TomÃ¡s Hammes)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/vGg5Y8jMSirh4nSZpkxhjmVW2Q4=/filters:quality(10):strip_icc()/s2.glbimg.com/PHoBR_8zjfYEkSeUXqawvHiR_2Q=/564x450:3381x1724/155x70/s.glbimg.com/es/ge/f/original/2015/08/07/img_8859.jpg" alt="Aguirre diz nÃ£o ver razÃµes para saÃ­da: &#39;Surpreso&#39; (TomÃ¡s Hammes)" title="Aguirre diz nÃ£o ver razÃµes para saÃ­da: &#39;Surpreso&#39; (TomÃ¡s Hammes)"
         data-original-image="s2.glbimg.com/PHoBR_8zjfYEkSeUXqawvHiR_2Q=/564x450:3381x1724/155x70/s.glbimg.com/es/ge/f/original/2015/08/07/img_8859.jpg" data-url-smart_horizontal="3ZW03QKUtaSBqPInyHRQ0rh6T1g=/90x56/smart/filters:strip_icc()/" data-url-smart="3ZW03QKUtaSBqPInyHRQ0rh6T1g=/90x56/smart/filters:strip_icc()/" data-url-feature="3ZW03QKUtaSBqPInyHRQ0rh6T1g=/90x56/smart/filters:strip_icc()/" data-url-tablet="H0pZLGSyptjd8W55ExTeffUzhPA=/160xorig/smart/filters:strip_icc()/" data-url-desktop="yeeSuXX24aKfWP63V_Eh1MPbFUw=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Aguirre diz nÃ£o ver razÃµes para saÃ­da: &#39;Surpreso&#39;</h2></div></a></div></div></div></div></div><div class="grid-base narrow ultimo"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://g1.globo.com/sao-paulo/noticia/2015/08/ato-pede-punicao-por-atentado-bomba-na-sede-do-instituto-lula.html" class="foto " title="SP: ato prÃ³-Lula condena bomba em seu instituto (Newton Menezes/Futura Press/EstadÃ£o ConteÃºdo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/zfVcYZBM4VmQwn6V7M6nvzf4Qyw=/filters:quality(10):strip_icc()/s2.glbimg.com/keG-OEm5DNyFDum2sfD-Q5jpZqw=/328x375:1259x1133/155x126/s.glbimg.com/jo/g1/f/original/2015/08/07/ato-instituto_lula_newton_menezes_estadao_conteudo.jpg" alt="SP: ato prÃ³-Lula condena bomba em seu instituto (Newton Menezes/Futura Press/EstadÃ£o ConteÃºdo)" title="SP: ato prÃ³-Lula condena bomba em seu instituto (Newton Menezes/Futura Press/EstadÃ£o ConteÃºdo)"
         data-original-image="s2.glbimg.com/keG-OEm5DNyFDum2sfD-Q5jpZqw=/328x375:1259x1133/155x126/s.glbimg.com/jo/g1/f/original/2015/08/07/ato-instituto_lula_newton_menezes_estadao_conteudo.jpg" data-url-smart_horizontal="d5I6B8rzWIK9Mt8XnIUxeFTQJCM=/90x56/smart/filters:strip_icc()/" data-url-smart="d5I6B8rzWIK9Mt8XnIUxeFTQJCM=/90x56/smart/filters:strip_icc()/" data-url-feature="d5I6B8rzWIK9Mt8XnIUxeFTQJCM=/90x56/smart/filters:strip_icc()/" data-url-tablet="zmWLrxUlhnU-yOstz7DqVr-WPyQ=/160xorig/smart/filters:strip_icc()/" data-url-desktop="i1aRCvH0Xi0QUTQ7tnxgqFjaRBY=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>SP: ato prÃ³-Lula condena bomba em seu instituto</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://g1.globo.com/sao-paulo/noticia/2015/08/videos-mostram-tiroteio-no-shopping-tucuruvi-em-sao-paulo.html" class="foto " title="VÃ­deos mostram troca de tiros em shopping de SP (TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/zfBqOns88I6KlBKKfHHo89D-0oU=/filters:quality(10):strip_icc()/s2.glbimg.com/odysG64J5pxO2GHrTSxcqt_0pks=/227x212:679x579/155x126/s.glbimg.com/en/ho/f/original/2015/08/07/shopping.jpg" alt="VÃ­deos mostram troca de tiros em shopping de SP (TV Globo)" title="VÃ­deos mostram troca de tiros em shopping de SP (TV Globo)"
         data-original-image="s2.glbimg.com/odysG64J5pxO2GHrTSxcqt_0pks=/227x212:679x579/155x126/s.glbimg.com/en/ho/f/original/2015/08/07/shopping.jpg" data-url-smart_horizontal="rcgAuKFslHBgs72oDrbvoexO_GI=/90x56/smart/filters:strip_icc()/" data-url-smart="rcgAuKFslHBgs72oDrbvoexO_GI=/90x56/smart/filters:strip_icc()/" data-url-feature="rcgAuKFslHBgs72oDrbvoexO_GI=/90x56/smart/filters:strip_icc()/" data-url-tablet="qQEqSLnDsH4bsXDFAlwHt8wXCOk=/160xorig/smart/filters:strip_icc()/" data-url-desktop="DZ76CvMG-nmRJnIaO5_A-w7cnBM=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>VÃ­deos mostram troca de tiros em shopping de SP</h2></div></a></div></div></div><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/sp/bauru-marilia/noticia/2015/08/apos-frases-racistas-simbolo-nazista-e-pichado-em-banheiro-da-unesp.html" class="foto " title="SÃ­mbolo nazista
Ã© desenhado em faculdade de SP (TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/DGFCg-UzTEI0X1KPktKx0irYp7U=/filters:quality(10):strip_icc()/s2.glbimg.com/2SlRgF1mZtmeh6XGPz38II9nmvg=/286x0:1274x446/155x70/s.glbimg.com/en/ho/f/original/2015/08/07/unesp.jpg" alt="SÃ­mbolo nazista
Ã© desenhado em faculdade de SP (TV Globo)" title="SÃ­mbolo nazista
Ã© desenhado em faculdade de SP (TV Globo)"
         data-original-image="s2.glbimg.com/2SlRgF1mZtmeh6XGPz38II9nmvg=/286x0:1274x446/155x70/s.glbimg.com/en/ho/f/original/2015/08/07/unesp.jpg" data-url-smart_horizontal="UWVfN95JptNH2mbyW2mHDYiDkTo=/90x56/smart/filters:strip_icc()/" data-url-smart="UWVfN95JptNH2mbyW2mHDYiDkTo=/90x56/smart/filters:strip_icc()/" data-url-feature="UWVfN95JptNH2mbyW2mHDYiDkTo=/90x56/smart/filters:strip_icc()/" data-url-tablet="IM_V03tpFs4twnAcHZC1UlOGp20=/160xorig/smart/filters:strip_icc()/" data-url-desktop="RVF3mkkDBgfZSbpcQ0sKF2sDcx4=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>SÃ­mbolo nazista<br />Ã© desenhado em faculdade de SP</h2></div></a></div></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/times/flamengo/noticia/2015/08/heranca-de-luxa-nao-rende-frutos-e-para-e-o-unico-ainda-na-briga-por-vaga.html" class=" " title="HeranÃ§a de Luxa nÃ£o rende frutos, e ParÃ¡ Ã© Ãºnico na luta por vaga no Fla"><div class="conteudo"><h2>HeranÃ§a de Luxa nÃ£o rende frutos, e ParÃ¡ Ã© Ãºnico na luta por vaga no Fla</h2></div></a></div></div><div class="ultimo-destaque"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/times/palmeiras/noticia/2015/08/valdivia-vai-justica-para-que-google-exclua-da-web-fotos-comprometedoras.html" class=" " title="Valdivia tenta na JustiÃ§a excluir fotos com outra (Cesar Greco/Ag Palmeiras)"><div class="conteudo"><h2>Valdivia tenta na JustiÃ§a excluir  fotos com outra</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/times/internacional/noticia/2015/08/aguirre-nao-ve-razoes-claras-para-demissao-so-quem-ganha-e-bom.html" class=" " title="Aguirre diz nÃ£o ver razÃµes para saÃ­da: &#39;Surpreso&#39; (TomÃ¡s Hammes)"><div class="conteudo"><h2>Aguirre diz nÃ£o ver razÃµes para saÃ­da: &#39;Surpreso&#39;</h2></div></a></div></div></div></div></div><div class="grid-base wide pull-left wide-inferior"></div></div><script>
var init,
stid;if(typeof window.homeColumns === 'undefined' && !window.homeColumns){window.homeColumns=new HomeColumns({});}
init=function(ev) {window.homeColumns.initFirstScroll();if(!window.homeColumns.mergedFirstScroll){responsiveHub.updateImages();}
window.removeEventListener("resize", init);clearTimeout(stid);};stid=setTimeout(function(){init();}, 200);window.addEventListener("resize", init);</script><div id="area-widgets-direita-topo" class="grid-base narrow area-widgets-direta-topo analytics-area analytics-id-V"><div id="ad-position-top1" class="opec"><div id="banner_slim_topo" class="tag-manager-publicidade-container"></div></div><div class="libby-agrupador-destaque-globotv-com-canais length-5"><div class="main-area mobile-grid-full"><div class="header-area analytics-area analytics-id-header"><a class="logo-link" href="http://globotv.globo.com"><div class="logo"></div></a><h3><a class="title-link" href="http://globotv.globo.com/rede-globo/mais-voce/">Mais vocÃª</a></h3></div><div class="hover-area"><div class="image-area"><ul><li class="active analytics-area analytics-id-1 analytics-id-I glb-hl-style-entretenimento" data-href="http://globotv.globo.com/rede-globo/mais-voce/t/programas/v/veja-como-estao-as-adolescentes-que-encararam-o-desafio-barriga-zero/4376053/"><a href="http://globotv.globo.com/rede-globo/mais-voce/t/programas/v/veja-como-estao-as-adolescentes-que-encararam-o-desafio-barriga-zero/4376053/"><div class="image-container"><div class="image-wrapper"><img src="http://s2.glbimg.com/5-SJXFd6zijROxJWBs4r_kQn8-I=/23x26:661x359/335x175/s.glbimg.com/en/ho/f/original/2015/08/07/antes_e_depois1.jpg" alt="Participantes mostram resultado do &#39;Desafio Barriga Zero&#39;" /><div class="time">03:20</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">alguns perderam 13 Kg</span><span class="title">Participantes mostram resultado do &#39;Desafio Barriga Zero&#39;</span></div></a></li><li class=" analytics-area analytics-id-2 analytics-id-I glb-hl-style-entretenimento" data-href="http://globotv.globo.com/rede-globo/mais-voce/v/conheca-historia-emocionante-do-tio-que-supriu-a-necessidade-de-pai-da-sobrinha/4376003/"><a href="http://globotv.globo.com/rede-globo/mais-voce/v/conheca-historia-emocionante-do-tio-que-supriu-a-necessidade-de-pai-da-sobrinha/4376003/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4376003.jpg" alt="Veja histÃ³ria do tio que supriu a necessidade de pai da sobrinha" /><div class="time">07:42</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">emocionante </span><span class="title">Veja histÃ³ria do tio que supriu a necessidade de pai da sobrinha</span></div></a></li><li class=" analytics-area analytics-id-3 analytics-id-I glb-hl-style-entretenimento" data-href="http://globotv.globo.com/rede-globo/mais-voce/v/ana-maria-anuncia-elenco-que-vai-participar-do-super-chef-celebridades-2015/4376092/"><a href="http://globotv.globo.com/rede-globo/mais-voce/v/ana-maria-anuncia-elenco-que-vai-participar-do-super-chef-celebridades-2015/4376092/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4376092.jpg" alt="Ana Maria anuncia elenco do Super Chef Celebridades 2015" /><div class="time">01:06</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">cheio de famosos</span><span class="title">Ana Maria anuncia elenco do Super Chef Celebridades 2015</span></div></a></li></ul></div><div class="title-area"><ul><li class="active analytics-area analytics-id-1 analytics-id-T glb-hl-style-entretenimento"><a href="http://globotv.globo.com/rede-globo/mais-voce/t/programas/v/veja-como-estao-as-adolescentes-que-encararam-o-desafio-barriga-zero/4376053/"><span class="subtitle">alguns perderam 13 Kg</span><span class="title">Participantes mostram resultado do &#39;Desafio Barriga Zero&#39;</span></a></li><li class=" analytics-area analytics-id-2 analytics-id-T glb-hl-style-entretenimento"><a href="http://globotv.globo.com/rede-globo/mais-voce/v/conheca-historia-emocionante-do-tio-que-supriu-a-necessidade-de-pai-da-sobrinha/4376003/"><span class="subtitle">emocionante </span><span class="title">Veja histÃ³ria do tio que supriu a necessidade de pai da sobrinha</span></a></li><li class=" analytics-area analytics-id-3 analytics-id-T glb-hl-style-entretenimento"><a href="http://globotv.globo.com/rede-globo/mais-voce/v/ana-maria-anuncia-elenco-que-vai-participar-do-super-chef-celebridades-2015/4376092/"><span class="subtitle">cheio de famosos</span><span class="title">Ana Maria anuncia elenco do Super Chef Celebridades 2015</span></a></li></ul></div></div><div class="controls-area"><ul class="step-marker glb-hl-style-entretenimento"><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-1"><a href="#" data-related="http://globotv.globo.com/rede-globo/mais-voce/t/programas/v/veja-como-estao-as-adolescentes-que-encararam-o-desafio-barriga-zero/4376053/"><div class="ball active"></div></a></div></li><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-2"><a href="#" data-related="http://globotv.globo.com/rede-globo/mais-voce/v/conheca-historia-emocionante-do-tio-que-supriu-a-necessidade-de-pai-da-sobrinha/4376003/"><div class="ball "></div></a></div></li><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-3"><a href="#" data-related="http://globotv.globo.com/rede-globo/mais-voce/v/ana-maria-anuncia-elenco-que-vai-participar-do-super-chef-celebridades-2015/4376092/"><div class="ball "></div></a></div></li></ul><ul class="navigation-buttons"><li><a href="#left"><div class="left"></div></a></li><li><a href="#right"><div class="right"></div></a></li></ul></div><div class="menu-area"><ul><li><a href="http://globotv.globo.com/rede-globo/"><div class="channels rede-globo"></div></a></li><li><a href="http://globotv.globo.com/sportv/"><div class="channels sportv"></div></a></li><li><a href="http://globotv.globo.com/globo-news/"><div class="channels globo-news"></div></a></li><li><a href="http://globotv.globo.com/multishow/"><div class="channels multishow"></div></a></li><li><a href="http://globotv.globo.com/gnt/"><div class="channels gnt"></div></a></li></ul></div><div class="footer-area analytics-area analytics-id-footer"><a class="title-link" href="http://globotv.globo.com/">mais<strong> vÃ­deos</strong><span class="arrow"> âº</span></a></div></div></div><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded-agrupador-destaque-globotv-com-canais']);</script></div></div><div class="separator-first-scroll-container"><section><div><div id="opec-banner-middle-container" class="opec-banner-middle-container"><div id="ad-position-middle" class="opec"><div id="banner_slb_meio" class="tag-manager-publicidade-container"></div></div></div></div></section></div><div id="x62" class="opec-area opec-mobile opec-x62 grid-12"><div id="banner_mobile_meio" class="tag-manager-publicidade-container"></div></div><div id="container-columns" class="container columns clearfix glb-area-colunas"><section class="area news-column   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/bahia/noticia/2015/08/acidente-na-br-116-mata-4-pessoas-da-mesma-familia-na-bahia.html" class="foto" title="Passageiro morto com mais trÃªs de sua famÃ­lia ia para o enterro do pai na Bahia (Jeferson Ollafer/Blog Voz da Bahia)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/FPFxdktkEE4aHp3UR7zNBd7Es40=/filters:quality(10):strip_icc()/s2.glbimg.com/xyzXIDB_QWpr0cK28O4SwtA4V08=/0x69:620x402/335x180/s.glbimg.com/jo/g1/f/original/2015/08/07/acidente_marcus.jpg" alt="Passageiro morto com mais trÃªs de sua famÃ­lia ia para o enterro do pai na Bahia (Jeferson Ollafer/Blog Voz da Bahia)" title="Passageiro morto com mais trÃªs de sua famÃ­lia ia para o enterro do pai na Bahia (Jeferson Ollafer/Blog Voz da Bahia)"
                data-original-image="s2.glbimg.com/xyzXIDB_QWpr0cK28O4SwtA4V08=/0x69:620x402/335x180/s.glbimg.com/jo/g1/f/original/2015/08/07/acidente_marcus.jpg" data-url-smart_horizontal="92hNciMGeV_XzK2Jn5EnGhxCluY=/90x0/smart/filters:strip_icc()/" data-url-smart="92hNciMGeV_XzK2Jn5EnGhxCluY=/90x0/smart/filters:strip_icc()/" data-url-feature="92hNciMGeV_XzK2Jn5EnGhxCluY=/90x0/smart/filters:strip_icc()/" data-url-tablet="4SBM0IxVSZugpaJ4dE1p0owctiM=/220x125/smart/filters:strip_icc()/" data-url-desktop="GZoctxVN5ibTqLSJ4WM92zOCJx4=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Passageiro morto com mais trÃªs de sua famÃ­lia ia para o enterro do pai na Bahia</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-07 17:21:47" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/noticias/mundo/mae-acredita-ter-encontrado-filho-desparecido-em-foto-tirada-em-jogo-do-chelsea-17115435.html#" class="foto" title="MÃ£e acredita ter achado filho sumido hÃ¡ 11 meses em foto tirada em jogo do Chelsea (ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/ao_r2OhR5TxK3jW_QIttAlUBlWw=/filters:quality(10):strip_icc()/s2.glbimg.com/776IeR2o1JK-5JQ_z0UQu93XzJE=/0x260:448x558/120x80/s.glbimg.com/en/ho/f/original/2015/08/07/chelsea.jpg" alt="MÃ£e acredita ter achado filho sumido hÃ¡ 11 meses em foto tirada em jogo do Chelsea (ReproduÃ§Ã£o)" title="MÃ£e acredita ter achado filho sumido hÃ¡ 11 meses em foto tirada em jogo do Chelsea (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/776IeR2o1JK-5JQ_z0UQu93XzJE=/0x260:448x558/120x80/s.glbimg.com/en/ho/f/original/2015/08/07/chelsea.jpg" data-url-smart_horizontal="ZiPDP9lQ9R0cHKtIxyfWoSbEj38=/90x0/smart/filters:strip_icc()/" data-url-smart="ZiPDP9lQ9R0cHKtIxyfWoSbEj38=/90x0/smart/filters:strip_icc()/" data-url-feature="ZiPDP9lQ9R0cHKtIxyfWoSbEj38=/90x0/smart/filters:strip_icc()/" data-url-tablet="UsNo1sINvCJsCfw9_tQWnbiYs_U=/70x50/smart/filters:strip_icc()/" data-url-desktop="ptiFS-tsA0zv8KclGzoN1fTamrQ=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>MÃ£e acredita ter achado filho sumido hÃ¡ 11 meses em foto tirada em jogo do Chelsea</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-07 19:50:22" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/sp/mogi-das-cruzes-suzano/noticia/2015/08/pai-que-matou-filhas-e-se-enforcou-ja-tinha-tentado-suicidio-diz-irmao.html" class="foto" title="Homem que matou filhas de 2 e 4 anos em SP jÃ¡ tinha tentado se matar antes (Paulo Henrique da Silva/Arquivo Pessoal)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/hKzdAaaVxzKTMfP0J6zr4wtxOj8=/filters:quality(10):strip_icc()/s2.glbimg.com/DZrVhAcy8FZYgKXpal8TGrGAaTY=/125x107:605x427/120x80/s.glbimg.com/jo/g1/f/original/2015/08/07/irmas_mortas.jpg" alt="Homem que matou filhas de 2 e 4 anos em SP jÃ¡ tinha tentado se matar antes (Paulo Henrique da Silva/Arquivo Pessoal)" title="Homem que matou filhas de 2 e 4 anos em SP jÃ¡ tinha tentado se matar antes (Paulo Henrique da Silva/Arquivo Pessoal)"
                data-original-image="s2.glbimg.com/DZrVhAcy8FZYgKXpal8TGrGAaTY=/125x107:605x427/120x80/s.glbimg.com/jo/g1/f/original/2015/08/07/irmas_mortas.jpg" data-url-smart_horizontal="GrH34qL5-lUZTWGZgxNtyPdcuCM=/90x0/smart/filters:strip_icc()/" data-url-smart="GrH34qL5-lUZTWGZgxNtyPdcuCM=/90x0/smart/filters:strip_icc()/" data-url-feature="GrH34qL5-lUZTWGZgxNtyPdcuCM=/90x0/smart/filters:strip_icc()/" data-url-tablet="6uAULLQFw9ojEJhXnUrX0dHqCfs=/70x50/smart/filters:strip_icc()/" data-url-desktop="e9HhRGXRUHCqyrN5bhs3rqrI2xc=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Homem que matou filhas de 2 e 4 anos em SP jÃ¡ tinha tentado se matar antes</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-07 20:08:57" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/noticias/brasil/professora-filmada-agredindo-criancas-em-creche-municipal-17119940.html" class="" title="Professora Ã© flagrada agredindo crianÃ§as em creche de Minas Gerais; confira as imagens (reproduÃ§Ã£o)" rel="bookmark"><span class="conteudo"><p>Professora Ã© flagrada agredindo crianÃ§as em <br />creche de Minas Gerais; confira as imagens</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-07 21:36:02" class="chamada chamada-principal mobile-grid-full"><a href="http://globosatplay.globo.com/globonews/v/3741949/?utm_source=globo.com&amp;utm_medium=onias&amp;utm_campaign=torrededavid" class="foto" title="PrÃ©dio de 45 andares Ã© maior favela vertical do mundo; veja doc que concorre ao Emmy (Globo News)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/5wWceJGEblsDureRW10i6zI4MRo=/filters:quality(10):strip_icc()/s2.glbimg.com/gSM-RIKKda_KjbPCwS52nZcMpDc=/212x26:851x453/120x80/s.glbimg.com/en/ho/f/original/2015/08/07/caracas.jpg" alt="PrÃ©dio de 45 andares Ã© maior favela vertical do mundo; veja doc que concorre ao Emmy (Globo News)" title="PrÃ©dio de 45 andares Ã© maior favela vertical do mundo; veja doc que concorre ao Emmy (Globo News)"
                data-original-image="s2.glbimg.com/gSM-RIKKda_KjbPCwS52nZcMpDc=/212x26:851x453/120x80/s.glbimg.com/en/ho/f/original/2015/08/07/caracas.jpg" data-url-smart_horizontal="XkUJBaCp7qpMoqDz51doFi_B71M=/90x0/smart/filters:strip_icc()/" data-url-smart="XkUJBaCp7qpMoqDz51doFi_B71M=/90x0/smart/filters:strip_icc()/" data-url-feature="XkUJBaCp7qpMoqDz51doFi_B71M=/90x0/smart/filters:strip_icc()/" data-url-tablet="vUASn2FYX_-NHIDei_PypJyknV4=/70x50/smart/filters:strip_icc()/" data-url-desktop="WeCIgOhS3wZa5KtvIJLRHiGpHpo=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>PrÃ©dio de 45 andares Ã© maior favela vertical do mundo; veja doc que concorre ao Emmy</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-07 15:30:02" class="chamada chamada-principal mobile-grid-full"><a href="http://www.techtudo.com.br/noticias/noticia/2015/08/microsoft-paga-r-350-mil-para-quem-achar-falha-de-seguranca-no-windows-10.html" class="foto" title="Liberado de graÃ§a, Windows 10 dÃ¡ R$ 350 mil para quem achar erros no sistema; veja (Confira 10 dÃºvidas sobre o Windows 10 (Foto: ReproduÃ§Ã£o/Microsoft))" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/pA_K5MuZ2sRIFmFMgpYlmZNBT80=/filters:quality(10):strip_icc()/s2.glbimg.com/yb-H9szGl9mnl9ZPy-sABRN-GuA=/63x0:650x391/120x80/s.glbimg.com/po/tt2/f/original/2015/06/03/untitled-11.jpg" alt="Liberado de graÃ§a, Windows 10 dÃ¡ R$ 350 mil para quem achar erros no sistema; veja (Confira 10 dÃºvidas sobre o Windows 10 (Foto: ReproduÃ§Ã£o/Microsoft))" title="Liberado de graÃ§a, Windows 10 dÃ¡ R$ 350 mil para quem achar erros no sistema; veja (Confira 10 dÃºvidas sobre o Windows 10 (Foto: ReproduÃ§Ã£o/Microsoft))"
                data-original-image="s2.glbimg.com/yb-H9szGl9mnl9ZPy-sABRN-GuA=/63x0:650x391/120x80/s.glbimg.com/po/tt2/f/original/2015/06/03/untitled-11.jpg" data-url-smart_horizontal="wD-RBvdNT0gA7L-hdugxktt3p1U=/90x0/smart/filters:strip_icc()/" data-url-smart="wD-RBvdNT0gA7L-hdugxktt3p1U=/90x0/smart/filters:strip_icc()/" data-url-feature="wD-RBvdNT0gA7L-hdugxktt3p1U=/90x0/smart/filters:strip_icc()/" data-url-tablet="vZ2h_JDKES_kdMb_6WP5MVXyIGw=/70x50/smart/filters:strip_icc()/" data-url-desktop="hk87--twqsvOGOs-3lnqSQ9qo88=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Liberado de graÃ§a, Windows <br />10 dÃ¡ R$ 350 mil para quem <br />achar erros no sistema; veja</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/sp/bauru-marilia/noticia/2015/08/pai-morre-apos-ver-filha-desacordada-em-pronto-socorro-de-pompeia.html" class="" title="Homem vÃª a filha desacordada apÃ³s ser atropelada, tem parada cardÃ­aca e morre em SP" rel="bookmark"><span class="conteudo"><p>Homem vÃª a filha desacordada apÃ³s ser atropelada, tem parada cardÃ­aca e morre em SP</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-07 19:59:35" class="chamada chamada-principal mobile-grid-full"><a href="http://oglobo.globo.com/mundo/idosa-russa-admite-matar-11-fazer-canibalismo-sou-assombrada-por-um-maniaco-17122462" class="foto" title="Idosa pede para ficar presa apÃ³s confessar 11 mortes e canibalismo na RÃºssia (reproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/qh3e8TSIH7unTHPn9l4sx5d7jwY=/filters:quality(10):strip_icc()/s2.glbimg.com/oRzoCejcn2issiyGcK8RtmfoeGQ=/120x110:445x326/120x80/s.glbimg.com/en/ho/f/original/2015/08/07/tamara.jpg" alt="Idosa pede para ficar presa apÃ³s confessar 11 mortes e canibalismo na RÃºssia (reproduÃ§Ã£o)" title="Idosa pede para ficar presa apÃ³s confessar 11 mortes e canibalismo na RÃºssia (reproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/oRzoCejcn2issiyGcK8RtmfoeGQ=/120x110:445x326/120x80/s.glbimg.com/en/ho/f/original/2015/08/07/tamara.jpg" data-url-smart_horizontal="g7X0tjHCgXpWnlyyRtjVu7HqH0w=/90x0/smart/filters:strip_icc()/" data-url-smart="g7X0tjHCgXpWnlyyRtjVu7HqH0w=/90x0/smart/filters:strip_icc()/" data-url-feature="g7X0tjHCgXpWnlyyRtjVu7HqH0w=/90x0/smart/filters:strip_icc()/" data-url-tablet="TTbVzFuCXxSkfrtsymkfacOc2gY=/70x50/smart/filters:strip_icc()/" data-url-desktop="ZRSdecZd48bS0YByE2xoFRd7tPQ=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Idosa pede para ficar presa apÃ³s confessar 11 mortes e canibalismo na RÃºssia</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/planeta-bizarro/noticia/2015/08/cinegrafista-registra-salto-incrivel-de-tubarao-branco-na-africa-do-sul.html" class="foto" title="TubarÃ£o branco dÃ¡ salto em mar da Ãfrica do Sul e cinegrafista registra; veja (ReproduÃ§Ã£o/YouTube/White Shark Video)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/GC71mN6-9wV3rqbZCdWEdwf4SM0=/filters:quality(10):strip_icc()/s2.glbimg.com/zgpnhQ2K2QsmpbReXmEGYqxrYuU=/182x68:511x288/120x80/s.glbimg.com/jo/g1/f/original/2015/08/07/tubarao.jpg" alt="TubarÃ£o branco dÃ¡ salto em mar da Ãfrica do Sul e cinegrafista registra; veja (ReproduÃ§Ã£o/YouTube/White Shark Video)" title="TubarÃ£o branco dÃ¡ salto em mar da Ãfrica do Sul e cinegrafista registra; veja (ReproduÃ§Ã£o/YouTube/White Shark Video)"
                data-original-image="s2.glbimg.com/zgpnhQ2K2QsmpbReXmEGYqxrYuU=/182x68:511x288/120x80/s.glbimg.com/jo/g1/f/original/2015/08/07/tubarao.jpg" data-url-smart_horizontal="gOLRND3I_qCghQ5oRdnjCZ-zBKw=/90x0/smart/filters:strip_icc()/" data-url-smart="gOLRND3I_qCghQ5oRdnjCZ-zBKw=/90x0/smart/filters:strip_icc()/" data-url-feature="gOLRND3I_qCghQ5oRdnjCZ-zBKw=/90x0/smart/filters:strip_icc()/" data-url-tablet="cGi2z_Itk3cTwjIvSAnrWEHocMg=/70x50/smart/filters:strip_icc()/" data-url-desktop="qAmr1d5nv7mz53FdqJNaTXaP8wE=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>TubarÃ£o branco dÃ¡ salto <br />em mar da Ãfrica do Sul e cinegrafista registra; veja</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section><section class="area sports-column   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://sportv.globo.com/site/combate/noticia/2015/08/glover-teixeira-exibe-boa-forma-em-pesagem-para-luta-contra-st-preux.html" class="foto" title="Glover exibe boa forma na pesagem para duelo contra St-Preux pelo UFC nos EUA (Getty Images)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/mqsHJs5sSu6wXqy-Vn_Bk3DBdTw=/filters:quality(10):strip_icc()/s2.glbimg.com/-YwYlwc_C6zLIHk0CRCQoR3Rkis=/190x87:1826x965/335x180/s.glbimg.com/es/ge/f/original/2015/08/07/gettyimages-483344294_1.jpg" alt="Glover exibe boa forma na pesagem para duelo contra St-Preux pelo UFC nos EUA (Getty Images)" title="Glover exibe boa forma na pesagem para duelo contra St-Preux pelo UFC nos EUA (Getty Images)"
                data-original-image="s2.glbimg.com/-YwYlwc_C6zLIHk0CRCQoR3Rkis=/190x87:1826x965/335x180/s.glbimg.com/es/ge/f/original/2015/08/07/gettyimages-483344294_1.jpg" data-url-smart_horizontal="dJAH3VL14_zKQJflS0gjiIN-wBk=/90x0/smart/filters:strip_icc()/" data-url-smart="dJAH3VL14_zKQJflS0gjiIN-wBk=/90x0/smart/filters:strip_icc()/" data-url-feature="dJAH3VL14_zKQJflS0gjiIN-wBk=/90x0/smart/filters:strip_icc()/" data-url-tablet="Ftq1pxr7p6kZfDT_hs-s3rfTocI=/220x125/smart/filters:strip_icc()/" data-url-desktop="Ryt4l72KbvehjONsR8qJeX1uKeA=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Glover exibe boa forma na pesagem para duelo contra St-Preux pelo UFC nos EUA</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-07 19:52:14" class="chamada chamada-principal mobile-grid-full"><a href="http://sportv.globo.com/site/combate/blogs/especial-blog/ultimmato/post/ufc-responde-floyd-mayweather-sobre-ronda-ela-nao-precisa-de-12-rounds.html" class="foto" title="UFC rebate Mayweather apÃ³s pugilista postar provocaÃ§Ã£o Ã  campeÃ£ Ronda Rousey (ReproduÃ§Ã£o/Twitter)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/ufx8m4SoUWchXTbNnk20s4sORZo=/filters:quality(10):strip_icc()/s2.glbimg.com/75J5dPNr0DoWC7SbUe9gs0b5oUo=/124x182:532x454/120x80/s.glbimg.com/es/ge/f/original/2015/08/07/ronda-ufc-12rounds.jpg" alt="UFC rebate Mayweather apÃ³s pugilista postar provocaÃ§Ã£o Ã  campeÃ£ Ronda Rousey (ReproduÃ§Ã£o/Twitter)" title="UFC rebate Mayweather apÃ³s pugilista postar provocaÃ§Ã£o Ã  campeÃ£ Ronda Rousey (ReproduÃ§Ã£o/Twitter)"
                data-original-image="s2.glbimg.com/75J5dPNr0DoWC7SbUe9gs0b5oUo=/124x182:532x454/120x80/s.glbimg.com/es/ge/f/original/2015/08/07/ronda-ufc-12rounds.jpg" data-url-smart_horizontal="ZszaZL-wIoT6cKqLF2h-E3uowps=/90x0/smart/filters:strip_icc()/" data-url-smart="ZszaZL-wIoT6cKqLF2h-E3uowps=/90x0/smart/filters:strip_icc()/" data-url-feature="ZszaZL-wIoT6cKqLF2h-E3uowps=/90x0/smart/filters:strip_icc()/" data-url-tablet="aOzpykcafZpJGGtsvbS33G3jSPU=/70x50/smart/filters:strip_icc()/" data-url-desktop="22OhkZj1ho9C_CyVSnr8cZlBWwU=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>UFC rebate Mayweather apÃ³s pugilista postar provocaÃ§Ã£o Ã  campeÃ£ Ronda Rousey</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-07 20:08:57" class="chamada chamada-principal mobile-grid-full"><a href="http://sportv.globo.com/site/combate/blogs/especial-blog/ultimmato/post/junior-cigano-provoca-fabricio-werdum-em-rede-social-ta-com-medinho.html" class="foto" title="Junior Cigano publica vÃ­deo em rede social para provocar Werdum: &#39;TÃ¡ com medinho?&#39; (Getty Images)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/LHjtmnw-UmZIBaoWxStnMShm-OI=/filters:quality(10):strip_icc()/s2.glbimg.com/auJ843Y8ErKoe9d4uGWXI_zxObE=/904x326:1884x980/120x80/s.glbimg.com/es/ge/f/original/2014/12/14/juniorcigano2-get.jpg" alt="Junior Cigano publica vÃ­deo em rede social para provocar Werdum: &#39;TÃ¡ com medinho?&#39; (Getty Images)" title="Junior Cigano publica vÃ­deo em rede social para provocar Werdum: &#39;TÃ¡ com medinho?&#39; (Getty Images)"
                data-original-image="s2.glbimg.com/auJ843Y8ErKoe9d4uGWXI_zxObE=/904x326:1884x980/120x80/s.glbimg.com/es/ge/f/original/2014/12/14/juniorcigano2-get.jpg" data-url-smart_horizontal="9etLRdBUrySA6LJuyG27fNDxHHo=/90x0/smart/filters:strip_icc()/" data-url-smart="9etLRdBUrySA6LJuyG27fNDxHHo=/90x0/smart/filters:strip_icc()/" data-url-feature="9etLRdBUrySA6LJuyG27fNDxHHo=/90x0/smart/filters:strip_icc()/" data-url-tablet="B8F9u7jrwiAwlDchCCgADEtMYaA=/70x50/smart/filters:strip_icc()/" data-url-desktop="xULecqJONIlMe_geYdujRWhkFkI=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Junior Cigano publica vÃ­deo<br /> em rede social para provocar Werdum: &#39;TÃ¡ com medinho?&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-07 20:18:31" class="chamada chamada-principal mobile-grid-full"><a href="http://epoca.globo.com/vida/esporte/noticia/2015/08/quem-substitui-nike-com-novo-modelo-santos-tomara-decisao-de-r-10-milhoes.html" class="" title="ConheÃ§a as quatro propostas que o Santos tem para substituir a atual fornecedora em 2016 ( ReproduÃ§Ã£o/ Instagram)" rel="bookmark"><span class="conteudo"><p>ConheÃ§a as quatro propostas que o Santos tem para substituir a atual fornecedora em 2016</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-07 17:32:10" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/futebol-internacional/jogo/07-08-2015/lille-parissaintgermain/index.html" class="foto" title="Com vaga ameaÃ§ada por Di MarÃ­a, Lucas marca, e PSG vence com menos um; vÃ­deo (SporTV)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/ShxSjQNX0ZHpagv2AxIQP_pr7ZU=/filters:quality(10):strip_icc()/s2.glbimg.com/p9XCeILozq3D_QRY8fboSA-FLRo=/363x142:685x357/120x80/s.glbimg.com/en/ho/f/original/2015/08/07/lucas.jpg" alt="Com vaga ameaÃ§ada por Di MarÃ­a, Lucas marca, e PSG vence com menos um; vÃ­deo (SporTV)" title="Com vaga ameaÃ§ada por Di MarÃ­a, Lucas marca, e PSG vence com menos um; vÃ­deo (SporTV)"
                data-original-image="s2.glbimg.com/p9XCeILozq3D_QRY8fboSA-FLRo=/363x142:685x357/120x80/s.glbimg.com/en/ho/f/original/2015/08/07/lucas.jpg" data-url-smart_horizontal="iEoAFBBqsHlPNCwGVOPHrh1WrpU=/90x0/smart/filters:strip_icc()/" data-url-smart="iEoAFBBqsHlPNCwGVOPHrh1WrpU=/90x0/smart/filters:strip_icc()/" data-url-feature="iEoAFBBqsHlPNCwGVOPHrh1WrpU=/90x0/smart/filters:strip_icc()/" data-url-tablet="C78UE8dDOLJhr0GUAjIX8V_cS-I=/70x50/smart/filters:strip_icc()/" data-url-desktop="-kuzZ-o-aHFqIcBTsT_GzGgV6GE=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Com vaga ameaÃ§ada por <br />Di MarÃ­a, Lucas marca, e PSG vence com menos um; vÃ­deo</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/fluminense/noticia/2015/08/de-roger-gerson-vendas-de-jovens-de-xerem-geraram-cerca-de-r-235-mi.html" class="foto" title="De Roger a Gerson: Flu soma R$ 235 mi em venda de joias da base nos Ãºltimos 15 anos (ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/sdpRYiBbmIEFk91B6_C_Tp1aeo4=/filters:quality(10):strip_icc()/s2.glbimg.com/sDbwFP6XclviAcKbeSmdnFtOXWg=/0x30:300x230/120x80/s.glbimg.com/es/ge/f/original/2012/02/08/untitled-4_33.jpg" alt="De Roger a Gerson: Flu soma R$ 235 mi em venda de joias da base nos Ãºltimos 15 anos (ReproduÃ§Ã£o)" title="De Roger a Gerson: Flu soma R$ 235 mi em venda de joias da base nos Ãºltimos 15 anos (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/sDbwFP6XclviAcKbeSmdnFtOXWg=/0x30:300x230/120x80/s.glbimg.com/es/ge/f/original/2012/02/08/untitled-4_33.jpg" data-url-smart_horizontal="8KRNWEDSWzrlptIFVpli4ffcmDI=/90x0/smart/filters:strip_icc()/" data-url-smart="8KRNWEDSWzrlptIFVpli4ffcmDI=/90x0/smart/filters:strip_icc()/" data-url-feature="8KRNWEDSWzrlptIFVpli4ffcmDI=/90x0/smart/filters:strip_icc()/" data-url-tablet="x2wH7bFEoahwxkPnw11lp0vHi6A=/70x50/smart/filters:strip_icc()/" data-url-desktop="tk5rxFtcAvl8r8z2D9IHEedWjQA=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>De Roger a Gerson: Flu soma R$ 235 mi em venda de joias da base nos Ãºltimos 15 anos</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-07 16:48:51" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/olimpiadas/noticia/2015/08/barbara-leoncio-tenta-resgatar-brilho-do-rosto-eternizado-na-escolha-do-rio.html" class="" title="Aos 23, velocista sÃ­mbolo da campanha Rio 2016 sonha com vaga apÃ³s passar de aposta a dÃºvida (GloboEsporte.com)" rel="bookmark"><span class="conteudo"><p>Aos 23, velocista sÃ­mbolo da campanha Rio 2016 sonha com vaga apÃ³s passar de aposta a dÃºvida</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-07 20:30:04" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/natacao/noticia/2015/08/empate-triplo-provoca-podio-inusitado-com-cinco-atletas-no-mundial-de-kazan.html" class="foto" title="Empate triplo provoca pÃ³dio inusitado com cinco atletas no Mundial de nataÃ§Ã£o; veja (Getty Images)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/m-pjD1RXSmGclWRdL2JOR9fonPs=/filters:quality(10):strip_icc()/s2.glbimg.com/fxECYi8naNQP0zNpMa7zouDepTg=/189x128:1777x1187/120x80/s.glbimg.com/es/ge/f/original/2015/08/07/gettyimages-483305692_1.jpg" alt="Empate triplo provoca pÃ³dio inusitado com cinco atletas no Mundial de nataÃ§Ã£o; veja (Getty Images)" title="Empate triplo provoca pÃ³dio inusitado com cinco atletas no Mundial de nataÃ§Ã£o; veja (Getty Images)"
                data-original-image="s2.glbimg.com/fxECYi8naNQP0zNpMa7zouDepTg=/189x128:1777x1187/120x80/s.glbimg.com/es/ge/f/original/2015/08/07/gettyimages-483305692_1.jpg" data-url-smart_horizontal="gi7fYWc1CaN9Z72etUQv3YOIBEc=/90x0/smart/filters:strip_icc()/" data-url-smart="gi7fYWc1CaN9Z72etUQv3YOIBEc=/90x0/smart/filters:strip_icc()/" data-url-feature="gi7fYWc1CaN9Z72etUQv3YOIBEc=/90x0/smart/filters:strip_icc()/" data-url-tablet="VxpLgoGJwl67H6wzmo0w11PhFuE=/70x50/smart/filters:strip_icc()/" data-url-desktop="h33NZ7GTrmMzCke668OCh2ecedM=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Empate triplo provoca pÃ³dio inusitado com cinco atletas<br /> no Mundial de nataÃ§Ã£o; veja</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-07 14:18:59" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/esporte/jornal-austriaco-divulga-fotos-de-kira-grunberg-musa-do-salto-com-vara-que-ficou-tetraplegica-17113885.html" class="foto" title="Jornal divulga fotos de atleta que ficou tetraplÃ©gica apÃ³s salto em cama de hospital (ReproduÃ§Ã£o / Kronen Zeitung)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/JKDo2NwKVfvtvdo6Kx-uCNJpnS4=/filters:quality(10):strip_icc()/s2.glbimg.com/xLOOz1s9lYqWxHXEAw-BLqqeAJU=/0x217:448x516/120x80/s.glbimg.com/en/ho/f/original/2015/08/07/austriaca.jpg" alt="Jornal divulga fotos de atleta que ficou tetraplÃ©gica apÃ³s salto em cama de hospital (ReproduÃ§Ã£o / Kronen Zeitung)" title="Jornal divulga fotos de atleta que ficou tetraplÃ©gica apÃ³s salto em cama de hospital (ReproduÃ§Ã£o / Kronen Zeitung)"
                data-original-image="s2.glbimg.com/xLOOz1s9lYqWxHXEAw-BLqqeAJU=/0x217:448x516/120x80/s.glbimg.com/en/ho/f/original/2015/08/07/austriaca.jpg" data-url-smart_horizontal="lySmEMc-4IgvxPCbfRb7vhDrBiI=/90x0/smart/filters:strip_icc()/" data-url-smart="lySmEMc-4IgvxPCbfRb7vhDrBiI=/90x0/smart/filters:strip_icc()/" data-url-feature="lySmEMc-4IgvxPCbfRb7vhDrBiI=/90x0/smart/filters:strip_icc()/" data-url-tablet="SaPYwpGjUak9ls7oURiy3R_W6sk=/70x50/smart/filters:strip_icc()/" data-url-desktop="zviYjxnFdKufsBrkcQZnvvoCCGQ=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Jornal divulga fotos de atleta que ficou tetraplÃ©gica apÃ³s salto em cama de hospital</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section><section class="area etc-column analytics-area   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/famosos/aos-63-anos-bruna-lombardi-sensualiza-de-camisola-ganha-elogios-coloca-muita-menininha-no-chinelo-17122542.html" class="foto" title="Aos 63 anos, Bruna Lombardi posa sÃ³ de lingerie e fÃ£s vÃ£o ao delÃ­rio na web (reproduÃ§Ã£o)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/SHyYy_f3k4ehat2HfsHGfkO3I1g=/filters:quality(10):strip_icc()/s2.glbimg.com/6CEKhtX7_Sz6tAoXWttZVBs83kI=/0x0:640x343/335x180/s.glbimg.com/en/ho/f/original/2015/08/07/bruna-lombardi-lingerie.jpg" alt="Aos 63 anos, Bruna Lombardi posa sÃ³ de lingerie e fÃ£s vÃ£o ao delÃ­rio na web (reproduÃ§Ã£o)" title="Aos 63 anos, Bruna Lombardi posa sÃ³ de lingerie e fÃ£s vÃ£o ao delÃ­rio na web (reproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/6CEKhtX7_Sz6tAoXWttZVBs83kI=/0x0:640x343/335x180/s.glbimg.com/en/ho/f/original/2015/08/07/bruna-lombardi-lingerie.jpg" data-url-smart_horizontal="LRw7YnonGpZwpB2jyXhHFeqbe2I=/90x0/smart/filters:strip_icc()/" data-url-smart="LRw7YnonGpZwpB2jyXhHFeqbe2I=/90x0/smart/filters:strip_icc()/" data-url-feature="LRw7YnonGpZwpB2jyXhHFeqbe2I=/90x0/smart/filters:strip_icc()/" data-url-tablet="TUEdMc5HbbYdLtpsS3ijQKwUl-A=/220x125/smart/filters:strip_icc()/" data-url-desktop="a6oOwyqWW-k_jsfz7K_XSDoRnRg=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Aos 63 anos, Bruna Lombardi posa sÃ³ de lingerie e fÃ£s vÃ£o ao delÃ­rio na web</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-07 18:59:43" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/08/denize-taccto-aos-43-anos-faz-tratamento-pra-levantar-bumbum.html" class="foto" title="Ex de Gerson Brenner faz &#39;pump&#39; no bumbum: &#39;Fiz para manter empinado&#39;; fotos (Thiago Tas / CauÃª Garcia (DivulgaÃ§Ã£o))" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/4WDQoOTUj5RHA2Yk6_yCdg2Bo98=/filters:quality(10):strip_icc()/s2.glbimg.com/Su7hwhfHfrEbQbrzfjsTkI01A_U=/65x531:1074x1203/120x80/s.glbimg.com/jo/eg/f/original/2015/08/07/img_7193.jpg" alt="Ex de Gerson Brenner faz &#39;pump&#39; no bumbum: &#39;Fiz para manter empinado&#39;; fotos (Thiago Tas / CauÃª Garcia (DivulgaÃ§Ã£o))" title="Ex de Gerson Brenner faz &#39;pump&#39; no bumbum: &#39;Fiz para manter empinado&#39;; fotos (Thiago Tas / CauÃª Garcia (DivulgaÃ§Ã£o))"
                data-original-image="s2.glbimg.com/Su7hwhfHfrEbQbrzfjsTkI01A_U=/65x531:1074x1203/120x80/s.glbimg.com/jo/eg/f/original/2015/08/07/img_7193.jpg" data-url-smart_horizontal="BVhRkhj-ywFVBZgabi39C0N0Cqg=/90x0/smart/filters:strip_icc()/" data-url-smart="BVhRkhj-ywFVBZgabi39C0N0Cqg=/90x0/smart/filters:strip_icc()/" data-url-feature="BVhRkhj-ywFVBZgabi39C0N0Cqg=/90x0/smart/filters:strip_icc()/" data-url-tablet="Rw6fkTEl6NnW50tij1COyO1WpcM=/70x50/smart/filters:strip_icc()/" data-url-desktop="FZ8164uR7ISORweMP4NOx934OTU=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Ex de Gerson Brenner faz &#39;pump&#39; no bumbum: &#39;Fiz para manter empinado&#39;; fotos</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-07 21:45:31" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/08/gravida-vice-miss-bumbum-posta-foto-estranha-e-pode-perder-o-titulo.html" class="foto" title="GrÃ¡vida? Vice Miss Bumbum levanta suspeita em foto na web e pode ficar sem o tÃ­tulo (ReproduÃ§Ã£o/Instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/HH2rGBq_Ze3Yw0R4t4FEdVz9AyI=/filters:quality(10):strip_icc()/s2.glbimg.com/olRDLsUoGKUPfYYL_C6G8PxK24A=/63x83:588x433/120x80/s.glbimg.com/jo/eg/f/original/2015/08/07/20150807165201.jpg" alt="GrÃ¡vida? Vice Miss Bumbum levanta suspeita em foto na web e pode ficar sem o tÃ­tulo (ReproduÃ§Ã£o/Instagram)" title="GrÃ¡vida? Vice Miss Bumbum levanta suspeita em foto na web e pode ficar sem o tÃ­tulo (ReproduÃ§Ã£o/Instagram)"
                data-original-image="s2.glbimg.com/olRDLsUoGKUPfYYL_C6G8PxK24A=/63x83:588x433/120x80/s.glbimg.com/jo/eg/f/original/2015/08/07/20150807165201.jpg" data-url-smart_horizontal="whOg9ulysr_0OGeH-MU47vSS168=/90x0/smart/filters:strip_icc()/" data-url-smart="whOg9ulysr_0OGeH-MU47vSS168=/90x0/smart/filters:strip_icc()/" data-url-feature="whOg9ulysr_0OGeH-MU47vSS168=/90x0/smart/filters:strip_icc()/" data-url-tablet="SXe1UmvtodVVJ9WdPfRUryZfpPE=/70x50/smart/filters:strip_icc()/" data-url-desktop="oKSpI2-rY8yPFi6GbMpl5UNywJI=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>GrÃ¡vida? Vice Miss Bumbum levanta suspeita em foto na web e pode ficar sem o tÃ­tulo</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-07 19:55:29" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/08/giullia-buscacio-de-i-love-paraisopolis-faz-ensaio-com-o-pai.html" class="" title="Atriz de &#39;I Love&#39; posa em ensaio com o pai, ex-jogador de futebol; veja as fotos (Drica Donato)" rel="bookmark"><span class="conteudo"><p>Atriz de &#39;I Love&#39; posa em ensaio com o pai, ex-jogador de futebol; veja as fotos</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-07 16:06:37" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/famosos/ex-bbb-aline-dahlen-assume-romance-com-sergio-mallandro-estamos-juntos-eu-gosto-dele-ele-de-mim-17117294.html" class="foto" title="ApÃ³s negar, ex-BBB Aline Dahlen assume que estÃ¡ 
com SÃ©rgio Mallandro (reproduÃ§Ã£o/instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/JIrzHJQM8rF6Ln-gbNoF-ugqNwY=/filters:quality(10):strip_icc()/s2.glbimg.com/5zm0LLeGoHIh-bpJzmfvnjh-1f4=/159x88:342x210/120x80/s.glbimg.com/en/ho/f/original/2015/08/07/aline_1.jpg" alt="ApÃ³s negar, ex-BBB Aline Dahlen assume que estÃ¡ 
com SÃ©rgio Mallandro (reproduÃ§Ã£o/instagram)" title="ApÃ³s negar, ex-BBB Aline Dahlen assume que estÃ¡ 
com SÃ©rgio Mallandro (reproduÃ§Ã£o/instagram)"
                data-original-image="s2.glbimg.com/5zm0LLeGoHIh-bpJzmfvnjh-1f4=/159x88:342x210/120x80/s.glbimg.com/en/ho/f/original/2015/08/07/aline_1.jpg" data-url-smart_horizontal="TOZqJ3ESl31q3WQz8I93W9QodSs=/90x0/smart/filters:strip_icc()/" data-url-smart="TOZqJ3ESl31q3WQz8I93W9QodSs=/90x0/smart/filters:strip_icc()/" data-url-feature="TOZqJ3ESl31q3WQz8I93W9QodSs=/90x0/smart/filters:strip_icc()/" data-url-tablet="0Fxfwc4cn_-CIgUsTta3mOYdSK8=/70x50/smart/filters:strip_icc()/" data-url-desktop="zQ5hyiw3gnLZtA2EzN9B0y3pcxI=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>ApÃ³s negar, ex-BBB Aline Dahlen assume que estÃ¡ <br />com SÃ©rgio Mallandro</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-07 21:46:39" class="chamada chamada-principal mobile-grid-full"><a href="http://revistaquem.globo.com/QUEM-News/noticia/2015/08/aos-37-anos-michelle-rodriguez-exibe-o-corpao-bordo-de-iate.html" class="foto" title="Chegando aos 40, atriz de &#39;Velozes e Furiosos&#39; Ã© clicada de biquÃ­ni em passeio de iate (AKM-GSI)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/oWSrFdgSCSVwMay3BqGoPtQUyx4=/filters:quality(10):strip_icc()/s2.glbimg.com/D3A1AnL0nG9VpD0i2834_S99vUo=/155x44:514x284/120x80/e.glbimg.com/og/ed/f/original/2015/08/07/michelle2.jpg" alt="Chegando aos 40, atriz de &#39;Velozes e Furiosos&#39; Ã© clicada de biquÃ­ni em passeio de iate (AKM-GSI)" title="Chegando aos 40, atriz de &#39;Velozes e Furiosos&#39; Ã© clicada de biquÃ­ni em passeio de iate (AKM-GSI)"
                data-original-image="s2.glbimg.com/D3A1AnL0nG9VpD0i2834_S99vUo=/155x44:514x284/120x80/e.glbimg.com/og/ed/f/original/2015/08/07/michelle2.jpg" data-url-smart_horizontal="p3MqgXeWGa5arxi2Zwi-n--mwJc=/90x0/smart/filters:strip_icc()/" data-url-smart="p3MqgXeWGa5arxi2Zwi-n--mwJc=/90x0/smart/filters:strip_icc()/" data-url-feature="p3MqgXeWGa5arxi2Zwi-n--mwJc=/90x0/smart/filters:strip_icc()/" data-url-tablet="MdjFUGUR1g3TJDVml0YpUSOv8i8=/70x50/smart/filters:strip_icc()/" data-url-desktop="hAWk536Rk8hS0-D89sVkIZ3nOvc=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Chegando aos 40, atriz de &#39;Velozes e Furiosos&#39; Ã© clicada de biquÃ­ni em passeio de iate</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-07 21:36:02" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/08/mais-magro-leandro-hassum-curte-dia-de-sol-de-sunga-em-piscina.html" class="" title="Mais magro, Leandro Hassum posta foto em clima de fÃ©rias curtindo piscininha em Miami (ReproduÃ§Ã£o/Instagram)" rel="bookmark"><span class="conteudo"><p>Mais magro, Leandro Hassum posta foto em clima de fÃ©rias curtindo piscininha em Miami</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-08-07 21:17:58" class="chamada chamada-principal mobile-grid-full"><a href="http://gshow.globo.com/Bastidores/noticia/2015/08/patricia-poeta-traz-objeto-do-marrocos-para-decorar-o-e-de-casa.html" class="foto" title="Poeta volta do Marrocos e mostra o que trouxe para o &#39;Ã de casa&#39;: &#39;Para boa sorte&#39; (Tv Globo/Gshow)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/OZw3bCyl5ZMsNdVfFS8BZhq_E6c=/filters:quality(10):strip_icc()/s2.glbimg.com/yh1A_ckciOJypUI2bLjLANWp9Bg=/117x0:1137x680/120x80/s.glbimg.com/et/gs/f/original/2015/08/07/patricia-poeta-compra-objet.jpg" alt="Poeta volta do Marrocos e mostra o que trouxe para o &#39;Ã de casa&#39;: &#39;Para boa sorte&#39; (Tv Globo/Gshow)" title="Poeta volta do Marrocos e mostra o que trouxe para o &#39;Ã de casa&#39;: &#39;Para boa sorte&#39; (Tv Globo/Gshow)"
                data-original-image="s2.glbimg.com/yh1A_ckciOJypUI2bLjLANWp9Bg=/117x0:1137x680/120x80/s.glbimg.com/et/gs/f/original/2015/08/07/patricia-poeta-compra-objet.jpg" data-url-smart_horizontal="eMcheVYaCdshuJCeNsJ-0mZWeuM=/90x0/smart/filters:strip_icc()/" data-url-smart="eMcheVYaCdshuJCeNsJ-0mZWeuM=/90x0/smart/filters:strip_icc()/" data-url-feature="eMcheVYaCdshuJCeNsJ-0mZWeuM=/90x0/smart/filters:strip_icc()/" data-url-tablet="G7cBbDZwnWCDam6GlZPkRRfcCrY=/70x50/smart/filters:strip_icc()/" data-url-desktop="0Vl5EksQxB6b2YIPsLtxWAowupg=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Poeta volta do Marrocos e mostra o que trouxe para o &#39;Ã de casa&#39;: &#39;Para boa sorte&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry publieditorial"><div data-photo-subtitle="2015-08-06 14:41:55" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/beleza/especial-publicitario/noticia/2015/08/lider-mundial-em-venda-direta-avon-se-consolida-no-mercado-brasileiro.html?utm_source=fake-banner&amp;utm_medium=chamada-editorial&amp;utm_term=15-08-07&amp;utm_content=Avon&amp;utm_campaign=Ego-Beleza" class="foto" title="CosmÃ©ticos: lÃ­der mundial, Avon se consolida no Brasil em venda direta (DivulgaÃ§Ã£o Avon)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/W2YtF-Jt1RUSzZ-sELITj2L5Z8Y=/filters:quality(10):strip_icc()/s2.glbimg.com/0Y8Vd5GNw0Fmwyb16h2FSplCyJ0=/0x14:682x469/120x80/s.glbimg.com/en/ho/f/original/2015/08/06/avon.jpg" alt="CosmÃ©ticos: lÃ­der mundial, Avon se consolida no Brasil em venda direta (DivulgaÃ§Ã£o Avon)" title="CosmÃ©ticos: lÃ­der mundial, Avon se consolida no Brasil em venda direta (DivulgaÃ§Ã£o Avon)"
                data-original-image="s2.glbimg.com/0Y8Vd5GNw0Fmwyb16h2FSplCyJ0=/0x14:682x469/120x80/s.glbimg.com/en/ho/f/original/2015/08/06/avon.jpg" data-url-smart_horizontal="AuHEztjudnDSmXue9zPSW17lDlo=/90x0/smart/filters:strip_icc()/" data-url-smart="AuHEztjudnDSmXue9zPSW17lDlo=/90x0/smart/filters:strip_icc()/" data-url-feature="AuHEztjudnDSmXue9zPSW17lDlo=/90x0/smart/filters:strip_icc()/" data-url-tablet="wLyw25M3HJ2g6hOsy_kffBKIACU=/70x50/smart/filters:strip_icc()/" data-url-desktop="yUUJrTfKHL9N4Z592CH9XcKU9Wo=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><span>ESPECIAL PUBLICITÃRIO</span><p>CosmÃ©ticos: lÃ­der mundial, Avon se consolida no Brasil em venda direta</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section></div><div id="ad-position-middle3" class="opec"><div id="banner_slb_fim" class="tag-manager-publicidade-container"></div></div><div class="container columns clearfix glb-area-colunas second-area"><section class="area news-column"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-noticias
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}});}
</script><div id="libby-home-widget-economia" class="libby-home-widgetBase libby-home-widget-economia analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais notÃ­cias" href="http://g1.globo.com/"><span class="text">mais notÃ­cias</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area sports-column"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-times soccer-teams
"></div><div id="libby-home-widget-brasileirao" class="libby-home-widgetBase libby-home-widget-brasileirao analytics-area analytics-id-W"><input type="hidden" name="rodada_selecionada" value="current"></div><a class="link-see-more analytics-area analytics-id-L" title="mais esportes" href="http://globoesporte.globo.com/"><span class="text">mais esportes</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area etc-column analytics-area"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-novelas
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2014/index.html", "logo": "http://s2.glbimg.com/kX8ylHaY-BqxGG2GesdMgR7SxQc=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2014/07/15/malhacao-2014_38x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2014/rss", "logo_tv": "http://s2.glbimg.com/tKm4gbARSp2eHSq6zr2alVV82hc=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2014/07/15/malhacao-2014_45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/wDQDURw8tQncX6dLdavJtiUCwkE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/07/14/logo38x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/APKhqTvAHfh9O2TFUkXyG2JQqdw=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/07/14/logo45x30.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "I Love Parais\u00f3polis", "url": "http://gshow.globo.com/novelas/i-love-paraisopolis/index.html", "logo": "http://s2.glbimg.com/ChecsIkfHlI4H_yHoe_c_6tXTA4=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis36x20.jpg", "slug": "alto-astral", "url_feed": "http://gshow.globo.com/novelas/i-love-paraisopolis/rss/", "logo_tv": "http://s2.glbimg.com/6Kj2EtayTr5_t0ya4Iy8HHIn-vg=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "Babil\u00f4nia", "url": "http://gshow.globo.com/novelas/babilonia/index.html", "logo": "http://s2.glbimg.com/Y5g2FJUyYReCohIkHaT8oSfmpcM=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/03/16/logo_36x20.png", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/babilonia/rss", "logo_tv": "http://s2.glbimg.com/X2KQ_AbGfD9t-AOIC6IYxCp422g=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/03/16/logo_45x30_1.png"}, {"ordering": 70, "default": false, "cor": "#FF7F00", "name": "Verdades Secretas", "url": "http://gshow.globo.com/novelas/verdades-secretas/index.html", "logo": "http://s2.glbimg.com/ouhqJ9mLuAXh-gcEUW8wk165DUQ=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_1.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/verdades-secretas/rss", "logo_tv": "http://s2.glbimg.com/Huyh0y1AcqgjDpRaBLWCCkQY-Oo=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_2.jpg"}]);}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2014/index.html", "logo": "http://s2.glbimg.com/kX8ylHaY-BqxGG2GesdMgR7SxQc=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2014/07/15/malhacao-2014_38x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2014/rss", "logo_tv": "http://s2.glbimg.com/tKm4gbARSp2eHSq6zr2alVV82hc=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2014/07/15/malhacao-2014_45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/wDQDURw8tQncX6dLdavJtiUCwkE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/07/14/logo38x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/APKhqTvAHfh9O2TFUkXyG2JQqdw=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/07/14/logo45x30.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "I Love Parais\u00f3polis", "url": "http://gshow.globo.com/novelas/i-love-paraisopolis/index.html", "logo": "http://s2.glbimg.com/ChecsIkfHlI4H_yHoe_c_6tXTA4=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis36x20.jpg", "slug": "alto-astral", "url_feed": "http://gshow.globo.com/novelas/i-love-paraisopolis/rss/", "logo_tv": "http://s2.glbimg.com/6Kj2EtayTr5_t0ya4Iy8HHIn-vg=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "Babil\u00f4nia", "url": "http://gshow.globo.com/novelas/babilonia/index.html", "logo": "http://s2.glbimg.com/Y5g2FJUyYReCohIkHaT8oSfmpcM=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/03/16/logo_36x20.png", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/babilonia/rss", "logo_tv": "http://s2.glbimg.com/X2KQ_AbGfD9t-AOIC6IYxCp422g=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/03/16/logo_45x30_1.png"}, {"ordering": 70, "default": false, "cor": "#FF7F00", "name": "Verdades Secretas", "url": "http://gshow.globo.com/novelas/verdades-secretas/index.html", "logo": "http://s2.glbimg.com/ouhqJ9mLuAXh-gcEUW8wk165DUQ=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_1.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/verdades-secretas/rss", "logo_tv": "http://s2.glbimg.com/Huyh0y1AcqgjDpRaBLWCCkQY-Oo=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_2.jpg"}]);}});}
</script><div id="libby-home-widget-horoscopo" class="libby-home-widgetBase libby-home-widget-horoscopo analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais entretenimento" href="http://variedades.globo.com/"><span class="text">mais variedades</span><span class="arrow">&rsaquo;</span></a></div></section></div><section class="area central container analytics-area analytics-id-O"><div class="separator"></div><section class="agrupador-quadruplo-tecnologia analytics-area analytics-id-T clearfix franja-inferior "><div class="cabecalho"><h2><a href="http://www.techtudo.com.br/" title="tecnologia &amp; games"><span class="word word-0">tecnologia</span><span class="word word-1">&</span><span class="word word-2">games</span></a></h2><div id="ad-position-x34" class="opec"><div id="banner_selo4" class="tag-manager-publicidade-container"></div></div><aside class="links analytics-area analytics-id-T"><ul><li><a href="http://www.techtudo.com.br/jogos/" title="jogos">jogos</a></li><li><a href="http://www.techtudo.com.br/downloads/" title="downloads">downloads</a></li><li><a href="http://forum.techtudo.com.br/" title="fÃ³rum">fÃ³rum</a></li><li><a href="http://www.techtudo.com.br/mobile/" title="celulares">celulares</a></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaque"><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/listas/noticia/2015/08/conheca-as-tecnologias-que-nasceram-no-cinema-e-ja-sao-realidade.html" title="Veja os aparelhos &#39;incrÃ­veis&#39; dos filmes que viraram realidade hoje"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/1JwnQfxSy7TinAkP9Dsjk5TQihM=/filters:quality(10):strip_icc()/s2.glbimg.com/gelPE-xDpWp2gRNOmsewU4rkhyc=/15x0:500x257/245x130/s.glbimg.com/po/tt2/f/original/2015/08/07/tumblr_lzd1m6zshr1qc7jsy.jpg" alt="Veja os aparelhos &#39;incrÃ­veis&#39; dos filmes que viraram realidade hoje (ReproduÃ§Ã£o/YouTube)" title="Veja os aparelhos &#39;incrÃ­veis&#39; dos filmes que viraram realidade hoje (ReproduÃ§Ã£o/YouTube)"
             data-original-image="s2.glbimg.com/gelPE-xDpWp2gRNOmsewU4rkhyc=/15x0:500x257/245x130/s.glbimg.com/po/tt2/f/original/2015/08/07/tumblr_lzd1m6zshr1qc7jsy.jpg" data-url-smart_horizontal="F4KaTKfswGRdjbZPGNtUQpQWdsU=/90x56/smart/filters:strip_icc()/" data-url-smart="F4KaTKfswGRdjbZPGNtUQpQWdsU=/90x56/smart/filters:strip_icc()/" data-url-feature="F4KaTKfswGRdjbZPGNtUQpQWdsU=/90x56/smart/filters:strip_icc()/" data-url-tablet="lseZ1Q6wsaYoL5pZ0U9YbqG7OHo=/160x96/smart/filters:strip_icc()/" data-url-desktop="9La8p6Y1dhIfLvlakuMjdVFrC34=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Veja os aparelhos &#39;incrÃ­veis&#39; dos <br />filmes que viraram realidade hoje</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/listas/noticia/2015/08/confira-melhores-curiosidades-sobre-franquia-mario-kart.html" title="Mario Kart: conheÃ§a as maiores 
&#39;bizarrices&#39; do game de corrida"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/saXzbk3JxMrBQeAvkoyD10X1afQ=/filters:quality(10):strip_icc()/s2.glbimg.com/q0QHuRtLrZJ9ASJoeNowhSZFyw8=/0x152:695x521/245x130/s.glbimg.com/po/tt2/f/original/2015/08/04/2483713-scene_1024x768.jpg" alt="Mario Kart: conheÃ§a as maiores 
&#39;bizarrices&#39; do game de corrida (Mario Kart: confira as curiosidades sobre a sÃ©rie (Foto: ReproduÃ§Ã£o/Giant Bomb))" title="Mario Kart: conheÃ§a as maiores 
&#39;bizarrices&#39; do game de corrida (Mario Kart: confira as curiosidades sobre a sÃ©rie (Foto: ReproduÃ§Ã£o/Giant Bomb))"
             data-original-image="s2.glbimg.com/q0QHuRtLrZJ9ASJoeNowhSZFyw8=/0x152:695x521/245x130/s.glbimg.com/po/tt2/f/original/2015/08/04/2483713-scene_1024x768.jpg" data-url-smart_horizontal="0GeSzcFV3AX_GXBgNiGY5Ek4WWg=/90x56/smart/filters:strip_icc()/" data-url-smart="0GeSzcFV3AX_GXBgNiGY5Ek4WWg=/90x56/smart/filters:strip_icc()/" data-url-feature="0GeSzcFV3AX_GXBgNiGY5Ek4WWg=/90x56/smart/filters:strip_icc()/" data-url-tablet="IwRO1oX6MYPBN48M_TpNF7kjUys=/160x96/smart/filters:strip_icc()/" data-url-desktop="oHtBXTs5S5Dq-JCuGDX_7YJ7yds=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Mario Kart: conheÃ§a as maiores <br />&#39;bizarrices&#39; do game de corrida</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/noticias/noticia/2015/08/galaxy-note-5-e-s6-edge-plus-cartaz-de-evento-da-pistas-sobre-novos-foblets.html" title="Smarts &#39;top&#39; da Samsung ganham novas pistas; veja o que esperar"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/H9nDi7eoaZBixeEJrDA70eWdekw=/filters:quality(10):strip_icc()/s2.glbimg.com/-5pwDVe-5gTt07ejakyCKIQrCHc=/17x0:672x347/245x130/s.glbimg.com/po/tt2/f/original/2015/08/07/galaxy_note_edge_12.jpg" alt="Smarts &#39;top&#39; da Samsung ganham novas pistas; veja o que esperar (TechTudo)" title="Smarts &#39;top&#39; da Samsung ganham novas pistas; veja o que esperar (TechTudo)"
             data-original-image="s2.glbimg.com/-5pwDVe-5gTt07ejakyCKIQrCHc=/17x0:672x347/245x130/s.glbimg.com/po/tt2/f/original/2015/08/07/galaxy_note_edge_12.jpg" data-url-smart_horizontal="C5dBxyrxKPmh4czN_mPbefJbseQ=/90x56/smart/filters:strip_icc()/" data-url-smart="C5dBxyrxKPmh4czN_mPbefJbseQ=/90x56/smart/filters:strip_icc()/" data-url-feature="C5dBxyrxKPmh4czN_mPbefJbseQ=/90x56/smart/filters:strip_icc()/" data-url-tablet="T9Eew2_8lLFMMDvlRrZzlKK6OxU=/160x96/smart/filters:strip_icc()/" data-url-desktop="a69sLQ5pN_Q2dUPm60bGhszpLoo=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Smarts &#39;top&#39; da Samsung ganham <br />novas pistas; veja o que esperar</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://forum.techtudo.com.br/perguntas/197411/moto-x-style-e-moto-g3-o-que-voces-estao-achando" title="Vale a pena comprar os novos Moto G e X? UsuÃ¡rios opinam"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/oys67WwSVh1DXmb1hYwYZOHCC4A=/filters:quality(10):strip_icc()/s2.glbimg.com/NZMrYzYuNC_ojqjWe_mAEGQ-lPU=/0x43:695x412/245x130/s.glbimg.com/po/tt2/f/original/2015/08/05/moto_g_3_sete1.jpg" alt="Vale a pena comprar os novos Moto G e X? UsuÃ¡rios opinam (O TechTudo testou o Moto G 3, novo smart intermediÃ¡rio da Motorola (Foto: Nicolly Vimercate/TechTudo))" title="Vale a pena comprar os novos Moto G e X? UsuÃ¡rios opinam (O TechTudo testou o Moto G 3, novo smart intermediÃ¡rio da Motorola (Foto: Nicolly Vimercate/TechTudo))"
             data-original-image="s2.glbimg.com/NZMrYzYuNC_ojqjWe_mAEGQ-lPU=/0x43:695x412/245x130/s.glbimg.com/po/tt2/f/original/2015/08/05/moto_g_3_sete1.jpg" data-url-smart_horizontal="qWhyL_pDCyS_Mqezs312dNC9Dgo=/90x56/smart/filters:strip_icc()/" data-url-smart="qWhyL_pDCyS_Mqezs312dNC9Dgo=/90x56/smart/filters:strip_icc()/" data-url-feature="qWhyL_pDCyS_Mqezs312dNC9Dgo=/90x56/smart/filters:strip_icc()/" data-url-tablet="Kg7nRfqlgTH0GhmnKZjPbdvodRY=/160x96/smart/filters:strip_icc()/" data-url-desktop="HmwQriiGKAEcCnNLjRU1H6HIZTc=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Vale a pena comprar os novos <br />Moto G e X? UsuÃ¡rios opinam</p></span></a></li></ul><div class="destaque-inferior analytics-area analytics-id-B"><ul><li class="techtudo"><a href="http://www.techtudo.com.br/" title="Teste agora a velocidade da sua internet"><span class="logo"></span><span class="title">Teste agora a velocidade da sua internet &rsaquo;</span></a></li><li class="exty"><a href="http://www.techtudo.com.br/tudo-sobre/extensao-globo-com.html" title="Adicione e veja as notÃ­cias em tempo real"><span class="logo"></span><span class="title">Adicione e veja as notÃ­cias em tempo real &rsaquo;</span></a></li></ul></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-tecnologia']);</script><div class="separator"></div><section class="agrupador-sextuplo analytics-area analytics-id-S"><section class="analytics-area analytics-id-E esquerda tvg"><div class="cabecalho"><h2><span class="word word-0">moda</span><span class="word word-1">&amp;</span><span class="word word-2">beleza</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://revistamarieclaire.globo.com/Beleza/noticia/2015/08/megahair-de-sobrancelhas-conheca-tecnica-que-promete-um-resultado-natural-e-sem-dor.html" alt="Megahair de sobrancelhas: conheÃ§a a tÃ©cnica que promete resultado natural"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/903pMxqpal8XEqhfpd0tdAskI2c=/filters:quality(10):strip_icc()/s2.glbimg.com/H_bUUT2WSvueELerYuZFXTl00rk=/0x0:620x399/155x100/e.glbimg.com/og/ed/f/original/2015/08/06/sobrancelha.jpg" alt="Megahair de sobrancelhas: conheÃ§a a tÃ©cnica que promete resultado natural (Thinkstock)" title="Megahair de sobrancelhas: conheÃ§a a tÃ©cnica que promete resultado natural (Thinkstock)"
            data-original-image="s2.glbimg.com/H_bUUT2WSvueELerYuZFXTl00rk=/0x0:620x399/155x100/e.glbimg.com/og/ed/f/original/2015/08/06/sobrancelha.jpg" data-url-smart_horizontal="fIzpuyWPByZ3oguTMsr6qGcHTFg=/90x56/smart/filters:strip_icc()/" data-url-smart="fIzpuyWPByZ3oguTMsr6qGcHTFg=/90x56/smart/filters:strip_icc()/" data-url-feature="fIzpuyWPByZ3oguTMsr6qGcHTFg=/90x56/smart/filters:strip_icc()/" data-url-tablet="jchbCCtToY6nwqGgOmzmMq9rQgM=/122x75/smart/filters:strip_icc()/" data-url-desktop="olCbI2DgqsWOvKKHsdGN9CO689M=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>sem dor</h3><p>Megahair de sobrancelhas: conheÃ§a a tÃ©cnica que promete resultado natural</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://gnt.globo.com/bem-estar/materias/veja-como-engordar-sem-comprometer-saude.htm" alt="Especialistas dÃ£o dicas para aumentar seu peso sem comprometer a saÃºde"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/6OC9yDgKDx3fm8zbMsR0_LrTbwM=/filters:quality(10):strip_icc()/s2.glbimg.com/XBGkQpT9OO0Sve7pp0hniuaOQEA=/119x10:597x318/155x100/s.glbimg.com/en/ho/f/original/2015/08/07/ganhar_peso.jpg" alt="Especialistas dÃ£o dicas para aumentar seu peso sem comprometer a saÃºde (ReproduÃ§Ã£o / Getty Images)" title="Especialistas dÃ£o dicas para aumentar seu peso sem comprometer a saÃºde (ReproduÃ§Ã£o / Getty Images)"
            data-original-image="s2.glbimg.com/XBGkQpT9OO0Sve7pp0hniuaOQEA=/119x10:597x318/155x100/s.glbimg.com/en/ho/f/original/2015/08/07/ganhar_peso.jpg" data-url-smart_horizontal="yZ_qwgJZlk1EjxTVFqbfmPB0dg0=/90x56/smart/filters:strip_icc()/" data-url-smart="yZ_qwgJZlk1EjxTVFqbfmPB0dg0=/90x56/smart/filters:strip_icc()/" data-url-feature="yZ_qwgJZlk1EjxTVFqbfmPB0dg0=/90x56/smart/filters:strip_icc()/" data-url-tablet="U62s45G2LZWemmpUbOxJ7PWBdC4=/122x75/smart/filters:strip_icc()/" data-url-desktop="WZ-XSXUCkaBZvXg_AOZdmz_7Mn0=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>dificuldade para engordar?</h3><p>Especialistas dÃ£o dicas para aumentar seu peso  sem comprometer a saÃºde</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://vogue.globo.com/beleza/pele/noticia/2015/08/noiva-um-roteiro-para-pele-perfeita-no-dia-do-casamento.html" alt="Roteiro de tratamentos de um mÃªs ajuda a chegar Ã  pele ideal ao casar"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/phSdtpMGjCzfklo6yK9yphu6Lrg=/filters:quality(10):strip_icc()/s2.glbimg.com/NmXzqN0lAISwXcPAHWYYlgdxQPc=/454x261:1442x899/155x100/e.glbimg.com/og/ed/f/original/2015/06/16/noiva23_beleza_tratamentos68-1.jpg" alt="Roteiro de tratamentos de um mÃªs ajuda a chegar Ã  pele ideal ao casar (Zee Nunes/arquivo Vogue Brasil)" title="Roteiro de tratamentos de um mÃªs ajuda a chegar Ã  pele ideal ao casar (Zee Nunes/arquivo Vogue Brasil)"
            data-original-image="s2.glbimg.com/NmXzqN0lAISwXcPAHWYYlgdxQPc=/454x261:1442x899/155x100/e.glbimg.com/og/ed/f/original/2015/06/16/noiva23_beleza_tratamentos68-1.jpg" data-url-smart_horizontal="NpD_gvKH4GpS6Yv1ef7Hba2Nw3w=/90x56/smart/filters:strip_icc()/" data-url-smart="NpD_gvKH4GpS6Yv1ef7Hba2Nw3w=/90x56/smart/filters:strip_icc()/" data-url-feature="NpD_gvKH4GpS6Yv1ef7Hba2Nw3w=/90x56/smart/filters:strip_icc()/" data-url-tablet="RjbaVCqN0Ym2C7DcvSNUQ2xxskY=/122x75/smart/filters:strip_icc()/" data-url-desktop="JD79rvLt6IZE3gBwNR_z2LKrh8Q=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>pele incrÃ­vel no dia do &#39;sim&#39;</h3><p>Roteiro de tratamentos de um mÃªs ajuda a chegar Ã  pele ideal ao casar</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://gnt.globo.com/" title="GNT">GNT</a></h3></li><li><h3><a href="http://vogue.globo.com/" title="vogue">vogue</a></h3></li><li></li></ul></aside></section><div class="separator"></div><section class="analytics-area analytics-id-D direita tvg"><div class="cabecalho"><h2><span class="word word-0">CASA</span><span class="word word-1">&amp;</span><span class="word word-2">DECORAÃÃO</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://revista.zapimoveis.com.br/entenda-a-diferenca-entre-mofo-e-bolor-e-veja-como-preveni-los-em-casa-4197247-sc/" alt="Entenda a diferenÃ§a entre mofo e bolor e saiba como preveni-los em casa"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/24OH014vdhYvzM5j1M-uDno769c=/filters:quality(10):strip_icc()/s2.glbimg.com/j2qs0enTkEBDu9Fb-AugmmGGpFM=/0x12:580x387/155x100/s.glbimg.com/en/ho/f/original/2015/08/07/roupas-no-armario_1.jpg" alt="Entenda a diferenÃ§a entre mofo e bolor e saiba como preveni-los em casa (Shutterstock)" title="Entenda a diferenÃ§a entre mofo e bolor e saiba como preveni-los em casa (Shutterstock)"
            data-original-image="s2.glbimg.com/j2qs0enTkEBDu9Fb-AugmmGGpFM=/0x12:580x387/155x100/s.glbimg.com/en/ho/f/original/2015/08/07/roupas-no-armario_1.jpg" data-url-smart_horizontal="bHMsZcXGC-r5jYoR0Ae_8fHnWLg=/90x56/smart/filters:strip_icc()/" data-url-smart="bHMsZcXGC-r5jYoR0Ae_8fHnWLg=/90x56/smart/filters:strip_icc()/" data-url-feature="bHMsZcXGC-r5jYoR0Ae_8fHnWLg=/90x56/smart/filters:strip_icc()/" data-url-tablet="CxWbYPb3KY3QpwxJGcFNXM3BdoU=/122x75/smart/filters:strip_icc()/" data-url-desktop="enZCfnYG1SLK9-6pHFLwy-gLckM=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>roupas sequinhas</h3><p>Entenda a diferenÃ§a entre mofo e bolor e saiba como preveni-los em casa</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revistacasaejardim.globo.com/Casa-e-Jardim/Decoracao/Ambientes/noticia/2013/05/tipo-uma-casa.html" alt="Projeto que inclui varanda com ares de quintal deixa apÃª com cara de casa"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/cTVf5UfsVfEJ3uuaZJm9jVjd_xU=/filters:quality(10):strip_icc()/s2.glbimg.com/YixFXWyCXriBhUd_xFZmi32cnng=/0x0:2000x1291/155x100/e.glbimg.com/og/ed/f/original/2013/05/28/cj683decogabriela_150.jpg" alt="Projeto que inclui varanda com ares de quintal deixa apÃª com cara de casa (Victor Affaro)" title="Projeto que inclui varanda com ares de quintal deixa apÃª com cara de casa (Victor Affaro)"
            data-original-image="s2.glbimg.com/YixFXWyCXriBhUd_xFZmi32cnng=/0x0:2000x1291/155x100/e.glbimg.com/og/ed/f/original/2013/05/28/cj683decogabriela_150.jpg" data-url-smart_horizontal="hxcjUZ50lyzRzHK8UY6CDI6xZnI=/90x56/smart/filters:strip_icc()/" data-url-smart="hxcjUZ50lyzRzHK8UY6CDI6xZnI=/90x56/smart/filters:strip_icc()/" data-url-feature="hxcjUZ50lyzRzHK8UY6CDI6xZnI=/90x56/smart/filters:strip_icc()/" data-url-tablet="LlaqKm1HHBYMKTd__A0o2QwKbcY=/122x75/smart/filters:strip_icc()/" data-url-desktop="ZE3_63VGYHKIo8KdEj9G6OaV1S4=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>Com tijolos Ã  vista</h3><p>Projeto que inclui varanda com ares<br /> de quintal deixa apÃª com cara de casa</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://casavogue.globo.com/Interiores/casas/noticia/2015/08/casa-mansao-madonna-vend.html" alt="Casa de Madonna com 15 banheiros e cinema estÃ¡ a venda por US$ 30 mi"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/NxFRcaK9elzlRHw1TB0MFornJB8=/filters:quality(10):strip_icc()/s2.glbimg.com/sDuV84PdWd7lwc91JjWULvZ-iUM=/0x0:620x400/155x100/e.glbimg.com/og/ed/f/original/2015/08/05/casa-madonna-sunset-boulevard--02.jpg" alt="Casa de Madonna com 15 banheiros e cinema estÃ¡ a venda por US$ 30 mi (TopTenRealEstateDeals / divulga)" title="Casa de Madonna com 15 banheiros e cinema estÃ¡ a venda por US$ 30 mi (TopTenRealEstateDeals / divulga)"
            data-original-image="s2.glbimg.com/sDuV84PdWd7lwc91JjWULvZ-iUM=/0x0:620x400/155x100/e.glbimg.com/og/ed/f/original/2015/08/05/casa-madonna-sunset-boulevard--02.jpg" data-url-smart_horizontal="tMdh-UpVKlXWZg1Q4BhiR43YMVQ=/90x56/smart/filters:strip_icc()/" data-url-smart="tMdh-UpVKlXWZg1Q4BhiR43YMVQ=/90x56/smart/filters:strip_icc()/" data-url-feature="tMdh-UpVKlXWZg1Q4BhiR43YMVQ=/90x56/smart/filters:strip_icc()/" data-url-tablet="Ypbw087ZZ6oTkybx_xrLgp4upgM=/122x75/smart/filters:strip_icc()/" data-url-desktop="Pl1bm-GLiAt7Nw1H1WC6dQ9BtZQ=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>palacete onde cantora viveu com Guy Ritchie</h3><p>Casa de Madonna com 15 banheiros <br />e cinema estÃ¡ a venda por US$ 30 mi</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://revistacasaejardim.globo.com/" title="CASA E JARDIM">CASA E JARDIM</a></h3></li><li><h3><a href="http://casavogue.globo.com/" title="CASA VOGUE">CASA VOGUE</a></h3></li><li></li></ul></aside></section></section><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior famosos-style"><div class="cabecalho"><h2><a title="famosos"><span class='logo'>famosos</span><span class='texto'>famosos</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://ego.globo.com/">ego</a></h3></li><li class=""><h3><a href="http://revistaquem.globo.com/">quem</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://revistaquem.globo.com/QUEM-News/noticia/2015/08/nanda-costa-muda-o-visual-e-adota-cabelos-curtos-e-loiros.html" title="Nanda Costa muda o visual e fica com madeixas curtas e loiras"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/iktJVn-TjS4gjkJ5AI5Gqu34oZ0=/filters:quality(10):strip_icc()/s2.glbimg.com/IPSZNOnpyufYMSm5aj8HQ8RL2OQ=/0x147:640x486/245x130/e.glbimg.com/og/ed/f/original/2015/08/07/nanda.jpg" alt="Nanda Costa muda o visual e fica com madeixas curtas e loiras (ReproduÃ§Ã£o/Instagram)" title="Nanda Costa muda o visual e fica com madeixas curtas e loiras (ReproduÃ§Ã£o/Instagram)"
                    data-original-image="s2.glbimg.com/IPSZNOnpyufYMSm5aj8HQ8RL2OQ=/0x147:640x486/245x130/e.glbimg.com/og/ed/f/original/2015/08/07/nanda.jpg" data-url-smart_horizontal="4BkDNPcqLIR6Tf96cIPg8G9XiMg=/90x56/smart/filters:strip_icc()/" data-url-smart="4BkDNPcqLIR6Tf96cIPg8G9XiMg=/90x56/smart/filters:strip_icc()/" data-url-feature="4BkDNPcqLIR6Tf96cIPg8G9XiMg=/90x56/smart/filters:strip_icc()/" data-url-tablet="2G6q65iEcmyvzEwcBHZ5hpDoCVI=/160x95/smart/filters:strip_icc()/" data-url-desktop="Zx6we9GitvD8t79wtCaEJ9yemKE=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Nanda Costa muda o visual e fica com madeixas curtas e loiras</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gq.globo.com/Musa/noticia/2015/08/nina-adgal-deixa-roupas-de-lado-em-ensaio-para-revista.html" title="Dinamarquesa deixa as roupas de lado em ensaio para revista"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/AOix-0l6DLuuO7ZoM9B9uSk5yB0=/filters:quality(10):strip_icc()/s2.glbimg.com/VfFC4uQpKUNewlrWkMZYGJTNRkg=/34x2:617x311/245x130/s.glbimg.com/en/ho/f/original/2015/08/07/nina.jpg" alt="Dinamarquesa deixa as roupas de lado em ensaio para revista (globo.com)" title="Dinamarquesa deixa as roupas de lado em ensaio para revista (globo.com)"
                    data-original-image="s2.glbimg.com/VfFC4uQpKUNewlrWkMZYGJTNRkg=/34x2:617x311/245x130/s.glbimg.com/en/ho/f/original/2015/08/07/nina.jpg" data-url-smart_horizontal="xq8XHl_QLUSEbgcDemV_xFZSjy8=/90x56/smart/filters:strip_icc()/" data-url-smart="xq8XHl_QLUSEbgcDemV_xFZSjy8=/90x56/smart/filters:strip_icc()/" data-url-feature="xq8XHl_QLUSEbgcDemV_xFZSjy8=/90x56/smart/filters:strip_icc()/" data-url-tablet="YdYbIrTBoHFKFQY1E4nSEolWQhg=/160x95/smart/filters:strip_icc()/" data-url-desktop="-fvNaIsioKyAvL2IQP9xhnCDAkc=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Dinamarquesa deixa as roupas de lado em ensaio para revista</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://revistacrescer.globo.com/Pais-famosos/fotos/2015/08/maes-famosas-compartilham-fotos-amamentando-os-filhos.html#foto-1" title="FÃª Machado e mais famosas apoiam amamentaÃ§Ã£o com fotos"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/mOTQZ1al2Gyt3FF9GFLG12uNEjw=/filters:quality(10):strip_icc()/s2.glbimg.com/8nndb7pfyL_trEeh2EPbLziUAJA=/0x287:1080x860/245x130/e.glbimg.com/og/ed/f/original/2015/08/07/11420857_1673889452832800_471843773_n.jpg" alt="FÃª Machado e mais famosas apoiam amamentaÃ§Ã£o com fotos (ReproduÃ§Ã£o/Instagram)" title="FÃª Machado e mais famosas apoiam amamentaÃ§Ã£o com fotos (ReproduÃ§Ã£o/Instagram)"
                    data-original-image="s2.glbimg.com/8nndb7pfyL_trEeh2EPbLziUAJA=/0x287:1080x860/245x130/e.glbimg.com/og/ed/f/original/2015/08/07/11420857_1673889452832800_471843773_n.jpg" data-url-smart_horizontal="wDkze_w9wU4jlz1X1DE1Iq96_4U=/90x56/smart/filters:strip_icc()/" data-url-smart="wDkze_w9wU4jlz1X1DE1Iq96_4U=/90x56/smart/filters:strip_icc()/" data-url-feature="wDkze_w9wU4jlz1X1DE1Iq96_4U=/90x56/smart/filters:strip_icc()/" data-url-tablet="hKZ_PT-yMlLOqx6fGOXWKjPHEo4=/160x95/smart/filters:strip_icc()/" data-url-desktop="NNyefZEKdhs4xgeolBG1AYwan8s=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>FÃª Machado e mais famosas apoiam amamentaÃ§Ã£o com fotos</p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://ego.globo.com/famosos/noticia/2015/08/dani-sperle-reforca-o-bronzeado-e-mostra-marquinha-em-frente-e-verso.html" title="Dani Sperle mostra quase tudo em dia de bronzeamento; amplie"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/ZHiInw-kGaV0LmSZzBj9rPUsrOc=/filters:quality(10):strip_icc()/s2.glbimg.com/UUfNa3aqlJkGb1h48doBT2-DZF8=/0x0:640x339/245x130/s.glbimg.com/jo/eg/f/original/2015/08/07/11356462_589970614474688_84406344_n.jpg" alt="Dani Sperle mostra quase tudo em dia de bronzeamento; amplie (ReproduÃ§Ã£o/Instagram)" title="Dani Sperle mostra quase tudo em dia de bronzeamento; amplie (ReproduÃ§Ã£o/Instagram)"
                    data-original-image="s2.glbimg.com/UUfNa3aqlJkGb1h48doBT2-DZF8=/0x0:640x339/245x130/s.glbimg.com/jo/eg/f/original/2015/08/07/11356462_589970614474688_84406344_n.jpg" data-url-smart_horizontal="ph348WnptxJJ0C26AtIA1Q026IU=/90x56/smart/filters:strip_icc()/" data-url-smart="ph348WnptxJJ0C26AtIA1Q026IU=/90x56/smart/filters:strip_icc()/" data-url-feature="ph348WnptxJJ0C26AtIA1Q026IU=/90x56/smart/filters:strip_icc()/" data-url-tablet="7gxwBIgTHCmqaU_xD5Tb5EbyJCc=/160x95/smart/filters:strip_icc()/" data-url-desktop="LAUXxvxK9vm0zyvYVHADHj7fbS8=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Dani Sperle mostra quase tudo em dia de bronzeamento; amplie</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_famosos-style']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior gshow"><div class="cabecalho"><h2><a href="http://gshow.globo.com/" title="novelas, sÃ©ries, programas e muito mais"><span class='logo'>novelas, sÃ©ries, programas e muito mais</span><span class='texto'>novelas, sÃ©ries, programas e muito mais</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://gshow.globo.com/estilo-tv/">ESTILO TV</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/babilonia/index.html">BABILÃNIA</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/i-love-paraisopolis/index.html">I LOVE PARAISÃPOLIS</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/alem-do-tempo/">ALÃM DO TEMPO</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/malhacao/2014/index.html">MALHAÃÃO</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/i-love-paraisopolis/Vem-por-ai/noticia/2015/08/mari-se-aproxima-do-irmao-joaquim.html" title="Em &#39;I Love ParaisÃ³polis&#39;, Mari se aproxima do irmÃ£o, Joaquim"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/KK_Qc5mIF20NQUvU2ldSyjg5l6s=/filters:quality(10):strip_icc()/s2.glbimg.com/jo3HLEZiQboF1J3fhGuncjNXOhA=/59x55:678x384/245x130/s.glbimg.com/et/gs/f/original/2015/08/06/mari-e-joaquim-1.jpg" alt="Em &#39;I Love ParaisÃ³polis&#39;, Mari se aproxima do irmÃ£o, Joaquim (Ellen Soares/Gshow)" title="Em &#39;I Love ParaisÃ³polis&#39;, Mari se aproxima do irmÃ£o, Joaquim (Ellen Soares/Gshow)"
                    data-original-image="s2.glbimg.com/jo3HLEZiQboF1J3fhGuncjNXOhA=/59x55:678x384/245x130/s.glbimg.com/et/gs/f/original/2015/08/06/mari-e-joaquim-1.jpg" data-url-smart_horizontal="kRreEskgXD-jA_oJ5z6DNMq6Wao=/90x56/smart/filters:strip_icc()/" data-url-smart="kRreEskgXD-jA_oJ5z6DNMq6Wao=/90x56/smart/filters:strip_icc()/" data-url-feature="kRreEskgXD-jA_oJ5z6DNMq6Wao=/90x56/smart/filters:strip_icc()/" data-url-tablet="8cfK3mVNHbNgphOGlPLpEzt4vqs=/160x95/smart/filters:strip_icc()/" data-url-desktop="t9cDAmceOM9ielT6DwDXiuzJB2Y=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Em &#39;I Love ParaisÃ³polis&#39;, Mari se aproxima do irmÃ£o, Joaquim</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/alem-do-tempo/vem-por-ai/noticia/2015/08/roberto-provoca-situacao-constrangedora-com-afonso.html" title="&#39;AlÃ©m&#39;: Roberto provoca situaÃ§Ã£o constrangedora com Afonso"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/3yYxTC9NuBa1X74xyLx73QcMy50=/filters:quality(10):strip_icc()/s2.glbimg.com/TRe1tDPl2UT-lG0nRCxhyzuTLDI=/0x29:690x396/245x130/s.glbimg.com/et/gs/f/original/2015/08/06/roberto-romulo-estrela_1.jpg" alt="&#39;AlÃ©m&#39;: Roberto provoca situaÃ§Ã£o constrangedora com Afonso (TV Globo)" title="&#39;AlÃ©m&#39;: Roberto provoca situaÃ§Ã£o constrangedora com Afonso (TV Globo)"
                    data-original-image="s2.glbimg.com/TRe1tDPl2UT-lG0nRCxhyzuTLDI=/0x29:690x396/245x130/s.glbimg.com/et/gs/f/original/2015/08/06/roberto-romulo-estrela_1.jpg" data-url-smart_horizontal="8nBhKCZ8ymPOkkqdv67jQ_0RItg=/90x56/smart/filters:strip_icc()/" data-url-smart="8nBhKCZ8ymPOkkqdv67jQ_0RItg=/90x56/smart/filters:strip_icc()/" data-url-feature="8nBhKCZ8ymPOkkqdv67jQ_0RItg=/90x56/smart/filters:strip_icc()/" data-url-tablet="NdZE1qR_LDnOiNf0tIwnhdpWnt4=/160x95/smart/filters:strip_icc()/" data-url-desktop="7IMytmR7T7HpSQupDwJcuMm-d4c=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>&#39;AlÃ©m&#39;: Roberto provoca situaÃ§Ã£o constrangedora com Afonso</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/malhacao/2014/Vem-por-ai/noticia/2015/08/ultimos-capitulos-mestre-flagra-amasso-perina.html" title="Ãltimos capÃ­tulos: Mestre flagra amasso de Perina em &#39;MalhaÃ§Ã£o&#39;"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/KnXOrqaVQw6_682iIlUiCl5QVxk=/filters:quality(10):strip_icc()/s2.glbimg.com/XCs7SgYmJBqQ9Im2Vw3989lRXjU=/56x125:509x365/245x130/s.glbimg.com/et/gs/f/original/2015/07/31/perina-no-amasso.jpg" alt="Ãltimos capÃ­tulos: Mestre flagra amasso de Perina em &#39;MalhaÃ§Ã£o&#39; (Gshow)" title="Ãltimos capÃ­tulos: Mestre flagra amasso de Perina em &#39;MalhaÃ§Ã£o&#39; (Gshow)"
                    data-original-image="s2.glbimg.com/XCs7SgYmJBqQ9Im2Vw3989lRXjU=/56x125:509x365/245x130/s.glbimg.com/et/gs/f/original/2015/07/31/perina-no-amasso.jpg" data-url-smart_horizontal="utMeAYyBfTAkNDExBgg1VIlncY8=/90x56/smart/filters:strip_icc()/" data-url-smart="utMeAYyBfTAkNDExBgg1VIlncY8=/90x56/smart/filters:strip_icc()/" data-url-feature="utMeAYyBfTAkNDExBgg1VIlncY8=/90x56/smart/filters:strip_icc()/" data-url-tablet="rkBEOkDB4JmCxrAfBGnBwDn25xM=/160x95/smart/filters:strip_icc()/" data-url-desktop="AQOI7jnPeFL9Q6gT8fLozL0s3Dw=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Ãltimos capÃ­tulos: Mestre flagra amasso de Perina em &#39;MalhaÃ§Ã£o&#39;</p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://gshow.globo.com/tv/noticia/2015/08/publico-vibra-com-elenco-e-nova-chamada-de-regra-do-jogo.html" title="PÃºblico vibra com chamada de novela do autor de &#39;Avenida&#39;"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/W3zSVJEA-EpRuzqQCoTC5iVXRpU=/filters:quality(10):strip_icc()/s2.glbimg.com/FurDz1GV-uZPEeRjpJimUfUgq6w=/0x0:483x256/245x130/s.glbimg.com/og/rg/f/original/2015/08/06/ret.jpg" alt="PÃºblico vibra com chamada de novela do autor de &#39;Avenida&#39; (divulgaÃ§Ã£o)" title="PÃºblico vibra com chamada de novela do autor de &#39;Avenida&#39; (divulgaÃ§Ã£o)"
                    data-original-image="s2.glbimg.com/FurDz1GV-uZPEeRjpJimUfUgq6w=/0x0:483x256/245x130/s.glbimg.com/og/rg/f/original/2015/08/06/ret.jpg" data-url-smart_horizontal="CCIl3PAmtMgZu07sD5uyzsvPyBA=/90x56/smart/filters:strip_icc()/" data-url-smart="CCIl3PAmtMgZu07sD5uyzsvPyBA=/90x56/smart/filters:strip_icc()/" data-url-feature="CCIl3PAmtMgZu07sD5uyzsvPyBA=/90x56/smart/filters:strip_icc()/" data-url-tablet="Eg6Tm_8NZiFi11ZpVlfe8ELJVAI=/160x95/smart/filters:strip_icc()/" data-url-desktop="ooGAbDqyNY7JSoAd4U8FL0jrqug=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>PÃºblico vibra com chamada de novela do autor de &#39;Avenida&#39;</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_gshow']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-musica-triplo analytics-area analytics-id-U franja-inferior"><div class="cabecalho"><h2><a href="http://musica.com.br/" title="MÃSICA">
            MÃSICA
        </a></h2><aside class="links analytics-area analytics-id-T"><ul><li class="mobile-grid-full "><h3><a href="http://musica.com.br/">MÃSICA.COM.BR</a></h3></li><li class="mobile-grid-full "><h3><a href="http://g1.globo.com/musica/">G1 MÃSICA</a></h3></li><li class="mobile-grid-full "><h3><a href="http://multishow.globo.com/musica/">MULTISHOW</a></h3></li><li class="mobile-grid-full "><h3><a href="http://radiobeat.com.br/">RADIOBEAT</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://extra.globo.com/famosos/jorge-mateus-mantem-cache-mais-alto-ha-mais-de-um-ano-lucas-lucco-teve-aumento-de-100-saiba-quanto-ganham-os-sertanejos-17111836.html" title="Com dupla Jorge e Mateus na lideranÃ§a, jornal lista o
 cachÃª dos sertanejos no paÃ­s (ReproduÃ§Ã£o/ Instagram)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/s8OGzMvFQme35Yliz19UAgLKAQE=/filters:quality(10):strip_icc()/s2.glbimg.com/HSHIxW9z7I2fBp6h7LxQS_sFf4g=/0x31:448x302/215x130/s.glbimg.com/en/ho/f/original/2015/08/07/jorge-mateus-platina.jpg"
                data-original-image="s2.glbimg.com/HSHIxW9z7I2fBp6h7LxQS_sFf4g=/0x31:448x302/215x130/s.glbimg.com/en/ho/f/original/2015/08/07/jorge-mateus-platina.jpg" data-url-smart_horizontal="or6dGCZuiHBlfv1C1zYli6mNy6Q=/90x56/smart/filters:strip_icc()/" data-url-smart="or6dGCZuiHBlfv1C1zYli6mNy6Q=/90x56/smart/filters:strip_icc()/" data-url-feature="or6dGCZuiHBlfv1C1zYli6mNy6Q=/90x56/smart/filters:strip_icc()/" data-url-tablet="BS31zJ2mhqdx85ex-pS5zzjrfbo=/120x80/smart/filters:strip_icc()/" data-url-desktop="A9zymGNaoX4wcodZWa3bSDbPPok=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Com dupla Jorge e Mateus na lideranÃ§a, jornal lista o<br /> cachÃª dos sertanejos no paÃ­s</div></div></a></li><li class="mobile-grid-full"><a class="foto" href="http://extra.globo.com/tv-e-lazer/finalistas-do-superstar-festejam-primeiro-reencontro-apos-reality-em-show-no-rio-17109872.html" title="Finalistas do &#39;SuperStar&#39; festejam 1Âº reencontro apÃ³s o reality em show no Rio (ReproduÃ§Ã£o)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/ILqqoHECPq3O0TueEMDoVCTZW0M=/filters:quality(10):strip_icc()/s2.glbimg.com/ow5hXjrhJUahLfGQJoB2aF1jQ7A=/22x0:618x360/215x130/s.glbimg.com/en/ho/f/original/2015/08/07/superstar.jpg"
                data-original-image="s2.glbimg.com/ow5hXjrhJUahLfGQJoB2aF1jQ7A=/22x0:618x360/215x130/s.glbimg.com/en/ho/f/original/2015/08/07/superstar.jpg" data-url-smart_horizontal="PowWi1VHblMK5E116_UHg-I_bvA=/90x56/smart/filters:strip_icc()/" data-url-smart="PowWi1VHblMK5E116_UHg-I_bvA=/90x56/smart/filters:strip_icc()/" data-url-feature="PowWi1VHblMK5E116_UHg-I_bvA=/90x56/smart/filters:strip_icc()/" data-url-tablet="OaIOGihbwU9xtn9E_RTGbAE3eDk=/120x80/smart/filters:strip_icc()/" data-url-desktop="vM0tqZ-jdLhqdzXSJK9uAHCXCrI=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Finalistas do &#39;SuperStar&#39; festejam 1Âº reencontro apÃ³s<br /> o reality em show no Rio</div></div></a></li><li class="mobile-grid-full"><a class="foto" href="http://oglobo.globo.com/cultura/musica/emicida-mergulha-nas-semelhancas-entre-africa-brasil-em-novo-disco-17108480" title="Emicida mergulha nas semelhanÃ§as entre Ãfrica e Brasil em novo disco (Michel Filho / AgÃªncia O Globo)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/msNstDthHheO7xfAsE9jJeVqhHg=/filters:quality(10):strip_icc()/s2.glbimg.com/3WJ2w_ogBG8ttrCoCcXxHP_eSok=/326x27:1046x462/215x130/s.glbimg.com/en/ho/f/original/2015/08/07/emicida.jpg"
                data-original-image="s2.glbimg.com/3WJ2w_ogBG8ttrCoCcXxHP_eSok=/326x27:1046x462/215x130/s.glbimg.com/en/ho/f/original/2015/08/07/emicida.jpg" data-url-smart_horizontal="Y5BEV7e1MN1zUZSIF4hwpJAlxkw=/90x56/smart/filters:strip_icc()/" data-url-smart="Y5BEV7e1MN1zUZSIF4hwpJAlxkw=/90x56/smart/filters:strip_icc()/" data-url-feature="Y5BEV7e1MN1zUZSIF4hwpJAlxkw=/90x56/smart/filters:strip_icc()/" data-url-tablet="wJSg9C8ZUON0v7ug4iC0URQfBcA=/120x80/smart/filters:strip_icc()/" data-url-desktop="I9z49yn3a59EF6_C4Tq29dtRUo0=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Emicida mergulha nas semelhanÃ§as entre Ãfrica<br /> e Brasil em novo disco</div></div></a></li></ul><div class="top-container analytics-area analytics-id-L mobile-grid-full"><h3><a href="http://musica.com.br/">
            TOP HITS
        </a></h3><ul><li><a class="foto" href="http://musica.com.br/artistas/luan-santana/m/escreve-ai/letra.html"><div class="area-foto"><div class="number">1</div><img width="45" height="45" src="http://s2.glbimg.com/DejQr6wzwaLjEToKO0IBZYMpp9E=/filters:quality(10):strip_icc()/s2.glbimg.com/gffJ6jy3x9TdEFawxnsku-K_Igs=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/12/20/artista-65018-2013151926.jpg"
                data-original-image="s2.glbimg.com/gffJ6jy3x9TdEFawxnsku-K_Igs=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/12/20/artista-65018-2013151926.jpg" data-url-smart_horizontal="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-smart="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-feature="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-tablet="jBcm2gM8Cbg_Vkcg-0n1BAJmuKE=/60x60/smart/filters:strip_icc()/" data-url-desktop="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Luan Santana</div><div class="title">Escreve AÃ­</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li><li><a class="foto" href="http://musica.com.br/artistas/mc-anitta/m/no-meu-talento-part-mc-guime/letra.html"><div class="area-foto"><div class="number">2</div><img width="45" height="45" src="http://s2.glbimg.com/yHnJPHom5FPA3tk69JCcIphdNJE=/filters:quality(10):strip_icc()/s2.glbimg.com/i6tbAg-HNpqidGWWFJqVJ788g6o=/65x65/smart/s.glbimg.com/po/ms/f/original/2014/03/28/anitta-nova-cut.jpg"
                data-original-image="s2.glbimg.com/i6tbAg-HNpqidGWWFJqVJ788g6o=/65x65/smart/s.glbimg.com/po/ms/f/original/2014/03/28/anitta-nova-cut.jpg" data-url-smart_horizontal="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-smart="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-feature="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-tablet="AODuTqXeNCV61xTQZim-SUSZDBg=/60x60/smart/filters:strip_icc()/" data-url-desktop="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Anitta</div><div class="title">No Meu Talento (part. MC GuimÃª)</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li><li class="last-top"><a class="foto" href="http://musica.com.br/artistas/jorge-e-mateus/m/nocaute/letra.html"><div class="area-foto"><div class="number">3</div><img width="45" height="45" src="http://s2.glbimg.com/jdC6lPjKcSpWF8v5jzFy0uuol6c=/filters:quality(10):strip_icc()/s2.glbimg.com/JgDLLARwYvvuCkqHPgmPue-pxa8=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/06/26/jorge.jpg"
                data-original-image="s2.glbimg.com/JgDLLARwYvvuCkqHPgmPue-pxa8=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/06/26/jorge.jpg" data-url-smart_horizontal="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-smart="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-feature="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-tablet="EfTWuRDHHuk_LklhIijQiqahj3A=/60x60/smart/filters:strip_icc()/" data-url-desktop="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Jorge e Mateus</div><div class="title">Nocaute</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li></ul></div><div class="separator"></div><div class="widget analytics-area analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-musica-triplo']);</script></section><section class="area topglobocom"><div class="container area"><div class="separator"></div><div id="topglobocom3" class="clearfix destaque topglobocom3"><div class="topo"><h3 class="titulo">
            top <strong>globo</strong><span class="subtitulo">tÃ¡ todo mundo clicando...</span></h3></div><div id="mais-vistas" class="clearfix mais-vistas tab-content analytics-area analytics-id-R"><div class="slider"><ol class="noticias analytics-area analytics-id-J"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/sp/bauru-marilia/noticia/2015/08/licenca-maternidade-e-passada-para-pai-de-gemeos-apos-morte-da-esposa.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">LicenÃ§a maternidade Ã© passada para pai de gÃªmeos apÃ³s morte da esposa no parto</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/carros/noticia/2015/08/mercedes-benz-poe-7-mil-em-licenca-e-paralisa-fabrica-no-abc.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Mercedes-Benz deixa 7 mil funcionÃ¡rios em licenÃ§a e paralisa fÃ¡brica no ABC</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/rs/rio-grande-do-sul/noticia/2015/08/conviviam-com-meu-filho-diz-pai-de-jovem-morto-no-rs-sobre-suspeitos.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">&#39;Conviviam com meu filho&#39;, diz pai de <br />jovem morto no RS sobre suspeitos</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/politica/noticia/2015/08/cidades-registram-protestos-durante-programa-do-pt-com-dilma-e-lula.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">VÃ¡rias cidades tÃªm panelaÃ§o durante programa do PT na televisÃ£o; assista</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globosatplay.globo.com/gnt/v/4372227/" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Homem e duas mulheres contam como Ã© viver como &#39;casal a trÃªs&#39; no Brasil; assista</span></span></a></div></div></li></ol><ol class="esportes analytics-area analytics-id-E"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/natacao/noticia/2015/08/menina-de-10-anos-fica-em-ultimo-mas-rouba-cena-no-mundial-de-kazan.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Atleta de 10 anos Ã© Ãºltima, mas rouba cena no Mundial de nataÃ§Ã£o em Kazan</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://sportv.globo.com/site/combate/noticia/2015/08/mayweather-responde-ronda-me-liga-quando-fizer-us-300-mi-em-36-min.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Mayweather reage e provoca Ronda: &#39;Me liga quando ganhar US$ 300 mi em 36m&#39;</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/motor/formula-1/noticia/2015/08/ladroes-dopam-button-e-sua-esposa-e-roubam-anel-de-r-13-milhao.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Button e esposa sÃ£o dopados, e anel de <br />R$ 1,3 milhÃ£o Ã© levado por ladrÃµes</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://sportv.globo.com/site/programas/sportv-news/noticia/2015/08/muito-assediado-guerrero-comenta-todo-mundo-no-rio-e-torcedor-do-fla.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Muito assediado, Guerrero comenta: &#39;Todo mundo no Rio Ã© torcedor do Flamengo&#39;</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://extra.globo.com/esporte/vasco/torcida-do-vasco-ganha-nova-musa-jessica-garducci-mulher-do-meia-atacante-nene-17103817.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Torcida do Vasco ganha nova musa: Jessica Garducci, mulher do meia-atacante NenÃª</span></span></a></div></div></li></ol><ol class="entretenimento analytics-area analytics-id-M"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://revistaquem.globo.com/QUEM-News/noticia/2015/08/ronaldo-e-celina-locks-namoram-muito-em-dia-de-praia.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Ronaldo e Celina Locks namoram muito em dia de praia na Espanha; veja as fotos</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/ego-teen/noticia/2015/08/lourdes-maria-usa-look-de-gosto-duvidoso-em-passeio-com-o-namorado.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Filha de Madonna veste calÃ§a de moletom e <br />deixa calcinha Ã  mostra em passeio</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://gshow.globo.com/novelas/babilonia/Vem-por-ai/noticia/2015/08/regina-causa-confusao-ao-chegar-para-julgamento.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Em &#39;BabilÃ´nia&#39;, Regina causa confusÃ£o entre jornalistas ao chegar para julgamento</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://kogut.oglobo.globo.com/noticias-da-tv/noticia/2015/08/kadu-moliterno-volta-tv-no-vai-que-cola-e-fala-da-parceria-profissional-com-namorada-fisiculturista-ja-e-um-casamento.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Kadu Moliterno fala sobre vida de &#39;casal fitness&#39; com a namorada fisiculturista</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://kogut.oglobo.globo.com/noticias-da-tv/novelas/noticia/2015/08/cris-sera-presa-e-pagara-por-todos-os-crimes-que-cometeu.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Cris serÃ¡ presa e pagarÃ¡ por todos os crimes que cometeu em &#39;BabilÃ´nia&#39;</span></span></a></div></div></li></ol></div></div><div class="controls-area"><ul class="step-marker"><li class=" glb-hl-style-noticias"><div class=""><a href="#" data-related=""><div class="ball active"></div></a></div></li><li class=" glb-hl-style-esporte"><div class=""><a href="#" data-related=""><div class="ball"></div></a></div></li><li class=" glb-hl-style-entretenimento"><div class=""><a href="#" data-related=""><div class="ball "></div></a></div></li></ul></div></div><section class="top-videos analytics-area analytics-id-R"><h1><a href="http://globotv.globo.com/">globo.tv</a></h1><div class="separator-line"></div><ul class="destaque analytics-area analytics-id-V"><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">1</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">2</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">3</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">4</span><span class="conteudo"><p>descr</p></span></a></li><li class="ult"><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">5</span><span class="conteudo"><p>descr</p></span></a></li></ul></section></div></section><section class="area globo-servicos container analytics-area analytics-id-C"><h2 class="titulo">serviÃ§os</h2><div class="borda"><span class="rodape"></span></div><div class="content"><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-loterias
" 
    data-lotteries-url="http://api.globo.com/lotteries/megasena.jsonp"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-fipe
"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-zap
" 
    data-zap-url="http://api.globo.com/zap/search/"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"></div></div></section><section class="area widgets_inferior franja-inferior container analytics-area analytics-id-C"><div class="box-links-servicos"><div class="links"><p>MAIS SERVIÃOS</p><a href="http://revistapegn.globo.com/Franquias/compare/">Abra uma franquia</a><a href="http://globoesporte.globo.com/eu-atleta/calculo-imc.html">AvaliaÃ§Ã£o fÃ­sica</a><a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">Conversor de moedas</a><a href="http://www.techtudo.com.br/softwares/">Downloads</a><a href="http://g1.globo.com/previsao-do-tempo/">PrevisÃ£o do tempo</a><a href="http://g1.globo.com/radar-g1/platb/">TrÃ¢nsito</a></div></div><div id="glb-shopping-widget" class="area globoshopping container" data-url="http://vitrines.globo.com/vitrine/vitrine.min.js" data-sitepage="globocom/home" data-responsive="0" data-branding-color="0669DE"></div></section><section class="area diretorio"><div class="container analytics-area analytics-id-Z"><div class="diretorio-responsive"><a href="/todos-os-sites.html">veja todos os sites da <strong>globo.com</strong> âº</a></div><div id="glb-diretorio"><div class="glb-conteudo"><div class="glb-bloco"><div class="glb-grid-12"><ul class="diretorio-indice"><li class="diretorio-linha diretorio-first "><ul><li class="diretorio-first-level analytics-product glb-menu-g1 diretorio-quebra "><a title="g1" href="http://g1.globo.com/">g1</a><ul><li class="diretorio-second-level"><a title="Acre" href="http://g1.globo.com/ac/acre/">Acre</a></li><li class="diretorio-second-level"><a title="Alagoas" href="http://g1.globo.com/al/alagoas/">Alagoas</a></li><li class="diretorio-second-level"><a title="Amazonas" href="http://g1.globo.com/am/amazonas/">Amazonas</a></li><li class="diretorio-second-level"><a title="Bahia" href="http://g1.globo.com/bahia/?noAudience=true">Bahia</a></li><li class="diretorio-second-level"><a title="Bem Estar" href="http://g1.globo.com/bemestar/">Bem Estar</a></li><li class="diretorio-second-level"><a title="Bom Dia Brasil" href="http://g1.globo.com/bom-dia-brasil/">Bom Dia Brasil</a></li><li class="diretorio-second-level"><a title="Brasil" href="http://g1.globo.com/brasil/">Brasil</a></li><li class="diretorio-second-level"><a title="Carnaval" href="http://g1.globo.com/carnaval/2013/">Carnaval</a></li><li class="diretorio-second-level"><a title="Carros" href="http://g1.globo.com/carros/">Carros</a></li><li class="diretorio-second-level"><a title="CearÃ¡" href="http://g1.globo.com/ceara/">CearÃ¡</a></li><li class="diretorio-second-level"><a title="CiÃªncia e SaÃºde" href="http://g1.globo.com/ciencia-e-saude/">CiÃªncia e SaÃºde</a></li><li class="diretorio-second-level"><a title="Como SerÃ¡?" href="http://redeglobo.globo.com/como-sera/index.html">Como SerÃ¡?</a></li><li class="diretorio-second-level"><a title="Concursos e Emprego" href="http://g1.globo.com/concursos-e-emprego/">Concursos e Emprego</a></li><li class="diretorio-second-level"><a title="Distrito Federal" href="http://g1.globo.com/distrito-federal/">Distrito Federal</a></li><li class="diretorio-second-level"><a title="Economia" href="http://g1.globo.com/economia/">Economia</a></li><li class="diretorio-second-level"><a title="EducaÃ§Ã£o" href="http://www.educacao.globo.com/">EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="EspÃ­rito Santo" href="http://g1.globo.com/espirito-santo/">EspÃ­rito Santo</a></li><li class="diretorio-second-level"><a title="FantÃ¡stico" href="http://fantastico.globo.com/">FantÃ¡stico</a></li><li class="diretorio-second-level"><a title="G1 EducaÃ§Ã£o" href="http://g1.globo.com/educacao/">G1 EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Globo Natureza" href="http://g1.globo.com/natureza/">Globo Natureza</a></li><li class="diretorio-second-level"><a title="Globo News" href="http://globonews.globo.com/">Globo News</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://g1.globo.com/economia/agronegocios/">Globo Rural</a></li><li class="diretorio-second-level"><a title="Globo RepÃ³rter" href="http://g1.globo.com/globo-reporter/">Globo RepÃ³rter</a></li><li class="diretorio-second-level"><a title="GoiÃ¡s" href="http://g1.globo.com/goias/">GoiÃ¡s</a></li><li class="diretorio-second-level"><a title="Jornal da Globo" href="http://g1.globo.com/jornal-da-globo/">Jornal da Globo</a></li><li class="diretorio-second-level"><a title="Jornal Hoje" href="http://g1.globo.com/jornal-hoje/">Jornal Hoje</a></li><li class="diretorio-second-level"><a title="Jornal Nacional" href="http://g1.globo.com/jornal-nacional/">Jornal Nacional</a></li><li class="diretorio-second-level"><a title="MaranhÃ£o" href="http://g1.globo.com/ma/maranhao/">MaranhÃ£o</a></li><li class="diretorio-second-level"><a title="Mato Grosso" href="http://g1.globo.com/mato-grosso/">Mato Grosso</a></li><li class="diretorio-second-level"><a title="Mato Grosso do Sul" href="http://g1.globo.com/mato-grosso-do-sul/">Mato Grosso do Sul</a></li><li class="diretorio-second-level"><a title="Minas Gerais" href="http://g1.globo.com/minas-gerais/">Minas Gerais</a></li><li class="diretorio-second-level"><a title="Mundo" href="http://g1.globo.com/mundo/">Mundo</a></li><li class="diretorio-second-level"><a title="ParÃ¡" href="http://g1.globo.com/pa/para/">ParÃ¡</a></li><li class="diretorio-second-level"><a title="ParaÃ­ba" href="http://g1.globo.com/paraiba/">ParaÃ­ba</a></li><li class="diretorio-second-level"><a title="ParanÃ¡" href="http://g1.globo.com/parana/">ParanÃ¡</a></li><li class="diretorio-second-level"><a title="Pernambuco" href="http://g1.globo.com/pernambuco/">Pernambuco</a></li><li class="diretorio-second-level"><a title="PiauÃ­" href="http://g1.globo.com/pi/piaui/">PiauÃ­</a></li><li class="diretorio-second-level"><a title="Planeta Bizarro" href="http://g1.globo.com/planeta-bizarro/">Planeta Bizarro</a></li><li class="diretorio-second-level"><a title="PolÃ­tica" href="http://g1.globo.com/politica/">PolÃ­tica</a></li><li class="diretorio-second-level"><a title="Pop &amp; Arte" href="http://g1.globo.com/pop-arte/">Pop &amp; Arte</a></li><li class="diretorio-second-level"><a title="PrevisÃ£o do Tempo" href="http://g1.globo.com/previsao-do-tempo.html">PrevisÃ£o do Tempo</a></li><li class="diretorio-second-level"><a title="ProfissÃ£o RepÃ³rter" href="http://g1.globo.com/profissao-reporter/">ProfissÃ£o RepÃ³rter</a></li><li class="diretorio-second-level"><a title="Rio de Janeiro" href="http://g1.globo.com/rio-de-janeiro/">Rio de Janeiro</a></li><li class="diretorio-second-level"><a title="Rio Grande do Norte" href="http://g1.globo.com/rn/rio-grande-do-norte/">Rio Grande do Norte</a></li><li class="diretorio-second-level"><a title="Rio Grande do Sul" href="http://g1.globo.com/rs/rio-grande-do-sul/">Rio Grande do Sul</a></li><li class="diretorio-second-level"><a title="RondÃ´nia" href="http://g1.globo.com/ro/rondonia/">RondÃ´nia</a></li><li class="diretorio-second-level"><a title="Santa Catarina" href="http://g1.globo.com/sc/santa-catarina/">Santa Catarina</a></li><li class="diretorio-second-level"><a title="SÃ£o Paulo" href="http://g1.globo.com/sao-paulo/">SÃ£o Paulo</a></li><li class="diretorio-second-level"><a title="Sergipe" href="http://g1.globo.com/se/sergipe/">Sergipe</a></li><li class="diretorio-second-level"><a title="Serra, Lagos e Norte" href="http://g1.globo.com/rj/serra-lagos-norte/">Serra, Lagos e Norte</a></li><li class="diretorio-second-level"><a title="Tecnologia e Games" href="http://g1.globo.com/tecnologia/">Tecnologia e Games</a></li><li class="diretorio-second-level"><a title="Turismo e Viagem" href="http://g1.globo.com/turismo-e-viagem/">Turismo e Viagem</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-globoesporte diretorio-quebra "><a title="globoesporte" href="http://globoesporte.globo.com/">globoesporte</a><ul><li class="diretorio-second-level"><a title="Atletismo" href="http://globoesporte.globo.com/atletismo/">Atletismo</a></li><li class="diretorio-second-level"><a title="Basquete" href="http://globoesporte.globo.com/basquete/">Basquete</a></li><li class="diretorio-second-level"><a title="Basquete/NBB" href="http://globoesporte.globo.com/basquete/nbb/">Basquete/NBB</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie A" href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">BrasileirÃ£o SÃ©rie A</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie B" href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">BrasileirÃ£o SÃ©rie B</a></li><li class="diretorio-second-level"><a title="Campeonato Carioca" href="http://globoesporte.globo.com/rj/futebol/campeonato-carioca/index.html">Campeonato Carioca</a></li><li class="diretorio-second-level"><a title="Campeonato GaÃºcho" href="http://globoesporte.globo.com/rs/futebol/campeonato-gaucho/index.html">Campeonato GaÃºcho</a></li><li class="diretorio-second-level"><a title="Campeonato Mineiro" href="http://globoesporte.globo.com/mg/futebol/campeonato-mineiro/index.html">Campeonato Mineiro</a></li><li class="diretorio-second-level"><a title="Campeonato Paulista" href="http://globoesporte.globo.com/sp/futebol/campeonato-paulista/index.html">Campeonato Paulista</a></li><li class="diretorio-second-level"><a title="Cartola FC" href="http://globoesporte.globo.com/cartola-fc/">Cartola FC</a></li><li class="diretorio-second-level"><a title="Copa do Mundo da Fifa" href="http://globoesporte.globo.com/futebol/copa-do-mundo/index.html">Copa do Mundo da Fifa</a></li><li class="diretorio-second-level"><a title="Copa AmÃ©rica" href="http://globoesporte.globo.com/futebol/copa-america/">Copa AmÃ©rica</a></li><li class="diretorio-second-level"><a title="Copa Sul-Americana" href="http://globoesporte.globo.com/futebol/copa-sul-americana/index.html">Copa Sul-Americana</a></li><li class="diretorio-second-level"><a title="Desafio de Futsal" href="http://globoesporte.globo.com/eventos/desafio-de-futsal/">Desafio de Futsal</a></li><li class="diretorio-second-level"><a title="Esporte Espetacular" href="http://globoesporte.globo.com/programas/esporte-espetacular/">Esporte Espetacular</a></li><li class="diretorio-second-level"><a title="Esportes AquÃ¡ticos" href="http://globoesporte.globo.com/aquaticos/">Esportes AquÃ¡ticos</a></li><li class="diretorio-second-level"><a title="Esportes Radicais" href="http://globoesporte.globo.com/radicais/">Esportes Radicais</a></li><li class="diretorio-second-level"><a title="Eu Atleta" href="http://globoesporte.globo.com/eu-atleta/">Eu Atleta</a></li><li class="diretorio-second-level"><a title="FÃ³rmula 1" href="http://globoesporte.globo.com/motor/formula-1/">FÃ³rmula 1</a></li><li class="diretorio-second-level"><a title="Futebol" href="http://globoesporte.globo.com/futebol/">Futebol</a></li><li class="diretorio-second-level"><a title="Futebol Internacional" href="http://globoesporte.globo.com/futebol/futebol-internacional/">Futebol Internacional</a></li><li class="diretorio-second-level"><a title="FutpÃ©dia" href="http://futpedia.globo.com/">FutpÃ©dia</a></li><li class="diretorio-second-level"><a title="Game Futebol" href="http://gamefutebol.globoesporte.globo.com/">Game Futebol</a></li><li class="diretorio-second-level"><a title="Liga dos CampeÃµes" href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">Liga dos CampeÃµes</a></li><li class="diretorio-second-level"><a title="OlimpÃ­adas" href="http://globoesporte.globo.com/olimpiadas/">OlimpÃ­adas</a></li><li class="diretorio-second-level"><a title="SeleÃ§Ã£o Brasileira" href="http://globoesporte.globo.com/futebol/selecao-brasileira/">SeleÃ§Ã£o Brasileira</a></li><li class="diretorio-second-level"><a title="StockCar" href="http://globoesporte.globo.com/motor/stock-car/">StockCar</a></li><li class="diretorio-second-level"><a title="Surfe" href="http://globoesporte.globo.com/radicais/surfe/">Surfe</a></li><li class="diretorio-second-level"><a title="TÃªnis" href="http://globoesporte.globo.com/tenis/">TÃªnis</a></li><li class="diretorio-second-level"><a title="Vai-e-vem do Mercado" href="http://globoesporte.globo.com/futebol/vai-e-vem-do-mercado/">Vai-e-vem do Mercado</a></li><li class="diretorio-second-level"><a title="VerÃ£o Espetacular" href="http://globoesporte.globo.com/programas/verao-espetacular/">VerÃ£o Espetacular</a></li><li class="diretorio-second-level"><a title="VÃ´lei" href="http://globoesporte.globo.com/volei/">VÃ´lei</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-gshow diretorio-quebra "><a title="gshow" href="http://gshow.globo.com/">gshow</a><ul><li class="diretorio-second-level"><a title="Amorteamo" href="http://gshow.globo.com/programas/amorteamo/">Amorteamo</a></li><li class="diretorio-second-level"><a title="A Teia" href="http://gshow.globo.com/programas/a-teia/">A Teia</a></li><li class="diretorio-second-level"><a title="Altas Horas" href="http://altashoras.globo.com/">Altas Horas</a></li><li class="diretorio-second-level"><a title="Alto Astral" href="http://gshow.globo.com/novelas/alto-astral/index.html">Alto Astral</a></li><li class="diretorio-second-level"><a title="Amor &amp; Sexo" href="http://amoresexo.globo.com/">Amor &amp; Sexo</a></li><li class="diretorio-second-level"><a title="BabilÃ´nia" href="http://gshow.globo.com/novelas/babilonia/index.html">BabilÃ´nia</a></li><li class="diretorio-second-level"><a title="BBB" href="http://bbb.globo.com/">BBB</a></li><li class="diretorio-second-level"><a title="Boogie Oogie" href="http://gshow.globo.com/novelas/boogie-oogie/index.html">Boogie Oogie</a></li><li class="diretorio-second-level"><a title="CaldeirÃ£o do Huck" href="http://caldeiraodohuck.globo.com/">CaldeirÃ£o do Huck</a></li><li class="diretorio-second-level"><a title="Cobras &amp; Lagartos" href="http://gshow.globo.com/novelas/cobras-e-lagartos/videos">Cobras &amp; Lagartos</a></li><li class="diretorio-second-level"><a title="DomingÃ£o do FaustÃ£o" href="http://domingaodofaustao.globo.com/">DomingÃ£o do FaustÃ£o</a></li><li class="diretorio-second-level"><a title="Dupla Identidade" href="http://gshow.globo.com/programas/dupla-identidade/index.html">Dupla Identidade</a></li><li class="diretorio-second-level"><a title="Encontro com FÃ¡tima Bernardes" href="http://tvg.globo.com/programas/encontro-com-fatima-bernardes/">Encontro com FÃ¡tima Bernardes</a></li><li class="diretorio-second-level"><a title="Esquenta!" href="http://tvg.globo.com/programas/esquenta/">Esquenta!</a></li><li class="diretorio-second-level"><a title="Estrelas" href="http://estrelas.globo.com/">Estrelas</a></li><li class="diretorio-second-level"><a title="Felizes para sempre?" href="http://gshow.globo.com/programas/felizes-para-sempre/index.html">Felizes para sempre?</a></li><li class="diretorio-second-level"><a title="ImpÃ©rio" href="http://gshow.globo.com/novelas/imperio/index.html">ImpÃ©rio</a></li><li class="diretorio-second-level"><a title="Mais VocÃª" href="http://maisvoce.globo.com/">Mais VocÃª</a></li><li class="diretorio-second-level"><a title="MalhaÃ§Ã£o" href="http://malhacao.globo.com/">MalhaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Na Moral" href="http://gshow.globo.com/programas/na-moral/">Na Moral</a></li><li class="diretorio-second-level"><a title="O Rei do Gado" href="http://gshow.globo.com/novelas/o-rei-do-gado/videos/">O Rei do Gado</a></li><li class="diretorio-second-level"><a title="PÃ© na Cova" href="http://tvg.globo.com/programas/pe-na-cova/index.html">PÃ© na Cova</a></li><li class="diretorio-second-level"><a title="Programa do JÃ´" href="http://programadojo.globo.com/">Programa do JÃ´</a></li><li class="diretorio-second-level"><a title="Receitas.com" href="http://www.receitas.com/">Receitas.com</a></li><li class="diretorio-second-level"><a title="Receitas Mais VocÃª" href="http://www.receitas.com/maisvoce/">Receitas Mais VocÃª</a></li><li class="diretorio-second-level"><a title="Sete Vidas" href="http://gshow.globo.com/novelas/sete-vidas/index.html">Sete Vidas</a></li><li class="diretorio-second-level"><a title="SuperStar" href="http://gshow.globo.com/programas/superstar/index.html">SuperStar</a></li><li class="diretorio-second-level"><a title="TÃ¡ no Ar" href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/index.html">TÃ¡ no Ar</a></li><li class="diretorio-second-level"><a title="Tapas &amp; Beijos" href="http://tapasebeijos.globo.com/">Tapas &amp; Beijos</a></li><li class="diretorio-second-level"><a title="Sexo e as Negas" href="http://gshow.globo.com/programas/sexo-e-as-negas/index.html">Sexo e as Negas</a></li><li class="diretorio-second-level"><a title="The Voice Brasil" href="http://tvg.globo.com/programas/the-voice-brasil/">The Voice Brasil</a></li><li class="diretorio-second-level"><a title="The Voice Kids" href="http://gshow.globo.com/programas/the-voice-kids/">The Voice Kids</a></li><li class="diretorio-second-level"><a title="Verdades Secretas" href="http://gshow.globo.com/novelas/verdades-secretas/index.html">Verdades Secretas</a></li><li class="diretorio-second-level"><a title="Video Show" href="http://videoshow.globo.com/">Video Show</a></li><li class="diretorio-second-level"><a title="WebsÃ©ries " href="http://gshow.globo.com/programas/webseries/videos/">WebsÃ©ries </a></li><li class="diretorio-second-level"><a title="ZORRA" href="http://gshow.globo.com/programas/zorra/">ZORRA</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-famosos-etc diretorio-quebra "><a title="famosos &amp; etc" href="http://famosos.globo.com/">famosos &amp; etc</a><ul><li class="diretorio-second-level"><a title="EGO" href="http://ego.globo.com/">EGO</a></li><li class="diretorio-second-level"><a title="G1 Cinema" href="http://g1.globo.com/pop-arte/cinema/">G1 Cinema</a></li><li class="diretorio-second-level"><a title="G1 MÃºsica" href="http://g1.globo.com/pop-arte/musica/">G1 MÃºsica</a></li><li class="diretorio-second-level"><a title="Globo Jogos" href="http://jogos.globo.com/">Globo Jogos</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="HorÃ³scopo" href="http://horoscopo.ego.globo.com/ne/home.html">HorÃ³scopo</a></li><li class="diretorio-second-level"><a title="Meus 5 minutos" href="http://www.meus5minutos.com.br/">Meus 5 minutos</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="MÃºsica" href="http://musica.com.br/">MÃºsica</a></li><li class="diretorio-second-level"><a title="Namoro na Web" href="http://tracking.parperfeito.com.br/ppbanner/bannerTracker?originId=13&amp;identifierId=41763&amp;actionId=1">Namoro na Web</a></li><li class="diretorio-second-level"><a title="Paparazzo" href="http://paparazzo.globo.com/">Paparazzo</a></li><li class="diretorio-second-level"><a title="Patricia Kogut" href="http://oglobo.globo.com/cultura/kogut/">Patricia Kogut</a></li><li class="diretorio-second-level"><a title="Retratos da Vida" href="http://extra.globo.com/lazer/retratosdavida/">Retratos da Vida</a></li><li class="diretorio-second-level"><a title="Senninha" href="http://senninha.globo.com/">Senninha</a></li><li class="diretorio-second-level"><a title="Teatro" href="http://www.agentesevenoteatro.com.br/">Teatro</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-grupo-globo diretorio-quebra "><a title="Grupo Globo" href="http://grupoglobo.globo.com/">Grupo Globo</a><ul><li class="diretorio-second-level"><a title="princÃ­pios editoriais" href="http://g1.globo.com/principios-editoriais-do-grupo-globo.html">princÃ­pios editoriais</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-editora-globo diretorio-sem-quebra "><a title="editora globo" href="http://editoraglobo.globo.com/">editora globo</a><ul><li class="diretorio-second-level"><a title="Auto Esporte" href="http://revistaautoesporte.globo.com/">Auto Esporte</a></li><li class="diretorio-second-level"><a title="Casa e Comida" href="http://revistacasaejardim.globo.com/Casa-e-Comida/">Casa e Comida</a></li><li class="diretorio-second-level"><a title="Casa e Jardim" href="http://revistacasaejardim.globo.com/">Casa e Jardim</a></li><li class="diretorio-second-level"><a title="Casa Vogue" href="http://casavogue.globo.com/">Casa Vogue</a></li><li class="diretorio-second-level"><a title="Crescer" href="http://revistacrescer.globo.com/">Crescer</a></li><li class="diretorio-second-level"><a title="Ãpoca" href="http://revistaepoca.globo.com/">Ãpoca</a></li><li class="diretorio-second-level"><a title="Ãpoca NegÃ³cios" href="http://epocanegocios.globo.com/">Ãpoca NegÃ³cios</a></li><li class="diretorio-second-level"><a title="Galileu" href="http://revistagalileu.globo.com/">Galileu</a></li><li class="diretorio-second-level"><a title="Glamour" href="http://revistaglamour.globo.com/">Glamour</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://revistagloborural.globo.com/">Globo Rural</a></li><li class="diretorio-second-level"><a title="GQ Brasil" href="http://gq.globo.com/">GQ Brasil</a></li><li class="diretorio-second-level"><a title="Marie Claire" href="http://revistamarieclaire.globo.com/">Marie Claire</a></li><li class="diretorio-second-level"><a title="Meus 5 Minutos" href="http://meus5minutos.globo.com/">Meus 5 Minutos</a></li><li class="diretorio-second-level"><a title="Monet" href="http://revistamonet.globo.com/">Monet</a></li><li class="diretorio-second-level"><a title="Mundo do SÃ­tio" href="http://mundodositio.globo.com/">Mundo do SÃ­tio</a></li><li class="diretorio-second-level"><a title="PEGN" href="http://revistapegn.globo.com/">PEGN</a></li><li class="diretorio-second-level"><a title="Quem" href="http://revistaquem.globo.com/">Quem</a></li><li class="diretorio-second-level"><a title="Vogue" href="http://vogue.globo.com/">Vogue</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-infoglobo diretorio-sem-quebra "><a title="infoglobo" href="http://infoglobo.com.br/Anuncie/Home.aspx">infoglobo</a><ul><li class="diretorio-second-level"><a title="Ela Digital" href="http://ela.oglobo.globo.com/">Ela Digital</a></li><li class="diretorio-second-level"><a title="Extra" href="http://extra.globo.com/">Extra</a></li><li class="diretorio-second-level"><a title="O Globo" href="http://oglobo.globo.com/">O Globo</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-last "><ul><li class="diretorio-first-level analytics-product glb-menu-globosat diretorio-quebra "><a title="globosat" href="http://canaisglobosat.globo.com/">globosat</a><ul><li class="diretorio-second-level"><a title="Globosat Play" href="http://globosatplay.globo.com/">Globosat Play</a></li><li class="diretorio-second-level"><a title="+Globosat" href="http://maisglobosat.globo.com/">+Globosat</a></li><li class="diretorio-second-level"><a title="BIS" href="http://canalbis.globo.com/">BIS</a></li><li class="diretorio-second-level"><a title="Canal Brasil" href="http://canalbrasil.globo.com/">Canal Brasil</a></li><li class="diretorio-second-level"><a title="Canal Futura" href="http://www.futura.org.br/">Canal Futura</a></li><li class="diretorio-second-level"><a title="Combate" href="http://combate.globo.com/">Combate</a></li><li class="diretorio-second-level"><a title="GloboNews" href="http://globonews.globo.com/">GloboNews</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="Megapix" href="http://megapix.globo.com/">Megapix</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="Off" href="http://canaloff.globo.com/">Off</a></li><li class="diretorio-second-level"><a title="Premiere" href="http://sociopremiere.globo.com/">Premiere</a></li><li class="diretorio-second-level"><a title="SporTV" href="http://sportv.globo.com/">SporTV</a></li><li class="diretorio-second-level"><a title="Studio Universal" href="http://studiouniversal.globo.com/">Studio Universal</a></li><li class="diretorio-second-level"><a title="Syfy" href="http://syfy.globo.com/">Syfy</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Universal Channel" href="http://uc.globo.com/">Universal Channel</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-sistema-globo-de-radio diretorio-sem-quebra "><a title="sistema globo de rÃ¡dio" href="http://globoradio.globo.com/home/HOME.htm">sistema globo de rÃ¡dio</a><ul><li class="diretorio-second-level"><a title="BHFM" href="http://bhfm.globoradio.globo.com/home/HOME.htm">BHFM</a></li><li class="diretorio-second-level"><a title="CBN" href="http://cbn.globoradio.globo.com/home/HOME.htm">CBN</a></li><li class="diretorio-second-level"><a title="RADIOBEAT" href="http://radiobeat.com.br/">RADIOBEAT</a></li><li class="diretorio-second-level"><a title="RÃ¡dio Globo" href="http://radioglobo.globoradio.globo.com/home/HOME.htm">RÃ¡dio Globo</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-globo-filmes diretorio-sem-quebra "><a title="globo filmes" href="http://globofilmes.globo.com/">globo filmes</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-som-livre diretorio-sem-quebra "><a title="som livre" href="http://www.somlivre.com/">som livre</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-blog-globocom diretorio-sem-quebra "><a title="blog globo.com" href="http://blog.globo.com/1.html">blog globo.com</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-zap-imoveis diretorio-sem-quebra "><a title="zap imÃ³veis" href="http://www.zapimoveis.com.br/">zap imÃ³veis</a></li></ul></li></ul></div></div></div></div></div></section><div id="x61" class="opec-area opec-mobile opec-x61 grid-12"><div id="banner_mobile_fim" class="tag-manager-publicidade-container"></div></div><footer><div class="container analytics-area analytics-id-B"><a href="http://www.globo.com/" title="Globo.com">Globo.com</a><span class="copyright">&copy; Copyright 2000-2015 Globo ComunicaÃ§Ã£o e ParticipaÃ§Ãµes S.A.</span><span class="copyright-480 hidden"><p>&copy; 2000-2015.</p><p>Todos os direitos reservados.</p></span><ul class="footer-menu"><li><a href="/privacidade.html">polÃ­tica de privacidade</a></li><li><a href="/todos-os-sites.html">todos os sites</a></li><li><a href="http://anuncie.globo.com/">anuncie conosco</a></li><li><a href="http://meuperfil.globo.com">central globo.com</a></li></ul></div></footer></div><!--[if lt IE 9]><script src="http://s.glbimg.com/en/ho/static/libby/css3pie/js/PIE.js" type="text/javascript"></script><![endif]--><script>
responsiveHub.updateImages();</script><script>
(function(){var scr=document.createElement('script');scr.type = 'text/javascript';scr.async=true;scr.src = 'http://s.glbimg.com/en/ho/static/CACHE/js/129b040b26c6.js';document.getElementsByTagName('head')[0].appendChild(scr);}());</script><div id="opec-frame1"><div id="banner_floating" class="tag-manager-publicidade-container"></div></div><style> .opec-x60{height:auto}</style><script>
window.glb=window.glb || {};window.glb.analyticsConfig=window.glb.analyticsConfig  || {};window.glb.analyticsConfig = {scrollEventChance: 10};</script><script>
document.cookie = 'globoapi.geo.geolocation=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.estado=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.novela=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.time=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';setTimeout(function(){window.glb.barra=window.glb.barra || {};window.glb.barra.component=window.glb.barra.component || {};window.glb.barra.component.barraDiv = {className: ''};}, 1000);</script><style>
#assinante-menu .barra-notifications-area .float-box.config-active .save-area{bottom:0;position:absolute;width:100%}#assinante-menu .barra-notifications-area .float-box.config-active .config-box{padding-bottom:44px}</style><style>
header .container{height:150px}.box-destaque-uber .destaques-primario .titulo{font-family:"ProximaNovaA-Bold"}@media (min-width:1600px){.conteudo-uber .box-destaque-uber{position:static;max-width:1600px;margin:0 auto}}</style><style>
.agrupador-quadruplo-tecnologia .destaque-inferior ul{display:none;// css de folder}</style></body></html>
<!-- PÃ¡gina gerada em 07/08/2015 22:05:27 -->
