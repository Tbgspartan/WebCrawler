<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-773a99c.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-773a99c.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-773a99c.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-773a99c.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kat.cr/sc-773a99c.js');

        sc('setHost', 'a.kat.cr');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: '773a99c',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-773a99c.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<div id="_60318cd4e8d28f6fb76fe34e9bd9c498"></div>
<div id="_39ecb76dd457e5ac33776fdf11500d56"></div>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka">e</i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka">g</i></button></div>
			</form>
		</div>
        <div id="_277923e5f9d753c5b0630c28e641790c"></div>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080p/" class="tag5">1080p</a>
	<a href="/search/2015/" class="tag10">2015</a>
	<a href="/search/2015%20tamil%20movie/" class="tag2">2015 tamil movie</a>
	<a href="/search/3d/" class="tag3">3d</a>
	<a href="/search/android/" class="tag2">android</a>
	<a href="/search/ant%20man/" class="tag2">ant man</a>
	<a href="/search/autodata/" class="tag2">autodata</a>
	<a href="/search/bajrangi%20bhaijaan/" class="tag2">bajrangi bhaijaan</a>
	<a href="/search/big%20bang%20theory/" class="tag2">big bang theory</a>
	<a href="/search/blindspot/" class="tag2">blindspot</a>
	<a href="/search/castle/" class="tag2">castle</a>
	<a href="/search/discography/" class="tag3">discography</a>
	<a href="/search/doctor%20who/" class="tag2">doctor who</a>
	<a href="/search/fear%20the%20walking%20dead/" class="tag5">fear the walking dead</a>
	<a href="/search/fear%20the%20walking%20dead%20s01e03/" class="tag2">fear the walking dead s01e03</a>
	<a href="/search/french/" class="tag3">french</a>
	<a href="/search/gotham/" class="tag4">gotham</a>
	<a href="/search/gotham%20s02e01/" class="tag2">gotham s02e01</a>
	<a href="/search/hindi/" class="tag10">hindi</a>
	<a href="/search/hindi%202015/" class="tag5">hindi 2015</a>
	<a href="/search/inside%20out/" class="tag2">inside out</a>
	<a href="/search/ita/" class="tag2">ita</a>
	<a href="/search/kat%20ph%20com/" class="tag10">kat ph com</a>
	<a href="/search/katti%20batti/" class="tag2">katti batti</a>
	<a href="/search/limitless/" class="tag2">limitless</a>
	<a href="/search/lost%20on%20paradise%20island/" class="tag2">lost on paradise island</a>
	<a href="/search/mad%20max/" class="tag3">mad max</a>
	<a href="/search/malayalam/" class="tag3">malayalam</a>
	<a href="/search/minority%20report/" class="tag2">minority report</a>
	<a href="/search/movies/" class="tag2">movies</a>
	<a href="/search/narcos/" class="tag2">narcos</a>
	<a href="/search/nezu/" class="tag10">nezu</a>
	<a href="/search/rick%20and%20morty/" class="tag2">rick and morty</a>
	<a href="/search/ripsalot/" class="tag4">ripsalot</a>
	<a href="/search/scorpion/" class="tag2">scorpion</a>
	<a href="/search/soma/" class="tag3">soma</a>
	<a href="/search/tamil/" class="tag4">tamil</a>
	<a href="/search/telugu/" class="tag3">telugu</a>
	<a href="/search/telugu%202015/" class="tag2">telugu 2015</a>
	<a href="/search/the%20big%20bang%20theory/" class="tag2">the big bang theory</a>
	<a href="/search/the%20big%20bang%20theory%20s09e01/" class="tag2">the big bang theory s09e01</a>
	<a href="/search/the%20big%20bang%20theory%20season%209/" class="tag2">the big bang theory season 9</a>
	<a href="/search/the%20walking%20dead/" class="tag2">the walking dead</a>
	<a href="/search/tomorrowland/" class="tag2">tomorrowland</a>
	<a href="/search/wwe/" class="tag2">wwe</a>
	<a href="/search/yify/" class="tag10">yify</a>
	<a href="/search/yify%201080p/" class="tag4">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag4">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11300779,0" class="icommentjs icon16" href="/tomorrowland-2015-720p-brrip-x264-yify-t11300779.html#comment"> <em class="iconvalue">88</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/tomorrowland-2015-720p-brrip-x264-yify-t11300779.html" class="cellMainLink">Tomorrowland (2015) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="914684200">872.31 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="22 Sep 2015, 16:26">1&nbsp;day</span></td>
			<td class="green center">13124</td>
			<td class="red lasttd center">15432</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11298258,0" class="icommentjs icon16" href="/me-and-earl-and-the-dying-girl-2015-1080p-brrip-x264-yify-t11298258.html#comment"> <em class="iconvalue">21</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/me-and-earl-and-the-dying-girl-2015-1080p-brrip-x264-yify-t11298258.html" class="cellMainLink">Me and Earl and the Dying Girl (2015) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="1758365755">1.64 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="22 Sep 2015, 05:06">1&nbsp;day</span></td>
			<td class="green center">7708</td>
			<td class="red lasttd center">7729</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11292246,0" class="icommentjs icon16" href="/amnesiac-2015-1080p-brrip-x264-yify-t11292246.html#comment"> <em class="iconvalue">24</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/amnesiac-2015-1080p-brrip-x264-yify-t11292246.html" class="cellMainLink">Amnesiac (2015) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="1327672421">1.24 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="21 Sep 2015, 03:57">2&nbsp;days</span></td>
			<td class="green center">4277</td>
			<td class="red lasttd center">3033</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11291484,0" class="icommentjs icon16" href="/everest-2015-hd-ts-xvid-ac3-hq-hive-cm8-t11291484.html#comment"> <em class="iconvalue">50</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/everest-2015-hd-ts-xvid-ac3-hq-hive-cm8-t11291484.html" class="cellMainLink">Everest 2015 HD-TS XVID AC3 HQ Hive-CM8</a></div>
			</td>
			<td class="nobr center" data-sort="1646504640">1.53 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="20 Sep 2015, 23:18">3&nbsp;days</span></td>
			<td class="green center">3718</td>
			<td class="red lasttd center">3867</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11303325,0" class="icommentjs icon16" href="/pixels-2015-hdrip-xvid-ac3-evo-t11303325.html#comment"> <em class="iconvalue">58</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/pixels-2015-hdrip-xvid-ac3-evo-t11303325.html" class="cellMainLink">Pixels 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1524305064">1.42 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="23 Sep 2015, 04:12">19&nbsp;hours</span></td>
			<td class="green center">654</td>
			<td class="red lasttd center">4266</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11289836,0" class="icommentjs icon16" href="/last-shift-2014-1080p-brrip-x264-yify-t11289836.html#comment"> <em class="iconvalue">21</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/last-shift-2014-1080p-brrip-x264-yify-t11289836.html" class="cellMainLink">Last Shift (2014) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="1329877944">1.24 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="20 Sep 2015, 14:47">3&nbsp;days</span></td>
			<td class="green center">1816</td>
			<td class="red lasttd center">1377</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11299151,0" class="icommentjs icon16" href="/the-messenger-2015-hdrip-xvid-ac3-evo-t11299151.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/the-messenger-2015-hdrip-xvid-ac3-evo-t11299151.html" class="cellMainLink">The Messenger 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1512399261">1.41 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="22 Sep 2015, 10:13">1&nbsp;day</span></td>
			<td class="green center">1337</td>
			<td class="red lasttd center">1461</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11295079,0" class="icommentjs icon16" href="/closer-to-the-moon-2014-dvdrip-xvid-etrg-t11295079.html#comment"> <em class="iconvalue">17</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/closer-to-the-moon-2014-dvdrip-xvid-etrg-t11295079.html" class="cellMainLink">Closer to the Moon 2014 DVDRip XviD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="742981549">708.56 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="21 Sep 2015, 15:22">2&nbsp;days</span></td>
			<td class="green center">1225</td>
			<td class="red lasttd center">1533</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11299578,0" class="icommentjs icon16" href="/hidden-2015-hdrip-xvid-etrg-t11299578.html#comment"> <em class="iconvalue">23</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/hidden-2015-hdrip-xvid-etrg-t11299578.html" class="cellMainLink">Hidden 2015 HDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="739297533">705.05 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="22 Sep 2015, 12:20">1&nbsp;day</span></td>
			<td class="green center">1275</td>
			<td class="red lasttd center">1379</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11286094,0" class="icommentjs icon16" href="/in-their-skin-2012-720p-brrip-x264-yify-t11286094.html#comment"> <em class="iconvalue">16</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/in-their-skin-2012-720p-brrip-x264-yify-t11286094.html" class="cellMainLink">In Their Skin (2012) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="790239339">753.63 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="20 Sep 2015, 00:39">3&nbsp;days</span></td>
			<td class="green center">1419</td>
			<td class="red lasttd center">687</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11299686,0" class="icommentjs icon16" href="/jurassic-world-o-mundo-dos-dinossauros-2015-5-1-ch-dublado-1080p-web-dl-thepiratefilmes-t11299686.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/jurassic-world-o-mundo-dos-dinossauros-2015-5-1-ch-dublado-1080p-web-dl-thepiratefilmes-t11299686.html" class="cellMainLink">Jurassic World â O Mundo dos Dinossauros (2015) 5.1 CH Dublado 1080p [WEB-DL] ThePirateFilmes</a></div>
			</td>
			<td class="nobr center" data-sort="2512800541">2.34 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="22 Sep 2015, 12:50">1&nbsp;day</span></td>
			<td class="green center">615</td>
			<td class="red lasttd center">1742</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11301768,0" class="icommentjs icon16" href="/the-goob-2014-720p-brrip-x264-yify-t11301768.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/the-goob-2014-720p-brrip-x264-yify-t11301768.html" class="cellMainLink">The Goob (2014) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="728573137">694.82 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="22 Sep 2015, 18:55">1&nbsp;day</span></td>
			<td class="green center">935</td>
			<td class="red lasttd center">814</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11298624,0" class="icommentjs icon16" href="/sword-of-vengeance-2015-1080p-brrip-x264-dts-jyk-t11298624.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/sword-of-vengeance-2015-1080p-brrip-x264-dts-jyk-t11298624.html" class="cellMainLink">Sword of Vengeance 2015 1080p BRRip x264 DTS-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="2348903057">2.19 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="22 Sep 2015, 06:53">1&nbsp;day</span></td>
			<td class="green center">927</td>
			<td class="red lasttd center">711</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11299223,0" class="icommentjs icon16" href="/tiger-house-2015-brrip-xvid-ac3-evo-t11299223.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/tiger-house-2015-brrip-xvid-ac3-evo-t11299223.html" class="cellMainLink">Tiger House 2015 BRRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1519732073">1.42 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="22 Sep 2015, 10:35">1&nbsp;day</span></td>
			<td class="green center">729</td>
			<td class="red lasttd center">1058</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11290982,0" class="icommentjs icon16" href="/blunt-force-trauma-2015-brrip-xvid-sc0rp-t11290982.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/blunt-force-trauma-2015-brrip-xvid-sc0rp-t11290982.html" class="cellMainLink">Blunt Force Trauma 2015 BRRiP XViD-sC0rp</a></div>
			</td>
			<td class="nobr center" data-sort="744149643">709.68 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="20 Sep 2015, 20:39">3&nbsp;days</span></td>
			<td class="green center">879</td>
			<td class="red lasttd center">730</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11297234,0" class="icommentjs icon16" href="/the-big-bang-theory-s09e01-hdtv-x264-lol-ettv-t11297234.html#comment"> <em class="iconvalue">197</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/the-big-bang-theory-s09e01-hdtv-x264-lol-ettv-t11297234.html" class="cellMainLink">The Big Bang Theory S09E01 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="136399244">130.08 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="21 Sep 2015, 23:02">2&nbsp;days</span></td>
			<td class="green center">12458</td>
			<td class="red lasttd center">4032</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11297482,0" class="icommentjs icon16" href="/gotham-s02e01-hdtv-x264-lol-ettv-t11297482.html#comment"> <em class="iconvalue">112</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/gotham-s02e01-hdtv-x264-lol-ettv-t11297482.html" class="cellMainLink">Gotham S02E01 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="269222943">256.75 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="22 Sep 2015, 00:59">1&nbsp;day</span></td>
			<td class="green center">6624</td>
			<td class="red lasttd center">2767</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11297698,0" class="icommentjs icon16" href="/scorpion-s02e01-hdtv-x264-lol-ettv-t11297698.html#comment"> <em class="iconvalue">68</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/scorpion-s02e01-hdtv-x264-lol-ettv-t11297698.html" class="cellMainLink">Scorpion S02E01 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="326036376">310.93 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="22 Sep 2015, 01:58">1&nbsp;day</span></td>
			<td class="green center">6098</td>
			<td class="red lasttd center">2832</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11297744,0" class="icommentjs icon16" href="/minority-report-s01e01-hdtv-x264-killers-ettv-t11297744.html#comment"> <em class="iconvalue">79</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/minority-report-s01e01-hdtv-x264-killers-ettv-t11297744.html" class="cellMainLink">Minority Report S01E01 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="445145314">424.52 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="22 Sep 2015, 02:14">1&nbsp;day</span></td>
			<td class="green center">6479</td>
			<td class="red lasttd center">1635</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11297704,0" class="icommentjs icon16" href="/blindspot-s01e01-hdtv-x264-lol-ettv-t11297704.html#comment"> <em class="iconvalue">86</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/blindspot-s01e01-hdtv-x264-lol-ettv-t11297704.html" class="cellMainLink">Blindspot S01E01 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="278301743">265.41 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="22 Sep 2015, 02:02">1&nbsp;day</span></td>
			<td class="green center">5708</td>
			<td class="red lasttd center">2475</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11292124,0" class="icommentjs icon16" href="/the-strain-s02e11-hdtv-x264-batv-ettv-t11292124.html#comment"> <em class="iconvalue">93</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/the-strain-s02e11-hdtv-x264-batv-ettv-t11292124.html" class="cellMainLink">The Strain S02E11 HDTV x264-BATV[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="318609783">303.85 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="21 Sep 2015, 03:22">2&nbsp;days</span></td>
			<td class="green center">6299</td>
			<td class="red lasttd center">828</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11297469,0" class="icommentjs icon16" href="/castle-2009-s08e01-hdtv-x264-lol-ettv-t11297469.html#comment"> <em class="iconvalue">47</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/castle-2009-s08e01-hdtv-x264-lol-ettv-t11297469.html" class="cellMainLink">Castle 2009 S08E01 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="280160931">267.18 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="22 Sep 2015, 00:55">1&nbsp;day</span></td>
			<td class="green center">4075</td>
			<td class="red lasttd center">1658</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11291842,0" class="icommentjs icon16" href="/ray-donovan-s03e11-hdtv-x264-lol-ettv-t11291842.html#comment"> <em class="iconvalue">64</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/ray-donovan-s03e11-hdtv-x264-lol-ettv-t11291842.html" class="cellMainLink">Ray Donovan S03E11 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="278208977">265.32 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="21 Sep 2015, 01:47">2&nbsp;days</span></td>
			<td class="green center">3697</td>
			<td class="red lasttd center">929</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11298024,0" class="icommentjs icon16" href="/wwe-raw-09-21-2015-hdtv-x264-killer9-sparrow-t11298024.html#comment"> <em class="iconvalue">46</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/wwe-raw-09-21-2015-hdtv-x264-killer9-sparrow-t11298024.html" class="cellMainLink">WWE Raw 09 21 2015 HDTV x264-Killer9 -={SPARROW}=-</a></div>
			</td>
			<td class="nobr center" data-sort="1563356127">1.46 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="22 Sep 2015, 04:02">1&nbsp;day</span></td>
			<td class="green center">3115</td>
			<td class="red lasttd center">1405</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11292255,0" class="icommentjs icon16" href="/rick-and-morty-s02e08-720p-hdtv-x264-batv-rartv-t11292255.html#comment"> <em class="iconvalue">48</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/rick-and-morty-s02e08-720p-hdtv-x264-batv-rartv-t11292255.html" class="cellMainLink">Rick and Morty S02E08 720p HDTV x264-BATV[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="478057832">455.91 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="21 Sep 2015, 04:03">2&nbsp;days</span></td>
			<td class="green center">3472</td>
			<td class="red lasttd center">203</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11302838,0" class="icommentjs icon16" href="/scream-queens-2015-s01e01-hdtv-x264-killers-ettv-t11302838.html#comment"> <em class="iconvalue">42</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/scream-queens-2015-s01e01-hdtv-x264-killers-ettv-t11302838.html" class="cellMainLink">Scream Queens 2015 S01E01 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="385290730">367.44 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="23 Sep 2015, 01:37">21&nbsp;hours</span></td>
			<td class="green center">2166</td>
			<td class="red lasttd center">2427</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11297420,0" class="icommentjs icon16" href="/the-muppets-s01e01-hdtv-x264-killers-ettv-t11297420.html#comment"> <em class="iconvalue">42</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/the-muppets-s01e01-hdtv-x264-killers-ettv-t11297420.html" class="cellMainLink">The Muppets S01E01 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="218826347">208.69 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="22 Sep 2015, 00:38">1&nbsp;day</span></td>
			<td class="green center">2670</td>
			<td class="red lasttd center">1126</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11297349,0" class="icommentjs icon16" href="/ncis-los-angeles-s07e01-hdtv-x264-lol-ettv-t11297349.html#comment"> <em class="iconvalue">24</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/ncis-los-angeles-s07e01-hdtv-x264-lol-ettv-t11297349.html" class="cellMainLink">NCIS Los Angeles S07E01 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="302163266">288.17 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="22 Sep 2015, 00:02">1&nbsp;day</span></td>
			<td class="green center">2006</td>
			<td class="red lasttd center">902</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11293081,0" class="icommentjs icon16" href="/the-67th-annual-primetime-emmy-awards-2015-hdtv-x264-2hd-ettv-t11293081.html#comment"> <em class="iconvalue">28</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/the-67th-annual-primetime-emmy-awards-2015-hdtv-x264-2hd-ettv-t11293081.html" class="cellMainLink">The 67th Annual Primetime Emmy Awards 2015 HDTV x264-2HD[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="1039049795">990.92 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="21 Sep 2015, 07:31">2&nbsp;days</span></td>
			<td class="green center">2075</td>
			<td class="red lasttd center">691</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11302740,0" class="icommentjs icon16" href="/ncis-s13e01-hdtv-x264-lol-ettv-t11302740.html#comment"> <em class="iconvalue">21</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/ncis-s13e01-hdtv-x264-lol-ettv-t11302740.html" class="cellMainLink">NCIS S13E01 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="271052792">258.5 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="23 Sep 2015, 01:02">22&nbsp;hours</span></td>
			<td class="green center">1269</td>
			<td class="red lasttd center">1710</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11292352,0" class="icommentjs icon16" href="/drake-future-what-a-time-to-be-alive-320kbps-mixjoint-t11292352.html#comment"> <em class="iconvalue">17</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/drake-future-what-a-time-to-be-alive-320kbps-mixjoint-t11292352.html" class="cellMainLink">Drake &amp; Future - What A Time To Be Alive [320Kbps] (MixJoint)</a></div>
			</td>
			<td class="nobr center" data-sort="94966882">90.57 <span>MB</span></td>
			<td class="center">14</td>
			<td class="center"><span title="21 Sep 2015, 04:34">2&nbsp;days</span></td>
			<td class="green center">844</td>
			<td class="red lasttd center">221</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11295732,0" class="icommentjs icon16" href="/billboard-top-100-singles-90s-1990-1999-mp3-vbr-h4ckus-glodls-t11295732.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/billboard-top-100-singles-90s-1990-1999-mp3-vbr-h4ckus-glodls-t11295732.html" class="cellMainLink">Billboard - Top 100 Singles 90s (1990 - 1999) [MP3-VBR] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="5900173389">5.49 <span>GB</span></td>
			<td class="center">1003</td>
			<td class="center"><span title="21 Sep 2015, 17:36">2&nbsp;days</span></td>
			<td class="green center">283</td>
			<td class="red lasttd center">629</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11295973,0" class="icommentjs icon16" href="/chvrches-every-open-eye-2015-mp3-320kbps-h4ckus-glodls-t11295973.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/chvrches-every-open-eye-2015-mp3-320kbps-h4ckus-glodls-t11295973.html" class="cellMainLink">Chvrches - Every Open Eye [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="101148704">96.46 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span title="21 Sep 2015, 18:11">2&nbsp;days</span></td>
			<td class="green center">331</td>
			<td class="red lasttd center">63</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11299203,0" class="icommentjs icon16" href="/va-now-that-s-what-i-call-pop-3cd-2015-mp3-320kbps-h4ckus-glodls-t11299203.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/va-now-that-s-what-i-call-pop-3cd-2015-mp3-320kbps-h4ckus-glodls-t11299203.html" class="cellMainLink">VA - NOW Thatâs What I Call Pop [3CD] [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="600138990">572.34 <span>MB</span></td>
			<td class="center">80</td>
			<td class="center"><span title="22 Sep 2015, 10:30">1&nbsp;day</span></td>
			<td class="green center">276</td>
			<td class="red lasttd center">158</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11289689,0" class="icommentjs icon16" href="/various-artist-now-vol-1-2015-mp3-320kbps-h4ckus-glodls-t11289689.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/various-artist-now-vol-1-2015-mp3-320kbps-h4ckus-glodls-t11289689.html" class="cellMainLink">Various Artist - NOW, Vol. 1 [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="174080850">166.02 <span>MB</span></td>
			<td class="center">25</td>
			<td class="center"><span title="20 Sep 2015, 14:12">3&nbsp;days</span></td>
			<td class="green center">298</td>
			<td class="red lasttd center">55</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11298979,0" class="icommentjs icon16" href="/va-guitar-gods-of-80-s-and-90-s-2007-320ak-t11298979.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/va-guitar-gods-of-80-s-and-90-s-2007-320ak-t11298979.html" class="cellMainLink">VA - Guitar Gods Of 80&#039;s and 90&#039;s 2007 320ak</a></div>
			</td>
			<td class="nobr center" data-sort="362375417">345.59 <span>MB</span></td>
			<td class="center">34</td>
			<td class="center"><span title="22 Sep 2015, 09:04">1&nbsp;day</span></td>
			<td class="green center">253</td>
			<td class="red lasttd center">69</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/b-b-king-lonely-and-blue-2015-mp3-320kbps-h4ckus-glodls-t11301323.html" class="cellMainLink">B.B. King - Lonely And Blue [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="340026737">324.27 <span>MB</span></td>
			<td class="center">52</td>
			<td class="center"><span title="22 Sep 2015, 17:30">1&nbsp;day</span></td>
			<td class="green center">154</td>
			<td class="red lasttd center">80</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11296491,0" class="icommentjs icon16" href="/howard-stern-show-wrap-up-09-21-15-t11296491.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/howard-stern-show-wrap-up-09-21-15-t11296491.html" class="cellMainLink">Howard Stern Show + Wrap Up 09-21-15</a></div>
			</td>
			<td class="nobr center" data-sort="261240178">249.14 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="21 Sep 2015, 19:23">2&nbsp;days</span></td>
			<td class="green center">155</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11301213,0" class="icommentjs icon16" href="/billboard-hot-100-singles-chart-03rd-october-2015-rz-rg-mp3-320kbps-glodls-t11301213.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/billboard-hot-100-singles-chart-03rd-october-2015-rz-rg-mp3-320kbps-glodls-t11301213.html" class="cellMainLink">Billboard Hot 100 Singles Chart (03rd October 2015) RZ-RG [MP3-320KBPS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="900680205">858.96 <span>MB</span></td>
			<td class="center">104</td>
			<td class="center"><span title="22 Sep 2015, 17:11">1&nbsp;day</span></td>
			<td class="green center">101</td>
			<td class="red lasttd center">103</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11294695,0" class="icommentjs icon16" href="/pryda-pryda-10-vol-iii-2015-320-kbps-edm-enigmaticabhi-t11294695.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/pryda-pryda-10-vol-iii-2015-320-kbps-edm-enigmaticabhi-t11294695.html" class="cellMainLink">Pryda - Pryda 10 Vol. III (2015) [320 KBPS] [EDM] *EnigmaticAbhi*</a></div>
			</td>
			<td class="nobr center" data-sort="229019335">218.41 <span>MB</span></td>
			<td class="center">13</td>
			<td class="center"><span title="21 Sep 2015, 13:43">2&nbsp;days</span></td>
			<td class="green center">100</td>
			<td class="red lasttd center">19</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11301904,0" class="icommentjs icon16" href="/one-direction-infinity-single-2015-mp3-320kbps-h4ckus-glodls-t11301904.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/one-direction-infinity-single-2015-mp3-320kbps-h4ckus-glodls-t11301904.html" class="cellMainLink">One Direction - Infinity [Single] [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="10515015">10.03 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="22 Sep 2015, 19:29">1&nbsp;day</span></td>
			<td class="green center">75</td>
			<td class="red lasttd center">52</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11299573,0" class="icommentjs icon16" href="/new-order-music-complete-chattchitto-rg-t11299573.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/new-order-music-complete-chattchitto-rg-t11299573.html" class="cellMainLink">New Order - Music Complete [ChattChitto RG]</a></div>
			</td>
			<td class="nobr center" data-sort="175130749">167.02 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span title="22 Sep 2015, 12:19">1&nbsp;day</span></td>
			<td class="green center">80</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11298853,0" class="icommentjs icon16" href="/latest-malayalam-movie-songs-320kbps-sep-2015-kanal-pathemari-life-of-josutty-kohinoor-Î©mega39-t11298853.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/latest-malayalam-movie-songs-320kbps-sep-2015-kanal-pathemari-life-of-josutty-kohinoor-Î©mega39-t11298853.html" class="cellMainLink">Latest Malayalam Movie Songs 320Kbps_[sep 2015]_(Kanal,Pathemari,Life of Josutty,Kohinoor)_Î©mega39</a></div>
			</td>
			<td class="nobr center" data-sort="164968117">157.33 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center"><span title="22 Sep 2015, 08:13">1&nbsp;day</span></td>
			<td class="green center">64</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11298907,0" class="icommentjs icon16" href="/silversun-pickups-better-nature-2015-320-kbps-freak37-t11298907.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/silversun-pickups-better-nature-2015-320-kbps-freak37-t11298907.html" class="cellMainLink">Silversun Pickups â Better Nature (2015) 320 KBPS - Freak37</a></div>
			</td>
			<td class="nobr center" data-sort="123398982">117.68 <span>MB</span></td>
			<td class="center">11</td>
			<td class="center"><span title="22 Sep 2015, 08:38">1&nbsp;day</span></td>
			<td class="green center">63</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/alabama-southern-drawl-2015-mp3-320kbps-freak37-t11301407.html" class="cellMainLink">Alabama - Southern Drawl (2015) [ MP3 320kbps ] - Freak37</a></div>
			</td>
			<td class="nobr center" data-sort="129950453">123.93 <span>MB</span></td>
			<td class="center">14</td>
			<td class="center"><span title="22 Sep 2015, 17:51">1&nbsp;day</span></td>
			<td class="green center">2</td>
			<td class="red lasttd center">119</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11299044,0" class="icommentjs icon16" href="/soma-reloaded-t11299044.html#comment"> <em class="iconvalue">34</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/soma-reloaded-t11299044.html" class="cellMainLink">SOMA-RELOADED</a></div>
			</td>
			<td class="nobr center" data-sort="11877323228">11.06 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="22 Sep 2015, 09:32">1&nbsp;day</span></td>
			<td class="green center">648</td>
			<td class="red lasttd center">1072</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11299165,0" class="icommentjs icon16" href="/mad-max-r-g-mechanics-t11299165.html#comment"> <em class="iconvalue">77</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/mad-max-r-g-mechanics-t11299165.html" class="cellMainLink">Mad Max [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center" data-sort="4283230728">3.99 <span>GB</span></td>
			<td class="center">8</td>
			<td class="center"><span title="22 Sep 2015, 10:19">1&nbsp;day</span></td>
			<td class="green center">752</td>
			<td class="red lasttd center">346</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11287659,0" class="icommentjs icon16" href="/battlefield-hardline-r-g-games-t11287659.html#comment"> <em class="iconvalue">37</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-Type"></i>
                    <a href="/battlefield-hardline-r-g-games-t11287659.html" class="cellMainLink">Battlefield Hardline [R.G. Games]</a></div>
			</td>
			<td class="nobr center" data-sort="31551259603">29.38 <span>GB</span></td>
			<td class="center">16</td>
			<td class="center"><span title="20 Sep 2015, 08:09">3&nbsp;days</span></td>
			<td class="green center">297</td>
			<td class="red lasttd center">681</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11294847,0" class="icommentjs icon16" href="/the-settlers-rise-of-an-empire-gold-edition-gog-t11294847.html#comment"> <em class="iconvalue">18</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/the-settlers-rise-of-an-empire-gold-edition-gog-t11294847.html" class="cellMainLink">The Settlers: Rise of an Empire - Gold Edition (GOG)</a></div>
			</td>
			<td class="nobr center" data-sort="3754424093">3.5 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="21 Sep 2015, 14:16">2&nbsp;days</span></td>
			<td class="green center">198</td>
			<td class="red lasttd center">276</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11298886,0" class="icommentjs icon16" href="/blood-bowl-2-codex-t11298886.html#comment"> <em class="iconvalue">31</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/blood-bowl-2-codex-t11298886.html" class="cellMainLink">Blood.Bowl.2-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="4175632014">3.89 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="22 Sep 2015, 08:29">1&nbsp;day</span></td>
			<td class="green center">230</td>
			<td class="red lasttd center">181</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11290142,0" class="icommentjs icon16" href="/middle-earth-shadow-of-mordor-game-of-the-year-edition-update-8-2014-pc-steam-rip-Ð¾Ñ-r-g-origins-t11290142.html#comment"> <em class="iconvalue">62</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/middle-earth-shadow-of-mordor-game-of-the-year-edition-update-8-2014-pc-steam-rip-Ð¾Ñ-r-g-origins-t11290142.html" class="cellMainLink">Middle-Earth: Shadow of Mordor - Game of the Year Edition [Update 8] (2014) PC | Steam-Rip Ð¾Ñ R.G. Origins</a></div>
			</td>
			<td class="nobr center" data-sort="57777945424">53.81 <span>GB</span></td>
			<td class="center">72</td>
			<td class="center"><span title="20 Sep 2015, 16:10">3&nbsp;days</span></td>
			<td class="green center">72</td>
			<td class="red lasttd center">426</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11300700,0" class="icommentjs icon16" href="/fifa-16-xbox360-complex-t11300700.html#comment"> <em class="iconvalue">28</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/fifa-16-xbox360-complex-t11300700.html" class="cellMainLink">FIFA.16.XBOX360-COMPLEX</a></div>
			</td>
			<td class="nobr center" data-sort="8738849184">8.14 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="22 Sep 2015, 16:09">1&nbsp;day</span></td>
			<td class="green center">68</td>
			<td class="red lasttd center">117</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11291932,0" class="icommentjs icon16" href="/rambo-v1-0-android-t11291932.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/rambo-v1-0-android-t11291932.html" class="cellMainLink">RAMBO V1.0 android</a></div>
			</td>
			<td class="nobr center" data-sort="118530519">113.04 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="21 Sep 2015, 02:18">2&nbsp;days</span></td>
			<td class="green center">108</td>
			<td class="red lasttd center">18</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11294872,0" class="icommentjs icon16" href="/age-of-wonders-3-deluxe-edition-v-1-700-4-dlc-2014-pc-steam-rip-Ð¾Ñ-r-g-gamblers-t11294872.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/age-of-wonders-3-deluxe-edition-v-1-700-4-dlc-2014-pc-steam-rip-Ð¾Ñ-r-g-gamblers-t11294872.html" class="cellMainLink">Age of Wonders 3: Deluxe Edition [v 1.700 + 4 DLC] (2014) PC | Steam-Rip Ð¾Ñ R.G. Gamblers</a></div>
			</td>
			<td class="nobr center" data-sort="3658281810">3.41 <span>GB</span></td>
			<td class="center">9</td>
			<td class="center"><span title="21 Sep 2015, 14:23">2&nbsp;days</span></td>
			<td class="green center">70</td>
			<td class="red lasttd center">63</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11291280,0" class="icommentjs icon16" href="/fifa-16-super-deluxe-edition-multi15-not-cracked-fitgirl-repack-selective-download-from-8-6-gb-t11291280.html#comment"> <em class="iconvalue">75</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/fifa-16-super-deluxe-edition-multi15-not-cracked-fitgirl-repack-selective-download-from-8-6-gb-t11291280.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/fifa-16-super-deluxe-edition-multi15-not-cracked-fitgirl-repack-selective-download-from-8-6-gb-t11291280.html" class="cellMainLink">FIFA 16: Super Deluxe Edition (MULTI15, NOT CRACKED) [FitGirl Repack, Selective Download - from 8.6 GB]</a></div>
			</td>
			<td class="nobr center" data-sort="15956926801">14.86 <span>GB</span></td>
			<td class="center">17</td>
			<td class="center"><span title="20 Sep 2015, 22:24">3&nbsp;days</span></td>
			<td class="green center">17</td>
			<td class="red lasttd center">150</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11302060,0" class="icommentjs icon16" href="/system-shock-enhanced-edition-gog-t11302060.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/system-shock-enhanced-edition-gog-t11302060.html" class="cellMainLink">System Shock: Enhanced Edition (GOG)</a></div>
			</td>
			<td class="nobr center" data-sort="933334512">890.1 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span title="22 Sep 2015, 20:20">1&nbsp;day</span></td>
			<td class="green center">56</td>
			<td class="red lasttd center">39</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11298318,0" class="icommentjs icon16" href="/terratech-v0-5-12-t11298318.html#comment"> <em class="iconvalue">15</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/terratech-v0-5-12-t11298318.html" class="cellMainLink">TerraTech v0.5.12</a></div>
			</td>
			<td class="nobr center" data-sort="233256426">222.45 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="22 Sep 2015, 05:19">1&nbsp;day</span></td>
			<td class="green center">63</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11293151,0" class="icommentjs icon16" href="/anomaly-trilogy-r-g-mechanics-t11293151.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/anomaly-trilogy-r-g-mechanics-t11293151.html" class="cellMainLink">Anomaly: Trilogy [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center" data-sort="2964273955">2.76 <span>GB</span></td>
			<td class="center">64</td>
			<td class="center"><span title="21 Sep 2015, 07:50">2&nbsp;days</span></td>
			<td class="green center">43</td>
			<td class="red lasttd center">47</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11301516,0" class="icommentjs icon16" href="/afro-samurai-2-revenge-of-kuma-volume-one-codex-t11301516.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/afro-samurai-2-revenge-of-kuma-volume-one-codex-t11301516.html" class="cellMainLink">Afro Samurai 2 Revenge of Kuma Volume One-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="1843632098">1.72 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="22 Sep 2015, 18:17">1&nbsp;day</span></td>
			<td class="green center">56</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11290472,0" class="icommentjs icon16" href="/project-cars-update-7-dlc-s-2015-pc-steam-rip-Ð¾Ñ-r-g-origins-t11290472.html#comment"> <em class="iconvalue">13</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/project-cars-update-7-dlc-s-2015-pc-steam-rip-Ð¾Ñ-r-g-origins-t11290472.html" class="cellMainLink">Project CARS [Update 7 + DLC&#039;s] (2015) PC | Steam-Rip Ð¾Ñ R.G. Origins</a></div>
			</td>
			<td class="nobr center" data-sort="19698586763">18.35 <span>GB</span></td>
			<td class="center">29</td>
			<td class="center"><span title="20 Sep 2015, 17:42">3&nbsp;days</span></td>
			<td class="green center">41</td>
			<td class="red lasttd center">36</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11289256,0" class="icommentjs icon16" href="/android-best-apps-and-games-pack-18-9-15-appzdam-t11289256.html#comment"> <em class="iconvalue">18</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/android-best-apps-and-games-pack-18-9-15-appzdam-t11289256.html" class="cellMainLink">Android Best Apps and Games Pack (18/9/15) - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="1890518045">1.76 <span>GB</span></td>
			<td class="center">81</td>
			<td class="center"><span title="20 Sep 2015, 12:31">3&nbsp;days</span></td>
			<td class="green center">174</td>
			<td class="red lasttd center">196</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11295084,0" class="icommentjs icon16" href="/windows-10-pro-x64-multi-6-pre-activated-sep2015-generation2-t11295084.html#comment"> <em class="iconvalue">33</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/windows-10-pro-x64-multi-6-pre-activated-sep2015-generation2-t11295084.html" class="cellMainLink">Windows 10 Pro X64 MULTi-6 Pre-Activated Sep2015 {Generation2}</a></div>
			</td>
			<td class="nobr center" data-sort="3811396457">3.55 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="21 Sep 2015, 15:23">2&nbsp;days</span></td>
			<td class="green center">148</td>
			<td class="red lasttd center">200</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11301889,0" class="icommentjs icon16" href="/office-professional-plus-2016-x86-x64-en-us-msdn-t11301889.html#comment"> <em class="iconvalue">130</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/office-professional-plus-2016-x86-x64-en-us-msdn-t11301889.html" class="cellMainLink">Office Professional Plus 2016 | x86 x64 | en-us - MSDN</a></div>
			</td>
			<td class="nobr center" data-sort="2421989376">2.26 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="22 Sep 2015, 19:23">1&nbsp;day</span></td>
			<td class="green center">124</td>
			<td class="red lasttd center">147</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11287446,0" class="icommentjs icon16" href="/o-o-defrag-professional-edition-19-0-build-87-32bit-64bit-eng-serial-at-team-t11287446.html#comment"> <em class="iconvalue">15</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/o-o-defrag-professional-edition-19-0-build-87-32bit-64bit-eng-serial-at-team-t11287446.html" class="cellMainLink">O&amp;O Defrag Professional Edition 19.0 Build 87 - 32bit &amp; 64bit [ENG] [Serial] [AT-TEAM]</a></div>
			</td>
			<td class="nobr center" data-sort="48824043">46.56 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="20 Sep 2015, 07:11">3&nbsp;days</span></td>
			<td class="green center">171</td>
			<td class="red lasttd center">13</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11287142,0" class="icommentjs icon16" href="/internet-examiner-toolkit-5-15-1509-1323-crack-4realtorrentz-t11287142.html#comment"> <em class="iconvalue">18</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/internet-examiner-toolkit-5-15-1509-1323-crack-4realtorrentz-t11287142.html" class="cellMainLink">Internet Examiner Toolkit 5.15.1509.1323 + Crack [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="118841928">113.34 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="20 Sep 2015, 05:45">3&nbsp;days</span></td>
			<td class="green center">133</td>
			<td class="red lasttd center">28</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11287160,0" class="icommentjs icon16" href="/maxon-cinema-4d-r17-hybrid-multilingual-crack-win-mac-appzdam-t11287160.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/maxon-cinema-4d-r17-hybrid-multilingual-crack-win-mac-appzdam-t11287160.html" class="cellMainLink">Maxon Cinema 4D R17 HYBRID Multilingual + Crack (WIN / MAC) - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="7652620978">7.13 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="20 Sep 2015, 05:52">3&nbsp;days</span></td>
			<td class="green center">85</td>
			<td class="red lasttd center">90</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11287604,0" class="icommentjs icon16" href="/android-top-of-the-week-apps-pack-20-9-2015-appzdam-t11287604.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/android-top-of-the-week-apps-pack-20-9-2015-appzdam-t11287604.html" class="cellMainLink">Android Top Of The Week Apps Pack (20/9/2015) - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="57605209">54.94 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="20 Sep 2015, 07:54">3&nbsp;days</span></td>
			<td class="green center">91</td>
			<td class="red lasttd center">13</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11289414,0" class="icommentjs icon16" href="/daz3d-poser-painful-moment-poses-for-g2m-g2f-t11289414.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/daz3d-poser-painful-moment-poses-for-g2m-g2f-t11289414.html" class="cellMainLink">Daz3D - Poser - Painful Moment Poses for G2M, G2F</a></div>
			</td>
			<td class="nobr center" data-sort="2787827">2.66 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="20 Sep 2015, 13:06">3&nbsp;days</span></td>
			<td class="green center">58</td>
			<td class="red lasttd center">1</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11296861,0" class="icommentjs icon16" href="/daz3d-poser-daz-studio-iray-hdr-interiors-dson-ds-only-t11296861.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/daz3d-poser-daz-studio-iray-hdr-interiors-dson-ds-only-t11296861.html" class="cellMainLink">DAZ3D - POSER DAZ Studio Iray HDR Interiors (DSON) (DS only)</a></div>
			</td>
			<td class="nobr center" data-sort="323540710">308.55 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="21 Sep 2015, 21:15">2&nbsp;days</span></td>
			<td class="green center">52</td>
			<td class="red lasttd center">7</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11294346,0" class="icommentjs icon16" href="/spectrasonics-omnisphere-patch-library-update-v2-1-0f-dada-t11294346.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/spectrasonics-omnisphere-patch-library-update-v2-1-0f-dada-t11294346.html" class="cellMainLink">Spectrasonics - Omnisphere Patch Library UPDATE v2.1.0f [dada]</a></div>
			</td>
			<td class="nobr center" data-sort="31877570">30.4 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="21 Sep 2015, 12:24">2&nbsp;days</span></td>
			<td class="green center">45</td>
			<td class="red lasttd center">2</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11290361,0" class="icommentjs icon16" href="/daz3d-nano-suit-for-genesis-2-male-s-t11290361.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/daz3d-nano-suit-for-genesis-2-male-s-t11290361.html" class="cellMainLink">Daz3D - Nano Suit for Genesis 2 Male(s)</a></div>
			</td>
			<td class="nobr center" data-sort="78068511">74.45 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="20 Sep 2015, 17:08">3&nbsp;days</span></td>
			<td class="green center">42</td>
			<td class="red lasttd center">0</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11289446,0" class="icommentjs icon16" href="/daz3d-poser-diesel-outfit-for-genesis-2-male-s-t11289446.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/daz3d-poser-diesel-outfit-for-genesis-2-male-s-t11289446.html" class="cellMainLink">Daz3D - Poser - Diesel Outfit for Genesis 2 Male(s)</a></div>
			</td>
			<td class="nobr center" data-sort="45984134">43.85 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="20 Sep 2015, 13:14">3&nbsp;days</span></td>
			<td class="green center">40</td>
			<td class="red lasttd center">3</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11296919,0" class="icommentjs icon16" href="/daz3d-poser-ro-88708-midnight-obsession-for-v4-dim-t11296919.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/daz3d-poser-ro-88708-midnight-obsession-for-v4-dim-t11296919.html" class="cellMainLink">DAZ3D - POSER RO-88708 Midnight OBSESSION for V4 (DIM)</a></div>
			</td>
			<td class="nobr center" data-sort="11708259">11.17 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="21 Sep 2015, 21:28">2&nbsp;days</span></td>
			<td class="green center">38</td>
			<td class="red lasttd center">2</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11298011,0" class="icommentjs icon16" href="/poser-daz3d-nikisatez-move-trainers-v4-t11298011.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/poser-daz3d-nikisatez-move-trainers-v4-t11298011.html" class="cellMainLink">Poser - Daz3D - nikisatez - Move Trainers V4</a></div>
			</td>
			<td class="nobr center" data-sort="9137685">8.71 <span>MB</span></td>
			<td class="center">8</td>
			<td class="center"><span title="22 Sep 2015, 03:55">1&nbsp;day</span></td>
			<td class="green center">32</td>
			<td class="red lasttd center">1</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11294314,0" class="icommentjs icon16" href="/hotspot-shield-elite-vpn-proxy-5-20-1-patch-b-tman-t11294314.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/hotspot-shield-elite-vpn-proxy-5-20-1-patch-b-tman-t11294314.html" class="cellMainLink">Hotspot Shield Elite VPN &amp; Proxy 5.20.1 + Patch {B@tman}</a></div>
			</td>
			<td class="nobr center" data-sort="30688405">29.27 <span>MB</span></td>
			<td class="center">11</td>
			<td class="center"><span title="21 Sep 2015, 12:17">2&nbsp;days</span></td>
			<td class="green center">31</td>
			<td class="red lasttd center">3</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11290428,0" class="icommentjs icon16" href="/dragon-ball-super-011-french-subbed-mp4-t11290428.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/dragon-ball-super-011-french-subbed-mp4-t11290428.html" class="cellMainLink">Dragon Ball Super - 011 [French Subbed].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="332025015">316.64 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="20 Sep 2015, 17:29">3&nbsp;days</span></td>
			<td class="green center">1335</td>
			<td class="red lasttd center">65</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11289999,0" class="icommentjs icon16" href="/horriblesubs-god-eater-08-720p-mkv-t11289999.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/horriblesubs-god-eater-08-720p-mkv-t11289999.html" class="cellMainLink">[HorribleSubs] GOD EATER - 08 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="613867443">585.43 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="20 Sep 2015, 15:30">3&nbsp;days</span></td>
			<td class="green center">1137</td>
			<td class="red lasttd center">91</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11287888,0" class="icommentjs icon16" href="/animerg-dragon-ball-super-011-720p-phr0sty-mkv-t11287888.html#comment"> <em class="iconvalue">33</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/animerg-dragon-ball-super-011-720p-phr0sty-mkv-t11287888.html" class="cellMainLink">[AnimeRG] Dragon Ball Super 011 - 720p [Phr0stY].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="540298301">515.27 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="20 Sep 2015, 09:21">3&nbsp;days</span></td>
			<td class="green center">780</td>
			<td class="red lasttd center">97</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/leopard-raws-overlord-12-raw-atx-1280x720-x264-aac-mp4-t11303260.html" class="cellMainLink">[Leopard-Raws] Overlord - 12 RAW (ATX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="418335830">398.96 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="23 Sep 2015, 03:45">19&nbsp;hours</span></td>
			<td class="green center">476</td>
			<td class="red lasttd center">320</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11292313,0" class="icommentjs icon16" href="/deadfish-the-last-naruto-the-movie-movie-bd-720p-aac-mp4-t11292313.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/deadfish-the-last-naruto-the-movie-movie-bd-720p-aac-mp4-t11292313.html" class="cellMainLink">[DeadFish] The Last: Naruto the Movie - Movie [BD][720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="2559868040">2.38 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="21 Sep 2015, 04:21">2&nbsp;days</span></td>
			<td class="green center">222</td>
			<td class="red lasttd center">236</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/fff-shokugeki-no-souma-21-d63a85d5-mkv-t11294413.html" class="cellMainLink">[FFF] Shokugeki no Souma - 21 [D63A85D5].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="638512939">608.93 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="21 Sep 2015, 12:45">2&nbsp;days</span></td>
			<td class="green center">203</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11297280,0" class="icommentjs icon16" href="/ae-one-punch-man-01-preair-720p-mp4-t11297280.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/ae-one-punch-man-01-preair-720p-mp4-t11297280.html" class="cellMainLink">[AE] One-Punch Man - 01 [Preair] [720p].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="185885527">177.27 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="21 Sep 2015, 23:26">1&nbsp;day</span></td>
			<td class="green center">198</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11300735,0" class="icommentjs icon16" href="/bakedfish-overlord-12-720p-aac-mp4-t11300735.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/bakedfish-overlord-12-720p-aac-mp4-t11300735.html" class="cellMainLink">[BakedFish] Overlord - 12 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="327777042">312.59 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="22 Sep 2015, 16:16">1&nbsp;day</span></td>
			<td class="green center">156</td>
			<td class="red lasttd center">28</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/commie-bikini-warriors-12-b5f43ca8-mkv-t11302179.html" class="cellMainLink">[Commie] Bikini Warriors - 12 [B5F43CA8].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="56086471">53.49 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="22 Sep 2015, 20:55">1&nbsp;day</span></td>
			<td class="green center">160</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11292413,0" class="icommentjs icon16" href="/deadfish-overlord-ple-ple-pleiades-01-07-specials-batch-720p-mp4-aac-t11292413.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/deadfish-overlord-ple-ple-pleiades-01-07-specials-batch-720p-mp4-aac-t11292413.html" class="cellMainLink">[DeadFish] Overlord: Ple Ple Pleiades - 01 - 07 - Specials - Batch [720p][MP4][AAC]</a></div>
			</td>
			<td class="nobr center" data-sort="314843241">300.26 <span>MB</span></td>
			<td class="center">7</td>
			<td class="center"><span title="21 Sep 2015, 04:52">2&nbsp;days</span></td>
			<td class="green center">77</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11287221,0" class="icommentjs icon16" href="/one-piece-710-480p-eng-sub-72mb-iorchid-t11287221.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/one-piece-710-480p-eng-sub-72mb-iorchid-t11287221.html" class="cellMainLink">One Piece - 710 [480p][Eng Sub][72mb]_iORcHiD</a></div>
			</td>
			<td class="nobr center" data-sort="74722819">71.26 <span>MB</span></td>
			<td class="center">8</td>
			<td class="center"><span title="20 Sep 2015, 06:10">3&nbsp;days</span></td>
			<td class="green center">67</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/commie-gatchaman-crowds-insight-11-a9fefdc8-mkv-t11285989.html" class="cellMainLink">[Commie] Gatchaman Crowds insight - 11 [A9FEFDC8].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="424370419">404.71 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="19 Sep 2015, 23:40">3&nbsp;days</span></td>
			<td class="green center">50</td>
			<td class="red lasttd center">4</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/commie-ace-of-the-diamond-second-season-25-c9237452-mkv-t11297134.html" class="cellMainLink">[Commie] Ace of the Diamond ~Second Season~ - 25 [C9237452].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="357879971">341.3 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="21 Sep 2015, 22:35">2&nbsp;days</span></td>
			<td class="green center">48</td>
			<td class="red lasttd center">4</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/deadfish-rokka-no-yuusha-11-720p-aac-mp4-t11302024.html" class="cellMainLink">[DeadFish] Rokka no Yuusha - 11 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="289577972">276.16 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="22 Sep 2015, 20:04">1&nbsp;day</span></td>
			<td class="green center">39</td>
			<td class="red lasttd center">9</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/animerg-one-piece-710-720p-scavvykid-mp4-t11286563.html" class="cellMainLink">[AnimeRG] One Piece - 710 [720p] [ScavvyKiD].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="473839270">451.89 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="20 Sep 2015, 02:29">3&nbsp;days</span></td>
			<td class="green center">34</td>
			<td class="red lasttd center">6</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11288049,0" class="icommentjs icon16" href="/hacking-web-intelligence-open-source-intelligence-and-web-reconnaissance-concepts-and-techniques-1st-edition-2015-pdf-gooner-t11288049.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/hacking-web-intelligence-open-source-intelligence-and-web-reconnaissance-concepts-and-techniques-1st-edition-2015-pdf-gooner-t11288049.html" class="cellMainLink">Hacking Web Intelligence - Open Source Intelligence and Web Reconnaissance Concepts and Techniques - 1st Edition (2015).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="38227148">36.46 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="20 Sep 2015, 10:22">3&nbsp;days</span></td>
			<td class="green center">458</td>
			<td class="red lasttd center">32</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11292039,0" class="icommentjs icon16" href="/the-new-york-times-bestsellers-september-27-2015-fiction-non-fiction-t11292039.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/the-new-york-times-bestsellers-september-27-2015-fiction-non-fiction-t11292039.html" class="cellMainLink">The New York Times Bestsellers - September 27, 2015 [Fiction / Non-Fiction]</a></div>
			</td>
			<td class="nobr center" data-sort="104109879">99.29 <span>MB</span></td>
			<td class="center">23</td>
			<td class="center"><span title="21 Sep 2015, 02:58">2&nbsp;days</span></td>
			<td class="green center">360</td>
			<td class="red lasttd center">79</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11291063,0" class="icommentjs icon16" href="/healthy-brain-happy-life-a-personal-program-to-activate-your-brain-and-do-everything-better-2015-epub-gooner-t11291063.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/healthy-brain-happy-life-a-personal-program-to-activate-your-brain-and-do-everything-better-2015-epub-gooner-t11291063.html" class="cellMainLink">Healthy Brain, Happy Life - A Personal Program to Activate Your Brain and Do Everything Better (2015).epub Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="1073109">1.02 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="20 Sep 2015, 21:22">3&nbsp;days</span></td>
			<td class="green center">373</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11288007,0" class="icommentjs icon16" href="/hacking-and-penetration-testing-with-low-power-devices-1st-edition-2014-pdf-gooner-t11288007.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/hacking-and-penetration-testing-with-low-power-devices-1st-edition-2014-pdf-gooner-t11288007.html" class="cellMainLink">Hacking and Penetration Testing with Low Power Devices - 1st Edition (2014).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="73476944">70.07 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="20 Sep 2015, 10:08">3&nbsp;days</span></td>
			<td class="green center">350</td>
			<td class="red lasttd center">27</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11286403,0" class="icommentjs icon16" href="/photography-magazines-september-20-2015-true-pdf-t11286403.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/photography-magazines-september-20-2015-true-pdf-t11286403.html" class="cellMainLink">Photography Magazines - September 20 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="74301909">70.86 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="20 Sep 2015, 01:26">3&nbsp;days</span></td>
			<td class="green center">334</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11286874,0" class="icommentjs icon16" href="/travel-magazines-bundle-sept-20-2015-true-pdf-t11286874.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/travel-magazines-bundle-sept-20-2015-true-pdf-t11286874.html" class="cellMainLink">Travel Magazines Bundle - Sept 20 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="178631027">170.36 <span>MB</span></td>
			<td class="center">9</td>
			<td class="center"><span title="20 Sep 2015, 04:14">3&nbsp;days</span></td>
			<td class="green center">244</td>
			<td class="red lasttd center">52</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11301185,0" class="icommentjs icon16" href="/injustice-gods-among-us-year-four-021-2015-digital-son-of-ultron-empire-cbr-nem-t11301185.html#comment"> <em class="iconvalue">17</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/injustice-gods-among-us-year-four-021-2015-digital-son-of-ultron-empire-cbr-nem-t11301185.html" class="cellMainLink">Injustice - Gods Among Us - Year Four 021 (2015) (digital) (Son of Ultron-Empire).cbr (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="24838866">23.69 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="22 Sep 2015, 17:01">1&nbsp;day</span></td>
			<td class="green center">245</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11285954,0" class="icommentjs icon16" href="/bicycle-magazines-september-20-2015-true-pdf-t11285954.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/bicycle-magazines-september-20-2015-true-pdf-t11285954.html" class="cellMainLink">Bicycle Magazines - September 20 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="152590569">145.52 <span>MB</span></td>
			<td class="center">8</td>
			<td class="center"><span title="19 Sep 2015, 23:28">3&nbsp;days</span></td>
			<td class="green center">209</td>
			<td class="red lasttd center">39</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11290493,0" class="icommentjs icon16" href="/herbs-and-natural-supplements-an-evidence-based-guide-4th-edition-volume-1-2-2015-pdf-gooner-t11290493.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/herbs-and-natural-supplements-an-evidence-based-guide-4th-edition-volume-1-2-2015-pdf-gooner-t11290493.html" class="cellMainLink">Herbs and Natural Supplements - An Evidence-Based Guide - 4th Edition (Volume 1 &amp; 2) (2015) (Pdf) Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="49375003">47.09 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="20 Sep 2015, 17:54">3&nbsp;days</span></td>
			<td class="green center">175</td>
			<td class="red lasttd center">27</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11290991,0" class="icommentjs icon16" href="/happiness-a-philosopher-s-guide-2015-epub-gooner-t11290991.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/happiness-a-philosopher-s-guide-2015-epub-gooner-t11290991.html" class="cellMainLink">Happiness - A Philosopher&#039;s Guide (2015).epub Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="2101332">2 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="20 Sep 2015, 20:43">3&nbsp;days</span></td>
			<td class="green center">158</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11298618,0" class="icommentjs icon16" href="/j-k-rowling-harry-potter-audio-books-1-7-read-by-stephen-fry-mp3-t11298618.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/j-k-rowling-harry-potter-audio-books-1-7-read-by-stephen-fry-mp3-t11298618.html" class="cellMainLink">J. K. Rowling - Harry Potter Audio Books 1-7; Read by Stephen Fry [MP3]</a></div>
			</td>
			<td class="nobr center" data-sort="1350620034">1.26 <span>GB</span></td>
			<td class="center">202</td>
			<td class="center"><span title="22 Sep 2015, 06:51">1&nbsp;day</span></td>
			<td class="green center">117</td>
			<td class="red lasttd center">77</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11286798,0" class="icommentjs icon16" href="/assorted-magazines-september-20-2015-true-pdf-t11286798.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/assorted-magazines-september-20-2015-true-pdf-t11286798.html" class="cellMainLink">Assorted Magazines - September 20 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="166966518">159.23 <span>MB</span></td>
			<td class="center">11</td>
			<td class="center"><span title="20 Sep 2015, 03:49">3&nbsp;days</span></td>
			<td class="green center">101</td>
			<td class="red lasttd center">48</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/hot-and-hip-grilling-secrets-a-fresh-look-at-cooking-with-fire-2015-pdf-epub-gooner-t11296544.html" class="cellMainLink">Hot and Hip Grilling Secrets - A Fresh Look at Cooking with Fire (2015) (Pdf &amp; Epub) Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="56817264">54.19 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="21 Sep 2015, 19:33">2&nbsp;days</span></td>
			<td class="green center">109</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11299684,0" class="icommentjs icon16" href="/maximum-pc-november-2015-t11299684.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/maximum-pc-november-2015-t11299684.html" class="cellMainLink">Maximum PC â November 2015</a></div>
			</td>
			<td class="nobr center" data-sort="31430432">29.97 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="22 Sep 2015, 12:50">1&nbsp;day</span></td>
			<td class="green center">110</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11303061,0" class="icommentjs icon16" href="/home-garden-magazines-sept-23-2015-true-pdf-t11303061.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/home-garden-magazines-sept-23-2015-true-pdf-t11303061.html" class="cellMainLink">Home &amp; Garden Magazines - Sept 23 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="1117004255">1.04 <span>GB</span></td>
			<td class="center">48</td>
			<td class="center"><span title="23 Sep 2015, 02:37">20&nbsp;hours</span></td>
			<td class="green center">3</td>
			<td class="red lasttd center">222</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11295773,0" class="icommentjs icon16" href="/va-romantic-piano-moods-3-cd-boxed-set-flac-t11295773.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/va-romantic-piano-moods-3-cd-boxed-set-flac-t11295773.html" class="cellMainLink">VA - Romantic Piano Moods 3 CD Boxed Set [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1190751266">1.11 <span>GB</span></td>
			<td class="center">72</td>
			<td class="center"><span title="21 Sep 2015, 17:41">2&nbsp;days</span></td>
			<td class="green center">145</td>
			<td class="red lasttd center">95</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11294487,0" class="icommentjs icon16" href="/faces-you-can-make-me-dance-5cd-box-2015-flac-t11294487.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/faces-you-can-make-me-dance-5cd-box-2015-flac-t11294487.html" class="cellMainLink">Faces - You Can Make Me Dance (5CD-Box 2015) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1793942882">1.67 <span>GB</span></td>
			<td class="center">122</td>
			<td class="center"><span title="21 Sep 2015, 13:02">2&nbsp;days</span></td>
			<td class="green center">120</td>
			<td class="red lasttd center">39</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11288002,0" class="icommentjs icon16" href="/buddy-guy-born-to-play-guitar-2015-24-96-hd-flac-t11288002.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/buddy-guy-born-to-play-guitar-2015-24-96-hd-flac-t11288002.html" class="cellMainLink">Buddy Guy - Born To Play Guitar (2015) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="2087092420">1.94 <span>GB</span></td>
			<td class="center">42</td>
			<td class="center"><span title="20 Sep 2015, 10:06">3&nbsp;days</span></td>
			<td class="green center">115</td>
			<td class="red lasttd center">48</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/eric-clapton-live-at-budokan-flac-tntvillage-t11297407.html" class="cellMainLink">Eric Clapton - Live at Budokan [Flac][TntVillage]</a></div>
			</td>
			<td class="nobr center" data-sort="405327350">386.55 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center"><span title="22 Sep 2015, 00:30">1&nbsp;day</span></td>
			<td class="green center">93</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11299074,0" class="icommentjs icon16" href="/santana-welcome-2014-24-96-hd-flac-t11299074.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/santana-welcome-2014-24-96-hd-flac-t11299074.html" class="cellMainLink">Santana - Welcome (2014) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1202782311">1.12 <span>GB</span></td>
			<td class="center">24</td>
			<td class="center"><span title="22 Sep 2015, 09:43">1&nbsp;day</span></td>
			<td class="green center">81</td>
			<td class="red lasttd center">33</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11299576,0" class="icommentjs icon16" href="/new-order-music-complete-japanese-edition-2015-flac-sn3h1t87-glodls-t11299576.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/new-order-music-complete-japanese-edition-2015-flac-sn3h1t87-glodls-t11299576.html" class="cellMainLink">New Order - Music Complete (Japanese Edition) [2015] [FLAC] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="536392947">511.54 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span title="22 Sep 2015, 12:19">1&nbsp;day</span></td>
			<td class="green center">85</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11293434,0" class="icommentjs icon16" href="/david-gilmour-rattlethat-lock-2015-wav-24-96-t11293434.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/david-gilmour-rattlethat-lock-2015-wav-24-96-t11293434.html" class="cellMainLink">David Gilmour -RattleThat Lock 2015 wav 24-96</a></div>
			</td>
			<td class="nobr center" data-sort="1781434440">1.66 <span>GB</span></td>
			<td class="center">24</td>
			<td class="center"><span title="21 Sep 2015, 09:14">2&nbsp;days</span></td>
			<td class="green center">83</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11285997,0" class="icommentjs icon16" href="/grateful-dead-30-trips-around-the-sun-the-definitive-live-story-4cd-box-set-2015-flac-beolab1700-t11285997.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/grateful-dead-30-trips-around-the-sun-the-definitive-live-story-4cd-box-set-2015-flac-beolab1700-t11285997.html" class="cellMainLink">Grateful Dead - 30 Trips Around the Sun The Definitive Live Story [4CD Box Set] (2015) FLAC Beolab1700</a></div>
			</td>
			<td class="nobr center" data-sort="1647357296">1.53 <span>GB</span></td>
			<td class="center">34</td>
			<td class="center"><span title="19 Sep 2015, 23:42">3&nbsp;days</span></td>
			<td class="green center">85</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11287664,0" class="icommentjs icon16" href="/va-oriental-garden-the-world-of-oriental-grooves-the-box-6-cd-box-set-flac-t11287664.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/va-oriental-garden-the-world-of-oriental-grooves-the-box-6-cd-box-set-flac-t11287664.html" class="cellMainLink">VA - Oriental Garden - The World of Oriental Grooves The Box 6 CD Box Set [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="3560986276">3.32 <span>GB</span></td>
			<td class="center">107</td>
			<td class="center"><span title="20 Sep 2015, 08:10">3&nbsp;days</span></td>
			<td class="green center">65</td>
			<td class="red lasttd center">57</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11287364,0" class="icommentjs icon16" href="/va-classics-on-a-summer-s-day-double-cd-flac-t11287364.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/va-classics-on-a-summer-s-day-double-cd-flac-t11287364.html" class="cellMainLink">VA - Classics on a Summer&#039;s Day Double CD [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="507162788">483.67 <span>MB</span></td>
			<td class="center">34</td>
			<td class="center"><span title="20 Sep 2015, 06:49">3&nbsp;days</span></td>
			<td class="green center">68</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11290337,0" class="icommentjs icon16" href="/ornette-coleman-the-shape-of-jazz-to-come-2013-stereo-24-192-hd-flac-t11290337.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/ornette-coleman-the-shape-of-jazz-to-come-2013-stereo-24-192-hd-flac-t11290337.html" class="cellMainLink">Ornette Coleman - The Shape Of Jazz To Come (2013) [Stereo 24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1418891019">1.32 <span>GB</span></td>
			<td class="center">18</td>
			<td class="center"><span title="20 Sep 2015, 17:02">3&nbsp;days</span></td>
			<td class="green center">64</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11299723,0" class="icommentjs icon16" href="/john-coltrane-blue-train-2012-24-192-hd-flac-t11299723.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/john-coltrane-blue-train-2012-24-192-hd-flac-t11299723.html" class="cellMainLink">John Coltrane - Blue Train (2012) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="2146708144">2 <span>GB</span></td>
			<td class="center">17</td>
			<td class="center"><span title="22 Sep 2015, 13:01">1&nbsp;day</span></td>
			<td class="green center">50</td>
			<td class="red lasttd center">36</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11301745,0" class="icommentjs icon16" href="/rolling-stones-out-of-our-heads-us-uk-2005-24-88-hd-flac-t11301745.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/rolling-stones-out-of-our-heads-us-uk-2005-24-88-hd-flac-t11301745.html" class="cellMainLink">Rolling Stones - Out Of Our Heads US + UK (2005) [24-88 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="870844725">830.5 <span>MB</span></td>
			<td class="center">68</td>
			<td class="center"><span title="22 Sep 2015, 18:50">1&nbsp;day</span></td>
			<td class="green center">55</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11293445,0" class="icommentjs icon16" href="/bruce-springsteen-the-promise-the-darkness-on-the-edge-of-town-story-2010-flac-t11293445.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/bruce-springsteen-the-promise-the-darkness-on-the-edge-of-town-story-2010-flac-t11293445.html" class="cellMainLink">Bruce Springsteen - The Promise The Darkness On The Edge Of Town Story (2010) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="890664270">849.4 <span>MB</span></td>
			<td class="center">60</td>
			<td class="center"><span title="21 Sep 2015, 09:18">2&nbsp;days</span></td>
			<td class="green center">47</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/frank-sinatra-past-present-future-vinyl-yeraycito-master-series-t11300470.html" class="cellMainLink">Frank Sinatra - Past, Present &amp; Future (VINYL) YERAYCITO MASTER SERIES</a></div>
			</td>
			<td class="nobr center" data-sort="2189285811">2.04 <span>GB</span></td>
			<td class="center">29</td>
			<td class="center"><span title="22 Sep 2015, 15:32">1&nbsp;day</span></td>
			<td class="green center">38</td>
			<td class="red lasttd center">18</td>
        </tr>
			</table>


	<script type="text/javascript">
		if ($.cookie('kat_settings[fpw_column]') != null && $.cookie('kat_settings[fpw_order]')) {
			var colIndex = $.cookie('kat_settings[fpw_column]')
			var ascend = $.cookie('kat_settings[fpw_order]');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			sortTables(colIndex, ascend);
		}
		$('.frontPageWidget .firstr th .fpw-sort').click(function() {
			var colIndex = $(this).parent().index();
			var ascend = !$(this).hasClass('sortedAsc')?1:0;
			$('.fpw-sort').removeClass('sortedAsc sortedDesc');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			updateFPWsettings(colIndex, ascend);
			sortTables(colIndex, ascend);
		});
		function sortTables(colIndex, ascend) {	
			$('.frontPageWidget').each(function() {
				var data = [];
				$('tr:not(.firstr)', $(this)).each(function() {
					var name = $('td:first .cellMainLink', $(this)).text();
					var id = $('td:first .cellMainLink', $(this)).attr('href').match(/.*-t(\d+)\.html/)[1];
					var fileSize = $('td:eq(1)', $(this)).attr('data-sort');
					data.push({
						'col0':name, 'col1':padNum(fileSize), 'col2':padNum($('td:eq(2)',$(this)).text()), 'col3':padNum(id), 'col4':padNum($('td:eq(4)',$(this)).text()), 'col5':padNum($('td:eq(5)',$(this)).text()), 'html':$(this).html()
					});
				});
				data.sort(function(a, b) {
					var x = a['col'+colIndex].toLowerCase(); var y = b['col'+colIndex].toLowerCase();
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
				if (!ascend||ascend=='0') data.reverse();
				$(this).find('tr:not(.firstr)').remove();
				for (var i=0;i<data.length;i++) {
					$(this).append('<tr class="'+(i+1%2==0?'even':'odd')+'">'+data[i].html+'</tr>');
				}
			});
		}
		function padNum(val) {
			val = '0000000000000000'+val;
			return val.substring(val.length-15);
		}
		function updateFPWsettings(t, o) {
			$.cookie('kat_settings[fpw_column]', t, { expires: 365, path: '/' });
			$.cookie('kat_settings[fpw_order]', o, { expires: 365, path: '/' });
		}
	</script>
		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
    
            
        <div id="_119b0a17fab5493361a252d04bf527db"></div>
    
                
    	    <div class="advertising">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <div id="_7063408f1c01d50e0dc2d833186ce962"></div>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/mr-dj-repacks-pc-games/?unread=16926467">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Mr DJ repacks - PC Games
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/xxoxxo/">xxoxxo</a></span></span> <span title="23 Sep 2015, 23:16">2&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/intel-or-amd/?unread=16926464">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				intel or AMD
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/beastmaster11/">beastmaster11</a></span></span> <span title="23 Sep 2015, 23:15">4&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/quiz-can-you-guess-movie-v14/?unread=16926463">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Quiz; Can you guess this movie? V14
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/Annagp3/">Annagp3</a></span></span> <span title="23 Sep 2015, 23:14">4&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/courtesies-downloading/?unread=16926461">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Courtesies of Downloading
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_2"><a class="plain" href="/user/MG./">MG.</a></span></span> <span title="23 Sep 2015, 23:14">5&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/redbaron58-uploads/?unread=16926458">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				RedBaron58 Uploads
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_8"><a class="plain" href="/user/RedBaron58/">RedBaron58</a></span></span> <span title="23 Sep 2015, 23:13">5&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/vpn-tried-pia-it-s-way-too-slow/?unread=16926457">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				VPN....tried PIA but it&#039;s way too slow.
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/zi69y/">zi69y</a></span></span> <span title="23 Sep 2015, 23:13">6&nbsp;min.&nbsp;ago</span></span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/summer-updates-september-1/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Summer updates [September, 1]
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="01 Sep 2015, 16:13">3&nbsp;weeks&nbsp;ago</span></span>
	</li>
	<li>
		<a href="/blog/post/kickasstorrents-is-moving-to-kat-cr-domain/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				KickassTorrents is moving to kat.cr domain
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="24 Apr 2015, 10:13">5&nbsp;months&nbsp;ago</span></span>
	</li>
	<li>
		<a href="/blog/post/happy-torrents-day-2015/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Happy Torrents Day 2015!
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="30 Mar 2015, 10:06">5&nbsp;months&nbsp;ago</span></span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/olderthangod/post/lucy/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Lucy</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/olderthangod/">olderthangod</a> <span title="23 Sep 2015, 22:25">53&nbsp;min.&nbsp;ago</span></span></li>
	<li><a href="/blog/lithium_love/post/muslims-demand-jersey-city-schools-close-down-for-their-holiday/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Muslims Demand Jersey City Schools close down for their holiday</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/lithium_love/">lithium_love</a> <span title="23 Sep 2015, 20:32">2&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/magicpotions/post/on-a-swiss-revolution-by-watertiger/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> On a Swiss revolution by watertiger</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/magicpotions/">magicpotions</a> <span title="23 Sep 2015, 17:43">5&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/CtShoN/post/my-uploads-are-on-hold-for-a-while/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> My Uploads are On Hold For a While</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/CtShoN/">CtShoN</a> <span title="23 Sep 2015, 15:40">7&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/floofiness/post/perspective-perception-and-other-musings/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Perspective, perception and other musings</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/floofiness/">floofiness</a> <span title="23 Sep 2015, 15:08">8&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/OptimusPr1me/post/mali-memories/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Mali Memories</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/OptimusPr1me/">OptimusPr1me</a> <span title="23 Sep 2015, 07:41">15&nbsp;hours&nbsp;ago</span></span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/hindi%20movies/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				hindi movies
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/drag%20race%20season%203%20is_safe%3A1/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				drag race season 3 is_safe:1
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/2014%20nl/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				2014 nl
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/funny%20games%20%282007%29/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Funny Games (2007)
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/monster%20cock%2015%20inch/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				monster cock 15 inch
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/damien%20rice%20discography/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				damien rice discography
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/parks%20and%20rec%20season%207/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				parks and rec season 7
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/disturbing%20behavior/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Disturbing Behavior
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/the%20age%20of%20manipulation/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				the age of manipulation
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/switchback/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				switchback
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/anna%20bell%20peaks/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Anna Bell Peaks
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		<li>
			<a data-nop href="http://www.torrentreactor.net/" target="_blank" rel="external">
				<span class="itorreact thirdPartIcons"></span>TorrentReactor
			</a>
		</li>
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a onclick="setLanguage('bsc', '.kat.cr');return false;" class="plain">Bosnian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a onclick="setLanguage('ko', '.kat.cr');return false;" class="plain">Korean</a></li>
                                <li><a onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
		<li><a href="/welcome/">welcome</a></li>
		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <div id="_673e31f53f8166159b8e996c4124765b"></div>
        <div id="_e7050fb15fd39b3e4e99a5be4a57b6ea"></div>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script><script>
 sc('addGlobal', 'pagetype', 'front');
 sc('addSlot', '_60318cd4e8d28f6fb76fe34e9bd9c498');
sc('addSlot', '_39ecb76dd457e5ac33776fdf11500d56');
sc('addSlot', '_277923e5f9d753c5b0630c28e641790c');
sc('addSlot', '_119b0a17fab5493361a252d04bf527db');
sc('addSlot', '_7063408f1c01d50e0dc2d833186ce962', { 'searchQuery': '' });
sc('addSlot', '_673e31f53f8166159b8e996c4124765b');
sc('addSlot', '_e7050fb15fd39b3e4e99a5be4a57b6ea');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>

</body>
</html>
