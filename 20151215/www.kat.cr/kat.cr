<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="auto">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-3cf7bb5.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-3cf7bb5.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-3cf7bb5.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-3cf7bb5.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kickass.to/sc-3cf7bb5.js');

        sc('setHost', 'a.kickass.to');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: '3cf7bb5',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-3cf7bb5.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<div  data-sc-slot="_60318cd4e8d28f6fb76fe34e9bd9c498"></div>
<div  data-sc-slot="_39ecb76dd457e5ac33776fdf11500d56"></div>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka ka-settings"></i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka ka-search"></i></button></div>
			</form>
		</div>
        <div  data-sc-slot="_277923e5f9d753c5b0630c28e641790c"></div>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080p/" class="tag6">1080p</a>
	<a href="/search/2015/" class="tag10">2015</a>
	<a href="/search/3d/" class="tag3">3d</a>
	<a href="/search/adele/" class="tag2">adele</a>
	<a href="/search/agent%20x/" class="tag2">agent x</a>
	<a href="/search/android/" class="tag2">android</a>
	<a href="/search/apk/" class="tag2">apk</a>
	<a href="/search/christmas/" class="tag5">christmas</a>
	<a href="/search/creed/" class="tag2">creed</a>
	<a href="/search/csi%20cyber/" class="tag2">csi cyber</a>
	<a href="/search/discography/" class="tag3">discography</a>
	<a href="/search/dual%20audio%20hindi/" class="tag3">dual audio hindi</a>
	<a href="/search/etrg/" class="tag2">etrg</a>
	<a href="/search/family%20guy/" class="tag2">family guy</a>
	<a href="/search/fargo/" class="tag2">fargo</a>
	<a href="/search/french/" class="tag3">french</a>
	<a href="/search/hindi/" class="tag9">hindi</a>
	<a href="/search/hindi%202015/" class="tag5">hindi 2015</a>
	<a href="/search/homeland/" class="tag4">homeland</a>
	<a href="/search/homeland%20s05e11/" class="tag3">homeland s05e11</a>
	<a href="/search/into%20the%20badlands/" class="tag2">into the badlands</a>
	<a href="/search/is%20safe%201/" class="tag3">is safe 1</a>
	<a href="/search/ita/" class="tag2">ita</a>
	<a href="/search/legend/" class="tag2">legend</a>
	<a href="/search/limitless/" class="tag2">limitless</a>
	<a href="/search/malayalam/" class="tag3">malayalam</a>
	<a href="/search/nezu/" class="tag10">nezu</a>
	<a href="/search/nl/" class="tag2">nl</a>
	<a href="/search/one%20punch%20man/" class="tag3">one punch man</a>
	<a href="/search/quantico/" class="tag3">quantico</a>
	<a href="/search/quantico%20s01e11/" class="tag2">quantico s01e11</a>
	<a href="/search/sicario/" class="tag2">sicario</a>
	<a href="/search/spectre/" class="tag3">spectre</a>
	<a href="/search/star%20wars/" class="tag5">star wars</a>
	<a href="/search/tamil/" class="tag4">tamil</a>
	<a href="/search/telugu/" class="tag3">telugu</a>
	<a href="/search/telugu%202015/" class="tag3">telugu 2015</a>
	<a href="/search/the%20affair/" class="tag2">the affair</a>
	<a href="/search/the%20blacklist/" class="tag2">the blacklist</a>
	<a href="/search/the%20intern/" class="tag3">the intern</a>
	<a href="/search/the%20martian/" class="tag3">the martian</a>
	<a href="/search/the%20walking%20dead/" class="tag4">the walking dead</a>
	<a href="/search/walking%20dead/" class="tag2">walking dead</a>
	<a href="/search/wwe/" class="tag2">wwe</a>
	<a href="/search/wwe%20tlc%202015/" class="tag2">wwe tlc 2015</a>
	<a href="/search/yify/" class="tag10">yify</a>
	<a href="/search/yify%201080p/" class="tag4">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag4">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11744988,0" class="icommentjs kaButton smallButton rightButton" href="/the-intern-2015-hdrip-xvid-ac3-evo-t11744988.html#comment">169 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-intern-2015-hdrip-xvid-ac3-evo-t11744988.html" class="cellMainLink">The Intern 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1539925746">1.43 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T19:07:32+00:00">12 Dec 2015, 19:07:32</span></td>
			<td class="green center">18569</td>
			<td class="red lasttd center">12657</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11746177,0" class="icommentjs kaButton smallButton rightButton" href="/pan-2015-1080p-bluray-x264-dts-jyk-t11746177.html#comment">40 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/pan-2015-1080p-bluray-x264-dts-jyk-t11746177.html" class="cellMainLink">Pan 2015 1080p BluRay x264 DTS-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="3018021659">2.81 <span>GB</span></td>
			<td class="center">8</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T01:52:33+00:00">13 Dec 2015, 01:52:33</span></td>
			<td class="green center">7892</td>
			<td class="red lasttd center">4621</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11743263,0" class="icommentjs kaButton smallButton rightButton" href="/you-call-it-passion-2015-720p-hdrip-h264-cinefox-movietam-t11743263.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/you-call-it-passion-2015-720p-hdrip-h264-cinefox-movietam-t11743263.html" class="cellMainLink">You Call It Passion 2015 720p HDRip H264-CINEFOX [MovietaM]</a></div>
			</td>
			<td class="nobr center" data-sort="2613390864">2.43 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T12:37:46+00:00">12 Dec 2015, 12:37:46</span></td>
			<td class="green center">6218</td>
			<td class="red lasttd center">1261</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11755698,0" class="icommentjs kaButton smallButton rightButton" href="/marvel-super-hero-adventures-frost-2015-hdrip-xvid-ac3-evo-t11755698.html#comment">15 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/marvel-super-hero-adventures-frost-2015-hdrip-xvid-ac3-evo-t11755698.html" class="cellMainLink">Marvel Super Hero Adventures Frost 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1494957150">1.39 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T18:49:12+00:00">14 Dec 2015, 18:49:12</span></td>
			<td class="green center">2902</td>
			<td class="red lasttd center">5736</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11750316,0" class="icommentjs kaButton smallButton rightButton" href="/crimson-peak-2015-1080p-web-dl-x264-ac3-jyk-t11750316.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/crimson-peak-2015-1080p-web-dl-x264-ac3-jyk-t11750316.html" class="cellMainLink">Crimson Peak 2015 1080p WEB-DL x264 AC3-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="3501593551">3.26 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T18:18:48+00:00">13 Dec 2015, 18:18:48</span></td>
			<td class="green center">3183</td>
			<td class="red lasttd center">1937</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11744969,0" class="icommentjs kaButton smallButton rightButton" href="/ronaldo-2015-hdrip-xvid-ac3-filoumoutonrip-t11744969.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ronaldo-2015-hdrip-xvid-ac3-filoumoutonrip-t11744969.html" class="cellMainLink">Ronaldo 2015 HDRip XviD AC3 filoumoutonRIP</a></div>
			</td>
			<td class="nobr center" data-sort="1496226624">1.39 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T19:03:44+00:00">12 Dec 2015, 19:03:44</span></td>
			<td class="green center">3449</td>
			<td class="red lasttd center">1345</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11744617,0" class="icommentjs kaButton smallButton rightButton" href="/hitman-agent-47-2015-truefrench-bdrip-xvid-avitech-filoumoutonrip-t11744617.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/hitman-agent-47-2015-truefrench-bdrip-xvid-avitech-filoumoutonrip-t11744617.html" class="cellMainLink">Hitman Agent 47 2015 TRUEFRENCH BDRiP XViD-AViTECH filoumoutonRIP</a></div>
			</td>
			<td class="nobr center" data-sort="734665582">700.63 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T17:43:41+00:00">12 Dec 2015, 17:43:41</span></td>
			<td class="green center">3231</td>
			<td class="red lasttd center">662</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11755959,0" class="icommentjs kaButton smallButton rightButton" href="/kill-kapone-2015-hdrip-xvid-ac3-evo-t11755959.html#comment">17 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/kill-kapone-2015-hdrip-xvid-ac3-evo-t11755959.html" class="cellMainLink">Kill Kapone 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1459665394">1.36 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T19:57:08+00:00">14 Dec 2015, 19:57:08</span></td>
			<td class="green center">1857</td>
			<td class="red lasttd center">3330</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11743273,0" class="icommentjs kaButton smallButton rightButton" href="/the-sound-of-a-flower-2015-720p-hdrip-h264-aac-pchd-movietam-t11743273.html#comment">15 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-sound-of-a-flower-2015-720p-hdrip-h264-aac-pchd-movietam-t11743273.html" class="cellMainLink">The Sound Of A Flower 2015 720p HDRip H264 AAC-PCHD [MovietaM]</a></div>
			</td>
			<td class="nobr center" data-sort="2663095205">2.48 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T12:39:35+00:00">12 Dec 2015, 12:39:35</span></td>
			<td class="green center">2989</td>
			<td class="red lasttd center">695</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11755676,0" class="icommentjs kaButton smallButton rightButton" href="/city-of-vultures-2015-hdrip-xvid-ac3-evo-t11755676.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/city-of-vultures-2015-hdrip-xvid-ac3-evo-t11755676.html" class="cellMainLink">City of Vultures 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1495113566">1.39 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T18:43:27+00:00">14 Dec 2015, 18:43:27</span></td>
			<td class="green center">1755</td>
			<td class="red lasttd center">3068</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11740233,0" class="icommentjs kaButton smallButton rightButton" href="/dixieland-2015-hdrip-xvid-ac3-evo-t11740233.html#comment">20 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dixieland-2015-hdrip-xvid-ac3-evo-t11740233.html" class="cellMainLink">Dixieland 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1468289041">1.37 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T21:04:41+00:00">11 Dec 2015, 21:04:41</span></td>
			<td class="green center">2218</td>
			<td class="red lasttd center">1725</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11745397,0" class="icommentjs kaButton smallButton rightButton" href="/brooklyn-bizarre-2015-720p-webrip-x264-aac-etrg-t11745397.html#comment">17 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/brooklyn-bizarre-2015-720p-webrip-x264-aac-etrg-t11745397.html" class="cellMainLink">Brooklyn Bizarre 2015 720p WEBRip x264 AAC-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="787965729">751.46 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T21:04:58+00:00">12 Dec 2015, 21:04:58</span></td>
			<td class="green center">2144</td>
			<td class="red lasttd center">1479</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11758207,0" class="icommentjs kaButton smallButton rightButton" href="/the-lobster-2015-hdrip-xvid-ac3-evo-t11758207.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-lobster-2015-hdrip-xvid-ac3-evo-t11758207.html" class="cellMainLink">The Lobster 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1498824602">1.4 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-15T06:51:55+00:00">15 Dec 2015, 06:51:55</span></td>
			<td class="green center">1259</td>
			<td class="red lasttd center">2816</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11739754,0" class="icommentjs kaButton smallButton rightButton" href="/the-ridiculous-6-2015-hdrip-xvid-ac3-evo-t11739754.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-ridiculous-6-2015-hdrip-xvid-ac3-evo-t11739754.html" class="cellMainLink">The Ridiculous 6 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1510780875">1.41 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T18:40:49+00:00">11 Dec 2015, 18:40:49</span></td>
			<td class="green center">1867</td>
			<td class="red lasttd center">1192</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11758782,0" class="icommentjs kaButton smallButton rightButton" href="/paranormal-activity-the-ghost-dimension-2015-unrated-hdrip-xvid-ac3-evo-t11758782.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/paranormal-activity-the-ghost-dimension-2015-unrated-hdrip-xvid-ac3-evo-t11758782.html" class="cellMainLink">Paranormal Activity The Ghost Dimension 2015 UNRATED HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1467539496">1.37 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-15T09:25:35+00:00">15 Dec 2015, 09:25:35</span></td>
			<td class="green center">1085</td>
			<td class="red lasttd center">2522</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11757176,0" class="icommentjs kaButton smallButton rightButton" href="/supergirl-s01e08-hdtv-x264-lol-ettv-t11757176.html#comment">72 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/supergirl-s01e08-hdtv-x264-lol-ettv-t11757176.html" class="cellMainLink">Supergirl S01E08 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="301322087">287.36 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-15T01:58:28+00:00">15 Dec 2015, 01:58:28</span></td>
			<td class="green center">14858</td>
			<td class="red lasttd center">7423</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11751941,0" class="icommentjs kaButton smallButton rightButton" href="/homeland-s05e11-web-dl-x264-fum-ettv-t11751941.html#comment">81 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/homeland-s05e11-web-dl-x264-fum-ettv-t11751941.html" class="cellMainLink">Homeland S05E11 WEB-DL x264-FUM[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="370118346">352.97 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T02:29:26+00:00">14 Dec 2015, 02:29:26</span></td>
			<td class="green center">12472</td>
			<td class="red lasttd center">1537</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11751825,0" class="icommentjs kaButton smallButton rightButton" href="/quantico-s01e11-hdtv-x264-lol-ettv-t11751825.html#comment">116 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/quantico-s01e11-hdtv-x264-lol-ettv-t11751825.html" class="cellMainLink">Quantico S01E11 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="287262095">273.95 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T01:58:25+00:00">14 Dec 2015, 01:58:25</span></td>
			<td class="green center">11305</td>
			<td class="red lasttd center">2469</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11757383,0" class="icommentjs kaButton smallButton rightButton" href="/scorpion-s02e12-hdtv-x264-lol-ettv-t11757383.html#comment">34 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/scorpion-s02e12-hdtv-x264-lol-ettv-t11757383.html" class="cellMainLink">Scorpion S02E12 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="281517038">268.48 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-15T02:58:20+00:00">15 Dec 2015, 02:58:20</span></td>
			<td class="green center">7954</td>
			<td class="red lasttd center">3333</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11757770,0" class="icommentjs kaButton smallButton rightButton" href="/wwe-raw-2015-12-14-hdtv-x264-jkkk-sparrow-t11757770.html#comment">22 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/wwe-raw-2015-12-14-hdtv-x264-jkkk-sparrow-t11757770.html" class="cellMainLink">WWE RAW 2015 12 14 HDTV x264-jkkk -={SPARROW}=-</a></div>
			</td>
			<td class="nobr center" data-sort="1603402884">1.49 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-15T05:05:58+00:00">15 Dec 2015, 05:05:58</span></td>
			<td class="green center">4944</td>
			<td class="red lasttd center">4764</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11757616,0" class="icommentjs kaButton smallButton rightButton" href="/fargo-s02e10-hdtv-x264-killers-rartv-t11757616.html#comment">39 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fargo-s02e10-hdtv-x264-killers-rartv-t11757616.html" class="cellMainLink">Fargo S02E10 HDTV x264-KILLERS[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="303095036">289.05 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-15T04:15:07+00:00">15 Dec 2015, 04:15:07</span></td>
			<td class="green center">5559</td>
			<td class="red lasttd center">1813</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11751879,0" class="icommentjs kaButton smallButton rightButton" href="/brooklyn-nine-nine-s03e10-hdtv-x264-killers-ettv-t11751879.html#comment">40 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/brooklyn-nine-nine-s03e10-hdtv-x264-killers-ettv-t11751879.html" class="cellMainLink">Brooklyn Nine-Nine S03E10 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="197545693">188.39 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T02:08:24+00:00">14 Dec 2015, 02:08:24</span></td>
			<td class="green center">5659</td>
			<td class="red lasttd center">607</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11742380,0" class="icommentjs kaButton smallButton rightButton" href="/ash-vs-evil-dead-s01e07-hdtv-x264-killers-ettv-t11742380.html#comment">98 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ash-vs-evil-dead-s01e07-hdtv-x264-killers-ettv-t11742380.html" class="cellMainLink">Ash vs Evil Dead S01E07 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="297308860">283.54 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T07:54:26+00:00">12 Dec 2015, 07:54:26</span></td>
			<td class="green center">5102</td>
			<td class="red lasttd center">406</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11751953,0" class="icommentjs kaButton smallButton rightButton" href="/family-guy-s14e09-hdtv-x264-killers-ettv-t11751953.html#comment">34 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/family-guy-s14e09-hdtv-x264-killers-ettv-t11751953.html" class="cellMainLink">Family Guy S14E09 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="80954164">77.2 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T02:32:18+00:00">14 Dec 2015, 02:32:18</span></td>
			<td class="green center">4807</td>
			<td class="red lasttd center">367</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11752039,0" class="icommentjs kaButton smallButton rightButton" href="/agent-x-us-s01e07-hdtv-x264-lol-ettv-t11752039.html#comment">33 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/agent-x-us-s01e07-hdtv-x264-lol-ettv-t11752039.html" class="cellMainLink">Agent X US S01E07 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="272570286">259.94 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T03:02:19+00:00">14 Dec 2015, 03:02:19</span></td>
			<td class="green center">4159</td>
			<td class="red lasttd center">729</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11757779,0" class="icommentjs kaButton smallButton rightButton" href="/fargo-s02e10-repack-hdtv-x264-killers-ettv-t11757779.html#comment">37 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fargo-s02e10-repack-hdtv-x264-killers-ettv-t11757779.html" class="cellMainLink">Fargo S02E10 REPACK HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="327747671">312.56 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-15T05:08:18+00:00">15 Dec 2015, 05:08:18</span></td>
			<td class="green center">3865</td>
			<td class="red lasttd center">1138</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11752024,0" class="icommentjs kaButton smallButton rightButton" href="/the-good-wife-s07e10-hdtv-x264-lol-ettv-t11752024.html#comment">23 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-good-wife-s07e10-hdtv-x264-lol-ettv-t11752024.html" class="cellMainLink">The Good Wife S07E10 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="189677546">180.89 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T02:59:19+00:00">14 Dec 2015, 02:59:19</span></td>
			<td class="green center">3795</td>
			<td class="red lasttd center">320</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11752057,0" class="icommentjs kaButton smallButton rightButton" href="/the-last-man-on-earth-s02e10-hdtv-x264-killers-ettv-t11752057.html#comment">52 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-last-man-on-earth-s02e10-hdtv-x264-killers-ettv-t11752057.html" class="cellMainLink">The Last Man On Earth S02E10 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="146521656">139.73 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T03:05:23+00:00">14 Dec 2015, 03:05:23</span></td>
			<td class="green center">3534</td>
			<td class="red lasttd center">371</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11753649,0" class="icommentjs kaButton smallButton rightButton" href="/keeping-up-with-the-kardashians-s11e05-hdtv-x264-ltbs-mp4-t11753649.html#comment">26 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/keeping-up-with-the-kardashians-s11e05-hdtv-x264-ltbs-mp4-t11753649.html" class="cellMainLink">Keeping Up with the Kardashians S11E05 HDTV x264-LTBS mp4</a></div>
			</td>
			<td class="nobr center" data-sort="424677142">405 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T09:31:55+00:00">14 Dec 2015, 09:31:55</span></td>
			<td class="green center">3151</td>
			<td class="red lasttd center">514</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11741397,0" class="icommentjs kaButton smallButton rightButton" href="/grimm-s05e06-hdtv-x264-fleet-rartv-t11741397.html#comment">72 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/grimm-s05e06-hdtv-x264-fleet-rartv-t11741397.html" class="cellMainLink">Grimm S05E06 HDTV x264-FLEET[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="284556218">271.37 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T03:00:24+00:00">12 Dec 2015, 03:00:24</span></td>
			<td class="green center">3193</td>
			<td class="red lasttd center">351</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11740200,0" class="icommentjs kaButton smallButton rightButton" href="/lacrim-r-i-p-r-o-vol-2-t11740200.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/lacrim-r-i-p-r-o-vol-2-t11740200.html" class="cellMainLink">Lacrim - R I P R O Vol 2</a></div>
			</td>
			<td class="nobr center" data-sort="95548450">91.12 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T20:52:50+00:00">11 Dec 2015, 20:52:50</span></td>
			<td class="green center">2224</td>
			<td class="red lasttd center">153</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11755569,0" class="icommentjs kaButton smallButton rightButton" href="/va-44-hits-winter-2016-2015-mp3-320-kbps-t11755569.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-44-hits-winter-2016-2015-mp3-320-kbps-t11755569.html" class="cellMainLink">VA - 44 Hits Winter 2016 (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="393745939">375.51 <span>MB</span></td>
			<td class="center">46</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T18:19:02+00:00">14 Dec 2015, 18:19:02</span></td>
			<td class="green center">296</td>
			<td class="red lasttd center">173</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11755630,0" class="icommentjs kaButton smallButton rightButton" href="/megadeth-the-threat-is-surreal-ep-2015-320ak-t11755630.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/megadeth-the-threat-is-surreal-ep-2015-320ak-t11755630.html" class="cellMainLink">Megadeth - The Threat Is Surreal (EP) 2015) 320ak</a></div>
			</td>
			<td class="nobr center" data-sort="52437041">50.01 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T18:31:25+00:00">14 Dec 2015, 18:31:25</span></td>
			<td class="green center">343</td>
			<td class="red lasttd center">62</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/nas-unreleased-2015-320-kbps-t11755825.html" class="cellMainLink">Nas â Unreleased (2015) 320 KBPS</a></div>
			</td>
			<td class="nobr center" data-sort="108044591">103.04 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T19:20:46+00:00">14 Dec 2015, 19:20:46</span></td>
			<td class="green center">289</td>
			<td class="red lasttd center">128</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11745286,0" class="icommentjs kaButton smallButton rightButton" href="/va-vocal-deep-house-vol-12-2015-mp3-t11745286.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-vocal-deep-house-vol-12-2015-mp3-t11745286.html" class="cellMainLink">VA - Vocal Deep House Vol.12 (2015) MP3</a></div>
			</td>
			<td class="nobr center" data-sort="325369261">310.3 <span>MB</span></td>
			<td class="center">31</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T20:28:37+00:00">12 Dec 2015, 20:28:37</span></td>
			<td class="green center">295</td>
			<td class="red lasttd center">67</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11755337,0" class="icommentjs kaButton smallButton rightButton" href="/juicy-j-os-to-oscars-320kbps-2015-official-mixjoint-t11755337.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/juicy-j-os-to-oscars-320kbps-2015-official-mixjoint-t11755337.html" class="cellMainLink">Juicy J - Os To Oscars [320Kbps] [2015] [Official] (MixJoint)</a></div>
			</td>
			<td class="nobr center" data-sort="119018968">113.51 <span>MB</span></td>
			<td class="center">19</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T17:14:10+00:00">14 Dec 2015, 17:14:10</span></td>
			<td class="green center">247</td>
			<td class="red lasttd center">82</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11739363,0" class="icommentjs kaButton smallButton rightButton" href="/willow-smith-ardipithecus-alternative-2015-mp3-t11739363.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/willow-smith-ardipithecus-alternative-2015-mp3-t11739363.html" class="cellMainLink">Willow Smith - ARDIPITHECUS [Alternative 2015 MP3]</a></div>
			</td>
			<td class="nobr center" data-sort="131890285">125.78 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T17:15:34+00:00">11 Dec 2015, 17:15:34</span></td>
			<td class="green center">261</td>
			<td class="red lasttd center">47</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11749663,0" class="icommentjs kaButton smallButton rightButton" href="/prince-hitnrun-phase-two-pop-2015-t11749663.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/prince-hitnrun-phase-two-pop-2015-t11749663.html" class="cellMainLink">Prince - HITnRUN Phase Two [Pop 2015]</a></div>
			</td>
			<td class="nobr center" data-sort="139970410">133.49 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T15:52:45+00:00">13 Dec 2015, 15:52:45</span></td>
			<td class="green center">260</td>
			<td class="red lasttd center">32</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11755738,0" class="icommentjs kaButton smallButton rightButton" href="/the-best-of-2015-vol-1-mp3-divxtotal-t11755738.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-best-of-2015-vol-1-mp3-divxtotal-t11755738.html" class="cellMainLink">The Best Of 2015, Vol. 1 MP3-DiVXTOTAL</a></div>
			</td>
			<td class="nobr center" data-sort="276126791">263.34 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T19:00:22+00:00">14 Dec 2015, 19:00:22</span></td>
			<td class="green center">243</td>
			<td class="red lasttd center">62</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11755501,0" class="icommentjs kaButton smallButton rightButton" href="/the-howard-stern-show-and-the-wrap-up-show-2015-12-14-t11755501.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-howard-stern-show-and-the-wrap-up-show-2015-12-14-t11755501.html" class="cellMainLink">The Howard Stern Show and The Wrap Up Show - 2015-12-14</a></div>
			</td>
			<td class="nobr center" data-sort="271506154">258.93 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T18:03:01+00:00">14 Dec 2015, 18:03:01</span></td>
			<td class="green center">180</td>
			<td class="red lasttd center">63</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11749513,0" class="icommentjs kaButton smallButton rightButton" href="/hinder-when-the-smoke-clears-2015-ak-t11749513.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/hinder-when-the-smoke-clears-2015-ak-t11749513.html" class="cellMainLink">Hinder - When the Smoke Clears 2015 ak</a></div>
			</td>
			<td class="nobr center" data-sort="84087714">80.19 <span>MB</span></td>
			<td class="center">11</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T15:08:59+00:00">13 Dec 2015, 15:08:59</span></td>
			<td class="green center">161</td>
			<td class="red lasttd center">33</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11753196,0" class="icommentjs kaButton smallButton rightButton" href="/muddy-waters-the-chess-singles-collection-2015-freak37-t11753196.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/muddy-waters-the-chess-singles-collection-2015-freak37-t11753196.html" class="cellMainLink">Muddy Waters - The Chess Singles Collection (2015)...Freak37</a></div>
			</td>
			<td class="nobr center" data-sort="568713951">542.37 <span>MB</span></td>
			<td class="center">83</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T07:30:50+00:00">14 Dec 2015, 07:30:50</span></td>
			<td class="green center">120</td>
			<td class="red lasttd center">73</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11757871,0" class="icommentjs kaButton smallButton rightButton" href="/christmas-collection-2015-vol-12-glodls-t11757871.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/christmas-collection-2015-vol-12-glodls-t11757871.html" class="cellMainLink">Christmas Collection 2015 VOL 12 [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="4609586966">4.29 <span>GB</span></td>
			<td class="center">620</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-15T05:29:28+00:00">15 Dec 2015, 05:29:28</span></td>
			<td class="green center">68</td>
			<td class="red lasttd center">155</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/various-artists-ball-dancing-2015-mp3-t11758915.html" class="cellMainLink">Various Artists - Ball Dancing (2015) MP3</a></div>
			</td>
			<td class="nobr center" data-sort="1109328978">1.03 <span>GB</span></td>
			<td class="center">102</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-15T09:57:51+00:00">15 Dec 2015, 09:57:51</span></td>
			<td class="green center">103</td>
			<td class="red lasttd center">77</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11755853,0" class="icommentjs kaButton smallButton rightButton" href="/va-classical-music-for-christmas-2015-320-kbps-t11755853.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-classical-music-for-christmas-2015-320-kbps-t11755853.html" class="cellMainLink">VA â Classical Music for Christmas (2015) 320 KBPS</a></div>
			</td>
			<td class="nobr center" data-sort="144333318">137.65 <span>MB</span></td>
			<td class="center">19</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T19:29:17+00:00">14 Dec 2015, 19:29:17</span></td>
			<td class="green center">97</td>
			<td class="red lasttd center">28</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11751383,0" class="icommentjs kaButton smallButton rightButton" href="/fallout-4-v-1-2-37-2015-r-g-mechanics-t11751383.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/fallout-4-v-1-2-37-2015-r-g-mechanics-t11751383.html" class="cellMainLink">Fallout 4 [v 1.2.37] (2015) [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center" data-sort="20218963675">18.83 <span>GB</span></td>
			<td class="center">21</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T23:32:02+00:00">13 Dec 2015, 23:32:02</span></td>
			<td class="green center">722</td>
			<td class="red lasttd center">854</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11739756,0" class="icommentjs kaButton smallButton rightButton" href="/total-war-attila-update-6-dlcs-rus-2015-pc-repack-Ð¾Ñ-xatab-t11739756.html#comment">13 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/total-war-attila-update-6-dlcs-rus-2015-pc-repack-Ð¾Ñ-xatab-t11739756.html" class="cellMainLink">Total War: ATTILA [Update 6 + DLCs][RUS] (2015) PC | RePack Ð¾Ñ xatab</a></div>
			</td>
			<td class="nobr center" data-sort="9939043226">9.26 <span>GB</span></td>
			<td class="center">10</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T18:41:46+00:00">11 Dec 2015, 18:41:46</span></td>
			<td class="green center">605</td>
			<td class="red lasttd center">153</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11743388,0" class="icommentjs kaButton smallButton rightButton" href="/the-sims-4-deluxe-edition-2014-v-1-13-104-1010-pc-repack-by-r-g-freedom-t11743388.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/the-sims-4-deluxe-edition-2014-v-1-13-104-1010-pc-repack-by-r-g-freedom-t11743388.html" class="cellMainLink">The SIMS 4 Deluxe Edition (2014) v 1.13.104.1010 PC RePack by R.G. Freedom</a></div>
			</td>
			<td class="nobr center" data-sort="11075956331">10.32 <span>GB</span></td>
			<td class="center">9</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T13:09:14+00:00">12 Dec 2015, 13:09:14</span></td>
			<td class="green center">535</td>
			<td class="red lasttd center">201</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11744326,0" class="icommentjs kaButton smallButton rightButton" href="/pes-2016-pro-evolution-soccer-2016-v-1-03-00-2015-repack-mizantrop1337-t11744326.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/pes-2016-pro-evolution-soccer-2016-v-1-03-00-2015-repack-mizantrop1337-t11744326.html" class="cellMainLink">PES 2016 : Pro Evolution Soccer 2016 [v 1.03.00] (2015) RePack [Mizantrop1337]</a></div>
			</td>
			<td class="nobr center" data-sort="3930853406">3.66 <span>GB</span></td>
			<td class="center">19</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T16:42:13+00:00">12 Dec 2015, 16:42:13</span></td>
			<td class="green center">347</td>
			<td class="red lasttd center">246</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11747843,0" class="icommentjs kaButton smallButton rightButton" href="/plague-inc-evolved-v0-9-0-3-2014-repack-decepticon-t11747843.html#comment">17 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/plague-inc-evolved-v0-9-0-3-2014-repack-decepticon-t11747843.html" class="cellMainLink">Plague Inc: Evolved [v0.9.0.3] (2014) RePack [Decepticon]</a></div>
			</td>
			<td class="nobr center" data-sort="295972849">282.26 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T07:42:48+00:00">13 Dec 2015, 07:42:48</span></td>
			<td class="green center">316</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11741679,0" class="icommentjs kaButton smallButton rightButton" href="/lightning-returns-final-fantasy-xiii-multi8-all-dlcs-fitgirl-repack-selective-download-from-7-58-gb-t11741679.html#comment">66 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/lightning-returns-final-fantasy-xiii-multi8-all-dlcs-fitgirl-repack-selective-download-from-7-58-gb-t11741679.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/lightning-returns-final-fantasy-xiii-multi8-all-dlcs-fitgirl-repack-selective-download-from-7-58-gb-t11741679.html" class="cellMainLink">Lightning Returns: Final Fantasy XIII (MULTI8 + All DLCs) [FitGirl Repack, Selective Download - from 7.58 GB]</a></div>
			</td>
			<td class="nobr center" data-sort="12567745406">11.7 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T04:47:34+00:00">12 Dec 2015, 04:47:34</span></td>
			<td class="green center">94</td>
			<td class="red lasttd center">446</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11742975,0" class="icommentjs kaButton smallButton rightButton" href="/spirit-of-revenge-3-gem-fury-collector-s-edition-asg-t11742975.html#comment">15 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/spirit-of-revenge-3-gem-fury-collector-s-edition-asg-t11742975.html" class="cellMainLink">Spirit of Revenge 3 - Gem Fury Collector&#039;s Edition [ASG]</a></div>
			</td>
			<td class="nobr center" data-sort="976099762">930.88 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T10:58:17+00:00">12 Dec 2015, 10:58:17</span></td>
			<td class="green center">268</td>
			<td class="red lasttd center">38</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11755819,0" class="icommentjs kaButton smallButton rightButton" href="/starcraft-ii-legacy-of-the-void-russian-prophet-t11755819.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/starcraft-ii-legacy-of-the-void-russian-prophet-t11755819.html" class="cellMainLink">StarCraft II Legacy of The Void [RUSSiAN] - PROPHET</a></div>
			</td>
			<td class="nobr center" data-sort="20771215194">19.34 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T19:19:12+00:00">14 Dec 2015, 19:19:12</span></td>
			<td class="green center">136</td>
			<td class="red lasttd center">214</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11748063,0" class="icommentjs kaButton smallButton rightButton" href="/dying-light-ultimate-edition-v1-6-2-dlcs-2015-fixed-repack-r-g-freedom-t11748063.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/dying-light-ultimate-edition-v1-6-2-dlcs-2015-fixed-repack-r-g-freedom-t11748063.html" class="cellMainLink">Dying Light: Ultimate Edition [v1.6.2 + DLCs] (2015)Fixed-Repack [R.G. Freedom]</a></div>
			</td>
			<td class="nobr center" data-sort="11042647569">10.28 <span>GB</span></td>
			<td class="center">14</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T08:48:31+00:00">13 Dec 2015, 08:48:31</span></td>
			<td class="green center">173</td>
			<td class="red lasttd center">133</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11743736,0" class="icommentjs kaButton smallButton rightButton" href="/the-sims-4-get-together-full-games4theworld-t11743736.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/the-sims-4-get-together-full-games4theworld-t11743736.html" class="cellMainLink">The Sims 4: Get Together [FULL] * Games4theworld *</a></div>
			</td>
			<td class="nobr center" data-sort="4018984971">3.74 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T14:39:59+00:00">12 Dec 2015, 14:39:59</span></td>
			<td class="green center">164</td>
			<td class="red lasttd center">118</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11747930,0" class="icommentjs kaButton smallButton rightButton" href="/this-war-of-mine-v-2-0-2014-steamrip-let-sÐ lay-t11747930.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/this-war-of-mine-v-2-0-2014-steamrip-let-sÐ lay-t11747930.html" class="cellMainLink">This War of Mine [v.2.0] (2014) SteamRip [Let&#039;sÐ lay]</a></div>
			</td>
			<td class="nobr center" data-sort="1335549083">1.24 <span>GB</span></td>
			<td class="center">8</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T08:09:54+00:00">13 Dec 2015, 08:09:54</span></td>
			<td class="green center">193</td>
			<td class="red lasttd center">36</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11747865,0" class="icommentjs kaButton smallButton rightButton" href="/might-and-magic-heroes-vii-deluxe-edition-v-1-60-2015-repack-decepticon-t11747865.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/might-and-magic-heroes-vii-deluxe-edition-v-1-60-2015-repack-decepticon-t11747865.html" class="cellMainLink">Might and Magic Heroes VII: Deluxe Edition [v 1.60] (2015) RePack[Decepticon]</a></div>
			</td>
			<td class="nobr center" data-sort="9726783913">9.06 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T07:50:23+00:00">13 Dec 2015, 07:50:23</span></td>
			<td class="green center">150</td>
			<td class="red lasttd center">54</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11742788,0" class="icommentjs kaButton smallButton rightButton" href="/killing-floor-2-v1018-repack-by-w-a-l-t11742788.html#comment">13 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/killing-floor-2-v1018-repack-by-w-a-l-t11742788.html" class="cellMainLink">Killing Floor 2 v1018 Repack by [W.A.L]</a></div>
			</td>
			<td class="nobr center" data-sort="8882599640">8.27 <span>GB</span></td>
			<td class="center">167</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T09:46:51+00:00">12 Dec 2015, 09:46:51</span></td>
			<td class="green center">134</td>
			<td class="red lasttd center">64</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/xatab-repack-divinity-original-sin-enhanced-edition-v-2-0-103-346-2015-t11755385.html" class="cellMainLink">[Xatab] Repack : Divinity: Original Sin - Enhanced Edition [v 2.0.103.346] (2015)</a></div>
			</td>
			<td class="nobr center" data-sort="9529282343">8.87 <span>GB</span></td>
			<td class="center">9</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T17:31:45+00:00">14 Dec 2015, 17:31:45</span></td>
			<td class="green center">121</td>
			<td class="red lasttd center">83</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11751237,0" class="icommentjs kaButton smallButton rightButton" href="/project-cars-update-13-dlc-s-2015-steam-rip-let-sÐ lay-t11751237.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/project-cars-update-13-dlc-s-2015-steam-rip-let-sÐ lay-t11751237.html" class="cellMainLink">Project CARS [Update 13 + DLC&#039;s] (2015) Steam-Rip [Let&#039;sÐ lay]</a></div>
			</td>
			<td class="nobr center" data-sort="20307823084">18.91 <span>GB</span></td>
			<td class="center">11</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T22:32:04+00:00">13 Dec 2015, 22:32:04</span></td>
			<td class="green center">73</td>
			<td class="red lasttd center">147</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11742955,0" class="icommentjs kaButton smallButton rightButton" href="/gimp-2-8-16-setup-exe-t11742955.html#comment">20 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/gimp-2-8-16-setup-exe-t11742955.html" class="cellMainLink">gimp-2 8 16-setup exe</a></div>
			</td>
			<td class="nobr center" data-sort="96819488">92.33 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T10:48:59+00:00">12 Dec 2015, 10:48:59</span></td>
			<td class="green center">1987</td>
			<td class="red lasttd center">35</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11742511,0" class="icommentjs kaButton smallButton rightButton" href="/internet-download-manager-idm-6-25-build-8-registered-32bit-64bit-patch-crackingpatching-t11742511.html#comment">49 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/internet-download-manager-idm-6-25-build-8-registered-32bit-64bit-patch-crackingpatching-t11742511.html" class="cellMainLink">Internet Download Manager (IDM) 6.25 Build 8 Registered (32bit + 64bit Patch) [CrackingPatching]</a></div>
			</td>
			<td class="nobr center" data-sort="10372305">9.89 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T08:37:39+00:00">12 Dec 2015, 08:37:39</span></td>
			<td class="green center">549</td>
			<td class="red lasttd center">218</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11743679,0" class="icommentjs kaButton smallButton rightButton" href="/keys-for-eset-kaspersky-avast-dr-web-avira-december-12-2015-t11743679.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/keys-for-eset-kaspersky-avast-dr-web-avira-december-12-2015-t11743679.html" class="cellMainLink">KEYS for ESET, Kaspersky, Avast, Dr.Web, Avira [December 12] (2015)</a></div>
			</td>
			<td class="nobr center" data-sort="447071">436.59 <span>KB</span></td>
			<td class="center">111</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T14:24:59+00:00">12 Dec 2015, 14:24:59</span></td>
			<td class="green center">235</td>
			<td class="red lasttd center">127</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11742041,0" class="icommentjs kaButton smallButton rightButton" href="/windows-7-x86-x64-aio-33in1-oem-en-us-dec-2015-generation2-t11742041.html#comment">49 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-7-x86-x64-aio-33in1-oem-en-us-dec-2015-generation2-t11742041.html" class="cellMainLink">Windows 7 X86 X64 AIO 33in1 OEM en-US Dec 2015 {Generation2}</a></div>
			</td>
			<td class="nobr center" data-sort="4804391900">4.47 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T06:16:58+00:00">12 Dec 2015, 06:16:58</span></td>
			<td class="green center">167</td>
			<td class="red lasttd center">183</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11747618,0" class="icommentjs kaButton smallButton rightButton" href="/ashampoo-burning-studio-2016-build-16-0-2-13-multilingual-crack-4realtorrentz-t11747618.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/ashampoo-burning-studio-2016-build-16-0-2-13-multilingual-crack-4realtorrentz-t11747618.html" class="cellMainLink">Ashampoo Burning Studio 2016 Build 16.0.2.13 Multilingual + Crack [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="90290466">86.11 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T06:26:24+00:00">13 Dec 2015, 06:26:24</span></td>
			<td class="green center">196</td>
			<td class="red lasttd center">18</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11743365,0" class="icommentjs kaButton smallButton rightButton" href="/spyhunter-4-20-9-4533-12-12-2015-2015-pc-repack-portable-by-d-akov-t11743365.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/spyhunter-4-20-9-4533-12-12-2015-2015-pc-repack-portable-by-d-akov-t11743365.html" class="cellMainLink">SpyHunter 4.20.9.4533 [12.12.2015] (2015) PC | RePack &amp; Portable by D! Akov</a></div>
			</td>
			<td class="nobr center" data-sort="46547078">44.39 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T13:04:53+00:00">12 Dec 2015, 13:04:53</span></td>
			<td class="green center">175</td>
			<td class="red lasttd center">19</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11743089,0" class="icommentjs kaButton smallButton rightButton" href="/dll-suite-9-0-0-2259-multilingual-crack-4realtorrentz-t11743089.html#comment">13 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/dll-suite-9-0-0-2259-multilingual-crack-4realtorrentz-t11743089.html" class="cellMainLink">DLL Suite 9.0.0.2259 Multilingual + Crack [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="23780207">22.68 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T11:45:25+00:00">12 Dec 2015, 11:45:25</span></td>
			<td class="green center">153</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11745360,0" class="icommentjs kaButton smallButton rightButton" href="/windows-7-ultimate-sp1-x64-en-us-esd-dec2015-pre-activated-team-os-t11745360.html#comment">29 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-7-ultimate-sp1-x64-en-us-esd-dec2015-pre-activated-team-os-t11745360.html" class="cellMainLink">Windows 7 Ultimate Sp1 x64 En-Us ESD Dec2015 Pre-Activated=-{TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="3008850105">2.8 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T20:51:06+00:00">12 Dec 2015, 20:51:06</span></td>
			<td class="green center">100</td>
			<td class="red lasttd center">115</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11756210,0" class="icommentjs kaButton smallButton rightButton" href="/reg-organizer-7-20-final-dc-14-12-2015-2015-repack-portable-kpojiuk-t11756210.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/reg-organizer-7-20-final-dc-14-12-2015-2015-repack-portable-kpojiuk-t11756210.html" class="cellMainLink">Reg Organizer 7.20 Final [DC 14.12.2015] (2015) RePack &amp; Portable [KpoJIuK]</a></div>
			</td>
			<td class="nobr center" data-sort="5947790">5.67 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T21:06:10+00:00">14 Dec 2015, 21:06:10</span></td>
			<td class="green center">146</td>
			<td class="red lasttd center">5</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11740103,0" class="icommentjs kaButton smallButton rightButton" href="/corel-paintshop-pro-x8-ultimate-18-1-0-67-multilingual-addons-incl-keygen-team-os-t11740103.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/corel-paintshop-pro-x8-ultimate-18-1-0-67-multilingual-addons-incl-keygen-team-os-t11740103.html" class="cellMainLink">Corel PaintShop Pro X8 Ultimate 18.1.0.67 Multilingual+Addons Incl Keygen-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="1575486068">1.47 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T20:27:28+00:00">11 Dec 2015, 20:27:28</span></td>
			<td class="green center">115</td>
			<td class="red lasttd center">62</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11749901,0" class="icommentjs kaButton smallButton rightButton" href="/aomei-partition-assistant-v6-0-final-serials-techtools-t11749901.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/aomei-partition-assistant-v6-0-final-serials-techtools-t11749901.html" class="cellMainLink">AOMEI Partition Assistant v6.0 FINAL + Serials [TechTools]</a></div>
			</td>
			<td class="nobr center" data-sort="44052410">42.01 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T16:46:40+00:00">13 Dec 2015, 16:46:40</span></td>
			<td class="green center">137</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11749884,0" class="icommentjs kaButton smallButton rightButton" href="/destroy-windows-10-spying-1-6-build-716-2015-mr-p2d-t11749884.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/destroy-windows-10-spying-1-6-build-716-2015-mr-p2d-t11749884.html" class="cellMainLink">Destroy Windows 10 Spying 1.6 Build 716 (2015)-=Mr.P2d=-</a></div>
			</td>
			<td class="nobr center" data-sort="303616">296.5 <span>KB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T16:42:21+00:00">13 Dec 2015, 16:42:21</span></td>
			<td class="green center">142</td>
			<td class="red lasttd center">5</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11750093,0" class="icommentjs kaButton smallButton rightButton" href="/daz3d-g3-male-body-morphs-head-morphs-expressions-t11750093.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/daz3d-g3-male-body-morphs-head-morphs-expressions-t11750093.html" class="cellMainLink">DAZ3D G3 Male Body Morphs - Head Morphs - Expressions</a></div>
			</td>
			<td class="nobr center" data-sort="7295898">6.96 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T17:32:15+00:00">13 Dec 2015, 17:32:15</span></td>
			<td class="green center">95</td>
			<td class="red lasttd center">6</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11756934,0" class="icommentjs kaButton smallButton rightButton" href="/piriform-speccy-professional-technician-1-29-714-multilingual-portable-4realtorrentz-t11756934.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/piriform-speccy-professional-technician-1-29-714-multilingual-portable-4realtorrentz-t11756934.html" class="cellMainLink">Piriform Speccy Professional &amp; Technician 1.29.714 Multilingual Portable [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="11084689">10.57 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-15T00:16:15+00:00">15 Dec 2015, 00:16:15</span></td>
			<td class="green center">85</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/slysoft-anydvd-anydvd-hd-7-6-6-0-final-crack-techtools-t11755972.html" class="cellMainLink">SlySoft AnyDVD &amp; AnyDVD HD 7.6.6.0 FINAL + Crack [TechTools]</a></div>
			</td>
			<td class="nobr center" data-sort="14634074">13.96 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T19:58:49+00:00">14 Dec 2015, 19:58:49</span></td>
			<td class="green center">76</td>
			<td class="red lasttd center">18</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11750599,0" class="icommentjs kaButton smallButton rightButton" href="/animerg-one-punch-man-11-720p-mkv-t11750599.html#comment">33 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-one-punch-man-11-720p-mkv-t11750599.html" class="cellMainLink">[AnimeRG] One Punch Man - 11 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="295956619">282.25 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T19:12:45+00:00">13 Dec 2015, 19:12:45</span></td>
			<td class="green center">1920</td>
			<td class="red lasttd center">394</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dragon-ball-super-23-720p-french-suubed-mp4-t11756397.html" class="cellMainLink">Dragon Ball Super 23 720p [FRENCH SUUBED].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="296810997">283.06 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T22:03:05+00:00">14 Dec 2015, 22:03:05</span></td>
			<td class="green center">1875</td>
			<td class="red lasttd center">211</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/kaerizaki-fansub-one-piece-722-vostfr-hd-1280x720-mp4-t11749273.html" class="cellMainLink">[Kaerizaki-Fansub] One Piece 722 [VOSTFR][HD 1280x720].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="294317781">280.68 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T14:19:04+00:00">13 Dec 2015, 14:19:04</span></td>
			<td class="green center">1039</td>
			<td class="red lasttd center">70</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11750086,0" class="icommentjs kaButton smallButton rightButton" href="/horriblesubs-one-punch-man-11-1080p-mkv-t11750086.html#comment">26 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-one-punch-man-11-1080p-mkv-t11750086.html" class="cellMainLink">[HorribleSubs] One-Punch Man - 11 [1080p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="948554861">904.61 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T17:30:02+00:00">13 Dec 2015, 17:30:02</span></td>
			<td class="green center">933</td>
			<td class="red lasttd center">65</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11749416,0" class="icommentjs kaButton smallButton rightButton" href="/arrg-dragon-ball-super-23-english-subbed-720p-sehjada-t11749416.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/arrg-dragon-ball-super-23-english-subbed-720p-sehjada-t11749416.html" class="cellMainLink">[ARRG]Dragon Ball Super - 23 English Subbed [720P] (Sehjada)</a></div>
			</td>
			<td class="nobr center" data-sort="159748775">152.35 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T14:42:58+00:00">13 Dec 2015, 14:42:58</span></td>
			<td class="green center">391</td>
			<td class="red lasttd center">41</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11750018,0" class="icommentjs kaButton smallButton rightButton" href="/one-punch-man-11-english-subbed-720p-arrg-lucifer22-t11750018.html#comment">17 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/one-punch-man-11-english-subbed-720p-arrg-lucifer22-t11750018.html" class="cellMainLink">One Punch Man - 11 English Subbed [720P][ARRG][Lucifer22]</a></div>
			</td>
			<td class="nobr center" data-sort="146807846">140.01 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T17:08:36+00:00">13 Dec 2015, 17:08:36</span></td>
			<td class="green center">231</td>
			<td class="red lasttd center">40</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11739386,0" class="icommentjs kaButton smallButton rightButton" href="/ninja-scoll-the-animated-series-480p-hevc-x265-t11739386.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ninja-scoll-the-animated-series-480p-hevc-x265-t11739386.html" class="cellMainLink">Ninja Scoll (The Animated Series) [480p] [HEVC] [x265]</a></div>
			</td>
			<td class="nobr center" data-sort="1369944204">1.28 <span>GB</span></td>
			<td class="center">20</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T17:20:37+00:00">11 Dec 2015, 17:20:37</span></td>
			<td class="green center">176</td>
			<td class="red lasttd center">105</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11746317,0" class="icommentjs kaButton smallButton rightButton" href="/arrg-fate-stay-night-unlimited-blade-works-episodes-00-25-complete-720p-dual-audio-lucifer22-t11746317.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/arrg-fate-stay-night-unlimited-blade-works-episodes-00-25-complete-720p-dual-audio-lucifer22-t11746317.html" class="cellMainLink">[ARRG] Fate/Stay Night - Unlimited Blade Works Episodes 00-25 Complete [720P][Dual Audio][Lucifer22]</a></div>
			</td>
			<td class="nobr center" data-sort="2386820582">2.22 <span>GB</span></td>
			<td class="center">26</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T02:36:36+00:00">13 Dec 2015, 02:36:36</span></td>
			<td class="green center">157</td>
			<td class="red lasttd center">130</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11739355,0" class="icommentjs kaButton smallButton rightButton" href="/ninja-scroll-1993-movie-dual-audio-1080p-hevc-x265-t11739355.html#comment">16 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ninja-scroll-1993-movie-dual-audio-1080p-hevc-x265-t11739355.html" class="cellMainLink">Ninja Scroll (1993 Movie) [DUAL-AUDIO] [1080p] [HEVC] [x265]</a></div>
			</td>
			<td class="nobr center" data-sort="810279039">772.74 <span>MB</span></td>
			<td class="center">8</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T17:13:41+00:00">11 Dec 2015, 17:13:41</span></td>
			<td class="green center">135</td>
			<td class="red lasttd center">67</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11746389,0" class="icommentjs kaButton smallButton rightButton" href="/one-piece-722-english-subbed-480p-arrg-lucifer22-t11746389.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/one-piece-722-english-subbed-480p-arrg-lucifer22-t11746389.html" class="cellMainLink">One Piece - 722 English Subbed 480p [ARRG][Lucifer22]</a></div>
			</td>
			<td class="nobr center" data-sort="76840246">73.28 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T02:55:09+00:00">13 Dec 2015, 02:55:09</span></td>
			<td class="green center">90</td>
			<td class="red lasttd center">13</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11750762,0" class="icommentjs kaButton smallButton rightButton" href="/iorchid-one-punch-man-11-480p-engsub-mkv-t11750762.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/iorchid-one-punch-man-11-480p-engsub-mkv-t11750762.html" class="cellMainLink">[iORcHiD] One-Punch Man - 11 [480p][EngSub].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="103301955">98.52 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T20:04:12+00:00">13 Dec 2015, 20:04:12</span></td>
			<td class="green center">80</td>
			<td class="red lasttd center">28</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11757017,0" class="icommentjs kaButton smallButton rightButton" href="/shepardtds-ef-a-tale-of-memories-dual-audio-720p-8-bit-x265-hevc-1-12-complete-t11757017.html#comment">1 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/shepardtds-ef-a-tale-of-memories-dual-audio-720p-8-bit-x265-hevc-1-12-complete-t11757017.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/shepardtds-ef-a-tale-of-memories-dual-audio-720p-8-bit-x265-hevc-1-12-complete-t11757017.html" class="cellMainLink">[ShepardTDS] Ef - A Tale of Memories Dual Audio [720p 8-bit x265 HEVC] 1-12 Complete</a></div>
			</td>
			<td class="nobr center" data-sort="2410950930">2.25 <span>GB</span></td>
			<td class="center">19</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-15T00:49:56+00:00">15 Dec 2015, 00:49:56</span></td>
			<td class="green center">54</td>
			<td class="red lasttd center">72</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11750053,0" class="icommentjs kaButton smallButton rightButton" href="/bakedfish-one-punch-man-11-720p-aac-mp4-t11750053.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/bakedfish-one-punch-man-11-720p-aac-mp4-t11750053.html" class="cellMainLink">[BakedFish] One Punch Man - 11 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="509033725">485.45 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T17:20:18+00:00">13 Dec 2015, 17:20:18</span></td>
			<td class="green center">73</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11751937,0" class="icommentjs kaButton smallButton rightButton" href="/kamifs-dragon-ball-super-023-720p-engsub-mkv-t11751937.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/kamifs-dragon-ball-super-023-720p-engsub-mkv-t11751937.html" class="cellMainLink">[KamiFS] Dragon Ball Super - 023 [720p][EngSub].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="411007945">391.97 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T02:29:04+00:00">14 Dec 2015, 02:29:04</span></td>
			<td class="green center">73</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11757392,0" class="icommentjs kaButton smallButton rightButton" href="/the-last-naruto-the-movie-dub-1080p-bd-x264-dushikushi-t11757392.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-last-naruto-the-movie-dub-1080p-bd-x264-dushikushi-t11757392.html" class="cellMainLink">The Last - Naruto the Movie (Dub) 1080p BD x264 - DushiKushi</a></div>
			</td>
			<td class="nobr center" data-sort="2238663404">2.08 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-15T03:01:04+00:00">15 Dec 2015, 03:01:04</span></td>
			<td class="green center">39</td>
			<td class="red lasttd center">49</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11746333,0" class="icommentjs kaButton smallButton rightButton" href="/assorted-magazines-bundle-december-13-2015-true-pdf-t11746333.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/assorted-magazines-bundle-december-13-2015-true-pdf-t11746333.html" class="cellMainLink">Assorted Magazines Bundle - December 13 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="339800209">324.06 <span>MB</span></td>
			<td class="center">30</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T02:39:40+00:00">13 Dec 2015, 02:39:40</span></td>
			<td class="green center">474</td>
			<td class="red lasttd center">301</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11745024,0" class="icommentjs kaButton smallButton rightButton" href="/the-new-york-times-best-sellers-december-20-2015-fiction-non-fiction-t11745024.html#comment">16 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-new-york-times-best-sellers-december-20-2015-fiction-non-fiction-t11745024.html" class="cellMainLink">The New York Times Best Sellers - December 20, 2015 (Fiction / Non-Fiction)</a></div>
			</td>
			<td class="nobr center" data-sort="65696102">62.65 <span>MB</span></td>
			<td class="center">23</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T19:15:35+00:00">12 Dec 2015, 19:15:35</span></td>
			<td class="green center">418</td>
			<td class="red lasttd center">100</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11748183,0" class="icommentjs kaButton smallButton rightButton" href="/new-york-times-best-sellers-fiction-non-fiction-the-10-best-book-of-2015-t11748183.html#comment">30 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/new-york-times-best-sellers-fiction-non-fiction-the-10-best-book-of-2015-t11748183.html" class="cellMainLink">New York Times Best Sellers Fiction &amp; Non-Fiction - The 10 Best Book of 2015</a></div>
			</td>
			<td class="nobr center" data-sort="89429218">85.29 <span>MB</span></td>
			<td class="center">13</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T09:25:02+00:00">13 Dec 2015, 09:25:02</span></td>
			<td class="green center">385</td>
			<td class="red lasttd center">82</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11749443,0" class="icommentjs kaButton smallButton rightButton" href="/the-complete-survival-shelters-handbook-a-step-by-step-guide-to-building-life-saving-structures-for-every-climate-and-wilderness-situation-epub-t11749443.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-complete-survival-shelters-handbook-a-step-by-step-guide-to-building-life-saving-structures-for-every-climate-and-wilderness-situation-epub-t11749443.html" class="cellMainLink">The Complete Survival Shelters Handbook: A Step-by-Step Guide to Building Life-saving Structures for Every Climate and Wilderness Situation [epub]</a></div>
			</td>
			<td class="nobr center" data-sort="95186518">90.78 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T14:50:57+00:00">13 Dec 2015, 14:50:57</span></td>
			<td class="green center">382</td>
			<td class="red lasttd center">51</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11740628,0" class="icommentjs kaButton smallButton rightButton" href="/great-meat-classic-techniques-and-award-winning-recipes-for-selecting-cutting-and-cooking-beef-lamb-pork-poultry-and-game-2013-epub-gooner-t11740628.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/great-meat-classic-techniques-and-award-winning-recipes-for-selecting-cutting-and-cooking-beef-lamb-pork-poultry-and-game-2013-epub-gooner-t11740628.html" class="cellMainLink">Great Meat - Classic Techniques and Award-Winning Recipes for Selecting, Cutting, and Cooking Beef, Lamb, Pork, Poultry and Game (2013).epub Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="16521051">15.76 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T22:55:43+00:00">11 Dec 2015, 22:55:43</span></td>
			<td class="green center">367</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11752035,0" class="icommentjs kaButton smallButton rightButton" href="/best-mystery-thriller-books-2015-goodreads-t11752035.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/best-mystery-thriller-books-2015-goodreads-t11752035.html" class="cellMainLink">Best Mystery &amp; Thriller Books 2015 - Goodreads</a></div>
			</td>
			<td class="nobr center" data-sort="28276660">26.97 <span>MB</span></td>
			<td class="center">60</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T03:01:15+00:00">14 Dec 2015, 03:01:15</span></td>
			<td class="green center">323</td>
			<td class="red lasttd center">69</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11750774,0" class="icommentjs kaButton smallButton rightButton" href="/best-fiction-books-2015-goodreads-t11750774.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/best-fiction-books-2015-goodreads-t11750774.html" class="cellMainLink">Best Fiction Books 2015 - Goodreads</a></div>
			</td>
			<td class="nobr center" data-sort="25228629">24.06 <span>MB</span></td>
			<td class="center">60</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T20:07:44+00:00">13 Dec 2015, 20:07:44</span></td>
			<td class="green center">324</td>
			<td class="red lasttd center">58</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11751214,0" class="icommentjs kaButton smallButton rightButton" href="/mastering-the-art-of-chinese-cooking-2009-pdf-epub-gooner-t11751214.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/mastering-the-art-of-chinese-cooking-2009-pdf-epub-gooner-t11751214.html" class="cellMainLink">Mastering the Art of Chinese Cooking (2009) (Pdf &amp; Epub) Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="43275283">41.27 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T22:19:45+00:00">13 Dec 2015, 22:19:45</span></td>
			<td class="green center">322</td>
			<td class="red lasttd center">48</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11745641,0" class="icommentjs kaButton smallButton rightButton" href="/beginning-the-linux-command-line-2nd-ed-2015-edition-t11745641.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/beginning-the-linux-command-line-2nd-ed-2015-edition-t11745641.html" class="cellMainLink">Beginning the Linux Command Line 2nd ed. 2015 Edition</a></div>
			</td>
			<td class="nobr center" data-sort="4813079">4.59 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T22:16:40+00:00">12 Dec 2015, 22:16:40</span></td>
			<td class="green center">304</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11746052,0" class="icommentjs kaButton smallButton rightButton" href="/national-geographic-usa-january-2016-t11746052.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/national-geographic-usa-january-2016-t11746052.html" class="cellMainLink">National Geographic USA - January 2016</a></div>
			</td>
			<td class="nobr center" data-sort="33271413">31.73 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T01:08:27+00:00">13 Dec 2015, 01:08:27</span></td>
			<td class="green center">256</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11749493,0" class="icommentjs kaButton smallButton rightButton" href="/excel-2016-bible-the-comprehensive-tutorial-resource-epub-pdf-ertb-t11749493.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/excel-2016-bible-the-comprehensive-tutorial-resource-epub-pdf-ertb-t11749493.html" class="cellMainLink">Excel 2016 Bible - The Comprehensive Tutorial Resource [ePUB + PDF] {{ERTB}}</a></div>
			</td>
			<td class="nobr center" data-sort="116500706">111.1 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T15:04:46+00:00">13 Dec 2015, 15:04:46</span></td>
			<td class="green center">245</td>
			<td class="red lasttd center">42</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11747271,0" class="icommentjs kaButton smallButton rightButton" href="/automobile-magazines-december-13-2015-true-pdf-t11747271.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/automobile-magazines-december-13-2015-true-pdf-t11747271.html" class="cellMainLink">Automobile Magazines - December 13 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="486732857">464.18 <span>MB</span></td>
			<td class="center">27</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T04:48:23+00:00">13 Dec 2015, 04:48:23</span></td>
			<td class="green center">219</td>
			<td class="red lasttd center">89</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11747834,0" class="icommentjs kaButton smallButton rightButton" href="/home-garden-magazines-december-13-2015-true-pdf-t11747834.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/home-garden-magazines-december-13-2015-true-pdf-t11747834.html" class="cellMainLink">Home &amp; Garden Magazines - December 13 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="291699932">278.19 <span>MB</span></td>
			<td class="center">19</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T07:39:20+00:00">13 Dec 2015, 07:39:20</span></td>
			<td class="green center">214</td>
			<td class="red lasttd center">68</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11739938,0" class="icommentjs kaButton smallButton rightButton" href="/office-2016-at-work-for-dummies-epub-pdf-ertb-t11739938.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/office-2016-at-work-for-dummies-epub-pdf-ertb-t11739938.html" class="cellMainLink">Office 2016 at Work For Dummies [ePUB + PDF] {{ERTB}}</a></div>
			</td>
			<td class="nobr center" data-sort="94961274">90.56 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T19:28:28+00:00">11 Dec 2015, 19:28:28</span></td>
			<td class="green center">222</td>
			<td class="red lasttd center">49</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11747504,0" class="icommentjs kaButton smallButton rightButton" href="/gun-fishing-magazines-december-13-2015-true-pdf-t11747504.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/gun-fishing-magazines-december-13-2015-true-pdf-t11747504.html" class="cellMainLink">Gun &amp; Fishing Magazines - December 13 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="250861253">239.24 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T05:49:55+00:00">13 Dec 2015, 05:49:55</span></td>
			<td class="green center">191</td>
			<td class="red lasttd center">64</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11747458,0" class="icommentjs kaButton smallButton rightButton" href="/va-uper-instrumental-music-collection-30cd-2015-flac-t11747458.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-uper-instrumental-music-collection-30cd-2015-flac-t11747458.html" class="cellMainLink">VA - $uper Instrumental Music Collection [30CD] (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="13400726709">12.48 <span>GB</span></td>
			<td class="center">634</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T05:38:21+00:00">13 Dec 2015, 05:38:21</span></td>
			<td class="green center">179</td>
			<td class="red lasttd center">206</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11744564,0" class="icommentjs kaButton smallButton rightButton" href="/fleetwood-mac-the-very-best-of-2009-flac-soup-t11744564.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/fleetwood-mac-the-very-best-of-2009-flac-soup-t11744564.html" class="cellMainLink">Fleetwood Mac - The Very Best Of (2009) FLAC Soup</a></div>
			</td>
			<td class="nobr center" data-sort="1005377773">958.8 <span>MB</span></td>
			<td class="center">54</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T17:31:16+00:00">12 Dec 2015, 17:31:16</span></td>
			<td class="green center">198</td>
			<td class="red lasttd center">57</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11744179,0" class="icommentjs kaButton smallButton rightButton" href="/mahler-symphonies-nos-1-9-lso-valery-gergiev-2012-24-48-hd-flac-t11744179.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/mahler-symphonies-nos-1-9-lso-valery-gergiev-2012-24-48-hd-flac-t11744179.html" class="cellMainLink">Mahler - Symphonies Nos 1-9 - LSO, Valery Gergiev (2012) [24-48 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="6392445433">5.95 <span>GB</span></td>
			<td class="center">94</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T16:15:21+00:00">12 Dec 2015, 16:15:21</span></td>
			<td class="green center">135</td>
			<td class="red lasttd center">131</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11743689,0" class="icommentjs kaButton smallButton rightButton" href="/va-jingle-jazz-the-best-christmas-music-in-a-jazzy-style-2015-flac-t11743689.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-jingle-jazz-the-best-christmas-music-in-a-jazzy-style-2015-flac-t11743689.html" class="cellMainLink">VA - Jingle Jazz! [The Best Christmas Music In a Jazzy Style] (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="592478564">565.03 <span>MB</span></td>
			<td class="center">31</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T14:27:28+00:00">12 Dec 2015, 14:27:28</span></td>
			<td class="green center">161</td>
			<td class="red lasttd center">43</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11755196,0" class="icommentjs kaButton smallButton rightButton" href="/mozart-symphonies-nos-35-41-berliner-philharmoniker-karl-bÃ¶hm-2015-24-96-hd-flac-t11755196.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/mozart-symphonies-nos-35-41-berliner-philharmoniker-karl-bÃ¶hm-2015-24-96-hd-flac-t11755196.html" class="cellMainLink">Mozart - Symphonies Nos. 35-41 - Berliner Philharmoniker, Karl BÃ¶hm (2015) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="3060690521">2.85 <span>GB</span></td>
			<td class="center">42</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T16:38:37+00:00">14 Dec 2015, 16:38:37</span></td>
			<td class="green center">117</td>
			<td class="red lasttd center">126</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11748973,0" class="icommentjs kaButton smallButton rightButton" href="/beethoven-symphonies-nos-7-9-wiener-philharmoniker-thielemann-2015-24-48-hd-flac-t11748973.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/beethoven-symphonies-nos-7-9-wiener-philharmoniker-thielemann-2015-24-48-hd-flac-t11748973.html" class="cellMainLink">Beethoven - Symphonies Nos.7-9 - Wiener Philharmoniker, Thielemann (2015) [24-48 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1409140178">1.31 <span>GB</span></td>
			<td class="center">20</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T12:55:14+00:00">13 Dec 2015, 12:55:14</span></td>
			<td class="green center">143</td>
			<td class="red lasttd center">67</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11740052,0" class="icommentjs kaButton smallButton rightButton" href="/depeche-mode-never-feel-the-silence-remixed-by-isaac-junkie-2015-flac-t11740052.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/depeche-mode-never-feel-the-silence-remixed-by-isaac-junkie-2015-flac-t11740052.html" class="cellMainLink">Depeche Mode - Never Feel The Silence (Remixed by Isaac Junkie) (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="297655314">283.87 <span>MB</span></td>
			<td class="center">14</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T20:09:55+00:00">11 Dec 2015, 20:09:55</span></td>
			<td class="green center">157</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11753991,0" class="icommentjs kaButton smallButton rightButton" href="/kenny-g-faith-a-holiday-album-flac-tntvillage-t11753991.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/kenny-g-faith-a-holiday-album-flac-tntvillage-t11753991.html" class="cellMainLink">Kenny G - Faith A Holiday Album [Flac][TntVillage]</a></div>
			</td>
			<td class="nobr center" data-sort="290697482">277.23 <span>MB</span></td>
			<td class="center">21</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T10:55:14+00:00">14 Dec 2015, 10:55:14</span></td>
			<td class="green center">130</td>
			<td class="red lasttd center">42</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11756398,0" class="icommentjs kaButton smallButton rightButton" href="/prince-hitnrun-phase-one-two-2015-flac-t11756398.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/prince-hitnrun-phase-one-two-2015-flac-t11756398.html" class="cellMainLink">Prince - HITnRUN Phase One &amp; Two (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="664917535">634.11 <span>MB</span></td>
			<td class="center">34</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T22:03:13+00:00">14 Dec 2015, 22:03:13</span></td>
			<td class="green center">120</td>
			<td class="red lasttd center">43</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11754623,0" class="icommentjs kaButton smallButton rightButton" href="/tom-petty-the-heartbreakers-hard-promises-2015-24-96-hd-flac-t11754623.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/tom-petty-the-heartbreakers-hard-promises-2015-24-96-hd-flac-t11754623.html" class="cellMainLink">Tom Petty &amp; The Heartbreakers - Hard Promises (2015) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="966462815">921.69 <span>MB</span></td>
			<td class="center">27</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T13:55:51+00:00">14 Dec 2015, 13:55:51</span></td>
			<td class="green center">107</td>
			<td class="red lasttd center">43</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11750043,0" class="icommentjs kaButton smallButton rightButton" href="/the-guess-who-discography-1965-1976-plus-flac-t11750043.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-guess-who-discography-1965-1976-plus-flac-t11750043.html" class="cellMainLink">The Guess Who - Discography 1965-1976 plus [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="6802909679">6.34 <span>GB</span></td>
			<td class="center">449</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T17:16:53+00:00">13 Dec 2015, 17:16:53</span></td>
			<td class="green center">83</td>
			<td class="red lasttd center">83</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11739528,0" class="icommentjs kaButton smallButton rightButton" href="/oscar-peterson-plays-the-george-gershwin-song-book-2015-24-192-hd-flac-t11739528.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/oscar-peterson-plays-the-george-gershwin-song-book-2015-24-192-hd-flac-t11739528.html" class="cellMainLink">Oscar Peterson - Plays The George Gershwin Song Book (2015) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1583786607">1.48 <span>GB</span></td>
			<td class="center">31</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T17:49:47+00:00">11 Dec 2015, 17:49:47</span></td>
			<td class="green center">100</td>
			<td class="red lasttd center">38</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11748094,0" class="icommentjs kaButton smallButton rightButton" href="/love-forever-changes-2015-24-192-hd-flac-t11748094.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/love-forever-changes-2015-24-192-hd-flac-t11748094.html" class="cellMainLink">Love - Forever Changes (2015) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1876458789">1.75 <span>GB</span></td>
			<td class="center">28</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-13T08:57:34+00:00">13 Dec 2015, 08:57:34</span></td>
			<td class="green center">76</td>
			<td class="red lasttd center">45</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/mylÃ¨ne-farmer-greatest-hits-2cd-2013-flac-t11758956.html" class="cellMainLink">MylÃ¨ne Farmer - Greatest Hits [2CD] (2013) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1175704572">1.09 <span>GB</span></td>
			<td class="center">24</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-15T10:05:54+00:00">15 Dec 2015, 10:05:54</span></td>
			<td class="green center">75</td>
			<td class="red lasttd center">34</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11754900,0" class="icommentjs kaButton smallButton rightButton" href="/black-oak-arkansas-discography-flac-t11754900.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/black-oak-arkansas-discography-flac-t11754900.html" class="cellMainLink">Black Oak Arkansas - Discography [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="4713314302">4.39 <span>GB</span></td>
			<td class="center">314</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-14T15:04:34+00:00">14 Dec 2015, 15:04:34</span></td>
			<td class="green center">63</td>
			<td class="red lasttd center">37</td>
        </tr>
			</table>


	<script type="text/javascript">
		if ($.cookie('kat_settings[fpw_column]') != null && $.cookie('kat_settings[fpw_order]')) {
			var colIndex = $.cookie('kat_settings[fpw_column]')
			var ascend = $.cookie('kat_settings[fpw_order]');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			sortTables(colIndex, ascend);
		}
		$('.frontPageWidget .firstr th .fpw-sort').click(function() {
			var colIndex = $(this).parent().index();
			var ascend = !$(this).hasClass('sortedAsc')?1:0;
			$('.fpw-sort').removeClass('sortedAsc sortedDesc');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			updateFPWsettings(colIndex, ascend);
			sortTables(colIndex, ascend);
		});
		function sortTables(colIndex, ascend) {	
			$('.frontPageWidget').each(function() {
				var data = [];
				$('tr:not(.firstr)', $(this)).each(function() {
					var name = $('td:first .cellMainLink', $(this)).text();
					var id = $('td:first .cellMainLink', $(this)).attr('href').match(/.*-t(\d+)\.html/)[1];
					var fileSize = $('td:eq(1)', $(this)).attr('data-sort');
					data.push({
						'col0':name, 'col1':padNum(fileSize), 'col2':padNum($('td:eq(2)',$(this)).text()), 'col3':padNum(id), 'col4':padNum($('td:eq(4)',$(this)).text()), 'col5':padNum($('td:eq(5)',$(this)).text()), 'html':$(this).html()
					});
				});
				data.sort(function(a, b) {
					var x = a['col'+colIndex].toLowerCase(); var y = b['col'+colIndex].toLowerCase();
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
				if (!ascend||ascend=='0') data.reverse();
				$(this).find('tr:not(.firstr)').remove();
				for (var i=0;i<data.length;i++) {
					$(this).append('<tr class="'+((i+1)%2==0?'even':'odd')+'">'+data[i].html+'</tr>');
				}
			});
		}
		function padNum(val) {
			val = '0000000000000000'+val;
			return val.substring(val.length-15);
		}
		function updateFPWsettings(t, o) {
			$.cookie('kat_settings[fpw_column]', t, { expires: 365, path: '/' });
			$.cookie('kat_settings[fpw_order]', o, { expires: 365, path: '/' });
		}
	</script>
		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
    
            
        <div  data-sc-slot="_119b0a17fab5493361a252d04bf527db"></div>
    
                
    	    <div class="spareBlock">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <div  data-sc-slot="_7063408f1c01d50e0dc2d833186ce962" data-sc-params="{ 'searchQuery': '' }"></div>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/adopt-uploader-program-v12-all-users-help-thread-115043/?unread=17215712">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				**Adopt an uploader Program v12-- For all Users to Help**
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/QUiNTeSSeNT_L/">QUiNTeSSeNT_L</a></span></span> <time class="timeago" datetime="2015-12-15T17:06:07+00:00">15 Dec 2015, 17:06</time></span>
	</li>
		<li>
		<a href="/community/show/uk-top-40/?unread=17215710">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				uk top 40???
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/danallen2015/">danallen2015</a></span></span> <time class="timeago" datetime="2015-12-15T17:06:05+00:00">15 Dec 2015, 17:06</time></span>
	</li>
		<li>
		<a href="/community/show/latest-book-uploads-v3/?unread=17215709">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Latest Book Uploads V3
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_verified"><a class="plain" href="/user/pradyutvam2/">pradyutvam2</a></span></span> <time class="timeago" datetime="2015-12-15T17:05:57+00:00">15 Dec 2015, 17:05</time></span>
	</li>
		<li>
		<a href="/community/show/club-protocol-uploads/?unread=17215706">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				&quot;Club Protocol&quot; Uploads
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_verified"><a class="plain" href="/user/pradyutvam2/">pradyutvam2</a></span></span> <time class="timeago" datetime="2015-12-15T17:05:41+00:00">15 Dec 2015, 17:05</time></span>
	</li>
		<li>
		<a href="/community/show/rap-hip-hop-related-request-thread/?unread=17215701">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				RAP &amp; Hip-Hop Related Request Thread
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/P00R-B0Y/">P00R-B0Y</a></span></span> <time class="timeago" datetime="2015-12-15T17:04:02+00:00">15 Dec 2015, 17:04</time></span>
	</li>
		<li>
		<a href="/community/show/please-request-ebooks-and-audio-books-here-v12/?unread=17215699">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Please request ebooks and audio books here. V12
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/silentnewb/">silentnewb</a></span></span> <time class="timeago" datetime="2015-12-15T17:03:47+00:00">15 Dec 2015, 17:03</time></span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/new-site-rules/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				New site Rules
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-15T14:18:43+00:00">15 Oct 2015, 14:18</time></span>
	</li>
	<li>
		<a href="/blog/post/look-mama-i-m-popular/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Look, mama, I&#039;m popular!
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-05T17:42:40+00:00">05 Oct 2015, 17:42</time></span>
	</li>
	<li>
		<a href="/blog/post/summer-updates-september-1/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Summer updates [September, 1]
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-09-01T16:13:36+00:00">01 Sep 2015, 16:13</time></span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/Fartaclaus/post/immortality/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Immortality</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/Fartaclaus/">Fartaclaus</a> <time class="timeago" datetime="2015-12-15T14:52:40+00:00">15 Dec 2015, 14:52</time></span></li>
	<li><a href="/blog/TheDels/post/death-is-just-a-transit/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Death is just a transit</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/TheDels/">TheDels</a> <time class="timeago" datetime="2015-12-15T10:23:17+00:00">15 Dec 2015, 10:23</time></span></li>
	<li><a href="/blog/Bubanee/post/avvy-for-respect-to-smittech/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Avvy for respect to Smittech</p></a><span class="explanation">by <a class="plain aclColor_3" href="/user/Bubanee/">Bubanee</a> <time class="timeago" datetime="2015-12-15T08:06:53+00:00">15 Dec 2015, 08:06</time></span></li>
	<li><a href="/blog/Snow.Vixen/post/kat-giraffes-beer-nascar/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Kat, Giraffes, Beer &amp; Nascar</p></a><span class="explanation">by <a class="plain aclColor_5" href="/user/Snow.Vixen/">Snow.Vixen</a> <time class="timeago" datetime="2015-12-15T03:04:25+00:00">15 Dec 2015, 03:04</time></span></li>
	<li><a href="/blog/olderthangod/post/the-office-party-pt-1/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> The Office Party Pt 1</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/olderthangod/">olderthangod</a> <time class="timeago" datetime="2015-12-14T23:41:44+00:00">14 Dec 2015, 23:41</time></span></li>
	<li><a href="/blog/Hellfya/post/mavka-wishlist-3d-content-updated-12-13/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Mavka Wishlist (3D Content) *Updated 12/13</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/Hellfya/">Hellfya</a> <time class="timeago" datetime="2015-12-14T01:51:36+00:00">14 Dec 2015, 01:51</time></span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/fargo%20s02%20subtit/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				fargo s02 subtit
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/la%20bella%20durmiente/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				la bella durmiente
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/in%20the%20heart%20on%20the%20sea/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				in the heart on the sea
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/injustice%20nem/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				injustice nem
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/emma%20brown/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Emma Brown
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/sex%20model/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Sex model
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/joan%20baez%20flac/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				joan baez flac
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/zhu/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				zhu
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/devil%20playground/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				devil playground
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/quantico%20s01e02/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				quantico s01e02
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/mary%20land/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				mary land
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a href="#" onclick="setLanguage('en', '.katproxy.is');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a href="#" onclick="setLanguage('af', '.katproxy.is');return false;" class="plain">Afrikaans</a></li>
                                <li><a href="#" onclick="setLanguage('al', '.katproxy.is');return false;" class="plain">Albanian</a></li>
                                <li><a href="#" onclick="setLanguage('ar', '.katproxy.is');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a href="#" onclick="setLanguage('eu', '.katproxy.is');return false;" class="plain">Basque</a></li>
                                <li><a href="#" onclick="setLanguage('bn', '.katproxy.is');return false;" class="plain">Bengali</a></li>
                                <li><a href="#" onclick="setLanguage('bs', '.katproxy.is');return false;" class="plain">Bosnian</a></li>
                                <li><a href="#" onclick="setLanguage('br', '.katproxy.is');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a href="#" onclick="setLanguage('bg', '.katproxy.is');return false;" class="plain">Bulgarian</a></li>
                                <li><a href="#" onclick="setLanguage('ch', '.katproxy.is');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a href="#" onclick="setLanguage('tw', '.katproxy.is');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a href="#" onclick="setLanguage('hr', '.katproxy.is');return false;" class="plain">Croatian</a></li>
                                <li><a href="#" onclick="setLanguage('cz', '.katproxy.is');return false;" class="plain">Czech</a></li>
                                <li><a href="#" onclick="setLanguage('da', '.katproxy.is');return false;" class="plain">Danish</a></li>
                                <li><a href="#" onclick="setLanguage('nl', '.katproxy.is');return false;" class="plain">Dutch</a></li>
                                <li><a href="#" onclick="setLanguage('tl', '.katproxy.is');return false;" class="plain">Filipino</a></li>
                                <li><a href="#" onclick="setLanguage('fi', '.katproxy.is');return false;" class="plain">Finnish</a></li>
                                <li><a href="#" onclick="setLanguage('fr', '.katproxy.is');return false;" class="plain">French</a></li>
                                <li><a href="#" onclick="setLanguage('ka', '.katproxy.is');return false;" class="plain">Georgian</a></li>
                                <li><a href="#" onclick="setLanguage('de', '.katproxy.is');return false;" class="plain">German</a></li>
                                <li><a href="#" onclick="setLanguage('el', '.katproxy.is');return false;" class="plain">Greek</a></li>
                                <li><a href="#" onclick="setLanguage('he', '.katproxy.is');return false;" class="plain">Hebrew</a></li>
                                <li><a href="#" onclick="setLanguage('hi', '.katproxy.is');return false;" class="plain">Hindi</a></li>
                                <li><a href="#" onclick="setLanguage('hu', '.katproxy.is');return false;" class="plain">Hungarian</a></li>
                                <li><a href="#" onclick="setLanguage('id', '.katproxy.is');return false;" class="plain">Indonesian</a></li>
                                <li><a href="#" onclick="setLanguage('it', '.katproxy.is');return false;" class="plain">Italian</a></li>
                                <li><a href="#" onclick="setLanguage('kn', '.katproxy.is');return false;" class="plain">Kannada</a></li>
                                <li><a href="#" onclick="setLanguage('ko', '.katproxy.is');return false;" class="plain">Korean</a></li>
                                <li><a href="#" onclick="setLanguage('lv', '.katproxy.is');return false;" class="plain">Latvian</a></li>
                                <li><a href="#" onclick="setLanguage('lt', '.katproxy.is');return false;" class="plain">Lithuanian</a></li>
                                <li><a href="#" onclick="setLanguage('mk', '.katproxy.is');return false;" class="plain">Macedonian</a></li>
                                <li><a href="#" onclick="setLanguage('ml', '.katproxy.is');return false;" class="plain">Malayalam</a></li>
                                <li><a href="#" onclick="setLanguage('ms', '.katproxy.is');return false;" class="plain">Malaysian</a></li>
                                <li><a href="#" onclick="setLanguage('no', '.katproxy.is');return false;" class="plain">Norwegian</a></li>
                                <li><a href="#" onclick="setLanguage('pr', '.katproxy.is');return false;" class="plain">Pirate</a></li>
                                <li><a href="#" onclick="setLanguage('pl', '.katproxy.is');return false;" class="plain">Polish</a></li>
                                <li><a href="#" onclick="setLanguage('pt', '.katproxy.is');return false;" class="plain">Portuguese</a></li>
                                <li><a href="#" onclick="setLanguage('pa', '.katproxy.is');return false;" class="plain">Punjabi</a></li>
                                <li><a href="#" onclick="setLanguage('ro', '.katproxy.is');return false;" class="plain">Romanian</a></li>
                                <li><a href="#" onclick="setLanguage('ru', '.katproxy.is');return false;" class="plain">Russian</a></li>
                                <li><a href="#" onclick="setLanguage('sr', '.katproxy.is');return false;" class="plain">Serbian</a></li>
                                <li><a href="#" onclick="setLanguage('src', '.katproxy.is');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a href="#" onclick="setLanguage('bsc', '.katproxy.is');return false;" class="plain">Serbian-Cyrillic (ijekavica)</a></li>
                                <li><a href="#" onclick="setLanguage('si', '.katproxy.is');return false;" class="plain">Sinhala</a></li>
                                <li><a href="#" onclick="setLanguage('sk', '.katproxy.is');return false;" class="plain">Slovak</a></li>
                                <li><a href="#" onclick="setLanguage('sl', '.katproxy.is');return false;" class="plain">Slovenian</a></li>
                                <li><a href="#" onclick="setLanguage('es', '.katproxy.is');return false;" class="plain">Spanish</a></li>
                                <li><a href="#" onclick="setLanguage('sv', '.katproxy.is');return false;" class="plain">Swedish</a></li>
                                <li><a href="#" onclick="setLanguage('ta', '.katproxy.is');return false;" class="plain">Tamil</a></li>
                                <li><a href="#" onclick="setLanguage('te', '.katproxy.is');return false;" class="plain">Telugu</a></li>
                                <li><a href="#" onclick="setLanguage('tr', '.katproxy.is');return false;" class="plain">Turkish</a></li>
                                <li><a href="#" onclick="setLanguage('uk', '.katproxy.is');return false;" class="plain">Ukrainian</a></li>
                                <li><a href="#" onclick="setLanguage('ur', '.katproxy.is');return false;" class="plain">Urdu</a></li>
                                <li><a href="#" onclick="setLanguage('vi', '.katproxy.is');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" data-nop href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
        		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
        <li><a href="https://kastatus.com">KAT status</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <div  data-sc-slot="_673e31f53f8166159b8e996c4124765b"></div>
        <div  data-sc-slot="_e7050fb15fd39b3e4e99a5be4a57b6ea"></div>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script>
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "Organization",
"name": "KickassTorrents",
"url": "https://kat.cr",
"logo": "https://kat.cr/content/images/kickasslogo.png",
"sameAs": [
    "https://www.facebook.com/Official.KAT.Fanclub",
    "https://twitter.com/kickasstorrents"
]
}
</script>
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "WebSite",
"url": "https://kat.cr",
"potentialAction": {
    "@type": "SearchAction",
    "target": "https://kat.cr/usearch/{q}/",
    "query-input": {
        "@type": "PropertyValueSpecification",
        "valueRequired": true,
        "valueName": "q"
    }
}
}
</script>
<script>
 sc('addGlobal', 'pagetype', 'front');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>

</body>
</html>
