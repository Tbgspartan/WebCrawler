<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="auto">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-365d493.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-365d493.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-365d493.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-365d493.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kickass.to/sc-365d493.js');

        sc('setHost', 'a.kickass.to');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: '365d493',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-365d493.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<div  data-sc-slot="_60318cd4e8d28f6fb76fe34e9bd9c498"></div>
<div  data-sc-slot="_39ecb76dd457e5ac33776fdf11500d56"></div>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<a href="#" id="showHideSearch"><i class="ka ka-zoom"></i></a>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka ka-settings"></i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka ka-search"></i></button></div>
			</form>
		</div>
        <div  data-sc-slot="_277923e5f9d753c5b0630c28e641790c"></div>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080p/" class="tag7">1080p</a>
	<a href="/search/2015/" class="tag10">2015</a>
	<a href="/search/3d/" class="tag4">3d</a>
	<a href="/search/720p/" class="tag3">720p</a>
	<a href="/search/android/" class="tag2">android</a>
	<a href="/search/apk/" class="tag2">apk</a>
	<a href="/search/bajirao%20mastani/" class="tag2">bajirao mastani</a>
	<a href="/search/boruto%20naruto%20the%20movie/" class="tag2">boruto naruto the movie</a>
	<a href="/search/bridge%20of%20spies/" class="tag2">bridge of spies</a>
	<a href="/search/creed/" class="tag1">creed</a>
	<a href="/search/daddy%20s%20home/" class="tag2">daddy s home</a>
	<a href="/search/dilwale/" class="tag3">dilwale</a>
	<a href="/search/discography/" class="tag3">discography</a>
	<a href="/search/doctor%20who/" class="tag1">doctor who</a>
	<a href="/search/dual%20audio%20hindi/" class="tag4">dual audio hindi</a>
	<a href="/search/dvdscr/" class="tag5">dvdscr</a>
	<a href="/search/etrg/" class="tag2">etrg</a>
	<a href="/search/french/" class="tag3">french</a>
	<a href="/search/goosebumps/" class="tag2">goosebumps</a>
	<a href="/search/hateful%20eight/" class="tag1">hateful eight</a>
	<a href="/search/hindi/" class="tag9">hindi</a>
	<a href="/search/hindi%202015/" class="tag6">hindi 2015</a>
	<a href="/search/hunger%20games/" class="tag2">hunger games</a>
	<a href="/search/is%20safe%201/" class="tag2">is safe 1</a>
	<a href="/search/ita/" class="tag3">ita</a>
	<a href="/search/legend/" class="tag2">legend</a>
	<a href="/search/malayalam/" class="tag3">malayalam</a>
	<a href="/search/motorhead/" class="tag2">motorhead</a>
	<a href="/search/nezu/" class="tag10">nezu</a>
	<a href="/search/nl/" class="tag2">nl</a>
	<a href="/search/one%20punch%20man/" class="tag2">one punch man</a>
	<a href="/search/sicario/" class="tag2">sicario</a>
	<a href="/search/spectre/" class="tag4">spectre</a>
	<a href="/search/star%20wars/" class="tag6">star wars</a>
	<a href="/search/star%20wars%20the%20force%20awakens/" class="tag3">star wars the force awakens</a>
	<a href="/search/straight%20outta%20compton/" class="tag2">straight outta compton</a>
	<a href="/search/tamil/" class="tag5">tamil</a>
	<a href="/search/telugu/" class="tag4">telugu</a>
	<a href="/search/telugu%202015/" class="tag3">telugu 2015</a>
	<a href="/search/the%20big%20short/" class="tag2">the big short</a>
	<a href="/search/the%20hateful%20eight/" class="tag2">the hateful eight</a>
	<a href="/search/the%20martian/" class="tag3">the martian</a>
	<a href="/search/the%20revenant/" class="tag2">the revenant</a>
	<a href="/search/the%20walking%20dead/" class="tag3">the walking dead</a>
	<a href="/search/wwe/" class="tag2">wwe</a>
	<a href="/search/yify/" class="tag10">yify</a>
	<a href="/search/yify%201080p/" class="tag4">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag4">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11832126,0" class="icommentjs kaButton smallButton rightButton" href="/the-big-short-2015-dvdscr-xvid-ac3-hq-hive-cm8-t11832126.html#comment">94 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-big-short-2015-dvdscr-xvid-ac3-hq-hive-cm8-t11832126.html" class="cellMainLink">The Big Short 2015 DVDScr XVID AC3 HQ Hive-CM8</a></div>
			</td>
			<td class="nobr center" data-sort="1636458960">1.52 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T10:21:06+00:00">30 Dec 2015, 10:21:06</span></td>
			<td class="green center">15714</td>
			<td class="red lasttd center">15563</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11832189,0" class="icommentjs kaButton smallButton rightButton" href="/burnt-2015-brrip-xvid-etrg-t11832189.html#comment">43 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/burnt-2015-brrip-xvid-etrg-t11832189.html" class="cellMainLink">Burnt 2015 BRRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="741843178">707.48 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T10:45:39+00:00">30 Dec 2015, 10:45:39</span></td>
			<td class="green center">7965</td>
			<td class="red lasttd center">7044</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11829852,0" class="icommentjs kaButton smallButton rightButton" href="/bridge-of-spies-2015-repack-truefrench-dvdscr-md-xvid-zt-avi-t11829852.html#comment">17 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/bridge-of-spies-2015-repack-truefrench-dvdscr-md-xvid-zt-avi-t11829852.html" class="cellMainLink">Bridge of Spies 2015 REPACK TRUEFRENCH DVDSCR MD XViD-ZT avi</a></div>
			</td>
			<td class="nobr center" data-sort="1471070208">1.37 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-29T20:20:27+00:00">29 Dec 2015, 20:20:27</span></td>
			<td class="green center">7544</td>
			<td class="red lasttd center">3830</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11830464,0" class="icommentjs kaButton smallButton rightButton" href="/the-martian-2015-1080p-bluray-h264-aac-rarbg-t11830464.html#comment">39 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-martian-2015-1080p-bluray-h264-aac-rarbg-t11830464.html" class="cellMainLink">The Martian 2015 1080p BluRay H264 AAC-RARBG</a></div>
			</td>
			<td class="nobr center" data-sort="2939058844">2.74 <span>GB</span></td>
			<td class="center">10</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T00:03:06+00:00">30 Dec 2015, 00:03:06</span></td>
			<td class="green center">3826</td>
			<td class="red lasttd center">5938</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11829854,0" class="icommentjs kaButton smallButton rightButton" href="/algerie-pour-toujours-les-portes-du-soleil-2014-french-dvdrip-xvid-sensys-avi-t11829854.html#comment">48 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/algerie-pour-toujours-les-portes-du-soleil-2014-french-dvdrip-xvid-sensys-avi-t11829854.html" class="cellMainLink">Algerie Pour Toujours Les Portes Du Soleil 2014 FRENCH DVDRiP XViD-sensys avi</a></div>
			</td>
			<td class="nobr center" data-sort="1361503668">1.27 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-29T20:21:07+00:00">29 Dec 2015, 20:21:07</span></td>
			<td class="green center">5227</td>
			<td class="red lasttd center">2428</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11823795,0" class="icommentjs kaButton smallButton rightButton" href="/straight-outta-compton-2015-hdrip-xvid-ac3-etrg-t11823795.html#comment">123 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/straight-outta-compton-2015-hdrip-xvid-ac3-etrg-t11823795.html" class="cellMainLink">Straight Outta Compton.2015.HDRip.XViD.AC3-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="1517728054">1.41 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T15:58:27+00:00">28 Dec 2015, 15:58:27</span></td>
			<td class="green center">3839</td>
			<td class="red lasttd center">2901</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11832102,0" class="icommentjs kaButton smallButton rightButton" href="/anomalisa-2015-dvdscr-xvid-ac3-hq-hive-cm8-t11832102.html#comment">35 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/anomalisa-2015-dvdscr-xvid-ac3-hq-hive-cm8-t11832102.html" class="cellMainLink">Anomalisa 2015 DVDScr XVID AC3 HQ Hive-CM8</a></div>
			</td>
			<td class="nobr center" data-sort="1291049231">1.2 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T10:13:17+00:00">30 Dec 2015, 10:13:17</span></td>
			<td class="green center">3446</td>
			<td class="red lasttd center">3432</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11832149,0" class="icommentjs kaButton smallButton rightButton" href="/burnt-2015-720p-brrip-x264-aac-etrg-t11832149.html#comment">23 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/burnt-2015-720p-brrip-x264-aac-etrg-t11832149.html" class="cellMainLink">Burnt 2015 720p BRRip x264 AAC-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="794249379">757.46 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T10:31:03+00:00">30 Dec 2015, 10:31:03</span></td>
			<td class="green center">3780</td>
			<td class="red lasttd center">2151</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11830105,0" class="icommentjs kaButton smallButton rightButton" href="/rockstar-2015-malayalam-dvdrip-x264-700mb-mkv-t11830105.html#comment">24 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/rockstar-2015-malayalam-dvdrip-x264-700mb-mkv-t11830105.html" class="cellMainLink">Rockstar - 2015 - Malayalam DVDRip x264 700MB.mkv</a></div>
			</td>
			<td class="nobr center" data-sort="718707235">685.41 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-29T22:02:25+00:00">29 Dec 2015, 22:02:25</span></td>
			<td class="green center">3253</td>
			<td class="red lasttd center">1016</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11828345,0" class="icommentjs kaButton smallButton rightButton" href="/riot-2015-bdrip-xvid-ac3-evo-t11828345.html#comment">30 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/riot-2015-bdrip-xvid-ac3-evo-t11828345.html" class="cellMainLink">Riot 2015 BDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1507413597">1.4 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-29T15:25:52+00:00">29 Dec 2015, 15:25:52</span></td>
			<td class="green center">2725</td>
			<td class="red lasttd center">2042</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/a-walk-in-the-woods-2015-french-bdrip-xvid-vivi-avi-t11834572.html" class="cellMainLink">A Walk in the Woods 2015 FRENCH BDRip XviD-ViVi avi</a></div>
			</td>
			<td class="nobr center" data-sort="1493398706">1.39 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-31T00:13:30+00:00">31 Dec 2015, 00:13:30</span></td>
			<td class="green center">2755</td>
			<td class="red lasttd center">1709</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11833247,0" class="icommentjs kaButton smallButton rightButton" href="/white-rabbit-2015-hdrip-xvid-ac3-evo-t11833247.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/white-rabbit-2015-hdrip-xvid-ac3-evo-t11833247.html" class="cellMainLink">White Rabbit 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1474098577">1.37 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T16:27:59+00:00">30 Dec 2015, 16:27:59</span></td>
			<td class="green center">1698</td>
			<td class="red lasttd center">2684</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11828941,0" class="icommentjs kaButton smallButton rightButton" href="/the-peanuts-movie-2015-french-dvdscr-md-xvid-zt-avi-t11828941.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-peanuts-movie-2015-french-dvdscr-md-xvid-zt-avi-t11828941.html" class="cellMainLink">The Peanuts Movie 2015 FRENCH DVDScr MD XViD-ZT avi</a></div>
			</td>
			<td class="nobr center" data-sort="734205450">700.19 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-29T18:22:03+00:00">29 Dec 2015, 18:22:03</span></td>
			<td class="green center">2737</td>
			<td class="red lasttd center">596</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11829249,0" class="icommentjs kaButton smallButton rightButton" href="/dumb-criminals-the-movie-2015-hdrip-xvid-ac3-evo-t11829249.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dumb-criminals-the-movie-2015-hdrip-xvid-ac3-evo-t11829249.html" class="cellMainLink">Dumb Criminals The Movie 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1488047126">1.39 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-29T19:16:49+00:00">29 Dec 2015, 19:16:49</span></td>
			<td class="green center">1873</td>
			<td class="red lasttd center">2172</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11826418,0" class="icommentjs kaButton smallButton rightButton" href="/surprise-2015-ts720p-x264-aac-mandarin-chs-eng-mp4ba-t11826418.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/surprise-2015-ts720p-x264-aac-mandarin-chs-eng-mp4ba-t11826418.html" class="cellMainLink">Surprise 2015 TS720P X264 AAC Mandarin CHS-ENG Mp4Ba</a></div>
			</td>
			<td class="nobr center" data-sort="1794637946">1.67 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-29T05:44:12+00:00">29 Dec 2015, 05:44:12</span></td>
			<td class="green center">392</td>
			<td class="red lasttd center">3109</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11822092,0" class="icommentjs kaButton smallButton rightButton" href="/second-chance-s01e01-hdtv-x264-batv-ettv-t11822092.html#comment">31 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/second-chance-s01e01-hdtv-x264-batv-ettv-t11822092.html" class="cellMainLink">Second Chance S01E01 HDTV x264-BATV[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="370139300">352.99 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T06:49:20+00:00">28 Dec 2015, 06:49:20</span></td>
			<td class="green center">2630</td>
			<td class="red lasttd center">426</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11821267,0" class="icommentjs kaButton smallButton rightButton" href="/agent-x-us-s01e09-hdtv-x264-lol-ettv-t11821267.html#comment">47 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/agent-x-us-s01e09-hdtv-x264-lol-ettv-t11821267.html" class="cellMainLink">Agent X US S01E09 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="224756447">214.34 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T03:01:23+00:00">28 Dec 2015, 03:01:23</span></td>
			<td class="green center">2501</td>
			<td class="red lasttd center">241</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11820570,0" class="icommentjs kaButton smallButton rightButton" href="/and-then-there-were-none-s01e02-hdtv-x264-river-ettv-t11820570.html#comment">44 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/and-then-there-were-none-s01e02-hdtv-x264-river-ettv-t11820570.html" class="cellMainLink">And Then There Were None S01E02 HDTV x264-RiVER[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="295064289">281.4 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-27T22:26:29+00:00">27 Dec 2015, 22:26:29</span></td>
			<td class="green center">2128</td>
			<td class="red lasttd center">359</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11821107,0" class="icommentjs kaButton smallButton rightButton" href="/the-librarians-us-s02e10-hdtv-x264-killers-ettv-t11821107.html#comment">46 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-librarians-us-s02e10-hdtv-x264-killers-ettv-t11821107.html" class="cellMainLink">The Librarians US S02E10 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="383035141">365.29 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T02:04:32+00:00">28 Dec 2015, 02:04:32</span></td>
			<td class="green center">1546</td>
			<td class="red lasttd center">134</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11825846,0" class="icommentjs kaButton smallButton rightButton" href="/legends-2014-s02e08-hdtv-x264-killers-ettv-t11825846.html#comment">26 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/legends-2014-s02e08-hdtv-x264-killers-ettv-t11825846.html" class="cellMainLink">Legends 2014 S02E08 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="237862734">226.84 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-29T03:03:14+00:00">29 Dec 2015, 03:03:14</span></td>
			<td class="green center">1147</td>
			<td class="red lasttd center">127</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11831207,0" class="icommentjs kaButton smallButton rightButton" href="/the-real-housewives-of-beverly-hills-s06e05-will-power-hdtv-x264-weby-mp4-t11831207.html#comment">23 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-real-housewives-of-beverly-hills-s06e05-will-power-hdtv-x264-weby-mp4-t11831207.html" class="cellMainLink">The Real Housewives of Beverly Hills s06e05 Will Power HDTV x264-Weby mp4</a></div>
			</td>
			<td class="nobr center" data-sort="678478278">647.05 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T04:54:49+00:00">30 Dec 2015, 04:54:49</span></td>
			<td class="green center">1140</td>
			<td class="red lasttd center">103</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11828064,0" class="icommentjs kaButton smallButton rightButton" href="/beowulf-return-to-the-shieldlands-s01e01-preair-vostr-webrip-xvid-zt-avi-t11828064.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/beowulf-return-to-the-shieldlands-s01e01-preair-vostr-webrip-xvid-zt-avi-t11828064.html" class="cellMainLink">Beowulf Return To The Shieldlands S01E01 PREAIR VOSTR WEBRip XviD-ZT avi</a></div>
			</td>
			<td class="nobr center" data-sort="309432208">295.1 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-29T14:05:50+00:00">29 Dec 2015, 14:05:50</span></td>
			<td class="green center">914</td>
			<td class="red lasttd center">100</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11830283,0" class="icommentjs kaButton smallButton rightButton" href="/tripped-s01e04-hdtv-x264-tla-ettv-t11830283.html#comment">21 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/tripped-s01e04-hdtv-x264-tla-ettv-t11830283.html" class="cellMainLink">Tripped S01E04 HDTV x264-TLA[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="359754050">343.09 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-29T23:05:34+00:00">29 Dec 2015, 23:05:34</span></td>
			<td class="green center">869</td>
			<td class="red lasttd center">185</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11820215,0" class="icommentjs kaButton smallButton rightButton" href="/jekyll-and-hyde-s01e09-the-incubus-hdtv-x264-organic-ettv-t11820215.html#comment">31 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/jekyll-and-hyde-s01e09-the-incubus-hdtv-x264-organic-ettv-t11820215.html" class="cellMainLink">Jekyll and Hyde S01E09 The Incubus HDTV x264-ORGANiC[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="314626896">300.05 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-27T20:01:35+00:00">27 Dec 2015, 20:01:35</span></td>
			<td class="green center">822</td>
			<td class="red lasttd center">128</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11831719,0" class="icommentjs kaButton smallButton rightButton" href="/the-curse-of-oak-island-s03e08-phantom-of-the-deep-hdtv-x264-spasm-t11831719.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-curse-of-oak-island-s03e08-phantom-of-the-deep-hdtv-x264-spasm-t11831719.html" class="cellMainLink">The Curse of Oak Island S03E08 Phantom of the Deep HDTV x264-SPASM</a></div>
			</td>
			<td class="nobr center" data-sort="380997295">363.35 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T07:50:46+00:00">30 Dec 2015, 07:50:46</span></td>
			<td class="green center">712</td>
			<td class="red lasttd center">95</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11821516,0" class="icommentjs kaButton smallButton rightButton" href="/ash-vs-evil-dead-s01e09-hdtv-x264-killers-rartv-t11821516.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ash-vs-evil-dead-s01e09-hdtv-x264-killers-rartv-t11821516.html" class="cellMainLink">Ash vs Evil Dead S01E09 HDTV x264-KILLERS[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="225919850">215.45 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T04:23:46+00:00">28 Dec 2015, 04:23:46</span></td>
			<td class="green center">508</td>
			<td class="red lasttd center">71</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11831393,0" class="icommentjs kaButton smallButton rightButton" href="/girlfriends-guide-to-divorce-s02e05-hdtv-x264-killers-ettv-t11831393.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/girlfriends-guide-to-divorce-s02e05-hdtv-x264-killers-ettv-t11831393.html" class="cellMainLink">Girlfriends Guide to Divorce S02E05 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="341933656">326.09 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T05:58:29+00:00">30 Dec 2015, 05:58:29</span></td>
			<td class="green center">427</td>
			<td class="red lasttd center">74</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11822042,0" class="icommentjs kaButton smallButton rightButton" href="/vanderpump-rules-s04e09-what-happened-in-vegas-mer-mp4-t11822042.html#comment">10 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/vanderpump-rules-s04e09-what-happened-in-vegas-mer-mp4-t11822042.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/vanderpump-rules-s04e09-what-happened-in-vegas-mer-mp4-t11822042.html" class="cellMainLink">Vanderpump Rules S04E09 What Happened In Vegas - MER.mp4</a></div>
			</td>
			<td class="nobr center" data-sort="205154692">195.65 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T06:30:08+00:00">28 Dec 2015, 06:30:08</span></td>
			<td class="green center">430</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11834575,0" class="icommentjs kaButton smallButton rightButton" href="/mozart-in-the-jungle-s02e03-720p-webrip-x264-failed-rartv-t11834575.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/mozart-in-the-jungle-s02e03-720p-webrip-x264-failed-rartv-t11834575.html" class="cellMainLink">Mozart In The Jungle S02E03 720p WEBRip x264-FaiLED[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="556935138">531.13 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-31T00:15:12+00:00">31 Dec 2015, 00:15:12</span></td>
			<td class="green center">373</td>
			<td class="red lasttd center">108</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11825659,0" class="icommentjs kaButton smallButton rightButton" href="/love-and-hip-hop-s06e03-whats-poppin-webrip-megatv-t11825659.html#comment">15 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/love-and-hip-hop-s06e03-whats-poppin-webrip-megatv-t11825659.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/love-and-hip-hop-s06e03-whats-poppin-webrip-megatv-t11825659.html" class="cellMainLink">Love And Hip Hop S06E03 Whats Poppin WEBRIP-MegaTV</a></div>
			</td>
			<td class="nobr center" data-sort="416916224">397.6 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-29T02:01:36+00:00">29 Dec 2015, 02:01:36</span></td>
			<td class="green center">401</td>
			<td class="red lasttd center">37</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11831770,0" class="icommentjs kaButton smallButton rightButton" href="/va-100-new-year-dance-party-2016-2015-mp3-t11831770.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-100-new-year-dance-party-2016-2015-mp3-t11831770.html" class="cellMainLink">VA - 100 New Year Dance Party 2016 (2015) MP3</a></div>
			</td>
			<td class="nobr center" data-sort="1397731128">1.3 <span>GB</span></td>
			<td class="center">100</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T08:08:50+00:00">30 Dec 2015, 08:08:50</span></td>
			<td class="green center">1032</td>
			<td class="red lasttd center">571</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11831084,0" class="icommentjs kaButton smallButton rightButton" href="/motÃ¶rhead-the-best-of-motorhead-2-cd-2015-320ak-t11831084.html#comment">19 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/motÃ¶rhead-the-best-of-motorhead-2-cd-2015-320ak-t11831084.html" class="cellMainLink">MotÃ¶rhead - The Best Of Motorhead (2-CD) 2015 320ak</a></div>
			</td>
			<td class="nobr center" data-sort="443050610">422.53 <span>MB</span></td>
			<td class="center">64</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T03:56:07+00:00">30 Dec 2015, 03:56:07</span></td>
			<td class="green center">1058</td>
			<td class="red lasttd center">289</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11828912,0" class="icommentjs kaButton smallButton rightButton" href="/adele-greatest-hits-2015-t11828912.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/adele-greatest-hits-2015-t11828912.html" class="cellMainLink">Adele - Greatest Hits (2015)</a></div>
			</td>
			<td class="nobr center" data-sort="241999514">230.79 <span>MB</span></td>
			<td class="center">29</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-29T18:11:27+00:00">29 Dec 2015, 18:11:27</span></td>
			<td class="green center">696</td>
			<td class="red lasttd center">183</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11824456,0" class="icommentjs kaButton smallButton rightButton" href="/avantasia-ghostlights-2016-192ak-t11824456.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/avantasia-ghostlights-2016-192ak-t11824456.html" class="cellMainLink">Avantasia - Ghostlights (2016) 192ak</a></div>
			</td>
			<td class="nobr center" data-sort="108688746">103.65 <span>MB</span></td>
			<td class="center">14</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T19:15:54+00:00">28 Dec 2015, 19:15:54</span></td>
			<td class="green center">391</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11820225,0" class="icommentjs kaButton smallButton rightButton" href="/va-memories-are-made-of-this-10-cd-1999-boxset-mp3-320-tfm-t11820225.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pictureType">
                    <a href="/va-memories-are-made-of-this-10-cd-1999-boxset-mp3-320-tfm-t11820225.html" class="cellMainLink">VA - Memories Are Made of This - 10 CD - (1999) - [BoxSet] - [MP3-320] - [TFM]</a></div>
			</td>
			<td class="nobr center" data-sort="1393318585">1.3 <span>GB</span></td>
			<td class="center">225</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-27T20:06:28+00:00">27 Dec 2015, 20:06:28</span></td>
			<td class="green center">339</td>
			<td class="red lasttd center">123</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/b-a-r-2015-week-51-jds-rg-glodls-t11832570.html" class="cellMainLink">B.A.R. 2015 Week 51 JDS-RG [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="3701741919">3.45 <span>GB</span></td>
			<td class="center">548</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T13:21:04+00:00">30 Dec 2015, 13:21:04</span></td>
			<td class="green center">167</td>
			<td class="red lasttd center">214</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11831891,0" class="icommentjs kaButton smallButton rightButton" href="/va-the-electronic-music-anthology-trip-hop-2015-mp3-320-kbps-t11831891.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-the-electronic-music-anthology-trip-hop-2015-mp3-320-kbps-t11831891.html" class="cellMainLink">VA - The Electronic Music Anthology Trip-Hop - (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="627902994">598.81 <span>MB</span></td>
			<td class="center">61</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T08:41:08+00:00">30 Dec 2015, 08:41:08</span></td>
			<td class="green center">216</td>
			<td class="red lasttd center">104</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11822693,0" class="icommentjs kaButton smallButton rightButton" href="/michael-schenker-guitar-master-3-cd-s-2015-320ak-t11822693.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/michael-schenker-guitar-master-3-cd-s-2015-320ak-t11822693.html" class="cellMainLink">Michael Schenker - Guitar Master (3-CD&#039;s) 2015 320ak</a></div>
			</td>
			<td class="nobr center" data-sort="519500918">495.43 <span>MB</span></td>
			<td class="center">59</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T10:46:11+00:00">28 Dec 2015, 10:46:11</span></td>
			<td class="green center">234</td>
			<td class="red lasttd center">57</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11836150,0" class="icommentjs kaButton smallButton rightButton" href="/david-bowie-blackstar-2016-320-leak-kat-2016-t11836150.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/david-bowie-blackstar-2016-320-leak-kat-2016-t11836150.html" class="cellMainLink">David Bowie Blackstar [2016] 320 LEAK KAT 2016</a></div>
			</td>
			<td class="nobr center" data-sort="98141601">93.6 <span>MB</span></td>
			<td class="center">9</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-31T09:05:01+00:00">31 Dec 2015, 09:05:01</span></td>
			<td class="green center">234</td>
			<td class="red lasttd center">52</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11832378,0" class="icommentjs kaButton smallButton rightButton" href="/stratovarius-the-very-best-of-2016-320ak-t11832378.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/stratovarius-the-very-best-of-2016-320ak-t11832378.html" class="cellMainLink">Stratovarius - The Very Best Of....2016 320ak</a></div>
			</td>
			<td class="nobr center" data-sort="348965269">332.8 <span>MB</span></td>
			<td class="center">43</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T12:11:12+00:00">30 Dec 2015, 12:11:12</span></td>
			<td class="green center">215</td>
			<td class="red lasttd center">72</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11820201,0" class="icommentjs kaButton smallButton rightButton" href="/va-my-chillout-music-2015-mp3-320-kbps-t11820201.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-my-chillout-music-2015-mp3-320-kbps-t11820201.html" class="cellMainLink">VA - My Chillout Music (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="588387758">561.13 <span>MB</span></td>
			<td class="center">51</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-27T19:57:35+00:00">27 Dec 2015, 19:57:35</span></td>
			<td class="green center">190</td>
			<td class="red lasttd center">91</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11822178,0" class="icommentjs kaButton smallButton rightButton" href="/va-blues-forever-vol-40-2015-mp3-t11822178.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-blues-forever-vol-40-2015-mp3-t11822178.html" class="cellMainLink">VA - Blues Forever, Vol.40 (2015) Mp3</a></div>
			</td>
			<td class="nobr center" data-sort="197917667">188.75 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T07:15:13+00:00">28 Dec 2015, 07:15:13</span></td>
			<td class="green center">176</td>
			<td class="red lasttd center">34</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11821594,0" class="icommentjs kaButton smallButton rightButton" href="/luxemusic-dance-super-chart-vol-48-2015-mp3-t11821594.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/luxemusic-dance-super-chart-vol-48-2015-mp3-t11821594.html" class="cellMainLink">LUXEmusic - Dance Super Chart Vol.48 (2015) MP3</a></div>
			</td>
			<td class="nobr center" data-sort="520946568">496.81 <span>MB</span></td>
			<td class="center">51</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T04:44:08+00:00">28 Dec 2015, 04:44:08</span></td>
			<td class="green center">155</td>
			<td class="red lasttd center">31</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/new-year-party-2016-t11835492.html" class="cellMainLink">New Year Party (2016)</a></div>
			</td>
			<td class="nobr center" data-sort="461379938">440.01 <span>MB</span></td>
			<td class="center">47</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-31T06:01:25+00:00">31 Dec 2015, 06:01:25</span></td>
			<td class="green center">126</td>
			<td class="red lasttd center">25</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/k-camp-k-i-s-s-3-320kbps-2015-official-mixjoint-t11831759.html" class="cellMainLink">K Camp - K.I.S.S. 3 [320Kbps] [2015] [Official] (MixJoint)</a></div>
			</td>
			<td class="nobr center" data-sort="71192089">67.89 <span>MB</span></td>
			<td class="center">12</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T08:03:07+00:00">30 Dec 2015, 08:03:07</span></td>
			<td class="green center">119</td>
			<td class="red lasttd center">23</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11831107,0" class="icommentjs kaButton smallButton rightButton" href="/mortal-kombat-komplete-edition-r-g-mechanics-t11831107.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/mortal-kombat-komplete-edition-r-g-mechanics-t11831107.html" class="cellMainLink">Mortal Kombat: Komplete Edition [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center" data-sort="7104082717">6.62 <span>GB</span></td>
			<td class="center">19</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T04:04:42+00:00">30 Dec 2015, 04:04:42</span></td>
			<td class="green center">179</td>
			<td class="red lasttd center">380</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11831114,0" class="icommentjs kaButton smallButton rightButton" href="/the-evil-within-r-g-mechanics-t11831114.html#comment">22 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/the-evil-within-r-g-mechanics-t11831114.html" class="cellMainLink">The Evil Within [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center" data-sort="17668525950">16.46 <span>GB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T04:07:30+00:00">30 Dec 2015, 04:07:30</span></td>
			<td class="green center">163</td>
			<td class="red lasttd center">267</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11830147,0" class="icommentjs kaButton smallButton rightButton" href="/assassin-s-creed-syndicate-jack-the-ripper-dlc-v1-31-update-multi15-fitgirl-repack-selective-download-2-3-gb-t11830147.html#comment">135 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/assassin-s-creed-syndicate-jack-the-ripper-dlc-v1-31-update-multi15-fitgirl-repack-selective-download-2-3-gb-t11830147.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/assassin-s-creed-syndicate-jack-the-ripper-dlc-v1-31-update-multi15-fitgirl-repack-selective-download-2-3-gb-t11830147.html" class="cellMainLink">Assassin&#039;s Creed - Syndicate - Jack the Ripper DLC + v1.31 Update (MULTI15) [FitGirl Repack, Selective Download - 2.3 GB]</a></div>
			</td>
			<td class="nobr center" data-sort="2938208081">2.74 <span>GB</span></td>
			<td class="center">19</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-29T22:13:21+00:00">29 Dec 2015, 22:13:21</span></td>
			<td class="green center">141</td>
			<td class="red lasttd center">261</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11831112,0" class="icommentjs kaButton smallButton rightButton" href="/minecraft-story-mode-r-g-mechanics-t11831112.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/minecraft-story-mode-r-g-mechanics-t11831112.html" class="cellMainLink">Minecraft: Story Mode [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center" data-sort="1785729849">1.66 <span>GB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T04:06:35+00:00">30 Dec 2015, 04:06:35</span></td>
			<td class="green center">209</td>
			<td class="red lasttd center">92</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11826869,0" class="icommentjs kaButton smallButton rightButton" href="/battle-of-empires-1914-1918-full-skidrow-t11826869.html#comment">21 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/battle-of-empires-1914-1918-full-skidrow-t11826869.html" class="cellMainLink">Battle of Empires : 1914-1918 Full-SKIDROW</a></div>
			</td>
			<td class="nobr center" data-sort="3670269540">3.42 <span>GB</span></td>
			<td class="center">8</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-29T07:59:06+00:00">29 Dec 2015, 07:59:06</span></td>
			<td class="green center">170</td>
			<td class="red lasttd center">161</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11836749,0" class="icommentjs kaButton smallButton rightButton" href="/sable-maze-5-soul-catcher-collector-s-edition-asg-t11836749.html#comment">2 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/sable-maze-5-soul-catcher-collector-s-edition-asg-t11836749.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/sable-maze-5-soul-catcher-collector-s-edition-asg-t11836749.html" class="cellMainLink">Sable Maze 5 - Soul Catcher Collector&#039;s Edition [ASG]</a></div>
			</td>
			<td class="nobr center" data-sort="1292719645">1.2 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-31T12:42:18+00:00">31 Dec 2015, 12:42:18</span></td>
			<td class="green center">99</td>
			<td class="red lasttd center">167</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11831109,0" class="icommentjs kaButton smallButton rightButton" href="/this-war-of-mine-r-g-mechanics-t11831109.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/this-war-of-mine-r-g-mechanics-t11831109.html" class="cellMainLink">This War of Mine [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center" data-sort="709311543">676.45 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T04:06:04+00:00">30 Dec 2015, 04:06:04</span></td>
			<td class="green center">164</td>
			<td class="red lasttd center">37</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11821029,0" class="icommentjs kaButton smallButton rightButton" href="/call-of-dutyblack-ops-zombies-v1-0-8-mod-money-android-t11821029.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/call-of-dutyblack-ops-zombies-v1-0-8-mod-money-android-t11821029.html" class="cellMainLink">Call of DutyBlack Ops Zombies v1.0.8 [Mod Money] Android</a></div>
			</td>
			<td class="nobr center" data-sort="48430648">46.19 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T01:33:46+00:00">28 Dec 2015, 01:33:46</span></td>
			<td class="green center">164</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11831124,0" class="icommentjs kaButton smallButton rightButton" href="/game-of-thrones-a-telltale-game-series-r-g-mechanics-t11831124.html#comment">26 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/game-of-thrones-a-telltale-game-series-r-g-mechanics-t11831124.html" class="cellMainLink">Game of Thrones: A Telltale Game Series [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center" data-sort="5564785811">5.18 <span>GB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T04:11:09+00:00">30 Dec 2015, 04:11:09</span></td>
			<td class="green center">97</td>
			<td class="red lasttd center">137</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11831115,0" class="icommentjs kaButton smallButton rightButton" href="/tales-from-the-borderlands-r-g-mechanics-t11831115.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/tales-from-the-borderlands-r-g-mechanics-t11831115.html" class="cellMainLink">Tales from the Borderlands [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center" data-sort="3243865589">3.02 <span>GB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T04:08:10+00:00">30 Dec 2015, 04:08:10</span></td>
			<td class="green center">114</td>
			<td class="red lasttd center">44</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11819624,0" class="icommentjs kaButton smallButton rightButton" href="/universe-sandbox-2-alpha-18b-t11819624.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/universe-sandbox-2-alpha-18b-t11819624.html" class="cellMainLink">Universe Sandbox 2 Alpha 18b</a></div>
			</td>
			<td class="nobr center" data-sort="590465621">563.11 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-27T17:11:42+00:00">27 Dec 2015, 17:11:42</span></td>
			<td class="green center">114</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11834734,0" class="icommentjs kaButton smallButton rightButton" href="/l-a-noire-the-complete-edition-v1-3-2617-multi6-fitgirl-repack-t11834734.html#comment">32 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/l-a-noire-the-complete-edition-v1-3-2617-multi6-fitgirl-repack-t11834734.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/l-a-noire-the-complete-edition-v1-3-2617-multi6-fitgirl-repack-t11834734.html" class="cellMainLink">L.A. Noire: The Complete Edition (v1.3.2617, MULTI6) [FitGirl Repack]</a></div>
			</td>
			<td class="nobr center" data-sort="12002738535">11.18 <span>GB</span></td>
			<td class="center">11</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-31T01:47:40+00:00">31 Dec 2015, 01:47:40</span></td>
			<td class="green center">32</td>
			<td class="red lasttd center">165</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11820089,0" class="icommentjs kaButton smallButton rightButton" href="/the-sims-2-the-complete-collection-full-games4theworld-t11820089.html#comment">23 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/the-sims-2-the-complete-collection-full-games4theworld-t11820089.html" class="cellMainLink">The Sims 2: The Complete Collection [FULL] *Games4theworld*</a></div>
			</td>
			<td class="nobr center" data-sort="12411175482">11.56 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-27T19:27:28+00:00">27 Dec 2015, 19:27:28</span></td>
			<td class="green center">59</td>
			<td class="red lasttd center">101</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11831121,0" class="icommentjs kaButton smallButton rightButton" href="/the-wolf-among-us-r-g-mechanics-t11831121.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/the-wolf-among-us-r-g-mechanics-t11831121.html" class="cellMainLink">The Wolf Among Us [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center" data-sort="1670003143">1.56 <span>GB</span></td>
			<td class="center">14</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T04:09:27+00:00">30 Dec 2015, 04:09:27</span></td>
			<td class="green center">75</td>
			<td class="red lasttd center">60</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11820255,0" class="icommentjs kaButton smallButton rightButton" href="/asphalt-nitro-v1-1-0i-mod-money-android-t11820255.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/asphalt-nitro-v1-1-0i-mod-money-android-t11820255.html" class="cellMainLink">Asphalt Nitro v1.1.0i [Mod Money] Android</a></div>
			</td>
			<td class="nobr center" data-sort="25011667">23.85 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-27T20:18:30+00:00">27 Dec 2015, 20:18:30</span></td>
			<td class="green center">100</td>
			<td class="red lasttd center">9</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11829796,0" class="icommentjs kaButton smallButton rightButton" href="/driverpack-solution-camaro-17-3-1-dec2015-final-seven7i-t11829796.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/driverpack-solution-camaro-17-3-1-dec2015-final-seven7i-t11829796.html" class="cellMainLink">DriverPack Solution Camaro 17.3.1 Dec2015 Final Seven7i</a></div>
			</td>
			<td class="nobr center" data-sort="12652466176">11.78 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-29T19:52:23+00:00">29 Dec 2015, 19:52:23</span></td>
			<td class="green center">926</td>
			<td class="red lasttd center">1320</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11828875,0" class="icommentjs kaButton smallButton rightButton" href="/internet-download-manager-idm-6-25-build-10-registered-32bit-64bit-patch-crackingpatching-t11828875.html#comment">54 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/internet-download-manager-idm-6-25-build-10-registered-32bit-64bit-patch-crackingpatching-t11828875.html" class="cellMainLink">Internet Download Manager (IDM) 6.25 Build 10 Registered (32bit + 64bit Patch) [CrackingPatching]</a></div>
			</td>
			<td class="nobr center" data-sort="10661054">10.17 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-29T17:58:05+00:00">29 Dec 2015, 17:58:05</span></td>
			<td class="green center">1104</td>
			<td class="red lasttd center">528</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11820877,0" class="icommentjs kaButton smallButton rightButton" href="/windows-kms-activator-ultimate-2016-v2-7-4realtorrentz-t11820877.html#comment">49 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-kms-activator-ultimate-2016-v2-7-4realtorrentz-t11820877.html" class="cellMainLink">Windows KMS Activator Ultimate 2016 v2.7 [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="20198609">19.26 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T00:28:24+00:00">28 Dec 2015, 00:28:24</span></td>
			<td class="green center">609</td>
			<td class="red lasttd center">27</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11835356,0" class="icommentjs kaButton smallButton rightButton" href="/destroy-windows-10-spying-final-version-exe-t11835356.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/destroy-windows-10-spying-final-version-exe-t11835356.html" class="cellMainLink">Destroy Windows 10 Spying [Final version].exe</a></div>
			</td>
			<td class="nobr center" data-sort="462848">452 <span>KB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-31T05:33:55+00:00">31 Dec 2015, 05:33:55</span></td>
			<td class="green center">434</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11826783,0" class="icommentjs kaButton smallButton rightButton" href="/avast-internet-security-premier-antivirus-2016-build-11-1-2245-keys-4realtorrentz-t11826783.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/avast-internet-security-premier-antivirus-2016-build-11-1-2245-keys-4realtorrentz-t11826783.html" class="cellMainLink">Avast Internet Security / Premier Antivirus 2016 Build 11.1.2245 + Keys [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="416424223">397.13 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-29T07:30:53+00:00">29 Dec 2015, 07:30:53</span></td>
			<td class="green center">283</td>
			<td class="red lasttd center">94</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11823017,0" class="icommentjs kaButton smallButton rightButton" href="/bitdefender-antivirus-plus-internet-security-total-security-2016-x86x64-incl-keys-dec2015-seven7i-t11823017.html#comment">40 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/bitdefender-antivirus-plus-internet-security-total-security-2016-x86x64-incl-keys-dec2015-seven7i-t11823017.html" class="cellMainLink">Bitdefender Antivirus Plus/Internet Security/Total Security 2016 x86x64 + Incl Keys Dec2015 Seven7i</a></div>
			</td>
			<td class="nobr center" data-sort="753531710">718.62 <span>MB</span></td>
			<td class="center">8</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T12:43:06+00:00">28 Dec 2015, 12:43:06</span></td>
			<td class="green center">176</td>
			<td class="red lasttd center">119</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11828042,0" class="icommentjs kaButton smallButton rightButton" href="/any-video-converter-ultimate-5-8-7-crack-karanpc-t11828042.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/any-video-converter-ultimate-5-8-7-crack-karanpc-t11828042.html" class="cellMainLink">Any Video Converter Ultimate 5.8.7 + Crack [KaranPC]</a></div>
			</td>
			<td class="nobr center" data-sort="39755183">37.91 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-29T14:01:31+00:00">29 Dec 2015, 14:01:31</span></td>
			<td class="green center">179</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11820938,0" class="icommentjs kaButton smallButton rightButton" href="/iobit-protected-folder-1-2-dc-26-12-2015-serial-4realtorrentz-t11820938.html#comment">16 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/iobit-protected-folder-1-2-dc-26-12-2015-serial-4realtorrentz-t11820938.html" class="cellMainLink">IObit Protected Folder 1.2 DC 26.12.2015 + Serial [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="3026696">2.89 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T00:54:14+00:00">28 Dec 2015, 00:54:14</span></td>
			<td class="green center">146</td>
			<td class="red lasttd center">7</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11830929,0" class="icommentjs kaButton smallButton rightButton" href="/eset-nod32-mobile-security-antivirus-premium-apk-v-3-2-4-0-inc-key-t11830929.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/eset-nod32-mobile-security-antivirus-premium-apk-v-3-2-4-0-inc-key-t11830929.html" class="cellMainLink">ESET NOD32 Mobile Security &amp; Antivirus Premium APK v.3.2.4.0 + InC. Key</a></div>
			</td>
			<td class="nobr center" data-sort="10144281">9.67 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T02:55:59+00:00">30 Dec 2015, 02:55:59</span></td>
			<td class="green center">118</td>
			<td class="red lasttd center">7</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11831999,0" class="icommentjs kaButton smallButton rightButton" href="/parallels-desktop-buisness-edition-v11-1-2-32408-mac-osx-core-x-t11831999.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/parallels-desktop-buisness-edition-v11-1-2-32408-mac-osx-core-x-t11831999.html" class="cellMainLink">Parallels Desktop Buisness Edition v11.1.2 (32408) {Mac OSX} - {Core-X}</a></div>
			</td>
			<td class="nobr center" data-sort="376461940">359.02 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T09:26:06+00:00">30 Dec 2015, 09:26:06</span></td>
			<td class="green center">101</td>
			<td class="red lasttd center">27</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11823042,0" class="icommentjs kaButton smallButton rightButton" href="/picshop-photo-editor-v3-0-3-apk-xpoz-t11823042.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/picshop-photo-editor-v3-0-3-apk-xpoz-t11823042.html" class="cellMainLink">PicShop â Photo Editor v3.0.3 Apk-XpoZ</a></div>
			</td>
			<td class="nobr center" data-sort="37708729">35.96 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T12:47:21+00:00">28 Dec 2015, 12:47:21</span></td>
			<td class="green center">94</td>
			<td class="red lasttd center">13</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11829720,0" class="icommentjs kaButton smallButton rightButton" href="/mx-player-pro-v1-8-2-armv7-neon-ac3-dts-xpoz-t11829720.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/mx-player-pro-v1-8-2-armv7-neon-ac3-dts-xpoz-t11829720.html" class="cellMainLink">MX Player Pro v1.8.2 [ARMv7 NEON] AC3-DTS-XpoZ</a></div>
			</td>
			<td class="nobr center" data-sort="15340984">14.63 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-29T19:24:18+00:00">29 Dec 2015, 19:24:18</span></td>
			<td class="green center">82</td>
			<td class="red lasttd center">20</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/Ð¡ÐºÐ°ÑÐ°ÑÑ-ÑÐ¾ÑÑÐµÐ½Ñ-ÐÐ»ÑÑÐ¸-Ð´Ð»Ñ-eset-kaspersky-avast-dr-web-avira-Ð¾Ñ-30-Ð´ÐµÐºÐ°Ð±ÑÑ-2015-pc-t11835316.html" class="cellMainLink">Ð¡ÐºÐ°ÑÐ°ÑÑ ÑÐ¾ÑÑÐµÐ½Ñ ÐÐ»ÑÑÐ¸ Ð´Ð»Ñ ESET, Kaspersky, Avast, Dr.Web, Avira [Ð¾Ñ 30 Ð´ÐµÐºÐ°Ð±ÑÑ] (2015) PC</a></div>
			</td>
			<td class="nobr center" data-sort="3354003">3.2 <span>MB</span></td>
			<td class="center">116</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-31T05:27:03+00:00">31 Dec 2015, 05:27:03</span></td>
			<td class="green center">58</td>
			<td class="red lasttd center">60</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11821017,0" class="icommentjs kaButton smallButton rightButton" href="/easeus-todo-backup-advanced-server-9-0-0-1-crack-4realtorrentz-t11821017.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/easeus-todo-backup-advanced-server-9-0-0-1-crack-4realtorrentz-t11821017.html" class="cellMainLink">EASEUS Todo Backup Advanced Server 9.0.0.1 + Crack [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="130182229">124.15 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T01:29:02+00:00">28 Dec 2015, 01:29:02</span></td>
			<td class="green center">76</td>
			<td class="red lasttd center">11</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11835979,0" class="icommentjs kaButton smallButton rightButton" href="/wintousb-enterprise-v2-7-setup-crack-core-x-t11835979.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/wintousb-enterprise-v2-7-setup-crack-core-x-t11835979.html" class="cellMainLink">WinToUsb Enterprise v2.7 Setup + Crack - {Core-X}</a></div>
			</td>
			<td class="nobr center" data-sort="5569801">5.31 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-31T08:09:28+00:00">31 Dec 2015, 08:09:28</span></td>
			<td class="green center">75</td>
			<td class="red lasttd center">13</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11820230,0" class="icommentjs kaButton smallButton rightButton" href="/blaze077-boruto-naruto-the-movie-720p-kor-audio-eng-subbed-t11820230.html#comment">88 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/blaze077-boruto-naruto-the-movie-720p-kor-audio-eng-subbed-t11820230.html" class="cellMainLink">[Blaze077] Boruto - Naruto The Movie [720p][Kor. Audio][Eng. Subbed]</a></div>
			</td>
			<td class="nobr center" data-sort="962596272">918 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-27T20:08:51+00:00">27 Dec 2015, 20:08:51</span></td>
			<td class="green center">1385</td>
			<td class="red lasttd center">365</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11833569,0" class="icommentjs kaButton smallButton rightButton" href="/animerg-boruto-naruto-the-movie-720p-10bit-eng-subbed-jrr-be22d4c8-mkv-t11833569.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-boruto-naruto-the-movie-720p-10bit-eng-subbed-jrr-be22d4c8-mkv-t11833569.html" class="cellMainLink">[AnimeRG] Boruto : Naruto the Movie [720p] [10bit] [Eng Subbed] [JRR] [BE22D4C8].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="580161983">553.29 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T17:43:21+00:00">30 Dec 2015, 17:43:21</span></td>
			<td class="green center">396</td>
			<td class="red lasttd center">217</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11820679,0" class="icommentjs kaButton smallButton rightButton" href="/arrg-boruto-naruto-the-movie-english-subbed-720p-sehjada-t11820679.html#comment">24 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/arrg-boruto-naruto-the-movie-english-subbed-720p-sehjada-t11820679.html" class="cellMainLink">[ARRG] Boruto: Naruto The Movie English Subbed 720P (Sehjada)</a></div>
			</td>
			<td class="nobr center" data-sort="486592560">464.05 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-27T22:57:07+00:00">27 Dec 2015, 22:57:07</span></td>
			<td class="green center">383</td>
			<td class="red lasttd center">94</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11819827,0" class="icommentjs kaButton smallButton rightButton" href="/arrg-dragon-ball-super-25-english-subbed-480p-sehjada-t11819827.html#comment">25 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/arrg-dragon-ball-super-25-english-subbed-480p-sehjada-t11819827.html" class="cellMainLink">[ARRG]Dragon Ball Super - 25 English Subbed [480P] (Sehjada)</a></div>
			</td>
			<td class="nobr center" data-sort="145798456">139.04 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-27T18:20:05+00:00">27 Dec 2015, 18:20:05</span></td>
			<td class="green center">361</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11831861,0" class="icommentjs kaButton smallButton rightButton" href="/boruto-naruto-the-movie-720p-dual-audio-eng-subbed-arrg-lucifer22-t11831861.html#comment">19 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/boruto-naruto-the-movie-720p-dual-audio-eng-subbed-arrg-lucifer22-t11831861.html" class="cellMainLink">Boruto - Naruto The Movie [720p][Dual Audio][Eng. Subbed][ARRG][Lucifer22]</a></div>
			</td>
			<td class="nobr center" data-sort="412455869">393.35 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T08:35:50+00:00">30 Dec 2015, 08:35:50</span></td>
			<td class="green center">239</td>
			<td class="red lasttd center">80</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11832481,0" class="icommentjs kaButton smallButton rightButton" href="/break-blade-broken-blade-dual-audio-1080p-hevc-x265-t11832481.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/break-blade-broken-blade-dual-audio-1080p-hevc-x265-t11832481.html" class="cellMainLink">Break Blade (Broken Blade) [DUAL-AUDIO] [1080p] [HEVC] [x265]</a></div>
			</td>
			<td class="nobr center" data-sort="3867335100">3.6 <span>GB</span></td>
			<td class="center">13</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T12:44:45+00:00">30 Dec 2015, 12:44:45</span></td>
			<td class="green center">94</td>
			<td class="red lasttd center">198</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-magi-01-25-720p-batch-t11826562.html" class="cellMainLink">[HorribleSubs] Magi (01-25) [720p] (Batch)</a></div>
			</td>
			<td class="nobr center" data-sort="8782011247">8.18 <span>GB</span></td>
			<td class="center">25</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-29T06:25:12+00:00">29 Dec 2015, 06:25:12</span></td>
			<td class="green center">116</td>
			<td class="red lasttd center">123</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11823671,0" class="icommentjs kaButton smallButton rightButton" href="/animerg-strike-the-blood-ova-02-uncensored-bd-1280x720-x264-aac-scavvykid-mkv-t11823671.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-strike-the-blood-ova-02-uncensored-bd-1280x720-x264-aac-scavvykid-mkv-t11823671.html" class="cellMainLink">[AnimeRG] Strike The Blood OVA - 02 [Uncensored] [BD 1280x720 x264 AAC] [ScavvyKiD].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="208073023">198.43 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T15:34:18+00:00">28 Dec 2015, 15:34:18</span></td>
			<td class="green center">124</td>
			<td class="red lasttd center">25</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11828284,0" class="icommentjs kaButton smallButton rightButton" href="/animerg-noragami-aragoto-complete-720p-eng-subbed-scavvykid-t11828284.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-noragami-aragoto-complete-720p-eng-subbed-scavvykid-t11828284.html" class="cellMainLink">[AnimeRG] Noragami Aragoto Complete [720p] [Eng Subbed] [ScavvyKiD]</a></div>
			</td>
			<td class="nobr center" data-sort="1705001559">1.59 <span>GB</span></td>
			<td class="center">13</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-29T15:08:56+00:00">29 Dec 2015, 15:08:56</span></td>
			<td class="green center">79</td>
			<td class="red lasttd center">96</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11832440,0" class="icommentjs kaButton smallButton rightButton" href="/animerg-heavy-object-season-1-ep-1-12-x265-1080p-mp4-optimuspr1me-t11832440.html#comment">1 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/animerg-heavy-object-season-1-ep-1-12-x265-1080p-mp4-optimuspr1me-t11832440.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-heavy-object-season-1-ep-1-12-x265-1080p-mp4-optimuspr1me-t11832440.html" class="cellMainLink">[AnimeRG] Heavy Object Season 1 [EP 1-12] [x265] [1080p] [MP4] [[OptimusPr1me]]</a></div>
			</td>
			<td class="nobr center" data-sort="2279145655">2.12 <span>GB</span></td>
			<td class="center">12</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T12:30:02+00:00">30 Dec 2015, 12:30:02</span></td>
			<td class="green center">77</td>
			<td class="red lasttd center">70</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-ani-tore-ex-mx-1280x720-x264-aac-t11824230.html" class="cellMainLink">[Leopard-Raws] Ani Tore! EX (MX 1280x720 x264 AAC)</a></div>
			</td>
			<td class="nobr center" data-sort="884424451">843.45 <span>MB</span></td>
			<td class="center">12</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T18:00:02+00:00">28 Dec 2015, 18:00:02</span></td>
			<td class="green center">81</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11835841,0" class="icommentjs kaButton smallButton rightButton" href="/animerg-tokyo-ghoul-ova-pinto-bd-1280x720-x264-aac-uncensored-scavvykid-mkv-t11835841.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-tokyo-ghoul-ova-pinto-bd-1280x720-x264-aac-uncensored-scavvykid-mkv-t11835841.html" class="cellMainLink">[AnimeRG] Tokyo Ghoul OVA (Pinto) (BD 1280x720 x264 AAC) [Uncensored] [ScavvyKiD].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="212081200">202.26 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-31T07:28:42+00:00">31 Dec 2015, 07:28:42</span></td>
			<td class="green center">65</td>
			<td class="red lasttd center">13</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fff-noragami-aragoto-13-25bef3b7-mkv-t11821401.html" class="cellMainLink">[FFF] Noragami Aragoto - 13 [25BEF3B7].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="322178679">307.25 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T03:50:02+00:00">28 Dec 2015, 03:50:02</span></td>
			<td class="green center">22</td>
			<td class="red lasttd center">87</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11821188,0" class="icommentjs kaButton smallButton rightButton" href="/dragon-ball-super-025-english-subbed-720p-arizone-t11821188.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dragon-ball-super-025-english-subbed-720p-arizone-t11821188.html" class="cellMainLink">Dragon Ball Super - 025 [ENGLISH SUBBED] 720p ~ARIZONE</a></div>
			</td>
			<td class="nobr center" data-sort="147574346">140.74 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T02:30:08+00:00">28 Dec 2015, 02:30:08</span></td>
			<td class="green center">44</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fff-aku-no-hana-bd-720p-aac-t11831675.html" class="cellMainLink">[FFF] Aku no Hana [BD][720p-AAC]</a></div>
			</td>
			<td class="nobr center" data-sort="3296081890">3.07 <span>GB</span></td>
			<td class="center">13</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T07:35:03+00:00">30 Dec 2015, 07:35:03</span></td>
			<td class="green center">39</td>
			<td class="red lasttd center">21</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11826503,0" class="icommentjs kaButton smallButton rightButton" href="/how-to-fix-absolutely-anything-a-homeowner-s-guide-t11826503.html#comment">16 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/how-to-fix-absolutely-anything-a-homeowner-s-guide-t11826503.html" class="cellMainLink">How to Fix Absolutely Anything: A Homeowner&#039;s Guide</a></div>
			</td>
			<td class="nobr center" data-sort="101557590">96.85 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-29T06:08:48+00:00">29 Dec 2015, 06:08:48</span></td>
			<td class="green center">1202</td>
			<td class="red lasttd center">129</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11824439,0" class="icommentjs kaButton smallButton rightButton" href="/how-to-remember-things-10-memory-tricks-to-recall-everything-2015-epub-ertb-t11824439.html#comment">23 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/how-to-remember-things-10-memory-tricks-to-recall-everything-2015-epub-ertb-t11824439.html" class="cellMainLink">How to Remember Things - 10 Memory Tricks to Recall Everything 2015 [ePUB] {{ERTB}}</a></div>
			</td>
			<td class="nobr center" data-sort="162180">158.38 <span>KB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T19:03:04+00:00">28 Dec 2015, 19:03:04</span></td>
			<td class="green center">1036</td>
			<td class="red lasttd center">105</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11833148,0" class="icommentjs kaButton smallButton rightButton" href="/dc-week-12-30-2015-aka-dc-you-week-31-nem-t11833148.html#comment">21 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/dc-week-12-30-2015-aka-dc-you-week-31-nem-t11833148.html" class="cellMainLink">DC Week+ (12-30-2015) (aka DC YOU Week 31) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="621112331">592.34 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T15:51:41+00:00">30 Dec 2015, 15:51:41</span></td>
			<td class="green center">571</td>
			<td class="red lasttd center">301</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11820283,0" class="icommentjs kaButton smallButton rightButton" href="/the-new-slow-cooker-cookbook-more-than-200-modern-healthy-and-easy-recipes-for-the-classic-cooker-t11820283.html#comment">16 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-new-slow-cooker-cookbook-more-than-200-modern-healthy-and-easy-recipes-for-the-classic-cooker-t11820283.html" class="cellMainLink">The New Slow Cooker Cookbook: More than 200 Modern, Healthy--and Easy--Recipes for the Classic Cooker</a></div>
			</td>
			<td class="nobr center" data-sort="8971622">8.56 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-27T20:29:15+00:00">27 Dec 2015, 20:29:15</span></td>
			<td class="green center">466</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11819826,0" class="icommentjs kaButton smallButton rightButton" href="/71-science-experiments-making-science-simpler-for-you-2012-epub-gooner-t11819826.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/71-science-experiments-making-science-simpler-for-you-2012-epub-gooner-t11819826.html" class="cellMainLink">71 Science Experiments - Making Science Simpler For You (2012).epub Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="10148609">9.68 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-27T18:19:59+00:00">27 Dec 2015, 18:19:59</span></td>
			<td class="green center">402</td>
			<td class="red lasttd center">19</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11819750,0" class="icommentjs kaButton smallButton rightButton" href="/47-easy-to-do-classic-science-experiments-2012-epub-gooner-t11819750.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/47-easy-to-do-classic-science-experiments-2012-epub-gooner-t11819750.html" class="cellMainLink">47 Easy-to-Do Classic Science Experiments (2012).epub Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="8656324">8.26 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-27T17:54:30+00:00">27 Dec 2015, 17:54:30</span></td>
			<td class="green center">358</td>
			<td class="red lasttd center">9</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11828693,0" class="icommentjs kaButton smallButton rightButton" href="/injustice-gods-among-us-year-five-002-2015-digital-son-of-ultron-empire-cbr-nem-t11828693.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/injustice-gods-among-us-year-five-002-2015-digital-son-of-ultron-empire-cbr-nem-t11828693.html" class="cellMainLink">Injustice - Gods Among Us - Year Five 002 (2015) (digital) (Son of Ultron-Empire).cbr (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="21301448">20.31 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-29T17:01:47+00:00">29 Dec 2015, 17:01:47</span></td>
			<td class="green center">305</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11825575,0" class="icommentjs kaButton smallButton rightButton" href="/assorted-magazines-bundle-group-1-december-28-2015-true-pdf-deluxas-t11825575.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/assorted-magazines-bundle-group-1-december-28-2015-true-pdf-deluxas-t11825575.html" class="cellMainLink">Assorted Magazines Bundle [group 1] - December 28 2015 (True PDF) - DeLUXAS</a></div>
			</td>
			<td class="nobr center" data-sort="575870032">549.19 <span>MB</span></td>
			<td class="center">12</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-29T01:34:19+00:00">29 Dec 2015, 01:34:19</span></td>
			<td class="green center">249</td>
			<td class="red lasttd center">125</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11821921,0" class="icommentjs kaButton smallButton rightButton" href="/homemade-sausage-recipes-and-techniques-to-grind-stuff-and-twist-artisanal-sausage-at-home-t11821921.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/homemade-sausage-recipes-and-techniques-to-grind-stuff-and-twist-artisanal-sausage-at-home-t11821921.html" class="cellMainLink">Homemade Sausage Recipes and Techniques to Grind, Stuff, and Twist Artisanal Sausage at Home</a></div>
			</td>
			<td class="nobr center" data-sort="38840411">37.04 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T06:03:38+00:00">28 Dec 2015, 06:03:38</span></td>
			<td class="green center">294</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11821945,0" class="icommentjs kaButton smallButton rightButton" href="/the-complete-book-of-jerky-how-to-process-prepare-and-dry-beef-venison-turkey-fish-and-more-t11821945.html#comment">19 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/the-complete-book-of-jerky-how-to-process-prepare-and-dry-beef-venison-turkey-fish-and-more-t11821945.html" class="cellMainLink">The Complete Book of Jerky How to Process, Prepare, and Dry Beef, Venison, Turkey, Fish, and More</a></div>
			</td>
			<td class="nobr center" data-sort="58001860">55.31 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T06:07:13+00:00">28 Dec 2015, 06:07:13</span></td>
			<td class="green center">247</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11831597,0" class="icommentjs kaButton smallButton rightButton" href="/visual-basic-crash-course-the-ultimate-beginner-s-course-to-learning-visual-basic-programming-in-under-12-hours-t11831597.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/visual-basic-crash-course-the-ultimate-beginner-s-course-to-learning-visual-basic-programming-in-under-12-hours-t11831597.html" class="cellMainLink">Visual Basic Crash Course - The Ultimate Beginner&#039;s Course to Learning Visual Basic Programming in Under 12 Hours</a></div>
			</td>
			<td class="nobr center" data-sort="3794550">3.62 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T07:02:06+00:00">30 Dec 2015, 07:02:06</span></td>
			<td class="green center">220</td>
			<td class="red lasttd center">28</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/playboy-usa-january-february-2016-ertb-t11835958.html" class="cellMainLink">Playboy USA - January - February 2016 {{ERTB}}</a></div>
			</td>
			<td class="nobr center" data-sort="135163782">128.9 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-31T08:04:00+00:00">31 Dec 2015, 08:04:00</span></td>
			<td class="green center">2</td>
			<td class="red lasttd center">450</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11832228,0" class="icommentjs kaButton smallButton rightButton" href="/0-day-week-of-2015-12-23-t11832228.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/0-day-week-of-2015-12-23-t11832228.html" class="cellMainLink">0-Day Week of 2015.12.23</a></div>
			</td>
			<td class="nobr center" data-sort="6826286044">6.36 <span>GB</span></td>
			<td class="center">135</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T11:05:57+00:00">30 Dec 2015, 11:05:57</span></td>
			<td class="green center">121</td>
			<td class="red lasttd center">208</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11831969,0" class="icommentjs kaButton smallButton rightButton" href="/justice-league-047-2016-webrip-the-last-kryptonian-dcp-cbr-t11831969.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/justice-league-047-2016-webrip-the-last-kryptonian-dcp-cbr-t11831969.html" class="cellMainLink">Justice League 047 (2016) (Webrip) (The Last Kryptonian-DCP).cbr</a></div>
			</td>
			<td class="nobr center" data-sort="40312546">38.45 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T09:08:46+00:00">30 Dec 2015, 09:08:46</span></td>
			<td class="green center">204</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11832427,0" class="icommentjs kaButton smallButton rightButton" href="/squadron-supreme-002-2016-digital-zone-empire-cbr-t11832427.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/squadron-supreme-002-2016-digital-zone-empire-cbr-t11832427.html" class="cellMainLink">Squadron Supreme 002 (2016) (Digital) (Zone-Empire).cbr</a></div>
			</td>
			<td class="nobr center" data-sort="36459084">34.77 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T12:27:40+00:00">30 Dec 2015, 12:27:40</span></td>
			<td class="green center">199</td>
			<td class="red lasttd center">14</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11823869,0" class="icommentjs kaButton smallButton rightButton" href="/bob-marley-legend-remastered-2012-24-192-hd-flac-t11823869.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bob-marley-legend-remastered-2012-24-192-hd-flac-t11823869.html" class="cellMainLink">Bob Marley - Legend Remastered (2012) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="2705321977">2.52 <span>GB</span></td>
			<td class="center">39</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T16:21:47+00:00">28 Dec 2015, 16:21:47</span></td>
			<td class="green center">274</td>
			<td class="red lasttd center">165</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11823888,0" class="icommentjs kaButton smallButton rightButton" href="/va-ultimate-country-4cd-2015-flac-t11823888.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-ultimate-country-4cd-2015-flac-t11823888.html" class="cellMainLink">VA - Ultimate... Country [4CD] (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1606009787">1.5 <span>GB</span></td>
			<td class="center">108</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T16:24:58+00:00">28 Dec 2015, 16:24:58</span></td>
			<td class="green center">159</td>
			<td class="red lasttd center">73</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11832235,0" class="icommentjs kaButton smallButton rightButton" href="/the-specials-specials-2015-24-96-hd-flac-t11832235.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-specials-specials-2015-24-96-hd-flac-t11832235.html" class="cellMainLink">The Specials - Specials (2015) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1125341075">1.05 <span>GB</span></td>
			<td class="center">37</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T11:09:56+00:00">30 Dec 2015, 11:09:56</span></td>
			<td class="green center">152</td>
			<td class="red lasttd center">57</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/andrea-bocelli-discography-1994-2013-flac-classical-crossover-opera-pop-t11823058.html" class="cellMainLink">Andrea Bocelli - Discography (1994-2013) FLAC [Classical crossover, Opera, Pop]</a></div>
			</td>
			<td class="nobr center" data-sort="23143127676">21.55 <span>GB</span></td>
			<td class="center">530</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T12:52:30+00:00">28 Dec 2015, 12:52:30</span></td>
			<td class="green center">63</td>
			<td class="red lasttd center">158</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11824335,0" class="icommentjs kaButton smallButton rightButton" href="/randy-newman-studio-discography-1968-2008-flac-t11824335.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/randy-newman-studio-discography-1968-2008-flac-t11824335.html" class="cellMainLink">Randy Newman - Studio Discography (1968-2008) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="4073484316">3.79 <span>GB</span></td>
			<td class="center">402</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T18:32:04+00:00">28 Dec 2015, 18:32:04</span></td>
			<td class="green center">86</td>
			<td class="red lasttd center">79</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11828889,0" class="icommentjs kaButton smallButton rightButton" href="/haydn-symphonies-nos-92-93-97-99-lso-colin-davis-2014-24-96-hd-flac-t11828889.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/haydn-symphonies-nos-92-93-97-99-lso-colin-davis-2014-24-96-hd-flac-t11828889.html" class="cellMainLink">Haydn - Symphonies Nos. 92 &amp; 93, 97-99 - LSO, Colin Davis (2014) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="2883361581">2.69 <span>GB</span></td>
			<td class="center">38</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-29T18:04:50+00:00">29 Dec 2015, 18:04:50</span></td>
			<td class="green center">75</td>
			<td class="red lasttd center">73</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11820047,0" class="icommentjs kaButton smallButton rightButton" href="/stevie-ray-vaughan-live-at-the-austin-opera-house-1984-sbd-flac-t11820047.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/stevie-ray-vaughan-live-at-the-austin-opera-house-1984-sbd-flac-t11820047.html" class="cellMainLink">Stevie Ray Vaughan - Live at the Austin Opera House 1984 (SBD) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="501879178">478.63 <span>MB</span></td>
			<td class="center">40</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-27T19:19:07+00:00">27 Dec 2015, 19:19:07</span></td>
			<td class="green center">97</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/haydn-the-complete-string-quartets-angeles-quartet-t11833941.html" class="cellMainLink">Haydn - The Complete String Quartets - Angeles Quartet</a></div>
			</td>
			<td class="nobr center" data-sort="6304634665">5.87 <span>GB</span></td>
			<td class="center">302</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T19:57:40+00:00">30 Dec 2015, 19:57:40</span></td>
			<td class="green center">41</td>
			<td class="red lasttd center">125</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11820152,0" class="icommentjs kaButton smallButton rightButton" href="/blackmore-s-rainbow-dio-hits-2005-flac-ak-t11820152.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/blackmore-s-rainbow-dio-hits-2005-flac-ak-t11820152.html" class="cellMainLink">Blackmore&#039;s Rainbow - DIO Hits 2005 FLAC ak</a></div>
			</td>
			<td class="nobr center" data-sort="1162764690">1.08 <span>GB</span></td>
			<td class="center">22</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-27T19:44:11+00:00">27 Dec 2015, 19:44:11</span></td>
			<td class="green center">88</td>
			<td class="red lasttd center">30</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11836349,0" class="icommentjs kaButton smallButton rightButton" href="/lucio-battisti-pensieri-emozioni-2cd-flac-tntvillage-t11836349.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/lucio-battisti-pensieri-emozioni-2cd-flac-tntvillage-t11836349.html" class="cellMainLink">Lucio Battisti - Pensieri Emozioni (2CD)[Flac][TntVillage]</a></div>
			</td>
			<td class="nobr center" data-sort="760629394">725.39 <span>MB</span></td>
			<td class="center">44</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-31T10:18:57+00:00">31 Dec 2015, 10:18:57</span></td>
			<td class="green center">72</td>
			<td class="red lasttd center">54</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11827073,0" class="icommentjs kaButton smallButton rightButton" href="/luigi-tenco-tenco-2cd-flac-tntvillage-t11827073.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/luigi-tenco-tenco-2cd-flac-tntvillage-t11827073.html" class="cellMainLink">Luigi Tenco - Tenco (2CD)[Flac][TntVillage]</a></div>
			</td>
			<td class="nobr center" data-sort="800342680">763.27 <span>MB</span></td>
			<td class="center">64</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-29T09:13:49+00:00">29 Dec 2015, 09:13:49</span></td>
			<td class="green center">73</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11832001,0" class="icommentjs kaButton smallButton rightButton" href="/paul-butterfield-band-rockpalast-blues-rock-legends-vol-2-2010-flac-t11832001.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/paul-butterfield-band-rockpalast-blues-rock-legends-vol-2-2010-flac-t11832001.html" class="cellMainLink">Paul Butterfield Band - Rockpalast-Blues Rock Legends Vol. 2 (2010) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="428979358">409.11 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-30T09:28:29+00:00">30 Dec 2015, 09:28:29</span></td>
			<td class="green center">77</td>
			<td class="red lasttd center">13</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11824964,0" class="icommentjs kaButton smallButton rightButton" href="/mountain-official-bootleg-series-collection-flac-t11824964.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/mountain-official-bootleg-series-collection-flac-t11824964.html" class="cellMainLink">Mountain - Official Bootleg Series - Collection [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="2386692073">2.22 <span>GB</span></td>
			<td class="center">85</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T21:53:16+00:00">28 Dec 2015, 21:53:16</span></td>
			<td class="green center">67</td>
			<td class="red lasttd center">28</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11823088,0" class="icommentjs kaButton smallButton rightButton" href="/pino-daniele-discography-1977-2013-flac-rumba-blues-jazz-rock-tarantella-folk-fusion-soul-funk-pop-t11823088.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/pino-daniele-discography-1977-2013-flac-rumba-blues-jazz-rock-tarantella-folk-fusion-soul-funk-pop-t11823088.html" class="cellMainLink">Pino Daniele - Discography (1977-2013) FLAC [Rumba, Blues, Jazz, Rock, Tarantella, Folk, Fusion, Soul, Funk, Pop]</a></div>
			</td>
			<td class="nobr center" data-sort="9626754275">8.97 <span>GB</span></td>
			<td class="center">167</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T12:59:22+00:00">28 Dec 2015, 12:59:22</span></td>
			<td class="green center">27</td>
			<td class="red lasttd center">69</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11823079,0" class="icommentjs kaButton smallButton rightButton" href="/franco-battiato-collection-1980-2009-flac-beat-new-wave-experimental-progressive-pop-opera-free-jazz-t11823079.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/franco-battiato-collection-1980-2009-flac-beat-new-wave-experimental-progressive-pop-opera-free-jazz-t11823079.html" class="cellMainLink">Franco Battiato - Collection (1980-2009) FLAC [Beat, New wave, Experimental, Progressive pop, Opera, Free jazz]</a></div>
			</td>
			<td class="nobr center" data-sort="6440604173">6 <span>GB</span></td>
			<td class="center">174</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-28T12:57:27+00:00">28 Dec 2015, 12:57:27</span></td>
			<td class="green center">28</td>
			<td class="red lasttd center">46</td>
        </tr>
			</table>


	<script type="text/javascript">
		if ($.cookie('kat_settings[fpw_column]') != null && $.cookie('kat_settings[fpw_order]')) {
			var colIndex = $.cookie('kat_settings[fpw_column]')
			var ascend = $.cookie('kat_settings[fpw_order]');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			sortTables(colIndex, ascend);
		}
		$('.frontPageWidget .firstr th .fpw-sort').click(function() {
			var colIndex = $(this).parent().index();
			var ascend = !$(this).hasClass('sortedAsc')?1:0;
			$('.fpw-sort').removeClass('sortedAsc sortedDesc');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			updateFPWsettings(colIndex, ascend);
			sortTables(colIndex, ascend);
		});
		function sortTables(colIndex, ascend) {	
			$('.frontPageWidget').each(function() {
				var data = [];
				$('tr:not(.firstr)', $(this)).each(function() {
					var name = $('td:first .cellMainLink', $(this)).text();
					var id = $('td:first .cellMainLink', $(this)).attr('href').match(/.*-t(\d+)\.html/)[1];
					var fileSize = $('td:eq(1)', $(this)).attr('data-sort');
					data.push({
						'col0':name, 'col1':padNum(fileSize), 'col2':padNum($('td:eq(2)',$(this)).text()), 'col3':padNum(id), 'col4':padNum($('td:eq(4)',$(this)).text()), 'col5':padNum($('td:eq(5)',$(this)).text()), 'html':$(this).html()
					});
				});
				data.sort(function(a, b) {
					var x = a['col'+colIndex].toLowerCase(); var y = b['col'+colIndex].toLowerCase();
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
				if (!ascend||ascend=='0') data.reverse();
				$(this).find('tr:not(.firstr)').remove();
				for (var i=0;i<data.length;i++) {
					$(this).append('<tr class="'+((i+1)%2==0?'even':'odd')+'">'+data[i].html+'</tr>');
				}
			});
		}
		function padNum(val) {
			val = '0000000000000000'+val;
			return val.substring(val.length-15);
		}
		function updateFPWsettings(t, o) {
			$.cookie('kat_settings[fpw_column]', t, { expires: 365, path: '/' });
			$.cookie('kat_settings[fpw_order]', o, { expires: 365, path: '/' });
		}
	</script>
		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
    
            
        <div  data-sc-slot="_119b0a17fab5493361a252d04bf527db"></div>
    
                
    	    <div class="spareBlock">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <div  data-sc-slot="_7063408f1c01d50e0dc2d833186ce962" data-sc-params="{ 'searchQuery': '' }"></div>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/what-movie-did-you-last-watch-and-what-did-you-think-it-v-3/?unread=17269081">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What movie did you last watch? And what did you think of it? V.3!
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_4"><a class="plain" href="/user/Cheetah.Baze/">Cheetah.Baze</a></span></span> <time class="timeago" datetime="2015-12-31T17:08:56+00:00">31 Dec 2015, 17:08</time></span>
	</li>
		<li>
		<a href="/community/show/club-protocol-uploads/?unread=17269078">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				&quot;Club Protocol&quot; Uploads
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_verified"><a class="plain" href="/user/D-DMan/">D-DMan</a></span></span> <time class="timeago" datetime="2015-12-31T17:08:09+00:00">31 Dec 2015, 17:08</time></span>
	</li>
		<li>
		<a href="/community/show/what-tv-show-are-you-watching-right-now-v4-thread-120579/?unread=17269077">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What TV show are you watching right now? V4
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_4"><a class="plain" href="/user/Cheetah.Baze/">Cheetah.Baze</a></span></span> <time class="timeago" datetime="2015-12-31T17:07:42+00:00">31 Dec 2015, 17:07</time></span>
	</li>
		<li>
		<a href="/community/show/adopt-uploader-program-v12-all-users-help-thread-115043/?unread=17269068">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				**Adopt an uploader Program v12-- For all Users to Help**
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/Katelugu/">Katelugu</a></span></span> <time class="timeago" datetime="2015-12-31T17:05:07+00:00">31 Dec 2015, 17:05</time></span>
	</li>
		<li>
		<a href="/community/show/what-are-you-listening-right-now-v5-thread-116026/?unread=17269066">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What are you listening to right now? V5
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/sorsselson/">sorsselson</a></span></span> <time class="timeago" datetime="2015-12-31T17:03:04+00:00">31 Dec 2015, 17:03</time></span>
	</li>
		<li>
		<a href="/community/show/games-request/?unread=17269058">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Games Request
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_2"><a class="plain" href="/user/DJ840/">DJ840</a></span></span> <time class="timeago" datetime="2015-12-31T16:59:59+00:00">31 Dec 2015, 16:59</time></span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/merry-xmas/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Merry Xmas
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-12-23T16:54:28+00:00">23 Dec 2015, 16:54</time></span>
	</li>
	<li>
		<a href="/blog/post/new-site-rules/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				New site Rules
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-15T14:18:43+00:00">15 Oct 2015, 14:18</time></span>
	</li>
	<li>
		<a href="/blog/post/look-mama-i-m-popular/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Look, mama, I&#039;m popular!
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-05T17:42:40+00:00">05 Oct 2015, 17:42</time></span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/TheDels/post/10-9-8/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> 10... 9.... 8.....</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/TheDels/">TheDels</a> <time class="timeago" datetime="2015-12-31T15:03:07+00:00">31 Dec 2015, 15:03</time></span></li>
	<li><a href="/blog/johnno23/post/2015-bye-bye/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> 2015 bye bye</p></a><span class="explanation">by <a class="plain aclColor_5" href="/user/johnno23/">johnno23</a> <time class="timeago" datetime="2015-12-31T13:53:51+00:00">31 Dec 2015, 13:53</time></span></li>
	<li><a href="/blog/ScavvyKiD/post/are-credits-for-the-source-important-anymore-on-public-torrent-sites/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Are credits for the source important anymore on public torrent sites?</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/ScavvyKiD/">ScavvyKiD</a> <time class="timeago" datetime="2015-12-31T12:41:09+00:00">31 Dec 2015, 12:41</time></span></li>
	<li><a href="/blog/Eng.CiviL/post/finally/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Finally!</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/Eng.CiviL/">Eng.CiviL</a> <time class="timeago" datetime="2015-12-31T06:47:07+00:00">31 Dec 2015, 06:47</time></span></li>
	<li><a href="/blog/TheDels/post/2015-a-look-back/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> 2015 (A look back)</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/TheDels/">TheDels</a> <time class="timeago" datetime="2015-12-30T12:17:36+00:00">30 Dec 2015, 12:17</time></span></li>
	<li><a href="/blog/OptimusPr1me/post/anyone-want-some-hot-cocoa/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Anyone want some hot cocoa ?</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/OptimusPr1me/">OptimusPr1me</a> <time class="timeago" datetime="2015-12-30T04:32:57+00:00">30 Dec 2015, 04:32</time></span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/katy%20d/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				katy d
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/true%20detective%201080p/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				true detective 1080p
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/super%20mario%20maker/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Super Mario Maker
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/kendra%20lust/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Kendra Lust
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/the%20words/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				the words
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/ebooks%20collections/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				ebooks collections
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/on%20golden%20pond/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				On Golden Pond
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/iron%20man/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Iron man
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/arrow%20s02e06/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				arrow S02E06
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/mother%20and%20child/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				mother and child
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/marc%20dorcel/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				marc dorcel
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="line-height:140%;-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a href="#" onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a href="#" onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a href="#" onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a href="#" onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a href="#" onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a href="#" onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a href="#" onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a href="#" onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a href="#" onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a href="#" onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a href="#" onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a href="#" onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a href="#" onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a href="#" onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a href="#" onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a href="#" onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a href="#" onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a href="#" onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a href="#" onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a href="#" onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a href="#" onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a href="#" onclick="setLanguage('he', '.kat.cr');return false;" class="plain">Hebrew</a></li>
                                <li><a href="#" onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a href="#" onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a href="#" onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a href="#" onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a href="#" onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a href="#" onclick="setLanguage('ko', '.kat.cr');return false;" class="plain">Korean</a></li>
                                <li><a href="#" onclick="setLanguage('lv', '.kat.cr');return false;" class="plain">Latvian</a></li>
                                <li><a href="#" onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a href="#" onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a href="#" onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a href="#" onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a href="#" onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a href="#" onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a href="#" onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a href="#" onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a href="#" onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a href="#" onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a href="#" onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a href="#" onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a href="#" onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a href="#" onclick="setLanguage('bsc', '.kat.cr');return false;" class="plain">Serbian-Cyrillic (ijekavica)</a></li>
                                <li><a href="#" onclick="setLanguage('si', '.kat.cr');return false;" class="plain">Sinhala</a></li>
                                <li><a href="#" onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a href="#" onclick="setLanguage('sl', '.kat.cr');return false;" class="plain">Slovenian</a></li>
                                <li><a href="#" onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a href="#" onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a href="#" onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a href="#" onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a href="#" onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a href="#" onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a href="#" onclick="setLanguage('ur', '.kat.cr');return false;" class="plain">Urdu</a></li>
                                <li><a href="#" onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" data-nop href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
        		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
        <li><a href="https://kastatus.com">KAT status</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <div  data-sc-slot="_673e31f53f8166159b8e996c4124765b"></div>
        <div  data-sc-slot="_e7050fb15fd39b3e4e99a5be4a57b6ea"></div>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script>
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "Organization",
"name": "KickassTorrents",
"url": "https://kat.cr",
"logo": "https://kat.cr/content/images/kickasslogo.png",
"sameAs": [
    "https://www.facebook.com/Official.KAT.Fanclub",
    "https://twitter.com/kickasstorrents"
]
}
</script>
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "WebSite",
"url": "https://kat.cr",
"potentialAction": {
    "@type": "SearchAction",
    "target": "https://kat.cr/usearch/{q}/",
    "query-input": {
        "@type": "PropertyValueSpecification",
        "valueRequired": true,
        "valueName": "q"
    }
}
}
</script>
<script>
 sc('addGlobal', 'pagetype', 'front');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>

</body>
</html>
