<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trending  stories on Indian Lifestyle, Culture, Relationships, Food, Travel, Entertainment, News & New Technology News - Indiatimes.com</title>

<meta name="keywords" content="" />
<meta name="description" content="Indiatimes.com brings you the news, articles, stories and videos on entertainment, latest lifestyle, culture & new technologies emerging worldwide." />



		

<link rel="image_src" href="http://media.indiatimes.in/resources/images/fbimage.png" />
<meta property="og:title" content="" />    
<meta property="og:url" content="http://www.indiatimes.com" />
<meta property="og:image" content="http://media.indiatimes.in/resources/images/fbimage.png" />

<meta property="og:site_name" content="indiatimes.com" />
<meta property="fb:app_id" content="117787264903013" />
<meta property="og:description" content="" />
<meta name="google-site-verification" content="ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw" />
<meta name="msvalidate.01" content="BAB71AAED7CCC01DB106A58E99625EB4" />
<link href="https://plus.google.com/+indiatimes/posts" rel="publisher" />

<meta property="og:type" content="website" />
    
<!--<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />-->

<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">    <link rel="shortcut icon" href="http://media.indiatimes.in/resources/images/favicon.ico?v=25" type="image/x-icon"><link rel="stylesheet" href="http://www.indiatimes.com/fonts/fonts.min.css?v=100.69" media="screen"><link rel="stylesheet" href="http://media.indiatimes.in/resources/css/site.css?v=100.69" media="screen"><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery-1.11.1.min.js?v=100.69"></script><script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/handlebars-v1.3.0.js?v=100.69"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.inview.min.js?v=100.69"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.preload.min.js?v=100.69"></script><script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script><!-- Facebook Pixel Code --><script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');fbq('init', '853339751421540');fbq('track', 'PageView');</script><noscript><img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=853339751421540&ev=PageView&noscript=1"/></noscript><!-- End Facebook Pixel Code -->           <!--  Analytics Code Begin -->    <!-- Begin comScore Tag -->    <script>        var _comscore = _comscore || [];        _comscore.push({ c1: "2", c2: "6036484" });        (function() {            var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";            el.parentNode.insertBefore(s, el);        })();    </script>    <noscript>    <img src="http://b.scorecardresearch.com/p?c1=2&c2=6036484&cv=2.0&cj=1" />    </noscript>    <!-- End comScore Tag -->    <!-- Begin Google Analytics Tag -->    <script>        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');        ga('create', 'UA-198011-6', 'indiatimes.com');            ga('require', 'displayfeatures');        ga('send', 'pageview');    </script>    <!-- End Google Analytics Tag -->    <!--  Analytics Code Begin --><!--adcode common script--><script type='text/javascript' src="http://ad.crwdcntrl.net/5/c=2800/pe=y/var=_ccaud"></script><script type="text/javascript">    var googletag = googletag || {};    googletag.cmd = googletag.cmd || [];    (function() {        var gads = document.createElement("script");        gads.async = true;        gads.type = "text/javascript";        var useSSL = "https:" == document.location.protocol;        gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";        var node =document.getElementsByTagName("script")[0];        node.parentNode.insertBefore(gads, node);    })();</script> <script type="text/javascript">        var nid='';        var cid='';        var sid='';        var brand_name='';                                                             </script>    <script>
    var contType = "";
    </script>
        <script type="text/javascript" src="http://media.indiatimes.in/resources/js/banner/home.js?v=100.69"></script>
            
<!--[if lt IE 9]>
  <script type="text/javascript" src="http://media.indiatimes.in/js/html5.js?v1.1"></script>
  <![endif]-->
<script>
var isDetailPage = 0</script>
</head>
<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];   
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=117787264903013&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script> 	
	
<div id="wrap" class="cf"><!--wrap start-->
    <div class='darkDisabledBg' id='disabledBg'></div>
<header>
    <div class="header-inner">
        <div class="comm-header ">
            <nav>
                <div class='forTop'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>

                    <div class="lnks">
                                                        <a href="http://www.indiatimes.com/news/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','News');">News</a> 
                                                               <a href="http://www.indiatimes.com/lifestyle/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Lifestyle');">Lifestyle</a> 
                                                               <a href="http://www.indiatimes.com/entertainment/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Entertainment');">Entertainment</a> 
                                                               <a href="http://www.indiatimes.com/health/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Health');">Health</a> 
                                                               <a href="http://www.indiatimes.com/videocafe/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Videos');">Videos</a> 
                                                                                          <a href='http://www.indiatimes.com/trending'  target="_blank" class="#fffff" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Trending');">Trending</a> 
                                                            <a href='http://www.indiatimes.com/stories-that-matter-in-2016'  target="_blank" class="" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','2015 In Shots');">2015 In Shots</a> 
                                                </div>
                </div>
                <div class='onScrolled'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>
                    <a href="javascript:;" class="menu rc sprite" id="headerMenu">Menu</a>
                    <div class="has-tag">
                                                       <a href='http://www.indiatimes.com/stories-that-matter-in-2016'  target="_blank" class="" >2015 In Shots</a> 
                                                </div>
                </div>

                                
            </nav>
			                 
	    <div id="sticker" style="display:none;"><span class="readTitle">
				</span></div>
                    <div class="socls fr share">
            </div>
                        
            
			 <div class="social fr">
                            <a title="facebook" target="_blank"  class="sprite fb" href="https://www.facebook.com/indiatimes">facebook</a>
                            <a title="twitter" target="_blank"  class="sprite twt" href="https://twitter.com/indiatimes">twitter</a>
                            <a title="search"  class="sprite serach" onclick="$('body').showSearch();" href="javascript:void(0);">search</a>
                        </div>
        </div>
    </div>
</header>
<div class="clr"></div>
<div id="overlay" class="animated bounceOut"><!--overlay start-->
    <div class="search_conts"><!--search-cont start-->
        <form onsubmit="return false;" class="serach-form">
            <input type="text" onkeyup="search_data();" class="input" onfocus="if(this.value=='Search'){this.value=''}" onblur="if(this.value==''){this.value='Search'};" value="Search" name="q" id="q" autocomplete="off">
            <input type="button" class="sprite src-btn" onclick="search_data();">
            <a class="sprite sclose" id="close" href="javascript:void(0);">X</a>
        </form>
        <div class="scroll-panes" id="scroll-panes">
            <div class="clr"></div>
            <div class="res-msg" id="res-msg"></div>
            <div class="search-list cf" id="results"></div>
            <div class="clr"></div>
            <div class="loadmore" id="progressBar" style="display: none;">Loading......</div>
            <a class="loadmore" style="display:none;" onclick="load_search_data(); " id="load_more_button" href="javascript:void(0);">Load more</a>
        </div>
    </div>
</div>
<div class="blk">
    <div  id="leftMenu" class="leftMenu accordion inner">
        <dl >
            <dt data-color="red-bg" class=""><a href="http://www.indiatimes.com">Home</a></dt>
            <dd> </dd>
                            <dt  data-color="blue-bg" ><a href="http://www.indiatimes.com/news/">News</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/news/india/'>India</a>
                                     
                            <a href='http://www.indiatimes.com/news/world/'>World</a>
                                     
                            <a href='http://www.indiatimes.com/news/sports/'>Sports</a>
                                     
                            <a href='http://www.indiatimes.com/news/weird/'>Weird</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="purple-bg" ><a href="http://www.indiatimes.com/lifestyle/">Lifestyle</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/lifestyle/self/'>Self</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/style/'>Style</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                     
                            <a href='http://www.indiatimes.com/culture/who-we-are/'>Who we are</a>
                                     
                            <a href='http://www.indiatimes.com/culture/travel/'>Travel</a>
                                     
                            <a href='http://www.indiatimes.com/culture/food/'>Food</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="red-bg" ><a href="http://www.indiatimes.com/entertainment/">Entertainment</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/entertainment/bollywood/'>Bollywood</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/celebs/'>Celebscoop</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/hollywood/'>Hollywood</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="green-bg" ><a href="http://www.indiatimes.com/health/">Health</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                     
                            <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                     
                            <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                     
                            <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                     
                            <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="orange-bg" ><a href="http://www.indiatimes.com/videocafe/">Videos</a>
                                </dt>
                				<dt data-color="" class=""><a href="http://www.indiatimes.com/trending/">Trending</a>
                           </dt>
            <dt class="pink-bg">
            <div class="follow">Follow indiatimes </div>
            <div class="follow_cont"> 
                <a href="https://www.facebook.com/indiatimes" class="fbleft sprite"> facebook</a> 
                <a href="https://twitter.com/indiatimes" class="twtleft sprite"> twitter</a> 
            </div>
            </dt>

            <dd>&nbsp;</dd>
            <div class="clr"></div>
        </dl>
    </div>
</div>


<div class="clr"></div>
<div class="dummy-cont">&nbsp;</div>
<div class="clr"></div>


<div id="pushdown">
    <div class="pull-ad"><!--pull-ad start-->   
	
                <script type="text/javascript"> 
                    showHeaderCode();
                </script>
	    
</div><!--pull-ad end-->
</div>
    <!--testing 15-12-31 23:50:03-->  

    <section id="hp_block_1" class="container cf" style="border: 0px solid red;"><!--container start-->
        <div class="top-heading">
    <a href="http://www.indiatimes.com/culture/who-we-are/21-launches-100-million-forex-earned-an-indian-gps-on-its-way-here-s-8-reasons-isro-has-kept-kalam-s-legacy-alive-248953.html" class="big-card-top red"></a>
</div>

<div class="clr"></div>
<div class="big-image">
	<div class="gradient-b"></div>
	<a href="http://www.indiatimes.com/culture/who-we-are/21-launches-100-million-forex-earned-an-indian-gps-on-its-way-here-s-8-reasons-isro-has-kept-kalam-s-legacy-alive-248953.html" class="big-card-small">21 Launches, $100 Million Forex Earned & An Indian GPS On Its Way! Here's 8 Ways ISRO Kept Kalam's Legacy Alive In 2015!</a>
                <a href="http://www.indiatimes.com/culture/who-we-are/21-launches-100-million-forex-earned-an-indian-gps-on-its-way-here-s-8-reasons-isro-has-kept-kalam-s-legacy-alive-248953.html" class="tint"><img src="http://media.indiatimes.in/media/content/2015/Dec/for-cards_1451552478_1451552502_980x457.jpg"/></a>
</div>
<div class="clr"></div>
<div class="top-heading">
    
</div>

        

        <div class="news-panel cf"><!--news-panel start-->
            <h2>Big news</h2>

            <div class="feature-list cf"><!--feature-list start-->	
                                    <figure>
                        <div class="hash-tag">
                             13 hours ago                         </div>
						 						          
                        <a href="http://www.indiatimes.com/news/india/after-indian-students-us-now-deports-businessmen-livid-india-tells-us-to-honour-its-own-visas-248944.html" class=" tint" title="After Indian Students, US Now Deports Businessmen. Livid India Tells US To 'Honour Its Own Visas'">
                            <img  class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Dec/card_1451539555_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Dec/card_1451539555_236x111.jpg"  border="0" alt="After Indian Students, US Now Deports Businessmen. Livid India Tells US To 'Honour Its Own Visas'"/></a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/after-indian-students-us-now-deports-businessmen-livid-india-tells-us-to-honour-its-own-visas-248944.html" title="After Indian Students, US Now Deports Businessmen. Livid India Tells US To 'Honour Its Own Visas'">
                            After Indian Students, US Now Deports Businessmen. Livid India Tells US To 'Honour Its Own Visas'                        </a>
                    </figcaption>
                            </div><!--feature-list end-->

                            <div class="news-panel-list cf" id="column1_0"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            6 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/weird/angry-punjabi-man-bites-air-hostess-pilots-turn-flight-around-have-him-arrested-248978.html" title="Angry Punjabi Man Bites Air Hostess, Pilots Turn Flight Around, Have Him Arrested!" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Dec/5_1451561690_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Dec/5_1451561690_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/weird/angry-punjabi-man-bites-air-hostess-pilots-turn-flight-around-have-him-arrested-248978.html" title="Angry Punjabi Man Bites Air Hostess, Pilots Turn Flight Around, Have Him Arrested!">
                            Angry Punjabi Man Bites Air Hostess, Pilots Turn Flight Around, Have Him Arrested!                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_1"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            7 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/world/jackie-chan-s-gift-to-taiwan-museum-vandalized-and-defaced-with-red-paint-248976.html" title="Jackie Chan's Gift To Taiwan Museum Vandalized And Defaced With Red Paint!" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Dec/jc-card_1451560634_1451560644_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Dec/jc-card_1451560634_1451560644_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/jackie-chan-s-gift-to-taiwan-museum-vandalized-and-defaced-with-red-paint-248976.html" title="Jackie Chan's Gift To Taiwan Museum Vandalized And Defaced With Red Paint!">
                            Jackie Chan's Gift To Taiwan Museum Vandalized And Defaced With Red Paint!                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_2"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            7 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/india/someone-decided-to-make-a-list-of-2015-s-best-twitter-trolls-and-its-hilarious-248972.html" title="Someone Decided To Make A List Of 2015's Best Twitter Trolls And Its Hilarious" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Dec/502_1451558107_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Dec/502_1451558107_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/someone-decided-to-make-a-list-of-2015-s-best-twitter-trolls-and-its-hilarious-248972.html" title="Someone Decided To Make A List Of 2015's Best Twitter Trolls And Its Hilarious">
                            Someone Decided To Make A List Of 2015's Best Twitter Trolls And Its Hilarious                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_3"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            7 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/sports/ravichandran-ashwin-becomes-first-indian-in-42-years-to-end-the-year-as-no-1-test-bowler-248974.html" title="Ravichandran Ashwin Becomes First Indian In 42 Years To End The Year As No 1 Test Bowler" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Dec/ashwinaward2bccl_1451558886_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Dec/ashwinaward2bccl_1451558886_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/ravichandran-ashwin-becomes-first-indian-in-42-years-to-end-the-year-as-no-1-test-bowler-248974.html" title="Ravichandran Ashwin Becomes First Indian In 42 Years To End The Year As No 1 Test Bowler">
                            Ravichandran Ashwin Becomes First Indian In 42 Years To End The Year As No 1 Test Bowler                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                    </div><!--news-panel end-->

        <div class="life-panel cf" id="middleBlock1"><!--life-panel start-->
            <h2>lifestyle</h2>

                                <div class="life-panel-list cf"><!--life-panel-list start-->
                        <figure><span class="label-tag spon">Partner</span>
                            
                               
                            <a href="http://www.indiatimes.com/news/sports/8-things-only-an-indian-soccer-fan-will-understand-248897.html" class=" tint" title="" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/news/sports/8-things-only-an-indian-soccer-fan-will-understand-248897.html');">
                                <img  class="greyBg23 lazy23"  src="http://media.indiatimes.in/media/content/2015/Dec/card-image_1451463282_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Dec/card-image_1451463282_502x234.jpg"  border="0" alt="8 Things Only An Indian Soccer Fan Will Understand" class="img-responsive"/>
                            </a>
                        </figure>
                        <figcaption>
                            <a href="http://www.indiatimes.com/news/sports/8-things-only-an-indian-soccer-fan-will-understand-248897.html" title="8 Things Only An Indian Soccer Fan Will Understand" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/news/sports/8-things-only-an-indian-soccer-fan-will-understand-248897.html');">8 Things Only An Indian Soccer Fan Will Understand</a>
                        </figcaption>
                    </div><!--life-panel-list end-->
                                    <div class="life-panel-list cf"><!--life-panel-list start-->
                        <figure><span class="label-tag spon">Partner</span>
                            <a class='video-btn sprite' href='http://www.indiatimes.com/culture/who-we-are/this-video-might-make-you-rethink-your-new-year-eve-s-plans-248854.html'>video</a>
                               
                            <a href="http://www.indiatimes.com/culture/who-we-are/this-video-might-make-you-rethink-your-new-year-eve-s-plans-248854.html" class=" tint" title="" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/this-video-might-make-you-rethink-your-new-year-eve-s-plans-248854.html');">
                                <img  class="greyBg23 lazy23"  src="http://media.indiatimes.in/media/videocafe/2015/Dec/card_1451551983_1451551987_502x234.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Dec/card_1451551983_1451551987_502x234.jpg"  border="0" alt="This Video Might Make You Rethink Your New Year Eveâs Plans!" class="img-responsive"/>
                            </a>
                        </figure>
                        <figcaption>
                            <a href="http://www.indiatimes.com/culture/who-we-are/this-video-might-make-you-rethink-your-new-year-eve-s-plans-248854.html" title="This Video Might Make You Rethink Your New Year Eveâs Plans!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/this-video-might-make-you-rethink-your-new-year-eve-s-plans-248854.html');">This Video Might Make You Rethink Your New Year Eveâs Plans!</a>
                        </figcaption>
                    </div><!--life-panel-list end-->
                                    <div class="life-panel-list cf"><!--life-panel-list start-->
                        <figure><span class="label-tag spon">Partner</span>
                            
                               
                            <a href="http://www.indiatimes.com/culture/who-we-are/10-risky-adventure-sports-from-around-the-world-that-are-totally-worth-the-thrill-248840.html" class=" tint" title="" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/10-risky-adventure-sports-from-around-the-world-that-are-totally-worth-the-thrill-248840.html');">
                                <img  class="greyBg23 lazy23"  src="http://media.indiatimes.in/media/content/2015/Dec/rsz_3maxresdefault_1451485993_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Dec/rsz_3maxresdefault_1451485993_502x234.jpg"  border="0" alt="10 Risky Adventure Sports From Around The World That Should Be In Your 2016 Bucket List" class="img-responsive"/>
                            </a>
                        </figure>
                        <figcaption>
                            <a href="http://www.indiatimes.com/culture/who-we-are/10-risky-adventure-sports-from-around-the-world-that-are-totally-worth-the-thrill-248840.html" title="10 Risky Adventure Sports From Around The World That Should Be In Your 2016 Bucket List" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/10-risky-adventure-sports-from-around-the-world-that-are-totally-worth-the-thrill-248840.html');">10 Risky Adventure Sports From Around The World That Should Be In Your 2016 Bucket List</a>
                        </figcaption>
                    </div><!--life-panel-list end-->
                

                        
        </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_0">
                    <span class="strip skyblue-bg"></span>                    <figure>

                                                <a href="http://www.indiatimes.com/culture/who-we-are/an-open-letter-from-an-introvert-to-all-extroverts-229438.html" class="tint" title="An Open Letter From An Introvert To All Extroverts">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Dec/card_1451544150_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Dec/card_1451544150_218x102.jpg" border="0" alt="An Open Letter From An Introvert To All Extroverts"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/an-open-letter-from-an-introvert-to-all-extroverts-229438.html" title="An Open Letter From An Introvert To All Extroverts">
                            An Open Letter From An Introvert To All Extroverts                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_1">
                                        <figure>

                                                <a href="http://www.indiatimes.com/lifestyle/self/11-ways-to-bring-in-the-new-year-if-you-don-t-have-any-plans-248852.html" class="tint" title="11 Ways To Bring In The New Year If You Don't Have Any Plans">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Dec/card_1451541106_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Dec/card_1451541106_218x102.jpg" border="0" alt="11 Ways To Bring In The New Year If You Don't Have Any Plans"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/11-ways-to-bring-in-the-new-year-if-you-don-t-have-any-plans-248852.html" title="11 Ways To Bring In The New Year If You Don't Have Any Plans">
                            11 Ways To Bring In The New Year If You Don't Have Any Plans                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_2">
                                        <figure>

                                                <a href="http://www.indiatimes.com/entertainment/bollywood/these-10-minimal-posters-aptly-describe-the-best-films-of-the-year-that-has-gone-by-248954.html" class="tint" title="These 10 Minimal Posters Aptly Describe The Best Films Of The Year That Has Gone By">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Dec/card_1451548536_1451548543_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Dec/card_1451548536_1451548543_218x102.jpg" border="0" alt="These 10 Minimal Posters Aptly Describe The Best Films Of The Year That Has Gone By"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/these-10-minimal-posters-aptly-describe-the-best-films-of-the-year-that-has-gone-by-248954.html" title="These 10 Minimal Posters Aptly Describe The Best Films Of The Year That Has Gone By">
                            These 10 Minimal Posters Aptly Describe The Best Films Of The Year That Has Gone By                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_3">
                                        <figure>

                                                <a href="http://www.indiatimes.com/entertainment/celebs/shah-rukh-khan-kajol-taking-a-case-of-their-hater-is-so-lame-that-it-s-actually-cool-248914.html" class="tint" title="Shah Rukh Khan-Kajol Taking A Case Of Their Hater Is So Lame That It's Actually Cool">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Dec/card1_1451457592_1451457602_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Dec/card1_1451457592_1451457602_218x102.jpg" border="0" alt="Shah Rukh Khan-Kajol Taking A Case Of Their Hater Is So Lame That It's Actually Cool"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/shah-rukh-khan-kajol-taking-a-case-of-their-hater-is-so-lame-that-it-s-actually-cool-248914.html" title="Shah Rukh Khan-Kajol Taking A Case Of Their Hater Is So Lame That It's Actually Cool">
                            Shah Rukh Khan-Kajol Taking A Case Of Their Hater Is So Lame That It's Actually Cool                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_4">
                                        <figure>

                                                <a href="http://www.indiatimes.com/health/buzz/eating-dinner-late-at-night-not-only-causes-weight-gain-but-also-affects-the-brain-248894.html" class="tint" title="Eating Dinner Late At Night Not Only Causes Weight Gain But Also Affects The Brain!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Dec/cover_1451388462_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Dec/cover_1451388462_218x102.jpg" border="0" alt="Eating Dinner Late At Night Not Only Causes Weight Gain But Also Affects The Brain!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/buzz/eating-dinner-late-at-night-not-only-causes-weight-gain-but-also-affects-the-brain-248894.html" title="Eating Dinner Late At Night Not Only Causes Weight Gain But Also Affects The Brain!">
                            Eating Dinner Late At Night Not Only Causes Weight Gain But Also Affects The Brain!                        </a>
                    </figcaption>
                </div>
                        </div><!--trending-panel end-->
    </section><!--container end-->

<!-- -------------------    end Block 1  ------------------------------------- -->
<section class="big-ads" id="ad1"> 
<div id="bigAd1_slot"></div>
    <script>        
        showBigAD1('bigAd1_slot');
    </script> 
</section>
<!-- -------------------    start Block 2  ------------------------------------- -->

    <section id="hp_block_2" class="container cf" style="border: 0px solid red;"><!--container start-->
        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
                            <div class="news-panel-list cf" id="column1_4"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            8 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/mumbai-police-stoops-to-a-new-low-force-minor-rape-victim-to-marry-her-rapist-248969.html" title="Mumbai Police Stoops To A New Low, Force Minor Rape Victim To Marry Her Rapist" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1451556960_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/mumbai-police-stoops-to-a-new-low-force-minor-rape-victim-to-marry-her-rapist-248969.html" title="Mumbai Police Stoops To A New Low, Force Minor Rape Victim To Marry Her Rapist">
                            Mumbai Police Stoops To A New Low, Force Minor Rape Victim To Marry Her Rapist                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_5"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            8 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/sports/virat-kohli-named-bcci-cricketer-of-the-year-mithali-raj-gets-top-women-s-player-award-248966.html" title="Virat Kohli Named BCCI Cricketer Of The Year, Mithali Raj Gets Top Women's Player Award" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/kohlimithali_1451555528_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/sports/virat-kohli-named-bcci-cricketer-of-the-year-mithali-raj-gets-top-women-s-player-award-248966.html" title="Virat Kohli Named BCCI Cricketer Of The Year, Mithali Raj Gets Top Women's Player Award">
                            Virat Kohli Named BCCI Cricketer Of The Year, Mithali Raj Gets Top Women's Player Award                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_6"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            8 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/world/users-are-now-slamming-5-millon-lawsuit-on-apple-for-ios-9-s-poor-performance-on-iphone-4s_-248964.html" title="Users Are Now Slamming $5 Millon Lawsuit On Apple For iOS 9's Poor Performance On iPhone 4s" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/502_1451554844_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/world/users-are-now-slamming-5-millon-lawsuit-on-apple-for-ios-9-s-poor-performance-on-iphone-4s_-248964.html" title="Users Are Now Slamming $5 Millon Lawsuit On Apple For iOS 9's Poor Performance On iPhone 4s">
                            Users Are Now Slamming $5 Millon Lawsuit On Apple For iOS 9's Poor Performance On iPhone 4s                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_7"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            9 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/here-are-the-names-of-18-of-india-s-biggest-tax-defaulters-248959.html" title="Here Are The Names Of 18 Of India's Biggest Tax Defaulters" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/rtr25etx5_1451554929_1451554936_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/here-are-the-names-of-18-of-india-s-biggest-tax-defaulters-248959.html" title="Here Are The Names Of 18 Of India's Biggest Tax Defaulters">
                            Here Are The Names Of 18 Of India's Biggest Tax Defaulters                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_8"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            9 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/here-s-how-mumbai-s-dabbawalas-plan-to-deliver-your-leftover-food-to-the-needy-248958.html" title="Here's How Mumbai's Dabbawalas Plan To Deliver Your Leftover Food To The Needy" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/dabba5_1451550339_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/here-s-how-mumbai-s-dabbawalas-plan-to-deliver-your-leftover-food-to-the-needy-248958.html" title="Here's How Mumbai's Dabbawalas Plan To Deliver Your Leftover Food To The Needy">
                            Here's How Mumbai's Dabbawalas Plan To Deliver Your Leftover Food To The Needy                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/healthyliving/14-healthy-indian-snacks-you-can-carry-around-to-munch-on-when-you-get-hungry-248931.html" class="tint" title="14 Healthy Indian Snacks You Can Carry Around To Munch On When You Get Hungry!">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/cover1_1451479842_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/14-healthy-indian-snacks-you-can-carry-around-to-munch-on-when-you-get-hungry-248931.html" title="14 Healthy Indian Snacks You Can Carry Around To Munch On When You Get Hungry!">
                            14 Healthy Indian Snacks You Can Carry Around To Munch On When You Get Hungry!                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/lifestyle/self/new-year-s-eve-party-expectations-vs-reality-248901.html" class="tint" title="New Year's Eve Party: Expectations Vs Reality">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/virat_1451391849_1451391875_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/new-year-s-eve-party-expectations-vs-reality-248901.html" title="New Year's Eve Party: Expectations Vs Reality">
                            New Year's Eve Party: Expectations Vs Reality                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/naseeruddin-shah-voices-the-frustrations-of-every-delhiite-who-is-dreading-the-evenodd-rule-in-this-spoof-248975.html'>video</a>                          
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/videocafe/naseeruddin-shah-voices-the-frustrations-of-every-delhiite-who-is-dreading-the-evenodd-rule-in-this-spoof-248975.html" class="tint" title="Naseeruddin Shah Voices The Frustrations Of Every Delhiite Who Is Dreading The #EvenOdd Rule In This Spoof!">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/awednesday_card_1451560179_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/naseeruddin-shah-voices-the-frustrations-of-every-delhiite-who-is-dreading-the-evenodd-rule-in-this-spoof-248975.html" title="Naseeruddin Shah Voices The Frustrations Of Every Delhiite Who Is Dreading The #EvenOdd Rule In This Spoof!">
                            Naseeruddin Shah Voices The Frustrations Of Every Delhiite Who Is Dreading The #EvenOdd Rule In This Spoof!                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_5"><!--trending-panel-list start-->
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/any-change-in-mindset-begins-at-home-and-this-thought-provoking-tvc-shows-you-exactly-how-248922.html'>video</a>
                        <a href="http://www.indiatimes.com/videocafe/any-change-in-mindset-begins-at-home-and-this-thought-provoking-tvc-shows-you-exactly-how-248922.html" class="tint" title="Any Change In Mindset Begins At Home, And This Thought-Provoking TVC Shows You Exactly How">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/changetheconvention_card_1451463712_218x102.jpg" border="0" alt="Any Change In Mindset Begins At Home, And This Thought-Provoking TVC Shows You Exactly How"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/any-change-in-mindset-begins-at-home-and-this-thought-provoking-tvc-shows-you-exactly-how-248922.html" title="Any Change In Mindset Begins At Home, And This Thought-Provoking TVC Shows You Exactly How">
                            Any Change In Mindset Begins At Home, And This Thought-Provoking TVC Shows You Exactly How                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_6"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/celebs/15-reasons-why-deepika-padukone-should-be-crowned-the-queen-of-2015_-248923.html" class="tint" title="15 Reasons Why Deepika Padukone Was Bollywood's One True Superstar In 2015!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/deepika-card1_1451479185_1451479197_218x102.jpg" border="0" alt="15 Reasons Why Deepika Padukone Was Bollywood's One True Superstar In 2015!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/15-reasons-why-deepika-padukone-should-be-crowned-the-queen-of-2015_-248923.html" title="15 Reasons Why Deepika Padukone Was Bollywood's One True Superstar In 2015!">
                            15 Reasons Why Deepika Padukone Was Bollywood's One True Superstar In 2015!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_7"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/videocafe/from-cold-winds-to-warm-smiles-this-travel-video-will-make-you-fall-in-love-with-sikkim-and-it-s-people-248919.html" class="tint" title="From Cold Winds To Warm Smiles, This Travel Video Will Make You Fall In Love With Sikkim And It's People!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/sikkim_card1_1451462299_218x102.jpg" border="0" alt="From Cold Winds To Warm Smiles, This Travel Video Will Make You Fall In Love With Sikkim And It's People!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/from-cold-winds-to-warm-smiles-this-travel-video-will-make-you-fall-in-love-with-sikkim-and-it-s-people-248919.html" title="From Cold Winds To Warm Smiles, This Travel Video Will Make You Fall In Love With Sikkim And It's People!">
                            From Cold Winds To Warm Smiles, This Travel Video Will Make You Fall In Love With Sikkim And It's People!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_8"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/culture/who-we-are/10-risky-adventure-sports-from-around-the-world-that-are-totally-worth-the-thrill-248840.html" class="tint" title="10 Risky Adventure Sports From Around The World That Should Be In Your 2016 Bucket List">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/rsz_3maxresdefault_1451485993_218x102.jpg" border="0" alt="10 Risky Adventure Sports From Around The World That Should Be In Your 2016 Bucket List"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/10-risky-adventure-sports-from-around-the-world-that-are-totally-worth-the-thrill-248840.html" title="10 Risky Adventure Sports From Around The World That Should Be In Your 2016 Bucket List">
                            10 Risky Adventure Sports From Around The World That Should Be In Your 2016 Bucket List                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_9"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/culture/who-we-are/10-risky-adventure-sports-from-around-the-world-that-are-totally-worth-the-thrill-248840.html" class="tint" title="10 Risky Adventure Sports From Around The World That Should Be In Your 2016 Bucket List">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/rsz_3maxresdefault_1451485993_218x102.jpg" border="0" alt="10 Risky Adventure Sports From Around The World That Should Be In Your 2016 Bucket List"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/10-risky-adventure-sports-from-around-the-world-that-are-totally-worth-the-thrill-248840.html" title="10 Risky Adventure Sports From Around The World That Should Be In Your 2016 Bucket List">
                            10 Risky Adventure Sports From Around The World That Should Be In Your 2016 Bucket List                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div>

    </section><!--container end-->

<!--------------------------- end Block2 ------------------------------------------>
<section class="big-ads" id="ad2"> 
<div id="bigAd2_slot"></div>
</section>
<!--------------------------- start Block3 ------------------------------------------>

    <section id="hp_block_3" class="container cf"><!--container start-->

        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
          
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_9">
                    <figure>
                        <div class="hash-tag">
                            10 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/fatima-khatoon-who-got-trafficked-at-9-in-name-of-marriage-receives-a-death-threat-for-fighting-human-trafficking-248951.html" title="Fatima Khatoon, Who Got Trafficked At 9 In Name Of Marriage Receives A Death Threat For Fighting Human Trafficking!" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/fatima-khatoon-card_1451545712_1451545719_236x111.jpg" border="0" alt="Fatima Khatoon, Who Got Trafficked At 9 In Name Of Marriage Receives A Death Threat For Fighting Human Trafficking!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/fatima-khatoon-who-got-trafficked-at-9-in-name-of-marriage-receives-a-death-threat-for-fighting-human-trafficking-248951.html" title="Fatima Khatoon, Who Got Trafficked At 9 In Name Of Marriage Receives A Death Threat For Fighting Human Trafficking!">
                            Fatima Khatoon, Who Got Trafficked At 9 In Name Of Marriage Receives A Death Threat For Fighting Human Trafficking!                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_10">
                    <figure>
                        <div class="hash-tag">
                            10 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/world/these-15-spectacular-images-of-earth-s-surface-from-outer-space-will-leave-you-spellbound-248947.html" title="These 15 Spectacular Images Of Earth's Surface From Outer Space Will Leave You Spellbound!" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1451547263_236x111.jpg" border="0" alt="These 15 Spectacular Images Of Earth's Surface From Outer Space Will Leave You Spellbound!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/these-15-spectacular-images-of-earth-s-surface-from-outer-space-will-leave-you-spellbound-248947.html" title="These 15 Spectacular Images Of Earth's Surface From Outer Space Will Leave You Spellbound!">
                            These 15 Spectacular Images Of Earth's Surface From Outer Space Will Leave You Spellbound!                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_11">
                    <figure>
                        <div class="hash-tag">
                            10 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/not-feeling-the-winter-chill-yet-even-the-north-pole-is-20-degrees-above-normal-temperature-248957.html" title="Not Feeling The Winter Chill Yet? Even The North Pole Is 20 Degrees Above Normal Temperature" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1451550401_236x111.jpg" border="0" alt="Not Feeling The Winter Chill Yet? Even The North Pole Is 20 Degrees Above Normal Temperature"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/not-feeling-the-winter-chill-yet-even-the-north-pole-is-20-degrees-above-normal-temperature-248957.html" title="Not Feeling The Winter Chill Yet? Even The North Pole Is 20 Degrees Above Normal Temperature">
                            Not Feeling The Winter Chill Yet? Even The North Pole Is 20 Degrees Above Normal Temperature                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_12">
                    <figure>
                        <div class="hash-tag">
                            11 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/13-year-old-boy-to-get-national-bravery-award-for-saving-his-mother-from-a-burglar-248950.html" title="13-Year-Old Boy To Get National Bravery Award For Saving His Mother From A Burglar" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/family502_1451549511_236x111.jpg" border="0" alt="13-Year-Old Boy To Get National Bravery Award For Saving His Mother From A Burglar"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/13-year-old-boy-to-get-national-bravery-award-for-saving-his-mother-from-a-burglar-248950.html" title="13-Year-Old Boy To Get National Bravery Award For Saving His Mother From A Burglar">
                            13-Year-Old Boy To Get National Bravery Award For Saving His Mother From A Burglar                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_13">
                    <figure>
                        <div class="hash-tag">
                            11 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/if-you-re-caught-violating-oddeven-rule-you-won-t-even-get-space-to-park-in-delhi-says-aap-248952.html" title="If You're Caught Violating #OddEven Rule, You Won't Even Get Space To Park In Delhi, Says AAP" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1451549713_236x111.jpg" border="0" alt="If You're Caught Violating #OddEven Rule, You Won't Even Get Space To Park In Delhi, Says AAP"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/if-you-re-caught-violating-oddeven-rule-you-won-t-even-get-space-to-park-in-delhi-says-aap-248952.html" title="If You're Caught Violating #OddEven Rule, You Won't Even Get Space To Park In Delhi, Says AAP">
                            If You're Caught Violating #OddEven Rule, You Won't Even Get Space To Park In Delhi, Says AAP                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                     
                <div class="life-panel-list cf">
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-deaf-man-s-reaction-on-a-becoming-father-is-the-best-thing-you-ll-watch-today-248955.html'>video</a>                          
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/videocafe/this-deaf-man-s-reaction-on-a-becoming-father-is-the-best-thing-you-ll-watch-today-248955.html" class="tint" title="This Deaf Man's Reaction On Becoming A Father Is The Best Thing You'll Watch Today!">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/deafhusband_card_1451548750_502x234.jpg" border="0" alt="http://www.indiatimes.com/videocafe/this-deaf-man-s-reaction-on-a-becoming-father-is-the-best-thing-you-ll-watch-today-248955.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/this-deaf-man-s-reaction-on-a-becoming-father-is-the-best-thing-you-ll-watch-today-248955.html" title="This Deaf Man's Reaction On Becoming A Father Is The Best Thing You'll Watch Today!">
                            This Deaf Man's Reaction On Becoming A Father Is The Best Thing You'll Watch Today!                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/celebs/here-s-why-pooja-bedi-s-daughter-aaliya-vinod-khanna-s-son-sakshi-are-winning-the-internet-248967.html" class="tint" title="Here's Why Pooja Bedi's Daughter Aaliya & Vinod Khanna's Son Sakshi Are Winning The Internet">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/kids-card_1451556260_1451556267_502x234.jpg" border="0" alt="http://www.indiatimes.com/entertainment/celebs/here-s-why-pooja-bedi-s-daughter-aaliya-vinod-khanna-s-son-sakshi-are-winning-the-internet-248967.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/here-s-why-pooja-bedi-s-daughter-aaliya-vinod-khanna-s-son-sakshi-are-winning-the-internet-248967.html" title="Here's Why Pooja Bedi's Daughter Aaliya & Vinod Khanna's Son Sakshi Are Winning The Internet">
                            Here's Why Pooja Bedi's Daughter Aaliya & Vinod Khanna's Son Sakshi Are Winning The Internet                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/health/tips-tricks/check-out-these-7-amazing-household-hacks-using-salt-248935.html'>video</a>                          
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/tips-tricks/check-out-these-7-amazing-household-hacks-using-salt-248935.html" class="tint" title="Check Out These 7 Amazing Household Hacks Using Salt!">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/cover_1451476139_502x234.jpg" border="0" alt="http://www.indiatimes.com/health/tips-tricks/check-out-these-7-amazing-household-hacks-using-salt-248935.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/tips-tricks/check-out-these-7-amazing-household-hacks-using-salt-248935.html" title="Check Out These 7 Amazing Household Hacks Using Salt!">
                            Check Out These 7 Amazing Household Hacks Using Salt!                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_10"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/lifestyle/self/14-lessons-you-wish-you-could-give-your-younger-self-this-new-year-248849.html" class="tint" title="14 Lessons You Wish You Could Give Your Younger Self This New Year">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/ddd1card2_1451302509_1451302517_218x102.jpg" border="0" alt="14 Lessons You Wish You Could Give Your Younger Self This New Year"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/14-lessons-you-wish-you-could-give-your-younger-self-this-new-year-248849.html" title="14 Lessons You Wish You Could Give Your Younger Self This New Year">
                            14 Lessons You Wish You Could Give Your Younger Self This New Year                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_11"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/lifestyle/self/gemini-this-is-your-complete-horoscope-for-2016_-248861.html" class="tint" title="Gemini! This Is Your Complete Horoscope For 2016">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/cp_1451374279_218x102.jpg" border="0" alt="Gemini! This Is Your Complete Horoscope For 2016"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/gemini-this-is-your-complete-horoscope-for-2016_-248861.html" title="Gemini! This Is Your Complete Horoscope For 2016">
                            Gemini! This Is Your Complete Horoscope For 2016                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_12"><!--trending-panel-list start-->
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/culture/who-we-are/this-video-might-make-you-rethink-your-new-year-eve-s-plans-248854.html'>video</a>		

                        <a href="http://www.indiatimes.com/culture/who-we-are/this-video-might-make-you-rethink-your-new-year-eve-s-plans-248854.html" class="tint" title="This Video Might Make You Rethink Your New Year Eveâs Plans!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/card_1451551983_1451551987_218x102.jpg" border="0" alt="This Video Might Make You Rethink Your New Year Eveâs Plans!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/this-video-might-make-you-rethink-your-new-year-eve-s-plans-248854.html" title="This Video Might Make You Rethink Your New Year Eveâs Plans!">
                            This Video Might Make You Rethink Your New Year Eveâs Plans!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_13"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/celebs/12-times-nawazuddin-siddiqui-said-something-no-one-else-had-the-balls-for-248877.html" class="tint" title="12 Times Nawazuddin Siddiqui Said Something No One Else Had The Balls For!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/maxresdefault_1451456098_1451456106_218x102.jpg" border="0" alt="12 Times Nawazuddin Siddiqui Said Something No One Else Had The Balls For!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/12-times-nawazuddin-siddiqui-said-something-no-one-else-had-the-balls-for-248877.html" title="12 Times Nawazuddin Siddiqui Said Something No One Else Had The Balls For!">
                            12 Times Nawazuddin Siddiqui Said Something No One Else Had The Balls For!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_14"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/lifestyle/self/16-relationship-resolutions-we-should-all-make-this-year-248754.html" class="tint" title="16 Relationship Resolutions We Should All Make This Year">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/1_1450958951_218x102.jpg" border="0" alt="16 Relationship Resolutions We Should All Make This Year"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/16-relationship-resolutions-we-should-all-make-this-year-248754.html" title="16 Relationship Resolutions We Should All Make This Year">
                            16 Relationship Resolutions We Should All Make This Year                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div><!--trending-panel end-->

    </section><!--container end-->

<!------------------------------- end Block 3 ---------------------------------------->
<section class="big-ads">
<div id="bigAd3_slot"></div>
</section>

<!------------------------------- start Block4  -------------------------------------->

    <section class="container cf" id="container4"><!--container start-->
        <div class="news-panel cf ">
            <h2>news</h2>

                            <div class="news-panel-list cf" id="column1_14">
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/did-you-know-sonia-gandhi-s-house-is-bigger-than-narendra-modi-s-7-rcr-home-248948.html" title="Did You Know Sonia Gandhi's House Is Bigger Than Narendra Modi's 7 RCR Home?" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/main5_1451543931_236x111.jpg" border="0" alt="Did You Know Sonia Gandhi's House Is Bigger Than Narendra Modi's 7 RCR Home?"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/did-you-know-sonia-gandhi-s-house-is-bigger-than-narendra-modi-s-7-rcr-home-248948.html" title="Did You Know Sonia Gandhi's House Is Bigger Than Narendra Modi's 7 RCR Home?">
                            Did You Know Sonia Gandhi's House Is Bigger Than Narendra Modi's 7 RCR Home?                        </a>
                    </figcaption> 
                </div>
                <div class='container1'>                <div class="news-panel-list cf" id="column1_15">
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/this-russian-man-got-drunk-died-came-back-alive-only-to-start-drinking-once-again-248942.html" title="This Russian Man Got Drunk, Died & Came Back Alive Only To Start Drinking Once Again!" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/drunk5_1451539062_236x111.jpg" border="0" alt="This Russian Man Got Drunk, Died & Came Back Alive Only To Start Drinking Once Again!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/this-russian-man-got-drunk-died-came-back-alive-only-to-start-drinking-once-again-248942.html" title="This Russian Man Got Drunk, Died & Came Back Alive Only To Start Drinking Once Again!">
                            This Russian Man Got Drunk, Died & Came Back Alive Only To Start Drinking Once Again!                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_16">
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/meet-sir-harpal-singh-kumar-the-indian-origin-cancer-researcher-who-just-got-knighted-in-uk-248946.html" title="Meet Sir Harpal Singh Kumar, The Indian-Origin Cancer Researcher Who Just Got Knighted In UK" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1451542144_236x111.jpg" border="0" alt="Meet Sir Harpal Singh Kumar, The Indian-Origin Cancer Researcher Who Just Got Knighted In UK"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/meet-sir-harpal-singh-kumar-the-indian-origin-cancer-researcher-who-just-got-knighted-in-uk-248946.html" title="Meet Sir Harpal Singh Kumar, The Indian-Origin Cancer Researcher Who Just Got Knighted In UK">
                            Meet Sir Harpal Singh Kumar, The Indian-Origin Cancer Researcher Who Just Got Knighted In UK                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_17">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/ramdev-s-troubles-continue-now-muslim-organisation-issues-fatwa-against-patanjali-products-248940.html" title="Ramdev's Troubles Continue, Now Muslim Organisation Issues Fatwa Against Patanjali Products!" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/patanjali---card_1451480652_236x111.jpg" border="0" alt="Ramdev's Troubles Continue, Now Muslim Organisation Issues Fatwa Against Patanjali Products!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/ramdev-s-troubles-continue-now-muslim-organisation-issues-fatwa-against-patanjali-products-248940.html" title="Ramdev's Troubles Continue, Now Muslim Organisation Issues Fatwa Against Patanjali Products!">
                            Ramdev's Troubles Continue, Now Muslim Organisation Issues Fatwa Against Patanjali Products!                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_18">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/us-sets-eyes-on-colonising-mars-nasa-gets-55-million-funding-to-send-humans-to-the-red-planet-248938.html" title="US Sets Eyes On Colonising Mars, NASA Gets $55 Million Funding To Send Humans To The Red Planet" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1451479109_236x111.jpg" border="0" alt="US Sets Eyes On Colonising Mars, NASA Gets $55 Million Funding To Send Humans To The Red Planet"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/us-sets-eyes-on-colonising-mars-nasa-gets-55-million-funding-to-send-humans-to-the-red-planet-248938.html" title="US Sets Eyes On Colonising Mars, NASA Gets $55 Million Funding To Send Humans To The Red Planet">
                            US Sets Eyes On Colonising Mars, NASA Gets $55 Million Funding To Send Humans To The Red Planet                        </a>
                    </figcaption> 
                </div>
                 
        </div>
    </div><!--news-panel end-->

    <div class="life-panel cf container2" id="b4c2"><!--life-panel start-->
        <h2>lifestyle</h2>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/farhan-akhtar-amitabh-bachchan-singing-atrangi-yaari-is-a-grand-treat-for-your-eyes-and-ears-248962.html" class="tint" title="Farhan Akhtar & Amitabh Bachchan Singing 'Atrangi Yaari' Is A Grand Treat For Your Eyes And Ears!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/wazir_1451554743_1451554760_502x234.jpg" border="0" alt="Farhan Akhtar & Amitabh Bachchan Singing 'Atrangi Yaari' Is A Grand Treat For Your Eyes And Ears!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/farhan-akhtar-amitabh-bachchan-singing-atrangi-yaari-is-a-grand-treat-for-your-eyes-and-ears-248962.html" title="Farhan Akhtar & Amitabh Bachchan Singing 'Atrangi Yaari' Is A Grand Treat For Your Eyes And Ears!">
                        Farhan Akhtar & Amitabh Bachchan Singing 'Atrangi Yaari' Is A Grand Treat For Your Eyes And Ears!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/these-10-minimal-posters-aptly-describe-the-best-films-of-the-year-that-has-gone-by-248954.html" class="tint" title="These 10 Minimal Posters Aptly Describe The Best Films Of The Year That Has Gone By">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1451548536_1451548543_502x234.jpg" border="0" alt="These 10 Minimal Posters Aptly Describe The Best Films Of The Year That Has Gone By" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/these-10-minimal-posters-aptly-describe-the-best-films-of-the-year-that-has-gone-by-248954.html" title="These 10 Minimal Posters Aptly Describe The Best Films Of The Year That Has Gone By">
                        These 10 Minimal Posters Aptly Describe The Best Films Of The Year That Has Gone By                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/an-open-letter-from-an-introvert-to-all-extroverts-229438.html" class="tint" title="An Open Letter From An Introvert To All Extroverts">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1451544150_502x234.jpg" border="0" alt="An Open Letter From An Introvert To All Extroverts" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/an-open-letter-from-an-introvert-to-all-extroverts-229438.html" title="An Open Letter From An Introvert To All Extroverts">
                        An Open Letter From An Introvert To All Extroverts                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/15-bollywood-films-that-left-us-disappointed-in-2015_-248949.html" class="tint" title="15 Bollywood Films That Left Us Disappointed In 2015">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1451546343_1451546350_502x234.jpg" border="0" alt="15 Bollywood Films That Left Us Disappointed In 2015" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/15-bollywood-films-that-left-us-disappointed-in-2015_-248949.html" title="15 Bollywood Films That Left Us Disappointed In 2015">
                        15 Bollywood Films That Left Us Disappointed In 2015                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/11-work-resolutions-to-ace-your-career-goals-in-2016_-248920.html" class="tint" title="11 Work Resolutions To Ace Your Career Goals In 2016">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1451541631_502x234.jpg" border="0" alt="11 Work Resolutions To Ace Your Career Goals In 2016" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/11-work-resolutions-to-ace-your-career-goals-in-2016_-248920.html" title="11 Work Resolutions To Ace Your Career Goals In 2016">
                        11 Work Resolutions To Ace Your Career Goals In 2016                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/ebola-on-the-decline-after-2-years-and-over-2500-deaths-guinea-is-finally-declared-ebola-free-248925.html" class="tint" title="Ebola On The Decline. After 2 Years And Over 2500 Deaths, Guinea Is Finally Declared Ebola-Free!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/cover_1451468325_502x234.jpg" border="0" alt="Ebola On The Decline. After 2 Years And Over 2500 Deaths, Guinea Is Finally Declared Ebola-Free!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/ebola-on-the-decline-after-2-years-and-over-2500-deaths-guinea-is-finally-declared-ebola-free-248925.html" title="Ebola On The Decline. After 2 Years And Over 2500 Deaths, Guinea Is Finally Declared Ebola-Free!">
                        Ebola On The Decline. After 2 Years And Over 2500 Deaths, Guinea Is Finally Declared Ebola-Free!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/a-sanskaari-pahlaj-nihalani-lands-in-a-soup-for-passing-not-so-sanskaari-trailers-248945.html" class="tint" title="'Sanskaari' Pahlaj Nihalani Lands In A Soup For Passing 'Not So Sanskaari' Trailers!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/censor-board_1451541632_1451541642_502x234.jpg" border="0" alt="'Sanskaari' Pahlaj Nihalani Lands In A Soup For Passing 'Not So Sanskaari' Trailers!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/a-sanskaari-pahlaj-nihalani-lands-in-a-soup-for-passing-not-so-sanskaari-trailers-248945.html" title="'Sanskaari' Pahlaj Nihalani Lands In A Soup For Passing 'Not So Sanskaari' Trailers!">
                        'Sanskaari' Pahlaj Nihalani Lands In A Soup For Passing 'Not So Sanskaari' Trailers!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/cancer-this-is-your-complete-horoscope-for-the-year-2016_-248879.html" class="tint" title="Cancer! This Is Your Complete Horoscope For The Year 2016">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/cp_1451377580_502x234.jpg" border="0" alt="Cancer! This Is Your Complete Horoscope For The Year 2016" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/cancer-this-is-your-complete-horoscope-for-the-year-2016_-248879.html" title="Cancer! This Is Your Complete Horoscope For The Year 2016">
                        Cancer! This Is Your Complete Horoscope For The Year 2016                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/this-little-girl-from-a-bombay-slum-tells-us-why-we-shouldn-t-judge-a-book-by-its-cover-248943.html" class="tint" title="This Little Girl From A Bombay Slum Tells Us Why We Shouldn't Judge A Book By Its Cover!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1451539789_502x234.jpg" border="0" alt="This Little Girl From A Bombay Slum Tells Us Why We Shouldn't Judge A Book By Its Cover!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/this-little-girl-from-a-bombay-slum-tells-us-why-we-shouldn-t-judge-a-book-by-its-cover-248943.html" title="This Little Girl From A Bombay Slum Tells Us Why We Shouldn't Judge A Book By Its Cover!">
                        This Little Girl From A Bombay Slum Tells Us Why We Shouldn't Judge A Book By Its Cover!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/this-10-minute-detox-yoga-sequence-will-cure-that-hangover-thankuslater-248937.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/this-10-minute-detox-yoga-sequence-will-cure-that-hangover-thankuslater-248937.html" class="tint" title="This 10 Minute Detox Yoga Sequence Will Cure That Hangover! #ThankUsLater">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/cover_1451478487_502x234.jpg" border="0" alt="This 10 Minute Detox Yoga Sequence Will Cure That Hangover! #ThankUsLater" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/this-10-minute-detox-yoga-sequence-will-cure-that-hangover-thankuslater-248937.html" title="This 10 Minute Detox Yoga Sequence Will Cure That Hangover! #ThankUsLater">
                        This 10 Minute Detox Yoga Sequence Will Cure That Hangover! #ThankUsLater                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/11-ways-to-bring-in-the-new-year-if-you-don-t-have-any-plans-248852.html" class="tint" title="11 Ways To Bring In The New Year If You Don't Have Any Plans">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1451541106_502x234.jpg" border="0" alt="11 Ways To Bring In The New Year If You Don't Have Any Plans" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/11-ways-to-bring-in-the-new-year-if-you-don-t-have-any-plans-248852.html" title="11 Ways To Bring In The New Year If You Don't Have Any Plans">
                        11 Ways To Bring In The New Year If You Don't Have Any Plans                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/eating-dinner-late-at-night-not-only-causes-weight-gain-but-also-affects-the-brain-248894.html" class="tint" title="Eating Dinner Late At Night Not Only Causes Weight Gain But Also Affects The Brain!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/cover_1451388462_502x234.jpg" border="0" alt="Eating Dinner Late At Night Not Only Causes Weight Gain But Also Affects The Brain!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/eating-dinner-late-at-night-not-only-causes-weight-gain-but-also-affects-the-brain-248894.html" title="Eating Dinner Late At Night Not Only Causes Weight Gain But Also Affects The Brain!">
                        Eating Dinner Late At Night Not Only Causes Weight Gain But Also Affects The Brain!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/meet-the-real-heroes-of-chennai-who-helped-the-flood-ravaged-city-get-back-to-normalcy-iamchennai-248936.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/meet-the-real-heroes-of-chennai-who-helped-the-flood-ravaged-city-get-back-to-normalcy-iamchennai-248936.html" class="tint" title="Meet The Real Heroes Of Chennai Who Helped The Flood-Ravaged City Get Back To Normalcy! #IAmChennai">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/iamchennai_card_1451477542_502x234.jpg" border="0" alt="Meet The Real Heroes Of Chennai Who Helped The Flood-Ravaged City Get Back To Normalcy! #IAmChennai" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/meet-the-real-heroes-of-chennai-who-helped-the-flood-ravaged-city-get-back-to-normalcy-iamchennai-248936.html" title="Meet The Real Heroes Of Chennai Who Helped The Flood-Ravaged City Get Back To Normalcy! #IAmChennai">
                        Meet The Real Heroes Of Chennai Who Helped The Flood-Ravaged City Get Back To Normalcy! #IAmChennai                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/ranveer-singh-s-candid-confessions-about-deepika-endorsing-condoms-growing-up-will-make-you-want-to-give-him-a-hug-248933.html" class="tint" title="Ranveer Singh's Candid Confessions About Deepika, Endorsing Condoms & Growing Up Will Make You Want To Give Him A Hug!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/ranveer-singh_1451476403_1451476407_502x234.jpg" border="0" alt="Ranveer Singh's Candid Confessions About Deepika, Endorsing Condoms & Growing Up Will Make You Want To Give Him A Hug!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/ranveer-singh-s-candid-confessions-about-deepika-endorsing-condoms-growing-up-will-make-you-want-to-give-him-a-hug-248933.html" title="Ranveer Singh's Candid Confessions About Deepika, Endorsing Condoms & Growing Up Will Make You Want To Give Him A Hug!">
                        Ranveer Singh's Candid Confessions About Deepika, Endorsing Condoms & Growing Up Will Make You Want To Give Him A Hug!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/15-times-people-brought-some-joy-to-the-lives-of-syrian-immigrants-248903.html" class="tint" title="15 Times People Brought Some Joy To The Lives Of Syrian Immigrants">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/2_1451392532_1451392537_502x234.jpg" border="0" alt="15 Times People Brought Some Joy To The Lives Of Syrian Immigrants" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/15-times-people-brought-some-joy-to-the-lives-of-syrian-immigrants-248903.html" title="15 Times People Brought Some Joy To The Lives Of Syrian Immigrants">
                        15 Times People Brought Some Joy To The Lives Of Syrian Immigrants                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/15-reasons-why-deepika-padukone-should-be-crowned-the-queen-of-2015_-248923.html" class="tint" title="15 Reasons Why Deepika Padukone Was Bollywood's One True Superstar In 2015!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/deepika-card1_1451479185_1451479197_502x234.jpg" border="0" alt="15 Reasons Why Deepika Padukone Was Bollywood's One True Superstar In 2015!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/15-reasons-why-deepika-padukone-should-be-crowned-the-queen-of-2015_-248923.html" title="15 Reasons Why Deepika Padukone Was Bollywood's One True Superstar In 2015!">
                        15 Reasons Why Deepika Padukone Was Bollywood's One True Superstar In 2015!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-version-of-men-will-be-men-will-remind-you-of-andaz-apna-apna-s-epic-bus-scene-248928.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-version-of-men-will-be-men-will-remind-you-of-andaz-apna-apna-s-epic-bus-scene-248928.html" class="tint" title="This Version Of 'Men Will Be Men' Will Remind You Of Andaz Apna Apna's Epic Bus Scene!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/menwillbemwn_card_1451470145_502x234.jpg" border="0" alt="This Version Of 'Men Will Be Men' Will Remind You Of Andaz Apna Apna's Epic Bus Scene!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-version-of-men-will-be-men-will-remind-you-of-andaz-apna-apna-s-epic-bus-scene-248928.html" title="This Version Of 'Men Will Be Men' Will Remind You Of Andaz Apna Apna's Epic Bus Scene!">
                        This Version Of 'Men Will Be Men' Will Remind You Of Andaz Apna Apna's Epic Bus Scene!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/from-cold-winds-to-warm-smiles-this-travel-video-will-make-you-fall-in-love-with-sikkim-and-it-s-people-248919.html" class="tint" title="From Cold Winds To Warm Smiles, This Travel Video Will Make You Fall In Love With Sikkim And It's People!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/sikkim_card1_1451462299_502x234.jpg" border="0" alt="From Cold Winds To Warm Smiles, This Travel Video Will Make You Fall In Love With Sikkim And It's People!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/from-cold-winds-to-warm-smiles-this-travel-video-will-make-you-fall-in-love-with-sikkim-and-it-s-people-248919.html" title="From Cold Winds To Warm Smiles, This Travel Video Will Make You Fall In Love With Sikkim And It's People!">
                        From Cold Winds To Warm Smiles, This Travel Video Will Make You Fall In Love With Sikkim And It's People!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/health/recipes/make-this-yummy-and-fat-free-tandoori-chicken-recipe-at-home-without-a-tandoor-248875.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/recipes/make-this-yummy-and-fat-free-tandoori-chicken-recipe-at-home-without-a-tandoor-248875.html" class="tint" title="Make This Yummy And Fat-Free Tandoori Chicken Recipe At Home, Without A Tandoor!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/cover_1451375426_502x234.jpg" border="0" alt="Make This Yummy And Fat-Free Tandoori Chicken Recipe At Home, Without A Tandoor!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/recipes/make-this-yummy-and-fat-free-tandoori-chicken-recipe-at-home-without-a-tandoor-248875.html" title="Make This Yummy And Fat-Free Tandoori Chicken Recipe At Home, Without A Tandoor!">
                        Make This Yummy And Fat-Free Tandoori Chicken Recipe At Home, Without A Tandoor!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/artist-re-imagines-the-solar-system-with-daily-life-problems-results-are-hilarious-248760.html" class="tint" title="If Our Solar System Had Daily Life Problems, These Would Be It">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/earth_1450962751_1450962761_502x234.jpg" border="0" alt="If Our Solar System Had Daily Life Problems, These Would Be It" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/artist-re-imagines-the-solar-system-with-daily-life-problems-results-are-hilarious-248760.html" title="If Our Solar System Had Daily Life Problems, These Would Be It">
                        If Our Solar System Had Daily Life Problems, These Would Be It                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/any-change-in-mindset-begins-at-home-and-this-thought-provoking-tvc-shows-you-exactly-how-248922.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/any-change-in-mindset-begins-at-home-and-this-thought-provoking-tvc-shows-you-exactly-how-248922.html" class="tint" title="Any Change In Mindset Begins At Home, And This Thought-Provoking TVC Shows You Exactly How">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/changetheconvention_card_1451463712_502x234.jpg" border="0" alt="Any Change In Mindset Begins At Home, And This Thought-Provoking TVC Shows You Exactly How" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/any-change-in-mindset-begins-at-home-and-this-thought-provoking-tvc-shows-you-exactly-how-248922.html" title="Any Change In Mindset Begins At Home, And This Thought-Provoking TVC Shows You Exactly How">
                        Any Change In Mindset Begins At Home, And This Thought-Provoking TVC Shows You Exactly How                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/5-home-decorating-tips-that-are-guaranteed-to-fight-stress-and-improve-your-mood-248887.html" class="tint" title="5 Home Decorating Tips That Are Guaranteed To Fight Stress And Improve Your Mood!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/cover_1451385574_502x234.jpg" border="0" alt="5 Home Decorating Tips That Are Guaranteed To Fight Stress And Improve Your Mood!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/5-home-decorating-tips-that-are-guaranteed-to-fight-stress-and-improve-your-mood-248887.html" title="5 Home Decorating Tips That Are Guaranteed To Fight Stress And Improve Your Mood!">
                        5 Home Decorating Tips That Are Guaranteed To Fight Stress And Improve Your Mood!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/shah-rukh-khan-kajol-taking-a-case-of-their-hater-is-so-lame-that-it-s-actually-cool-248914.html" class="tint" title="Shah Rukh Khan-Kajol Taking A Case Of Their Hater Is So Lame That It's Actually Cool">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card1_1451457592_1451457602_502x234.jpg" border="0" alt="Shah Rukh Khan-Kajol Taking A Case Of Their Hater Is So Lame That It's Actually Cool" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/shah-rukh-khan-kajol-taking-a-case-of-their-hater-is-so-lame-that-it-s-actually-cool-248914.html" title="Shah Rukh Khan-Kajol Taking A Case Of Their Hater Is So Lame That It's Actually Cool">
                        Shah Rukh Khan-Kajol Taking A Case Of Their Hater Is So Lame That It's Actually Cool                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/which-city-should-you-live-in-find-out-with-suriya-248537.html" class="tint" title="Which City Should You Live In? Find Out With Tamil Superstar Suriya.">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/quiz/2015/Dec/s2_1451391349_502x234.jpg" border="0" alt="Which City Should You Live In? Find Out With Tamil Superstar Suriya." class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/which-city-should-you-live-in-find-out-with-suriya-248537.html" title="Which City Should You Live In? Find Out With Tamil Superstar Suriya.">
                        Which City Should You Live In? Find Out With Tamil Superstar Suriya.                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/salman-khan-s-new-initiative-runs-into-troubled-waters-might-face-lawsuit-from-delhi-s-traders-248911.html" class="tint" title="Delhi's Khan Market Traders To Sue Salman Khan Over His New Website's Name!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/salman-card_1451455477_1451455482_502x234.jpg" border="0" alt="Delhi's Khan Market Traders To Sue Salman Khan Over His New Website's Name!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/salman-khan-s-new-initiative-runs-into-troubled-waters-might-face-lawsuit-from-delhi-s-traders-248911.html" title="Delhi's Khan Market Traders To Sue Salman Khan Over His New Website's Name!">
                        Delhi's Khan Market Traders To Sue Salman Khan Over His New Website's Name!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/gemini-this-is-your-complete-horoscope-for-2016_-248861.html" class="tint" title="Gemini! This Is Your Complete Horoscope For 2016">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/cp_1451374279_502x234.jpg" border="0" alt="Gemini! This Is Your Complete Horoscope For 2016" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/gemini-this-is-your-complete-horoscope-for-2016_-248861.html" title="Gemini! This Is Your Complete Horoscope For 2016">
                        Gemini! This Is Your Complete Horoscope For 2016                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/have-you-been-doing-squats-the-right-way-this-quick-video-teaches-you-the-correct-technique-248853.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/have-you-been-doing-squats-the-right-way-this-quick-video-teaches-you-the-correct-technique-248853.html" class="tint" title="Have You Been Doing Squats Wrong All Along?">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/cover_1451306538_502x234.jpg" border="0" alt="Have You Been Doing Squats Wrong All Along?" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/have-you-been-doing-squats-the-right-way-this-quick-video-teaches-you-the-correct-technique-248853.html" title="Have You Been Doing Squats Wrong All Along?">
                        Have You Been Doing Squats Wrong All Along?                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/14-lessons-you-wish-you-could-give-your-younger-self-this-new-year-248849.html" class="tint" title="14 Lessons You Wish You Could Give Your Younger Self This New Year">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/ddd1card2_1451302509_1451302517_502x234.jpg" border="0" alt="14 Lessons You Wish You Could Give Your Younger Self This New Year" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/14-lessons-you-wish-you-could-give-your-younger-self-this-new-year-248849.html" title="14 Lessons You Wish You Could Give Your Younger Self This New Year">
                        14 Lessons You Wish You Could Give Your Younger Self This New Year                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/12-times-nawazuddin-siddiqui-said-something-no-one-else-had-the-balls-for-248877.html" class="tint" title="12 Times Nawazuddin Siddiqui Said Something No One Else Had The Balls For!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/maxresdefault_1451456098_1451456106_502x234.jpg" border="0" alt="12 Times Nawazuddin Siddiqui Said Something No One Else Had The Balls For!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/12-times-nawazuddin-siddiqui-said-something-no-one-else-had-the-balls-for-248877.html" title="12 Times Nawazuddin Siddiqui Said Something No One Else Had The Balls For!">
                        12 Times Nawazuddin Siddiqui Said Something No One Else Had The Balls For!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/9-popular-tv-celebs-who-made-smashing-bollywood-debuts-in-2015_-248898.html" class="tint" title="9 Popular TV Celebs Who Made Smashing Bollywood Debuts In 2015">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1451390648_1451390666_502x234.jpg" border="0" alt="9 Popular TV Celebs Who Made Smashing Bollywood Debuts In 2015" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/9-popular-tv-celebs-who-made-smashing-bollywood-debuts-in-2015_-248898.html" title="9 Popular TV Celebs Who Made Smashing Bollywood Debuts In 2015">
                        9 Popular TV Celebs Who Made Smashing Bollywood Debuts In 2015                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/peta-declares-anushka-sharma-aamir-khan-as-hottest-fittest-vegetarians-in-india-248900.html" class="tint" title="PETA Declares Anushka Sharma, Aamir Khan As Hottest & Fittest Vegetarians In India!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card1_1451391588_1451391594_502x234.jpg" border="0" alt="PETA Declares Anushka Sharma, Aamir Khan As Hottest & Fittest Vegetarians In India!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/peta-declares-anushka-sharma-aamir-khan-as-hottest-fittest-vegetarians-in-india-248900.html" title="PETA Declares Anushka Sharma, Aamir Khan As Hottest & Fittest Vegetarians In India!">
                        PETA Declares Anushka Sharma, Aamir Khan As Hottest & Fittest Vegetarians In India!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/a-step-by-step-guide-on-how-to-take-care-of-a-drunk-person-248763.html" class="tint" title="A Step-By-Step Guide On How To Take Care Of A Drunk Person">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/asassa_1450966995_502x234.jpg" border="0" alt="A Step-By-Step Guide On How To Take Care Of A Drunk Person" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/a-step-by-step-guide-on-how-to-take-care-of-a-drunk-person-248763.html" title="A Step-By-Step Guide On How To Take Care Of A Drunk Person">
                        A Step-By-Step Guide On How To Take Care Of A Drunk Person                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/this-is-what-the-end-of-the-world-could-potentially-look-like-and-it-is-not-pretty-248828.html" class="tint" title="This Is What The End Of The World Could Potentially Look Like, And It Is Not Pretty">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/11_1451284820_1451284841_502x234.jpg" border="0" alt="This Is What The End Of The World Could Potentially Look Like, And It Is Not Pretty" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/this-is-what-the-end-of-the-world-could-potentially-look-like-and-it-is-not-pretty-248828.html" title="This Is What The End Of The World Could Potentially Look Like, And It Is Not Pretty">
                        This Is What The End Of The World Could Potentially Look Like, And It Is Not Pretty                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/the-way-this-woman-rescued-these-puppies-will-warm-the-cockles-of-your-heart-248871.html" class="tint" title="The Way This Woman Rescued These Puppies Will Warm The Cockles Of Your Heart">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/woman_1451371240_1451371248_502x234.jpg" border="0" alt="The Way This Woman Rescued These Puppies Will Warm The Cockles Of Your Heart" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/the-way-this-woman-rescued-these-puppies-will-warm-the-cockles-of-your-heart-248871.html" title="The Way This Woman Rescued These Puppies Will Warm The Cockles Of Your Heart">
                        The Way This Woman Rescued These Puppies Will Warm The Cockles Of Your Heart                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/big-b-met-his-special-fan-proved-yet-again-that-he-s-an-epitome-of-humility-respect-248872.html" class="tint" title="Big B Met His Special Fan & Proved Yet Again That He's An Epitome Of Humility #Respect">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card-amitabh_1451372030_1451372035_502x234.jpg" border="0" alt="Big B Met His Special Fan & Proved Yet Again That He's An Epitome Of Humility #Respect" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/big-b-met-his-special-fan-proved-yet-again-that-he-s-an-epitome-of-humility-respect-248872.html" title="Big B Met His Special Fan & Proved Yet Again That He's An Epitome Of Humility #Respect">
                        Big B Met His Special Fan & Proved Yet Again That He's An Epitome Of Humility #Respect                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/overworked-priyanka-chopra-gives-the-most-kickass-reply-to-being-underpaid-248865.html" class="tint" title="'Overworked' Priyanka Chopra Gives The Most Kickass Reply To Being 'Underpaid'!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/1_1451370161_1451370166_502x234.jpg" border="0" alt="'Overworked' Priyanka Chopra Gives The Most Kickass Reply To Being 'Underpaid'!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/overworked-priyanka-chopra-gives-the-most-kickass-reply-to-being-underpaid-248865.html" title="'Overworked' Priyanka Chopra Gives The Most Kickass Reply To Being 'Underpaid'!">
                        'Overworked' Priyanka Chopra Gives The Most Kickass Reply To Being 'Underpaid'!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-guy-explains-why-facebook-s-free-basics-is-worse-than-a-woman-friend-zoning-a-guy-248866.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-guy-explains-why-facebook-s-free-basics-is-worse-than-a-woman-friend-zoning-a-guy-248866.html" class="tint" title="This Guy Explains Why Facebook's 'Free Basics' Is Worse Than A Woman Friend-Zoning A Man!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/abish_card_1451369467_502x234.jpg" border="0" alt="This Guy Explains Why Facebook's 'Free Basics' Is Worse Than A Woman Friend-Zoning A Man!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-guy-explains-why-facebook-s-free-basics-is-worse-than-a-woman-friend-zoning-a-guy-248866.html" title="This Guy Explains Why Facebook's 'Free Basics' Is Worse Than A Woman Friend-Zoning A Man!">
                        This Guy Explains Why Facebook's 'Free Basics' Is Worse Than A Woman Friend-Zoning A Man!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/16-of-the-most-epic-fails-of-2015_-248761.html" class="tint" title="16 Of The Most Epic Fails Of 2015">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1450964368_502x234.jpg" border="0" alt="16 Of The Most Epic Fails Of 2015" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/16-of-the-most-epic-fails-of-2015_-248761.html" title="16 Of The Most Epic Fails Of 2015">
                        16 Of The Most Epic Fails Of 2015                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/16-relationship-resolutions-we-should-all-make-this-year-248754.html" class="tint" title="16 Relationship Resolutions We Should All Make This Year">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/1_1450958951_502x234.jpg" border="0" alt="16 Relationship Resolutions We Should All Make This Year" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/16-relationship-resolutions-we-should-all-make-this-year-248754.html" title="16 Relationship Resolutions We Should All Make This Year">
                        16 Relationship Resolutions We Should All Make This Year                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/are-men-really-from-mars-and-women-from-venus-these-14-images-prove-that-it-may-be-true-248858.html" class="tint" title="Are Men Really From Mars And Women From Venus? These 14 Images Prove That It May Be True">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/realiy_1451374221_1451374244_502x234.jpg" border="0" alt="Are Men Really From Mars And Women From Venus? These 14 Images Prove That It May Be True" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/are-men-really-from-mars-and-women-from-venus-these-14-images-prove-that-it-may-be-true-248858.html" title="Are Men Really From Mars And Women From Venus? These 14 Images Prove That It May Be True">
                        Are Men Really From Mars And Women From Venus? These 14 Images Prove That It May Be True                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/if-you-thought-bollywood-celebs-didn-t-care-about-fans-these-8-kind-gestures-will-prove-you-wrong-248845.html" class="tint" title="If You Thought Bollywood Celebs Didn't Care About Fans, These 8 Kind Gestures Will Prove You Wrong">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1451299867_1451299873_502x234.jpg" border="0" alt="If You Thought Bollywood Celebs Didn't Care About Fans, These 8 Kind Gestures Will Prove You Wrong" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/if-you-thought-bollywood-celebs-didn-t-care-about-fans-these-8-kind-gestures-will-prove-you-wrong-248845.html" title="If You Thought Bollywood Celebs Didn't Care About Fans, These 8 Kind Gestures Will Prove You Wrong">
                        If You Thought Bollywood Celebs Didn't Care About Fans, These 8 Kind Gestures Will Prove You Wrong                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/here-s-why-india-is-one-of-the-most-powerful-countries-in-the-world-proudtobeanindian-248805.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/here-s-why-india-is-one-of-the-most-powerful-countries-in-the-world-proudtobeanindian-248805.html" class="tint" title="Here's Why India Is One Of The Most Powerful Countries In The World #ProudToBeAnIndian">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/manufacturing_card_1451366032_502x234.jpg" border="0" alt="Here's Why India Is One Of The Most Powerful Countries In The World #ProudToBeAnIndian" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/here-s-why-india-is-one-of-the-most-powerful-countries-in-the-world-proudtobeanindian-248805.html" title="Here's Why India Is One Of The Most Powerful Countries In The World #ProudToBeAnIndian">
                        Here's Why India Is One Of The Most Powerful Countries In The World #ProudToBeAnIndian                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/taurus-this-is-your-complete-horoscope-for-the-year-2016_-248795.html" class="tint" title="Taurus! This Is Your Complete Horoscope For The Year 2016">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/cp_1451121101_502x234.jpg" border="0" alt="Taurus! This Is Your Complete Horoscope For The Year 2016" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/taurus-this-is-your-complete-horoscope-for-the-year-2016_-248795.html" title="Taurus! This Is Your Complete Horoscope For The Year 2016">
                        Taurus! This Is Your Complete Horoscope For The Year 2016                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-parody-of-fan-aptly-shows-how-every-srk-fan-felt-after-watching-dilwale-248850.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-parody-of-fan-aptly-shows-how-every-srk-fan-felt-after-watching-dilwale-248850.html" class="tint" title="This Parody Of 'FAN' Aptly Shows How Every SRK Fan Felt After Watching Dilwale!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/srkfans_card_1451303863_502x234.jpg" border="0" alt="This Parody Of 'FAN' Aptly Shows How Every SRK Fan Felt After Watching Dilwale!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-parody-of-fan-aptly-shows-how-every-srk-fan-felt-after-watching-dilwale-248850.html" title="This Parody Of 'FAN' Aptly Shows How Every SRK Fan Felt After Watching Dilwale!">
                        This Parody Of 'FAN' Aptly Shows How Every SRK Fan Felt After Watching Dilwale!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/here-s-what-the-lines-on-the-palm-of-your-hand-really-mean-248830.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/here-s-what-the-lines-on-the-palm-of-your-hand-really-mean-248830.html" class="tint" title="Here's What The Lines On The Palm Of Your Hand Really Mean!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/palmcard1_1451295951_502x234.jpg" border="0" alt="Here's What The Lines On The Palm Of Your Hand Really Mean!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/here-s-what-the-lines-on-the-palm-of-your-hand-really-mean-248830.html" title="Here's What The Lines On The Palm Of Your Hand Really Mean!">
                        Here's What The Lines On The Palm Of Your Hand Really Mean!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/pilibhit-sanctuary-takes-a-dig-at-salman-on-his-50th-birthday-seeks-his-help-to-save-black-bucks-248841.html" class="tint" title="Pilibhit Sanctuary Takes A Dig At Salman On His 50th Birthday, Seeks His Help To Save Blackbucks!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/salman-khan-card_1451296571_1451296581_502x234.jpg" border="0" alt="Pilibhit Sanctuary Takes A Dig At Salman On His 50th Birthday, Seeks His Help To Save Blackbucks!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/pilibhit-sanctuary-takes-a-dig-at-salman-on-his-50th-birthday-seeks-his-help-to-save-black-bucks-248841.html" title="Pilibhit Sanctuary Takes A Dig At Salman On His 50th Birthday, Seeks His Help To Save Blackbucks!">
                        Pilibhit Sanctuary Takes A Dig At Salman On His 50th Birthday, Seeks His Help To Save Blackbucks!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/12-things-every-party-lover-does-on-new-year-s-eve-248739.html" class="tint" title="12 Things Every Party Lover Does On New Yearâs Eve">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1450952821_502x234.jpg" border="0" alt="12 Things Every Party Lover Does On New Yearâs Eve" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/12-things-every-party-lover-does-on-new-year-s-eve-248739.html" title="12 Things Every Party Lover Does On New Yearâs Eve">
                        12 Things Every Party Lover Does On New Yearâs Eve                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/20-of-nat-geo-s-most-popular-photographs-in-2015_-248759.html" class="tint" title="20 Of Nat Geo's Most Popular Photographs In 2015">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1450962024_502x234.jpg" border="0" alt="20 Of Nat Geo's Most Popular Photographs In 2015" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/20-of-nat-geo-s-most-popular-photographs-in-2015_-248759.html" title="20 Of Nat Geo's Most Popular Photographs In 2015">
                        20 Of Nat Geo's Most Popular Photographs In 2015                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/indian-women-talking-about-their-first-incident-of-sexual-abuse-shows-what-s-wrong-with-our-country-248834.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/indian-women-talking-about-their-first-incident-of-sexual-abuse-shows-what-s-wrong-with-our-country-248834.html" class="tint" title="Indian Women Talking About Their First Incident Of Sexual Abuse Shows What's Wrong With Our Country!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/sexualabuse_card4_1451296696_502x234.jpg" border="0" alt="Indian Women Talking About Their First Incident Of Sexual Abuse Shows What's Wrong With Our Country!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/indian-women-talking-about-their-first-incident-of-sexual-abuse-shows-what-s-wrong-with-our-country-248834.html" title="Indian Women Talking About Their First Incident Of Sexual Abuse Shows What's Wrong With Our Country!">
                        Indian Women Talking About Their First Incident Of Sexual Abuse Shows What's Wrong With Our Country!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/the-8-step-guide-to-weight-loss-for-busy-people-248843.html" class="tint" title="The 8 Step Guide To Weight Loss For Busy People!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/cover1_1451302805_502x234.jpg" border="0" alt="The 8 Step Guide To Weight Loss For Busy People!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/the-8-step-guide-to-weight-loss-for-busy-people-248843.html" title="The 8 Step Guide To Weight Loss For Busy People!">
                        The 8 Step Guide To Weight Loss For Busy People!                    </a>
                </figcaption>
            </div>
        
        <script id="HpMiddleBlock_tpl" type="text/x-handlebars-template">
            {{#if MoreData}}
            {{#each MoreData}} 
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>


                    <a  class='video-btn sprite' href="{{guid}}" style="display:{{card_id}}"> </a>

    				{{#if label_name.length}}
                    <a href="{{guid}}" class="{{label_name}} sticker">&nbsp;</a>
    						 {{/if}} 
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="{{guid}}" title="{{carousal_headline}}" class="tint">
                        <img class="greyBg lazy" data-original="{{thumbnail}}" border="0" alt="{{carousal_headline}}" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="{{guid}}" title="{{carousal_headline}}">
                        {{carousal_headline}}
                    </a>
                </figcaption>
            </div>  
            {{/each}} 
            {{/if}}

            </script>

        </div><!--life-panel end-->

        <div class="trending-panel cf "><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_15"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/lifestyle/self/a-step-by-step-guide-on-how-to-take-care-of-a-drunk-person-248763.html" class="tint" title="A Step-By-Step Guide On How To Take Care Of A Drunk Person">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/asassa_1450966995_218x102.jpg" border="0" alt="A Step-By-Step Guide On How To Take Care Of A Drunk Person"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/a-step-by-step-guide-on-how-to-take-care-of-a-drunk-person-248763.html" title="A Step-By-Step Guide On How To Take Care Of A Drunk Person">
                            A Step-By-Step Guide On How To Take Care Of A Drunk Person                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                <div class='container3'>                <div class="trending-panel-list cf" id="column3_16"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/celebs/peta-declares-anushka-sharma-aamir-khan-as-hottest-fittest-vegetarians-in-india-248900.html" class="tint" title="PETA Declares Anushka Sharma, Aamir Khan As Hottest & Fittest Vegetarians In India!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card1_1451391588_1451391594_218x102.jpg" border="0" alt="PETA Declares Anushka Sharma, Aamir Khan As Hottest & Fittest Vegetarians In India!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/peta-declares-anushka-sharma-aamir-khan-as-hottest-fittest-vegetarians-in-india-248900.html" title="PETA Declares Anushka Sharma, Aamir Khan As Hottest & Fittest Vegetarians In India!">
                            PETA Declares Anushka Sharma, Aamir Khan As Hottest & Fittest Vegetarians In India!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_17"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/celebs/ranveer-singh-s-candid-confessions-about-deepika-endorsing-condoms-growing-up-will-make-you-want-to-give-him-a-hug-248933.html" class="tint" title="Ranveer Singh's Candid Confessions About Deepika, Endorsing Condoms & Growing Up Will Make You Want To Give Him A Hug!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/ranveer-singh_1451476403_1451476407_218x102.jpg" border="0" alt="Ranveer Singh's Candid Confessions About Deepika, Endorsing Condoms & Growing Up Will Make You Want To Give Him A Hug!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/ranveer-singh-s-candid-confessions-about-deepika-endorsing-condoms-growing-up-will-make-you-want-to-give-him-a-hug-248933.html" title="Ranveer Singh's Candid Confessions About Deepika, Endorsing Condoms & Growing Up Will Make You Want To Give Him A Hug!">
                            Ranveer Singh's Candid Confessions About Deepika, Endorsing Condoms & Growing Up Will Make You Want To Give Him A Hug!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_18"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/celebs/overworked-priyanka-chopra-gives-the-most-kickass-reply-to-being-underpaid-248865.html" class="tint" title="'Overworked' Priyanka Chopra Gives The Most Kickass Reply To Being 'Underpaid'!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/1_1451370161_1451370166_218x102.jpg" border="0" alt="'Overworked' Priyanka Chopra Gives The Most Kickass Reply To Being 'Underpaid'!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/overworked-priyanka-chopra-gives-the-most-kickass-reply-to-being-underpaid-248865.html" title="'Overworked' Priyanka Chopra Gives The Most Kickass Reply To Being 'Underpaid'!">
                            'Overworked' Priyanka Chopra Gives The Most Kickass Reply To Being 'Underpaid'!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_19"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/culture/who-we-are/this-is-what-the-end-of-the-world-could-potentially-look-like-and-it-is-not-pretty-248828.html" class="tint" title="This Is What The End Of The World Could Potentially Look Like, And It Is Not Pretty">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/11_1451284820_1451284841_218x102.jpg" border="0" alt="This Is What The End Of The World Could Potentially Look Like, And It Is Not Pretty"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/this-is-what-the-end-of-the-world-could-potentially-look-like-and-it-is-not-pretty-248828.html" title="This Is What The End Of The World Could Potentially Look Like, And It Is Not Pretty">
                            This Is What The End Of The World Could Potentially Look Like, And It Is Not Pretty                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                
        </div>

    </div><!--trending-panel end-->

    </section>
    <section class="container cf" id="loader" style="display: none;border: 0px solid red;">
    <div class="news-panel cf ">&nbsp;</div>
    <div class="life-panel cf" style="text-align: center;"><img src="http://media.indiatimes.in/resources/images/loading.gif" border="0" /></div>
    <div class="trending-panel cf ">&nbsp;</div>
</section>
<section class="big-ads remove-fixed-home" id="adfooter">
<div id="badRos_slot"></div> 
</section>   

<!------------------------ end Block 4------------------------------------------------>
<!--container end-->

<script type="text/javascript">
   
    $('#bigAd1_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    showBigAD2('bigAd2_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible     
       
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#bigAd2_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    showBigAD3('bigAd3_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible

    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible  
      
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#bigAd3_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    BADros('badRos_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});

$(document).ready(function() {
	/* spotlight onload tracking homepage */
	//console.log("homepage");
			ga('send', 'event', 'OnLoad Partner Stories', '248897', 'homepage', {'nonInteraction': 1});
			ga('send', 'event', 'OnLoad Partner Stories', '248854', 'homepage', {'nonInteraction': 1});
			ga('send', 'event', 'OnLoad Partner Stories', '248840', 'homepage', {'nonInteraction': 1});
	           
});

$(document).ready(function(){
        var trigger_depth={b2:99, b3:499, b4:1199};
        var is_trigger_active={b2:true, b3:true, b4:true};
        var call_on_scroll=true;
        
            $(window).on("scroll",function(){
            if((!(is_trigger_active.b2)&&!(is_trigger_active.b3)&&!(is_trigger_active.b4))==false)
            {
                callPreload(is_trigger_active,trigger_depth);
                
            }
    });
});

function callPreload(active,depth)
{
    var scroll_top = $(window).scrollTop();
    if((active.b2)&&(scroll_top > depth.b2))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Dec/card_1451556960_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/kohlimithali_1451555528_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/502_1451554844_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/rtr25etx5_1451554929_1451554936_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/dabba5_1451550339_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/cover1_1451479842_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/virat_1451391849_1451391875_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/awednesday_card_1451560179_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/changetheconvention_card_1451463712_218x102.jpg","http://media.indiatimes.in/media/content/2015/Dec/deepika-card1_1451479185_1451479197_218x102.jpg","http://media.indiatimes.in/media/content/2015/Dec/sikkim_card1_1451462299_218x102.jpg","http://media.indiatimes.in/media/content/2015/Dec/rsz_3maxresdefault_1451485993_218x102.jpg","http://media.indiatimes.in/media/content/2015/Dec/rsz_3maxresdefault_1451485993_218x102.jpg");
        active.b2=false;
    }
    if((active.b3)&&(scroll_top > depth.b3))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Dec/fatima-khatoon-card_1451545712_1451545719_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1451547263_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1451550401_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/family502_1451549511_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1451549713_236x111.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/deafhusband_card_1451548750_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/kids-card_1451556260_1451556267_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/cover_1451476139_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/ddd1card2_1451302509_1451302517_218x102.jpg","http://media.indiatimes.in/media/content/2015/Dec/cp_1451374279_218x102.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/card_1451551983_1451551987_218x102.jpg","http://media.indiatimes.in/media/content/2015/Dec/maxresdefault_1451456098_1451456106_218x102.jpg","http://media.indiatimes.in/media/content/2015/Dec/1_1450958951_218x102.jpg");
        active.b3=false;
    }
    
    if((active.b4)&&(scroll_top > depth.b4))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Dec/main5_1451543931_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/drunk5_1451539062_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1451542144_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/patanjali---card_1451480652_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1451479109_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/wazir_1451554743_1451554760_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1451548536_1451548543_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1451544150_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1451546343_1451546350_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1451541631_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/cover_1451468325_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/censor-board_1451541632_1451541642_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/cp_1451377580_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1451539789_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/cover_1451478487_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1451541106_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/cover_1451388462_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/iamchennai_card_1451477542_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/ranveer-singh_1451476403_1451476407_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/2_1451392532_1451392537_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/deepika-card1_1451479185_1451479197_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/menwillbemwn_card_1451470145_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/sikkim_card1_1451462299_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/cover_1451375426_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/earth_1450962751_1450962761_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/changetheconvention_card_1451463712_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/cover_1451385574_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card1_1451457592_1451457602_502x234.jpg","http://media.indiatimes.in/media/quiz/2015/Dec/s2_1451391349_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/salman-card_1451455477_1451455482_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/cp_1451374279_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/cover_1451306538_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/ddd1card2_1451302509_1451302517_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/maxresdefault_1451456098_1451456106_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1451390648_1451390666_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card1_1451391588_1451391594_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/asassa_1450966995_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/11_1451284820_1451284841_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/woman_1451371240_1451371248_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card-amitabh_1451372030_1451372035_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/1_1451370161_1451370166_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/abish_card_1451369467_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1450964368_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/1_1450958951_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/realiy_1451374221_1451374244_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1451299867_1451299873_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/manufacturing_card_1451366032_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/cp_1451121101_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/srkfans_card_1451303863_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/palmcard1_1451295951_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/salman-khan-card_1451296571_1451296581_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1450952821_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1450962024_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/sexualabuse_card4_1451296696_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/cover1_1451302805_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/asassa_1450966995_218x102.jpg","http://media.indiatimes.in/media/content/2015/Dec/card1_1451391588_1451391594_218x102.jpg","http://media.indiatimes.in/media/content/2015/Dec/ranveer-singh_1451476403_1451476407_218x102.jpg","http://media.indiatimes.in/media/content/2015/Dec/1_1451370161_1451370166_218x102.jpg","http://media.indiatimes.in/media/content/2015/Dec/11_1451284820_1451284841_218x102.jpg");
        active.b4=false;
    }
}
</script>    <div class="clr"></div>
      
	
                 <script> 
				                    showFooterCode();
					            </script>
	           <style>
.bottom-strip{background:#ebebeb;display:block;padding:15px 0;overflow:hidden;}.bottom-strip-box{margin:0 auto;display:table;max-width:410px;width:100%;}.square-logo{width:66px;height:64px;float:left;margin-right:10px;}.bottom-strip-content{float:left;margin-top:14px;}.bottom-strip-content span{display:block;color:#737272;font:12px/1 "RobotoRegular", Arial, Helvetica, sans-serif;margin-bottom:2px;}.bottom-strip-content a{color:#1b85dd;font:24px/1 "RobotoRegular", Arial, Helvetica, sans-serif;text-decoration:none;}
</style>
<div class="last-container">
    <!--<br>--> 
    <!--social-panel start-->
    <div class="social-panel red-bg">
        <div class="soc-inner"><!--soc-inner start-->
            <p class="soc-title">Be a Part of The New & Next</p>
            <div class="soc-count"><!--soc-count start-->
                <a href="https://www.facebook.com/indiatimes" target="_blank" title="facebook" class="sprite soc-fb"></a>
                <p>
                    4,141,636<span class="black"> FRIENDS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://twitter.com/indiatimes" target="_blank" title="twitter" class="sprite soc-twt"></a>
                <p>10343  <span class="black">FOLLOWERS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://plus.google.com/+indiatimes" target="_blank" title="gplus" class="sprite soc-gplus"></a>
                <p>104,163 <span class="black">MEMBERS</span></p>
            </div>
            <div class="soc-count"><!--soc-count start-->
                <a href="javascript:void(0);" title="E-mail" class="sprite soc-mail" id="subscribers_id"></a>
                <p>51,532 <span class="black">SUBSCRIBERS</span></p>
            </div>
            <!--soc-count end-->
        </div>
        <!--soc-inner end-->
    </div>
    <!--social-panel end-->
    <div class="clr"></div>

    <footer>
        <!-- social container start-->
        <div class="socical">
            <div class="containers">
                <div class="fl left_cont"> Get your weekly dose of virality sent to your inbox! </div>
                <div class="fr right_cont">

                    <input type="text" value="Please Enter Email Address" class="Email" name="UserEmail" id="UserEmail" defaultValue="Please Enter Email Address"/>
                    <input type="submit" id="btn-submit" value="SIGN UP" name="btn-submit" class="Subscribe">
                    <span id="subsEmal" style='color: red; padding-left: 66px; text-align: left;'></span>
                </div>

            </div>
        </div><!-- social containers end-->
        <div class="footer"><!-- footer grey start-->
            <div class="sub_container">
                <!-- container start-->
                <div class="cont_new">
                    <h2>Our Channels</h2>
                                                <a href='http://www.indiatimes.com/news/' class="blue size">News</a> 
                                                    <a href='http://www.indiatimes.com/lifestyle/' class="purple size">Lifestyle</a> 
                                                    <a href='http://www.indiatimes.com/entertainment/' class="red size">Entertainment</a> 
                                                    <a href='http://www.indiatimes.com/health/' class="green size">Health</a> 
                                                    <a href='http://www.indiatimes.com/videocafe/' class="orange size">Videos</a> 
                        					<a href='http://www.indiatimes.com/trending' class="size" style="color:white">Trending</a> 
                </div><!-- container closed-->

                <div class="cont"> <!-- container start-->
                    <h2>Sections</h2>
                                            <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/news/india/'>india</a>
                                             
                                    <a href='http://www.indiatimes.com/news/world/'>world</a>
                                             
                                    <a href='http://www.indiatimes.com/news/sports/'>sports</a>
                                             
                                    <a href='http://www.indiatimes.com/news/weird/'>weird</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/lifestyle/self/'>self</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/style/'>style</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/who-we-are/'>who we are</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/travel/'>travel</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/food/'>food</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/entertainment/bollywood/'>bollywood</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/celebs/'>celebscoop</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/hollywood/'>hollywood</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                             
                                    <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                             
                                    <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                             
                                    <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                             
                                    <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                
                             </div>                        <div class="sub_link">
                             </div>                    <div class="sub_link"><a href="http://www.indiatimes.com/photogallery">Photogallery</a></div>
                </div><!-- container closed-->
                <div class="cont_new"> <!-- container start-->
                    <h2>Indiatimes Lifestyle Network</h2>

                    <div class="sub_link">                             <a href="http://www.mensxp.com"   target="_blank" >MENS XP</a>
                                                    <a href="http://www.idiva.com"   target="_blank" >iDiva</a>
                                                    <a href="http://luxpresso.com"   target="_blank" >Luxury</a>
                                                    <a href="http://in.askmen.com/"   target="_blank" >Ask Men</a>
                                                    <a href="http://www.pursuitist.in/"   target="_blank" >Pursuitist</a>
                        </div>
                </div><!-- container closed-->
                <div class="cont"> <!-- container start-->
                    <h2>Other Sites</h2>
                    <div class="sub_link"> 
                        
                            <a href="http://www.cricbuzz.com"  target="_blank">CricBuzz</a> 
                        
                            <a href="http://www.happytrips.com/"  target="_blank">Happy Trips</a> 
                        
                            <a href="http://www.businessinsider.in/"  target="_blank">Business Insider</a> 
                        
                            <a href="http://www.zoomtv.com"  target="_blank">ZoomTv</a> 
                        
                            <a href="http://www.gizmodo.in/"  target="_blank">Gizmodo</a> 
                        
                            <a href="http://www.lifehacker.co.in/"  target="_blank">Lifehacker</a> 
                        
                            <a href="http://in.ign.com/"  target="_blank">IGN</a> 
                        
                            <a href="http://boxtv.com"  target="_blank">BoxTV</a> 
                        
                            <a href="http://www.gaana.com"  target="_blank">Gaana</a> 
                        
                            <a href="http://shopping.indiatimes.com"  target="_blank">Online Shopping</a> 
                        
                            <a href="http://www.in.techradar.com/"  target="_blank">Techradar</a> 
                        
                            <a href="http://timesdeal.com"  target="_blank">Daily Deals</a> 
                        
                            <a href="http://www.gitanjalishop.com/"  target="_blank">Gitanjali Shop</a> 
                        
                            <a href="http://www.satvikshop.com/"  target="_blank">Satvik Shop</a> 
                        
                            <a href="http://whatshot.in"  target="_blank">WHAT'S HOT</a> 
                        
                            <a href="http://zigwheels.com"  target="_blank">ZigWheels</a> 
                        
                            <a href="http://filmipop.com"  target="_blank">FILMIPOP</a> 
                        
                            <a href="http://www.filmfare.com/"  target="_blank">Filmfare</a> 
                        
                            <a href="http://www.femina.in"  target="_blank">Femina</a> 
                        
                            <a href="http://mobile.indiatimes.com"  target="_blank">Mobile</a> 
                        
                            <a href="http://www.greetzap.com"  target="_blank">Greetings</a> 
                        
                            <a href="http://www.follo.co.in"  target="_blank">Follo</a> 
                        
                            <a href="http://timesjobs.com"  target="_blank">Jobs</a> 
                        
                            <a href="http://magicbricks.com"  target="_blank">Property</a> 
                        
                            <a href="http://www.astrospeak.com"  target="_blank">Astrology</a> 
                        
                            <a href="http://speakingtree.in"  target="_blank">Speaking Tree</a> 
                        
                            <a href="http://simplymarry.com"  target="_blank">Matrimonial</a> 
                        
                            <a href="http://itimes.com"  target="_blank">iTimes</a> 
                        
                            <a href="http://www.ads2book.com/"  target="_blank">Ads2book</a> 
                        
                            <a href="http://www.tcnext.com/"  target="_blank">TcNext</a> 
                        
                            <a href="http://www.romedynow.com"  target="_blank">Romedy Now</a> 
                                            </div>
                </div><!-- container closed-->
                <div class="cont border_none"><!-- container start-->
                    <h2>About us</h2>
                    <div class="sub_link">
                                                    <a href='http://www.indiatimes.com/privacypolicy' >Privacy Policy</a> 
                                                    <a href='http://www.indiatimes.com/sitemap.html' >sitemap</a> 
                                                    <a href='http://www.indiatimes.com/termsandcondition' >Terms & CONDITIONS</a> 
                                                    <a href='http://www.indiatimes.com/seoarchive' >ARCHIVES</a> 
                                                    <a href='http://www.indiatimes.com/contactus' >Contact us</a> 
                                                    <a href='http://www.indiatimes.com/aboutus' >ABOUT INDIATIMES</a> 
                                            </div>
                </div><!-- container closed-->
            </div><!-- footer grey closed-->
        </div><!-- footer black closed-->

        <div class="black_footer"><!-- footer black start-->
            <div class="containers">
                <p><a href="javascript:void(0)" class="copyrt">Copyright Â© 2015 Times Internet Limited. Powered by Indiatimes Lifestyle Network. All rights reserved</a></p>
                <span><a href="http://www.indiatimes.com/seoarchive/">Archive</a> | <a href="http://www.indiatimes.com/sitemap.html">Sitemap</a> | <a href="http://www.indiatimes.com/aboutus/">About Us</a> | <a href="http://www.indiatimes.com/contactus/">Contact Us</a> | <a href="http://www.indiatimes.com/privacypolicy/">Privacy Policy</a> | <a target="_blank" href="http://m.indiatimes.com">Indiatimes Mobile</a> | <a href="http://www.indiatimes.com/termsandcondition/">Terms of Use and Grievance Redressal Policy </a> </span> 
            </div>
        </div><!-- footer black end-->
    </footer>
</div>
<script>
    $( document ).ready(function() {
        $("#subscribers_id").click(function(){
            $("#UserEmail").focus();
        });
    });
    function socialRHSHide(){}
</script>

    <script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.lazyload.min.js?v=100.69" charset="utf-8"></script>
<script defer type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.easing.1.3.min.js?v=100.69"></script>

<input type="hidden" name="currentId" id="currentId" value="">
    <div class="animate_cont dawn floating delay5">
        <span class="animate_arrow sprite_image"></span>
    </div>




<script type="text/javascript" src="http://media.indiatimes.in/resources/js/main.js?v=100.69"></script>    
<script type="text/javascript" src="http://media.indiatimes.in/resources/js/common.js?v=100.69"></script>
<script  defer type="text/javascript" src="http://media.indiatimes.in/resources/js/jquery-scrolltofixed.js?v=100.69"></script>

<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://cdn.mouseflow.com/projects/1e238441-c30e-4d1b-8edd-c0f14d8fa6b7.js' type='text/javascript'%3E%3C/script%3E"));</script>
<!-- mouse flow code ends-->
<script  src="http://tags.crwdcntrl.net/c/2818/cc.js?ns=_cc2818" id="LOTCC_2818"></script> 

<script> 
    _cc2818.bcp(); 
</script>

	 <p id="back-top" style="display:none;"><a href="#top"><span></span></a> </p>
</div><!--wrap end-->


</body>
</html>