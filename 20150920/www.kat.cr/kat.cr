<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-773a99c.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-773a99c.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-773a99c.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-773a99c.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kat.cr/sc-773a99c.js');

        sc('setHost', 'a.kat.cr');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: '773a99c',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-773a99c.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<div id="_60318cd4e8d28f6fb76fe34e9bd9c498"></div>
<div id="_39ecb76dd457e5ac33776fdf11500d56"></div>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka">e</i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka">g</i></button></div>
			</form>
		</div>
        <div id="_277923e5f9d753c5b0630c28e641790c"></div>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080p/" class="tag6">1080p</a>
	<a href="/search/2014/" class="tag2">2014</a>
	<a href="/search/2015/" class="tag10">2015</a>
	<a href="/search/3d/" class="tag3">3d</a>
	<a href="/search/3d%20remux/" class="tag1">3d remux</a>
	<a href="/search/android/" class="tag2">android</a>
	<a href="/search/ant%20man/" class="tag2">ant man</a>
	<a href="/search/apk/" class="tag2">apk</a>
	<a href="/search/bahubali/" class="tag2">bahubali</a>
	<a href="/search/bajrangi%20bhaijaan/" class="tag1">bajrangi bhaijaan</a>
	<a href="/search/discography/" class="tag3">discography</a>
	<a href="/search/everest/" class="tag1">everest</a>
	<a href="/search/fear%20the%20walking%20dead/" class="tag2">fear the walking dead</a>
	<a href="/search/french/" class="tag3">french</a>
	<a href="/search/gotham/" class="tag2">gotham</a>
	<a href="/search/hindi/" class="tag10">hindi</a>
	<a href="/search/hindi%202015/" class="tag6">hindi 2015</a>
	<a href="/search/inside%20out/" class="tag2">inside out</a>
	<a href="/search/insurgent/" class="tag1">insurgent</a>
	<a href="/search/ita/" class="tag2">ita</a>
	<a href="/search/kat%20ph%20com/" class="tag8">kat ph com</a>
	<a href="/search/katti%20batti/" class="tag5">katti batti</a>
	<a href="/search/mad%20max/" class="tag4">mad max</a>
	<a href="/search/malayalam/" class="tag3">malayalam</a>
	<a href="/search/metal%20gear%20solid/" class="tag1">metal gear solid</a>
	<a href="/search/metal%20gear%20solid%20v/" class="tag2">metal gear solid v</a>
	<a href="/search/mia%20khalifa/" class="tag2">mia khalifa</a>
	<a href="/search/minions/" class="tag2">minions</a>
	<a href="/search/movies/" class="tag2">movies</a>
	<a href="/search/mr%20robot/" class="tag2">mr robot</a>
	<a href="/search/narcos/" class="tag2">narcos</a>
	<a href="/search/nezu/" class="tag10">nezu</a>
	<a href="/search/nl/" class="tag2">nl</a>
	<a href="/search/pixels/" class="tag2">pixels</a>
	<a href="/search/ripsalot/" class="tag5">ripsalot</a>
	<a href="/search/south%20park/" class="tag2">south park</a>
	<a href="/search/star%20wars/" class="tag2">star wars</a>
	<a href="/search/straight%20outta%20compton/" class="tag2">straight outta compton</a>
	<a href="/search/tamil/" class="tag4">tamil</a>
	<a href="/search/telugu/" class="tag3">telugu</a>
	<a href="/search/telugu%202015/" class="tag3">telugu 2015</a>
	<a href="/search/the%20visit/" class="tag2">the visit</a>
	<a href="/search/the%20walking%20dead/" class="tag2">the walking dead</a>
	<a href="/search/welcome%20back%202015/" class="tag2">welcome back 2015</a>
	<a href="/search/windows%2010/" class="tag2">windows 10</a>
	<a href="/search/yify/" class="tag10">yify</a>
	<a href="/search/yify%201080p/" class="tag4">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag5">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11265705,0" class="icommentjs icon16" href="/dracula-reborn-2015-hdrip-xvid-ac3-evo-t11265705.html#comment"> <em class="iconvalue">58</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/dracula-reborn-2015-hdrip-xvid-ac3-evo-t11265705.html" class="cellMainLink">Dracula Reborn 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1474207486">1.37 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="16 Sep 2015, 03:59">3&nbsp;days</span></td>
			<td class="green center">4943</td>
			<td class="red lasttd center">4703</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11265683,0" class="icommentjs icon16" href="/arthur-and-merlin-2015-hdrip-xvid-ac3-evo-t11265683.html#comment"> <em class="iconvalue">62</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/arthur-and-merlin-2015-hdrip-xvid-ac3-evo-t11265683.html" class="cellMainLink">Arthur and Merlin 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1495287752">1.39 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="16 Sep 2015, 03:54">3&nbsp;days</span></td>
			<td class="green center">4875</td>
			<td class="red lasttd center">3911</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11279654,0" class="icommentjs icon16" href="/vacation-2015-hc-hdrip-xvid-etrg-t11279654.html#comment"> <em class="iconvalue">44</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/vacation-2015-hc-hdrip-xvid-etrg-t11279654.html" class="cellMainLink">Vacation 2015 HC HDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="743196063">708.77 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="18 Sep 2015, 17:49">1&nbsp;day</span></td>
			<td class="green center">4273</td>
			<td class="red lasttd center">4273</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11276702,0" class="icommentjs icon16" href="/last-shift-2014-720p-brrip-x264-yify-t11276702.html#comment"> <em class="iconvalue">43</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/last-shift-2014-720p-brrip-x264-yify-t11276702.html" class="cellMainLink">Last Shift (2014) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="733838683">699.84 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="18 Sep 2015, 05:17">1&nbsp;day</span></td>
			<td class="green center">4473</td>
			<td class="red lasttd center">3320</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11278141,0" class="icommentjs icon16" href="/pay-the-ghost-2015-hdrip-xvid-etrg-t11278141.html#comment"> <em class="iconvalue">34</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/pay-the-ghost-2015-hdrip-xvid-etrg-t11278141.html" class="cellMainLink">Pay the Ghost 2015 HDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="741359954">707.02 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="18 Sep 2015, 11:11">1&nbsp;day</span></td>
			<td class="green center">3996</td>
			<td class="red lasttd center">3563</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11272012,0" class="icommentjs icon16" href="/cop-car-2015-1080p-brrip-x264-yify-t11272012.html#comment"> <em class="iconvalue">23</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/cop-car-2015-1080p-brrip-x264-yify-t11272012.html" class="cellMainLink">Cop Car (2015) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="1335511785">1.24 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="17 Sep 2015, 04:48">2&nbsp;days</span></td>
			<td class="green center">4289</td>
			<td class="red lasttd center">2643</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11276945,0" class="icommentjs icon16" href="/10-000-saints-2015-720p-brrip-x264-yify-t11276945.html#comment"> <em class="iconvalue">13</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/10-000-saints-2015-720p-brrip-x264-yify-t11276945.html" class="cellMainLink">10,000 Saints (2015) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="847561958">808.3 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="18 Sep 2015, 06:01">1&nbsp;day</span></td>
			<td class="green center">3261</td>
			<td class="red lasttd center">2312</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11270070,0" class="icommentjs icon16" href="/insidious-chapter-3-2015-hdrip-xvid-ac3-etrg-t11270070.html#comment"> <em class="iconvalue">45</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/insidious-chapter-3-2015-hdrip-xvid-ac3-etrg-t11270070.html" class="cellMainLink">Insidious Chapter 3 2015 HDRip XViD AC3-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="1490914846">1.39 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="16 Sep 2015, 21:07">3&nbsp;days</span></td>
			<td class="green center">2668</td>
			<td class="red lasttd center">2387</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11277397,0" class="icommentjs icon16" href="/cooties-2015-hdrip-xvid-ac3-evo-t11277397.html#comment"> <em class="iconvalue">27</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/cooties-2015-hdrip-xvid-ac3-evo-t11277397.html" class="cellMainLink">Cooties 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1495788394">1.39 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="18 Sep 2015, 08:16">1&nbsp;day</span></td>
			<td class="green center">2112</td>
			<td class="red lasttd center">2413</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11277254,0" class="icommentjs icon16" href="/we-are-still-here-2015-720p-brrip-x264-yify-t11277254.html#comment"> <em class="iconvalue">21</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/we-are-still-here-2015-720p-brrip-x264-yify-t11277254.html" class="cellMainLink">We Are Still Here (2015) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="730617828">696.77 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="18 Sep 2015, 07:38">1&nbsp;day</span></td>
			<td class="green center">1876</td>
			<td class="red lasttd center">1472</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11274025,0" class="icommentjs icon16" href="/rhymes-for-young-ghouls-2013-1080p-brrip-x264-yify-t11274025.html#comment"> <em class="iconvalue">35</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/rhymes-for-young-ghouls-2013-1080p-brrip-x264-yify-t11274025.html" class="cellMainLink">Rhymes for Young Ghouls (2013) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="1332528096">1.24 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="17 Sep 2015, 14:34">2&nbsp;days</span></td>
			<td class="green center">1962</td>
			<td class="red lasttd center">1193</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11280373,0" class="icommentjs icon16" href="/tomorrowland-2015-hdrip-xvid-ac3-evo-t11280373.html#comment"> <em class="iconvalue">69</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/tomorrowland-2015-hdrip-xvid-ac3-evo-t11280373.html" class="cellMainLink">Tomorrowland 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1541730983">1.44 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="18 Sep 2015, 21:02">1&nbsp;day</span></td>
			<td class="green center">1187</td>
			<td class="red lasttd center">2413</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11277450,0" class="icommentjs icon16" href="/nocturna-2015-bdrip-xvid-ac3-evo-t11277450.html#comment"> <em class="iconvalue">13</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/nocturna-2015-bdrip-xvid-ac3-evo-t11277450.html" class="cellMainLink">Nocturna 2015 BDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1516988657">1.41 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="18 Sep 2015, 08:29">1&nbsp;day</span></td>
			<td class="green center">975</td>
			<td class="red lasttd center">1441</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11272736,0" class="icommentjs icon16" href="/switch-2013-chinese-1080p-brrip-x264-dts-jyk-t11272736.html#comment"> <em class="iconvalue">13</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/switch-2013-chinese-1080p-brrip-x264-dts-jyk-t11272736.html" class="cellMainLink">Switch 2013 CHINESE 1080p BRRip x264 DTS-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="3308878374">3.08 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="17 Sep 2015, 08:31">2&nbsp;days</span></td>
			<td class="green center">1201</td>
			<td class="red lasttd center">880</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11277316,0" class="icommentjs icon16" href="/me-and-earl-and-the-dying-girl-2015-hdrip-xvid-etrg-t11277316.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/me-and-earl-and-the-dying-girl-2015-hdrip-xvid-etrg-t11277316.html" class="cellMainLink">Me and Earl and the Dying Girl 2015 HDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="741443394">707.1 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="18 Sep 2015, 07:53">1&nbsp;day</span></td>
			<td class="green center">1068</td>
			<td class="red lasttd center">983</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11265724,0" class="icommentjs icon16" href="/the-bastard-executioner-s01e01-e02-hdtv-x264-2hd-ettv-t11265724.html#comment"> <em class="iconvalue">158</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/the-bastard-executioner-s01e01-e02-hdtv-x264-2hd-ettv-t11265724.html" class="cellMainLink">The Bastard Executioner S01E01-E02 HDTV x264-2HD[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="965217159">920.5 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="16 Sep 2015, 04:08">3&nbsp;days</span></td>
			<td class="green center">6422</td>
			<td class="red lasttd center">2146</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11277854,0" class="icommentjs icon16" href="/limitless-s01e01-pilot-webrip-xvid-fum-ettv-t11277854.html#comment"> <em class="iconvalue">111</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/limitless-s01e01-pilot-webrip-xvid-fum-ettv-t11277854.html" class="cellMainLink">Limitless S01E01 Pilot WEBRip XviD-FUM[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="365410873">348.48 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="18 Sep 2015, 09:56">1&nbsp;day</span></td>
			<td class="green center">5895</td>
			<td class="red lasttd center">1679</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11271450,0" class="icommentjs icon16" href="/south-park-s19e01-720p-hdtv-x264-killers-rartv-t11271450.html#comment"> <em class="iconvalue">94</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/south-park-s19e01-720p-hdtv-x264-killers-rartv-t11271450.html" class="cellMainLink">South Park S19E01 720p HDTV x264-KILLERS[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="381478460">363.81 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="17 Sep 2015, 02:33">2&nbsp;days</span></td>
			<td class="green center">5318</td>
			<td class="red lasttd center">442</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11276313,0" class="icommentjs icon16" href="/dominion-s02e11-hdtv-x264-killers-ettv-t11276313.html#comment"> <em class="iconvalue">56</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/dominion-s02e11-hdtv-x264-killers-ettv-t11276313.html" class="cellMainLink">Dominion S02E11 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="288825733">275.45 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="18 Sep 2015, 03:10">1&nbsp;day</span></td>
			<td class="green center">3569</td>
			<td class="red lasttd center">541</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11265354,0" class="icommentjs icon16" href="/zoo-s01e13-hdtv-x264-lol-ettv-t11265354.html#comment"> <em class="iconvalue">74</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/zoo-s01e13-hdtv-x264-lol-ettv-t11265354.html" class="cellMainLink">Zoo S01E13 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="319756157">304.94 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="16 Sep 2015, 02:00">3&nbsp;days</span></td>
			<td class="green center">1868</td>
			<td class="red lasttd center">646</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11271478,0" class="icommentjs icon16" href="/masterchef-us-s06e19e20-hdtv-x264-lol-ettv-t11271478.html#comment"> <em class="iconvalue">26</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/masterchef-us-s06e19e20-hdtv-x264-lol-ettv-t11271478.html" class="cellMainLink">MasterChef US S06E19E20 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="882187837">841.32 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="17 Sep 2015, 02:43">2&nbsp;days</span></td>
			<td class="green center">1757</td>
			<td class="red lasttd center">277</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11271861,0" class="icommentjs icon16" href="/the-ultimate-fighter-s22-e02-hdtv-x264-jkkk-sparrow-t11271861.html#comment"> <em class="iconvalue">32</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/the-ultimate-fighter-s22-e02-hdtv-x264-jkkk-sparrow-t11271861.html" class="cellMainLink">The Ultimate Fighter S22 E02 HDTV x264-jkkk -={SPARROW}=-</a></div>
			</td>
			<td class="nobr center" data-sort="544488756">519.26 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="17 Sep 2015, 04:06">2&nbsp;days</span></td>
			<td class="green center">1760</td>
			<td class="red lasttd center">158</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11275287,0" class="icommentjs icon16" href="/wwe-smackdown-2015-09-17-hdtv-x264-ebi-sparrow-t11275287.html#comment"> <em class="iconvalue">33</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/wwe-smackdown-2015-09-17-hdtv-x264-ebi-sparrow-t11275287.html" class="cellMainLink">WWE Smackdown 2015 09 17 HDTV x264-Ebi -={SPARROW}=-</a></div>
			</td>
			<td class="nobr center" data-sort="895772477">854.28 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="17 Sep 2015, 19:49">2&nbsp;days</span></td>
			<td class="green center">1506</td>
			<td class="red lasttd center">521</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11281519,0" class="icommentjs icon16" href="/z-nation-s02e02-hdtv-x264-killers-ettv-t11281519.html#comment"> <em class="iconvalue">42</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/z-nation-s02e02-hdtv-x264-killers-ettv-t11281519.html" class="cellMainLink">Z Nation S02E02 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="436321516">416.11 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="19 Sep 2015, 03:07">20&nbsp;hours</span></td>
			<td class="green center">811</td>
			<td class="red lasttd center">1840</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11281387,0" class="icommentjs icon16" href="/continuum-s04e03-hdtv-x264-killers-ettv-t11281387.html#comment"> <em class="iconvalue">32</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/continuum-s04e03-hdtv-x264-killers-ettv-t11281387.html" class="cellMainLink">Continuum S04E03 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="321803611">306.9 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="19 Sep 2015, 02:09">21&nbsp;hours</span></td>
			<td class="green center">957</td>
			<td class="red lasttd center">1203</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11276328,0" class="icommentjs icon16" href="/graceland-s03e13-hdtv-x264-killers-ettv-t11276328.html#comment"> <em class="iconvalue">21</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/graceland-s03e13-hdtv-x264-killers-ettv-t11276328.html" class="cellMainLink">Graceland S03E13 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="305640868">291.48 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="18 Sep 2015, 03:18">1&nbsp;day</span></td>
			<td class="green center">1015</td>
			<td class="red lasttd center">502</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11271481,0" class="icommentjs icon16" href="/the-league-s07e02-hdtv-x264-batv-ettv-t11271481.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/the-league-s07e02-hdtv-x264-batv-ettv-t11271481.html" class="cellMainLink">The League S07E02 HDTV x264-BATV[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="268361048">255.93 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="17 Sep 2015, 02:43">2&nbsp;days</span></td>
			<td class="green center">1066</td>
			<td class="red lasttd center">103</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11278065,0" class="icommentjs icon16" href="/ì©íì´-yong-pal-e14-150917-720p-hdtv-x264-rain-t11278065.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/ì©íì´-yong-pal-e14-150917-720p-hdtv-x264-rain-t11278065.html" class="cellMainLink">ì©íì´ (Yong Pal) E14 150917 720p HDTV x264-RAiN</a></div>
			</td>
			<td class="nobr center" data-sort="1244547187">1.16 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="18 Sep 2015, 10:49">1&nbsp;day</span></td>
			<td class="green center">1014</td>
			<td class="red lasttd center">39</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11271170,0" class="icommentjs icon16" href="/americas-next-top-model-s22e07-the-guy-who-acts-a-fool-hdtv-x264-w4f-rartv-t11271170.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/americas-next-top-model-s22e07-the-guy-who-acts-a-fool-hdtv-x264-w4f-rartv-t11271170.html" class="cellMainLink">Americas Next Top Model S22E07 The Guy Who Acts a Fool HDTV x264-W4F[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="250149878">238.56 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="17 Sep 2015, 02:03">2&nbsp;days</span></td>
			<td class="green center">932</td>
			<td class="red lasttd center">76</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11267012,0" class="icommentjs icon16" href="/longmire-s04-webrip-xvid-fum-ettv-t11267012.html#comment"> <em class="iconvalue">16</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/longmire-s04-webrip-xvid-fum-ettv-t11267012.html" class="cellMainLink">Longmire S04 WEBRip XviD-FUM[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="4925018277">4.59 <span>GB</span></td>
			<td class="center">11</td>
			<td class="center"><span title="16 Sep 2015, 10:00">3&nbsp;days</span></td>
			<td class="green center">657</td>
			<td class="red lasttd center">569</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11273855,0" class="icommentjs icon16" href="/david-gilmour-rattle-that-lock-2015-320ak-t11273855.html#comment"> <em class="iconvalue">19</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/david-gilmour-rattle-that-lock-2015-320ak-t11273855.html" class="cellMainLink">David Gilmour - Rattle That Lock 2015 320ak</a></div>
			</td>
			<td class="nobr center" data-sort="128017133">122.09 <span>MB</span></td>
			<td class="center">25</td>
			<td class="center"><span title="17 Sep 2015, 13:54">2&nbsp;days</span></td>
			<td class="green center">866</td>
			<td class="red lasttd center">135</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11274911,0" class="icommentjs icon16" href="/keith-richards-crosseyed-heart-2015-320ak-t11274911.html#comment"> <em class="iconvalue">14</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/keith-richards-crosseyed-heart-2015-320ak-t11274911.html" class="cellMainLink">Keith Richards - Crosseyed Heart 2015 320ak</a></div>
			</td>
			<td class="nobr center" data-sort="143072634">136.44 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span title="17 Sep 2015, 18:10">2&nbsp;days</span></td>
			<td class="green center">722</td>
			<td class="red lasttd center">70</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11275141,0" class="icommentjs icon16" href="/chris-cornell-higher-truth-2015-320ak-t11275141.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/chris-cornell-higher-truth-2015-320ak-t11275141.html" class="cellMainLink">Chris Cornell - Higher Truth 2015 320ak</a></div>
			</td>
			<td class="nobr center" data-sort="160625521">153.18 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span title="17 Sep 2015, 19:13">2&nbsp;days</span></td>
			<td class="green center">569</td>
			<td class="red lasttd center">77</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11274737,0" class="icommentjs icon16" href="/billboard-hot-100-singles-chart-26th-september-2015-rz-rg-mp3-320kbps-glodls-t11274737.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/billboard-hot-100-singles-chart-26th-september-2015-rz-rg-mp3-320kbps-glodls-t11274737.html" class="cellMainLink">Billboard Hot 100 Singles Chart (26th September 2015) RZ-RG [MP3-320KBPS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="899768218">858.09 <span>MB</span></td>
			<td class="center">103</td>
			<td class="center"><span title="17 Sep 2015, 17:19">2&nbsp;days</span></td>
			<td class="green center">435</td>
			<td class="red lasttd center">323</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11273085,0" class="icommentjs icon16" href="/mp3-new-releases-2015-week-37-suprax-glodls-t11273085.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/mp3-new-releases-2015-week-37-suprax-glodls-t11273085.html" class="cellMainLink">MP3 NEW RELEASES 2015 WEEK 37 - SUPRAX [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="4144757891">3.86 <span>GB</span></td>
			<td class="center">578</td>
			<td class="center"><span title="17 Sep 2015, 10:47">2&nbsp;days</span></td>
			<td class="green center">400</td>
			<td class="red lasttd center">322</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11271763,0" class="icommentjs icon16" href="/young-thug-slime-season-320kbps-mixjoint-t11271763.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/young-thug-slime-season-320kbps-mixjoint-t11271763.html" class="cellMainLink">Young Thug - Slime Season [320Kbps] (MixJoint)</a></div>
			</td>
			<td class="nobr center" data-sort="169644915">161.79 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center"><span title="17 Sep 2015, 03:18">2&nbsp;days</span></td>
			<td class="green center">376</td>
			<td class="red lasttd center">51</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11269498,0" class="icommentjs icon16" href="/va-reggae-summer-jam-europe-2015-mp3-320-kbps-t11269498.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/va-reggae-summer-jam-europe-2015-mp3-320-kbps-t11269498.html" class="cellMainLink">VA - Reggae Summer Jam Europe (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="580429068">553.54 <span>MB</span></td>
			<td class="center">66</td>
			<td class="center"><span title="16 Sep 2015, 18:15">3&nbsp;days</span></td>
			<td class="green center">268</td>
			<td class="red lasttd center">66</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11269451,0" class="icommentjs icon16" href="/va-music-of-my-life-2015-mp3-320-kbps-t11269451.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/va-music-of-my-life-2015-mp3-320-kbps-t11269451.html" class="cellMainLink">VA - Music Of My Life (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="884629409">843.65 <span>MB</span></td>
			<td class="center">101</td>
			<td class="center"><span title="16 Sep 2015, 18:07">3&nbsp;days</span></td>
			<td class="green center">212</td>
			<td class="red lasttd center">110</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11278875,0" class="icommentjs icon16" href="/va-keep-calm-and-unwind-2015-mp3-320kbps-h4ckus-glodls-t11278875.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/va-keep-calm-and-unwind-2015-mp3-320kbps-h4ckus-glodls-t11278875.html" class="cellMainLink">VA - Keep Calm and Unwind [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="360752047">344.04 <span>MB</span></td>
			<td class="center">43</td>
			<td class="center"><span title="18 Sep 2015, 14:15">1&nbsp;day</span></td>
			<td class="green center">139</td>
			<td class="red lasttd center">42</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11269520,0" class="icommentjs icon16" href="/va-italo-disco-megamix-2015-mp3-320-kbps-t11269520.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/va-italo-disco-megamix-2015-mp3-320-kbps-t11269520.html" class="cellMainLink">VA - Italo Disco Megamix (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="1414499400">1.32 <span>GB</span></td>
			<td class="center">102</td>
			<td class="center"><span title="16 Sep 2015, 18:23">3&nbsp;days</span></td>
			<td class="green center">116</td>
			<td class="red lasttd center">58</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11274834,0" class="icommentjs icon16" href="/b-a-r-2015-week-37-glodls-t11274834.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/b-a-r-2015-week-37-glodls-t11274834.html" class="cellMainLink">B.A.R. 2015 Week 37 - [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="4569446775">4.26 <span>GB</span></td>
			<td class="center">559</td>
			<td class="center"><span title="17 Sep 2015, 17:43">2&nbsp;days</span></td>
			<td class="green center">97</td>
			<td class="red lasttd center">75</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11278734,0" class="icommentjs icon16" href="/va-ministry-of-sound-i-am-raver-2015-mp3-320kbps-h4ckus-glodls-t11278734.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/va-ministry-of-sound-i-am-raver-2015-mp3-320kbps-h4ckus-glodls-t11278734.html" class="cellMainLink">VA - Ministry of Sound : I Am Raver [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="1221685570">1.14 <span>GB</span></td>
			<td class="center">66</td>
			<td class="center"><span title="18 Sep 2015, 13:52">1&nbsp;day</span></td>
			<td class="green center">76</td>
			<td class="red lasttd center">57</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11275056,0" class="icommentjs icon16" href="/annihilator-suicide-society-deluxe-edition-2-cd-2015-320ak-t11275056.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/annihilator-suicide-society-deluxe-edition-2-cd-2015-320ak-t11275056.html" class="cellMainLink">Annihilator - Suicide Society (Deluxe Edition 2-CD) 2015 320ak</a></div>
			</td>
			<td class="nobr center" data-sort="249325586">237.78 <span>MB</span></td>
			<td class="center">29</td>
			<td class="center"><span title="17 Sep 2015, 18:47">2&nbsp;days</span></td>
			<td class="green center">94</td>
			<td class="red lasttd center">13</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11275408,0" class="icommentjs icon16" href="/atreyu-long-live-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11275408.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/atreyu-long-live-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11275408.html" class="cellMainLink">Atreyu - Long Live (2015) [MP3-320Kbps] [CBR] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="124234157">118.48 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span title="17 Sep 2015, 20:27">2&nbsp;days</span></td>
			<td class="green center">88</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11278315,0" class="icommentjs icon16" href="/lana-del-rey-honeymoon-us-version-2015-mp3-vbr-h4ckus-glodls-t11278315.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/lana-del-rey-honeymoon-us-version-2015-mp3-vbr-h4ckus-glodls-t11278315.html" class="cellMainLink">Lana Del Rey - Honeymoon (US Version) [2015] [MP3-VBR] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="111036728">105.89 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center"><span title="18 Sep 2015, 11:57">1&nbsp;day</span></td>
			<td class="green center">90</td>
			<td class="red lasttd center">19</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11272745,0" class="icommentjs icon16" href="/mad-max-v-1-0-1-1-3-dlc-2015-pc-repack-Ð¾Ñ-xatab-t11272745.html#comment"> <em class="iconvalue">129</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/mad-max-v-1-0-1-1-3-dlc-2015-pc-repack-Ð¾Ñ-xatab-t11272745.html" class="cellMainLink">Mad Max [v 1.0.1.1 + 3 DLC] (2015) PC | RePack Ð¾Ñ xatab</a></div>
			</td>
			<td class="nobr center" data-sort="24202197178">22.54 <span>GB</span></td>
			<td class="center">11</td>
			<td class="center"><span title="17 Sep 2015, 08:33">2&nbsp;days</span></td>
			<td class="green center">1502</td>
			<td class="red lasttd center">1523</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11276169,0" class="icommentjs icon16" href="/fifa-16-ntsc-xbox360-proton-t11276169.html#comment"> <em class="iconvalue">50</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/fifa-16-ntsc-xbox360-proton-t11276169.html" class="cellMainLink">FIFA 16 NTSC XBOX360-PROTON</a></div>
			</td>
			<td class="nobr center" data-sort="8444490963">7.86 <span>GB</span></td>
			<td class="center">87</td>
			<td class="center"><span title="18 Sep 2015, 02:23">1&nbsp;day</span></td>
			<td class="green center">369</td>
			<td class="red lasttd center">1242</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11277498,0" class="icommentjs icon16" href="/pes-2016-2015-pc-repack-by-r-g-freedom-t11277498.html#comment"> <em class="iconvalue">51</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/pes-2016-2015-pc-repack-by-r-g-freedom-t11277498.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/pes-2016-2015-pc-repack-by-r-g-freedom-t11277498.html" class="cellMainLink">PES 2016 2015 PC RePack by R G Freedom</a></div>
			</td>
			<td class="nobr center" data-sort="3420539362">3.19 <span>GB</span></td>
			<td class="center">8</td>
			<td class="center"><span title="18 Sep 2015, 08:45">1&nbsp;day</span></td>
			<td class="green center">453</td>
			<td class="red lasttd center">167</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11269055,0" class="icommentjs icon16" href="/skyshines-bedlam-codex-t11269055.html#comment"> <em class="iconvalue">34</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/skyshines-bedlam-codex-t11269055.html" class="cellMainLink">Skyshines.Bedlam-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="2330170934">2.17 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="16 Sep 2015, 16:45">3&nbsp;days</span></td>
			<td class="green center">210</td>
			<td class="red lasttd center">73</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11277256,0" class="icommentjs icon16" href="/fifa-15-ultimate-team-edition-cpy-t11277256.html#comment"> <em class="iconvalue">69</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/fifa-15-ultimate-team-edition-cpy-t11277256.html" class="cellMainLink">FIFA 15 Ultimate Team Edition-CPY</a></div>
			</td>
			<td class="nobr center" data-sort="13649134805">12.71 <span>GB</span></td>
			<td class="center">57</td>
			<td class="center"><span title="18 Sep 2015, 07:38">1&nbsp;day</span></td>
			<td class="green center">75</td>
			<td class="red lasttd center">258</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11278819,0" class="icommentjs icon16" href="/metal-gear-solid-v-the-phantom-pain-v1-0-0-5-multi8-repack-by-corepack-t11278819.html#comment"> <em class="iconvalue">120</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/metal-gear-solid-v-the-phantom-pain-v1-0-0-5-multi8-repack-by-corepack-t11278819.html" class="cellMainLink">Metal Gear Solid V: The Phantom Pain (v1.0.0.5) [Multi8] - RePack by CorePack</a></div>
			</td>
			<td class="nobr center" data-sort="12657894965">11.79 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="18 Sep 2015, 14:06">1&nbsp;day</span></td>
			<td class="green center">55</td>
			<td class="red lasttd center">232</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11270540,0" class="icommentjs icon16" href="/the-incredible-adventures-of-van-helsing-iii-gog-t11270540.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/the-incredible-adventures-of-van-helsing-iii-gog-t11270540.html" class="cellMainLink">The Incredible Adventures of Van Helsing III (GOG)</a></div>
			</td>
			<td class="nobr center" data-sort="10935062938">10.18 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="16 Sep 2015, 23:02">3&nbsp;days</span></td>
			<td class="green center">75</td>
			<td class="red lasttd center">188</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11275761,0" class="icommentjs icon16" href="/shovel-knight-incl-plague-of-shadows-dlc-2-9-0-16-gog-t11275761.html#comment"> <em class="iconvalue">20</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/shovel-knight-incl-plague-of-shadows-dlc-2-9-0-16-gog-t11275761.html" class="cellMainLink">Shovel Knight Incl Plague of Shadows DLC (2.9.0.16) (GOG)</a></div>
			</td>
			<td class="nobr center" data-sort="163069178">155.51 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="17 Sep 2015, 23:11">2&nbsp;days</span></td>
			<td class="green center">131</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11273024,0" class="icommentjs icon16" href="/midnight-calling-anabel-ce-2015-pc-final-t11273024.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/midnight-calling-anabel-ce-2015-pc-final-t11273024.html" class="cellMainLink">Midnight Calling: Anabel CE (2015) PC [FINAL]</a></div>
			</td>
			<td class="nobr center" data-sort="747320392">712.7 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="17 Sep 2015, 10:22">2&nbsp;days</span></td>
			<td class="green center">122</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11268928,0" class="icommentjs icon16" href="/hatred-update-10-1-dlc-2015-pc-repack-by-rg-mechanics-t11268928.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/hatred-update-10-1-dlc-2015-pc-repack-by-rg-mechanics-t11268928.html" class="cellMainLink">Hatred [Update 10 +1 DLC] (2015) PC | RePack by RG Mechanics</a></div>
			</td>
			<td class="nobr center" data-sort="1330139785">1.24 <span>GB</span></td>
			<td class="center">8</td>
			<td class="center"><span title="16 Sep 2015, 16:18">3&nbsp;days</span></td>
			<td class="green center">98</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11279808,0" class="icommentjs icon16" href="/fifa-16-super-deluxe-edition-3dm-t11279808.html#comment"> <em class="iconvalue">20</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/fifa-16-super-deluxe-edition-3dm-t11279808.html" class="cellMainLink">FIFA 16 Super Deluxe Edition-3DM</a></div>
			</td>
			<td class="nobr center" data-sort="20090317111">18.71 <span>GB</span></td>
			<td class="center">23</td>
			<td class="center"><span title="18 Sep 2015, 18:24">1&nbsp;day</span></td>
			<td class="green center">20</td>
			<td class="red lasttd center">145</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11269824,0" class="icommentjs icon16" href="/magicka-2-gates-of-midgaard-challenge-pack-skidrow-t11269824.html#comment"> <em class="iconvalue">17</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/magicka-2-gates-of-midgaard-challenge-pack-skidrow-t11269824.html" class="cellMainLink">Magicka 2 Gates of Midgaard Challenge Pack-SKIDROW</a></div>
			</td>
			<td class="nobr center" data-sort="2912268713">2.71 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="16 Sep 2015, 19:40">3&nbsp;days</span></td>
			<td class="green center">60</td>
			<td class="red lasttd center">44</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11280290,0" class="icommentjs icon16" href="/pc-fifa-16-super-deluxe-edition-rldgames-t11280290.html#comment"> <em class="iconvalue">25</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/pc-fifa-16-super-deluxe-edition-rldgames-t11280290.html" class="cellMainLink">[PC] FIFA 16 Super Deluxe Edition-RLDGAMES</a></div>
			</td>
			<td class="nobr center" data-sort="14123948699">13.15 <span>GB</span></td>
			<td class="center">12</td>
			<td class="center"><span title="18 Sep 2015, 20:31">1&nbsp;day</span></td>
			<td class="green center">9</td>
			<td class="red lasttd center">146</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11279003,0" class="icommentjs icon16" href="/pro-evolution-soccer-2016-multi7-ps3-unlimited-t11279003.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-Type"></i>
                    <a href="/pro-evolution-soccer-2016-multi7-ps3-unlimited-t11279003.html" class="cellMainLink">Pro Evolution Soccer 2016 MULTi7 PS3-UNLiMiTED</a></div>
			</td>
			<td class="nobr center" data-sort="9442021525">8.79 <span>GB</span></td>
			<td class="center">55</td>
			<td class="center"><span title="18 Sep 2015, 14:52">1&nbsp;day</span></td>
			<td class="green center">17</td>
			<td class="red lasttd center">124</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11269447,0" class="icommentjs icon16" href="/ride-digital-deluxe-edition-4-dlc-update-2-true-multi10-fitgirl-repack-t11269447.html#comment"> <em class="iconvalue">20</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/ride-digital-deluxe-edition-4-dlc-update-2-true-multi10-fitgirl-repack-t11269447.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/ride-digital-deluxe-edition-4-dlc-update-2-true-multi10-fitgirl-repack-t11269447.html" class="cellMainLink">RIDE: Digital Deluxe Edition (4 DLC, Update 2, True MULTI10) [FitGirl Repack]</a></div>
			</td>
			<td class="nobr center" data-sort="8997103751">8.38 <span>GB</span></td>
			<td class="center">11</td>
			<td class="center"><span title="16 Sep 2015, 18:07">3&nbsp;days</span></td>
			<td class="green center">37</td>
			<td class="red lasttd center">68</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11275020,0" class="icommentjs icon16" href="/avast-pro-antivirus-internet-security-10-4-2233-final-incl-lic-team-os-t11275020.html#comment"> <em class="iconvalue">33</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/avast-pro-antivirus-internet-security-10-4-2233-final-incl-lic-team-os-t11275020.html" class="cellMainLink">avast! Pro Antivirus &amp; Internet Security 10.4.2233 Final incl Lic-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="404709734">385.96 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="17 Sep 2015, 18:40">2&nbsp;days</span></td>
			<td class="green center">279</td>
			<td class="red lasttd center">168</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11267055,0" class="icommentjs icon16" href="/windows-7-infinium-v-5-2015-x64-pre-activated-team-os-t11267055.html#comment"> <em class="iconvalue">32</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/windows-7-infinium-v-5-2015-x64-pre-activated-team-os-t11267055.html" class="cellMainLink">Windows 7 Infinium V.5 2015 X64 Pre-activated-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="7558052358">7.04 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="16 Sep 2015, 10:15">3&nbsp;days</span></td>
			<td class="green center">147</td>
			<td class="red lasttd center">268</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11278655,0" class="icommentjs icon16" href="/avg-internet-security-2016-16-0-7134-x86-x64-serials-techtools-t11278655.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/avg-internet-security-2016-16-0-7134-x86-x64-serials-techtools-t11278655.html" class="cellMainLink">AVG Internet Security 2016 16.0.7134 (x86 - x64) + Serials [TechTools]</a></div>
			</td>
			<td class="nobr center" data-sort="453201635">432.21 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="18 Sep 2015, 13:23">1&nbsp;day</span></td>
			<td class="green center">194</td>
			<td class="red lasttd center">158</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11273036,0" class="icommentjs icon16" href="/folder-lock-7-5-5-key-4realtorrentz-t11273036.html#comment"> <em class="iconvalue">14</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/folder-lock-7-5-5-key-4realtorrentz-t11273036.html" class="cellMainLink">Folder Lock 7.5.5 + Key [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="9539338">9.1 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="17 Sep 2015, 10:27">2&nbsp;days</span></td>
			<td class="green center">156</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11274298,0" class="icommentjs icon16" href="/topaz-labs-photoshop-plugins-bundle-2015-31-08-2015-en-keys-appzdam-t11274298.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/topaz-labs-photoshop-plugins-bundle-2015-31-08-2015-en-keys-appzdam-t11274298.html" class="cellMainLink">Topaz Labs Photoshop Plugins Bundle 2015 (31.08.2015) [En] + Keys - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="857195119">817.48 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="17 Sep 2015, 15:39">2&nbsp;days</span></td>
			<td class="green center">104</td>
			<td class="red lasttd center">47</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11265706,0" class="icommentjs icon16" href="/destroy-windows-10-spying-1-5-0-build-361-portable-4realtorrentz-t11265706.html#comment"> <em class="iconvalue">20</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/destroy-windows-10-spying-1-5-0-build-361-portable-4realtorrentz-t11265706.html" class="cellMainLink">Destroy Windows 10 Spying 1.5.0 Build 361 Portable [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="214715">209.68 <span>KB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="16 Sep 2015, 04:00">3&nbsp;days</span></td>
			<td class="green center">115</td>
			<td class="red lasttd center">9</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11277154,0" class="icommentjs icon16" href="/total-commander-8-52a-final-multilanguage-wincmd-key-portable-at-team-t11277154.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/total-commander-8-52a-final-multilanguage-wincmd-key-portable-at-team-t11277154.html" class="cellMainLink">Total Commander 8.52a Final [Multilanguage] [Wincmd.key] [+Portable] [AT-TEAM]</a></div>
			</td>
			<td class="nobr center" data-sort="12406041">11.83 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="18 Sep 2015, 07:11">1&nbsp;day</span></td>
			<td class="green center">77</td>
			<td class="red lasttd center">7</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11265168,0" class="icommentjs icon16" href="/daemon-tools-pro-advanced-6-2-0-0496-x32-x64-multi-activator-t11265168.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/daemon-tools-pro-advanced-6-2-0-0496-x32-x64-multi-activator-t11265168.html" class="cellMainLink">DAEMON Tools Pro Advanced 6.2.0.0496 (x32/x64)[Multi][Activator]</a></div>
			</td>
			<td class="nobr center" data-sort="34366795">32.77 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="16 Sep 2015, 01:19">3&nbsp;days</span></td>
			<td class="green center">67</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11270360,0" class="icommentjs icon16" href="/graphicriver-7325557-realistic-t-shirt-mock-up-pack-photoshop-project-files-fluck3r-t11270360.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/graphicriver-7325557-realistic-t-shirt-mock-up-pack-photoshop-project-files-fluck3r-t11270360.html" class="cellMainLink">[Graphicriver][7325557] Realistic T-shirt Mock-up Pack - Photoshop Project Files [Fluck3r]</a></div>
			</td>
			<td class="nobr center" data-sort="1004414621">957.88 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="16 Sep 2015, 22:01">3&nbsp;days</span></td>
			<td class="green center">43</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11275685,0" class="icommentjs icon16" href="/gospel-musicians-neo-soul-keys-new-3x-version-kontakt-t11275685.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/gospel-musicians-neo-soul-keys-new-3x-version-kontakt-t11275685.html" class="cellMainLink">Gospel Musicians Neo-Soul Keys NEW 3X Version KONTAKT</a></div>
			</td>
			<td class="nobr center" data-sort="6202404793">5.78 <span>GB</span></td>
			<td class="center">8</td>
			<td class="center"><span title="17 Sep 2015, 22:23">2&nbsp;days</span></td>
			<td class="green center">36</td>
			<td class="red lasttd center">28</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11270326,0" class="icommentjs icon16" href="/creativemarket-246034-mega-empire-powerpoint-bundle-templates-fluck3r-t11270326.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/creativemarket-246034-mega-empire-powerpoint-bundle-templates-fluck3r-t11270326.html" class="cellMainLink">[Creativemarket][246034] Mega Empire - Powerpoint Bundle - Templates [Fluck3r]</a></div>
			</td>
			<td class="nobr center" data-sort="1710068249">1.59 <span>GB</span></td>
			<td class="center">11</td>
			<td class="center"><span title="16 Sep 2015, 21:58">3&nbsp;days</span></td>
			<td class="green center">33</td>
			<td class="red lasttd center">33</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pictureType"></i>
                    <a href="/daz3d-poser-111086-federica-t11280374.html" class="cellMainLink">Daz3D - Poser - 111086_Federica</a></div>
			</td>
			<td class="nobr center" data-sort="156233615">149 <span>MB</span></td>
			<td class="center">251</td>
			<td class="center"><span title="18 Sep 2015, 21:03">1&nbsp;day</span></td>
			<td class="green center">35</td>
			<td class="red lasttd center">20</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11278030,0" class="icommentjs icon16" href="/avg-pc-tuneup-2016-v16-2-1-18873-x86-x64-final-multilanguage-keygen-b-tman-t11278030.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/avg-pc-tuneup-2016-v16-2-1-18873-x86-x64-final-multilanguage-keygen-b-tman-t11278030.html" class="cellMainLink">AVG PC Tuneup 2016 v16.2.1.18873 (x86 &amp; x64) Final Multilanguage + Keygen {B@tman}</a></div>
			</td>
			<td class="nobr center" data-sort="223703698">213.34 <span>MB</span></td>
			<td class="center">7</td>
			<td class="center"><span title="18 Sep 2015, 10:41">1&nbsp;day</span></td>
			<td class="green center">38</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11281334,0" class="icommentjs icon16" href="/daz3d-kelly-for-victoria-7-t11281334.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/daz3d-kelly-for-victoria-7-t11281334.html" class="cellMainLink">Daz3D - Kelly for Victoria 7</a></div>
			</td>
			<td class="nobr center" data-sort="118079222">112.61 <span>MB</span></td>
			<td class="center">9</td>
			<td class="center"><span title="19 Sep 2015, 01:52">21&nbsp;hours</span></td>
			<td class="green center">32</td>
			<td class="red lasttd center">13</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11279817,0" class="icommentjs icon16" href="/windows-10-pro-insider-preview-build-10547-x64-en-us-t11279817.html#comment"> <em class="iconvalue">24</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/windows-10-pro-insider-preview-build-10547-x64-en-us-t11279817.html" class="cellMainLink">Windows 10 Pro | Insider Preview | Build 10547 | x64 | en-us</a></div>
			</td>
			<td class="nobr center" data-sort="2922020864">2.72 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="18 Sep 2015, 18:27">1&nbsp;day</span></td>
			<td class="green center">19</td>
			<td class="red lasttd center">38</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11279624,0" class="icommentjs icon16" href="/leopard-raws-gate-jieitai-kanochi-nite-kaku-tatakaeri-12-end-mx-1280x720-x264-aac-mp4-t11279624.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/leopard-raws-gate-jieitai-kanochi-nite-kaku-tatakaeri-12-end-mx-1280x720-x264-aac-mp4-t11279624.html" class="cellMainLink">[Leopard-Raws] Gate - Jieitai Kanochi nite, Kaku Tatakaeri - 12 END (MX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="418242323">398.87 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="18 Sep 2015, 17:40">1&nbsp;day</span></td>
			<td class="green center">1676</td>
			<td class="red lasttd center">407</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11275856,0" class="icommentjs icon16" href="/fansub-resistance-naruto-shippuuden-430-french-subbed-1280x720-mp4-t11275856.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/fansub-resistance-naruto-shippuuden-430-french-subbed-1280x720-mp4-t11275856.html" class="cellMainLink">[Fansub-Resistance]Naruto Shippuuden 430 [French Subbed] (1280x720).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="282966923">269.86 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="18 Sep 2015, 00:07">1&nbsp;day</span></td>
			<td class="green center">1629</td>
			<td class="red lasttd center">160</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11273168,0" class="icommentjs icon16" href="/horriblesubs-naruto-shippuuden-430-720p-mkv-t11273168.html#comment"> <em class="iconvalue">21</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/horriblesubs-naruto-shippuuden-430-720p-mkv-t11273168.html" class="cellMainLink">[HorribleSubs] Naruto Shippuuden - 430 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="330803311">315.48 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="17 Sep 2015, 11:10">2&nbsp;days</span></td>
			<td class="green center">1494</td>
			<td class="red lasttd center">133</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/leopard-raws-himouto-umaru-chan-11-raw-abc-1280x720-x264-aac-mp4-t11271900.html" class="cellMainLink">[Leopard-Raws] Himouto! Umaru-chan - 11 RAW (ABC 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="383614520">365.84 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="17 Sep 2015, 04:15">2&nbsp;days</span></td>
			<td class="green center">909</td>
			<td class="red lasttd center">68</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/leopard-raws-joukamachi-no-dandelion-12-end-tbs-1280x720-x264-aac-mp4-t11276497.html" class="cellMainLink">[Leopard-Raws] Joukamachi no Dandelion - 12 END (TBS 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="357569146">341 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="18 Sep 2015, 04:15">1&nbsp;day</span></td>
			<td class="green center">481</td>
			<td class="red lasttd center">108</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11269604,0" class="icommentjs icon16" href="/leopard-raws-kuusen-madoushi-kouhosei-no-kyoukan-11-raw-sun-1280x720-x264-aac-mp4-t11269604.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/leopard-raws-kuusen-madoushi-kouhosei-no-kyoukan-11-raw-sun-1280x720-x264-aac-mp4-t11269604.html" class="cellMainLink">[Leopard-Raws] Kuusen Madoushi Kouhosei no Kyoukan - 11 RAW (SUN 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="476489297">454.42 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="16 Sep 2015, 18:45">3&nbsp;days</span></td>
			<td class="green center">479</td>
			<td class="red lasttd center">56</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/leopard-raws-ranpo-kitan-game-of-laplace-11-end-cx-1280x720-x264-aac-mp4-t11276495.html" class="cellMainLink">[Leopard-Raws] Ranpo Kitan - Game of Laplace - 11 END (CX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="623662602">594.77 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="18 Sep 2015, 04:15">1&nbsp;day</span></td>
			<td class="green center">436</td>
			<td class="red lasttd center">133</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/leopard-raws-miss-monochrome-the-animation-2-12-raw-atx-1280x720-x264-aac-mp4-t11279501.html" class="cellMainLink">[Leopard-Raws] Miss Monochrome The Animation 2 - 12 RAW (ATX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="129771513">123.76 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="18 Sep 2015, 17:10">1&nbsp;day</span></td>
			<td class="green center">355</td>
			<td class="red lasttd center">72</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11273502,0" class="icommentjs icon16" href="/naruto-shippuden-430-eng-sub-480p-l-mbert-t11273502.html#comment"> <em class="iconvalue">24</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/naruto-shippuden-430-eng-sub-480p-l-mbert-t11273502.html" class="cellMainLink">Naruto Shippuden 430 [EnG SuB] 480p L@mBerT</a></div>
			</td>
			<td class="nobr center" data-sort="69110673">65.91 <span>MB</span></td>
			<td class="center">10</td>
			<td class="center"><span title="17 Sep 2015, 12:31">2&nbsp;days</span></td>
			<td class="green center">299</td>
			<td class="red lasttd center">132</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/vivid-gakkou-gurashi-11-96b024af-mkv-t11278123.html" class="cellMainLink">[Vivid] Gakkou Gurashi! - 11 [96B024AF].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="405487635">386.7 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="18 Sep 2015, 11:05">1&nbsp;day</span></td>
			<td class="green center">251</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/leopard-raws-okusama-ga-seito-kaichou-12-end-atx-1280x720-x264-aac-mp4-t11271903.html" class="cellMainLink">[Leopard-Raws] Okusama ga Seito Kaichou! - 12 END (ATX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="130224036">124.19 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="17 Sep 2015, 04:15">2&nbsp;days</span></td>
			<td class="green center">255</td>
			<td class="red lasttd center">19</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11275855,0" class="icommentjs icon16" href="/deadfish-dragon-ball-super-10-720p-aac-mp4-t11275855.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/deadfish-dragon-ball-super-10-720p-aac-mp4-t11275855.html" class="cellMainLink">[DeadFish] Dragon Ball Super - 10 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="487791381">465.19 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="18 Sep 2015, 00:05">1&nbsp;day</span></td>
			<td class="green center">188</td>
			<td class="red lasttd center">49</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/leopard-raws-suzakinishi-the-animation-11-raw-mx-1280x720-x264-aac-mp4-t11269225.html" class="cellMainLink">[Leopard-Raws] Suzakinishi The Animation - 11 RAW (MX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="67599071">64.47 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="16 Sep 2015, 17:20">3&nbsp;days</span></td>
			<td class="green center">132</td>
			<td class="red lasttd center">7</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/bakedfish-prison-school-11-720p-aac-mp4-t11279871.html" class="cellMainLink">[BakedFish] Prison School - 11 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="236979665">226 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="18 Sep 2015, 18:43">1&nbsp;day</span></td>
			<td class="green center">108</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/commie-prison-school-06-7ccced09-mkv-t11280358.html" class="cellMainLink">[Commie] Prison School - 06 [7CCCED09].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="210031877">200.3 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="18 Sep 2015, 20:55">1&nbsp;day</span></td>
			<td class="green center">87</td>
			<td class="red lasttd center">20</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11268647,0" class="icommentjs icon16" href="/marvel-week-09-16-2015-nem-t11268647.html#comment"> <em class="iconvalue">31</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/marvel-week-09-16-2015-nem-t11268647.html" class="cellMainLink">Marvel Week+ (09-16-2015) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="664693263">633.9 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span title="16 Sep 2015, 15:19">3&nbsp;days</span></td>
			<td class="green center">588</td>
			<td class="red lasttd center">224</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11269709,0" class="icommentjs icon16" href="/dc-week-09-16-2015-aka-dc-you-week-16-nem-t11269709.html#comment"> <em class="iconvalue">25</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/dc-week-09-16-2015-aka-dc-you-week-16-nem-t11269709.html" class="cellMainLink">DC Week+ (09-16-2015) (aka DC YOU Week 16) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="467787348">446.12 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span title="16 Sep 2015, 19:13">3&nbsp;days</span></td>
			<td class="green center">402</td>
			<td class="red lasttd center">141</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11265530,0" class="icommentjs icon16" href="/assorted-magazines-bundle-september-16-2015-true-pdf-t11265530.html#comment"> <em class="iconvalue">14</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/assorted-magazines-bundle-september-16-2015-true-pdf-t11265530.html" class="cellMainLink">Assorted Magazines Bundle - September 16 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="267237758">254.86 <span>MB</span></td>
			<td class="center">19</td>
			<td class="center"><span title="16 Sep 2015, 02:58">3&nbsp;days</span></td>
			<td class="green center">245</td>
			<td class="red lasttd center">112</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11264346,0" class="icommentjs icon16" href="/scientific-american-september-2015-t11264346.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/scientific-american-september-2015-t11264346.html" class="cellMainLink">Scientific American - September 2015</a></div>
			</td>
			<td class="nobr center" data-sort="20330907">19.39 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="15 Sep 2015, 23:28">3&nbsp;days</span></td>
			<td class="green center">257</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11264504,0" class="icommentjs icon16" href="/computer-gadget-gamer-mags-september-16-2015-true-pdf-t11264504.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/computer-gadget-gamer-mags-september-16-2015-true-pdf-t11264504.html" class="cellMainLink">Computer Gadget &amp; Gamer Mags - September 16 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="78772408">75.12 <span>MB</span></td>
			<td class="center">8</td>
			<td class="center"><span title="16 Sep 2015, 00:36">3&nbsp;days</span></td>
			<td class="green center">232</td>
			<td class="red lasttd center">51</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11270763,0" class="icommentjs icon16" href="/automobile-truck-magazines-sept-17-2015-true-pdf-t11270763.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/automobile-truck-magazines-sept-17-2015-true-pdf-t11270763.html" class="cellMainLink">Automobile &amp; Truck Magazines - Sept 17 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="608288184">580.11 <span>MB</span></td>
			<td class="center">30</td>
			<td class="center"><span title="17 Sep 2015, 00:30">2&nbsp;days</span></td>
			<td class="green center">201</td>
			<td class="red lasttd center">69</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11265365,0" class="icommentjs icon16" href="/national-geographic-october-2015-usa-t11265365.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/national-geographic-october-2015-usa-t11265365.html" class="cellMainLink">National Geographic - October 2015 USA</a></div>
			</td>
			<td class="nobr center" data-sort="18840566">17.97 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="16 Sep 2015, 02:03">3&nbsp;days</span></td>
			<td class="green center">219</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11274618,0" class="icommentjs icon16" href="/top-20-usa-today-s-best-selling-books-09-13-15-epub-mobi-t11274618.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/top-20-usa-today-s-best-selling-books-09-13-15-epub-mobi-t11274618.html" class="cellMainLink">TOP 20 USA TODAY&#039;s Best-Selling Books 09-13-15 [.epub, .mobi]</a></div>
			</td>
			<td class="nobr center" data-sort="56547910">53.93 <span>MB</span></td>
			<td class="center">43</td>
			<td class="center"><span title="17 Sep 2015, 16:49">2&nbsp;days</span></td>
			<td class="green center">189</td>
			<td class="red lasttd center">55</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11266681,0" class="icommentjs icon16" href="/0-day-week-of-2015-09-09-t11266681.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/0-day-week-of-2015-09-09-t11266681.html" class="cellMainLink">0-Day Week of 2015.09.09</a></div>
			</td>
			<td class="nobr center" data-sort="9510416481">8.86 <span>GB</span></td>
			<td class="center">176</td>
			<td class="center"><span title="16 Sep 2015, 08:31">3&nbsp;days</span></td>
			<td class="green center">81</td>
			<td class="red lasttd center">102</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11270310,0" class="icommentjs icon16" href="/tokyo-ghost-001-2015-digital-d-argh-empire-cbr-nem-t11270310.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/tokyo-ghost-001-2015-digital-d-argh-empire-cbr-nem-t11270310.html" class="cellMainLink">Tokyo Ghost 001 (2015) (digital) (d&#039;argh-Empire).cbr (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="72000429">68.66 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="16 Sep 2015, 21:57">3&nbsp;days</span></td>
			<td class="green center">127</td>
			<td class="red lasttd center">5</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11267904,0" class="icommentjs icon16" href="/invincible-123-2015-digital-minutemen-faessla-cbz-t11267904.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-Type"></i>
                    <a href="/invincible-123-2015-digital-minutemen-faessla-cbz-t11267904.html" class="cellMainLink">Invincible 123 (2015) (digital) (Minutemen-Faessla).cbz</a></div>
			</td>
			<td class="nobr center" data-sort="32221742">30.73 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="16 Sep 2015, 13:56">3&nbsp;days</span></td>
			<td class="green center">112</td>
			<td class="red lasttd center">4</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11275529,0" class="icommentjs icon16" href="/image-week-09-16-2015-nem-t11275529.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/image-week-09-16-2015-nem-t11275529.html" class="cellMainLink">Image Week (09-16-2015) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="1029380366">981.69 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span title="17 Sep 2015, 21:16">2&nbsp;days</span></td>
			<td class="green center">87</td>
			<td class="red lasttd center">50</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11275955,0" class="icommentjs icon16" href="/x-men-v2-v4-legacy-v1-v2-new-x-men-v1-v2-all-new-x-men-1991-2015-scans-39-digital-minutemen-nem-t11275955.html#comment"> <em class="iconvalue">14</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/x-men-v2-v4-legacy-v1-v2-new-x-men-v1-v2-all-new-x-men-1991-2015-scans-39-digital-minutemen-nem-t11275955.html" class="cellMainLink">X-Men (v2-v4+Legacy v1-v2,New X-Men v1-v2 &amp; All-New X-Men) (1991-2015) (scans(39)+digital) (Minutemen) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="17030693859">15.86 <span>GB</span></td>
			<td class="center">615</td>
			<td class="center"><span title="18 Sep 2015, 01:00">1&nbsp;day</span></td>
			<td class="green center">41</td>
			<td class="red lasttd center">139</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11281069,0" class="icommentjs icon16" href="/hacking-basic-security-penetration-testing-and-how-to-hack-2015-pdf-mobi-gooner-t11281069.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/hacking-basic-security-penetration-testing-and-how-to-hack-2015-pdf-mobi-gooner-t11281069.html" class="cellMainLink">Hacking - Basic Security, Penetration Testing and How to Hack (2015) (Pdf &amp; Mobi) Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="638876">623.9 <span>KB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="19 Sep 2015, 00:06">23&nbsp;hours</span></td>
			<td class="green center">90</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11277493,0" class="icommentjs icon16" href="/the-principia-mathematical-principles-of-natural-philosophy-1999-pdf-gooner-t11277493.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/the-principia-mathematical-principles-of-natural-philosophy-1999-pdf-gooner-t11277493.html" class="cellMainLink">The Principia - Mathematical Principles of Natural Philosophy (1999).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="23935909">22.83 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="18 Sep 2015, 08:42">1&nbsp;day</span></td>
			<td class="green center">91</td>
			<td class="red lasttd center">10</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11273046,0" class="icommentjs icon16" href="/david-gilmour-rattle-that-lock-2015-flac-t11273046.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/david-gilmour-rattle-that-lock-2015-flac-t11273046.html" class="cellMainLink">David Gilmour - Rattle That Lock (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="309954594">295.6 <span>MB</span></td>
			<td class="center">10</td>
			<td class="center"><span title="17 Sep 2015, 10:29">2&nbsp;days</span></td>
			<td class="green center">319</td>
			<td class="red lasttd center">40</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11273044,0" class="icommentjs icon16" href="/keith-richards-crosseyed-heart-2015-flac-t11273044.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/keith-richards-crosseyed-heart-2015-flac-t11273044.html" class="cellMainLink">Keith Richards - Crosseyed Heart (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="405680121">386.89 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span title="17 Sep 2015, 10:28">2&nbsp;days</span></td>
			<td class="green center">199</td>
			<td class="red lasttd center">40</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11269766,0" class="icommentjs icon16" href="/lana-del-rey-honeymoon-2015-flac-t11269766.html#comment"> <em class="iconvalue">13</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/lana-del-rey-honeymoon-2015-flac-t11269766.html" class="cellMainLink">Lana Del Rey - Honeymoon (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="379383260">361.81 <span>MB</span></td>
			<td class="center">19</td>
			<td class="center"><span title="16 Sep 2015, 19:24">3&nbsp;days</span></td>
			<td class="green center">147</td>
			<td class="red lasttd center">46</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11273042,0" class="icommentjs icon16" href="/scorpions-return-to-forever-sony-legacy-edition-2015-t11273042.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/scorpions-return-to-forever-sony-legacy-edition-2015-t11273042.html" class="cellMainLink">Scorpions - Return To Forever (Sony Legacy Edition) 2015</a></div>
			</td>
			<td class="nobr center" data-sort="642999267">613.21 <span>MB</span></td>
			<td class="center">21</td>
			<td class="center"><span title="17 Sep 2015, 10:28">2&nbsp;days</span></td>
			<td class="green center">117</td>
			<td class="red lasttd center">19</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/va-the-ultimate-collection-gospel-4-cd-boxed-set-flac-t11272682.html" class="cellMainLink">VA - The Ultimate Collection : Gospel 4 CD Boxed Set [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="934897623">891.59 <span>MB</span></td>
			<td class="center">64</td>
			<td class="center"><span title="17 Sep 2015, 08:19">2&nbsp;days</span></td>
			<td class="green center">85</td>
			<td class="red lasttd center">39</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11281187,0" class="icommentjs icon16" href="/best-of-bond-james-bond-50th-anniversary-collection-flac-tntvillage-t11281187.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/best-of-bond-james-bond-50th-anniversary-collection-flac-tntvillage-t11281187.html" class="cellMainLink">Best Of Bond James Bond 50th Anniversary Collection [Flac][TntVillage]</a></div>
			</td>
			<td class="nobr center" data-sort="536584060">511.73 <span>MB</span></td>
			<td class="center">31</td>
			<td class="center"><span title="19 Sep 2015, 00:46">22&nbsp;hours</span></td>
			<td class="green center">78</td>
			<td class="red lasttd center">51</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11275142,0" class="icommentjs icon16" href="/art-blakey-a-night-at-birdland-vol-2-2014-24-192-hd-flac-t11275142.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/art-blakey-a-night-at-birdland-vol-2-2014-24-192-hd-flac-t11275142.html" class="cellMainLink">Art Blakey - A Night At Birdland Vol. 2 (2014) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1895189902">1.77 <span>GB</span></td>
			<td class="center">16</td>
			<td class="center"><span title="17 Sep 2015, 19:13">2&nbsp;days</span></td>
			<td class="green center">90</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11267775,0" class="icommentjs icon16" href="/keith-jarrett-bop-be-2015-24-192-hd-flac-t11267775.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/keith-jarrett-bop-be-2015-24-192-hd-flac-t11267775.html" class="cellMainLink">Keith Jarrett - Bop-Be (2015) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1575138436">1.47 <span>GB</span></td>
			<td class="center">20</td>
			<td class="center"><span title="16 Sep 2015, 13:37">3&nbsp;days</span></td>
			<td class="green center">83</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/va-nostalgie-the-summer-feeling-5cd-2015-flac-t11272714.html" class="cellMainLink">VA - Nostalgie - The Summer Feeling [5CD] (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="2792868296">2.6 <span>GB</span></td>
			<td class="center">132</td>
			<td class="center"><span title="17 Sep 2015, 08:26">2&nbsp;days</span></td>
			<td class="green center">71</td>
			<td class="red lasttd center">33</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11269541,0" class="icommentjs icon16" href="/john-coltrane-giant-steps-2014-24-192-hd-flac-t11269541.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/john-coltrane-giant-steps-2014-24-192-hd-flac-t11269541.html" class="cellMainLink">John Coltrane - Giant Steps (2014) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="691996134">659.94 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center"><span title="16 Sep 2015, 18:30">3&nbsp;days</span></td>
			<td class="green center">75</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11277105,0" class="icommentjs icon16" href="/bob-dylan-johnny-cash-nashville-1969-flac-t11277105.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/bob-dylan-johnny-cash-nashville-1969-flac-t11277105.html" class="cellMainLink">Bob Dylan &amp; Johnny Cash - Nashville 1969 [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="363116797">346.3 <span>MB</span></td>
			<td class="center">30</td>
			<td class="center"><span title="18 Sep 2015, 06:58">1&nbsp;day</span></td>
			<td class="green center">77</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11280162,0" class="icommentjs icon16" href="/thelonious-monk-quartet-misterioso-2012-24-192-hd-flac-t11280162.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/thelonious-monk-quartet-misterioso-2012-24-192-hd-flac-t11280162.html" class="cellMainLink">Thelonious Monk Quartet - Misterioso (2012) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="3036919875">2.83 <span>GB</span></td>
			<td class="center">25</td>
			<td class="center"><span title="18 Sep 2015, 20:01">1&nbsp;day</span></td>
			<td class="green center">66</td>
			<td class="red lasttd center">34</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11278502,0" class="icommentjs icon16" href="/bill-evans-trio-waltz-for-debby-2011-24-192-hd-flac-t11278502.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/bill-evans-trio-waltz-for-debby-2011-24-192-hd-flac-t11278502.html" class="cellMainLink">Bill Evans Trio - Waltz For Debby (2011) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="2432590637">2.27 <span>GB</span></td>
			<td class="center">16</td>
			<td class="center"><span title="18 Sep 2015, 12:47">1&nbsp;day</span></td>
			<td class="green center">57</td>
			<td class="red lasttd center">19</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/various-artists-cine-classic-standards-flac-tntvillage-t11270694.html" class="cellMainLink">Various Artists - Cine Classic Standards [Flac][TntVillage]</a></div>
			</td>
			<td class="nobr center" data-sort="227349422">216.82 <span>MB</span></td>
			<td class="center">23</td>
			<td class="center"><span title="16 Sep 2015, 23:57">2&nbsp;days</span></td>
			<td class="green center">58</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11277501,0" class="icommentjs icon16" href="/rolling-stones-live-1965-music-from-charlie-is-my-darling-2014-24-192-hd-flac-t11277501.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/rolling-stones-live-1965-music-from-charlie-is-my-darling-2014-24-192-hd-flac-t11277501.html" class="cellMainLink">Rolling Stones - Live 1965 Music From Charlie Is My Darling (2014) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1938854996">1.81 <span>GB</span></td>
			<td class="center">30</td>
			<td class="center"><span title="18 Sep 2015, 08:46">1&nbsp;day</span></td>
			<td class="green center">52</td>
			<td class="red lasttd center">20</td>
        </tr>
			</table>


	<script type="text/javascript">
		if ($.cookie('kat_settings[fpw_column]') != null && $.cookie('kat_settings[fpw_order]')) {
			var colIndex = $.cookie('kat_settings[fpw_column]')
			var ascend = $.cookie('kat_settings[fpw_order]');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			sortTables(colIndex, ascend);
		}
		$('.frontPageWidget .firstr th .fpw-sort').click(function() {
			var colIndex = $(this).parent().index();
			var ascend = !$(this).hasClass('sortedAsc')?1:0;
			$('.fpw-sort').removeClass('sortedAsc sortedDesc');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			updateFPWsettings(colIndex, ascend);
			sortTables(colIndex, ascend);
		});
		function sortTables(colIndex, ascend) {	
			$('.frontPageWidget').each(function() {
				var data = [];
				$('tr:not(.firstr)', $(this)).each(function() {
					var name = $('td:first .cellMainLink', $(this)).text();
					var id = $('td:first .cellMainLink', $(this)).attr('href').match(/.*-t(\d+)\.html/)[1];
					var fileSize = $('td:eq(1)', $(this)).attr('data-sort');
					data.push({
						'col0':name, 'col1':padNum(fileSize), 'col2':padNum($('td:eq(2)',$(this)).text()), 'col3':padNum(id), 'col4':padNum($('td:eq(4)',$(this)).text()), 'col5':padNum($('td:eq(5)',$(this)).text()), 'html':$(this).html()
					});
				});
				data.sort(function(a, b) {
					var x = a['col'+colIndex].toLowerCase(); var y = b['col'+colIndex].toLowerCase();
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
				if (!ascend||ascend=='0') data.reverse();
				$(this).find('tr:not(.firstr)').remove();
				for (var i=0;i<data.length;i++) {
					$(this).append('<tr class="'+(i+1%2==0?'even':'odd')+'">'+data[i].html+'</tr>');
				}
			});
		}
		function padNum(val) {
			val = '0000000000000000'+val;
			return val.substring(val.length-15);
		}
		function updateFPWsettings(t, o) {
			$.cookie('kat_settings[fpw_column]', t, { expires: 365, path: '/' });
			$.cookie('kat_settings[fpw_order]', o, { expires: 365, path: '/' });
		}
	</script>
		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
    
            
        <div id="_119b0a17fab5493361a252d04bf527db"></div>
    
                
    	    <div class="advertising">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <div id="_7063408f1c01d50e0dc2d833186ce962"></div>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/applicants-wanted-languages-within-translation-department-se/?unread=16914217">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Applicants wanted for Languages within the Translation Department *September 2015
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/zi69y/">zi69y</a></span></span> <span title="19 Sep 2015, 23:15">2&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/shout-out-mods-and-uploaders-making-super-resourceful-place/?unread=16914216">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				A shout out to the mods and uploaders for making this a super resourceful place!
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_3"><a class="plain" href="/user/zeke23/">zeke23</a></span></span> <span title="19 Sep 2015, 23:15">2&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/please-request-ebooks-and-audio-books-here-v11/?unread=16914213">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Please request ebooks and audio books here. V11
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/BadBuddha365/">BadBuddha365</a></span></span> <span title="19 Sep 2015, 23:11">6&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/specific-page-search/?unread=16914212">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Specific page search?
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/DragonAttack/">DragonAttack</a></span></span> <span title="19 Sep 2015, 23:11">6&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/torrents-need-updating-kat-changing-mod-work-thread-only-v5/?unread=16914209">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Torrents that need updating / KAT changing/ Mod Work Thread Only..V5
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_2"><a class="plain" href="/user/Touro73/">Touro73</a></span></span> <span title="19 Sep 2015, 23:08">8&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/adopt-uploader-program-v11-all-users-help/?unread=16914205">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				**Adopt an uploader Program v11-- For all Users to Help**
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_verified"><a class="plain" href="/user/KayOs-GFX/">KayOs-GFX</a></span></span> <span title="19 Sep 2015, 23:06">11&nbsp;min.&nbsp;ago</span></span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/summer-updates-september-1/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Summer updates [September, 1]
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="01 Sep 2015, 16:13">2&nbsp;weeks&nbsp;ago</span></span>
	</li>
	<li>
		<a href="/blog/post/kickasstorrents-is-moving-to-kat-cr-domain/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				KickassTorrents is moving to kat.cr domain
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="24 Apr 2015, 10:13">4&nbsp;months&nbsp;ago</span></span>
	</li>
	<li>
		<a href="/blog/post/happy-torrents-day-2015/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Happy Torrents Day 2015!
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="30 Mar 2015, 10:06">5&nbsp;months&nbsp;ago</span></span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/Touro73/post/new-movie-and-blu-ray-releases-week-39-40-september-21-october-4/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> New Movie and Blu-ray releases Week 39-40 (September 21 - October 4)</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/Touro73/">Touro73</a> <span title="19 Sep 2015, 21:35">1&nbsp;hour&nbsp;ago</span></span></li>
	<li><a href="/blog/Sality/post/about-my-last-blog/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> About my last blog</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/Sality/">Sality</a> <span title="19 Sep 2015, 16:30">6&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/olderthangod/post/chapter-6-of-7/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Chapter 6 of 7</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/olderthangod/">olderthangod</a> <span title="19 Sep 2015, 10:03">13&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/Secludish/post/why-cats/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> WHY... cats..</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/Secludish/">Secludish</a> <span title="19 Sep 2015, 07:02">16&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/Some1Special/post/how-i-started-and-quit-smoking/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> How I started and quit smoking</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/Some1Special/">Some1Special</a> <span title="18 Sep 2015, 18:46">yesterday</span></span></li>
	<li><a href="/blog/magicpotions/post/boumboumboum-aka-the-reckoning-aka-the-rant-by-watertiger/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> BoumBoumBoum (aka The Reckoning aka The Rant) by watertiger</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/magicpotions/">magicpotions</a> <span title="18 Sep 2015, 17:31">yesterday</span></span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/murdoch%20mysteries%20s08e09/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				murdoch mysteries s08e09
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/orange%20is%20the%20new%20black%20s02e08/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Orange Is The New Black s02e08
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/randy%20houser/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				randy houser
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/alan%20carr/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				alan carr
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/four%20weddings%20and%20a%20honeymoon/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Four Weddings and A Honeymoon
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/vlc%20for%20mac/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				vlc for mac
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/90%27s%20pop/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				90&#039;s pop
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/driver%20crack/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				driver crack
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/personal%20jesus%20depech%20mode/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				personal jesus depech mode
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/the%20oranges/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				The Oranges
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/jay%20rock/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Jay Rock
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		<li>
			<a data-nop href="http://www.torrentreactor.net/" target="_blank" rel="external">
				<span class="itorreact thirdPartIcons"></span>TorrentReactor
			</a>
		</li>
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a onclick="setLanguage('bsc', '.kat.cr');return false;" class="plain">Bosnian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a onclick="setLanguage('ko', '.kat.cr');return false;" class="plain">Korean</a></li>
                                <li><a onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
		<li><a href="/welcome/">welcome</a></li>
		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <div id="_673e31f53f8166159b8e996c4124765b"></div>
        <div id="_e7050fb15fd39b3e4e99a5be4a57b6ea"></div>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script><script>
 sc('addGlobal', 'pagetype', 'front');
 sc('addSlot', '_60318cd4e8d28f6fb76fe34e9bd9c498');
sc('addSlot', '_39ecb76dd457e5ac33776fdf11500d56');
sc('addSlot', '_277923e5f9d753c5b0630c28e641790c');
sc('addSlot', '_119b0a17fab5493361a252d04bf527db');
sc('addSlot', '_7063408f1c01d50e0dc2d833186ce962', { 'searchQuery': '' });
sc('addSlot', '_673e31f53f8166159b8e996c4124765b');
sc('addSlot', '_e7050fb15fd39b3e4e99a5be4a57b6ea');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>

</body>
</html>
