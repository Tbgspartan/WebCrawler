<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>    Imgur: The most awesome images on the Internet</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="robots" content="follow,index" />
    <meta name="keywords" content="images, funny, image host, image sharing, reaction gif, viral images, current events, cute, visual storytelling, gif" />
    <meta name="description" content="Imgur is the best place to share and enjoy the most awesome images on the Internet. Every day, millions of people use Imgur to be entertained and inspired by funny, heartwarming and helpful images and stories from all around the world." />
    <meta name="copyright" content="Copyright 2015 Imgur, Inc." />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge;" />
    
    <link rel="alternate" type="application/rss+xml" title="Imgur Gallery" href="http://feeds.feedburner.com/ImgurGallery?format=xml" />
    <link rel="icon" type="image/png" href="//s.imgur.com/images/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="//s.imgur.com/images/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="//s.imgur.com/images/favicon-16x16.png" sizes="16x16">
    <link rel="apple-touch-icon-precomposed" href="//s.imgur.com/images/favicon-152.png">
    <meta name="msapplication-TileColor" content="#2cd63c">
    <meta name="msapplication-TileImage" content="//s.imgur.com/images/favicon-144.png">
            <link rel="stylesheet" type="text/css" href="//s.imgur.com/min/global.css?1442441990" />

        
        
    
        <link rel="stylesheet" type="text/css" href="//s.imgur.com/min/gallery.css?1442441990" />

            <!--[if IE 9]><link rel="stylesheet" href="//s.imgur.com/include/css/ie-sucks.css?0" type="text/css" /><![endif]-->
    
            <link rel="canonical" href="http://imgur.com/" />
        <meta property="og:url" content="http://imgur.com/" />
    
    <meta name="p:domain_verify" content="a1e7abe8af908cc6dfaf935dd9a20384"/>
    <meta property="og:site_name" content="Imgur" />
    <meta property="fb:admins" content="12331492,12301369" />
    <meta name="twitter:site" content="@imgur" />
    <meta name="twitter:domain" content="imgur.com" />
    <meta name="twitter:creator" content="@imgur" />
    <meta name="twitter:app:id:googleplay" content="com.imgur.mobile" />
        <meta name="twitter:card" content="summary_large_image" />
    <meta property="og:type" content="website" />
            <meta property="og:title" content="Imgur" />
        <meta name="twitter:title" content="Imgur" />
    
            <meta property="og:description" content="Imgur: The most awesome images on the Internet." />
        <meta name="twitter:description" content="Imgur: The most awesome images on the Internet." />
    
    <meta property="og:image" content="http://s.imgur.com/images/logo-1200-630.jpg?2" />
    <meta property="og:image:height" content="630" />
    <meta property="og:image:width" content="1200" />
    <meta name="twitter:image:src" content="http://s.imgur.com/images/logo-1200-630.jpg?2" />
    <meta name="twitter:image:height" content="630" />
    <meta name="twitter:image:width" content="1200" />
    <!--[if lte IE 8]><script type="text/javascript" src="//s.imgur.com/min/iepoly.js?1442441990"></script>
<![endif]-->
    
        
</head>
<body>
            
            <noscript>
                <iframe src="//www.googletagmanager.com/ns.html?id=GTM-W9LTJC" height="0" width="0" style="display:none;visibility:hidden"></iframe>
            </noscript>
            <script type="text/javascript">
                (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-W9LTJC');
            </script>
        
    

                
    <div id="topbar">
    <div class="header-center">
        <div id="header-logo">

            <a href="//imgur.com" class="logo " data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@logo@@}}">
                
                <span class="logo-icon"></span>
            </a>
        </div>
        <div id="main-nav">
            <ul>
                <li class="menu-btn">
                    <div class="menu-icon">
                        <div class="bar"></div>
                        <div class="bar"></div>
                        <div class="bar"></div>
                        
                    </div>
                    <div class="link-menu">
                        <div class="test-1868-show">
                            <ul class="topics cf">
                                <li class="item bold" data-value="hot"><a href="//imgur.com/hot" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@hot@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Most Viral</a></li>
                                <li class="item bold" data-value="new"><a href="//imgur.com/new" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@new@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">User Submitted</a></li>
                                                                                                            <li class="item" data-value="Funny">
                                            <a href="//imgur.com/topic/Funny" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Funny@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Funny</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Awesome">
                                            <a href="//imgur.com/topic/Awesome" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Awesome@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Awesome</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Aww">
                                            <a href="//imgur.com/topic/Aww" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Aww@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Aww</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="The More You Know">
                                            <a href="//imgur.com/topic/The_More_You_Know" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@The More You Know@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">The More You Know</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Storytime">
                                            <a href="//imgur.com/topic/Storytime" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Storytime@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Storytime</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Current Events">
                                            <a href="//imgur.com/topic/Current_Events" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Current Events@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Current Events</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Design &amp; Art">
                                            <a href="//imgur.com/topic/Design_&amp;_Art" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Design &amp;amp; Art@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Design &amp; Art</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Reaction">
                                            <a href="//imgur.com/topic/Reaction" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Reaction@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Reaction</a>
                                        </li>
                                    
                                                                    
                                
                            </ul>
                            <ul class="dropdown-footer cf">
                                <li><a href="//imgur.com/blog" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@blog@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">blog</a></li>
                                <li><a href="//imgur.com/about"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@about@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">about</a></li>
                                <li><a href="//store.imgur.com" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@store@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">store</a></li>
                                <li><a href="//imgur.com/jobs"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@jobs@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">jobs</a></li>
                                <li><a href="//help.imgur.com/hc/en-us" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@help@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">help</a></li>
                                <li><a href="//imgur.com/apps"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@apps@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">apps</a></li>
                                <li><a href="//imgur.com/tos"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@terms@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">terms</a></li>
                                <li><a href="//imgur.com/privacy"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@privacy@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">privacy</a></li>
                                <li><a href="//imgur.com/removalrequest" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@deletion@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">request deletion</a></li>
                                <li><a href="//api.imgur.com" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@api@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">api</a></li>
                                <li><a href="//imgur.com/privacy#adchoices" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@adchoices@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">ad choices</a></li>
                            </ul>
                        </div>
                        <ul class="test-1868-hide">
                            <li><a href="//imgur.com/jobs" class="new" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@jobs@@}}">we're hiring!</a></li>
                                                            <li class="title-w" title="Notify all the things!"><a href="http://imgur.com/blog/2015/08/13/an-update-to-notifications/" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@blog@@,@@new@@:true}}">blog<span class="red tiptext">new post!</span></a></li>
                            
                            <li><a href="//imgur.com/about"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@about@@}}">about imgur</a></li>
                            <li><a href="//store.imgur.com" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@store@@}}">imgur store</a></li>
                            <li><a href="//imgur.com/apps"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@apps@@}}">official apps</a></li>
                            <li><a href="//api.imgur.com" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@api@@}}">developer api</a></li>
                            <li><a href="//help.imgur.com/hc/en-us" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@help@@}}">need help?</a></li>
                        </ul>
                        <div class="clear test-1868-hide"></div>
                        <div class="dropdown-footer hamburger-menu test-1868-hide">
                            <div class="terms-footer">
                                <a href="//imgur.com/tos"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@terms@@}}">terms</a>
                                <a href="//imgur.com/privacy"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@privacy@@}}">privacy</a>
                                <a class="small-margin-top" href="//imgur.com/privacy#adchoices" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@adchoices@@}}">ad choices</a>
                            </div>
                            <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Ffacebook.com%2Fimgur&amp;width&amp;layout=button&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=35" scrolling="no" frameborder="0" style="display:inline-block; border:none; overflow:hidden; height:20px; width:50px; margin-right:7px;" allowTransparency="true"></iframe>
                            <a href="https://twitter.com/imgur" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false"></a>
                        </div>
                    </div>
                </li>
                <li class="upload-button-container">
                    <a class="upload-button disabled upload-blue" href="javascript:;">
                        <div class="icon icon-upload"></div>
                        <span class="upload-btn-text">upload images</span>
                        <span class="upload-queue-length">23</span>
                        <div id="upload-global-top-bar-mini-progress"></div>
                    </a>
                    <div class="creation-dropdown upload-blue">
                        <div class="selection icon icon-arrow-down"></div>
                        <div class="options">
                            <ul>
                                <li class="item">
                                    <a class="creation-dropdown-upload" href="javascript:;">Upload Images</a>
                                </li>
                                <li class="item">
                                    <a href="//imgur.com/vidgif">Video to GIF</a>
                                </li>
                                <li class="item">
                                    <a href="//imgur.com/memegen" data-jafo="{@@event@@:@@makeAMeme@@,@@meta@@:{@@control@@:@@header@@}}">Make a Meme</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="random-button title-n " title="random mode">
                    <a href="//imgur.com/random" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@random@@}}">
                        <div class="random-icon"></div>
                    </a>
                </li>
            </ul>
        </div>
        <div id="secondary-nav">
            <ul class="user-nav">
                <li id="global-search-container">
                    <form class="search-form" method="get" action="//imgur.com/search">
                        <input id="search-global-input" type="text" name="q" class="search" blur placeholder="Search images"  />
                    </form>

                    <div class="search-icon-container">
                        <div class="search-icon-container-icon"></div>
                    </div>
                </li>
                                    <li class="signin-link"><a class="" href="https://imgur.com/signin" data-jafo="{@@event@@:@@signinButtonClick@@}">sign in</a></li>
                    <li id="register"><a class="" href="https://imgur.com/register" data-jafo="{@@event@@:@@registerButtonClick@@}">sign up</a></li>
                
            </ul>
        </div> <!-- #secondary-nav -->
    </div> <!-- .header-center -->
</div> <!-- #topbar -->
<div id="cta-container-placeholder"></div>

    

    

            
        
        
    

            <a href="javascript:;" id="upload-global-album-tipsy" class="title-nw nodisplay" title="You can't create an album with just one image"></a>

<div id="upload-global" class="popup">
    <form method="post" id="upload-global-form" enctype="multipart/form-data" action="http://imgur.com/upload">
        

        <div id="upload-global-buttons">
            <div id="upload-global-upload-loader" class="textbox">
                Loading...
            </div>

            <div id="upload-global-file-wrapper">
                <span class="computerIcon"></span>
                <span class="green">browse</span> your computer

                <input accept="image/*" type="file" name="Filedata" multiple="multiple" id="upload-global-file">
            </div>

            <div class="left">
                <div id="upload-global-url">
                    <span class="webIcon"></span>
                    <input type="text" id="upload-global-link-input" class="placeholder-onkeydown" title="enter image URLs" placeholder="enter image URLs" />
                </div>
            </div>

            <div class="clear"></div>

            <div id="upload-global-dragdrop">
                <span class="dragdropIcon"></span>
                <span class="green">drag and drop</span> here
            </div>

            <div class="left">
                <div id="upload-global-clipboard">
                    <div style="text-align: center"><span class="clipboardIcons">Ctrl + V</span></div>
                    <span class="green">paste</span> from your clipboard
                </div>
            </div>

            <div class="clear"></div>
        </div>

        <div class="textbox center" id="upload-global-instructions"></div>

        <!-- gallery submit vars -->
        <input type="hidden" name="current_upload" value="0"/>
        <input type="hidden" name="total_uploads" value="0"/>
        <input type="hidden" name="terms" value="0"/>
        <input type="hidden" name="gallery_type" value=""/>


        <div id="upload-global-errors" class="textbox error" style="display: none;"></div>

        <div id="upload-global-queue-description" class="textbox nodisplay"></div>

        <div id="upload-global-start-upload">
            <div class="panel">
                <div>

                    <table id="upload-global-actions">
                        <tbody>
                            <tr>

                                <td width="50.4%">

                                    <div id="upload-global-album-checkbox-div">
                                        <input type="hidden" name="gallery_submit" value="0"/>

                                        <button id="upload-global-gallery-checkbox" type="button">
                                            <span class="gallery-icon"></span>publish to Imgur
                                        </button>

                                        <div id="upload-global-gallery-checkbox-overlay"></div>

                                        <input type="hidden" name="create_album" value="0"/>

                                        <button id="upload-global-album-checkbox" type="button">
                                            <span class="createAlbum-icon"></span>create album
                                        </button>

                                    </div>

                                </td>
                                <td width="49.6%">
                                    <button type="button" id="upload-global-start-button" class="button-big green" type="submit">
                                        <span class="start-btn-icon"></span>
                                        Start Upload
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>

                <div id="upload-global-album-options">
                    <table id="upload-global-album-layout">
    <tr>
        <td id="upload-global-album-td">
            <input name="album_title" type="text" title="Optional Album Title" class="placeholder-onkeydown" id="upload-global-album-title" maxlength="55" />
        </td>
    </tr>
</table>

                </div>

                <div class="textbox list">
                    <table id="upload-global-file-list-header">
                        <tr class="top-tr">
                            <td width="45" class="edit-td"><h2>Edit</h2></td>
                            <td><h2>Images <span class="upload-queue-length">(0)</span></h2></td>
                            <td width="40"></td>
                        </tr>
                    </table>

                    <table id="upload-global-file-list">
                    </table>
                                            <div class="agreement-notice">By uploading, you agree to our <a href="//imgur.com/tos" target="_blank">terms of service</a></div>
                    
                </div>
            </div>
        </div>

        <div id="upload-global-progress-wrapper">
            <div id="upload-global-progressbar">
                <div class="progressBarBG">
                    <div class="progressBar"></div>
                </div>
            </div>
            <div id="upload-global-upload-queue"></div>

            <div id="upload-global-converting">
                <p>
                    Optimizing your large GIFs...
                </p>
            </div>
        </div>
    </form>
</div>

<div id="upload-global-FF-paste-box"></div>

<div id="hiddenDropZone" class="nodisplay"></div>

<div class="nodisplay">
    <div id="upload-global-image-formats">
         That file type is not supported!

        <div class="textbox supported-formats">
            Supported formats: JPEG, GIF, PNG, APNG, TIFF, BMP, PDF, XCF
        </div>
    </div>
</div>

<div id="upload-global-flash-container" title="upload images from your computer">
    <div id="upload-global-flash-browse"></div>
</div>

    

        <div class="nodisplay">
	<div id="colorbox-confirm" class="popup">
		<h3 id="colorbox-confirm-title"></h3>
		
		<div class="textbox" id="colorbox-confirm-message"></div>

		<div class="margin-top">
			<div class="left">
				<a href="javascript:;" class="colorbox-no-confirm" id="colorbox-confirm-no">No way!</a>
			</div>
			
			<div class="right">
				<div class="small-loader"></div>
				<a href="javascript:;" class="colorbox-confirm" id="colorbox-confirm-yes">I'm sure</a>
			</div>
			
			<div class="clear"></div>
		</div>
	</div>
</div>


    
            
    

    
    
    
    

    <div id="fullbleed-bg"></div>
    <div id="content" class="outside main">
        
                    <div class="sentence-sorting">
            <span class="before-text">
    The

</span>
<div id="section" class="combobox front-page-section sorting-text-align" name="section">
    <div class="selection">
                <div data-type="base" data-value="hot">
            <span class="name">Most Viral</span>
        </div>
        
        
    </div>

    <div class="options nano">
        <div class="js-galleries content">
            <div class="combobox-header-current">current: 
                <div class="combobox-current green bold">
                                            Most Viral
                    
                </div>
            </div>
            <ul>
                <li class="bold item nodisplay" data-type="base" data-value="hot">
                    <a href="/hot/viral" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@hot@@,@@gallerySort@@:@@viral@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Most Viral</a>
                </li>
                <li class="bold item" data-type="base" data-value="new">
                                            <a href="/new/viral" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@new@@,@@gallerySort@@:@@viral@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">User Submitted</a>
                    
                </li>
                <li class="combobox-hairline"></li>
                                                            <li class="item" data-value="Funny">
                            <a href="/topic/Funny" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Funny@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Funny</a>
                        </li>
                    
                                                            <li class="item" data-value="Awesome">
                            <a href="/topic/Awesome" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Awesome@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Awesome</a>
                        </li>
                    
                                                            <li class="item" data-value="Aww">
                            <a href="/topic/Aww" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Aww@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Aww</a>
                        </li>
                    
                                                            <li class="item" data-value="The More You Know">
                            <a href="/topic/The_More_You_Know" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@The More You Know@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">The More You Know</a>
                        </li>
                    
                                                            <li class="item" data-value="Storytime">
                            <a href="/topic/Storytime" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Storytime@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Storytime</a>
                        </li>
                    
                                                            <li class="item" data-value="Current Events">
                            <a href="/topic/Current_Events" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Current Events@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Current Events</a>
                        </li>
                    
                                                            <li class="item" data-value="Design &amp; Art">
                            <a href="/topic/Design_&amp;_Art" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Design &amp;amp; Art@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Design &amp; Art</a>
                        </li>
                    
                                                            <li class="item" data-value="Reaction">
                            <a href="/topic/Reaction" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Reaction@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Reaction</a>
                        </li>
                    
                                    
                
            </ul>
        </div>
        <div id="custom-group" class="item new-group"><a href="/gallery/custom" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@custom@@,@@gallerySort@@:@@viral@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Custom Gallery</a></div>
    </div>
    <input type="hidden" name="section" value="hot">
</div><span class="middle-text"> <!-- intentionally not closed -->

    images on the Internet, 


        sorted by
    
    </span> <!-- .middle-text -->
    
    
        
            
    

            
    
    <div id="sort" class="combobox">
        <div class="selection">
            popularity
        </div>
        <div class="options sorting-text-align">
            <div class="combobox-header-current">current: 
                <div class="combobox-current green bold">
                    popularity
                </div>
            </div>
            <ul>
                                    <li class="item nodisplay" value="viral">
                                                    <a href="/hot/viral">popularity</a>
                        
                    </li>
                    <li class="item" value="time">
                                                    <a href="/hot/time">newest first</a>
                        
                    </li>
                                        <li class="item" value="top">
                                                    <a href="/top">highest scoring</a>
                        
                    </li>
                    
                
            </ul>
        </div>
        <input type="hidden" name="sort" value="viral">
    </div>

    

    <div class="sort-options">
                                    <ul>
                                            <li><a href="/gallery/random" id="random-button" class="title-n" title="random mode"><span></span></a></li>
                    
                                            <li><a href="javascript:void(0)" id="past-link" class="title-n" title="past images"><span></span></a></li>
                    
                </ul>
            
        
    </div>
    <div id="user-gallery-message">
        
    </div>
</div>

               
        

                    <div class="panel hidden">
                <div id="imagelist" class="home-gallery">
                                            <script type="text/tag-group-data">[]</script>


<div class="posts br5 first-child">
    

            
        <div class="outside-loader"></div>
        
                    
        

        <div class="cards">
                            <div id="pivsCdS" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/pivsCdS" data-page="0">
        <img alt="" src="//i.imgur.com/pivsCdSb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="pivsCdS" type="image" data-up="12370">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="pivsCdS" type="image" data-downs="177">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-pivsCdS">12,193</span>
                            <span class="points-text-pivsCdS">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Places where you can buy the coolest stuff</p>
        
        
        <div class="post-info">
            animated &middot; 541,262 views
        </div>
    </div>
    
</div>

                            <div id="2GybVZX" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/2GybVZX" data-page="0">
        <img alt="" src="//i.imgur.com/2GybVZXb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="2GybVZX" type="image" data-up="7428">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="2GybVZX" type="image" data-downs="434">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-2GybVZX">6,994</span>
                            <span class="points-text-2GybVZX">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Give it a few years.</p>
        
        
        <div class="post-info">
            image &middot; 1,412,893 views
        </div>
    </div>
    
</div>

                            <div id="q1wkqcn" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/q1wkqcn" data-page="0">
        <img alt="" src="//i.imgur.com/q1wkqcnb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="q1wkqcn" type="image" data-up="5572">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="q1wkqcn" type="image" data-downs="192">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-q1wkqcn">5,380</span>
                            <span class="points-text-q1wkqcn">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Now this is what I like to see.</p>
        
        
        <div class="post-info">
            image &middot; 172,289 views
        </div>
    </div>
    
</div>

                            <div id="bc6AyBI" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/bc6AyBI" data-page="0">
        <img alt="" src="//i.imgur.com/bc6AyBIb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="bc6AyBI" type="image" data-up="8842">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="bc6AyBI" type="image" data-downs="159">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-bc6AyBI">8,683</span>
                            <span class="points-text-bc6AyBI">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Remember this guy? Sadly, he has passed away. Clovis Acosta Fernandez, rest in peace.</p>
        
        
        <div class="post-info">
            animated &middot; 683,806 views
        </div>
    </div>
    
</div>

                            <div id="c6TXZoo" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/c6TXZoo" data-page="0">
        <img alt="" src="//i.imgur.com/c6TXZoob.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="c6TXZoo" type="image" data-up="6065">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="c6TXZoo" type="image" data-downs="278">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-c6TXZoo">5,787</span>
                            <span class="points-text-c6TXZoo">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Morse Code</p>
        
        
        <div class="post-info">
            image &middot; 1,227,410 views
        </div>
    </div>
    
</div>

                            <div id="RFAtSmw" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/RFAtSmw" data-page="0">
        <img alt="" src="//i.imgur.com/RFAtSmwb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="RFAtSmw" type="image" data-up="4584">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="RFAtSmw" type="image" data-downs="80">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-RFAtSmw">4,504</span>
                            <span class="points-text-RFAtSmw">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Even my dishwasher is putting me down today</p>
        
        
        <div class="post-info">
            image &middot; 1,320,848 views
        </div>
    </div>
    
</div>

                            <div id="I3dl7rE" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/I3dl7rE" data-page="0">
        <img alt="" src="//i.imgur.com/I3dl7rEb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="I3dl7rE" type="image" data-up="4596">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="I3dl7rE" type="image" data-downs="400">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-I3dl7rE">4,196</span>
                            <span class="points-text-I3dl7rE">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>I found a bomb in my apartment; how do I defuse it?</p>
        
        
        <div class="post-info">
            image &middot; 572,866 views
        </div>
    </div>
    
</div>

                            <div id="bXnhXpK" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/bXnhXpK" data-page="0">
        <img alt="" src="//i.imgur.com/bXnhXpKb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="bXnhXpK" type="image" data-up="8215">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="bXnhXpK" type="image" data-downs="309">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-bXnhXpK">7,906</span>
                            <span class="points-text-bXnhXpK">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>I started building my clock tonight.</p>
        
        
        <div class="post-info">
            image &middot; 376,411 views
        </div>
    </div>
    
</div>

                            <div id="Qljz0pU" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/Qljz0pU" data-page="0">
        <img alt="" src="//i.imgur.com/Qljz0pUb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="Qljz0pU" type="image" data-up="5606">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="Qljz0pU" type="image" data-downs="339">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-Qljz0pU">5,267</span>
                            <span class="points-text-Qljz0pU">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Hillary&#039;s Super-PAC-attack is now affecting her FB. This is the top comment on her latest post.</p>
        
        
        <div class="post-info">
            image &middot; 743,592 views
        </div>
    </div>
    
</div>

                            <div id="WGDuAc9" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/WGDuAc9" data-page="0">
        <img alt="" src="//i.imgur.com/WGDuAc9b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="WGDuAc9" type="image" data-up="10176">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="WGDuAc9" type="image" data-downs="478">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-WGDuAc9">9,698</span>
                            <span class="points-text-WGDuAc9">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Just found a goddamn bomb in my house! IT&#039;S TICKING!!</p>
        
        
        <div class="post-info">
            image &middot; 1,830,779 views
        </div>
    </div>
    
</div>

                            <div id="zJJaPim" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/zJJaPim" data-page="0">
        <img alt="" src="//i.imgur.com/zJJaPimb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="zJJaPim" type="image" data-up="3573">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="zJJaPim" type="image" data-downs="36">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-zJJaPim">3,537</span>
                            <span class="points-text-zJJaPim">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Toothpick</p>
        
        
        <div class="post-info">
            animated &middot; 1,229,720 views
        </div>
    </div>
    
</div>

                            <div id="xfcwFWB" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/xfcwFWB" data-page="0">
        <img alt="" src="//i.imgur.com/xfcwFWBb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="xfcwFWB" type="image" data-up="7142">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="xfcwFWB" type="image" data-downs="178">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-xfcwFWB">6,964</span>
                            <span class="points-text-xfcwFWB">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>That&#039;ll look cute.</p>
        
        
        <div class="post-info">
            image &middot; 797,048 views
        </div>
    </div>
    
</div>

                            <div id="H6hIm" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/H6hIm" data-page="0">
        <img alt="" src="//i.imgur.com/cfMgEp7b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="H6hIm" type="image" data-up="7367">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="H6hIm" type="image" data-downs="114">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-H6hIm">7,253</span>
                            <span class="points-text-H6hIm">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>That smile</p>
        
        
        <div class="post-info">
            album &middot; 174,876 views
        </div>
    </div>
    
</div>

                            <div id="32d218P" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/32d218P" data-page="0">
        <img alt="" src="//i.imgur.com/32d218Pb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="32d218P" type="image" data-up="2966">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="32d218P" type="image" data-downs="54">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-32d218P">2,912</span>
                            <span class="points-text-32d218P">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>mrw my gf says she bought the best vacuum money can buy</p>
        
        
        <div class="post-info">
            animated &middot; 173,419 views
        </div>
    </div>
    
</div>

                            <div id="4N2T6eq" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/4N2T6eq" data-page="0">
        <img alt="" src="//i.imgur.com/4N2T6eqb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="4N2T6eq" type="image" data-up="4989">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="4N2T6eq" type="image" data-downs="580">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-4N2T6eq">4,409</span>
                            <span class="points-text-4N2T6eq">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Albanian virus</p>
        
        
        <div class="post-info">
            image &middot; 1,667,231 views
        </div>
    </div>
    
</div>

                            <div id="brhLl6l" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/brhLl6l" data-page="0">
        <img alt="" src="//i.imgur.com/brhLl6lb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="brhLl6l" type="image" data-up="7070">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="brhLl6l" type="image" data-downs="164">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-brhLl6l">6,906</span>
                            <span class="points-text-brhLl6l">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>MRW my short ass flatmate has hung the bathroom mirror too low.</p>
        
        
        <div class="post-info">
            animated &middot; 552,396 views
        </div>
    </div>
    
</div>

                            <div id="WEjEBNB" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/WEjEBNB" data-page="0">
        <img alt="" src="//i.imgur.com/WEjEBNBb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="WEjEBNB" type="image" data-up="4661">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="WEjEBNB" type="image" data-downs="91">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-WEjEBNB">4,570</span>
                            <span class="points-text-WEjEBNB">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Warning, slutty trains approaching</p>
        
        
        <div class="post-info">
            image &middot; 1,219,152 views
        </div>
    </div>
    
</div>

                            <div id="nkzmQ" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/nkzmQ" data-page="0">
        <img alt="" src="//i.imgur.com/2dy9zhmb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="nkzmQ" type="image" data-up="6482">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="nkzmQ" type="image" data-downs="216">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-nkzmQ">6,266</span>
                            <span class="points-text-nkzmQ">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Pure gold - enjoy</p>
        
        
        <div class="post-info">
            album &middot; 163,862 views
        </div>
    </div>
    
</div>

                            <div id="nQ2R1Qn" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/nQ2R1Qn" data-page="0">
        <img alt="" src="//i.imgur.com/nQ2R1Qnb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="nQ2R1Qn" type="image" data-up="3515">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="nQ2R1Qn" type="image" data-downs="235">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-nQ2R1Qn">3,280</span>
                            <span class="points-text-nQ2R1Qn">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Update. So glad this is  happening!</p>
        
        
        <div class="post-info">
            image &middot; 161,846 views
        </div>
    </div>
    
</div>

                            <div id="IJpYgvb" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/IJpYgvb" data-page="0">
        <img alt="" src="//i.imgur.com/IJpYgvbb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="IJpYgvb" type="image" data-up="19685">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="IJpYgvb" type="image" data-downs="635">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-IJpYgvb">19,050</span>
                            <span class="points-text-IJpYgvb">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>I stand with Ahmed</p>
        
        
        <div class="post-info">
            image &middot; 708,931 views
        </div>
    </div>
    
</div>

                            <div id="jGZ8RBU" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/jGZ8RBU" data-page="0">
        <img alt="" src="//i.imgur.com/jGZ8RBUb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="jGZ8RBU" type="image" data-up="1939">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="jGZ8RBU" type="image" data-downs="103">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-jGZ8RBU">1,836</span>
                            <span class="points-text-jGZ8RBU">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>TIL what a bomb looks like to people in Irving, TX</p>
        
        
        <div class="post-info">
            image &middot; 1,481,032 views
        </div>
    </div>
    
</div>

                            <div id="Ca1Jpl1" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/Ca1Jpl1" data-page="0">
        <img alt="" src="//i.imgur.com/Ca1Jpl1b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="Ca1Jpl1" type="image" data-up="2160">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="Ca1Jpl1" type="image" data-downs="50">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-Ca1Jpl1">2,110</span>
                            <span class="points-text-Ca1Jpl1">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Found my girlfriend&#039;s cat the perfect home.</p>
        
        
        <div class="post-info">
            image &middot; 676,872 views
        </div>
    </div>
    
</div>

                            <div id="9kQsYtq" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/9kQsYtq" data-page="0">
        <img alt="" src="//i.imgur.com/9kQsYtqb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="9kQsYtq" type="image" data-up="1542">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="9kQsYtq" type="image" data-downs="28">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-9kQsYtq">1,514</span>
                            <span class="points-text-9kQsYtq">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>My buddy teaches English in China, his student might be cheating on google translate..</p>
        
        
        <div class="post-info">
            image &middot; 637,063 views
        </div>
    </div>
    
</div>

                            <div id="MALPPQU" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/MALPPQU" data-page="0">
        <img alt="" src="//i.imgur.com/MALPPQUb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="MALPPQU" type="image" data-up="1863">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="MALPPQU" type="image" data-downs="295">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-MALPPQU">1,568</span>
                            <span class="points-text-MALPPQU">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>&quot;Texas&quot; by GeneralPayN in funny</p>
        
        
        <div class="post-info">
            image &middot; 1,089,820 views
        </div>
    </div>
    
</div>

                            <div id="opzan2t" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/opzan2t" data-page="0">
        <img alt="" src="//i.imgur.com/opzan2tb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="opzan2t" type="image" data-up="4368">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="opzan2t" type="image" data-downs="70">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-opzan2t">4,298</span>
                            <span class="points-text-opzan2t">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Chlorine and Brake Fluid</p>
        
        
        <div class="post-info">
            animated &middot; 624,553 views
        </div>
    </div>
    
</div>

                            <div id="URBSd" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/URBSd" data-page="0">
        <img alt="" src="//i.imgur.com/RJ6ZyE8b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="URBSd" type="image" data-up="2282">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="URBSd" type="image" data-downs="69">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-URBSd">2,213</span>
                            <span class="points-text-URBSd">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Where the Republican presidential candidates stand</p>
        
        
        <div class="post-info">
            album &middot; 53,402 views
        </div>
    </div>
    
</div>

                            <div id="Y6mXf" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/Y6mXf" data-page="0">
        <img alt="" src="//i.imgur.com/RZI1Q1Db.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="Y6mXf" type="image" data-up="9421">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="Y6mXf" type="image" data-downs="300">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-Y6mXf">9,121</span>
                            <span class="points-text-Y6mXf">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Random stuff that made me smile.</p>
        
        
        <div class="post-info">
            album &middot; 232,388 views
        </div>
    </div>
    
</div>

                            <div id="IbCpAzq" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/IbCpAzq" data-page="0">
        <img alt="" src="//i.imgur.com/IbCpAzqb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="IbCpAzq" type="image" data-up="2462">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="IbCpAzq" type="image" data-downs="61">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-IbCpAzq">2,401</span>
                            <span class="points-text-IbCpAzq">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>When your doctor asks if you are sexually active and you are dating his daughter</p>
        
        
        <div class="post-info">
            animated &middot; 152,020 views
        </div>
    </div>
    
</div>

                            <div id="cVQls97" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/cVQls97" data-page="0">
        <img alt="" src="//i.imgur.com/cVQls97b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="cVQls97" type="image" data-up="3230">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="cVQls97" type="image" data-downs="147">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-cVQls97">3,083</span>
                            <span class="points-text-cVQls97">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Now we&#039;re asking the real questions</p>
        
        
        <div class="post-info">
            image &middot; 148,108 views
        </div>
    </div>
    
</div>

                            <div id="uEMKN" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/uEMKN" data-page="0">
        <img alt="" src="//i.imgur.com/vZ5KgZ0b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="uEMKN" type="image" data-up="4866">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="uEMKN" type="image" data-downs="177">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-uEMKN">4,689</span>
                            <span class="points-text-uEMKN">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>ISIS (full version)</p>
        
        
        <div class="post-info">
            album &middot; 198,232 views
        </div>
    </div>
    
</div>

                            <div id="YMGQXXr" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/YMGQXXr" data-page="0">
        <img alt="" src="//i.imgur.com/YMGQXXrb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="YMGQXXr" type="image" data-up="3666">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="YMGQXXr" type="image" data-downs="105">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-YMGQXXr">3,561</span>
                            <span class="points-text-YMGQXXr">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>learning about burns in anatomy</p>
        
        
        <div class="post-info">
            image &middot; 1,046,247 views
        </div>
    </div>
    
</div>

                            <div id="EhIJB" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/EhIJB" data-page="0">
        <img alt="" src="//i.imgur.com/4b7clXUb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="EhIJB" type="image" data-up="9403">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="EhIJB" type="image" data-downs="246">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-EhIJB">9,157</span>
                            <span class="points-text-EhIJB">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Shower Thoughts</p>
        
        
        <div class="post-info">
            album &middot; 275,682 views
        </div>
    </div>
    
</div>

                            <div id="x6ya1hL" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/x6ya1hL" data-page="0">
        <img alt="" src="//i.imgur.com/x6ya1hLb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="x6ya1hL" type="image" data-up="1545">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="x6ya1hL" type="image" data-downs="41">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-x6ya1hL">1,504</span>
                            <span class="points-text-x6ya1hL">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>&quot;Never safe&#039;</p>
        
        
        <div class="post-info">
            animated &middot; 91,708 views
        </div>
    </div>
    
</div>

                            <div id="BkOb6P7" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/BkOb6P7" data-page="0">
        <img alt="" src="//i.imgur.com/BkOb6P7b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="BkOb6P7" type="image" data-up="5686">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="BkOb6P7" type="image" data-downs="118">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-BkOb6P7">5,568</span>
                            <span class="points-text-BkOb6P7">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>yes</p>
        
        
        <div class="post-info">
            animated &middot; 905,308 views
        </div>
    </div>
    
</div>

                            <div id="MBHrZcq" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/MBHrZcq" data-page="0">
        <img alt="" src="//i.imgur.com/MBHrZcqb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="MBHrZcq" type="image" data-up="8469">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="MBHrZcq" type="image" data-downs="215">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-MBHrZcq">8,254</span>
                            <span class="points-text-MBHrZcq">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Internet was down earlier and this is the only thing that would load. Ok.</p>
        
        
        <div class="post-info">
            image &middot; 348,013 views
        </div>
    </div>
    
</div>

                            <div id="x6vEq" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/x6vEq" data-page="0">
        <img alt="" src="//i.imgur.com/pHUwtZhb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="x6vEq" type="image" data-up="2803">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="x6vEq" type="image" data-downs="251">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-x6vEq">2,552</span>
                            <span class="points-text-x6vEq">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Dragons Burn</p>
        
        
        <div class="post-info">
            album &middot; 79,023 views
        </div>
    </div>
    
</div>

                            <div id="LT9orfm" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/LT9orfm" data-page="0">
        <img alt="" src="//i.imgur.com/LT9orfmb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="LT9orfm" type="image" data-up="3044">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="LT9orfm" type="image" data-downs="90">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-LT9orfm">2,954</span>
                            <span class="points-text-LT9orfm">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Conan O&#039;Brien holding the cutest kitten I&#039;ve ever seen.</p>
        
        
        <div class="post-info">
            image &middot; 638,548 views
        </div>
    </div>
    
</div>

                            <div id="JfRZN3i" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/JfRZN3i" data-page="0">
        <img alt="" src="//i.imgur.com/JfRZN3ib.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="JfRZN3i" type="image" data-up="4631">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="JfRZN3i" type="image" data-downs="281">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-JfRZN3i">4,350</span>
                            <span class="points-text-JfRZN3i">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>It all makes sense now</p>
        
        
        <div class="post-info">
            image &middot; 965,016 views
        </div>
    </div>
    
</div>

                            <div id="nqdbbM1" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/nqdbbM1" data-page="0">
        <img alt="" src="//i.imgur.com/nqdbbM1b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="nqdbbM1" type="image" data-up="2919">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="nqdbbM1" type="image" data-downs="79">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-nqdbbM1">2,840</span>
                            <span class="points-text-nqdbbM1">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Some times he forgets what animal he is.</p>
        
        
        <div class="post-info">
            image &middot; 754,031 views
        </div>
    </div>
    
</div>

                            <div id="s7ybo" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/s7ybo" data-page="0">
        <img alt="" src="//i.imgur.com/0cX6lvGb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="s7ybo" type="image" data-up="2079">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="s7ybo" type="image" data-downs="91">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-s7ybo">1,988</span>
                            <span class="points-text-s7ybo">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>&quot;When the Old Gods Return&quot;</p>
        
        
        <div class="post-info">
            album &middot; 48,344 views
        </div>
    </div>
    
</div>

                            <div id="eSbBkDP" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/eSbBkDP" data-page="0">
        <img alt="" src="//i.imgur.com/eSbBkDPb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="eSbBkDP" type="image" data-up="3920">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="eSbBkDP" type="image" data-downs="349">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-eSbBkDP">3,571</span>
                            <span class="points-text-eSbBkDP">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>I tested the water permeable pavement from yesterday&#039;s post.</p>
        
        
        <div class="post-info">
            animated &middot; 1,733,479 views
        </div>
    </div>
    
</div>

                            <div id="8x9eG" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/8x9eG" data-page="0">
        <img alt="" src="//i.imgur.com/0FHquOJb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="8x9eG" type="image" data-up="2623">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="8x9eG" type="image" data-downs="76">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-8x9eG">2,547</span>
                            <span class="points-text-8x9eG">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>&quot;I don&#039;t want chemicals in my food&quot;</p>
        
        
        <div class="post-info">
            album &middot; 55,869 views
        </div>
    </div>
    
</div>

                            <div id="3nLf9" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/3nLf9" data-page="0">
        <img alt="" src="//i.imgur.com/BxUtyE1b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="3nLf9" type="image" data-up="4131">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="3nLf9" type="image" data-downs="88">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-3nLf9">4,043</span>
                            <span class="points-text-3nLf9">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Sadness IRL</p>
        
        
        <div class="post-info">
            album &middot; 109,363 views
        </div>
    </div>
    
</div>

                            <div id="YzMSJCD" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/YzMSJCD" data-page="0">
        <img alt="" src="//i.imgur.com/YzMSJCDb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="YzMSJCD" type="image" data-up="1394">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="YzMSJCD" type="image" data-downs="69">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-YzMSJCD">1,325</span>
                            <span class="points-text-YzMSJCD">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Ahmed is now a &#039;notable person&#039; on his high school&#039;s Wikipedia page, joining several athletes and a porn star</p>
        
        
        <div class="post-info">
            image &middot; 507,396 views
        </div>
    </div>
    
</div>

                            <div id="05I8CQd" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/05I8CQd" data-page="0">
        <img alt="" src="//i.imgur.com/05I8CQdb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="05I8CQd" type="image" data-up="161">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="05I8CQd" type="image" data-downs="62">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-05I8CQd">99</span>
                            <span class="points-text-05I8CQd">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Just bought this new clock at Ikea... Pretty sick</p>
        
        
        <div class="post-info">
            image &middot; 316,783 views
        </div>
    </div>
    
</div>

                            <div id="roNmfnT" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/roNmfnT" data-page="0">
        <img alt="" src="//i.imgur.com/roNmfnTb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="roNmfnT" type="image" data-up="6098">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="roNmfnT" type="image" data-downs="223">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-roNmfnT">5,875</span>
                            <span class="points-text-roNmfnT">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Little girl in 7-Eleven shirt</p>
        
        
        <div class="post-info">
            image &middot; 2,088,573 views
        </div>
    </div>
    
</div>

                            <div id="1KEkE" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/1KEkE" data-page="0">
        <img alt="" src="//i.imgur.com/vtVjMkqb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="1KEkE" type="image" data-up="5796">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="1KEkE" type="image" data-downs="137">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-1KEkE">5,659</span>
                            <span class="points-text-1KEkE">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Food &amp; Weight &amp; Exercise</p>
        
        
        <div class="post-info">
            album &middot; 137,445 views
        </div>
    </div>
    
</div>

                            <div id="LeC9l7q" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/LeC9l7q" data-page="0">
        <img alt="" src="//i.imgur.com/LeC9l7qb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="LeC9l7q" type="image" data-up="2984">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="LeC9l7q" type="image" data-downs="291">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-LeC9l7q">2,693</span>
                            <span class="points-text-LeC9l7q">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>&quot;If you don&#039;t want your tax dollars to help the poor...&quot; - President Jimmy Carter </p>
        
        
        <div class="post-info">
            image &middot; 294,008 views
        </div>
    </div>
    
</div>

                            <div id="dsiKWbF" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/dsiKWbF" data-page="0">
        <img alt="" src="//i.imgur.com/dsiKWbFb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="dsiKWbF" type="image" data-up="1263">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="dsiKWbF" type="image" data-downs="68">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-dsiKWbF">1,195</span>
                            <span class="points-text-dsiKWbF">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>An Amish Burn</p>
        
        
        <div class="post-info">
            animated &middot; 305,039 views
        </div>
    </div>
    
</div>

                            <div id="mAbIDsm" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/mAbIDsm" data-page="0">
        <img alt="" src="//i.imgur.com/mAbIDsmb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="mAbIDsm" type="image" data-up="3165">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="mAbIDsm" type="image" data-downs="114">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-mAbIDsm">3,051</span>
                            <span class="points-text-mAbIDsm">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Guess he really wanted that Apple Watch.</p>
        
        
        <div class="post-info">
            animated &middot; 2,131,481 views
        </div>
    </div>
    
</div>

                            <div id="byUU6Dx" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/byUU6Dx" data-page="0">
        <img alt="" src="//i.imgur.com/byUU6Dxb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="byUU6Dx" type="image" data-up="1547">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="byUU6Dx" type="image" data-downs="78">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-byUU6Dx">1,469</span>
                            <span class="points-text-byUU6Dx">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Sneezing today made me think of you glorious twatwaffles.</p>
        
        
        <div class="post-info">
            image &middot; 75,060 views
        </div>
    </div>
    
</div>

                            <div id="euGLZB4" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/euGLZB4" data-page="0">
        <img alt="" src="//i.imgur.com/euGLZB4b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="euGLZB4" type="image" data-up="2338">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="euGLZB4" type="image" data-downs="259">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-euGLZB4">2,079</span>
                            <span class="points-text-euGLZB4">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>That&#039;s just nasty. Who the hell paints Donald Trump?</p>
        
        
        <div class="post-info">
            animated &middot; 237,082 views
        </div>
    </div>
    
</div>

                            <div id="aBpKfIJ" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/aBpKfIJ" data-page="0">
        <img alt="" src="//i.imgur.com/aBpKfIJb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="aBpKfIJ" type="image" data-up="1298">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="aBpKfIJ" type="image" data-downs="39">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-aBpKfIJ">1,259</span>
                            <span class="points-text-aBpKfIJ">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>For those of you that wanted to see a piglet being woken up by a cheerio</p>
        
        
        <div class="post-info">
            animated &middot; 78,455 views
        </div>
    </div>
    
</div>

                            <div id="kYJonIa" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/kYJonIa" data-page="0">
        <img alt="" src="//i.imgur.com/kYJonIab.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="kYJonIa" type="image" data-up="3846">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="kYJonIa" type="image" data-downs="129">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-kYJonIa">3,717</span>
                            <span class="points-text-kYJonIa">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Being in school is tough</p>
        
        
        <div class="post-info">
            image &middot; 195,923 views
        </div>
    </div>
    
</div>

                            <div id="K5vq7" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/K5vq7" data-page="0">
        <img alt="" src="//i.imgur.com/bXLJUvGb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="K5vq7" type="image" data-up="1515">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="K5vq7" type="image" data-downs="142">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-K5vq7">1,373</span>
                            <span class="points-text-K5vq7">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Just Manly Things</p>
        
        
        <div class="post-info">
            album &middot; 37,586 views
        </div>
    </div>
    
</div>

                            <div id="1cfVEpk" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/1cfVEpk" data-page="0">
        <img alt="" src="//i.imgur.com/1cfVEpkb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="1cfVEpk" type="image" data-up="536">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="1cfVEpk" type="image" data-downs="69">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-1cfVEpk">467</span>
                            <span class="points-text-1cfVEpk">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>&quot;Other items bravely confiscated by Texas police&quot; by HammockComplex in funny</p>
        
        
        <div class="post-info">
            image &middot; 595,698 views
        </div>
    </div>
    
</div>

                            <div id="HD5vMrc" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/HD5vMrc" data-page="0">
        <img alt="" src="//i.imgur.com/HD5vMrcb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="HD5vMrc" type="image" data-up="942">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="HD5vMrc" type="image" data-downs="29">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-HD5vMrc">913</span>
                            <span class="points-text-HD5vMrc">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Groundskeepers accidentally spread weed killer instead of fertilizer on HS football field</p>
        
        
        <div class="post-info">
            image &middot; 204,987 views
        </div>
    </div>
    
</div>

                            <div id="gRH7bpT" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/gRH7bpT" data-page="0">
        <img alt="" src="//i.imgur.com/gRH7bpTb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="gRH7bpT" type="image" data-up="3441">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="gRH7bpT" type="image" data-downs="128">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-gRH7bpT">3,313</span>
                            <span class="points-text-gRH7bpT">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Relevant</p>
        
        
        <div class="post-info">
            image &middot; 235,653 views
        </div>
    </div>
    
</div>

                            <div id="iP4wRyJ" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/iP4wRyJ" data-page="0">
        <img alt="" src="//i.imgur.com/iP4wRyJb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="iP4wRyJ" type="image" data-up="1277">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="iP4wRyJ" type="image" data-downs="98">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-iP4wRyJ">1,179</span>
                            <span class="points-text-iP4wRyJ">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>No wonder you&#039;re pregnant</p>
        
        
        <div class="post-info">
            image &middot; 345,789 views
        </div>
    </div>
    
</div>

                            <div id="ATuOtuj" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/ATuOtuj" data-page="0">
        <img alt="" src="//i.imgur.com/ATuOtujb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="ATuOtuj" type="image" data-up="5793">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="ATuOtuj" type="image" data-downs="246">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-ATuOtuj">5,547</span>
                            <span class="points-text-ATuOtuj">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>join us!</p>
        
        
        <div class="post-info">
            animated &middot; 728,705 views
        </div>
    </div>
    
</div>

            
        </div>

        <div class="clear"></div>

        <div class="imagelist-loader textbox center lineheight small-margin-top">
            <img src="//s.imgur.com/images/loaders/ddddd1_181817/48.gif" />
        </div>
    


    </div>
            <div class="page-divider"><a href="javascript:;" class="scroll-top-text" data-jafo="{@@event@@:@@galleryBackToTop@@,@@meta@@:{@@galleryType@@:@@hot@@,@@gallerySort@@:@@viral@@,@@pageNum@@:0}}">back to top</a></div>
    


                    
                </div>
            
                                                            <div id="top-comments" class="left-column">
                                                                                    <div id="outside-tagging" class="br5">
                                    <div class="group-add-done nodisplay"></div>
        
                                    <div id="tags-current-list" class="tag-list tag-list-immutable">
                                                                                        <div class="panel">
        <div class="textbox">
            <h2>Today&#039;s Best Comments</h2>
        </div>
    
        <div class="captions best-captions">
                            <div class="caption-container rank-0">
                    <p class="clickability">
                        <a href="/gallery/IJpYgvb/comment/477625403"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="I stand with Ahmed" src="//i.imgur.com/IJpYgvbb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/cranialpleasure">cranialpleasure</a> 5,848 points
                        </div>
        
                                                    How to crush an interest in science and engineer and foster bitterness towards the system in one easy step.
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container rank-1">
                    <p class="clickability">
                        <a href="/gallery/IJpYgvb/comment/477607239"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="I stand with Ahmed" src="//i.imgur.com/IJpYgvbb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/mrguyperson">mrguyperson</a> 5,639 points
                        </div>
        
                                                    Here&#039;s the best quote from the police in the article: &quot;It looks like a movie bomb to me.&quot;  Good thing detective dipshit was on the case.
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container rank-2">
                    <p class="clickability">
                        <a href="/gallery/IJpYgvb/comment/477559568"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="I stand with Ahmed" src="//i.imgur.com/IJpYgvbb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/Skinnyev">Skinnyev</a> 5,046 points
                        </div>
        
                                                    Dudes got a NASA shirt on! That should have been the first give way
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container">
                    <p class="clickability">
                        <a href="/gallery/3ua3o/comment/477478219"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="I churned some ointment for your BURN" src="//i.imgur.com/V1D86aJb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/ctth0mas">ctth0mas</a> 4,888 points
                        </div>
        
                                                    churn down for what
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container">
                    <p class="clickability">
                        <a href="/gallery/j2SGZh2/comment/477386299"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="Everyone fights now and then, but an argument with someone could be the last time you ever see them. Don&#039;t let saying something you&#039;ll regret later be the ending note in a conversation." src="//i.imgur.com/j2SGZh2b.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/sweeteizus">sweeteizus</a> 4,026 points
                        </div>
        
                                                    Now that&#039;s a real &#9829;ing friend.
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container">
                    <p class="clickability">
                        <a href="/gallery/roNmfnT/comment/477654969"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="Little girl in 7-Eleven shirt" src="//i.imgur.com/roNmfnTb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/lolsy">lolsy</a> 3,882 points
                        </div>
        
                                                    Considering how Asian women age, she might be 38 years old.
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container last-child">
                    <p class="clickability">
                        <a href="/gallery/gDyr4Rx/comment/477416121"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="[Image] More beautiful for having been broken..." src="//i.imgur.com/gDyr4Rxb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/CiriusTrio">CiriusTrio</a> 3,710 points
                        </div>
        
                                                    TAKE THAT, 13 YEAR OLD COUSIN WHO HAS YET TO BE IN A RELATIONSHIP BUT POSTS THIS ON FACEBOOK
                        
                    </div>
                </div>

                <div class="clear"></div>
            
    
            
        </div>
    </div>


                                        
                                    </div>
                                </div>
        
                                <div id="outside-tagging-showhide" class="no-select title-e short" title=" Show Sidebar">
                                                                            <div id="comment-tab">
                                            <div id="comment-icon-one"></div>
                                            <div id="comment-icon-two"></div>
                                            <div id="comment-icon-three"></div>
                                        </div>
                                    
                                </div>
                            
                        
                        </div>
                    
                
            </div>
        

        <div class="clear"></div>
    </div>

    <div class="nodisplay">
        <div id="past-wrapper" class="popup">
            <div>
                <div class="textbox left">
                    

                    <input type="text" id="days" />

                                            days ago
                    
                </div>
                
                <div id="past-calendar-wrapper" class="textbox left">
                    <div id="past-calendar"></div>
                </div>

                <div id="slider-wrapper" class="textbox left">
                    <div class="ticker negative">&laquo;</div>
                    <div id="slider"></div>
                    <div class="ticker positive">&raquo;</div>
                </div>

                <div id="button-wrapper" class="textbox left">
                    <input type="button" class="button-medium" value="Go" id="past-submit" />
                </div>

                <div class="clear"></div>
            </div>

            <div id="past-loader"></div>
            <div id="past-preview"></div>
        </div>
    </div>

    <div class="tag-group-selector-container"></div>
            <div id="top-tag-container"></div>
    

    <input id="sid" type="hidden" value="ce8a5bba254c738d6ccae20c7b371617" />
        

    

            
    
    
    

            <script type="text/javascript">
            (function() {
                var roll = Math.random();
                if(roll < 0.05) {
                    
                    (function(_,e,rr,s){
                        _errs=[s];var c=_.onerror;_.onerror=function(){var a=arguments;_errs.push(a);
                        c&&c.apply(this,a)};var b=function(){var c=e.createElement(rr),b=e.getElementsByTagName(rr)[0];
                        c.src="//beacon.errorception.com/"+s+".js";c.async=!0;b.parentNode.insertBefore(c,b)};
                        _.addEventListener?_.addEventListener("load",b,!1):_.attachEvent("onload",b)
                    })(window,document,"script","51d1e360b05ccb10310000d2");
                    
                }
            })();
        </script>

        
<!--[if lte IE 8]>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript">
if(typeof jQuery === 'undefined') {
    document.write('<script type="text/javascript" src="//s.imgur.com/include/js/ext/jquery-1.10.2.min.js"><' + '/script>');
}
</script>
<![endif]-->

<!--[if gt IE 8]>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
if(typeof jQuery === 'undefined') {
    document.write('<script type="text/javascript" src="//s.imgur.com/include/js/ext/jquery.2.1.1.min.js"><' + '/script>');
}
</script>
<![endif]-->

<!--[if !IE]> -->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
if(typeof jQuery === 'undefined') {
    document.write('<script type="text/javascript" src="//s.imgur.com/include/js/ext/jquery.2.1.1.min.js"><' + '/script>');
}
</script>
<!--<![endif]-->



        <script type="text/javascript" src="//s.imgur.com/min/global.js?1442441990"></script>

        <script type="text/javascript">
            Imgur.Environment = {
                isDev:         false,
                isSubdomain:   Imgur.Util.isSubdomain('imgur.com'),
                host:          'imgur.com',
                cdnUrl:        '//i.imgur.com',
                staticUrl:     '//s.imgur.com',
                signed:        false,
                auth:          {},
                recaptcha_key: '6LeZbt4SAAAAAKEsafT3QzEFp5vJ1-Z23uy5mPDz',
                msid:          'ce8a5bba254c738d6ccae20c7b371617',
                beta:          {
                    enabled:   false,
                    inBeta:    false
                },
                
                ctaConfig:     {"anonymous":[{"active":true,"type":"button","trackingName":"anonymous","localStorageName":"cta-anonymous","url":"\/","buttonText":"start exploring","title":"Welcome to Imgur!","description":"Your new home for the Internet's best visual stories.","background":"{STATIC}\/images\/house-cta\/cta-background.jpg","newTab":false,"buttonColor":"#77ac21"}],"user":[{"active":false,"type":"button","trackingName":"campImgur","localStorageName":"cta-camp1418760143","customClass":"u-pl95","url":"\/\/imgur.com\/camp","buttonText":"Get More Details","title":"Camp Imgur, August 2015","description":"We're 50% sold out! Reserve your space now.","background":"{STATIC}\/images\/house-cta\/cta-camp-imgur.jpg","newTab":true,"buttonColor":"#cf5033"}],"gaming":[{"active":true,"type":"button","trackingName":"gaming","localStorageName":"cta-gaming","url":"\/\/imgur.com\/t\/gaming\/top\/week","buttonText":"Check it out!","title":"You are now browsing the Internet on God Mode.","description":"Awesome unlocked! Imgur has gaming content like you wouldn't believe.","background":"{STATIC}\/images\/house-cta\/cta-twitch.jpg","newTab":false,"buttonColor":"#4d3280"}],"facebook":[{"active":true,"type":"custom","jsReactClassName":"Facebook","trackingName":"facebook","localStorageName":"cta-fb141008","title":"Like us on Facebook!","description":"We're bringing funny to your newsfeed. Like Imgur on Facebook!","background":"{STATIC}\/images\/house-cta\/facebook-day.jpg"}],"pinterest":[{"active":true,"type":"custom","jsReactClassName":"Pinterest","trackingName":"pinterest","localStorageName":"cta-pinterest-2015-07-14","title":"Follow us on Pinterest!","background":"{STATIC}\/images\/house-cta\/pinterest.png","customClass":"pinterest"}],"promoted":[{"active":true,"type":"custom","jsReactClassName":"Promoted","trackingName":"promoted","localStorageName":false,"buttonText":"Learn More","title":"What Are Promoted Posts?","description":"And why am I seeing them?","background":"{STATIC}\/images\/house-cta\/cta-promoted.jpg","url":"\/\/imgur.com\/blog\/?p=6101","newTab":true,"customClass":"pp-banner"}],"global":[{"active":false,"trackingName":"inCaseYouMissedIt","localStorageName":"cta-icymi-2015-07-14","type":"button","subtype":"app","background":"{STATIC}\/images\/house-cta\/cta-icymi.png","url":"\/\/imgur.com\/topic\/Hand_Picked\/","buttonText":"Take me there","title":"Catch up on the awesome posts you might have missed!","description":"Now featured in the Hand Picked topic.","newTab":false,"customClass":"u-pl260 icymi","buttonColor":"#252525","frontPageOnly":true},{"active":true,"trackingName":"mobileApps","localStorageName":"apps1433176979","type":"button","background":"\/\/s.imgur.com\/images\/house-cta\/cta-apps.jpg?1433176979","url":"\/\/imgur.com\/apps","buttonText":"Count me in!","title":"Get the Imgur Mobile App!","description":"Fully Native. Totally Awesome.","newTab":true,"customClass":"u-pl260"},{"active":true,"type":"custom","jsReactClassName":"Newsletter","trackingName":"inCaseYouMissedItForm","localStorageName":"apps1440530195","background":"{STATIC}\/images\/house-cta\/cta-icymi-form.png?1440530195","newTab":true,"customClass":"u-pl200"}]},
                experiments:   {"exp1868":{"active":false,"name":"hamburger-topics","inclusionProbability":1,"expirationDate":"2015-09-30T00:00:00.000Z","variations":[{"name":"hamburger-topics","inclusionProbability":0.5}]}},
                isGalleryAdmin:false,
                ppBlog: '//imgur.com/blog/?p=6101'
            };

            var imgur = Imgur.getInstance();
            imgur.init(_.merge(Imgur.Environment, {
                
                
                beta: {"enabled":false,"inBeta":false,"showInvite":false,"ctaInvite":[],"ctaWelcome":[]},
            }));

                            var uploader = Imgur.Upload.Global.getInstance(Imgur.getInstance()._);

                if(uploader._.el.$uploadGlobal.length > 0) {
                    uploader.bindPasteUpload();
                    uploader.generalInit();
                }
            

            imgur.generalInit();

            $(function() {
                
            });

            
            

            var _widgetFactory = new Imgur.Factory.Widget(Imgur.Environment);

            _widgetFactory.mergeConfig('global', Imgur.Environment);

            
        </script>
                                
        <script type="text/javascript">
            (function() {
                var roll = Math.random();
                if(roll < 0.0001) {
                    

                        // This is the NSONE Pulsar tag
                        var __nspid="1mvmyo";
                        var __nsptags=[];
                        (function(w, d) { var x = function() {
                        var j=d.createElement("script");j.type="text/javascript";j.async=true;
                        j.src="http"+("https:"===d.location.protocol?"s://cs":"://c")+".ns1p.net/p.js?a="+__nspid;
                        d.body.appendChild(j); }
                        if(w.addEventListener) { w.addEventListener("load", x, false); }
                        else if(w.attachEvent) { w.attachEvent("onload", x); }
                        else { w.onload = x; }
                        }(window, document));

                    
                }
            })();
        </script>

    
    
                    <script type="text/javascript">
            (function(widgetFactory) { 
                widgetFactory.mergeConfig('analytics', {
                    isAdmin: false,
                    logPixel: '//p.imgur.com/lumbar.gif'
                });
                widgetFactory.mergeConfig('global', {
                    analyticsAccountId: 'UA-6671908-2'
                });
    
                widgetFactory.mergeConfig('search', {
                    type: 'global', 
                    q: ''
                });

                widgetFactory.produceJafo();

                widgetFactory.startExperiments();

                                    widgetFactory.produceCtaBanner("/gallery/" + "pivsCdS");
                
            })(_widgetFactory);
        </script>
    

    <script type="text/javascript">
    (function(widgetFactory) { 
        widgetFactory.mergeConfig('global', {
            sid: 'ce8a5bba254c738d6ccae20c7b371617'
        });

        widgetFactory.mergeConfig('gallery', {
            sid         : 'ce8a5bba254c738d6ccae20c7b371617',
            signed      : false,
            baseURL     : decodeURIComponent('%2Fgallery'),
            sort        : 'viral',
            window      : 'day',
            page        : 0,
            isPro       : false,
            maxPage     : 1719,
            showPast    : true,
            searchQuery : '',
            inSearch    : false,
            advSearch   : null,
            bestCaps    : true
        });

        widgetFactory.mergeConfig('groups', {
            groups: {
                
            }
        });

        widgetFactory.mergeConfig('search', {
            
        });
    })(_widgetFactory);
    </script>

    <script type="text/javascript">
        (function(widgetFactory) {
            var group_type = 'base';

            var group = {
                id: '[hot]',
                tags: [
                    
                ]
            }

            widgetFactory.mergeConfig('gallery', {
                section            : 'hot',
                showGalleryNewInfo : false,
                gallery_type       : null,
                tag                : null,
                sponsoredTag       : null,
                tagFollower        : false,
                group_type         : group_type,
                group              : group
            });
        })(_widgetFactory);
    </script>


    <script type="text/javascript">
        if (typeof _widgetFactory !== 'undefined') {
            _widgetFactory.initNotifications({"post":{"postUpvote":[10,50,100,500,1000,5000],"postComment":[10,50,100],"postView":[100000,500000,1000000,5000000],"postEmbed":[1],"postViral":[1]},"comment":{"commentUpvote":[10,100,500,1000,5000,10000],"commentReply":[1],"commentTop7":[1],"commentMention":[1]},"trophy":{"userTrophy":[1]},"account":{"userNotoriety":[-1,399,999,1999,3999,7999,19999,9223372036854775807]},"message":{"message-msg":[1]}}, [{"name":"Forever Alone","reputation":-1},{"name":"Neutral","reputation":399},{"name":"Accepted","reputation":999},{"name":"Liked","reputation":1999},{"name":"Trusted","reputation":3999},{"name":"Idolized","reputation":7999},{"name":"Renowned","reputation":19999},{"name":"Glorious","reputation":9223372036854775807}]);
        }
    </script>

        <script type="text/javascript" src="//s.imgur.com/min/index.js?1442441990"></script>

    <script type="text/javascript" src="//s.imgur.com/min/gallery.js?1442441990"></script>

    <script type="text/javascript">
    (function(widgetFactory) {
        var emitter = new Imgur.Emitter();

        widgetFactory.produceLocationCommand(emitter);

        var galleryFactory = widgetFactory.produceOutsideGalleryFactory();
        var galleryGraph = galleryFactory.produceOutsideGallery(emitter);

        var gallery = galleryGraph.gallery,
            history = galleryGraph.history;

        var outsideTaggingWidget = galleryFactory.produceOutsideTaggingWidget(emitter, galleryGraph);

        gallery.outsideInit();

        Imgur.Upload.Index.getInstance().generalInit();
    })(_widgetFactory);
    </script>

    <script type="text/javascript">
    $(function() {
        if(!/^([^:]+:)\/\/([^.]+\.)*imgur(-dev)?\.com(\/.*)?$/.test(document.referrer)) {
            Imgur.Util.jafoLog({ event: 'galleryDisplay', meta: { gallerySort: 'viral', galleryType: 'hot' }});
        }
    });
    </script>
    
    

            <script type="text/javascript">
        if (typeof _widgetFactory !== 'undefined') {
            _widgetFactory.produceSearch();
            _widgetFactory.initIdleMonitor();
        }
        </script>
    

        
    

            <script type="text/javascript">
            Imgur.Util.getGoogleTracker();
            __ga('send', 'pageview');
        </script>

        
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
        

        

        
    

        <script type='text/javascript'>
        
        (function(){
            try{
                var msg = '\n      _\n     (_)\n      _ _ __ ___   __ _ _   _ _ __\n     | | \'_ ` _ \\ / _` | | | | \'__|\n     | | | | | | | (_| | |_| | |\n     |_|_| |_| |_|\\__, |\\__,_|_|\n                   __/ |\n                  |___/\n';
                msg += '========================================\nYou opened the console! Know some code,\ndo you? Want to work for one of the best\nstartups around? http://imgur.com/jobs\n========================================\n';
                imcon.log(msg);
            }catch(e){}
        })()
        
    </script>
    

        

</body>
</html>
