<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-e461882.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-e461882.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-e461882.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-e461882.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kat.cr/sc-e461882.js');

        sc('setHost', 'a.kat.cr');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: 'e461882',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-e461882.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<div id="_60318cd4e8d28f6fb76fe34e9bd9c498"></div>
<div id="_39ecb76dd457e5ac33776fdf11500d56"></div>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom" style="color: #ffeeb4; font-size: 34px; float:left; height: 50px; line-height: 50px;"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka">e</i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka">g</i></button></div>
			</form>
		</div>
        <div id="_277923e5f9d753c5b0630c28e641790c"></div>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080p/" class="tag5">1080p</a>
	<a href="/search/2014/" class="tag3">2014</a>
	<a href="/search/2015/" class="tag10">2015</a>
	<a href="/search/3d/" class="tag3">3d</a>
	<a href="/search/android/" class="tag2">android</a>
	<a href="/search/ant%20man/" class="tag2">ant man</a>
	<a href="/search/arrow/" class="tag2">arrow</a>
	<a href="/search/avengers/" class="tag2">avengers</a>
	<a href="/search/avengers%20age%20of%20ultron/" class="tag2">avengers age of ultron</a>
	<a href="/search/bajrangi%20bhaijaan/" class="tag2">bajrangi bhaijaan</a>
	<a href="/search/batman/" class="tag2">batman</a>
	<a href="/search/compton%20dr%20dre/" class="tag2">compton dr dre</a>
	<a href="/search/discography/" class="tag4">discography</a>
	<a href="/search/expert%20guide%20to/" class="tag7">expert guide to</a>
	<a href="/search/flac/" class="tag3">flac</a>
	<a href="/search/french/" class="tag3">french</a>
	<a href="/search/game%20of%20thrones/" class="tag3">game of thrones</a>
	<a href="/search/hannibal/" class="tag2">hannibal</a>
	<a href="/search/hindi/" class="tag8">hindi</a>
	<a href="/search/hindi%202015/" class="tag3">hindi 2015</a>
	<a href="/search/inside%20out/" class="tag2">inside out</a>
	<a href="/search/insurgent/" class="tag2">insurgent</a>
	<a href="/search/ita/" class="tag3">ita</a>
	<a href="/search/mad%20max/" class="tag2">mad max</a>
	<a href="/search/malayalam/" class="tag2">malayalam</a>
	<a href="/search/minions/" class="tag2">minions</a>
	<a href="/search/movies/" class="tag3">movies</a>
	<a href="/search/mr%20robot/" class="tag5">mr robot</a>
	<a href="/search/mr%20robot%20s01e08/" class="tag3">mr robot s01e08</a>
	<a href="/search/nezu/" class="tag10">nezu</a>
	<a href="/search/nl/" class="tag3">nl</a>
	<a href="/search/ripsalot/" class="tag4">ripsalot</a>
	<a href="/search/san%20andreas/" class="tag2">san andreas</a>
	<a href="/search/southpaw/" class="tag2">southpaw</a>
	<a href="/search/suits/" class="tag3">suits</a>
	<a href="/search/suits%20s05e08/" class="tag2">suits s05e08</a>
	<a href="/search/tamil/" class="tag3">tamil</a>
	<a href="/search/telugu/" class="tag3">telugu</a>
	<a href="/search/terminator%20genisys/" class="tag2">terminator genisys</a>
	<a href="/search/the%20big%20bang%20theory/" class="tag4">the big bang theory</a>
	<a href="/search/the%20flash/" class="tag2">the flash</a>
	<a href="/search/the%20walking%20dead/" class="tag5">the walking dead</a>
	<a href="/search/true%20detective/" class="tag2">true detective</a>
	<a href="/search/windows%2010/" class="tag2">windows 10</a>
	<a href="/search/wwe/" class="tag2">wwe</a>
	<a href="/search/yify/" class="tag10">yify</a>
	<a href="/search/yify%201080p/" class="tag3">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag4">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="rsssign" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11091342,0" class="icomment icommentjs icon16" href="/mad-max-fury-road-2015-720p-brrip-x264-yify-t11091342.html#comment"> <em class="iconvalue">176</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/mad-max-fury-road-2015-720p-brrip-x264-yify-t11091342.html" class="cellMainLink">Mad Max: Fury Road (2015) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="909921729">867.77 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="13 Aug 2015, 22:27">1&nbsp;day</span></td>
			<td class="green center">24102</td>
			<td class="red lasttd center">23024</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11087366,0" class="icomment icommentjs icon16" href="/dragon-nest-warriors-dawn-2014-720p-brrip-x264-yify-t11087366.html#comment"> <em class="iconvalue">103</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dragon-nest-warriors-dawn-2014-720p-brrip-x264-yify-t11087366.html" class="cellMainLink">Dragon Nest: Warriors Dawn (2014) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="732754949">698.81 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="13 Aug 2015, 02:59">1&nbsp;day</span></td>
			<td class="green center">12743</td>
			<td class="red lasttd center">6476</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11092920,0" class="icomment icommentjs icon16" href="/air-2015-hdrip-xvid-etrg-t11092920.html#comment"> <em class="iconvalue">31</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/air-2015-hdrip-xvid-etrg-t11092920.html" class="cellMainLink">Air 2015.HDRip.XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="741163911">706.83 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="14 Aug 2015, 06:52">16&nbsp;hours</span></td>
			<td class="green center">9229</td>
			<td class="red lasttd center">8753</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11093174,0" class="icomment icommentjs icon16" href="/cop-car-2015-hdrip-xvid-etrg-t11093174.html#comment"> <em class="iconvalue">39</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/cop-car-2015-hdrip-xvid-etrg-t11093174.html" class="cellMainLink">Cop Car 2015 HDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="748991553">714.29 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="14 Aug 2015, 07:48">15&nbsp;hours</span></td>
			<td class="green center">8365</td>
			<td class="red lasttd center">8287</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11095274,0" class="icomment icommentjs icon16" href="/dark-places-2015-720p-brrip-x264-yify-t11095274.html#comment"> <em class="iconvalue">15</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dark-places-2015-720p-brrip-x264-yify-t11095274.html" class="cellMainLink">Dark Places (2015) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="853807387">814.25 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="14 Aug 2015, 16:27">6&nbsp;hours</span></td>
			<td class="green center">3646</td>
			<td class="red lasttd center">5479</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11083789,0" class="icomment icommentjs icon16" href="/black-white-sex-2012-720p-brrip-x264-yify-t11083789.html#comment"> <em class="iconvalue">35</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/black-white-sex-2012-720p-brrip-x264-yify-t11083789.html" class="cellMainLink">Black &amp; White &amp; Sex (2012) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="788030460">751.52 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="12 Aug 2015, 09:41">2&nbsp;days</span></td>
			<td class="green center">4336</td>
			<td class="red lasttd center">2858</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11076250,0" class="icomment icommentjs icon16" href="/dragon-ball-z-resurrection-f-2015-cam-eng-dub-x264-aac-warning-t11076250.html#comment"> <em class="iconvalue">72</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dragon-ball-z-resurrection-f-2015-cam-eng-dub-x264-aac-warning-t11076250.html" class="cellMainLink">Dragon Ball Z Resurrection &#039;F 2015 CAM Eng Dub x264 AAC-Warning</a></div>
			</td>
			<td class="nobr center" data-sort="850231549">810.84 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="11 Aug 2015, 03:43">3&nbsp;days</span></td>
			<td class="green center">4040</td>
			<td class="red lasttd center">2353</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11094414,0" class="icomment icommentjs icon16" href="/brothers-2015-cam-rip-xvid-1cd-team-ictv-exclusive-sparrow-t11094414.html#comment"> <em class="iconvalue">34</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/brothers-2015-cam-rip-xvid-1cd-team-ictv-exclusive-sparrow-t11094414.html" class="cellMainLink">Brothers (2015) Cam Rip - XviD - [1CD] - Team IcTv Exclusive -={SPARROW}=-</a></div>
			</td>
			<td class="nobr center" data-sort="728883414">695.12 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="14 Aug 2015, 13:35">9&nbsp;hours</span></td>
			<td class="green center">1882</td>
			<td class="red lasttd center">4201</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11090150,0" class="icomment icommentjs icon16" href="/into-the-grizzly-maze-2014-french-dvdrip-x264-ac3-bzh-mkv-t11090150.html#comment"> <em class="iconvalue">13</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/into-the-grizzly-maze-2014-french-dvdrip-x264-ac3-bzh-mkv-t11090150.html" class="cellMainLink">Into The Grizzly Maze 2014 FRENCH DVDRip x264 AC3-BzH mkv</a></div>
			</td>
			<td class="nobr center" data-sort="1119105017">1.04 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="13 Aug 2015, 16:34">1&nbsp;day</span></td>
			<td class="green center">3984</td>
			<td class="red lasttd center">1852</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11078456,0" class="icomment icommentjs icon16" href="/time-out-of-mind-2015-hdrip-xvid-ac3-evo-t11078456.html#comment"> <em class="iconvalue">33</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/time-out-of-mind-2015-hdrip-xvid-ac3-evo-t11078456.html" class="cellMainLink">Time Out of Mind 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1509217737">1.41 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="11 Aug 2015, 14:17">3&nbsp;days</span></td>
			<td class="green center">2729</td>
			<td class="red lasttd center">2213</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11093908,0" class="icomment icommentjs icon16" href="/big-sky-2015-hdrip-xvid-etrg-t11093908.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/big-sky-2015-hdrip-xvid-etrg-t11093908.html" class="cellMainLink">Big Sky 2015 HDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="742567636">708.17 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="14 Aug 2015, 11:15">12&nbsp;hours</span></td>
			<td class="green center">1839</td>
			<td class="red lasttd center">2656</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11079618,0" class="icomment icommentjs icon16" href="/area-51-2015-dvdrip-xvid-evo-t11079618.html#comment"> <em class="iconvalue">42</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/area-51-2015-dvdrip-xvid-evo-t11079618.html" class="cellMainLink">Area 51 2015 DVDRip XviD-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="976875287">931.62 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="11 Aug 2015, 19:09">3&nbsp;days</span></td>
			<td class="green center">2654</td>
			<td class="red lasttd center">1729</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11085511,0" class="icomment icommentjs icon16" href="/full-strike-2015-720p-bluray-x264-wiki-t11085511.html#comment"> <em class="iconvalue">22</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/full-strike-2015-720p-bluray-x264-wiki-t11085511.html" class="cellMainLink">Full Strike 2015 720p BluRay x264-WiKi</a></div>
			</td>
			<td class="nobr center" data-sort="5918226958">5.51 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="12 Aug 2015, 17:22">2&nbsp;days</span></td>
			<td class="green center">1323</td>
			<td class="red lasttd center">2726</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11088020,0" class="icomment icommentjs icon16" href="/mad-max-estrada-da-fÃºria-2015-web-dl-1080p-dublado-andretpf-t11088020.html#comment"> <em class="iconvalue">10</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/mad-max-estrada-da-fÃºria-2015-web-dl-1080p-dublado-andretpf-t11088020.html" class="cellMainLink">Mad Max - Estrada Da FÃºria (2015) WEB-DL 1080p Dublado - AndreTPF</a></div>
			</td>
			<td class="nobr center" data-sort="2376746051">2.21 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="13 Aug 2015, 06:31">1&nbsp;day</span></td>
			<td class="green center">2008</td>
			<td class="red lasttd center">1291</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11095593,0" class="icomment icommentjs icon16" href="/vsop-2015-dvdscr-x264-1cdrip-700mb-t11095593.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/vsop-2015-dvdscr-x264-1cdrip-700mb-t11095593.html" class="cellMainLink">VSOP (2015)[DVDScr - x264 - 1CDRip - 700MB]</a></div>
			</td>
			<td class="nobr center" data-sort="728559426">694.81 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="14 Aug 2015, 17:50">5&nbsp;hours</span></td>
			<td class="green center">3</td>
			<td class="red lasttd center">3187</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="rsssign" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11087216,0" class="icomment icommentjs icon16" href="/suits-s05e08-hdtv-x264-killers-ettv-t11087216.html#comment"> <em class="iconvalue">82</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/suits-s05e08-hdtv-x264-killers-ettv-t11087216.html" class="cellMainLink">Suits S05E08 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="242826009">231.58 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="13 Aug 2015, 02:09">1&nbsp;day</span></td>
			<td class="green center">16960</td>
			<td class="red lasttd center">3259</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11087393,0" class="icomment icommentjs icon16" href="/mr-robot-s01e08-hdtv-x264-killers-ettv-t11087393.html#comment"> <em class="iconvalue">215</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/mr-robot-s01e08-hdtv-x264-killers-ettv-t11087393.html" class="cellMainLink">Mr Robot S01E08 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="234322651">223.47 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="13 Aug 2015, 03:09">1&nbsp;day</span></td>
			<td class="green center">11933</td>
			<td class="red lasttd center">1636</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11091549,0" class="icomment icommentjs icon16" href="/under-the-dome-s03e09-hdtv-x264-lol-ettv-t11091549.html#comment"> <em class="iconvalue">66</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/under-the-dome-s03e09-hdtv-x264-lol-ettv-t11091549.html" class="cellMainLink">Under the Dome S03E09 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="283369003">270.24 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="14 Aug 2015, 00:01">23&nbsp;hours</span></td>
			<td class="green center">6885</td>
			<td class="red lasttd center">3731</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11092216,0" class="icomment icommentjs icon16" href="/dominion-s02e06-hdtv-x264-killers-ettv-t11092216.html#comment"> <em class="iconvalue">41</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dominion-s02e06-hdtv-x264-killers-ettv-t11092216.html" class="cellMainLink">Dominion S02E06 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="280692841">267.69 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="14 Aug 2015, 03:15">20&nbsp;hours</span></td>
			<td class="green center">4947</td>
			<td class="red lasttd center">2522</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11092213,0" class="icomment icommentjs icon16" href="/hannibal-s03e11-hdtv-x264-killers-ettv-t11092213.html#comment"> <em class="iconvalue">47</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/hannibal-s03e11-hdtv-x264-killers-ettv-t11092213.html" class="cellMainLink">Hannibal S03E11 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="203329477">193.91 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="14 Aug 2015, 03:15">20&nbsp;hours</span></td>
			<td class="green center">4632</td>
			<td class="red lasttd center">2313</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11075057,0" class="icomment icommentjs icon16" href="/devious-maids-s03e11-hdtv-x264-killers-ettv-t11075057.html#comment"> <em class="iconvalue">25</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/devious-maids-s03e11-hdtv-x264-killers-ettv-t11075057.html" class="cellMainLink">Devious Maids S03E11 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="328816893">313.58 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="11 Aug 2015, 02:14">3&nbsp;days</span></td>
			<td class="green center">4948</td>
			<td class="red lasttd center">1643</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11081187,0" class="icomment icommentjs icon16" href="/tyrant-s02e09-hdtv-x264-killers-ettv-t11081187.html#comment"> <em class="iconvalue">42</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/tyrant-s02e09-hdtv-x264-killers-ettv-t11081187.html" class="cellMainLink">Tyrant S02E09 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="342137581">326.29 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="12 Aug 2015, 03:10">2&nbsp;days</span></td>
			<td class="green center">4100</td>
			<td class="red lasttd center">2450</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11087175,0" class="icomment icommentjs icon16" href="/extant-s02e08-hdtv-x264-lol-ettv-t11087175.html#comment"> <em class="iconvalue">51</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/extant-s02e08-hdtv-x264-lol-ettv-t11087175.html" class="cellMainLink">Extant S02E08 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="277334366">264.49 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="13 Aug 2015, 01:58">1&nbsp;day</span></td>
			<td class="green center">4067</td>
			<td class="red lasttd center">2142</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11075875,0" class="icomment icommentjs icon16" href="/teen-wolf-s05e08-hdtv-x264-killers-ettv-t11075875.html#comment"> <em class="iconvalue">85</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/teen-wolf-s05e08-hdtv-x264-killers-ettv-t11075875.html" class="cellMainLink">Teen Wolf S05E08 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="261689726">249.57 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="11 Aug 2015, 03:11">3&nbsp;days</span></td>
			<td class="green center">3473</td>
			<td class="red lasttd center">1109</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11075811,0" class="icomment icommentjs icon16" href="/the-whispers-s01e10-hdtv-x264-killers-ettv-t11075811.html#comment"> <em class="iconvalue">66</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-whispers-s01e10-hdtv-x264-killers-ettv-t11075811.html" class="cellMainLink">The Whispers S01E10 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="274794816">262.06 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="11 Aug 2015, 03:07">3&nbsp;days</span></td>
			<td class="green center">2763</td>
			<td class="red lasttd center">1294</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11080698,0" class="icomment icommentjs icon16" href="/zoo-s01e07-hdtv-x264-lol-ettv-t11080698.html#comment"> <em class="iconvalue">69</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/zoo-s01e07-hdtv-x264-lol-ettv-t11080698.html" class="cellMainLink">Zoo S01E07 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="318770677">304 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="12 Aug 2015, 01:58">2&nbsp;days</span></td>
			<td class="green center">2821</td>
			<td class="red lasttd center">857</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11087124,0" class="icomment icommentjs icon16" href="/masterchef-us-s06e14-hdtv-x264-lol-ettv-t11087124.html#comment"> <em class="iconvalue">14</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/masterchef-us-s06e14-hdtv-x264-lol-ettv-t11087124.html" class="cellMainLink">MasterChef US S06E14 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="448372110">427.6 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="13 Aug 2015, 01:39">1&nbsp;day</span></td>
			<td class="green center">2223</td>
			<td class="red lasttd center">1318</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11091873,0" class="icomment icommentjs icon16" href="/mistresses-us-s03e10-hdtv-x264-killers-ettv-t11091873.html#comment"> <em class="iconvalue">7</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/mistresses-us-s03e10-hdtv-x264-killers-ettv-t11091873.html" class="cellMainLink">Mistresses US S03E10 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="317849683">303.13 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="14 Aug 2015, 02:10">21&nbsp;hours</span></td>
			<td class="green center">2141</td>
			<td class="red lasttd center">1146</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11081196,0" class="icomment icommentjs icon16" href="/scream-the-tv-series-s01e07-hdtv-x264-killers-ettv-t11081196.html#comment"> <em class="iconvalue">45</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/scream-the-tv-series-s01e07-hdtv-x264-killers-ettv-t11081196.html" class="cellMainLink">Scream The TV Series S01E07 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="281932590">268.87 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="12 Aug 2015, 03:13">2&nbsp;days</span></td>
			<td class="green center">2111</td>
			<td class="red lasttd center">770</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11091845,0" class="icomment icommentjs icon16" href="/complications-s01e10-hdtv-x264-lol-ettv-t11091845.html#comment"> <em class="iconvalue">15</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/complications-s01e10-hdtv-x264-lol-ettv-t11091845.html" class="cellMainLink">Complications S01E10 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="330143117">314.85 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="14 Aug 2015, 02:01">21&nbsp;hours</span></td>
			<td class="green center">1374</td>
			<td class="red lasttd center">885</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="rsssign" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11089782,0" class="icomment icommentjs icon16" href="/mp3-new-releases-2015-week-32-suprax-glodls-t11089782.html#comment"> <em class="iconvalue">15</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/mp3-new-releases-2015-week-32-suprax-glodls-t11089782.html" class="cellMainLink">MP3 NEW RELEASES 2015 WEEK 32 - SUPRAX [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="4231852069">3.94 <span>GB</span></td>
			<td class="center">505</td>
			<td class="center"><span title="13 Aug 2015, 15:14">1&nbsp;day</span></td>
			<td class="green center">544</td>
			<td class="red lasttd center">837</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11083493,0" class="icomment icommentjs icon16" href="/billboard-hot-100-singles-chart-22th-aug-2015-cbr-320-kbps-aryan-l33t-t11083493.html#comment"> <em class="iconvalue">11</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/billboard-hot-100-singles-chart-22th-aug-2015-cbr-320-kbps-aryan-l33t-t11083493.html" class="cellMainLink">Billboard Hot 100 Singles Chart (22th Aug 2015) CBR 320 Kbps [AryaN_L33T]</a></div>
			</td>
			<td class="nobr center" data-sort="875039074">834.5 <span>MB</span></td>
			<td class="center">103</td>
			<td class="center"><span title="12 Aug 2015, 08:15">2&nbsp;days</span></td>
			<td class="green center">707</td>
			<td class="red lasttd center">485</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11089993,0" class="icomment icommentjs icon16" href="/bullet-for-my-valentine-venom-deluxe-edition-2015-320ak-t11089993.html#comment"> <em class="iconvalue">14</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bullet-for-my-valentine-venom-deluxe-edition-2015-320ak-t11089993.html" class="cellMainLink">Bullet For My Valentine - Venom (Deluxe Edition) (2015)320ak</a></div>
			</td>
			<td class="nobr center" data-sort="138086615">131.69 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span title="13 Aug 2015, 15:58">1&nbsp;day</span></td>
			<td class="green center">600</td>
			<td class="red lasttd center">180</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11077808,0" class="icomment icommentjs icon16" href="/club-deep-electro-house-pop-dance-trance-dubstep-va-discoteka-2015-dance-club-vol-141-Ð¾Ñ-nnnb-mp3-320kbps-t11077808.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/club-deep-electro-house-pop-dance-trance-dubstep-va-discoteka-2015-dance-club-vol-141-Ð¾Ñ-nnnb-mp3-320kbps-t11077808.html" class="cellMainLink">(Club, Deep, Electro House, Pop, Dance, Trance, Dubstep) VA - Discoteka 2015 Dance Club Vol. 141 Ð¾Ñ NNNB - mp3 - 320kbps</a></div>
			</td>
			<td class="nobr center" data-sort="2164808602">2.02 <span>GB</span></td>
			<td class="center">165</td>
			<td class="center"><span title="11 Aug 2015, 11:14">3&nbsp;days</span></td>
			<td class="green center">434</td>
			<td class="red lasttd center">195</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11089454,0" class="icomment icommentjs icon16" href="/the-police-ghosts-of-the-police-2015-320ak-t11089454.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-police-ghosts-of-the-police-2015-320ak-t11089454.html" class="cellMainLink">The Police - Ghosts of the Police 2015 320ak</a></div>
			</td>
			<td class="nobr center" data-sort="271227752">258.66 <span>MB</span></td>
			<td class="center">28</td>
			<td class="center"><span title="13 Aug 2015, 13:42">1&nbsp;day</span></td>
			<td class="green center">511</td>
			<td class="red lasttd center">114</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11094101,0" class="icomment icommentjs icon16" href="/disturbed-immortalized-deluxe-edition-2015-l-audio-l-album-track-l-vbr-l-mp3-l-sn3h1t87-t11094101.html#comment"> <em class="iconvalue">17</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/disturbed-immortalized-deluxe-edition-2015-l-audio-l-album-track-l-vbr-l-mp3-l-sn3h1t87-t11094101.html" class="cellMainLink">Disturbed - Immortalized (Deluxe Edition) [2015] l Audio l Album Track l VBR l Mp3 l sn3h1t87</a></div>
			</td>
			<td class="nobr center" data-sort="160616048">153.18 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center"><span title="14 Aug 2015, 12:12">11&nbsp;hours</span></td>
			<td class="green center">353</td>
			<td class="red lasttd center">224</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11087539,0" class="icomment icommentjs icon16" href="/2-chainz-tity-boi-trapavelli-tre-320kbps-mixjoint-t11087539.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/2-chainz-tity-boi-trapavelli-tre-320kbps-mixjoint-t11087539.html" class="cellMainLink">2 Chainz (Tity Boi) - Trapavelli Tre [320Kbps] (MixJoint)</a></div>
			</td>
			<td class="nobr center" data-sort="124486914">118.72 <span>MB</span></td>
			<td class="center">19</td>
			<td class="center"><span title="13 Aug 2015, 04:16">1&nbsp;day</span></td>
			<td class="green center">454</td>
			<td class="red lasttd center">92</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11093923,0" class="icomment icommentjs icon16" href="/hero-2015-full-album-mp3-groo-t11093923.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/hero-2015-full-album-mp3-groo-t11093923.html" class="cellMainLink">Hero (2015) Full Album Mp3 Groo</a></div>
			</td>
			<td class="nobr center" data-sort="95305398">90.89 <span>MB</span></td>
			<td class="center">10</td>
			<td class="center"><span title="14 Aug 2015, 11:18">12&nbsp;hours</span></td>
			<td class="green center">215</td>
			<td class="red lasttd center">280</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11085186,0" class="icomment icommentjs icon16" href="/va-billboard-hot-100-singles-chart-22nd-august-2015-rz-rg-mp3-320kbps-glodls-t11085186.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-billboard-hot-100-singles-chart-22nd-august-2015-rz-rg-mp3-320kbps-glodls-t11085186.html" class="cellMainLink">VA - Billboard Hot 100 Singles Chart (22nd August 2015) RZ-RG [MP3-320KBPS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="875808429">835.24 <span>MB</span></td>
			<td class="center">103</td>
			<td class="center"><span title="12 Aug 2015, 16:10">2&nbsp;days</span></td>
			<td class="green center">232</td>
			<td class="red lasttd center">226</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11094089,0" class="icomment icommentjs icon16" href="/b-o-b-psycadelik-thoughtz-2015-cbr-320-kbps-aryan-l33t-t11094089.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/b-o-b-psycadelik-thoughtz-2015-cbr-320-kbps-aryan-l33t-t11094089.html" class="cellMainLink">B.o.B - Psycadelik Thoughtz (2015) CBR 320 Kbps [AryaN_L33T]</a></div>
			</td>
			<td class="nobr center" data-sort="101579880">96.87 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span title="14 Aug 2015, 12:07">11&nbsp;hours</span></td>
			<td class="green center">243</td>
			<td class="red lasttd center">122</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-official-uk-top-40-singles-chart-14th-aug-2015-cbr-320-kbps-aryan-l33t-t11095335.html" class="cellMainLink">The Official UK TOP 40 Singles Chart (14th Aug 2015) CBR 320 Kbps [AryaN_L33T]</a></div>
			</td>
			<td class="nobr center" data-sort="359535654">342.88 <span>MB</span></td>
			<td class="center">43</td>
			<td class="center"><span title="14 Aug 2015, 16:50">6&nbsp;hours</span></td>
			<td class="green center">3</td>
			<td class="red lasttd center">350</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11091120,0" class="icomment icommentjs icon16" href="/armin-van-buuren-a-state-of-trance-726-2015-08-13-split-clubtime-t11091120.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/armin-van-buuren-a-state-of-trance-726-2015-08-13-split-clubtime-t11091120.html" class="cellMainLink">Armin Van Buuren - A State Of Trance 726 (2015-08-13) (Split - ClubTime)</a></div>
			</td>
			<td class="nobr center" data-sort="306460552">292.26 <span>MB</span></td>
			<td class="center">37</td>
			<td class="center"><span title="13 Aug 2015, 20:51">1&nbsp;day</span></td>
			<td class="green center">209</td>
			<td class="red lasttd center">90</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11084226,0" class="icomment icommentjs icon16" href="/va-billboard-us-top-40-22nd-august-2015-rz-rg-mp3-320kbps-glodls-t11084226.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-billboard-us-top-40-22nd-august-2015-rz-rg-mp3-320kbps-glodls-t11084226.html" class="cellMainLink">VA - Billboard US TOP 40 (22nd August 2015) RZ-RG [MP3-320KBPS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="336968322">321.36 <span>MB</span></td>
			<td class="center">43</td>
			<td class="center"><span title="12 Aug 2015, 11:57">2&nbsp;days</span></td>
			<td class="green center">122</td>
			<td class="red lasttd center">157</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11083909,0" class="icomment icommentjs icon16" href="/billboard-songs-of-the-summer-chart-22nd-august-2015-mp3-320kbps-h4ckus-glodls-t11083909.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/billboard-songs-of-the-summer-chart-22nd-august-2015-mp3-320kbps-h4ckus-glodls-t11083909.html" class="cellMainLink">Billboard - Songs of the Summer Chart (22nd August 2015) [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="177832424">169.59 <span>MB</span></td>
			<td class="center">24</td>
			<td class="center"><span title="12 Aug 2015, 10:12">2&nbsp;days</span></td>
			<td class="green center">220</td>
			<td class="red lasttd center">58</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11085370,0" class="icomment icommentjs icon16" href="/b-a-r-2015-week-31-newjds-glodls-t11085370.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/b-a-r-2015-week-31-newjds-glodls-t11085370.html" class="cellMainLink">B.A.R. 2015 Week 31 NEWJDS - [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="4239205178">3.95 <span>GB</span></td>
			<td class="center">461</td>
			<td class="center"><span title="12 Aug 2015, 16:47">2&nbsp;days</span></td>
			<td class="green center">102</td>
			<td class="red lasttd center">173</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="rsssign" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11088696,0" class="icomment icommentjs icon16" href="/battlefield-hardline-cpy-t11088696.html#comment"> <em class="iconvalue">234</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/battlefield-hardline-cpy-t11088696.html" class="cellMainLink">Battlefield.Hardline-CPY</a></div>
			</td>
			<td class="nobr center" data-sort="51993829328">48.42 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="13 Aug 2015, 10:12">1&nbsp;day</span></td>
			<td class="green center">1062</td>
			<td class="red lasttd center">6173</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11084432,0" class="icomment icommentjs icon16" href="/galactic-civilizations-iii-by-xatab-t11084432.html#comment"> <em class="iconvalue">31</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/galactic-civilizations-iii-by-xatab-t11084432.html" class="cellMainLink">Galactic Civilizations III By XATAB</a></div>
			</td>
			<td class="nobr center" data-sort="6308251884">5.88 <span>GB</span></td>
			<td class="center">9</td>
			<td class="center"><span title="12 Aug 2015, 12:39">2&nbsp;days</span></td>
			<td class="green center">784</td>
			<td class="red lasttd center">288</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11089684,0" class="icomment icommentjs icon16" href="/3dmgame-battlefield-hardline-v2-0-update-and-crack-3dm-t11089684.html#comment"> <em class="iconvalue">61</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/3dmgame-battlefield-hardline-v2-0-update-and-crack-3dm-t11089684.html" class="cellMainLink">3DMGAME-Battlefield Hardline v2 0 Update and Crack-3DM</a></div>
			</td>
			<td class="nobr center" data-sort="1475412122">1.37 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="13 Aug 2015, 14:48">1&nbsp;day</span></td>
			<td class="green center">435</td>
			<td class="red lasttd center">204</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11091147,0" class="icomment icommentjs icon16" href="/metal-gear-solid-v-ground-zeroes-v-1-005-2014-steam-rip-r-g-steamgames-t11091147.html#comment"> <em class="iconvalue">20</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/metal-gear-solid-v-ground-zeroes-v-1-005-2014-steam-rip-r-g-steamgames-t11091147.html" class="cellMainLink">Metal Gear Solid V Ground Zeroes [v 1.005] (2014) [Steam-Rip] - R.G. Steamgames</a></div>
			</td>
			<td class="nobr center" data-sort="3161919388">2.94 <span>GB</span></td>
			<td class="center">13</td>
			<td class="center"><span title="13 Aug 2015, 21:03">1&nbsp;day</span></td>
			<td class="green center">291</td>
			<td class="red lasttd center">296</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11085376,0" class="icomment icommentjs icon16" href="/toy-soldiers-war-chest-codex-t11085376.html#comment"> <em class="iconvalue">19</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/toy-soldiers-war-chest-codex-t11085376.html" class="cellMainLink">Toy Soldiers War Chest-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="4287599988">3.99 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="12 Aug 2015, 16:51">2&nbsp;days</span></td>
			<td class="green center">264</td>
			<td class="red lasttd center">269</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11079053,0" class="icomment icommentjs icon16" href="/mortal-kombat-x-update-14-2015-pc-steam-rip-Ð¾Ñ-let-splay-t11079053.html#comment"> <em class="iconvalue">46</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/mortal-kombat-x-update-14-2015-pc-steam-rip-Ð¾Ñ-let-splay-t11079053.html" class="cellMainLink">Mortal Kombat X [Update 14] (2015) PC | Steam-Rip Ð¾Ñ Let&#039;sPlay</a></div>
			</td>
			<td class="nobr center" data-sort="35207302633">32.79 <span>GB</span></td>
			<td class="center">11</td>
			<td class="center"><span title="11 Aug 2015, 16:35">3&nbsp;days</span></td>
			<td class="green center">118</td>
			<td class="red lasttd center">402</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11094484,0" class="icomment icommentjs icon16" href="/the-witcher-3-wild-hunt-patch-v1-08-1-gog-t11094484.html#comment"> <em class="iconvalue">24</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/the-witcher-3-wild-hunt-patch-v1-08-1-gog-t11094484.html" class="cellMainLink">The Witcher 3: Wild Hunt Patch v1.08.1 (GOG)</a></div>
			</td>
			<td class="nobr center" data-sort="4070574748">3.79 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="14 Aug 2015, 13:48">9&nbsp;hours</span></td>
			<td class="green center">148</td>
			<td class="red lasttd center">333</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11086175,0" class="icomment icommentjs icon16" href="/gauntlet-slayer-edition-plaza-t11086175.html#comment"> <em class="iconvalue">22</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/gauntlet-slayer-edition-plaza-t11086175.html" class="cellMainLink">Gauntlet.Slayer.Edition-PLAZA</a></div>
			</td>
			<td class="nobr center" data-sort="1932201723">1.8 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="12 Aug 2015, 20:08">2&nbsp;days</span></td>
			<td class="green center">260</td>
			<td class="red lasttd center">139</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11090827,0" class="icomment icommentjs icon16" href="/ark-survival-evolved-v1-96-2-x64-crack-lumaemu-kortal-t11090827.html#comment"> <em class="iconvalue">16</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/ark-survival-evolved-v1-96-2-x64-crack-lumaemu-kortal-t11090827.html" class="cellMainLink">ARK Survival Evolved v1.96.2 x64 crack LumaEmu #Kortal</a></div>
			</td>
			<td class="nobr center" data-sort="9851733680">9.18 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="13 Aug 2015, 19:00">1&nbsp;day</span></td>
			<td class="green center">53</td>
			<td class="red lasttd center">286</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11096542,0" class="icomment icommentjs icon16" href="/the-sims-4-spa-day-addon-reloaded-t11096542.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/the-sims-4-spa-day-addon-reloaded-t11096542.html" class="cellMainLink">The Sims 4 Spa Day Addon-RELOADED</a></div>
			</td>
			<td class="nobr center" data-sort="4327424759">4.03 <span>GB</span></td>
			<td class="center">89</td>
			<td class="center"><span title="14 Aug 2015, 22:26">55&nbsp;min.</span></td>
			<td class="green center">7</td>
			<td class="red lasttd center">250</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11081854,0" class="icomment icommentjs icon16" href="/cognition-game-of-the-year-edition-2-1-0-9-gog-t11081854.html#comment"> <em class="iconvalue">9</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/cognition-game-of-the-year-edition-2-1-0-9-gog-t11081854.html" class="cellMainLink">Cognition: Game of the Year Edition (2.1.0.9) (GOG)</a></div>
			</td>
			<td class="nobr center" data-sort="5181600319">4.83 <span>GB</span></td>
			<td class="center">11</td>
			<td class="center"><span title="12 Aug 2015, 05:16">2&nbsp;days</span></td>
			<td class="green center">100</td>
			<td class="red lasttd center">149</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11089983,0" class="icomment icommentjs icon16" href="/brawl-reloaded-t11089983.html#comment"> <em class="iconvalue">7</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/brawl-reloaded-t11089983.html" class="cellMainLink">BRAWL-RELOADED</a></div>
			</td>
			<td class="nobr center" data-sort="2764902656">2.58 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="13 Aug 2015, 15:57">1&nbsp;day</span></td>
			<td class="green center">97</td>
			<td class="red lasttd center">84</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11085113,0" class="icomment icommentjs icon16" href="/agarest-generations-of-war-2-gog-t11085113.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/agarest-generations-of-war-2-gog-t11085113.html" class="cellMainLink">Agarest: Generations of War 2 (GOG)</a></div>
			</td>
			<td class="nobr center" data-sort="6712836217">6.25 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="12 Aug 2015, 15:54">2&nbsp;days</span></td>
			<td class="green center">62</td>
			<td class="red lasttd center">116</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11080473,0" class="icomment icommentjs icon16" href="/this-war-of-mine-v1-1-0-android-t11080473.html#comment"> <em class="iconvalue">22</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/this-war-of-mine-v1-1-0-android-t11080473.html" class="cellMainLink">This War of Mine v1.1.0 Android</a></div>
			</td>
			<td class="nobr center" data-sort="427908957">408.09 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="12 Aug 2015, 00:23">2&nbsp;days</span></td>
			<td class="green center">117</td>
			<td class="red lasttd center">54</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11095940,0" class="icomment icommentjs icon16" href="/dark-realm-2-princess-of-ice-collector-s-edition-asg-t11095940.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/dark-realm-2-princess-of-ice-collector-s-edition-asg-t11095940.html" class="cellMainLink">Dark Realm 2- Princess of Ice Collector&#039;s Edition [ASG]</a></div>
			</td>
			<td class="nobr center" data-sort="967531808">922.71 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="14 Aug 2015, 19:22">3&nbsp;hours</span></td>
			<td class="green center">16</td>
			<td class="red lasttd center">47</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="rsssign" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11077619,0" class="icomment icommentjs icon16" href="/ms-windows-10-x64-untouched-genuine-key-toolkit-sb-danhuk-t11077619.html#comment"> <em class="iconvalue">125</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/ms-windows-10-x64-untouched-genuine-key-toolkit-sb-danhuk-t11077619.html" class="cellMainLink">MS Windows 10 (x64) Untouched+Genuine Key+Toolkit+SB [danhuk]</a></div>
			</td>
			<td class="nobr center" data-sort="4175728780">3.89 <span>GB</span></td>
			<td class="center">11</td>
			<td class="center"><span title="11 Aug 2015, 10:25">3&nbsp;days</span></td>
			<td class="green center">499</td>
			<td class="red lasttd center">702</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11083878,0" class="icomment icommentjs icon16" href="/kmspico-10-1-1-final-portable-office-and-windows-10-activator-techtools-t11083878.html#comment"> <em class="iconvalue">50</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/kmspico-10-1-1-final-portable-office-and-windows-10-activator-techtools-t11083878.html" class="cellMainLink">KMSpico 10.1.1 FINAL + Portable (Office and Windows 10 Activator) [TechTools]</a></div>
			</td>
			<td class="nobr center" data-sort="7081274">6.75 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="12 Aug 2015, 10:03">2&nbsp;days</span></td>
			<td class="green center">817</td>
			<td class="red lasttd center">72</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11085064,0" class="icomment icommentjs icon16" href="/microsoft-office-profissional-plus-2016-x32-x64-portugues-br-ativacao-t11085064.html#comment"> <em class="iconvalue">14</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/microsoft-office-profissional-plus-2016-x32-x64-portugues-br-ativacao-t11085064.html" class="cellMainLink">Microsoft Office Profissional Plus 2016 x32 x64 Portugues BR + Ativacao</a></div>
			</td>
			<td class="nobr center" data-sort="2741424672">2.55 <span>GB</span></td>
			<td class="center">20</td>
			<td class="center"><span title="12 Aug 2015, 15:39">2&nbsp;days</span></td>
			<td class="green center">315</td>
			<td class="red lasttd center">359</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11084949,0" class="icomment icommentjs icon16" href="/microsoft-visual-studio-professional-2015-serial-h4ckus-glodls-t11084949.html#comment"> <em class="iconvalue">7</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/microsoft-visual-studio-professional-2015-serial-h4ckus-glodls-t11084949.html" class="cellMainLink">Microsoft Visual Studio Professional 2015 + Serial [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="4070976069">3.79 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="12 Aug 2015, 15:02">2&nbsp;days</span></td>
			<td class="green center">186</td>
			<td class="red lasttd center">346</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11085020,0" class="icomment icommentjs icon16" href="/utorrent-pro-3-4-3-build-40907-stable-crack-h4ckus-glodls-t11085020.html#comment"> <em class="iconvalue">61</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/utorrent-pro-3-4-3-build-40907-stable-crack-h4ckus-glodls-t11085020.html" class="cellMainLink">uTorrent PRO 3.4.3 build 40907 Stable + Crack [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="5702535">5.44 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="12 Aug 2015, 15:28">2&nbsp;days</span></td>
			<td class="green center">419</td>
			<td class="red lasttd center">40</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11095093,0" class="icomment icommentjs icon16" href="/centos-7-x86-64-minimal-1503-01-t11095093.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/centos-7-x86-64-minimal-1503-01-t11095093.html" class="cellMainLink">CentOS 7 x86_64 Minimal 1503 01</a></div>
			</td>
			<td class="nobr center" data-sort="666900362">636.01 <span>MB</span></td>
			<td class="center">7</td>
			<td class="center"><span title="14 Aug 2015, 15:33">7&nbsp;hours</span></td>
			<td class="green center">341</td>
			<td class="red lasttd center">66</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11092185,0" class="icomment icommentjs icon16" href="/windows-7-ultimate-sp1-x64-en-us-oem-esd-aug2015-pre-activation-team-os-t11092185.html#comment"> <em class="iconvalue">11</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-7-ultimate-sp1-x64-en-us-oem-esd-aug2015-pre-activation-team-os-t11092185.html" class="cellMainLink">Windows 7 Ultimate Sp1 x64 En-Us OEM ESD Aug2015 Pre-Activation=-{TEAM OS}=</a></div>
			</td>
			<td class="nobr center" data-sort="3302232665">3.08 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="14 Aug 2015, 03:00">20&nbsp;hours</span></td>
			<td class="green center">88</td>
			<td class="red lasttd center">187</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11085578,0" class="icomment icommentjs icon16" href="/android-studio-1-2-2-build-ai-141-1980579-appzdam-t11085578.html#comment"> <em class="iconvalue">16</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/android-studio-1-2-2-build-ai-141-1980579-appzdam-t11085578.html" class="cellMainLink">Android Studio 1.2.2 Build #AI-141.1980579 - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="930456592">887.35 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="12 Aug 2015, 17:32">2&nbsp;days</span></td>
			<td class="green center">109</td>
			<td class="red lasttd center">95</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11087621,0" class="icomment icommentjs icon16" href="/image-line-fl-studio-12-1-2-producer-edition-32bit-64bit-eng-regkey-r2r-at-team-t11087621.html#comment"> <em class="iconvalue">12</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/image-line-fl-studio-12-1-2-producer-edition-32bit-64bit-eng-regkey-r2r-at-team-t11087621.html" class="cellMainLink">Image-Line FL Studio 12.1.2 Producer Edition - 32bit / 64bit [ENG] [RegKey-R2R] [AT-TEAM]</a></div>
			</td>
			<td class="nobr center" data-sort="517071122">493.12 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="13 Aug 2015, 04:41">1&nbsp;day</span></td>
			<td class="green center">137</td>
			<td class="red lasttd center">62</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11091610,0" class="icomment icommentjs icon16" href="/serif-drawplus-x8-v14-0-0-19-incl-keymaker-core-deepstatus-t11091610.html#comment"> <em class="iconvalue">15</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/serif-drawplus-x8-v14-0-0-19-incl-keymaker-core-deepstatus-t11091610.html" class="cellMainLink">Serif DrawPlus X8 v14.0.0.19 Incl.Keymaker-CORE [deepstatus]</a></div>
			</td>
			<td class="nobr center" data-sort="522575942">498.37 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="14 Aug 2015, 00:20">23&nbsp;hours</span></td>
			<td class="green center">114</td>
			<td class="red lasttd center">83</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11092194,0" class="icomment icommentjs icon16" href="/windows-8-1-pro-vl-update-3-x64-en-us-esd-aug-2015-incl-activator-team-os-t11092194.html#comment"> <em class="iconvalue">14</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-8-1-pro-vl-update-3-x64-en-us-esd-aug-2015-incl-activator-team-os-t11092194.html" class="cellMainLink">Windows 8.1 Pro Vl Update 3 x64 En-Us ESD Aug 2015 Incl Activator-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="4210272903">3.92 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="14 Aug 2015, 03:06">20&nbsp;hours</span></td>
			<td class="green center">57</td>
			<td class="red lasttd center">134</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11085619,0" class="icomment icommentjs icon16" href="/corel-videostudio-ultimate-x8-18-5-0-23-sp2-64-bit-content-patch-appzdam-t11085619.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/corel-videostudio-ultimate-x8-18-5-0-23-sp2-64-bit-content-patch-appzdam-t11085619.html" class="cellMainLink">Corel VideoStudio Ultimate X8 18.5.0.23 SP2 [64 bit] + Content + Patch - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="4327286447">4.03 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span title="12 Aug 2015, 17:44">2&nbsp;days</span></td>
			<td class="green center">80</td>
			<td class="red lasttd center">82</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11086405,0" class="icomment icommentjs icon16" href="/e-instruments-session-keys-grand-y-ver-1-1-kontakt-t11086405.html#comment"> <em class="iconvalue">8</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/e-instruments-session-keys-grand-y-ver-1-1-kontakt-t11086405.html" class="cellMainLink">E-Instruments Session Keys Grand Y ver 1.1 KONTAKT</a></div>
			</td>
			<td class="nobr center" data-sort="4337836953">4.04 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span title="12 Aug 2015, 21:03">2&nbsp;days</span></td>
			<td class="green center">41</td>
			<td class="red lasttd center">46</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11094105,0" class="icomment icommentjs icon16" href="/nvidia-geforce-desktop-355-60-for-notebooks-t11094105.html#comment"> <em class="iconvalue">8</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/nvidia-geforce-desktop-355-60-for-notebooks-t11094105.html" class="cellMainLink">NVIDIA GeForce Desktop 355.60 + For Notebooks</a></div>
			</td>
			<td class="nobr center" data-sort="1497989248">1.4 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="14 Aug 2015, 12:16">11&nbsp;hours</span></td>
			<td class="green center">29</td>
			<td class="red lasttd center">43</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11095951,0" class="icomment icommentjs icon16" href="/k-lite-codec-pack-11-3-6-mega-full-basic-standard-update-2015-pc-t11095951.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/k-lite-codec-pack-11-3-6-mega-full-basic-standard-update-2015-pc-t11095951.html" class="cellMainLink">K-Lite Codec Pack 11.3.6 Mega/Full/Basic/Standard + Update (2015) PC</a></div>
			</td>
			<td class="nobr center" data-sort="158531024">151.19 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="14 Aug 2015, 19:25">3&nbsp;hours</span></td>
			<td class="green center">23</td>
			<td class="red lasttd center">40</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="rsssign" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ohys-raws-overlord-06-at-x-1280x720-x264-aac-mp4-t11078849.html" class="cellMainLink">[Ohys-Raws] Overlord - 06 (AT-X 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="351374464">335.1 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="11 Aug 2015, 15:46">3&nbsp;days</span></td>
			<td class="green center">1287</td>
			<td class="red lasttd center">535</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11090750,0" class="icomment icommentjs icon16" href="/fansub-resistance-naruto-shippuuden-424-1280x720-mp4-t11090750.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fansub-resistance-naruto-shippuuden-424-1280x720-mp4-t11090750.html" class="cellMainLink">[Fansub-Resistance]Naruto Shippuuden 424 (1280x720).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="207605657">197.99 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="13 Aug 2015, 18:40">1&nbsp;day</span></td>
			<td class="green center">1340</td>
			<td class="red lasttd center">455</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11090798,0" class="icomment icommentjs icon16" href="/horriblesubs-okusama-ga-seitokaichou-uncensored-07-720p-mkv-t11090798.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-okusama-ga-seitokaichou-uncensored-07-720p-mkv-t11090798.html" class="cellMainLink">[HorribleSubs] Okusama ga Seitokaichou! (Uncensored) - 07 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="114240710">108.95 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="13 Aug 2015, 18:55">1&nbsp;day</span></td>
			<td class="green center">927</td>
			<td class="red lasttd center">272</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11089229,0" class="icomment icommentjs icon16" href="/naruto-shippuden-episode-424-english-subbed-480p-arizone-t11089229.html#comment"> <em class="iconvalue">25</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/naruto-shippuden-episode-424-english-subbed-480p-arizone-t11089229.html" class="cellMainLink">Naruto Shippuden Episode 424 [English Subbed] 480p ~ARIZONE</a></div>
			</td>
			<td class="nobr center" data-sort="89261999">85.13 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="13 Aug 2015, 12:34">1&nbsp;day</span></td>
			<td class="green center">567</td>
			<td class="red lasttd center">254</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11084580,0" class="icomment icommentjs icon16" href="/rhxhy-sore-ga-seiyuu-06-big5-720p-mp4-t11084580.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/rhxhy-sore-ga-seiyuu-06-big5-720p-mp4-t11084580.html" class="cellMainLink">[RHxHY]Sore_ga_Seiyuu[06][BIG5][720P].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="180528807">172.17 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="12 Aug 2015, 13:22">2&nbsp;days</span></td>
			<td class="green center">428</td>
			<td class="red lasttd center">152</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11095765,0" class="icomment icommentjs icon16" href="/horriblesubs-fate-kaleid-liner-prisma-ilya-2wei-herz-04-720p-mkv-t11095765.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-fate-kaleid-liner-prisma-ilya-2wei-herz-04-720p-mkv-t11095765.html" class="cellMainLink">[HorribleSubs] Fate Kaleid Liner PRISMA ILYA 2wei Herz! - 04 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="335554391">320.01 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="14 Aug 2015, 18:40">4&nbsp;hours</span></td>
			<td class="green center">49</td>
			<td class="red lasttd center">355</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11083719,0" class="icomment icommentjs icon16" href="/dmxf-hkacg-monster-musume-no-iru-nichijou-06-big5-x264-aac-720p-mp4-t11083719.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dmxf-hkacg-monster-musume-no-iru-nichijou-06-big5-x264-aac-720p-mp4-t11083719.html" class="cellMainLink">[DMXF&amp;HKACG][Monster Musume no Iru Nichijou][06][BIG5][x264_AAC][720p].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="204418146">194.95 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="12 Aug 2015, 09:18">2&nbsp;days</span></td>
			<td class="green center">213</td>
			<td class="red lasttd center">121</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11079248,0" class="icomment icommentjs icon16" href="/elfen-lied-complete-series-ost-high-quality-dual-audio-mkv-720p-blu-ray-rip-t11079248.html#comment"> <em class="iconvalue">12</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/elfen-lied-complete-series-ost-high-quality-dual-audio-mkv-720p-blu-ray-rip-t11079248.html" class="cellMainLink">Elfen Lied Complete Series + OST (High Quality)(Dual Audio) MKV [720p] Blu-Ray Rip</a></div>
			</td>
			<td class="nobr center" data-sort="11154335413">10.39 <span>GB</span></td>
			<td class="center">46</td>
			<td class="center"><span title="11 Aug 2015, 17:22">3&nbsp;days</span></td>
			<td class="green center">106</td>
			<td class="red lasttd center">198</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/vivid-non-non-biyori-repeat-06-c475136b-mkv-t11078483.html" class="cellMainLink">[Vivid] Non Non Biyori Repeat - 06 [C475136B].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="327424970">312.26 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="11 Aug 2015, 14:23">3&nbsp;days</span></td>
			<td class="green center">131</td>
			<td class="red lasttd center">171</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/c-cå¨æ¼«-ccwzz-cc-çº¢é¾æå½¹-ç¬¬07è¯-720p-mp4-t11089917.html" class="cellMainLink">ãc.cå¨æ¼« ccwzz.ccãçº¢é¾æå½¹ãç¬¬07è¯ã720P.mp4</a></div>
			</td>
			<td class="nobr center" data-sort="177165059">168.96 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="13 Aug 2015, 15:40">1&nbsp;day</span></td>
			<td class="green center">145</td>
			<td class="red lasttd center">125</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11089200,0" class="icomment icommentjs icon16" href="/animerg-naruto-shippuuden-424-english-subbed-480p-kami-t11089200.html#comment"> <em class="iconvalue">8</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-naruto-shippuuden-424-english-subbed-480p-kami-t11089200.html" class="cellMainLink">[AnimeRG] Naruto Shippuuden - 424 [English Subbed][480p] [KaMi]</a></div>
			</td>
			<td class="nobr center" data-sort="78559861">74.92 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="13 Aug 2015, 12:27">1&nbsp;day</span></td>
			<td class="green center">154</td>
			<td class="red lasttd center">109</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11074633,0" class="icomment icommentjs icon16" href="/æ¢¦èå­å¹ç»-åå¦aæ¢¦2015å¤§éçå®å®è±éè®°-doraemon-movie2015-nobita-s-speace-heros-1080p-gb-mkv-mp4-x264-x265-t11074633.html#comment"> <em class="iconvalue">7</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/æ¢¦èå­å¹ç»-åå¦aæ¢¦2015å¤§éçå®å®è±éè®°-doraemon-movie2015-nobita-s-speace-heros-1080p-gb-mkv-mp4-x264-x265-t11074633.html" class="cellMainLink">[æ¢¦èå­å¹ç»]åå¦Aæ¢¦2015å¤§éçå®å®è±éè®°-Doraemon movie2015 Nobita&#039;s Speace Heros[1080p][GB][mkv&amp;mp4][x264&amp;x265]</a></div>
			</td>
			<td class="nobr center" data-sort="5620390400">5.23 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="10 Aug 2015, 23:57">3&nbsp;days</span></td>
			<td class="green center">83</td>
			<td class="red lasttd center">96</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-the-idolm-ster-cinderella-girls-18-1080p-mkv-t11095369.html" class="cellMainLink">[HorribleSubs] THE iDOLM@STER CINDERELLA GIRLS - 18 [1080p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="829012621">790.61 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="14 Aug 2015, 16:55">6&nbsp;hours</span></td>
			<td class="green center">16</td>
			<td class="red lasttd center">68</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-himouto-umaru-chan-06-raw-abc-1280x720-x264-aac-mp4-t11087983.html" class="cellMainLink">[Leopard-Raws] Himouto! Umaru-chan - 06 RAW (ABC 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="335967578">320.4 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="13 Aug 2015, 06:20">1&nbsp;day</span></td>
			<td class="green center">3</td>
			<td class="red lasttd center">81</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/vivid-gakkou-gurashi-06-0a634917-mkv-t11094270.html" class="cellMainLink">[Vivid] Gakkou Gurashi! - 06 [0A634917].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="438707696">418.38 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="14 Aug 2015, 12:55">10&nbsp;hours</span></td>
			<td class="green center">4</td>
			<td class="red lasttd center">59</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="rsssign" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11085515,0" class="icomment icommentjs icon16" href="/marvel-week-08-12-2015-nem-t11085515.html#comment"> <em class="iconvalue">24</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/marvel-week-08-12-2015-nem-t11085515.html" class="cellMainLink">Marvel Week+ (08-12-2015) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="687332495">655.49 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center"><span title="12 Aug 2015, 17:22">2&nbsp;days</span></td>
			<td class="green center">731</td>
			<td class="red lasttd center">364</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11085249,0" class="icomment icommentjs icon16" href="/dc-week-08-12-2015-aka-dc-you-week-11-nem-t11085249.html#comment"> <em class="iconvalue">30</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/dc-week-08-12-2015-aka-dc-you-week-11-nem-t11085249.html" class="cellMainLink">DC Week+ (08-12-2015) (aka DC YOU Week 11) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="513694504">489.9 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span title="12 Aug 2015, 16:25">2&nbsp;days</span></td>
			<td class="green center">634</td>
			<td class="red lasttd center">269</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11091330,0" class="icomment icommentjs icon16" href="/assorted-magazines-bundle-august-14-2015-true-pdf-t11091330.html#comment"> <em class="iconvalue">8</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/assorted-magazines-bundle-august-14-2015-true-pdf-t11091330.html" class="cellMainLink">Assorted Magazines Bundle - August 14 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="480346217">458.09 <span>MB</span></td>
			<td class="center">26</td>
			<td class="center"><span title="13 Aug 2015, 22:21">1&nbsp;day</span></td>
			<td class="green center">416</td>
			<td class="red lasttd center">380</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11084042,0" class="icomment icommentjs icon16" href="/secret-wars-05-of-08-2015-digital-zone-empire-cbr-t11084042.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/secret-wars-05-of-08-2015-digital-zone-empire-cbr-t11084042.html" class="cellMainLink">Secret Wars 05 (of 08) (2015) (Digital) (Zone-Empire).cbr</a></div>
			</td>
			<td class="nobr center" data-sort="40034790">38.18 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="12 Aug 2015, 10:58">2&nbsp;days</span></td>
			<td class="green center">477</td>
			<td class="red lasttd center">50</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11081180,0" class="icomment icommentjs icon16" href="/food-magazines-bundle-august-12-2015-true-pdf-t11081180.html#comment"> <em class="iconvalue">7</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/food-magazines-bundle-august-12-2015-true-pdf-t11081180.html" class="cellMainLink">Food Magazines Bundle - August 12 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="419855695">400.41 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span title="12 Aug 2015, 03:08">2&nbsp;days</span></td>
			<td class="green center">305</td>
			<td class="red lasttd center">124</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11083138,0" class="icomment icommentjs icon16" href="/0-day-week-of-2015-08-05-t11083138.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/0-day-week-of-2015-08-05-t11083138.html" class="cellMainLink">0-Day Week of 2015.08.05</a></div>
			</td>
			<td class="nobr center" data-sort="9000188930">8.38 <span>GB</span></td>
			<td class="center">180</td>
			<td class="center"><span title="12 Aug 2015, 06:40">2&nbsp;days</span></td>
			<td class="green center">104</td>
			<td class="red lasttd center">288</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11090341,0" class="icomment icommentjs icon16" href="/latest-ebook-dump-pack-1-romance-mystery-thriller-sci-fi-history-biography-t11090341.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/latest-ebook-dump-pack-1-romance-mystery-thriller-sci-fi-history-biography-t11090341.html" class="cellMainLink">Latest eBook Dump (Pack #1) [Romance, Mystery, Thriller, Sci-Fi, History, Biography]</a></div>
			</td>
			<td class="nobr center" data-sort="632521864">603.22 <span>MB</span></td>
			<td class="center">463</td>
			<td class="center"><span title="13 Aug 2015, 17:19">1&nbsp;day</span></td>
			<td class="green center">261</td>
			<td class="red lasttd center">105</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11078671,0" class="icomment icommentjs icon16" href="/injustice-gods-among-us-year-four-015-2015-digital-son-of-ultron-empire-cbr-nem-t11078671.html#comment"> <em class="iconvalue">16</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/injustice-gods-among-us-year-four-015-2015-digital-son-of-ultron-empire-cbr-nem-t11078671.html" class="cellMainLink">Injustice - Gods Among Us - Year Four 015 (2015) (digital) (Son of Ultron-Empire).cbr (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="17688702">16.87 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="11 Aug 2015, 15:11">3&nbsp;days</span></td>
			<td class="green center">306</td>
			<td class="red lasttd center">40</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11087274,0" class="icomment icommentjs icon16" href="/athletic-fitness-health-sports-magazines-august-13-2015-true-pdf-t11087274.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/athletic-fitness-health-sports-magazines-august-13-2015-true-pdf-t11087274.html" class="cellMainLink">Athletic Fitness Health &amp; Sports Magazines - August 13 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="223736138">213.37 <span>MB</span></td>
			<td class="center">11</td>
			<td class="center"><span title="13 Aug 2015, 02:26">1&nbsp;day</span></td>
			<td class="green center">174</td>
			<td class="red lasttd center">140</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11074738,0" class="icomment icommentjs icon16" href="/automobile-magazines-bundle-august-11-2015-true-pdf-t11074738.html#comment"> <em class="iconvalue">9</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/automobile-magazines-bundle-august-11-2015-true-pdf-t11074738.html" class="cellMainLink">Automobile Magazines Bundle - August 11 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="646416621">616.47 <span>MB</span></td>
			<td class="center">21</td>
			<td class="center"><span title="11 Aug 2015, 00:42">3&nbsp;days</span></td>
			<td class="green center">209</td>
			<td class="red lasttd center">97</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11084015,0" class="icomment icommentjs icon16" href="/a-force-003-2015-digital-zone-empire-cbr-t11084015.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/a-force-003-2015-digital-zone-empire-cbr-t11084015.html" class="cellMainLink">A-Force 003 (2015) (Digital) (Zone-Empire).cbr</a></div>
			</td>
			<td class="nobr center" data-sort="39603208">37.77 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="12 Aug 2015, 10:52">2&nbsp;days</span></td>
			<td class="green center">176</td>
			<td class="red lasttd center">60</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11082649,0" class="icomment icommentjs icon16" href="/womens-magazines-bundle-august-12-2015-true-pdf-t11082649.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/womens-magazines-bundle-august-12-2015-true-pdf-t11082649.html" class="cellMainLink">Womens Magazines Bundle - August 12 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="229542930">218.91 <span>MB</span></td>
			<td class="center">7</td>
			<td class="center"><span title="12 Aug 2015, 05:56">2&nbsp;days</span></td>
			<td class="green center">142</td>
			<td class="red lasttd center">76</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11086055,0" class="icomment icommentjs icon16" href="/theories-of-personality-editions-4-6-7-8-9-10-2003-2012-pdf-gooner-t11086055.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/theories-of-personality-editions-4-6-7-8-9-10-2003-2012-pdf-gooner-t11086055.html" class="cellMainLink">Theories Of Personality - Editions 4,6,7,8,9 &amp; 10 (2003-2012) (Pdf) Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="195119473">186.08 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="12 Aug 2015, 19:44">2&nbsp;days</span></td>
			<td class="green center">100</td>
			<td class="red lasttd center">68</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11083156,0" class="icomment icommentjs icon16" href="/hitlist-week-of-2015-08-05-t11083156.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/hitlist-week-of-2015-08-05-t11083156.html" class="cellMainLink">Hitlist Week of 2015.08.05</a></div>
			</td>
			<td class="nobr center" data-sort="26991008282">25.14 <span>GB</span></td>
			<td class="center">390</td>
			<td class="center"><span title="12 Aug 2015, 06:50">2&nbsp;days</span></td>
			<td class="green center">36</td>
			<td class="red lasttd center">111</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11085225,0" class="icomment icommentjs icon16" href="/earth-2-society-003-2015-digital-thatguy-empire-cbz-t11085225.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/earth-2-society-003-2015-digital-thatguy-empire-cbz-t11085225.html" class="cellMainLink">Earth 2 - Society 003 (2015) (Digital) (ThatGuy-Empire).cbz</a></div>
			</td>
			<td class="nobr center" data-sort="37151505">35.43 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="12 Aug 2015, 16:19">2&nbsp;days</span></td>
			<td class="green center">77</td>
			<td class="red lasttd center">49</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="rsssign" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11080104,0" class="icomment icommentjs icon16" href="/jethro-tull-war-child-2014-anniversary-edition-96-24-dvd-a-flac-t11080104.html#comment"> <em class="iconvalue">13</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/jethro-tull-war-child-2014-anniversary-edition-96-24-dvd-a-flac-t11080104.html" class="cellMainLink">Jethro Tull - War Child (2014 Anniversary Edition) [96-24 DVD-A FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="3265953428">3.04 <span>GB</span></td>
			<td class="center">66</td>
			<td class="center"><span title="11 Aug 2015, 22:30">3&nbsp;days</span></td>
			<td class="green center">121</td>
			<td class="red lasttd center">91</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11074802,0" class="icomment icommentjs icon16" href="/tchaikovsky-symphonies-rostropovich-t11074802.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/tchaikovsky-symphonies-rostropovich-t11074802.html" class="cellMainLink">Tchaikovsky - Symphonies - Rostropovich</a></div>
			</td>
			<td class="nobr center" data-sort="1745244019">1.63 <span>GB</span></td>
			<td class="center">57</td>
			<td class="center"><span title="11 Aug 2015, 01:09">3&nbsp;days</span></td>
			<td class="green center">130</td>
			<td class="red lasttd center">54</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11089489,0" class="icomment icommentjs icon16" href="/emerson-lake-palmer-brain-salad-surgery-deluxe-2014-24-96-44-hd-flac-t11089489.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/emerson-lake-palmer-brain-salad-surgery-deluxe-2014-24-96-44-hd-flac-t11089489.html" class="cellMainLink">Emerson, Lake &amp; Palmer - Brain Salad Surgery (Deluxe 2014) [24-96/44 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1806247515">1.68 <span>GB</span></td>
			<td class="center">34</td>
			<td class="center"><span title="13 Aug 2015, 13:48">1&nbsp;day</span></td>
			<td class="green center">95</td>
			<td class="red lasttd center">49</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11090680,0" class="icomment icommentjs icon16" href="/stray-cats-runaway-boys-a-retrospective-81-to-92-1996-flac-vtwin88cube-t11090680.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/stray-cats-runaway-boys-a-retrospective-81-to-92-1996-flac-vtwin88cube-t11090680.html" class="cellMainLink">Stray Cats - Runaway Boys A Retrospective &#039;81 To &#039;92 (1996) {FLAC} vtwin88cube</a></div>
			</td>
			<td class="nobr center" data-sort="567712678">541.41 <span>MB</span></td>
			<td class="center">31</td>
			<td class="center"><span title="13 Aug 2015, 18:26">1&nbsp;day</span></td>
			<td class="green center">115</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11078460,0" class="icomment icommentjs icon16" href="/joe-bonamassa-muddy-wolf-at-red-rocks-2015-flac-t11078460.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/joe-bonamassa-muddy-wolf-at-red-rocks-2015-flac-t11078460.html" class="cellMainLink">Joe Bonamassa - Muddy Wolf at Red Rocks (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="878721406">838.01 <span>MB</span></td>
			<td class="center">53</td>
			<td class="center"><span title="11 Aug 2015, 14:18">3&nbsp;days</span></td>
			<td class="green center">109</td>
			<td class="red lasttd center">25</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11092416,0" class="icomment icommentjs icon16" href="/the-paul-butterfield-blues-band-the-resurrection-of-pigboy-crabshaw-2015-24-192-hd-flac-t11092416.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-paul-butterfield-blues-band-the-resurrection-of-pigboy-crabshaw-2015-24-192-hd-flac-t11092416.html" class="cellMainLink">The Paul Butterfield Blues Band - The Resurrection of Pigboy Crabshaw (2015) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1868961295">1.74 <span>GB</span></td>
			<td class="center">24</td>
			<td class="center"><span title="14 Aug 2015, 04:09">19&nbsp;hours</span></td>
			<td class="green center">71</td>
			<td class="red lasttd center">54</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11085700,0" class="icomment icommentjs icon16" href="/lou-reed-berlin-2015-96-24-hd-flac-t11085700.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/lou-reed-berlin-2015-96-24-hd-flac-t11085700.html" class="cellMainLink">Lou Reed - Berlin (2015) [96-24 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1062262531">1013.05 <span>MB</span></td>
			<td class="center">25</td>
			<td class="center"><span title="12 Aug 2015, 18:03">2&nbsp;days</span></td>
			<td class="green center">78</td>
			<td class="red lasttd center">25</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11077871,0" class="icomment icommentjs icon16" href="/bill-withers-menagerie-2015-hdtracks-24bit-96-flac-beolab1700-t11077871.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bill-withers-menagerie-2015-hdtracks-24bit-96-flac-beolab1700-t11077871.html" class="cellMainLink">Bill Withers - Menagerie (2015) [HDtracks] 24bit 96 FLAC Beolab1700</a></div>
			</td>
			<td class="nobr center" data-sort="807152653">769.76 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span title="11 Aug 2015, 11:32">3&nbsp;days</span></td>
			<td class="green center">69</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/v-a-all-the-best-from-india-classical-ragas-1994-abee-t11088365.html" class="cellMainLink">V.A. - All the Best from India (Classical Ragas) 1994 ABEE</a></div>
			</td>
			<td class="nobr center" data-sort="292910941">279.34 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span title="13 Aug 2015, 08:16">1&nbsp;day</span></td>
			<td class="green center">67</td>
			<td class="red lasttd center">25</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11088670,0" class="icomment icommentjs icon16" href="/bill-withers-making-music-2015-hdtracks-24bit-96-flac-beolab1700-t11088670.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bill-withers-making-music-2015-hdtracks-24bit-96-flac-beolab1700-t11088670.html" class="cellMainLink">Bill Withers - Making Music (2015) [HDtracks] 24bit 96 FLAC Beolab1700</a></div>
			</td>
			<td class="nobr center" data-sort="862974652">823 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span title="13 Aug 2015, 10:03">1&nbsp;day</span></td>
			<td class="green center">62</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/jack-white-loretta-lynn-van-lear-rose-limited-edition-2015-tmr-24bit-flac-beolab1700-t11084003.html" class="cellMainLink">( Jack White ) Loretta Lynn - Van Lear Rose [Limited Edition] (2015) TMR 24bit FLAC Beolab1700</a></div>
			</td>
			<td class="nobr center" data-sort="945947255">902.13 <span>MB</span></td>
			<td class="center">33</td>
			<td class="center"><span title="12 Aug 2015, 10:47">2&nbsp;days</span></td>
			<td class="green center">57</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11094659,0" class="icomment icommentjs icon16" href="/buddy-guy-1999-buddy-s-baddest-the-best-of-buddy-guy-eac-flac-t11094659.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/buddy-guy-1999-buddy-s-baddest-the-best-of-buddy-guy-eac-flac-t11094659.html" class="cellMainLink">Buddy Guy - 1999 - Buddy&#039;s Baddest: The Best of Buddy Guy [EAC FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="529703584">505.16 <span>MB</span></td>
			<td class="center">24</td>
			<td class="center"><span title="14 Aug 2015, 14:24">8&nbsp;hours</span></td>
			<td class="green center">30</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11092523,0" class="icomment icommentjs icon16" href="/bullet-for-my-valentine-venom-deluxe-edition-2015-flac-pirate-shovon-t11092523.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bullet-for-my-valentine-venom-deluxe-edition-2015-flac-pirate-shovon-t11092523.html" class="cellMainLink">Bullet For My Valentine - Venom [Deluxe Edition] [2015] [FLAC] [Pirate Shovon]</a></div>
			</td>
			<td class="nobr center" data-sort="429831765">409.92 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span title="14 Aug 2015, 04:44">18&nbsp;hours</span></td>
			<td class="green center">29</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/vivaldi-complete-opus-8-12-concertos-including-the-four-seasons-harnoncourt-concentus-musicus-wien-2009-flac-t11089748.html" class="cellMainLink">Vivaldi - Complete Opus 8: 12 Concertos including The Four Seasons [Harnoncourt - Concentus musicus Wien] (2009) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="479900036">457.67 <span>MB</span></td>
			<td class="center">44</td>
			<td class="center"><span title="13 Aug 2015, 15:02">1&nbsp;day</span></td>
			<td class="green center">35</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11087050,0" class="icomment icommentjs icon16" href="/peter-apfelbaum-and-the-hieroglyphics-ensemble-signs-of-life-flac-mp3-big-papi-1991-exotic-jazz-t11087050.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/peter-apfelbaum-and-the-hieroglyphics-ensemble-signs-of-life-flac-mp3-big-papi-1991-exotic-jazz-t11087050.html" class="cellMainLink">Peter Apfelbaum and the Hieroglyphics Ensemble - Signs of Life [FLAC+MP3](Big Papi) 1991 Exotic Jazz</a></div>
			</td>
			<td class="nobr center" data-sort="514569547">490.73 <span>MB</span></td>
			<td class="center">28</td>
			<td class="center"><span title="13 Aug 2015, 01:08">1&nbsp;day</span></td>
			<td class="green center">23</td>
			<td class="red lasttd center">15</td>
        </tr>
			</table>


	<script type="text/javascript">
		if ($.cookie('kat_settings[fpw_column]') != null && $.cookie('kat_settings[fpw_order]')) {
			var colIndex = $.cookie('kat_settings[fpw_column]')
			var ascend = $.cookie('kat_settings[fpw_order]');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			sortTables(colIndex, ascend);
		}
		$('.frontPageWidget .firstr th .fpw-sort').click(function() {
			var colIndex = $(this).parent().index();
			var ascend = !$(this).hasClass('sortedAsc')?1:0;
			$('.fpw-sort').removeClass('sortedAsc sortedDesc');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			updateFPWsettings(colIndex, ascend);
			sortTables(colIndex, ascend);
		});
		function sortTables(colIndex, ascend) {	
			$('.frontPageWidget').each(function() {
				var data = [];
				$('tr:not(.firstr)', $(this)).each(function() {
					var name = $('td:first .cellMainLink', $(this)).text();
					var id = $('td:first .cellMainLink', $(this)).attr('href').match(/.*-t(\d+)\.html/)[1];
					var fileSize = $('td:eq(1)', $(this)).attr('data-sort');
					data.push({
						'col0':name, 'col1':padNum(fileSize), 'col2':padNum($('td:eq(2)',$(this)).text()), 'col3':padNum(id), 'col4':padNum($('td:eq(4)',$(this)).text()), 'col5':padNum($('td:eq(5)',$(this)).text()), 'html':$(this).html()
					});
				});
				data.sort(function(a, b) {
					var x = a['col'+colIndex].toLowerCase(); var y = b['col'+colIndex].toLowerCase();
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
				if (!ascend||ascend=='0') data.reverse();
				$(this).find('tr:not(.firstr)').remove();
				for (var i=0;i<data.length;i++) {
					$(this).append('<tr class="'+(i+1%2==0?'even':'odd')+'">'+data[i].html+'</tr>');
				}
			});
		}
		function padNum(val) {
			val = '0000000000000000'+val;
			return val.substring(val.length-15);
		}
		function updateFPWsettings(t, o) {
			$.cookie('kat_settings[fpw_column]', t, { expires: 365, path: '/' });
			$.cookie('kat_settings[fpw_order]', o, { expires: 365, path: '/' });
		}
	</script>
		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
    
            
        <div id="_119b0a17fab5493361a252d04bf527db"></div>
    
                
    	    <div class="advertising">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <div id="_7063408f1c01d50e0dc2d833186ce962"></div>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/what-movie-you-can-watch-over-and-over-again-without-ever-ge/?unread=16775656">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What movie you can watch over and over again, without ever getting bored?
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/iAMega/">iAMega</a></span></span> <span title="14 Aug 2015, 23:21">50&nbsp;sec.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/adding-subs-retagging-not-acceptable/?unread=16775654">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Adding Subs &amp; Retagging is NOT Acceptable
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_verified"><a class="plain" href="/user/ChillSpoon/">ChillSpoon</a></span></span> <span title="14 Aug 2015, 23:16">5&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/quiz-can-you-guess-movie-v11/?unread=16775651">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Quiz; Can you guess this movie? V11
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/titovicanjoey/">titovicanjoey</a></span></span> <span title="14 Aug 2015, 23:15">6&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/official-ff-ebook-requests-thread-not-final-fantasy-requests/?unread=16775648">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Official FF Ebook Requests Thread (not for Final Fantasy requests)
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/shipping_mad/">shipping_mad</a></span></span> <span title="14 Aug 2015, 23:13">8&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/what-movie-are-you-watching-right-now/?unread=16775647">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What Movie are you watching right now?
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_3"><a class="plain" href="/user/Judas/">Judas</a></span></span> <span title="14 Aug 2015, 23:13">8&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/fans-hannibal/?unread=16775645">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Fans Of Hannibal
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/floofiness/">floofiness</a></span></span> <span title="14 Aug 2015, 23:10">11&nbsp;min.&nbsp;ago</span></span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/kickasstorrents-is-moving-to-kat-cr-domain/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				KickassTorrents is moving to kat.cr domain
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="24 Apr 2015, 10:13">3&nbsp;months&nbsp;ago</span></span>
	</li>
	<li>
		<a href="/blog/post/happy-torrents-day-2015/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Happy Torrents Day 2015!
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="30 Mar 2015, 10:06">4&nbsp;months&nbsp;ago</span></span>
	</li>
	<li>
		<a href="/blog/post/technical-maintenance-march-17/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Technical maintenance (March, 17)
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="17 Mar 2015, 11:46">5&nbsp;months&nbsp;ago</span></span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/pleasebaby/post/english-is-a-difficult-language-for-some/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> English is a difficult language .... for some !</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/pleasebaby/">pleasebaby</a> <span title="14 Aug 2015, 15:37">7&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/eclipze_angel/post/we-want-everyone-to-have-a-good-time/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> We Want Everyone to Have a Good Time..</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/eclipze_angel/">eclipze_angel</a> <span title="14 Aug 2015, 11:01">12&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/ChillSpoon/post/my-trip-to-mexico/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> My Trip To Mexico</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/ChillSpoon/">ChillSpoon</a> <span title="14 Aug 2015, 09:54">13&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/Sality/post/erotic-portrait-and-metaphorical-robe/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> Erotic portrait and metaphorical robe</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/Sality/">Sality</a> <span title="14 Aug 2015, 09:03">14&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/http/post/my-happy-blog-without-an-ounce-of-sarcasm/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> My Happy Blog (Without an Ounce of Sarcasm)</p></a><span class="explanation">by <a class="plain aclColor_3" href="/user/http/">http</a> <span title="14 Aug 2015, 08:13">15&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/14MR3DS4ND/post/my-struggle-with-type-1-diabetes/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> My struggle with Type 1 Diabetes</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/14MR3DS4ND/">14MR3DS4ND</a> <span title="14 Aug 2015, 06:56">16&nbsp;hours&nbsp;ago</span></span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/step%20up%201/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				step up 1
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/how%20to%20train%20your%20dragon%202/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				how to train your dragon 2
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/malaysian%202015%20race/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				malaysian 2015 race
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/wwe%202013%20ppv/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				wwe 2013 ppv
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/ray%20donovan%20season%202/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				ray donovan season 2
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/paige%20net/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				paige net
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/the%2Bforgotten%2Bage/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				the+forgotten+age
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/au%20secours/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				au secours
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/p4f/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				p4f
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/pdf%20to%20ward/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				pdf to ward
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/n.w.a.%20torrent/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				n.w.a. torrent
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		<li>
			<a data-nop href="http://www.torrentreactor.net/" target="_blank" rel="external">
				<span class="itorreact thirdPartIcons"></span>TorrentReactor
			</a>
		</li>
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
		<li><a href="/welcome/">welcome</a></li>
		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <div id="_673e31f53f8166159b8e996c4124765b"></div>
        <div id="_e7050fb15fd39b3e4e99a5be4a57b6ea"></div>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script><script>
 sc('addGlobal', 'pagetype', 'front');
 sc('addSlot', '_60318cd4e8d28f6fb76fe34e9bd9c498');
sc('addSlot', '_39ecb76dd457e5ac33776fdf11500d56');
sc('addSlot', '_277923e5f9d753c5b0630c28e641790c');
sc('addSlot', '_119b0a17fab5493361a252d04bf527db');
sc('addSlot', '_7063408f1c01d50e0dc2d833186ce962', { 'searchQuery': '' });
sc('addSlot', '_673e31f53f8166159b8e996c4124765b');
sc('addSlot', '_e7050fb15fd39b3e4e99a5be4a57b6ea');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>
</body>
</html>
