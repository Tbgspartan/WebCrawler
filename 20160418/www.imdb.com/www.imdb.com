



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

    
    
    

    
    
    

    <meta name="apple-itunes-app" content="app-id=342792525, app-argument=imdb:///?src=mdot">
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "058-3091348-0031784";
                var ue_id = "12XARZSAAFGZ0P7VQVTB";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        
        <meta property="pageType" content="home" />
        <meta property="subpageType" content="main" />


        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />

    <meta property='fb:app_id' content='115109575169727' />

    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="12XARZSAAFGZ0P7VQVTB" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1e-c3-2xl-i-88695312.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-903233829._CB298432192_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-3796876714._CB298540748_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-2454701167._CB293329573_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-2508181169._CB288096406_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-3988610918._CB289934424_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
!function a(b,c,d){function e(g,h){if(!c[g]){if(!b[g]){var i="function"==typeof require&&require;if(!h&&i)return i(g,!0);if(f)return f(g,!0);var j=new Error("Cannot find module '"+g+"'");throw j.code="MODULE_NOT_FOUND",j}var k=c[g]={exports:{}};b[g][0].call(k.exports,function(a){var c=b[g][1][a];return e(c?c:a)},k,k.exports,a,b,c,d)}return c[g].exports}for(var f="function"==typeof require&&require,g=0;g<d.length;g++)e(d[g]);return e}({1:[function(a,b,c){"use strict";a(2)},{2:2}],2:[function(a,b,c){"use strict";!function(){var a,b,c=function(a){return"[object Array]"===Object.prototype.toString.call(a)},d=function(a,b){for(var c=0;c<a.length;c++)c in a&&b.call(null,a[c],c)},e=[],f=!1,g=!1,h=function(){var a=[],b=[],c={};return d(e,function(e){var f="";d(e.dartsite.split("/"),function(b){""!==b&&(b in c||(c[b]=a.length,a.push(b)),f+="/"+c[b])}),b.push(f)}),{iu_parts:a,enc_prev_ius:b}},i=function(){var a=[];return d(e,function(b){var c=[];d(b.sizes,function(a){c.push(a.join("x"))}),a.push(c.join("|"))}),a},j=function(){var a=[];return d(e,function(b){a.push(k(b.targeting))}),a.join("|")},k=function(a,b){var c,d=[];for(var e in a){c=[];for(var f=0;f<a[e].length;f++)c.push(encodeURIComponent(a[e][f]));b?d.push(e+"="+encodeURIComponent(c.join(","))):d.push(e+"="+c.join(","))}return d.join("&")},l=function(){var a=+new Date;g||document.readyState&&"loaded"!==document.readyState||(g=!0,f&&imdbads.cmd.push(function(){for(var b=0;b<e.length;b++)generic.monitoring.record_metric(e[b].name+".fail",csm.duration(a))}))};window.tinygpt={define_slot:function(a,b,c,d){e.push({dartsite:a.replace(/\/$/,""),sizes:b,name:c,targeting:d})},set_targeting:function(b){a=b},callback:function(a){for(var c,d,f={},g=+new Date,h=0;h<e.length;h++)c=e[h].dartsite,d=e[h].name,a[h][c]?f[d]=a[h][c]:window.console&&console.error&&console.error("Unable to correlate GPT response for "+d);imdbads.cmd.push(function(){for(var a=0;a<e.length;a++)ad_utils.slot_events.trigger(e[a].name,"request",{timestamp:b}),ad_utils.slot_events.trigger(e[a].name,"tagdeliver",{timestamp:g});ad_utils.gpt.handle_response(f)})},send:function(){var d,g,m=[],n=function(a,b){c(b)&&(b=b.join(",")),b&&m.push(a+"="+encodeURIComponent(""+b))};if(0===e.length)return void tinygpt.callback({});n("gdfp_req","1"),n("correlator",Math.floor(4503599627370496*Math.random())),n("output","json_html"),n("callback","tinygpt.callback"),n("impl","fifs"),n("json_a","1");var o=h();n("iu_parts",o.iu_parts),n("enc_prev_ius",o.enc_prev_ius),n("prev_iu_szs",i()),n("prev_scp",j()),n("cust_params",k(a,!0)),d=document.createElement("script"),g=document.getElementsByTagName("script")[0],d.async=!0,d.type="text/javascript",d.src="http://pubads.g.doubleclick.net/gampad/ads?"+m.join("&"),d.id="tinygpt",d.onload=d.onerror=d.onreadystatechange=l,f=!0,g.parentNode.insertBefore(d,g),b=+new Date}}}()},{}]},{},[1]);</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[300,600],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['f'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['1'],
'u': ['882104874321'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-tarnhelm-4261578659._CB296130991_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"9cf3740d197f14a1c551ac4546b27d254af299fb",
"2016-04-18T17%3A03%3A30GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 50190;
generic.days_to_midnight = 0.5809027552604675;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-3230827532._CB299577821_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'f']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm == null || window.generic == null || window.consoleLog == null)) {
                    if (window.console && console.log) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-3292747090._CB299577773_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=882104874321;ord=882104874321?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript> <a href="http://pubads.g.doubleclick.net/gampad/jump?&iu=4215/imdb2.consumer.homepage/&sz=1008x150|1008x200|1008x30|970x250|9x1&t=p%3Dtop%26p%3Dt%26fv%3D1%26ab%3Df%26bpx%3D1%26c%3D0%26s%3D3075%26s%3D32&tile=0&c=882104874321" target="_blank"> <img src="http://pubads.g.doubleclick.net/gampad/ad?&iu=4215/imdb2.consumer.homepage/&sz=1008x150|1008x200|1008x30|970x250|9x1&t=p%3Dtop%26p%3Dt%26fv%3D1%26ab%3Df%26bpx%3D1%26c%3D0%26s%3D3075%26s%3D32&tile=0&c=882104874321" border="0" alt="advertisement" /> </a> </noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/chart/toptv/?ref_=nv_tp_tv250_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                        <li><a href="/chart/top?ref_=nv_mv_250_6"
>Top Rated Movies</a></li>
                        <li><a href="/chart/moviemeter?ref_=nv_mv_mpm_7"
>Most Popular Movies</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/chart/?ref_=nv_ch_cht_1"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_2"
>Oscar Winners</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_3"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/imdbpicks/tv-releases/?ref_=nv_tvv_picks_1"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_2"
>On Tonight</a></li>
                        <li><a href="/chart/toptv/?ref_=nv_tvv_250_3"
>Top Rated TV Shows</a></li>
                        <li><a href="/chart/tvmeter?ref_=nv_tvv_mptv_4"
>Most Popular TV Shows</a></li>
                        <li><a href="/feature/watch-now-on-amazon/?ref_=nv_tvv_wn_5"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_6"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_2"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/imdbpicks/?ref_=nv_sf_picks_3"
>IMDb Picks</a></li>
                        <li><a href="/best-of/?ref_=nv_sf_bstof_4"
>Best of 2015</a></li>
                        <li><a href="/scary-good/?ref_=nv_sf_sg_5"
>Scary Good</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=04-18&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_3"
>Sundance</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_6"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_7"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_8"
>Emmy Awards</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_9"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_10"
>Toronto Film Festival</a></li>
                        <li><a href="/festival-central/?ref_=nv_ev_fc_11"
>Festival Central</a></li>
                        <li><a href="/festival-central/tribeca?ref_=nv_ev_tff_12"
>Tribeca</a></li>
                        <li><a href="/event/all/?ref_=nv_ev_all_13"
>All Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni59730094/?ref_=nv_nw_tn_1"
> 'Jungle Book' Opens with Massive $103 Million, Global Cume Tops $290M
</a><br />
                        <span class="time">17 April 2016 4:37 PM, UTC</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59731962/?ref_=nv_nw_tn_2"
> Cannes: Criticsâ Week Announces 2016 Lineup
</a><br />
                        <span class="time">6 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59732288/?ref_=nv_nw_tn_3"
> CinemaCon: The Buzz, the Highs and the (Surprise) Biggest Loser
</a><br />
                        <span class="time">4 hours ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_3"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5>
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB276458993_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB276458997_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB276458995_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0169547/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTU4MzEwODQzOF5BMl5BanBnXkFtZTcwMTIyODY3Mw@@._V1._SY315_CR0,0,410,315_CT50_.jpg",
            titleYears : "1999",
            rank : 63,
                    headline : "American Beauty"
    },
    nameAd : {
            clickThru : "/name/nm0350453/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMjAwMDQ0MjQzMF5BMl5BanBnXkFtZTcwNDczNjQ4NA@@._V1._SX250_CR0,0,250,315_.jpg",
            rank : 112,
            headline : "Jake Gyllenhaal"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB276459002_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYmE9t7QZGs_CgKC47VLo42CSaettKdfGePLXR84PYIiqElZ-WCEsiNsdSQjX3uqQkJ3t3Ih246%0D%0AVSOGZjVxh_R_0FLuox4SDQTrZO_1rPLRTG_ZsqKBfy7CxoKYAhU12LwV8Qh_hfqBmCC8pTkAAgNQ%0D%0AfRGkHtmUaUfhbrGGN4U-3GNh9qxdSU54ZV3VaWi1WZJqTRf2pV_6R0wdWs3jLEFMvw%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYlUD9SkzYbUnObBUlu686GNUYokz5VPXg056VNci5oEKyKurxpd1_638OkgWnKyfzO898aAHdm%0D%0AxYOTmgFJLtV9iYiw-mjKBrI1Gn4D1hkVry0tKibBT750O9-W2D3GvcyALecso3V7sw4kwahKoWKY%0D%0AB6dG53oXdh-UvXN6_QyJMF0ddpkf7DLXhloryhW0LfqW%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYuXURatT0Oe0vocb5W4YVGdT-7FwqN4_oY-AxnD2Z6huhMjd-dj6sRHP68LQsyJmQ86-KmEa2F%0D%0AT0MnsrgYNJXQ0f35PhrxIZQxrEEivFS1tMStuC8ZfR_QsaBVO7PcYMs3FH7GZmzSbgCmaqZ_HgBp%0D%0AANfuLTfEnwY6rB92QJDjZud8H3-nHdEnW7oLVfz9FJdH%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=882104874321;ord=882104874321?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>

    <div id="pagecontent">
    <div class="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=882104874321;ord=882104874321?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	

    
    
    

    
    
    

    
    
    
    </div>
    <div id="top-slot-wrapper" class="pagecontent">

    
    
    

    
    
    
    </div>
    <div class="pagecontent">
        <div id="content-2-wide">
            <div id="main">

    
    
        <a name="slot_hero"></a>
        <div class="heroWidget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','HeroWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_hero">
<div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi4172854297?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471979922&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_1" data-video="vi4172854297" data-source="bylist" data-id="ls002922459" data-rid="12XARZSAAFGZ0P7VQVTB" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="Nia and Silas are two people who work together in a futuristic society known as The Collective, which has ended crime and violence by genetically eliminating all human emotions. Despite this, Nia and Silas can't help noticing a growing attraction between them, leading them to a forbidden relationship -- at first tentative, but then exploding into a passionate romance. As suspicion begins to mount among their superiors, the couple will be forced to choose between going back to the safety of the lives they have always known, or risk it all to try and pull off a daring escape." alt="Nia and Silas are two people who work together in a futuristic society known as The Collective, which has ended crime and violence by genetically eliminating all human emotions. Despite this, Nia and Silas can't help noticing a growing attraction between them, leading them to a forbidden relationship -- at first tentative, but then exploding into a passionate romance. As suspicion begins to mount among their superiors, the couple will be forced to choose between going back to the safety of the lives they have always known, or risk it all to try and pull off a daring escape." src="http://ia.media-imdb.com/images/M/MV5BMTg3NTQ5MDU3OF5BMl5BanBnXkFtZTgwODc2Mzk5NzE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg3NTQ5MDU3OF5BMl5BanBnXkFtZTgwODc2Mzk5NzE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Nia and Silas are two people who work together in a futuristic society known as The Collective, which has ended crime and violence by genetically eliminating all human emotions. Despite this, Nia and Silas can't help noticing a growing attraction between them, leading them to a forbidden relationship -- at first tentative, but then exploding into a passionate romance. As suspicion begins to mount among their superiors, the couple will be forced to choose between going back to the safety of the lives they have always known, or risk it all to try and pull off a daring escape." title="Nia and Silas are two people who work together in a futuristic society known as The Collective, which has ended crime and violence by genetically eliminating all human emotions. Despite this, Nia and Silas can't help noticing a growing attraction between them, leading them to a forbidden relationship -- at first tentative, but then exploding into a passionate romance. As suspicion begins to mount among their superiors, the couple will be forced to choose between going back to the safety of the lives they have always known, or risk it all to try and pull off a daring escape." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Nia and Silas are two people who work together in a futuristic society known as The Collective, which has ended crime and violence by genetically eliminating all human emotions. Despite this, Nia and Silas can't help noticing a growing attraction between them, leading them to a forbidden relationship -- at first tentative, but then exploding into a passionate romance. As suspicion begins to mount among their superiors, the couple will be forced to choose between going back to the safety of the lives they have always known, or risk it all to try and pull off a daring escape." title="Nia and Silas are two people who work together in a futuristic society known as The Collective, which has ended crime and violence by genetically eliminating all human emotions. Despite this, Nia and Silas can't help noticing a growing attraction between them, leading them to a forbidden relationship -- at first tentative, but then exploding into a passionate romance. As suspicion begins to mount among their superiors, the couple will be forced to choose between going back to the safety of the lives they have always known, or risk it all to try and pull off a daring escape." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt3289728/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471979922&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_1" > Equals </a> </div> </div> <div class="secondary ellipsis"> New Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi374256921?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471979922&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_2" data-video="vi374256921" data-source="bylist" data-id="ls002653141" data-rid="12XARZSAAFGZ0P7VQVTB" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="Japan is plunged into chaos upon the appearance of a giant monster." alt="Japan is plunged into chaos upon the appearance of a giant monster." src="http://ia.media-imdb.com/images/M/MV5BZmYxZGZjZDgtNjFhYi00MWY3LTk0ZTYtZDNmODJjNWQ1OTM4XkEyXkFqcGdeQXVyNDQ2OTk4MzI@._V1_SY298_CR5,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BZmYxZGZjZDgtNjFhYi00MWY3LTk0ZTYtZDNmODJjNWQ1OTM4XkEyXkFqcGdeQXVyNDQ2OTk4MzI@._V1_SY298_CR5,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Japan is plunged into chaos upon the appearance of a giant monster." title="Japan is plunged into chaos upon the appearance of a giant monster." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Japan is plunged into chaos upon the appearance of a giant monster." title="Japan is plunged into chaos upon the appearance of a giant monster." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt4262980/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471979922&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_2" > Godzilla Resurgence </a> </div> </div> <div class="secondary ellipsis"> Japanese Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3830297881?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471979922&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_3" data-video="vi3830297881" data-source="bylist" data-id="ls002309697" data-rid="12XARZSAAFGZ0P7VQVTB" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="After his career is destroyed, a brilliant but arrogant and conceited surgeon gets a new lease on life when a sorcerer takes him under his wing and trains him to defend the world against evil." alt="After his career is destroyed, a brilliant but arrogant and conceited surgeon gets a new lease on life when a sorcerer takes him under his wing and trains him to defend the world against evil." src="http://ia.media-imdb.com/images/M/MV5BMTA3OTYyODM2NjJeQTJeQWpwZ15BbWU4MDM0NzYwNjgx._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTA3OTYyODM2NjJeQTJeQWpwZ15BbWU4MDM0NzYwNjgx._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="After his career is destroyed, a brilliant but arrogant and conceited surgeon gets a new lease on life when a sorcerer takes him under his wing and trains him to defend the world against evil." title="After his career is destroyed, a brilliant but arrogant and conceited surgeon gets a new lease on life when a sorcerer takes him under his wing and trains him to defend the world against evil." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="After his career is destroyed, a brilliant but arrogant and conceited surgeon gets a new lease on life when a sorcerer takes him under his wing and trains him to defend the world against evil." title="After his career is destroyed, a brilliant but arrogant and conceited surgeon gets a new lease on life when a sorcerer takes him under his wing and trains him to defend the world against evil." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt1211837/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471979922&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_3" > Doctor Strange </a> </div> </div> <div class="secondary ellipsis"> World Premiere Trailer </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/trailers?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471979922&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_sm" class="position_bottom supplemental" >Browse more trailers</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','HeroWidget',{wb:1});}
                if(typeof uet === 'function'){uet("cf");}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-1"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/imdbpicks/top-10-game-of-thrones-episodes/ls033176359?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471269082&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_got_hd" > <h3>Ranking the Top 10 "Game of Thrones" Episodes</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/top-10-game-of-thrones-episodes/ls033176359?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471269082&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_got_i_1#image3" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTQ3MzEzMjA1Ml5BMl5BanBnXkFtZTcwMTcyMTEwOQ@@._UX1600_CR480,320,614,460_SY230_SX307_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ3MzEzMjA1Ml5BMl5BanBnXkFtZTcwMTcyMTEwOQ@@._UX1600_CR480,320,614,460_SY230_SX307_AL_UY460_UX614_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/top-10-game-of-thrones-episodes/ls033176359?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471269082&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_got_i_2#image8" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjExNzA3Mjk1Ml5BMl5BanBnXkFtZTgwNTI1MTA2MTE@._SX800_CR180,0,614,460_SY230_SX307_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjExNzA3Mjk1Ml5BMl5BanBnXkFtZTgwNTI1MTA2MTE@._SX800_CR180,0,614,460_SY230_SX307_AL_UY460_UX614_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Ready for the return of "<a href="/title/tt0944947/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471269082&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_got_lk1">Game of Thrones</a>" this Sunday? Check out the series' best-rated episodes ever, as judged by IMDb users. If you disagree, make sure to rate your favorites!</p> <p class="seemore"><a href="/imdbpicks/top-10-game-of-thrones-episodes/ls033176359?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471269082&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_got_sm" class="position_bottom supplemental" >View the complete list</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
                if(typeof uet === 'function'){uet("af");}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-2"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/scary-good/containment-things-to-know/ls032224675?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471276302&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_scg_con_hd" > <h3>10 Things to Know About The CW's "Containment"</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:624px;height:auto;" > <div style="width:624px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/scary-good/containment-things-to-know/ls032224675?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471276302&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_scg_con_i_1" > <img itemprop="image" class="pri_image" title="Containment (2016)" alt="Containment (2016)" src="http://ia.media-imdb.com/images/M/MV5BMTAzODE1NjEwNzJeQTJeQWpwZ15BbWU4MDYyMTIwNzUx._V1_SY351_SX624_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTAzODE1NjEwNzJeQTJeQWpwZ15BbWU4MDYyMTIwNzUx._V1_SY351_SX624_AL_UY702_UX1248_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Learn more about <a href="/title/tt4421578/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471276302&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_scg_con_lk1">The CW's new disease drama</a>, where a mysterious and highly contagious virus sweeps Atlanta, home of the Center for Disease Control and Prevention (CDC).</p> <p class="seemore"><a href="/scary-good/containment-things-to-know/ls032224675?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471276302&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_scg_con_sm" class="position_bottom supplemental" >Learn more about "Containment"</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-3"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NewsDeskWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59730094?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTUyOTgwNDk3OV5BMl5BanBnXkFtZTgwMzU0NjU5NzE@._V1_SY150_CR78,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59730094?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1"
class="headlines" >'Jungle Book' Opens with Massive $103 Million, Global Cume Tops $290M</a>
    <div class="infobar">
            <span class="text-muted">17 April 2016 5:37 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000159?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_src"
>Box Office Mojo</a></span>
    </div>
                                </div>
<p>Disney's <a href="/title/tt3040964?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk1">The Jungle Book</a> delivers the third $100+ million opener of 2016, two more than 2015 had at this point in the year and <a href="/title/tt3498820?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk2">Captain America: Civil War</a> is just around the corner. The weekend's two other new wide releases saw WB and MGM's <a href="/title/tt3628584?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk3">Barbershop: The Next Cut</a> finish second while Lionsgate's ...                                        <span class="nobr"><a href="/news/ni59730094?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59731962?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2"
class="headlines" >Cannes: Criticsâ Week Announces 2016 Lineup</a>
    <div class="infobar">
            <span class="text-muted">6 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59732288?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3"
class="headlines" >CinemaCon: The Buzz, the Highs and the (Surprise) Biggest Loser</a>
    <div class="infobar">
            <span class="text-muted">4 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000001?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3_src"
>The Hollywood Reporter</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59732969?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4"
class="headlines" >Naomi Watts To Lead Sam Taylor-Johnson's Psychological Thriller Series âGypsyâ For Netflix</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0011867?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4_src"
>The Playlist</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59731303?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5"
class="headlines" >Amazon takes aim at Netflix</a>
    <div class="infobar">
            <span class="text-muted">11 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0056136?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5_src"
>ScreenDaily</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59732584?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjIyMzMyMDY3OV5BMl5BanBnXkFtZTcwNzQ3NzIzOQ@@._V1_SY150_CR2,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59732584?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1"
class="headlines" >Jon Chu Returning to Direct âNow You See Me 3â (Exclusive)</a>
    <div class="infobar">
            <span class="text-muted">2 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_src"
>Variety - Film News</a></span>
    </div>
                                </div>
<p> <a href="/name/nm0160840?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk1">Jon M. Chu</a> is returning to the directorâs chair for <a href="/company/co0173285?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk2">Lionsgate</a>âs â<a href="/title/tt4712810?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk3">Now You See Me 3</a>,â Variety has learned exclusively. The studio struck a dealÂ with Chu nearly two months before it releases his â<a href="/title/tt3110958?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk4">Now You See Me 2</a>â on June 10 â a signal that it believes the sequel will perform in the same range as the...                                        <span class="nobr"><a href="/news/ni59732584?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59730094?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2"
class="headlines" >'Jungle Book' Opens with Massive $103 Million, Global Cume Tops $290M</a>
    <div class="infobar">
            <span class="text-muted">17 April 2016 5:37 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000159?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2_src"
>Box Office Mojo</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59732084?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3"
class="headlines" >Max Landis: there are âno A-list female Asian celebritiesâ who could have taken Scarlett Johanssonâs Ghost in a Shell role</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000680?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3_src"
>The Guardian - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59731393?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4"
class="headlines" >Fox's New Mutants Movie Just Took Another Huge Step Forward</a>
    <div class="infobar">
            <span class="text-muted">11 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000098?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4_src"
>cinemablend.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59731131?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5"
class="headlines" >Giuseppe Tornatore, David Twohy line up China projects</a>
    <div class="infobar">
            <span class="text-muted">12 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0056136?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5_src"
>ScreenDaily</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59732969?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjIzMjY1NTA4OF5BMl5BanBnXkFtZTcwNjk3MDYwOQ@@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59732969?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1"
class="headlines" >Naomi Watts To Lead Sam Taylor-Johnson's Psychological Thriller Series âGypsyâ For Netflix</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0011867?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_src"
>The Playlist</a></span>
    </div>
                                </div>
<p><a href="/name/nm0915208?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk1">Naomi Watts</a> has been dipping her toes in different waters lately, going blockbuster with "<a href="/title/tt3410834?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk2">Allegiant</a>" and indie with fare like "The Sea Of Trees." Now she's going to give a Netflix series a try. The actress will star in "<a href="/title/tt2322441?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk3">Fifty Shades Of Grey</a>" director <a href="/name/nm0853374?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk4">Sam Taylor-Johnson</a>'s previously announced ...                                        <span class="nobr"><a href="/news/ni59732969?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59730957?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2"
class="headlines" >âThe Good Wifeâ Showrunners Donât Deny Josh Charles Return Rumors</a>
    <div class="infobar">
            <span class="text-muted">16 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59731100?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3"
class="headlines" >Akiva Goldsman on âUndergroundâ: America Should Understand That âOur Heroes Are Not All White Menâ</a>
    <div class="infobar">
            <span class="text-muted">14 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59732184?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4"
class="headlines" >âTwilight Zoneâ Interactive Reboot in the Works (Exclusive)</a>
    <div class="infobar">
            <span class="text-muted">4 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4_src"
>The Wrap</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59732177?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5"
class="headlines" >âNarcosâ Director JosÃ© Padilha Reteams with Netflix</a>
    <div class="infobar">
            <span class="text-muted">4 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59732170?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTU3MDkwNzI5Nl5BMl5BanBnXkFtZTgwODMzOTE3NzE@._V1_SY150_CR19,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59732170?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1"
class="headlines" >Amber Heard Pleads Guilty in Dog-Smuggling Case and Releases Apology Video with Husband Johnny Depp</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_src"
>PEOPLE.com</a></span>
    </div>
                                </div>
<p><a href="/name/nm1720028?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk1">Amber Heard</a> pleaded guilty on Monday to providing a false immigration document when entering Australia last May with her and husband <a href="/name/nm0000136?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk2">Johnny Depp</a>'s dogs - and the couple issued a video apology. The 29-year-old actress avoided jail time in the case, with prosecutors dropping charges that Heard ...                                        <span class="nobr"><a href="/news/ni59732170?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59729850?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2"
class="headlines" >Megan Fox and Brian Austin Green Look Happy and Relaxed Following Pregnancy News</a>
    <div class="infobar">
            <span class="text-muted">14 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000088?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2_src"
>Popsugar.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59732434?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3"
class="headlines" >James Franco Does Not Regret Co-Hosting the Oscars With Anne Hathaway (Although They "Got a Lot of S--- for It")</a>
    <div class="infobar">
            <span class="text-muted">3 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3_src"
>E! Online</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59732114?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4"
class="headlines" >Suri Cruise Turns 10: How the World of Celebrity-Kid Worship & Scrutiny Has Changed Since Katie Holmes & Tom Cruise's Daughter Was Born</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59732996?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5"
class="headlines" >Kardashians Share Adorable Throwback Photos of Kourtney on Her Birthday</a>
    <div class="infobar">
            <span class="text-muted">58 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5_src"
>E! Online</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NewsDeskWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-4"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/imdbpicks/photos-we-love-0415/rg1455659776?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471838262&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_hd" > <h3>IMDb Snapshot: This Week's Photos We Love</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/photos-we-love-0415/rg1455659776?imageid=rm1825316096&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471838262&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_1" > <img itemprop="image" class="pri_image" title="Amy Poehler and Chris Pratt" alt="Amy Poehler and Chris Pratt" src="http://ia.media-imdb.com/images/M/MV5BMTA4MjIxODA1MTBeQTJeQWpwZ15BbWU4MDQ1OTgzNjgx._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTA4MjIxODA1MTBeQTJeQWpwZ15BbWU4MDQ1OTgzNjgx._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/photos-we-love-0415/rg1455659776?imageid=rm1858870528&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471838262&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_2" > <img itemprop="image" class="pri_image" title="Alexander SkarsgÃ¥rd" alt="Alexander SkarsgÃ¥rd" src="http://ia.media-imdb.com/images/M/MV5BMTY1MjQxNjEzOF5BMl5BanBnXkFtZTgwMTU5ODM2ODE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY1MjQxNjEzOF5BMl5BanBnXkFtZTgwMTU5ODM2ODE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/photos-we-love-0415/rg1455659776?imageid=rm1875647744&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471838262&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_3" > <img itemprop="image" class="pri_image" title="Hugh Laurie" alt="Hugh Laurie" src="http://ia.media-imdb.com/images/M/MV5BMjI5ODIzNzEyMV5BMl5BanBnXkFtZTgwMDU5ODM2ODE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI5ODIzNzEyMV5BMl5BanBnXkFtZTgwMDU5ODM2ODE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/imdbpicks/photos-we-love-0415/rg1455659776?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471838262&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_sm" class="position_bottom supplemental" >View the photos</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-5"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','BornTodayWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_borntoday">
<span class="widget_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=4-18&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_hd" > <h3>Born Today</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1429380?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_1" > <img itemprop="image" class="pri_image" title="Britt Robertson" alt="Britt Robertson" src="http://ia.media-imdb.com/images/M/MV5BMzM3NjAyNTQxOF5BMl5BanBnXkFtZTgwMDU2NDQ1ODE@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzM3NjAyNTQxOF5BMl5BanBnXkFtZTgwMDU2NDQ1ODE@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1429380?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_1">Britt Robertson</a> (26) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0790057?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_2" > <img itemprop="image" class="pri_image" title="Alia Shawkat" alt="Alia Shawkat" src="http://ia.media-imdb.com/images/M/MV5BMTUxOTQ3NjY3MV5BMl5BanBnXkFtZTcwNzY1MzY1MQ@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUxOTQ3NjY3MV5BMl5BanBnXkFtZTcwNzY1MzY1MQ@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0790057?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_2">Alia Shawkat</a> (27) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0855039?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_3" > <img itemprop="image" class="pri_image" title="David Tennant" alt="David Tennant" src="http://ia.media-imdb.com/images/M/MV5BMTQxODM2MjUxNV5BMl5BanBnXkFtZTgwMjA4OTY0MTE@._V1_SY172_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQxODM2MjUxNV5BMl5BanBnXkFtZTgwMjA4OTY0MTE@._V1_SY172_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0855039?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_3">David Tennant</a> (45) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm4032297?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_4" > <img itemprop="image" class="pri_image" title="Chloe Bennet" alt="Chloe Bennet" src="http://ia.media-imdb.com/images/M/MV5BMjEzNDU5NTIxM15BMl5BanBnXkFtZTcwODUwOTk4OQ@@._V1_SY172_CR5,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjEzNDU5NTIxM15BMl5BanBnXkFtZTcwODUwOTk4OQ@@._V1_SY172_CR5,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm4032297?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_4">Chloe Bennet</a> (24) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0004997?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_5" > <img itemprop="image" class="pri_image" title="Melissa Joan Hart" alt="Melissa Joan Hart" src="http://ia.media-imdb.com/images/M/MV5BMTYyNTU1MjU2Ml5BMl5BanBnXkFtZTcwODg2MzI2MQ@@._V1_SY172_CR1,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTYyNTU1MjU2Ml5BMl5BanBnXkFtZTcwODg2MzI2MQ@@._V1_SY172_CR1,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0004997?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_5">Melissa Joan Hart</a> (40) </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/search/name?refine=birth_monthday&birth_monthday=4-18&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_sm" class="position_bottom supplemental" >See all birthdays</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','BornTodayWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-7"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/gallery/rg2076351232?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471253802&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_sib_hd" > <h3>Star Siblings: Famous Brothers and Sisters</h3> </a> </span> </span> <p class="blurb">From the Baldwin brothers to the Olsen sisters, take a look at some famous families.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3876750592/rg2076351232?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471253802&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_sib_i_1" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTQxMDU4ODE5NF5BMl5BanBnXkFtZTcwMDYyMzE0NA@@._UX402_CR0,56,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQxMDU4ODE5NF5BMl5BanBnXkFtZTcwMDYyMzE0NA@@._UX402_CR0,56,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3239752448/rg2076351232?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471253802&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_sib_i_2" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTQwNTk0ODI1NV5BMl5BanBnXkFtZTcwNTk4OTUxNw@@._UX402_CR0,0,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQwNTk0ODI1NV5BMl5BanBnXkFtZTcwNTk4OTUxNw@@._UX402_CR0,0,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm2515598336/rg2076351232?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471253802&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_sib_i_3" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BNDE3NzM4MjYxNF5BMl5BanBnXkFtZTgwNzI5NTIwNDE@._UX402_CR0,0,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNDE3NzM4MjYxNF5BMl5BanBnXkFtZTgwNzI5NTIwNDE@._UX402_CR0,0,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/gallery/rg2076351232?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471253802&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_sib_sm" class="position_bottom supplemental" >See the full gallery</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-8"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>Indie Focus: Celebrate 'Holidays'</h3> </span> </span> <p class="blurb">This horror anthology from directors including <a href="/name/nm0003620?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471439762&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_lk1">Kevin Smith</a> presents a twisted, subversive take on a year's worth of holidays. The exclusive featured clip is from <i>Father's Day</i>, directed by <a href="/name/nm2227980/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471439762&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_lk2">Anthony Scott Burns</a>.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt4419364/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471439762&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_1" > <img itemprop="image" class="pri_image" title="Holidays (2016)" alt="Holidays (2016)" src="http://ia.media-imdb.com/images/M/MV5BNTA5MDgxMzYwN15BMl5BanBnXkFtZTgwNzI4MDA0ODE@._V1_SX170_CR0,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNTA5MDgxMzYwN15BMl5BanBnXkFtZTgwNzI4MDA0ODE@._V1_SX170_CR0,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3082007577?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471439762&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_2" data-video="vi3082007577" data-rid="12XARZSAAFGZ0P7VQVTB" data-type="single" class="video-colorbox" data-refsuffix="hm_if" data-ref="hm_if_i_2"> <img itemprop="image" class="pri_image" title="Clip from the anthology horror film 'Holidays'" alt="Clip from the anthology horror film 'Holidays'" src="http://ia.media-imdb.com/images/M/MV5BMzk5MzMyMTM5Nl5BMl5BanBnXkFtZTgwOTcyMzMzODE@._V1_SX444_CR0,0,444,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzk5MzMyMTM5Nl5BMl5BanBnXkFtZTgwOTcyMzMzODE@._V1_SX444_CR0,0,444,250_AL_UY500_UX888_AL_.jpg" /> <img alt="Clip from the anthology horror film 'Holidays'" title="Clip from the anthology horror film 'Holidays'" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Clip from the anthology horror film 'Holidays'" title="Clip from the anthology horror film 'Holidays'" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
        <a name="slot_center-11"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/feature/watch-now-on-amazon/?tag=imdb-aiv-tv-20&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471323142&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_aiv_hd" > <h3>Now Trending on Amazon Video</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471323142&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYq-CzAU3BxWL-AL-M7v16ZOLUhEG6qdumL432l8SZwx0ZK7txE5qjCaG4n-uEWdrAptPmnqrc2%0D%0AHd7XqES5dNFLnpmQxGESG8JG3XPdXRpaMyTejzezNsI-FRpufVXMimWkvnG0yqeb1cWRYg0tlRYi%0D%0AjHe4moYWfazzGxOzr2AoCIyLkxG44Uw2s-gyo3ceMfSE13jIFfI9Aef9ZBrB1qYgA7gyEy23pofU%0D%0AI58nMB4CW0o7ypW9b_El0FQ3699Kk99KAFYzPF99qZwCLfigvC0JWA%0D%0A&ref_=hm_aiv_i_1" > <img itemprop="image" class="pri_image" title="Harrison Ford, Anthony Daniels, Carrie Fisher, Peter Mayhew, Oscar Isaac, Lupita Nyong'o, Adam Driver, Gwendoline Christie, John Boyega and Daisy Ridley in Star Wars: Episode VII - The Force Awakens (2015)" alt="Harrison Ford, Anthony Daniels, Carrie Fisher, Peter Mayhew, Oscar Isaac, Lupita Nyong'o, Adam Driver, Gwendoline Christie, John Boyega and Daisy Ridley in Star Wars: Episode VII - The Force Awakens (2015)" src="http://ia.media-imdb.com/images/M/MV5BOTAzODEzNDAzMl5BMl5BanBnXkFtZTgwMDU1MTgzNzE@._V1_SY172_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BOTAzODEzNDAzMl5BMl5BanBnXkFtZTgwMDU1MTgzNzE@._V1_SY172_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> <img alt="Harrison Ford, Anthony Daniels, Carrie Fisher, Peter Mayhew, Oscar Isaac, Lupita Nyong'o, Adam Driver, Gwendoline Christie, John Boyega and Daisy Ridley in Star Wars: Episode VII - The Force Awakens (2015)" title="Harrison Ford, Anthony Daniels, Carrie Fisher, Peter Mayhew, Oscar Isaac, Lupita Nyong'o, Adam Driver, Gwendoline Christie, John Boyega and Daisy Ridley in Star Wars: Episode VII - The Force Awakens (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" /> <img alt="Harrison Ford, Anthony Daniels, Carrie Fisher, Peter Mayhew, Oscar Isaac, Lupita Nyong'o, Adam Driver, Gwendoline Christie, John Boyega and Daisy Ridley in Star Wars: Episode VII - The Force Awakens (2015)" title="Harrison Ford, Anthony Daniels, Carrie Fisher, Peter Mayhew, Oscar Isaac, Lupita Nyong'o, Adam Driver, Gwendoline Christie, John Boyega and Daisy Ridley in Star Wars: Episode VII - The Force Awakens (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471323142&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYk8Bhn0HMnL0z2qzTLHvGBOw6yeOdl5UV9Uc_qfdM-K38aL3MPB6GTp5L7KirVRqo3HWGP8nA2%0D%0Aq4BWPsjyvnP3wTfSidtL2aSPceOGclQznfLl559jc_2kCNTrx77SfZmCJQ8B1PNThQtqD8H_pwHF%0D%0Ae6RbylkLraRch9Ott1nrTynmlClgQNzmpz1aYoOqxlmOFsNmX3p9yn3vBkThGKRNgpBQ7jb6Xb_Z%0D%0AJEQiH9taqR4_IhE7Sc69DlY8LaZp_3E1COHmNeqSMpjAwoJ42Yzn1w%0D%0A&ref_=hm_aiv_cap_pri_1" > <i>Star Wars: The Force Awakens</i> </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471323142&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYjp3DYMb60Y7Kiq6-RW5Nns6BFvEfTzuLFrgZ4NLDUWKY9lRqm7tW_wAOGaKqJKZ05RLR_h3gs%0D%0ADq9loNOkGK5BOxHGFxkLNEE02glyjNiIjJ4Jy1Jj3WVDyOKLG3uzgT_GJxPM8UeGKQOWayQo0Am3%0D%0ApEuugZXQBXLlAVXcsoF2UTb-n0V1yTW4HxC_Z4KpsiH9aXkBe3htzkhCJBH5c5SdmnKD3ANqE5d8%0D%0A1vQC240X0izgP74MNvk1HPXIxthNzravLCN9K1uY9JJukhkOh7mY5Q%0D%0A&ref_=hm_aiv_i_2" > <img itemprop="image" class="pri_image" title="The Walking Dead (2010)" alt="The Walking Dead (2010)" src="http://ia.media-imdb.com/images/M/MV5BMTc5NTU3Njg0N15BMl5BanBnXkFtZTgwMzY4MjM0ODE@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc5NTU3Njg0N15BMl5BanBnXkFtZTgwMzY4MjM0ODE@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> <img alt="The Walking Dead (2010)" title="The Walking Dead (2010)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" /> <img alt="The Walking Dead (2010)" title="The Walking Dead (2010)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471323142&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYl35m_eqTXRcWzkflGZC1MtNw_u05K7PfLIdaHP_AF4qNeWd5Ubz4pE0y7mb1lZe7-hGQ5pXXW%0D%0ALEMJr8IOYCbt0sN9ae0fBPtZxKknrsSXmCebrVzSOETYWUGUUbtgknhOkbZrmPh3YgtgwBVJ9oIb%0D%0ADXvo0rZbcDK9DTJ7caZqhD1aeskzJFJSVIjDQ3rX7SgSBdz5ksHrNNsu7QNBVRDkcT7iSBWFGrA8%0D%0AuPPreoUqLWnVgCh2PN2o7tvn1etwq8ZdVL3PTq7kWSlVJil5pYNvWw%0D%0A&ref_=hm_aiv_cap_pri_2" > "The Walking Dead - Season 6" </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471323142&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYrq4S0OBC0zZlu8w_ikHD9KMUJzVyWkvzOrf4v7JYy1qQD4DyJXsdVmNQ_OQO6-4Z3pIcTToYZ%0D%0ACW4gaiOslbWBdk4oR0LybIHRFkGR1netwrpBXiUwLyicJyauN0q8M9V2nBUtMszl4JiXdTfs2ezw%0D%0A7mwvKKHvvwVF2aHwTSme9ekMESCL83OUrA4bdJ6GB_qteAwSd6OPeQo3rsxLSK15HJxumPQd4Uwn%0D%0AEVRqn4knD_JHo-EaykhWFGZPnUhfNPJ4PwJuLAV23W7VCFH2RLuIfA%0D%0A&ref_=hm_aiv_i_3" > <img itemprop="image" class="pri_image" title="Mark Wahlberg and Will Ferrell in Daddy's Home (2015)" alt="Mark Wahlberg and Will Ferrell in Daddy's Home (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTQ0OTE1MTk4N15BMl5BanBnXkFtZTgwMDM5OTk5NjE@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ0OTE1MTk4N15BMl5BanBnXkFtZTgwMDM5OTk5NjE@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> <img alt="Mark Wahlberg and Will Ferrell in Daddy's Home (2015)" title="Mark Wahlberg and Will Ferrell in Daddy's Home (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" /> <img alt="Mark Wahlberg and Will Ferrell in Daddy's Home (2015)" title="Mark Wahlberg and Will Ferrell in Daddy's Home (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471323142&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYlhWFQY8-qhMzgw3O1Ao2RFWJU2PJacGg4dbHJIn1c85-oOANsv4X_vO0QTGfuDp1NtwG5bpLj%0D%0A8kPusDk0qEJb3U4A77V8kJv7YWcIXm9EyuXplfYBlsR8ZTwFiAkIRxbR6Gf8VLMFYakVWPzKx1jm%0D%0Ay43R74Ff2OqFqFUH7rOwHTZ2lCFf8btStlgbl6jAFrFsNK7izJksmhYKYD4g06FyGLNKHWNea3H7%0D%0AomXU3qms7vlt9ZmT-wUpowDbR4s_IX1q1tW7k4rq4n2V82W_ZFaq6g%0D%0A&ref_=hm_aiv_cap_pri_3" > <i>Daddy's Home</i> </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471323142&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYh2bTN-RErZBTGbQVqCrk2ycV9MEkKPLy9okjQsILywVnuXDCRUAW1VjzG441qd4UIuHvpGXJ7%0D%0A4pMM1btneLWkU2EwFnB0U93SBL0i7g0U3COQGIoosgpSPWf1HdIX3_Fg3ggaHoHTvsL0dBghrgyk%0D%0An2pArbeCkA3NdYycDsEbEQ3kTar9EsNByHejateBf-sQapSIAIdRmy6A-k9AvmdnsgoQvHRujz_s%0D%0A9LuxDBegt-1u2rRE5R8g6mwPZwtwfs8n%0D%0A&ref_=hm_aiv_i_4" > <img itemprop="image" class="pri_image" title="Vikings (2013)" alt="Vikings (2013)" src="http://ia.media-imdb.com/images/M/MV5BOTEzNzI3MDc0N15BMl5BanBnXkFtZTgwMzk1MzA5NzE@._V1_SY172_CR8,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BOTEzNzI3MDc0N15BMl5BanBnXkFtZTgwMzk1MzA5NzE@._V1_SY172_CR8,0,116,172_AL_UY344_UX232_AL_.jpg" /> <img alt="Vikings (2013)" title="Vikings (2013)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" /> <img alt="Vikings (2013)" title="Vikings (2013)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471323142&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYv81INNWOK5DTTU3j0QveAIE6_NhP6EOdnO1-abEKzZepqbfs5l-XmKIUGuexIR-4B05QKyNK4%0D%0ANGMuZ9QHZhwK4bh6tTvMTh-NURre8mEUpVw77RFwePlAbDCKys-M6YZ4SodrsCWMp2UDJjlu5-sT%0D%0Aqf-cJvF6686Ov6CPc4xArwC_t1hfiD-m8qyIhucPsoNC2b4l3wDvhFju2byF4-uS97JIK7EtclA2%0D%0A--y3MjzdajSBwBzrdKVGSpG4O5nl3oYY0Mo2sTwEhv_4zcz6PTQnCw%0D%0A&ref_=hm_aiv_cap_pri_4" > "The Vikings - Season 4" </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471323142&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYhsRCTpJOoCTK25Nvu0gH3H9yAectrqIVffGURLOlS4fx_hMt4GYLGkpcMiwO1dmYpWyAhp6YA%0D%0AbSVqHoorV7ZpUVf_c6Ubiqi2Yj4y_SKq60kdL58xjg_ErWZu7nB1RAz1unsweqvz4qfd1UoHgI5q%0D%0AUFRyhhiwfsP5fbcO2wugNlx9pmAAtHFg6HXywAx2l-WkRjotfec66prP-uZNoD1w52pf_hVQ35y9%0D%0A5K3d2KAb5QKrwzxTtl5geBPLyUjbgljxSEdZr24fzylt_dXi36LLOg%0D%0A&ref_=hm_aiv_i_5" > <img itemprop="image" class="pri_image" title="Fear the Walking Dead (2015)" alt="Fear the Walking Dead (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTU0NzUxNTA0Ml5BMl5BanBnXkFtZTgwMzQxMTc1ODE@._V1_SY172_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU0NzUxNTA0Ml5BMl5BanBnXkFtZTgwMzQxMTc1ODE@._V1_SY172_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> <img alt="Fear the Walking Dead (2015)" title="Fear the Walking Dead (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" /> <img alt="Fear the Walking Dead (2015)" title="Fear the Walking Dead (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471323142&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYt4U03G0Io2dqHmpmCLqqEhBMg1xIL3MTCbol1RfUKtPHsyqNvACOt1KTjpufN09GplHFTrH50%0D%0AR92YYD3QV_VrjIw4LIbh_b5DVIC4qDdpjlsgE9_U2LKf5giBhQAfrMy3v6jfpfoiN9VwuAlQZyYT%0D%0AgF53MjfFW_HjlarGis58_TEJBBoyZFY-bbyeld3tYwwoZ5XHih7DoMc0KQZw4U69p8706udDOm1g%0D%0A5TSBIQOjSu-9M4TD22sPSHFDwgq-_qbNxZWVPpL6nVmQiRjkinRimg%0D%0A&ref_=hm_aiv_cap_pri_5" > "Fear the Walking Dead" </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">"The Walking Dead - Season 6" might be over, but it's still trending near the top of the charts on Amazon Video. New to the top five this week is "Fear the Walking Dead - Season 2." Those looking for comedic relief from all the zombies are flocking to the Will Ferrell/Mark Wahlberg comedy <i>Daddy's Home</i>.</p> <p class="seemore"><a href="/feature/watch-now-on-amazon/?tag=imdb-aiv-tv-20&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471323142&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_aiv_sm" class="position_bottom supplemental" >See which other movies and TV shows are trending on Amazon Video</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-13"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','RecsWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','RecsWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-19"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','TriviaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_trivia">
<span class="widget_header"> <span class="oneline"> <a href="/title/tt2381941/trivia?item=tr1791635&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_hd" > <h3>Did You Know?</h3> </a> </span> </span> <div class="widget_content inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt2381941?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_i_1" > <img itemprop="image" class="pri_image" title="Fokus (2015)" alt="Fokus (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTUwODg2OTA4OF5BMl5BanBnXkFtZTgwOTE5MTE4MzE@._V1_SX89_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUwODg2OTA4OF5BMl5BanBnXkFtZTgwOTE5MTE4MzE@._V1_SX89_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> <div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="widget_inline_blurb"><strong class="text-large"><a href="/title/tt2381941?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Focus</a></strong> <p class="blurb">After <a href="/name/nm0331516?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Ryan Gosling</a> dropped out, the role was offered to <a href="/name/nm0000093?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk2">Brad Pitt</a> but he declined and <a href="/name/nm0000255?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk3">Ben Affleck</a>, who dropped out citing "scheduling conflicts."</p> <p class="seemore"><a href="/title/tt2381941/trivia?item=tr1791635&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_sm" class="position_blurb supplemental" >See more trivia</a></p> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','TriviaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-26"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','PollWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;aJWGd5UPaKc&quot;}">
        
    

    <div class="ab_poll poll">
<span class="widget_header"> <span class="oneline"> <a href="/poll/aJWGd5UPaKc/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471861042&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_hd" > <h3>Poll: So, Your Favorite Film is About to Get a Remake</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471861042&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_mp">More Polls</a></h4> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/aJWGd5UPaKc/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471861042&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_1" > <img itemprop="image" class="pri_image" title="RoboCop (2014)" alt="RoboCop (2014)" src="http://ia.media-imdb.com/images/M/MV5BMjAyOTUzMTcxN15BMl5BanBnXkFtZTgwMjkyOTc1MDE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjAyOTUzMTcxN15BMl5BanBnXkFtZTgwMjkyOTc1MDE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/aJWGd5UPaKc/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471861042&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_2" > <img itemprop="image" class="pri_image" title="Clash of the Titans (2010)" alt="Clash of the Titans (2010)" src="http://ia.media-imdb.com/images/M/MV5BMTYxNjg4MzU5OV5BMl5BanBnXkFtZTcwOTA3NTUwMw@@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTYxNjg4MzU5OV5BMl5BanBnXkFtZTcwOTA3NTUwMw@@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/aJWGd5UPaKc/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471861042&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_3" > <img itemprop="image" class="pri_image" title="Terminator Genisys (2015)" alt="Terminator Genisys (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjM1NTc0NzE4OF5BMl5BanBnXkFtZTgwNDkyNjQ1NTE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjM1NTc0NzE4OF5BMl5BanBnXkFtZTgwNDkyNjQ1NTE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/aJWGd5UPaKc/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471861042&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_4" > <img itemprop="image" class="pri_image" title="Speed 2: Cruise Control (1997)" alt="Speed 2: Cruise Control (1997)" src="http://ia.media-imdb.com/images/M/MV5BMTg5OTAzMDA4Ml5BMl5BanBnXkFtZTcwMTM1MzE1MQ@@._V1_SY207_CR2,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg5OTAzMDA4Ml5BMl5BanBnXkFtZTcwMTM1MzE1MQ@@._V1_SY207_CR2,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/aJWGd5UPaKc/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471861042&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_5" > <img itemprop="image" class="pri_image" title="The Thing (2011)" alt="The Thing (2011)" src="http://ia.media-imdb.com/images/M/MV5BMTMxMjI0MzUyNl5BMl5BanBnXkFtZTcwNjc1NzE5NQ@@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTMxMjI0MzUyNl5BMl5BanBnXkFtZTcwNjc1NzE5NQ@@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Or a sequel. Or a prequel. Or a reboot. What would be the worst possible thing that your favorite film gets?<br /><br />Discuss <a href="http://www.imdb.com/board/bd0000088/nest/255651330?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471861042&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk1">here</a> after voting.</p> <p class="seemore"><a href="/poll/aJWGd5UPaKc/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2471861042&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_sm" class="position_bottom supplemental" >Vote now</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','PollWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    
            </div>
            <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-3292747090._CB299577773_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,300x600,11x1;p=tr;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=882104874321;ord=882104874321?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript> <a href="http://pubads.g.doubleclick.net/gampad/jump?&iu=4215/imdb2.consumer.homepage/&sz=300x250|300x600|11x1&t=p%3Dtr%26fv%3D1%26ab%3Df%26bpx%3D1%26c%3D0%26s%3D3075%26s%3D32&tile=1&c=882104874321" target="_blank"> <img src="http://pubads.g.doubleclick.net/gampad/ad?&iu=4215/imdb2.consumer.homepage/&sz=300x250|300x600|11x1&t=p%3Dtr%26fv%3D1%26ab%3Df%26bpx%3D1%26c%3D0%26s%3D3075%26s%3D32&tile=1&c=882104874321" border="0" alt="advertisement" /> </a> </noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	

    
    
    

    
    
    

    
    
        <a name="slot_right-3"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_hd" > <h3>Opening This Week</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2381991"></div> <div class="title"> <a href="/title/tt2381991?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t0"> The Huntsman: Winter's War </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2093991"></div> <div class="title"> <a href="/title/tt2093991?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t1"> Elvis & Nixon </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2980210"></div> <div class="title"> <a href="/title/tt2980210?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t2"> A Hologram for the King </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3278330"></div> <div class="title"> <a href="/title/tt3278330?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t3"> Tale of Tales </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3367294"></div> <div class="title"> <a href="/title/tt3367294?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t4"> Compadres </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4501454"></div> <div class="title"> <a href="/title/tt4501454?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t5"> The Meddler </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_sm" class="position_bottom supplemental" >See more opening this week</a></p>
                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','WatchableTitlesListWidget',{wb:1});}
            </script>
        




        </div>
    
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	

    
    
        <a name="slot_right-4"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="showtimesMessagingWidget"> <p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2006955922&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> </div> </div> </span>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-6"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','BoxOfficeListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/chart/boxoffice?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_hd" > <h3>Now Playing (Box Office)</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3040964"></div> <div class="title"> <a href="/title/tt3040964?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t0"> The Jungle Book </a> <span class="secondary-text">Weekend: $103.6M</span> </div> <div class="action"> <a href="/showtimes/title/tt3040964?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3628584"></div> <div class="title"> <a href="/title/tt3628584?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t1"> Barbershop: The Next Cut </a> <span class="secondary-text">Weekend: $20.2M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2702724"></div> <div class="title"> <a href="/title/tt2702724?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t2"> The Boss </a> <span class="secondary-text">Weekend: $10.2M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2975590"></div> <div class="title"> <a href="/title/tt2975590?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t3"> Batman v Superman: Dawn of Justice </a> <span class="secondary-text">Weekend: $9.0M</span> </div> <div class="action"> <a href="/showtimes/title/tt2975590?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2948356"></div> <div class="title"> <a href="/title/tt2948356?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t4"> Zootopia </a> <span class="secondary-text">Weekend: $8.2M</span> </div> <div class="action"> <a href="/showtimes/title/tt2948356?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> </div> </div> </div> <p class="seemore"><a href="/chart/boxoffice?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_sm" class="position_bottom supplemental" >See more box office results</a></p>
                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','BoxOfficeListWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-8"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_hd" > <h3>Coming Soon</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2865120"></div> <div class="title"> <a href="/title/tt2865120?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t0"> Ratchet & Clank </a> <span class="secondary-text"></span> </div> <div class="action"> <a href="/showtimes/title/tt2865120?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4139124"></div> <div class="title"> <a href="/title/tt4139124?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t1"> Keanu </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4824302"></div> <div class="title"> <a href="/title/tt4824302?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t2"> Mother's Day </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2749282"></div> <div class="title"> <a href="/title/tt2749282?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t3"> Papa Hemingway in Cuba </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2800050"></div> <div class="title"> <a href="/title/tt2800050?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t4"> A Beautiful Planet </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_sm" class="position_bottom supplemental" >See more coming soon</a></p>
                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','WatchableTitlesListWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-12"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','TwitterWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_twitter">
<span class="widget_header"> <span class="oneline"> <h3>Follow Us On Twitter</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src="http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3626940295._CB298540565_.html?config=%7B%22width%22%3A%22300px%22%2C%22height%22%3A%22500px%22%2C%22href%22%3A%22https%3A%2F%2Ftwitter.com%2FIMDb%22%2C%22heading%22%3A%22Follow%20Us%20On%20Twitter%22%2C%22screen-name%22%3A%22IMDb%22%2C%22list-name%22%3Anull%2C%22widget-id%22%3A%22354387022028357633%22%7D"></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','TwitterWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-14"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','FacebookWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_facebook">
<span class="widget_header"> <span class="oneline"> <h3>Find Us On Facebook</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','FacebookWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-30"></a>
        <div class="aux-content-widget-2 sticky-widget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/gallery/rg3972045568/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2467121862&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ecw_rdj_hd" > <h3>Robert Downey Jr. Through the Years</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm957647872/rg3972045568?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2467121862&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ecw_rdj_i_1" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTk3NzIwNTA3MF5BMl5BanBnXkFtZTcwMjUyNTIzMw@@._UY300_CR40,30,307,230_SY230_SX307_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk3NzIwNTA3MF5BMl5BanBnXkFtZTcwMjUyNTIzMw@@._UY300_CR40,30,307,230_SY230_SX307_AL_UY460_UX614_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">From <a href="/title/tt0371746/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2467121862&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ecw_rdj_lk1"><i>Iron Man</i></a> to <a href="/title/tt0988045/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2467121862&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ecw_rdj_lk2"><i>Sherlock Holmes</i></a>, take a look back at <a href="/name/nm0000375/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2467121862&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ecw_rdj_lk3">Robert Downey Jr.</a>'s movie career in photos.</p> <p class="seemore"><a href="/gallery/rg3972045568/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2467121862&pf_rd_r=12XARZSAAFGZ0P7VQVTB&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ecw_rdj_sm" class="position_bottom supplemental" >View the gallery</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    
            </div>
            <br class="clear" />
        </div>
        

    
    
    

    
    
    
        
    
        <br class="clear" />
    </div>
</div>
                





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYvI0l10Z6utQCicvET2e7bjhasgEygAe2U3IeU1_7YaxSRUQy0YHA2GNpd28-h6leEKh8v56zq%0D%0AepUpy0i7nNx1a00wqPQn17n4WceF9KXpuEueGNpth9vLBUemXxt3fO5x3oYJiS_Mml0wPpHl8L-y%0D%0AqWbv667rUZ7R8beJYrOhsRtFjj27kwZgkEPqoIGRoG0qUEbOjKquj231E_N6P1lOhzyXVjVGJEaT%0D%0AS-jKzabB420%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYpop6nO2iS3Is1bLLhYWQsBAqKc5x-LrbQlIk9lD7NsiItDCQqH5NVgg1Pb2faL9tSxjhzgf0H%0D%0AwgabIKdlikDxSbEnpYIQmzmA-th8hnh9RxbUVNLjU8UVLCuqX7wiR9tAwRgE4-3w9X1Pf1ki-miB%0D%0Ajz6OFvhNlUIROPAKLQ6y_ewQ1TLtCrY9C_FrxiQSO7w4eHjXBZy0VoAkanx86_U-wQ%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYucRl4XVHe9eEdQJiHJbQIlOEw5CuadJt0zWT1_OkVUE4cTwhW3IJcvv4l-oN-qNrbBsEHA0C0%0D%0AxSOLybg5a5SBuymKejhoCJRRGAzkdK-ZAUCykr77c-LCJjKH9ejGwytrD-Bug3yviuOQDm7B1530%0D%0Asp_z7PpkZU60tIrAVVPx-4xs-AQPxuK1m89O_vctG2dtcEUQ3Mh0aDTrNLDgRWMbpNVcRrQhF_v_%0D%0AMo3Og0dYuLFViRYCQnWrCGaKJ-bX68dBKq2pAum3etCSLLTjMIiObWyvxGHyl1UQ9u1cIQnxdPQi%0D%0A9UxNG7qL5lwxXNJciqPTRZieUlTfP2jSGpijApB1gLqNQMN7qaHe2I_7oolanS6AcrKmGjBCzlno%0D%0A_MV3UQB__cN3eRSQV6tKdAf0l_xgWmPROdcyQNV7mu4s4XqBYrI%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYrzTOvarBB6zQPvTfuFadeJisp2G5aHdEsIkwcVnGC6K3pZg0sFiSL7MTRoNhU8TQ74hvQoU4w%0D%0ANaQt7dgp10XKO75PxXJv3OzRAK97DV8iK1WZV_KN4PD6R6qIBuwd9zPQxdvFPkn6DmhYJLQz8ikw%0D%0ADGEnEPCph9PnpTvlShVcMxZ5ufSQBJ1TmHcnDuAIf0sbIqwXtncm8NV2-N8UvYj1Zg%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYi76qrKUdGeVRjDELmGpJWsxIQWyqQf9jdda5fGihwqQxfVYACn7Wvo9wR1s5jlHFeTW82rleH%0D%0AonfpMYIZWbCAQKjUyqBcIxfYX_fdla6zK-_f1pxKeAgmLP6MprXBrLOyhEy7t73FAHV6jWYBviSN%0D%0AwdhsdHTnIsFNRrMi3aV6ss4Riw9OWjS4PKSlDWDVs2LS%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYq_KbeG9MsalFr93X-cmcJ_zntXblsdeYl33dbwdFofXSNDcsFCjRXZYU9trCcdFDQNP-x60Ue%0D%0A_z-RkrX98e0DDOZfjMfCw0hqG7brStuTE3sJE-ojhAZPQMoQxvdvdRBngBliN_HzdAoGn-9OorjG%0D%0AimVAuRvMFYH6baETPLwqJlt2DTbynHiXOCSd6ymKRBsG%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top Rated Movies</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Box Office</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li><a href="/advertising/?ref_=ft_ad"
>Advertising</a></li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYgb5Bl4RMzK_uQL5KsgxtonDNyduNmlfh600xGjExH2ytF8K0v4mMZXAjrT2OIt-rr1GO99mp-%0D%0AJkrc6QxGNnxwaqyp0KpmtW-FL1nHl0HzkRiXSKqnpDUrViQxz88pFXllYDjkH-HRWS4-gJGWFIT4%0D%0A0leDLts49-g3ib_mrd8mujDOxxAqYw0aPJmHoZHPK2iJXQ6POUdYCfjRpvEN30E4-Q%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYv5Uci1clA_-1Br1oFH-o4HDTsuI0qXl2O9eHKC3S3KNuf19vtaK0eckq8hPMiVvcagmAGdWJQ%0D%0ACfbt_PYRaSopy20skUS6UWsgaFvHvak2C9qbsWoKr_fJdqm5y3tPfOaGfcOMp3NMtR7EjPedePi2%0D%0AnNV5ZBqrvieRFR-sQrFfacnZjSh-cveFsFpvSKnY17Cz%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYlJ2SLCJszh5q9xlsoIz1S9TLZKSoDWOlkJDOO33f9BVzUgCX1k8JkUHw9BK7Hyy_zqML5PvXF%0D%0AdsEO5PDiJsbO67i8GCBa4YeyFvFQzCO4TbEbD4VOWNKj8scP3x4ZvQSY6rnLplJHMeWYzsgY66Tn%0D%0A0MFp6k9zFxOFkAkop_D1rHDU1xdW4JdcRuG9UlSi1oq6ojWkk-Y9t1IEyOEOoGzPEkNOc68KqpUW%0D%0AFdnN1mOYOIV0btix1eGKCmqBoMNe5vza%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2016
                <a
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYrn2yvQZXvF532o7ylvZj4nl74KW7YljsSuHk9nFso_bhcyF4sPrb2-yWXgxrmduTtCpCiaWYh%0D%0A-GefNAZUHtzqmfYP_MUY8dJyQqGJnYZSHh2OYBTzL4aM4PEUWSRjxvXu_OORGdJZDxC_O_Dzfs-T%0D%0AkkNExr1yyEYiZAU7y5PXjPi_y8kK4b5mADXq66RQaItG6oOOsFGhEVCMhbnWSqYr4rSllP-2k5DP%0D%0AkkSw-_xABuI%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYsrv7KPKSPIlbMgxPbzRLszz0OM_M_cTQaibQphYSEyB57-HwdRuDSHLRdgclPPFO2gktsC9b9%0D%0ARmNTamRuSBEk4Ph6-2eEML-uTMMjFQjhqXzyyPoWKtUZk2twMI59CF1IQu6eRdd3SFs8o2C1tMN7%0D%0Anw1we5npa1rYXNqs-XvILp6qefzgw4ISz1ZgU82W7uSvGIsWOAiu6ZPTHCjKiOK8GPwH7gqoKg04%0D%0AWEEAYv2NF0k%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYqi0MAqHuXhl3ElzgczP18Y0pgDiVuqEcogvW9a1NeXoa8jN_lWiBo41poA7HwsrJhYesT0VxT%0D%0AsmZGKhcUSlgW7TT9hTphYSlnOwNwLXHC1AxNEbOO5vOMae6tlX4P3llByDAfI2BXP1jdlNRFfVSd%0D%0AbZl0aP8YEnTwOlGYLLB06t8DNS_Acm09tnTQBN0JvVBfHm10h_UrKgAXcoC8QMdvNFwn6FFNG052%0D%0Ada4BvTq_uXg%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYuSh_n6epegKhD364Kqndx_g-i4L0Qu7-phyNlY669g7APj8r5UCxAXfS1mbFnJIgDWYu8Kge9%0D%0A3hdXeaE4NHTFGn4KhEyxb2_z3CkedJrj2-41iTtZ5le0ifh6pJH9HuQ9Qtx4-dDUYUsnj-pb1gfC%0D%0ABm-fE3r_2p10cvHTvEHxs_hwqiAoqhrJ8mvuBhCvFKOcIoAKpaK1m4X0gPr5SN4eJnkByLEopjXs%0D%0ABrQ93on9dbA%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYh3lvJJk5MCJqAIKrjhL1-4j4zhCd7p3Xhubkeh1SEXTstCYrakQNiJTXR7lHqVUA1uIf7kTp1%0D%0AwURnWVvuQOzYCGszvHv7c1C9ZELAgA1gjxP2Yr7GMQhc2TRg7TExarrOYbCnaO_GxB90BCpYb-q7%0D%0AIyl2hbnq0suL836VsxwLnJ25ByD5sZYSNQfKlbLVKHv4oNdwp989HYOsrL1wLuakDGST2oSo1WWX%0D%0AzTD0CEI8tOw%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYm3d4S6_HK_zf50cHSaoKqU3tqCqpl3_PfkIzRmvkNFnNi5MqxEjWwh2Harn9qAb0QFKAO83K4%0D%0ANTbmu09xwjINjEVNvGOGp8eqHt-THS3e6WbG5W0EzJF3xxtiraO0lRbrjIHuarZj1lKMel7ljqX1%0D%0Ay0SXawiiWsq2yvxgvuOHJ5j8uinzVs1pq5P2tuUcEHNnYqOXyxFRmZgZTU2NRCj4fqcIJghzbv4e%0D%0A5bT0Ur-mdu5flm22Er0F9A9Wq2DxC_qM%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYno69864-q3-n3wzYBqTYHhTM8-V_DyCXiSOhw9-Ef6eQfPjk1tU3Q6-gK3wh9NwEDPRToASvU%0D%0AlLC8ynWiW8FL13SDDEi4ApqL1RwXIbctKrSBJNF5UC1_oO8kcZPImEQU0zQxAlJqduRLJA-whK1L%0D%0AXO17JgUTM3Eu2U-YPUqspvg%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYvHMCQTM0h5zu5ANq4ZJPn2FHHMPoFXrIPzw8FyT43vMshJUxEmf1uNSR2VJ9WUFk0GKXMqG7B%0D%0A48FhXeYRNtjiKWFFflswPQ6RdkRh8ts87qb28Hiu-Eto-V9v1BgUtv0jzqz2yB_zqs8miwB9Pt5T%0D%0A1uLJYOxwskP0Kk1dMvGnxBQ%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/common-1944661907._CB274936654_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-3420335593._CB274889133_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-335260898._CB289148898_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-194820129._CB286493173_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-2126106747._CB289323193_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=0101c2d1a9b23424c10bbac7e3623997498ced05edd80d4f21e313a50b4b41c3d1b4",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=882104874321"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-458185442._CB294884543_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=882104874321&ord=882104874321";
    },"unable to request AAN pixel");
</script>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="546"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
