<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-fe96285.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-fe96285.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-fe96285.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-fe96285.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kat.cr/sc-fe96285.js');

        sc('setHost', 'a.kat.cr');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: 'fe96285',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-fe96285.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<div id="_60318cd4e8d28f6fb76fe34e9bd9c498"></div>
<div id="_39ecb76dd457e5ac33776fdf11500d56"></div>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka">e</i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka">g</i></button></div>
			</form>
		</div>
        <div id="_277923e5f9d753c5b0630c28e641790c"></div>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080p/" class="tag4">1080p</a>
	<a href="/search/2014/" class="tag1">2014</a>
	<a href="/search/2015/" class="tag10">2015</a>
	<a href="/search/3d/" class="tag3">3d</a>
	<a href="/search/ant%20man/" class="tag2">ant man</a>
	<a href="/search/blindspot/" class="tag1">blindspot</a>
	<a href="/search/boruto%20naruto%20the%20movie/" class="tag2">boruto naruto the movie</a>
	<a href="/search/discography/" class="tag2">discography</a>
	<a href="/search/doctor%20who/" class="tag2">doctor who</a>
	<a href="/search/downton%20abbey/" class="tag2">downton abbey</a>
	<a href="/search/everest/" class="tag1">everest</a>
	<a href="/search/family%20guy/" class="tag1">family guy</a>
	<a href="/search/fear%20the%20walking%20dead/" class="tag3">fear the walking dead</a>
	<a href="/search/french/" class="tag2">french</a>
	<a href="/search/gotham/" class="tag2">gotham</a>
	<a href="/search/heroes%20reborn/" class="tag1">heroes reborn</a>
	<a href="/search/hindi/" class="tag7">hindi</a>
	<a href="/search/hindi%202015/" class="tag3">hindi 2015</a>
	<a href="/search/hindi%20movies%202015/" class="tag3">hindi movies 2015</a>
	<a href="/search/homeland/" class="tag2">homeland</a>
	<a href="/search/homeland%20s05e02/" class="tag2">homeland s05e02</a>
	<a href="/search/inside%20out/" class="tag1">inside out</a>
	<a href="/search/ita/" class="tag1">ita</a>
	<a href="/search/kat%20ph%20com/" class="tag8">kat ph com</a>
	<a href="/search/mad%20max/" class="tag1">mad max</a>
	<a href="/search/malayalam/" class="tag2">malayalam</a>
	<a href="/search/nezu/" class="tag10">nezu</a>
	<a href="/search/once%20upon%20a%20time/" class="tag2">once upon a time</a>
	<a href="/search/once%20upon%20a%20time%20s05e03/" class="tag2">once upon a time s05e03</a>
	<a href="/search/quantico/" class="tag3">quantico</a>
	<a href="/search/quantico%20s01e03/" class="tag1">quantico s01e03</a>
	<a href="/search/ripsalot/" class="tag4">ripsalot</a>
	<a href="/search/sicario/" class="tag1">sicario</a>
	<a href="/search/star%20wars/" class="tag2">star wars</a>
	<a href="/search/tamil/" class="tag3">tamil</a>
	<a href="/search/telugu/" class="tag3">telugu</a>
	<a href="/search/telugu%202015/" class="tag2">telugu 2015</a>
	<a href="/search/the%20blacklist/" class="tag1">the blacklist</a>
	<a href="/search/the%20martian/" class="tag2">the martian</a>
	<a href="/search/the%20walking%20dead/" class="tag10">the walking dead</a>
	<a href="/search/the%20walking%20dead%20s06/" class="tag2">the walking dead s06</a>
	<a href="/search/the%20walking%20dead%20s06e01/" class="tag6">the walking dead s06e01</a>
	<a href="/search/undefined/" class="tag10">undefined</a>
	<a href="/search/walking%20dead/" class="tag5">walking dead</a>
	<a href="/search/walking%20dead%20s06e01/" class="tag2">walking dead s06e01</a>
	<a href="/search/yify/" class="tag8">yify</a>
	<a href="/search/yify%201080p/" class="tag3">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag3">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11387419,0" class="icommentjs icon16" href="/dope-2015-1080p-brrip-x264-yify-t11387419.html#comment"> <em class="iconvalue">39</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dope-2015-1080p-brrip-x264-yify-t11387419.html" class="cellMainLink">Dope (2015) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="1758584009">1.64 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-10T04:47:08+00:00">10 Oct 2015, 04:47:08</span></td>
			<td class="green center">6895</td>
			<td class="red lasttd center">5622</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11387064,0" class="icommentjs icon16" href="/the-martian-2015-hdts-900mb-mkvcage-t11387064.html#comment"> <em class="iconvalue">64</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-martian-2015-hdts-900mb-mkvcage-t11387064.html" class="cellMainLink">The Martian (2015) HDTS 900MB - MkvCage</a></div>
			</td>
			<td class="nobr center" data-sort="947777676">903.87 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-10T02:48:14+00:00">10 Oct 2015, 02:48:14</span></td>
			<td class="green center">6505</td>
			<td class="red lasttd center">3238</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11400910,0" class="icommentjs icon16" href="/garm-wars-the-last-druid-2014-hdrip-xvid-ac3-evo-t11400910.html#comment"> <em class="iconvalue">34</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/garm-wars-the-last-druid-2014-hdrip-xvid-ac3-evo-t11400910.html" class="cellMainLink">Garm Wars The Last Druid 2014 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1507899718">1.4 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T17:18:33+00:00">12 Oct 2015, 17:18:33</span></td>
			<td class="green center">4765</td>
			<td class="red lasttd center">4944</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11400692,0" class="icommentjs icon16" href="/the-new-adventures-of-peter-pan-2015-hdrip-xvid-ac3-evo-t11400692.html#comment"> <em class="iconvalue">14</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-new-adventures-of-peter-pan-2015-hdrip-xvid-ac3-evo-t11400692.html" class="cellMainLink">The New Adventures of Peter Pan 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1510355387">1.41 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T16:30:04+00:00">12 Oct 2015, 16:30:04</span></td>
			<td class="green center">4016</td>
			<td class="red lasttd center">5325</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11397819,0" class="icommentjs icon16" href="/painkillers-2015-hdrip-xvid-ac3-evo-t11397819.html#comment"> <em class="iconvalue">23</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/painkillers-2015-hdrip-xvid-ac3-evo-t11397819.html" class="cellMainLink">Painkillers 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1494227624">1.39 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T06:08:20+00:00">12 Oct 2015, 06:08:20</span></td>
			<td class="green center">3750</td>
			<td class="red lasttd center">3758</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11401382,0" class="icommentjs icon16" href="/the-ouija-exorcism-2015-hdrip-xvid-ac3-evo-t11401382.html#comment"> <em class="iconvalue">20</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-ouija-exorcism-2015-hdrip-xvid-ac3-evo-t11401382.html" class="cellMainLink">The Ouija Exorcism 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1517696385">1.41 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T18:46:45+00:00">12 Oct 2015, 18:46:45</span></td>
			<td class="green center">3027</td>
			<td class="red lasttd center">3803</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11404003,0" class="icommentjs icon16" href="/vacation-2015-hdrip-xvid-etrg-t11404003.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/vacation-2015-hdrip-xvid-etrg-t11404003.html" class="cellMainLink">Vacation 2015 HDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="739515074">705.26 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T07:07:06+00:00">13 Oct 2015, 07:07:06</span></td>
			<td class="green center">2668</td>
			<td class="red lasttd center">3299</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11390012,0" class="icommentjs icon16" href="/turbo-kid-2015-1080p-brrip-x264-yify-t11390012.html#comment"> <em class="iconvalue">30</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/turbo-kid-2015-1080p-brrip-x264-yify-t11390012.html" class="cellMainLink">Turbo Kid (2015) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="1550486195">1.44 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-10T14:32:19+00:00">10 Oct 2015, 14:32:19</span></td>
			<td class="green center">2667</td>
			<td class="red lasttd center">1693</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11388367,0" class="icommentjs icon16" href="/cooties-2014-1080p-brrip-x264-yify-t11388367.html#comment"> <em class="iconvalue">28</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/cooties-2014-1080p-brrip-x264-yify-t11388367.html" class="cellMainLink">Cooties (2014) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="1328766059">1.24 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-10T09:13:06+00:00">10 Oct 2015, 09:13:06</span></td>
			<td class="green center">2451</td>
			<td class="red lasttd center">1455</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11393254,0" class="icommentjs icon16" href="/lesbian-vampire-killers-2009-720p-bluray-x264-aac-etrg-t11393254.html#comment"> <em class="iconvalue">66</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/lesbian-vampire-killers-2009-720p-bluray-x264-aac-etrg-t11393254.html" class="cellMainLink">Lesbian Vampire Killers 2009 720p BluRay x264 AAC-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="677801107">646.4 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T08:14:57+00:00">11 Oct 2015, 08:14:57</span></td>
			<td class="green center">2520</td>
			<td class="red lasttd center">969</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11402459,0" class="icommentjs icon16" href="/the-land-before-time-1988-1080p-brrip-x264-yify-t11402459.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-land-before-time-1988-1080p-brrip-x264-yify-t11402459.html" class="cellMainLink">The Land Before Time (1988) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="1322775060">1.23 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T00:08:56+00:00">13 Oct 2015, 00:08:56</span></td>
			<td class="green center">2122</td>
			<td class="red lasttd center">1640</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11396327,0" class="icommentjs icon16" href="/lifted-2010-1080p-brrip-x264-yify-t11396327.html#comment"> <em class="iconvalue">20</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/lifted-2010-1080p-brrip-x264-yify-t11396327.html" class="cellMainLink">Lifted (2010) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="1760919127">1.64 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T21:53:58+00:00">11 Oct 2015, 21:53:58</span></td>
			<td class="green center">1652</td>
			<td class="red lasttd center">1080</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11407125,0" class="icommentjs icon16" href="/some-kind-of-beautiful-2014-720p-brrip-x264-yify-t11407125.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/some-kind-of-beautiful-2014-720p-brrip-x264-yify-t11407125.html" class="cellMainLink">Some Kind Of Beautiful (2014) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="792380449">755.67 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T18:14:58+00:00">13 Oct 2015, 18:14:58</span></td>
			<td class="green center">870</td>
			<td class="red lasttd center">1918</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11405918,0" class="icommentjs icon16" href="/5-to-7-2014-720p-brrip-x264-yify-t11405918.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/5-to-7-2014-720p-brrip-x264-yify-t11405918.html" class="cellMainLink">5 to 7 (2014) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="793829028">757.05 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T15:10:20+00:00">13 Oct 2015, 15:10:20</span></td>
			<td class="green center">976</td>
			<td class="red lasttd center">1235</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11402474,0" class="icommentjs icon16" href="/inside-out-2015-1080p-web-dl-x264-aac-jyk-t11402474.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/inside-out-2015-1080p-web-dl-x264-aac-jyk-t11402474.html" class="cellMainLink">Inside Out 2015 1080p WEB-DL x264 AAC-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="2635370902">2.45 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T00:19:06+00:00">13 Oct 2015, 00:19:06</span></td>
			<td class="green center">1089</td>
			<td class="red lasttd center">848</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11397375,0" class="icommentjs icon16" href="/the-walking-dead-s06e01-proper-hdtv-x264-killers-ettv-t11397375.html#comment"> <em class="iconvalue">350</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-walking-dead-s06e01-proper-hdtv-x264-killers-ettv-t11397375.html" class="cellMainLink">The Walking Dead S06E01 PROPER HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="855438191">815.81 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T03:31:23+00:00">12 Oct 2015, 03:31:23</span></td>
			<td class="green center">38044</td>
			<td class="red lasttd center">7105</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11402549,0" class="icommentjs icon16" href="/gotham-s02e04-hdtv-x264-lol-ettv-t11402549.html#comment"> <em class="iconvalue">111</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/gotham-s02e04-hdtv-x264-lol-ettv-t11402549.html" class="cellMainLink">Gotham S02E04 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="240766052">229.61 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T01:00:19+00:00">13 Oct 2015, 01:00:19</span></td>
			<td class="green center">22014</td>
			<td class="red lasttd center">5724</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11402293,0" class="icommentjs icon16" href="/the-big-bang-theory-s09e04-hdtv-x264-lol-ettv-t11402293.html#comment"> <em class="iconvalue">118</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-big-bang-theory-s09e04-hdtv-x264-lol-ettv-t11402293.html" class="cellMainLink">The Big Bang Theory S09E04 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="138541040">132.12 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T23:04:18+00:00">12 Oct 2015, 23:04:18</span></td>
			<td class="green center">22502</td>
			<td class="red lasttd center">4341</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11402724,0" class="icommentjs icon16" href="/blindspot-s01e04-hdtv-x264-lol-ettv-t11402724.html#comment"> <em class="iconvalue">83</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/blindspot-s01e04-hdtv-x264-lol-ettv-t11402724.html" class="cellMainLink">Blindspot S01E04 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="323641233">308.65 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T02:02:17+00:00">13 Oct 2015, 02:02:17</span></td>
			<td class="green center">14318</td>
			<td class="red lasttd center">5513</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11397039,0" class="icommentjs icon16" href="/quantico-s01e03-hdtv-x264-lol-ettv-t11397039.html#comment"> <em class="iconvalue">178</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/quantico-s01e03-hdtv-x264-lol-ettv-t11397039.html" class="cellMainLink">Quantico S01E03 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="302660657">288.64 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T02:03:19+00:00">12 Oct 2015, 02:03:19</span></td>
			<td class="green center">11618</td>
			<td class="red lasttd center">2378</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11397194,0" class="icommentjs icon16" href="/homeland-s05e02-web-dl-x264-fum-ettv-t11397194.html#comment"> <em class="iconvalue">61</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/homeland-s05e02-web-dl-x264-fum-ettv-t11397194.html" class="cellMainLink">Homeland S05E02 WEB-DL x264-FUM[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="278960539">266.04 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T02:42:24+00:00">12 Oct 2015, 02:42:24</span></td>
			<td class="green center">10328</td>
			<td class="red lasttd center">1331</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11402717,0" class="icommentjs icon16" href="/scorpion-s02e04-hdtv-x264-lol-ettv-t11402717.html#comment"> <em class="iconvalue">44</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/scorpion-s02e04-hdtv-x264-lol-ettv-t11402717.html" class="cellMainLink">Scorpion S02E04 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="309626423">295.28 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T02:00:20+00:00">13 Oct 2015, 02:00:20</span></td>
			<td class="green center">8499</td>
			<td class="red lasttd center">2825</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11402913,0" class="icommentjs icon16" href="/fargo-s02e01-hdtv-x264-killers-ettv-t11402913.html#comment"> <em class="iconvalue">59</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fargo-s02e01-hdtv-x264-killers-ettv-t11402913.html" class="cellMainLink">Fargo S02E01 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="280754123">267.75 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T03:12:23+00:00">13 Oct 2015, 03:12:23</span></td>
			<td class="green center">8259</td>
			<td class="red lasttd center">2447</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11402878,0" class="icommentjs icon16" href="/minority-report-s01e04-hdtv-x264-fum-ettv-t11402878.html#comment"> <em class="iconvalue">38</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/minority-report-s01e04-hdtv-x264-fum-ettv-t11402878.html" class="cellMainLink">Minority Report S01E04 HDTV x264-FUM[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="284386915">271.21 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T02:56:28+00:00">13 Oct 2015, 02:56:28</span></td>
			<td class="green center">7933</td>
			<td class="red lasttd center">2616</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11392366,0" class="icommentjs icon16" href="/the-last-kingdom-s01e01-hdtv-x264-killers-ettv-t11392366.html#comment"> <em class="iconvalue">114</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-last-kingdom-s01e01-hdtv-x264-killers-ettv-t11392366.html" class="cellMainLink">The Last Kingdom S01E01 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="445404586">424.77 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T03:28:16+00:00">11 Oct 2015, 03:28:16</span></td>
			<td class="green center">8587</td>
			<td class="red lasttd center">1263</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11396866,0" class="icommentjs icon16" href="/once-upon-a-time-s05e03-hdtv-x264-killers-ettv-t11396866.html#comment"> <em class="iconvalue">66</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/once-upon-a-time-s05e03-hdtv-x264-killers-ettv-t11396866.html" class="cellMainLink">Once Upon A Time S05E03 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="356212275">339.71 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T01:07:16+00:00">12 Oct 2015, 01:07:16</span></td>
			<td class="green center">7582</td>
			<td class="red lasttd center">1308</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11402919,0" class="icommentjs icon16" href="/castle-2009-s08e04-hdtv-xvid-fum-ettv-t11402919.html#comment"> <em class="iconvalue">28</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/castle-2009-s08e04-hdtv-xvid-fum-ettv-t11402919.html" class="cellMainLink">Castle 2009 S08E04 HDTV XviD-FUM[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="365904886">348.95 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T03:13:25+00:00">13 Oct 2015, 03:13:25</span></td>
			<td class="green center">7083</td>
			<td class="red lasttd center">1947</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11397150,0" class="icommentjs icon16" href="/brooklyn-nine-nine-s03e03-hdtv-x264-fum-ettv-t11397150.html#comment"> <em class="iconvalue">24</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/brooklyn-nine-nine-s03e03-hdtv-x264-fum-ettv-t11397150.html" class="cellMainLink">Brooklyn Nine-Nine S03E03 HDTV x264-FUM[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="174463897">166.38 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T02:28:24+00:00">12 Oct 2015, 02:28:24</span></td>
			<td class="green center">5197</td>
			<td class="red lasttd center">701</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11403015,0" class="icommentjs icon16" href="/wwe-raw-10-12-2015-hdtv-x264-killer9-sparrow-t11403015.html#comment"> <em class="iconvalue">29</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/wwe-raw-10-12-2015-hdtv-x264-killer9-sparrow-t11403015.html" class="cellMainLink">WWE Raw 10 12 2015 HDTV x264-Killer9 -={SPARROW}=-</a></div>
			</td>
			<td class="nobr center" data-sort="1412362666">1.32 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T03:49:53+00:00">13 Oct 2015, 03:49:53</span></td>
			<td class="green center">4192</td>
			<td class="red lasttd center">2466</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11396942,0" class="icommentjs icon16" href="/family-guy-s14e03-hdtv-x264-killers-ettv-t11396942.html#comment"> <em class="iconvalue">56</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/family-guy-s14e03-hdtv-x264-killers-ettv-t11396942.html" class="cellMainLink">Family Guy S14E03 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="65414094">62.38 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T01:32:21+00:00">12 Oct 2015, 01:32:21</span></td>
			<td class="green center">4904</td>
			<td class="red lasttd center">321</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11390820,0" class="icommentjs icon16" href="/billboard-hot-100-singles-chart-17th-october-2015-rz-rg-mp3-320kbps-glodls-t11390820.html#comment"> <em class="iconvalue">16</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/billboard-hot-100-singles-chart-17th-october-2015-rz-rg-mp3-320kbps-glodls-t11390820.html" class="cellMainLink">Billboard Hot 100 Singles Chart (17th October 2015) RZ-RG [MP3-320KBPS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="895572982">854.08 <span>MB</span></td>
			<td class="center">104</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-10T18:25:58+00:00">10 Oct 2015, 18:25:58</span></td>
			<td class="green center">1482</td>
			<td class="red lasttd center">882</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11392486,0" class="icommentjs icon16" href="/the-weeknd-the-hills-remixes-with-nicki-minaj-and-eminem-t11392486.html#comment"> <em class="iconvalue">29</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-weeknd-the-hills-remixes-with-nicki-minaj-and-eminem-t11392486.html" class="cellMainLink">The Weeknd - The Hills (Remixes with Nicki Minaj and Eminem)</a></div>
			</td>
			<td class="nobr center" data-sort="13730830">13.09 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T04:24:50+00:00">11 Oct 2015, 04:24:50</span></td>
			<td class="green center">779</td>
			<td class="red lasttd center">67</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11390309,0" class="icommentjs icon16" href="/va-ibiza-2015-closing-party-2015-mp3-320-kbps-t11390309.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-ibiza-2015-closing-party-2015-mp3-320-kbps-t11390309.html" class="cellMainLink">VA - Ibiza 2015 Closing Party (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="1116452026">1.04 <span>GB</span></td>
			<td class="center">52</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-10T16:02:56+00:00">10 Oct 2015, 16:02:56</span></td>
			<td class="green center">583</td>
			<td class="red lasttd center">249</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11394707,0" class="icommentjs icon16" href="/janet-jackson-unbreakable-deluxe-edition-2015-mp3-vbr-h4ckus-glodls-t11394707.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/janet-jackson-unbreakable-deluxe-edition-2015-mp3-vbr-h4ckus-glodls-t11394707.html" class="cellMainLink">Janet Jackson - Unbreakable [Deluxe Edition] [2015] [MP3-VBR] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="132646525">126.5 <span>MB</span></td>
			<td class="center">24</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T14:50:18+00:00">11 Oct 2015, 14:50:18</span></td>
			<td class="green center">504</td>
			<td class="red lasttd center">122</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11400816,0" class="icommentjs icon16" href="/va-fitness-hits-vol-1-2015-mp3-320-kbps-t11400816.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-fitness-hits-vol-1-2015-mp3-320-kbps-t11400816.html" class="cellMainLink">VA - Fitness Hits Vol. 1 (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="650968735">620.81 <span>MB</span></td>
			<td class="center">45</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T17:01:04+00:00">12 Oct 2015, 17:01:04</span></td>
			<td class="green center">418</td>
			<td class="red lasttd center">242</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11403600,0" class="icommentjs icon16" href="/jean-michel-jarre-electronica-1-the-time-machine-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11403600.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/jean-michel-jarre-electronica-1-the-time-machine-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11403600.html" class="cellMainLink">Jean-Michel Jarre â Electronica 1. The Time Machine [2015] [MP3-320Kbps] [CBR] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="167841479">160.07 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T05:24:36+00:00">13 Oct 2015, 05:24:36</span></td>
			<td class="green center">335</td>
			<td class="red lasttd center">111</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11389665,0" class="icommentjs icon16" href="/the-world-s-greatest-jazz-collection-500-cd-box-set-part-1-5-2008-smg-t11389665.html#comment"> <em class="iconvalue">15</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-world-s-greatest-jazz-collection-500-cd-box-set-part-1-5-2008-smg-t11389665.html" class="cellMainLink">The World&#039;s Greatest Jazz Collection [500 CD Box Set, Part 1-5] (2008) - SMG</a></div>
			</td>
			<td class="nobr center" data-sort="77496409230">72.17 <span>GB</span></td>
			<td class="center">9039</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-10T13:08:06+00:00">10 Oct 2015, 13:08:06</span></td>
			<td class="green center">20</td>
			<td class="red lasttd center">656</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11395705,0" class="icommentjs icon16" href="/va-club-dance-ambience-vol-40-2015-mp3-320-kbps-t11395705.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-club-dance-ambience-vol-40-2015-mp3-320-kbps-t11395705.html" class="cellMainLink">VA - Club Dance Ambience vol.40 (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="1167482803">1.09 <span>GB</span></td>
			<td class="center">101</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T18:32:09+00:00">11 Oct 2015, 18:32:09</span></td>
			<td class="green center">219</td>
			<td class="red lasttd center">180</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11394625,0" class="icommentjs icon16" href="/va-now-that-s-what-i-call-movies-2015-mp3-320kbps-h4ckus-glodls-t11394625.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-now-that-s-what-i-call-movies-2015-mp3-320kbps-h4ckus-glodls-t11394625.html" class="cellMainLink">VA - Now That&#039;s What I Call Movies [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="165839959">158.16 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T14:23:58+00:00">11 Oct 2015, 14:23:58</span></td>
			<td class="green center">274</td>
			<td class="red lasttd center">55</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-deep-house-collection-vol-40-2015-mp3-320-kbps-t11395764.html" class="cellMainLink">VA - Deep House Collection vol.40 (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="2996406612">2.79 <span>GB</span></td>
			<td class="center">201</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T18:57:11+00:00">11 Oct 2015, 18:57:11</span></td>
			<td class="green center">175</td>
			<td class="red lasttd center">201</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11400923,0" class="icommentjs icon16" href="/va-happy-hardcore-top-50-best-ever-2015-mp3-320-kbps-t11400923.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-happy-hardcore-top-50-best-ever-2015-mp3-320-kbps-t11400923.html" class="cellMainLink">VA - Happy Hardcore Top 50 Best Ever (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="375624483">358.22 <span>MB</span></td>
			<td class="center">52</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T17:21:44+00:00">12 Oct 2015, 17:21:44</span></td>
			<td class="green center">138</td>
			<td class="red lasttd center">83</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-best-of-house-music-vol-4-2015-mp3-320-kbps-t11390470.html" class="cellMainLink">VA - Best of House Music vol.4 (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="3430148106">3.19 <span>GB</span></td>
			<td class="center">300</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-10T16:52:41+00:00">10 Oct 2015, 16:52:41</span></td>
			<td class="green center">86</td>
			<td class="red lasttd center">149</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11399809,0" class="icommentjs icon16" href="/b-a-r-2015-week-40-glodls-t11399809.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/b-a-r-2015-week-40-glodls-t11399809.html" class="cellMainLink">B.A.R. 2015 Week 40 - [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="4456946295">4.15 <span>GB</span></td>
			<td class="center">554</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T13:27:17+00:00">12 Oct 2015, 13:27:17</span></td>
			<td class="green center">78</td>
			<td class="red lasttd center">133</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11400901,0" class="icommentjs icon16" href="/va-endless-deep-finest-deep-house-grooves-vol-1-2015-mp3-320-kbps-t11400901.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-endless-deep-finest-deep-house-grooves-vol-1-2015-mp3-320-kbps-t11400901.html" class="cellMainLink">VA - Endless Deep: Finest Deep House Grooves Vol. 1 (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="238661371">227.61 <span>MB</span></td>
			<td class="center">27</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T17:17:38+00:00">12 Oct 2015, 17:17:38</span></td>
			<td class="green center">107</td>
			<td class="red lasttd center">43</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11400116,0" class="icommentjs icon16" href="/kiss-fm-fresh-top-40-octombrie-2015-romanian-extremlymtorrents-t11400116.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/kiss-fm-fresh-top-40-octombrie-2015-romanian-extremlymtorrents-t11400116.html" class="cellMainLink">Kiss Fm - Fresh Top 40 - Octombrie 2015 Romanian ExtremlymTorrents</a></div>
			</td>
			<td class="nobr center" data-sort="140062208">133.57 <span>MB</span></td>
			<td class="center">40</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T14:20:42+00:00">12 Oct 2015, 14:20:42</span></td>
			<td class="green center">119</td>
			<td class="red lasttd center">6</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11401982,0" class="icommentjs icon16" href="/the-witcher-3-wild-hunt-hearts-of-stone-gog-t11401982.html#comment"> <em class="iconvalue">121</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/the-witcher-3-wild-hunt-hearts-of-stone-gog-t11401982.html" class="cellMainLink">The Witcher 3: Wild Hunt - Hearts of Stone (GOG)</a></div>
			</td>
			<td class="nobr center" data-sort="4563157730">4.25 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T21:55:50+00:00">12 Oct 2015, 21:55:50</span></td>
			<td class="green center">1383</td>
			<td class="red lasttd center">2155</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11406717,0" class="icommentjs icon16" href="/minecraft-story-mode-episode-1-reloaded-t11406717.html#comment"> <em class="iconvalue">29</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/minecraft-story-mode-episode-1-reloaded-t11406717.html" class="cellMainLink">Minecraft Story Mode Episode 1-RELOADED</a></div>
			</td>
			<td class="nobr center" data-sort="764947807">729.51 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T17:17:32+00:00">13 Oct 2015, 17:17:32</span></td>
			<td class="green center">759</td>
			<td class="red lasttd center">684</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11404187,0" class="icommentjs icon16" href="/wasteland-2-directors-cut-codex-t11404187.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/wasteland-2-directors-cut-codex-t11404187.html" class="cellMainLink">Wasteland 2 Directors Cut-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="12023601002">11.2 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T07:45:06+00:00">13 Oct 2015, 07:45:06</span></td>
			<td class="green center">153</td>
			<td class="red lasttd center">415</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11389746,0" class="icommentjs icon16" href="/sid-meier-s-civilization-beyond-earth-rising-tide-multi11-2-dlc-fitgirl-repack-selective-download-from-2-2-gb-t11389746.html#comment"> <em class="iconvalue">44</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/sid-meier-s-civilization-beyond-earth-rising-tide-multi11-2-dlc-fitgirl-repack-selective-download-from-2-2-gb-t11389746.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/sid-meier-s-civilization-beyond-earth-rising-tide-multi11-2-dlc-fitgirl-repack-selective-download-from-2-2-gb-t11389746.html" class="cellMainLink">Sid Meier&#039;s Civilization: Beyond Earth - Rising Tide (MULTI11, 2 DLC) [FitGirl Repack, Selective Download - from 2.2 GB]</a></div>
			</td>
			<td class="nobr center" data-sort="3824799313">3.56 <span>GB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-10T13:30:36+00:00">10 Oct 2015, 13:30:36</span></td>
			<td class="green center">160</td>
			<td class="red lasttd center">188</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11391070,0" class="icommentjs icon16" href="/galactic-civilizations-3-v1-32-5-dlc-multi4-fitgirl-repack-selective-download-5-6-gb-t11391070.html#comment"> <em class="iconvalue">28</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/galactic-civilizations-3-v1-32-5-dlc-multi4-fitgirl-repack-selective-download-5-6-gb-t11391070.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/galactic-civilizations-3-v1-32-5-dlc-multi4-fitgirl-repack-selective-download-5-6-gb-t11391070.html" class="cellMainLink">Galactic Civilizations 3 (v1.32 + 5 DLC, MULTI4) [FitGirl Repack, Selective Download - 5.6 GB]</a></div>
			</td>
			<td class="nobr center" data-sort="7260509247">6.76 <span>GB</span></td>
			<td class="center">15</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-10T19:28:01+00:00">10 Oct 2015, 19:28:01</span></td>
			<td class="green center">100</td>
			<td class="red lasttd center">209</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11396257,0" class="icommentjs icon16" href="/wrc-5-fia-world-rally-championship-v1-0-2-multi8-fitgirl-repack-100-lossless-t11396257.html#comment"> <em class="iconvalue">60</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/wrc-5-fia-world-rally-championship-v1-0-2-multi8-fitgirl-repack-100-lossless-t11396257.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/wrc-5-fia-world-rally-championship-v1-0-2-multi8-fitgirl-repack-100-lossless-t11396257.html" class="cellMainLink">WRC 5 FIA World Rally Championship (v1.0.2, MULTI8) [FitGirl Repack, 100% Lossless]</a></div>
			</td>
			<td class="nobr center" data-sort="5501525304">5.12 <span>GB</span></td>
			<td class="center">8</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T21:21:32+00:00">11 Oct 2015, 21:21:32</span></td>
			<td class="green center">79</td>
			<td class="red lasttd center">248</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11405480,0" class="icommentjs icon16" href="/borderlands-2-v-1-8-0-dlc-s-2012-pc-repack-by-seyter-t11405480.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/borderlands-2-v-1-8-0-dlc-s-2012-pc-repack-by-seyter-t11405480.html" class="cellMainLink">Borderlands 2 [v 1.8.0 + DLC&#039;s] (2012) PC | RePack by SEYTER</a></div>
			</td>
			<td class="nobr center" data-sort="7156584075">6.67 <span>GB</span></td>
			<td class="center">18</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T13:37:11+00:00">13 Oct 2015, 13:37:11</span></td>
			<td class="green center">108</td>
			<td class="red lasttd center">90</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11401800,0" class="icommentjs icon16" href="/lula-the-sexy-empire-good-old-games-t11401800.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/lula-the-sexy-empire-good-old-games-t11401800.html" class="cellMainLink">Lula The Sexy Empire - Good Old Games</a></div>
			</td>
			<td class="nobr center" data-sort="371223712">354.03 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T20:41:50+00:00">12 Oct 2015, 20:41:50</span></td>
			<td class="green center">118</td>
			<td class="red lasttd center">51</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11387844,0" class="icommentjs icon16" href="/off-the-record-5-the-final-interview-collectors-edition-asg-t11387844.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/off-the-record-5-the-final-interview-collectors-edition-asg-t11387844.html" class="cellMainLink">Off the Record - 5 The Final Interview Collectors Edition [ASG]</a></div>
			</td>
			<td class="nobr center" data-sort="868509981">828.28 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-10T06:27:28+00:00">10 Oct 2015, 06:27:28</span></td>
			<td class="green center">107</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11402947,0" class="icommentjs icon16" href="/huniepop-gog-t11402947.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/huniepop-gog-t11402947.html" class="cellMainLink">HuniePop (GOG)</a></div>
			</td>
			<td class="nobr center" data-sort="311343396">296.92 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T03:23:50+00:00">13 Oct 2015, 03:23:50</span></td>
			<td class="green center">84</td>
			<td class="red lasttd center">19</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11394064,0" class="icommentjs icon16" href="/project-cars-gold-v-5-0-all-dlc-maxagent-repack-t11394064.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/project-cars-gold-v-5-0-all-dlc-maxagent-repack-t11394064.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/project-cars-gold-v-5-0-all-dlc-maxagent-repack-t11394064.html" class="cellMainLink">Project CARS GOLD v.5.0 + All DLC - MAXAGENT Repack</a></div>
			</td>
			<td class="nobr center" data-sort="15649313474">14.57 <span>GB</span></td>
			<td class="center">14</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T11:56:26+00:00">11 Oct 2015, 11:56:26</span></td>
			<td class="green center">20</td>
			<td class="red lasttd center">93</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11395195,0" class="icommentjs icon16" href="/dragons-rise-of-berk-v1-14-9-mod-free-shopping-apk-xpoz-t11395195.html#comment"> <em class="iconvalue">17</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/dragons-rise-of-berk-v1-14-9-mod-free-shopping-apk-xpoz-t11395195.html" class="cellMainLink">Dragons-Rise of Berk v1.14.9 Mod (Free Shopping) Apk-XpoZ</a></div>
			</td>
			<td class="nobr center" data-sort="50277762">47.95 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T16:44:50+00:00">11 Oct 2015, 16:44:50</span></td>
			<td class="green center">57</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11393894,0" class="icommentjs icon16" href="/dead-island-game-of-the-year-edition-dead-island-riptide-repack-mr-dj-t11393894.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/dead-island-game-of-the-year-edition-dead-island-riptide-repack-mr-dj-t11393894.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/dead-island-game-of-the-year-edition-dead-island-riptide-repack-mr-dj-t11393894.html" class="cellMainLink">Dead Island - Game of the Year Edition + Dead Island: Riptide repack Mr DJ</a></div>
			</td>
			<td class="nobr center" data-sort="4415893663">4.11 <span>GB</span></td>
			<td class="center">8</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T11:14:06+00:00">11 Oct 2015, 11:14:06</span></td>
			<td class="green center">34</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11398768,0" class="icommentjs icon16" href="/vortex-the-gateway-v0-671-t11398768.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/vortex-the-gateway-v0-671-t11398768.html" class="cellMainLink">Vortex: The Gateway v0.671</a></div>
			</td>
			<td class="nobr center" data-sort="893429301">852.04 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T10:57:36+00:00">12 Oct 2015, 10:57:36</span></td>
			<td class="green center">26</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11398745,0" class="icommentjs icon16" href="/darkest-dungeon-build-11015-t11398745.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/darkest-dungeon-build-11015-t11398745.html" class="cellMainLink">Darkest Dungeon Build 11015</a></div>
			</td>
			<td class="nobr center" data-sort="1319025986">1.23 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T10:51:44+00:00">12 Oct 2015, 10:51:44</span></td>
			<td class="green center">24</td>
			<td class="red lasttd center">11</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11395952,0" class="icommentjs icon16" href="/internet-download-manager-idm-6-25-build-1-registered-32bit-64bit-patch-crackingpatching-t11395952.html#comment"> <em class="iconvalue">29</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/internet-download-manager-idm-6-25-build-1-registered-32bit-64bit-patch-crackingpatching-t11395952.html" class="cellMainLink">Internet Download Manager (IDM) 6.25 Build 1 Registered (32bit + 64bit Patch) [CrackingPatching]</a></div>
			</td>
			<td class="nobr center" data-sort="10306187">9.83 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T19:46:54+00:00">11 Oct 2015, 19:46:54</span></td>
			<td class="green center">740</td>
			<td class="red lasttd center">386</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11394305,0" class="icommentjs icon16" href="/disk-doctors-windows-data-recovery-3-0-3-353-crack-techtools-t11394305.html#comment"> <em class="iconvalue">17</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/disk-doctors-windows-data-recovery-3-0-3-353-crack-techtools-t11394305.html" class="cellMainLink">Disk Doctors Windows Data Recovery 3.0.3.353 + Crack [TechTools]</a></div>
			</td>
			<td class="nobr center" data-sort="17923622">17.09 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T13:00:31+00:00">11 Oct 2015, 13:00:31</span></td>
			<td class="green center">422</td>
			<td class="red lasttd center">32</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11389415,0" class="icommentjs icon16" href="/nero-burning-rom-2016-17-0-00300-final-crack-techtools-t11389415.html#comment"> <em class="iconvalue">22</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/nero-burning-rom-2016-17-0-00300-final-crack-techtools-t11389415.html" class="cellMainLink">Nero Burning ROM 2016 17.0.00300 FINAL + Crack [TechTools]</a></div>
			</td>
			<td class="nobr center" data-sort="131857317">125.75 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-10T11:54:25+00:00">10 Oct 2015, 11:54:25</span></td>
			<td class="green center">320</td>
			<td class="red lasttd center">38</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11401314,0" class="icommentjs icon16" href="/malwarebytes-anti-malware-premium-2-2-0-1024-final-multilingual-incl-keygen-team-os-t11401314.html#comment"> <em class="iconvalue">26</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/malwarebytes-anti-malware-premium-2-2-0-1024-final-multilingual-incl-keygen-team-os-t11401314.html" class="cellMainLink">Malwarebytes Anti-Malware Premium 2.2.0.1024 Final Multilingual incl Keygen-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="22993764">21.93 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T18:34:05+00:00">12 Oct 2015, 18:34:05</span></td>
			<td class="green center">250</td>
			<td class="red lasttd center">37</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11403961,0" class="icommentjs icon16" href="/adobe-acrobat-xi-pro-11-0-13-multilingual-update-incl-patch-team-os-t11403961.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/adobe-acrobat-xi-pro-11-0-13-multilingual-update-incl-patch-team-os-t11403961.html" class="cellMainLink">Adobe Acrobat XI Pro 11.0.13 Multilingual+update incl Patch-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="760804786">725.56 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T06:53:01+00:00">13 Oct 2015, 06:53:01</span></td>
			<td class="green center">192</td>
			<td class="red lasttd center">149</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11387992,0" class="icommentjs icon16" href="/avg-internet-security-business-edition-2016-v2016-0-7161-x86-x64-multilingual-key-4realtorrentz-t11387992.html#comment"> <em class="iconvalue">26</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/avg-internet-security-business-edition-2016-v2016-0-7161-x86-x64-multilingual-key-4realtorrentz-t11387992.html" class="cellMainLink">AVG Internet Security Business Edition 2016 v2016.0.7161 (x86+x64) Multilingual + Key [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="452676677">431.71 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-10T07:13:24+00:00">10 Oct 2015, 07:13:24</span></td>
			<td class="green center">221</td>
			<td class="red lasttd center">56</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11404778,0" class="icommentjs icon16" href="/eset-nod32-antivirus-smart-security-9-0-318-x86x64-incl-serial-team-os-t11404778.html#comment"> <em class="iconvalue">13</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/eset-nod32-antivirus-smart-security-9-0-318-x86x64-incl-serial-team-os-t11404778.html" class="cellMainLink">ESET NOD32 Antivirus &amp; Smart Security 9.0.318 (x86x64) incl Serial-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="370049602">352.91 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T10:18:44+00:00">13 Oct 2015, 10:18:44</span></td>
			<td class="green center">166</td>
			<td class="red lasttd center">108</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11387166,0" class="icommentjs icon16" href="/winrar-v5-30-beta-5-x86x64-incl-key-4realtorrentz-t11387166.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/winrar-v5-30-beta-5-x86x64-incl-key-4realtorrentz-t11387166.html" class="cellMainLink">WinRAR.v5.30 Beta 5 (x86x64) Incl Key [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="2135434">2.04 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-10T03:26:11+00:00">10 Oct 2015, 03:26:11</span></td>
			<td class="green center">166</td>
			<td class="red lasttd center">11</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11405321,0" class="icommentjs icon16" href="/adobe-flash-player-19-0-0-207-final-2015-pc-t11405321.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/adobe-flash-player-19-0-0-207-final-2015-pc-t11405321.html" class="cellMainLink">Adobe Flash Player 19.0.0.207 Final (2015) PC</a></div>
			</td>
			<td class="nobr center" data-sort="57684768">55.01 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T13:02:23+00:00">13 Oct 2015, 13:02:23</span></td>
			<td class="green center">153</td>
			<td class="red lasttd center">36</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11390690,0" class="icommentjs icon16" href="/izotope-rx-audio-editor-advanced-v5-00-deepstatus-t11390690.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/izotope-rx-audio-editor-advanced-v5-00-deepstatus-t11390690.html" class="cellMainLink">iZotope RX Audio Editor Advanced v5.00 [deepstatus]</a></div>
			</td>
			<td class="nobr center" data-sort="305095897">290.96 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-10T17:53:14+00:00">10 Oct 2015, 17:53:14</span></td>
			<td class="green center">120</td>
			<td class="red lasttd center">20</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11400416,0" class="icommentjs icon16" href="/acdsee-ultimate-9-0-build-565-x64-11-10-2015-2015-pc-repack-by-kpojiuk-t11400416.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/acdsee-ultimate-9-0-build-565-x64-11-10-2015-2015-pc-repack-by-kpojiuk-t11400416.html" class="cellMainLink">ACDSee Ultimate 9.0 Build 565 [x64] (11.10.2015) (2015) PC | RePack by KpoJIuK</a></div>
			</td>
			<td class="nobr center" data-sort="68459602">65.29 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T15:31:46+00:00">12 Oct 2015, 15:31:46</span></td>
			<td class="green center">112</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11397546,0" class="icommentjs icon16" href="/cyberlink-youcam-deluxe-6-0-4601-0-multilingual-team-os-t11397546.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/cyberlink-youcam-deluxe-6-0-4601-0-multilingual-team-os-t11397546.html" class="cellMainLink">CyberLink YouCam Deluxe 6.0.4601.0 Multilingual-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="464396014">442.88 <span>MB</span></td>
			<td class="center">88</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T04:24:09+00:00">12 Oct 2015, 04:24:09</span></td>
			<td class="green center">84</td>
			<td class="red lasttd center">49</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11392295,0" class="icommentjs icon16" href="/ad-aware-pro-security-antivirus-11-8-586-8535-multilingual-key-appzdam-t11392295.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/ad-aware-pro-security-antivirus-11-8-586-8535-multilingual-key-appzdam-t11392295.html" class="cellMainLink">Ad-Aware Pro Security (AntiVirus) 11.8.586.8535 Multilingual + Key - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="507582908">484.07 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T03:03:51+00:00">11 Oct 2015, 03:03:51</span></td>
			<td class="green center">89</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11392003,0" class="icommentjs icon16" href="/adobe-photoshop-lightroom-cc-v6-2-1-update-only-multilang-deepstatus-t11392003.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/adobe-photoshop-lightroom-cc-v6-2-1-update-only-multilang-deepstatus-t11392003.html" class="cellMainLink">Adobe Photoshop Lightroom CC v6.2.1 Update Only MultiLang [deepstatus]</a></div>
			</td>
			<td class="nobr center" data-sort="283143830">270.03 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T01:01:40+00:00">11 Oct 2015, 01:01:40</span></td>
			<td class="green center">74</td>
			<td class="red lasttd center">7</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11390143,0" class="icommentjs icon16" href="/daz3d-poser-cheerleader-textures-pom-pom-poses-for-genesis-2-female-t11390143.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/daz3d-poser-cheerleader-textures-pom-pom-poses-for-genesis-2-female-t11390143.html" class="cellMainLink">Daz3D - Poser - Cheerleader + Textures + Pom Pom Poses for Genesis 2 Female</a></div>
			</td>
			<td class="nobr center" data-sort="56893411">54.26 <span>MB</span></td>
			<td class="center">11</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-10T15:09:56+00:00">10 Oct 2015, 15:09:56</span></td>
			<td class="green center">63</td>
			<td class="red lasttd center">3</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dragon-ball-super-14-french-subbed-avi-t11398647.html" class="cellMainLink">Dragon Ball Super 14 [French Subbed].avi</a></div>
			</td>
			<td class="nobr center" data-sort="555198464">529.48 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T10:10:09+00:00">12 Oct 2015, 10:10:09</span></td>
			<td class="green center">2398</td>
			<td class="red lasttd center">295</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11393240,0" class="icommentjs icon16" href="/animerg-dragon-ball-super-014-720p-phr0sty-mkv-t11393240.html#comment"> <em class="iconvalue">31</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-dragon-ball-super-014-720p-phr0sty-mkv-t11393240.html" class="cellMainLink">[AnimeRG] Dragon Ball Super 014 - 720p [Phr0stY].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="537779465">512.87 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T08:08:17+00:00">11 Oct 2015, 08:08:17</span></td>
			<td class="green center">918</td>
			<td class="red lasttd center">130</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11387450,0" class="icommentjs icon16" href="/ipunisher-dragon-ball-z-resurrection-f-dual-audio-bd-720p-x264-aac-mkv-t11387450.html#comment"> <em class="iconvalue">32</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/ipunisher-dragon-ball-z-resurrection-f-dual-audio-bd-720p-x264-aac-mkv-t11387450.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ipunisher-dragon-ball-z-resurrection-f-dual-audio-bd-720p-x264-aac-mkv-t11387450.html" class="cellMainLink">[iPUNISHER] Dragon Ball Z - Resurrection &#039;F&#039; (Dual Audio) [BD 720p x264 AAC].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="1195073817">1.11 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-10T04:55:50+00:00">10 Oct 2015, 04:55:50</span></td>
			<td class="green center">495</td>
			<td class="red lasttd center">134</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11393809,0" class="icommentjs icon16" href="/horriblesubs-mobile-suit-gundam-iron-blooded-orphans-02-720p-mkv-t11393809.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-mobile-suit-gundam-iron-blooded-orphans-02-720p-mkv-t11393809.html" class="cellMainLink">[HorribleSubs] Mobile Suit Gundam - Iron-Blooded Orphans - 02 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="619080719">590.4 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T10:50:04+00:00">11 Oct 2015, 10:50:04</span></td>
			<td class="green center">526</td>
			<td class="red lasttd center">39</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-hidan-no-aria-aa-02-raw-tva-1280x720-x264-aac-mp4-t11407267.html" class="cellMainLink">[Leopard-Raws] Hidan no Aria AA - 02 RAW (TVA 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="355821086">339.34 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T18:45:02+00:00">13 Oct 2015, 18:45:02</span></td>
			<td class="green center">253</td>
			<td class="red lasttd center">242</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-kidou-senshi-gundam-tekketsu-no-orphans-02-raw-tbs-1280x720-x264-aac-mp4-t11396106.html" class="cellMainLink">[Leopard-Raws] Kidou Senshi Gundam - Tekketsu no Orphans - 02 RAW (TBS 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="450852590">429.97 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T20:30:03+00:00">11 Oct 2015, 20:30:03</span></td>
			<td class="green center">325</td>
			<td class="red lasttd center">35</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-dia-no-ace-second-season-28-raw-tx-1280x720-x264-aac-mp4-t11404160.html" class="cellMainLink">[Leopard-Raws] Dia no Ace - Second Season - 28 RAW (TX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="409176183">390.22 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T07:35:04+00:00">13 Oct 2015, 07:35:04</span></td>
			<td class="green center">177</td>
			<td class="red lasttd center">35</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/vivid-owari-no-seraph-13-700eba12-mkv-t11398942.html" class="cellMainLink">[Vivid] Owari no Seraph - 13 [700EBA12].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="351619841">335.33 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T11:45:03+00:00">12 Oct 2015, 11:45:03</span></td>
			<td class="green center">185</td>
			<td class="red lasttd center">11</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11392217,0" class="icommentjs icon16" href="/animerg-one-piece-713-english-subbed-480p-kami-t11392217.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-one-piece-713-english-subbed-480p-kami-t11392217.html" class="cellMainLink">[AnimeRG] One Piece - 713 [English Subbed][480p] [KaMi]</a></div>
			</td>
			<td class="nobr center" data-sort="81956263">78.16 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T02:29:14+00:00">11 Oct 2015, 02:29:14</span></td>
			<td class="green center">164</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fff-infinite-stratos-2-bd-720p-aac-t11402519.html" class="cellMainLink">[FFF] Infinite Stratos 2 [BD][720p-AAC]</a></div>
			</td>
			<td class="nobr center" data-sort="7101149916">6.61 <span>GB</span></td>
			<td class="center">25</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T00:40:04+00:00">13 Oct 2015, 00:40:04</span></td>
			<td class="green center">107</td>
			<td class="red lasttd center">113</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11393066,0" class="icommentjs icon16" href="/fff-gakusen-toshi-asterisk-01-a272496f-mkv-t11393066.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fff-gakusen-toshi-asterisk-01-a272496f-mkv-t11393066.html" class="cellMainLink">[FFF] Gakusen Toshi Asterisk - 01 [A272496F].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="627215420">598.16 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T07:15:02+00:00">11 Oct 2015, 07:15:02</span></td>
			<td class="green center">120</td>
			<td class="red lasttd center">4</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/commie-one-punch-man-02-2166bee3-mkv-t11399646.html" class="cellMainLink">[Commie] One-Punch Man - 02 [2166BEE3].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="486873602">464.32 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T12:50:02+00:00">12 Oct 2015, 12:50:02</span></td>
			<td class="green center">94</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/bakedfish-one-punch-man-02-720p-aac-mp4-t11396275.html" class="cellMainLink">[BakedFish] One Punch Man - 02 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="469354584">447.61 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T21:29:33+00:00">11 Oct 2015, 21:29:33</span></td>
			<td class="green center">96</td>
			<td class="red lasttd center">11</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fff-noragami-aragoto-01-9798cc96-mkv-t11387875.html" class="cellMainLink">[FFF] Noragami Aragoto - 01 [9798CC96].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="478466470">456.3 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-10T06:35:02+00:00">10 Oct 2015, 06:35:02</span></td>
			<td class="green center">91</td>
			<td class="red lasttd center">7</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fff-dungeon-ni-deai-wo-motomeru-no-wa-machigatteiru-darou-ka-vol-01-bd-720p-aac-t11407307.html" class="cellMainLink">[FFF] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka - Vol.01 [BD][720p-AAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1156374768">1.08 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T18:55:03+00:00">13 Oct 2015, 18:55:03</span></td>
			<td class="green center">67</td>
			<td class="red lasttd center">37</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11388145,0" class="icommentjs icon16" href="/assorted-magazines-bundle-october-11-2015-true-pdf-t11388145.html#comment"> <em class="iconvalue">20</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/assorted-magazines-bundle-october-11-2015-true-pdf-t11388145.html" class="cellMainLink">Assorted Magazines Bundle - October 11 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="895047020">853.58 <span>MB</span></td>
			<td class="center">61</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-10T08:08:00+00:00">10 Oct 2015, 08:08:00</span></td>
			<td class="green center">593</td>
			<td class="red lasttd center">310</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11395703,0" class="icommentjs icon16" href="/linux-bible-9th-edition-2015-pdf-epub-mobi-gooner-t11395703.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/linux-bible-9th-edition-2015-pdf-epub-mobi-gooner-t11395703.html" class="cellMainLink">Linux Bible - 9th Edition (2015) (Pdf, Epub &amp; Mobi) Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="57176562">54.53 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T18:30:52+00:00">11 Oct 2015, 18:30:52</span></td>
			<td class="green center">546</td>
			<td class="red lasttd center">87</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11395743,0" class="icommentjs icon16" href="/make-electronics-learning-through-discovery-2nd-edition-2015-pdf-epub-gooner-t11395743.html#comment"> <em class="iconvalue">17</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/make-electronics-learning-through-discovery-2nd-edition-2015-pdf-epub-gooner-t11395743.html" class="cellMainLink">Make Electronics - Learning Through Discovery - 2nd Edition (2015) (Pdf &amp; Epub) Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="149630236">142.7 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T18:48:41+00:00">11 Oct 2015, 18:48:41</span></td>
			<td class="green center">515</td>
			<td class="red lasttd center">126</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11392090,0" class="icommentjs icon16" href="/new-york-times-bestsellers-october-11-2015-fiction-non-fiction-ertb-t11392090.html#comment"> <em class="iconvalue">80</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/new-york-times-bestsellers-october-11-2015-fiction-non-fiction-ertb-t11392090.html" class="cellMainLink">New York Times Bestsellers (October 11, 2015)(Fiction + Non Fiction) {{ERTB}}</a></div>
			</td>
			<td class="nobr center" data-sort="126232539">120.38 <span>MB</span></td>
			<td class="center">30</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T01:36:00+00:00">11 Oct 2015, 01:36:00</span></td>
			<td class="green center">490</td>
			<td class="red lasttd center">88</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11390158,0" class="icommentjs icon16" href="/mens-magazines-bundle-october-11-2015-true-pdf-t11390158.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/mens-magazines-bundle-october-11-2015-true-pdf-t11390158.html" class="cellMainLink">Mens Magazines Bundle - October 11 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="146743259">139.95 <span>MB</span></td>
			<td class="center">8</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-10T15:14:52+00:00">10 Oct 2015, 15:14:52</span></td>
			<td class="green center">442</td>
			<td class="red lasttd center">93</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11388391,0" class="icommentjs icon16" href="/learning-c-by-example-2015-pdf-epub-gooner-t11388391.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/learning-c-by-example-2015-pdf-epub-gooner-t11388391.html" class="cellMainLink">Learning C by Example (2015) (Pdf &amp; Epub) Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="5686780">5.42 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-10T09:19:54+00:00">10 Oct 2015, 09:19:54</span></td>
			<td class="green center">364</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11388926,0" class="icommentjs icon16" href="/learning-python-network-programming-2015-pdf-epub-mobi-gooner-t11388926.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/learning-python-network-programming-2015-pdf-epub-mobi-gooner-t11388926.html" class="cellMainLink">Learning Python Network Programming (2015) (Pdf, Epub &amp; Mobi) Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="26350206">25.13 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-10T10:21:29+00:00">10 Oct 2015, 10:21:29</span></td>
			<td class="green center">348</td>
			<td class="red lasttd center">49</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11392294,0" class="icommentjs icon16" href="/home-magazines-bundle-october-11-2015-true-pdf-t11392294.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/home-magazines-bundle-october-11-2015-true-pdf-t11392294.html" class="cellMainLink">Home Magazines Bundle - October 11 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="537773976">512.86 <span>MB</span></td>
			<td class="center">19</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T03:03:40+00:00">11 Oct 2015, 03:03:40</span></td>
			<td class="green center">308</td>
			<td class="red lasttd center">124</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11401214,0" class="icommentjs icon16" href="/solar-energy-engineering-processes-and-systems-2nd-edition-2014-pdf-t11401214.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/solar-energy-engineering-processes-and-systems-2nd-edition-2014-pdf-t11401214.html" class="cellMainLink">Solar Energy Engineering: Processes and Systems, 2nd Edition [2014] [PDF]</a></div>
			</td>
			<td class="nobr center" data-sort="18289560">17.44 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T18:16:54+00:00">12 Oct 2015, 18:16:54</span></td>
			<td class="green center">306</td>
			<td class="red lasttd center">30</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11405727,0" class="icommentjs icon16" href="/injustice-gods-among-us-year-four-024-2015-digital-son-of-ultron-empire-cbr-nem-t11405727.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/injustice-gods-among-us-year-four-024-2015-digital-son-of-ultron-empire-cbr-nem-t11405727.html" class="cellMainLink">Injustice - Gods Among Us - Year Four 024 (2015) (digital) (Son of Ultron-Empire).cbr (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="21346545">20.36 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T14:30:02+00:00">13 Oct 2015, 14:30:02</span></td>
			<td class="green center">290</td>
			<td class="red lasttd center">31</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11392134,0" class="icommentjs icon16" href="/food-magazines-bundle-october-11-2015-true-pdf-t11392134.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/food-magazines-bundle-october-11-2015-true-pdf-t11392134.html" class="cellMainLink">Food Magazines Bundle - October 11 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="176815537">168.62 <span>MB</span></td>
			<td class="center">11</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T01:52:43+00:00">11 Oct 2015, 01:52:43</span></td>
			<td class="green center">275</td>
			<td class="red lasttd center">51</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11388432,0" class="icommentjs icon16" href="/lean-websites-1st-edition-2015-pdf-epub-mobi-gooner-t11388432.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/lean-websites-1st-edition-2015-pdf-epub-mobi-gooner-t11388432.html" class="cellMainLink">Lean Websites - 1st Edition (2015) (Pdf, Epub &amp; Mobi) Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="25368194">24.19 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-10T09:33:01+00:00">10 Oct 2015, 09:33:01</span></td>
			<td class="green center">245</td>
			<td class="red lasttd center">45</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11392389,0" class="icommentjs icon16" href="/outdoors-guns-hiking-magazines-october-11-2015-true-pdf-t11392389.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/outdoors-guns-hiking-magazines-october-11-2015-true-pdf-t11392389.html" class="cellMainLink">Outdoors Guns &amp; Hiking Magazines - October 11 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="231150696">220.44 <span>MB</span></td>
			<td class="center">13</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T03:35:25+00:00">11 Oct 2015, 03:35:25</span></td>
			<td class="green center">228</td>
			<td class="red lasttd center">65</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11401690,0" class="icommentjs icon16" href="/machine-learning-in-python-essential-techniques-for-predictive-analysis-1st-edition-2015-pdf-epub-mobi-gooner-t11401690.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/machine-learning-in-python-essential-techniques-for-predictive-analysis-1st-edition-2015-pdf-epub-mobi-gooner-t11401690.html" class="cellMainLink">Machine Learning in Python - Essential Techniques for Predictive Analysis - 1st Edition (2015) (Pdf, Epub &amp; Mobi) Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="32046202">30.56 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T20:12:55+00:00">12 Oct 2015, 20:12:55</span></td>
			<td class="green center">215</td>
			<td class="red lasttd center">48</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11401565,0" class="icommentjs icon16" href="/life-s-greatest-secret-the-race-to-crack-the-genetic-code-2015-epub-gooner-t11401565.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/life-s-greatest-secret-the-race-to-crack-the-genetic-code-2015-epub-gooner-t11401565.html" class="cellMainLink">Life&#039;s Greatest Secret - The Race to Crack the Genetic Code (2015).epub Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="20957156">19.99 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T19:29:51+00:00">12 Oct 2015, 19:29:51</span></td>
			<td class="green center">205</td>
			<td class="red lasttd center">21</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11393832,0" class="icommentjs icon16" href="/martin-scorsese-presents-the-blues-a-musical-journey-2003-t11393832.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/martin-scorsese-presents-the-blues-a-musical-journey-2003-t11393832.html" class="cellMainLink">Martin Scorsese Presents the Blues - A Musical Journey (2003)</a></div>
			</td>
			<td class="nobr center" data-sort="1832883716">1.71 <span>GB</span></td>
			<td class="center">163</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T10:55:24+00:00">11 Oct 2015, 10:55:24</span></td>
			<td class="green center">218</td>
			<td class="red lasttd center">88</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11401137,0" class="icommentjs icon16" href="/linda-ronstadt-for-sentimental-reasons-2014-24-96-hd-flac-t11401137.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/linda-ronstadt-for-sentimental-reasons-2014-24-96-hd-flac-t11401137.html" class="cellMainLink">Linda Ronstadt - For Sentimental Reasons (2014) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="898260959">856.65 <span>MB</span></td>
			<td class="center">28</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T17:57:09+00:00">12 Oct 2015, 17:57:09</span></td>
			<td class="green center">150</td>
			<td class="red lasttd center">44</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11395799,0" class="icommentjs icon16" href="/va-the-look-of-love-the-burt-bacharach-collection-2001-flac-soup-t11395799.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-the-look-of-love-the-burt-bacharach-collection-2001-flac-soup-t11395799.html" class="cellMainLink">VA - The Look of Love the Burt Bacharach Collection (2001) FLAC Soup</a></div>
			</td>
			<td class="nobr center" data-sort="874082671">833.59 <span>MB</span></td>
			<td class="center">62</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T19:06:51+00:00">11 Oct 2015, 19:06:51</span></td>
			<td class="green center">125</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11398990,0" class="icommentjs icon16" href="/miles-davis-amandla-2011-24-192-hd-flac-t11398990.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/miles-davis-amandla-2011-24-192-hd-flac-t11398990.html" class="cellMainLink">Miles Davis - Amandla (2011) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1853835654">1.73 <span>GB</span></td>
			<td class="center">22</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T11:55:24+00:00">12 Oct 2015, 11:55:24</span></td>
			<td class="green center">113</td>
			<td class="red lasttd center">48</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11391059,0" class="icommentjs icon16" href="/canned-heat-living-the-blues-2014-24-192-hd-flac-t11391059.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/canned-heat-living-the-blues-2014-24-192-hd-flac-t11391059.html" class="cellMainLink">Canned Heat - Living The Blues (2014) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="3626208615">3.38 <span>GB</span></td>
			<td class="center">26</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-10T19:24:37+00:00">10 Oct 2015, 19:24:37</span></td>
			<td class="green center">95</td>
			<td class="red lasttd center">57</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11403339,0" class="icommentjs icon16" href="/bruce-springsteen-1984-08-05-brendan-byrne-arena-meadwolands-nj-flac-t11403339.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bruce-springsteen-1984-08-05-brendan-byrne-arena-meadwolands-nj-flac-t11403339.html" class="cellMainLink">Bruce Springsteen 1984-08-05 Brendan Byrne Arena, Meadwolands, NJ [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1412933847">1.32 <span>GB</span></td>
			<td class="center">31</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T04:21:09+00:00">13 Oct 2015, 04:21:09</span></td>
			<td class="green center">83</td>
			<td class="red lasttd center">50</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11399656,0" class="icommentjs icon16" href="/tom-jones-long-lost-suitcase-2015-flac-t11399656.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/tom-jones-long-lost-suitcase-2015-flac-t11399656.html" class="cellMainLink">Tom Jones - Long Lost Suitcase (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="266168637">253.84 <span>MB</span></td>
			<td class="center">24</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T12:52:56+00:00">12 Oct 2015, 12:52:56</span></td>
			<td class="green center">97</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11404221,0" class="icommentjs icon16" href="/keith-jarrett-creation-2015-24-48-hd-flac-t11404221.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/keith-jarrett-creation-2015-24-48-hd-flac-t11404221.html" class="cellMainLink">Keith Jarrett - Creation (2015) [24-48 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="653769439">623.48 <span>MB</span></td>
			<td class="center">26</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T07:56:27+00:00">13 Oct 2015, 07:56:27</span></td>
			<td class="green center">83</td>
			<td class="red lasttd center">37</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/toni-braxton-essential-mixes-2010-2015-extended-mix-flac-pirate-shovon-t11398770.html" class="cellMainLink">Toni Braxton - Essential Mixes [2010] [2015 Extended Mix] [FLAC] [Pirate Shovon]</a></div>
			</td>
			<td class="nobr center" data-sort="577008629">550.28 <span>MB</span></td>
			<td class="center">14</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-12T10:58:11+00:00">12 Oct 2015, 10:58:11</span></td>
			<td class="green center">84</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11388247,0" class="icommentjs icon16" href="/billie-holiday-last-recordings-2015-24-192-hd-flac-t11388247.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/billie-holiday-last-recordings-2015-24-192-hd-flac-t11388247.html" class="cellMainLink">Billie Holiday - Last Recordings (2015) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1931351895">1.8 <span>GB</span></td>
			<td class="center">30</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-10T08:38:15+00:00">10 Oct 2015, 08:38:15</span></td>
			<td class="green center">76</td>
			<td class="red lasttd center">33</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11393903,0" class="icommentjs icon16" href="/santana-festival-2014-24-96-hd-flac-t11393903.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/santana-festival-2014-24-96-hd-flac-t11393903.html" class="cellMainLink">Santana - Festival (2014) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="975334925">930.15 <span>MB</span></td>
			<td class="center">29</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T11:17:12+00:00">11 Oct 2015, 11:17:12</span></td>
			<td class="green center">75</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/julio-iglesias-mexico-2015-flac-t11393970.html" class="cellMainLink">Julio Iglesias - Mexico (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="346192150">330.15 <span>MB</span></td>
			<td class="center">24</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T11:39:02+00:00">11 Oct 2015, 11:39:02</span></td>
			<td class="green center">77</td>
			<td class="red lasttd center">13</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11395266,0" class="icommentjs icon16" href="/coheed-and-cambria-the-color-before-the-sun-2015-flac-sn3h1t87-glodls-t11395266.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/coheed-and-cambria-the-color-before-the-sun-2015-flac-sn3h1t87-glodls-t11395266.html" class="cellMainLink">Coheed and Cambria - The Color Before The Sun (2015) [FLAC] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="335612756">320.07 <span>MB</span></td>
			<td class="center">12</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-11T17:07:32+00:00">11 Oct 2015, 17:07:32</span></td>
			<td class="green center">74</td>
			<td class="red lasttd center">11</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11405656,0" class="icommentjs icon16" href="/v-a-mille-papaveri-rossi-le-canzoni-di-fabrizio-de-andrÃ©-flac-tntvillage-t11405656.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/v-a-mille-papaveri-rossi-le-canzoni-di-fabrizio-de-andrÃ©-flac-tntvillage-t11405656.html" class="cellMainLink">V.A. - Mille Papaveri Rossi Le canzoni di Fabrizio De AndrÃ© [Flac][TntVillage]</a></div>
			</td>
			<td class="nobr center" data-sort="819979507">781.99 <span>MB</span></td>
			<td class="center">51</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T14:12:37+00:00">13 Oct 2015, 14:12:37</span></td>
			<td class="green center">61</td>
			<td class="red lasttd center">35</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11404998,0" class="icommentjs icon16" href="/commodores-machine-gun-2015-24-192-hd-flac-t11404998.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/commodores-machine-gun-2015-24-192-hd-flac-t11404998.html" class="cellMainLink">Commodores - Machine Gun (2015) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1613594070">1.5 <span>GB</span></td>
			<td class="center">26</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-13T11:27:48+00:00">13 Oct 2015, 11:27:48</span></td>
			<td class="green center">59</td>
			<td class="red lasttd center">34</td>
        </tr>
			</table>


	<script type="text/javascript">
		if ($.cookie('kat_settings[fpw_column]') != null && $.cookie('kat_settings[fpw_order]')) {
			var colIndex = $.cookie('kat_settings[fpw_column]')
			var ascend = $.cookie('kat_settings[fpw_order]');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			sortTables(colIndex, ascend);
		}
		$('.frontPageWidget .firstr th .fpw-sort').click(function() {
			var colIndex = $(this).parent().index();
			var ascend = !$(this).hasClass('sortedAsc')?1:0;
			$('.fpw-sort').removeClass('sortedAsc sortedDesc');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			updateFPWsettings(colIndex, ascend);
			sortTables(colIndex, ascend);
		});
		function sortTables(colIndex, ascend) {	
			$('.frontPageWidget').each(function() {
				var data = [];
				$('tr:not(.firstr)', $(this)).each(function() {
					var name = $('td:first .cellMainLink', $(this)).text();
					var id = $('td:first .cellMainLink', $(this)).attr('href').match(/.*-t(\d+)\.html/)[1];
					var fileSize = $('td:eq(1)', $(this)).attr('data-sort');
					data.push({
						'col0':name, 'col1':padNum(fileSize), 'col2':padNum($('td:eq(2)',$(this)).text()), 'col3':padNum(id), 'col4':padNum($('td:eq(4)',$(this)).text()), 'col5':padNum($('td:eq(5)',$(this)).text()), 'html':$(this).html()
					});
				});
				data.sort(function(a, b) {
					var x = a['col'+colIndex].toLowerCase(); var y = b['col'+colIndex].toLowerCase();
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
				if (!ascend||ascend=='0') data.reverse();
				$(this).find('tr:not(.firstr)').remove();
				for (var i=0;i<data.length;i++) {
					$(this).append('<tr class="'+((i+1)%2==0?'even':'odd')+'">'+data[i].html+'</tr>');
				}
			});
		}
		function padNum(val) {
			val = '0000000000000000'+val;
			return val.substring(val.length-15);
		}
		function updateFPWsettings(t, o) {
			$.cookie('kat_settings[fpw_column]', t, { expires: 365, path: '/' });
			$.cookie('kat_settings[fpw_order]', o, { expires: 365, path: '/' });
		}
	</script>
		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
    
            
        <div id="_119b0a17fab5493361a252d04bf527db"></div>
    
                
    	    <div class="advertising">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <div id="_7063408f1c01d50e0dc2d833186ce962"></div>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/what-movie-did-you-last-watch-and-what-did-you-think-it-v-3/?unread=16993730">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What movie did you last watch? And what did you think of it? V.3!
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/Onchu/">Onchu</a></span></span> <time class="timeago" datetime="2015-10-14T00:00:35+00:00">14 Oct 2015, 00:00</time></span>
	</li>
		<li>
		<a href="/community/show/stitchinwitch-s-upload-page/?unread=16993729">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				StitchinWitch&#039;s upload page
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_verified"><a class="plain" href="/user/StitchinWitch/">StitchinWitch</a></span></span> <time class="timeago" datetime="2015-10-13T23:59:01+00:00">13 Oct 2015, 23:59</time></span>
	</li>
		<li>
		<a href="/community/show/my-first-torrent-please-post-here-and-help-me/?unread=16993728">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				My First Torrent - Please POST Here! and Help Me..
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/082775/">082775</a></span></span> <time class="timeago" datetime="2015-10-13T23:58:09+00:00">13 Oct 2015, 23:58</time></span>
	</li>
		<li>
		<a href="/community/show/what-movie-are-you-watching/?unread=16993726">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What movie are you watching?
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/ScotAT/">ScotAT</a></span></span> <time class="timeago" datetime="2015-10-13T23:56:24+00:00">13 Oct 2015, 23:56</time></span>
	</li>
		<li>
		<a href="/community/show/redbaron58-uploads/?unread=16993721">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				RedBaron58 Uploads
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_8"><a class="plain" href="/user/RedBaron58/">RedBaron58</a></span></span> <time class="timeago" datetime="2015-10-13T23:51:13+00:00">13 Oct 2015, 23:51</time></span>
	</li>
		<li>
		<a href="/community/show/za-la-thu-1215-heavy-metal/?unread=16993720">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				ZÄ LÃ¤ ThÃ¼ - 1215 Heavy Metal
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/082775/">082775</a></span></span> <time class="timeago" datetime="2015-10-13T23:51:05+00:00">13 Oct 2015, 23:51</time></span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/look-mama-i-m-popular/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Look, mama, I&#039;m popular!
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-05T17:42:40+00:00">05 Oct 2015, 17:42</time></span>
	</li>
	<li>
		<a href="/blog/post/summer-updates-september-1/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Summer updates [September, 1]
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-09-01T16:13:36+00:00">01 Sep 2015, 16:13</time></span>
	</li>
	<li>
		<a href="/blog/post/kickasstorrents-is-moving-to-kat-cr-domain/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				KickassTorrents is moving to kat.cr domain
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-04-24T10:13:38+00:00">24 Apr 2015, 10:13</time></span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/thLullaby/post/dear-anon/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Dear Anon..</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/thLullaby/">thLullaby</a> <time class="timeago" datetime="2015-10-13T11:56:09+00:00">13 Oct 2015, 11:56</time></span></li>
	<li><a href="/blog/Cerberus/post/the-superslab-taking-mother-shopping-and-the-sunset-hosted-for-katasaurus/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> The Superslab, taking mother shopping and the sunset. - hosted for Katasaurus</p></a><span class="explanation">by <a class="plain aclColor_8" href="/user/Cerberus/">Cerberus</a> <time class="timeago" datetime="2015-10-13T04:59:09+00:00">13 Oct 2015, 04:59</time></span></li>
	<li><a href="/blog/WarhoundOne/post/the-history-of-me-part-2/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> The History of Me...Part 2</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/WarhoundOne/">WarhoundOne</a> <time class="timeago" datetime="2015-10-12T20:55:47+00:00">12 Oct 2015, 20:55</time></span></li>
	<li><a href="/blog/ChillSpoon/post/kickass-torrents-is-illuminati/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Kickass Torrents is illuminati.</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/ChillSpoon/">ChillSpoon</a> <time class="timeago" datetime="2015-10-12T09:17:41+00:00">12 Oct 2015, 09:17</time></span></li>
	<li><a href="/blog/OptimusPr1me/post/happiness-murmur/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Happiness Murmur</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/OptimusPr1me/">OptimusPr1me</a> <time class="timeago" datetime="2015-10-12T07:26:49+00:00">12 Oct 2015, 07:26</time></span></li>
	<li><a href="/blog/vks39/post/1-year-celebration-with-kat-family-friends/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> 1 YEAR CELEBRATION WITH KAT FAMILY &amp; FRIENDS</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/vks39/">vks39</a> <time class="timeago" datetime="2015-10-11T06:32:22+00:00">11 Oct 2015, 06:32</time></span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/walk%20through%20hell/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				walk through hell
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/rosetta%20stone%20japanese/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				rosetta stone japanese
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/the%20witch/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				the witch
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/brooklyn%20nine-nine%20s03e01/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				brooklyn nine-nine s03e01
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/white%20iverson/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				white iverson
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/game%20of%20thrones%20s04e02/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				game of thrones s04e02
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/jarhead%202/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Jarhead 2
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/supernatural%20complete%20season%202/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				supernatural complete season 2
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/1939.battle.of.westerplatte.2013/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				1939.battle.of.westerplatte.2013
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/david%20gilmour/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				david gilmour
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/community%20season%201/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				community season 1
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		<li>
			<a data-nop href="http://www.torrentreactor.net/" target="_blank" rel="external">
				<span class="itorreact thirdPartIcons"></span>TorrentReactor
			</a>
		</li>
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a onclick="setLanguage('bsc', '.kat.cr');return false;" class="plain">Bosnian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a onclick="setLanguage('ko', '.kat.cr');return false;" class="plain">Korean</a></li>
                                <li><a onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a onclick="setLanguage('sl', '.kat.cr');return false;" class="plain">Slovenian</a></li>
                                <li><a onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
		<li><a href="/welcome/">welcome</a></li>
		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
        <li><a href="https://kastatus.com">KAT status</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <div id="_673e31f53f8166159b8e996c4124765b"></div>
        <div id="_e7050fb15fd39b3e4e99a5be4a57b6ea"></div>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script><script>
 sc('addGlobal', 'pagetype', 'front');
 sc('addSlot', '_60318cd4e8d28f6fb76fe34e9bd9c498');
sc('addSlot', '_39ecb76dd457e5ac33776fdf11500d56');
sc('addSlot', '_277923e5f9d753c5b0630c28e641790c');
sc('addSlot', '_119b0a17fab5493361a252d04bf527db');
sc('addSlot', '_7063408f1c01d50e0dc2d833186ce962', { 'searchQuery': '' });
sc('addSlot', '_673e31f53f8166159b8e996c4124765b');
sc('addSlot', '_e7050fb15fd39b3e4e99a5be4a57b6ea');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>

</body>
</html>
