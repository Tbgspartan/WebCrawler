



<!DOCTYPE html>

<!--[if IE]><![endif]--><html><head><meta charset="utf-8" /><meta http-equiv="Content-Type" content="text/html;charset=UTF-8" /><title>globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento</title><meta name="google-site-verification" content="BKmmuVQac1JM6sKlj3IoXQvffyIRJvJfbicMouA2a88" /><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.5" /><meta property="og:title" content="globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento"/><meta property="og:type" content="website"/><meta property="og:url" content="http://www.globo.com/"/><meta property="og:image" content="http://s.glbimg.com/en/ho/static/globocom2012/img/fb_marca.png"/><meta property="og:site_name" content="globo.com"/><meta property="og:description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta property="fb:page_id" content="224969370851736" /><meta property='busca:title' content='Globo.com' /><meta property='busca:species' content='Home' /><meta property='busca:issued' content='13/10/2015 23:07:10' /><meta property='busca:modified' content='13/10/2015 23:07:10' /><meta property='busca:publisher' content='www.globo.com' /><meta name="description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta name="keywords" content="NotÃ­cias, Entretenimento, Esporte, Tecnologia, Portal, ConteÃºdo, Rede Globo, TV Globo, VÃ­deos, TelevisÃ£o"/><meta name="application-name" content="Globo.com"/><meta name="msapplication-TileColor" content="#0669DE"/><meta name="msapplication-TileImage" content="http://s.glbimg.com/en/ho/static/globocom2012/img/globo-win-tile.png"/><link rel="canonical" href="http://www.globo.com/" /><link rel="shortcut icon" href="http://s.glbimg.com/en/ho/static/globocom2012/img/favicon.png" type="image/ico" /><link rel="apple-touch-icon-precomposed" sizes="57x57" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone.png" /><link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-ipad.png" /><link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone-retina.png" /><link rel="stylesheet" href="http://s.glbimg.com/en/ho/static/CACHE/css/8ec5a2c5d5e4.css" type="text/css" media="screen" /><!--[if lt IE 10]><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/d783f8b1c48f.js"></script><![endif]--><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/9137697c93da.js"></script></head><!--[if lt IE 7 ]><body class="ie ie6"><![endif]--><!--[if IE 7 ]><body class="ie ie7"><![endif]--><!--[if IE 8 ]><body class="ie ie8"><![endif]--><!--[if IE 9 ]><body class="ie ie9gt ie9"><![endif]--><!--[if gt IE 9 ]><body class="ie ie9gt ie10gt"><![endif]--><!--[if !IE]><!--><body><!--<![endif]--><div id="home-push-menu"><div class="home-push-menu-container"><div class="home-push-menu-initial"><div class="home-push-menu-header">NAVEGUE</div><ul class="barra-itens" data-analytics-context="Link Produto"><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://g1.globo.com" accesskey="n" class="barra-item-g1 link-produto analytics-area analytics-id-T">g1</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://globoesporte.globo.com" accesskey="e" class="barra-item-globoesporte link-produto analytics-area analytics-id-T">globoesporte</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://gshow.globo.com" accesskey="t" class="barra-item-gshow link-produto analytics-area analytics-id-T">gshow</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://famosos.globo.com" accesskey="t" class="barra-item-famosos-etc link-produto analytics-area analytics-id-T">famosos &amp; etc</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://techtudo.com.br" accesskey="b" class="barra-item-tech link-produto analytics-area analytics-id-T">tecnologia</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://video.globo.com" accesskey="v" class="barra-item-videos link-produto analytics-area analytics-id-T">vÃ­deos</a></li><li><a href="#" id="home-push-menu-show-email">e-mail</a><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="2.423,0 0,2.399 5.153,7.5 0,12.602 2.423,15 10,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-right.png"></image></svg></span></li><li><a href="https://meuperfil.globo.com/">central globo.com</a></li></ul></div><div class="home-push-menu-email"><ul><li><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="7.577,0 10,2.399 4.847,7.5 10,12.602 7.577,15 0,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-left.png"></image></svg></span><a id="home-push-menu-hide-email" href="#">e-mail</a></li><li><a href="https://login.globo.com/login/1948">globomail free</a></li><li><a href="https://login.globo.com/login/1">globomail pro</a></li></ul></div></div></div><div id="home-pagecontent" class=" home-menu-rendered" style=""><script type="text/javascript">
function comScore(C){var A="comScore",j=document,y=j.location,B="",z="undefined",x=2048,D,v,i,w,c="characterSet",l="defaultCharset",u=(typeof encodeURIComponent!=z?encodeURIComponent:escape);if(j.cookie.indexOf(A+"=")!=-1){i=j.cookie.split(";");for(w=0,y=i.length;w<y;w++){var d=i[w].indexOf(A+"=");if(d!=-1){B="&"+unescape(i[w].substring(d+A.length+1))}}}C=C+"&ns__t="+(new Date().getTime());C=C+"&ns_c="+(j[c]?j[c]:(j[l]?j[l]:""))+"&c8="+u(j.title)+B+"&c7="+u(y&&y.href?y.href:j.URL)+"&c9="+u(j.referrer);if(C.length>x&&C.indexOf("&")>0){D=C.substr(0,x-8).lastIndexOf("&");C=(C.substring(0,D)+"&ns_cut="+u(C.substring(D+1))).substr(0,x)}if(j.images){v=new Image();if(typeof ns_p==z){ns_p=v}v.src=C}else{j.write('<p><img src="'+C+'" height="1" width="1" alt="*"/></p>')}}comScore("http"+(document.location.href.charAt(4)=="s"?"s://sb":"://b")+".scorecardresearch.com/p?c1=2&c2=6035227");</script><noscript><p><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6035227&amp;c4=http%3A%2F%2Fwww.globo.com%2Findex.html" height="1" width="1" alt="*"/></p></noscript><script>
libby.loadScript('http://b.scorecardresearch.com/c2/6035227/cs.js');</script><script type="text/javascript">
var utag_data = {"structure_tree": "[\"globocom\"]", "ad_site_page": "{\"adUnit\":\"tvg_Globo.com.Home\", \"adPositions\":[\"banner_mobile_topo\",\"banner_mobile_meio\",\"banner_mobile_fim\",\"banner_slim_topo\",\"banner_slb_meio\",\"banner_slb_fim\",\"banner_selo4\",\"banner_floating\"]}", "page_name": "index"};</script><script type="text/javascript">
(function (a,b,c,d) {a = '//tags.globo.com/utag/globo/home/prod/utag.js';b=document;c = 'script';d=b.createElement(c);d.src=a;d.type = 'text/java' + c;d.async=true;a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);})();
</script><header><div id="base-container-width-element" class="container"><div class="header-mobile-top analytics-area analytics-id-H"><a id="open-menu" class="open-menu" href="#"><svg version="1.1" class="burger-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19px" height="13px" viewBox="0 0 19 13" enable-background="new 0 0 19 13" xml:space="preserve"><g><path fill="#1063E0" d="M17.493,5H1.485C0.665,5,0,5.665,0,6.485C0,7.305,0.665,7.97,1.485,7.97h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,5.665,18.313,5,17.493,5z M1.485,2.971h16.008c0.82,0,1.486-0.666,1.486-1.486C18.979,0.666,18.313,0,17.493,0H1.485C0.665,0,0,0.666,0,1.484C0,2.305,0.665,2.971,1.485,2.971z M17.493,10H1.485C0.665,10,0,10.666,0,11.485c0,0.82,0.665,1.485,1.485,1.485h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,10.666,18.313,10,17.493,10z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/menu-button.png"></image></svg></a><a class="logo-globo analytics-area analytics-id-T" href="http://globo.com" title="globo.com"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 98 22" enable-background="new 0 0 98 22" xml:space="preserve"><g><path fill="#1063E0" d="M92.982,5.033c-1.115,0-2.549,0.601-3.459,1.625c-0.865-1.069-2.026-1.625-3.392-1.625c-1.205,0-2.252,0.489-3.049,1.336c-0.114-0.757-0.661-1.203-1.502-1.203c-0.957,0-1.639,0.712-1.639,1.781v8.551c0,1.069,0.682,1.781,1.639,1.781s1.638-0.712,1.638-1.781V9.909c0-1.292,0.592-2.205,1.936-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.682,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.909c0-1.292,0.728-2.205,1.934-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.683,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.352C97.466,6.547,95.645,5.033,92.982,5.033z M10.036,5.166c-0.91,0-1.434,0.49-1.639,1.336c-0.614-0.98-1.889-1.469-3.004-1.469C2.093,5.033,0,8.039,0,11.268c0,3.184,1.616,6.146,5.052,6.146c1.502,0,2.617-0.535,3.299-1.515h0.045v0.757c0,1.625-1.183,2.538-2.776,2.538c-1.889,0-3.049-1.001-4.097-1.001c-0.637,0-1.319,0.8-1.319,1.403C0.205,21.354,4.028,22,5.371,22c3.732,0,6.304-1.938,6.304-5.791V6.947C11.674,5.878,10.991,5.166,10.036,5.166z M5.826,14.474c-1.707,0-2.549-1.559-2.549-3.273c0-1.492,0.842-3.229,2.549-3.229c1.797,0,2.571,1.781,2.571,3.339C8.397,12.871,7.578,14.474,5.826,14.474z M14.318,0c-0.956,0-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781s1.639-0.712,1.639-1.781V1.782C15.957,0.712,15.274,0,14.318,0z M22.479,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169c3.527,0,5.872-2.829,5.872-6.169C28.35,7.994,25.938,5.033,22.479,5.033z M22.479,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.797-3.273,2.594-3.273c1.798,0,2.594,1.737,2.594,3.273C25.073,12.782,24.254,14.474,22.479,14.474z M35.371,5.033c-1.138,0-2.253,0.378-3.118,1.113V1.782C32.253,0.712,31.57,0,30.614,0s-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781c0.911,0,1.434-0.49,1.639-1.336c0.614,0.979,1.889,1.471,3.004,1.471c3.3,0,5.393-3.008,5.393-6.036C40.65,8.261,38.897,5.033,35.371,5.033z M34.825,14.474c-1.798,0-2.572-1.781-2.572-3.34c0-1.559,0.819-3.162,2.572-3.162c1.707,0,2.548,1.759,2.548,3.273C37.373,12.737,36.531,14.474,34.825,14.474z M46.863,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169s5.872-2.829,5.872-6.169C52.735,7.994,50.322,5.033,46.863,5.033z M46.863,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273s2.594,1.737,2.594,3.273C49.458,12.782,48.638,14.474,46.863,14.474z M55.425,13.939c-0.978,0-1.774,0.779-1.774,1.736s0.796,1.738,1.774,1.738c0.979,0,1.775-0.781,1.775-1.738S56.404,13.939,55.425,13.939z M66.583,13.805c-0.386,0-1.616,0.669-2.436,0.669c-1.774,0-2.753-1.648-2.753-3.229c0-1.648,0.888-3.273,2.753-3.273c1.048,0,1.776,0.623,2.594,0.623c0.775,0,1.253-0.868,1.253-1.536c0-1.56-2.709-2.026-4.006-2.026c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.343,6.169,5.871,6.169c1.184,0,4.006-0.558,4.006-2.206C67.994,14.451,67.355,13.805,66.583,13.805z M73.446,5.033c-3.459,0-5.872,2.961-5.872,6.212c0,3.34,2.344,6.169,5.872,6.169c3.527,0,5.871-2.829,5.871-6.169C79.317,7.994,76.904,5.033,73.446,5.033z M73.446,14.474c-1.774,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273c1.797,0,2.594,1.737,2.594,3.273C76.04,12.782,75.221,14.474,73.446,14.474z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/logo.png"></image></svg></a><form id="search-form" action="http://www.globo.com/busca/" method="get"><div id="search-container" class="search-container"><span><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="12px" height="12px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><g><path fill="#999999" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/search-small.png"></image></svg></span><div class="search-internal-container"><input type="text" id="search-globo" name="q" placeholder="encontre na globo.com"></div><a id="close-search" class="close-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="11px" height="11px" viewBox="0 0 11 11" enable-background="new 0 0 11 11" xml:space="preserve"><g><path fill="#666766" d="M6.884,5.501c1.955-1.955,3.733-3.732,3.809-3.808c0.388-0.388,0.388-1.015,0-1.403 c-0.387-0.386-1.015-0.386-1.402,0C9.215,0.365,7.437,2.144,5.482,4.099c-1.947-1.947-3.714-3.714-3.79-3.79 c-0.387-0.388-1.015-0.388-1.402,0c-0.387,0.387-0.387,1.016,0,1.401c0.076,0.076,1.842,1.843,3.79,3.791 C2.135,7.446,0.372,9.209,0.297,9.285c-0.387,0.387-0.387,1.015,0,1.401c0.387,0.389,1.015,0.389,1.402,0 c0.075-0.075,1.838-1.838,3.784-3.783c1.953,1.952,3.727,3.727,3.802,3.802c0.387,0.387,1.015,0.387,1.402,0 c0.387-0.387,0.387-1.015,0-1.402C10.611,9.228,8.837,7.454,6.884,5.501z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/close.png"></image></svg></a></div><a id="open-search" class="open-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><path fill="#1063e0" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path><image src="http://s.glbimg.com/gl/ba/img/common/search.png"></image></svg></a></form></div><h1 class="analytics-area analytics-id-H"><a class="logo-topo analytics-area analytics-id-T" href="/" title="globo.com">
                globo.com
            </a></h1><div class="complementos analytics-area analytics-id-T"><div id="busca-padrao"><form action="http://www.globo.com/busca/" method="get" accept-charset="utf-8"><fieldset><legend>buscar</legend><label for="busca-campo" class="label-for-search">buscar</label><input id="busca-campo" type="text" name="q" autocomplete="off" accesskey="s" lang="pt-BR" x-webkit-speech speech /><button type="submit">buscar</button></fieldset></form></div><div id="libby-box-previsao-tempo" class="analytics-area analytics-id-L"></div></div><nav><div class="spacer"></div><header id="header-produto" class="header-navegacao header-home" data-analytics="T"><div id="glbbarrawidget"></div><div class="floating-bar"><div class="header-bar"><div class="grid-12 clearfix area-principal row"><div class="column"><div class="menu-area to-left" data-analytics="S"><div class="menu-button"><div class="burger"><b></b><b></b><b></b></div><span class="menu-label">MENU</span></div><span class="menu-produto"></span></div><a class="logo-area" href="http://www.globo.com/"><h1 class="logo icon-produto">Home</h1></a><div class="to-right"><div class="search-area"><form id="frmBuscaScroll" action="#"><input id="search-input-floating" type="text" name="q" placeholder="BUSCAR" class="search" /><label for="search-input-floating" class="glass-container"><div class="glass"><div class="iglass css3pie"></div></div></label></form></div></div></div></div></div></div><div class="cube-container"><div class="cube"><div class="face front"><div class="grid-12 clearfix area-principal row"><div class="column"><div class="menu-area to-left" data-analytics="S"><div class="menu-button"><div class="burger"><b></b><b></b><b></b></div><span class="menu-label">MENU</span></div><span class="menu-produto"></span></div><a class="logo-area" href="http://www.globo.com/"><h1 class="logo icon-produto">Home</h1></a><div class="to-right"><div class="search-area"><form id="frmBuscaScroll" action="#"><input id="search-input-top" type="text" name="q" placeholder="BUSCAR" class="search" /><label for="search-input-top" class="glass-container"><div class="glass"><div class="iglass css3pie"></div></div></label></form></div><div id="busca-padrao"></div></div></div></div></div></div></div></header><script>
(function(window,document) {try{new CustomEvent("test");} catch(e){var CustomEvent=function(event,params) {var evt;params=params || {bubbles: false,
cancelable: false,
detail: undefined};evt=document.createEvent("CustomEvent");evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);return evt;};
CustomEvent.prototype=window.Event.prototype;window.CustomEvent=CustomEvent;}
document.dispatchEvent(new CustomEvent('glb.headerDom.ready'));})(window,document);</script><style type="text/css">
    /*
       evita que o markup do menu seja exibido sem estilo enquanto
       ele nÃ£o foi carregado
    */
    #menu-container {
        display: none;
    }
    #menu-fonts {
        height: 1px;
        width: 1px;
        color: transparent;
        overflow: hidden;
        position: absolute;
    }
</style>

<nav id="menu-container" class="regua-navegacao-tab menu-no-animation">
    

<script id="menu-carousel-template" type="x-tmpl-mustache">

<div id="menu-carousel-header-sizes-infos" class="menu-carousel-header-sizes-infos">
</div>
<div id="menu-carousel-header" class="menu-carousel menu-carousel-header swiper-container">
    <ul class="menu-brands swiper-wrapper">
        {{#portals}}
        <li data-produto="{{name}}" class="menu-carousel-item menu-carousel-{{name}} {{#active}}menu-carousel-item-active{{/active}} swiper-slide">
            <a href="{{link}}" class="menu-carousel-link" data-index="{{index}}">
                <svg class="brand-icon">
                    <use class="menu-logo" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#eixo-icone-{{name}}"></use>
                </svg>
            </a>
            <div class="menu-carousel-link-shadow"></div>
        </li>
        {{/portals}}
    </ul>
</div>
<div id="menu-carousel-body" class="menu-carousel menu-carousel-body swiper-container">
    <ul class="menu-brands swiper-wrapper">
        {{#portals}}
        <li data-produto="{{name}}" class="menu-carousel-item menu-carousel-{{name}} {{#active}}menu-carousel-item-active{{/active}} swiper-slide">
            <div id="menu-rounder" class="menu-rounder">{{{menu}}}</div>
        </li>
        {{/portals}}
    </ul>
</div>

</script>

<script id="menu-rounder-template" type="x-tmpl-mustache">

  <div id="menu-cascade" class="menu-cascade" data-menu-index="{{index}}">
    <ul class="menu-root menu-level menu-level-0">{{#items}}{{>recursive_partial}}{{/items}}</ul>
  </div>
  <div id="menu-addon" class="menu-addon"></div>

</script>

<script id="menu-custom-template" type="x-tmpl-mustache">

    {{#items}}{{>recursive_partial}}{{/items}}

</script>


<script id="menu-dinamico-template" type="x-tmpl-mustache">

<li id="menu-{{deepness}}-{{tituloSlug}}" class="menu-item {{#separador}} menu-item-separator{{/separador}}{{#destacado}} menu-item-highlighted{{/destacado}}{{#agrupador}} menu-item-grouped{{/agrupador}}{{#hasChildren}} is-father{{/hasChildren}}{{#hasSubmenuBroken}} menu-item-submenu-broken{{/hasSubmenuBroken}}">

    {{#shouldBeLink}}<a href="{{link}}" class="menu-item-link">{{/shouldBeLink}}
    {{^shouldBeLink}}<span class="menu-item-link">{{/shouldBeLink}}
        <span class="menu-item-title">{{titulo}}<span class="menu-item-arrow"><svg class="regua-navegacao-icon"><use xlink:href="#regua-arrow" /></svg></span></span>
    {{#shouldBeLink}}</a>{{/shouldBeLink}}
    {{^shouldBeLink}}</span>{{/shouldBeLink}}

    {{#hasChildren}}
        <div class="menu-level menu-submenu menu-submenu-level{{deepness}}{{#hasSubmenuBroken}} menu-item-submenu-broken{{/hasSubmenuBroken}} menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back"><span class="menu-item-arrow"><svg class="regua-navegacao-icon"><use xlink:href="#regua-arrow" /></svg></span><span class="regua-navegacao-menu-item">{{titulo}}</span></a>
            <ul class="menu-submenu-vertical-scroll">
            {{#children}}
                {{>recursive_partial}}
            {{/children}}
            <ul>
        </div>
    {{/hasChildren}}
</li>

</script>


    <div id="menu-rounder" class="menu-rounder">
        <div id="menu-cascade" class="menu-cascade" data-menu-index="0">
            

<ul class="menu-root menu-level menu-level-0">
    
        




<li class="menu-item is-father " id="menu-1-famosos">

    <a class="menu-item-link">
        <span class="menu-item-title">Famosos</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Famosos</a>
            
                

                




<li class="menu-item " id="menu-2-ego">

    <a href="http://ego.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-paparazzo">

    <a href="http://ego.globo.com/paparazzo/index.html" class="menu-item-link">
        <span class="menu-item-title">Paparazzo</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-patricia-kogut">

    <a href="http://kogut.oglobo.globo.com/noticias-da-tv/index.html" class="menu-item-link">
        <span class="menu-item-title">Patricia Kogut</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-quem">

    <a href="http://revistaquem.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Quem</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-retratos-da-vida">

    <a href="http://extra.globo.com/famosos/" class="menu-item-link">
        <span class="menu-item-title">Retratos da Vida</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-moda-estilo">

    <a class="menu-item-link">
        <span class="menu-item-title">Moda &amp; Estilo</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Moda &amp; Estilo</a>
            
                

                




<li class="menu-item " id="menu-2-ela-no-o-globo">

    <a href="http://ela.oglobo.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Ela no O Globo</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-estilo-no-gshow">

    <a href="http://gshow.globo.com/Estilo/" class="menu-item-link">
        <span class="menu-item-title">Estilo no Gshow</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-moda-no-ego">

    <a href="http://ego.globo.com/moda/index.html" class="menu-item-link">
        <span class="menu-item-title">Moda no Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-moda-no-gnt">

    <a href="http://gnt.globo.com/moda/" class="menu-item-link">
        <span class="menu-item-title">Moda no GNT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-glamour">

    <a href="http://revistaglamour.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Glamour</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-gq-brasil">

    <a href="http://gq.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista GQ Brasil</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-marie-claire">

    <a href="http://revistamarieclaire.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Marie Claire</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-vogue">

    <a href="http://vogue.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Vogue</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-bem-estar-saude">

    <a class="menu-item-link">
        <span class="menu-item-title">Bem-Estar &amp; SaÃºde</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Bem-Estar &amp; SaÃºde</a>
            
                

                




<li class="menu-item " id="menu-2-beleza-no-ego">

    <a href="http://ego.globo.com/beleza/index.html" class="menu-item-link">
        <span class="menu-item-title">Beleza no Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-beleza-no-gnt">

    <a href="http://gnt.globo.com/beleza/" class="menu-item-link">
        <span class="menu-item-title">Beleza no GNT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-beleza-no-gshow">

    <a href="http://gshow.globo.com/Estilo/Beleza/" class="menu-item-link">
        <span class="menu-item-title">Beleza no Gshow</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-bem-estar-no-g1">

    <a href="http://g1.globo.com/bemestar/" class="menu-item-link">
        <span class="menu-item-title">Bem Estar no G1</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-bem-estar-no-gnt">

    <a href="http://gnt.com.br/bemestar" class="menu-item-link">
        <span class="menu-item-title">Bem Estar no GNT </span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-horoscopo-no-ego">

    <a href="http://horoscopo.ego.globo.com/" class="menu-item-link">
        <span class="menu-item-title">HorÃ³scopo no Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-maes-no-gnt">

    <a href="http://gnt.globo.com/maes-e-filhos/" class="menu-item-link">
        <span class="menu-item-title">MÃ£es no GNT </span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-meus-5-minutos">

    <a href="http://meus5minutos.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Meus 5 Minutos</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-crescer">

    <a href="http://revistacrescer.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Crescer</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-casa-decoracao">

    <a class="menu-item-link">
        <span class="menu-item-title">Casa &amp; DecoraÃ§Ã£o</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Casa &amp; DecoraÃ§Ã£o</a>
            
                

                




<li class="menu-item " id="menu-2-casa-no-gnt">

    <a href="http://gnt.globo.com/casa-e-decoracao/" class="menu-item-link">
        <span class="menu-item-title">Casa no GNT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-casa-vogue">

    <a href="http://casavogue.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Casa Vogue</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-casa-e-jardim">

    <a href="http://revistacasaejardim.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Casa e Jardim</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-do-zap">

    <a href="http://revista.zapimoveis.com.br/?utm_source=globo.com-etc&amp;utm_medium=botao-casa-decoracao&amp;utm_campaign=revista-imoveis" class="menu-item-link">
        <span class="menu-item-title">Revista do Zap</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-gastronomia">

    <a class="menu-item-link">
        <span class="menu-item-title">Gastronomia</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Gastronomia</a>
            
                

                




<li class="menu-item " id="menu-2-casa-e-comida">

    <a href="http://revistacasaejardim.globo.com/Casa-e-Comida/" class="menu-item-link">
        <span class="menu-item-title">Casa e Comida</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-colheradas">

    <a href="http://meus5minutos.globo.com/blogs/Colheradas/" class="menu-item-link">
        <span class="menu-item-title">Colheradas</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-receitas-da-ana-maria">

    <a href="http://gshow.globo.com/programas/mais-voce/Receitas-da-Ana-Maria/" class="menu-item-link">
        <span class="menu-item-title">Receitas da Ana Maria</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-receitascom">

    <a href="http://gshow.globo.com/receitas/" class="menu-item-link">
        <span class="menu-item-title">Receitas.com</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-receitas-no-gnt">

    <a href="http://gnt.globo.com/receitas/" class="menu-item-link">
        <span class="menu-item-title">Receitas no GNT</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-musica">

    <a class="menu-item-link">
        <span class="menu-item-title">MÃºsica</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">MÃºsica</a>
            
                

                




<li class="menu-item " id="menu-2-altas-horas">

    <a href=" http://gshow.globo.com/programas/altas-horas/" class="menu-item-link">
        <span class="menu-item-title">Altas Horas </span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-g1-musica">

    <a href="http://g1.globo.com/musica/" class="menu-item-link">
        <span class="menu-item-title">G1 MÃºsica</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-gshow-musica">

    <a href="http://gshow.globo.com/Musica/" class="menu-item-link">
        <span class="menu-item-title">Gshow MÃºsica</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-musica-no-multishow">

    <a href="http://multishow.globo.com/musica/" class="menu-item-link">
        <span class="menu-item-title">MÃºsica no Multishow</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-musicacombr">

    <a href="http://musica.com.br/" class="menu-item-link">
        <span class="menu-item-title">MÃºsica.com.br</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-radiobeat">

    <a href="http://radiobeat.com.br/" class="menu-item-link">
        <span class="menu-item-title">RADIOBEAT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-som-brasil">

    <a href="http://globotv.globo.com/rede-globo/som-brasil/" class="menu-item-link">
        <span class="menu-item-title">Som Brasil</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-cinema-teatro">

    <a class="menu-item-link">
        <span class="menu-item-title">Cinema &amp; Teatro</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Cinema &amp; Teatro</a>
            
                

                




<li class="menu-item " id="menu-2-canal-brasil">

    <a href="http://canalbrasil.globo.com/ " class="menu-item-link">
        <span class="menu-item-title">Canal Brasil</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-canal-universal">

    <a href="http://universal.globo.com" class="menu-item-link">
        <span class="menu-item-title">Canal Universal</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-filmes-na-globo">

    <a href="http://redeglobo.globo.com/filmes/index.html" class="menu-item-link">
        <span class="menu-item-title">Filmes na Globo</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-g1-cinema">

    <a href="http://g1.globo.com/pop-arte/cinema/" class="menu-item-link">
        <span class="menu-item-title">G1 Cinema</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-globo-filmes">

    <a href=" http://globofilmes.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Globo Filmes</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-megapix">

    <a href="http://megapix.globo.com/" class="menu-item-link">
        <span class="menu-item-title">MegaPix</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-monet">

    <a href="http://revistamonet.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Monet</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-teatro">

    <a href="http://www.agentesevenoteatro.com.br/" class="menu-item-link">
        <span class="menu-item-title">Teatro</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-telecine">

    <a href="http://telecine.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Telecine</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item menu-item-highlighted " id="menu-1-ego">

    <a href="http://ego.globo.com/" class="menu-item-link">
        <span class="menu-item-title">ego</span>
    </a>

    
</li>

    
        




<li class="menu-item menu-item-highlighted " id="menu-1-quem">

    <a href="http://revistaquem.globo.com/" class="menu-item-link">
        <span class="menu-item-title">quem</span>
    </a>

    
</li>

    
        




<li class="menu-item menu-item-highlighted " id="menu-1-patricia-kogut">

    <a href="http://kogut.oglobo.globo.com/" class="menu-item-link">
        <span class="menu-item-title">patricia kogut</span>
    </a>

    
</li>

    
</ul>

        </div>
        <div id="menu-addon" class="menu-addon"></div>
    </div>

    
</nav>

<div id="menu-fonts">
    
    
    <span class="open-sans">BESbswy</span>
    <span class="open-sans-bold">BESbswy</span>
    <span class="open-sans-light">BESbswy</span>
    <span class="roboto-slab">BESbswy</span>
    <span class="proximanova-semibold">BESbswy</span>
</div>

<div id="menu-content-overlay"></div>
<script>
(function (window) {window.REGUA_SETTINGS=window.REGUA_SETTINGS || {};window.REGUA_SETTINGS.portalName = "home";window.REGUA_SETTINGS.portalHome = "http://www.globo.com/";window.REGUA_SETTINGS.portalsList = ["home","g1","ge","gshow","famosos","techtudo","globotv","globosatplay"];window.REGUA_SETTINGS.portalsLinkList = ["http://www.globo.com/","http://g1.globo.com/index.html","http://globoesporte.globo.com/","http://gshow.globo.com/","http://famosos.globo.com/","http://www.techtudo.com.br/","http://globotv.globo.com/","http://globosatplay.globo.com/"];window.REGUA_SETTINGS.staticUrl = "http://s.glbimg.com/en/ho/static/";window.REGUA_SETTINGS.svgSpriteName = "sprite-1f2df10dde.svg";window.REGUA_SETTINGS.suggestUrl = "";})(window);
</script><script>(function(){var e,t,n,o;e=function(){function e(){this.name="Android",this.bridge=window.GloboBridge||{navigated:function(e){},menuAction:function(e){},reguaAction:function(e){}}}return e.prototype.navigateTo=function(e){this.bridge.navigated(JSON.stringify(e))},e.prototype.menuAction=function(e){this.bridge.menuAction(JSON.stringify(e))},e.prototype.reguaAction=function(e){this.bridge.reguaAction(JSON.stringify(e))},e}(),t=function(){function e(){this.name="iOS",this.connectWebViewJavascriptBridge(function(e){return e.init(function(e,t){return window.glb.masterApp.manageMessage(e)})})}return e.prototype.connectWebViewJavascriptBridge=function(e){window.WebViewJavascriptBridge?e(WebViewJavascriptBridge):document.addEventListener("WebViewJavascriptBridgeReady",function(){e(WebViewJavascriptBridge)},!1)},e.prototype.brigdeSendJson=function(e){return this.connectWebViewJavascriptBridge(function(t){t.send(JSON.stringify(e))})},e.prototype.navigateTo=function(e){this.brigdeSendJson(e)},e.prototype.menuAction=function(e){this.brigdeSendJson(e)},e.prototype.reguaAction=function(e){this.brigdeSendJson(e)},e}(),n=function(){function n(e){this.platformApp=e,this.initCalled=!1,null!=this.platformApp&&this.bind()}return n.prototype.init=function(){this.initCalled||(this.initCalled=!0,this.header&&(this.processHeaderInfo(),this.bindAfterInit()))},n.prototype.processHeaderInfo=function(){this.countHeaderTries=0,this.maxHeaderTries=100,this.headerCubeContainer=this.header.querySelector(".cube-container"),this.headerCubeContainer?this.processHeaderInfoAction():setTimeout(function(e){return function(){return e.processHeaderInfoAction()}}(this),1)},n.prototype.processHeaderInfoAction=function(){var e;if(this.countHeaderTries++,this.headerCubeContainer=this.header.querySelector(".cube-container"),this.headerCubeContainer)this.cacheVariables(),e=this.getHeaderData(),this.navigateTo(e);else{if(this.countHeaderTries>this.maxHeaderTries)return void console.log("Nao achou o markup interno do header");setTimeout(function(e){return function(){return e.processHeaderInfoAction()}}(this),1)}},n.prototype.bind=function(){var e;return this.bindAppEvents(),this.header=document.querySelector("#header-produto"),e=function(e){return function(){return e.header||(e.header=document.querySelector("#header-produto")),e.init()}}(this),this.header?this.init():(document.addEventListener("glb.headerDom.ready",e),document.addEventListener("DOMContentLoaded",e))},n.prototype.cacheVariables=function(){return this.elements={},this.elements.header=this.header,this.elements.headerCubeContainer=this.headerCubeContainer,this.elements.headerFront=this.elements.headerCubeContainer.querySelector(".front"),this.elements.headerLogoProduto=this.elements.headerCubeContainer.querySelector(".logo-produto-container"),this.elements.headerLogoProdutoLink=this.elements.headerLogoProduto?this.elements.headerLogoProduto.parentNode:null,this.elements.headerLogoArea=this.elements.headerCubeContainer.querySelector(".logo-area .logo"),this.elements.headerLogoAreaLink=this.elements.headerLogoArea.parentNode,this.elements.headerSubeditoria=this.elements.headerFront.querySelector(".menu-subeditoria"),this.elements.headerSubeditoriaLink=this.elements.headerSubeditoria?this.elements.headerSubeditoria.querySelector("a"):null,!0},n.prototype.bindAppEvents=function(){document.addEventListener("glb.menu-carousel.ready",function(e){return function(){return e.platformApp.menuAction({action:"menuReady"})}}(this)),document.addEventListener("glb.menu-carousel.shown.before",function(e){return function(){return e.platformApp.menuAction({action:"menuShownBefore"})}}(this)),document.addEventListener("glb.menu-carousel.shown.after",function(e){return function(){return e.platformApp.menuAction({action:"menuShownAfter"})}}(this)),document.addEventListener("glb.menu-carousel.off.before",function(e){return function(){return e.platformApp.menuAction({action:"menuOffBefore"})}}(this)),document.addEventListener("glb.menu-carousel.off.after",function(e){return function(){return e.platformApp.menuAction({action:"menuOffAfter"})}}(this)),document.addEventListener("glb.regua.exists",function(e){return function(){return e.platformApp.reguaAction({action:"reguaExists"})}}(this))},n.prototype.bindAfterInit=function(){},n.prototype._getText=function(e){return e?(e.innerText||e.textContent).trim()||"":""},n.prototype._getHref=function(e){return e?e.getAttribute("href")||"":""},n.prototype._getCssProp=function(e,t){var n;return e?(n=window.getComputedStyle(e),n.getPropertyValue(t)||""):""},n.prototype._getBackgroundColor=function(e){return this._getCssProp(e,"background-color")},n.prototype.getHeaderData=function(){var e;return e={color:this._getBackgroundColor(this.elements.headerFront),editoria:"",editoriaHref:"",subeditoria:"",subeditoriaHref:"",logo:"",logoHref:""},this.elements.headerLogoProduto?(e.logo=this._getText(this.elements.headerLogoProduto),e.logoHref=this._getHref(this.elements.headerLogoProdutoLink),e.editoria=this._getText(this.elements.headerLogoArea),e.editoriaHref=this._getHref(this.elements.headerLogoArea),this.elements.headerSubeditoria&&(e.subeditoria=this._getText(this.elements.headerSubeditoria),e.subeditoriaHref=this._getHref(this.elements.headerSubeditoriaLink))):(e.logo=this._getText(this.elements.headerLogoArea),e.logoHref=this._getHref(this.elements.headerLogoAreaLink)),e},n.prototype.navigateTo=function(e){e.action="navigateTo",e.url=window.location.href,this.platformApp?this.platformApp.navigateTo(e):console.log("Sem Plataforma")},n.prototype.manageMessage=function(e){var t,n;return e&&(t=JSON.parse(e),t.action="regua-change")?(n=t.rulerItem||"home",null!=window.$?$(document).trigger("glb.regua.change",[n]):console.log("Sem jQuery")):void 0},n.prototype.getUserAgent=function(){return window.getUserAgent?window.getUserAgent():window.navigator.userAgent},n.prototype.getPlatformApp=function(){var n;return n=this.getUserAgent(),n.match(/(iPad|iPhone|iPod)/g)?new t:n.match(/(Android)/g)?new e:void 0},n}(),window.glb=window.glb||{},window.glb.MasterApp=n,window.glb.AndroidApp=e,window.glb.IosApp=t,window.initMasterApp=function(){var e;if(!window.glb.masterApp)return e=n.prototype.getPlatformApp(),window.glb.masterApp=new n(e)},o=null!=window.noAutoLoadReguaNavegacao&&window.noAutoLoadReguaNavegacao,o||window.initMasterApp()}).call(this);</script><script>(function(){var n,o,e,t;e=function(){return/MobApp_Android|MobApp_iOS|MobApp_WP/},o=function(){return window.getUserAgent?window.getUserAgent():window.navigator.userAgent},n=function(){var n,t,a,i,r;i=o(),r=i.split(" ");for(a in r)if(n=r[a],e().test(n))return t=n.split("/"),{name:t[0].replace("_"," "),version:t[1],frameworkVersion:t[2]};return null},window.addGlbOnAppClass=function(){var o;window.glb=window.glb||{},window.glb.extractAppFromUserAgent||(window.glb.extractAppFromUserAgent=n,window.glb.nativeAppInfo=n(),null!==window.glb.nativeAppInfo&&(window.glb.nativeAppInfo.frameworkVersion?(o=parseInt(window.glb.nativeAppInfo.frameworkVersion,10),o>=1&&(document.documentElement.className+=" glb-on-app")):document.documentElement.className+=" glb-on-app-comnio"))},t=null!=window.noAutoLoadReguaNavegacao&&window.noAutoLoadReguaNavegacao,t||window.addGlbOnAppClass()}).call(this);</script><script>(function(){var n,o,e,i,t,w,d,a,r,s,u,l;try{new CustomEvent("test")}catch(c){e=c,n=function(n,o){var e;return e=void 0,o=o||{bubbles:!1,cancelable:!1,detail:void 0},e=document.createEvent("CustomEvent"),e.initCustomEvent(n,o.bubbles,o.cancelable,o.detail),e},n.prototype=window.Event.prototype,window.CustomEvent=n}r=function(n,o){var e;return e=new RegExp("(?:^|\\s)"+o+"(?!\\S)"),!!n.className.match(e)},o=function(n,o){r(n,o)||(n.className+=" "+o)},u=function(n,o){var e;r(n,o)&&(e=new RegExp("(?:^|\\s)"+o+"(?!\\S)","g"),n.className=n.className.replace(e,""))},t=function(){return window.myInnerWidth||window.innerWidth},i=function(){return window.myInnerHeight||window.innerHeight},w=function(){var n;return null!=window.isAndroidBrowser?window.isAndroidBrowser:(n=navigator.userAgent,window.isAndroidBrowser=n.indexOf("Mozilla/5.0")>-1&&n.indexOf("Android ")>-1&&n.indexOf("AppleWebKit")>-1&&-1===n.indexOf("Chrome"),window.isAndroidBrowser)},d=function(){return null!=window.isPortrait?window.isPortrait:(window.isPortrait=t()<=i(),window.isPortrait)},a=function(){return null!=window.isTouchable?window.isTouchable:(window.isTouchable="ontouchstart"in window||navigator.msMaxTouchPoints||window.DocumentTouch&&document instanceof window.DocumentTouch,window.isTouchable)},s=function(){var n;return window.REGUAMAXWIDTHPORTRAIT=window.REGUAMAXWIDTHPORTRAIT||640,window.REGUAMAXWIDTHLANDSCAPE=window.REGUAMAXWIDTHLANDSCAPE||767,window.isPortrait=d(),window.isTouchable=a(),window.isAndroidBrowser=w(),n=t(),(window.isPortrait&&n<=window.REGUAMAXWIDTHPORTRAIT||!window.isPortrait&&n<=window.REGUAMAXWIDTHLANDSCAPE)&&!window.isAndroidBrowser&&document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image","1.1")&&(null==window.glb.nativeAppInfo||window.glb.nativeAppInfo.frameworkVersion)},window.glb=window.glb||{},window.glb.addClass=o,window.glb.removeClass=u,window.addHasReguaClass=function(){window.glb.reguaShouldStart||(window.glb.reguaShouldStart=s,window.glb.reguaShouldStart()?(window.glb.addClass(document.documentElement,"has-regua"),window.glb.addClass(document.documentElement,"svg-support"),window.glb.removeClass(document.documentElement,"has-not-regua"),document.dispatchEvent(new CustomEvent("glb.regua.exists"))):(window.glb.removeClass(document.documentElement,"has-regua"),window.glb.removeClass(document.documentElement,"svg-support"),window.glb.addClass(document.documentElement,"has-not-regua")))},l=null!=window.noAutoLoadReguaNavegacao&&window.noAutoLoadReguaNavegacao,l||window.addHasReguaClass()}).call(this);</script><nav id="regua-navegacao" class="regua-navegacao "><div id="regua-svg-container" class="regua-svg-container"><svg xmlns="http://www.w3.org/2000/svg"><symbol id="eixo-icone-busca" viewBox="0 0 24 24"><path d="M21.67 20.27l-5.72-5.73c.98-1.26 1.55-2.83 1.55-4.54 0-4.14-3.36-7.5-7.5-7.5-4.15 0-7.5 3.36-7.5 7.5 0 4.14 3.35 7.5 7.5 7.5 1.7 0 3.27-.57 4.53-1.54l5.73 5.73c.42.41 1.07.43 1.46.04.39-.39.37-1.04-.05-1.46zM10 15.5c-3.04 0-5.5-2.46-5.5-5.5S6.96 4.5 10 4.5c3.03 0 5.5 2.46 5.5 5.5s-2.47 5.5-5.5 5.5z"/></symbol><symbol id="eixo-icone-feed" viewBox="0 0 24 24"><path d="M17 14c0 .55-.45 1-1 1h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1zM19 10c0 .55-.45 1-1 1h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1z"/><path fill="#FFF" d="M17 14c0 .55-.45 1-1 1h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1zM19 10c0 .55-.45 1-1 1h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1z"/><path d="M19 10c0 .55-.45 1-1 1h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1zM17 14c0 .55-.45 1-1 1h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1z"/><path d="M4 3v4H0v11c0 1.66 1.34 3 3 3h19c1.1 0 2-.9 2-2V3H4zm0 15.09c0 .5-.45.91-1 .91s-1-.41-1-.91V9h2v9.09zM22 19H6V5h16v14z"/><path fill="#FFF" d="M4 9v9.09c0 .5-.45.91-1 .91s-1-.41-1-.91V9h2zM6 5v14h16V5H6zm10 10h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1s-.45 1-1 1zm2-4h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1s-.45 1-1 1z"/></symbol><symbol id="eixo-icone-menu" viewBox="0 0 24 24"><path d="M22 6c0 .552-.447 1-1 1H3c-.552 0-1-.448-1-1 0-.553.448-1 1-1h18c.553 0 1 .447 1 1zM22 12c0 .552-.447 1-1 1H3c-.552 0-1-.448-1-1 0-.553.448-1 1-1h18c.553 0 1 .447 1 1zM22 18c0 .552-.447 1-1 1H3c-.552 0-1-.448-1-1 0-.553.448-1 1-1h18c.553 0 1 .447 1 1z"/></symbol><symbol id="eixo-icone-usuario" viewBox="0 0 24 24"><path d="M12 2C8.4 2 5.5 4.9 5.5 8.5c0 3.61 2.9 6.5 6.5 6.5s6.5-2.89 6.5-6.5C18.5 4.9 15.6 2 12 2zm0 11.07c-2.49 0-4.5-2.02-4.5-4.5s2.01-4.5 4.5-4.5 4.5 2.02 4.5 4.5-2.01 4.5-4.5 4.5zM22.893 23c-.733 0-1.074-.455-1.074-.86C21.817 20.26 18.22 19 12 19s-9.818 1.136-9.818 3.14c0 .406-.41.86-1.092.86C.358 23 0 22.513 0 22.108 0 18.958 4.598 17 12 17s12 1.958 12 5.108c0 .405-.398.892-1.107.892z"/></symbol><symbol id="regua-arrow" viewBox="0 0 5 14"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.902 6.64L1.422.386C1.217.017.762-.11.402.104.05.317-.074.79.133 1.16l3.25 5.843L.1 12.847c-.208.368-.085.838.274 1.05.357.212.814.086 1.02-.28l3.412-6.074c.21-.243.26-.606.096-.904z"/></symbol></svg></div><div class="regua-navegacao-container"><ul class="regua-lista regua-lista-home"><li class="regua-navegacao-item active"><span data-href="http://www.globo.com/"
                                            data-target="regua-tab-home" class="home-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-feed"/></svg></span></li><li class="regua-navegacao-item "><span data-target="regua-tab-menu" class="menu-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-menu"/></svg></span></li><li class="regua-navegacao-item "><span data-target="regua-tab-busca" class="busca-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-busca"/></svg></span></li><li class="regua-navegacao-item "><span data-target="regua-tab-usuario" class="usuario-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-usuario"/></svg></span></li></ul></div></nav><nav id="regua-user-container" class="regua-user-container regua-user-home regua-navegacao-tab"><div id="regua-user-controls" class="regua-user-controls"></div><div id="regua-user-tabs" class="regua-tabs regua-user-tab-2"><div id="tab-labels" class="tab-labels"><span class="tab-label tab-label-1" data-index="1">notificaÃ§Ãµes</span><span class="tab-label tab-label-2" data-index="2">minha conta</span></div><div class="regua-tab regua-tab-1" data-index="1"><div class="regua-content"><ul id="regua-user-profile-notifications" class="regua-user-row regua-user-profile-notifications"></ul></div></div><div class="regua-tab regua-tab-2" data-index="2"><div class="regua-content"><ul id="regua-user-profile-list" class="regua-user-row regua-user-profile-list"></ul><div id="regua-user-logout" class="regua-user-logout"></div></div></div></div></nav><div class="regua-navegacao-tab regua-tab-busca"><div class="regua-search-header"><div class="regua-search-box"><form class="regua-search-form" action="http://globo.com/busca/" method="GET"><div class="regua-search-form-container regua-table"><div class="regua-search-input-box regua-table-cell"><input class="regua-search-input" name="q" type="search" placeholder="o que vocÃª procura?" autocorrect="off" autocapitalize="off" autocomplete="off"/></div><div class="regua-search-buttons regua-table-cell"><div class="regua-search-clear-container"><a href="#" class="regua-search-clear-button">clear</a></div><div class="regua-search-buttons-separator">&nbsp;</div><div class="regua-search-speech-go-container"><a href="#" class="regua-search-mic-button">mic</a><input type="submit" value="go" class="regua-search-go-button"/><div class="regua-icon-block"><svg class="regua-navegacao-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#eixo-icone-busca"></use></svg></div></div></div></div></form></div></div><div class="regua-container-pre-suggest"><ul class="regua-pre-suggest"></ul></div><div class="regua-container-suggest"><ul class="regua-suggest"></ul></div><div class="regua-container-results"></div></div><script type='text/javascript'> window.SETTINGS=window.SETTINGS || {};window.SETTINGS.optimizelyId = "2223881511";window.OptimizelyAbCallback.includeABScript(SETTINGS.optimizelyId); </script>



<ul id="home-menu" class="nested widget-menu">
    
        <li class="g1 analytics-product analytics-multi-product">
            <a href="http://g1.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">g1</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://g1.globo.com/" data-menu-id="g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">g1</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="jornais-revistas-e-radio">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">jornais, revistas e rÃ¡dio</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="servicos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">serviÃ§os</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-g1">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-g1" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/economia/">
                                                <span class="titulo">Economia</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/carros/">
                                                <span class="titulo">Carros</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/">
                                                <span class="titulo">G1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/politica/">
                                                <span class="titulo">PolÃ­tica</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/mundo/">
                                                <span class="titulo">Mundo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/concursos-e-emprego/">
                                                <span class="titulo">Concursos e Empregos</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://fantastico.globo.com/">
                                                <span class="titulo">FantÃ¡stico</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/jornal-nacional/">
                                                <span class="titulo">Jornal Nacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/">
                                                <span class="titulo">Jornal Extra</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/">
                                                <span class="titulo">Jornal O Globo</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="g1">
                                        <div class="submenu-title">g1</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Carros</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/concursos-e-emprego/">
                                                    <span class="titulo">Concursos e Empregos</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/">
                                                    <span class="titulo">Economia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/educacao/">
                                                    <span class="titulo">EducaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/imposto-de-renda/2015/index.html">
                                                    <span class="titulo">Imposto de Renda 2015</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/mundo/">
                                                    <span class="titulo">Mundo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/planeta-bizarro/">
                                                    <span class="titulo">Planeta Bizarro</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/politica/">
                                                    <span class="titulo">PolÃ­tica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/">
                                                    <span class="titulo">Pop &amp; Arte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/tecnologia/">
                                                    <span class="titulo">Tecnologia &amp; Games</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bom-dia-brasil/">
                                                    <span class="titulo">Bom dia Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://fantastico.globo.com/">
                                                    <span class="titulo">FantÃ¡stico</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/agronegocios/">
                                                    <span class="titulo">Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/globo-news/">
                                                    <span class="titulo">GloboNews</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/hora1">
                                                    <span class="titulo">Hora 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-da-globo/">
                                                    <span class="titulo">Jornal da Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-hoje/">
                                                    <span class="titulo">Jornal Hoje</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-nacional/">
                                                    <span class="titulo">Jornal Nacional</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="jornais-revistas-e-radio">
                                        <div class="submenu-title">jornais, revistas e rÃ¡dio</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/">
                                                    <span class="titulo">Jornal Extra</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/">
                                                    <span class="titulo">Jornal O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://cbn.globoradio.globo.com/home/HOME.htm">
                                                    <span class="titulo">RÃ¡dio CBN</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaautoesporte.globo.com/">
                                                    <span class="titulo">Revista Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaepoca.globo.com/">
                                                    <span class="titulo">Revista Ãpoca</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagalileu.globo.com/">
                                                    <span class="titulo">Revista Galileu</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagloborural.globo.com/">
                                                    <span class="titulo">Revista Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radioglobo.globoradio.globo.com/">
                                                    <span class="titulo">RÃ¡dio Globo</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="servicos">
                                        <div class="submenu-title">serviÃ§os</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/agenda.html">
                                                    <span class="titulo">Agenda de Shows</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://especiais.g1.globo.com/educacao/app-g1-enem/">
                                                    <span class="titulo">Aplicativo G1 Enem</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/">
                                                    <span class="titulo">Conversor de Moedas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">
                                                    <span class="titulo">CotaÃ§Ãµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/">
                                                    <span class="titulo">Enem e Vestibular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/loteria/">
                                                    <span class="titulo">Loterias</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/previsao-do-tempo.html">
                                                    <span class="titulo">PrevisÃ£o do tempo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/tabela-fipe/index.html">
                                                    <span class="titulo">Tabela FIPE</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/telecurso/">
                                                    <span class="titulo">Telecurso</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.techtudo.com.br/velocimetro.html">
                                                    <span class="titulo">VelocÃ­metro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-g1"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="globoesporte analytics-product analytics-multi-product">
            <a href="http://globoesporte.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">globoesporte</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-globoesporte">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/" data-menu-id="globoesportecom">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globoesporte.com</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://sportv.globo.com/" data-menu-id="sportv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sportv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/futebol/" data-menu-id="futebol">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">futebol</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="mais-esportes">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mais esportes</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-globoesporte">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-globoesporte" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/selecao-brasileira/">
                                                <span class="titulo">SeleÃ§Ã£o Brasileira</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/cartola-fc/">
                                                <span class="titulo">Cartola FC</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                <span class="titulo">Futebol Internacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie B</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                <span class="titulo">FÃ³rmula 1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/">
                                                <span class="titulo">Globo Esporte</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/">
                                                <span class="titulo">Futebol Nacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://sportv.globo.com/site/combate/">
                                                <span class="titulo">MMA</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-c/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie C</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="globoesportecom">
                                        <div class="submenu-title">globoesporte.com</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://futpedia.globo.com/">
                                                    <span class="titulo">FutpÃ©dia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/cartola-fc/">
                                                    <span class="titulo">Cartola FC</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="sportv">
                                        <div class="submenu-title">sportv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/bolao/">
                                                    <span class="titulo">BolÃ£o</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/eventos/combate/">
                                                    <span class="titulo">Combate</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/corujao-do-esporte/">
                                                    <span class="titulo">CorujÃ£o do Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/esporte-espetacular/">
                                                    <span class="titulo">Esporte Espetacular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/">
                                                    <span class="titulo">Globo Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://app.globoesporte.globo.com/tv/planeta-extremo/">
                                                    <span class="titulo">Planeta Extremo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sociopremiere.globo.com/">
                                                    <span class="titulo">Premiere</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/">
                                                    <span class="titulo">SporTV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/lutas/tuf-brasil/">
                                                    <span class="titulo">The Ultimate Fighter Brasil</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="futebol">
                                        <div class="submenu-title">futebol</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie B</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-c/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie C</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-do-brasil/">
                                                    <span class="titulo">Copa do Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-sul-americana/">
                                                    <span class="titulo">Copa Sul-Americana</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                    <span class="titulo">Futebol Internacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol Nacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">
                                                    <span class="titulo">Liga dos CampeÃµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/selecao-brasileira/">
                                                    <span class="titulo">SeleÃ§Ã£o Brasileira</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="mais-esportes">
                                        <div class="submenu-title">mais esportes</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/atletismo/">
                                                    <span class="titulo">Atletismo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/basquete/">
                                                    <span class="titulo">Basquete</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eu-atleta/">
                                                    <span class="titulo">Eu Atleta</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                    <span class="titulo">FÃ³rmula 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eventos/futsal/">
                                                    <span class="titulo">Futsal</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/combate/">
                                                    <span class="titulo">MMA</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/natacao/">
                                                    <span class="titulo">NataÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/stock-car/">
                                                    <span class="titulo">StockCar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/tenis/">
                                                    <span class="titulo">TÃªnis</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/volei/">
                                                    <span class="titulo">VÃ´lei</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-globoesporte"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="gshow analytics-product analytics-multi-product">
            <a href="http://gshow.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">gshow</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/" data-menu-id="gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gshow</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="programas">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">programas</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="series">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sÃ©ries</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/programas/webseries/" data-menu-id="webseries">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">websÃ©ries</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com/" data-menu-id="rede-globo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">rede globo</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-gshow">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-gshow" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/realities/the-voice-brasil/">
                                                <span class="titulo">The Voice</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/mais-voce/">
                                                <span class="titulo">Mais VocÃª</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                <span class="titulo">VÃ­deo Show</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://redeglobo.globo.com/programacao.html">
                                                <span class="titulo">ProgramaÃ§Ã£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/">
                                                <span class="titulo">Gshow</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                <span class="titulo">Encontro com FÃ¡tima</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                <span class="titulo">CaldeirÃ£o do Huck</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://redeglobo.globo.com/">
                                                <span class="titulo">Rede globo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/programa-do-jo/">
                                                <span class="titulo">Programa do JÃ´</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="gshow">
                                        <div class="submenu-title">gshow</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Bastidores/">
                                                    <span class="titulo">Bastidores</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/como-fazer/">
                                                    <span class="titulo">Como Fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo">
                                                    <span class="titulo">Estilo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/especial-blog/gshow-troll/1.html">
                                                    <span class="titulo">Gshow Troll</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/participe/">
                                                    <span class="titulo">Participe</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas-da-Ana-Maria/">
                                                    <span class="titulo">Receitas da Ana Maria</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/tv/plantao/">
                                                    <span class="titulo">TV</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="programas">
                                        <div class="submenu-title">programas</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://gshow.globo.com/programas/altas-horas/">
                                                    <span class="titulo">Altas Horas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                    <span class="titulo">CaldeirÃ£o do Huck</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                    <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/e-de-casa/">
                                                    <span class="titulo">Ã de Casa</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                    <span class="titulo">Encontro com FÃ¡tima</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/estrelas/index.html">
                                                    <span class="titulo">Estrelas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/">
                                                    <span class="titulo">Mais VocÃª</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/programa-do-jo/">
                                                    <span class="titulo">Programa do JÃ´</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/realities/the-voice-brasil/">
                                                    <span class="titulo">The Voice</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                    <span class="titulo">VÃ­deo Show</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="series">
                                        <div class="submenu-title">sÃ©ries</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/amorteamo/">
                                                    <span class="titulo">Amorteamo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/chapa-quente/">
                                                    <span class="titulo">Chapa Quente</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/dupla-identidade/index.html">
                                                    <span class="titulo">Dupla Identidade</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/felizes-para-sempre/index.html">
                                                    <span class="titulo">Felizes para sempre?</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/mister-brau/2015/">
                                                    <span class="titulo">Mister Brau</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/os-experientes/">
                                                    <span class="titulo">Os Experientes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/pe-na-cova/2015/">
                                                    <span class="titulo">PÃ© na Cova</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/especial-blog/ta-no-ar-a-tv-na-tv/1.html">
                                                    <span class="titulo">TÃ¡ no ar: a TV na TV </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/tapas-e-beijos/">
                                                    <span class="titulo">Tapas &amp; Beijos</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/zorra/">
                                                    <span class="titulo">Zorra</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="webseries">
                                        <div class="submenu-title">websÃ©ries</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/como-fazer/no-ar.html">
                                                    <span class="titulo">Como fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/dulce-delight/">
                                                    <span class="titulo">Dulce Delight</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/look-do-dia-com-tia-suelly/no-ar.html">
                                                    <span class="titulo">Look do Dia com Tia Suelly</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/marrom-combina-com-tudo/no-ar.html">
                                                    <span class="titulo">Marrom Combina com Tudo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/morri-na-tv/no-ar.html">
                                                    <span class="titulo">Morri na TV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/sebastiana-quebra-galho/no-ar.html">
                                                    <span class="titulo">Sebastiana Quebra-Galho</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/sete-vidas-casos-reais/">
                                                    <span class="titulo">Sete Vidas: casos reais</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/o-incrivel-superonix/no-ar.html">
                                                    <span class="titulo">SuperÃnix</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/tome-prumo/">
                                                    <span class="titulo">Tome Prumo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/vlog-da-valeska/no-ar.html">
                                                    <span class="titulo">Vlog da Valeska</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="rede-globo">
                                        <div class="submenu-title">rede globo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/programacao.html">
                                                    <span class="titulo">ProgramaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-gshow"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="famosos-etc analytics-product analytics-multi-product">
            <a href="http://famosos.globo.com" class="analytics-area analytics-id-T">
                <span class="titulo">famosos &amp; etc</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-famosos-etc">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://famosos.globo.com/" data-menu-id="famosos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">famosos</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="moda-estilo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">moda & estilo</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="bem-estar-saude">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">bem-estar & saÃºde</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="casa-decoracao">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">casa & decoraÃ§Ã£o</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="gastronomia">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gastronomia</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="musica-cinema-e-teatro">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mÃºsica, cinema e teatro</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="canais-de-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">canais de tv</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-famosos-etc">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-famosos-etc" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://ego.globo.com/">
                                                <span class="titulo">Ego</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://paparazzo.globo.com/">
                                                <span class="titulo">Paparazzo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://revistaquem.globo.com/">
                                                <span class="titulo">Quem</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/cultura/kogut/">
                                                <span class="titulo">Patricia Kogut</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/famosos/">
                                                <span class="titulo">Retratos da Vida</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://famosos.globo.com/">
                                                <span class="titulo">Famosos</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="famosos">
                                        <div class="submenu-title">famosos</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/">
                                                    <span class="titulo">Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://paparazzo.globo.com/">
                                                    <span class="titulo">Paparazzo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/cultura/kogut/">
                                                    <span class="titulo">Patricia Kogut</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaquem.globo.com/">
                                                    <span class="titulo">Quem</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/famosos/">
                                                    <span class="titulo">Retratos da Vida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="moda-estilo">
                                        <div class="submenu-title">moda &amp; estilo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/moda/index.html">
                                                    <span class="titulo">Moda no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ela.oglobo.globo.com/">
                                                    <span class="titulo">Ela no O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo/">
                                                    <span class="titulo">Estilo no Gshow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaglamour.globo.com/">
                                                    <span class="titulo">Revista Glamour</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gq.globo.com/">
                                                    <span class="titulo">Revista GQ Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistamarieclaire.globo.com/">
                                                    <span class="titulo">Revista Marie Claire </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/moda/">
                                                    <span class="titulo">Moda no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://vogue.globo.com/">
                                                    <span class="titulo">Revista Vogue</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="bem-estar-saude">
                                        <div class="submenu-title">bem-estar &amp; saÃºde</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/beleza/index.html">
                                                    <span class="titulo">Beleza no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/beleza/">
                                                    <span class="titulo">Beleza no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.com.br/bemestar">
                                                    <span class="titulo">Bem Estar no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar no G1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/maes-e-filhos/">
                                                    <span class="titulo">MÃ£es no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/">
                                                    <span class="titulo">Meus 5 Minutos </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://horoscopo.ego.globo.com/">
                                                    <span class="titulo">HorÃ³scopo no Ego</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="casa-decoracao">
                                        <div class="submenu-title">casa &amp; decoraÃ§Ã£o</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/">
                                                    <span class="titulo">Revista Casa e Jardim</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/casa-e-decoracao/">
                                                    <span class="titulo">Casa no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://casavogue.globo.com/">
                                                    <span class="titulo">Casa Vogue </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revista.zapimoveis.com.br/?utm_source=globo.com-etc&amp;utm_medium=botao-casa-decoracao&amp;utm_campaign=Revista-Home">
                                                    <span class="titulo">Revista do Zap</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="gastronomia">
                                        <div class="submenu-title">gastronomia</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/receitas/">
                                                    <span class="titulo">Receitas no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas/index.html">
                                                    <span class="titulo">Receitas no Mais VocÃª </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/blogs/Colheradas/">
                                                    <span class="titulo">Colheradas </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/Casa-e-Comida/">
                                                    <span class="titulo">Casa e Comida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="musica-cinema-e-teatro">
                                        <div class="submenu-title">mÃºsica, cinema e teatro</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/cinema/">
                                                    <span class="titulo">G1 Cinema</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/">
                                                    <span class="titulo">G1 MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">Gshow MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://musica.com.br/">
                                                    <span class="titulo">MÃºsica.com.br</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radiobeat.com.br/">
                                                    <span class="titulo">RADIOBEAT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.agentesevenoteatro.com.br/">
                                                    <span class="titulo">Teatro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="canais-de-tv">
                                        <div class="submenu-title">canais de tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/">
                                                    <span class="titulo">Rede Globo </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalbrasil.globo.com/">
                                                    <span class="titulo">Canal Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://mundogloob.globo.com/">
                                                    <span class="titulo">Gloob</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/">
                                                    <span class="titulo">GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://megapix.globo.com/">
                                                    <span class="titulo">Megapix</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://multishow.globo.com/">
                                                    <span class="titulo">Multishow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://telecine.globo.com/">
                                                    <span class="titulo">Telecine</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://uc.globo.com/">
                                                    <span class="titulo">Universal Channel</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalviva.globo.com/">
                                                    <span class="titulo">Viva</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://canaloff.globo.com">
                                                    <span class="titulo">Off</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-famosos-etc"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="tecnologia analytics-product analytics-multi-product">
            <a href="http://www.techtudo.com.br/" class="analytics-area analytics-id-T">
                <span class="titulo">tecnologia</span>

        
        
            </a>
        </li>
        
    
        <li class="videos analytics-product analytics-multi-product">
            <a href="http://globotv.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">vÃ­deos</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                            
                                <li class="has-link ">
                                    <a href="http://globotv.globo.com/" data-menu-id="globotv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globo.tv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link ">
                                    <a href="http://globosatplay.globo.com/" data-menu-id="globosat-play">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globosat play</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-videos">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
</ul>
<!-- gerado em: 2015-10-1323:07:37Z --><div id="assinante-menu"></div></nav></div><div class="header-bottom"></div></header><script>
libby.loadScript('http://s.glbimg.com/en/ho/static/etc/busca/js/jquery.buscaPadrao.v2.compressed.js', function(){var searchArea=document.getElementById('busca-padrao'),
placeholderValue = 'encontre na globo.com';if(typeof searchWidget !== "undefined" && searchWidget){searchWidget(searchArea, {suggestionsEnabled: true,
baseSearchUrl: 'http://www.globo.com',
addClass:'home-search',
placeholder: placeholderValue,
buscaHome: true,
qtd_min_letras_pro_suggest: 2,
qtd_min_letras_pra_busca: 2,
addStyleSheet: ''});}
searchArea.style.display = "block";if(searchArea && searchArea.querySelectorAll){var inputElement=searchArea.querySelectorAll('input')[0];inputElement.onwebkitspeechchange=inputElement.onspeechchange=function (ev) {inputElement.value=inputElement.value.replace(placeholderValue, '');inputElement.form.submit();};}});</script><div id="x60" class="opec-area opec-mobile opec-x60 grid-12"><div id="banner_mobile_topo" class="tag-manager-publicidade-container"></div></div><div id="urgente" class="container urgente analytics-area analytics-id-U "><div class="grid-base super-wide"></div></div><div id="bloco-principal" class="container first-scroll-container clearfix"><div class="grid-base wide analytics-area analytics-id-A"><div class="grid-base narrow pull-left primeira-area"><div class="destaque-principal-vertical-foto-topo destaque destaque-primeiro-scroll principal glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/futebol-internacional/jogo/13-10-2015/brasil-venezuela/index.html" class="foto " title="AO VIVO: Willian faz o 2Âº dele e da SeleÃ§Ã£o (AP)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/qhN0BT_xGxSU8bKAqDrTg2havAo=/filters:quality(10):strip_icc()/s2.glbimg.com/gAC3IiaVlNm0d-LoOPxNEb_MTTY=/0x703:1926x1422/335x125/s.glbimg.com/es/ge/f/original/2015/10/13/brazil_venezuela_wcup_amar.jpg" alt="AO VIVO: Willian faz o 2Âº dele e da SeleÃ§Ã£o (AP)" title="AO VIVO: Willian faz o 2Âº dele e da SeleÃ§Ã£o (AP)"
         data-original-image="s2.glbimg.com/gAC3IiaVlNm0d-LoOPxNEb_MTTY=/0x703:1926x1422/335x125/s.glbimg.com/es/ge/f/original/2015/10/13/brazil_venezuela_wcup_amar.jpg" data-url-smart_horizontal="ytXdOpvbOsoCK55WFlh_60g2g-A=/400xorig/smart/filters:strip_icc()/" data-url-smart="ytXdOpvbOsoCK55WFlh_60g2g-A=/400xorig/smart/filters:strip_icc()/" data-url-feature="ytXdOpvbOsoCK55WFlh_60g2g-A=/400xorig/smart/filters:strip_icc()/" data-url-tablet="u-OaubNwyp2Aa_CDmm1YT-yZY8k=/340xorig/smart/filters:strip_icc()/" data-url-desktop="YXGt7xdtDrJitJnIrJBMgvnR9DM=/335xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>AO VIVO: Willian faz <br />o 2Âº dele e da SeleÃ§Ã£o</h2></div></a></div></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/noticia/2015/10/dilma-aponta-golpe-diz-que-crise-politica-e-seria-e-pede-estabilidade.html" class=" " title="Dilma fala em &#39;golpe&#39; e vÃª crise polÃ­tica &#39;sÃ©ria&#39;"><div class="conteudo"><h2>Dilma fala em &#39;golpe&#39; e vÃª crise polÃ­tica &#39;sÃ©ria&#39;</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/noticia/2015/10/ministra-reforca-suspensao-de-rito-feito-por-cunha-para-impeachment.html" class=" " title="STF barra regras de Cunha para o impeachment"><div class="conteudo"><h2>STF barra regras de Cunha para <br />o impeachment</h2></div></a></div></div></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/blog/blog-do-camarotti/post/se-derrubo-dilma-no-dia-seguinte-voces-me-derrubam-diz-cunha-oposicao.html" class=" " title="Camarotti: Se derrubo Dilma, vocÃªs me derrubam, diz Cunha Ã  oposiÃ§Ã£o"><div class="conteudo"><h2>Camarotti: Se derrubo Dilma, vocÃªs me derrubam, diz Cunha Ã  oposiÃ§Ã£o</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Afastar Cunha Ã© um sentimento majoritÃ¡rio, diz AÃ©cio" href="http://g1.globo.com/politica/noticia/2015/10/aecio-diz-que-afastamento-de-cunha-e-sentimento-majoritario-na-oposicao.html">Afastar Cunha Ã© um sentimento majoritÃ¡rio, diz AÃ©cio</a></div></li><li><div class="mobile-grid-partial"><a title="Metade da bancada do PT assina aÃ§Ã£o contra Cunha" href="http://g1.globo.com/politica/noticia/2015/10/psol-entra-com-representacao-no-conselho-de-etica-contra-cunha.html">Metade da bancada do PT assina aÃ§Ã£o contra Cunha</a></div></li></ul></div><div class="ultimo-destaque"><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-entretenimento analytics-area  analytics-id-M
        "><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/vem-por-ai/noticia/2015/10/janete-anuncia-namoro-com-bonitao-e-mel-se-revolta.html" class="foto " title="&#39;Regra&#39;: Janete anuncia namoro e causa revolta (TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/CTBWu8VM_8COLY46Pyf2PB3Q4mE=/filters:quality(10):strip_icc()/s2.glbimg.com/kb6iAFmmNlszDHJOo7AOPiN9xto=/0x17:690x329/155x70/s.glbimg.com/et/gs/f/original/2015/10/12/nenemzinho-e-janete.jpg" alt="&#39;Regra&#39;: Janete anuncia namoro e causa revolta (TV Globo)" title="&#39;Regra&#39;: Janete anuncia namoro e causa revolta (TV Globo)"
         data-original-image="s2.glbimg.com/kb6iAFmmNlszDHJOo7AOPiN9xto=/0x17:690x329/155x70/s.glbimg.com/et/gs/f/original/2015/10/12/nenemzinho-e-janete.jpg" data-url-smart_horizontal="zBvIEiOEpU8mkY5rHkXjr6tSWPI=/90x56/smart/filters:strip_icc()/" data-url-smart="zBvIEiOEpU8mkY5rHkXjr6tSWPI=/90x56/smart/filters:strip_icc()/" data-url-feature="zBvIEiOEpU8mkY5rHkXjr6tSWPI=/90x56/smart/filters:strip_icc()/" data-url-tablet="pFef1zP4U0axWD1oqQ1Y7W7kbZE=/160xorig/smart/filters:strip_icc()/" data-url-desktop="8zOK4Adq80rIYx6PIiMbSKX2bOA=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>&#39;Regra&#39;: Janete anuncia namoro e causa revolta</h2></div></a></div></div></div></div><div class="grid-base narrow ultimo"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/futebol-internacional/jogo/13-10-2015/paraguai-argentina/" class="foto " title="Argentina segue sem vencer: 0-0 com o  Paraguai (Reuters)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/DXtxcmJYyscsXKB1Z_yLyQSGHug=/filters:quality(10):strip_icc()/s2.glbimg.com/gjTMaofDxJGMwofkfDyEIByFMxI=/590x162:2093x1375/155x125/s.glbimg.com/es/ge/f/original/2015/10/13/2015-10-14t004320z_1271086581_gf10000244170_rtrmadp_3_soccer-world.jpg" alt="Argentina segue sem vencer: 0-0 com o  Paraguai (Reuters)" title="Argentina segue sem vencer: 0-0 com o  Paraguai (Reuters)"
         data-original-image="s2.glbimg.com/gjTMaofDxJGMwofkfDyEIByFMxI=/590x162:2093x1375/155x125/s.glbimg.com/es/ge/f/original/2015/10/13/2015-10-14t004320z_1271086581_gf10000244170_rtrmadp_3_soccer-world.jpg" data-url-smart_horizontal="GE0IAGAXuVEwA9typlq_zo99XE0=/90x56/smart/filters:strip_icc()/" data-url-smart="GE0IAGAXuVEwA9typlq_zo99XE0=/90x56/smart/filters:strip_icc()/" data-url-feature="GE0IAGAXuVEwA9typlq_zo99XE0=/90x56/smart/filters:strip_icc()/" data-url-tablet="CxqJRANM7_bXgqQK3O2KvVrtkbA=/160xorig/smart/filters:strip_icc()/" data-url-desktop="UhVyb0g27MEKh79Z8t1zn2qNZKM=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Argentina segue sem vencer: 0-0 com o  Paraguai</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="TEMPO REAL: Chile e Peru duelam em Lima" href="http://globoesporte.globo.com/futebol/futebol-internacional/jogo/13-10-2015/peru-chile/">TEMPO REAL: Chile e Peru duelam em Lima</a></div></li></ul></div><div class="direita"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/futebol-internacional/jogo/13-10-2015/uruguai-colombia/" class="foto " title="Uruguai domina ColÃ´mbia, vence e assume ponta (AP)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/AWchxyAbicoqA2Il5t7qmoOkmRo=/filters:quality(10):strip_icc()/s2.glbimg.com/NHfzhnuuhq-LokpRLGlDasldZOw=/1702x240:3448x1648/155x125/s.glbimg.com/es/ge/f/original/2015/10/13/uruguay_colombia_wcup_amar_1.jpg" alt="Uruguai domina ColÃ´mbia, vence e assume ponta (AP)" title="Uruguai domina ColÃ´mbia, vence e assume ponta (AP)"
         data-original-image="s2.glbimg.com/NHfzhnuuhq-LokpRLGlDasldZOw=/1702x240:3448x1648/155x125/s.glbimg.com/es/ge/f/original/2015/10/13/uruguay_colombia_wcup_amar_1.jpg" data-url-smart_horizontal="7WBPw9KWvQW6FTDIrOIH9YV8ezg=/90x56/smart/filters:strip_icc()/" data-url-smart="7WBPw9KWvQW6FTDIrOIH9YV8ezg=/90x56/smart/filters:strip_icc()/" data-url-feature="7WBPw9KWvQW6FTDIrOIH9YV8ezg=/90x56/smart/filters:strip_icc()/" data-url-tablet="Y8HncIlZi9wLYYedS7Xf_AuV5VI=/160xorig/smart/filters:strip_icc()/" data-url-desktop="OFYApkFMyuJVXCLhHDIyekS202U=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Uruguai domina ColÃ´mbia, vence e assume ponta</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Equador supera BolÃ­via e se mantÃ©m com 100%" href="http://globoesporte.globo.com/futebol/futebol-internacional/jogo/13-10-2015/equador-bolivia/">Equador supera BolÃ­via e se mantÃ©m com 100%</a></div></li></ul></div></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://g1.globo.com/pop-arte/noticia/2015/10/google-deve-pagar-r-250-mil-daniela-cicarelli-por-video-intimo.html" class="foto " title="Google vai pagar R$ 250 mil por vÃ­deo a Cicarelli (Filipe AraÃºjo/EstadÃ£o ConteÃºdo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/MiCmLet3k9nlkJQJavFtO8acBmo=/filters:quality(10):strip_icc()/s2.glbimg.com/ZqtOXHxbcsOgdzOSBoG0ajj6gfs=/0x46:300x184/155x71/s.glbimg.com/jo/g1/f/original/2015/10/13/spfw-ellus_filipe_araujo_ae_1.jpg" alt="Google vai pagar R$ 250 mil por vÃ­deo a Cicarelli (Filipe AraÃºjo/EstadÃ£o ConteÃºdo)" title="Google vai pagar R$ 250 mil por vÃ­deo a Cicarelli (Filipe AraÃºjo/EstadÃ£o ConteÃºdo)"
         data-original-image="s2.glbimg.com/ZqtOXHxbcsOgdzOSBoG0ajj6gfs=/0x46:300x184/155x71/s.glbimg.com/jo/g1/f/original/2015/10/13/spfw-ellus_filipe_araujo_ae_1.jpg" data-url-smart_horizontal="cJnNYWCjGgxoSFH1sr1GB8Plr5s=/90x56/smart/filters:strip_icc()/" data-url-smart="cJnNYWCjGgxoSFH1sr1GB8Plr5s=/90x56/smart/filters:strip_icc()/" data-url-feature="cJnNYWCjGgxoSFH1sr1GB8Plr5s=/90x56/smart/filters:strip_icc()/" data-url-tablet="C_lpQY2-PN-fjx2sXm1HhuHiHJY=/160xorig/smart/filters:strip_icc()/" data-url-desktop="lNGroB8eHlheLcdGYbwC_ffbHgk=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Google vai pagar R$ 250 mil por vÃ­deo a Cicarelli</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://g1.globo.com/mundo/noticia/2015/10/fugitivos-de-alcatraz-em-1962-podem-estar-vivos-e-no-brasil-diz-familia.html" class="foto " title="Fugitivos de Alcatraz podem estar no Brasil (DivulgaÃ§Ã£o/History Channel/Arquivos da famÃ­lia Anglin)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/ZK2Q4xbUmpzsRGPZy-3KEhm2WxE=/filters:quality(10):strip_icc()/s2.glbimg.com/lP2ooS0yiyAyTHdRmqmK7m_9tT8=/115x29:585x242/155x70/s.glbimg.com/jo/g1/f/original/2015/10/13/anglin-photo.jpg" alt="Fugitivos de Alcatraz podem estar no Brasil (DivulgaÃ§Ã£o/History Channel/Arquivos da famÃ­lia Anglin)" title="Fugitivos de Alcatraz podem estar no Brasil (DivulgaÃ§Ã£o/History Channel/Arquivos da famÃ­lia Anglin)"
         data-original-image="s2.glbimg.com/lP2ooS0yiyAyTHdRmqmK7m_9tT8=/115x29:585x242/155x70/s.glbimg.com/jo/g1/f/original/2015/10/13/anglin-photo.jpg" data-url-smart_horizontal="CXm5H7a8PJDCpbQCge30Z1kcNvg=/90x56/smart/filters:strip_icc()/" data-url-smart="CXm5H7a8PJDCpbQCge30Z1kcNvg=/90x56/smart/filters:strip_icc()/" data-url-feature="CXm5H7a8PJDCpbQCge30Z1kcNvg=/90x56/smart/filters:strip_icc()/" data-url-tablet="z30_30otja-_QoDs4YIkoUUqKxU=/160xorig/smart/filters:strip_icc()/" data-url-desktop="qHRDjIVwIrO9XgzVLe-g9Tvp9_A=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Fugitivos de Alcatraz podem estar no Brasil</h2></div></a></div></div></div><div class="ultimo-destaque"><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-entretenimento analytics-area  analytics-id-M
        "><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/alem-do-tempo/vem-por-ai/noticia/2015/10/melissa-arma-para-ser-flagrada-na-cama-de-felipe.html" class="foto " title="&#39;AlÃ©m&#39;: Melissa arma para ser vista com Felipe (TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/cggg5BGBcpk1s194n25iz4e1_JI=/filters:quality(10):strip_icc()/s2.glbimg.com/9GPLVNMO0tATR7FmYpJr72Cex7M=/0x50:690x362/155x70/s.glbimg.com/et/gs/f/original/2015/10/13/melissa-felipe.jpg" alt="&#39;AlÃ©m&#39;: Melissa arma para ser vista com Felipe (TV Globo)" title="&#39;AlÃ©m&#39;: Melissa arma para ser vista com Felipe (TV Globo)"
         data-original-image="s2.glbimg.com/9GPLVNMO0tATR7FmYpJr72Cex7M=/0x50:690x362/155x70/s.glbimg.com/et/gs/f/original/2015/10/13/melissa-felipe.jpg" data-url-smart_horizontal="Id96oBmG3TX2QJFtM2AGFAWH3hw=/90x56/smart/filters:strip_icc()/" data-url-smart="Id96oBmG3TX2QJFtM2AGFAWH3hw=/90x56/smart/filters:strip_icc()/" data-url-feature="Id96oBmG3TX2QJFtM2AGFAWH3hw=/90x56/smart/filters:strip_icc()/" data-url-tablet="JZ17P7h6C205UhpnGDtWjDTXEPA=/160xorig/smart/filters:strip_icc()/" data-url-desktop="XJUFNlmM2pXUm8iVSUfRaplKbh0=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>&#39;AlÃ©m&#39;: Melissa arma para ser vista com Felipe</h2></div></a></div></div></div></div><div class="grid-base wide pull-left wide-inferior"></div></div><script>
var init,
stid;if(typeof window.homeColumns === 'undefined' && !window.homeColumns){window.homeColumns=new HomeColumns({});}
init=function(ev) {window.homeColumns.initFirstScroll();if(!window.homeColumns.mergedFirstScroll){responsiveHub.updateImages();}
window.removeEventListener("resize", init);clearTimeout(stid);};stid=setTimeout(function(){init();}, 200);window.addEventListener("resize", init);</script><div id="area-widgets-direita-topo" class="grid-base narrow area-widgets-direta-topo analytics-area analytics-id-V"><div id="ad-position-top1" class="opec"><div id="banner_slim_topo" class="tag-manager-publicidade-container"></div></div><div class="libby-agrupador-destaque-globotv-com-canais length-5"><div class="main-area mobile-grid-full"><div class="header-area analytics-area analytics-id-header"><a class="logo-link" href="http://globotv.globo.com"><div class="logo"></div></a><h3><a class="title-link" href="http://globotv.globo.com/busca?q=tempo+real&amp;dt=&amp;d=&amp;o=recentes&amp;c=&amp;p=&amp;page=1">futebol</a></h3></div><div class="hover-area"><div class="image-area"><ul><li class="active analytics-area analytics-id-1 analytics-id-I glb-hl-style-esporte" data-href="http://globotv.globo.com/rede-globo/eliminatorias-da-copa/v/gol-do-brasil-willian-invade-a-area-chuta-cruzado-e-abre-o-placar-aos-0-do-1o-tempo/4535822/"><a href="http://globotv.globo.com/rede-globo/eliminatorias-da-copa/v/gol-do-brasil-willian-invade-a-area-chuta-cruzado-e-abre-o-placar-aos-0-do-1o-tempo/4535822/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4535822.jpg" alt="Gol! Willian chuta forte da entrada da Ã¡rea, e goleirÃ£o nÃ£o segura" /><div class="time">01:15</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">brasil x venezuela</span><span class="title">Gol! Willian chuta forte da entrada da Ã¡rea, e goleirÃ£o nÃ£o segura</span></div></a></li><li class=" analytics-area analytics-id-2 analytics-id-I glb-hl-style-esporte" data-href="http://globotv.globo.com/sportv/futebol-internacional/v/os-gols-de-uruguai-3-x-0-colombia-pela-2a-rodada-das-eliminatorias-da-copa-2018/4535820/"><a href="http://globotv.globo.com/sportv/futebol-internacional/v/os-gols-de-uruguai-3-x-0-colombia-pela-2a-rodada-das-eliminatorias-da-copa-2018/4535820/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4535820.jpg" alt="Celeste se impÃµe no CentenÃ¡rio, bate ColÃ´mbia e chega Ã  2Âª vitÃ³ria" /><div class="time">03:13</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">uruguai 3 x 0 ColÃ´mbia</span><span class="title">Celeste se impÃµe no CentenÃ¡rio, bate ColÃ´mbia e chega Ã  2Âª vitÃ³ria</span></div></a></li><li class=" analytics-area analytics-id-3 analytics-id-I glb-hl-style-esporte" data-href="http://globotv.globo.com/sportv/futebol-internacional/v/os-gols-de-equador-2-x-0-bolivia-pelas-eliminatorias-da-copa/4535539/"><a href="http://globotv.globo.com/sportv/futebol-internacional/v/os-gols-de-equador-2-x-0-bolivia-pelas-eliminatorias-da-copa/4535539/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4535539.jpg" alt="ApÃ³s surpreender a Argentina, Equador vence BolÃ­via em Quito" /><div class="time">03:14</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">equador 2 x 0 BolÃ­via</span><span class="title">ApÃ³s surpreender a Argentina, Equador vence BolÃ­via em Quito</span></div></a></li></ul></div><div class="title-area"><ul><li class="active analytics-area analytics-id-1 analytics-id-T glb-hl-style-esporte"><a href="http://globotv.globo.com/rede-globo/eliminatorias-da-copa/v/gol-do-brasil-willian-invade-a-area-chuta-cruzado-e-abre-o-placar-aos-0-do-1o-tempo/4535822/"><span class="subtitle">brasil x venezuela</span><span class="title">Gol! Willian chuta forte da entrada da Ã¡rea, e goleirÃ£o nÃ£o segura</span></a></li><li class=" analytics-area analytics-id-2 analytics-id-T glb-hl-style-esporte"><a href="http://globotv.globo.com/sportv/futebol-internacional/v/os-gols-de-uruguai-3-x-0-colombia-pela-2a-rodada-das-eliminatorias-da-copa-2018/4535820/"><span class="subtitle">uruguai 3 x 0 ColÃ´mbia</span><span class="title">Celeste se impÃµe no CentenÃ¡rio, bate ColÃ´mbia e chega Ã  2Âª vitÃ³ria</span></a></li><li class=" analytics-area analytics-id-3 analytics-id-T glb-hl-style-esporte"><a href="http://globotv.globo.com/sportv/futebol-internacional/v/os-gols-de-equador-2-x-0-bolivia-pelas-eliminatorias-da-copa/4535539/"><span class="subtitle">equador 2 x 0 BolÃ­via</span><span class="title">ApÃ³s surpreender a Argentina, Equador vence BolÃ­via em Quito</span></a></li></ul></div></div><div class="controls-area"><ul class="step-marker glb-hl-style-esporte"><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-1"><a href="#" data-related="http://globotv.globo.com/rede-globo/eliminatorias-da-copa/v/gol-do-brasil-willian-invade-a-area-chuta-cruzado-e-abre-o-placar-aos-0-do-1o-tempo/4535822/"><div class="ball active"></div></a></div></li><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-2"><a href="#" data-related="http://globotv.globo.com/sportv/futebol-internacional/v/os-gols-de-uruguai-3-x-0-colombia-pela-2a-rodada-das-eliminatorias-da-copa-2018/4535820/"><div class="ball "></div></a></div></li><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-3"><a href="#" data-related="http://globotv.globo.com/sportv/futebol-internacional/v/os-gols-de-equador-2-x-0-bolivia-pelas-eliminatorias-da-copa/4535539/"><div class="ball "></div></a></div></li></ul><ul class="navigation-buttons"><li><a href="#left"><div class="left"></div></a></li><li><a href="#right"><div class="right"></div></a></li></ul></div><div class="menu-area"><ul><li><a href="http://globotv.globo.com/rede-globo/"><div class="channels rede-globo"></div></a></li><li><a href="http://globotv.globo.com/sportv/"><div class="channels sportv"></div></a></li><li><a href="http://globotv.globo.com/globo-news/"><div class="channels globo-news"></div></a></li><li><a href="http://globotv.globo.com/multishow/"><div class="channels multishow"></div></a></li><li><a href="http://globotv.globo.com/gnt/"><div class="channels gnt"></div></a></li></ul></div><div class="footer-area analytics-area analytics-id-footer"><a class="title-link" href="http://globotv.globo.com/">mais vÃ­deos <span class="arrow"> âº</span></a></div></div></div><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded-agrupador-destaque-globotv-com-canais']);</script></div></div><div class="separator-first-scroll-container"><section><div><div id="opec-banner-middle-container" class="opec-banner-middle-container"><div id="ad-position-middle" class="opec"><div id="banner_slb_meio" class="tag-manager-publicidade-container"></div></div></div></div></section></div><div id="x62" class="opec-area opec-mobile opec-x62 grid-12"><div id="banner_mobile_meio" class="tag-manager-publicidade-container"></div></div><div id="container-columns" class="container columns clearfix glb-area-colunas"><section class="area news-column   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/natureza/noticia/2015/10/zoologico-da-dinamarca-causa-furor-ao-anunciar-dissecacao-de-leao.html" class="foto" title="ZoolÃ³gico da Dinamarca causa furor ao anunciar dissecaÃ§Ã£o de leÃ£o jovem (ReproduÃ§Ã£o/facebook/Odense Zoo)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/RSIbXXceKk3qzOvlI5gd6ssZVM0=/filters:quality(10):strip_icc()/s2.glbimg.com/88_UD1q5H_SwEikBSjA4kSI8XmM=/0x48:620x381/335x180/s.glbimg.com/jo/g1/f/original/2015/10/13/dissecacao-leao.jpg" alt="ZoolÃ³gico da Dinamarca causa furor ao anunciar dissecaÃ§Ã£o de leÃ£o jovem (ReproduÃ§Ã£o/facebook/Odense Zoo)" title="ZoolÃ³gico da Dinamarca causa furor ao anunciar dissecaÃ§Ã£o de leÃ£o jovem (ReproduÃ§Ã£o/facebook/Odense Zoo)"
                data-original-image="s2.glbimg.com/88_UD1q5H_SwEikBSjA4kSI8XmM=/0x48:620x381/335x180/s.glbimg.com/jo/g1/f/original/2015/10/13/dissecacao-leao.jpg" data-url-smart_horizontal="uY-cG6CqcSATYIMbPYgdgpyT0lw=/90x0/smart/filters:strip_icc()/" data-url-smart="uY-cG6CqcSATYIMbPYgdgpyT0lw=/90x0/smart/filters:strip_icc()/" data-url-feature="uY-cG6CqcSATYIMbPYgdgpyT0lw=/90x0/smart/filters:strip_icc()/" data-url-tablet="Z_80A8rVwMLCLJFrTaClDjugiM4=/220x125/smart/filters:strip_icc()/" data-url-desktop="7dIeJwU8scqGW4-Al2n1Wj2FMUM=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>ZoolÃ³gico da Dinamarca causa furor ao anunciar dissecaÃ§Ã£o de leÃ£o jovem</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-13 21:18:16" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/mato-grosso-do-sul/noticia/2015/10/em-ms-exames-nao-apontam-causa-da-morte-da-jornalista-priscilla.html" class="foto" title="Exame descarta vÃ­rus, mas nÃ£o indica causa da morte de apresentadora de TV (ReproduÃ§Ã£o/ TV Morena)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/RKkrpmfDuFWuGPoHnQsyCOX8qx0=/filters:quality(10):strip_icc()/s2.glbimg.com/-CLDgOJlDTfaIgi8DmmC5sME9Wk=/100x60:851x561/120x80/s.glbimg.com/jo/g1/f/original/2015/10/07/554426_259095720846750_1380926218_n.jpg" alt="Exame descarta vÃ­rus, mas nÃ£o indica causa da morte de apresentadora de TV (ReproduÃ§Ã£o/ TV Morena)" title="Exame descarta vÃ­rus, mas nÃ£o indica causa da morte de apresentadora de TV (ReproduÃ§Ã£o/ TV Morena)"
                data-original-image="s2.glbimg.com/-CLDgOJlDTfaIgi8DmmC5sME9Wk=/100x60:851x561/120x80/s.glbimg.com/jo/g1/f/original/2015/10/07/554426_259095720846750_1380926218_n.jpg" data-url-smart_horizontal="rz3YnC6Nj_XNkx28QLedsqlzBX4=/90x0/smart/filters:strip_icc()/" data-url-smart="rz3YnC6Nj_XNkx28QLedsqlzBX4=/90x0/smart/filters:strip_icc()/" data-url-feature="rz3YnC6Nj_XNkx28QLedsqlzBX4=/90x0/smart/filters:strip_icc()/" data-url-tablet="hDkZvBuHJzTn0fuL-KBl0QHbrfc=/70x50/smart/filters:strip_icc()/" data-url-desktop="ZVPTW4TxWVIxy0zpHf4YlQPZaVs=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Exame descarta vÃ­rus, mas nÃ£o indica causa da morte de apresentadora de TV</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-13 18:22:32" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/rs/rio-grande-do-sul/noticia/2015/10/jovem-morta-em-farmacia-tinha-se-separado-de-suspeito-ha-dois-meses.html" class="foto" title="RS: atendente morta em farmÃ¡cia tinha se separado do suspeito hÃ¡ dois meses (ReproduÃ§Ã£o / Facebook)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/DClRmC07q2RgGUNqTTsvf5VYTbs=/filters:quality(10):strip_icc()/s2.glbimg.com/AM15xeVSPdT_peqbNhP-3yxXjEo=/0x59:448x358/120x80/s.glbimg.com/en/ho/f/original/2015/10/13/morta-1.jpg" alt="RS: atendente morta em farmÃ¡cia tinha se separado do suspeito hÃ¡ dois meses (ReproduÃ§Ã£o / Facebook)" title="RS: atendente morta em farmÃ¡cia tinha se separado do suspeito hÃ¡ dois meses (ReproduÃ§Ã£o / Facebook)"
                data-original-image="s2.glbimg.com/AM15xeVSPdT_peqbNhP-3yxXjEo=/0x59:448x358/120x80/s.glbimg.com/en/ho/f/original/2015/10/13/morta-1.jpg" data-url-smart_horizontal="QkeEWAHT4h7hpZAFC3VrT-inK0k=/90x0/smart/filters:strip_icc()/" data-url-smart="QkeEWAHT4h7hpZAFC3VrT-inK0k=/90x0/smart/filters:strip_icc()/" data-url-feature="QkeEWAHT4h7hpZAFC3VrT-inK0k=/90x0/smart/filters:strip_icc()/" data-url-tablet="hHl7RQ8kGy2Au9JO6mt5WMk-7KE=/70x50/smart/filters:strip_icc()/" data-url-desktop="We0OsdZtvJ_-4HsBg4aMD3u2Orc=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>RS: atendente morta em farmÃ¡cia tinha se separado do suspeito hÃ¡ dois meses</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-13 22:07:59" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/loterias/noticia/2015/10/mega-sena-concurso-1750-ninguem-acerta-e-premio-vai-r-31-milhoes.html" class="" title="Mega-Sena acumula e prÃªmio deve ir a R$ 31 milhÃµes; quina paga R$ 51,7 mil a 23 apostas" rel="bookmark"><span class="conteudo"><p>Mega-Sena acumula e prÃªmio deve ir a R$ 31 milhÃµes; quina paga R$ 51,7 mil a 23 apostas</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/pr/parana/noticia/2015/10/fim-de-semana-de-beto-richa-em-paris-gera-discussoes-na-assembleia.html" class="foto" title="Governador passa fim de semana com luxo em Paris e provoca polÃªmica no PR (RPC TV)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/ZyCiTslPmReIn93RGyyu0iGd8WU=/filters:quality(10):strip_icc()/s2.glbimg.com/-x5ikk3qu6ykUm-er9YDjitD-t8=/78x78:289x219/120x80/s.glbimg.com/en/ho/f/original/2015/10/13/richa.jpg" alt="Governador passa fim de semana com luxo em Paris e provoca polÃªmica no PR (RPC TV)" title="Governador passa fim de semana com luxo em Paris e provoca polÃªmica no PR (RPC TV)"
                data-original-image="s2.glbimg.com/-x5ikk3qu6ykUm-er9YDjitD-t8=/78x78:289x219/120x80/s.glbimg.com/en/ho/f/original/2015/10/13/richa.jpg" data-url-smart_horizontal="lwtZkwHMCw3-WsZOCobd5PlD3_A=/90x0/smart/filters:strip_icc()/" data-url-smart="lwtZkwHMCw3-WsZOCobd5PlD3_A=/90x0/smart/filters:strip_icc()/" data-url-feature="lwtZkwHMCw3-WsZOCobd5PlD3_A=/90x0/smart/filters:strip_icc()/" data-url-tablet="UerusqE_7LBjX5Bn6VD54ZdenEA=/70x50/smart/filters:strip_icc()/" data-url-desktop="sS3wHMghIB_smdu-buorUR9-lHo=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Governador passa fim de semana com luxo em Paris e provoca polÃªmica no PR</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-13 11:09:56" class="chamada chamada-principal mobile-grid-full"><a href="http://www.techtudo.com.br/listas/noticia/2015/10/iphone-esquentando-e-normal-entenda-os-motivos-e-veja-como-resolver.html" class="foto" title="Seu iPhone fica quente? 
ConheÃ§a os motivos e evite estragar o seu smartphone (TechTudo)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/A6KByG1L5Zs5mLa8UE3eerZxZmw=/filters:quality(10):strip_icc()/s2.glbimg.com/tp9LkN1GNtAJai_iyVC_mxF7p5E=/5x0:664x439/120x80/s.glbimg.com/po/tt2/f/original/2015/10/13/arrkq5tea7zxgq4gt-xgjlo0ibp94k2tw8bj20uzib7b_copy.jpg" alt="Seu iPhone fica quente? 
ConheÃ§a os motivos e evite estragar o seu smartphone (TechTudo)" title="Seu iPhone fica quente? 
ConheÃ§a os motivos e evite estragar o seu smartphone (TechTudo)"
                data-original-image="s2.glbimg.com/tp9LkN1GNtAJai_iyVC_mxF7p5E=/5x0:664x439/120x80/s.glbimg.com/po/tt2/f/original/2015/10/13/arrkq5tea7zxgq4gt-xgjlo0ibp94k2tw8bj20uzib7b_copy.jpg" data-url-smart_horizontal="Pmx8b-QpquUXbZ86_RHu-vOqOoA=/90x0/smart/filters:strip_icc()/" data-url-smart="Pmx8b-QpquUXbZ86_RHu-vOqOoA=/90x0/smart/filters:strip_icc()/" data-url-feature="Pmx8b-QpquUXbZ86_RHu-vOqOoA=/90x0/smart/filters:strip_icc()/" data-url-tablet="_oMNjYIOjBZa5Zkl5L85H5EN_7A=/70x50/smart/filters:strip_icc()/" data-url-desktop="itGBnMKHReRG-O49J6Ug_yrNLds=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Seu iPhone fica quente? <br />ConheÃ§a os motivos e evite <br />estragar o seu smartphone</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-13 21:22:15" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/sao-paulo/itapetininga-regiao/noticia/2015/10/suspeita-de-obrigar-filha-fazer-sexo-e-fria-e-egoista-diz-delegada.html" class="" title="Suspeita de obrigar filha de 12 anos a fazer sexo em SP &#39;Ã© fria e egoÃ­sta&#39;, diz delegada (Jurandir Felix/Arquivo Pessoal)" rel="bookmark"><span class="conteudo"><p>Suspeita de obrigar filha de 12 anos a fazer sexo em SP &#39;Ã© fria e egoÃ­sta&#39;, diz delegada</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-13 19:57:41" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/mundo/noticia/2015/10/brasileira-desmaia-e-morre-durante-visita-machu-picchu.html" class="foto" title="Brasileira desmaia, Ã© atendida por turista, mas morre em Machu Picchu (BBC)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/qtvDQQe23VxxAedFaKymZ6Q0M1E=/filters:quality(10):strip_icc()/s2.glbimg.com/T4CEO0X0jh2VF9kvYjBYgfvHt04=/19x19:490x333/120x80/s.glbimg.com/jo/g1/f/original/2015/08/22/150820144857_lonely_planet_.jpg" alt="Brasileira desmaia, Ã© atendida por turista, mas morre em Machu Picchu (BBC)" title="Brasileira desmaia, Ã© atendida por turista, mas morre em Machu Picchu (BBC)"
                data-original-image="s2.glbimg.com/T4CEO0X0jh2VF9kvYjBYgfvHt04=/19x19:490x333/120x80/s.glbimg.com/jo/g1/f/original/2015/08/22/150820144857_lonely_planet_.jpg" data-url-smart_horizontal="pdTh9p4HrLhL1Tq8B3BYG41xsPA=/90x0/smart/filters:strip_icc()/" data-url-smart="pdTh9p4HrLhL1Tq8B3BYG41xsPA=/90x0/smart/filters:strip_icc()/" data-url-feature="pdTh9p4HrLhL1Tq8B3BYG41xsPA=/90x0/smart/filters:strip_icc()/" data-url-tablet="qAlyaRLZSq3aHPo1OphItB9ptXE=/70x50/smart/filters:strip_icc()/" data-url-desktop="A-fHkJX6tcYDIC0WvmiVXQxJX5w=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Brasileira desmaia, Ã© atendida por turista, mas morre em Machu Picchu</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-13 18:35:14" class="chamada chamada-principal mobile-grid-full"><a href="http://revistamarieclaire.globo.com/Web/noticia/2015/10/tia-processa-sobrinho-de-12-anos-em-r-500-mil-apos-quebrar-o-pulso-em-abraco.html" class="foto" title="Tia processa crianÃ§a e
 pede R$ 500 mil apÃ³s cumprimento &#39;efusivo&#39; (ReproduÃ§Ã£o/Facebook)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/me3FiNTo_KEWsvcWuXnf42vQ0eA=/filters:quality(10):strip_icc()/s2.glbimg.com/ajwLK1Vmd71gmQRPeKpqO7bhMs8=/0x91:448x390/120x80/s.glbimg.com/en/ho/f/original/2015/10/13/tia.jpg" alt="Tia processa crianÃ§a e
 pede R$ 500 mil apÃ³s cumprimento &#39;efusivo&#39; (ReproduÃ§Ã£o/Facebook)" title="Tia processa crianÃ§a e
 pede R$ 500 mil apÃ³s cumprimento &#39;efusivo&#39; (ReproduÃ§Ã£o/Facebook)"
                data-original-image="s2.glbimg.com/ajwLK1Vmd71gmQRPeKpqO7bhMs8=/0x91:448x390/120x80/s.glbimg.com/en/ho/f/original/2015/10/13/tia.jpg" data-url-smart_horizontal="Uc7dT1Oc5ZIylbb5I1uPiAlZHKY=/90x0/smart/filters:strip_icc()/" data-url-smart="Uc7dT1Oc5ZIylbb5I1uPiAlZHKY=/90x0/smart/filters:strip_icc()/" data-url-feature="Uc7dT1Oc5ZIylbb5I1uPiAlZHKY=/90x0/smart/filters:strip_icc()/" data-url-tablet="YzMyiOTWuhw_9iuaNsXci_RxmYY=/70x50/smart/filters:strip_icc()/" data-url-desktop="Gf7npUNE45DS5UjMOTwonbPDGM4=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Tia processa crianÃ§a e<br /> pede R$ 500 mil apÃ³s cumprimento &#39;efusivo&#39;</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section><section class="area sports-column   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/eliminatorias-da-eurocopa/noticia/2015/10/nova-lider-do-ranking-fifa-belgica-bate-israel-e-avanca-euro-em-primeiro.html" class="foto" title="Que dia! BÃ©lgica vence, avanÃ§a Ã  Euro e assume lideranÃ§a do ranking da Fifa (Reuters)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/RvUpFdMZ-8Vh4fzoqeNAAMzdme0=/filters:quality(10):strip_icc()/s2.glbimg.com/1H6IYxNwi6mWlZ92dfop0btwN_I=/83x86:1828x1024/335x180/s.glbimg.com/es/ge/f/original/2015/10/13/2015-10-13t204529z_317297934_gf10000243902_rtrmadp_3_soccer-euro_1.jpg" alt="Que dia! BÃ©lgica vence, avanÃ§a Ã  Euro e assume lideranÃ§a do ranking da Fifa (Reuters)" title="Que dia! BÃ©lgica vence, avanÃ§a Ã  Euro e assume lideranÃ§a do ranking da Fifa (Reuters)"
                data-original-image="s2.glbimg.com/1H6IYxNwi6mWlZ92dfop0btwN_I=/83x86:1828x1024/335x180/s.glbimg.com/es/ge/f/original/2015/10/13/2015-10-13t204529z_317297934_gf10000243902_rtrmadp_3_soccer-euro_1.jpg" data-url-smart_horizontal="QgSDjhNq80YIhQBuq-mmk5L89Oo=/90x0/smart/filters:strip_icc()/" data-url-smart="QgSDjhNq80YIhQBuq-mmk5L89Oo=/90x0/smart/filters:strip_icc()/" data-url-feature="QgSDjhNq80YIhQBuq-mmk5L89Oo=/90x0/smart/filters:strip_icc()/" data-url-tablet="RCJgnD6PzS-WQ0eQ8ENSX0mMuL8=/220x125/smart/filters:strip_icc()/" data-url-desktop="ydIGhmjFm8UsxWBcWl2k78TEzyc=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Que dia! BÃ©lgica vence, avanÃ§a Ã  Euro <br />e assume lideranÃ§a do ranking da Fifa</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/futebol-internacional/jogo/13-10-2015/holanda-republica-tcheca/" class="foto" title="Vexame! Van Persie marca contra, Holanda perde em casa e nÃ£o vai para a Euro (EFE/KOEN VAN WEEL)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/pN8aGJ-eXaf9SgrY8ozDlBbChm8=/filters:quality(10):strip_icc()/s2.glbimg.com/JrPcGHI7wtvbMscezS61-7CHdj0=/424x94:732x299/120x80/s.glbimg.com/es/ge/f/original/2015/10/13/635803708958368979w.jpg" alt="Vexame! Van Persie marca contra, Holanda perde em casa e nÃ£o vai para a Euro (EFE/KOEN VAN WEEL)" title="Vexame! Van Persie marca contra, Holanda perde em casa e nÃ£o vai para a Euro (EFE/KOEN VAN WEEL)"
                data-original-image="s2.glbimg.com/JrPcGHI7wtvbMscezS61-7CHdj0=/424x94:732x299/120x80/s.glbimg.com/es/ge/f/original/2015/10/13/635803708958368979w.jpg" data-url-smart_horizontal="XFqRjBEe8wUh-BN0z_dmd1TxG7I=/90x0/smart/filters:strip_icc()/" data-url-smart="XFqRjBEe8wUh-BN0z_dmd1TxG7I=/90x0/smart/filters:strip_icc()/" data-url-feature="XFqRjBEe8wUh-BN0z_dmd1TxG7I=/90x0/smart/filters:strip_icc()/" data-url-tablet="knSDblUOK4SIbX0BSAKokX3PIbU=/70x50/smart/filters:strip_icc()/" data-url-desktop="4HGnGnfSPOoW_L5cjqQOtYrbgJc=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Vexame! Van Persie marca contra, Holanda perde em casa e nÃ£o vai para a Euro</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-13 20:40:10" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/sao-paulo/noticia/2015/10/convite-de-leco-ataide-volta-ser-vice-de-futebol-do-sao-paulo.html" class="foto" title="A convite de Leco, pivÃ´ de briga com Aidar retoma seu cargo no SÃ£o Paulo (LEANDRO MARTINS/FRAME/ESTADÃO CONTEÃDO)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/Lbccd_CevVELJagmlCL22EtUE-Q=/filters:quality(10):strip_icc()/s2.glbimg.com/bzXiRhNwPRSeRR8Dq_KzvDhIXYQ=/288x184:1493x988/120x80/s.glbimg.com/es/ge/f/original/2015/08/25/frm20150825044_1.jpg" alt="A convite de Leco, pivÃ´ de briga com Aidar retoma seu cargo no SÃ£o Paulo (LEANDRO MARTINS/FRAME/ESTADÃO CONTEÃDO)" title="A convite de Leco, pivÃ´ de briga com Aidar retoma seu cargo no SÃ£o Paulo (LEANDRO MARTINS/FRAME/ESTADÃO CONTEÃDO)"
                data-original-image="s2.glbimg.com/bzXiRhNwPRSeRR8Dq_KzvDhIXYQ=/288x184:1493x988/120x80/s.glbimg.com/es/ge/f/original/2015/08/25/frm20150825044_1.jpg" data-url-smart_horizontal="PCSrtjDeg5HYvMbQQtq4hj691RU=/90x0/smart/filters:strip_icc()/" data-url-smart="PCSrtjDeg5HYvMbQQtq4hj691RU=/90x0/smart/filters:strip_icc()/" data-url-feature="PCSrtjDeg5HYvMbQQtq4hj691RU=/90x0/smart/filters:strip_icc()/" data-url-tablet="6MSHg88GQHOEgu7Q2n9KcSKdR0s=/70x50/smart/filters:strip_icc()/" data-url-desktop="EPQjxKwpjVN70YrctzwjRWq4tWc=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>A convite de Leco, pivÃ´ de briga com Aidar retoma seu cargo no SÃ£o Paulo</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/futebol-internacional/futebol-frances/noticia/2015/10/cisse-e-liberado-pela-policia-horas-apos-ser-preso-por-suposta-chantagem.html" class="" title="CissÃ© Ã© liberado pela polÃ­cia depois de ser detido por suspeita de chantagear Valbuena (Reuters)" rel="bookmark"><span class="conteudo"><p>CissÃ© Ã© liberado pela polÃ­cia depois de ser detido por suspeita de chantagear Valbuena</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/sensual/noticia/2015/10/conheca-modelo-loira-que-encantou-neymar.html" class="foto" title="Loira flagrada com Neymar Ã© a mesma que teve vÃ­deo vazado com ex-GrÃªmio (Walmor Oliveira/DivulgaÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/Y5UQOIDxovYq4FMIXm4a0oPPhnM=/filters:quality(10):strip_icc()/s2.glbimg.com/DogWSJq2jl8UNjhE098mvAcEGCQ=/0x47:500x380/120x80/s.glbimg.com/jo/eg/f/original/2015/10/13/20151013130330.jpg" alt="Loira flagrada com Neymar Ã© a mesma que teve vÃ­deo vazado com ex-GrÃªmio (Walmor Oliveira/DivulgaÃ§Ã£o)" title="Loira flagrada com Neymar Ã© a mesma que teve vÃ­deo vazado com ex-GrÃªmio (Walmor Oliveira/DivulgaÃ§Ã£o)"
                data-original-image="s2.glbimg.com/DogWSJq2jl8UNjhE098mvAcEGCQ=/0x47:500x380/120x80/s.glbimg.com/jo/eg/f/original/2015/10/13/20151013130330.jpg" data-url-smart_horizontal="-QOuWx3T6R9-ll2Hn_EpHCkLX1M=/90x0/smart/filters:strip_icc()/" data-url-smart="-QOuWx3T6R9-ll2Hn_EpHCkLX1M=/90x0/smart/filters:strip_icc()/" data-url-feature="-QOuWx3T6R9-ll2Hn_EpHCkLX1M=/90x0/smart/filters:strip_icc()/" data-url-tablet="4-EB59rVD8O4pp6KLJ8c33FSbuE=/70x50/smart/filters:strip_icc()/" data-url-desktop="_3keBZvP_QORWhsktHqnrTiDP1I=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Loira flagrada com Neymar Ã© a mesma que teve vÃ­deo vazado com ex-GrÃªmio</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/10/lamar-odom-e-hospitalizado-apos-ser-achado-inconsciente-em-bordel-diz-site.html" class="foto" title="Jogador da NBA Ã© levado a hospital apÃ³s ser achado inconsciente em bordel (Getty Images)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/1fMj1TY3SGn6wFlF7Y8O92X7xvg=/filters:quality(10):strip_icc()/s2.glbimg.com/ZYGiP-j6AtG7sv0b1iRFxkZKd9s=/0x60:607x465/120x80/e.glbimg.com/og/ed/f/original/2015/08/13/khloe.jpg" alt="Jogador da NBA Ã© levado a hospital apÃ³s ser achado inconsciente em bordel (Getty Images)" title="Jogador da NBA Ã© levado a hospital apÃ³s ser achado inconsciente em bordel (Getty Images)"
                data-original-image="s2.glbimg.com/ZYGiP-j6AtG7sv0b1iRFxkZKd9s=/0x60:607x465/120x80/e.glbimg.com/og/ed/f/original/2015/08/13/khloe.jpg" data-url-smart_horizontal="DbHycs3tvLA5gebEcKb029wugkU=/90x0/smart/filters:strip_icc()/" data-url-smart="DbHycs3tvLA5gebEcKb029wugkU=/90x0/smart/filters:strip_icc()/" data-url-feature="DbHycs3tvLA5gebEcKb029wugkU=/90x0/smart/filters:strip_icc()/" data-url-tablet="iAyS6HJUBzNcOVAoLGJeikEUbn0=/70x50/smart/filters:strip_icc()/" data-url-desktop="sXM9jWYiJCbhXR2PSjJwxNSvczA=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Jogador da NBA Ã© levado<br /> a hospital apÃ³s ser achado inconsciente em bordel</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-13 22:58:52" class="chamada chamada-principal mobile-grid-full"><a href="http://sportv.globo.com/site/combate/noticia/2015/10/mae-de-ronda-chama-treinador-da-filha-de-terrivel-e-critica-pessoa-ruim.html" class="" title="MÃ£e de Ronda dispara contra tÃ©cnico da filha: &#39;Ã um treinador terrÃ­vel e uma pessoa ruim&#39;" rel="bookmark"><span class="conteudo"><p>MÃ£e de Ronda dispara contra tÃ©cnico da filha: &#39;Ã um treinador terrÃ­vel e uma pessoa ruim&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-13 23:00:43" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol-americano/noticia/2015/10/jogador-da-nfl-posa-com-dentadura-formada-por-ouro-e-300-diamantes.html" class="foto" title="Derrota nÃ£o diminui Ã¢nimo de jogador da NFL de exibir &#39;dente&#39; de ouro e diamante (ReproduÃ§Ã£o Instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/dsAIkw7Sf7dfO-3cplprbzhhkQ4=/filters:quality(10):strip_icc()/s2.glbimg.com/p74mBcUz1hhiUxpQZBX9YJGU_fQ=/14x84:605x478/120x80/s.glbimg.com/es/ge/f/original/2015/10/13/keenan.jpg" alt="Derrota nÃ£o diminui Ã¢nimo de jogador da NFL de exibir &#39;dente&#39; de ouro e diamante (ReproduÃ§Ã£o Instagram)" title="Derrota nÃ£o diminui Ã¢nimo de jogador da NFL de exibir &#39;dente&#39; de ouro e diamante (ReproduÃ§Ã£o Instagram)"
                data-original-image="s2.glbimg.com/p74mBcUz1hhiUxpQZBX9YJGU_fQ=/14x84:605x478/120x80/s.glbimg.com/es/ge/f/original/2015/10/13/keenan.jpg" data-url-smart_horizontal="oc8-WA1iXo6wxdtNN5qR3i5dLAA=/90x0/smart/filters:strip_icc()/" data-url-smart="oc8-WA1iXo6wxdtNN5qR3i5dLAA=/90x0/smart/filters:strip_icc()/" data-url-feature="oc8-WA1iXo6wxdtNN5qR3i5dLAA=/90x0/smart/filters:strip_icc()/" data-url-tablet="m0YqBFbviCBBCqYjtTHxcUbKgiY=/70x50/smart/filters:strip_icc()/" data-url-desktop="VXMf3TCXvIEERhd5Hx5LlLTbZx4=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Derrota nÃ£o diminui Ã¢nimo de jogador da NFL de exibir &#39;dente&#39; de ouro e diamante</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-13 18:12:32" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/natacao/noticia/2015/10/esposa-de-cielo-festeja-primeiro-dia-das-criancas-do-filho-nosso-baby.html" class="foto" title="Esposa de Cielo posta foto do filho pela primeira vez com tubarÃ£o no macacÃ£o (ReproduÃ§Ã£o/Instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/355kvRRzJ1B-96yMBFPBj76CFkM=/filters:quality(10):strip_icc()/s2.glbimg.com/-LnGCEKQMiD8AchPI-IuMsL3rhA=/86x27:585x359/120x80/s.glbimg.com/es/ge/f/original/2015/10/13/cielo.jpg" alt="Esposa de Cielo posta foto do filho pela primeira vez com tubarÃ£o no macacÃ£o (ReproduÃ§Ã£o/Instagram)" title="Esposa de Cielo posta foto do filho pela primeira vez com tubarÃ£o no macacÃ£o (ReproduÃ§Ã£o/Instagram)"
                data-original-image="s2.glbimg.com/-LnGCEKQMiD8AchPI-IuMsL3rhA=/86x27:585x359/120x80/s.glbimg.com/es/ge/f/original/2015/10/13/cielo.jpg" data-url-smart_horizontal="PMrsbPfjwzyAeiSCGfQIRvUZXhY=/90x0/smart/filters:strip_icc()/" data-url-smart="PMrsbPfjwzyAeiSCGfQIRvUZXhY=/90x0/smart/filters:strip_icc()/" data-url-feature="PMrsbPfjwzyAeiSCGfQIRvUZXhY=/90x0/smart/filters:strip_icc()/" data-url-tablet="zI9dp0WKOTmYxw6-KO7Hct9Irg8=/70x50/smart/filters:strip_icc()/" data-url-desktop="3zWRJOGdrqppWSGnslNrNQIP_oA=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Esposa de Cielo posta foto do filho pela primeira vez com tubarÃ£o no macacÃ£o</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section><section class="area etc-column analytics-area   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/biquini/noticia/2015/10/ticiane-pinheiro-posa-de-biquini-em-ferias-com-o-namorado-cesar-tralli.html" class="foto" title="Tici Pinheiro posa de biquÃ­ni para Tralli na piscina privativa de quarto de hotel (Instagram / ReproduÃ§Ã£o)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/0tpvQdRnWkoWFyy2gpP8e-dz53M=/filters:quality(10):strip_icc()/s2.glbimg.com/HAvqWiKtrIdyjKY_xpT7S0B2j5s=/73x223:521x464/335x180/s.glbimg.com/jo/eg/f/original/2015/10/13/12142003_1507134709603727_1563780494_n.jpg" alt="Tici Pinheiro posa de biquÃ­ni para Tralli na piscina privativa de quarto de hotel (Instagram / ReproduÃ§Ã£o)" title="Tici Pinheiro posa de biquÃ­ni para Tralli na piscina privativa de quarto de hotel (Instagram / ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/HAvqWiKtrIdyjKY_xpT7S0B2j5s=/73x223:521x464/335x180/s.glbimg.com/jo/eg/f/original/2015/10/13/12142003_1507134709603727_1563780494_n.jpg" data-url-smart_horizontal="DhO9xWh0uBCUHYw2PT_SKN2j2CA=/90x0/smart/filters:strip_icc()/" data-url-smart="DhO9xWh0uBCUHYw2PT_SKN2j2CA=/90x0/smart/filters:strip_icc()/" data-url-feature="DhO9xWh0uBCUHYw2PT_SKN2j2CA=/90x0/smart/filters:strip_icc()/" data-url-tablet="Fvtq5bv6vJPULb05WRcYryWFC1c=/220x125/smart/filters:strip_icc()/" data-url-desktop="HDfvmtWG5iC3qxXT_tw0pkRKshQ=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Tici Pinheiro posa de biquÃ­ni para Tralli na piscina privativa de quarto de hotel</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-13 13:39:33" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/famosos/mao-de-giovanna-ewbank-some-em-foto-bruno-gagliasso-faz-piada-esta-linda-17761132.html" class="foto" title="IlusÃ£o de Ã³tica? Ewbank &#39;perde&#39; a mÃ£o em foto e Gagliasso zoa: &#39;Ficou linda&#39; (ReproduÃ§Ã£o / Instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/N0sSjyhogRQgQ9CreVMyGTQ8Jlg=/filters:quality(10):strip_icc()/s2.glbimg.com/P99mYmYiFcpcvuRhBTmyzMytvWk=/91x79:402x286/120x80/s.glbimg.com/en/ho/f/original/2015/10/13/giovana24_1.jpg" alt="IlusÃ£o de Ã³tica? Ewbank &#39;perde&#39; a mÃ£o em foto e Gagliasso zoa: &#39;Ficou linda&#39; (ReproduÃ§Ã£o / Instagram)" title="IlusÃ£o de Ã³tica? Ewbank &#39;perde&#39; a mÃ£o em foto e Gagliasso zoa: &#39;Ficou linda&#39; (ReproduÃ§Ã£o / Instagram)"
                data-original-image="s2.glbimg.com/P99mYmYiFcpcvuRhBTmyzMytvWk=/91x79:402x286/120x80/s.glbimg.com/en/ho/f/original/2015/10/13/giovana24_1.jpg" data-url-smart_horizontal="73EK0BrgDNKVeNlXUAglMnxUm9Q=/90x0/smart/filters:strip_icc()/" data-url-smart="73EK0BrgDNKVeNlXUAglMnxUm9Q=/90x0/smart/filters:strip_icc()/" data-url-feature="73EK0BrgDNKVeNlXUAglMnxUm9Q=/90x0/smart/filters:strip_icc()/" data-url-tablet="E8Q_91fOlcdDWirF2t3ExNFZs5g=/70x50/smart/filters:strip_icc()/" data-url-desktop="nbwzc0btrvQB4gVv6iwI7s0KNJ0=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>IlusÃ£o de Ã³tica? Ewbank &#39;perde&#39; a mÃ£o em foto e Gagliasso zoa: &#39;Ficou linda&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-13 18:11:57" class="chamada chamada-principal mobile-grid-full"><a href="http://gshow.globo.com/Musica/noticia/2015/10/anitta-lanca-novo-disco-bang-queria-inovar-e-acho-que-consegui.html" class="foto" title="Anitta lanÃ§a novo disco e provoca com gesto obsceno em jaqueta (Carol Caminha / Gshow)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/GGk_QmF8IhhmRQfZ_Cel4rdWSho=/filters:quality(10):strip_icc()/s2.glbimg.com/YnCDF4vxRvX4KzMSpiW4ZBxRpvA=/114x81:569x385/120x80/s.glbimg.com/et/gs/f/original/2015/10/13/anitta_1.jpg" alt="Anitta lanÃ§a novo disco e provoca com gesto obsceno em jaqueta (Carol Caminha / Gshow)" title="Anitta lanÃ§a novo disco e provoca com gesto obsceno em jaqueta (Carol Caminha / Gshow)"
                data-original-image="s2.glbimg.com/YnCDF4vxRvX4KzMSpiW4ZBxRpvA=/114x81:569x385/120x80/s.glbimg.com/et/gs/f/original/2015/10/13/anitta_1.jpg" data-url-smart_horizontal="TPTZ6pcsNbPgM9GRffuwoY_K4-A=/90x0/smart/filters:strip_icc()/" data-url-smart="TPTZ6pcsNbPgM9GRffuwoY_K4-A=/90x0/smart/filters:strip_icc()/" data-url-feature="TPTZ6pcsNbPgM9GRffuwoY_K4-A=/90x0/smart/filters:strip_icc()/" data-url-tablet="WN_NSVKsRjMAJu20XtQmGcLU-5g=/70x50/smart/filters:strip_icc()/" data-url-desktop="YbU8IRrQGoWIfDj20uAYVl0qSvk=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Anitta lanÃ§a novo disco<br /> e provoca com gesto obsceno em jaqueta</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-13 14:00:17" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/moda/noticia/2015/10/sthefany-brito-sobre-divorcio-de-pato-veio-para-me-ensinar-algumas-coisas.html" class="" title="Aos 28 anos, Sthefany posa para ensaio e 
diz ver &#39;lado bom&#39; em divÃ³rcio de Pato (Iwi Onodera / EGO)" rel="bookmark"><span class="conteudo"><p>Aos 28 anos, Sthefany posa para ensaio e <br />diz ver &#39;lado bom&#39; em divÃ³rcio de Pato</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-13 18:29:13" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/10/gyselle-soares-da-boa-noite-seguidores-com-body-transparente.html" class="foto" title="Ex-BBB mostra demais ao dar boa noite a seguidores com look transparente (ReproduÃ§Ã£o/Instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/0YICqTtLBlZHqXlvgItpkQSoMw8=/filters:quality(10):strip_icc()/s2.glbimg.com/1HIuzSHH2XFpUD_TvqnK2rJ0vOY=/192x48:482x242/120x80/s.glbimg.com/jo/eg/f/original/2015/10/13/12070947_646190795483937_1883101887_n.jpg" alt="Ex-BBB mostra demais ao dar boa noite a seguidores com look transparente (ReproduÃ§Ã£o/Instagram)" title="Ex-BBB mostra demais ao dar boa noite a seguidores com look transparente (ReproduÃ§Ã£o/Instagram)"
                data-original-image="s2.glbimg.com/1HIuzSHH2XFpUD_TvqnK2rJ0vOY=/192x48:482x242/120x80/s.glbimg.com/jo/eg/f/original/2015/10/13/12070947_646190795483937_1883101887_n.jpg" data-url-smart_horizontal="zh082y1DDLLiYU3a2XAP2Iw-mjQ=/90x0/smart/filters:strip_icc()/" data-url-smart="zh082y1DDLLiYU3a2XAP2Iw-mjQ=/90x0/smart/filters:strip_icc()/" data-url-feature="zh082y1DDLLiYU3a2XAP2Iw-mjQ=/90x0/smart/filters:strip_icc()/" data-url-tablet="r5kw5K7_JhrnbvCfIQvGuvitQdI=/70x50/smart/filters:strip_icc()/" data-url-desktop="ntgrNWbqfb2jUoG4k2Ahln1amt0=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Ex-BBB mostra demais ao dar boa noite a seguidores com look transparente</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-13 16:59:29" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/10/juju-salimeni-posta-foto-de-maio-e-coxa-sarada-chama-atencao.html" class="foto" title="Juju Salimeni impressiona pelas &#39;supercoxas&#39; em foto de maiÃ´ postada por ela (ReproduÃ§Ã£o / Instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/ybW4yTpepN01TdAGgy2Z9FiuUQ4=/filters:quality(10):strip_icc()/s2.glbimg.com/yT8BOSU8TqAogzqT7GAGFKi3Pzc=/0x32:480x352/120x80/s.glbimg.com/jo/eg/f/original/2015/10/13/12145263_711614778969709_1898610540_n.jpg" alt="Juju Salimeni impressiona pelas &#39;supercoxas&#39; em foto de maiÃ´ postada por ela (ReproduÃ§Ã£o / Instagram)" title="Juju Salimeni impressiona pelas &#39;supercoxas&#39; em foto de maiÃ´ postada por ela (ReproduÃ§Ã£o / Instagram)"
                data-original-image="s2.glbimg.com/yT8BOSU8TqAogzqT7GAGFKi3Pzc=/0x32:480x352/120x80/s.glbimg.com/jo/eg/f/original/2015/10/13/12145263_711614778969709_1898610540_n.jpg" data-url-smart_horizontal="WgyBjrYI3FODLkvHmcNDtk_Y4bc=/90x0/smart/filters:strip_icc()/" data-url-smart="WgyBjrYI3FODLkvHmcNDtk_Y4bc=/90x0/smart/filters:strip_icc()/" data-url-feature="WgyBjrYI3FODLkvHmcNDtk_Y4bc=/90x0/smart/filters:strip_icc()/" data-url-tablet="CaNtzL9nYCFaCNN5cjnjJlMbVLo=/70x50/smart/filters:strip_icc()/" data-url-desktop="nIkMpOm-DuV05P7_cDOYwWI4Zq4=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Juju Salimeni impressiona pelas &#39;supercoxas&#39; em foto de maiÃ´ postada por ela</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-13 15:28:22" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/10/gianecchini-sobre-nudez-em-novela-fiquei-4-horas-pelado-amarradao.html" class="" title="Gianecchini relembra cenas quentes em &#39;Verdades&#39;: &#39;Fiquei 4h pelado amarradÃ£o&#39; (ReproduÃ§Ã£o/Instagram)" rel="bookmark"><span class="conteudo"><p>Gianecchini relembra cenas quentes em &#39;Verdades&#39;: &#39;Fiquei 4h pelado amarradÃ£o&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-13 10:31:46" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/10/andressa-urach-posta-foto-de-antes-e-depois.html" class="foto" title="Hoje evangÃ©lica, Andressa Urach posta foto de &#39;antes e depois&#39; e cita seu livro (ReproduÃ§Ã£o/Instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/RlYntN9IyuKaqTwe0AquhF72U5I=/filters:quality(10):strip_icc()/s2.glbimg.com/WcnF5ecwJF0OxfUTbm3q-Zc61co=/9x15:742x504/120x80/s.glbimg.com/jo/eg/f/original/2015/10/13/andressa_urach_antes_e_depois.jpg" alt="Hoje evangÃ©lica, Andressa Urach posta foto de &#39;antes e depois&#39; e cita seu livro (ReproduÃ§Ã£o/Instagram)" title="Hoje evangÃ©lica, Andressa Urach posta foto de &#39;antes e depois&#39; e cita seu livro (ReproduÃ§Ã£o/Instagram)"
                data-original-image="s2.glbimg.com/WcnF5ecwJF0OxfUTbm3q-Zc61co=/9x15:742x504/120x80/s.glbimg.com/jo/eg/f/original/2015/10/13/andressa_urach_antes_e_depois.jpg" data-url-smart_horizontal="k81B8TNYPjxmQ33Ep6ByzOjb9Mo=/90x0/smart/filters:strip_icc()/" data-url-smart="k81B8TNYPjxmQ33Ep6ByzOjb9Mo=/90x0/smart/filters:strip_icc()/" data-url-feature="k81B8TNYPjxmQ33Ep6ByzOjb9Mo=/90x0/smart/filters:strip_icc()/" data-url-tablet="L7W-jxHkxenqqzfA-1vgIhOP-fw=/70x50/smart/filters:strip_icc()/" data-url-desktop="kJhiXySnQ-5ilckk_sorEM521YM=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Hoje evangÃ©lica, Andressa Urach posta foto de &#39;antes e depois&#39; e cita seu livro</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry publieditorial"><div data-photo-subtitle="2015-10-11 18:00:51" class="chamada chamada-principal mobile-grid-full"><a href="http://gshow.globo.com/programas/estrelas/ep/dog-chow/noticia/2015/10/dona-do-cara-de-fuinha-se-derrete-com-cenas-inusitadas-do-seu-cao.html?utm_source=home-globocom&amp;utm_medium=fake-banner&amp;utm_term=15-10-13&amp;utm_content=dog-chow&amp;utm_campaign=dog-chow" class="foto" title="Sheron Menezzes fala das estripulias do seu cÃ£o no Estrelas. Confira! (DivulgaÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/GjX7UY_Gj9pr6e0FQFqrJNfi0Lw=/filters:quality(10):strip_icc()/s2.glbimg.com/Uf4ZpFn8WrNvfGxKVsl9UhYuwYk=/222x78:584x319/120x80/s.glbimg.com/en/ho/f/original/2015/10/13/angelicaestrelas.jpg" alt="Sheron Menezzes fala das estripulias do seu cÃ£o no Estrelas. Confira! (DivulgaÃ§Ã£o)" title="Sheron Menezzes fala das estripulias do seu cÃ£o no Estrelas. Confira! (DivulgaÃ§Ã£o)"
                data-original-image="s2.glbimg.com/Uf4ZpFn8WrNvfGxKVsl9UhYuwYk=/222x78:584x319/120x80/s.glbimg.com/en/ho/f/original/2015/10/13/angelicaestrelas.jpg" data-url-smart_horizontal="Hs1axgTyB8Nyc7K1y6_goQwfgjA=/90x0/smart/filters:strip_icc()/" data-url-smart="Hs1axgTyB8Nyc7K1y6_goQwfgjA=/90x0/smart/filters:strip_icc()/" data-url-feature="Hs1axgTyB8Nyc7K1y6_goQwfgjA=/90x0/smart/filters:strip_icc()/" data-url-tablet="BRB89VIlNbtsBpIuB0-JoywDv58=/70x50/smart/filters:strip_icc()/" data-url-desktop="UijpcuNA78wp0YWqWwHaW9qV4VU=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><span>ESPECIAL PUBLICITÃRIO</span><p>Sheron Menezzes fala das estripulias do seu cÃ£o no Estrelas. Confira!</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section></div><div id="ad-position-middle3" class="opec"><div id="banner_slb_fim" class="tag-manager-publicidade-container"></div></div><div class="container columns clearfix glb-area-colunas second-area"><section class="area news-column"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-noticias
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}});}
</script><div id="libby-home-widget-economia" class="libby-home-widgetBase libby-home-widget-economia analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais notÃ­cias" href="http://g1.globo.com/"><span class="text">mais notÃ­cias</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area sports-column"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-times soccer-teams
"></div><div id="libby-home-widget-brasileirao" class="libby-home-widgetBase libby-home-widget-brasileirao analytics-area analytics-id-W"><input type="hidden" name="rodada_selecionada" value="current"></div><a class="link-see-more analytics-area analytics-id-L" title="mais esportes" href="http://globoesporte.globo.com/"><span class="text">mais esportes</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area etc-column analytics-area"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-novelas
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2015/", "logo": "http://s2.glbimg.com/tjsQl1SMvddbD-BGiSoC2Nb6kqg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-36x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2015/rss/", "logo_tv": "http://s2.glbimg.com/y8thjGY4IQuSvc4VfunyXi0d8C4=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/wDQDURw8tQncX6dLdavJtiUCwkE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/07/14/logo38x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/APKhqTvAHfh9O2TFUkXyG2JQqdw=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/07/14/logo45x30.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "I Love Parais\u00f3polis", "url": "http://gshow.globo.com/novelas/i-love-paraisopolis/index.html", "logo": "http://s2.glbimg.com/ChecsIkfHlI4H_yHoe_c_6tXTA4=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis36x20.jpg", "slug": "alto-astral", "url_feed": "http://gshow.globo.com/novelas/i-love-paraisopolis/rss/", "logo_tv": "http://s2.glbimg.com/6Kj2EtayTr5_t0ya4Iy8HHIn-vg=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "A Regra do Jogo", "url": "http://gshow.globo.com/novelas/a-regra-do-jogo/", "logo": "http://s2.glbimg.com/K-Nvvvm7zp2F7zS8TcY6TkIcxoE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-36x20.jpg", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/a-regra-do-jogo/rss/", "logo_tv": "http://s2.glbimg.com/4B7Jk0R6K1HlAAhuZIrDOJp8hbs=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-45x30.jpg"}, {"ordering": 70, "default": false, "cor": "#FF7F00", "name": "Verdades Secretas", "url": "http://gshow.globo.com/novelas/verdades-secretas/index.html", "logo": "http://s2.glbimg.com/ouhqJ9mLuAXh-gcEUW8wk165DUQ=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_1.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/verdades-secretas/rss", "logo_tv": "http://s2.glbimg.com/Huyh0y1AcqgjDpRaBLWCCkQY-Oo=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_2.jpg"}]);}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2015/", "logo": "http://s2.glbimg.com/tjsQl1SMvddbD-BGiSoC2Nb6kqg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-36x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2015/rss/", "logo_tv": "http://s2.glbimg.com/y8thjGY4IQuSvc4VfunyXi0d8C4=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/wDQDURw8tQncX6dLdavJtiUCwkE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/07/14/logo38x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/APKhqTvAHfh9O2TFUkXyG2JQqdw=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/07/14/logo45x30.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "I Love Parais\u00f3polis", "url": "http://gshow.globo.com/novelas/i-love-paraisopolis/index.html", "logo": "http://s2.glbimg.com/ChecsIkfHlI4H_yHoe_c_6tXTA4=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis36x20.jpg", "slug": "alto-astral", "url_feed": "http://gshow.globo.com/novelas/i-love-paraisopolis/rss/", "logo_tv": "http://s2.glbimg.com/6Kj2EtayTr5_t0ya4Iy8HHIn-vg=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "A Regra do Jogo", "url": "http://gshow.globo.com/novelas/a-regra-do-jogo/", "logo": "http://s2.glbimg.com/K-Nvvvm7zp2F7zS8TcY6TkIcxoE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-36x20.jpg", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/a-regra-do-jogo/rss/", "logo_tv": "http://s2.glbimg.com/4B7Jk0R6K1HlAAhuZIrDOJp8hbs=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-45x30.jpg"}, {"ordering": 70, "default": false, "cor": "#FF7F00", "name": "Verdades Secretas", "url": "http://gshow.globo.com/novelas/verdades-secretas/index.html", "logo": "http://s2.glbimg.com/ouhqJ9mLuAXh-gcEUW8wk165DUQ=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_1.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/verdades-secretas/rss", "logo_tv": "http://s2.glbimg.com/Huyh0y1AcqgjDpRaBLWCCkQY-Oo=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_2.jpg"}]);}});}
</script><div id="libby-home-widget-horoscopo" class="libby-home-widgetBase libby-home-widget-horoscopo analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais entretenimento" href="http://variedades.globo.com/"><span class="text">mais variedades</span><span class="arrow">&rsaquo;</span></a></div></section></div><section class="area central container analytics-area analytics-id-O"><div class="separator"></div><section class="agrupador-quadruplo-tecnologia analytics-area analytics-id-T clearfix franja-inferior tecnologia"><div class="cabecalho"><h2><a href="http://www.techtudo.com.br/" title="TECNOLOGIA &amp; GAMES"><span class="word word-0">TECNOLOGIA</span><span class="word word-1">&</span><span class="word word-2">GAMES</span></a></h2><div id="ad-position-x34" class="opec"><div id="banner_selo4" class="tag-manager-publicidade-container"></div></div><aside class="links analytics-area analytics-id-T"><ul><li><a href="http://www.techtudo.com.br/jogos/" title="jogos">jogos</a></li><li><a href="http://www.techtudo.com.br/downloads/" title="downloads">downloads</a></li><li><a href="http://forum.techtudo.com.br/" title="fÃ³rum">fÃ³rum</a></li><li><a href="http://www.techtudo.com.br/mobile/" title="celulares">celulares</a></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaque"><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/noticias/noticia/2015/10/veja-caixas-de-som-baratinhas-que-deixam-o-som-do-android-muito-top.html" title="Veja caixinhas &#39;baratinhas&#39; que turbinam o som do seu Android"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/kLvMijR4K_av4MckvGY-ZuF6dhA=/filters:quality(10):strip_icc()/s2.glbimg.com/Hs9sRGl_AmZJLV1S0YQzd8D150s=/23x0:563x287/245x130/s.glbimg.com/po/tt2/f/original/2015/10/13/mini-caixa-de-som-oex.jpg" alt="Veja caixinhas &#39;baratinhas&#39; que turbinam o som do seu Android (DivulgaÃ§Ã£o)" title="Veja caixinhas &#39;baratinhas&#39; que turbinam o som do seu Android (DivulgaÃ§Ã£o)"
             data-original-image="s2.glbimg.com/Hs9sRGl_AmZJLV1S0YQzd8D150s=/23x0:563x287/245x130/s.glbimg.com/po/tt2/f/original/2015/10/13/mini-caixa-de-som-oex.jpg" data-url-smart_horizontal="xpoNl0M6YPOv4tskAuCtpg3PAeY=/90x56/smart/filters:strip_icc()/" data-url-smart="xpoNl0M6YPOv4tskAuCtpg3PAeY=/90x56/smart/filters:strip_icc()/" data-url-feature="xpoNl0M6YPOv4tskAuCtpg3PAeY=/90x56/smart/filters:strip_icc()/" data-url-tablet="AXIbjbdjAIyVS3LMKHHLX4x5Ke4=/160x96/smart/filters:strip_icc()/" data-url-desktop="VAipl5NgC5UORJf_IgpXTDkXc0s=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Veja caixinhas &#39;baratinhas&#39; que <br />turbinam o som do seu Android</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/dicas-e-tutoriais/noticia/2015/10/veja-como-jogar-games-do-super-nintendo-gratuitamente-no-seu-pc.html" title="Jogue games clÃ¡ssicos do Super Nintendo de graÃ§a e sem baixar"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/t-oZAr3f0WrCD6jPFoIDk0ih3Ps=/filters:quality(10):strip_icc()/s2.glbimg.com/8GuwfKX2ctP2x451vSb0FbddrvE=/0x7:594x323/245x130/s.glbimg.com/po/tt2/f/original/2015/10/13/super-mario-world-nintendo-jogar-online.jpg" alt="Jogue games clÃ¡ssicos do Super Nintendo de graÃ§a e sem baixar (DivulgaÃ§Ã£o)" title="Jogue games clÃ¡ssicos do Super Nintendo de graÃ§a e sem baixar (DivulgaÃ§Ã£o)"
             data-original-image="s2.glbimg.com/8GuwfKX2ctP2x451vSb0FbddrvE=/0x7:594x323/245x130/s.glbimg.com/po/tt2/f/original/2015/10/13/super-mario-world-nintendo-jogar-online.jpg" data-url-smart_horizontal="ngegCSesaT_WTqxqzelzwuIccy0=/90x56/smart/filters:strip_icc()/" data-url-smart="ngegCSesaT_WTqxqzelzwuIccy0=/90x56/smart/filters:strip_icc()/" data-url-feature="ngegCSesaT_WTqxqzelzwuIccy0=/90x56/smart/filters:strip_icc()/" data-url-tablet="9YfFj0vXP9t3334WlPftoELCbkI=/160x96/smart/filters:strip_icc()/" data-url-desktop="qhhB0Bt3qphkRjJkojTGQ2Si1TA=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Jogue games clÃ¡ssicos do Super <br />Nintendo de graÃ§a e sem baixar</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/noticias/noticia/2015/10/galaxy-gran-prime-e-bom-cinco-coisas-para-saber-antes-de-comprar.html" title="Smart &#39;baratinho&#39; tem 2 chips, televisÃ£o e tela grande; conheÃ§a"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/Pki7kScd159PNL0vcJfcJiQGy8M=/filters:quality(10):strip_icc()/s2.glbimg.com/EOL97rWMsUe1qlyoj3oTDpc78ow=/43x15:633x328/245x130/s.glbimg.com/po/tt2/f/original/2015/10/13/gran-prime-1_3.jpg" alt="Smart &#39;baratinho&#39; tem 2 chips, televisÃ£o e tela grande; conheÃ§a (TechTudo)" title="Smart &#39;baratinho&#39; tem 2 chips, televisÃ£o e tela grande; conheÃ§a (TechTudo)"
             data-original-image="s2.glbimg.com/EOL97rWMsUe1qlyoj3oTDpc78ow=/43x15:633x328/245x130/s.glbimg.com/po/tt2/f/original/2015/10/13/gran-prime-1_3.jpg" data-url-smart_horizontal="cHxfFLmW15H5GOB94Vq8BzifQ8Y=/90x56/smart/filters:strip_icc()/" data-url-smart="cHxfFLmW15H5GOB94Vq8BzifQ8Y=/90x56/smart/filters:strip_icc()/" data-url-feature="cHxfFLmW15H5GOB94Vq8BzifQ8Y=/90x56/smart/filters:strip_icc()/" data-url-tablet="3LQdYWqXCdxqb4yS-sLhcXYbsrI=/160x96/smart/filters:strip_icc()/" data-url-desktop="dGNTlS8MZfMcsuFWxAfBbk8Mp_s=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Smart &#39;baratinho&#39; tem 2 chips, televisÃ£o e tela grande; conheÃ§a</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://forum.techtudo.com.br/perguntas/165461/capinha-com-ima-estraga-o-celular" title="Verdade ou mito: capinha pode estragar o smart? UsuÃ¡rios dizem"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/hY6o6Okn1y6Q_5Zt2Ig0OCwELQI=/filters:quality(10):strip_icc()/s2.glbimg.com/b1fd3yOaAPcAUEgLmFHpjZqYttY=/57x37:620x336/245x130/s.glbimg.com/po/tt2/f/original/2015/10/13/img_0154.jpg" alt="Verdade ou mito: capinha pode estragar o smart? UsuÃ¡rios dizem (TechTudo)" title="Verdade ou mito: capinha pode estragar o smart? UsuÃ¡rios dizem (TechTudo)"
             data-original-image="s2.glbimg.com/b1fd3yOaAPcAUEgLmFHpjZqYttY=/57x37:620x336/245x130/s.glbimg.com/po/tt2/f/original/2015/10/13/img_0154.jpg" data-url-smart_horizontal="-bA5BhReo7HMmbnxc7mS0oqPjG0=/90x56/smart/filters:strip_icc()/" data-url-smart="-bA5BhReo7HMmbnxc7mS0oqPjG0=/90x56/smart/filters:strip_icc()/" data-url-feature="-bA5BhReo7HMmbnxc7mS0oqPjG0=/90x56/smart/filters:strip_icc()/" data-url-tablet="px_p3QoUdkyVCi-dfJKKDxvqMzg=/160x96/smart/filters:strip_icc()/" data-url-desktop="KnzXDFacOdV36nOGeOQQGrNFFUA=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Verdade ou mito: capinha pode estragar o smart? UsuÃ¡rios dizem</p></span></a></li></ul></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-tecnologia']);</script><div class="separator"></div><section class="agrupador-sextuplo analytics-area analytics-id-S"><section class="analytics-area analytics-id-E esquerda tvg"><div class="cabecalho"><h2><span class="word word-0">moda</span><span class="word word-1">&amp;</span><span class="word word-2">beleza</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://revistamarieclaire.globo.com/Moda/noticia/2015/10/blogueiras-tiram-5-duvidas-de-moda-plus-size-mais-comuns-da-web.html" alt="Blogueiras tiram as 5 dÃºvidas de moda plus size mais comuns da web"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/xFVR29shrYMvuw5SpXE2OM3y-9g=/filters:quality(10):strip_icc()/s2.glbimg.com/3mupjQwQhr5Ud4ofYSHX4DsfQMU=/16x2:606x382/155x100/s.glbimg.com/en/ho/f/original/2015/10/13/saia-plus.jpg" alt="Blogueiras tiram as 5 dÃºvidas de moda plus size mais comuns da web (REPRODUÃÃO INSTAGRAM PAULA BARTOS/RENATA POSKUS)" title="Blogueiras tiram as 5 dÃºvidas de moda plus size mais comuns da web (REPRODUÃÃO INSTAGRAM PAULA BARTOS/RENATA POSKUS)"
            data-original-image="s2.glbimg.com/3mupjQwQhr5Ud4ofYSHX4DsfQMU=/16x2:606x382/155x100/s.glbimg.com/en/ho/f/original/2015/10/13/saia-plus.jpg" data-url-smart_horizontal="CyAna_f4X1DHLqIsfya4JbbOnr8=/90x56/smart/filters:strip_icc()/" data-url-smart="CyAna_f4X1DHLqIsfya4JbbOnr8=/90x56/smart/filters:strip_icc()/" data-url-feature="CyAna_f4X1DHLqIsfya4JbbOnr8=/90x56/smart/filters:strip_icc()/" data-url-tablet="EdBiEJDI00bVEDd1OL10GAAti9k=/122x75/smart/filters:strip_icc()/" data-url-desktop="LILkWzYwpMKMiQvGTu5K336MHYw=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>elas respondem</h3><p>Blogueiras tiram as 5 dÃºvidas de moda plus size mais comuns da web</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://vogue.globo.com/moda/moda-tendencias/noticia/2015/10/das-quadras-para-passarela-os-abrigos-esportivos-estao-de-volta.html" alt="Das quadras para a passarela: os abrigos esportivos estÃ£o de volta"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/KPOMAnmAUkyBAYRD0Nqns015cUI=/filters:quality(10):strip_icc()/s2.glbimg.com/pcUUst89j9M4MyVxu9xADWH35Fg=/360x83:1139x585/155x100/s.glbimg.com/en/ho/f/original/2015/10/13/abrigos_4.jpg" alt="Das quadras para a passarela: os abrigos esportivos estÃ£o de volta (getty images)" title="Das quadras para a passarela: os abrigos esportivos estÃ£o de volta (getty images)"
            data-original-image="s2.glbimg.com/pcUUst89j9M4MyVxu9xADWH35Fg=/360x83:1139x585/155x100/s.glbimg.com/en/ho/f/original/2015/10/13/abrigos_4.jpg" data-url-smart_horizontal="A9KJ3Z4kzYGk-FmXn2IYxKqC5mE=/90x56/smart/filters:strip_icc()/" data-url-smart="A9KJ3Z4kzYGk-FmXn2IYxKqC5mE=/90x56/smart/filters:strip_icc()/" data-url-feature="A9KJ3Z4kzYGk-FmXn2IYxKqC5mE=/90x56/smart/filters:strip_icc()/" data-url-tablet="4Za-EMg7hO3MUoIO-vKrVjMSyp4=/122x75/smart/filters:strip_icc()/" data-url-desktop="WjFXeO5bVUTSsHKOmxotl8og_Ss=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>retorno triunfal</h3><p>Das quadras para a passarela: os abrigos esportivos estÃ£o de volta</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://gnt.globo.com/moda/votacoes/ecobags-ajudam-compor-looks-casuais-de-mulheres-e-homens.htm" alt="Usaria ou nÃ£o? Avalie as produÃ§Ãµes com ecobags de mulheres e homens"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/z0_akErOLAcjniBbgjeyGoRqL7c=/filters:quality(10):strip_icc()/s2.glbimg.com/2x9-t3K8-zj53BG4_a5nmZOmFIE=/0x0:155x100/155x100/s.glbimg.com/en/ho/f/original/2015/10/12/ecobag2.jpg" alt="Usaria ou nÃ£o? Avalie as produÃ§Ãµes com ecobags de mulheres e homens (ReproduÃ§Ã£o)" title="Usaria ou nÃ£o? Avalie as produÃ§Ãµes com ecobags de mulheres e homens (ReproduÃ§Ã£o)"
            data-original-image="s2.glbimg.com/2x9-t3K8-zj53BG4_a5nmZOmFIE=/0x0:155x100/155x100/s.glbimg.com/en/ho/f/original/2015/10/12/ecobag2.jpg" data-url-smart_horizontal="rAqDZHvlVbKE47DZ_6FBZ6TLtSc=/90x56/smart/filters:strip_icc()/" data-url-smart="rAqDZHvlVbKE47DZ_6FBZ6TLtSc=/90x56/smart/filters:strip_icc()/" data-url-feature="rAqDZHvlVbKE47DZ_6FBZ6TLtSc=/90x56/smart/filters:strip_icc()/" data-url-tablet="BwJxYuh0qKnlk-NmfXw7xiRp0PY=/122x75/smart/filters:strip_icc()/" data-url-desktop="YUBSx0-zG9orH6Pa-5OgjmfDwVY=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>looks casuais no gnt</h3><p>Usaria ou nÃ£o? Avalie as produÃ§Ãµes com ecobags de  mulheres e homens</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://gnt.globo.com/" title="GNT">GNT</a></h3></li><li><h3><a href="http://vogue.globo.com/" title="vogue">vogue</a></h3></li><li></li></ul></aside></section><div class="separator"></div><section class="analytics-area analytics-id-D direita tvg"><div class="cabecalho"><h2><span class="word word-0">CASA</span><span class="word word-1">&amp;</span><span class="word word-2">DECORAÃÃO</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://revista.zapimoveis.com.br/veja-5-dicas-de-como-economizar-na-decoracao-do-quarto-do-bebe/" alt="Veja 5 dicas de como economizar na decoraÃ§Ã£o do quarto do bebÃª"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/OlIkVmakEUM3MTQYg4cZmsW-Im8=/filters:quality(10):strip_icc()/s2.glbimg.com/iCKe4otiyRQj6q5Ro3AgXor_iTI=/0x45:580x419/155x100/s.glbimg.com/en/ho/f/original/2015/10/13/carimbo-parede-bebe.jpg" alt="Veja 5 dicas de como economizar na decoraÃ§Ã£o do quarto do bebÃª (ReproduÃ§Ã£o/Pinterest)" title="Veja 5 dicas de como economizar na decoraÃ§Ã£o do quarto do bebÃª (ReproduÃ§Ã£o/Pinterest)"
            data-original-image="s2.glbimg.com/iCKe4otiyRQj6q5Ro3AgXor_iTI=/0x45:580x419/155x100/s.glbimg.com/en/ho/f/original/2015/10/13/carimbo-parede-bebe.jpg" data-url-smart_horizontal="sCmyU_LZeFOpb_ZVKDwYbQeOT8A=/90x56/smart/filters:strip_icc()/" data-url-smart="sCmyU_LZeFOpb_ZVKDwYbQeOT8A=/90x56/smart/filters:strip_icc()/" data-url-feature="sCmyU_LZeFOpb_ZVKDwYbQeOT8A=/90x56/smart/filters:strip_icc()/" data-url-tablet="v0OU7W7U-nS5HJhXn1-59T5i3CI=/122x75/smart/filters:strip_icc()/" data-url-desktop="pERDajHmA2zXsSnUPYEQReUBcu4=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>aproveite material de baixo custo</h3><p>Veja 5 dicas de como economizar<br /> na decoraÃ§Ã£o do quarto do bebÃª</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revistacasaejardim.globo.com/Casa-e-Jardim/Dicas/noticia/2015/10/solucoes-caseiras-vinagre-limao-gelo-papel-aluminio-e-bicarbonato-de-sodio-cafe-e-pasta-de-dente.html" alt="Veja soluÃ§Ãµes caseiras com vinagre, limÃ£o, gelo, papel alumÃ­nio e mais"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/Wa8mKDbEz5QM6CkvxQPysx4u_aA=/filters:quality(10):strip_icc()/s2.glbimg.com/02C9PbfK5uNbL4QbEN1JK3Wf2-Q=/0x20:620x420/155x100/e.glbimg.com/og/ed/f/original/2015/10/06/solucoes-caseiras.jpg" alt="Veja soluÃ§Ãµes caseiras com vinagre, limÃ£o, gelo, papel alumÃ­nio e mais (Stock Photos)" title="Veja soluÃ§Ãµes caseiras com vinagre, limÃ£o, gelo, papel alumÃ­nio e mais (Stock Photos)"
            data-original-image="s2.glbimg.com/02C9PbfK5uNbL4QbEN1JK3Wf2-Q=/0x20:620x420/155x100/e.glbimg.com/og/ed/f/original/2015/10/06/solucoes-caseiras.jpg" data-url-smart_horizontal="HQcJrr2sVTJ3gsCHzSLsqtu2TzY=/90x56/smart/filters:strip_icc()/" data-url-smart="HQcJrr2sVTJ3gsCHzSLsqtu2TzY=/90x56/smart/filters:strip_icc()/" data-url-feature="HQcJrr2sVTJ3gsCHzSLsqtu2TzY=/90x56/smart/filters:strip_icc()/" data-url-tablet="xOO1OrIM1R-a1dtyRVxJh5BLPT0=/122x75/smart/filters:strip_icc()/" data-url-desktop="QTeF9NzjjcuIR9w6ZSHDC9p5H_A=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>Limpar rejunte do banheiro, desentupir pia...</h3><p>Veja soluÃ§Ãµes caseiras com vinagre, limÃ£o, gelo, papel alumÃ­nio e mais</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://casavogue.globo.com/Interiores/Ambientes/noticia/2015/10/12-perfis-de-decoracao-para-seguir-no-instagram.html" alt="Faltou inspiraÃ§Ã£o? ConheÃ§a 12 perfis de dÃ©cor no Instagram para seguir"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/4i6eJTD79VJ32aqknAizrdvWTa4=/filters:quality(10):strip_icc()/s2.glbimg.com/fIWCtFQxJMh29QZc1G3ArXpcUBQ=/0x44:607x435/155x100/e.glbimg.com/og/ed/f/original/2015/10/06/vamosreceber3.jpg" alt="Faltou inspiraÃ§Ã£o? ConheÃ§a 12 perfis de dÃ©cor no Instagram para seguir (Instagram/ ReproduÃ§Ã£o)" title="Faltou inspiraÃ§Ã£o? ConheÃ§a 12 perfis de dÃ©cor no Instagram para seguir (Instagram/ ReproduÃ§Ã£o)"
            data-original-image="s2.glbimg.com/fIWCtFQxJMh29QZc1G3ArXpcUBQ=/0x44:607x435/155x100/e.glbimg.com/og/ed/f/original/2015/10/06/vamosreceber3.jpg" data-url-smart_horizontal="MeT8Wa_5SzqNo13hYXLexNtoTUU=/90x56/smart/filters:strip_icc()/" data-url-smart="MeT8Wa_5SzqNo13hYXLexNtoTUU=/90x56/smart/filters:strip_icc()/" data-url-feature="MeT8Wa_5SzqNo13hYXLexNtoTUU=/90x56/smart/filters:strip_icc()/" data-url-tablet="07lPoWDNzu__SnblLo8qiirVGAY=/122x75/smart/filters:strip_icc()/" data-url-desktop="-MvMhjbdt620BcS3H6l4M58jGUc=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>Estilos variados e fotos lindas</h3><p>Faltou inspiraÃ§Ã£o? ConheÃ§a 12 perfis de dÃ©cor no Instagram para seguir</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://revistacasaejardim.globo.com/" title="CASA E JARDIM">CASA E JARDIM</a></h3></li><li><h3><a href="http://casavogue.globo.com/" title="CASA VOGUE">CASA VOGUE</a></h3></li><li></li></ul></aside></section></section><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior famosos-style"><div class="cabecalho"><h2><a title="famosos"><span class='logo'>famosos</span><span class='texto'>famosos</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://ego.globo.com/">ego</a></h3></li><li class=""><h3><a href="http://revistaquem.globo.com/">quem</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://extra.globo.com/famosos/aos-18-anos-recem-solteiro-nicolas-prattes-revela-que-sexo-nao-tabu-diz-nao-pensar-em-namoro-agora-estou-tranquilo-17757580.html" title="Prattes diz que sexo nÃ£o Ã© tabu e nÃ£o pensa em namoro agora"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/p9qvvXLT-ObIn0aP2yAg_at4wyM=/filters:quality(10):strip_icc()/s2.glbimg.com/ZyNdjnCZkgxUymcjg7j2h_ibeg4=/0x33:448x271/245x130/s.glbimg.com/en/ho/f/original/2015/10/13/nicolas-prattes.jpg" alt="Prattes diz que sexo nÃ£o Ã© tabu e nÃ£o pensa em namoro agora (DivulgaÃ§Ã£o)" title="Prattes diz que sexo nÃ£o Ã© tabu e nÃ£o pensa em namoro agora (DivulgaÃ§Ã£o)"
                    data-original-image="s2.glbimg.com/ZyNdjnCZkgxUymcjg7j2h_ibeg4=/0x33:448x271/245x130/s.glbimg.com/en/ho/f/original/2015/10/13/nicolas-prattes.jpg" data-url-smart_horizontal="vF66KsOauLRtbK6SfEvQzJXvs7Q=/90x56/smart/filters:strip_icc()/" data-url-smart="vF66KsOauLRtbK6SfEvQzJXvs7Q=/90x56/smart/filters:strip_icc()/" data-url-feature="vF66KsOauLRtbK6SfEvQzJXvs7Q=/90x56/smart/filters:strip_icc()/" data-url-tablet="LFGaAq6TJ45E_aEXItt9dl6n8cY=/160x95/smart/filters:strip_icc()/" data-url-desktop="Y1lUOMK-3Akys0gqnWl0_oQqWfg=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Prattes diz que sexo nÃ£o Ã© tabu e nÃ£o pensa em namoro agora</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://ego.globo.com/famosos/noticia/2015/10/sheila-mello-mergulha-em-fernando-de-noronha.html" title="Sheila Mello exibe corpÃ£o em mergulho durante viagem"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/K9L1W5RdbN8R76cam30wMJ61nlY=/filters:quality(10):strip_icc()/s2.glbimg.com/b9dAbOVAcA3WofSqFJvd8vQP4b8=/98x335:947x785/245x130/s.glbimg.com/jo/eg/f/original/2015/10/13/sheila_mello.jpg" alt="Sheila Mello exibe corpÃ£o em mergulho durante viagem (ReproduÃ§Ã£o/Instagram)" title="Sheila Mello exibe corpÃ£o em mergulho durante viagem (ReproduÃ§Ã£o/Instagram)"
                    data-original-image="s2.glbimg.com/b9dAbOVAcA3WofSqFJvd8vQP4b8=/98x335:947x785/245x130/s.glbimg.com/jo/eg/f/original/2015/10/13/sheila_mello.jpg" data-url-smart_horizontal="6bp5F1bijRdZXIs137hloi7FxAE=/90x56/smart/filters:strip_icc()/" data-url-smart="6bp5F1bijRdZXIs137hloi7FxAE=/90x56/smart/filters:strip_icc()/" data-url-feature="6bp5F1bijRdZXIs137hloi7FxAE=/90x56/smart/filters:strip_icc()/" data-url-tablet="i2kxMYWDnJGL2gvzrMOOdEwTQyk=/160x95/smart/filters:strip_icc()/" data-url-desktop="3yc9Wluj0lfaT0Qk_1lCdf-apvY=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Sheila Mello exibe corpÃ£o em mergulho durante viagem</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://oglobo.globo.com/ela/moda/kim-khloe-roubam-cena-com-decotes-em-encontro-da-familia-kardashian-no-tapete-vermelho-17758140" title="Kim e KhloÃ© roubam cena com decotes em encontro da famÃ­lia"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/yz6VBEMqPywqScusYvDZQ-uvKig=/filters:quality(10):strip_icc()/s2.glbimg.com/q4xQoGysUXEfyap6PzSaUCri9v8=/27x10:660x345/245x130/s.glbimg.com/en/ho/f/original/2015/10/13/kard.jpg" alt="Kim e KhloÃ© roubam cena com decotes em encontro da famÃ­lia (Frederick M. Brown / AFP)" title="Kim e KhloÃ© roubam cena com decotes em encontro da famÃ­lia (Frederick M. Brown / AFP)"
                    data-original-image="s2.glbimg.com/q4xQoGysUXEfyap6PzSaUCri9v8=/27x10:660x345/245x130/s.glbimg.com/en/ho/f/original/2015/10/13/kard.jpg" data-url-smart_horizontal="Nsw-FyJlCk50BN5FAp-syd4IcBg=/90x56/smart/filters:strip_icc()/" data-url-smart="Nsw-FyJlCk50BN5FAp-syd4IcBg=/90x56/smart/filters:strip_icc()/" data-url-feature="Nsw-FyJlCk50BN5FAp-syd4IcBg=/90x56/smart/filters:strip_icc()/" data-url-tablet="Zi2nvKjNc_2Jz95Qm6brOQduOpQ=/160x95/smart/filters:strip_icc()/" data-url-desktop="Eo-DjelvyH77f0Kp243o99y0OcA=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Kim e KhloÃ© roubam cena com decotes em encontro da famÃ­lia</p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://revistamonet.globo.com/Listas/noticia/2015/10/20-celebridades-que-buscaram-ajuda-em-clinicas-de-reabilitacao.html" title="ConheÃ§a astros que buscaram ajuda em clÃ­nica de reabilitaÃ§Ã£o"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/MIvXf0FlGaVtGhsdgV9G6U1akjA=/filters:quality(10):strip_icc()/s2.glbimg.com/9rfja8Jk5pqdhJY7O7MklkEslPU=/17x0:1046x546/245x130/e.glbimg.com/og/ed/f/original/2015/09/30/12091411_1028978673820158_7030080571813743126_o.jpg" alt="ConheÃ§a astros que buscaram ajuda em clÃ­nica de reabilitaÃ§Ã£o (Getty Images)" title="ConheÃ§a astros que buscaram ajuda em clÃ­nica de reabilitaÃ§Ã£o (Getty Images)"
                    data-original-image="s2.glbimg.com/9rfja8Jk5pqdhJY7O7MklkEslPU=/17x0:1046x546/245x130/e.glbimg.com/og/ed/f/original/2015/09/30/12091411_1028978673820158_7030080571813743126_o.jpg" data-url-smart_horizontal="RPpZ73tPSdtvEQLAsI5Q4e-Nwqc=/90x56/smart/filters:strip_icc()/" data-url-smart="RPpZ73tPSdtvEQLAsI5Q4e-Nwqc=/90x56/smart/filters:strip_icc()/" data-url-feature="RPpZ73tPSdtvEQLAsI5Q4e-Nwqc=/90x56/smart/filters:strip_icc()/" data-url-tablet="cfS-lECXWVh1_BlIYYkEZR5QvUI=/160x95/smart/filters:strip_icc()/" data-url-desktop="w6bTE8a5Gw4UjZdV5qJm3wIVDbM=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>ConheÃ§a astros que buscaram ajuda em clÃ­nica de reabilitaÃ§Ã£o</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_famosos-style']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior gshow"><div class="cabecalho"><h2><a href="http://gshow.globo.com/" title="novelas, sÃ©ries, programas e muito mais"><span class='logo'>novelas, sÃ©ries, programas e muito mais</span><span class='texto'>novelas, sÃ©ries, programas e muito mais</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://gshow.globo.com/estilo-tv/">ESTILO TV</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/">A REGRA DO JOGO</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/i-love-paraisopolis/index.html">I LOVE PARAISÃPOLIS</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/alem-do-tempo/">ALÃM DO TEMPO</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/malhacao/2015/">MALHAÃÃO</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/i-love-paraisopolis/Vem-por-ai/noticia/2015/10/dom-peppino-e-carinhoso-com-filha-de-benjamin.html" title="Em &#39;I Love&#39;, Dom Peppino Ã© carinhoso com a filha de Ben"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/GDdO6822NM766PazvX8aetVgKO0=/filters:quality(10):strip_icc()/s2.glbimg.com/ru7ILLCxxcofoJ-Db_BNSAyu4GA=/0x55:690x421/245x130/s.glbimg.com/et/gs/f/original/2015/10/13/peppino.jpg" alt="Em &#39;I Love&#39;, Dom Peppino Ã© carinhoso com a filha de Ben (Artur Meninea/Gshow)" title="Em &#39;I Love&#39;, Dom Peppino Ã© carinhoso com a filha de Ben (Artur Meninea/Gshow)"
                    data-original-image="s2.glbimg.com/ru7ILLCxxcofoJ-Db_BNSAyu4GA=/0x55:690x421/245x130/s.glbimg.com/et/gs/f/original/2015/10/13/peppino.jpg" data-url-smart_horizontal="xl8w8VQdL2ZV1aRNJ4u8R1XaCE8=/90x56/smart/filters:strip_icc()/" data-url-smart="xl8w8VQdL2ZV1aRNJ4u8R1XaCE8=/90x56/smart/filters:strip_icc()/" data-url-feature="xl8w8VQdL2ZV1aRNJ4u8R1XaCE8=/90x56/smart/filters:strip_icc()/" data-url-tablet="KAVwXvoVHDyf8MRmn69JSfeo8GQ=/160x95/smart/filters:strip_icc()/" data-url-desktop="PSBeyj5Ul8fK_dJj-xEp9cG57Co=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Em &#39;I Love&#39;, Dom Peppino Ã© carinhoso com a filha de Ben</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/malhacao/2015/Vem-por-ai/noticia/2015/10/rodrigo-implora-para-luciana-nao-terminar-com-ele.html" title="&#39;MalhaÃ§Ã£o&#39;: Rodrigo implora para Luciana nÃ£o terminar"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/ho1nGDFfSHFWRS8L5fY_Jg0c30o=/filters:quality(10):strip_icc()/s2.glbimg.com/X6Ny-W98ivlfw8KF0L3602K5A2M=/0x77:690x443/245x130/s.glbimg.com/et/gs/f/original/2015/10/13/ludrigo1.jpg" alt="&#39;MalhaÃ§Ã£o&#39;: Rodrigo implora para Luciana nÃ£o terminar (Bruno Cavalieri/Gshow)" title="&#39;MalhaÃ§Ã£o&#39;: Rodrigo implora para Luciana nÃ£o terminar (Bruno Cavalieri/Gshow)"
                    data-original-image="s2.glbimg.com/X6Ny-W98ivlfw8KF0L3602K5A2M=/0x77:690x443/245x130/s.glbimg.com/et/gs/f/original/2015/10/13/ludrigo1.jpg" data-url-smart_horizontal="_CrkQDbDIJSsgQ5wGOmr6kuwJLo=/90x56/smart/filters:strip_icc()/" data-url-smart="_CrkQDbDIJSsgQ5wGOmr6kuwJLo=/90x56/smart/filters:strip_icc()/" data-url-feature="_CrkQDbDIJSsgQ5wGOmr6kuwJLo=/90x56/smart/filters:strip_icc()/" data-url-tablet="v0fUkwHhtB5HXRah5nDZ9gqZSj8=/160x95/smart/filters:strip_icc()/" data-url-desktop="dFsqZ_htyp_WVo6D4fePVI8TxpA=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>&#39;MalhaÃ§Ã£o&#39;: Rodrigo implora para Luciana nÃ£o terminar</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/tv/noticia/2015/10/candidata-canta-fat-family-e-tenta-vaga-no-voice-brasil.html" title="Candidata canta Fat Family no &#39;The Voice Brasil&#39;; veja a prÃ©via"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/U3UIvsQsG6djtUYb1WuEj-feK8w=/filters:quality(10):strip_icc()/s2.glbimg.com/wCu6t2gQuenc-lzRp3fflRtk-3E=/0x71:690x437/245x130/s.glbimg.com/et/gs/f/original/2015/10/13/claudia_leitte_escuta_voz_de_candidata_tentando_vaga_no_the_voice_brasil.jpg" alt="Candidata canta Fat Family no &#39;The Voice Brasil&#39;; veja a prÃ©via (Gshow)" title="Candidata canta Fat Family no &#39;The Voice Brasil&#39;; veja a prÃ©via (Gshow)"
                    data-original-image="s2.glbimg.com/wCu6t2gQuenc-lzRp3fflRtk-3E=/0x71:690x437/245x130/s.glbimg.com/et/gs/f/original/2015/10/13/claudia_leitte_escuta_voz_de_candidata_tentando_vaga_no_the_voice_brasil.jpg" data-url-smart_horizontal="_k9xgmmQa9sGikAjNYPoMUCh9J0=/90x56/smart/filters:strip_icc()/" data-url-smart="_k9xgmmQa9sGikAjNYPoMUCh9J0=/90x56/smart/filters:strip_icc()/" data-url-feature="_k9xgmmQa9sGikAjNYPoMUCh9J0=/90x56/smart/filters:strip_icc()/" data-url-tablet="LneaoIeybHkk-DdlAi-Pd-RhKsQ=/160x95/smart/filters:strip_icc()/" data-url-desktop="zItpuZdjwDbss9Omx3iqsWzi8LQ=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Candidata canta Fat Family no &#39;The Voice Brasil&#39;; veja a prÃ©via</p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://gshow.globo.com/Bastidores/noticia/2015/10/ex-bbb-amanda-djehdian-visita-las-vegas-pela-primeira-vez.html" title="Ex-BBB Amanda abre Ã¡lbum de viagem de 1Âª visita a Las Vegas"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/NxMWD_AcUJ4MfedgIMTnmjCSuDg=/filters:quality(10):strip_icc()/s2.glbimg.com/2GY8Y3D7wYQ-OKxtuAqGrTHYwUY=/0x20:690x386/245x130/s.glbimg.com/et/gs/f/original/2015/10/12/amanda_las_vegas_4.jpg" alt="Ex-BBB Amanda abre Ã¡lbum de viagem de 1Âª visita a Las Vegas (Arquivo Pessoal)" title="Ex-BBB Amanda abre Ã¡lbum de viagem de 1Âª visita a Las Vegas (Arquivo Pessoal)"
                    data-original-image="s2.glbimg.com/2GY8Y3D7wYQ-OKxtuAqGrTHYwUY=/0x20:690x386/245x130/s.glbimg.com/et/gs/f/original/2015/10/12/amanda_las_vegas_4.jpg" data-url-smart_horizontal="VYg_feqQAD6Qv00fAd98yYIhgJk=/90x56/smart/filters:strip_icc()/" data-url-smart="VYg_feqQAD6Qv00fAd98yYIhgJk=/90x56/smart/filters:strip_icc()/" data-url-feature="VYg_feqQAD6Qv00fAd98yYIhgJk=/90x56/smart/filters:strip_icc()/" data-url-tablet="G6ACZMn0K4nhoZwvrtG5TlDIfeY=/160x95/smart/filters:strip_icc()/" data-url-desktop="BZO8BcgB6oLO7eGWT7VSUdoSUZ4=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Ex-BBB Amanda abre Ã¡lbum de viagem de 1Âª visita a Las Vegas</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_gshow']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-musica-triplo analytics-area analytics-id-U franja-inferior"><div class="cabecalho"><h2><a href="http://musica.com.br/" title="MÃSICA">
            MÃSICA
        </a></h2><aside class="links analytics-area analytics-id-T"><ul><li class="mobile-grid-full "><h3><a href="http://musica.com.br/">MÃSICA.COM.BR</a></h3></li><li class="mobile-grid-full "><h3><a href="http://g1.globo.com/musica/">G1 MÃSICA</a></h3></li><li class="mobile-grid-full "><h3><a href="http://multishow.globo.com/musica/">MULTISHOW</a></h3></li><li class="mobile-grid-full "><h3><a href="http://radiobeat.com.br/">RADIOBEAT</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://revistaglamour.globo.com/Celebridades/noticia/2015/10/novo-clipe-de-anitta-bate-marca-de-4-milhoes-de-views-vem-ver.html" title="Novo clipe de Anitta atinge a marca de 4 milhÃµes de visualizaÃ§Ãµes; assista aqui (DivulgaÃ§Ã£o)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/0Z0-lkjxHLCuAJVLOnOmQjWo1sc=/filters:quality(10):strip_icc()/s2.glbimg.com/czitB2bq1X2CyLm7ssmk8w32gR4=/0x0:607x367/215x130/e.glbimg.com/og/ed/f/original/2015/10/09/bang_anitta.jpg"
                data-original-image="s2.glbimg.com/czitB2bq1X2CyLm7ssmk8w32gR4=/0x0:607x367/215x130/e.glbimg.com/og/ed/f/original/2015/10/09/bang_anitta.jpg" data-url-smart_horizontal="iIyrnYOruRRROu32w9S3jUTNWSI=/90x56/smart/filters:strip_icc()/" data-url-smart="iIyrnYOruRRROu32w9S3jUTNWSI=/90x56/smart/filters:strip_icc()/" data-url-feature="iIyrnYOruRRROu32w9S3jUTNWSI=/90x56/smart/filters:strip_icc()/" data-url-tablet="D3pRVRJWw4w2_0tL0g3YbVqAxeY=/120x80/smart/filters:strip_icc()/" data-url-desktop="pbJevVSOW7D_DY_RAp-IJ0MLEJc=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Novo clipe de Anitta atinge <br />a marca de 4 milhÃµes de visualizaÃ§Ãµes; assista aqui</div></div></a></li><li class="mobile-grid-full"><a class="foto" href="http://g1.globo.com/musica/noticia/2015/10/justin-bieber-cd-e-vetado-no-oriente-medio-por-capa-provocante-diz-site.html" title="CD de Justin Bieber Ã© vetado no Oriente MÃ©dio por capa provocante, segundo site (DivulgaÃ§Ã£o)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/rNVXQQF5qksWrWO7IJb4xEWxDz8=/filters:quality(10):strip_icc()/s2.glbimg.com/BqX5nqNfypgGUV5c2n3LevBD4DM=/0x30:620x405/215x130/s.glbimg.com/jo/g1/f/original/2015/10/13/justin-bieber-purpose.jpg"
                data-original-image="s2.glbimg.com/BqX5nqNfypgGUV5c2n3LevBD4DM=/0x30:620x405/215x130/s.glbimg.com/jo/g1/f/original/2015/10/13/justin-bieber-purpose.jpg" data-url-smart_horizontal="JqZH2RRGKQpS2Efgixpm0DJxzNQ=/90x56/smart/filters:strip_icc()/" data-url-smart="JqZH2RRGKQpS2Efgixpm0DJxzNQ=/90x56/smart/filters:strip_icc()/" data-url-feature="JqZH2RRGKQpS2Efgixpm0DJxzNQ=/90x56/smart/filters:strip_icc()/" data-url-tablet="2iLTc2jojXQUXTVB1RrpMe9k68g=/120x80/smart/filters:strip_icc()/" data-url-desktop="GaUP9TA8CzVcbxTSRfaUZ_KogmQ=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">CD de Justin Bieber Ã© vetado no Oriente MÃ©dio por capa provocante, segundo site</div></div></a></li><li class="mobile-grid-full"><a class="foto" href="http://oglobo.globo.com/cultura/musica/critica-janet-jackson-volta-ao-sucesso-com-album-reflexivo-17756048" title="Janet Jackson refaz parcerias da dÃ©cada de 80 e  lanÃ§a Ã¡lbum reflexivo apÃ³s 7 anos (ReproduÃ§Ã£o)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/UbpRuimyjen1GIwnhNCS5AZDnms=/filters:quality(10):strip_icc()/s2.glbimg.com/9pgFHlhsc1ywsZR-UqPgbcTpkyw=/0x0:695x420/215x130/s.glbimg.com/en/ho/f/original/2015/10/13/janet.jpg"
                data-original-image="s2.glbimg.com/9pgFHlhsc1ywsZR-UqPgbcTpkyw=/0x0:695x420/215x130/s.glbimg.com/en/ho/f/original/2015/10/13/janet.jpg" data-url-smart_horizontal="Vt9A6X-MX6FJzwmbStf-Lc0_bKA=/90x56/smart/filters:strip_icc()/" data-url-smart="Vt9A6X-MX6FJzwmbStf-Lc0_bKA=/90x56/smart/filters:strip_icc()/" data-url-feature="Vt9A6X-MX6FJzwmbStf-Lc0_bKA=/90x56/smart/filters:strip_icc()/" data-url-tablet="PkltjBpJG2G4TayXhvoQ13Uvo8Y=/120x80/smart/filters:strip_icc()/" data-url-desktop="2b_-oTowad9Qh8vIaTVB98IwaIg=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Janet Jackson refaz parcerias da dÃ©cada de 80 e  lanÃ§a Ã¡lbum reflexivo apÃ³s 7 anos</div></div></a></li></ul><div class="top-container analytics-area analytics-id-L mobile-grid-full"><h3><a href="http://musica.com.br/">
            TOP HITS
        </a></h3><ul><li><a class="foto" href="http://musica.com.br/artistas/luan-santana/m/escreve-ai/letra.html"><div class="area-foto"><div class="number">1</div><img width="45" height="45" src="http://s2.glbimg.com/DejQr6wzwaLjEToKO0IBZYMpp9E=/filters:quality(10):strip_icc()/s2.glbimg.com/gffJ6jy3x9TdEFawxnsku-K_Igs=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/12/20/artista-65018-2013151926.jpg"
                data-original-image="s2.glbimg.com/gffJ6jy3x9TdEFawxnsku-K_Igs=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/12/20/artista-65018-2013151926.jpg" data-url-smart_horizontal="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-smart="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-feature="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-tablet="jBcm2gM8Cbg_Vkcg-0n1BAJmuKE=/60x60/smart/filters:strip_icc()/" data-url-desktop="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Luan Santana</div><div class="title">Escreve AÃ­</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li><li><a class="foto" href="http://musica.com.br/artistas/mc-anitta/m/no-meu-talento-part-mc-guime/letra.html"><div class="area-foto"><div class="number">2</div><img width="45" height="45" src="http://s2.glbimg.com/yHnJPHom5FPA3tk69JCcIphdNJE=/filters:quality(10):strip_icc()/s2.glbimg.com/i6tbAg-HNpqidGWWFJqVJ788g6o=/65x65/smart/s.glbimg.com/po/ms/f/original/2014/03/28/anitta-nova-cut.jpg"
                data-original-image="s2.glbimg.com/i6tbAg-HNpqidGWWFJqVJ788g6o=/65x65/smart/s.glbimg.com/po/ms/f/original/2014/03/28/anitta-nova-cut.jpg" data-url-smart_horizontal="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-smart="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-feature="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-tablet="AODuTqXeNCV61xTQZim-SUSZDBg=/60x60/smart/filters:strip_icc()/" data-url-desktop="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Anitta</div><div class="title">No Meu Talento (part. MC GuimÃª)</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li><li class="last-top"><a class="foto" href="http://musica.com.br/artistas/jorge-e-mateus/m/nocaute/letra.html"><div class="area-foto"><div class="number">3</div><img width="45" height="45" src="http://s2.glbimg.com/jdC6lPjKcSpWF8v5jzFy0uuol6c=/filters:quality(10):strip_icc()/s2.glbimg.com/JgDLLARwYvvuCkqHPgmPue-pxa8=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/06/26/jorge.jpg"
                data-original-image="s2.glbimg.com/JgDLLARwYvvuCkqHPgmPue-pxa8=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/06/26/jorge.jpg" data-url-smart_horizontal="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-smart="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-feature="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-tablet="EfTWuRDHHuk_LklhIijQiqahj3A=/60x60/smart/filters:strip_icc()/" data-url-desktop="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Jorge e Mateus</div><div class="title">Nocaute</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li></ul></div><div class="separator"></div><div class="widget analytics-area analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-musica-triplo']);</script></section><section class="area topglobocom"><div class="container area"><div class="separator"></div><div id="topglobocom3" class="clearfix destaque topglobocom3"><div class="topo"><h3 class="titulo">
            top <strong>globo</strong><span class="subtitulo">tÃ¡ todo mundo clicando...</span></h3></div><div id="mais-vistas" class="clearfix mais-vistas tab-content analytics-area analytics-id-R"><div class="slider"><ol class="noticias analytics-area analytics-id-J"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://extra.globo.com/casos-de-policia/atendente-morre-apos-ser-esfaqueada-em-farmacia-no-rio-grande-do-sul-17758444.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Atendente morre apÃ³s ser esfaqueada em farmÃ¡cia, em cidade do RS</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://extra.globo.com/noticias/mundo/foto-de-medica-socorrendo-vitimas-de-acidente-no-dia-do-casamento-faz-sucesso-na-web-17758048.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Foto de mÃ©dica socorrendo vÃ­timas de acidente apÃ³s se casar vira hit na web</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/fotos/fotos/2015/10/fotos-imagens-do-dia-13-de-outubro-de-2015.html#F1806285" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">CrianÃ§a sÃ­ria Ã© socorrida em hospital de campo apÃ³s bombardeio</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://oglobo.globo.com/brasil/lula-diz-que-pedaladas-pagaram-bolsa-familia-casas-17760947" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Lula diz que &#39;pedaladas&#39; do governo pagaram Bolsa FamÃ­lia e casas</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/politica/noticia/2015/10/ministra-reforca-suspensao-de-rito-feito-por-cunha-para-impeachment.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">STF dÃ¡ a terceira liminar contra rito de Cunha para o impeachment</span></span></a></div></div></li></ol><ol class="esportes analytics-area analytics-id-E"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://extra.globo.com/famosos/neymar-leva-duas-loiras-para-hotel-se-irrita-com-flagra-de-paparazzo-17757255.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Flagra com duas mulheres em hotel irrita Neymar durante passagem pelo RJ</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/blogs/especial-blog/bastidores-fc/post/aidar-se-despede-dos-funcionarios-no-sao-paulo.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Carlos Miguel Aidar se despede dos funcionÃ¡rios no SÃ£o Paulo por e-mail</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://sportv.globo.com/site/combate/blogs/especial-blog/ultimmato/post/jon-jones-levanta-247kg-bate-recorde-pessoal-e-provoca-dc-ainda-nao-pode-me-parar.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Jones levanta 247kg, bate marca pessoal e provoca DC: &#39;NÃ£o pode me parar&#39;</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/futebol/times/corinthians/noticia/2015/10/corinthians-sobe-o-preco-de-pato-e-busca-reaproximacao-com-atacante.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Corinthians sobe o preÃ§o por Pato e busca reaproximaÃ§Ã£o com atacante</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/futebol/selecao-brasileira/noticia/2015/10/dunga-ve-indignacao-na-selecao-mas-admite-eramos-mais-sanguineos.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Dunga vÃª indignaÃ§Ã£o na SeleÃ§Ã£o, mas admite: &#39;Ãramos mais sanguÃ­neos&#39;</span></span></a></div></div></li></ol><ol class="entretenimento analytics-area analytics-id-M"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://extra.globo.com/famosos/mao-de-giovanna-ewbank-some-em-foto-bruno-gagliasso-faz-piada-esta-linda-17761132.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">IlusÃ£o de Ã³tica? Ewbank &#39;perde&#39; a mÃ£o em foto e Gagliasso zoa: &#39;Ficou linda&#39;</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/famosos/noticia/2015/10/sheila-mello-mergulha-em-fernando-de-noronha.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Sheila Mello exibe o corpo escultural em mergulho em viagem para Noronha</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/biquini/noticia/2015/10/ticiane-pinheiro-posa-de-biquini-em-ferias-com-o-namorado-cesar-tralli.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Tici Pinheiro posa de biquÃ­ni para Tralli na piscina privativa de quarto de hotel</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/famosos/noticia/2015/10/andressa-urach-posta-foto-de-antes-e-depois.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Hoje evangÃ©lica, Andressa Urach posta foto de &#39;antes e depois&#39; e cita seu livro</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://gshow.globo.com/Bastidores/noticia/2015/10/dani-winits-sobre-genetica-boa-minha-mae-esta-bombando.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">&#39;DifÃ­cil ir Ã  praia com minha mÃ£e, ela estÃ¡ bombando&#39;, diz Winits sobre o corpÃ£o</span></span></a></div></div></li></ol></div></div><div class="controls-area"><ul class="step-marker"><li class=" glb-hl-style-noticias"><div class=""><a href="#" data-related=""><div class="ball active"></div></a></div></li><li class=" glb-hl-style-esporte"><div class=""><a href="#" data-related=""><div class="ball"></div></a></div></li><li class=" glb-hl-style-entretenimento"><div class=""><a href="#" data-related=""><div class="ball "></div></a></div></li></ul></div></div><section class="top-videos analytics-area analytics-id-R"><h1><a href="http://globotv.globo.com/">globo.tv</a></h1><div class="separator-line"></div><ul class="destaque analytics-area analytics-id-V"><li ><a class="foto" href="#"><span class="borda-foto has-video"><span></span><img width="190" height="110"></span><span class="number">1</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto has-video"><span></span><img width="190" height="110"></span><span class="number">2</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto has-video"><span></span><img width="190" height="110"></span><span class="number">3</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto has-video"><span></span><img width="190" height="110"></span><span class="number">4</span><span class="conteudo"><p>descr</p></span></a></li><li class="ult"><a class="foto" href="#"><span class="borda-foto has-video"><span></span><img width="190" height="110"></span><span class="number">5</span><span class="conteudo"><p>descr</p></span></a></li></ul></section></div></section><section class="area globo-servicos container analytics-area analytics-id-C"><h2 class="titulo">serviÃ§os</h2><div class="borda"><span class="rodape"></span></div><div class="content"><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-loterias
" 
    data-lotteries-url="http://api.globo.com/lotteries/megasena.jsonp"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-fipe
"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-zap
" 
    data-zap-url="http://api.globo.com/zap/search/"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"></div></div></section><section class="area widgets_inferior franja-inferior container analytics-area analytics-id-C"><div class="box-links-servicos"><div class="links"><p>MAIS SERVIÃOS</p><a href="http://revistapegn.globo.com/Franquias/compare/">Abra uma franquia</a><a href="http://globoesporte.globo.com/eu-atleta/calculo-imc.html">AvaliaÃ§Ã£o fÃ­sica</a><a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">Conversor de moedas</a><a href="http://www.techtudo.com.br/softwares/">Downloads</a><a href="http://g1.globo.com/previsao-do-tempo/">PrevisÃ£o do tempo</a><a href="http://g1.globo.com/radar-g1/platb/">TrÃ¢nsito</a></div></div><div id="glb-shopping-widget" class="area globoshopping container" data-url="http://vitrines.globo.com/vitrine/vitrine.min.js" data-sitepage="globocom/home" data-responsive="0" data-branding-color="0669DE"></div></section><section class="area diretorio"><div class="container analytics-area analytics-id-Z"><div class="diretorio-responsive"><a href="/todos-os-sites.html">veja todos os sites da <strong>globo.com</strong> âº</a></div><div id="glb-diretorio"><div class="glb-conteudo"><div class="glb-bloco"><div class="glb-grid-12"><ul class="diretorio-indice"><li class="diretorio-linha diretorio-first "><ul><li class="diretorio-first-level analytics-product glb-menu-g1 diretorio-quebra "><a title="g1" href="http://g1.globo.com/">g1</a><ul><li class="diretorio-second-level"><a title="Acre" href="http://g1.globo.com/ac/acre/">Acre</a></li><li class="diretorio-second-level"><a title="Alagoas" href="http://g1.globo.com/al/alagoas/">Alagoas</a></li><li class="diretorio-second-level"><a title="Amazonas" href="http://g1.globo.com/am/amazonas/">Amazonas</a></li><li class="diretorio-second-level"><a title="Bahia" href="http://g1.globo.com/bahia/?noAudience=true">Bahia</a></li><li class="diretorio-second-level"><a title="Bem Estar" href="http://g1.globo.com/bemestar/">Bem Estar</a></li><li class="diretorio-second-level"><a title="Bom Dia Brasil" href="http://g1.globo.com/bom-dia-brasil/">Bom Dia Brasil</a></li><li class="diretorio-second-level"><a title="Brasil" href="http://g1.globo.com/brasil/">Brasil</a></li><li class="diretorio-second-level"><a title="Carnaval" href="http://g1.globo.com/carnaval/2013/">Carnaval</a></li><li class="diretorio-second-level"><a title="Carros" href="http://g1.globo.com/carros/">Carros</a></li><li class="diretorio-second-level"><a title="CearÃ¡" href="http://g1.globo.com/ceara/">CearÃ¡</a></li><li class="diretorio-second-level"><a title="CiÃªncia e SaÃºde" href="http://g1.globo.com/ciencia-e-saude/">CiÃªncia e SaÃºde</a></li><li class="diretorio-second-level"><a title="Como SerÃ¡?" href="http://redeglobo.globo.com/como-sera/index.html">Como SerÃ¡?</a></li><li class="diretorio-second-level"><a title="Concursos e Emprego" href="http://g1.globo.com/concursos-e-emprego/">Concursos e Emprego</a></li><li class="diretorio-second-level"><a title="Distrito Federal" href="http://g1.globo.com/distrito-federal/">Distrito Federal</a></li><li class="diretorio-second-level"><a title="Economia" href="http://g1.globo.com/economia/">Economia</a></li><li class="diretorio-second-level"><a title="EducaÃ§Ã£o" href="http://www.educacao.globo.com/">EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="EspÃ­rito Santo" href="http://g1.globo.com/espirito-santo/">EspÃ­rito Santo</a></li><li class="diretorio-second-level"><a title="FantÃ¡stico" href="http://fantastico.globo.com/">FantÃ¡stico</a></li><li class="diretorio-second-level"><a title="G1 EducaÃ§Ã£o" href="http://g1.globo.com/educacao/">G1 EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Globo Natureza" href="http://g1.globo.com/natureza/">Globo Natureza</a></li><li class="diretorio-second-level"><a title="Globo News" href="http://globonews.globo.com/">Globo News</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://g1.globo.com/economia/agronegocios/">Globo Rural</a></li><li class="diretorio-second-level"><a title="Globo RepÃ³rter" href="http://g1.globo.com/globo-reporter/">Globo RepÃ³rter</a></li><li class="diretorio-second-level"><a title="GoiÃ¡s" href="http://g1.globo.com/goias/">GoiÃ¡s</a></li><li class="diretorio-second-level"><a title="Jornal da Globo" href="http://g1.globo.com/jornal-da-globo/">Jornal da Globo</a></li><li class="diretorio-second-level"><a title="Jornal Hoje" href="http://g1.globo.com/jornal-hoje/">Jornal Hoje</a></li><li class="diretorio-second-level"><a title="Jornal Nacional" href="http://g1.globo.com/jornal-nacional/">Jornal Nacional</a></li><li class="diretorio-second-level"><a title="MaranhÃ£o" href="http://g1.globo.com/ma/maranhao/">MaranhÃ£o</a></li><li class="diretorio-second-level"><a title="Mato Grosso" href="http://g1.globo.com/mato-grosso/">Mato Grosso</a></li><li class="diretorio-second-level"><a title="Mato Grosso do Sul" href="http://g1.globo.com/mato-grosso-do-sul/">Mato Grosso do Sul</a></li><li class="diretorio-second-level"><a title="Minas Gerais" href="http://g1.globo.com/minas-gerais/">Minas Gerais</a></li><li class="diretorio-second-level"><a title="Mundo" href="http://g1.globo.com/mundo/">Mundo</a></li><li class="diretorio-second-level"><a title="ParÃ¡" href="http://g1.globo.com/pa/para/">ParÃ¡</a></li><li class="diretorio-second-level"><a title="ParaÃ­ba" href="http://g1.globo.com/paraiba/">ParaÃ­ba</a></li><li class="diretorio-second-level"><a title="ParanÃ¡" href="http://g1.globo.com/parana/">ParanÃ¡</a></li><li class="diretorio-second-level"><a title="Pernambuco" href="http://g1.globo.com/pernambuco/">Pernambuco</a></li><li class="diretorio-second-level"><a title="PiauÃ­" href="http://g1.globo.com/pi/piaui/">PiauÃ­</a></li><li class="diretorio-second-level"><a title="Planeta Bizarro" href="http://g1.globo.com/planeta-bizarro/">Planeta Bizarro</a></li><li class="diretorio-second-level"><a title="PolÃ­tica" href="http://g1.globo.com/politica/">PolÃ­tica</a></li><li class="diretorio-second-level"><a title="Pop &amp; Arte" href="http://g1.globo.com/pop-arte/">Pop &amp; Arte</a></li><li class="diretorio-second-level"><a title="PrevisÃ£o do Tempo" href="http://g1.globo.com/previsao-do-tempo.html">PrevisÃ£o do Tempo</a></li><li class="diretorio-second-level"><a title="ProfissÃ£o RepÃ³rter" href="http://g1.globo.com/profissao-reporter/">ProfissÃ£o RepÃ³rter</a></li><li class="diretorio-second-level"><a title="Rio de Janeiro" href="http://g1.globo.com/rio-de-janeiro/">Rio de Janeiro</a></li><li class="diretorio-second-level"><a title="Rio Grande do Norte" href="http://g1.globo.com/rn/rio-grande-do-norte/">Rio Grande do Norte</a></li><li class="diretorio-second-level"><a title="Rio Grande do Sul" href="http://g1.globo.com/rs/rio-grande-do-sul/">Rio Grande do Sul</a></li><li class="diretorio-second-level"><a title="RondÃ´nia" href="http://g1.globo.com/ro/rondonia/">RondÃ´nia</a></li><li class="diretorio-second-level"><a title="Santa Catarina" href="http://g1.globo.com/sc/santa-catarina/">Santa Catarina</a></li><li class="diretorio-second-level"><a title="SÃ£o Paulo" href="http://g1.globo.com/sao-paulo/">SÃ£o Paulo</a></li><li class="diretorio-second-level"><a title="Sergipe" href="http://g1.globo.com/se/sergipe/">Sergipe</a></li><li class="diretorio-second-level"><a title="Serra, Lagos e Norte" href="http://g1.globo.com/rj/serra-lagos-norte/">Serra, Lagos e Norte</a></li><li class="diretorio-second-level"><a title="Tecnologia e Games" href="http://g1.globo.com/tecnologia/">Tecnologia e Games</a></li><li class="diretorio-second-level"><a title="Turismo e Viagem" href="http://g1.globo.com/turismo-e-viagem/">Turismo e Viagem</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-globoesporte diretorio-quebra "><a title="globoesporte" href="http://globoesporte.globo.com/">globoesporte</a><ul><li class="diretorio-second-level"><a title="Atletismo" href="http://globoesporte.globo.com/atletismo/">Atletismo</a></li><li class="diretorio-second-level"><a title="Basquete" href="http://globoesporte.globo.com/basquete/">Basquete</a></li><li class="diretorio-second-level"><a title="Basquete/NBB" href="http://globoesporte.globo.com/basquete/nbb/">Basquete/NBB</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie A" href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">BrasileirÃ£o SÃ©rie A</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie B" href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">BrasileirÃ£o SÃ©rie B</a></li><li class="diretorio-second-level"><a title="Campeonato Carioca" href="http://globoesporte.globo.com/rj/futebol/campeonato-carioca/index.html">Campeonato Carioca</a></li><li class="diretorio-second-level"><a title="Campeonato GaÃºcho" href="http://globoesporte.globo.com/rs/futebol/campeonato-gaucho/index.html">Campeonato GaÃºcho</a></li><li class="diretorio-second-level"><a title="Campeonato Mineiro" href="http://globoesporte.globo.com/mg/futebol/campeonato-mineiro/index.html">Campeonato Mineiro</a></li><li class="diretorio-second-level"><a title="Campeonato Paulista" href="http://globoesporte.globo.com/sp/futebol/campeonato-paulista/index.html">Campeonato Paulista</a></li><li class="diretorio-second-level"><a title="Cartola FC" href="http://globoesporte.globo.com/cartola-fc/">Cartola FC</a></li><li class="diretorio-second-level"><a title="Copa do Mundo da Fifa" href="http://globoesporte.globo.com/futebol/copa-do-mundo/index.html">Copa do Mundo da Fifa</a></li><li class="diretorio-second-level"><a title="Copa AmÃ©rica" href="http://globoesporte.globo.com/futebol/copa-america/">Copa AmÃ©rica</a></li><li class="diretorio-second-level"><a title="Copa Sul-Americana" href="http://globoesporte.globo.com/futebol/copa-sul-americana/index.html">Copa Sul-Americana</a></li><li class="diretorio-second-level"><a title="Desafio de Futsal" href="http://globoesporte.globo.com/eventos/desafio-de-futsal/">Desafio de Futsal</a></li><li class="diretorio-second-level"><a title="Esporte Espetacular" href="http://globoesporte.globo.com/programas/esporte-espetacular/">Esporte Espetacular</a></li><li class="diretorio-second-level"><a title="Esportes AquÃ¡ticos" href="http://globoesporte.globo.com/aquaticos/">Esportes AquÃ¡ticos</a></li><li class="diretorio-second-level"><a title="Esportes Radicais" href="http://globoesporte.globo.com/radicais/">Esportes Radicais</a></li><li class="diretorio-second-level"><a title="Eu Atleta" href="http://globoesporte.globo.com/eu-atleta/">Eu Atleta</a></li><li class="diretorio-second-level"><a title="FÃ³rmula 1" href="http://globoesporte.globo.com/motor/formula-1/">FÃ³rmula 1</a></li><li class="diretorio-second-level"><a title="Futebol" href="http://globoesporte.globo.com/futebol/">Futebol</a></li><li class="diretorio-second-level"><a title="Futebol Internacional" href="http://globoesporte.globo.com/futebol/futebol-internacional/">Futebol Internacional</a></li><li class="diretorio-second-level"><a title="FutpÃ©dia" href="http://futpedia.globo.com/">FutpÃ©dia</a></li><li class="diretorio-second-level"><a title="Game Futebol" href="http://gamefutebol.globoesporte.globo.com/">Game Futebol</a></li><li class="diretorio-second-level"><a title="Liga dos CampeÃµes" href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">Liga dos CampeÃµes</a></li><li class="diretorio-second-level"><a title="OlimpÃ­adas" href="http://globoesporte.globo.com/olimpiadas/">OlimpÃ­adas</a></li><li class="diretorio-second-level"><a title="SeleÃ§Ã£o Brasileira" href="http://globoesporte.globo.com/futebol/selecao-brasileira/">SeleÃ§Ã£o Brasileira</a></li><li class="diretorio-second-level"><a title="StockCar" href="http://globoesporte.globo.com/motor/stock-car/">StockCar</a></li><li class="diretorio-second-level"><a title="Surfe" href="http://globoesporte.globo.com/radicais/surfe/">Surfe</a></li><li class="diretorio-second-level"><a title="TÃªnis" href="http://globoesporte.globo.com/tenis/">TÃªnis</a></li><li class="diretorio-second-level"><a title="Vai-e-vem do Mercado" href="http://globoesporte.globo.com/futebol/vai-e-vem-do-mercado/">Vai-e-vem do Mercado</a></li><li class="diretorio-second-level"><a title="VerÃ£o Espetacular" href="http://globoesporte.globo.com/programas/verao-espetacular/">VerÃ£o Espetacular</a></li><li class="diretorio-second-level"><a title="VÃ´lei" href="http://globoesporte.globo.com/volei/">VÃ´lei</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-gshow diretorio-quebra "><a title="gshow" href="http://gshow.globo.com/">gshow</a><ul><li class="diretorio-second-level"><a title="Amorteamo" href="http://gshow.globo.com/programas/amorteamo/">Amorteamo</a></li><li class="diretorio-second-level"><a title="A Teia" href="http://gshow.globo.com/programas/a-teia/">A Teia</a></li><li class="diretorio-second-level"><a title="Altas Horas" href="http://altashoras.globo.com/">Altas Horas</a></li><li class="diretorio-second-level"><a title="Alto Astral" href="http://gshow.globo.com/novelas/alto-astral/index.html">Alto Astral</a></li><li class="diretorio-second-level"><a title="Amor &amp; Sexo" href="http://amoresexo.globo.com/">Amor &amp; Sexo</a></li><li class="diretorio-second-level"><a title="BabilÃ´nia" href="http://gshow.globo.com/novelas/babilonia/index.html">BabilÃ´nia</a></li><li class="diretorio-second-level"><a title="BBB" href="http://bbb.globo.com/">BBB</a></li><li class="diretorio-second-level"><a title="Boogie Oogie" href="http://gshow.globo.com/novelas/boogie-oogie/index.html">Boogie Oogie</a></li><li class="diretorio-second-level"><a title="CaldeirÃ£o do Huck" href="http://caldeiraodohuck.globo.com/">CaldeirÃ£o do Huck</a></li><li class="diretorio-second-level"><a title="Cobras &amp; Lagartos" href="http://gshow.globo.com/novelas/cobras-e-lagartos/videos">Cobras &amp; Lagartos</a></li><li class="diretorio-second-level"><a title="DomingÃ£o do FaustÃ£o" href="http://domingaodofaustao.globo.com/">DomingÃ£o do FaustÃ£o</a></li><li class="diretorio-second-level"><a title="Dupla Identidade" href="http://gshow.globo.com/programas/dupla-identidade/index.html">Dupla Identidade</a></li><li class="diretorio-second-level"><a title="Encontro com FÃ¡tima Bernardes" href="http://tvg.globo.com/programas/encontro-com-fatima-bernardes/">Encontro com FÃ¡tima Bernardes</a></li><li class="diretorio-second-level"><a title="Esquenta!" href="http://tvg.globo.com/programas/esquenta/">Esquenta!</a></li><li class="diretorio-second-level"><a title="Estrelas" href="http://estrelas.globo.com/">Estrelas</a></li><li class="diretorio-second-level"><a title="Felizes para sempre?" href="http://gshow.globo.com/programas/felizes-para-sempre/index.html">Felizes para sempre?</a></li><li class="diretorio-second-level"><a title="ImpÃ©rio" href="http://gshow.globo.com/novelas/imperio/index.html">ImpÃ©rio</a></li><li class="diretorio-second-level"><a title="Mais VocÃª" href="http://maisvoce.globo.com/">Mais VocÃª</a></li><li class="diretorio-second-level"><a title="MalhaÃ§Ã£o" href="http://malhacao.globo.com/">MalhaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Na Moral" href="http://gshow.globo.com/programas/na-moral/">Na Moral</a></li><li class="diretorio-second-level"><a title="O Rei do Gado" href="http://gshow.globo.com/novelas/o-rei-do-gado/videos/">O Rei do Gado</a></li><li class="diretorio-second-level"><a title="PÃ© na Cova" href="http://gshow.globo.com/series/pe-na-cova/2015/">PÃ© na Cova</a></li><li class="diretorio-second-level"><a title="Programa do JÃ´" href="http://programadojo.globo.com/">Programa do JÃ´</a></li><li class="diretorio-second-level"><a title="Receitas.com" href="http://www.receitas.com/">Receitas.com</a></li><li class="diretorio-second-level"><a title="Receitas Mais VocÃª" href="http://www.receitas.com/maisvoce/">Receitas Mais VocÃª</a></li><li class="diretorio-second-level"><a title="Sete Vidas" href="http://gshow.globo.com/novelas/sete-vidas/index.html">Sete Vidas</a></li><li class="diretorio-second-level"><a title="SuperStar" href="http://gshow.globo.com/programas/superstar/index.html">SuperStar</a></li><li class="diretorio-second-level"><a title="TÃ¡ no Ar" href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/index.html">TÃ¡ no Ar</a></li><li class="diretorio-second-level"><a title="Tapas &amp; Beijos" href="http://tapasebeijos.globo.com/">Tapas &amp; Beijos</a></li><li class="diretorio-second-level"><a title="Sexo e as Negas" href="http://gshow.globo.com/programas/sexo-e-as-negas/index.html">Sexo e as Negas</a></li><li class="diretorio-second-level"><a title="The Voice Brasil" href="http://gshow.globo.com/realities/the-voice-brasil/">The Voice Brasil</a></li><li class="diretorio-second-level"><a title="The Voice Kids" href="http://gshow.globo.com/programas/the-voice-kids/">The Voice Kids</a></li><li class="diretorio-second-level"><a title="Totalmente Demais" href="http://gshow.globo.com/novelas/totalmente-demais/">Totalmente Demais</a></li><li class="diretorio-second-level"><a title="Verdades Secretas" href="http://gshow.globo.com/novelas/verdades-secretas/index.html">Verdades Secretas</a></li><li class="diretorio-second-level"><a title="Video Show" href="http://videoshow.globo.com/">Video Show</a></li><li class="diretorio-second-level"><a title="WebsÃ©ries " href="http://gshow.globo.com/programas/webseries/videos/">WebsÃ©ries </a></li><li class="diretorio-second-level"><a title="ZORRA" href="http://gshow.globo.com/programas/zorra/">ZORRA</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-famosos-etc diretorio-quebra "><a title="famosos &amp; etc" href="http://famosos.globo.com/">famosos &amp; etc</a><ul><li class="diretorio-second-level"><a title="EGO" href="http://ego.globo.com/">EGO</a></li><li class="diretorio-second-level"><a title="G1 Cinema" href="http://g1.globo.com/pop-arte/cinema/">G1 Cinema</a></li><li class="diretorio-second-level"><a title="G1 MÃºsica" href="http://g1.globo.com/pop-arte/musica/">G1 MÃºsica</a></li><li class="diretorio-second-level"><a title="Globo Jogos" href="http://jogos.globo.com/">Globo Jogos</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="HorÃ³scopo" href="http://horoscopo.ego.globo.com/ne/home.html">HorÃ³scopo</a></li><li class="diretorio-second-level"><a title="Meus 5 minutos" href="http://www.meus5minutos.com.br/">Meus 5 minutos</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="MÃºsica" href="http://musica.com.br/">MÃºsica</a></li><li class="diretorio-second-level"><a title="Namoro na Web" href="http://tracking.parperfeito.com.br/ppbanner/bannerTracker?originId=13&amp;identifierId=41763&amp;actionId=1">Namoro na Web</a></li><li class="diretorio-second-level"><a title="Paparazzo" href="http://paparazzo.globo.com/">Paparazzo</a></li><li class="diretorio-second-level"><a title="Patricia Kogut" href="http://oglobo.globo.com/cultura/kogut/">Patricia Kogut</a></li><li class="diretorio-second-level"><a title="Retratos da Vida" href="http://extra.globo.com/lazer/retratosdavida/">Retratos da Vida</a></li><li class="diretorio-second-level"><a title="Senninha" href="http://senninha.globo.com/">Senninha</a></li><li class="diretorio-second-level"><a title="Teatro" href="http://www.agentesevenoteatro.com.br/">Teatro</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-grupo-globo diretorio-quebra "><a title="Grupo Globo" href="http://grupoglobo.globo.com/">Grupo Globo</a><ul><li class="diretorio-second-level"><a title="princÃ­pios editoriais" href="http://g1.globo.com/principios-editoriais-do-grupo-globo.html">princÃ­pios editoriais</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-editora-globo diretorio-sem-quebra "><a title="editora globo" href="http://editoraglobo.globo.com/">editora globo</a><ul><li class="diretorio-second-level"><a title="Auto Esporte" href="http://revistaautoesporte.globo.com/">Auto Esporte</a></li><li class="diretorio-second-level"><a title="Casa e Comida" href="http://revistacasaejardim.globo.com/Casa-e-Comida/">Casa e Comida</a></li><li class="diretorio-second-level"><a title="Casa e Jardim" href="http://revistacasaejardim.globo.com/">Casa e Jardim</a></li><li class="diretorio-second-level"><a title="Casa Vogue" href="http://casavogue.globo.com/">Casa Vogue</a></li><li class="diretorio-second-level"><a title="Crescer" href="http://revistacrescer.globo.com/">Crescer</a></li><li class="diretorio-second-level"><a title="Ãpoca" href="http://revistaepoca.globo.com/">Ãpoca</a></li><li class="diretorio-second-level"><a title="Ãpoca NegÃ³cios" href="http://epocanegocios.globo.com/">Ãpoca NegÃ³cios</a></li><li class="diretorio-second-level"><a title="Galileu" href="http://revistagalileu.globo.com/">Galileu</a></li><li class="diretorio-second-level"><a title="Glamour" href="http://revistaglamour.globo.com/">Glamour</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://revistagloborural.globo.com/">Globo Rural</a></li><li class="diretorio-second-level"><a title="GQ Brasil" href="http://gq.globo.com/">GQ Brasil</a></li><li class="diretorio-second-level"><a title="Marie Claire" href="http://revistamarieclaire.globo.com/">Marie Claire</a></li><li class="diretorio-second-level"><a title="Meus 5 Minutos" href="http://meus5minutos.globo.com/">Meus 5 Minutos</a></li><li class="diretorio-second-level"><a title="Monet" href="http://revistamonet.globo.com/">Monet</a></li><li class="diretorio-second-level"><a title="Mundo do SÃ­tio" href="http://mundodositio.globo.com/">Mundo do SÃ­tio</a></li><li class="diretorio-second-level"><a title="PEGN" href="http://revistapegn.globo.com/">PEGN</a></li><li class="diretorio-second-level"><a title="Quem" href="http://revistaquem.globo.com/">Quem</a></li><li class="diretorio-second-level"><a title="Vogue" href="http://vogue.globo.com/">Vogue</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-infoglobo diretorio-sem-quebra "><a title="infoglobo" href="http://infoglobo.com.br/Anuncie/Home.aspx">infoglobo</a><ul><li class="diretorio-second-level"><a title="Ela Digital" href="http://ela.oglobo.globo.com/">Ela Digital</a></li><li class="diretorio-second-level"><a title="Extra" href="http://extra.globo.com/">Extra</a></li><li class="diretorio-second-level"><a title="O Globo" href="http://oglobo.globo.com/">O Globo</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-last "><ul><li class="diretorio-first-level analytics-product glb-menu-globosat diretorio-quebra "><a title="globosat" href="http://canaisglobosat.globo.com/">globosat</a><ul><li class="diretorio-second-level"><a title="Globosat Play" href="http://globosatplay.globo.com/">Globosat Play</a></li><li class="diretorio-second-level"><a title="+Globosat" href="http://maisglobosat.globo.com/">+Globosat</a></li><li class="diretorio-second-level"><a title="BIS" href="http://canalbis.globo.com/">BIS</a></li><li class="diretorio-second-level"><a title="Canal Brasil" href="http://canalbrasil.globo.com/">Canal Brasil</a></li><li class="diretorio-second-level"><a title="Canal Futura" href="http://www.futura.org.br/">Canal Futura</a></li><li class="diretorio-second-level"><a title="Combate" href="http://combate.globo.com/">Combate</a></li><li class="diretorio-second-level"><a title="GloboNews" href="http://globonews.globo.com/">GloboNews</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="Megapix" href="http://megapix.globo.com/">Megapix</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="Off" href="http://canaloff.globo.com/">Off</a></li><li class="diretorio-second-level"><a title="Premiere" href="http://sociopremiere.globo.com/">Premiere</a></li><li class="diretorio-second-level"><a title="SporTV" href="http://sportv.globo.com/">SporTV</a></li><li class="diretorio-second-level"><a title="Studio Universal" href="http://studiouniversal.globo.com/">Studio Universal</a></li><li class="diretorio-second-level"><a title="Syfy" href="http://syfy.globo.com/">Syfy</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Universal Channel" href="http://uc.globo.com/">Universal Channel</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-sistema-globo-de-radio diretorio-sem-quebra "><a title="sistema globo de rÃ¡dio" href="http://globoradio.globo.com/home/HOME.htm">sistema globo de rÃ¡dio</a><ul><li class="diretorio-second-level"><a title="BHFM" href="http://bhfm.globoradio.globo.com/home/HOME.htm">BHFM</a></li><li class="diretorio-second-level"><a title="CBN" href="http://cbn.globoradio.globo.com/home/HOME.htm">CBN</a></li><li class="diretorio-second-level"><a title="RADIOBEAT" href="http://radiobeat.com.br/">RADIOBEAT</a></li><li class="diretorio-second-level"><a title="RÃ¡dio Globo" href="http://radioglobo.globoradio.globo.com/home/HOME.htm">RÃ¡dio Globo</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-globo-filmes diretorio-sem-quebra "><a title="globo filmes" href="http://globofilmes.globo.com/">globo filmes</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-som-livre diretorio-sem-quebra "><a title="som livre" href="http://www.somlivre.com/">som livre</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-blog-globocom diretorio-sem-quebra "><a title="blog globo.com" href="http://blog.globo.com/1.html">blog globo.com</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-zap-imoveis diretorio-sem-quebra "><a title="zap imÃ³veis" href="http://www.zapimoveis.com.br/">zap imÃ³veis</a></li></ul></li></ul></div></div></div></div></div></section><div id="x61" class="opec-area opec-mobile opec-x61 grid-12"><div id="banner_mobile_fim" class="tag-manager-publicidade-container"></div></div><footer><div class="container analytics-area analytics-id-B"><a href="http://www.globo.com/" title="Globo.com">Globo.com</a><span class="copyright">&copy; Copyright 2000-2015 Globo ComunicaÃ§Ã£o e ParticipaÃ§Ãµes S.A.</span><span class="copyright-480 hidden"><p>&copy; 2000-2015.</p><p>Todos os direitos reservados.</p></span><ul class="footer-menu"><li><a href="/privacidade.html">polÃ­tica de privacidade</a></li><li><a href="/todos-os-sites.html">todos os sites</a></li><li><a href="http://anuncie.globo.com/">anuncie conosco</a></li><li><a href="http://meuperfil.globo.com">central globo.com</a></li></ul></div></footer></div><!--[if lt IE 9]><script src="http://s.glbimg.com/en/ho/static/libby/css3pie/js/PIE.js" type="text/javascript"></script><![endif]--><script>
responsiveHub.updateImages();</script><script>
(function(){var scr=document.createElement('script');scr.type = 'text/javascript';scr.async=true;scr.src = 'http://s.glbimg.com/en/ho/static/CACHE/js/d76a896ab28e.js';document.getElementsByTagName('head')[0].appendChild(scr);}());</script><div id="opec-frame1"><div id="banner_floating" class="tag-manager-publicidade-container"></div></div><script>if(document.body && document.body.offsetWidth <= 711){}else{setTimeout(function(){var bannerMiddleTop = $("#ad-position-middle"),
bannerMiddleBottom = $('#ad-position-middle3');if(bannerMiddleTop.height() < 15){bannerMiddleTop.addClass('without-opec');}if(bannerMiddleBottom.height() < 15){bannerMiddleBottom.addClass('without-opec');}}, 6000);}
</script><script>
window.glb=window.glb || {};window.glb.analyticsConfig=window.glb.analyticsConfig  || {};window.glb.analyticsConfig = {scrollEventChance: 0};</script><script>
document.cookie = 'globoapi.geo.geolocation=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.estado=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.novela=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.time=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';setTimeout(function(){window.glb.barra=window.glb.barra || {};window.glb.barra.component=window.glb.barra.component || {};window.glb.barra.component.barraDiv = {className: ''};}, 1000);</script><style>
#assinante-menu .barra-notifications-area .float-box.config-active .save-area{bottom:0;position:absolute;width:100%}#assinante-menu .barra-notifications-area .float-box.config-active .config-box{padding-bottom:44px}</style><style>
header .container{height:150px}.box-destaque-uber .destaques-primario .titulo{font-family:"ProximaNovaA-Bold"}@media (min-width:1600px){.conteudo-uber .box-destaque-uber{position:static;max-width:1600px;margin:0 auto}}</style><style>
.agrupador-quadruplo-tecnologia .destaque-inferior ul{display:none;// css de folder}</style></body></html>
<!-- PÃ¡gina gerada em 13/10/2015 23:07:12 -->
