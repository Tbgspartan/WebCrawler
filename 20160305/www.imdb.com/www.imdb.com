



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

    
    
    

    
    
    

    <meta name="apple-itunes-app" content="app-id=342792525, app-argument=imdb:///?src=mdot">
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "033-7860351-1263178";
                var ue_id = "1WESE1RQ98S4756V8A37";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        
        <meta property="pageType" content="home" />
        <meta property="subpageType" content="main" />


        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />

    <meta property='fb:app_id' content='115109575169727' />

    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="1WESE1RQ98S4756V8A37" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1d-c3-2xl-i-c15cd777.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-903233829._CB298432192_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-3796876714._CB298540748_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-2454701167._CB293329573_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-2508181169._CB288096406_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-3988610918._CB289934424_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
!function a(b,c,d){function e(g,h){if(!c[g]){if(!b[g]){var i="function"==typeof require&&require;if(!h&&i)return i(g,!0);if(f)return f(g,!0);var j=new Error("Cannot find module '"+g+"'");throw j.code="MODULE_NOT_FOUND",j}var k=c[g]={exports:{}};b[g][0].call(k.exports,function(a){var c=b[g][1][a];return e(c?c:a)},k,k.exports,a,b,c,d)}return c[g].exports}for(var f="function"==typeof require&&require,g=0;g<d.length;g++)e(d[g]);return e}({1:[function(a,b,c){"use strict";a(2)},{2:2}],2:[function(a,b,c){"use strict";!function(){var a,b,c=function(a){return"[object Array]"===Object.prototype.toString.call(a)},d=function(a,b){for(var c=0;c<a.length;c++)c in a&&b.call(null,a[c],c)},e=[],f=!1,g=!1,h=function(){var a=[],b=[],c={};return d(e,function(e){var f="";d(e.dartsite.split("/"),function(b){""!==b&&(b in c||(c[b]=a.length,a.push(b)),f+="/"+c[b])}),b.push(f)}),{iu_parts:a,enc_prev_ius:b}},i=function(){var a=[];return d(e,function(b){var c=[];d(b.sizes,function(a){c.push(a.join("x"))}),a.push(c.join("|"))}),a},j=function(){var a=[];return d(e,function(b){a.push(k(b.targeting))}),a.join("|")},k=function(a,b){var c,d=[];for(var e in a){c=[];for(var f=0;f<a[e].length;f++)c.push(encodeURIComponent(a[e][f]));b?d.push(e+"="+encodeURIComponent(c.join(","))):d.push(e+"="+c.join(","))}return d.join("&")},l=function(){var a=+new Date;g||document.readyState&&"loaded"!==document.readyState||(g=!0,f&&imdbads.cmd.push(function(){for(var b=0;b<e.length;b++)generic.monitoring.record_metric(e[b].name+".fail",csm.duration(a))}))};window.tinygpt={define_slot:function(a,b,c,d){e.push({dartsite:a.replace(/\/$/,""),sizes:b,name:c,targeting:d})},set_targeting:function(b){a=b},callback:function(a){for(var c,d,f={},g=+new Date,h=0;h<e.length;h++)c=e[h].dartsite,d=e[h].name,a[h][c]?f[d]=a[h][c]:window.console&&console.error&&console.error("Unable to correlate GPT response for "+d);imdbads.cmd.push(function(){for(var a=0;a<e.length;a++)ad_utils.slot_events.trigger(e[a].name,"request",{timestamp:b}),ad_utils.slot_events.trigger(e[a].name,"tagdeliver",{timestamp:g});ad_utils.gpt.handle_response(f)})},send:function(){var d,g,m=[],n=function(a,b){c(b)&&(b=b.join(",")),b&&m.push(a+"="+encodeURIComponent(""+b))};if(0===e.length)return void tinygpt.callback({});n("gdfp_req","1"),n("correlator",Math.floor(4503599627370496*Math.random())),n("output","json_html"),n("callback","tinygpt.callback"),n("impl","fifs"),n("json_a","1");var o=h();n("iu_parts",o.iu_parts),n("enc_prev_ius",o.enc_prev_ius),n("prev_iu_szs",i()),n("prev_scp",j()),n("cust_params",k(a,!0)),d=document.createElement("script"),g=document.getElementsByTagName("script")[0],d.async=!0,d.type="text/javascript",d.src="http://pubads.g.doubleclick.net/gampad/ads?"+m.join("&"),d.id="tinygpt",d.onload=d.onerror=d.onreadystatechange=l,f=!0,g.parentNode.insertBefore(d,g),b=+new Date}}}()},{}]},{},[1]);</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[300,600],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['b'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['1'],
'u': ['485480988022'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-tarnhelm-4261578659._CB296130991_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"7888ef620eddf223d688c98970222768320e587d",
"2016-03-05T18%3A03%3A27GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 50193;
generic.days_to_midnight = 0.5809375047683716;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-3230827532._CB299577821_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'b']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm == null || window.generic == null || window.consoleLog == null)) {
                    if (window.console && console.log) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-3292747090._CB299577773_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=485480988022;ord=485480988022?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=485480988022?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=485480988022?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/chart/toptv/?ref_=nv_tp_tv250_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                        <li><a href="/chart/top?ref_=nv_mv_250_6"
>Top Rated Movies</a></li>
                        <li><a href="/chart/moviemeter?ref_=nv_mv_mpm_7"
>Most Popular Movies</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/chart/?ref_=nv_ch_cht_1"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_2"
>Oscar Winners</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_3"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/imdbpicks/tv-releases/?ref_=nv_tvv_picks_1"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_2"
>On Tonight</a></li>
                        <li><a href="/chart/toptv/?ref_=nv_tvv_250_3"
>Top Rated TV Shows</a></li>
                        <li><a href="/chart/tvmeter?ref_=nv_tvv_mptv_4"
>Most Popular TV Shows</a></li>
                        <li><a href="/feature/watch-now-on-amazon/?ref_=nv_tvv_wn_5"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_6"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_2"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/imdbpicks/?ref_=nv_sf_picks_3"
>IMDb Picks</a></li>
                        <li><a href="/best-of/?ref_=nv_sf_bstof_4"
>Best of 2015</a></li>
                        <li><a href="/youtube-originals/?ref_=nv_sf_yto_5"
>YouTube Originals</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=03-05&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/awards-central/oscars/?ref_=nv_ev_osc_2"
>Oscar Nominees</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_3"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_4"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_5"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_6"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_7"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_8"
>Emmy Awards</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_9"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_10"
>Toronto Film Festival</a></li>
                        <li><a href="/event/all/?ref_=nv_ev_all_11"
>All Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni59582924/?ref_=nv_nw_tn_1"
> Pat Conroy, Author of âThe Prince of Tides,â âThe Great Santini,â Dies at 70
</a><br />
                        <span class="time">12 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59583597/?ref_=nv_nw_tn_2"
> âZootopiaâ Tops Box Office Food Chain With $70 Million Debut
</a><br />
                        <span class="time">1 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59581676/?ref_=nv_nw_tn_3"
> Spider-Man Spinoff âVenomâ Gets New Life at Sony With âEdge of Tomorrowâ Writer
</a><br />
                        <span class="time">19 hours ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_3"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5>
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0053604/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMjAwODAyOTcwOF5BMl5BanBnXkFtZTcwNTAxNjMyNA@@._V1._SY315_CR35,0,410,315_CT10_.jpg",
            titleYears : "1960",
            rank : 102,
                    headline : "The Apartment"
    },
    nameAd : {
            clickThru : "/name/nm0001401/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTg4MDgyNDQxOF5BMl5BanBnXkFtZTcwMjU5MDY0Nw@@._V1._SX250_CR0,15,250,315_.jpg",
            rank : 175,
            headline : "Angelina Jolie"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYtX-w6Ev2PN5jqJ45_rf_JfZu4MOVKWgyTfzvz0kln3C2--JsASTOW26OLXUyfb-LhRtWSsC2_%0D%0A75KCOkkNP5RbQU-RVm3Ev8m3tjDXEZvv_fxGhTvBUFVLC3GPGtxRW3mvu9GoDNcpughCXNitc6WC%0D%0A8q03ZqXt0y6uVh7_JB9C497ZWRv7xbW2LfdrPlHA8bNLMdktakaudzPFJtwrNKJa_Q%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYn5m_L9OWDrCSohPgQjbsvEkzAK_AzcOXwrzsjs4jnSU8M96ydPmrzyJf5l_SFPRZ32RT2ExQf%0D%0A1rpONl23FEZhd-I6ISJcThBOYO27ytZmERBpDks0ewF398X0biCaNhtPWx4sYWPuT9ZYwytj8-ih%0D%0AaLQDzoYYmYIzdq5JXN2bk3Mnm0uxZ6dXOFhx_dY-PP7H%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYkM9Qr2CUETyWyhNduRxca9vLglXlQkrM0CfUFuXgEqA3dj2fhOyPUbFnMsoqJprUO4pwoGZLo%0D%0ANQx6h49qMNe5FbodwiFaB64u_HatybxB8GIOPTj5SBmrppqr_ygjtoYVHrfJLqW2ekNQgn7ISTTN%0D%0ASMtPytmaCrd-PYtH7ZcTeXsrDZSdgRo767HGUx58gRuk%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=485480988022;ord=485480988022?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>

    <div id="pagecontent">
    <div class="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=485480988022;ord=485480988022?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	

    
    
    

    
    
    

    
    
    
    </div>
    <div id="top-slot-wrapper" class="pagecontent">

    
    
    

    
    
    
    </div>
    <div class="pagecontent">
        <div id="content-2-wide">
            <div id="main">

    
    
        <a name="slot_hero"></a>
        <div class="heroWidget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','HeroWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_hero">
<div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3895375129?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425850342&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_1" data-video="vi3895375129" data-source="bylist" data-id="ls002309697" data-rid="1WESE1RQ98S4756V8A37" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="Thirty years after the beloved original franchise took the world by storm, director Paul Feig brings his fresh take to the supernatural comedy, joined by Melissa McCarthy, Kristen Wiig, Kate McKinnon, Leslie Jones, and Chris Hemsworth." alt="Thirty years after the beloved original franchise took the world by storm, director Paul Feig brings his fresh take to the supernatural comedy, joined by Melissa McCarthy, Kristen Wiig, Kate McKinnon, Leslie Jones, and Chris Hemsworth." src="http://ia.media-imdb.com/images/M/MV5BNzAzODQ1NTk4OF5BMl5BanBnXkFtZTgwODIwOTIwODE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNzAzODQ1NTk4OF5BMl5BanBnXkFtZTgwODIwOTIwODE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Thirty years after the beloved original franchise took the world by storm, director Paul Feig brings his fresh take to the supernatural comedy, joined by Melissa McCarthy, Kristen Wiig, Kate McKinnon, Leslie Jones, and Chris Hemsworth." title="Thirty years after the beloved original franchise took the world by storm, director Paul Feig brings his fresh take to the supernatural comedy, joined by Melissa McCarthy, Kristen Wiig, Kate McKinnon, Leslie Jones, and Chris Hemsworth." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Thirty years after the beloved original franchise took the world by storm, director Paul Feig brings his fresh take to the supernatural comedy, joined by Melissa McCarthy, Kristen Wiig, Kate McKinnon, Leslie Jones, and Chris Hemsworth." title="Thirty years after the beloved original franchise took the world by storm, director Paul Feig brings his fresh take to the supernatural comedy, joined by Melissa McCarthy, Kristen Wiig, Kate McKinnon, Leslie Jones, and Chris Hemsworth." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt1289401/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425850342&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_1" > Ghostbusters </a> </div> </div> <div class="secondary ellipsis"> Official Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1813099801?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425850342&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_2" data-video="vi1813099801" data-source="bylist" data-id="ls002252034" data-rid="1WESE1RQ98S4756V8A37" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="'Middle School: The Worst Years of My Life' chronicles the trials and triumphs of Rafe Khatchadorian, as he uses his wits to battle bullies, hormones and the tyrannical, test-obsessed Principal Dwight." alt="'Middle School: The Worst Years of My Life' chronicles the trials and triumphs of Rafe Khatchadorian, as he uses his wits to battle bullies, hormones and the tyrannical, test-obsessed Principal Dwight." src="http://ia.media-imdb.com/images/M/MV5BMzk1MzAzMzg2OV5BMl5BanBnXkFtZTgwNTY4ODIyODE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzk1MzAzMzg2OV5BMl5BanBnXkFtZTgwNTY4ODIyODE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="'Middle School: The Worst Years of My Life' chronicles the trials and triumphs of Rafe Khatchadorian, as he uses his wits to battle bullies, hormones and the tyrannical, test-obsessed Principal Dwight." title="'Middle School: The Worst Years of My Life' chronicles the trials and triumphs of Rafe Khatchadorian, as he uses his wits to battle bullies, hormones and the tyrannical, test-obsessed Principal Dwight." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="'Middle School: The Worst Years of My Life' chronicles the trials and triumphs of Rafe Khatchadorian, as he uses his wits to battle bullies, hormones and the tyrannical, test-obsessed Principal Dwight." title="'Middle School: The Worst Years of My Life' chronicles the trials and triumphs of Rafe Khatchadorian, as he uses his wits to battle bullies, hormones and the tyrannical, test-obsessed Principal Dwight." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt4981636/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425850342&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_2" > Middle School: The Worst Years of My Life </a> </div> </div> <div class="secondary ellipsis"> Trailer #1 </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2752623897?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425850342&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_3" data-video="vi2752623897" data-source="bylist" data-id="ls002322762" data-rid="1WESE1RQ98S4756V8A37" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="'The Meddler' follows Marnie Minervini (Susan Sarandon), recent widow and eternal optimist, as she moves from New Jersey to Los Angeles to be closer to her daughter (Rose Byrne). Armed with an iPhone and a full bank account, Marnie sets out to make friends, find her purpose, and possibly open up to someone new." alt="'The Meddler' follows Marnie Minervini (Susan Sarandon), recent widow and eternal optimist, as she moves from New Jersey to Los Angeles to be closer to her daughter (Rose Byrne). Armed with an iPhone and a full bank account, Marnie sets out to make friends, find her purpose, and possibly open up to someone new." src="http://ia.media-imdb.com/images/M/MV5BMTc0NDQ2Njk4Nl5BMl5BanBnXkFtZTgwMTQwMDMyODE@._V1_SY298_CR1,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc0NDQ2Njk4Nl5BMl5BanBnXkFtZTgwMTQwMDMyODE@._V1_SY298_CR1,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="'The Meddler' follows Marnie Minervini (Susan Sarandon), recent widow and eternal optimist, as she moves from New Jersey to Los Angeles to be closer to her daughter (Rose Byrne). Armed with an iPhone and a full bank account, Marnie sets out to make friends, find her purpose, and possibly open up to someone new." title="'The Meddler' follows Marnie Minervini (Susan Sarandon), recent widow and eternal optimist, as she moves from New Jersey to Los Angeles to be closer to her daughter (Rose Byrne). Armed with an iPhone and a full bank account, Marnie sets out to make friends, find her purpose, and possibly open up to someone new." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="'The Meddler' follows Marnie Minervini (Susan Sarandon), recent widow and eternal optimist, as she moves from New Jersey to Los Angeles to be closer to her daughter (Rose Byrne). Armed with an iPhone and a full bank account, Marnie sets out to make friends, find her purpose, and possibly open up to someone new." title="'The Meddler' follows Marnie Minervini (Susan Sarandon), recent widow and eternal optimist, as she moves from New Jersey to Los Angeles to be closer to her daughter (Rose Byrne). Armed with an iPhone and a full bank account, Marnie sets out to make friends, find her purpose, and possibly open up to someone new." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt4501454/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425850342&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_3" > The Meddler </a> </div> </div> <div class="secondary ellipsis"> Official Trailer </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/trailers?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425850342&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_sm" class="position_bottom supplemental" >Browse more trailers</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','HeroWidget',{wb:1});}
                if(typeof uet === 'function'){uet("cf");}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-1"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/imdbpicks/downton-abbey-stars-in-out-costume?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425809202&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_do_hd" > <h3>"Downton Abbey" in and out of Costume</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/downton-abbey-stars-in-out-costume?imageid=rm1450811904&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425809202&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_do_i_1" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BNTI5ODI3MzUxM15BMl5BanBnXkFtZTcwNDA3NDIwOQ@@._UX600_CR70,200,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNTI5ODI3MzUxM15BMl5BanBnXkFtZTcwNDA3NDIwOQ@@._UX600_CR70,200,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/downton-abbey-stars-in-out-costume?imageid=rm3213927168&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425809202&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_do_i_2" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTQwMTI2ODIyNl5BMl5BanBnXkFtZTcwMzA4NzIxOQ@@._UX700_CR160,80,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQwMTI2ODIyNl5BMl5BanBnXkFtZTcwMzA4NzIxOQ@@._UX700_CR160,80,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/downton-abbey-stars-in-out-costume?imageid=rm3709787392&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425809202&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_do_i_3" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BNzQ2MDEyMjA5MF5BMl5BanBnXkFtZTgwMjM2MDQ1MDE@._UX1300_CR450,20,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNzQ2MDEyMjA5MF5BMl5BanBnXkFtZTgwMjM2MDQ1MDE@._UX1300_CR450,20,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Prepare to say goodbye to "<a href="/title/tt1606375/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425809202&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_do_lk1">Downton Abbey</a>" by taking a look at photos of the cast in character and in real life.</p> <p class="seemore"><a href="/imdbpicks/downton-abbey-stars-in-out-costume?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425809202&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_do_sm" class="position_bottom supplemental" >See more photos of the "Downton" cast</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
                if(typeof uet === 'function'){uet("af");}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-2"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>It's a "Walking Dead" Weekend</h3> </span> </span> <p class="blurb">Get ready for another episode of <a href="/title/tt1520211/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425790502&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_wd_lk1">AMC's hit series</a> this Sunday with photos from Season 6 and a look at the much-anticipated Negan.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/the-walking-dead-photos?imageid=rm2009733888&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425790502&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_wd_i_1" > <img itemprop="image" class="pri_image" title="Still of Norman Reedus in The Walking Dead (2010)" alt="Still of Norman Reedus in The Walking Dead (2010)" src="http://ia.media-imdb.com/images/M/MV5BMjQ0MzI5OTMyNF5BMl5BanBnXkFtZTgwMDAyMTExODE@._V1_SY230_CR19,0,307,230_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQ0MzI5OTMyNF5BMl5BanBnXkFtZTgwMDAyMTExODE@._V1_SY230_CR19,0,307,230_AL_UY460_UX614_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/imdbpicks/the-walking-dead-photos?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425790502&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_wd_cap_pri_1" > Photos From Season 6 </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/walking-dead-who-is-negan/ls031009976?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425790502&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_wd_i_2" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjI0MTgwMzYyNV5BMl5BanBnXkFtZTgwMDgyOTg4NTE@._UX700_CR50,45,614,460_SY230_SX307_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI0MTgwMzYyNV5BMl5BanBnXkFtZTgwMDgyOTg4NTE@._UX700_CR50,45,614,460_SY230_SX307_AL_UY460_UX614_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/imdbpicks/walking-dead-who-is-negan/ls031009976?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425790502&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_wd_cap_pri_2" > Who Is Negan? When Is He Showing Up? </a> </div> </div> </div> </div> </div> </div> </div> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-3"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NewsDeskWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59582924?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTkwMjM0MzY1NF5BMl5BanBnXkFtZTgwNTUxMDQyODE@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59582924?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1"
class="headlines" >Pat Conroy, Author of âThe Prince of Tides,â âThe Great Santini,â Dies at 70</a>
    <div class="infobar">
            <span class="text-muted">12 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_src"
>Variety - Film News</a></span>
    </div>
                                </div>
<p> <a href="/name/nm0175852?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk1">Pat Conroy</a>, the South Carolina author of bestsellers including â<a href="/title/tt0102713?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk2">The Prince of Tides</a>,â â<a href="/title/tt0079239?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk3">The Great Santini</a>â and â<a href="/title/tt0085867?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk4">The Lords of Discipline</a>,â died Friday in Beaufort, S.C. He was 70 and had been battling pancreatic cancer. Conroy was known for his family novels and memoirs that were often based on his ...                                        <span class="nobr"><a href="/news/ni59582924?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59583597?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2"
class="headlines" >âZootopiaâ Tops Box Office Food Chain With $70 Million Debut</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59581676?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3"
class="headlines" >Spider-Man Spinoff âVenomâ Gets New Life at Sony With âEdge of Tomorrowâ Writer</a>
    <div class="infobar">
            <span class="text-muted">19 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3_src"
>The Wrap</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59582337?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4"
class="headlines" >How Mark Rylance Got An Oscar For âBridge Of Spiesâ Without Ever Stepping Onto The Campaign Circuit</a>
    <div class="infobar">
            <span class="text-muted">16 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4_src"
>Deadline</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59582511?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5"
class="headlines" >Sony Flush With 2017 Franchises With âThe Dark Towerâ, âBad Boys 3â, Barbie & Maybe âMIB23â Slotted</a>
    <div class="infobar">
            <span class="text-muted">16 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5_src"
>Deadline</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59583597?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BOTMyMjEyNzIzMV5BMl5BanBnXkFtZTgwNzIyNjU0NzE@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59583597?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1"
class="headlines" >âZootopiaâ Tops Box Office Food Chain With $70 Million Debut</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_src"
>Variety - Film News</a></span>
    </div>
                                </div>
<p>Disneyâs â<a href="/title/tt2948356?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk1">Zootopia</a>â isÂ apex predator atÂ the box office this weekend with an expected three-day haul around $70 million, according to studio estimates.Herds of moviegoers are expected to turn out for the animated family comedy on Saturday and Sunday after the pic pulled in $19.5Â million on Friday ...                                        <span class="nobr"><a href="/news/ni59583597?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59582924?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2"
class="headlines" >Pat Conroy, Author of âThe Prince of Tides,â âThe Great Santini,â Dies at 70</a>
    <div class="infobar">
            <span class="text-muted">12 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59582337?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3"
class="headlines" >How Mark Rylance Got An Oscar For âBridge Of Spiesâ Without Ever Stepping Onto The Campaign Circuit</a>
    <div class="infobar">
            <span class="text-muted">16 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3_src"
>Deadline</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59582523?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4"
class="headlines" >Joseph Gordon-Levittâs âSandmanâ Advances With âConjuring 2â Writer</a>
    <div class="infobar">
            <span class="text-muted">15 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59583500?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5"
class="headlines" >Disney Making Live-Action âNutcrackerâ With âChocolatâ Director Lasse HallstrÃ¶m</a>
    <div class="infobar">
            <span class="text-muted">3 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000151?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5_src"
>Slash Film</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59582335?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjQwNDg5OTI3NF5BMl5BanBnXkFtZTgwOTcxMzcyNzE@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59582335?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1"
class="headlines" >Courteney Coxâs Assistant Cast in Fox Comedy Pilot âCharity Caseâ</a>
    <div class="infobar">
            <span class="text-muted">16 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_src"
>Variety - TV News</a></span>
    </div>
                                </div>
<p><a href="/name/nm5169583?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk1">Diona Reasonover</a> has been cast in â<a href="/title/tt5511136?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk2">Charity Case</a>,â <a href="/name/nm0001073?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk3">Courteney Cox</a>âs Fox pilot,Â Variety has learned exclusively. She will play Coxâs on-screen assistant in the comedy. â<a href="/title/tt5511136?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk4">Charity Case</a>â revolves around HaileyÂ (Cox), who inherits her late billionaire husbandâs charity but quickly finds that changing the ...                                        <span class="nobr"><a href="/news/ni59582335?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59582610?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2"
class="headlines" >âTwin Peaksâ Reboot Adds Patrick Fischler & David Dastmalchian</a>
    <div class="infobar">
            <span class="text-muted">15 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004913?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2_src"
>Deadline TV</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59582336?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3"
class="headlines" >TV News Roundup: Julia Louis-Dreyfus Producing HBO Miniseries, âLegends of the Hidden Templeâ Reboot Casts Young Star</a>
    <div class="infobar">
            <span class="text-muted">16 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59581032?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4"
class="headlines" >Carla Gugino and More Are Returning For âWayward Pinesâ Season Two</a>
    <div class="infobar">
            <span class="text-muted">22 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000151?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4_src"
>Slash Film</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59580734?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5"
class="headlines" >âLady Gagaâ Says Sheâs Returning To âAmerican Horror Storyâ â But No Details</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004913?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5_src"
>Deadline TV</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59583629?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMzJkZTRjNTYtZGE4NC00MGVmLTg0NmItODBkNDg2MGQ2ZjI0XkEyXkFqcGdeQXVyMTExNDQ2MTI@._V1_SY150_CR24,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59583629?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1"
class="headlines" >Dwayne Johnson and Kelly Rohrbach Get Baywatch-Ready in Those Iconic Red Swimsuits</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_src"
>PEOPLE.com</a></span>
    </div>
                                </div>
<p>We already knew <a href="/name/nm0425005?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk1">Dwayne Johnson</a> and <a href="/name/nm4859097?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk2">Kelly Rohrbach</a> had great chemistry - and now we know what they look like together in their <a href="/title/tt1469304?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk3">Baywatch</a> outfits. In a word? Muscular. "Muscle contest with @therock .... I feel like I won," Rohrbach, 26, who will play CJ Parker in the 2017 big-screen reboot of the ...                                        <span class="nobr"><a href="/news/ni59583629?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59583764?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2"
class="headlines" >Leonardo DiCaprio Hits the Town with His Guy Friends Nearly One Week After Best Actor Oscar Win</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59582659?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3"
class="headlines" >Jennifer Garner Says Ben Affleck Dressing as Batman for Their Son's Birthday Party Was 'the Best Dad Moment in History'</a>
    <div class="infobar">
            <span class="text-muted">15 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59582936?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4"
class="headlines" >Charlie Sheen Asks Court to Reduce $55,000-a-Month Child Support Payment, Says His Income Has Dropped Significantly</a>
    <div class="infobar">
            <span class="text-muted">13 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59583531?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5"
class="headlines" >Sarah Michelle Gellar Dyes Her Hair Brown Ahead of Cruel Intentions TV Sequel</a>
    <div class="infobar">
            <span class="text-muted">2 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5_src"
>E! Online</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NewsDeskWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-4"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/gallery/rg1656920832?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2426533362&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_hd" > <h3>Photos We Love: Week of March 4th</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm927668224/rg1656920832?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2426533362&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_1" > <img itemprop="image" class="pri_image" title="Leonardo DiCaprio, Ken Ham, Ray Comfort, Kent Hovind and Eric Hovind" alt="Leonardo DiCaprio, Ken Ham, Ray Comfort, Kent Hovind and Eric Hovind" src="http://ia.media-imdb.com/images/M/MV5BMTk2MzAzOTQ1Ml5BMl5BanBnXkFtZTgwODYxNDMyODE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk2MzAzOTQ1Ml5BMl5BanBnXkFtZTgwODYxNDMyODE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm2341410816/rg1656920832?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2426533362&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_2" > <img itemprop="image" class="pri_image" title="The Walking Dead (2010)" alt="The Walking Dead (2010)" src="http://ia.media-imdb.com/images/M/MV5BN2U5ZjNjYmItNzIzMS00OGVlLWE2YzYtYmYzMzQ1OGM5YWMxXkEyXkFqcGdeQXVyNjA2NzAwOTc@._V1_SY201_CR70,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BN2U5ZjNjYmItNzIzMS00OGVlLWE2YzYtYmYzMzQ1OGM5YWMxXkEyXkFqcGdeQXVyNjA2NzAwOTc@._V1_SY201_CR70,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3528071168/rg1656920832?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2426533362&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_3" > <img itemprop="image" class="pri_image" title="Charlie Sheen" alt="Charlie Sheen" src="http://ia.media-imdb.com/images/M/MV5BMTAyNjczNTM5MTReQTJeQWpwZ15BbWU4MDQyMTQzMjgx._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTAyNjczNTM5MTReQTJeQWpwZ15BbWU4MDQyMTQzMjgx._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/gallery/rg1656920832?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2426533362&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_sm" class="position_bottom supplemental" >See the full gallery</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-5"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','BornTodayWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_borntoday">
<span class="widget_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=3-5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_hd" > <h3>Born Today</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0578949?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_1" > <img itemprop="image" class="pri_image" title="Eva Mendes" alt="Eva Mendes" src="http://ia.media-imdb.com/images/M/MV5BMjA0MDQwMDI4NF5BMl5BanBnXkFtZTcwMjc1OTU1NA@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA0MDQwMDI4NF5BMl5BanBnXkFtZTcwMjc1OTU1NA@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0578949?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_1">Eva Mendes</a> (42) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0086883?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_2" > <img itemprop="image" class="pri_image" title="Jolene Blalock" alt="Jolene Blalock" src="http://ia.media-imdb.com/images/M/MV5BMTYyMDUyNjE5MV5BMl5BanBnXkFtZTgwNTA5NDU5NjE@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTYyMDUyNjE5MV5BMl5BanBnXkFtZTgwNTA5NDU5NjE@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0086883?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_2">Jolene Blalock</a> (41) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1641251?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_3" > <img itemprop="image" class="pri_image" title="Riki Lindhome" alt="Riki Lindhome" src="http://ia.media-imdb.com/images/M/MV5BMTAxNzM1NTkxOTleQTJeQWpwZ15BbWU3MDUwNDU2MjE@._V1_SY172_CR3,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTAxNzM1NTkxOTleQTJeQWpwZ15BbWU3MDUwNDU2MjE@._V1_SY172_CR3,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1641251?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_3">Riki Lindhome</a> (37) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0005157?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_4" > <img itemprop="image" class="pri_image" title="Jake Lloyd" alt="Jake Lloyd" src="http://ia.media-imdb.com/images/M/MV5BMTk2MDExNzQwN15BMl5BanBnXkFtZTgwMzM2NzQwNjE@._V1_SY172_CR8,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk2MDExNzQwN15BMl5BanBnXkFtZTgwMzM2NzQwNjE@._V1_SY172_CR8,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0005157?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_4">Jake Lloyd</a> (27) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0175305?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_5" > <img itemprop="image" class="pri_image" title="Kevin Connolly" alt="Kevin Connolly" src="http://ia.media-imdb.com/images/M/MV5BMTM1NjcwOTg4N15BMl5BanBnXkFtZTcwMTg0NjA3Mg@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTM1NjcwOTg4N15BMl5BanBnXkFtZTcwMTg0NjA3Mg@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0175305?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_5">Kevin Connolly</a> (42) </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/search/name?refine=birth_monthday&birth_monthday=3-5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_sm" class="position_bottom supplemental" >See all birthdays</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','BornTodayWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-7"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/imdbpicks/march-picks/ls033648909?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425789522&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_mar_hd" > <h3>Editors' Picks: Movies & TV Shows We Can't Wait to See</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/march-picks/ls033648909?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425789522&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_mar_i_1" > <img itemprop="image" class="pri_image" title="10 Cloverfield Lane (2016)" alt="10 Cloverfield Lane (2016)" src="http://ia.media-imdb.com/images/M/MV5BMjEzMjczOTIxMV5BMl5BanBnXkFtZTgwOTUwMjI3NzE@._V1_SX148_CR0,0,148,219_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjEzMjczOTIxMV5BMl5BanBnXkFtZTgwOTUwMjI3NzE@._V1_SX148_CR0,0,148,219_AL_UY438_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/march-picks/ls033648909?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425789522&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_mar_i_2" > <img itemprop="image" class="pri_image" title="Tina Fey in Whiskey Tango Foxtrot (2016)" alt="Tina Fey in Whiskey Tango Foxtrot (2016)" src="http://ia.media-imdb.com/images/M/MV5BMjIxOTIzMTM5OF5BMl5BanBnXkFtZTgwNDIxNTA1NzE@._V1_SX148_CR0,0,148,219_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjIxOTIzMTM5OF5BMl5BanBnXkFtZTgwNDIxNTA1NzE@._V1_SX148_CR0,0,148,219_AL_UY438_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/march-picks/ls033648909?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425789522&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_mar_i_3" > <img itemprop="image" class="pri_image" title="Knight of Cups (2015)" alt="Knight of Cups (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjQyOTcwODIyNF5BMl5BanBnXkFtZTgwMDE4OTI4NzE@._V1_SX148_CR0,0,148,219_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQyOTcwODIyNF5BMl5BanBnXkFtZTgwMDE4OTI4NzE@._V1_SX148_CR0,0,148,219_AL_UY438_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/march-picks/ls033648909?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425789522&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_mar_i_4" > <img itemprop="image" class="pri_image" title="Damien (2016)" alt="Damien (2016)" src="http://ia.media-imdb.com/images/M/MV5BODVmMDQ4M2YtNDQ1ZS00N2E3LWEzMjQtNzYxYTlkM2U4MGFmXkEyXkFqcGdeQXVyNjEwNTM2Mzc@._V1_SY219_CR8,0,148,219_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BODVmMDQ4M2YtNDQ1ZS00N2E3LWEzMjQtNzYxYTlkM2U4MGFmXkEyXkFqcGdeQXVyNjEwNTM2Mzc@._V1_SY219_CR8,0,148,219_AL_UY438_UX296_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">What has us excited this March? The mysterious sorta-sequel <a href="/title/tt1179933/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425789522&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_mar_lk1"><i>10 Cloverfield Lane</i></a> for starters, as well as <i><a href="/title/tt3553442/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425789522&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_mar_lk2">Whiskey Tango Foxtrot</a></i>, Terrence Malick's <a href="/title/tt2101383/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425789522&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_mar_lk3"><i>Knight of Cups</i></a>, and A&E's "<a href="/title/tt4337944/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425789522&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_mar_lk4">Damien</a>." Read on for our complete list of picks.</p> <p class="seemore"><a href="/imdbpicks/march-picks/ls033648909?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425789522&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_mar_sm" class="position_bottom supplemental" >See our March movie and TV picks</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-8"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/youtube-originals/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2426230962&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_yto_pdp_hd" > <h3>YouTube Red Originals Spotlight: "Scare PewDiePie"</h3> </a> </span> </span> <p class="blurb">Check out photos of "<a href="/title/tt5435072/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2426230962&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_yto_pdp_lk1">Scare PewDiePie</a>," a <a href="/youtube-originals/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2426230962&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_yto_pdp_lk2">YouTube Red Originals</a> series that follows PewDiePie as he encounters terrifying situations inspired from his favorite video games. Plus, learn more about YouTube creator <a href="/name/nm5579304/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2426230962&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_yto_pdp_lk3">Felix (PewDiePie) Kjellberg</a>.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/youtube-originals/scare-pewdiepie-photos?imageid=rm3855227392&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2426230962&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_yto_pdp_i_1" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTQ2ODQ1MTE1MV5BMl5BanBnXkFtZTgwMjk4ODEwODE@._UX1000_CR110,70,614,460_SY230_SX307_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ2ODQ1MTE1MV5BMl5BanBnXkFtZTgwMjk4ODEwODE@._UX1000_CR110,70,614,460_SY230_SX307_AL_UY460_UX614_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/youtube-originals/scare-pewdiepie-photos?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2426230962&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_yto_pdp_cap_pri_1" > "Scare PewDiePie" Photos </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/youtube-originals/pewdiepie-trivia/ls033365956?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2426230962&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_yto_pdp_i_2" > <img itemprop="image" class="pri_image" title="Felix Kjellberg in Scare PewDiePie (2016)" alt="Felix Kjellberg in Scare PewDiePie (2016)" src="http://ia.media-imdb.com/images/M/MV5BMzc2NjMxOTA0MF5BMl5BanBnXkFtZTgwNDM5Njc5NzE@._V1_SY230_CR17,0,307,230_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzc2NjMxOTA0MF5BMl5BanBnXkFtZTgwNDM5Njc5NzE@._V1_SY230_CR17,0,307,230_AL_UY460_UX614_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/youtube-originals/pewdiepie-trivia/ls033365956?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2426230962&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_yto_pdp_cap_pri_2" > 5 Things to Know About PewDiePie </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/youtube-originals/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2426230962&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_yto_pdp_sm" class="position_bottom supplemental" >Discover YouTube Red Originals on IMDb</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-13"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','RecsWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','RecsWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-19"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','TriviaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_trivia">
<span class="widget_header"> <span class="oneline"> <a href="/title/tt3797868/trivia?item=tr2299422&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_hd" > <h3>Did You Know?</h3> </a> </span> </span> <div class="widget_content inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt3797868?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_i_1" > <img itemprop="image" class="pri_image" title="The Choice (2016)" alt="The Choice (2016)" src="http://ia.media-imdb.com/images/M/MV5BMTYzMDA2ODcxMV5BMl5BanBnXkFtZTgwODMzOTMzNzE@._V1_SX89_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTYzMDA2ODcxMV5BMl5BanBnXkFtZTgwODMzOTMzNzE@._V1_SX89_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> <div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="widget_inline_blurb"><strong class="text-large"><a href="/title/tt3797868?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">The Choice</a></strong> <p class="blurb">Clint Eastwood's son, Scott Eastwood, was originally cast in this film but was replaced by Tom Welling and put into another movie from a book also written by Sparks, called The Longest Ride, which came out in April 10, 2015.</p> <p class="seemore"><a href="/title/tt3797868/trivia?item=tr2299422&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_sm" class="position_blurb supplemental" >See more trivia</a></p> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','TriviaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-26"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','PollWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;5W9K4jnS5Y4&quot;}">
        
    

    <div class="ab_poll poll">
<span class="widget_header"> <span class="oneline"> <a href="/poll/5W9K4jnS5Y4/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2426533322&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_hd" > <h3>Poll: Most compelling film by George Miller?</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2426533322&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_mp">More Polls</a></h4> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/5W9K4jnS5Y4/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2426533322&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_1" > <img itemprop="image" class="pri_image" title="Mad Max (1979)" alt="Mad Max (1979)" src="http://ia.media-imdb.com/images/M/MV5BMTM4Mjg5ODEzMV5BMl5BanBnXkFtZTcwMDc3NDk0NA@@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTM4Mjg5ODEzMV5BMl5BanBnXkFtZTcwMDc3NDk0NA@@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/5W9K4jnS5Y4/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2426533322&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_2" > <img itemprop="image" class="pri_image" title="Mad Max 2: The Road Warrior (1981)" alt="Mad Max 2: The Road Warrior (1981)" src="http://ia.media-imdb.com/images/M/MV5BMTcxMDUyODY1OF5BMl5BanBnXkFtZTYwOTQzNDk4._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTcxMDUyODY1OF5BMl5BanBnXkFtZTYwOTQzNDk4._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/5W9K4jnS5Y4/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2426533322&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_3" > <img itemprop="image" class="pri_image" title="Mad Max Beyond Thunderdome (1985)" alt="Mad Max Beyond Thunderdome (1985)" src="http://ia.media-imdb.com/images/M/MV5BMTk0MDQ5NTYxNV5BMl5BanBnXkFtZTcwNTA0ODYyMQ@@._V1_SY207_CR7,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk0MDQ5NTYxNV5BMl5BanBnXkFtZTcwNTA0ODYyMQ@@._V1_SY207_CR7,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/5W9K4jnS5Y4/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2426533322&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_4" > <img itemprop="image" class="pri_image" title="The Witches of Eastwick (1987)" alt="The Witches of Eastwick (1987)" src="http://ia.media-imdb.com/images/M/MV5BMTI5MTgxMzIzMl5BMl5BanBnXkFtZTcwNDA5MTYyMQ@@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTI5MTgxMzIzMl5BMl5BanBnXkFtZTcwNDA5MTYyMQ@@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/5W9K4jnS5Y4/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2426533322&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_5" > <img itemprop="image" class="pri_image" title="Lorenzo's Oil (1992)" alt="Lorenzo's Oil (1992)" src="http://ia.media-imdb.com/images/M/MV5BMTI5OTcxMzI0M15BMl5BanBnXkFtZTcwNzAwNDAzMQ@@._V1_SY207_CR2,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTI5OTcxMzI0M15BMl5BanBnXkFtZTcwNzAwNDAzMQ@@._V1_SY207_CR2,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Which feature length film directed by George Miller is your favorite? Discuss <a href="http://www.imdb.com/board/bd0000088/nest/254190110?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2426533322&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk1">here</a> after voting.</p> <p class="seemore"><a href="/poll/5W9K4jnS5Y4/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2426533322&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_sm" class="position_bottom supplemental" >Vote now</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','PollWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-28"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2416509422&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_hd" > <h3>Catch Up on IMDb Asks</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2416509422&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_i_1" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTY0ODEwNTcwOV5BMl5BanBnXkFtZTgwMDYyMjcwODE@._UX402_CR0,0,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY0ODEwNTcwOV5BMl5BanBnXkFtZTgwMDYyMjcwODE@._UX402_CR0,0,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2416509422&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_i_2" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTU4NDQ0NDYxN15BMl5BanBnXkFtZTgwMTcyMjcwODE@._UX1100_CR610,100,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU4NDQ0NDYxN15BMl5BanBnXkFtZTgwMTcyMjcwODE@._UX1100_CR610,100,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2416509422&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_i_3" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTcyMzUxODM5N15BMl5BanBnXkFtZTgwMTA0Mjc5NzE@._UX550_CR90,50,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTcyMzUxODM5N15BMl5BanBnXkFtZTgwMTA0Mjc5NzE@._UX550_CR90,50,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2416509422&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_i_4" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTk0MTk2MzUwMF5BMl5BanBnXkFtZTgwNzY5MTk4NzE@._SX450_CR30,40,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk0MTk2MzUwMF5BMl5BanBnXkFtZTgwNzY5MTk4NzE@._SX450_CR30,40,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Did you miss one of our one-on-one interviews? Check out our archived interviews with your favorite stars.</p> <p class="seemore"><a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2416509422&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_sm" class="position_bottom supplemental" >Watch all our one-on-one interviews</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    
            </div>
            <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-3292747090._CB299577773_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,300x600,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=485480988022;ord=485480988022?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=485480988022?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=485480988022?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	

    
    
    

    
    
        <a name="slot_right-2"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425692502&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_hd" > <h3>Opening This Week</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2948356"></div> <div class="title"> <a href="/title/tt2948356?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425692502&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t0"> Zootopia </a> <span class="secondary-text"></span> </div> <div class="action"> <a href="/showtimes/title/tt2948356?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425692502&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3300542"></div> <div class="title"> <a href="/title/tt3300542?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425692502&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t1"> London Has Fallen </a> <span class="secondary-text"></span> </div> <div class="action"> <a href="/showtimes/title/tt3300542?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425692502&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3553442"></div> <div class="title"> <a href="/title/tt3553442?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425692502&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t2"> Whiskey Tango Foxtrot </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3702652"></div> <div class="title"> <a href="/title/tt3702652?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425692502&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t3"> The Other Side of the Door </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2101383"></div> <div class="title"> <a href="/title/tt2101383?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425692502&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t4"> Knight of Cups </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3147312"></div> <div class="title"> <a href="/title/tt3147312?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425692502&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t5"> Desierto </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3616916"></div> <div class="title"> <a href="/title/tt3616916?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425692502&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t6"> The Wave </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4503598"></div> <div class="title"> <a href="/title/tt4503598?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425692502&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t7"> Emelie </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425692502&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_sm" class="position_bottom supplemental" >See more opening this week</a></p>
                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','WatchableTitlesListWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_right-3"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="showtimesMessagingWidget"> <p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2425695262&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Tickets & Showtimes &raquo;</a></p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> </div> </div> </span>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	

    
    
        <a name="slot_right-4"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="showtimesMessagingWidget"> <p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2006955922&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> </div> </div> </span>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-6"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','BoxOfficeListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/chart/boxoffice?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_hd" > <h3>Now Playing (Box Office)</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1431045"></div> <div class="title"> <a href="/title/tt1431045?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t0"> Deadpool </a> <span class="secondary-text">$31.1M</span> </div> <div class="action"> <a href="/showtimes/title/tt1431045?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2404233"></div> <div class="title"> <a href="/title/tt2404233?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t1"> Gods of Egypt </a> <span class="secondary-text">$14.1M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2267968"></div> <div class="title"> <a href="/title/tt2267968?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t2"> Kung Fu Panda 3 </a> <span class="secondary-text">$8.9M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3231054"></div> <div class="title"> <a href="/title/tt3231054?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t3"> Risen </a> <span class="secondary-text">$6.8M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1712261"></div> <div class="title"> <a href="/title/tt1712261?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t4"> Triple 9 </a> <span class="secondary-text">$6.1M</span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/chart/boxoffice?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_sm" class="position_bottom supplemental" >See more box office results</a></p>
                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','BoxOfficeListWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-8"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_hd" > <h3>Coming Soon</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1179933"></div> <div class="title"> <a href="/title/tt1179933?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t0"> 10 Cloverfield Lane </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1002563"></div> <div class="title"> <a href="/title/tt1002563?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t1"> The Young Messiah </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4871980"></div> <div class="title"> <a href="/title/tt4871980?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t2"> The Perfect Match </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3381008"></div> <div class="title"> <a href="/title/tt3381008?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t3"> The Brothers Grimsby </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3277624"></div> <div class="title"> <a href="/title/tt3277624?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t4"> Creative Control </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_sm" class="position_bottom supplemental" >See more coming soon</a></p>
                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','WatchableTitlesListWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-12"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','TwitterWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_twitter">
<span class="widget_header"> <span class="oneline"> <h3>Follow Us On Twitter</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src="http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3626940295._CB298540565_.html?config=%7B%22width%22%3A%22300px%22%2C%22height%22%3A%22500px%22%2C%22href%22%3A%22https%3A%2F%2Ftwitter.com%2FIMDb%22%2C%22heading%22%3A%22Follow%20Us%20On%20Twitter%22%2C%22screen-name%22%3A%22IMDb%22%2C%22list-name%22%3Anull%2C%22widget-id%22%3A%22354387022028357633%22%7D"></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','TwitterWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-14"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','FacebookWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_facebook">
<span class="widget_header"> <span class="oneline"> <h3>Find Us On Facebook</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','FacebookWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-30"></a>
        <div class="aux-content-widget-2 sticky-widget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/youtube-originals/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418140142&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_yto_rhs_promo_hd" > <h3>YouTube Red Originals on IMDb</h3> </a> </span> </span> <p class="blurb">Learn more about the new YouTube Red Original series and movies brought to you by top YouTube creators.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/youtube-originals/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418140142&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_yto_rhs_promo_i_1" > <img itemprop="image" class="pri_image" title="Lazer Team (2015)" alt="Lazer Team (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTQ3NDM4NDMxNl5BMl5BanBnXkFtZTgwMTY5OTAwODE@._V1_SY525_CR12,0,700,525_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ3NDM4NDMxNl5BMl5BanBnXkFtZTgwMTY5OTAwODE@._V1_SY525_CR12,0,700,525_AL_UY1050_UX1400_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/youtube-originals/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418140142&pf_rd_r=1WESE1RQ98S4756V8A37&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_yto_rhs_promo_sm" class="position_bottom supplemental" >Visit the YouTube Red Originals section</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    
            </div>
            <br class="clear" />
        </div>
        

    
    
    

    
    
    
        
    
        <br class="clear" />
    </div>
</div>
                





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYvCIu9DacDVymDpBgkYG0i-k9Mxean9XQBoP3N24zliiX2IYnVudQv0FdwtSBt9ughgNi1aK42%0D%0AFDdnihzfiH7x3QcnJ27kbdoh77mC2B4rimGfn8Y364Mzv33WHbm3NL0EFXm20VJcUD3MpbrTz3Ia%0D%0AZeB94im044R6gM0QzW1uXNWJ67QMFQIDjW0UQjWXWkc8ijsDL-30Bo2aS3o15TYL1_s81SjNbvHS%0D%0ApqO-bukw1Ek%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYkxI5bNYNkpYvKhXq7W9FLPLDySkYf6X6juchHwCQN2YB6BKJ6FGDk3DioJlwEUyqyVFVfyykB%0D%0AhV4m_uNaOyTIk3St7kjxxPkT73ErnotMJBZdetQaaIdiocEoXBgktXPTVA8GqwKnmbgMnIXGSlci%0D%0AmhuYuXkUf8mUIVIIDOkGMJyhUtK8CvmE_1FeDajkgJoOzTxXVQ-O8HESkkoAHMXIrA%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYjYg9HB7gGFYjL5S9N0y1qKkNQaZqoooDRjeOXJt4fK3gwgIpFoN1ceBIM2e-P2iMw4nGb6ZUK%0D%0AS0KkqlH1LNoaCgnYOmc0S_rXSbSTLjbH9bWECQON_QBzr6zbVdQfzMOtLh7tVX5tl4GqKzlmQzlM%0D%0AZbxipt4u-p5VX4Qfou12ijmO-CRsM01zVUDzqAbXsZOrxZ1U7va1VEHab_xhya71Ci_KAx_TDPMC%0D%0Auef3Px2gHrwT0JYK3_YoH5Ai5Rdcl7LIZuYpaeVWyHBMrmQVs0tqIQNrq0HE5eFbHfq1P5To5-Xi%0D%0A3iRPpVzHQYXQ-IrxNA1jykRdeZcXtx9dd-UGwOhw8RTYH6t6VVvLWnZ4ud6-OCzQO9NK6CMyYKBZ%0D%0AtHxE_4E_EYUwjQCqyemkP7pKIQoWvKqy1birjUujf6RwPOXfN6c%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYkMegT1MexY1lNDagFTgwXD4BssZ5-e_jIU7Q_nDjLItDxc24vDlFyXNLGujfpv2rJS9PDF_JF%0D%0AecPdAUJ5dYpRnoPOrTranuI23vH5ySzgi989WrmCX2Vl5vd5I1rNLbo_VVMLNnbEfzOpAFlhOqT-%0D%0AypuPFEDG3w5C0cGgfkqt7XD4ukI858FUCkGIcQjBMMHvQ10G0k8zh1VkJt7OOydH5w%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYtULpFRDUJbYjbuKih27NmP4i1DV8gWAuEPR3_qD8AtMKpMvwk7OqYNOuG98BhWTa6lhJnzcon%0D%0Ax18K5YsewA52kxssBbZ47SqA77ZmxUmAj5riqlE3xgVFuQStZjIbURtZVpVx-UXrQyBAoOacrgSE%0D%0AfxKkz3yf-_enbiQDiTLH3y5YzTeWm1-CD-OvA21E13wQ%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYmioO97mFnD8xaO6kxuLcMjtI0nEZiBbbARJ7UAdSuR79_CM7dw6x5tdkAj9Q6EU1Slsqoa1ui%0D%0As-wG4WRAxLgXeIVr_jLHauQV2p8yJ0zxONKdB_-mAg-RobBxscipFLkmirRUidYjtalcOc3_arN_%0D%0A0dRaE_clCBIzr8upJrJEaXRG5i8okfzfolXHVhKyegKN%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top Rated Movies</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Box Office</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li><a href="/advertising/?ref_=ft_ad"
>Advertising</a></li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYhDofq5O9daD4EqZ6rZOHz296Nf5ia4Djz3mMhGuHe3wPDO9_gommxTmx-IAfj9nx87_W8tlM6%0D%0AVuljMPySzn53F8zZNoQiKMxe6S7QJ-nrESb1WE3DYzaZn8xWTVsUJUNKSEWYbbzGUaqQUypsavwN%0D%0A0R5QFvnplJYQkI_rnSFfsWsCcq8ZgV1CNnSKuFmmi2fg5aGWAp2ZNZaC7AOA52BVLg%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYqnOEZXh5oHcqaLj-ayKAuwX-G9uDSRXe4Nqdl-jmhyL8oexLUG9zYu_cCB1gPZT-mg8j5Yhyf%0D%0ArGMuDoXkLmBU9LuHMliELaoSuuMyb5WY-nJGmBYHeVyf_QBNm2mQpxmF4pRdGKRlG-3j4-yEfpGO%0D%0A6DSe7dde09iaKgxYtLYaBbPz0gu01ciuUtFgRiE4vEWi%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYtb1i9pOJS4nblPurb_OLt79UU5MGNwt-gUCqKfUygD0C_ZbNyule8yD0lJqEcQxe8DgOJZVBe%0D%0A8QPGd1Wb4qOs0yVndKRWGx8c7zoJdIGRL_gPFegtudOseB41LxatR4yCINeLMegMQ_mmcNyE4IAM%0D%0AczDVSCNpg4hb0qGlK3tr288cgQvoFudOWVL0iqKPL-03qg3JkY-j1HYMggRI4gLX2gL2MPTp0Ep5%0D%0APF04eXiOfSuhoX4zwEbrRIywPIXG3kdV%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2016
                <a
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYiSOcLOVPYaoPwU0ISPyf0OBFBekJWRxixSLxysU9eSTCB7tX9wUi0vWrJTNegksHipZ8IkwTZ%0D%0AD-3JXWwzsYDf8zq86gjML6CtU1svZ-Q5s40s_2c0hYJmKxXkDqmlooSkj23B1yZib3WrgNAR9amh%0D%0AaRn7Vy31DiS53i0bzZQR3bxp0F6z5M9hhe7sq89NYlC4RWvq8JkjKX8qRhVXoEV2gvkuyITqVJY7%0D%0ApHFt7sgrfKQ%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYouAadtajQHTgmvROdIzuVemo7WZTJQTRTmk8h0PvtEdy-imiG4jk5gkIy82n1ssnKGhMiyGNg%0D%0Axt2SDuQQy5QSBpzDN1OgvtlGMLcD21MHMPsUZ5Q_sRTbxj_WgmiB-uPiY0kaEigoXoLMe4VVHhmV%0D%0AVbTewEQ63tIejUB_QbSlcASJdJr3XUiPbHo98Cy3y7PQNtIfpMIU1x1PzL04KMjznS9zQZjZil0y%0D%0Aj8o5B3DxbbM%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYjML12BPfhFJrhdj6Vq6VyI4bc1qUs0seOla6S6DpwAzdub6Mg4J7B8lHwnvQKn9OS4NnBneWy%0D%0A9tSO_RpzXmubXCBa0Ey8D1JuaWAtNbHd3cqCvgyyvxf8JQgJo-4oo_siDRGtMO0ZYEqg6uPQAZg8%0D%0AhmmuoAP6_b6i5p1U8YU2lZC9o0JGYnzS-8JAXKnp48LH3Bnk96yy_izjhFCDP31b6ufVkfwrsEBT%0D%0AZuqx_jjcP-k%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYpgJ9M-6NDYEgT0jy8aNxCYS1HF_3y2K3EnZq91aLGjnaOdtkBVhxzyLxQfp8lh5HMl3Lt2fNz%0D%0AQVM9HiBiDn_C4q6uqrDGfXE7EDrPOANoErTVJwzXqlX2SCu51cEclDzP6Ip2D1YQpu_QcW7-u2SB%0D%0Ad3KlYvUatFV-jJm3Ew9kj1mzlxpHsky4fJQv9aiNaQpTfV_cTL5I9fzI8cCixcT7I7P7AfzMJ2E9%0D%0AaZMxt2-ryC8%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYlmOI-EvITULE9mV5fJpwJavl0Wn5nfeheeiEaZkeWzhV7K4MnA_DHaR-sS_7kcGLPEbgD7sAO%0D%0A76InM85BFM8s93dunePulGw1W6sedYI3PkvYim4AYCtXk9tTPU5xQlRr_gLtJiJDO0SD1l0ir5e2%0D%0A3WSA7St6kfS3qC4ks4DrCu8OxlKTu4cIp2YTaRRGb0oVUG_PQro8f7qhj_ekLyBObcFbt6esKla2%0D%0AnB3YT4j_uZ8%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYhnGEfahjFbEk_SprENmNQeM3vPd2SoPBPysgwKhnu0Gy5om7E_qrgxc26K28VIMOgk9jKgiMw%0D%0ApkQC0GmNLlraByorMntnWZoS5tEVMznsLK9TxZNFjizEOHzC6-TTkdVGkXvmerVpJ6XddaP9Odsx%0D%0AtxpnGEO17IrpqqggrAP8BdzZjN27q-9vhP4C5SSYJUIb65T2uba8-5CiYKiw9AC2cTd_nF-lIBDE%0D%0ACYW8fNmj3Sg5BkAgv1ZMwnVWzN779LoJ%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYnwP2uuekQuHAphJVqjqVfp6ClleDoGkxan8g6tdEDEydxwAtg4U6JTbEmyAuYgfmxFIPo0JzU%0D%0A_7R-RCHEMi1ljSgFlEal32s3kMtvz_LfxXjvnzz5CdUOLefVwAfcYk7aR_py1vndMc__4v01zy1S%0D%0AOEeg9VLxh0vPPLR6l63ooaE%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYhgF15ZVjXO57f62mu6yNUlGOaP6lJAR1S2MPn-DCZfxvZtPu_KOpGBBlA-wbHmCvttbW0l8iI%0D%0A5_JOlgKeL9WSMG2_8OISrVqZdDzRVQiS6OUWAZxOlacSJLQLbybvxr6F1Im6zEwqVcQ4gejclVF8%0D%0Aq6BX0CUtSixJj7vq2A-huCI%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/common-774728307._CB299450936_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-816966180._CB298601395_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-335260898._CB289148898_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-194820129._CB286493173_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-2126106747._CB289323193_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=0101124709ee3161620906a1bb34414dd4d758bd25cb047dea874981e9ef6eaa4430",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=485480988022"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-1675743762._CB299577842_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=485480988022&ord=485480988022";
    },"unable to request AAN pixel");
</script>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="678"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
