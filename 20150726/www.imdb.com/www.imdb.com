



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>

                    
                    
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "937-6623932-2320329";
                var ue_id = "03NRF6PNMK2X8ATY8FC8";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        
    
        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />
    
    <meta property='fb:app_id' content='115109575169727' />
    
    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="03NRF6PNMK2X8ATY8FC8" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1a-c3-2xl-i-eebe6c10.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-1572047408._CB316444558_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-485555676._CB318510215_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-247421025._CB318510254_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-3978168775._CB318510288_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-1541130848._CB318510255_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
(function(){var d=function(o){return Object.prototype.toString.call(o)==="[object Array]";},g=function(q,p){var o;for(o=0;o<q.length;o++){if(o in q){p.call(null,q[o],o);}}},h=[],k,b,l=false,n=false,f=function(){var o=[],p=[],q={};g(h,function(s){var r="";g(s.dartsite.split("/"),function(t){if(t!==""){if(t in q){}else{q[t]=o.length;o.push(t);}r+="/"+q[t];}});p.push(r);});return{iu_parts:o,enc_prev_ius:p};},c=function(){var o=[];g(h,function(q){var p=[];g(q.sizes,function(r){p.push(r.join("x"));});o.push(p.join("|"));});return o;},m=function(){var o=[];g(h,function(p){o.push(a(p.targeting));});return o.join("|");},a=function(r,o){var q,p,s=[];for(q in r){p=[];for(j=0;j<r[q].length;j++){p.push(encodeURIComponent(r[q][j]));}if(o){s.push(q+"="+encodeURIComponent(p.join(",")));}else{s.push(q+"="+p.join(","));}}return s.join("&");},e=function(){var o=+new Date();if(n){return;}if(!this.readyState||"loaded"===this.readyState){n=true;if(l){imdbads.cmd.push(function(){for(i=0;i<h.length;i++){generic.monitoring.record_metric(h[i].name+".fail",csm.duration(o));}});}}};window.tinygpt={define_slot:function(r,q,o,p){h.push({dartsite:r.replace(/\/$/,""),sizes:q,name:o,targeting:p});},set_targeting:function(o){k=o;},callback:function(q){var r,p={},t,o,s=+new Date();l=false;for(r=0;r<h.length;r++){t=h[r].dartsite;o=h[r].name;if(q[r][t]){p[o]=q[r][t];}else{window.console&&console.error&&console.error("Unable to correlate GPT response for "+o);}}imdbads.cmd.push(function(){for(r=0;r<h.length;r++){ad_utils.slot_events.trigger(h[r].name,"request",{timestamp:b});ad_utils.slot_events.trigger(h[r].name,"tagdeliver",{timestamp:s});}ad_utils.gpt.handle_response(p);});},send:function(){var r=[],q=function(s,t){if(d(t)){t=t.join(",");}if(t){r.push(s+"="+encodeURIComponent(""+t));}},o,p;if(h.length===0){tinygpt.callback({});return;}q("gdfp_req","1");q("correlator",Math.floor(4503599627370496*Math.random()));q("output","json_html");q("callback","tinygpt.callback");q("impl","fifs");q("json_a","1");result=f();q("iu_parts",result.iu_parts);q("enc_prev_ius",result.enc_prev_ius);q("prev_iu_szs",c());q("prev_scp",m());q("cust_params",a(k,true));o=document.createElement("script");p=document.getElementsByTagName("script")[0];o.async=true;o.type="text/javascript";o.src="http://pubads.g.doubleclick.net/gampad/ads?"+r.join("&");o.id="tinygpt";o.onload=o.onerror=o.onreadystatechange=e;l=true;p.parentNode.insertBefore(o,p);b=+new Date();}};})();</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['a'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['2'],
'u': ['284832669079'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-3196534337._CB302695131_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"7a323416176705ac19e0a29e0ddde7ea2f3c3155",
"2015-07-25T23%3A57%3A21GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 25359;
generic.days_to_midnight = 0.2935069501399994;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-1582251138._CB317085808_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'a']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm === undefined || window.generic === undefined || window.consoleLog === undefined)) {
                    if (console !== undefined && console.log !== undefined) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-1949688977._CB306861155_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-3293651389._CB317087773_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=a;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=284832669079;ord=284832669079?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=a;bpx=2;c=0;s=3075;s=32;ord=284832669079?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=a;bpx=2;c=0;s=3075;s=32;ord=284832669079?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
onsubmit="(new Image()).src='/rg/SEARCH-BOX/HEADER/images/b.gif?link=/find';"
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/imdbpicks/monthly-tv-picks/?ref_=nv_tp_tvpicks_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/search/title?count=100&title_type=feature,tv_series,tv_movie&explore=title_type,genres,year,countries&ref_=nv_ch_mm_1"
>Popular Movies & TV</a></li>
                        <li><a href="/chart/?ref_=nv_ch_cht_2"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_3"
>Oscar Winners</a></li>
                        <li><a href="/chart/top?ref_=nv_ch_250_4"
>Top 250</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_5"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/imdbpicks/monthly-tv-picks/?ref_=nv_tvv_picks_1"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_2"
>On Tonight</a></li>
                        <li><a href="/list/ls074093523/?ref_=nv_tvv_wn_3"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_4"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/academymuseum/?ref_=nv_sf_am_1"
>Academy Museum</a></li>
                        <li><a href="/imdbpicks/?ref_=nv_sf_pks_2"
>IMDb Picks</a></li>
                        <li><a href="/best-of/?ref_=nv_sf_bo_3"
>Best of 2014</a></li>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_4"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/whattowatch/?ref_=nv_sf_wtw_5"
>What to Watch</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=07-25&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_2"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_3"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_4"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_5"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_6"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_7"
>Road to the Emmys</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_8"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_9"
>Toronto Film Festival</a></li>
                        <li><a href="/awards-central/?ref_=nv_ev_all_10"
>More Popular Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni58832383/?ref_=nv_nw_tn_1"
> Box Office: âPixelsâ Wins Friday, On Track for $25 Million Weekend
</a><br />
                        <span class="time">9 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni58831690/?ref_=nv_nw_tn_2"
> Sandra Bullockâs âCrisisâ Gets Awards Season Release Date
</a><br />
                        <span class="time">22 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni58831658/?ref_=nv_nw_tn_3"
> âVeepâ Stars Matt Walsh, Timothy Simons Developing Football Comedy
</a><br />
                        <span class="time">22 hours ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/games/guess?ref_=nv_cm_qz_3"
>Quiz Game</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_4"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5> 
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0083987/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMjIyMDU3NzE0OF5BMl5BanBnXkFtZTcwOTU1NDcwNw@@._V1._SY315_CR0,0,410,315_.jpg",
            titleYears : "1982",
            rank : 196,
                    headline : "Gandhi"
    },
    nameAd : {
            clickThru : "/name/nm0002071/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMjA5NzE3OTE0OF5BMl5BanBnXkFtZTcwNTg4MjQ1Nw@@._V1._SY600_CR80,0,250,315_CT10_.jpg",
            rank : 200,
            headline : "Will Ferrell"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYrE-dz6_WoFQRg4Ku1i4roGsN69_DrSqNbBtDkz2bEe9Fnn75-D2LyyefqFTJ7qUFyPKc_9cDd%0D%0Asz3Cuc0nG3RcDNUPYQZtXKSzq50bR-6jdR3ETOwpf27bAEdskx09jzOXZvR1b81tX8BzSAU-GTz6%0D%0ALwInytuIqA4o2o5DlJQ82fmFneP_scgERX1LtKGqOUtB2icy7sKY33ypA1FYusP46Q%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYgTyFjqZFXYD1AS5Ulfwin8IiT1KitIDtz6FFqBcHKbN0hXNr5GnBwyFVu33Qz1gzxsZaVFi9v%0D%0AHb2EfnBJMCvBFEtg0yDaMPLtgfeAYSQLKETm43IgJ_y9kM5Ce3DK0ZInqtfuGUhPYJnFGE_4OEOO%0D%0AvnESEFVLaHmnVdWjNZtLO3bSgXLiFtSF4Pd1H4933E6f%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYq40AWKMa1Y135PHOmAPc5Vuz9kv0ON4rfiLM83VQRXORoFrFD7y8aC9sAQPrsiyALW4sRVt-v%0D%0Angv0Gd-VPtdeijtBbZbuoB0p55fQmH1xHWb-m9y29aXYvoMEm3GYzzgxSMmxj2uSmAIPitUb4_lJ%0D%0AfTQEq0BG2s1dBVkHb3faKFqsywvwcqGDnkEfvona8ZGU_VGcuogbsTmiE3ladnl4kg%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=284832669079;ord=284832669079?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>
                    <div id="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=284832669079;ord=284832669079?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	

                    
                    

<div id="content-2-wide">
    <div id="main">
                    
        <div class="heroWidget">
        <span class="ab_widget"
        >
            <div class="ab_hero">
<div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi4254905113?ref_=hm_hp_i_1" data-video="vi4254905113" data-source="bylist" data-id="ls002309697" data-rid="03NRF6PNMK2X8ATY8FC8" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="An aspiring 23-year-old DJ named Cole spends his days scheming with his childhood friends and his nights working on the one track that will set the world on fire. All of this changes when he meets a charismatic but damaged older DJ named James, who takes him under his wing. Things get complicated, however, when Cole starts falling for Jamesï¿½ï¿½ï¿½ much younger girlfriend, Sophie." alt="An aspiring 23-year-old DJ named Cole spends his days scheming with his childhood friends and his nights working on the one track that will set the world on fire. All of this changes when he meets a charismatic but damaged older DJ named James, who takes him under his wing. Things get complicated, however, when Cole starts falling for Jamesï¿½ï¿½ï¿½ much younger girlfriend, Sophie." src="http://ia.media-imdb.com/images/M/MV5BMTAwMzU1OTE4MzdeQTJeQWpwZ15BbWU4MDIyMDQ1MTYx._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTAwMzU1OTE4MzdeQTJeQWpwZ15BbWU4MDIyMDQ1MTYx._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="An aspiring 23-year-old DJ named Cole spends his days scheming with his childhood friends and his nights working on the one track that will set the world on fire. All of this changes when he meets a charismatic but damaged older DJ named James, who takes him under his wing. Things get complicated, however, when Cole starts falling for Jamesï¿½ï¿½ï¿½ much younger girlfriend, Sophie." title="An aspiring 23-year-old DJ named Cole spends his days scheming with his childhood friends and his nights working on the one track that will set the world on fire. All of this changes when he meets a charismatic but damaged older DJ named James, who takes him under his wing. Things get complicated, however, when Cole starts falling for Jamesï¿½ï¿½ï¿½ much younger girlfriend, Sophie." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="An aspiring 23-year-old DJ named Cole spends his days scheming with his childhood friends and his nights working on the one track that will set the world on fire. All of this changes when he meets a charismatic but damaged older DJ named James, who takes him under his wing. Things get complicated, however, when Cole starts falling for Jamesï¿½ï¿½ï¿½ much younger girlfriend, Sophie." title="An aspiring 23-year-old DJ named Cole spends his days scheming with his childhood friends and his nights working on the one track that will set the world on fire. All of this changes when he meets a charismatic but damaged older DJ named James, who takes him under his wing. Things get complicated, however, when Cole starts falling for Jamesï¿½ï¿½ï¿½ much younger girlfriend, Sophie." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt3787590/?ref_=hm_hp_cap_pri_1" > We Are Your Friends </a> </div> </div> <div class="secondary ellipsis"> Trailer #2 </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3415913241?ref_=hm_hp_i_2" data-video="vi3415913241" data-source="bylist" data-id="ls002922459" data-rid="03NRF6PNMK2X8ATY8FC8" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="A decorated New Jersey police detective, Laurel is diagnosed with cancer and wants to leave her hard earned pension to her domestic partner, Stacie. However the county officials, Freeholders, conspire to prevent Laurel from doing this. Hard-nosed detective Dane Wells, and activist Steven Goldstein, unite in Laurel and Stacieï¿½ï¿½ï¿½s defense, rallying police officers and ordinary citizens to support their struggle for equality." alt="A decorated New Jersey police detective, Laurel is diagnosed with cancer and wants to leave her hard earned pension to her domestic partner, Stacie. However the county officials, Freeholders, conspire to prevent Laurel from doing this. Hard-nosed detective Dane Wells, and activist Steven Goldstein, unite in Laurel and Stacieï¿½ï¿½ï¿½s defense, rallying police officers and ordinary citizens to support their struggle for equality." src="http://ia.media-imdb.com/images/M/MV5BNzI3NTYyNTAzNl5BMl5BanBnXkFtZTgwMjQ4MDUzNjE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNzI3NTYyNTAzNl5BMl5BanBnXkFtZTgwMjQ4MDUzNjE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="A decorated New Jersey police detective, Laurel is diagnosed with cancer and wants to leave her hard earned pension to her domestic partner, Stacie. However the county officials, Freeholders, conspire to prevent Laurel from doing this. Hard-nosed detective Dane Wells, and activist Steven Goldstein, unite in Laurel and Stacieï¿½ï¿½ï¿½s defense, rallying police officers and ordinary citizens to support their struggle for equality." title="A decorated New Jersey police detective, Laurel is diagnosed with cancer and wants to leave her hard earned pension to her domestic partner, Stacie. However the county officials, Freeholders, conspire to prevent Laurel from doing this. Hard-nosed detective Dane Wells, and activist Steven Goldstein, unite in Laurel and Stacieï¿½ï¿½ï¿½s defense, rallying police officers and ordinary citizens to support their struggle for equality." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="A decorated New Jersey police detective, Laurel is diagnosed with cancer and wants to leave her hard earned pension to her domestic partner, Stacie. However the county officials, Freeholders, conspire to prevent Laurel from doing this. Hard-nosed detective Dane Wells, and activist Steven Goldstein, unite in Laurel and Stacieï¿½ï¿½ï¿½s defense, rallying police officers and ordinary citizens to support their struggle for equality." title="A decorated New Jersey police detective, Laurel is diagnosed with cancer and wants to leave her hard earned pension to her domestic partner, Stacie. However the county officials, Freeholders, conspire to prevent Laurel from doing this. Hard-nosed detective Dane Wells, and activist Steven Goldstein, unite in Laurel and Stacieï¿½ï¿½ï¿½s defense, rallying police officers and ordinary citizens to support their struggle for equality." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt1658801/?ref_=hm_hp_cap_pri_2" > Freeheld </a> </div> </div> <div class="secondary ellipsis"> Official Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi547074841?ref_=hm_hp_i_3" data-video="vi547074841" data-source="bylist" data-id="ls002653141" data-rid="03NRF6PNMK2X8ATY8FC8" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="In this next chapter of the Maze Runner saga, Thomas and his fellow Gladers face their greatest challenge yet: searching for clues about the mysterious and powerful organization known as WCKD. Their journey takes them to the Scorch, a desolate landscape filled with unimaginable obstacles. Teaming up with resistance fighters, the Gladers take on WCKD's vastly superior forces and uncover its shocking plans for them all." alt="In this next chapter of the Maze Runner saga, Thomas and his fellow Gladers face their greatest challenge yet: searching for clues about the mysterious and powerful organization known as WCKD. Their journey takes them to the Scorch, a desolate landscape filled with unimaginable obstacles. Teaming up with resistance fighters, the Gladers take on WCKD's vastly superior forces and uncover its shocking plans for them all." src="http://ia.media-imdb.com/images/M/MV5BMjE3MDU2NzQyMl5BMl5BanBnXkFtZTgwMzQxMDQ3NTE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE3MDU2NzQyMl5BMl5BanBnXkFtZTgwMzQxMDQ3NTE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="In this next chapter of the Maze Runner saga, Thomas and his fellow Gladers face their greatest challenge yet: searching for clues about the mysterious and powerful organization known as WCKD. Their journey takes them to the Scorch, a desolate landscape filled with unimaginable obstacles. Teaming up with resistance fighters, the Gladers take on WCKD's vastly superior forces and uncover its shocking plans for them all." title="In this next chapter of the Maze Runner saga, Thomas and his fellow Gladers face their greatest challenge yet: searching for clues about the mysterious and powerful organization known as WCKD. Their journey takes them to the Scorch, a desolate landscape filled with unimaginable obstacles. Teaming up with resistance fighters, the Gladers take on WCKD's vastly superior forces and uncover its shocking plans for them all." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="In this next chapter of the Maze Runner saga, Thomas and his fellow Gladers face their greatest challenge yet: searching for clues about the mysterious and powerful organization known as WCKD. Their journey takes them to the Scorch, a desolate landscape filled with unimaginable obstacles. Teaming up with resistance fighters, the Gladers take on WCKD's vastly superior forces and uncover its shocking plans for them all." title="In this next chapter of the Maze Runner saga, Thomas and his fellow Gladers face their greatest challenge yet: searching for clues about the mysterious and powerful organization known as WCKD. Their journey takes them to the Scorch, a desolate landscape filled with unimaginable obstacles. Teaming up with resistance fighters, the Gladers take on WCKD's vastly superior forces and uncover its shocking plans for them all." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt4046784/?ref_=hm_hp_cap_pri_3" > Maze Runner: Scorch Trials </a> </div> </div> <div class="secondary ellipsis"> Trailer #2 </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/trailers?ref_=hm_hp_sm" class="position_bottom supplemental" > Browse more trailers </a> </p>    </div>

        </span>
        </div>
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/gallery/rg1875548928?ref_=hm_ph_ex_ma_hd" > <h3>Exclusive Photos: <i>Mistress America</i></h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm4136560640/rg1875548928?ref_=hm_ph_ex_ma_i_1" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTA5Mzc0NTkyNzJeQTJeQWpwZ15BbWU4MDE4NTc1MzYx._V1._CR642,0,1365,1365_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTA5Mzc0NTkyNzJeQTJeQWpwZ15BbWU4MDE4NTc1MzYx._V1._CR642,0,1365,1365_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3901679616/rg1875548928?ref_=hm_ph_ex_ma_i_2" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjE0ODc1NzA5OV5BMl5BanBnXkFtZTgwOTc1NzUzNjE@._V1._CR342,0,1365,1365_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE0ODc1NzA5OV5BMl5BanBnXkFtZTgwOTc1NzUzNjE@._V1._CR342,0,1365,1365_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm4103006208/rg1875548928?ref_=hm_ph_ex_ma_i_3" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTU5Njk0MDU3MF5BMl5BanBnXkFtZTgwNTg1NzUzNjE@._V1._CR520,20,980,980_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU5Njk0MDU3MF5BMl5BanBnXkFtZTgwNTg1NzUzNjE@._V1._CR520,20,980,980_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb"><a href="/name/nm0000876/?ref_=hm_ph_ex_ma_lk1">Noah Baumbach</a> and his girlfriend and muse <a href="/name/nm1950086/?ref_=hm_ph_ex_ma_lk2">Greta Gerwig</a> re-team for <a href="/title/tt2872462/?ref_=hm_ph_ex_ma_lk3"><i>Mistress America</i></a>, a comedy about a college freshman in New York who is taken in by her adventurous, soon-to-be stepsister.</p> <p class="seemore"> <a href="/gallery/rg1875548928?ref_=hm_ph_ex_ma_sm" class="position_bottom supplemental" > See full photo gallery </a> </p>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58832383?ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTYxMzM4NDY5N15BMl5BanBnXkFtZTgwNzg1NTI3MzE@._V1_SY150_CR3,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58832383?ref_=hm_nw_tp1"
class="headlines" >Box Office: âPixelsâ Wins Friday, On Track for $25 Million Weekend</a>
    <div class="infobar">
            <span class="text-muted">9 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?ref_=hm_nw_tp1_src"
>Variety - Film News</a></span>
    </div>
                                </div>
<p><a href="/name/nm0001191?ref_=hm_nw_tp1_lk1">Adam Sandler</a>âs nostalgic video game comedy â<a href="/title/tt2120120?ref_=hm_nw_tp1_lk2">Pixels</a>â outdidÂ teenage romance pic â<a href="/title/tt3622592?ref_=hm_nw_tp1_lk3">Paper Towns</a>â at the FridayÂ box office, but both newcomers have toÂ deal with holdover hits âAnt-ManâÂ and âMinions.âSonyâs â<a href="/title/tt2120120?ref_=hm_nw_tp1_lk4">Pixels</a>âÂ generated $9 million in Friday showings, on track for $25 million on the weekend. Studio ...                                        <span class="nobr"><a href="/news/ni58832383?ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58831690?ref_=hm_nw_tp2"
class="headlines" >Sandra Bullockâs âCrisisâ Gets Awards Season Release Date</a>
    <div class="infobar">
            <span class="text-muted">22 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?ref_=hm_nw_tp2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58831658?ref_=hm_nw_tp3"
class="headlines" >âVeepâ Stars Matt Walsh, Timothy Simons Developing Football Comedy</a>
    <div class="infobar">
            <span class="text-muted">22 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?ref_=hm_nw_tp3_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58832217?ref_=hm_nw_tp4"
class="headlines" >BioShock creator Ken Levine is no longer writing Logan's Run</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000023?ref_=hm_nw_tp4_src"
>Digital Spy</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58831435?ref_=hm_nw_tp5"
class="headlines" >Michael B. Jordan Shows âJust Mercy'; Greg Kinnear, Jennifer Ehle Join Untitled Ira Sachs Film</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?ref_=hm_nw_tp5_src"
>Deadline</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58831690?ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTI5NDY5NjU3NF5BMl5BanBnXkFtZTcwMzQ0MTMyMw@@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58831690?ref_=hm_nw_mv1"
class="headlines" >Sandra Bullockâs âCrisisâ Gets Awards Season Release Date</a>
    <div class="infobar">
            <span class="text-muted">22 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?ref_=hm_nw_mv1_src"
>Variety - Film News</a></span>
    </div>
                                </div>
<p> <a href="/company/co0026840?ref_=hm_nw_mv1_lk1">Warner Bros.</a> has given an awards-season release date of Oct. 30 to political drama â<a href="/title/tt1018765?ref_=hm_nw_mv1_lk2">Our Brand Is Crisis</a>,â starring <a href="/name/nm0000113?ref_=hm_nw_mv1_lk3">Sandra Bullock</a> and <a href="/name/nm0000671?ref_=hm_nw_mv1_lk4">Billy Bob Thornton</a>. <a href="/name/nm0337773?ref_=hm_nw_mv1_lk5">David Gordon Green</a> directed from a screenplay by <a href="/name/nm1661186?ref_=hm_nw_mv1_lk6">Peter Straughan</a> (â<a href="/title/tt1340800?ref_=hm_nw_mv1_lk7">Tinker Tailor Soldier Spy</a>â). The film is an adaptation of the 2005 ...                                        <span class="nobr"><a href="/news/ni58831690?ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58832383?ref_=hm_nw_mv2"
class="headlines" >Box Office: âPixelsâ Wins Friday, On Track for $25 Million Weekend</a>
    <div class="infobar">
            <span class="text-muted">9 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?ref_=hm_nw_mv2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58831658?ref_=hm_nw_mv3"
class="headlines" >âVeepâ Stars Matt Walsh, Timothy Simons Developing Football Comedy</a>
    <div class="infobar">
            <span class="text-muted">22 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?ref_=hm_nw_mv3_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58831970?ref_=hm_nw_mv4"
class="headlines" >10 Stories You Might Have Missed: Benicio del Toro, 'Star Wars' villain?</a>
    <div class="infobar">
            <span class="text-muted">17 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000173?ref_=hm_nw_mv4_src"
>Hitfix</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58832215?ref_=hm_nw_mv5"
class="headlines" >8 Easter eggs to look out for in Pixar's Inside Out, from Pizza Planet Truck to Harry Styles</a>
    <div class="infobar">
            <span class="text-muted">15 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000023?ref_=hm_nw_mv5_src"
>Digital Spy</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58831689?ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTQ2Njk2MzY2M15BMl5BanBnXkFtZTgwMDkxODg3MDE@._V1_SY150_CR6,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58831689?ref_=hm_nw_tv1"
class="headlines" >âCommunityâ Stars Want Another Season & Movie; Ken Jeong Teases Cast Reunion</a>
    <div class="infobar">
            <span class="text-muted">22 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?ref_=hm_nw_tv1_src"
>Variety - TV News</a></span>
    </div>
                                </div>
<p>â<a href="/title/tt1439629?ref_=hm_nw_tv1_lk1">Community</a>â was granted a sixth season, after being axed by NBC and resurrected by Yahoo, but could there be a seventh? If were up to <a href="/name/nm0421822?ref_=hm_nw_tv1_lk2">Ken Jeong</a>, season seven couldnât come quicker.âIf they had a seventh season, I would be begging to be on it,â Jeong toldÂ Variety, as he was leaving a day of work on ...                                        <span class="nobr"><a href="/news/ni58831689?ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58831615?ref_=hm_nw_tv2"
class="headlines" >Jay Z & Will Smith Teaming With Producer Aaron Kaplan for HBO Miniseries on Emmett Till</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?ref_=hm_nw_tv2_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58831287?ref_=hm_nw_tv3"
class="headlines" >NBC to Air Back-to-Back Episodes of âMr. Robinson,â âCarmichael Showâ</a>
    <div class="infobar">
            <span class="text-muted">25 July 2015 12:06 AM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?ref_=hm_nw_tv3_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58832133?ref_=hm_nw_tv4"
class="headlines" >âGeneral Hospitalâ Replaces Head Writer</a>
    <div class="infobar">
            <span class="text-muted">20 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004913?ref_=hm_nw_tv4_src"
>Deadline TV</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58832476?ref_=hm_nw_tv5"
class="headlines" >Performer of the Week: Dylan O'Brien</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?ref_=hm_nw_tv5_src"
>TVLine.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58832385?ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTczNzUxNDMwOF5BMl5BanBnXkFtZTYwNzY3ODI4._V1_SY150_CR8,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58832385?ref_=hm_nw_cel1"
class="headlines" >Happy 60th Birthday, Iman! Here Are 33 Photos That Prove the Supermodel Is Virtually Ageless</a>
    <div class="infobar">
            <span class="text-muted">9 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?ref_=hm_nw_cel1_src"
>E! Online</a></span>
    </div>
                                </div>
<p>Happy 60th birthday, <a href="/name/nm0408081?ref_=hm_nw_cel1_lk1">Iman</a>! One of the fashion world's most timeless beauties reached her diamond year todayâand we're celebrating her, of course. Imanâa Somali native who was discovered while attending university in Nairobi, Kenya at age 19âpaved the way for black models to be treated equally in ...                                        <span class="nobr"><a href="/news/ni58832385?ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58831740?ref_=hm_nw_cel2"
class="headlines" >Did Kim Kardashian Just Make It Possible for Us to Edit Tweets?</a>
    <div class="infobar">
            <span class="text-muted">21 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?ref_=hm_nw_cel2_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58832472?ref_=hm_nw_cel3"
class="headlines" >Scott Disick Breaks Silence</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000936?ref_=hm_nw_cel3_src"
>Access Hollywood</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58832473?ref_=hm_nw_cel4"
class="headlines" >Jake Gyllenhaal Cried When His Parents Forbade Him From Starring in The Mighty Ducks</a>
    <div class="infobar">
            <span class="text-muted">12 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0045136?ref_=hm_nw_cel4_src"
>Us Weekly</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58832983?ref_=hm_nw_cel5"
class="headlines" >Kelly Rutherford and Her 2 Kids Pose for Playful Pics at Charity Event as Custody Battle ContinuesâSee the Photos!</a>
    <div class="infobar">
            <span class="text-muted">56 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?ref_=hm_nw_cel5_src"
>E! Online</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <h3>IMDb Snapshot</h3> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm2223433728/rg1841994496?ref_=hm_snp_i_1" > <img itemprop="image" class="pri_image" title="The Man from U.N.C.L.E. (2015)" alt="The Man from U.N.C.L.E. (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjEwNDc5NTExNl5BMl5BanBnXkFtZTgwMDQ2MjYzNjE@._V1_SY201_CR48,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjEwNDc5NTExNl5BMl5BanBnXkFtZTgwMDQ2MjYzNjE@._V1_SY201_CR48,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm2223433728/rg1841994496?ref_=hm_snp_cap_pri_1" > <i>The Man from U.N.C.L.E</i> Photocall </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm2424760320/rg1808440064?ref_=hm_snp_i_2" > <img itemprop="image" class="pri_image" title="Mission: Impossible - Rogue Nation (2015)" alt="Mission: Impossible - Rogue Nation (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjU4MDY0ODk2MF5BMl5BanBnXkFtZTgwMDE3MjYzNjE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjU4MDY0ODk2MF5BMl5BanBnXkFtZTgwMDE3MjYzNjE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm2424760320/rg1808440064?ref_=hm_snp_cap_pri_2" > <i>Mission: Impossible - Rogue Nation</i> World Premiere </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm146073600/rg1528338944?ref_=hm_snp_i_3" > <img itemprop="image" class="pri_image" title="Freeheld (2015)" alt="Freeheld (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTYyNDg5OTI1OF5BMl5BanBnXkFtZTgwMzQ4MDUzNjE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTYyNDg5OTI1OF5BMl5BanBnXkFtZTgwMzQ4MDUzNjE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm146073600/rg1528338944?ref_=hm_snp_cap_pri_3" > <i>Freeheld</i> Posters </a> </div> </div> </div> </div> </div> </div> </div>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
                <div class="ab_borntoday">
<span class="ninja_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=7-25&ref_=hm_brn_hd" > <h3>Born Today</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1416900?ref_=hm_brn_i_1" > <img itemprop="image" class="pri_image" title="Shantel VanSanten" alt="Shantel VanSanten" src="http://ia.media-imdb.com/images/M/MV5BMjAxNDk1MzcxNF5BMl5BanBnXkFtZTcwOTI1MDA1OA@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjAxNDk1MzcxNF5BMl5BanBnXkFtZTcwOTI1MDA1OA@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1416900?ref_=hm_brn_cap_pri_lk1_1">Shantel VanSanten</a> (30) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0001455?ref_=hm_brn_i_2" > <img itemprop="image" class="pri_image" title="Matt LeBlanc" alt="Matt LeBlanc" src="http://ia.media-imdb.com/images/M/MV5BODQ0NTI0OTk0M15BMl5BanBnXkFtZTcwMDk2MDg5Nw@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BODQ0NTI0OTk0M15BMl5BanBnXkFtZTcwMDk2MDg5Nw@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0001455?ref_=hm_brn_cap_pri_lk1_2">Matt LeBlanc</a> (48) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0219292?ref_=hm_brn_i_3" > <img itemprop="image" class="pri_image" title="David Denman" alt="David Denman" src="http://ia.media-imdb.com/images/M/MV5BMjExNzgzNTk5OF5BMl5BanBnXkFtZTcwMjgxNDA2Nw@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjExNzgzNTk5OF5BMl5BanBnXkFtZTcwMjgxNDA2Nw@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0219292?ref_=hm_brn_cap_pri_lk1_3">David Denman</a> (42) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0480798?ref_=hm_brn_i_4" > <img itemprop="image" class="pri_image" title="James Lafferty" alt="James Lafferty" src="http://ia.media-imdb.com/images/M/MV5BMTY0NDY0NDg2OF5BMl5BanBnXkFtZTcwNTMxMTgyNw@@._V1_SY172_CR6,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY0NDY0NDg2OF5BMl5BanBnXkFtZTcwNTMxMTgyNw@@._V1_SY172_CR6,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0480798?ref_=hm_brn_cap_pri_lk1_4">James Lafferty</a> (30) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0000605?ref_=hm_brn_i_5" > <img itemprop="image" class="pri_image" title="Brad Renfro" alt="Brad Renfro" src="http://ia.media-imdb.com/images/M/MV5BMTA1NDE3NDA1MDVeQTJeQWpwZ15BbWU3MDE1NTg5MjE@._V1_SY172_CR10,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTA1NDE3NDA1MDVeQTJeQWpwZ15BbWU3MDE1NTg5MjE@._V1_SY172_CR10,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0000605?ref_=hm_brn_cap_pri_lk1_5">Brad Renfro</a> (1982-2008) </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/search/name?refine=birth_monthday&birth_monthday=7-25&ref_=hm_brn_sm" class="position_bottom supplemental" > See all birthdays </a> </p>        </div>

        </span>
        </div>
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/title/tt4370926/?ref_=hm_if_ggid_hd" > <h3>Indie Focus: <i>A Gay Girl in Damascus: The Amina Profile</i></h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm4095601152/tt4370926?ref_=hm_if_ggid_i_1" > <img itemprop="image" class="pri_image" title="A Gay Girl in Damascus: The Amina Profile (2015)" alt="A Gay Girl in Damascus: The Amina Profile (2015)" src="http://ia.media-imdb.com/images/M/MV5BNzUyMjkyNDYyMl5BMl5BanBnXkFtZTgwMTk5MzUxNjE@._V1_SY250_CR1,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNzUyMjkyNDYyMl5BMl5BanBnXkFtZTgwMTk5MzUxNjE@._V1_SY250_CR1,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1184740121?ref_=hm_if_ggid_i_2" data-video="vi1184740121" data-rid="03NRF6PNMK2X8ATY8FC8" data-type="single" class="video-colorbox" data-refsuffix="hm_if_ggid" data-ref="hm_if_ggid_i_2"> <img itemprop="image" class="pri_image" title="A Gay Girl in Damascus: The Amina Profile (2015)" alt="A Gay Girl in Damascus: The Amina Profile (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjM0OTk3Nzc4NV5BMl5BanBnXkFtZTgwODEwNjUxNjE@._V1_SY250_CR0,0,444,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjM0OTk3Nzc4NV5BMl5BanBnXkFtZTgwODEwNjUxNjE@._V1_SY250_CR0,0,444,250_AL_UY500_UX888_AL_.jpg" /> <img alt="A Gay Girl in Damascus: The Amina Profile (2015)" title="A Gay Girl in Damascus: The Amina Profile (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="A Gay Girl in Damascus: The Amina Profile (2015)" title="A Gay Girl in Damascus: The Amina Profile (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">In this exclusive clip, Sandra Bagaria recounts the events detailed in Amina's blog entry entitled "My Father, My Hero," which was subsequently picked up by The Guardian and brought the blog to the global media's attention.</p> <p class="seemore"> <a href="/title/tt4370926/?ref_=hm_if_ggid_sm" class="position_bottom supplemental" > Learn more about <i>A Gay Girl in Damascus: The Amina Profile</i> </a> </p>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/gallery/rg1640667904?ref_=hm_ph_tv_ftwd_hd" > <h3>TV Spotlight: "Fear the Walking Dead"</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm747563008/rg1640667904?ref_=hm_ph_tv_ftwd_i_1" > <img itemprop="image" class="pri_image" title="Fear the Walking Dead (2015-)" alt="Fear the Walking Dead (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMjIxNDE1NzI4OF5BMl5BanBnXkFtZTgwMjgwNzUzNjE@._V1_SX148_CR0,0,148,148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjIxNDE1NzI4OF5BMl5BanBnXkFtZTgwMjgwNzUzNjE@._V1_SX148_CR0,0,148,148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm579790848/rg1640667904?ref_=hm_ph_tv_ftwd_i_2" > <img itemprop="image" class="pri_image" title="Fear the Walking Dead (2015-)" alt="Fear the Walking Dead (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMTg3NTIyNjcwN15BMl5BanBnXkFtZTgwNzcwNzUzNjE@._V1_SY148_CR37,0,148,148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg3NTIyNjcwN15BMl5BanBnXkFtZTgwNzcwNzUzNjE@._V1_SY148_CR37,0,148,148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm546236416/rg1640667904?ref_=hm_ph_tv_ftwd_i_3" > <img itemprop="image" class="pri_image" title="Fear the Walking Dead (2015-)" alt="Fear the Walking Dead (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMjA5OTQ2MTEzOV5BMl5BanBnXkFtZTgwODcwNzUzNjE@._V1_SY148_CR37,0,148,148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA5OTQ2MTEzOV5BMl5BanBnXkFtZTgwODcwNzUzNjE@._V1_SY148_CR37,0,148,148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1032775680/rg1640667904?ref_=hm_ph_tv_ftwd_i_4" > <img itemprop="image" class="pri_image" title="Fear the Walking Dead (2015-)" alt="Fear the Walking Dead (2015-)" src="http://ia.media-imdb.com/images/M/MV5BODg0NTU2MjQwNV5BMl5BanBnXkFtZTgwMDAxNzUzNjE@._V1_SY148_CR37,0,148,148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BODg0NTU2MjQwNV5BMl5BanBnXkFtZTgwMDAxNzUzNjE@._V1_SY148_CR37,0,148,148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">Set in Los Angeles<a href="/title/tt3743822/?ref_=hm_ph_tv_ftwd_lk1">, "Fear the Walking Dead"</a> gives an inside look at what was going on in other parts of the world as the horrifying apocalypse depicted in <a href="/title/tt1520211/?ref_=hm_ph_tv_ftwd_lk2">"The Walking Dead"</a> began.</p> <p class="seemore"> <a href="/gallery/rg1640667904?ref_=hm_ph_tv_ftwd_sm" class="position_bottom supplemental" > See full photo gallery </a> </p>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_trivia">
<span class="ninja_header"> <span class="oneline"> <a href="/title/tt2262227/trivia?item=tr2272684&ref_=hm_trv_hd" > <h3>Did You Know?</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt2262227?ref_=hm_trv_i_1" > <img itemprop="image" class="pri_image" title="The Book of Life (2014)" alt="The Book of Life (2014)" src="http://ia.media-imdb.com/images/M/MV5BMTkzOTgzNDYzOF5BMl5BanBnXkFtZTgwOTgxMTkyMjE@._V1_SX89_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTkzOTgzNDYzOF5BMl5BanBnXkFtZTgwOTgxMTkyMjE@._V1_SX89_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" > <strong class="text-large"><a href="/title/tt2262227?ref_=hm_trv_lk1">The Book of Life</a></strong> <p class="blurb">Near the end of film, the Candle Maker, voiced by <a href="/name/nm0001084?ref_=hm_trv_lk1">Ice Cube</a>, says "Today was a Good Day," the title of an Ice Cube song.</p></div> </div> </div> <p class="seemore"> <a href="/title/tt2262227/trivia?item=tr2272684&ref_=hm_trv_sm" class="position_bottom supplemental" > See more trivia </a> </p>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
    </div>
    <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-1949688977._CB306861155_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-3293651389._CB317087773_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,11x1;p=tr;fv=1;ab=a;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=284832669079;ord=284832669079?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,11x1;p=tr;fv=1;ab=a;bpx=2;c=0;s=3075;s=32;ord=284832669079?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,11x1;p=tr;fv=1;ab=a;bpx=2;c=0;s=3075;s=32;ord=284832669079?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	
                    
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/movies-in-theaters/?ref_=hm_otw_hd" > <h3>Opening This Week</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2120120"></div> <div class="title"> <a href="/title/tt2120120?ref_=hm_otw_t0"> Pixels </a> <span class="secondary-text"></span> </div> <div class="action"> <a href="/showtimes/title/tt2120120?ref_=hm_otw_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1798684"></div> <div class="title"> <a href="/title/tt1798684?ref_=hm_otw_t1"> Southpaw </a> <span class="secondary-text"></span> </div> <div class="action"> <a href="/showtimes/title/tt1798684?ref_=hm_otw_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3622592"></div> <div class="title"> <a href="/title/tt3622592?ref_=hm_otw_t2"> Paper Towns </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1524575"></div> <div class="title"> <a href="/title/tt1524575?ref_=hm_otw_t3"> The Vatican Tapes </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3399024"></div> <div class="title"> <a href="/title/tt3399024?ref_=hm_otw_t4"> Samba </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4145304"></div> <div class="title"> <a href="/title/tt4145304?ref_=hm_otw_t5"> Unexpected </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2946050"></div> <div class="title"> <a href="/title/tt2946050?ref_=hm_otw_t6"> Big Significant Things </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2764784"></div> <div class="title"> <a href="/title/tt2764784?ref_=hm_otw_t7"> Phoenix </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> </div> </div> </div> <div><a href="/movies-in-theaters/?ref_=hm_otw_sm" ><p class="seemore position_bottom">See more opening this week</p></a></div>
        </span>
        </div>
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
            <div class="ab_ninja showtimesMessagingWidget">
<p class="blurb"><a href="/showtimes?ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p>    </div>

        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/chart/?ref_=hm_cht_hd" > <h3>Now Playing (Box Office)</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt0478970"></div> <div class="title"> <a href="/title/tt0478970?ref_=hm_cht_t0"> Ant-Man </a> <span class="secondary-text">$57.2M</span> </div> <div class="action"> <a href="/showtimes/title/tt0478970?ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2293640"></div> <div class="title"> <a href="/title/tt2293640?ref_=hm_cht_t1"> Minions </a> <span class="secondary-text">$49.3M</span> </div> <div class="action"> <a href="/showtimes/title/tt2293640?ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3152624"></div> <div class="title"> <a href="/title/tt3152624?ref_=hm_cht_t2"> Trainwreck </a> <span class="secondary-text">$30.1M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2096673"></div> <div class="title"> <a href="/title/tt2096673?ref_=hm_cht_t3"> Inside Out </a> <span class="secondary-text">$11.5M</span> </div> <div class="action"> <a href="/showtimes/title/tt2096673?ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt0369610"></div> <div class="title"> <a href="/title/tt0369610?ref_=hm_cht_t4"> Jurassic World </a> <span class="secondary-text">$11.5M</span> </div> <div class="action"> <a href="/showtimes/title/tt0369610?ref_=hm_cht_gs"> Showtimes </a> </div> </div> </div> </div> </div> <div><a href="/chart/?ref_=hm_cht_sm" ><p class="seemore position_bottom">See more box office results</p></a></div>
        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/movies-coming-soon/?ref_=hm_cs_hd" > <h3>Coming Soon</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2381249"></div> <div class="title"> <a href="/title/tt2381249?ref_=hm_cs_t0"> Mission: Impossible - Rogue Nation </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1524930"></div> <div class="title"> <a href="/title/tt1524930?ref_=hm_cs_t1"> Vacation </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3518012"></div> <div class="title"> <a href="/title/tt3518012?ref_=hm_cs_t2"> Best of Enemies </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3416744"></div> <div class="title"> <a href="/title/tt3416744?ref_=hm_cs_t3"> The End of the Tour </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3214286"></div> <div class="title"> <a href="/title/tt3214286?ref_=hm_cs_t4"> A LEGO Brickumentary </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> </div> </div> </div> <div><a href="/movies-coming-soon/?ref_=hm_cs_sm" ><p class="seemore position_bottom">See more coming soon</p></a></div>
        </span>
        </div>
                    
                    
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
                <h3>Follow Us On Twitter</h3>
    <iframe class="twitter-frame" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3776329187._CB305760180_.html#{"screen-name":"IMDb","height":"500px","width":"300px"}'></iframe>

 

        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
            <div id="fb-root"></div>

    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    
    <h3>Find Us On Facebook</h3>
    <div class="fb-like-box" data-href="http://www.facebook.com/imdb" data-width="285" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
        <div class="aux-content-widget-2 sticky-widget">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/imdbpicks/?ref_=hm_pks_hd" > <h3>IMDb Picks: July</h3> </a> </span> </span> <p class="blurb">Visit our IMDb Picks section to see our recommendations of movies and TV shows coming out in July.</p> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/?ref_=hm_pks_i_1" > <img itemprop="image" class="pri_image" title="Ant-Man (2015)" alt="Ant-Man (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTQ5MDU4Mjc3OF5BMl5BanBnXkFtZTgwMTIyOTEzNTE@._V1_SY525_CR44,0,700,525_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ5MDU4Mjc3OF5BMl5BanBnXkFtZTgwMTIyOTEzNTE@._V1_SY525_CR44,0,700,525_AL_UY1050_UX1400_AL_.jpg" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/imdbpicks/?ref_=hm_pks_cap_pri_1" > Ant-Man </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/imdbpicks/?ref_=hm_pks_sm" class="position_bottom supplemental" > Visit the IMDb Picks section </a> </p>    </div>

        </span>
        </div>
    </div>
</div>

                    
                    


                   <br class="clear" />
                </div>





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYkw7SaOk0ntq3YT6NhXSr93ky0xdmQu55AsD0a1SDTybiQplgTL00NeG8MaGFt3YD7EGKHcoOn%0D%0ATFifC5_WpjFHDrbzisJ8Pyl-TrGy2bOwyO3wSctPw4gyBxkVV8-cDbRhR98ewYZihkOkR1QXqqIC%0D%0AcHeOQloPuRkNFXSmFsDi4UjvfgC6C2MW4utxL9hQEUN1arcx9c_usVuuDbhLZdBLrZ4kRD5vJp4r%0D%0ARxVTDGlQhrE%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYpB_jCt45ZHkg2u3TGuBoSca6kAQ4e4KWoGzTA_kBza6CMKYgktNifELVJYG8tNTrRHFVvKz8R%0D%0AIbiYCK7ztbCvSwk4cfljH8aM8XwSbUpBopNndufToGhde8dzdeTFvVLYaFvm20oYcFFmbWd_KB1j%0D%0AyTlbwbDkqZhuNUhUBkjXR958mpJf_eIQsHZiXpbfGKxqGmzX7t7XPLhcL8AhNSBA6Q%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYuqvlGvJgzIES_1q5bHcvWpAoKAbWeOhC7y8VdzFis2qbwtkBr76uzsXf7QqZl8RkOcw1Ou6ts%0D%0A1VGuAKgKZDJW5ZOLQlCZ1vr0cI5sckpvbKBiKs-rIsy4SAdCoy6gMkEsDWlzbTh4VEY7uVs4bxiB%0D%0AR4LXN-Zo2j4IqtDIOaRmEh4gh5kHNHJsBVW7lwRo8SQPXYkwNl_Ag4BBDsUB6YbhUADipCvDQGnb%0D%0Ay9dE8VvbUnjk2IEUdAqL8EY4160sUcTPicqb4LRWO_l8IRNK3itp_oWwKaDrU4NLSaWSlYcM6CmN%0D%0Awy27kDoPF4aoyqWTEy-XV6s_SrF7Yk948RBzO_3i6KT1I-Z87tu4BCCBT2jXO7vqBRL9dL2LDwra%0D%0Awuq0DeXIUVhB1wZ3RYYPEVARjBaPag%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYjJJrklTJMPU6ZZdfWGluwK_UQ2eQKeFnRFVACjaIFOTGmpaLMqL-vNrADRVUMd6EUZAx3cn4I%0D%0APT623Bmm6vOy0ODG2W6DlTTH0NNq6lhhQnm5DuKXZgO2l8441XXffsRAcmhqmcTQqq_8Hzbs98Si%0D%0A7-FwAqm9-VD5GeiUb1I3OAA4s82In49YB9M1eHFJL9ZP-ujJinbPoRm8Cr9Ydw7k0w%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYhG_p170Pc7srSvSZ5SeHO3taKWj1uyaqdoHgvntJ7oJ1obxIuKraYEL7CgkNCauYteBfV_OhN%0D%0Arbm_lnbIfIo3y5D9_wSYTurpdgPS16CoQ2D57Tir399ndboq2qAMgcNsqaOpE0Q79hunH1EYJ3VF%0D%0AkZTVDrsIraOdiMhvp_Mg0VyOeKNSj39JavckFlw6GFlf%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYiSC6WHjecPCRwXTEHpwym7wbyB5pdVBNgeY4_ypawLwmEZ9m4m7c95EvNBOKrVNzkf7c8wvYw%0D%0A-WeiCzK9_bZMO9EY1f9a_vex_UuKExhFU78ImkffaoEHEL-PYWnRpekVSH9m-3BtkvVqPsW9L80E%0D%0AGD5qF1scEPu7Y-YyJi4T_CNi2AO3I81Z8CIrhV-WZMQTeUO4vY-jBS_1TB_z08Wh8A%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top 250</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Top Movies</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li>    <a href="/offsite/?page-action=ft_ad&token=BCYhWNvcyi6O6fdZA9Vsm0lU-CNf3yp0t7HUr_UydWvu363AUbwl5OmOQaPKQdtSYSh2udnaLdvI%0D%0AS0Y5oCkAnAdqJcVX2Qeyh7aqBuZ2l4phAjQUw4M--8ijUavtvFdKumN-L0uoEwp4WKzXFWj9teFe%0D%0AWTgylsknIJvs6G44i9sAKTwgfOp4Kx5afmC-1ZqAtVU5BPjKCjazI7IqSvPgJ0BaREA90ZRaO1Ki%0D%0AVI5pN5phAakCrV0lwJRmyNOuSCHW1lgPOHrPrXI0jKfy5Lc_K_zeqA%0D%0A&ref_=ft_ad"
itemprop='url'>Advertising</a>
</li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYvHhhEJb12qUMKVBPWzPyb0iwdJmUgVwdnedsrCcReGpAOpfJe3TcRM5muGIAslW0z6-V5iG0o%0D%0At_PVmW_xi0XJ0y_7rSJavqvhrHokAOZ8XkFqtMZuPPsYWVHyKrl0F9QdcFRkPeW-zVD2JWIpxgBU%0D%0AvinW-vOU_me3keDQ5DwnNl3CWUljb6Ue_K9AVLmVluSS3D1Au2qFpO562rUIXxT26g%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYoJFac_64l-RoIelOZ5cjfvPDry2Q4TMyy1DNz0rOhyLY0-OkUvMCj9sOEjvI5wM68U-KibzlK%0D%0A38a6zTHFOeNUI0B3D1iEHpGp5uHxVAjziGI1Q3cCIqD2Pf9p2dz9-b7Oz4QtTQuxYqXAuRZ78jHo%0D%0AU2ZlrG_sjkjJqn6FzkUIVVMKqsIsEE7-Zx8EX-p1PGw0%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYsgfJbXBknMpFWYP7Kb9vEU2OvCWEqmrgVN0b92h8R4y43F3YinlwjMafnaNIZMol5YGhyslcv%0D%0AydjuHQjNxx9KN1rDaDwIyKAUuwBvqMITEUzFLnWDuxXQagMT5SPq7-ru42P78sYk6YyBiW2TuV0O%0D%0Af_-VNObVMydPwenXCdW28JsJb3wfffJHJHvW6iJP1F80qzxvA7yYqxGUSZp6L6UQ__6lNf570MGL%0D%0A0tLAN6db5399JN8h-UHBoBlzZMggeF4z%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/show_article?conditions';"
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2015
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/';"
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYo_Jx9jlmgKq5xLkQdnqgTibjOSObvovYZ15VxNUSXOwJGArtMTx9amuyHqXN-mHNEoA3W5ArV%0D%0AKcP8GPNAxitB1edrLXBF4kMoVgddBPGApDM224Bd4MnOWOuk8siUoxfh5xdj_OxrtcOgfvCsWmVW%0D%0A-XT6p9h18mwNLbqn99ZzuIltucw038C74Ej0MQa9UGCe1RA_G9d0L_IHi-dhcJx-sDMEz9ozHfMw%0D%0AKh3hu_gee8I%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Instant Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYvc4mAMqk_s-1oQNvWbVLPqKE_0GO9KUKEe360c8myr4fQO6Z_5LdOUM8esCdACbR5kx1Edobe%0D%0AqrEyfp751LQRzkp5uniM52VD54jPQUkt8v7oHmT6sZ-M2iMclQ8WnOsWBaEk-_zFJRUAc3XS1fIU%0D%0Azg8SHUHaBya1ay3ajFwiX5gJSUJgBbbsdGhYqjTM4NNZjoE8S2HISzLbQh4wriYYV9oEuIBhy-Y5%0D%0AAejkvoDJ7rM%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Instant Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYhMZE6SZo3OdJCAGQcrGAObHxXJ39BtaMXy4RiPt02gVoEZqVWIAVC5j9OS2ClUMMbqmAY8hqt%0D%0ACwByj0SBfz521PYN538Vebe_EP4neprFq9Y8do524iRmtH-IGyUefafplMdwX1DdADWS4rEMtqT8%0D%0AZdoU476DfxI5k-KUWRhG8hJZHjCwBm3OPF2LwZUwJbJI5a77Ozu1FOMNaWJNMwdbsEguj1WJ6rWH%0D%0A4TzrAvq9rPc%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYnOmwfzCyBtjzzDdGSBxI82AY0vy-2_KgZscGs28d531gXmeBmFVCryefZ61DLUzJrP6udsojG%0D%0AYBKEjH8iwNiNBoRyZoG7h5m_lo11Tn9RR0RKFKCYKpJV2xb65A_nZ-6ELzlt7tdGgLbkRAZ4DlVd%0D%0AFeimUMAY0MFiG-uYD1V2w_0-DldVt6kObwjZJwh2tVyFcd2_MViPuYkp-lCONJ47tFrDFxMZYBGq%0D%0Ad5VN4SF6oAE%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYiklxVmQWo8YBvpjf_uDKAZe1bWSBX7DyLZqz6jM3AcAQsZjqwpUSTZ17vM8kv0GjDq-GXxP-s%0D%0AhyAcn2KyNXbpaJ0tnDqP3CvU1ehrbta1OzWRSjsF6jgpFA9cs5ecqiPYJUXtUrCuO2CCWuRsmjDF%0D%0ANms6K3ww5Iw-TNCb7axpjuE3BiUuHdERv0fA1CuxUqY1GgQiIrRIekr4cSRtc6WYbrVoIWi1mVEu%0D%0AuMfMIdFKlPo%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYs2s2E_fqPXD8_H-WYab8vg3mOtyx2g6PUYfSSbwqkG6Z8Nem4uen9qnjnZboKjeR2stfp5N6U%0D%0Ap7wJlKFnyAbr5IMRqGZ2ba2DBxVvHRjNFpA5n1i6wNbp8QAue0_LRgEWseX11SpWXoQEWWyzGN8l%0D%0AUYMAlpgQtwyBi_RCx5qjItPAXvgeSYYnNu4u1hA8paHfe5GKE5VNd4TN9-NosAjvlknEh7BfQ_Bs%0D%0ApUQx7NO47ONYpLfskb3Oh_CdcFUe3Hyo%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYrhKjcVQfsvVrsllN5FbtAr21kTwOoKs-GMUqMcyaJiRE8QVWd8l8jetUO1meoKnZgCahUPeaG%0D%0A1qBYmCdcmQZJOpt8Nqp2KBnGet5dNsM-fb5yWKuUwRe0NcZ9J187TiMRXwea1TeDJ1ghS6bIVAQN%0D%0AYLUKiJgAb79_oQ9roTLTX5o%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYvO9UFROAiV1yWwjRuJtekDhUE5GYlo-4TEai6mkL0AVT_HVSX5Oc5-QDcOylHytYCjkPSu3o6%0D%0AoLrwGQutzxJHxbKm_BvhPKQUasnFTBGPFDEOlCHf606B62uakEuTdOj99rixcekZExvsjllFE913%0D%0ABL77QyBvEGU3AEDeHLOQJeE%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-1190212460._CB314627670_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-135080496._CB318528481_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-3627981178._CB318528484_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-2430112694._CB318528507_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=01018b3183d93d93c77a418e7222723704212b4054a73ffc3466872ead641afa27af",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=284832669079"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-232398347._CB349580400_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=284832669079&ord=284832669079";
    },"unable to request AAN pixel");
</script>

<div id="flashContent" style="width: 0px; height: 0px; overflow:hidden;">
    <script type="text/javascript">
        if (generic && generic.monitoring.record_metric && flashAdUtils.canPlayFlash) {
            var flashLoaded = 0;
            function swfLoaded() {
                flashLoaded = 1;
                generic.monitoring.record_metric('ads_flash_did_play', 1, true);
            }
            window.onload = function() {
                // Flash was not loaded
                if (flashLoaded === 0) {
                    // Flash could be loaded but wasn't
                    if (flashAdUtils.canPlayFlash()) {
                        generic.monitoring.record_metric('ads_flash_can_but_did_not_play', 0, true);
                    } else {
                        generic.monitoring.record_metric('ads_flash_cannot_play', 0, true);
                    }
                }
            }
            /**
             * There is a rare possibility of window.onload getting called before swfLoaded.
             * Firing this metric to keep track of such calls.
             */
             generic.monitoring.record_metric('ads_flash_page_loaded', 1, true);
         }
    </script>
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="1x1" align="middle">
        <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
        <param name="quality" value="high" />
        <param name="play" value="true" />
        <param name="loop" value="true" />
        <param name="wmode" value="transparent" />
        <param name="scale" value="showall" />
        <param name="menu" value="true" />
        <param name="devicefont" value="false" />
        <param name="salign" value="" />
        <param name="allowScriptAccess" value="always" />

        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" width="1" height="1">
            <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
            <param name="quality" value="high" />
            <param name="play" value="true" />
            <param name="loop" value="true" />
            <param name="wmode" value="transparent" />
            <param name="scale" value="showall" />
            <param name="menu" value="true" />
            <param name="devicefont" value="false" />
            <param name="salign" value="" />
            <param name="allowScriptAccess" value="always" />
        </object>
        <!--<![endif]-->
    </object>
</div>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="675"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
