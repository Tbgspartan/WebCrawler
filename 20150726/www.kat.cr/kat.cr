<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-82dc9f3.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-82dc9f3.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-82dc9f3.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-82dc9f3.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kat.cr/sc-82dc9f3.js');

        sc('setHost', 'a.kat.cr');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: '82dc9f3',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-82dc9f3.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<div id="_60318cd4e8d28f6fb76fe34e9bd9c498"></div>
<div id="_39ecb76dd457e5ac33776fdf11500d56"></div>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom" style="color: #ffeeb4; font-size: 34px; float:left; height: 50px; line-height: 50px;"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka">e</i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka">g</i></button></div>
			</form>
		</div>
        <div id="_277923e5f9d753c5b0630c28e641790c"></div>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080%20ted%202/" class="tag2">1080 ted 2</a>
	<a href="/search/1080p/" class="tag6">1080p</a>
	<a href="/search/1080p/" class="tag5">1080p</a>
	<a href="/search/2014/" class="tag3">2014</a>
	<a href="/search/2015/" class="tag9">2015</a>
	<a href="/search/2015/" class="tag7">2015</a>
	<a href="/search/3d/" class="tag4">3d</a>
	<a href="/search/3d%20remux/" class="tag2">3d remux</a>
	<a href="/search/android/" class="tag10">android</a>
	<a href="/search/android/" class="tag3">android</a>
	<a href="/search/ant%20man/" class="tag2">ant man</a>
	<a href="/search/arrow/" class="tag2">arrow</a>
	<a href="/search/avengers/" class="tag2">avengers</a>
	<a href="/search/baahubali/" class="tag2">baahubali</a>
	<a href="/search/bajrangi%20bhaijaan/" class="tag3">bajrangi bhaijaan</a>
	<a href="/search/batman/" class="tag2">batman</a>
	<a href="/search/discography/" class="tag3">discography</a>
	<a href="/search/fast%20and%20furious%207/" class="tag2">fast and furious 7</a>
	<a href="/search/flac/" class="tag2">flac</a>
	<a href="/search/french/" class="tag3">french</a>
	<a href="/search/game%20of%20thrones/" class="tag3">game of thrones</a>
	<a href="/search/hamari%20adhuri%20kahani/" class="tag2">hamari adhuri kahani</a>
	<a href="/search/hannibal/" class="tag2">hannibal</a>
	<a href="/search/hindi/" class="tag7">hindi</a>
	<a href="/search/hindi/" class="tag4">hindi</a>
	<a href="/search/hindi%202015/" class="tag2">hindi 2015</a>
	<a href="/search/hindi%20movies%202015/" class="tag2">hindi movies 2015</a>
	<a href="/search/inside%20out/" class="tag2">inside out</a>
	<a href="/search/insurgent/" class="tag2">insurgent</a>
	<a href="/search/ita/" class="tag3">ita</a>
	<a href="/search/kat%20ph%20com/" class="tag3">kat ph com</a>
	<a href="/search/mad%20max/" class="tag2">mad max</a>
	<a href="/search/malayalam/" class="tag2">malayalam</a>
	<a href="/search/minions/" class="tag2">minions</a>
	<a href="/search/movies/" class="tag3">movies</a>
	<a href="/search/nezu/" class="tag10">nezu</a>
	<a href="/search/nl/" class="tag3">nl</a>
	<a href="/search/spy%202015/" class="tag2">spy 2015</a>
	<a href="/search/tamil/" class="tag3">tamil</a>
	<a href="/search/telugu/" class="tag3">telugu</a>
	<a href="/search/the%20big%20bang%20theory/" class="tag3">the big bang theory</a>
	<a href="/search/the%20flash/" class="tag2">the flash</a>
	<a href="/search/the%20walking%20dead/" class="tag3">the walking dead</a>
	<a href="/search/undefined/" class="tag3">undefined</a>
	<a href="/search/under%20the%20dome/" class="tag2">under the dome</a>
	<a href="/search/yify/" class="tag10">yify</a>
	<a href="/search/yify%201080p/" class="tag3">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag3">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="rsssign" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10981174,0" class="icomment icommentjs icon16" href="/true-story-2015-720p-brrip-x264-yify-t10981174.html#comment"> <em class="iconvalue">103</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/true-story-2015-720p-brrip-x264-yify-t10981174.html" class="cellMainLink">True Story (2015) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center">755.97 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">17131</td>
			<td class="red lasttd center">8316</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10983550,0" class="icomment icommentjs icon16" href="/bajrangi-bhaijaan-2015-hindi-non-retail-dvd-team-exd-john-f-nash-t10983550.html#comment"> <em class="iconvalue">239</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/bajrangi-bhaijaan-2015-hindi-non-retail-dvd-team-exd-john-f-nash-t10983550.html" class="cellMainLink">Bajrangi Bhaijaan (2015) HINDI Non-Retail DVD Team EXD[John F Nash]</a></div>
			</td>
			<td class="nobr center">1.2 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">8482</td>
			<td class="red lasttd center">8613</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10993234,0" class="icomment icommentjs icon16" href="/terminator-genisys-2015-hd-ts-xvid-ac3-cpg-t10993234.html#comment"> <em class="iconvalue">37</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/terminator-genisys-2015-hd-ts-xvid-ac3-cpg-t10993234.html" class="cellMainLink">Terminator Genisys 2015 HD-TS XviD AC3-CPG</a></div>
			</td>
			<td class="nobr center">1.85 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center">13&nbsp;hours</td>
			<td class="green center">3524</td>
			<td class="red lasttd center">8179</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10987613,0" class="icomment icommentjs icon16" href="/descendants-2015-dvdrip-xvid-evo-t10987613.html#comment"> <em class="iconvalue">38</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/descendants-2015-dvdrip-xvid-evo-t10987613.html" class="cellMainLink">Descendants 2015 DVDRip XviD-EVO</a></div>
			</td>
			<td class="nobr center">1003.85 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">5356</td>
			<td class="red lasttd center">5330</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10982269,0" class="icomment icommentjs icon16" href="/war-pigs-2015-hdrip-xvid-etrg-t10982269.html#comment"> <em class="iconvalue">51</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/war-pigs-2015-hdrip-xvid-etrg-t10982269.html" class="cellMainLink">War Pigs 2015 HDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center">707.18 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">6034</td>
			<td class="red lasttd center">3489</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10985073,0" class="icomment icommentjs icon16" href="/the-dead-lands-2014-limited-french-bdrip-x264-melba-mkv-t10985073.html#comment"> <em class="iconvalue">10</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-dead-lands-2014-limited-french-bdrip-x264-melba-mkv-t10985073.html" class="cellMainLink">The Dead Lands 2014 LiMiTED FRENCH BDRip x264-MELBA mkv</a></div>
			</td>
			<td class="nobr center">899.52 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">6062</td>
			<td class="red lasttd center">2500</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10985489,0" class="icomment icommentjs icon16" href="/final-girl-2015-720p-brrip-x264-yify-t10985489.html#comment"> <em class="iconvalue">25</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/final-girl-2015-720p-brrip-x264-yify-t10985489.html" class="cellMainLink">Final Girl (2015) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center">696.09 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">4256</td>
			<td class="red lasttd center">4080</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10978585,0" class="icomment icommentjs icon16" href="/a-todo-gas-7-spanish-espaÃol-hdrip-xvid-elitetorrent-t10978585.html#comment"> <em class="iconvalue">46</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/a-todo-gas-7-spanish-espaÃol-hdrip-xvid-elitetorrent-t10978585.html" class="cellMainLink">A todo gas 7 SPANISH ESPAÃOL HDRip XviD-ELITETORRENT</a></div>
			</td>
			<td class="nobr center">2.21 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">5522</td>
			<td class="red lasttd center">2184</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10991611,0" class="icomment icommentjs icon16" href="/fast-and-furious-7-2015-extended-1080p-brrip-x264-dts-jyk-t10991611.html#comment"> <em class="iconvalue">35</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fast-and-furious-7-2015-extended-1080p-brrip-x264-dts-jyk-t10991611.html" class="cellMainLink">Fast and Furious 7 2015 EXTENDED 1080p BRRip x264 DTS-JYK</a></div>
			</td>
			<td class="nobr center">3.53 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center">21&nbsp;hours</td>
			<td class="green center">2548</td>
			<td class="red lasttd center">4064</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10982277,0" class="icomment icommentjs icon16" href="/adult-beginners-2014-720p-brrip-x264-yify-t10982277.html#comment"> <em class="iconvalue">29</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/adult-beginners-2014-720p-brrip-x264-yify-t10982277.html" class="cellMainLink">Adult Beginners (2014) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center">749.01 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">4073</td>
			<td class="red lasttd center">2465</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10990027,0" class="icomment icommentjs icon16" href="/lake-placid-vs-anaconda-2015-dvdrip-xvid-etrg-t10990027.html#comment"> <em class="iconvalue">24</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/lake-placid-vs-anaconda-2015-dvdrip-xvid-etrg-t10990027.html" class="cellMainLink">Lake Placid vs. Anaconda.2015.DVDRip.XViD-ETRG</a></div>
			</td>
			<td class="nobr center">707.29 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">3087</td>
			<td class="red lasttd center">3057</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10987253,0" class="icomment icommentjs icon16" href="/dark-was-the-night-2014-hdrip-xvid-etrg-t10987253.html#comment"> <em class="iconvalue">30</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dark-was-the-night-2014-hdrip-xvid-etrg-t10987253.html" class="cellMainLink">Dark Was the Night.2014.HDRip.XViD-ETRG</a></div>
			</td>
			<td class="nobr center">712.46 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">3419</td>
			<td class="red lasttd center">2578</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10982213,0" class="icomment icommentjs icon16" href="/survivor-spanish-espaÃol-hdrip-xvid-elitetorrent-t10982213.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/survivor-spanish-espaÃol-hdrip-xvid-elitetorrent-t10982213.html" class="cellMainLink">Survivor SPANISH ESPAÃOL HDRip XviD-ELITETORRENT</a></div>
			</td>
			<td class="nobr center">1.76 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">3688</td>
			<td class="red lasttd center">1688</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10982234,0" class="icomment icommentjs icon16" href="/a-little-chaos-2014-1080p-brrip-x264-yify-t10982234.html#comment"> <em class="iconvalue">16</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/a-little-chaos-2014-1080p-brrip-x264-yify-t10982234.html" class="cellMainLink">A Little Chaos (2014) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center">1.65 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">2927</td>
			<td class="red lasttd center">2285</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10982325,0" class="icomment icommentjs icon16" href="/velozes-e-furiosos-7-versÃ£o-estendida-2015-bluray-1080p-dual-Ãudio-dublado-t10982325.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					<a class="istill icon16" href="/velozes-e-furiosos-7-versÃ£o-estendida-2015-bluray-1080p-dual-Ãudio-dublado-t10982325.html#stills" title="Torrent Has Screenshots"><span></span></a>
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/velozes-e-furiosos-7-versÃ£o-estendida-2015-bluray-1080p-dual-Ãudio-dublado-t10982325.html" class="cellMainLink">Velozes e Furiosos 7 VersÃ£o Estendida (2015) BluRay 1080p Dual Ãudio Dublado</a></div>
			</td>
			<td class="nobr center">2.5 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">1457</td>
			<td class="red lasttd center">1638</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="rsssign" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10980560,0" class="icomment icommentjs icon16" href="/suits-s05e05-hdtv-x264-killers-ettv-t10980560.html#comment"> <em class="iconvalue">121</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/suits-s05e05-hdtv-x264-killers-ettv-t10980560.html" class="cellMainLink">Suits S05E05 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center">259.08 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">14037</td>
			<td class="red lasttd center">1144</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10980711,0" class="icomment icommentjs icon16" href="/mr-robot-s01e05-hdtv-x264-killers-ettv-t10980711.html#comment"> <em class="iconvalue">256</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/mr-robot-s01e05-hdtv-x264-killers-ettv-t10980711.html" class="cellMainLink">Mr Robot S01E05 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center">180.49 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">10288</td>
			<td class="red lasttd center">1045</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10985431,0" class="icomment icommentjs icon16" href="/under-the-dome-s03e06-hdtv-x264-lol-ettv-t10985431.html#comment"> <em class="iconvalue">82</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/under-the-dome-s03e06-hdtv-x264-lol-ettv-t10985431.html" class="cellMainLink">Under the Dome S03E06 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center">286.32 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">6856</td>
			<td class="red lasttd center">919</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10979761,0" class="icomment icommentjs icon16" href="/wayward-pines-s01e10-hdtv-xvid-fum-ettv-t10979761.html#comment"> <em class="iconvalue">142</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/wayward-pines-s01e10-hdtv-xvid-fum-ettv-t10979761.html" class="cellMainLink">Wayward Pines S01E10 HDTV XviD-FUM[ettv]</a></div>
			</td>
			<td class="nobr center">348.95 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">6696</td>
			<td class="red lasttd center">782</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10991912,0" class="icomment icommentjs icon16" href="/dark-matter-s01e07-hdtv-x264-killers-rartv-t10991912.html#comment"> <em class="iconvalue">55</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dark-matter-s01e07-hdtv-x264-killers-rartv-t10991912.html" class="cellMainLink">Dark Matter S01E07 HDTV x264-KILLERS[rartv]</a></div>
			</td>
			<td class="nobr center">256.55 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">20&nbsp;hours</td>
			<td class="green center">5514</td>
			<td class="red lasttd center">1367</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10991492,0" class="icomment icommentjs icon16" href="/killjoys-s01e06-hdtv-x264-killers-ettv-t10991492.html#comment"> <em class="iconvalue">46</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/killjoys-s01e06-hdtv-x264-killers-ettv-t10991492.html" class="cellMainLink">Killjoys S01E06 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center">367.13 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">21&nbsp;hours</td>
			<td class="green center">4911</td>
			<td class="red lasttd center">1520</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10975181,0" class="icomment icommentjs icon16" href="/pretty-little-liars-s06e07-hdtv-x264-lol-ettv-t10975181.html#comment"> <em class="iconvalue">35</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/pretty-little-liars-s06e07-hdtv-x264-lol-ettv-t10975181.html" class="cellMainLink">Pretty Little Liars S06E07 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center">194.6 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">5609</td>
			<td class="red lasttd center">394</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10990935,0" class="icomment icommentjs icon16" href="/defiance-s03e08-hdtv-x264-killers-rartv-t10990935.html#comment"> <em class="iconvalue">30</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/defiance-s03e08-hdtv-x264-killers-rartv-t10990935.html" class="cellMainLink">Defiance S03E08 HDTV x264-KILLERS[rartv]</a></div>
			</td>
			<td class="nobr center">353.94 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">22&nbsp;hours</td>
			<td class="green center">4270</td>
			<td class="red lasttd center">1703</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10986459,0" class="icomment icommentjs icon16" href="/dominion-s02e03-hdtv-x264-killers-rartv-t10986459.html#comment"> <em class="iconvalue">43</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dominion-s02e03-hdtv-x264-killers-rartv-t10986459.html" class="cellMainLink">Dominion S02E03 HDTV x264-KILLERS[rartv]</a></div>
			</td>
			<td class="nobr center">295.48 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">3920</td>
			<td class="red lasttd center">846</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10991534,0" class="icomment icommentjs icon16" href="/the-messengers-2015-s01e13-hdtv-x264-killers-ettv-t10991534.html#comment"> <em class="iconvalue">42</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-messengers-2015-s01e13-hdtv-x264-killers-ettv-t10991534.html" class="cellMainLink">The Messengers 2015 S01E13 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center">317.56 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">21&nbsp;hours</td>
			<td class="green center">3636</td>
			<td class="red lasttd center">1130</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10992032,0" class="icomment icommentjs icon16" href="/rookie-blue-s06e10-web-dl-xvid-fum-ettv-t10992032.html#comment"> <em class="iconvalue">21</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/rookie-blue-s06e10-web-dl-xvid-fum-ettv-t10992032.html" class="cellMainLink">Rookie Blue S06E10 WEB-DL XviD-FUM[ettv]</a></div>
			</td>
			<td class="nobr center">348.1 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center">19&nbsp;hours</td>
			<td class="green center">2044</td>
			<td class="red lasttd center">619</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10985748,0" class="icomment icommentjs icon16" href="/mistresses-us-s03e07-hdtv-x264-killers-rartv-t10985748.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/mistresses-us-s03e07-hdtv-x264-killers-rartv-t10985748.html" class="cellMainLink">Mistresses US S03E07 HDTV x264-KILLERS[rartv]</a></div>
			</td>
			<td class="nobr center">313.59 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">1764</td>
			<td class="red lasttd center">377</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10992553,0" class="icomment icommentjs icon16" href="/ancient-aliens-s08e01-aliens-b-c-720p-hdtv-x264-dhd-rartv-t10992553.html#comment"> <em class="iconvalue">14</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ancient-aliens-s08e01-aliens-b-c-720p-hdtv-x264-dhd-rartv-t10992553.html" class="cellMainLink">Ancient Aliens S08E01 Aliens B C 720p HDTV x264-DHD[rartv]</a></div>
			</td>
			<td class="nobr center">1.09 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center">17&nbsp;hours</td>
			<td class="green center">1151</td>
			<td class="red lasttd center">587</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10990837,0" class="icomment icommentjs icon16" href="/hannibal-s03e08-720p-hdtv-x264-killers-t10990837.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/hannibal-s03e08-720p-hdtv-x264-killers-t10990837.html" class="cellMainLink">Hannibal S03E08 720p HDTV x264-KILLERS</a></div>
			</td>
			<td class="nobr center">832.26 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">22&nbsp;hours</td>
			<td class="green center">579</td>
			<td class="red lasttd center">502</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10980543,0" class="icomment icommentjs icon16" href="/extant-s02e04-hdtv-x264-lol-rartv-t10980543.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/extant-s02e04-hdtv-x264-lol-rartv-t10980543.html" class="cellMainLink">Extant S02E04 HDTV x264-LOL[rartv]</a></div>
			</td>
			<td class="nobr center">262.99 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">509</td>
			<td class="red lasttd center">328</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="rsssign" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10983431,0" class="icomment icommentjs icon16" href="/billboard-hot-100-singles-chart-01st-august-2015-mp3-320kbps-h4ckus-glodls-t10983431.html#comment"> <em class="iconvalue">21</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/billboard-hot-100-singles-chart-01st-august-2015-mp3-320kbps-h4ckus-glodls-t10983431.html" class="cellMainLink">Billboard Hot 100 Singles Chart (01st August 2015) [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center">829.59 <span>MB</span></td>
			<td class="center">103</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">1616</td>
			<td class="red lasttd center">884</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10988052,0" class="icomment icommentjs icon16" href="/jill-scott-woman-2015-mp3-320kbps-glodls-t10988052.html#comment"> <em class="iconvalue">13</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/jill-scott-woman-2015-mp3-320kbps-glodls-t10988052.html" class="cellMainLink">Jill Scott - Woman [2015] [MP3-320KBPS] [GloDLS]</a></div>
			</td>
			<td class="nobr center">132.44 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">804</td>
			<td class="red lasttd center">91</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10981639,0" class="icomment icommentjs icon16" href="/lamb-of-god-vii-sturm-und-drang-de-2015-320-cd-t10981639.html#comment"> <em class="iconvalue">39</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/lamb-of-god-vii-sturm-und-drang-de-2015-320-cd-t10981639.html" class="cellMainLink">Lamb of God VII Sturm Und Drang [DE 2015] 320 CD</a></div>
			</td>
			<td class="nobr center">132.26 <span>MB</span></td>
			<td class="center">14</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">739</td>
			<td class="red lasttd center">102</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10985020,0" class="icomment icommentjs icon16" href="/mp3-new-releases-2015-week-29-suprax-glodls-t10985020.html#comment"> <em class="iconvalue">9</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/mp3-new-releases-2015-week-29-suprax-glodls-t10985020.html" class="cellMainLink">MP3 NEW RELEASES 2015 WEEK 29 - SUPRAX [GloDLS]</a></div>
			</td>
			<td class="nobr center">3.9 <span>GB</span></td>
			<td class="center">515</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">423</td>
			<td class="red lasttd center">401</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10993940,0" class="icomment icommentjs icon16" href="/va-the-official-uk-top-40-singles-chart-24th-july-2015-itunes-rip-m4a-aac-uj-rip-t10993940.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-the-official-uk-top-40-singles-chart-24th-july-2015-itunes-rip-m4a-aac-uj-rip-t10993940.html" class="cellMainLink">VA - The Official UK Top 40 Singles Chart - 24th July 2015 [iTunes Rip] [M4A] [AAC] [UJ.rip]</a></div>
			</td>
			<td class="nobr center">296.91 <span>MB</span></td>
			<td class="center">44</td>
			<td class="center">10&nbsp;hours</td>
			<td class="green center">1</td>
			<td class="red lasttd center">464</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10989633,0" class="icomment icommentjs icon16" href="/tech-house-2015-mp3-divxtotal-t10989633.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/tech-house-2015-mp3-divxtotal-t10989633.html" class="cellMainLink">Tech House 2015 MP3-DIVXTOTAL</a></div>
			</td>
			<td class="nobr center">686.35 <span>MB</span></td>
			<td class="center">34</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">307</td>
			<td class="red lasttd center">136</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10977887,0" class="icomment icommentjs icon16" href="/hopsin-pound-syndrome-2015-l-audio-l-album-track-l-web-l-320kbps-l-cbr-l-mp3-l-sn3h1t87-t10977887.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/hopsin-pound-syndrome-2015-l-audio-l-album-track-l-web-l-320kbps-l-cbr-l-mp3-l-sn3h1t87-t10977887.html" class="cellMainLink">Hopsin - Pound Syndrome (2015) l Audio l Album Track l Web l 320Kbps l CBR l Mp3 l sn3h1t87</a></div>
			</td>
			<td class="nobr center">115.96 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">342</td>
			<td class="red lasttd center">76</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10984593,0" class="icomment icommentjs icon16" href="/classic-summer-2015-mp3-divxtotal-t10984593.html#comment"> <em class="iconvalue">9</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/classic-summer-2015-mp3-divxtotal-t10984593.html" class="cellMainLink">Classic Summer (2015) MP3-DIVXTOTAL</a></div>
			</td>
			<td class="nobr center">686.23 <span>MB</span></td>
			<td class="center">66</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">280</td>
			<td class="red lasttd center">90</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10978587,0" class="icomment icommentjs icon16" href="/vocal-drum-bass-temptation-2015-mp3-divxtotal-t10978587.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/vocal-drum-bass-temptation-2015-mp3-divxtotal-t10978587.html" class="cellMainLink">Vocal Drum &amp; Bass Temptation (2015) MP3-DIVXTOTAL</a></div>
			</td>
			<td class="nobr center">1.35 <span>GB</span></td>
			<td class="center">120</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">205</td>
			<td class="red lasttd center">123</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10987672,0" class="icomment icommentjs icon16" href="/brothers-2015-full-album-mp3-320-kbps-groo-t10987672.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/brothers-2015-full-album-mp3-320-kbps-groo-t10987672.html" class="cellMainLink">Brothers (2015) Full Album MP3 320 Kbps Groo</a></div>
			</td>
			<td class="nobr center">66 <span>MB</span></td>
			<td class="center">7</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">210</td>
			<td class="red lasttd center">68</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10988918,0" class="icomment icommentjs icon16" href="/trance-va-a-state-of-trance-classics-vol-10-4cd-2015-mp3-v0-vbr-dds-edm-rg-t10988918.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/trance-va-a-state-of-trance-classics-vol-10-4cd-2015-mp3-v0-vbr-dds-edm-rg-t10988918.html" class="cellMainLink">(Trance) VA - A State Of Trance Classics Vol.10 4CD (2015) MP3, V0 [VBR] DDS [EDM RG]</a></div>
			</td>
			<td class="nobr center">526.4 <span>MB</span></td>
			<td class="center">43</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">186</td>
			<td class="red lasttd center">82</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10984002,0" class="icomment icommentjs icon16" href="/va-bravo-hits-vol-90-2cd-2015-mp3-vbr-h4ckus-glodls-t10984002.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-bravo-hits-vol-90-2cd-2015-mp3-vbr-h4ckus-glodls-t10984002.html" class="cellMainLink">VA - Bravo Hits Vol. 90 [2CD] [2015] [MP3-VBR] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center">291.89 <span>MB</span></td>
			<td class="center">51</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">149</td>
			<td class="red lasttd center">75</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10989212,0" class="icomment icommentjs icon16" href="/armin-van-buuren-a-state-of-trance-723-2015-07-23-split-inspiron-t10989212.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/armin-van-buuren-a-state-of-trance-723-2015-07-23-split-inspiron-t10989212.html" class="cellMainLink">Armin Van Buuren - A State Of Trance 723 (2015-07-23) (Split) (Inspiron)</a></div>
			</td>
			<td class="nobr center">300.84 <span>MB</span></td>
			<td class="center">38</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">130</td>
			<td class="red lasttd center">44</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10994611,0" class="icomment icommentjs icon16" href="/va-water-dance-100-tracks-2015-mp3-320-kbps-t10994611.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-water-dance-100-tracks-2015-mp3-320-kbps-t10994611.html" class="cellMainLink">VA - Water Dance 100 Tracks (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center">1003.62 <span>MB</span></td>
			<td class="center">102</td>
			<td class="center">6&nbsp;hours</td>
			<td class="green center">40</td>
			<td class="red lasttd center">68</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10994589,0" class="icomment icommentjs icon16" href="/va-we-are-dance-vol-3-2015-mp3-320-kbps-t10994589.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-we-are-dance-vol-3-2015-mp3-320-kbps-t10994589.html" class="cellMainLink">VA - We Are Dance Vol. 3 (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center">590.43 <span>MB</span></td>
			<td class="center">65</td>
			<td class="center">6&nbsp;hours</td>
			<td class="green center">25</td>
			<td class="red lasttd center">61</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="rsssign" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10985657,0" class="icomment icommentjs icon16" href="/five-nights-at-freddy-s-4-t10985657.html#comment"> <em class="iconvalue">69</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/five-nights-at-freddy-s-4-t10985657.html" class="cellMainLink">Five Nights at Freddy&#039;s 4</a></div>
			</td>
			<td class="nobr center">445.94 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">2057</td>
			<td class="red lasttd center">284</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10989737,0" class="icomment icommentjs icon16" href="/cradle-codex-t10989737.html#comment"> <em class="iconvalue">9</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/cradle-codex-t10989737.html" class="cellMainLink">Cradle-CODEX</a></div>
			</td>
			<td class="nobr center">1.71 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">1510</td>
			<td class="red lasttd center">343</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10978166,0" class="icomment icommentjs icon16" href="/mortal-kombat-x-update-12-2015-pc-repack-Ð¾Ñ-xatab-t10978166.html#comment"> <em class="iconvalue">96</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/mortal-kombat-x-update-12-2015-pc-repack-Ð¾Ñ-xatab-t10978166.html" class="cellMainLink">Mortal Kombat X [Update 12] (2015) PC | RePack Ð¾Ñ xatab</a></div>
			</td>
			<td class="nobr center">23.45 <span>GB</span></td>
			<td class="center">13</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">635</td>
			<td class="red lasttd center">882</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10983548,0" class="icomment icommentjs icon16" href="/way-of-the-samurai-4-codex-t10983548.html#comment"> <em class="iconvalue">74</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/way-of-the-samurai-4-codex-t10983548.html" class="cellMainLink">Way of the Samurai 4-CODEX</a></div>
			</td>
			<td class="nobr center">4.26 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">738</td>
			<td class="red lasttd center">689</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10988487,0" class="icomment icommentjs icon16" href="/battlefield-4-xatab-t10988487.html#comment"> <em class="iconvalue">34</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/battlefield-4-xatab-t10988487.html" class="cellMainLink">Battlefield 4- XATAB</a></div>
			</td>
			<td class="nobr center">18.86 <span>GB</span></td>
			<td class="center">15</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">446</td>
			<td class="red lasttd center">814</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10993031,0" class="icomment icommentjs icon16" href="/the-witcher-3-wild-hunt-v-1-07-15-dlc-2015-pc-steamrip-Ð¾Ñ-let-sÐ lay-t10993031.html#comment"> <em class="iconvalue">11</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/the-witcher-3-wild-hunt-v-1-07-15-dlc-2015-pc-steamrip-Ð¾Ñ-let-sÐ lay-t10993031.html" class="cellMainLink">The Witcher 3: Wild Hunt [v 1.07 + 15 DLC] (2015) PC | SteamRip Ð¾Ñ Let&#039;sÐ lay</a></div>
			</td>
			<td class="nobr center">29.79 <span>GB</span></td>
			<td class="center">11</td>
			<td class="center">14&nbsp;hours</td>
			<td class="green center">226</td>
			<td class="red lasttd center">620</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10980731,0" class="icomment icommentjs icon16" href="/star-wars-empire-at-war-gold-pack-gog-t10980731.html#comment"> <em class="iconvalue">21</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/star-wars-empire-at-war-gold-pack-gog-t10980731.html" class="cellMainLink">Star Wars: Empire at War - Gold Pack (GOG)</a></div>
			</td>
			<td class="nobr center">4.12 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">409</td>
			<td class="red lasttd center">433</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10988199,0" class="icomment icommentjs icon16" href="/victor-vran-codex-t10988199.html#comment"> <em class="iconvalue">25</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/victor-vran-codex-t10988199.html" class="cellMainLink">Victor.Vran-CODEX</a></div>
			</td>
			<td class="nobr center">3.42 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">336</td>
			<td class="red lasttd center">431</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10989206,0" class="icomment icommentjs icon16" href="/metro-2033-redux-update-5-2014-pc-gog-t10989206.html#comment"> <em class="iconvalue">13</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/metro-2033-redux-update-5-2014-pc-gog-t10989206.html" class="cellMainLink">Metro 2033 - Redux [Update 5] (2014) PC - [GOG]</a></div>
			</td>
			<td class="nobr center">6.82 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">452</td>
			<td class="red lasttd center">266</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10989216,0" class="icomment icommentjs icon16" href="/metro-last-light-redux-update-5-2014-pc-gog-t10989216.html#comment"> <em class="iconvalue">14</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/metro-last-light-redux-update-5-2014-pc-gog-t10989216.html" class="cellMainLink">Metro: Last Light - Redux [Update 5] (2014) PC - [GOG]</a></div>
			</td>
			<td class="nobr center">8.14 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">194</td>
			<td class="red lasttd center">266</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10982709,0" class="icomment icommentjs icon16" href="/the-talos-principle-road-to-gehenna-reloaded-t10982709.html#comment"> <em class="iconvalue">21</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/the-talos-principle-road-to-gehenna-reloaded-t10982709.html" class="cellMainLink">The Talos Principle Road To Gehenna-RELOADED</a></div>
			</td>
			<td class="nobr center">10.8 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">159</td>
			<td class="red lasttd center">252</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10975822,0" class="icomment icommentjs icon16" href="/project-cars-v2-5-rus-eng-ger-repack-by-xatab-t10975822.html#comment"> <em class="iconvalue">14</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/project-cars-v2-5-rus-eng-ger-repack-by-xatab-t10975822.html" class="cellMainLink">Project CARS [v2.5] [RUS | ENG | GER] RePack by xatab</a></div>
			</td>
			<td class="nobr center">12.51 <span>GB</span></td>
			<td class="center">9</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">148</td>
			<td class="red lasttd center">98</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10978124,0" class="icomment icommentjs icon16" href="/f1-2015-black-box-t10978124.html#comment"> <em class="iconvalue">17</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/f1-2015-black-box-t10978124.html" class="cellMainLink">F1 2015-Black Box</a></div>
			</td>
			<td class="nobr center">7.4 <span>GB</span></td>
			<td class="center">10</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">61</td>
			<td class="red lasttd center">164</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10978396,0" class="icomment icommentjs icon16" href="/game-of-thrones-a-telltale-games-series-episodes-1-5-fitgirl-repack-t10978396.html#comment"> <em class="iconvalue">26</em><span></span> </a> 					<a class="istill icon16" href="/game-of-thrones-a-telltale-games-series-episodes-1-5-fitgirl-repack-t10978396.html#stills" title="Torrent Has Screenshots"><span></span></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/game-of-thrones-a-telltale-games-series-episodes-1-5-fitgirl-repack-t10978396.html" class="cellMainLink">Game of Thrones: A Telltale Games Series - Episodes 1-5 [FitGirl Repack]</a></div>
			</td>
			<td class="nobr center">4.38 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">121</td>
			<td class="red lasttd center">101</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10977001,0" class="icomment icommentjs icon16" href="/f1-2015-update-3-2015-pc-repack-Ð¾Ñ-r-g-steamgames-t10977001.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/f1-2015-update-3-2015-pc-repack-Ð¾Ñ-r-g-steamgames-t10977001.html" class="cellMainLink">F1 2015 [Update 3] (2015) PC | RePack Ð¾Ñ R.G. Steamgames</a></div>
			</td>
			<td class="nobr center">7.49 <span>GB</span></td>
			<td class="center">8</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">83</td>
			<td class="red lasttd center">76</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="rsssign" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10980679,0" class="icomment icommentjs icon16" href="/microsoft-office-2013-sp1-professional-plus-visio-pro-project-pro-15-0-4727-1001-repack-kpojiuk-32-64-bit-appzdam-t10980679.html#comment"> <em class="iconvalue">8</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/microsoft-office-2013-sp1-professional-plus-visio-pro-project-pro-15-0-4727-1001-repack-kpojiuk-32-64-bit-appzdam-t10980679.html" class="cellMainLink">Microsoft Office 2013 SP1 Professional Plus + Visio Pro + Project Pro 15.0.4727.1001 RePack(KpoJIuK) [32-64 bit] - AppzDam</a></div>
			</td>
			<td class="nobr center">5.5 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">208</td>
			<td class="red lasttd center">281</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10982663,0" class="icomment icommentjs icon16" href="/windows-10-enterprise-10-0-10240-rtm-32-64-bit-appzdam-t10982663.html#comment"> <em class="iconvalue">34</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-10-enterprise-10-0-10240-rtm-32-64-bit-appzdam-t10982663.html" class="cellMainLink">Windows 10 Enterprise 10.0.10240 RTM [32-64 bit] - AppzDam</a></div>
			</td>
			<td class="nobr center">6.38 <span>GB</span></td>
			<td class="center">12</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">176</td>
			<td class="red lasttd center">293</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10989203,0" class="icomment icommentjs icon16" href="/visual-studio-2015-enterprice-iso-serial-keys-fullstuff-t10989203.html#comment"> <em class="iconvalue">17</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/visual-studio-2015-enterprice-iso-serial-keys-fullstuff-t10989203.html" class="cellMainLink">Visual Studio 2015 Enterprice ISO + Serial Keys - [Fullstuff]</a></div>
			</td>
			<td class="nobr center">3.83 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">88</td>
			<td class="red lasttd center">301</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10975134,0" class="icomment icommentjs icon16" href="/k-lite-codec-pack-11-3-0-mega-full-standard-basic-appzdam-t10975134.html#comment"> <em class="iconvalue">13</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/k-lite-codec-pack-11-3-0-mega-full-standard-basic-appzdam-t10975134.html" class="cellMainLink">K-Lite Codec Pack 11.3.0 Mega / Full / Standard / Basic - AppzDam</a></div>
			</td>
			<td class="nobr center">113.22 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">289</td>
			<td class="red lasttd center">96</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10980656,0" class="icomment icommentjs icon16" href="/windows-10-pro-home-10-0-10240-sign-off-rtm-32-64bit-appzdam-t10980656.html#comment"> <em class="iconvalue">28</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-10-pro-home-10-0-10240-sign-off-rtm-32-64bit-appzdam-t10980656.html" class="cellMainLink">Windows 10 Pro-Home 10.0.10240 SIGN-OFF RTM [32-64bit] - AppzDam</a></div>
			</td>
			<td class="nobr center">6.65 <span>GB</span></td>
			<td class="center">12</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">132</td>
			<td class="red lasttd center">222</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10977321,0" class="icomment icommentjs icon16" href="/autodesk-suites-2016-autocad-design-building-design-entertainment-creation-design-factory-infrastructure-design-plant-design-product-design-t10977321.html#comment"> <em class="iconvalue">26</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/autodesk-suites-2016-autocad-design-building-design-entertainment-creation-design-factory-infrastructure-design-plant-design-product-design-t10977321.html" class="cellMainLink">Autodesk Suites - 2016 (AutoCAD Design | Building Design | Entertainment Creation | Design Factory | Infrastructure Design | Plant Design | Product Design)</a></div>
			</td>
			<td class="nobr center">129.22 <span>GB</span></td>
			<td class="center">73</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">8</td>
			<td class="red lasttd center">306</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10987337,0" class="icomment icommentjs icon16" href="/microsoft-visual-studio-2015-14-0-23107-0-original-image-msdn-eng-ru-installation-keys-32-64-bit-appzdam-t10987337.html#comment"> <em class="iconvalue">12</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/microsoft-visual-studio-2015-14-0-23107-0-original-image-msdn-eng-ru-installation-keys-32-64-bit-appzdam-t10987337.html" class="cellMainLink">Microsoft Visual Studio 2015 (14.0.23107.0) original image MSDN [Eng+Ru] + Installation Keys [32-64 bit] - AppzDam</a></div>
			</td>
			<td class="nobr center">17.13 <span>GB</span></td>
			<td class="center">43</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">55</td>
			<td class="red lasttd center">248</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10990638,0" class="icomment icommentjs icon16" href="/internet-download-manager-6-23-build-17-retail-incl-patch-team-os-t10990638.html#comment"> <em class="iconvalue">19</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/internet-download-manager-6-23-build-17-retail-incl-patch-team-os-t10990638.html" class="cellMainLink">Internet Download Manager 6.23 Build 17 Retail Incl Patch-=TEAM OS=</a></div>
			</td>
			<td class="nobr center">6.28 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">169</td>
			<td class="red lasttd center">31</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10976520,0" class="icomment icommentjs icon16" href="/wolfram-mathematica-10-2-0-0-multilanguage-keygen-appzdam-t10976520.html#comment"> <em class="iconvalue">18</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/wolfram-mathematica-10-2-0-0-multilanguage-keygen-appzdam-t10976520.html" class="cellMainLink">Wolfram Mathematica 10.2.0.0 Multilanguage + Keygen - AppzDam</a></div>
			</td>
			<td class="nobr center">2.18 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">104</td>
			<td class="red lasttd center">80</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10984152,0" class="icomment icommentjs icon16" href="/windows-7-x64-12in1-oem-esd-en-us-juli-2015-generation2-t10984152.html#comment"> <em class="iconvalue">10</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-7-x64-12in1-oem-esd-en-us-juli-2015-generation2-t10984152.html" class="cellMainLink">Windows 7 X64 12in1 OEM ESD en-US Juli 2015 {Generation2}</a></div>
			</td>
			<td class="nobr center">3.11 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">63</td>
			<td class="red lasttd center">76</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10980746,0" class="icomment icommentjs icon16" href="/windows-10-pro-build-10240-16384-x64-cn-ru-es-de-by-white-death-team-os-t10980746.html#comment"> <em class="iconvalue">13</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-10-pro-build-10240-16384-x64-cn-ru-es-de-by-white-death-team-os-t10980746.html" class="cellMainLink">Windows 10 Pro BUILD-10240.16384-X64 CN-RU-ES-DE By White death-=TEAM OS=-</a></div>
			</td>
			<td class="nobr center">15.07 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">10</td>
			<td class="red lasttd center">80</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10987433,0" class="icomment icommentjs icon16" href="/windows-7-aio-16in1-esd-pt-br-july-2015-generation2-t10987433.html#comment"> <em class="iconvalue">9</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-7-aio-16in1-esd-pt-br-july-2015-generation2-t10987433.html" class="cellMainLink">Windows 7 AIO 16in1 ESD pt-BR July 2015 {Generation2}</a></div>
			</td>
			<td class="nobr center">4.29 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">11</td>
			<td class="red lasttd center">79</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/ashampoo-burning-studio-business-15-0-4-2-5010-multilanguage-crack-at-team-t10983817.html" class="cellMainLink">Ashampoo Burning Studio Business 15.0.4.2 (5010) [Multilanguage] [Crack] [AT-TEAM]</a></div>
			</td>
			<td class="nobr center">146.02 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">60</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10979514,0" class="icomment icommentjs icon16" href="/microsoft-windows-10-pro-core-rtm-10240-x86-oemret-english-dvd-by-whitedeath-teamos-original-t10979514.html#comment"> <em class="iconvalue">12</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/microsoft-windows-10-pro-core-rtm-10240-x86-oemret-english-dvd-by-whitedeath-teamos-original-t10979514.html" class="cellMainLink">MICROSOFT WINDOWS 10 PRO-CORE RTM 10240 X86 OEMRET ENGLISH DVD by:WhiteDeath[TeamOS]Original</a></div>
			</td>
			<td class="nobr center">2.84 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">39</td>
			<td class="red lasttd center">45</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10994531,0" class="icomment icommentjs icon16" href="/unity-asset-chronos-time-control-v2-0-2-akd-t10994531.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/unity-asset-chronos-time-control-v2-0-2-akd-t10994531.html" class="cellMainLink">Unity Asset - Chronos - Time Control v2.0.2[AKD]</a></div>
			</td>
			<td class="nobr center">5.91 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center">7&nbsp;hours</td>
			<td class="green center">0</td>
			<td class="red lasttd center">49</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="rsssign" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10985165,0" class="icomment icommentjs icon16" href="/horriblesubs-ranpo-kitan-game-of-laplace-04-720p-mkv-t10985165.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-ranpo-kitan-game-of-laplace-04-720p-mkv-t10985165.html" class="cellMainLink">[HorribleSubs] Ranpo Kitan - Game of Laplace - 04 [720p].mkv</a></div>
			</td>
			<td class="nobr center">405.6 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">1146</td>
			<td class="red lasttd center">435</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10984949,0" class="icomment icommentjs icon16" href="/fansub-resistance-naruto-shippuuden-420-french-subbed-1280x720-mp4-t10984949.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fansub-resistance-naruto-shippuuden-420-french-subbed-1280x720-mp4-t10984949.html" class="cellMainLink">[Fansub-Resistance]Naruto Shippuuden 420 [FRENCH Subbed] (1280x720).mp4</a></div>
			</td>
			<td class="nobr center">217.35 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">1297</td>
			<td class="red lasttd center">120</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-gate-jieitai-kanochi-nite-kaku-tatakaeri-04-raw-mx-1280x720-x264-aac-mp4-t10990599.html" class="cellMainLink">[Leopard-Raws] Gate - Jieitai Kanochi nite, Kaku Tatakaeri - 04 RAW (MX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">437.47 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">870</td>
			<td class="red lasttd center">413</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-ranpo-kitan-game-of-laplace-04-raw-cx-1280x720-x264-aac-mp4-t10987313.html" class="cellMainLink">[Leopard-Raws] Ranpo Kitan - Game of Laplace - 04 RAW (CX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">416.47 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">925</td>
			<td class="red lasttd center">167</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-joukamachi-no-dandelion-04-raw-tbs-1280x720-x264-aac-mp4-t10987314.html" class="cellMainLink">[Leopard-Raws] Joukamachi no Dandelion - 04 RAW (TBS 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">293.19 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">854</td>
			<td class="red lasttd center">147</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10992905,0" class="icomment icommentjs icon16" href="/leopard-raws-shokugeki-no-souma-16-raw-tbs-1280x720-x264-aac-mp4-t10992905.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-shokugeki-no-souma-16-raw-tbs-1280x720-x264-aac-mp4-t10992905.html" class="cellMainLink">[Leopard-Raws] Shokugeki no Souma - 16 RAW (TBS 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">503.26 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">15&nbsp;hours</td>
			<td class="green center">618</td>
			<td class="red lasttd center">379</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-idolmaster-cinderella-girls-2nd-02-raw-bs11-1280x720-x264-aac-mp4-t10990405.html" class="cellMainLink">[Leopard-Raws] Idolmaster - Cinderella Girls 2nd - 02 RAW (BS11 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">263.89 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">768</td>
			<td class="red lasttd center">228</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-fate-kaleid-liner-prisma-illya-zwei-herz-01-raw-mx-1280x720-x264-aac-mp4-t10992908.html" class="cellMainLink">[Leopard-Raws] Fate - Kaleid Liner Prisma Illya Zwei Herz! - 01 RAW (MX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">389.51 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">15&nbsp;hours</td>
			<td class="green center">570</td>
			<td class="red lasttd center">313</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-okusama-ga-seito-kaichou-04-raw-atx-1280x720-x264-aac-mp4-t10978619.html" class="cellMainLink">[Leopard-Raws] Okusama ga Seito Kaichou! - 04 RAW (ATX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">136.39 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">682</td>
			<td class="red lasttd center">159</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-himouto-umaru-chan-03-raw-abc-1280x720-x264-aac-mp4-t10981307.html" class="cellMainLink">[Leopard-Raws] Himouto! Umaru-chan - 03 RAW (ABC 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">331.74 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">553</td>
			<td class="red lasttd center">261</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-classroom-crisis-04-raw-tbs-1280x720-x264-aac-mp4-t10992907.html" class="cellMainLink">[Leopard-Raws] Classroom Crisis - 04 RAW (TBS 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">306.09 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">15&nbsp;hours</td>
			<td class="green center">504</td>
			<td class="red lasttd center">303</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10982639,0" class="icomment icommentjs icon16" href="/naruto-shippuden-420-eng-sub-480p-l-mbert-t10982639.html#comment"> <em class="iconvalue">36</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/naruto-shippuden-420-eng-sub-480p-l-mbert-t10982639.html" class="cellMainLink">Naruto Shippuden 420 [EnG SuB] 480p L@mBerT</a></div>
			</td>
			<td class="nobr center">61.57 <span>MB</span></td>
			<td class="center">9</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">456</td>
			<td class="red lasttd center">138</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-aquarion-logos-04-raw-kbs-1280x720-x264-aac-mp4-t10984436.html" class="cellMainLink">[Leopard-Raws] Aquarion Logos - 04 RAW (KBS 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">507.13 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">377</td>
			<td class="red lasttd center">122</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10987463,0" class="icomment icommentjs icon16" href="/cbm-no-game-no-life-1-12-sp-complete-dual-audio-bdrip-720p-8bit-t10987463.html#comment"> <em class="iconvalue">12</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/cbm-no-game-no-life-1-12-sp-complete-dual-audio-bdrip-720p-8bit-t10987463.html" class="cellMainLink">[CBM] No Game No Life 1-12+SP Complete (Dual Audio) [BDRip 720p 8bit]</a></div>
			</td>
			<td class="nobr center">7.88 <span>GB</span></td>
			<td class="center">19</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">126</td>
			<td class="red lasttd center">209</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fff-highschool-dxd-born-11-5fc44d4e-mkv-t10994279.html" class="cellMainLink">[FFF] Highschool DxD BorN - 11 [5FC44D4E].mkv</a></div>
			</td>
			<td class="nobr center">538.76 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">8&nbsp;hours</td>
			<td class="green center">22</td>
			<td class="red lasttd center">188</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="rsssign" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10979547,0" class="icomment icommentjs icon16" href="/marvel-week-07-22-2015-nem-t10979547.html#comment"> <em class="iconvalue">41</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/marvel-week-07-22-2015-nem-t10979547.html" class="cellMainLink">Marvel Week+ (07-22-2015) (- Nem -)</a></div>
			</td>
			<td class="nobr center">481.18 <span>MB</span></td>
			<td class="center">14</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">737</td>
			<td class="red lasttd center">244</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10986718,0" class="icomment icommentjs icon16" href="/new-ebook-packs-for-april-may-june-2015-t10986718.html#comment"> <em class="iconvalue">18</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/new-ebook-packs-for-april-may-june-2015-t10986718.html" class="cellMainLink">New ebook packs for April, May &amp; June 2015</a></div>
			</td>
			<td class="nobr center">22.28 <span>GB</span></td>
			<td class="center">120</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">178</td>
			<td class="red lasttd center">686</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10977782,0" class="icomment icommentjs icon16" href="/dc-week-07-22-2015-vertigo-aka-dc-you-week-08-nem-t10977782.html#comment"> <em class="iconvalue">36</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/dc-week-07-22-2015-vertigo-aka-dc-you-week-08-nem-t10977782.html" class="cellMainLink">DC Week+ (07-22-2015) (+ Vertigo) (aka DC YOU Week 08) (- Nem -)</a></div>
			</td>
			<td class="nobr center">515.97 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">558</td>
			<td class="red lasttd center">206</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10983648,0" class="icomment icommentjs icon16" href="/100-skills-you-ll-need-for-the-end-of-the-world-as-we-know-it-gnv64-t10983648.html#comment"> <em class="iconvalue">16</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/100-skills-you-ll-need-for-the-end-of-the-world-as-we-know-it-gnv64-t10983648.html" class="cellMainLink">100 Skills You&#039;ll Need for the End of the World (as We Know It)(gnv64)</a></div>
			</td>
			<td class="nobr center">22.09 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">541</td>
			<td class="red lasttd center">37</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10982000,0" class="icomment icommentjs icon16" href="/assorted-magazines-bundle-july-23-2015-true-pdf-t10982000.html#comment"> <em class="iconvalue">17</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/assorted-magazines-bundle-july-23-2015-true-pdf-t10982000.html" class="cellMainLink">Assorted Magazines Bundle - July 23 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center">286.35 <span>MB</span></td>
			<td class="center">13</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">332</td>
			<td class="red lasttd center">134</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10981503,0" class="icomment icommentjs icon16" href="/computer-gadget-gamer-magzines-july-23-2015-true-pdf-t10981503.html#comment"> <em class="iconvalue">9</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/computer-gadget-gamer-magzines-july-23-2015-true-pdf-t10981503.html" class="cellMainLink">Computer Gadget &amp; Gamer Magzines - July 23 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center">226.59 <span>MB</span></td>
			<td class="center">12</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">349</td>
			<td class="red lasttd center">99</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10977750,0" class="icomment icommentjs icon16" href="/old-man-logan-003-2015-digital-empire-cbr-t10977750.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/old-man-logan-003-2015-digital-empire-cbr-t10977750.html" class="cellMainLink">Old Man Logan 003 (2015) (Digital-Empire).cbr</a></div>
			</td>
			<td class="nobr center">32.95 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">370</td>
			<td class="red lasttd center">38</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10987816,0" class="icomment icommentjs icon16" href="/100-books-by-james-patterson-epub-t10987816.html#comment"> <em class="iconvalue">11</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/100-books-by-james-patterson-epub-t10987816.html" class="cellMainLink">100 Books by James Patterson (ePUB)</a></div>
			</td>
			<td class="nobr center">156.79 <span>MB</span></td>
			<td class="center">103</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">262</td>
			<td class="red lasttd center">41</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10992798,0" class="icomment icommentjs icon16" href="/athletic-sports-magazines-bundle-july-25-2015-true-pdf-t10992798.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/athletic-sports-magazines-bundle-july-25-2015-true-pdf-t10992798.html" class="cellMainLink">Athletic Sports Magazines Bundle - July 25 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center">443 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center">15&nbsp;hours</td>
			<td class="green center">141</td>
			<td class="red lasttd center">88</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10993250,0" class="icomment icommentjs icon16" href="/encyclopedia-of-caves-1st-2nd-edition-2004-2012-pdf-gooner-t10993250.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/encyclopedia-of-caves-1st-2nd-edition-2004-2012-pdf-gooner-t10993250.html" class="cellMainLink">Encyclopedia of Caves - 1st &amp; 2nd Edition (2004 &amp; 2012) Pdf Gooner</a></div>
			</td>
			<td class="nobr center">201.7 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center">13&nbsp;hours</td>
			<td class="green center">138</td>
			<td class="red lasttd center">62</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10976039,0" class="icomment icommentjs icon16" href="/0-day-week-of-2015-07-15-t10976039.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/0-day-week-of-2015-07-15-t10976039.html" class="cellMainLink">0-Day Week of 2015.07.15</a></div>
			</td>
			<td class="nobr center">6.18 <span>GB</span></td>
			<td class="center">140</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">89</td>
			<td class="red lasttd center">99</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10988143,0" class="icomment icommentjs icon16" href="/hulk-v2-001-067-variants-2008-2013-digital-empire-nem-t10988143.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/hulk-v2-001-067-variants-2008-2013-digital-empire-nem-t10988143.html" class="cellMainLink">Hulk v2 (001-067+Variants) (2008-2013) (digital) (Empire) (- Nem -)</a></div>
			</td>
			<td class="nobr center">2.22 <span>GB</span></td>
			<td class="center">117</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">104</td>
			<td class="red lasttd center">52</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10988772,0" class="icomment icommentjs icon16" href="/the-encyclopedia-of-cell-technology-1st-edition-2-volume-set-2000-pdf-gooner-t10988772.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-encyclopedia-of-cell-technology-1st-edition-2-volume-set-2000-pdf-gooner-t10988772.html" class="cellMainLink">The Encyclopedia of Cell Technology - 1st Edition (2 Volume Set) (2000).pdf Gooner</a></div>
			</td>
			<td class="nobr center">143.92 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">82</td>
			<td class="red lasttd center">53</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10976042,0" class="icomment icommentjs icon16" href="/hitlist-week-of-2015-07-15-t10976042.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/hitlist-week-of-2015-07-15-t10976042.html" class="cellMainLink">Hitlist Week of 2015.07.15</a></div>
			</td>
			<td class="nobr center">22.14 <span>GB</span></td>
			<td class="center">338</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">17</td>
			<td class="red lasttd center">61</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10979194,0" class="icomment icommentjs icon16" href="/zombie-tramp-vs-vampblade-001-2015-digital-thearchivist-empire-cbr-t10979194.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/zombie-tramp-vs-vampblade-001-2015-digital-thearchivist-empire-cbr-t10979194.html" class="cellMainLink">Zombie Tramp vs. Vampblade 001 (2015) (Digital) (TheArchivist-Empire).cbr</a></div>
			</td>
			<td class="nobr center">33.89 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">2</td>
			<td class="red lasttd center">37</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="rsssign" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10990068,0" class="icomment icommentjs icon16" href="/roger-waters-amused-to-death-2015-remastered-flac-beolab1700-t10990068.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/roger-waters-amused-to-death-2015-remastered-flac-beolab1700-t10990068.html" class="cellMainLink">Roger Waters - Amused To Death (2015) Remastered FLAC Beolab1700</a></div>
			</td>
			<td class="nobr center">362.06 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">234</td>
			<td class="red lasttd center">69</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10981789,0" class="icomment icommentjs icon16" href="/creedence-clearwater-revival-chronicle-vol-1-2-1986-flac-24-96-flac-t10981789.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/creedence-clearwater-revival-chronicle-vol-1-2-1986-flac-24-96-flac-t10981789.html" class="cellMainLink">Creedence Clearwater Revival - Chronicle Vol 1,2 (1986) FLAC 24 96 FLAC</a></div>
			</td>
			<td class="nobr center">2.73 <span>GB</span></td>
			<td class="center">43</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">171</td>
			<td class="red lasttd center">96</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10979542,0" class="icomment icommentjs icon16" href="/herb-alpert-sergio-mendes-brasil-66-very-best-of-flac-vtwin88cube-t10979542.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/herb-alpert-sergio-mendes-brasil-66-very-best-of-flac-vtwin88cube-t10979542.html" class="cellMainLink">Herb Alpert + Sergio Mendes &amp; Brasil &#039;66 Very Best Of {FLAC} vtwin88cube</a></div>
			</td>
			<td class="nobr center">592.13 <span>MB</span></td>
			<td class="center">43</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">192</td>
			<td class="red lasttd center">36</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/gloria-gaynor-greatest-hits-2015-flac-t10987639.html" class="cellMainLink">Gloria Gaynor Greatest Hits (2015) FLAC</a></div>
			</td>
			<td class="nobr center">655.67 <span>MB</span></td>
			<td class="center">26</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">187</td>
			<td class="red lasttd center">33</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10994238,0" class="icomment icommentjs icon16" href="/warren-haynes-ashes-dust-deluxe-edition-2015-96-24-hdtracks-flac-t10994238.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/warren-haynes-ashes-dust-deluxe-edition-2015-96-24-hdtracks-flac-t10994238.html" class="cellMainLink">Warren Haynes - Ashes &amp; Dust (Deluxe Edition 2015) [96-24 HDtracks FLAC]</a></div>
			</td>
			<td class="nobr center">2.23 <span>GB</span></td>
			<td class="center">39</td>
			<td class="center">8&nbsp;hours</td>
			<td class="green center">126</td>
			<td class="red lasttd center">73</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10993904,0" class="icomment icommentjs icon16" href="/va-my-favourite-hits-of-1977-16cd-2015-flac-Ð¾Ñ-don-music-t10993904.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-my-favourite-hits-of-1977-16cd-2015-flac-Ð¾Ñ-don-music-t10993904.html" class="cellMainLink">VA - My Favourite Hits of 1977 [16CD] (2015) FLAC Ð¾Ñ DON Music</a></div>
			</td>
			<td class="nobr center">7.56 <span>GB</span></td>
			<td class="center">247</td>
			<td class="center">10&nbsp;hours</td>
			<td class="green center">50</td>
			<td class="red lasttd center">141</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/tom-petty-the-heartbreakers-southern-accents-in-the-sunshine-state-2015-flac-t10993659.html" class="cellMainLink">Tom Petty &amp; The Heartbreakers - Southern Accents In The Sunshine State (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center">797.12 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center">11&nbsp;hours</td>
			<td class="green center">120</td>
			<td class="red lasttd center">67</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10988999,0" class="icomment icommentjs icon16" href="/herb-alpert-definitive-hits-2001-flac-vtwin88cube-t10988999.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/herb-alpert-definitive-hits-2001-flac-vtwin88cube-t10988999.html" class="cellMainLink">Herb Alpert - Definitive Hits (2001) {FLAC} vtwin88cube</a></div>
			</td>
			<td class="nobr center">431.26 <span>MB</span></td>
			<td class="center">26</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">123</td>
			<td class="red lasttd center">40</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10987749,0" class="icomment icommentjs icon16" href="/sly-the-family-stone-live-at-the-fillmore-east-1968-box-set-2015-flac-beolab1700-t10987749.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/sly-the-family-stone-live-at-the-fillmore-east-1968-box-set-2015-flac-beolab1700-t10987749.html" class="cellMainLink">Sly &amp; the Family Stone - Live at the Fillmore East 1968 [Box Set] (2015) FLAC Beolab1700</a></div>
			</td>
			<td class="nobr center">1.29 <span>GB</span></td>
			<td class="center">86</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">116</td>
			<td class="red lasttd center">45</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10981814,0" class="icomment icommentjs icon16" href="/va-nrj-party-hits-2015-2cd-2015-flac-t10981814.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-nrj-party-hits-2015-2cd-2015-flac-t10981814.html" class="cellMainLink">VA - NRJ Party Hits 2015 [2CD] (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center">1.03 <span>GB</span></td>
			<td class="center">56</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">94</td>
			<td class="red lasttd center">30</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10995700,0" class="icomment icommentjs icon16" href="/frankie-goes-to-hollywood-simply-hits-tracks-remixes-3cd-box-2015-flac-t10995700.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/frankie-goes-to-hollywood-simply-hits-tracks-remixes-3cd-box-2015-flac-t10995700.html" class="cellMainLink">Frankie Goes To Hollywood - Simply - Hits, Tracks &amp; Remixes (3CD-Box 2015) [FLAC]</a></div>
			</td>
			<td class="nobr center">1.3 <span>GB</span></td>
			<td class="center">81</td>
			<td class="center">3&nbsp;hours</td>
			<td class="green center">63</td>
			<td class="red lasttd center">59</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/joe-cocker-2006-joe-cocker-gold-2cd-eac-flac-t10993833.html" class="cellMainLink">Joe Cocker - 2006 - Joe Cocker Gold [2CD] [EAC FLAC]</a></div>
			</td>
			<td class="nobr center">991.38 <span>MB</span></td>
			<td class="center">61</td>
			<td class="center">10&nbsp;hours</td>
			<td class="green center">60</td>
			<td class="red lasttd center">28</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/sergio-mendes-brasil-66-foursider-1988-flac-vtwin88cube-t10989461.html" class="cellMainLink">Sergio Mendes &amp; Brasil &#039;66 - Foursider (1988) {FLAC} vtwin88cube</a></div>
			</td>
			<td class="nobr center">411.09 <span>MB</span></td>
			<td class="center">29</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">46</td>
			<td class="red lasttd center">33</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10990320,0" class="icomment icommentjs icon16" href="/exposÃ©-greatest-hits-flac-mp3-big-papi-80-s-90-s-pop-t10990320.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/exposÃ©-greatest-hits-flac-mp3-big-papi-80-s-90-s-pop-t10990320.html" class="cellMainLink">ExposÃ© - Greatest Hits [FLAC+MP3](Big Papi) 80&#039;s 90&#039;s Pop</a></div>
			</td>
			<td class="nobr center">564.84 <span>MB</span></td>
			<td class="center">36</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">40</td>
			<td class="red lasttd center">36</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/ccr-1969-bayou-country-sacd-24-88-flac-t10995413.html" class="cellMainLink">CCR 1969 Bayou Country (SACD) [24-88 FLAC]</a></div>
			</td>
			<td class="nobr center">643.73 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center">5&nbsp;hours</td>
			<td class="green center">1</td>
			<td class="red lasttd center">29</td>
        </tr>
			</table>


		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
                
        <div id="_119b0a17fab5493361a252d04bf527db"></div>
    
                
    	    <div class="advertising">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <div id="_7063408f1c01d50e0dc2d833186ce962"></div>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/what-are-you-listening-right-now-v4-thread-108003/?unread=16704607">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What are you listening to right now? V4
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/xxoxxo/">xxoxxo</a></span></span> 1&nbsp;min.&nbsp;ago</span>
	</li>
		<li>
		<a href="/community/show/stitchinwitch-s-upload-page/?unread=16704603">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				StitchinWitch&#039;s upload page
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_verified"><a class="plain" href="/user/StitchinWitch/">StitchinWitch</a></span></span> 3&nbsp;min.&nbsp;ago</span>
	</li>
		<li>
		<a href="/community/show/what-tv-show-are-you-watching-right-now-v3/?unread=16704600">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What TV show are you watching right now? V3
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_3"><a class="plain" href="/user/5fingerdis/">5fingerdis</a></span></span> 5&nbsp;min.&nbsp;ago</span>
	</li>
		<li>
		<a href="/community/show/please-request-ebooks-and-audio-books-here-v11/?unread=16704597">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Please request ebooks and audio books here. V11
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/MacemfWindu/">MacemfWindu</a></span></span> 6&nbsp;min.&nbsp;ago</span>
	</li>
		<li>
		<a href="/community/show/latest-book-uploads-v2/?unread=16704585">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Latest Book Uploads V2
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_verified"><a class="plain" href="/user/felix56/">felix56</a></span></span> 11&nbsp;min.&nbsp;ago</span>
	</li>
		<li>
		<a href="/community/show/what-movie-are-you-watching-right-now/?unread=16704583">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What Movie are you watching right now?
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/smurfettelee/">smurfettelee</a></span></span> 12&nbsp;min.&nbsp;ago</span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/kickasstorrents-is-moving-to-kat-cr-domain/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				KickassTorrents is moving to kat.cr domain
			</p>
		</a>
		<span class="explanation">by KickassTorrents 3&nbsp;months&nbsp;ago</span>
	</li>
	<li>
		<a href="/blog/post/happy-torrents-day-2015/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Happy Torrents Day 2015!
			</p>
		</a>
		<span class="explanation">by KickassTorrents 3&nbsp;months&nbsp;ago</span>
	</li>
	<li>
		<a href="/blog/post/technical-maintenance-march-17/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Technical maintenance (March, 17)
			</p>
		</a>
		<span class="explanation">by KickassTorrents 4&nbsp;months&nbsp;ago</span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/Touro73/post/new-movie-and-blu-ray-releases-week-31-july-27-august-2/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> New Movie and Blu-ray releases Week 31 (July 27 - August 2)</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/Touro73/">Touro73</a> 6&nbsp;hours&nbsp;ago</span></li>
	<li><a href="/blog/F1_Dance/post/vpn-check-proxy-check-mail-encryption-uhm/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> VPN? Check. Proxy? Check. Mail Encryption? Uhmâ¦</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/F1_Dance/">F1_Dance</a> 18&nbsp;hours&nbsp;ago</span></li>
	<li><a href="/blog/FreExpressn/post/about-my-torrent-uploads/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> About my .torrent uploads</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/FreExpressn/">FreExpressn</a> 20&nbsp;hours&nbsp;ago</span></li>
	<li><a href="/blog/Pringlescan/post/pringlescan-anime-recommendations-vol-34-rurouni-kenshin-reflection-ova/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> Pringlescan Anime Recommendations Vol. 34 Rurouni Kenshin: Reflection OVA</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/Pringlescan/">Pringlescan</a> yesterday</span></li>
	<li><a href="/blog/Plain.Thief/post/i-will-miss-you-old-friend/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> I will miss you old friend</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/Plain.Thief/">Plain.Thief</a> yesterday</span></li>
	<li><a href="/blog/Star.Sapphire/post/the-web-part-7/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> The Web: Part 7</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/Star.Sapphire/">Star.Sapphire</a> yesterday</span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/crash%20test%20dummies%20songs%20of%20the%20unforgiven/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				crash test dummies songs of the unforgiven
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/klaudia.kelly/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				klaudia.kelly
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/the%20dark/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				the dark
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/pooty%20tang/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				pooty tang
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/d%27sou/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				d&#039;sou
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/boondock%20saints%202/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				boondock saints 2
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/rocky%20in%20dual%20audio/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				rocky in dual audio
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/dual%20audio%20eng%20hindi%202015/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				dual audio eng hindi 2015
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/t4a%27/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				t4a&#039;
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/windows%20vista%20home%20premium/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				windows vista home premium
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/allkindsofgirl.com/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				allkindsofgirl.com
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		<li>
			<a data-nop href="http://www.torrentreactor.net/" target="_blank" rel="external">
				<span class="itorreact thirdPartIcons"></span>TorrentReactor
			</a>
		</li>
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
		<li><a href="/welcome/">welcome</a></li>
		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <div id="_673e31f53f8166159b8e996c4124765b"></div>
        <div id="_e7050fb15fd39b3e4e99a5be4a57b6ea"></div>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script><script>
 sc('addGlobal', 'pagetype', 'front');
 sc('addSlot', '_60318cd4e8d28f6fb76fe34e9bd9c498');
sc('addSlot', '_39ecb76dd457e5ac33776fdf11500d56');
sc('addSlot', '_277923e5f9d753c5b0630c28e641790c');
sc('addSlot', '_119b0a17fab5493361a252d04bf527db');
sc('addSlot', '_7063408f1c01d50e0dc2d833186ce962', { 'searchQuery': '' });
sc('addSlot', '_673e31f53f8166159b8e996c4124765b');
sc('addSlot', '_e7050fb15fd39b3e4e99a5be4a57b6ea');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>
</body>
</html>
