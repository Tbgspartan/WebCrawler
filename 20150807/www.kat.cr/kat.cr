<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-7635503.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-7635503.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-7635503.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-7635503.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kat.cr/sc-7635503.js');

        sc('setHost', 'a.kat.cr');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: '7635503',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-7635503.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<div id="_60318cd4e8d28f6fb76fe34e9bd9c498"></div>
<div id="_39ecb76dd457e5ac33776fdf11500d56"></div>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom" style="color: #ffeeb4; font-size: 34px; float:left; height: 50px; line-height: 50px;"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka">e</i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka">g</i></button></div>
			</form>
		</div>
        <div id="_277923e5f9d753c5b0630c28e641790c"></div>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080p/" class="tag5">1080p</a>
	<a href="/search/2014/" class="tag5">2014</a>
	<a href="/search/2015/" class="tag10">2015</a>
	<a href="/search/3d/" class="tag3">3d</a>
	<a href="/search/3d%20remux/" class="tag2">3d remux</a>
	<a href="/search/android/" class="tag3">android</a>
	<a href="/search/ant%20man/" class="tag2">ant man</a>
	<a href="/search/arrow/" class="tag2">arrow</a>
	<a href="/search/avengers/" class="tag2">avengers</a>
	<a href="/search/avengers%20age%20of%20ultron/" class="tag2">avengers age of ultron</a>
	<a href="/search/baahubali/" class="tag4">baahubali</a>
	<a href="/search/bajrangi%20bhaijaan/" class="tag2">bajrangi bhaijaan</a>
	<a href="/search/batman/" class="tag2">batman</a>
	<a href="/search/discography/" class="tag3">discography</a>
	<a href="/search/drishyam/" class="tag3">drishyam</a>
	<a href="/search/flac/" class="tag3">flac</a>
	<a href="/search/french/" class="tag3">french</a>
	<a href="/search/game%20of%20thrones/" class="tag3">game of thrones</a>
	<a href="/search/hamari%20adhuri%20kahani/" class="tag2">hamari adhuri kahani</a>
	<a href="/search/hindi/" class="tag9">hindi</a>
	<a href="/search/hindi%202015/" class="tag3">hindi 2015</a>
	<a href="/search/humans/" class="tag2">humans</a>
	<a href="/search/inside%20out/" class="tag2">inside out</a>
	<a href="/search/insurgent/" class="tag2">insurgent</a>
	<a href="/search/ita/" class="tag3">ita</a>
	<a href="/search/kat%20ph%20com/" class="tag7">kat ph com</a>
	<a href="/search/mad%20max/" class="tag2">mad max</a>
	<a href="/search/malayalam/" class="tag3">malayalam</a>
	<a href="/search/minions/" class="tag2">minions</a>
	<a href="/search/movies/" class="tag3">movies</a>
	<a href="/search/mr%20robot/" class="tag2">mr robot</a>
	<a href="/search/nezu/" class="tag10">nezu</a>
	<a href="/search/nl/" class="tag3">nl</a>
	<a href="/search/pitch%20perfect%202/" class="tag2">pitch perfect 2</a>
	<a href="/search/pretty%20little%20liars/" class="tag2">pretty little liars</a>
	<a href="/search/ripsalot/" class="tag4">ripsalot</a>
	<a href="/search/san%20andreas/" class="tag2">san andreas</a>
	<a href="/search/tamil/" class="tag4">tamil</a>
	<a href="/search/ted%202/" class="tag2">ted 2</a>
	<a href="/search/telugu/" class="tag3">telugu</a>
	<a href="/search/the%20big%20bang%20theory/" class="tag5">the big bang theory</a>
	<a href="/search/the%20flash/" class="tag2">the flash</a>
	<a href="/search/the%20walking%20dead/" class="tag3">the walking dead</a>
	<a href="/search/true%20detective/" class="tag2">true detective</a>
	<a href="/search/windows%2010/" class="tag2">windows 10</a>
	<a href="/search/yify/" class="tag10">yify</a>
	<a href="/search/yify%201080p/" class="tag4">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag4">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="rsssign" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11042226,0" class="icomment icommentjs icon16" href="/pitch-perfect-2-2015-1080p-brrip-x264-yify-t11042226.html#comment"> <em class="iconvalue">42</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/pitch-perfect-2-2015-1080p-brrip-x264-yify-t11042226.html" class="cellMainLink">Pitch Perfect 2 (2015) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="1984303147">1.85 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="04 Aug 2015, 11:05">2&nbsp;days</span></td>
			<td class="green center">10237</td>
			<td class="red lasttd center">10249</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11046544,0" class="icomment icommentjs icon16" href="/the-age-of-adaline-2015-1080p-brrip-x264-yify-t11046544.html#comment"> <em class="iconvalue">30</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-age-of-adaline-2015-1080p-brrip-x264-yify-t11046544.html" class="cellMainLink">The Age of Adaline (2015) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="1770379765">1.65 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="05 Aug 2015, 08:27">1&nbsp;day</span></td>
			<td class="green center">9539</td>
			<td class="red lasttd center">9662</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11051075,0" class="icomment icommentjs icon16" href="/the-runner-2015-bdrip-xvid-ac3-evo-t11051075.html#comment"> <em class="iconvalue">40</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-runner-2015-bdrip-xvid-ac3-evo-t11051075.html" class="cellMainLink">The Runner 2015 BDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1490313975">1.39 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="06 Aug 2015, 05:05">18&nbsp;hours</span></td>
			<td class="green center">7630</td>
			<td class="red lasttd center">10699</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11043190,0" class="icomment icommentjs icon16" href="/dead-rising-watchtower-2015-720p-brrip-x264-yify-t11043190.html#comment"> <em class="iconvalue">45</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dead-rising-watchtower-2015-720p-brrip-x264-yify-t11043190.html" class="cellMainLink">Dead Rising: Watchtower (2015) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="907588249">865.54 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="04 Aug 2015, 16:19">2&nbsp;days</span></td>
			<td class="green center">7377</td>
			<td class="red lasttd center">5966</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11049904,0" class="icomment icommentjs icon16" href="/vendetta-2015-720p-brrip-x264-yify-t11049904.html#comment"> <em class="iconvalue">24</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/vendetta-2015-720p-brrip-x264-yify-t11049904.html" class="cellMainLink">Vendetta (2015) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="733040271">699.08 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="05 Aug 2015, 22:46">1&nbsp;day</span></td>
			<td class="green center">7217</td>
			<td class="red lasttd center">5724</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11042476,0" class="icomment icommentjs icon16" href="/dark-moon-rising-2015-hdrip-xvid-etrg-t11042476.html#comment"> <em class="iconvalue">54</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dark-moon-rising-2015-hdrip-xvid-etrg-t11042476.html" class="cellMainLink">Dark Moon Rising 2015 HDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="747408751">712.78 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="04 Aug 2015, 12:14">2&nbsp;days</span></td>
			<td class="green center">4410</td>
			<td class="red lasttd center">2718</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11052747,0" class="icomment icommentjs icon16" href="/johan-falk-17-slutet-2015-swedish-720p-web-dl-x264-p2p-t11052747.html#comment"> <em class="iconvalue">7</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/johan-falk-17-slutet-2015-swedish-720p-web-dl-x264-p2p-t11052747.html" class="cellMainLink">Johan Falk 17 Slutet 2015 SWEDiSH 720p WEB-DL x264-P2P</a></div>
			</td>
			<td class="nobr center" data-sort="1873895194">1.75 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="06 Aug 2015, 14:20">9&nbsp;hours</span></td>
			<td class="green center">3135</td>
			<td class="red lasttd center">2724</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11046928,0" class="icomment icommentjs icon16" href="/that-sugar-film-2014-720p-brrip-x264-yify-t11046928.html#comment"> <em class="iconvalue">29</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/that-sugar-film-2014-720p-brrip-x264-yify-t11046928.html" class="cellMainLink">That Sugar Film (2014) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="847341595">808.09 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="05 Aug 2015, 10:12">1&nbsp;day</span></td>
			<td class="green center">3221</td>
			<td class="red lasttd center">2546</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11045917,0" class="icomment icommentjs icon16" href="/ant-man-2015-new-hdts-xvid-ac3-hq-hive-cm8-t11045917.html#comment"> <em class="iconvalue">24</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ant-man-2015-new-hdts-xvid-ac3-hq-hive-cm8-t11045917.html" class="cellMainLink">Ant-Man 2015 NEW HDTS XVID AC3 HQ Hive-CM8</a></div>
			</td>
			<td class="nobr center" data-sort="1711370386">1.59 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="05 Aug 2015, 04:49">1&nbsp;day</span></td>
			<td class="green center">1939</td>
			<td class="red lasttd center">3370</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11044255,0" class="icomment icommentjs icon16" href="/do-you-believe-2015-720p-brrip-x264-yify-t11044255.html#comment"> <em class="iconvalue">35</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/do-you-believe-2015-720p-brrip-x264-yify-t11044255.html" class="cellMainLink">Do You Believe? (2015) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="912326638">870.06 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="04 Aug 2015, 21:30">2&nbsp;days</span></td>
			<td class="green center">2711</td>
			<td class="red lasttd center">2588</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11041093,0" class="icomment icommentjs icon16" href="/five-star-2014-hdrip-xvid-etrg-t11041093.html#comment"> <em class="iconvalue">23</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/five-star-2014-hdrip-xvid-etrg-t11041093.html" class="cellMainLink">Five Star.2014.HDRip.XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="743956789">709.49 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="04 Aug 2015, 07:03">2&nbsp;days</span></td>
			<td class="green center">2017</td>
			<td class="red lasttd center">1325</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11046338,0" class="icomment icommentjs icon16" href="/bajrangi-bhaijaan-2015-censor-printrip-xvid-5-1ch-2cd-team-jaffa-t11046338.html#comment"> <em class="iconvalue">12</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/bajrangi-bhaijaan-2015-censor-printrip-xvid-5-1ch-2cd-team-jaffa-t11046338.html" class="cellMainLink">Bajrangi Bhaijaan (2015) - Censor - PrintRip - XviD - 5.1CH - 2CD [Team Jaffa]</a></div>
			</td>
			<td class="nobr center" data-sort="1550225476">1.44 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="05 Aug 2015, 07:25">1&nbsp;day</span></td>
			<td class="green center">968</td>
			<td class="red lasttd center">2115</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11047787,0" class="icomment icommentjs icon16" href="/drishyam-2015-dvdscr-rip-x264-ac3-5-1-upmix-1-3-m-subs-chap-team-ictv-sparrow-t11047787.html#comment"> <em class="iconvalue">8</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/drishyam-2015-dvdscr-rip-x264-ac3-5-1-upmix-1-3-m-subs-chap-team-ictv-sparrow-t11047787.html" class="cellMainLink">Drishyam (2015) DvDScr Rip -x264-AC3 5.1 (Upmix) - [1-3] M-Subs-Chap - Team IcTv -={SPARROW}=-</a></div>
			</td>
			<td class="nobr center" data-sort="1585527372">1.48 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="05 Aug 2015, 14:28">1&nbsp;day</span></td>
			<td class="green center">700</td>
			<td class="red lasttd center">2152</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11052732,0" class="icomment icommentjs icon16" href="/lila-eve-2015-1080p-brrip-x264-yify-t11052732.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/lila-eve-2015-1080p-brrip-x264-yify-t11052732.html" class="cellMainLink">Lila &amp; Eve (2015) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="1552147381">1.45 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="06 Aug 2015, 14:17">9&nbsp;hours</span></td>
			<td class="green center">1494</td>
			<td class="red lasttd center">1278</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11048147,0" class="icomment icommentjs icon16" href="/angrej-2015-punjabi-1cd-camrip-x264-aac-dus-7th-anniversary-t11048147.html#comment"> <em class="iconvalue">10</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/angrej-2015-punjabi-1cd-camrip-x264-aac-dus-7th-anniversary-t11048147.html" class="cellMainLink">ANGREJ (2015) Punjabi - 1CD CAMRip - X264 AAC - DUS 7th Anniversary</a></div>
			</td>
			<td class="nobr center" data-sort="731040349">697.17 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="05 Aug 2015, 15:38">1&nbsp;day</span></td>
			<td class="green center">938</td>
			<td class="red lasttd center">1512</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="rsssign" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11050443,0" class="icomment icommentjs icon16" href="/suits-s05e07-hdtv-x264-asap-rartv-t11050443.html#comment"> <em class="iconvalue">45</em><span></span> </a> 					<a class="istill icon16" href="/suits-s05e07-hdtv-x264-asap-rartv-t11050443.html#stills" title="Torrent Has Screenshots"><span></span></a>
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/suits-s05e07-hdtv-x264-asap-rartv-t11050443.html" class="cellMainLink">Suits S05E07 HDTV x264-ASAP[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="253722152">241.97 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="06 Aug 2015, 02:02">21&nbsp;hours</span></td>
			<td class="green center">15286</td>
			<td class="red lasttd center">3561</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11050670,0" class="icomment icommentjs icon16" href="/mr-robot-s01e07-720p-hdtv-x264-killers-rartv-t11050670.html#comment"> <em class="iconvalue">108</em><span></span> </a> 					<a class="istill icon16" href="/mr-robot-s01e07-720p-hdtv-x264-killers-rartv-t11050670.html#stills" title="Torrent Has Screenshots"><span></span></a>
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/mr-robot-s01e07-720p-hdtv-x264-killers-rartv-t11050670.html" class="cellMainLink">Mr Robot S01E07 720p HDTV x264-KILLERS[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="759416572">724.24 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="06 Aug 2015, 03:08">20&nbsp;hours</span></td>
			<td class="green center">12336</td>
			<td class="red lasttd center">4151</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11045129,0" class="icomment icommentjs icon16" href="/pretty-little-liars-s06e09-hdtv-x264-lol-ettv-t11045129.html#comment"> <em class="iconvalue">45</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/pretty-little-liars-s06e09-hdtv-x264-lol-ettv-t11045129.html" class="cellMainLink">Pretty Little Liars S06E09 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="285994325">272.75 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="05 Aug 2015, 01:05">1&nbsp;day</span></td>
			<td class="green center">8759</td>
			<td class="red lasttd center">4136</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11035475,0" class="icomment icommentjs icon16" href="/true-detective-s02e07-hdtv-x264-batv-ettv-t11035475.html#comment"> <em class="iconvalue">180</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/true-detective-s02e07-hdtv-x264-batv-ettv-t11035475.html" class="cellMainLink">True Detective S02E07 HDTV x264-BATV[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="422189010">402.63 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="03 Aug 2015, 03:58">3&nbsp;days</span></td>
			<td class="green center">10073</td>
			<td class="red lasttd center">2558</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11050452,0" class="icomment icommentjs icon16" href="/extant-s02e06e07-hdtv-x264-lol-ettv-t11050452.html#comment"> <em class="iconvalue">35</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/extant-s02e06e07-hdtv-x264-lol-ettv-t11050452.html" class="cellMainLink">Extant S02E06E07 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="499272644">476.14 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="06 Aug 2015, 02:04">21&nbsp;hours</span></td>
			<td class="green center">5364</td>
			<td class="red lasttd center">2011</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11035356,0" class="icomment icommentjs icon16" href="/the-strain-s02e04-hdtv-x264-2hd-ettv-t11035356.html#comment"> <em class="iconvalue">119</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-strain-s02e04-hdtv-x264-2hd-ettv-t11035356.html" class="cellMainLink">The Strain S02E04 HDTV x264-2HD[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="324856212">309.81 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="03 Aug 2015, 03:07">3&nbsp;days</span></td>
			<td class="green center">4918</td>
			<td class="red lasttd center">1488</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11045310,0" class="icomment icommentjs icon16" href="/zoo-s01e06-hdtv-x264-lol-ettv-t11045310.html#comment"> <em class="iconvalue">47</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/zoo-s01e06-hdtv-x264-lol-ettv-t11045310.html" class="cellMainLink">Zoo S01E06 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="328866853">313.63 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="05 Aug 2015, 01:58">1&nbsp;day</span></td>
			<td class="green center">3664</td>
			<td class="red lasttd center">1613</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11045576,0" class="icomment icommentjs icon16" href="/tyrant-s02e08-hdtv-x264-killers-ettv-t11045576.html#comment"> <em class="iconvalue">31</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/tyrant-s02e08-hdtv-x264-killers-ettv-t11045576.html" class="cellMainLink">Tyrant S02E08 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="279359478">266.42 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="05 Aug 2015, 03:08">1&nbsp;day</span></td>
			<td class="green center">3544</td>
			<td class="red lasttd center">1363</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11040302,0" class="icomment icommentjs icon16" href="/devious-maids-s03e10-hdtv-x264-asap-ettv-t11040302.html#comment"> <em class="iconvalue">15</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/devious-maids-s03e10-hdtv-x264-asap-ettv-t11040302.html" class="cellMainLink">Devious Maids S03E10 HDTV x264-ASAP[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="331468421">316.11 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="04 Aug 2015, 02:08">2&nbsp;days</span></td>
			<td class="green center">3354</td>
			<td class="red lasttd center">1487</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11035343,0" class="icomment icommentjs icon16" href="/falling-skies-s05e06-hdtv-x264-killers-rartv-t11035343.html#comment"> <em class="iconvalue">8</em><span></span> </a> 					<a class="istill icon16" href="/falling-skies-s05e06-hdtv-x264-killers-rartv-t11035343.html#stills" title="Torrent Has Screenshots"><span></span></a>
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/falling-skies-s05e06-hdtv-x264-killers-rartv-t11035343.html" class="cellMainLink">Falling Skies S05E06 HDTV x264-KILLERS[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="344520625">328.56 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="03 Aug 2015, 03:05">3&nbsp;days</span></td>
			<td class="green center">2869</td>
			<td class="red lasttd center">1509</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11045603,0" class="icomment icommentjs icon16" href="/scream-the-tv-series-s01e06-hdtv-x264-asap-ettv-t11045603.html#comment"> <em class="iconvalue">43</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/scream-the-tv-series-s01e06-hdtv-x264-asap-ettv-t11045603.html" class="cellMainLink">Scream The TV Series S01E06 HDTV x264-ASAP[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="259375304">247.36 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="05 Aug 2015, 03:13">1&nbsp;day</span></td>
			<td class="green center">2619</td>
			<td class="red lasttd center">1097</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11045337,0" class="icomment icommentjs icon16" href="/stitchers-s01e10-hdtv-x264-asap-ettv-t11045337.html#comment"> <em class="iconvalue">31</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/stitchers-s01e10-hdtv-x264-asap-ettv-t11045337.html" class="cellMainLink">Stitchers S01E10 HDTV x264-ASAP[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="313951069">299.41 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="05 Aug 2015, 02:08">1&nbsp;day</span></td>
			<td class="green center">2501</td>
			<td class="red lasttd center">1070</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11040413,0" class="icomment icommentjs icon16" href="/the-whispers-s01e09-hdtv-x264-killers-ettv-t11040413.html#comment"> <em class="iconvalue">54</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-whispers-s01e09-hdtv-x264-killers-ettv-t11040413.html" class="cellMainLink">The Whispers S01E09 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="313825849">299.29 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="04 Aug 2015, 03:07">2&nbsp;days</span></td>
			<td class="green center">2123</td>
			<td class="red lasttd center">768</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11050320,0" class="icomment icommentjs icon16" href="/masterchef-us-s06e13-hdtv-x264-lol-ettv-t11050320.html#comment"> <em class="iconvalue">15</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/masterchef-us-s06e13-hdtv-x264-lol-ettv-t11050320.html" class="cellMainLink">MasterChef US S06E13 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="445722899">425.07 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="06 Aug 2015, 01:24">22&nbsp;hours</span></td>
			<td class="green center">1608</td>
			<td class="red lasttd center">1227</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11034940,0" class="icomment icommentjs icon16" href="/the-flash-1Âª-temporada-2015-dual-Ãudio-legendas-720p-by-luanharper-t11034940.html#comment"> <em class="iconvalue">12</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-flash-1Âª-temporada-2015-dual-Ãudio-legendas-720p-by-luanharper-t11034940.html" class="cellMainLink">The Flash 1Âª Temporada (2015) - Dual Ãudio + Legendas 720p (By-LuanHarper)</a></div>
			</td>
			<td class="nobr center" data-sort="9730148165">9.06 <span>GB</span></td>
			<td class="center">49</td>
			<td class="center"><span title="03 Aug 2015, 00:07">3&nbsp;days</span></td>
			<td class="green center">498</td>
			<td class="red lasttd center">1485</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="rsssign" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11041009,0" class="icomment icommentjs icon16" href="/billboard-presents-summer-hits-2015-mp3-320kbps-glodls-t11041009.html#comment"> <em class="iconvalue">19</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/billboard-presents-summer-hits-2015-mp3-320kbps-glodls-t11041009.html" class="cellMainLink">Billboard Presents - Summer Hits [2015] [MP3-320KBPS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="964169244">919.5 <span>MB</span></td>
			<td class="center">103</td>
			<td class="center"><span title="04 Aug 2015, 06:24">2&nbsp;days</span></td>
			<td class="green center">1180</td>
			<td class="red lasttd center">621</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11048544,0" class="icomment icommentjs icon16" href="/va-the-ultimate-collection-70s-schooldays-100-super-hits-of-the-70s-2013-mp3-320kbps-glodls-t11048544.html#comment"> <em class="iconvalue">7</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-the-ultimate-collection-70s-schooldays-100-super-hits-of-the-70s-2013-mp3-320kbps-glodls-t11048544.html" class="cellMainLink">VA - The Ultimate Collection 70s Schooldays 100 Super Hits Of The 70s [2013] [MP3-320KBPS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="877222514">836.58 <span>MB</span></td>
			<td class="center">105</td>
			<td class="center"><span title="05 Aug 2015, 17:25">1&nbsp;day</span></td>
			<td class="green center">737</td>
			<td class="red lasttd center">366</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11048830,0" class="icomment icommentjs icon16" href="/mp3-new-releases-2015-week-31-suprax-glodls-t11048830.html#comment"> <em class="iconvalue">8</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/mp3-new-releases-2015-week-31-suprax-glodls-t11048830.html" class="cellMainLink">MP3 NEW RELEASES 2015 WEEK 31 - SUPRAX [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="4602268403">4.29 <span>GB</span></td>
			<td class="center">580</td>
			<td class="center"><span title="05 Aug 2015, 18:27">1&nbsp;day</span></td>
			<td class="green center">277</td>
			<td class="red lasttd center">755</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11042974,0" class="icomment icommentjs icon16" href="/va-200-great-classic-60s-top-collection-2015-320kbps-pirate-shovon-t11042974.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-200-great-classic-60s-top-collection-2015-320kbps-pirate-shovon-t11042974.html" class="cellMainLink">VA - 200 Great Classic [60s Top Collection] [2015] [320Kbps] [Pirate Shovon]</a></div>
			</td>
			<td class="nobr center" data-sort="1445765153">1.35 <span>GB</span></td>
			<td class="center">234</td>
			<td class="center"><span title="04 Aug 2015, 15:06">2&nbsp;days</span></td>
			<td class="green center">695</td>
			<td class="red lasttd center">310</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11041834,0" class="icomment icommentjs icon16" href="/va-usa-top-40-singles-chart-01st-august-2015-american-top-40-with-ryan-seacrest-at40-itunes-rip-m4a-aac-uj-rip-t11041834.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-usa-top-40-singles-chart-01st-august-2015-american-top-40-with-ryan-seacrest-at40-itunes-rip-m4a-aac-uj-rip-t11041834.html" class="cellMainLink">VA - USA Top 40 Singles Chart - 01st August 2015 [American Top 40 With Ryan Seacrest - AT40] [iTunes Rip M4A AAC] [UJ.rip]</a></div>
			</td>
			<td class="nobr center" data-sort="303059638">289.02 <span>MB</span></td>
			<td class="center">42</td>
			<td class="center"><span title="04 Aug 2015, 09:09">2&nbsp;days</span></td>
			<td class="green center">377</td>
			<td class="red lasttd center">128</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11035703,0" class="icomment icommentjs icon16" href="/ozzy-osbourne-the-ultimate-sin-collection-deluxe-2015-320ak-t11035703.html#comment"> <em class="iconvalue">16</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/ozzy-osbourne-the-ultimate-sin-collection-deluxe-2015-320ak-t11035703.html" class="cellMainLink">Ozzy Osbourne - The Ultimate Sin Collection (Deluxe) 2015 320ak</a></div>
			</td>
			<td class="nobr center" data-sort="475591693">453.56 <span>MB</span></td>
			<td class="center">42</td>
			<td class="center"><span title="03 Aug 2015, 04:49">3&nbsp;days</span></td>
			<td class="green center">250</td>
			<td class="red lasttd center">69</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11046157,0" class="icomment icommentjs icon16" href="/various-artists-dancefloor-dj-hits-2015-mp3-320-kbps-t11046157.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/various-artists-dancefloor-dj-hits-2015-mp3-320-kbps-t11046157.html" class="cellMainLink">Various Artists - Dancefloor Dj Hits (2015) MP3, 320 kbps</a></div>
			</td>
			<td class="nobr center" data-sort="381645239">363.97 <span>MB</span></td>
			<td class="center">41</td>
			<td class="center"><span title="05 Aug 2015, 06:17">1&nbsp;day</span></td>
			<td class="green center">201</td>
			<td class="red lasttd center">86</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11049574,0" class="icomment icommentjs icon16" href="/rammstein-greatest-hits-2cd-320ak-t11049574.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/rammstein-greatest-hits-2cd-320ak-t11049574.html" class="cellMainLink">Rammstein - Greatest Hits (2CD) 320ak</a></div>
			</td>
			<td class="nobr center" data-sort="348841488">332.68 <span>MB</span></td>
			<td class="center">38</td>
			<td class="center"><span title="05 Aug 2015, 21:33">1&nbsp;day</span></td>
			<td class="green center">151</td>
			<td class="red lasttd center">89</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-who-studio-discography-1965-2006-320kbps-pirate-shovon-t11049254.html" class="cellMainLink">The Who - Studio Discography [1965 - 2006] [320Kbps] [Pirate Shovon]</a></div>
			</td>
			<td class="nobr center" data-sort="1886007673">1.76 <span>GB</span></td>
			<td class="center">223</td>
			<td class="center"><span title="05 Aug 2015, 20:21">1&nbsp;day</span></td>
			<td class="green center">73</td>
			<td class="red lasttd center">137</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11038657,0" class="icomment icommentjs icon16" href="/va-deep-sound-deep-soul-best-house-deephouse-2015-mp3-320-kbps-t11038657.html#comment"> <em class="iconvalue">7</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-deep-sound-deep-soul-best-house-deephouse-2015-mp3-320-kbps-t11038657.html" class="cellMainLink">VA - Deep Sound Deep Soul (Best House &amp; Deephouse) (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="717068268">683.85 <span>MB</span></td>
			<td class="center">51</td>
			<td class="center"><span title="03 Aug 2015, 19:02">3&nbsp;days</span></td>
			<td class="green center">107</td>
			<td class="red lasttd center">100</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11048440,0" class="icomment icommentjs icon16" href="/b-a-r-2015-week-30-newjds-glodls-t11048440.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/b-a-r-2015-week-30-newjds-glodls-t11048440.html" class="cellMainLink">B.A.R. 2015 Week 30 NEWJDS - [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="4175916602">3.89 <span>GB</span></td>
			<td class="center">467</td>
			<td class="center"><span title="05 Aug 2015, 16:55">1&nbsp;day</span></td>
			<td class="green center">38</td>
			<td class="red lasttd center">158</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11054001,0" class="icomment icommentjs icon16" href="/luke-bryan-kill-the-lights-2015-mp3-album-vbuc-t11054001.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/luke-bryan-kill-the-lights-2015-mp3-album-vbuc-t11054001.html" class="cellMainLink">Luke Bryan - Kill the Lights 2015 {MP3 Album}~{VBUc}</a></div>
			</td>
			<td class="nobr center" data-sort="114841561">109.52 <span>MB</span></td>
			<td class="center">13</td>
			<td class="center"><span title="06 Aug 2015, 18:36">5&nbsp;hours</span></td>
			<td class="green center">135</td>
			<td class="red lasttd center">55</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-rtl-i-like-the-90-s-2014-320kbps-pirate-shovon-t11044085.html" class="cellMainLink">VA - RTL I Like The 90&#039;s [2014] [320Kbps] [Pirate Shovon]</a></div>
			</td>
			<td class="nobr center" data-sort="568838527">542.49 <span>MB</span></td>
			<td class="center">63</td>
			<td class="center"><span title="04 Aug 2015, 20:31">2&nbsp;days</span></td>
			<td class="green center">98</td>
			<td class="red lasttd center">88</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/billboard-songs-of-the-summer-chart-15th-august-2015-mp3-320kbps-h4ckus-glodls-t11048316.html" class="cellMainLink">Billboard - Songs of the Summer Chart (15th August 2015) [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="177833442">169.6 <span>MB</span></td>
			<td class="center">24</td>
			<td class="center"><span title="05 Aug 2015, 16:17">1&nbsp;day</span></td>
			<td class="green center">126</td>
			<td class="red lasttd center">51</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11052420,0" class="icomment icommentjs icon16" href="/fear-factory-genexus-2015-l-audio-l-album-track-l-320kbps-l-cbr-l-mp3-l-sn3h1t87-t11052420.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/fear-factory-genexus-2015-l-audio-l-album-track-l-320kbps-l-cbr-l-mp3-l-sn3h1t87-t11052420.html" class="cellMainLink">Fear Factory - Genexus (2015) l Audio l Album Track l 320kbps l CBR l Mp3 l sn3h1t87</a></div>
			</td>
			<td class="nobr center" data-sort="143177854">136.55 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span title="06 Aug 2015, 12:41">11&nbsp;hours</span></td>
			<td class="green center">102</td>
			<td class="red lasttd center">47</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="rsssign" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11038172,0" class="icomment icommentjs icon16" href="/lords-of-the-fallen-v1-6-repack-by-r-g-mechanics-t11038172.html#comment"> <em class="iconvalue">57</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/lords-of-the-fallen-v1-6-repack-by-r-g-mechanics-t11038172.html" class="cellMainLink">Lords Of The Fallen v1.6 Repack By [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center" data-sort="6092754411">5.67 <span>GB</span></td>
			<td class="center">15</td>
			<td class="center"><span title="03 Aug 2015, 16:31">3&nbsp;days</span></td>
			<td class="green center">447</td>
			<td class="red lasttd center">495</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11046980,0" class="icomment icommentjs icon16" href="/the-elder-scrolls-v-skyrim-the-journey-v-1-1-2015-pc-repack-by-gmax007-t11046980.html#comment"> <em class="iconvalue">40</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/the-elder-scrolls-v-skyrim-the-journey-v-1-1-2015-pc-repack-by-gmax007-t11046980.html" class="cellMainLink">The Elder Scrolls V: Skyrim - The Journey [v.1.1] (2015) PC | RePack By gmax007</a></div>
			</td>
			<td class="nobr center" data-sort="26636324822">24.81 <span>GB</span></td>
			<td class="center">16</td>
			<td class="center"><span title="05 Aug 2015, 10:38">1&nbsp;day</span></td>
			<td class="green center">388</td>
			<td class="red lasttd center">511</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11043879,0" class="icomment icommentjs icon16" href="/fairy-fencer-f-codex-t11043879.html#comment"> <em class="iconvalue">50</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/fairy-fencer-f-codex-t11043879.html" class="cellMainLink">Fairy.Fencer.F-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="2580092308">2.4 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="04 Aug 2015, 19:19">2&nbsp;days</span></td>
			<td class="green center">438</td>
			<td class="red lasttd center">270</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11048586,0" class="icomment icommentjs icon16" href="/sid-meier-s-civilization-v-the-complete-edition-2013-pc-repack-by-xatab-t11048586.html#comment"> <em class="iconvalue">14</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/sid-meier-s-civilization-v-the-complete-edition-2013-pc-repack-by-xatab-t11048586.html" class="cellMainLink">Sid Meier&#039;s Civilization V: The Complete Edition (2013) PC | Repack By Xatab</a></div>
			</td>
			<td class="nobr center" data-sort="4970244641">4.63 <span>GB</span></td>
			<td class="center">9</td>
			<td class="center"><span title="05 Aug 2015, 17:32">1&nbsp;day</span></td>
			<td class="green center">405</td>
			<td class="red lasttd center">216</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11041370,0" class="icomment icommentjs icon16" href="/the-forest-2014-en-ru-alpha-0-21c-steamrip-r-g-games-t11041370.html#comment"> <em class="iconvalue">17</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/the-forest-2014-en-ru-alpha-0-21c-steamrip-r-g-games-t11041370.html" class="cellMainLink">The Forest (2014) [En-Ru] (Alpha 0.21c) SteamRip R.G. Games</a></div>
			</td>
			<td class="nobr center" data-sort="991045157">945.13 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="04 Aug 2015, 08:21">2&nbsp;days</span></td>
			<td class="green center">544</td>
			<td class="red lasttd center">68</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11038895,0" class="icomment icommentjs icon16" href="/pro-cycling-manager-2015-codex-t11038895.html#comment"> <em class="iconvalue">25</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/pro-cycling-manager-2015-codex-t11038895.html" class="cellMainLink">Pro Cycling Manager 2015-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="7439586873">6.93 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="03 Aug 2015, 20:21">3&nbsp;days</span></td>
			<td class="green center">279</td>
			<td class="red lasttd center">265</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11040698,0" class="icomment icommentjs icon16" href="/tom-clansys-ghost-recon-anthology-repack-by-r-g-revenants-naswari-zohaib-t11040698.html#comment"> <em class="iconvalue">26</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/tom-clansys-ghost-recon-anthology-repack-by-r-g-revenants-naswari-zohaib-t11040698.html" class="cellMainLink">Tom Clansys Ghost Recon Anthology Repack By R.G Revenants NASWARI+ZOHAIB</a></div>
			</td>
			<td class="nobr center" data-sort="12988572766">12.1 <span>GB</span></td>
			<td class="center">50</td>
			<td class="center"><span title="04 Aug 2015, 04:55">2&nbsp;days</span></td>
			<td class="green center">117</td>
			<td class="red lasttd center">418</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11038229,0" class="icomment icommentjs icon16" href="/superhot-beta-version-t11038229.html#comment"> <em class="iconvalue">33</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/superhot-beta-version-t11038229.html" class="cellMainLink">SUPERHOT (Beta version)</a></div>
			</td>
			<td class="nobr center" data-sort="449758794">428.92 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="03 Aug 2015, 16:54">3&nbsp;days</span></td>
			<td class="green center">405</td>
			<td class="red lasttd center">68</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11040820,0" class="icomment icommentjs icon16" href="/submerged-reloaded-t11040820.html#comment"> <em class="iconvalue">24</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/submerged-reloaded-t11040820.html" class="cellMainLink">Submerged-RELOADED</a></div>
			</td>
			<td class="nobr center" data-sort="1283233035">1.2 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="04 Aug 2015, 05:29">2&nbsp;days</span></td>
			<td class="green center">311</td>
			<td class="red lasttd center">121</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11051958,0" class="icomment icommentjs icon16" href="/euro-truck-simulator-2-v-1-19-2-1s-27-dlc-repack-by-r-g-Ðechanics-t11051958.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/euro-truck-simulator-2-v-1-19-2-1s-27-dlc-repack-by-r-g-Ðechanics-t11051958.html" class="cellMainLink">Euro Truck Simulator 2 [v 1.19.2.1s + 27 DLC] | RePack By R.G. Ðechanics</a></div>
			</td>
			<td class="nobr center" data-sort="1702721086">1.59 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="06 Aug 2015, 10:06">13&nbsp;hours</span></td>
			<td class="green center">259</td>
			<td class="red lasttd center">132</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11048905,0" class="icomment icommentjs icon16" href="/whispered-secrets-4-golden-silence-collector-s-edition-asg-t11048905.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/whispered-secrets-4-golden-silence-collector-s-edition-asg-t11048905.html" class="cellMainLink">Whispered Secrets 4 - Golden Silence Collector&#039;s Edition [ASG]</a></div>
			</td>
			<td class="nobr center" data-sort="707539010">674.76 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="05 Aug 2015, 18:48">1&nbsp;day</span></td>
			<td class="green center">249</td>
			<td class="red lasttd center">94</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11048730,0" class="icomment icommentjs icon16" href="/hidden-on-the-trail-of-the-ancients-codex-t11048730.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/hidden-on-the-trail-of-the-ancients-codex-t11048730.html" class="cellMainLink">Hidden On the trail of the Ancients-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="3545994813">3.3 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="05 Aug 2015, 18:03">1&nbsp;day</span></td>
			<td class="green center">116</td>
			<td class="red lasttd center">93</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11053824,0" class="icomment icommentjs icon16" href="/batman-arkham-knight-black-box-t11053824.html#comment"> <em class="iconvalue">27</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/batman-arkham-knight-black-box-t11053824.html" class="cellMainLink">Batman Arkham Knight-Black Box</a></div>
			</td>
			<td class="nobr center" data-sort="27543372521">25.65 <span>GB</span></td>
			<td class="center">28</td>
			<td class="center"><span title="06 Aug 2015, 17:58">5&nbsp;hours</span></td>
			<td class="green center">2</td>
			<td class="red lasttd center">190</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11047977,0" class="icomment icommentjs icon16" href="/ark-survival-evolved-repack-maxagent-t11047977.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					<a class="istill icon16" href="/ark-survival-evolved-repack-maxagent-t11047977.html#stills" title="Torrent Has Screenshots"><span></span></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/ark-survival-evolved-repack-maxagent-t11047977.html" class="cellMainLink">ARK Survival Evolved RePack MAXAGENT</a></div>
			</td>
			<td class="nobr center" data-sort="5690056961">5.3 <span>GB</span></td>
			<td class="center">11</td>
			<td class="center"><span title="05 Aug 2015, 15:05">1&nbsp;day</span></td>
			<td class="green center">62</td>
			<td class="red lasttd center">119</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11048619,0" class="icomment icommentjs icon16" href="/time-ramesside-a-new-reckoning-tinyiso-t11048619.html#comment"> <em class="iconvalue">7</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/time-ramesside-a-new-reckoning-tinyiso-t11048619.html" class="cellMainLink">Time Ramesside A New Reckoning-TiNYiSO</a></div>
			</td>
			<td class="nobr center" data-sort="4007084706">3.73 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="05 Aug 2015, 17:38">1&nbsp;day</span></td>
			<td class="green center">38</td>
			<td class="red lasttd center">83</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="rsssign" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11050064,0" class="icomment icommentjs icon16" href="/microsoft-windows-10-6in1-aio-english-full-updated-8-5-15-preactivated-2015-by-whitedeath-t11050064.html#comment"> <em class="iconvalue">83</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/microsoft-windows-10-6in1-aio-english-full-updated-8-5-15-preactivated-2015-by-whitedeath-t11050064.html" class="cellMainLink">Microsoft Windows 10 6in1 AIO English Full Updated 8/5/15 Preactivated 2015 by:WhiteDeath</a></div>
			</td>
			<td class="nobr center" data-sort="5740410880">5.35 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="05 Aug 2015, 23:46">1&nbsp;day</span></td>
			<td class="green center">415</td>
			<td class="red lasttd center">1460</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11038486,0" class="icomment icommentjs icon16" href="/windows-10-aio-6in1-english-x86-x64-2015-by-whitedeath-teamos-t11038486.html#comment"> <em class="iconvalue">98</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-10-aio-6in1-english-x86-x64-2015-by-whitedeath-teamos-t11038486.html" class="cellMainLink">Windows 10 AIO 6IN1 English X86-x64 2015 By:WhiteDeath[TeamOS]</a></div>
			</td>
			<td class="nobr center" data-sort="4991850496">4.65 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="03 Aug 2015, 18:02">3&nbsp;days</span></td>
			<td class="green center">436</td>
			<td class="red lasttd center">811</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11043012,0" class="icomment icommentjs icon16" href="/internet-download-manager-idm-6-23-build-18-registered-32bit-64bit-patch-crackingpatching-t11043012.html#comment"> <em class="iconvalue">42</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/internet-download-manager-idm-6-23-build-18-registered-32bit-64bit-patch-crackingpatching-t11043012.html" class="cellMainLink">Internet Download Manager (IDM) 6.23 Build 18 Registered (32bit + 64bit Patch) [CrackingPatching]</a></div>
			</td>
			<td class="nobr center" data-sort="10154876">9.68 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="04 Aug 2015, 15:24">2&nbsp;days</span></td>
			<td class="green center">573</td>
			<td class="red lasttd center">287</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11041217,0" class="icomment icommentjs icon16" href="/avg-antivirus-pro-2015-15-0-build-6081-x86x64-multilingual-keys-4realtorrentz-t11041217.html#comment"> <em class="iconvalue">36</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/avg-antivirus-pro-2015-15-0-build-6081-x86x64-multilingual-keys-4realtorrentz-t11041217.html" class="cellMainLink">AVG Antivirus Pro 2015 15.0 Build 6081 (x86x64) Multilingual + Keys [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="377196995">359.72 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="04 Aug 2015, 07:41">2&nbsp;days</span></td>
			<td class="green center">422</td>
			<td class="red lasttd center">99</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11044094,0" class="icomment icommentjs icon16" href="/adobe-photoshop-cc-2015-20150722-r-168-repack-d-akov-pre-activate-appzdam-t11044094.html#comment"> <em class="iconvalue">11</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/adobe-photoshop-cc-2015-20150722-r-168-repack-d-akov-pre-activate-appzdam-t11044094.html" class="cellMainLink">Adobe Photoshop CC 2015 (20150722.r.168) RePack (D! Akov) Pre-Activate - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="1390940928">1.3 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="04 Aug 2015, 20:34">2&nbsp;days</span></td>
			<td class="green center">329</td>
			<td class="red lasttd center">132</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11038175,0" class="icomment icommentjs icon16" href="/k-lite-codec-pack-11-3-5-mega-full-standard-basic-update-appzdam-t11038175.html#comment"> <em class="iconvalue">7</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/k-lite-codec-pack-11-3-5-mega-full-standard-basic-update-appzdam-t11038175.html" class="cellMainLink">K-Lite Codec Pack 11.3.5 Mega / Full / Standard / Basic + Update - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="142030657">135.45 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="03 Aug 2015, 16:31">3&nbsp;days</span></td>
			<td class="green center">268</td>
			<td class="red lasttd center">95</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11039631,0" class="icomment icommentjs icon16" href="/paragon-hard-disk-manager-15-premium-10-1-25-772-win10-advanced-recovery-cd-x86x64-retail-iso-deepstatus-t11039631.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/paragon-hard-disk-manager-15-premium-10-1-25-772-win10-advanced-recovery-cd-x86x64-retail-iso-deepstatus-t11039631.html" class="cellMainLink">Paragon Hard Disk Manager 15 Premium 10.1.25.772 WiN10 Advanced Recovery CD x86x64 ReTAiL ISO [deepstatus]</a></div>
			</td>
			<td class="nobr center" data-sort="864434176">824.39 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="03 Aug 2015, 21:57">3&nbsp;days</span></td>
			<td class="green center">161</td>
			<td class="red lasttd center">62</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11047026,0" class="icomment icommentjs icon16" href="/keys-for-eset-kaspersky-avast-dr-web-avira-on-august-5-2015-pc-t11047026.html#comment"> <em class="iconvalue">7</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/keys-for-eset-kaspersky-avast-dr-web-avira-on-august-5-2015-pc-t11047026.html" class="cellMainLink">Keys for ESET, Kaspersky, Avast, Dr.Web, Avira [on August 5] (2015) PC</a></div>
			</td>
			<td class="nobr center" data-sort="3955408">3.77 <span>MB</span></td>
			<td class="center">796</td>
			<td class="center"><span title="05 Aug 2015, 10:50">1&nbsp;day</span></td>
			<td class="green center">127</td>
			<td class="red lasttd center">65</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11043521,0" class="icomment icommentjs icon16" href="/ableton-live-9-suite-v9-2-1-x64x86-incl-patch-io-deepstatus-t11043521.html#comment"> <em class="iconvalue">18</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/ableton-live-9-suite-v9-2-1-x64x86-incl-patch-io-deepstatus-t11043521.html" class="cellMainLink">Ableton Live 9 Suite v9.2.1 x64x86 Incl.Patch-iO [deepstatus]</a></div>
			</td>
			<td class="nobr center" data-sort="1462801950">1.36 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="04 Aug 2015, 17:47">2&nbsp;days</span></td>
			<td class="green center">112</td>
			<td class="red lasttd center">52</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11049168,0" class="icomment icommentjs icon16" href="/avg-pc-tuneup-2015-15-0-1001-638-multilingual-keygen-appzdam-t11049168.html#comment"> <em class="iconvalue">13</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/avg-pc-tuneup-2015-15-0-1001-638-multilingual-keygen-appzdam-t11049168.html" class="cellMainLink">AVG PC TuneUp 2015 15.0.1001.638 Multilingual + Keygen - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="117454684">112.01 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="05 Aug 2015, 19:59">1&nbsp;day</span></td>
			<td class="green center">122</td>
			<td class="red lasttd center">39</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11035341,0" class="icomment icommentjs icon16" href="/impact-soundworks-bravura-scoring-brass-complete-kontakt-synthic4te-t11035341.html#comment"> <em class="iconvalue">8</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/impact-soundworks-bravura-scoring-brass-complete-kontakt-synthic4te-t11035341.html" class="cellMainLink">Impact Soundworks Bravura Scoring Brass Complete KONTAKT-SYNTHiC4TE</a></div>
			</td>
			<td class="nobr center" data-sort="26819463455">24.98 <span>GB</span></td>
			<td class="center">67</td>
			<td class="center"><span title="03 Aug 2015, 03:05">3&nbsp;days</span></td>
			<td class="green center">37</td>
			<td class="red lasttd center">82</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11053172,0" class="icomment icommentjs icon16" href="/dvdfab-v9-2-0-7-final-crack-techtools-t11053172.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/dvdfab-v9-2-0-7-final-crack-techtools-t11053172.html" class="cellMainLink">DVDFab v9.2.0.7 FINAL + Crack [TechTools]</a></div>
			</td>
			<td class="nobr center" data-sort="64834684">61.83 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="06 Aug 2015, 15:44">8&nbsp;hours</span></td>
			<td class="green center">76</td>
			<td class="red lasttd center">35</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/symantec-backup-exec-2015-14-2-1180-fp1-multilingual-reged-deepstatus-t11053880.html" class="cellMainLink">Symantec Backup Exec 2015 14.2.1180 FP1 Multilingual Reged [deepstatus]</a></div>
			</td>
			<td class="nobr center" data-sort="2409074452">2.24 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="06 Aug 2015, 18:08">5&nbsp;hours</span></td>
			<td class="green center">1</td>
			<td class="red lasttd center">102</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType pictureType">
                    <a href="/daz3d-poser-98993-dentelle-for-ssp-t11043093.html" class="cellMainLink">Daz3d - Poser - 98993 Dentelle for SSP</a></div>
			</td>
			<td class="nobr center" data-sort="62658839">59.76 <span>MB</span></td>
			<td class="center">86</td>
			<td class="center"><span title="04 Aug 2015, 15:50">2&nbsp;days</span></td>
			<td class="green center">39</td>
			<td class="red lasttd center">19</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11035005,0" class="icomment icommentjs icon16" href="/microsoft-windows-10-rtm-2015-msdn-x86-x64-iso-s-part3-last-by-whitedeathteamos-t11035005.html#comment"> <em class="iconvalue">16</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/microsoft-windows-10-rtm-2015-msdn-x86-x64-iso-s-part3-last-by-whitedeathteamos-t11035005.html" class="cellMainLink">MICROSOFT WINDOWS 10 RTM 2015 MSDN x86 x64 ISO&#039;S PART3 Last By:WhiteDeathTeamOS</a></div>
			</td>
			<td class="nobr center" data-sort="89956569088">83.78 <span>GB</span></td>
			<td class="center">26</td>
			<td class="center"><span title="03 Aug 2015, 00:44">3&nbsp;days</span></td>
			<td class="green center">5</td>
			<td class="red lasttd center">49</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="rsssign" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11052290,0" class="icomment icommentjs icon16" href="/naruto-shippuden-422-480p-engsub-70mb-iorchid-t11052290.html#comment"> <em class="iconvalue">21</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/naruto-shippuden-422-480p-engsub-70mb-iorchid-t11052290.html" class="cellMainLink">Naruto Shippuden - 422 [480p][EngSub][70mb]_iORcHiD</a></div>
			</td>
			<td class="nobr center" data-sort="75347163">71.86 <span>MB</span></td>
			<td class="center">7</td>
			<td class="center"><span title="06 Aug 2015, 12:03">11&nbsp;hours</span></td>
			<td class="green center">688</td>
			<td class="red lasttd center">227</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11053697,0" class="icomment icommentjs icon16" href="/horriblesubs-ranpo-kitan-game-of-laplace-06-720p-mkv-t11053697.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-ranpo-kitan-game-of-laplace-06-720p-mkv-t11053697.html" class="cellMainLink">[HorribleSubs] Ranpo Kitan - Game of Laplace - 06 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="429444763">409.55 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="06 Aug 2015, 17:35">6&nbsp;hours</span></td>
			<td class="green center">59</td>
			<td class="red lasttd center">374</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11042478,0" class="icomment icommentjs icon16" href="/vivid-non-non-biyori-repeat-05-9816391f-mkv-t11042478.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/vivid-non-non-biyori-repeat-05-9816391f-mkv-t11042478.html" class="cellMainLink">[Vivid] Non Non Biyori Repeat - 05 [9816391F].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="420650348">401.16 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="04 Aug 2015, 12:15">2&nbsp;days</span></td>
			<td class="green center">282</td>
			<td class="red lasttd center">89</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11038782,0" class="icomment icommentjs icon16" href="/noobsubs-psycho-pass-the-movie-1080p-blu-ray-8bit-ac3-mp4-t11038782.html#comment"> <em class="iconvalue">9</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/noobsubs-psycho-pass-the-movie-1080p-blu-ray-8bit-ac3-mp4-t11038782.html" class="cellMainLink">[NoobSubs] Psycho-Pass The Movie (1080p Blu-ray 8bit AC3).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="7678051470">7.15 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="03 Aug 2015, 19:42">3&nbsp;days</span></td>
			<td class="green center">154</td>
			<td class="red lasttd center">114</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/doki-to-love-ru-darkness-2nd-05-1280x720-hi10p-aac-47994467-mkv-t11050022.html" class="cellMainLink">[Doki] To LOVE-Ru Darkness 2nd - 05 (1280x720 Hi10P AAC) [47994467].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="256925988">245.02 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="05 Aug 2015, 23:10">1&nbsp;day</span></td>
			<td class="green center">111</td>
			<td class="red lasttd center">146</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11035326,0" class="icomment icommentjs icon16" href="/animerg-dragon-ball-super-004-1080p-phr0sty-mkv-t11035326.html#comment"> <em class="iconvalue">16</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-dragon-ball-super-004-1080p-phr0sty-mkv-t11035326.html" class="cellMainLink">[AnimeRG] Dragon Ball Super 004 - 1080p [Phr0stY].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="759981405">724.77 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="03 Aug 2015, 03:02">3&nbsp;days</span></td>
			<td class="green center">226</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11035229,0" class="icomment icommentjs icon16" href="/dragon-ball-super-04-eng-sub-720p-l-mbert-t11035229.html#comment"> <em class="iconvalue">12</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dragon-ball-super-04-eng-sub-720p-l-mbert-t11035229.html" class="cellMainLink">Dragon Ball Super - 04 [EnG SuB] 720p L@mBerT</a></div>
			</td>
			<td class="nobr center" data-sort="77327194">73.74 <span>MB</span></td>
			<td class="center">9</td>
			<td class="center"><span title="03 Aug 2015, 02:28">3&nbsp;days</span></td>
			<td class="green center">112</td>
			<td class="red lasttd center">37</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11042438,0" class="icomment icommentjs icon16" href="/animerockers-rg-blood-blockade-battlefront-kekkai-sensen-ep-1-to-11-season-1-720p-complete-english-dubbed-rprip-t11042438.html#comment"> <em class="iconvalue">10</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerockers-rg-blood-blockade-battlefront-kekkai-sensen-ep-1-to-11-season-1-720p-complete-english-dubbed-rprip-t11042438.html" class="cellMainLink">[AnimeRockers-RG]Blood Blockade Battlefront \ Kekkai Sensen (Ep 1 to 11) Season 1 720p Complete English dubbed[RPRip]</a></div>
			</td>
			<td class="nobr center" data-sort="1330988494">1.24 <span>GB</span></td>
			<td class="center">15</td>
			<td class="center"><span title="04 Aug 2015, 11:59">2&nbsp;days</span></td>
			<td class="green center">73</td>
			<td class="red lasttd center">49</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11052782,0" class="icomment icommentjs icon16" href="/animerg-naruto-shippuuden-423-english-subbed-480p-kami-t11052782.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-naruto-shippuuden-423-english-subbed-480p-kami-t11052782.html" class="cellMainLink">[AnimeRG] Naruto Shippuuden - 423 [English Subbed][480p] [KaMi]</a></div>
			</td>
			<td class="nobr center" data-sort="62820281">59.91 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="06 Aug 2015, 14:27">9&nbsp;hours</span></td>
			<td class="green center">91</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-gakkou-gurashi-05-raw-bs11-1280x720-x264-aac-mp4-t11054107.html" class="cellMainLink">[Leopard-Raws] Gakkou Gurashi! - 05 RAW (BS11 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="373000610">355.72 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="06 Aug 2015, 18:55">4&nbsp;hours</span></td>
			<td class="green center">2</td>
			<td class="red lasttd center">65</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fff-black-bullet-vol-06-bd-720p-aac-t11038274.html" class="cellMainLink">[FFF] Black Bullet - Vol.06 [BD][720p-AAC]</a></div>
			</td>
			<td class="nobr center" data-sort="975553211">930.36 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="03 Aug 2015, 17:10">3&nbsp;days</span></td>
			<td class="green center">38</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-isuca-bd-vol-4-10-bit-x264-1080p-flac-xcelent-t11042037.html" class="cellMainLink">[AnimeRG] ISUCA BD Vol. 4 (10-bit x264 1080p FLAC) [Xcelent]</a></div>
			</td>
			<td class="nobr center" data-sort="1256116976">1.17 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="04 Aug 2015, 10:03">2&nbsp;days</span></td>
			<td class="green center">34</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11038880,0" class="icomment icommentjs icon16" href="/aliq-no-game-no-life-Î²-1080p-bd-dual-audio-x264-specials-t11038880.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/aliq-no-game-no-life-Î²-1080p-bd-dual-audio-x264-specials-t11038880.html" class="cellMainLink">[AliQ] No Game No Life [Î²;1080p;BD Dual-Audio+ x264] + Specials</a></div>
			</td>
			<td class="nobr center" data-sort="7283949855">6.78 <span>GB</span></td>
			<td class="center">18</td>
			<td class="center"><span title="03 Aug 2015, 20:15">3&nbsp;days</span></td>
			<td class="green center">14</td>
			<td class="red lasttd center">27</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11048819,0" class="icomment icommentjs icon16" href="/fff-mushishi-zoku-shou-vol-02-bd-1080p-flac-t11048819.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fff-mushishi-zoku-shou-vol-02-bd-1080p-flac-t11048819.html" class="cellMainLink">[FFF] Mushishi Zoku Shou - Vol.02 [BD][1080p-FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="4234271027">3.94 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="05 Aug 2015, 18:25">1&nbsp;day</span></td>
			<td class="green center">23</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11036729,0" class="icomment icommentjs icon16" href="/vivid-mushishi-zoku-shou-23-24-suzu-no-shizuku-bd-720p-aac-8bdf3e31-mkv-t11036729.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/vivid-mushishi-zoku-shou-23-24-suzu-no-shizuku-bd-720p-aac-8bdf3e31-mkv-t11036729.html" class="cellMainLink">[Vivid] Mushishi Zoku Shou - 23-24 - Suzu no Shizuku [BD 720p AAC] [8BDF3E31].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="650116011">620 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="03 Aug 2015, 10:15">3&nbsp;days</span></td>
			<td class="green center">4</td>
			<td class="red lasttd center">13</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="rsssign" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11048953,0" class="icomment icommentjs icon16" href="/marvel-week-08-05-2015-nem-t11048953.html#comment"> <em class="iconvalue">23</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/marvel-week-08-05-2015-nem-t11048953.html" class="cellMainLink">Marvel Week+ (08-05-2015) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="700431877">667.98 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center"><span title="05 Aug 2015, 19:01">1&nbsp;day</span></td>
			<td class="green center">832</td>
			<td class="red lasttd center">642</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11043633,0" class="icomment icommentjs icon16" href="/mens-magazines-bundle-august-5-2015-true-pdf-t11043633.html#comment"> <em class="iconvalue">14</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/mens-magazines-bundle-august-5-2015-true-pdf-t11043633.html" class="cellMainLink">Mens Magazines Bundle - August 5 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="287985570">274.64 <span>MB</span></td>
			<td class="center">11</td>
			<td class="center"><span title="04 Aug 2015, 18:27">2&nbsp;days</span></td>
			<td class="green center">711</td>
			<td class="red lasttd center">320</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11047535,0" class="icomment icommentjs icon16" href="/dc-week-08-05-2015-aka-dc-you-week-10-nem-t11047535.html#comment"> <em class="iconvalue">26</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/dc-week-08-05-2015-aka-dc-you-week-10-nem-t11047535.html" class="cellMainLink">DC Week+ (08-05-2015) (aka DC YOU Week 10) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="382275297">364.57 <span>MB</span></td>
			<td class="center">12</td>
			<td class="center"><span title="05 Aug 2015, 13:23">1&nbsp;day</span></td>
			<td class="green center">672</td>
			<td class="red lasttd center">283</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11044175,0" class="icomment icommentjs icon16" href="/assorted-magazines-bundle-august-5-2015-true-pdf-t11044175.html#comment"> <em class="iconvalue">13</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/assorted-magazines-bundle-august-5-2015-true-pdf-t11044175.html" class="cellMainLink">Assorted Magazines Bundle - August 5 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="362940427">346.13 <span>MB</span></td>
			<td class="center">19</td>
			<td class="center"><span title="04 Aug 2015, 21:02">2&nbsp;days</span></td>
			<td class="green center">515</td>
			<td class="red lasttd center">316</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11043428,0" class="icomment icommentjs icon16" href="/home-magazines-bundle-august-5-2015-true-pdf-t11043428.html#comment"> <em class="iconvalue">8</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/home-magazines-bundle-august-5-2015-true-pdf-t11043428.html" class="cellMainLink">Home Magazines Bundle - August 5 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="701143973">668.66 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center"><span title="04 Aug 2015, 17:29">2&nbsp;days</span></td>
			<td class="green center">384</td>
			<td class="red lasttd center">185</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11043990,0" class="icomment icommentjs icon16" href="/travel-magazines-august-5-2015-true-pdf-t11043990.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/travel-magazines-august-5-2015-true-pdf-t11043990.html" class="cellMainLink">Travel Magazines - August 5 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="132528774">126.39 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="04 Aug 2015, 19:50">2&nbsp;days</span></td>
			<td class="green center">329</td>
			<td class="red lasttd center">77</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11042516,0" class="icomment icommentjs icon16" href="/encyclopedia-of-psychology-1st-edition-8-volume-set-2000-pdf-gooner-t11042516.html#comment"> <em class="iconvalue">8</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/encyclopedia-of-psychology-1st-edition-8-volume-set-2000-pdf-gooner-t11042516.html" class="cellMainLink">Encyclopedia of Psychology - 1st Edition (8 Volume Set) (2000).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="119143723">113.62 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="04 Aug 2015, 12:39">2&nbsp;days</span></td>
			<td class="green center">252</td>
			<td class="red lasttd center">46</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11050263,0" class="icomment icommentjs icon16" href="/womens-magazines-bundle-august-6-2015-true-pdf-t11050263.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/womens-magazines-bundle-august-6-2015-true-pdf-t11050263.html" class="cellMainLink">Womens Magazines Bundle - August 6 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="312274124">297.81 <span>MB</span></td>
			<td class="center">13</td>
			<td class="center"><span title="06 Aug 2015, 01:11">22&nbsp;hours</span></td>
			<td class="green center">143</td>
			<td class="red lasttd center">120</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11046272,0" class="icomment icommentjs icon16" href="/0-day-week-of-2015-07-29-t11046272.html#comment"> <em class="iconvalue">8</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/0-day-week-of-2015-07-29-t11046272.html" class="cellMainLink">0-Day Week of 2015.07.29</a></div>
			</td>
			<td class="nobr center" data-sort="5093252183">4.74 <span>GB</span></td>
			<td class="center">87</td>
			<td class="center"><span title="05 Aug 2015, 07:05">1&nbsp;day</span></td>
			<td class="green center">110</td>
			<td class="red lasttd center">152</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11050907,0" class="icomment icommentjs icon16" href="/tabloid-magazines-bundle-august-6-2015-true-pdf-t11050907.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/tabloid-magazines-bundle-august-6-2015-true-pdf-t11050907.html" class="cellMainLink">Tabloid Magazines Bundle - August 6 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="311505139">297.07 <span>MB</span></td>
			<td class="center">25</td>
			<td class="center"><span title="06 Aug 2015, 04:05">19&nbsp;hours</span></td>
			<td class="green center">143</td>
			<td class="red lasttd center">105</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11047298,0" class="icomment icommentjs icon16" href="/guardians-of-knowhere-002-2015-digital-zone-empire-cbr-t11047298.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/guardians-of-knowhere-002-2015-digital-zone-empire-cbr-t11047298.html" class="cellMainLink">Guardians of Knowhere 002 (2015) (Digital) (Zone-Empire).cbr</a></div>
			</td>
			<td class="nobr center" data-sort="39650939">37.81 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="05 Aug 2015, 12:03">1&nbsp;day</span></td>
			<td class="green center">212</td>
			<td class="red lasttd center">32</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11053208,0" class="icomment icommentjs icon16" href="/the-gale-encyclopedia-of-medicine-4th-5th-edition-4-9-volume-sets-2014-2015-pdf-gooner-t11053208.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-gale-encyclopedia-of-medicine-4th-5th-edition-4-9-volume-sets-2014-2015-pdf-gooner-t11053208.html" class="cellMainLink">The Gale Encyclopedia of Medicine - 4th &amp; 5th Edition (4 &amp; 9 Volume Sets) (2014 &amp; 2015) (Pdf) Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="424702269">405.03 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="06 Aug 2015, 15:51">8&nbsp;hours</span></td>
			<td class="green center">84</td>
			<td class="red lasttd center">88</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11046122,0" class="icomment icommentjs icon16" href="/hitlist-week-of-2015-07-29-t11046122.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/hitlist-week-of-2015-07-29-t11046122.html" class="cellMainLink">Hitlist Week of 2015.07.29</a></div>
			</td>
			<td class="nobr center" data-sort="21374784511">19.91 <span>GB</span></td>
			<td class="center">299</td>
			<td class="center"><span title="05 Aug 2015, 06:05">1&nbsp;day</span></td>
			<td class="green center">28</td>
			<td class="red lasttd center">103</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11039007,0" class="icomment icommentjs icon16" href="/encyclopedia-of-virology-3rd-edition-5-volume-set-2008-pdf-gooner-t11039007.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/encyclopedia-of-virology-3rd-edition-5-volume-set-2008-pdf-gooner-t11039007.html" class="cellMainLink">Encyclopedia of Virology - 3rd Edition (5 Volume Set) (2008).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="187442921">178.76 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="03 Aug 2015, 20:53">3&nbsp;days</span></td>
			<td class="green center">69</td>
			<td class="red lasttd center">52</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11037272,0" class="icomment icommentjs icon16" href="/spawn-origins-collection-v08-v16-2010-2012-digital-zone-empire-nem-t11037272.html#comment"> <em class="iconvalue">7</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/spawn-origins-collection-v08-v16-2010-2012-digital-zone-empire-nem-t11037272.html" class="cellMainLink">Spawn Origins Collection (v08-v16) (2010-2012) (Digital) (Zone-Empire) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="2690915693">2.51 <span>GB</span></td>
			<td class="center">9</td>
			<td class="center"><span title="03 Aug 2015, 12:20">3&nbsp;days</span></td>
			<td class="green center">74</td>
			<td class="red lasttd center">35</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="rsssign" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11038015,0" class="icomment icommentjs icon16" href="/led-zeppelin-physical-graffiti-2015-deluxe-24-96-hd-flac-t11038015.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/led-zeppelin-physical-graffiti-2015-deluxe-24-96-hd-flac-t11038015.html" class="cellMainLink">Led Zeppelin - Physical Graffiti (2015 Deluxe) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="3036264956">2.83 <span>GB</span></td>
			<td class="center">62</td>
			<td class="center"><span title="03 Aug 2015, 15:41">3&nbsp;days</span></td>
			<td class="green center">150</td>
			<td class="red lasttd center">83</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/children-of-nuggets-original-artyfacts-from-the-second-psychedelic-era-1976-1996-2005-flac-vtwin88cube-t11053742.html" class="cellMainLink">Children Of Nuggets Original Artyfacts From The Second Psychedelic Era 1976-1996 (2005) {FLAC} vtwin88cube</a></div>
			</td>
			<td class="nobr center" data-sort="2358135041">2.2 <span>GB</span></td>
			<td class="center">236</td>
			<td class="center"><span title="06 Aug 2015, 17:46">6&nbsp;hours</span></td>
			<td class="green center">2</td>
			<td class="red lasttd center">228</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11042105,0" class="icomment icommentjs icon16" href="/the-beach-boys-surfin-usa-2015-192-24-hd-flac-t11042105.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-beach-boys-surfin-usa-2015-192-24-hd-flac-t11042105.html" class="cellMainLink">The Beach Boys - Surfin&#039; USA (2015) [192-24 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="2239272566">2.09 <span>GB</span></td>
			<td class="center">41</td>
			<td class="center"><span title="04 Aug 2015, 10:31">2&nbsp;days</span></td>
			<td class="green center">99</td>
			<td class="red lasttd center">95</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11041047,0" class="icomment icommentjs icon16" href="/the-paul-butterfield-blues-band-the-paul-butterfield-blues-band-2015-192-24-hd-flac-t11041047.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-paul-butterfield-blues-band-the-paul-butterfield-blues-band-2015-192-24-hd-flac-t11041047.html" class="cellMainLink">The Paul Butterfield Blues Band - The Paul Butterfield Blues Band (2015) [192-24 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1421300711">1.32 <span>GB</span></td>
			<td class="center">22</td>
			<td class="center"><span title="04 Aug 2015, 06:40">2&nbsp;days</span></td>
			<td class="green center">107</td>
			<td class="red lasttd center">86</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11042144,0" class="icomment icommentjs icon16" href="/the-who-the-who-hits-50-2-cd-deluxe-edition-lossless-flac-tbs-t11042144.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-who-the-who-hits-50-2-cd-deluxe-edition-lossless-flac-tbs-t11042144.html" class="cellMainLink">The Who - The Who Hits 50! (2-CD Deluxe Edition)Lossless-FLAC TBS</a></div>
			</td>
			<td class="nobr center" data-sort="955613230">911.34 <span>MB</span></td>
			<td class="center">63</td>
			<td class="center"><span title="04 Aug 2015, 10:41">2&nbsp;days</span></td>
			<td class="green center">124</td>
			<td class="red lasttd center">58</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11039907,0" class="icomment icommentjs icon16" href="/louis-armstrong-the-complete-rca-victor-recordings-2001-flac-pirate-shovon-t11039907.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/louis-armstrong-the-complete-rca-victor-recordings-2001-flac-pirate-shovon-t11039907.html" class="cellMainLink">Louis Armstrong - The Complete RCA Victor Recordings [2001] [FLAC] [Pirate Shovon]</a></div>
			</td>
			<td class="nobr center" data-sort="1111898205">1.04 <span>GB</span></td>
			<td class="center">77</td>
			<td class="center"><span title="03 Aug 2015, 23:46">3&nbsp;days</span></td>
			<td class="green center">123</td>
			<td class="red lasttd center">39</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11037964,0" class="icomment icommentjs icon16" href="/uper-instrumental-music-collection-flac-t11037964.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/uper-instrumental-music-collection-flac-t11037964.html" class="cellMainLink">$uper Instrumental Music Collection [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="11132736164">10.37 <span>GB</span></td>
			<td class="center">529</td>
			<td class="center"><span title="03 Aug 2015, 15:27">3&nbsp;days</span></td>
			<td class="green center">59</td>
			<td class="red lasttd center">96</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11039867,0" class="icomment icommentjs icon16" href="/the-supremes-gold-2005-flac-t11039867.html#comment"> <em class="iconvalue">7</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-supremes-gold-2005-flac-t11039867.html" class="cellMainLink">The Supremes - Gold (2005) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="824404867">786.21 <span>MB</span></td>
			<td class="center">55</td>
			<td class="center"><span title="03 Aug 2015, 23:33">3&nbsp;days</span></td>
			<td class="green center">118</td>
			<td class="red lasttd center">36</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11047795,0" class="icomment icommentjs icon16" href="/big-country-11-albums-flac-andyt1000-kat-t11047795.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/big-country-11-albums-flac-andyt1000-kat-t11047795.html" class="cellMainLink">Big Country 11 albums FLAC ANDYT1000 KAT</a></div>
			</td>
			<td class="nobr center" data-sort="4830448979">4.5 <span>GB</span></td>
			<td class="center">216</td>
			<td class="center"><span title="05 Aug 2015, 14:31">1&nbsp;day</span></td>
			<td class="green center">58</td>
			<td class="red lasttd center">77</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11038002,0" class="icomment icommentjs icon16" href="/va-bossa-nova-for-lovers-2015-flac-t11038002.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-bossa-nova-for-lovers-2015-flac-t11038002.html" class="cellMainLink">VA - Bossa Nova for Lovers (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="1075072961">1 <span>GB</span></td>
			<td class="center">50</td>
			<td class="center"><span title="03 Aug 2015, 15:38">3&nbsp;days</span></td>
			<td class="green center">74</td>
			<td class="red lasttd center">57</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11039717,0" class="icomment icommentjs icon16" href="/just-wailing-50-masterpieces-by-26-blues-harmonica-heroes-2013-flac-vtwin88cube-t11039717.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/just-wailing-50-masterpieces-by-26-blues-harmonica-heroes-2013-flac-vtwin88cube-t11039717.html" class="cellMainLink">Just Wailing 50 Masterpieces By 26 Blues Harmonica Heroes (2013) {FLAC} vtwin88cube</a></div>
			</td>
			<td class="nobr center" data-sort="445023346">424.41 <span>MB</span></td>
			<td class="center">70</td>
			<td class="center"><span title="03 Aug 2015, 22:42">3&nbsp;days</span></td>
			<td class="green center">100</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11036702,0" class="icomment icommentjs icon16" href="/blank-jones-relax-edition-nine-2cd-2015-flac-mr-p2d-t11036702.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/blank-jones-relax-edition-nine-2cd-2015-flac-mr-p2d-t11036702.html" class="cellMainLink">Blank &amp; Jones - Relax Edition Nine [2CD] (2015) FLAC -[Mr.P2d]</a></div>
			</td>
			<td class="nobr center" data-sort="720038256">686.68 <span>MB</span></td>
			<td class="center">30</td>
			<td class="center"><span title="03 Aug 2015, 10:04">3&nbsp;days</span></td>
			<td class="green center">51</td>
			<td class="red lasttd center">39</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11038025,0" class="icomment icommentjs icon16" href="/va-the-legacy-of-disco-2015-flac-t11038025.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-the-legacy-of-disco-2015-flac-t11038025.html" class="cellMainLink">VA - The Legacy of Disco (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="1662962282">1.55 <span>GB</span></td>
			<td class="center">30</td>
			<td class="center"><span title="03 Aug 2015, 15:44">3&nbsp;days</span></td>
			<td class="green center">54</td>
			<td class="red lasttd center">31</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11048234,0" class="icomment icommentjs icon16" href="/bachman-turner-overdrive-discography-1973-2012-flac-corrected-reseed-t11048234.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bachman-turner-overdrive-discography-1973-2012-flac-corrected-reseed-t11048234.html" class="cellMainLink">Bachman-Turner Overdrive - Discography (1973-2012) [FLAC] (corrected reseed)</a></div>
			</td>
			<td class="nobr center" data-sort="6774291903">6.31 <span>GB</span></td>
			<td class="center">416</td>
			<td class="center"><span title="05 Aug 2015, 15:54">1&nbsp;day</span></td>
			<td class="green center">37</td>
			<td class="red lasttd center">44</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11053754,0" class="icomment icommentjs icon16" href="/bill-withers-just-as-i-am-2015-96-24-hd-flac-t11053754.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bill-withers-just-as-i-am-2015-96-24-hd-flac-t11053754.html" class="cellMainLink">Bill Withers - Just as I Am (2015) [96-24 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="792178172">755.48 <span>MB</span></td>
			<td class="center">29</td>
			<td class="center"><span title="06 Aug 2015, 17:48">6&nbsp;hours</span></td>
			<td class="green center">34</td>
			<td class="red lasttd center">41</td>
        </tr>
			</table>


	<script type="text/javascript">
		if ($.cookie('kat_settings[fpw_column]') != null && $.cookie('kat_settings[fpw_order]')) {
			var colIndex = $.cookie('kat_settings[fpw_column]')
			var ascend = $.cookie('kat_settings[fpw_order]');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			sortTables(colIndex, ascend);
		}
		$('.frontPageWidget .firstr th .fpw-sort').click(function() {
			var colIndex = $(this).parent().index();
			var ascend = !$(this).hasClass('sortedAsc')?1:0;
			$('.fpw-sort').removeClass('sortedAsc sortedDesc');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			updateFPWsettings(colIndex, ascend);
			sortTables(colIndex, ascend);
		});
		function sortTables(colIndex, ascend) {	
			$('.frontPageWidget').each(function() {
				var data = [];
				$('tr:not(.firstr)', $(this)).each(function() {
					var name = $('td:first .cellMainLink', $(this)).text();
					var id = $('td:first .cellMainLink', $(this)).attr('href').match(/.*-t(\d+)\.html/)[1];
					var fileSize = $('td:eq(1)', $(this)).attr('data-sort');
					data.push({
						'col0':name, 'col1':padNum(fileSize), 'col2':padNum($('td:eq(2)',$(this)).text()), 'col3':padNum(id), 'col4':padNum($('td:eq(4)',$(this)).text()), 'col5':padNum($('td:eq(5)',$(this)).text()), 'html':$(this).html()
					});
				});
				data.sort(function(a, b) {
					var x = a['col'+colIndex].toLowerCase(); var y = b['col'+colIndex].toLowerCase();
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
				if (!ascend||ascend=='0') data.reverse();
				$(this).find('tr:not(.firstr)').remove();
				for (var i=0;i<data.length;i++) {
					$(this).append('<tr class="'+(i+1%2==0?'even':'odd')+'">'+data[i].html+'</tr>');
				}
			});
		}
		function padNum(val) {
			val = '0000000000000000'+val;
			return val.substring(val.length-15);
		}
		function updateFPWsettings(t, o) {
			$.cookie('kat_settings[fpw_column]', t, { expires: 365, path: '/' });
			$.cookie('kat_settings[fpw_order]', o, { expires: 365, path: '/' });
		}
	</script>
		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
    
            
        <div id="_119b0a17fab5493361a252d04bf527db"></div>
    
                
    	    <div class="advertising">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <div id="_7063408f1c01d50e0dc2d833186ce962"></div>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/whos-sexiest-actress-ever-no-porn/?unread=16745078">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Who&#039;s The Sexiest Actress Ever (no porn)
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/warrenamber/">warrenamber</a></span></span> <span title="06 Aug 2015, 23:51">1&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/ask-experienced-encoder-here/?unread=16745073">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Ask an Experienced Encoder Here
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/chay.meredith/">chay.meredith</a></span></span> <span title="06 Aug 2015, 23:49">3&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/stitchinwitch-s-upload-page/?unread=16745071">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				StitchinWitch&#039;s upload page
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_verified"><a class="plain" href="/user/StitchinWitch/">StitchinWitch</a></span></span> <span title="06 Aug 2015, 23:49">3&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/music-requests-new-v2/?unread=16745070">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Music Requests - New (V2)
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/sECT0R_zER0/">sECT0R_zER0</a></span></span> <span title="06 Aug 2015, 23:49">3&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/mr-robot-discussion-thread/?unread=16745069">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Mr. Robot Discussion Thread
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/JedeyeMoe/">JedeyeMoe</a></span></span> <span title="06 Aug 2015, 23:47">5&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/kat-s-problems/?unread=16745067">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				KAT&#039;s Problems
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_verified"><a class="plain" href="/user/linfield/">linfield</a></span></span> <span title="06 Aug 2015, 23:46">6&nbsp;min.&nbsp;ago</span></span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/kickasstorrents-is-moving-to-kat-cr-domain/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				KickassTorrents is moving to kat.cr domain
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="24 Apr 2015, 10:13">3&nbsp;months&nbsp;ago</span></span>
	</li>
	<li>
		<a href="/blog/post/happy-torrents-day-2015/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Happy Torrents Day 2015!
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="30 Mar 2015, 10:06">4&nbsp;months&nbsp;ago</span></span>
	</li>
	<li>
		<a href="/blog/post/technical-maintenance-march-17/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Technical maintenance (March, 17)
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="17 Mar 2015, 11:46">4&nbsp;months&nbsp;ago</span></span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/pleasebaby/post/chicken-and-bacon-roll-ups/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> Chicken and Bacon Roll-Ups</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/pleasebaby/">pleasebaby</a> <span title="06 Aug 2015, 16:24">7&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/ScribeOfGoD/post/tic-tac-toe-hosted-for-balcommon/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> Tic-Tac-Toe (hosted for BalCommon)</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/ScribeOfGoD/">ScribeOfGoD</a> <span title="06 Aug 2015, 10:18">13&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/ZombieQueen/post/you-pick-a-title/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> You pick a title.....</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/ZombieQueen/">ZombieQueen</a> <span title="06 Aug 2015, 08:42">15&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/OptimusPr1me/post/of-mice-and-men-a-different-kind-of-hunt/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> Of Mice And Men.. A Different Kind Of Hunt ...</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/OptimusPr1me/">OptimusPr1me</a> <span title="06 Aug 2015, 05:55">17&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/magicpotions/post/happy-b-day-elliott-smith/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> Happy B-Day Elliott Smith</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/magicpotions/">magicpotions</a> <span title="06 Aug 2015, 02:54">20&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/johnno23/post/rip-cecil/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> RIP Cecil.</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/johnno23/">johnno23</a> <span title="05 Aug 2015, 09:36">yesterday</span></span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/fairy%20tale%20complete/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Fairy Tale complete
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/the%20wolf%20of%20wall%20street%20720p/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				the wolf of wall street 720p
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/last%20week%20tonight%20with%20john%20oliver%20s01e20%20hdtv%20x264/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				last week tonight with john oliver s01e20 hdtv x264
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/ted2/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				ted2
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/introduction%20to%20earth%20science/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				introduction to earth science
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/njp/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				njp
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/emmerdale%202006/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				emmerdale 2006
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/alexis%20texas%20ass%20parade/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				alexis texas ass parade
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/telugu%202015/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				telugu 2015
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/opeth%20albert%20hall/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				opeth albert hall
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/ria%20rodriguez/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				ria rodriguez
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		<li>
			<a data-nop href="http://www.torrentreactor.net/" target="_blank" rel="external">
				<span class="itorreact thirdPartIcons"></span>TorrentReactor
			</a>
		</li>
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
		<li><a href="/welcome/">welcome</a></li>
		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <div id="_673e31f53f8166159b8e996c4124765b"></div>
        <div id="_e7050fb15fd39b3e4e99a5be4a57b6ea"></div>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script><script>
 sc('addGlobal', 'pagetype', 'front');
 sc('addSlot', '_60318cd4e8d28f6fb76fe34e9bd9c498');
sc('addSlot', '_39ecb76dd457e5ac33776fdf11500d56');
sc('addSlot', '_277923e5f9d753c5b0630c28e641790c');
sc('addSlot', '_119b0a17fab5493361a252d04bf527db');
sc('addSlot', '_7063408f1c01d50e0dc2d833186ce962', { 'searchQuery': '' });
sc('addSlot', '_673e31f53f8166159b8e996c4124765b');
sc('addSlot', '_e7050fb15fd39b3e4e99a5be4a57b6ea');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>
</body>
</html>
