



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>

                    
                    
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "223-4597552-7109096";
                var ue_id = "1WP1SQRTQKC0R9E8SCCR";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        
    
        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />
    
    <meta property='fb:app_id' content='115109575169727' />
    
    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="1WP1SQRTQKC0R9E8SCCR" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1a-c3-2xl-i-0022bbfc.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-1506377481._CB315509730_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-485555676._CB318510215_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-247421025._CB318510254_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-3978168775._CB318510288_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-1541130848._CB318510255_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
(function(){var d=function(o){return Object.prototype.toString.call(o)==="[object Array]";},g=function(q,p){var o;for(o=0;o<q.length;o++){if(o in q){p.call(null,q[o],o);}}},h=[],k,b,l=false,n=false,f=function(){var o=[],p=[],q={};g(h,function(s){var r="";g(s.dartsite.split("/"),function(t){if(t!==""){if(t in q){}else{q[t]=o.length;o.push(t);}r+="/"+q[t];}});p.push(r);});return{iu_parts:o,enc_prev_ius:p};},c=function(){var o=[];g(h,function(q){var p=[];g(q.sizes,function(r){p.push(r.join("x"));});o.push(p.join("|"));});return o;},m=function(){var o=[];g(h,function(p){o.push(a(p.targeting));});return o.join("|");},a=function(r,o){var q,p,s=[];for(q in r){p=[];for(j=0;j<r[q].length;j++){p.push(encodeURIComponent(r[q][j]));}if(o){s.push(q+"="+encodeURIComponent(p.join(",")));}else{s.push(q+"="+p.join(","));}}return s.join("&");},e=function(){var o=+new Date();if(n){return;}if(!this.readyState||"loaded"===this.readyState){n=true;if(l){imdbads.cmd.push(function(){for(i=0;i<h.length;i++){generic.monitoring.record_metric(h[i].name+".fail",csm.duration(o));}});}}};window.tinygpt={define_slot:function(r,q,o,p){h.push({dartsite:r.replace(/\/$/,""),sizes:q,name:o,targeting:p});},set_targeting:function(o){k=o;},callback:function(q){var r,p={},t,o,s=+new Date();l=false;for(r=0;r<h.length;r++){t=h[r].dartsite;o=h[r].name;if(q[r][t]){p[o]=q[r][t];}else{window.console&&console.error&&console.error("Unable to correlate GPT response for "+o);}}imdbads.cmd.push(function(){for(r=0;r<h.length;r++){ad_utils.slot_events.trigger(h[r].name,"request",{timestamp:b});ad_utils.slot_events.trigger(h[r].name,"tagdeliver",{timestamp:s});}ad_utils.gpt.handle_response(p);});},send:function(){var r=[],q=function(s,t){if(d(t)){t=t.join(",");}if(t){r.push(s+"="+encodeURIComponent(""+t));}},o,p;if(h.length===0){tinygpt.callback({});return;}q("gdfp_req","1");q("correlator",Math.floor(4503599627370496*Math.random()));q("output","json_html");q("callback","tinygpt.callback");q("impl","fifs");q("json_a","1");result=f();q("iu_parts",result.iu_parts);q("enc_prev_ius",result.enc_prev_ius);q("prev_iu_szs",c());q("prev_scp",m());q("cust_params",a(k,true));o=document.createElement("script");p=document.getElementsByTagName("script")[0];o.async=true;o.type="text/javascript";o.src="http://pubads.g.doubleclick.net/gampad/ads?"+r.join("&");o.id="tinygpt";o.onload=o.onerror=o.onreadystatechange=e;l=true;p.parentNode.insertBefore(o,p);b=+new Date();}};})();</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['b'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['1'],
'u': ['927985621476'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-3196534337._CB302695131_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"0f1cd3ba96a9a86402bef7f7053636b689640e01",
"2015-08-04T23%3A57%3A23GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 25357;
generic.days_to_midnight = 0.2934837937355042;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-1582251138._CB317085808_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'b']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm === undefined || window.generic === undefined || window.consoleLog === undefined)) {
                    if (console !== undefined && console.log !== undefined) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=927985621476;ord=927985621476?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=927985621476?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=927985621476?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
onsubmit="(new Image()).src='/rg/SEARCH-BOX/HEADER/images/b.gif?link=/find';"
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/imdbpicks/monthly-tv-picks/?ref_=nv_tp_tvpicks_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/search/title?count=100&title_type=feature,tv_series,tv_movie&explore=title_type,genres,year,countries&ref_=nv_ch_mm_1"
>Popular Movies & TV</a></li>
                        <li><a href="/chart/?ref_=nv_ch_cht_2"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_3"
>Oscar Winners</a></li>
                        <li><a href="/chart/top?ref_=nv_ch_250_4"
>Top 250</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_5"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/imdbpicks/monthly-tv-picks/?ref_=nv_tvv_picks_1"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_2"
>On Tonight</a></li>
                        <li><a href="/list/ls074093523/?ref_=nv_tvv_wn_3"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_4"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/academymuseum/?ref_=nv_sf_am_1"
>Academy Museum</a></li>
                        <li><a href="/imdbpicks/?ref_=nv_sf_pks_2"
>IMDb Picks</a></li>
                        <li><a href="/best-of/?ref_=nv_sf_bo_3"
>Best of 2014</a></li>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_4"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/whattowatch/?ref_=nv_sf_wtw_5"
>What to Watch</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=08-04&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_2"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_3"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_4"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_5"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_6"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_7"
>Road to the Emmys</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_8"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_9"
>Toronto Film Festival</a></li>
                        <li><a href="/awards-central/?ref_=nv_ev_all_10"
>More Popular Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni58862858/?ref_=nv_nw_tn_1"
> Summer Box Office Set to Cool Down
</a><br />
                        <span class="time">9 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni58863030/?ref_=nv_nw_tn_2"
> New Woody Allen Movie Adds Jeannie Berlin, Corey Stoll & More
</a><br />
                        <span class="time">7 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni58863098/?ref_=nv_nw_tn_3"
> âThe Hobbitâ Trilogy Extended Edition Heading to Theaters This Fall
</a><br />
                        <span class="time">8 hours ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/games/guess?ref_=nv_cm_qz_3"
>Quiz Game</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_4"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5> 
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0110357/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTY3MDA0NzA2MV5BMl5BanBnXkFtZTcwODQ2NTI3Ng@@._V1._SY450_CR100,40,410,315_.jpg",
            titleYears : "1994",
            rank : 54,
                    headline : "The Lion King"
    },
    nameAd : {
            clickThru : "/name/nm0000204/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMjI2NDQxNzUwM15BMl5BanBnXkFtZTcwNDg0MzcyNw@@._V1._SX250_CR0,0,250,315_.jpg",
            rank : 151,
            headline : "Natalie Portman"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYiRDFg5t0eQT-VAxKQbndQSzZ1T0joO1rXN-I5WCCPRaGY1eo0JkasR-OXvRtASWBwvlelyaU3%0D%0ASe9f1heaibiQfmQrqAzhmGOzI8UjNkYyh656uJGqIZ8KXM2SVunbVsd1fzBq1ZxV8dn2zlhHQxBF%0D%0AQixIcsR9jgIPMVL7CuAA4BL6SmETENp01fVoZTsl9713zdoeMNMAIWesXRUXfSfR-Q%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYpMh1TPZaRl6kHD_mt5RSa3A8DAh3cCtdufVasb5Jrxk66LEJJfpTIz3NFGJXBV-9TL3DYxLf7%0D%0AeGsl7Q53siLzWeL7kr0w4BJhKoWDjM6bcJFwdWudNlSaizdA-_5AxiC04UKwAvw0_HKZ4nhcMDE-%0D%0AxcbpUK7mzVZ8kYR1ovXTKPVq-Eb4_9n89tRwsEIGLxgq%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYjWNH9sxIsV6AnS2j9R5GMX0744urrbuGkaluIJngePIqmyKqnORIGVPxGvmNB9OHdducFMHZZ%0D%0AH1LM2S6ttSAaH4G1WHNkvLCIID1ll9bQT1JyF-Hni5PB5kOHnOrBHOceSTC3p2FlABRu7qon5UsU%0D%0A1UyVF32oOs8eFIo6Fi_TzH2dMjUkNkuakfU0zdvoqdPHnBbygxMX2Na17Zoj5iLFIA%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=927985621476;ord=927985621476?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>
                    <div id="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=927985621476;ord=927985621476?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	

                    
                    

<div id="content-2-wide">
    <div id="main">
                    
        <div class="heroWidget">
        <span class="ab_widget"
        >
            <div class="ab_hero">
<div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi788181785?ref_=hm_hp_i_1" data-video="vi788181785" data-source="bylist" data-id="ls002653141" data-rid="1WP1SQRTQKC0R9E8SCCR" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="NASA offers a declassified look at the psychological testing involved in preparing the ARES 3 crew for their journey to Mars." alt="NASA offers a declassified look at the psychological testing involved in preparing the ARES 3 crew for their journey to Mars." src="http://ia.media-imdb.com/images/M/MV5BMTcwMjI2NzM2MF5BMl5BanBnXkFtZTgwNDkyNTI5NTE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTcwMjI2NzM2MF5BMl5BanBnXkFtZTgwNDkyNTI5NTE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="NASA offers a declassified look at the psychological testing involved in preparing the ARES 3 crew for their journey to Mars." title="NASA offers a declassified look at the psychological testing involved in preparing the ARES 3 crew for their journey to Mars." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="NASA offers a declassified look at the psychological testing involved in preparing the ARES 3 crew for their journey to Mars." title="NASA offers a declassified look at the psychological testing involved in preparing the ARES 3 crew for their journey to Mars." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt3659388/?ref_=hm_hp_cap_pri_1" > The Martian </a> </div> </div> <div class="secondary ellipsis"> Ares 3: The Right Stuff </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi653964057?ref_=hm_hp_i_2" data-video="vi653964057" data-source="bylist" data-id="ls056131825" data-rid="1WP1SQRTQKC0R9E8SCCR" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="Set in a city where dreams and fortunes are made every day, along with hundreds of new cases of herpes. We'll meet a fresh batch of real Hotwives loving and clawing their way through the town with smiles on their faces - cause the Botox won't let them frown. So brace yourself for double the drama, triple the heartbreak and seven times the plastic surgery." alt="Set in a city where dreams and fortunes are made every day, along with hundreds of new cases of herpes. We'll meet a fresh batch of real Hotwives loving and clawing their way through the town with smiles on their faces - cause the Botox won't let them frown. So brace yourself for double the drama, triple the heartbreak and seven times the plastic surgery." src="http://ia.media-imdb.com/images/M/MV5BMjM3MzEyNzc4Ml5BMl5BanBnXkFtZTgwMTM5ODU0NjE@._V1_SX201_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjM3MzEyNzc4Ml5BMl5BanBnXkFtZTgwMTM5ODU0NjE@._V1_SX201_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Set in a city where dreams and fortunes are made every day, along with hundreds of new cases of herpes. We'll meet a fresh batch of real Hotwives loving and clawing their way through the town with smiles on their faces - cause the Botox won't let them frown. So brace yourself for double the drama, triple the heartbreak and seven times the plastic surgery." title="Set in a city where dreams and fortunes are made every day, along with hundreds of new cases of herpes. We'll meet a fresh batch of real Hotwives loving and clawing their way through the town with smiles on their faces - cause the Botox won't let them frown. So brace yourself for double the drama, triple the heartbreak and seven times the plastic surgery." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Set in a city where dreams and fortunes are made every day, along with hundreds of new cases of herpes. We'll meet a fresh batch of real Hotwives loving and clawing their way through the town with smiles on their faces - cause the Botox won't let them frown. So brace yourself for double the drama, triple the heartbreak and seven times the plastic surgery." title="Set in a city where dreams and fortunes are made every day, along with hundreds of new cases of herpes. We'll meet a fresh batch of real Hotwives loving and clawing their way through the town with smiles on their faces - cause the Botox won't let them frown. So brace yourself for double the drama, triple the heartbreak and seven times the plastic surgery." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt4633166/?ref_=hm_hp_cap_pri_2" > "The Hotwives of Las Vegas" </a> </div> </div> <div class="secondary ellipsis"> Series Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1408938777?ref_=hm_hp_i_3" data-video="vi1408938777" data-source="bylist" data-id="ls002922459" data-rid="1WP1SQRTQKC0R9E8SCCR" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="Mayor Nick Wasicsko took office in 1987 during Yonkers' worst crisis when federal courts ordered public housing built in the white, middle class side of town, dividing the city in a bitter battle fueled by fear, racism, murder and politics." alt="Mayor Nick Wasicsko took office in 1987 during Yonkers' worst crisis when federal courts ordered public housing built in the white, middle class side of town, dividing the city in a bitter battle fueled by fear, racism, murder and politics." src="http://ia.media-imdb.com/images/M/MV5BNjUwNTk2NTk5MF5BMl5BanBnXkFtZTgwMzY0OTU0NjE@._V1_SX201_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjUwNTk2NTk5MF5BMl5BanBnXkFtZTgwMzY0OTU0NjE@._V1_SX201_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Mayor Nick Wasicsko took office in 1987 during Yonkers' worst crisis when federal courts ordered public housing built in the white, middle class side of town, dividing the city in a bitter battle fueled by fear, racism, murder and politics." title="Mayor Nick Wasicsko took office in 1987 during Yonkers' worst crisis when federal courts ordered public housing built in the white, middle class side of town, dividing the city in a bitter battle fueled by fear, racism, murder and politics." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Mayor Nick Wasicsko took office in 1987 during Yonkers' worst crisis when federal courts ordered public housing built in the white, middle class side of town, dividing the city in a bitter battle fueled by fear, racism, murder and politics." title="Mayor Nick Wasicsko took office in 1987 during Yonkers' worst crisis when federal courts ordered public housing built in the white, middle class side of town, dividing the city in a bitter battle fueled by fear, racism, murder and politics." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2492296/?ref_=hm_hp_cap_pri_3" > "Show Me a Hero" </a> </div> </div> <div class="secondary ellipsis"> Series Trailer </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/trailers?ref_=hm_hp_sm" class="position_bottom supplemental" > Browse more trailers </a> </p>    </div>

        </span>
        </div>
                    
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58862858?ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTk0OTMyMDA0OF5BMl5BanBnXkFtZTgwMzY5NTkzNTE@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58862858?ref_=hm_nw_tp1"
class="headlines" >Summer Box Office Set to Cool Down</a>
    <div class="infobar">
            <span class="text-muted">9 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?ref_=hm_nw_tp1_src"
>Variety - Film News</a></span>
    </div>
                                </div>
<p> The summer box office has peaked. Nothing, not âFantastic Fourâ nor âStraight Outta Compton,â will allow it to suck in the rarefied air it was breathing in May and June. Over the next few weeks, domestic ticket sales will struggle to keep pace with the previous yearâs numbers. Although the summer ...                                        <span class="nobr"><a href="/news/ni58862858?ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58863030?ref_=hm_nw_tp2"
class="headlines" >New Woody Allen Movie Adds Jeannie Berlin, Corey Stoll & More</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?ref_=hm_nw_tp2_src"
>Deadline</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58863098?ref_=hm_nw_tp3"
class="headlines" >âThe Hobbitâ Trilogy Extended Edition Heading to Theaters This Fall</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000151?ref_=hm_nw_tp3_src"
>Slash Film</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58862314?ref_=hm_nw_tp4"
class="headlines" >Paolo Sorrentinoâs âYoung Popeâ Rounds Out Cast With James Cromwell And Slew Of Top International Talent</a>
    <div class="infobar">
            <span class="text-muted">13 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?ref_=hm_nw_tp4_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58862911?ref_=hm_nw_tp5"
class="headlines" >'Starred Up' Director David Mackenzie Now Shooting 'Comancheria' With Chris Pine, Ben Foster, And Jeff Bridges</a>
    <div class="infobar">
            <span class="text-muted">9 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0011867?ref_=hm_nw_tp5_src"
>The Playlist</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58862949?ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTY1NzIxNzkzM15BMl5BanBnXkFtZTgwMzAzNjIzNjE@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58862949?ref_=hm_nw_mv1"
class="headlines" >Meryl Streep May Not Get an Oscar Campaign for âRicki and the Flashâ</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?ref_=hm_nw_mv1_src"
>Variety - Film News</a></span>
    </div>
                                </div>
<p> Sony Picturesâ â<a href="/title/tt3623726?ref_=hm_nw_mv1_lk1">Ricki and the Flash</a>â premiered in New York on Monday night, with stars <a href="/name/nm0000658?ref_=hm_nw_mv1_lk2">Meryl Streep</a>, <a href="/name/nm0336701?ref_=hm_nw_mv1_lk3">Mamie Gummer</a> and <a href="/name/nm0819782?ref_=hm_nw_mv1_lk4">Rick Springfield</a>. The musical dramedy, which opens on Friday, had a budget of $18 million, and shot for six weeks in Westchester last year. Producer <a href="/name/nm0686887?ref_=hm_nw_mv1_lk5">Marc Platt</a> said he approached ...                                        <span class="nobr"><a href="/news/ni58862949?ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58862911?ref_=hm_nw_mv2"
class="headlines" >'Starred Up' Director David Mackenzie Now Shooting 'Comancheria' With Chris Pine, Ben Foster, And Jeff Bridges</a>
    <div class="infobar">
            <span class="text-muted">9 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0011867?ref_=hm_nw_mv2_src"
>The Playlist</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58863435?ref_=hm_nw_mv3"
class="headlines" >Charlize Theron Knows Nothing about a 'Mad Max: Fury Road' Sequel</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000055?ref_=hm_nw_mv3_src"
>Rope of Silicon</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58862890?ref_=hm_nw_mv4"
class="headlines" >Stephen Tobolowsky Concert Pic âThe Primary Instinctâ Goes To FilmBuff; Jessica Gomes Joins Bruce Willis Action Comedy</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?ref_=hm_nw_mv4_src"
>Deadline</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58863061?ref_=hm_nw_mv5"
class="headlines" >Donna Gigliottiâs Levantine Films options John Niven satire</a>
    <div class="infobar">
            <span class="text-muted">6 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0056136?ref_=hm_nw_mv5_src"
>ScreenDaily</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58863774?ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjIzMzUwNTAzNV5BMl5BanBnXkFtZTcwNDI4ODg3Ng@@._V1_SY150_CR9,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58863774?ref_=hm_nw_tv1"
class="headlines" >Miss Piggy and Kermit the Frog Have Broken Up!</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?ref_=hm_nw_tv1_src"
>E! Online</a></span>
    </div>
                                </div>
<p><a href="/name/nm1459387?ref_=hm_nw_tv1_lk1">Maroon 5</a> was right: this summer's gonna hurt like a mother--ker. Amid high-profile breakups (<a href="/name/nm0000255?ref_=hm_nw_tv1_lk2">Ben Affleck</a> and <a href="/name/nm0004950?ref_=hm_nw_tv1_lk3">Jennifer Garner</a>; <a href="/name/nm0569962?ref_=hm_nw_tv1_lk4">Gavin Rossdale</a> and <a href="/name/nm0005461?ref_=hm_nw_tv1_lk5">Gwen Stefani</a>), <a href="/title/tt1204342?ref_=hm_nw_tv1_lk6">The Muppets</a>' <a href="/name/nm1129383?ref_=hm_nw_tv1_lk7">Miss Piggy</a> announced via social media Tuesday that she is no longer with <a href="/name/nm0001345?ref_=hm_nw_tv1_lk8">Kermit the Frog</a>. Naturally, the spotlight-loving ...                                        <span class="nobr"><a href="/news/ni58863774?ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58862314?ref_=hm_nw_tv2"
class="headlines" >Paolo Sorrentinoâs âYoung Popeâ Rounds Out Cast With James Cromwell And Slew Of Top International Talent</a>
    <div class="infobar">
            <span class="text-muted">13 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?ref_=hm_nw_tv2_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58862844?ref_=hm_nw_tv3"
class="headlines" >Joel McHale Confirms That Season 6 of Community Was Its Last (For Real)</a>
    <div class="infobar">
            <span class="text-muted">9 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?ref_=hm_nw_tv3_src"
>Vulture</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58863552?ref_=hm_nw_tv4"
class="headlines" >UnREAL and the Pressures of a Sophomore Season</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?ref_=hm_nw_tv4_src"
>Vulture</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58863412?ref_=hm_nw_tv5"
class="headlines" >Miranda Mayo Joins ABCâs âBlood And Oilâ In Recasting</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004913?ref_=hm_nw_tv5_src"
>Deadline TV</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58862679?ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BOTQ3MDc1MTI2Nl5BMl5BanBnXkFtZTcwMzYxMDgzOQ@@._V1_SY150_CR18,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58862679?ref_=hm_nw_cel1"
class="headlines" >Amy Schumer Shares Details From Her Vacation With Jennifer Lawrence, Thinks She Looks Like the Actress' Coach</a>
    <div class="infobar">
            <span class="text-muted">10 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?ref_=hm_nw_cel1_src"
>E! Online</a></span>
    </div>
                                </div>
<p><a href="/name/nm2154960?ref_=hm_nw_cel1_lk1">Amy Schumer</a>'s star is on the rise. Her feature film debut, <a href="/title/tt3152624?ref_=hm_nw_cel1_lk2">Trainwreck</a>, is a box office hit, and her <a href="/company/co0029768?ref_=hm_nw_cel1_lk3">Comedy Central</a> series, <a href="/title/tt2578508?ref_=hm_nw_cel1_lk4">Inside Amy Schumer</a>, is nominated for seven Primetime Emmy Awards. As such, <a href="/title/tt0115147?ref_=hm_nw_cel1_lk5">Jon Stewart</a> asked his Daily Show guest Monday night, "Are you in the stratosphere now? I see a ...                                        <span class="nobr"><a href="/news/ni58862679?ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58862667?ref_=hm_nw_cel2"
class="headlines" >Rihanna and Lewis Hamilton Spark Romance Rumors at Carnival in BarbadosâSee the Pic!</a>
    <div class="infobar">
            <span class="text-muted">11 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?ref_=hm_nw_cel2_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58862168?ref_=hm_nw_cel3"
class="headlines" >'Real Housewives' Star Kim Richards Arrested for Shoplifting</a>
    <div class="infobar">
            <span class="text-muted">18 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000001?ref_=hm_nw_cel3_src"
>The Hollywood Reporter</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58862880?ref_=hm_nw_cel4"
class="headlines" >Coleen Gray, Star of âThe Killingâ and âKiss of Death,â Dies at 92</a>
    <div class="infobar">
            <span class="text-muted">9 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?ref_=hm_nw_cel4_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58864807?ref_=hm_nw_cel5"
class="headlines" >Ray Rice Speaks Out About His Domestic Violence: I Can Understand Some People Will Never Forgive MeâWatch Now</a>
    <div class="infobar">
            <span class="text-muted">42 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?ref_=hm_nw_cel5_src"
>E! Online</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <h3>IMDb Snapshot</h3> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm4108445952/rg1389009664?ref_=hm_snp_i_1" > <img itemprop="image" class="pri_image" title="Ron Perlman" alt="Ron Perlman" src="http://ia.media-imdb.com/images/M/MV5BMzQ0MDQzODk3OF5BMl5BanBnXkFtZTgwMzc2MTU0NjE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzQ0MDQzODk3OF5BMl5BanBnXkFtZTgwMzc2MTU0NjE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm4108445952/rg1389009664?ref_=hm_snp_cap_pri_1" > Summer TCA 2015: Day 7 </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3772901632/rg1372232448?ref_=hm_snp_i_2" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTU2MzgxMTU4OF5BMl5BanBnXkFtZTgwNDE2MTU0NjE@._V1._CR342,0,1365,1365_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU2MzgxMTU4OF5BMl5BanBnXkFtZTgwNDE2MTU0NjE@._V1._CR342,0,1365,1365_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm3772901632/rg1372232448?ref_=hm_snp_cap_pri_2" > Exclusive Photos: <i>Cooties</i> </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1223420160/rg1472895744?ref_=hm_snp_i_3" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTkwMDAyNTQ5MV5BMl5BanBnXkFtZTgwODcyNzQ0NjE@._V1._CR30,0,666,666_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTkwMDAyNTQ5MV5BMl5BanBnXkFtZTgwODcyNzQ0NjE@._V1._CR30,0,666,666_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm1223420160/rg1472895744?ref_=hm_snp_cap_pri_3" > <i>The Intern</i> </a> </div> </div> </div> </div> </div> </div> </div>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
                <div class="ab_borntoday">
<span class="ninja_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=8-4&ref_=hm_brn_hd" > <h3>Born Today</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0817844?ref_=hm_brn_i_1" > <img itemprop="image" class="pri_image" title="Abigail Spencer" alt="Abigail Spencer" src="http://ia.media-imdb.com/images/M/MV5BMTUzMTI2Njk3MV5BMl5BanBnXkFtZTcwNjc5MTg4OQ@@._V1_SY172_CR94,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUzMTI2Njk3MV5BMl5BanBnXkFtZTcwNjc5MTg4OQ@@._V1_SY172_CR94,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0817844?ref_=hm_brn_cap_pri_lk1_1">Abigail Spencer</a> (34) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1620783?ref_=hm_brn_i_2" > <img itemprop="image" class="pri_image" title="Meghan Markle" alt="Meghan Markle" src="http://ia.media-imdb.com/images/M/MV5BMjExNzU3ODY3NF5BMl5BanBnXkFtZTcwMjI0MzM0OA@@._V1_SY172_CR25,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjExNzU3ODY3NF5BMl5BanBnXkFtZTcwMjI0MzM0OA@@._V1_SY172_CR25,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1620783?ref_=hm_brn_cap_pri_lk1_2">Meghan Markle</a> (34) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0819850?ref_=hm_brn_i_3" > <img itemprop="image" class="pri_image" title="Cole Sprouse" alt="Cole Sprouse" src="http://ia.media-imdb.com/images/M/MV5BMTcxMzY3OTI3NF5BMl5BanBnXkFtZTcwNjI5MjkyMw@@._V1_SY172_CR4,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTcxMzY3OTI3NF5BMl5BanBnXkFtZTcwNjI5MjkyMw@@._V1_SY172_CR4,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0819850?ref_=hm_brn_cap_pri_lk1_3">Cole Sprouse</a> (23) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0000671?ref_=hm_brn_i_4" > <img itemprop="image" class="pri_image" title="Billy Bob Thornton" alt="Billy Bob Thornton" src="http://ia.media-imdb.com/images/M/MV5BMTcxOTUwMDI0OF5BMl5BanBnXkFtZTcwNjIyNzkzNw@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTcxOTUwMDI0OF5BMl5BanBnXkFtZTcwNjIyNzkzNw@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0000671?ref_=hm_brn_cap_pri_lk1_4">Billy Bob Thornton</a> (60) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1950086?ref_=hm_brn_i_5" > <img itemprop="image" class="pri_image" title="Greta Gerwig" alt="Greta Gerwig" src="http://ia.media-imdb.com/images/M/MV5BNDE5MTIxMTMzMV5BMl5BanBnXkFtZTcwMjMxMDYxOQ@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNDE5MTIxMTMzMV5BMl5BanBnXkFtZTcwMjMxMDYxOQ@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1950086?ref_=hm_brn_cap_pri_lk1_5">Greta Gerwig</a> (32) </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/search/name?refine=birth_monthday&birth_monthday=8-4&ref_=hm_brn_sm" class="position_bottom supplemental" > See all birthdays </a> </p>        </div>

        </span>
        </div>
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/title/tt4427060/?ref_=hm_if_ss_hd" > <h3>Indie Focus: <i>Seashore</i></h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1321069312/tt4427060?ref_=hm_if_ss_i_1" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTQ4NzgwMDI3OF5BMl5BanBnXkFtZTgwMzUyMzEyNjE@._V1._SY250_SX170_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ4NzgwMDI3OF5BMl5BanBnXkFtZTgwMzUyMzEyNjE@._V1._SY250_SX170_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2173285145?ref_=hm_if_ss_i_2" data-video="vi2173285145" data-rid="1WP1SQRTQKC0R9E8SCCR" data-type="single" class="video-colorbox" data-refsuffix="hm_if_ss" data-ref="hm_if_ss_i_2"> <img itemprop="image" class="pri_image" title="Seashore (2015)" alt="Seashore (2015)" src="http://ia.media-imdb.com/images/M/MV5BNTg4NjY1NzEyM15BMl5BanBnXkFtZTgwMzkxNDU0NjE@._V1._CR0,100,2048,1050_SY250_SX444_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNTg4NjY1NzEyM15BMl5BanBnXkFtZTgwMzkxNDU0NjE@._V1._CR0,100,2048,1050_SY250_SX444_AL_UY500_UX888_AL_.jpg" /> <img alt="Seashore (2015)" title="Seashore (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Seashore (2015)" title="Seashore (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">Friends since childhood, Brazilian teenagers Martin and Tomaz have since grown apart. When Martin's grandfather dies, Tomaz journeys with him on a mission to his estranged family's coastal town where the boys are challenged to sort out for themselves the meanings of friendship, independence, and love in a suddenly adult world.</p> <p class="seemore"> <a href="/title/tt4427060/?ref_=hm_if_ss_sm" class="position_bottom supplemental" > Learn more about <i>Seashore</i> </a> </p>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_trivia">
<span class="ninja_header"> <span class="oneline"> <a href="/title/tt0460627/trivia?item=tr1116451&ref_=hm_trv_hd" > <h3>Did You Know?</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt0460627?ref_=hm_trv_i_1" > <img itemprop="image" class="pri_image" title="Bones (2005-)" alt="Bones (2005-)" src="http://ia.media-imdb.com/images/M/MV5BODU0NzA1MTEwN15BMl5BanBnXkFtZTcwOTI1NjI5MQ@@._V1_SY132_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BODU0NzA1MTEwN15BMl5BanBnXkFtZTcwOTI1NjI5MQ@@._V1_SY132_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" > <strong class="text-large"><a href="/title/tt0460627?ref_=hm_trv_lk1">Bones</a></strong> <p class="blurb">Both <a href="/name/nm0862328?ref_=hm_trv_lk1">T.J. Thyne</a> and <a href="/name/nm0853231?ref_=hm_trv_lk2">Tamara Taylor</a> have appeared on <a href="/title/tt0364845?ref_=hm_trv_lk3">NCIS</a> (2003) but on different episodes; 'Michaela Conlin' appeared on the series from which NCIS was spun off, <a href="/title/tt0112022?ref_=hm_trv_lk4">JAG</a> (1995)</p></div> </div> </div> <p class="seemore"> <a href="/title/tt0460627/trivia?item=tr1116451&ref_=hm_trv_sm" class="position_bottom supplemental" > See more trivia </a> </p>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;cx7ddsnelYQ&quot;}"
        >
            <div class="ab_poll poll">
<span class="ninja_header"> <span class="oneline"> <a href="/poll/cx7ddsnelYQ/?ref_=hm_poll_hd" > <h3>Poll: Favorite Superhero Origin Movie</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?ref_=hm_poll_mp">More Polls</a></h4> </span> </span> <p class="blurb">Which superhero origin movie is your favorite? <a href="http://www.imdb.com/board/bd0000088/thread/245251634/?ref_=hm_poll_lk1">Discuss here</a> after voting! Mark which ones you've seen <a href="http://www.imdb.com/seen/ls074562652/?ref_=hm_poll_lk2">here</a>.</p> <p class="seemore"> <a href="/poll/cx7ddsnelYQ/?ref_=hm_poll_sm" class="position_blurb supplemental" > Vote now </a> </p> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:31.952%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/cx7ddsnelYQ/?ref_=hm_poll_i_1" > <img itemprop="image" class="pri_image" title="The Amazing Spider-Man (2012)" alt="The Amazing Spider-Man (2012)" src="http://ia.media-imdb.com/images/M/MV5BMjMyOTM4MDMxNV5BMl5BanBnXkFtZTcwNjIyNzExOA@@._V1_SX233_CR0,0,233,345_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjMyOTM4MDMxNV5BMl5BanBnXkFtZTcwNjIyNzExOA@@._V1_SX233_CR0,0,233,345_AL_UY690_UX466_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:31.952%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/cx7ddsnelYQ/?ref_=hm_poll_i_2" > <img itemprop="image" class="pri_image" title="Batman Begins (2005)" alt="Batman Begins (2005)" src="http://ia.media-imdb.com/images/M/MV5BNTM3OTc0MzM2OV5BMl5BanBnXkFtZTYwNzUwMTI3._V1_SX233_CR0,0,233,345_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNTM3OTc0MzM2OV5BMl5BanBnXkFtZTYwNzUwMTI3._V1_SX233_CR0,0,233,345_AL_UY690_UX466_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:31.952%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/cx7ddsnelYQ/?ref_=hm_poll_i_3" > <img itemprop="image" class="pri_image" title="Captain America: The First Avenger (2011)" alt="Captain America: The First Avenger (2011)" src="http://ia.media-imdb.com/images/M/MV5BMTYzOTc2NzU3N15BMl5BanBnXkFtZTcwNjY3MDE3NQ@@._V1_SX233_CR0,0,233,345_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTYzOTc2NzU3N15BMl5BanBnXkFtZTcwNjY3MDE3NQ@@._V1_SX233_CR0,0,233,345_AL_UY690_UX466_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
    </div>
    <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=927985621476;ord=927985621476?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=927985621476?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=927985621476?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	
                    
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/movies-in-theaters/?ref_=hm_otw_hd" > <h3>Opening This Week</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1502712"></div> <div class="title"> <a href="/title/tt1502712?ref_=hm_otw_t0"> Fantastic Four </a> <span class="secondary-text"></span> </div> <div class="action"> <a href="/showtimes/title/tt1502712?ref_=hm_otw_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2872750"></div> <div class="title"> <a href="/title/tt2872750?ref_=hm_otw_t1"> Shaun the Sheep Movie </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3623726"></div> <div class="title"> <a href="/title/tt3623726?ref_=hm_otw_t2"> Ricki and the Flash </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4178092"></div> <div class="title"> <a href="/title/tt4178092?ref_=hm_otw_t3"> The Gift </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3813310"></div> <div class="title"> <a href="/title/tt3813310?ref_=hm_otw_t4"> Cop Car </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3172532"></div> <div class="title"> <a href="/title/tt3172532?ref_=hm_otw_t5"> The Diary of a Teenage Girl </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1640718"></div> <div class="title"> <a href="/title/tt1640718?ref_=hm_otw_t6"> Kahlil Gibran's The Prophet </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3819668"></div> <div class="title"> <a href="/title/tt3819668?ref_=hm_otw_t7"> Dragon Ball Z: Resurrection 'F' </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2402101"></div> <div class="title"> <a href="/title/tt2402101?ref_=hm_otw_t8"> Dark Places </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> </div> </div> </div> <div><a href="/movies-in-theaters/?ref_=hm_otw_sm" ><p class="seemore position_bottom">See more opening this week</p></a></div>
        </span>
        </div>
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
            <div class="ab_ninja showtimesMessagingWidget">
<p class="blurb"><a href="/showtimes?ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p>    </div>

        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/chart/?ref_=hm_cht_hd" > <h3>Now Playing (Box Office)</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2381249"></div> <div class="title"> <a href="/title/tt2381249?ref_=hm_cht_t0"> Mission: Impossible - Rogue Nation </a> <span class="secondary-text">$55.5M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1524930"></div> <div class="title"> <a href="/title/tt1524930?ref_=hm_cht_t1"> Vacation </a> <span class="secondary-text">$14.7M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt0478970"></div> <div class="title"> <a href="/title/tt0478970?ref_=hm_cht_t2"> Ant-Man </a> <span class="secondary-text">$12.8M</span> </div> <div class="action"> <a href="/showtimes/title/tt0478970?ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2293640"></div> <div class="title"> <a href="/title/tt2293640?ref_=hm_cht_t3"> Minions </a> <span class="secondary-text">$12.4M</span> </div> <div class="action"> <a href="/showtimes/title/tt2293640?ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2120120"></div> <div class="title"> <a href="/title/tt2120120?ref_=hm_cht_t4"> Pixels </a> <span class="secondary-text">$10.5M</span> </div> <div class="action"> <a href="/showtimes/title/tt2120120?ref_=hm_cht_gs"> Showtimes </a> </div> </div> </div> </div> </div> <div><a href="/chart/?ref_=hm_cht_sm" ><p class="seemore position_bottom">See more box office results</p></a></div>
        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/movies-coming-soon/?ref_=hm_cs_hd" > <h3>Coming Soon</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1638355"></div> <div class="title"> <a href="/title/tt1638355?ref_=hm_cs_t0"> The Man from U.N.C.L.E. </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1398426"></div> <div class="title"> <a href="/title/tt1398426?ref_=hm_cs_t1"> Straight Outta Compton </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2872462"></div> <div class="title"> <a href="/title/tt2872462?ref_=hm_cs_t2"> Mistress America </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1634003"></div> <div class="title"> <a href="/title/tt1634003?ref_=hm_cs_t3"> Underdogs </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <div><a href="/movies-coming-soon/?ref_=hm_cs_sm" ><p class="seemore position_bottom">See more coming soon</p></a></div>
        </span>
        </div>
                    
                    
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
                <h3>Follow Us On Twitter</h3>
    <iframe class="twitter-frame" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3776329187._CB305760180_.html#{"screen-name":"IMDb","height":"500px","width":"300px"}'></iframe>

 

        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
            <div id="fb-root"></div>

    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    
    <h3>Find Us On Facebook</h3>
    <div class="fb-like-box" data-href="http://www.facebook.com/imdb" data-width="285" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
        <div class="aux-content-widget-2 sticky-widget">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/imdbpicks/?ref_=hm_pks_hd" > <h3>IMDb Picks: August</h3> </a> </span> </span> <p class="blurb">Visit our IMDb Picks section to see our recommendations of movies and TV shows coming out in August.</p> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/?ref_=hm_pks_i_1" > <img itemprop="image" class="pri_image" title="Fear the Walking Dead (2015-)" alt="Fear the Walking Dead (2015-)" src="http://ia.media-imdb.com/images/M/MV5BNzkxNTY4MzUyM15BMl5BanBnXkFtZTgwODQwNzUzNjE@._V1_SY525_CR0,0,700,525_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNzkxNTY4MzUyM15BMl5BanBnXkFtZTgwODQwNzUzNjE@._V1_SY525_CR0,0,700,525_AL_UY1050_UX1400_AL_.jpg" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/imdbpicks/?ref_=hm_pks_cap_pri_1" > Fear the Walking Dead </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/imdbpicks/?ref_=hm_pks_sm" class="position_bottom supplemental" > Visit the IMDb Picks section </a> </p>    </div>

        </span>
        </div>
    </div>
</div>

                    
                    


                   <br class="clear" />
                </div>





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYmjYWqTCruVzpmM2n8nY3QFjScM2po-bfLfGtIza_AraXex46aTz8RR_Bj7juKyJFV5m6GUqc3%0D%0ATNUfJPf7pZkMhtRy482CB-WpEPRINltEcpwcO9yZkGnjHwja_wzzbbzr5cJcdWYHU__6zOTkcYLj%0D%0AIXH4yipyrOUkmFiaVsJ5Lctflc3oVW0bCSS0KRVaOjNPhXf1Zf-2vRiQYKkAOzrDs_7TpXroNJRh%0D%0AFzTEjmTxgMk%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYgxLz2DRu8RllwjGjeDVv92UugbfS7lUmnu3HgqYadZeCNcFbw5MS1GtWpRyZFQCbPQWRuWn8N%0D%0AKvSdZPD8jAWo5JbEH6O469Pa1z-PK8awjlVBSe2-hRwkpl10FWcHEILWDLP9OpbpJe26srio_DSl%0D%0AB2B__Z-4ETGpBRr8C3AOiH97on_pyqDgYx1pNRH5Z5VBQN5thmpvcgFBKh5su_Cj1Q%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYlfsngEQE7ZPgO7ncRSsvitQK--ZslzPIt8cBo5qA9LpI-YRuE9km718uNq_ZE6tbyQpb4TYKt%0D%0A4M-VoH-vbTwhk4CiQd8Jz9KHaG-s1FeUNsR7iX-W1jKpjmZrg4XmK4vPg2dOCIUCoYGPAj1_5S9_%0D%0AEbAQYNcWXh7n0o8B_f3lrLoNbR021fzeYtH6Qw1eKpGIV1n_4uyxSVQ27394FTPRDJRrdU7VBYpG%0D%0AexoA8VkVvgLToZQ55FWhcVsUn6qsG1-P_GML92VccduyH8zMRSmT33eMfHx45zM5yHi3VlkUM-lx%0D%0AzT1V2aj2DBpGorUd35m7ETwiDKy9tucmEOf70j4f6MoHHXAI8X_LBaadxfl1JP1jXw-aRGW_FWhb%0D%0ArXWAZX_CTolXF2JvHF9xUYk34Kp3Qw%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYiKsNMFuZdmb6YxGvUgdR2u-yRDZR-y_pLGUjGKaWUdpMEb0gMcNW8x1_S3xhbfrNGmXUmU6qh%0D%0Aa_8GGVwFix2HO-J30wZGVjCW1sqSYnPjdb7ucJ09Z8AFeqzrOG0k4k9oLC5xswZV7HQQd6PZ1ueO%0D%0AoU5lXRscvXlCNEklepIg4TarE-afXPn6zBTtv8eNMITZGU4IsN3TG8EFrSxIg_GhZw%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYsNq9p9q3PB1Kw3QS9ZkktpoD91YYuKAuSKTetzidby1HKBLXBZHOk6G44KAlUFBBAfE67RoM4%0D%0AlDWrRh-cdYVIcs6QTlqDAYwk_rbeAaPZXFiJ6Zv6Z8nMcVOf2NlfscUGWCGkiwUCR49sPCLIwG_H%0D%0AUgLaLgS2WltqbiPp0Yk8znxlgjc8X6R0PaQE1wCiSA17%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYlHEtjBSDDgKDuyHibO7vJMKaP-Bd9XFuEu_4UOXFqwonvNH6wi42hiVtKG_p1GuJRoIQjSYHc%0D%0AhdnJKoyP13ZS4a7xWrJua81Mrm0qK4FMZ_88S_OGMzrffdukgKEd5zXGc4J0PdqfMe5m5TIZ7UpM%0D%0AvyGHPJgcUT8JirM-1f2maqRU9CHiGeOqYbP4hYk0ZDyneG9usC_pguzWBwl4h5ZkQg%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top 250</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Top Movies</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li>    <a href="/offsite/?page-action=ft_ad&token=BCYopLYzHOcCxxknE6sJshak_EZnwMbpK7l__ig01oBhEoR5cXWrl0U1BZ06aIPN7zF5WxibDmq3%0D%0AEwLh_LOXyAoENkqOY8nNxzfgZyyQ-Nvb_nqNriYGOja5FL7kyDdX3s63u4NT0RgpymzAOgACt_-8%0D%0AfTnb4nL4srd4n6iCvP3ynWiBijODq77ldcHbYrNUUGNW0u5nEUtxJ4SFA6tAIV-38EI71ukbALFw%0D%0AkBrAPmHf0XNMttWxz4BSDXU4VFYQ9KK_kwL8C7PaWbaJ0iZivI8Ohw%0D%0A&ref_=ft_ad"
itemprop='url'>Advertising</a>
</li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYtNwfEsKtOx093PsNmjOoZF_CGWLyeqm_1xbmvbAdt973EB9lbLflTREtnGEaWaBF00vRMToU4%0D%0AMkVQBFbF01H_RqGiNLJKHk0AQlH4QNLKz5myMsE-lKkqFqIbEM53caqZWk3oLf6HU_vtxNPmduCj%0D%0As4gJ8XINIeBRVadFonMMGBm3NYS-uaj2ueWGw_plhjeshAFfwa4h0UTS2fasnRC4AQ%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYpTHsGzwzrxdHqHLovBWfmUJenda3tl48HWvxHlQjc9mh78YeHi0FZ1KLpj5MF_zD9SLHFa_oy%0D%0AMkGp2IZZR0v7F14fwDFZjwFvr_rccn781KbwV7YAW1sTFxmde96yKmFnXCXNc7-cHB9yEyYGkYF_%0D%0AEvpemjqK8QEyRU35RuTJnE49vt47cnwK0I8L07uN36jG%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYjLC7XIEsdjtYeQbOUrLHWZBgEQc5_36rWtcYMilfBxJVoh0_xyZNSpClG4ck1cfZGQ5v8qDTo%0D%0ANsp-oygpQ1DJ4XPJ_6T3CkKs7eRXD6BgE6WWIexsrNTmZzMNKgTV9xvrnkjb_tOebSLhMrDONy-X%0D%0Avft0hmAtGYyqsIF1tDWR3Sw1dyT_CV-ME0jL8dGia87S8p0wXOcAPILhbGsUqTLqSbDzbEQ4hopY%0D%0Abw3dCq9SsRzsriQ6Q4dPE3BHb0XgbCtA%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/show_article?conditions';"
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2015
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/';"
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYlN1j1EHyMON7efBWrE3nrO23PofN_YweWgFU_3pcgijcyfTu1YlYbaX7JHlcMzDOWpWVwgsGJ%0D%0ACXidJbKeBY2EptFIjuSW9SONNj8i7suqjZcC-JUkQj55FlQHLenuNeW29o55eomUL1f-pxY9543r%0D%0AM2aXCTaGBSqQm2DeMyP2UHyfrLvh5IZdlMwgAu4M1c81P3iQ1je1BISLEI05h8-zQO0WiHy3koMj%0D%0A_Vpgnlwm-wc%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Instant Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYlmUBCds_yDL6mio8Jt3ebE7KJnHozlNstAOT25Ge6VNyWTA0kBwU7n5IGj4L6us_FbKID9xek%0D%0AQ9R6KDp-boRCM6bUMNbkc4bPSHTEwEsJXWqN-N2y4rMY2avzwFJVIiMwvUj3Qv1tVH5WCOnxvNHL%0D%0ATVIFW0l4aIqNA52y6-wn_GT7Zt4O-rJg8m6qqndV9-kZ6cguYT78gkw8d229swrpxCCI1BvR608d%0D%0AeomcfmLoOKE%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Instant Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYs9RUh2StdOThn3EeybdtYlASKKtEwATCO976n83r2IC7956Ee8WJYZplxWVW8HlCFlPIszGSA%0D%0AsGGQ8BhKcaNUHxJB5KD4Uv0luSIlnyF7UEY_6DccT9lRWVDD_HDQfG-EuUtJmR6y1Pr4aGqlz9DG%0D%0ARQdEmTyn9lwr3IGQWLV5rFtmAkus0K24t9LhOBCJ4jnFm3EamHjSlKZHHzGM1DNAh0Qn6BT7j1yx%0D%0A7KepuPtZznY%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYmZ85Qugq7eE1Pw0-1QxCZ2U8doJajI4ThDXirCZkidmf22wHLr-r1zHPbuBa-7m52TPOKKNPS%0D%0Av2EhdyGF_8Tfn7e_q-Xnh64hjdQuFMhwibev9WCJAgXWoiLitkJmRf3Vdl889eaOPZTCxwTHYMbo%0D%0AAiS7CWHjA79RUJbLizdj107XT8nWTj5yuNTq_wU64T4U8kNE-e9zA3CIwNYU7RPSvjVbxXLGBA5x%0D%0Am7QWuTlkNNY%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYq0TKTzaW5Xoj9txPS2JaOPwdiSzD8hpKKlah6Bu9KffmJVLSqShHP7cxI2r8V51kebJmHcwxQ%0D%0AwUy_SC1KfqxkB4CGgmorklOKuKryKeRiS2wyjST1nNsftcaefwde50DiyyDe9V8JCDIZB3ITBxkx%0D%0AqfTuQNuSUd8TvsrzoNJzBeWdbHpY2EUDH2ifn-BGHNN1fPK1Bi8HKXg5mlFQKylsoBMWSuD-qOuV%0D%0ARcwTJU7om0M%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYsJtUqWJQmBdj-GOiyG1k0FZZWCmJAu0sVVornDGDrYD0dScmd269AgzTW6yvE8wgEYeCg6H41%0D%0AyYVznlFu3WynxJ0w9s1kFe7bdH9CYHl6BKG5bea4D8Zbhac9umrMoE4A9ZIc4vGK_qi3qLOMivH2%0D%0AJHPunu9_aWG6unDr-cs_QpNfGCM0kF0DJNItXkzbucLJNH1Z-xQrvukl0If2x1hvB19c2lqDF3Vp%0D%0Au2IcyrPHUzK_SbkAPo-djrQ10kVEF3PD%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYnob_IF_IDwpYUUaMhbndvhXoas-lvN_o1ogWucItl2B93mo3Ql4UUjPuobfNiWMgRE01cwJZ2%0D%0Aw2KyGj9td2na4hs8hnjnlygGNF5o04FAC_99pxdxRu51Kv_PyKYrShRFs1X24uWxKy9MZow2yMPM%0D%0A7ml1Ws4PiGcY3DOZfEPYcxw%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYl9STm65XPG7lld_r1ENEKa5fi0ZXzU5p4AUP9R7NoxxEE97x1PPO6ESlGAl86IjlJ-0AxsnyA%0D%0AUO5Ujy-j80hMDnUmQ8DnMhwwsfaO_b81Nk7vbDys4ZloGLwDaHYoOOgS2TZ70iRo0W8e0YJwD6wD%0D%0AH1gqXHYEhm-Z7togZ3GWQ9w%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-4053134957._CB315300329_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-1753959650._CB315300322_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-3627981178._CB318528484_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-2430112694._CB318528507_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=0101badb6548ce4de38fcecc42c6439c18e632e9dfa41161f162da8714d47c848488",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=927985621476"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-232398347._CB349580400_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=927985621476&ord=927985621476";
    },"unable to request AAN pixel");
</script>

<div id="flashContent" style="width: 0px; height: 0px; overflow:hidden;">
    <script type="text/javascript">
        if (generic && generic.monitoring.record_metric && flashAdUtils.canPlayFlash) {
            var flashLoaded = 0;
            function swfLoaded() {
                flashLoaded = 1;
                generic.monitoring.record_metric('ads_flash_did_play', 1, true);
            }
            window.onload = function() {
                // Flash was not loaded
                if (flashLoaded === 0) {
                    // Flash could be loaded but wasn't
                    if (flashAdUtils.canPlayFlash()) {
                        generic.monitoring.record_metric('ads_flash_can_but_did_not_play', 0, true);
                    } else {
                        generic.monitoring.record_metric('ads_flash_cannot_play', 0, true);
                    }
                }
            }
            /**
             * There is a rare possibility of window.onload getting called before swfLoaded.
             * Firing this metric to keep track of such calls.
             */
             generic.monitoring.record_metric('ads_flash_page_loaded', 1, true);
         }
    </script>
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="1x1" align="middle">
        <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
        <param name="quality" value="high" />
        <param name="play" value="true" />
        <param name="loop" value="true" />
        <param name="wmode" value="transparent" />
        <param name="scale" value="showall" />
        <param name="menu" value="true" />
        <param name="devicefont" value="false" />
        <param name="salign" value="" />
        <param name="allowScriptAccess" value="always" />

        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" width="1" height="1">
            <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
            <param name="quality" value="high" />
            <param name="play" value="true" />
            <param name="loop" value="true" />
            <param name="wmode" value="transparent" />
            <param name="scale" value="showall" />
            <param name="menu" value="true" />
            <param name="devicefont" value="false" />
            <param name="salign" value="" />
            <param name="allowScriptAccess" value="always" />
        </object>
        <!--<![endif]-->
    </object>
</div>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="639"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
