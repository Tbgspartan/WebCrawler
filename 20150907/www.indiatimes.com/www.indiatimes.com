<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trending  stories on Indian Lifestyle, Culture, Relationships, Food, Travel, Entertainment, News & New Technology News - Indiatimes.com</title>

<meta name="keywords" content="" />
<meta name="description" content="Indiatimes.com brings you the news, articles, stories and videos on entertainment, latest lifestyle, culture & new technologies emerging worldwide." />



		

<link rel="image_src" href="http://media.indiatimes.in/resources/images/fbimage.png" />
<meta property="og:title" content="" />    
<meta property="og:url" content="http://www.indiatimes.com" />
<meta property="og:image" content="http://media.indiatimes.in/resources/images/fbimage.png" />

<meta property="og:site_name" content="indiatimes.com" />
<meta property="fb:app_id" content="117787264903013" />
<meta property="og:description" content="" />
<meta name="google-site-verification" content="ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw" />
<meta name="msvalidate.01" content="BAB71AAED7CCC01DB106A58E99625EB4" />
<link href="https://plus.google.com/+indiatimes/posts" rel="publisher" />

<!--<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />-->

<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">    <link rel="shortcut icon" href="http://media.indiatimes.in/resources/images/favicon.ico?v=25" type="image/x-icon"><link rel="stylesheet" href="http://media.indiatimes.in/resources/css/site.css?v=100.11" media="screen"><link rel="stylesheet" href="http://www.indiatimes.com/fonts/fonts.min.css?v=100.11" media="screen"><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery-1.11.1.min.js?v=100.11"></script><script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/handlebars-v1.3.0.js?v=100.11"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.inview.min.js?v=100.11"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.preload.min.js?v=100.11"></script><script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script><script>(function() {var _fbq = window._fbq || (window._fbq = []);if (!_fbq.loaded) {var fbds = document.createElement('script');fbds.async = true;fbds.src = '//connect.facebook.net/en_US/fbds.js';var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(fbds, s);_fbq.loaded = true;}_fbq.push(['addPixelId', '624029644406124']);})();window._fbq = window._fbq || [];window._fbq.push(['track', 'PixelInitialized', {}]);</script><noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=624029644406124&amp;ev=PixelInitialized" /></noscript>           <!--  Analytics Code Begin -->    <!-- Begin comScore Tag -->    <script>        var _comscore = _comscore || [];        _comscore.push({ c1: "2", c2: "6036484" });        (function() {            var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";            el.parentNode.insertBefore(s, el);        })();    </script>    <noscript>    <img src="http://b.scorecardresearch.com/p?c1=2&c2=6036484&cv=2.0&cj=1" />    </noscript>    <!-- End comScore Tag -->    <!-- Begin Google Analytics Tag -->    <script>        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');        ga('create', 'UA-198011-6', 'indiatimes.com');            ga('require', 'displayfeatures');        ga('send', 'pageview');    </script>    <!-- End Google Analytics Tag -->    <!--  Analytics Code Begin --><!--adcode common script--><script type='text/javascript' src="http://ad.crwdcntrl.net/5/c=2800/pe=y/var=_ccaud"></script><script type="text/javascript">    var googletag = googletag || {};    googletag.cmd = googletag.cmd || [];    (function() {        var gads = document.createElement("script");        gads.async = true;        gads.type = "text/javascript";        var useSSL = "https:" == document.location.protocol;        gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";        var node =document.getElementsByTagName("script")[0];        node.parentNode.insertBefore(gads, node);    })();</script> <script type="text/javascript">        var nid='';        var cid='';        var sid='';        var brand_name='';                                                        </script>            <script type="text/javascript" src="http://media.indiatimes.in/resources/js/banner/home.js?v=100.11"></script>
            
<!--[if lt IE 9]>
  <script type="text/javascript" src="http://media.indiatimes.in/js/html5.js?v1.1"></script>
  <![endif]-->
<script>
var isDetailPage = 0</script>
</head>
<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];   
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=117787264903013&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script> 	
	
<div id="wrap" class="cf"><!--wrap start-->
    <div class='darkDisabledBg' id='disabledBg'></div>
<header>
    <div class="header-inner">
        <div class="comm-header ">
            <nav>
                <div class='forTop'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>

                    <div class="lnks">
                                                        <a href="http://www.indiatimes.com/news/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','News');">News</a> 
                                                               <a href="http://www.indiatimes.com/lifestyle/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Lifestyle');">Lifestyle</a> 
                                                               <a href="http://www.indiatimes.com/entertainment/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Entertainment');">Entertainment</a> 
                                                               <a href="http://www.indiatimes.com/videocafe/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Videos');">Videos</a> 
                                                               <a href="http://www.indiatimes.com/health/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Health');">Health</a> 
                                                                                          <a href='http://www.indiatimes.com/trending'  target="_blank" class="#fffff" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Trending');">Trending</a> 
                                                </div>
                </div>
                <div class='onScrolled'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>
                    <a href="javascript:;" class="menu rc sprite" id="headerMenu">Menu</a>
                    <div class="has-tag">
                                           </div>
                </div>

                                
            </nav>
			                 
	    <div id="sticker" style="display:none;"><span class="readTitle"></span></div>
                    <div class="socls fr share">
            </div>
                        
            
			 <div class="social fr">
                            <a title="facebook" target="_blank"  class="sprite fb" href="https://www.facebook.com/indiatimes">facebook</a>
                            <a title="twitter" target="_blank"  class="sprite twt" href="https://twitter.com/indiatimes">twitter</a>
                            <a title="search"  class="sprite serach" onclick="$('body').showSearch();" href="javascript:void(0);">search</a>
                        </div>
        </div>
    </div>
</header>
<div class="clr"></div>
<div id="overlay" class="animated bounceOut"><!--overlay start-->
    <div class="search_conts"><!--search-cont start-->
        <form onsubmit="return false;" class="serach-form">
            <input type="text" onkeyup="search_data();" class="input" onfocus="if(this.value=='Search'){this.value=''}" onblur="if(this.value==''){this.value='Search'};" value="Search" name="q" id="q" autocomplete="off">
            <input type="button" class="sprite src-btn" onclick="search_data();">
            <a class="sprite sclose" id="close" href="javascript:void(0);">X</a>
        </form>
        <div class="scroll-panes" id="scroll-panes">
            <div class="clr"></div>
            <div class="res-msg" id="res-msg"></div>
            <div class="search-list cf" id="results"></div>
            <div class="clr"></div>
            <div class="loadmore" id="progressBar" style="display: none;">Loading......</div>
            <a class="loadmore" style="display:none;" onclick="load_search_data(); " id="load_more_button" href="javascript:void(0);">Load more</a>
        </div>
    </div>
</div>
<div class="blk">
    <div class="inner">
        <dl id="leftMenu" class="leftMenu accordion">
            <dt data-color="red-bg" class=""><a href="http://www.indiatimes.com">Home</a></dt>
            <dd> </dd>
                            <dt  data-color="blue-bg" ><a href="http://www.indiatimes.com/news/">News</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/news/india/'>India</a>
                                     
                            <a href='http://www.indiatimes.com/news/world/'>World</a>
                                     
                            <a href='http://www.indiatimes.com/news/sports/'>Sports</a>
                                     
                            <a href='http://www.indiatimes.com/news/weird/'>Weird</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="purple-bg" ><a href="http://www.indiatimes.com/lifestyle/">Lifestyle</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/lifestyle/self/'>Self</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/style/'>Style</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                     
                            <a href='http://www.indiatimes.com/culture/who-we-are/'>Who we are</a>
                                     
                            <a href='http://www.indiatimes.com/culture/travel/'>Travel</a>
                                     
                            <a href='http://www.indiatimes.com/culture/food/'>Food</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="red-bg" ><a href="http://www.indiatimes.com/entertainment/">Entertainment</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/entertainment/bollywood/'>Bollywood</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/celebs/'>Celebscoop</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/hollywood/'>Hollywood</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="orange-bg" ><a href="http://www.indiatimes.com/videocafe/">Videos</a>
                                </dt>
                                <dt  data-color="green-bg" ><a href="http://www.indiatimes.com/health/">Health</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                     
                            <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                     
                            <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                     
                            <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                     
                            <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                            </dd>
                    <div class="clr"></div>
                				<dt data-color="" class=""><a href="http://www.indiatimes.com/trending/">Trending</a>
                           </dt>
            <dt class="pink-bg">
            <div class="follow">Follow indiatimes </div>
            <div class="follow_cont"> 
                <a href="https://www.facebook.com/indiatimes" class="fbleft sprite"> facebook</a> 
                <a href="https://twitter.com/indiatimes" class="twtleft sprite"> twitter</a> 
            </div>
            </dt>

            <dd>&nbsp;</dd>
            <div class="clr"></div>
        </dl>
    </div>
</div>


<div class="clr"></div>
<div class="dummy-cont">&nbsp;</div>
<div class="clr"></div>


<div id="pushdown">
    <div class="pull-ad"><!--pull-ad start-->   
	
                <script type="text/javascript"> 
                    showHeaderCode();
                </script>
	    </div><!--pull-ad end-->
</div>
    <!--testing 15-09-07 06:00:02-->  

    <section id="hp_block_1" class="container cf" style="border: 0px solid red;"><!--container start-->
                

        <div class="news-panel cf"><!--news-panel start-->
            <h2>Big news</h2>

            <div class="feature-list cf"><!--feature-list start-->	
                                    <figure>
                        <div class="hash-tag">
                             17 hours ago                         </div>
						 						          
                        <a href="http://www.indiatimes.com/news/india/if-your-call-gets-dropped-due-to-poor-signal-you-might-not-get-billed-244926.html" class=" tint" title="If Your Call Gets Dropped Due To Poor Signal, You Might Not Get Billed!">
                            <img  class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Sep/rtxippq-6_1441522440_1441522443_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/rtxippq-6_1441522440_1441522443_236x111.jpg"  border="0" alt="If Your Call Gets Dropped Due To Poor Signal, You Might Not Get Billed!"/></a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/if-your-call-gets-dropped-due-to-poor-signal-you-might-not-get-billed-244926.html" title="If Your Call Gets Dropped Due To Poor Signal, You Might Not Get Billed!">
                            If Your Call Gets Dropped Due To Poor Signal, You Might Not Get Billed!                        </a>
                    </figcaption>
                            </div><!--feature-list end-->

                            <div class="news-panel-list cf" id="column1_0"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            7 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/sports/pakistan-more-talented-than-india-so-dont-run-after-them-for-cricket-javed-miandad-tells-pcb-244945.html" title="Pakistan More Talented Than India, So Don't Run After Them For Cricket: Javed Miandad Tells PCB" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Sep/miandadthakurcard_1441559027_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/miandadthakurcard_1441559027_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/pakistan-more-talented-than-india-so-dont-run-after-them-for-cricket-javed-miandad-tells-pcb-244945.html" title="Pakistan More Talented Than India, So Don't Run After Them For Cricket: Javed Miandad Tells PCB">
                            Pakistan More Talented Than India, So Don't Run After Them For Cricket: Javed Miandad Tells PCB                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_1"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            11 hours ago                         </div>
                        		
						 	<a class='video-btn sprite' href='http://www.indiatimes.com/news/world/germans-show-the-world-how-to-treat-syrian-refugees-and-its-beyond-awesome-244942.html'>video</a>                        <a href="http://www.indiatimes.com/news/world/germans-show-the-world-how-to-treat-syrian-refugees-and-its-beyond-awesome-244942.html" title="Germans Show The World How To Treat Syrian Refugees. And It's Beyond Awesome!" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/videocafe/2015/Sep/frankfurt-502_1441543964_236x111.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Sep/frankfurt-502_1441543964_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/germans-show-the-world-how-to-treat-syrian-refugees-and-its-beyond-awesome-244942.html" title="Germans Show The World How To Treat Syrian Refugees. And It's Beyond Awesome!">
                            Germans Show The World How To Treat Syrian Refugees. And It's Beyond Awesome!                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_2"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/weird/12-epic-brand-trolling-instances-that-left-us-all-laughing-for-days-244878.html" title="12 Epic Brand Trolling Instances That Left Us All Laughing For Days" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Sep/cp2_1441366250_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/cp2_1441366250_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/weird/12-epic-brand-trolling-instances-that-left-us-all-laughing-for-days-244878.html" title="12 Epic Brand Trolling Instances That Left Us All Laughing For Days">
                            12 Epic Brand Trolling Instances That Left Us All Laughing For Days                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_3"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/india/7-facts-you-need-to-know-about-abdul-kalam-island-indias-most-advanced-missile-testing-site-244940.html" title="7 Facts You Need To Know About Abdul Kalam Island, India's Most Advanced Missile Testing Site" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage-5_1441541053_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage-5_1441541053_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/7-facts-you-need-to-know-about-abdul-kalam-island-indias-most-advanced-missile-testing-site-244940.html" title="7 Facts You Need To Know About Abdul Kalam Island, India's Most Advanced Missile Testing Site">
                            7 Facts You Need To Know About Abdul Kalam Island, India's Most Advanced Missile Testing Site                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                    </div><!--news-panel end-->

        <div class="life-panel cf" id="middleBlock1"><!--life-panel start-->
            <h2>lifestyle</h2>

                                <div class="life-panel-list cf"><!--life-panel-list start-->
                        <figure><span class="label-tag spon">Partner</span>
                            
                               
                            <a href="http://www.indiatimes.com/culture/travel/7-epic-crossborder-road-trips-that-are-bound-to-captivate-your-imagination-244901.html" class=" tint" title="" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/travel/7-epic-crossborder-road-trips-that-are-bound-to-captivate-your-imagination-244901.html');">
                                <img  class="greyBg23 lazy23"  src="http://media.indiatimes.in/media/content/2015/Sep/cp_1441440497_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/cp_1441440497_502x234.jpg"  border="0" alt="7 Epic Cross-Border Road Trips That Are Bound To Captivate Your Imagination Like The India-Thailand Highway" class="img-responsive"/>
                            </a>
                        </figure>
                        <figcaption>
                            <a href="http://www.indiatimes.com/culture/travel/7-epic-crossborder-road-trips-that-are-bound-to-captivate-your-imagination-244901.html" title="7 Epic Cross-Border Road Trips That Are Bound To Captivate Your Imagination Like The India-Thailand Highway" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/travel/7-epic-crossborder-road-trips-that-are-bound-to-captivate-your-imagination-244901.html');">7 Epic Cross-Border Road Trips That Are Bound To Captivate Your Imagination Like The India-Thailand Highway</a>
                        </figcaption>
                    </div><!--life-panel-list end-->
                

                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                   
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/bollywood/11-proposals-from-bollywood-films-that-will-melt-your-heart-244839.html" class="tint" title="11 Proposals From Bollywood Films That Will Melt Your Heart" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/entertainment/bollywood/11-proposals-from-bollywood-films-that-will-melt-your-heart-244839.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Sep/srk_1441268952_1441268956_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/srk_1441268952_1441268956_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/11-proposals-from-bollywood-films-that-will-melt-your-heart-244839.html" title="11 Proposals From Bollywood Films That Will Melt Your Heart" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/entertainment/bollywood/11-proposals-from-bollywood-films-that-will-melt-your-heart-244839.html');">
                            11 Proposals From Bollywood Films That Will Melt Your Heart                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                   
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/healthyliving/5-legit-ways-in-which-alcohol-can-help-you-lose-those-kilos-244838.html" class="tint" title="5 Legit Ways In Which Alcohol Can Help You Lose Those Kilos" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/health/healthyliving/5-legit-ways-in-which-alcohol-can-help-you-lose-those-kilos-244838.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Sep/alcohol_1441267169_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/alcohol_1441267169_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/5-legit-ways-in-which-alcohol-can-help-you-lose-those-kilos-244838.html" title="5 Legit Ways In Which Alcohol Can Help You Lose Those Kilos" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/health/healthyliving/5-legit-ways-in-which-alcohol-can-help-you-lose-those-kilos-244838.html');">
                            5 Legit Ways In Which Alcohol Can Help You Lose Those Kilos                        </a>
                    </figcaption>
                </div>
                        
        </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_0">
                    <span class="strip skyblue-bg"></span>                    <figure>

                                                <a href="http://www.indiatimes.com/lifestyle/tom-and-jerry-+-9-of-our-favourite-cartoon-characters-now-leading-a-retired-life-244928.html" class="tint" title="Tom And Jerry + 9 Of Our Favourite Cartoon Characters, Now Leading A Retired Life">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage1_1441524187_1441524196_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage1_1441524187_1441524196_218x102.jpg" border="0" alt="Tom And Jerry + 9 Of Our Favourite Cartoon Characters, Now Leading A Retired Life"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/tom-and-jerry-+-9-of-our-favourite-cartoon-characters-now-leading-a-retired-life-244928.html" title="Tom And Jerry + 9 Of Our Favourite Cartoon Characters, Now Leading A Retired Life">
                            Tom And Jerry + 9 Of Our Favourite Cartoon Characters, Now Leading A Retired Life                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_1">
                                        <figure>

                                                <a href="http://www.indiatimes.com/entertainment/bollywood/11-proposals-from-bollywood-films-that-will-melt-your-heart-244839.html" class="tint" title="11 Proposals From Bollywood Films That Will Melt Your Heart">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Sep/srk_1441268952_1441268956_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/srk_1441268952_1441268956_218x102.jpg" border="0" alt="11 Proposals From Bollywood Films That Will Melt Your Heart"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/11-proposals-from-bollywood-films-that-will-melt-your-heart-244839.html" title="11 Proposals From Bollywood Films That Will Melt Your Heart">
                            11 Proposals From Bollywood Films That Will Melt Your Heart                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_2">
                                        <figure>

                                                <a href="http://www.indiatimes.com/entertainment/are-you-eagerly-waiting-for-bigg-boss-9-these-details-will-surely-kill-you-with-excitement-244927.html" class="tint" title="Are You Eagerly Waiting For Bigg Boss 9? These Details Will Surely Kill You With Excitement">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Sep/bna_1441524053_1441524060_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/bna_1441524053_1441524060_218x102.jpg" border="0" alt="Are You Eagerly Waiting For Bigg Boss 9? These Details Will Surely Kill You With Excitement"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/are-you-eagerly-waiting-for-bigg-boss-9-these-details-will-surely-kill-you-with-excitement-244927.html" title="Are You Eagerly Waiting For Bigg Boss 9? These Details Will Surely Kill You With Excitement">
                            Are You Eagerly Waiting For Bigg Boss 9? These Details Will Surely Kill You With Excitement                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_3">
                                        <figure>

                                                <a href="http://www.indiatimes.com/entertainment/25-years-on-mr-bean-still-managed-to-turn-heads-with-his-kooky-antics-at-buckingham-palace-244935.html" class="tint" title="25 Years On, Mr. Bean Still Managed To Turn Heads With His Kooky Antics At Buckingham Palace">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Sep/mr-beans1_1441535117_1441535141_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/mr-beans1_1441535117_1441535141_218x102.jpg" border="0" alt="25 Years On, Mr. Bean Still Managed To Turn Heads With His Kooky Antics At Buckingham Palace"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/25-years-on-mr-bean-still-managed-to-turn-heads-with-his-kooky-antics-at-buckingham-palace-244935.html" title="25 Years On, Mr. Bean Still Managed To Turn Heads With His Kooky Antics At Buckingham Palace">
                            25 Years On, Mr. Bean Still Managed To Turn Heads With His Kooky Antics At Buckingham Palace                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_4">
                                        <figure>

                                                <a href="http://www.indiatimes.com/health/healthyliving/5-legit-ways-in-which-alcohol-can-help-you-lose-those-kilos-244838.html" class="tint" title="5 Legit Ways In Which Alcohol Can Help You Lose Those Kilos">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Sep/alcohol_1441267169_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/alcohol_1441267169_218x102.jpg" border="0" alt="5 Legit Ways In Which Alcohol Can Help You Lose Those Kilos"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/5-legit-ways-in-which-alcohol-can-help-you-lose-those-kilos-244838.html" title="5 Legit Ways In Which Alcohol Can Help You Lose Those Kilos">
                            5 Legit Ways In Which Alcohol Can Help You Lose Those Kilos                        </a>
                    </figcaption>
                </div>
                        </div><!--trending-panel end-->
    </section><!--container end-->

<!-- -------------------    end Block 1  ------------------------------------- -->
<section class="big-ads" id="ad1"> 
<div id="bigAd1_slot"></div>
    <script>        
        showBigAD1('bigAd1_slot');
    </script> 
</section>
<!-- -------------------    start Block 2  ------------------------------------- -->

    <section id="hp_block_2" class="container cf" style="border: 0px solid red;"><!--container start-->
        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
                            <div class="news-panel-list cf" id="column1_4"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            12 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/world/nasas-new-spacecraft-will-hitchhike-across-the-galaxy-244941.html" title="NASA's New Spacecraft Will Hitchhike Across The Galaxy" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/1card1_1441541672_1441541679_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/world/nasas-new-spacecraft-will-hitchhike-across-the-galaxy-244941.html" title="NASA's New Spacecraft Will Hitchhike Across The Galaxy">
                            NASA's New Spacecraft Will Hitchhike Across The Galaxy                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_5"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            12 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/sunny-le-yoni-vapas-jao-this-is-the-lamest-protest-india-has-ever-seen-244938.html" title="'Sunny Le Yoni' Vapas Jao: This Is The Lamest Protest India Has Ever Seen" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/sunnyyy-5_1441539607_1441539611_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/sunny-le-yoni-vapas-jao-this-is-the-lamest-protest-india-has-ever-seen-244938.html" title="'Sunny Le Yoni' Vapas Jao: This Is The Lamest Protest India Has Ever Seen">
                            'Sunny Le Yoni' Vapas Jao: This Is The Lamest Protest India Has Ever Seen                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_6"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            12 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/world/this-video-will-give-you-an-inside-view-of-how-the-earthquake-shook-nepal-244936.html" title="Video By An Indian Rescue Team In Nepal Gives An Inside Look At The Effects Of The Earthquake" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/nepal-earthquake12_1441535162_1441535171_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/world/this-video-will-give-you-an-inside-view-of-how-the-earthquake-shook-nepal-244936.html" title="Video By An Indian Rescue Team In Nepal Gives An Inside Look At The Effects Of The Earthquake">
                            Video By An Indian Rescue Team In Nepal Gives An Inside Look At The Effects Of The Earthquake                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_7"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            12 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/heres-why-manipur-has-been-burning-for-the-last-two-months-244934.html" title="Here's Why Manipur Has Been Burning For The Last Two Months" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage-555_1441534748_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/heres-why-manipur-has-been-burning-for-the-last-two-months-244934.html" title="Here's Why Manipur Has Been Burning For The Last Two Months">
                            Here's Why Manipur Has Been Burning For The Last Two Months                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_8"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            13 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/bengaluru-man-hangs-self-after-naming-his-abettors-in-a-video-but-no-one-seems-to-care-244939.html" title="Bengaluru Man Hangs Self After Naming His Abettors In A Video. But No One Seems To Care" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/suicide-600_1441539781_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/bengaluru-man-hangs-self-after-naming-his-abettors-in-a-video-but-no-one-seems-to-care-244939.html" title="Bengaluru Man Hangs Self After Naming His Abettors In A Video. But No One Seems To Care">
                            Bengaluru Man Hangs Self After Naming His Abettors In A Video. But No One Seems To Care                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/videocafe/these-12-people-dont-have-any-right-to-drive-on-roads-heres-why-244932.html" class="tint" title="These 12 People Should Not Be Driving On Roads! Here's Why">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/worstdrivers_card_1441532487_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/these-12-people-dont-have-any-right-to-drive-on-roads-heres-why-244932.html" title="These 12 People Should Not Be Driving On Roads! Here's Why">
                            These 12 People Should Not Be Driving On Roads! Here's Why                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/25-years-on-mr-bean-still-managed-to-turn-heads-with-his-kooky-antics-at-buckingham-palace-244935.html" class="tint" title="25 Years On, Mr. Bean Still Managed To Turn Heads With His Kooky Antics At Buckingham Palace">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/mr-beans1_1441535117_1441535141_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/25-years-on-mr-bean-still-managed-to-turn-heads-with-his-kooky-antics-at-buckingham-palace-244935.html" title="25 Years On, Mr. Bean Still Managed To Turn Heads With His Kooky Antics At Buckingham Palace">
                            25 Years On, Mr. Bean Still Managed To Turn Heads With His Kooky Antics At Buckingham Palace                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/tips-tricks/6-tips-contact-lenses-wearers-should-follow-for-healthy-and-happy-eyes-244869.html" class="tint" title="6 Tips Contact Lenses Wearers Should Follow For Healthy And Happy Eyes">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/lens_1441347111_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/tips-tricks/6-tips-contact-lenses-wearers-should-follow-for-healthy-and-happy-eyes-244869.html" title="6 Tips Contact Lenses Wearers Should Follow For Healthy And Happy Eyes">
                            6 Tips Contact Lenses Wearers Should Follow For Healthy And Happy Eyes                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_5"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/11-iconic-film-locations-in-india-you-must-visit-if-you-are-a-diehard-bollywood-fan-233371.html" class="tint" title="11 Iconic Film Locations In India You Must Visit If You Are A Die-Hard Bollywood Fan!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jun/wakeupsid10_1433608319_1433608327_218x102.jpg" border="0" alt="11 Iconic Film Locations In India You Must Visit If You Are A Die-Hard Bollywood Fan!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/11-iconic-film-locations-in-india-you-must-visit-if-you-are-a-diehard-bollywood-fan-233371.html" title="11 Iconic Film Locations In India You Must Visit If You Are A Die-Hard Bollywood Fan!">
                            11 Iconic Film Locations In India You Must Visit If You Are A Die-Hard Bollywood Fan!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_6"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/lifestyle/self/american-man-shoves-an-iphone-in-a-cassette-player-instantly-becomes-a-comment-on-our-generation-244919.html" class="tint" title="American Man Shoves An iPhone In A Cassette Player, Instantly Becomes A Comment On Our Generation!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/iphone-502_1441513728_218x102.jpg" border="0" alt="American Man Shoves An iPhone In A Cassette Player, Instantly Becomes A Comment On Our Generation!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/american-man-shoves-an-iphone-in-a-cassette-player-instantly-becomes-a-comment-on-our-generation-244919.html" title="American Man Shoves An iPhone In A Cassette Player, Instantly Becomes A Comment On Our Generation!">
                            American Man Shoves An iPhone In A Cassette Player, Instantly Becomes A Comment On Our Generation!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_7"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/12-celebrities-who-are-killing-it-on-instagram-244931.html" class="tint" title="12 Celebrities Who Are Killing It On Instagram">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/kk12_1441537723_1441537728_218x102.jpg" border="0" alt="12 Celebrities Who Are Killing It On Instagram"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/12-celebrities-who-are-killing-it-on-instagram-244931.html" title="12 Celebrities Who Are Killing It On Instagram">
                            12 Celebrities Who Are Killing It On Instagram                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_8"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/videocafe/these-12-people-dont-have-any-right-to-drive-on-roads-heres-why-244932.html" class="tint" title="These 12 People Should Not Be Driving On Roads! Here's Why">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/worstdrivers_card_1441532487_218x102.jpg" border="0" alt="These 12 People Should Not Be Driving On Roads! Here's Why"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/these-12-people-dont-have-any-right-to-drive-on-roads-heres-why-244932.html" title="These 12 People Should Not Be Driving On Roads! Here's Why">
                            These 12 People Should Not Be Driving On Roads! Here's Why                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_9"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/lifestyle/self/8-ways-we-can-set-boundaries-in-relationships-244410.html" class="tint" title="8 Ways We Can Set Boundaries In Relationships">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/ram-leela_1440159493_1440159505_218x102.jpg" border="0" alt="8 Ways We Can Set Boundaries In Relationships"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/8-ways-we-can-set-boundaries-in-relationships-244410.html" title="8 Ways We Can Set Boundaries In Relationships">
                            8 Ways We Can Set Boundaries In Relationships                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div>

    </section><!--container end-->

<!--------------------------- end Block2 ------------------------------------------>
<section class="big-ads" id="ad2"> 
<div id="bigAd2_slot"></div>
</section>
<!--------------------------- start Block3 ------------------------------------------>

    <section id="hp_block_3" class="container cf"><!--container start-->

        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
          
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_9">
                    <figure>
                        <div class="hash-tag">
                            13 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/maharashtra-has-killed-10-rti-activists-in-10-years-just-because-they-were-seeking-the-truth-244930.html" title="Maharashtra Has Killed 10 RTI Activists In 10 Years, Just Because They Were Seeking The Truth" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage-5_1441529950_236x111.jpg" border="0" alt="Maharashtra Has Killed 10 RTI Activists In 10 Years, Just Because They Were Seeking The Truth"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/maharashtra-has-killed-10-rti-activists-in-10-years-just-because-they-were-seeking-the-truth-244930.html" title="Maharashtra Has Killed 10 RTI Activists In 10 Years, Just Because They Were Seeking The Truth">
                            Maharashtra Has Killed 10 RTI Activists In 10 Years, Just Because They Were Seeking The Truth                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_10">
                    <figure>
                        <div class="hash-tag">
                            15 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/african-student-gives-rajnath-singh-a-reality-check-complains-about-racial-discrimination-in-india-244933.html" title="African Student Gives Rajnath Singh A Reality Check, Complains About Racial Discrimination In India" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/rajnath-502_1441534286_236x111.jpg" border="0" alt="African Student Gives Rajnath Singh A Reality Check, Complains About Racial Discrimination In India"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/african-student-gives-rajnath-singh-a-reality-check-complains-about-racial-discrimination-in-india-244933.html" title="African Student Gives Rajnath Singh A Reality Check, Complains About Racial Discrimination In India">
                            African Student Gives Rajnath Singh A Reality Check, Complains About Racial Discrimination In India                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_11">
                    <figure>
                        <div class="hash-tag">
                            16 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/delhi-cops-turn-guardian-angels-constables-help-pregnant-woman-deliver-twins-in-police-van-244929.html" title="Delhi Cops Turn Guardian Angels, Constables Help Pregnant Woman Deliver Twins In Police Van" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/constables-502_1441526964_236x111.jpg" border="0" alt="Delhi Cops Turn Guardian Angels, Constables Help Pregnant Woman Deliver Twins In Police Van"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/delhi-cops-turn-guardian-angels-constables-help-pregnant-woman-deliver-twins-in-police-van-244929.html" title="Delhi Cops Turn Guardian Angels, Constables Help Pregnant Woman Deliver Twins In Police Van">
                            Delhi Cops Turn Guardian Angels, Constables Help Pregnant Woman Deliver Twins In Police Van                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_12">
                    <figure>
                        <div class="hash-tag">
                            17 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/everything-you-need-to-know-about-the-man-who-creates-magic-with-sand-sudarsan-pattnaik-244923.html" title="Everything You Need To Know About The Man Who Creates Magic With Sand - Sudarsan Pattnaik" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/skcard_1441537207_1441537213_236x111.jpg" border="0" alt="Everything You Need To Know About The Man Who Creates Magic With Sand - Sudarsan Pattnaik"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/everything-you-need-to-know-about-the-man-who-creates-magic-with-sand-sudarsan-pattnaik-244923.html" title="Everything You Need To Know About The Man Who Creates Magic With Sand - Sudarsan Pattnaik">
                            Everything You Need To Know About The Man Who Creates Magic With Sand - Sudarsan Pattnaik                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_13">
                    <figure>
                        <div class="hash-tag">
                            17 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/craving-for-maggi-baba-ramdev-has-a-solution-for-that-too-244925.html" title="Craving For Maggi? Baba Ramdev Has A Solution For That Too!" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage-5_1441521433_236x111.jpg" border="0" alt="Craving For Maggi? Baba Ramdev Has A Solution For That Too!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/craving-for-maggi-baba-ramdev-has-a-solution-for-that-too-244925.html" title="Craving For Maggi? Baba Ramdev Has A Solution For That Too!">
                            Craving For Maggi? Baba Ramdev Has A Solution For That Too!                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/12-celebrities-who-are-killing-it-on-instagram-244931.html" class="tint" title="12 Celebrities Who Are Killing It On Instagram">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/kk12_1441537723_1441537728_502x234.jpg" border="0" alt="http://www.indiatimes.com/entertainment/12-celebrities-who-are-killing-it-on-instagram-244931.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/12-celebrities-who-are-killing-it-on-instagram-244931.html" title="12 Celebrities Who Are Killing It On Instagram">
                            12 Celebrities Who Are Killing It On Instagram                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/are-you-eagerly-waiting-for-bigg-boss-9-these-details-will-surely-kill-you-with-excitement-244927.html" class="tint" title="Are You Eagerly Waiting For Bigg Boss 9? These Details Will Surely Kill You With Excitement">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/bna_1441524053_1441524060_502x234.jpg" border="0" alt="http://www.indiatimes.com/entertainment/are-you-eagerly-waiting-for-bigg-boss-9-these-details-will-surely-kill-you-with-excitement-244927.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/are-you-eagerly-waiting-for-bigg-boss-9-these-details-will-surely-kill-you-with-excitement-244927.html" title="Are You Eagerly Waiting For Bigg Boss 9? These Details Will Surely Kill You With Excitement">
                            Are You Eagerly Waiting For Bigg Boss 9? These Details Will Surely Kill You With Excitement                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/lifestyle/tom-and-jerry-+-9-of-our-favourite-cartoon-characters-now-leading-a-retired-life-244928.html" class="tint" title="Tom And Jerry + 9 Of Our Favourite Cartoon Characters, Now Leading A Retired Life">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage1_1441524187_1441524196_502x234.jpg" border="0" alt="http://www.indiatimes.com/lifestyle/tom-and-jerry-+-9-of-our-favourite-cartoon-characters-now-leading-a-retired-life-244928.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/tom-and-jerry-+-9-of-our-favourite-cartoon-characters-now-leading-a-retired-life-244928.html" title="Tom And Jerry + 9 Of Our Favourite Cartoon Characters, Now Leading A Retired Life">
                            Tom And Jerry + 9 Of Our Favourite Cartoon Characters, Now Leading A Retired Life                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_10"><!--trending-panel-list start-->
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-rare-video-of-25yearold-angelina-jolie-performing-in-an-acting-class-will-give-you-goosebumps-244921.html'>video</a>		

                        <a href="http://www.indiatimes.com/videocafe/this-rare-video-of-25yearold-angelina-jolie-performing-in-an-acting-class-will-give-you-goosebumps-244921.html" class="tint" title="This Rare Video Of 25-Year-Old Angelina Jolie Performing In An Acting Class Will Give You Goosebumps!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/angelina_card_1441518830_218x102.jpg" border="0" alt="This Rare Video Of 25-Year-Old Angelina Jolie Performing In An Acting Class Will Give You Goosebumps!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/this-rare-video-of-25yearold-angelina-jolie-performing-in-an-acting-class-will-give-you-goosebumps-244921.html" title="This Rare Video Of 25-Year-Old Angelina Jolie Performing In An Acting Class Will Give You Goosebumps!">
                            This Rare Video Of 25-Year-Old Angelina Jolie Performing In An Acting Class Will Give You Goosebumps!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_11"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/after-dating-some-of-the-hottest-btown-actresses-guess-who-yuvi-is-dating-now-244920.html" class="tint" title="After Dating Some Of The Hottest B-Town Actresses, Guess Who Yuvi Is Dating Now!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/cardimage_1441518756_1441518766_218x102.jpg" border="0" alt="After Dating Some Of The Hottest B-Town Actresses, Guess Who Yuvi Is Dating Now!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/after-dating-some-of-the-hottest-btown-actresses-guess-who-yuvi-is-dating-now-244920.html" title="After Dating Some Of The Hottest B-Town Actresses, Guess Who Yuvi Is Dating Now!">
                            After Dating Some Of The Hottest B-Town Actresses, Guess Who Yuvi Is Dating Now!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_12"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/health/tips-tricks/10-really-effective-ways-to-get-rid-of-blackheads-on-your-face-and-nose-244852.html" class="tint" title="10 Really Effective Ways To Get Rid Of Blackheads On Your Face And Nose">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441278788_218x102.jpg" border="0" alt="10 Really Effective Ways To Get Rid Of Blackheads On Your Face And Nose"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/tips-tricks/10-really-effective-ways-to-get-rid-of-blackheads-on-your-face-and-nose-244852.html" title="10 Really Effective Ways To Get Rid Of Blackheads On Your Face And Nose">
                            10 Really Effective Ways To Get Rid Of Blackheads On Your Face And Nose                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_13"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/culture/who-we-are/5-skills-every-entrepreneur-can-learn-from-lord-krishna-244902.html" class="tint" title="5 Skills Every Entrepreneur Can Learn From Lord Krishna">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage-five-krs_1441441645_218x102.jpg" border="0" alt="5 Skills Every Entrepreneur Can Learn From Lord Krishna"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/5-skills-every-entrepreneur-can-learn-from-lord-krishna-244902.html" title="5 Skills Every Entrepreneur Can Learn From Lord Krishna">
                            5 Skills Every Entrepreneur Can Learn From Lord Krishna                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_14"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/culture/who-we-are/this-chilling-and-tragic-love-story-of-two-women-is-a-mirror-of-indian-society-244907.html" class="tint" title="This Chilling And Tragic Love Story Of Two Women Is A Mirror Of Indian Society">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441447107_218x102.jpg" border="0" alt="This Chilling And Tragic Love Story Of Two Women Is A Mirror Of Indian Society"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/this-chilling-and-tragic-love-story-of-two-women-is-a-mirror-of-indian-society-244907.html" title="This Chilling And Tragic Love Story Of Two Women Is A Mirror Of Indian Society">
                            This Chilling And Tragic Love Story Of Two Women Is A Mirror Of Indian Society                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div><!--trending-panel end-->

    </section><!--container end-->

<!------------------------------- end Block 3 ---------------------------------------->
<section class="big-ads">
<div id="bigAd3_slot"></div>
</section>

<!------------------------------- start Block4  -------------------------------------->

    <section class="container cf" id="container4"><!--container start-->
        <div class="news-panel cf ">
            <h2>news</h2>

                            <div class="news-panel-list cf" id="column1_14">
                    <figure>
                        <div class="hash-tag">
                            18 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/pakistan-historian-admits-his-country-fudged-records-to-claim-victory-in-1965-war-244924.html" title="Pakistan Historian Admits His Country Fudged Records To Claim Victory In 1965 War" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/1965-card_1441519574_1441519598_236x111.jpg" border="0" alt="Pakistan Historian Admits His Country Fudged Records To Claim Victory In 1965 War"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/pakistan-historian-admits-his-country-fudged-records-to-claim-victory-in-1965-war-244924.html" title="Pakistan Historian Admits His Country Fudged Records To Claim Victory In 1965 War">
                            Pakistan Historian Admits His Country Fudged Records To Claim Victory In 1965 War                        </a>
                    </figcaption> 
                </div>
                <div class='container1'>                <div class="news-panel-list cf" id="column1_15">
                    <figure>
                        <div class="hash-tag">
                            18 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/manohar-parrikar-the-defence-minister-we-need-but-not-the-one-we-deserve-233627.html" title="Manohar Parrikar: The Defence Minister We Need But Not The One We Deserve" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card2_1441517672_236x111.jpg" border="0" alt="Manohar Parrikar: The Defence Minister We Need But Not The One We Deserve"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/manohar-parrikar-the-defence-minister-we-need-but-not-the-one-we-deserve-233627.html" title="Manohar Parrikar: The Defence Minister We Need But Not The One We Deserve">
                            Manohar Parrikar: The Defence Minister We Need But Not The One We Deserve                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_16">
                    <figure>
                        <div class="hash-tag">
                            18 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/congress-leader-digvijaya-singh-gets-married-to-tv-anchor-amrita-rai-twitterati-loses-it-244922.html" title="Congress Leader Digvijaya Singh Gets Married To TV Anchor Amrita Rai, Twitterati Loses It!" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/com-502_1441523613_236x111.jpg" border="0" alt="Congress Leader Digvijaya Singh Gets Married To TV Anchor Amrita Rai, Twitterati Loses It!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/congress-leader-digvijaya-singh-gets-married-to-tv-anchor-amrita-rai-twitterati-loses-it-244922.html" title="Congress Leader Digvijaya Singh Gets Married To TV Anchor Amrita Rai, Twitterati Loses It!">
                            Congress Leader Digvijaya Singh Gets Married To TV Anchor Amrita Rai, Twitterati Loses It!                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_17">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/shaolin-monk-sets-record-by-doing-the-unthinkable-runs-on-water-244916.html" title="Shaolin Monk Sets Record By Doing The Unthinkable, Runs On Water!" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441454855_236x111.jpg" border="0" alt="Shaolin Monk Sets Record By Doing The Unthinkable, Runs On Water!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/shaolin-monk-sets-record-by-doing-the-unthinkable-runs-on-water-244916.html" title="Shaolin Monk Sets Record By Doing The Unthinkable, Runs On Water!">
                            Shaolin Monk Sets Record By Doing The Unthinkable, Runs On Water!                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_18">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/this-us-military-pillow-fight-took-a-turn-for-the-worse-when-30-cadets-were-left-injured-244914.html" title="This US Military Pillow Fight Took A Turn For The Worse When 30 Cadets Were Left Injured" class=" tint">
                            <a class='video-btn sprite' href='http://www.indiatimes.com/news/world/this-us-military-pillow-fight-took-a-turn-for-the-worse-when-30-cadets-were-left-injured-244914.html'>video</a>
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/pillowfight-cp_1441453529_1441453541_236x111.jpg" border="0" alt="This US Military Pillow Fight Took A Turn For The Worse When 30 Cadets Were Left Injured"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/this-us-military-pillow-fight-took-a-turn-for-the-worse-when-30-cadets-were-left-injured-244914.html" title="This US Military Pillow Fight Took A Turn For The Worse When 30 Cadets Were Left Injured">
                            This US Military Pillow Fight Took A Turn For The Worse When 30 Cadets Were Left Injured                        </a>
                    </figcaption> 
                </div>
                 
        </div>
    </div><!--news-panel end-->

    <div class="life-panel cf container2" id="b4c2"><!--life-panel start-->
        <h2>lifestyle</h2>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/9-really-painful-things-about-having-a-conversation-with-a-knowitall-244850.html" class="tint" title="9 Really Painful Things About Having A Conversation With A Know-It-All">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/cp_1441275372_502x234.jpg" border="0" alt="9 Really Painful Things About Having A Conversation With A Know-It-All" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/9-really-painful-things-about-having-a-conversation-with-a-knowitall-244850.html" title="9 Really Painful Things About Having A Conversation With A Know-It-All">
                        9 Really Painful Things About Having A Conversation With A Know-It-All                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/everything-you-need-to-know-about-dengue-and-how-to-prevent-it-244786.html" class="tint" title="Everything You Need To Know About Dengue And How To Prevent It">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/dengue-card_1441173876_502x234.gif" border="0" alt="Everything You Need To Know About Dengue And How To Prevent It" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/everything-you-need-to-know-about-dengue-and-how-to-prevent-it-244786.html" title="Everything You Need To Know About Dengue And How To Prevent It">
                        Everything You Need To Know About Dengue And How To Prevent It                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/after-dating-some-of-the-hottest-btown-actresses-guess-who-yuvi-is-dating-now-244920.html" class="tint" title="After Dating Some Of The Hottest B-Town Actresses, Guess Who Yuvi Is Dating Now!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/cardimage_1441518756_1441518766_502x234.jpg" border="0" alt="After Dating Some Of The Hottest B-Town Actresses, Guess Who Yuvi Is Dating Now!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/after-dating-some-of-the-hottest-btown-actresses-guess-who-yuvi-is-dating-now-244920.html" title="After Dating Some Of The Hottest B-Town Actresses, Guess Who Yuvi Is Dating Now!">
                        After Dating Some Of The Hottest B-Town Actresses, Guess Who Yuvi Is Dating Now!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/7-stages-of-boredom-you-go-through-on-a-lazy-day-like-today-244864.html" class="tint" title="7 Stages Of Boredom You Go Through On A Lazy Day Like Today">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/cp_1441343197_1441343201_502x234.jpg" border="0" alt="7 Stages Of Boredom You Go Through On A Lazy Day Like Today" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/7-stages-of-boredom-you-go-through-on-a-lazy-day-like-today-244864.html" title="7 Stages Of Boredom You Go Through On A Lazy Day Like Today">
                        7 Stages Of Boredom You Go Through On A Lazy Day Like Today                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-rare-video-of-25yearold-angelina-jolie-performing-in-an-acting-class-will-give-you-goosebumps-244921.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-rare-video-of-25yearold-angelina-jolie-performing-in-an-acting-class-will-give-you-goosebumps-244921.html" class="tint" title="This Rare Video Of 25-Year-Old Angelina Jolie Performing In An Acting Class Will Give You Goosebumps!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/angelina_card_1441518830_502x234.jpg" border="0" alt="This Rare Video Of 25-Year-Old Angelina Jolie Performing In An Acting Class Will Give You Goosebumps!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-rare-video-of-25yearold-angelina-jolie-performing-in-an-acting-class-will-give-you-goosebumps-244921.html" title="This Rare Video Of 25-Year-Old Angelina Jolie Performing In An Acting Class Will Give You Goosebumps!">
                        This Rare Video Of 25-Year-Old Angelina Jolie Performing In An Acting Class Will Give You Goosebumps!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/11-iconic-film-locations-in-india-you-must-visit-if-you-are-a-diehard-bollywood-fan-233371.html" class="tint" title="11 Iconic Film Locations In India You Must Visit If You Are A Die-Hard Bollywood Fan!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jun/wakeupsid10_1433608319_1433608327_502x234.jpg" border="0" alt="11 Iconic Film Locations In India You Must Visit If You Are A Die-Hard Bollywood Fan!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/11-iconic-film-locations-in-india-you-must-visit-if-you-are-a-diehard-bollywood-fan-233371.html" title="11 Iconic Film Locations In India You Must Visit If You Are A Die-Hard Bollywood Fan!">
                        11 Iconic Film Locations In India You Must Visit If You Are A Die-Hard Bollywood Fan!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/10-really-effective-ways-to-get-rid-of-blackheads-on-your-face-and-nose-244852.html" class="tint" title="10 Really Effective Ways To Get Rid Of Blackheads On Your Face And Nose">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441278788_502x234.jpg" border="0" alt="10 Really Effective Ways To Get Rid Of Blackheads On Your Face And Nose" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/10-really-effective-ways-to-get-rid-of-blackheads-on-your-face-and-nose-244852.html" title="10 Really Effective Ways To Get Rid Of Blackheads On Your Face And Nose">
                        10 Really Effective Ways To Get Rid Of Blackheads On Your Face And Nose                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/american-man-shoves-an-iphone-in-a-cassette-player-instantly-becomes-a-comment-on-our-generation-244919.html" class="tint" title="American Man Shoves An iPhone In A Cassette Player, Instantly Becomes A Comment On Our Generation!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/iphone-502_1441513728_502x234.jpg" border="0" alt="American Man Shoves An iPhone In A Cassette Player, Instantly Becomes A Comment On Our Generation!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/american-man-shoves-an-iphone-in-a-cassette-player-instantly-becomes-a-comment-on-our-generation-244919.html" title="American Man Shoves An iPhone In A Cassette Player, Instantly Becomes A Comment On Our Generation!">
                        American Man Shoves An iPhone In A Cassette Player, Instantly Becomes A Comment On Our Generation!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/8-ways-we-can-set-boundaries-in-relationships-244410.html" class="tint" title="8 Ways We Can Set Boundaries In Relationships">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/ram-leela_1440159493_1440159505_502x234.jpg" border="0" alt="8 Ways We Can Set Boundaries In Relationships" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/8-ways-we-can-set-boundaries-in-relationships-244410.html" title="8 Ways We Can Set Boundaries In Relationships">
                        8 Ways We Can Set Boundaries In Relationships                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/this-hilarious-mashup-video-of-got-and-kuch-kuch-hota-hai-will-leave-you-in-splits-244918.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/this-hilarious-mashup-video-of-got-and-kuch-kuch-hota-hai-will-leave-you-in-splits-244918.html" class="tint" title="This Hilarious Mashup Video Of GoT And Kuch Kuch Hota Hai Will Leave You In Splits!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/picmonkey-collage_1441455701_1441455709_502x234.jpg" border="0" alt="This Hilarious Mashup Video Of GoT And Kuch Kuch Hota Hai Will Leave You In Splits!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/this-hilarious-mashup-video-of-got-and-kuch-kuch-hota-hai-will-leave-you-in-splits-244918.html" title="This Hilarious Mashup Video Of GoT And Kuch Kuch Hota Hai Will Leave You In Splits!">
                        This Hilarious Mashup Video Of GoT And Kuch Kuch Hota Hai Will Leave You In Splits!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/16-pictures-that-describe-the-essence-of-india-244859.html" class="tint" title="16 Pictures That Describe The Essence Of India">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/jama-masjid-card_1441280201_1441280211_502x234.jpg" border="0" alt="16 Pictures That Describe The Essence Of India" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/16-pictures-that-describe-the-essence-of-india-244859.html" title="16 Pictures That Describe The Essence Of India">
                        16 Pictures That Describe The Essence Of India                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/the-struggles-of-a-man-who-cant-find-a-place-to-pee-anywhere-244913.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/the-struggles-of-a-man-who-cant-find-a-place-to-pee-anywhere-244913.html" class="tint" title="The Struggles Of A Man Who Can't Find A Place To Pee Anywhere">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/picmonkey-collage-5_1441451444_502x234.jpg" border="0" alt="The Struggles Of A Man Who Can't Find A Place To Pee Anywhere" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/the-struggles-of-a-man-who-cant-find-a-place-to-pee-anywhere-244913.html" title="The Struggles Of A Man Who Can't Find A Place To Pee Anywhere">
                        The Struggles Of A Man Who Can't Find A Place To Pee Anywhere                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/alls-not-well-for-honey-singhs-dheere-dheere-revisited-244906.html" class="tint" title="All's Not Well For Honey Singh's 'Dheere Dheere Revisited'!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collagennn_1441446310_1441446318_502x234.jpg" border="0" alt="All's Not Well For Honey Singh's 'Dheere Dheere Revisited'!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/alls-not-well-for-honey-singhs-dheere-dheere-revisited-244906.html" title="All's Not Well For Honey Singh's 'Dheere Dheere Revisited'!">
                        All's Not Well For Honey Singh's 'Dheere Dheere Revisited'!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/this-chilling-and-tragic-love-story-of-two-women-is-a-mirror-of-indian-society-244907.html" class="tint" title="This Chilling And Tragic Love Story Of Two Women Is A Mirror Of Indian Society">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441447107_502x234.jpg" border="0" alt="This Chilling And Tragic Love Story Of Two Women Is A Mirror Of Indian Society" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/this-chilling-and-tragic-love-story-of-two-women-is-a-mirror-of-indian-society-244907.html" title="This Chilling And Tragic Love Story Of Two Women Is A Mirror Of Indian Society">
                        This Chilling And Tragic Love Story Of Two Women Is A Mirror Of Indian Society                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/7-people-who-made-your-life-hell-and-why-you-should-thank-them-244765.html" class="tint" title="7 People Who Made Your Life Hell And Why You Should Thank Them">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/cp_1441101099_1441101105_502x234.jpg" border="0" alt="7 People Who Made Your Life Hell And Why You Should Thank Them" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/7-people-who-made-your-life-hell-and-why-you-should-thank-them-244765.html" title="7 People Who Made Your Life Hell And Why You Should Thank Them">
                        7 People Who Made Your Life Hell And Why You Should Thank Them                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/businessman-refuses-to-settle-out-of-court-with-saif-ali-khan-over-the-brawl-case-in-2012_-244904.html" class="tint" title="Businessman Refuses To Settle Out Of Court With Saif Ali Khan Over The Brawl Case In 2012">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/1434632824_1441442753_1441442765_502x234.jpg" border="0" alt="Businessman Refuses To Settle Out Of Court With Saif Ali Khan Over The Brawl Case In 2012" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/businessman-refuses-to-settle-out-of-court-with-saif-ali-khan-over-the-brawl-case-in-2012_-244904.html" title="Businessman Refuses To Settle Out Of Court With Saif Ali Khan Over The Brawl Case In 2012">
                        Businessman Refuses To Settle Out Of Court With Saif Ali Khan Over The Brawl Case In 2012                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/5-skills-every-entrepreneur-can-learn-from-lord-krishna-244902.html" class="tint" title="5 Skills Every Entrepreneur Can Learn From Lord Krishna">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage-five-krs_1441441645_502x234.jpg" border="0" alt="5 Skills Every Entrepreneur Can Learn From Lord Krishna" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/5-skills-every-entrepreneur-can-learn-from-lord-krishna-244902.html" title="5 Skills Every Entrepreneur Can Learn From Lord Krishna">
                        5 Skills Every Entrepreneur Can Learn From Lord Krishna                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/9-dahi-handi-songs-from-bollywood-that-will-make-you-want-to-dance-right-now-244897.html" class="tint" title="9 Dahi Handi Songs From Bollywood That Will Make You Want To Dance Right Now!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage_1441435140_1441435150_502x234.jpg" border="0" alt="9 Dahi Handi Songs From Bollywood That Will Make You Want To Dance Right Now!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/9-dahi-handi-songs-from-bollywood-that-will-make-you-want-to-dance-right-now-244897.html" title="9 Dahi Handi Songs From Bollywood That Will Make You Want To Dance Right Now!">
                        9 Dahi Handi Songs From Bollywood That Will Make You Want To Dance Right Now!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/3-million-litres-of-whiskey-on-fire-thats-what-powered-this-terrifying-tornado-244900.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/3-million-litres-of-whiskey-on-fire-thats-what-powered-this-terrifying-tornado-244900.html" class="tint" title="3 Million Litres Of Whiskey On Fire. That's What Powered This Terrifying Tornado">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/firetornado-5_1441439916_1441439931_502x234.jpg" border="0" alt="3 Million Litres Of Whiskey On Fire. That's What Powered This Terrifying Tornado" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/3-million-litres-of-whiskey-on-fire-thats-what-powered-this-terrifying-tornado-244900.html" title="3 Million Litres Of Whiskey On Fire. That's What Powered This Terrifying Tornado">
                        3 Million Litres Of Whiskey On Fire. That's What Powered This Terrifying Tornado                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/whatsapp-can-now-tell-you-who-your-best-friends-are-using-this-hidden-tool-244891.html" class="tint" title="WhatsApp Can Now Tell You Who Your Best Friends Are, Using This Hidden Tool">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/whatsapp-card_1441370136_502x234.jpg" border="0" alt="WhatsApp Can Now Tell You Who Your Best Friends Are, Using This Hidden Tool" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/whatsapp-can-now-tell-you-who-your-best-friends-are-using-this-hidden-tool-244891.html" title="WhatsApp Can Now Tell You Who Your Best Friends Are, Using This Hidden Tool">
                        WhatsApp Can Now Tell You Who Your Best Friends Are, Using This Hidden Tool                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/music-composer-aadesh-srivastava-passes-away-one-day-before-his-birthday-leaving-a-musical-hole-244893.html" class="tint" title="Music Composer Aadesh Srivastava Passes Away One Day Before His Birthday, Leaving A Musical Hole">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/aadesh-srivastava-502_1441425674_502x234.jpg" border="0" alt="Music Composer Aadesh Srivastava Passes Away One Day Before His Birthday, Leaving A Musical Hole" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/music-composer-aadesh-srivastava-passes-away-one-day-before-his-birthday-leaving-a-musical-hole-244893.html" title="Music Composer Aadesh Srivastava Passes Away One Day Before His Birthday, Leaving A Musical Hole">
                        Music Composer Aadesh Srivastava Passes Away One Day Before His Birthday, Leaving A Musical Hole                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/20-classic-monologues-that-will-make-you-call-your-teacher-today-244844.html" class="tint" title="20 Classic Monologues That Will Make You Call Your Teacher Today">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/fish_1441272211_1441272267_502x234.jpg" border="0" alt="20 Classic Monologues That Will Make You Call Your Teacher Today" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/20-classic-monologues-that-will-make-you-call-your-teacher-today-244844.html" title="20 Classic Monologues That Will Make You Call Your Teacher Today">
                        20 Classic Monologues That Will Make You Call Your Teacher Today                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/28-brutally-funny-youtube-comments-that-cannot-be-ignored-244874.html" class="tint" title="28 Brutally Funny YouTube Comments That Cannot Be Ignored">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441356560_502x234.jpg" border="0" alt="28 Brutally Funny YouTube Comments That Cannot Be Ignored" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/28-brutally-funny-youtube-comments-that-cannot-be-ignored-244874.html" title="28 Brutally Funny YouTube Comments That Cannot Be Ignored">
                        28 Brutally Funny YouTube Comments That Cannot Be Ignored                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/12-pictures-depicting-a-childs-growth-over-an-year-are-the-cutest-thing-ever-244830.html" class="tint" title="These 12 Pictures Depicting A Child's Growth Over A Year Are The Cutest Thing Ever">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/fb-card_1441261199_502x234.jpg" border="0" alt="These 12 Pictures Depicting A Child's Growth Over A Year Are The Cutest Thing Ever" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/12-pictures-depicting-a-childs-growth-over-an-year-are-the-cutest-thing-ever-244830.html" title="These 12 Pictures Depicting A Child's Growth Over A Year Are The Cutest Thing Ever">
                        These 12 Pictures Depicting A Child's Growth Over A Year Are The Cutest Thing Ever                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/13-bollywood-actresses-who-made-the-jump-from-actress-to-producer-244890.html" class="tint" title="13 Bollywood Actresses Who Made The Jump From Actress To Producer">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441368879_502x234.jpg" border="0" alt="13 Bollywood Actresses Who Made The Jump From Actress To Producer" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/13-bollywood-actresses-who-made-the-jump-from-actress-to-producer-244890.html" title="13 Bollywood Actresses Who Made The Jump From Actress To Producer">
                        13 Bollywood Actresses Who Made The Jump From Actress To Producer                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/15-images-that-bring-the-kumbh-melas-most-epic-moments-alive-244854.html" class="tint" title="15 Images That Bring The Kumbh Mela's Most Epic Moments Alive">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/502_1441277554_502x234.jpg" border="0" alt="15 Images That Bring The Kumbh Mela's Most Epic Moments Alive" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/15-images-that-bring-the-kumbh-melas-most-epic-moments-alive-244854.html" title="15 Images That Bring The Kumbh Mela's Most Epic Moments Alive">
                        15 Images That Bring The Kumbh Mela's Most Epic Moments Alive                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/culture/who-we-are/this-teachers-day-a-hardworking-maushi-teaches-us-one-of-lifes-most-important-lessons-244886.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/this-teachers-day-a-hardworking-maushi-teaches-us-one-of-lifes-most-important-lessons-244886.html" class="tint" title="This Teachers' Day, A Hardworking Maushi Teaches Us One Of Life's Most Important Lessons">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/maushi_1441363761_1441363772_502x234.jpg" border="0" alt="This Teachers' Day, A Hardworking Maushi Teaches Us One Of Life's Most Important Lessons" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/this-teachers-day-a-hardworking-maushi-teaches-us-one-of-lifes-most-important-lessons-244886.html" title="This Teachers' Day, A Hardworking Maushi Teaches Us One Of Life's Most Important Lessons">
                        This Teachers' Day, A Hardworking Maushi Teaches Us One Of Life's Most Important Lessons                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/an-open-letter-to-the-boy-who-didnt-get-to-be-a-teacher-on-teachers-day-from-another-boy-who-didnt-244882.html" class="tint" title="An Open Letter To The Boy Who Didn't Get To Be A Teacher On Teacher's Day From Another Boy Who Didn't">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/download_1441366033_1441366041_502x234.jpg" border="0" alt="An Open Letter To The Boy Who Didn't Get To Be A Teacher On Teacher's Day From Another Boy Who Didn't" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/an-open-letter-to-the-boy-who-didnt-get-to-be-a-teacher-on-teachers-day-from-another-boy-who-didnt-244882.html" title="An Open Letter To The Boy Who Didn't Get To Be A Teacher On Teacher's Day From Another Boy Who Didn't">
                        An Open Letter To The Boy Who Didn't Get To Be A Teacher On Teacher's Day From Another Boy Who Didn't                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/dance-your-way-to-weight-loss-with-couples-masala-bhangra-workout-244772.html" class="tint" title="Dance Your Way To Weight Loss With This Couple's Masala Bhangra Workout">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card-mb_1441106085_502x234.gif" border="0" alt="Dance Your Way To Weight Loss With This Couple's Masala Bhangra Workout" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/dance-your-way-to-weight-loss-with-couples-masala-bhangra-workout-244772.html" title="Dance Your Way To Weight Loss With This Couple's Masala Bhangra Workout">
                        Dance Your Way To Weight Loss With This Couple's Masala Bhangra Workout                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/14-people-we-want-to-see-in-the-bigg-boss-house-including-rahul-baba-244858.html" class="tint" title="14 People We Want To See In The Bigg Boss House, Including Rahul Baba!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage_1441279987_1441279998_502x234.jpg" border="0" alt="14 People We Want To See In The Bigg Boss House, Including Rahul Baba!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/14-people-we-want-to-see-in-the-bigg-boss-house-including-rahul-baba-244858.html" title="14 People We Want To See In The Bigg Boss House, Including Rahul Baba!">
                        14 People We Want To See In The Bigg Boss House, Including Rahul Baba!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/food/these-17-pictures-of-the-last-meals-of-death-row-inmates-will-leave-you-with-a-mixture-of-emotions-244848.html" class="tint" title="These 17 Pictures Of The Last Meals Of Death Row Inmates Will Leave You With A Mixture Of Emotions">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/food-card_1441274635_1441274646_502x234.jpg" border="0" alt="These 17 Pictures Of The Last Meals Of Death Row Inmates Will Leave You With A Mixture Of Emotions" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/food/these-17-pictures-of-the-last-meals-of-death-row-inmates-will-leave-you-with-a-mixture-of-emotions-244848.html" title="These 17 Pictures Of The Last Meals Of Death Row Inmates Will Leave You With A Mixture Of Emotions">
                        These 17 Pictures Of The Last Meals Of Death Row Inmates Will Leave You With A Mixture Of Emotions                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/being-careless-+-5-other-reasons-why-people-pile-on-the-kilos-after-marriage-244791.html" class="tint" title="Being Careless + 5 Other Reasons Why People Pile On The Kilos After Marriage">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/fb-card_1441266613_502x234.jpg" border="0" alt="Being Careless + 5 Other Reasons Why People Pile On The Kilos After Marriage" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/being-careless-+-5-other-reasons-why-people-pile-on-the-kilos-after-marriage-244791.html" title="Being Careless + 5 Other Reasons Why People Pile On The Kilos After Marriage">
                        Being Careless + 5 Other Reasons Why People Pile On The Kilos After Marriage                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/angry-indian-goddesses-is-indias-first-allwomen-buddy-movie-and-it-looks-pretty-damn-riveting-244872.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/angry-indian-goddesses-is-indias-first-allwomen-buddy-movie-and-it-looks-pretty-damn-riveting-244872.html" class="tint" title="'Angry Indian Goddesses' Is India's First All-Women Buddy Movie And It Looks Pretty Damn Riveting!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/screenshot_2_1441350923_1441350930_502x234.jpg" border="0" alt="'Angry Indian Goddesses' Is India's First All-Women Buddy Movie And It Looks Pretty Damn Riveting!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/angry-indian-goddesses-is-indias-first-allwomen-buddy-movie-and-it-looks-pretty-damn-riveting-244872.html" title="'Angry Indian Goddesses' Is India's First All-Women Buddy Movie And It Looks Pretty Damn Riveting!">
                        'Angry Indian Goddesses' Is India's First All-Women Buddy Movie And It Looks Pretty Damn Riveting!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/this-guy-puts-the-life-in-lifeless-stock-images-and-its-hilarious-244834.html" class="tint" title="This Guy Puts The Life In Lifeless Stock Images And It's Hilarious">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/lady_1441261527_1441261537_502x234.jpg" border="0" alt="This Guy Puts The Life In Lifeless Stock Images And It's Hilarious" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/this-guy-puts-the-life-in-lifeless-stock-images-and-its-hilarious-244834.html" title="This Guy Puts The Life In Lifeless Stock Images And It's Hilarious">
                        This Guy Puts The Life In Lifeless Stock Images And It's Hilarious                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/7-reasons-why-you-should-have-a-kitchen-garden-244853.html" class="tint" title="7 Reasons Why You Should Have A Kitchen Garden">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/garden_1441277017_502x234.jpg" border="0" alt="7 Reasons Why You Should Have A Kitchen Garden" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/7-reasons-why-you-should-have-a-kitchen-garden-244853.html" title="7 Reasons Why You Should Have A Kitchen Garden">
                        7 Reasons Why You Should Have A Kitchen Garden                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/lifestyle/self/strange-and-awesome-facts-about-dreams-you-must-know-244133.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/strange-and-awesome-facts-about-dreams-you-must-know-244133.html" class="tint" title="Strange And Awesome Facts About Dreams You Must Know">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/dream-card_1439462257_502x234.gif" border="0" alt="Strange And Awesome Facts About Dreams You Must Know" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/strange-and-awesome-facts-about-dreams-you-must-know-244133.html" title="Strange And Awesome Facts About Dreams You Must Know">
                        Strange And Awesome Facts About Dreams You Must Know                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/welcome-is-back-but-should-you-be-spending-your-money-on-it-244846.html" class="tint" title="Welcome Is Back. But Should You Be Spending Your Money On It?">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/welcome-back--2-_1441280086_1441280097_1441280105_502x234.jpg" border="0" alt="Welcome Is Back. But Should You Be Spending Your Money On It?" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/welcome-is-back-but-should-you-be-spending-your-money-on-it-244846.html" title="Welcome Is Back. But Should You Be Spending Your Money On It?">
                        Welcome Is Back. But Should You Be Spending Your Money On It?                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/ajay-devgn-varun-dhawan-or-akshay-kumar-who-will-star-in-the-hindi-remake-of-transporter-244861.html" class="tint" title="Ajay Devgn, Varun Dhawan Or Akshay Kumar- Who Will Star In The Hindi Remake Of 'Transporter'?">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/bn_1441280906_1441280914_502x234.jpg" border="0" alt="Ajay Devgn, Varun Dhawan Or Akshay Kumar- Who Will Star In The Hindi Remake Of 'Transporter'?" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/ajay-devgn-varun-dhawan-or-akshay-kumar-who-will-star-in-the-hindi-remake-of-transporter-244861.html" title="Ajay Devgn, Varun Dhawan Or Akshay Kumar- Who Will Star In The Hindi Remake Of 'Transporter'?">
                        Ajay Devgn, Varun Dhawan Or Akshay Kumar- Who Will Star In The Hindi Remake Of 'Transporter'?                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/2-mothers-chronicle-their-kids-friendships-with-pets-and-the-result-is-picture-perfect-244792.html" class="tint" title="2 Mothers Chronicle Their Kids' Friendships With Pets, And The Result Is Picture Perfect">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/502_1441177517_502x234.jpg" border="0" alt="2 Mothers Chronicle Their Kids' Friendships With Pets, And The Result Is Picture Perfect" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/2-mothers-chronicle-their-kids-friendships-with-pets-and-the-result-is-picture-perfect-244792.html" title="2 Mothers Chronicle Their Kids' Friendships With Pets, And The Result Is Picture Perfect">
                        2 Mothers Chronicle Their Kids' Friendships With Pets, And The Result Is Picture Perfect                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/for-all-those-who-wanted-a-film-on-the-indrani-mukerjea-case-its-happening-244856.html" class="tint" title="For All Those Who Wanted A Film On The Indrani Mukerjea Case, It's Happening!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/a_1441278073_1441278087_502x234.jpg" border="0" alt="For All Those Who Wanted A Film On The Indrani Mukerjea Case, It's Happening!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/for-all-those-who-wanted-a-film-on-the-indrani-mukerjea-case-its-happening-244856.html" title="For All Those Who Wanted A Film On The Indrani Mukerjea Case, It's Happening!">
                        For All Those Who Wanted A Film On The Indrani Mukerjea Case, It's Happening!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/15-useful-whatsapp-hacks-thatll-sort-out-all-your-communication-woes-244773.html" class="tint" title="15 Useful WhatsApp Hacks That'll Sort Out All Your Communication Woes">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441107570_502x234.jpg" border="0" alt="15 Useful WhatsApp Hacks That'll Sort Out All Your Communication Woes" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/15-useful-whatsapp-hacks-thatll-sort-out-all-your-communication-woes-244773.html" title="15 Useful WhatsApp Hacks That'll Sort Out All Your Communication Woes">
                        15 Useful WhatsApp Hacks That'll Sort Out All Your Communication Woes                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-is-what-girls-in-the-capital-have-to-say-to-jasleen-kaur-and-its-exactly-what-needs-to-be-said-244862.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-is-what-girls-in-the-capital-have-to-say-to-jasleen-kaur-and-its-exactly-what-needs-to-be-said-244862.html" class="tint" title="This Is What Girls In The Capital Have To Say To Jasleen Kaur And It's Exactly What Needs To Be Said">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/jasleencase_card_1441281582_502x234.jpg" border="0" alt="This Is What Girls In The Capital Have To Say To Jasleen Kaur And It's Exactly What Needs To Be Said" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-is-what-girls-in-the-capital-have-to-say-to-jasleen-kaur-and-its-exactly-what-needs-to-be-said-244862.html" title="This Is What Girls In The Capital Have To Say To Jasleen Kaur And It's Exactly What Needs To Be Said">
                        This Is What Girls In The Capital Have To Say To Jasleen Kaur And It's Exactly What Needs To Be Said                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/the-desi-everybody-loves-raymond-+-9-reasons-why-indian-tv-needs-to-leave-english-shows-the-fu**-alone-244833.html" class="tint" title="The Desi Everybody Loves Raymond + 9 Reasons Why Indian TV Needs To Leave English Shows The Fu** Alone">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441262607_502x234.jpg" border="0" alt="The Desi Everybody Loves Raymond + 9 Reasons Why Indian TV Needs To Leave English Shows The Fu** Alone" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/the-desi-everybody-loves-raymond-+-9-reasons-why-indian-tv-needs-to-leave-english-shows-the-fu**-alone-244833.html" title="The Desi Everybody Loves Raymond + 9 Reasons Why Indian TV Needs To Leave English Shows The Fu** Alone">
                        The Desi Everybody Loves Raymond + 9 Reasons Why Indian TV Needs To Leave English Shows The Fu** Alone                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/move-over-munni-sheila-chameli-its-now-time-for-item-boy-ranbir-kapoor-244835.html" class="tint" title="Move Over Munni, Sheila & Chameli. It's Now Time For Item Boy Ranbir Kapoor!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/aaaa_1441269329_1441269335_502x234.jpg" border="0" alt="Move Over Munni, Sheila & Chameli. It's Now Time For Item Boy Ranbir Kapoor!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/move-over-munni-sheila-chameli-its-now-time-for-item-boy-ranbir-kapoor-244835.html" title="Move Over Munni, Sheila & Chameli. It's Now Time For Item Boy Ranbir Kapoor!">
                        Move Over Munni, Sheila & Chameli. It's Now Time For Item Boy Ranbir Kapoor!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/7-skin-care-secrets-that-will-help-you-put-your-best-face-forward-244810.html" class="tint" title="7 Skin Care Secrets That Will Help You Put Your Best Face Forward!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/skin_1441188472_502x234.jpg" border="0" alt="7 Skin Care Secrets That Will Help You Put Your Best Face Forward!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/7-skin-care-secrets-that-will-help-you-put-your-best-face-forward-244810.html" title="7 Skin Care Secrets That Will Help You Put Your Best Face Forward!">
                        7 Skin Care Secrets That Will Help You Put Your Best Face Forward!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/whoa-pyaar-ka-punchnama-boys-are-back-but-there-is-no-liquid-this-time-244841.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/whoa-pyaar-ka-punchnama-boys-are-back-but-there-is-no-liquid-this-time-244841.html" class="tint" title="Whoa! Pyaar Ka Punchnama Boys Are Back, But There Is No 'Liquid' This Time!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/cr_1441270504_1441270520_502x234.jpg" border="0" alt="Whoa! Pyaar Ka Punchnama Boys Are Back, But There Is No 'Liquid' This Time!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/whoa-pyaar-ka-punchnama-boys-are-back-but-there-is-no-liquid-this-time-244841.html" title="Whoa! Pyaar Ka Punchnama Boys Are Back, But There Is No 'Liquid' This Time!">
                        Whoa! Pyaar Ka Punchnama Boys Are Back, But There Is No 'Liquid' This Time!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/10-folic-acid-rich-foods-that-will-help-you-a-have-smoother-pregnancy-244766.html" class="tint" title="10 Folic Acid Rich Foods That Will Help You A Have Smoother Pregnancy">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1441103175_502x234.jpg" border="0" alt="10 Folic Acid Rich Foods That Will Help You A Have Smoother Pregnancy" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/10-folic-acid-rich-foods-that-will-help-you-a-have-smoother-pregnancy-244766.html" title="10 Folic Acid Rich Foods That Will Help You A Have Smoother Pregnancy">
                        10 Folic Acid Rich Foods That Will Help You A Have Smoother Pregnancy                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/how-these-people-react-when-their-loved-ones-rate-them-on-success-will-warm-your-heart-244826.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/how-these-people-react-when-their-loved-ones-rate-them-on-success-will-warm-your-heart-244826.html" class="tint" title="How These People React When Their Loved Ones Rate Them On Success Will Warm Your Heart">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/rate502_1441257884_502x234.jpg" border="0" alt="How These People React When Their Loved Ones Rate Them On Success Will Warm Your Heart" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/how-these-people-react-when-their-loved-ones-rate-them-on-success-will-warm-your-heart-244826.html" title="How These People React When Their Loved Ones Rate Them On Success Will Warm Your Heart">
                        How These People React When Their Loved Ones Rate Them On Success Will Warm Your Heart                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/the-most-animated-guy-in-pakistan-rants-against-modi-the-trouble-is-its-more-hilarious-than-offensive-244829.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/the-most-animated-guy-in-pakistan-rants-against-modi-the-trouble-is-its-more-hilarious-than-offensive-244829.html" class="tint" title="The Most Animated Guy In Pakistan Rants Against Modi, The Trouble Is - It's More Hilarious Than Offensive!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/modi_pak_card_1441259570_502x234.jpg" border="0" alt="The Most Animated Guy In Pakistan Rants Against Modi, The Trouble Is - It's More Hilarious Than Offensive!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/the-most-animated-guy-in-pakistan-rants-against-modi-the-trouble-is-its-more-hilarious-than-offensive-244829.html" title="The Most Animated Guy In Pakistan Rants Against Modi, The Trouble Is - It's More Hilarious Than Offensive!">
                        The Most Animated Guy In Pakistan Rants Against Modi, The Trouble Is - It's More Hilarious Than Offensive!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/travel/this-temple-has-a-pillar-that-does-not-rest-on-the-ground-a-walk-through-indiaâs-most-mysterious-temple-244780.html" class="tint" title="This Temple Has A Pillar That Does Not Rest On The Ground! A Walk Through Indiaâs Most Mysterious Temple">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/lepakshi_1441117284_1441117299_502x234.jpg" border="0" alt="This Temple Has A Pillar That Does Not Rest On The Ground! A Walk Through Indiaâs Most Mysterious Temple" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/travel/this-temple-has-a-pillar-that-does-not-rest-on-the-ground-a-walk-through-indiaâs-most-mysterious-temple-244780.html" title="This Temple Has A Pillar That Does Not Rest On The Ground! A Walk Through Indiaâs Most Mysterious Temple">
                        This Temple Has A Pillar That Does Not Rest On The Ground! A Walk Through Indiaâs Most Mysterious Temple                    </a>
                </figcaption>
            </div>
        
        <script id="HpMiddleBlock_tpl" type="text/x-handlebars-template">
            {{#if MoreData}}
            {{#each MoreData}} 
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>


                    <a  class='video-btn sprite' href="{{guid}}" style="display:{{card_id}}"> </a>

    				{{#if label_name.length}}
                    <a href="{{guid}}" class="{{label_name}} sticker">&nbsp;</a>
    						 {{/if}} 
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="{{guid}}" title="{{carousal_headline}}" class="tint">
                        <img class="greyBg lazy" data-original="{{thumbnail}}" border="0" alt="{{carousal_headline}}" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="{{guid}}" title="{{carousal_headline}}">
                        {{carousal_headline}}
                    </a>
                </figcaption>
            </div>  
            {{/each}} 
            {{/if}}

            </script>

        </div><!--life-panel end-->

        <div class="trending-panel cf "><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_15"><!--trending-panel-list start-->
                                        <figure>
                                
                        <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/this-hilarious-mashup-video-of-got-and-kuch-kuch-hota-hai-will-leave-you-in-splits-244918.html'>video</a>					
                        <a href="http://www.indiatimes.com/entertainment/bollywood/this-hilarious-mashup-video-of-got-and-kuch-kuch-hota-hai-will-leave-you-in-splits-244918.html" class="tint" title="This Hilarious Mashup Video Of GoT And Kuch Kuch Hota Hai Will Leave You In Splits!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/picmonkey-collage_1441455701_1441455709_218x102.jpg" border="0" alt="This Hilarious Mashup Video Of GoT And Kuch Kuch Hota Hai Will Leave You In Splits!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/this-hilarious-mashup-video-of-got-and-kuch-kuch-hota-hai-will-leave-you-in-splits-244918.html" title="This Hilarious Mashup Video Of GoT And Kuch Kuch Hota Hai Will Leave You In Splits!">
                            This Hilarious Mashup Video Of GoT And Kuch Kuch Hota Hai Will Leave You In Splits!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                <div class='container3'>                <div class="trending-panel-list cf" id="column3_16"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/culture/who-we-are/16-pictures-that-describe-the-essence-of-india-244859.html" class="tint" title="16 Pictures That Describe The Essence Of India">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/jama-masjid-card_1441280201_1441280211_218x102.jpg" border="0" alt="16 Pictures That Describe The Essence Of India"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/16-pictures-that-describe-the-essence-of-india-244859.html" title="16 Pictures That Describe The Essence Of India">
                            16 Pictures That Describe The Essence Of India                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_17"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/bollywood/music-composer-aadesh-srivastava-passes-away-one-day-before-his-birthday-leaving-a-musical-hole-244893.html" class="tint" title="Music Composer Aadesh Srivastava Passes Away One Day Before His Birthday, Leaving A Musical Hole">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/aadesh-srivastava-502_1441425674_218x102.jpg" border="0" alt="Music Composer Aadesh Srivastava Passes Away One Day Before His Birthday, Leaving A Musical Hole"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/music-composer-aadesh-srivastava-passes-away-one-day-before-his-birthday-leaving-a-musical-hole-244893.html" title="Music Composer Aadesh Srivastava Passes Away One Day Before His Birthday, Leaving A Musical Hole">
                            Music Composer Aadesh Srivastava Passes Away One Day Before His Birthday, Leaving A Musical Hole                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_18"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/celebs/9-dahi-handi-songs-from-bollywood-that-will-make-you-want-to-dance-right-now-244897.html" class="tint" title="9 Dahi Handi Songs From Bollywood That Will Make You Want To Dance Right Now!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage_1441435140_1441435150_218x102.jpg" border="0" alt="9 Dahi Handi Songs From Bollywood That Will Make You Want To Dance Right Now!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/9-dahi-handi-songs-from-bollywood-that-will-make-you-want-to-dance-right-now-244897.html" title="9 Dahi Handi Songs From Bollywood That Will Make You Want To Dance Right Now!">
                            9 Dahi Handi Songs From Bollywood That Will Make You Want To Dance Right Now!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_19"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/culture/travel/7-epic-crossborder-road-trips-that-are-bound-to-captivate-your-imagination-244901.html" class="tint" title="7 Epic Cross-Border Road Trips That Are Bound To Captivate Your Imagination Like The India-Thailand Highway">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/cp_1441440497_218x102.jpg" border="0" alt="7 Epic Cross-Border Road Trips That Are Bound To Captivate Your Imagination Like The India-Thailand Highway"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/travel/7-epic-crossborder-road-trips-that-are-bound-to-captivate-your-imagination-244901.html" title="7 Epic Cross-Border Road Trips That Are Bound To Captivate Your Imagination Like The India-Thailand Highway">
                            7 Epic Cross-Border Road Trips That Are Bound To Captivate Your Imagination Like The India-Thailand Highway                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                
        </div>

    </div><!--trending-panel end-->

    </section>
    <section class="container cf" id="loader" style="display: none;border: 0px solid red;">
    <div class="news-panel cf ">&nbsp;</div>
    <div class="life-panel cf" style="text-align: center;"><img src="http://media.indiatimes.in/resources/images/loading.gif" border="0" /></div>
    <div class="trending-panel cf ">&nbsp;</div>
</section>
<section class="big-ads remove-fixed-home" id="adfooter">
<div id="badRos_slot"></div> 
</section>   

<!------------------------ end Block 4------------------------------------------------>
<!--container end-->

<script type="text/javascript">
   
    $('#hp_block_2').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible     
       showBigAD2('bigAd2_slot');
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#hp_block_3').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    if (visiblePartY == 'top') {
      // top part of element is visible

    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible  
      showBigAD3('bigAd3_slot');
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#container4').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible

       BADros('badRos_slot');
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});

$(document).ready(function() {
	/* spotlight onload tracking homepage */
	//console.log("homepage");
			ga('send', 'event', 'OnLoad Partner Stories', '244901', 'homepage', {'nonInteraction': 1});
	           
});

$(document).ready(function(){
        var trigger_depth={b2:99, b3:499, b4:1199};
        var is_trigger_active={b2:true, b3:true, b4:true};
        var call_on_scroll=true;
        
            $(window).on("scroll",function(){
            if((!(is_trigger_active.b2)&&!(is_trigger_active.b3)&&!(is_trigger_active.b4))==false)
            {
                callPreload(is_trigger_active,trigger_depth);
                
            }
    });
});

function callPreload(active,depth)
{
    var scroll_top = $(window).scrollTop();
    if((active.b2)&&(scroll_top > depth.b2))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Sep/1card1_1441541672_1441541679_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/sunnyyy-5_1441539607_1441539611_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/nepal-earthquake12_1441535162_1441535171_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage-555_1441534748_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/suicide-600_1441539781_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/worstdrivers_card_1441532487_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/mr-beans1_1441535117_1441535141_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/lens_1441347111_502x234.jpg","http://media.indiatimes.in/media/content/2015/Jun/wakeupsid10_1433608319_1433608327_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/iphone-502_1441513728_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/kk12_1441537723_1441537728_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/worstdrivers_card_1441532487_218x102.jpg","http://media.indiatimes.in/media/content/2015/Aug/ram-leela_1440159493_1440159505_218x102.jpg");
        active.b2=false;
    }
    if((active.b3)&&(scroll_top > depth.b3))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage-5_1441529950_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/rajnath-502_1441534286_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/constables-502_1441526964_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/skcard_1441537207_1441537213_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage-5_1441521433_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/kk12_1441537723_1441537728_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/bna_1441524053_1441524060_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage1_1441524187_1441524196_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/angelina_card_1441518830_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/cardimage_1441518756_1441518766_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1441278788_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage-five-krs_1441441645_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1441447107_218x102.jpg");
        active.b3=false;
    }
    
    if((active.b4)&&(scroll_top > depth.b4))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Sep/1965-card_1441519574_1441519598_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/card2_1441517672_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/com-502_1441523613_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1441454855_236x111.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/pillowfight-cp_1441453529_1441453541_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/cp_1441275372_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/dengue-card_1441173876_502x234.gif","http://media.indiatimes.in/media/content/2015/Sep/cardimage_1441518756_1441518766_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/cp_1441343197_1441343201_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/angelina_card_1441518830_502x234.jpg","http://media.indiatimes.in/media/content/2015/Jun/wakeupsid10_1433608319_1433608327_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1441278788_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/iphone-502_1441513728_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/ram-leela_1440159493_1440159505_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/picmonkey-collage_1441455701_1441455709_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/jama-masjid-card_1441280201_1441280211_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/picmonkey-collage-5_1441451444_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collagennn_1441446310_1441446318_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1441447107_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/cp_1441101099_1441101105_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/1434632824_1441442753_1441442765_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage-five-krs_1441441645_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage_1441435140_1441435150_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/firetornado-5_1441439916_1441439931_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/whatsapp-card_1441370136_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/aadesh-srivastava-502_1441425674_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/fish_1441272211_1441272267_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1441356560_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/fb-card_1441261199_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1441368879_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/502_1441277554_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/maushi_1441363761_1441363772_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/download_1441366033_1441366041_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card-mb_1441106085_502x234.gif","http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage_1441279987_1441279998_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/food-card_1441274635_1441274646_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/fb-card_1441266613_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/screenshot_2_1441350923_1441350930_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/lady_1441261527_1441261537_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/garden_1441277017_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/dream-card_1439462257_502x234.gif","http://media.indiatimes.in/media/content/2015/Sep/welcome-back--2-_1441280086_1441280097_1441280105_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/bn_1441280906_1441280914_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/502_1441177517_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/a_1441278073_1441278087_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1441107570_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/jasleencase_card_1441281582_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1441262607_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/aaaa_1441269329_1441269335_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/skin_1441188472_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/cr_1441270504_1441270520_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1441103175_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/rate502_1441257884_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/modi_pak_card_1441259570_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/lepakshi_1441117284_1441117299_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/picmonkey-collage_1441455701_1441455709_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/jama-masjid-card_1441280201_1441280211_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/aadesh-srivastava-502_1441425674_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage_1441435140_1441435150_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/cp_1441440497_218x102.jpg");
        active.b4=false;
    }
}
</script>    <div class="clr"></div>
      
	
                 <script> 
				                    showFooterCode();
					            </script>
	           <style>
.bottom-strip{background:#ebebeb;display:block;padding:15px 0;overflow:hidden;}.bottom-strip-box{margin:0 auto;display:table;max-width:410px;width:100%;}.square-logo{width:66px;height:64px;float:left;margin-right:10px;}.bottom-strip-content{float:left;margin-top:14px;}.bottom-strip-content span{display:block;color:#737272;font:12px/1 "RobotoRegular", Arial, Helvetica, sans-serif;margin-bottom:2px;}.bottom-strip-content a{color:#1b85dd;font:24px/1 "RobotoRegular", Arial, Helvetica, sans-serif;text-decoration:none;}
</style>
<div class="last-container">
    <!--<br>--> 
    <!--social-panel start-->
    <div class="social-panel red-bg">
        <div class="soc-inner"><!--soc-inner start-->
            <p class="soc-title">Be a Part of The New & Next</p>
            <div class="soc-count"><!--soc-count start-->
                <a href="https://www.facebook.com/indiatimes" target="_blank" title="facebook" class="sprite soc-fb"></a>
                <p>
                    3,514,182<span class="black"> FRIENDS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://twitter.com/indiatimes" target="_blank" title="twitter" class="sprite soc-twt"></a>
                <p>10343  <span class="black">FOLLOWERS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://plus.google.com/+indiatimes" target="_blank" title="gplus" class="sprite soc-gplus"></a>
                <p>104,163 <span class="black">MEMBERS</span></p>
            </div>
            <div class="soc-count"><!--soc-count start-->
                <a href="javascript:void(0);" title="E-mail" class="sprite soc-mail"></a>
                <p>48,881 <span class="black">SUBSCRIBERS</span></p>
            </div>
            <!--soc-count end-->
        </div>
        <!--soc-inner end-->
    </div>
    <!--social-panel end-->
    <div class="clr"></div>

    <footer>
        <!-- social container start-->
        <div class="socical">
            <div class="containers">
                <div class="fl left_cont"> Get your weekly dose of virality sent to your inbox! </div>
                <div class="fr right_cont">

                    <input type="text" value="Please Enter Email Address" class="Email" name="UserEmail" id="UserEmail" defaultValue="Please Enter Email Address"/>
                    <input type="submit" id="btn-submit" value="SIGN UP" name="btn-submit" class="Subscribe">
                    <span id="subsEmal" style='color: red; padding-left: 66px; text-align: left;'></span>
                </div>

            </div>
        </div><!-- social containers end-->
        <div class="footer"><!-- footer grey start-->
            <div class="sub_container">
                <!-- container start-->
                <div class="cont_new">
                    <h2>Our Channels</h2>
                                                <a href='http://www.indiatimes.com/news/' class="blue size">News</a> 
                                                    <a href='http://www.indiatimes.com/lifestyle/' class="purple size">Lifestyle</a> 
                                                    <a href='http://www.indiatimes.com/entertainment/' class="red size">Entertainment</a> 
                                                    <a href='http://www.indiatimes.com/videocafe/' class="orange size">Videos</a> 
                                                    <a href='http://www.indiatimes.com/health/' class="green size">Health</a> 
                        					<a href='http://www.indiatimes.com/trending' class="size" style="color:white">Trending</a> 
                </div><!-- container closed-->

                <div class="cont"> <!-- container start-->
                    <h2>Sections</h2>
                                            <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/news/india/'>india</a>
                                             
                                    <a href='http://www.indiatimes.com/news/world/'>world</a>
                                             
                                    <a href='http://www.indiatimes.com/news/sports/'>sports</a>
                                             
                                    <a href='http://www.indiatimes.com/news/weird/'>weird</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/lifestyle/self/'>self</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/style/'>style</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/who-we-are/'>who we are</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/travel/'>travel</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/food/'>food</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/entertainment/bollywood/'>bollywood</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/celebs/'>celebscoop</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/hollywood/'>hollywood</a>
                                
                             </div>                        <div class="sub_link">
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                             
                                    <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                             
                                    <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                             
                                    <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                             
                                    <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                
                             </div>                    <div class="sub_link"><a href="http://www.indiatimes.com/photogallery">Photogallery</a></div>
                </div><!-- container closed-->
                <div class="cont_new"> <!-- container start-->
                    <h2>Indiatimes Lifestyle Network</h2>

                    <div class="sub_link">                             <a href="http://www.mensxp.com"   target="_blank" >MENS XP</a>
                                                    <a href="http://www.idiva.com"   target="_blank" >iDiva</a>
                                                    <a href="http://luxpresso.com"   target="_blank" >Luxury</a>
                                                    <a href="http://in.askmen.com/"   target="_blank" >Ask Men</a>
                                                    <a href="http://www.pursuitist.in/"   target="_blank" >Pursuitist</a>
                        </div>
                </div><!-- container closed-->
                <div class="cont"> <!-- container start-->
                    <h2>Other Sites</h2>
                    <div class="sub_link"> 
                        
                            <a href="http://www.cricbuzz.com"  target="_blank">CricBuzz</a> 
                        
                            <a href="http://www.happytrips.com/"  target="_blank">Happy Trips</a> 
                        
                            <a href="http://www.businessinsider.in/"  target="_blank">Business Insider</a> 
                        
                            <a href="http://www.zoomtv.com"  target="_blank">ZoomTv</a> 
                        
                            <a href="http://www.gizmodo.in/"  target="_blank">Gizmodo</a> 
                        
                            <a href="http://www.lifehacker.co.in/"  target="_blank">Lifehacker</a> 
                        
                            <a href="http://in.ign.com/"  target="_blank">IGN</a> 
                        
                            <a href="http://boxtv.com"  target="_blank">BoxTV</a> 
                        
                            <a href="http://www.gaana.com"  target="_blank">Gaana</a> 
                        
                            <a href="http://shopping.indiatimes.com"  target="_blank">Online Shopping</a> 
                        
                            <a href="http://www.in.techradar.com/"  target="_blank">Techradar</a> 
                        
                            <a href="http://timesdeal.com"  target="_blank">Daily Deals</a> 
                        
                            <a href="http://www.gitanjalishop.com/"  target="_blank">Gitanjali Shop</a> 
                        
                            <a href="http://www.satvikshop.com/"  target="_blank">Satvik Shop</a> 
                        
                            <a href="http://whatshot.in"  target="_blank">WHAT'S HOT</a> 
                        
                            <a href="http://zigwheels.com"  target="_blank">ZigWheels</a> 
                        
                            <a href="http://filmipop.com"  target="_blank">FILMIPOP</a> 
                        
                            <a href="http://www.filmfare.com/"  target="_blank">Filmfare</a> 
                        
                            <a href="http://www.femina.in"  target="_blank">Femina</a> 
                        
                            <a href="http://mobile.indiatimes.com"  target="_blank">Mobile</a> 
                        
                            <a href="http://www.greetzap.com"  target="_blank">Greetings</a> 
                        
                            <a href="http://www.follo.co.in"  target="_blank">Follo</a> 
                        
                            <a href="http://timesjobs.com"  target="_blank">Jobs</a> 
                        
                            <a href="http://magicbricks.com"  target="_blank">Property</a> 
                        
                            <a href="http://www.astrospeak.com"  target="_blank">Astrology</a> 
                        
                            <a href="http://speakingtree.in"  target="_blank">Speaking Tree</a> 
                        
                            <a href="http://simplymarry.com"  target="_blank">Matrimonial</a> 
                        
                            <a href="http://itimes.com"  target="_blank">iTimes</a> 
                        
                            <a href="http://www.ads2book.com/"  target="_blank">Ads2book</a> 
                        
                            <a href="http://www.tcnext.com/"  target="_blank">TcNext</a> 
                                            </div>
                </div><!-- container closed-->
                <div class="cont border_none"><!-- container start-->
                    <h2>About us</h2>
                    <div class="sub_link">
                                                    <a href='http://www.indiatimes.com/privacypolicy' >Privacy Policy</a> 
                                                    <a href='http://www.indiatimes.com/sitemap.html' >sitemap</a> 
                                                    <a href='http://www.indiatimes.com/termsandcondition' >Terms & CONDITIONS</a> 
                                                    <a href='http://www.indiatimes.com/seoarchive' >ARCHIVES</a> 
                                                    <a href='http://www.indiatimes.com/contactus' >Contact us</a> 
                                                    <a href='http://www.indiatimes.com/aboutus' >ABOUT INDIATIMES</a> 
                                            </div>
                </div><!-- container closed-->
            </div><!-- footer grey closed-->
        </div><!-- footer black closed-->

        <div class="black_footer"><!-- footer black start-->
            <div class="containers">
                <p><a href="javascript:void(0)" class="copyrt">Copyright Â© 2015 Times Internet Limited. Powered by Indiatimes Lifestyle Network. All rights reserved</a></p>
                <span><a href="http://www.indiatimes.com/seoarchive/">Archive</a> | <a href="http://www.indiatimes.com/sitemap.html">Sitemap</a> | <a href="http://www.indiatimes.com/aboutus/">About Us</a> | <a href="http://www.indiatimes.com/contactus/">Contact Us</a> | <a href="http://www.indiatimes.com/privacypolicy/">Privacy Policy</a> | <a target="_blank" href="http://m.indiatimes.com">Indiatimes Mobile</a> | <a href="http://www.indiatimes.com/termsandcondition/">Terms of Use and Grievance Redressal Policy </a> </span> 
            </div>
        </div><!-- footer black end-->
    </footer>
</div>

    <script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.lazyload.min.js?v=100.11" charset="utf-8"></script>
<script defer type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.easing.1.3.min.js?v=100.11"></script>

<input type="hidden" name="currentId" id="currentId" value="">
    <div class="animate_cont dawn floating delay5">
        <span class="animate_arrow sprite_image"></span>
    </div>



<script type="text/javascript" src="http://media.indiatimes.in/resources/js/main.js?v=100.11"></script>    
<script type="text/javascript" src="http://media.indiatimes.in/resources/js/common.js?v=100.11"></script>
<script  defer type="text/javascript" src="http://media.indiatimes.in/resources/js/jquery-scrolltofixed.js?v=100.11"></script>

<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://cdn.mouseflow.com/projects/1e238441-c30e-4d1b-8edd-c0f14d8fa6b7.js' type='text/javascript'%3E%3C/script%3E"));</script>
<!-- mouse flow code ends-->
<script  src="http://tags.crwdcntrl.net/c/2818/cc.js?ns=_cc2818" id="LOTCC_2818"></script> 

<script> 
    _cc2818.bcp(); 
</script>

	 <p id="back-top" style="display:none;"><a href="#top"><span></span></a> </p>
</div><!--wrap end-->


</body>
</html>