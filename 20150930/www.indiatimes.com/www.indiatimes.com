<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trending  stories on Indian Lifestyle, Culture, Relationships, Food, Travel, Entertainment, News & New Technology News - Indiatimes.com</title>

<meta name="keywords" content="" />
<meta name="description" content="Indiatimes.com brings you the news, articles, stories and videos on entertainment, latest lifestyle, culture & new technologies emerging worldwide." />



		

<link rel="image_src" href="http://media.indiatimes.in/resources/images/fbimage.png" />
<meta property="og:title" content="" />    
<meta property="og:url" content="http://www.indiatimes.com" />
<meta property="og:image" content="http://media.indiatimes.in/resources/images/fbimage.png" />

<meta property="og:site_name" content="indiatimes.com" />
<meta property="fb:app_id" content="117787264903013" />
<meta property="og:description" content="" />
<meta name="google-site-verification" content="ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw" />
<meta name="msvalidate.01" content="BAB71AAED7CCC01DB106A58E99625EB4" />
<link href="https://plus.google.com/+indiatimes/posts" rel="publisher" />

<!--<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />-->

<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">    <link rel="shortcut icon" href="http://media.indiatimes.in/resources/images/favicon.ico?v=25" type="image/x-icon"><link rel="stylesheet" href="http://media.indiatimes.in/resources/css/site.css?v=100.26" media="screen"><link rel="stylesheet" href="http://www.indiatimes.com/fonts/fonts.min.css?v=100.26" media="screen"><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery-1.11.1.min.js?v=100.26"></script><script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/handlebars-v1.3.0.js?v=100.26"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.inview.min.js?v=100.26"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.preload.min.js?v=100.26"></script><script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script><!-- Facebook Pixel Code --><script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');fbq('init', '853339751421540');fbq('track', 'PageView');</script><noscript><img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=853339751421540&ev=PageView&noscript=1"/></noscript><!-- End Facebook Pixel Code -->           <!--  Analytics Code Begin -->    <!-- Begin comScore Tag -->    <script>        var _comscore = _comscore || [];        _comscore.push({ c1: "2", c2: "6036484" });        (function() {            var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";            el.parentNode.insertBefore(s, el);        })();    </script>    <noscript>    <img src="http://b.scorecardresearch.com/p?c1=2&c2=6036484&cv=2.0&cj=1" />    </noscript>    <!-- End comScore Tag -->    <!-- Begin Google Analytics Tag -->    <script>        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');        ga('create', 'UA-198011-6', 'indiatimes.com');            ga('require', 'displayfeatures');        ga('send', 'pageview');    </script>    <!-- End Google Analytics Tag -->    <!--  Analytics Code Begin --><!--adcode common script--><script type='text/javascript' src="http://ad.crwdcntrl.net/5/c=2800/pe=y/var=_ccaud"></script><script type="text/javascript">    var googletag = googletag || {};    googletag.cmd = googletag.cmd || [];    (function() {        var gads = document.createElement("script");        gads.async = true;        gads.type = "text/javascript";        var useSSL = "https:" == document.location.protocol;        gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";        var node =document.getElementsByTagName("script")[0];        node.parentNode.insertBefore(gads, node);    })();</script> <script type="text/javascript">        var nid='';        var cid='';        var sid='';        var brand_name='';                                                        </script>            <script type="text/javascript" src="http://media.indiatimes.in/resources/js/banner/home.js?v=100.26"></script>
            
<!--[if lt IE 9]>
  <script type="text/javascript" src="http://media.indiatimes.in/js/html5.js?v1.1"></script>
  <![endif]-->
<script>
var isDetailPage = 0</script>
</head>
<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];   
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=117787264903013&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script> 	
	
<div id="wrap" class="cf"><!--wrap start-->
    <div class='darkDisabledBg' id='disabledBg'></div>
<header>
    <div class="header-inner">
        <div class="comm-header ">
            <nav>
                <div class='forTop'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>

                    <div class="lnks">
                                                        <a href="http://www.indiatimes.com/news/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','News');">News</a> 
                                                               <a href="http://www.indiatimes.com/lifestyle/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Lifestyle');">Lifestyle</a> 
                                                               <a href="http://www.indiatimes.com/entertainment/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Entertainment');">Entertainment</a> 
                                                               <a href="http://www.indiatimes.com/videocafe/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Videos');">Videos</a> 
                                                               <a href="http://www.indiatimes.com/health/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Health');">Health</a> 
                                                                                          <a href='http://www.indiatimes.com/trending'  target="_blank" class="#fffff" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Trending');">Trending</a> 
                                                </div>
                </div>
                <div class='onScrolled'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>
                    <a href="javascript:;" class="menu rc sprite" id="headerMenu">Menu</a>
                    <div class="has-tag">
                                           </div>
                </div>

                                
            </nav>
			                 
	    <div id="sticker" style="display:none;"><span class="readTitle"></span></div>
                    <div class="socls fr share">
            </div>
                        
            
			 <div class="social fr">
                            <a title="facebook" target="_blank"  class="sprite fb" href="https://www.facebook.com/indiatimes">facebook</a>
                            <a title="twitter" target="_blank"  class="sprite twt" href="https://twitter.com/indiatimes">twitter</a>
                            <a title="search"  class="sprite serach" onclick="$('body').showSearch();" href="javascript:void(0);">search</a>
                        </div>
        </div>
    </div>
</header>
<div class="clr"></div>
<div id="overlay" class="animated bounceOut"><!--overlay start-->
    <div class="search_conts"><!--search-cont start-->
        <form onsubmit="return false;" class="serach-form">
            <input type="text" onkeyup="search_data();" class="input" onfocus="if(this.value=='Search'){this.value=''}" onblur="if(this.value==''){this.value='Search'};" value="Search" name="q" id="q" autocomplete="off">
            <input type="button" class="sprite src-btn" onclick="search_data();">
            <a class="sprite sclose" id="close" href="javascript:void(0);">X</a>
        </form>
        <div class="scroll-panes" id="scroll-panes">
            <div class="clr"></div>
            <div class="res-msg" id="res-msg"></div>
            <div class="search-list cf" id="results"></div>
            <div class="clr"></div>
            <div class="loadmore" id="progressBar" style="display: none;">Loading......</div>
            <a class="loadmore" style="display:none;" onclick="load_search_data(); " id="load_more_button" href="javascript:void(0);">Load more</a>
        </div>
    </div>
</div>
<div class="blk">
    <div class="inner">
        <dl id="leftMenu" class="leftMenu accordion">
            <dt data-color="red-bg" class=""><a href="http://www.indiatimes.com">Home</a></dt>
            <dd> </dd>
                            <dt  data-color="blue-bg" ><a href="http://www.indiatimes.com/news/">News</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/news/india/'>India</a>
                                     
                            <a href='http://www.indiatimes.com/news/world/'>World</a>
                                     
                            <a href='http://www.indiatimes.com/news/sports/'>Sports</a>
                                     
                            <a href='http://www.indiatimes.com/news/weird/'>Weird</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="purple-bg" ><a href="http://www.indiatimes.com/lifestyle/">Lifestyle</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/lifestyle/self/'>Self</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/style/'>Style</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                     
                            <a href='http://www.indiatimes.com/culture/who-we-are/'>Who we are</a>
                                     
                            <a href='http://www.indiatimes.com/culture/travel/'>Travel</a>
                                     
                            <a href='http://www.indiatimes.com/culture/food/'>Food</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="red-bg" ><a href="http://www.indiatimes.com/entertainment/">Entertainment</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/entertainment/bollywood/'>Bollywood</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/celebs/'>Celebscoop</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/hollywood/'>Hollywood</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="orange-bg" ><a href="http://www.indiatimes.com/videocafe/">Videos</a>
                                </dt>
                                <dt  data-color="green-bg" ><a href="http://www.indiatimes.com/health/">Health</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                     
                            <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                     
                            <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                     
                            <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                     
                            <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                            </dd>
                    <div class="clr"></div>
                				<dt data-color="" class=""><a href="http://www.indiatimes.com/trending/">Trending</a>
                           </dt>
            <dt class="pink-bg">
            <div class="follow">Follow indiatimes </div>
            <div class="follow_cont"> 
                <a href="https://www.facebook.com/indiatimes" class="fbleft sprite"> facebook</a> 
                <a href="https://twitter.com/indiatimes" class="twtleft sprite"> twitter</a> 
            </div>
            </dt>

            <dd>&nbsp;</dd>
            <div class="clr"></div>
        </dl>
    </div>
</div>


<div class="clr"></div>
<div class="dummy-cont">&nbsp;</div>
<div class="clr"></div>


<div id="pushdown">
    <div class="pull-ad"><!--pull-ad start-->   
	
                <script type="text/javascript"> 
                    showHeaderCode();
                </script>
	    </div><!--pull-ad end-->
</div>
    <!--testing 15-09-30 06:40:02-->  

    <section id="hp_block_1" class="container cf" style="border: 0px solid red;"><!--container start-->
                

        <div class="news-panel cf"><!--news-panel start-->
            <h2>Big news</h2>

            <div class="feature-list cf"><!--feature-list start-->	
                                    <figure>
                        <div class="hash-tag">
                             20 hours ago                         </div>
						 						          
                        <a href="http://www.indiatimes.com/culture/travel/after-road-to-thailand-highway-to-bhutan-nepal-and-bangladesh-to-be-opened-soon-245658.html" class=" tint" title="After Road To Thailand, Highway To Bhutan, Nepal And Bangladesh To Be Opened Soon">
                            <img  class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Sep/road-card2-4_1443503717_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/road-card2-4_1443503717_236x111.jpg"  border="0" alt="After Road To Thailand, Highway To Bhutan, Nepal And Bangladesh To Be Opened Soon"/></a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/travel/after-road-to-thailand-highway-to-bhutan-nepal-and-bangladesh-to-be-opened-soon-245658.html" title="After Road To Thailand, Highway To Bhutan, Nepal And Bangladesh To Be Opened Soon">
                            After Road To Thailand, Highway To Bhutan, Nepal And Bangladesh To Be Opened Soon                        </a>
                    </figcaption>
                            </div><!--feature-list end-->

                            <div class="news-panel-list cf" id="column1_0"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            11 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/3-schoolboys-win-international-grant-after-spending-a-year-resolving-hunger-in-india-245715.html" title="3 Schoolboys Win International Grant After Spending A Year Resolving Hunger In India" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Sep/3-5_1443535373_1443535377_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/3-5_1443535373_1443535377_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/3-schoolboys-win-international-grant-after-spending-a-year-resolving-hunger-in-india-245715.html" title="3 Schoolboys Win International Grant After Spending A Year Resolving Hunger In India">
                            3 Schoolboys Win International Grant After Spending A Year Resolving Hunger In India                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_1"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                        		
						 	                                <a href="http://www.indiatimes.com/news/sports/shocking-players-run-for-their-lives-after-harassed-referee-pulls-out-gun-on-the-pitch-245714.html" class="wtf sticker">&nbsp;</a>
                            <a class='video-btn sprite' href='http://www.indiatimes.com/news/sports/shocking-players-run-for-their-lives-after-harassed-referee-pulls-out-gun-on-the-pitch-245714.html'>video</a>                        <a href="http://www.indiatimes.com/news/sports/shocking-players-run-for-their-lives-after-harassed-referee-pulls-out-gun-on-the-pitch-245714.html" title="Shocking! Players Run For Their Lives After Harassed Referee Pulls Out Gun On The Pitch" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/videocafe/2015/Sep/refereegunfootball_1443531711_236x111.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Sep/refereegunfootball_1443531711_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/shocking-players-run-for-their-lives-after-harassed-referee-pulls-out-gun-on-the-pitch-245714.html" title="Shocking! Players Run For Their Lives After Harassed Referee Pulls Out Gun On The Pitch">
                            Shocking! Players Run For Their Lives After Harassed Referee Pulls Out Gun On The Pitch                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_2"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/branded-a-pakistani-terrorist-this-muslim-man-was-beaten-to-death-245712.html" title="Branded A Pakistani Terrorist, This Muslim Man Was Beaten To Death" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Sep/reuters-5_1443530920_1443530939_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/reuters-5_1443530920_1443530939_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/branded-a-pakistani-terrorist-this-muslim-man-was-beaten-to-death-245712.html" title="Branded A Pakistani Terrorist, This Muslim Man Was Beaten To Death">
                            Branded A Pakistani Terrorist, This Muslim Man Was Beaten To Death                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_3"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            13 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/india/meet-vijay-chauthaiwale-the-man-behind-operation-namorica-245710.html" title="Meet Vijay Chauthaiwale, The Man Behind Operation NaMorica" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Sep/vijay-502_1443529466_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/vijay-502_1443529466_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/meet-vijay-chauthaiwale-the-man-behind-operation-namorica-245710.html" title="Meet Vijay Chauthaiwale, The Man Behind Operation NaMorica">
                            Meet Vijay Chauthaiwale, The Man Behind Operation NaMorica                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                    </div><!--news-panel end-->

        <div class="life-panel cf" id="middleBlock1"><!--life-panel start-->
            <h2>lifestyle</h2>

                                <div class="life-panel-list cf"><!--life-panel-list start-->
                        <figure><span class="label-tag spon">Partner</span>
                            
                               
                            <a href="http://www.indiatimes.com/culture/who-we-are/10-things-that-could-cross-your-mind-when-you-hear-the-word-million-245698.html" class=" tint" title="" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/10-things-that-could-cross-your-mind-when-you-hear-the-word-million-245698.html');">
                                <img  class="greyBg23 lazy23"  src="http://media.indiatimes.in/media/content/2015/Sep/card_1443527946_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/card_1443527946_502x234.jpg"  border="0" alt="10 Things That Could Cross Your Mind When You Hear The Word Million" class="img-responsive"/>
                            </a>
                        </figure>
                        <figcaption>
                            <a href="http://www.indiatimes.com/culture/who-we-are/10-things-that-could-cross-your-mind-when-you-hear-the-word-million-245698.html" title="10 Things That Could Cross Your Mind When You Hear The Word Million" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/10-things-that-could-cross-your-mind-when-you-hear-the-word-million-245698.html');">10 Things That Could Cross Your Mind When You Hear The Word Million</a>
                        </figcaption>
                    </div><!--life-panel-list end-->
                                    <div class="life-panel-list cf"><!--life-panel-list start-->
                        <figure><span class="label-tag spon">Partner</span>
                            
                               
                            <a href="http://www.indiatimes.com/culture/who-we-are/7-occasions-in-life-when-weâre-more-than-happy-to-say-âthe-sweets-are-on-usâ-245675.html" class=" tint" title="" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/7-occasions-in-life-when-weâre-more-than-happy-to-say-âthe-sweets-are-on-usâ-245675.html');">
                                <img  class="greyBg23 lazy23"  src="http://media.indiatimes.in/media/content/2015/Sep/card_1443528573_1443528576_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/card_1443528573_1443528576_502x234.jpg"  border="0" alt="7 Occasions In Life When Weâre More Than Happy To Say âThe Sweets Are On Usâ" class="img-responsive"/>
                            </a>
                        </figure>
                        <figcaption>
                            <a href="http://www.indiatimes.com/culture/who-we-are/7-occasions-in-life-when-weâre-more-than-happy-to-say-âthe-sweets-are-on-usâ-245675.html" title="7 Occasions In Life When Weâre More Than Happy To Say âThe Sweets Are On Usâ" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/7-occasions-in-life-when-weâre-more-than-happy-to-say-âthe-sweets-are-on-usâ-245675.html');">7 Occasions In Life When Weâre More Than Happy To Say âThe Sweets Are On Usâ</a>
                        </figcaption>
                    </div><!--life-panel-list end-->
                

                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                   
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/healthyliving/these-photos-of-200-calories-of-junk-food-vs-healthy-food-put-a-lot-of-things-in-perspective-245652.html" class="tint" title="These Photos Of 200 Calories Of Junk Food VS Healthy Food Put A Lot Of Things In Perspective" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/health/healthyliving/these-photos-of-200-calories-of-junk-food-vs-healthy-food-put-a-lot-of-things-in-perspective-245652.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Sep/card-1_1443432804_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/card-1_1443432804_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/these-photos-of-200-calories-of-junk-food-vs-healthy-food-put-a-lot-of-things-in-perspective-245652.html" title="These Photos Of 200 Calories Of Junk Food VS Healthy Food Put A Lot Of Things In Perspective" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/health/healthyliving/these-photos-of-200-calories-of-junk-food-vs-healthy-food-put-a-lot-of-things-in-perspective-245652.html');">
                            These Photos Of 200 Calories Of Junk Food VS Healthy Food Put A Lot Of Things In Perspective                        </a>
                    </figcaption>
                </div>
                        
        </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_0">
                    <span class="strip skyblue-bg"></span>                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/embarrassing-video-surfaces-showing-satya-nadella-wiping-his-hands-after-handshake-with-pm-modi-245681.html'>video</a>                        <a href="http://www.indiatimes.com/videocafe/embarrassing-video-surfaces-showing-satya-nadella-wiping-his-hands-after-handshake-with-pm-modi-245681.html" class="tint" title="Embarrassing Video Surfaces Showing Satya Nadella Wiping His Hands After Handshake With PM Modi">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/videocafe/2015/Sep/modi_card_1443510874_218x102.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Sep/modi_card_1443510874_218x102.jpg" border="0" alt="Embarrassing Video Surfaces Showing Satya Nadella Wiping His Hands After Handshake With PM Modi"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/embarrassing-video-surfaces-showing-satya-nadella-wiping-his-hands-after-handshake-with-pm-modi-245681.html" title="Embarrassing Video Surfaces Showing Satya Nadella Wiping His Hands After Handshake With PM Modi">
                            Embarrassing Video Surfaces Showing Satya Nadella Wiping His Hands After Handshake With PM Modi                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_1">
                                        <figure>

                                                <a href="http://www.indiatimes.com/entertainment/hollywood/paul-walkers-daughter-sues-porsche-over-the-accident-that-killed-her-dad-245679.html" class="tint" title="Paul Walker's Daughter Sues Porsche Over The Accident That Killed Her Dad">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage-5_1443510382_1443510389_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage-5_1443510382_1443510389_218x102.jpg" border="0" alt="Paul Walker's Daughter Sues Porsche Over The Accident That Killed Her Dad"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/hollywood/paul-walkers-daughter-sues-porsche-over-the-accident-that-killed-her-dad-245679.html" title="Paul Walker's Daughter Sues Porsche Over The Accident That Killed Her Dad">
                            Paul Walker's Daughter Sues Porsche Over The Accident That Killed Her Dad                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_2">
                                        <figure>

                                                <a href="http://www.indiatimes.com/entertainment/bollywood/11-things-well-really-miss-in-hera-pheri-3-since-akshay-kumar-is-not-a-part-of-it-245688.html" class="tint" title="11 Things We'll Really Miss In 'Hera Pheri 3' Since Akshay Kumar Is Not A Part Of It">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage-cd_1443521177_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage-cd_1443521177_218x102.jpg" border="0" alt="11 Things We'll Really Miss In 'Hera Pheri 3' Since Akshay Kumar Is Not A Part Of It"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/11-things-well-really-miss-in-hera-pheri-3-since-akshay-kumar-is-not-a-part-of-it-245688.html" title="11 Things We'll Really Miss In 'Hera Pheri 3' Since Akshay Kumar Is Not A Part Of It">
                            11 Things We'll Really Miss In 'Hera Pheri 3' Since Akshay Kumar Is Not A Part Of It                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_3">
                                        <figure>

                                                <a href="http://www.indiatimes.com/culture/travel/after-road-to-thailand-highway-to-bhutan-nepal-and-bangladesh-to-be-opened-soon-245658.html" class="tint" title="After Road To Thailand, Highway To Bhutan, Nepal And Bangladesh To Be Opened Soon">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Sep/road-card2-4_1443503717_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/road-card2-4_1443503717_218x102.jpg" border="0" alt="After Road To Thailand, Highway To Bhutan, Nepal And Bangladesh To Be Opened Soon"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/travel/after-road-to-thailand-highway-to-bhutan-nepal-and-bangladesh-to-be-opened-soon-245658.html" title="After Road To Thailand, Highway To Bhutan, Nepal And Bangladesh To Be Opened Soon">
                            After Road To Thailand, Highway To Bhutan, Nepal And Bangladesh To Be Opened Soon                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_4">
                                        <figure>

                                                <a href="http://www.indiatimes.com/health/healthyliving/these-photos-of-200-calories-of-junk-food-vs-healthy-food-put-a-lot-of-things-in-perspective-245652.html" class="tint" title="These Photos Of 200 Calories Of Junk Food VS Healthy Food Put A Lot Of Things In Perspective">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Sep/card-1_1443432804_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Sep/card-1_1443432804_218x102.jpg" border="0" alt="These Photos Of 200 Calories Of Junk Food VS Healthy Food Put A Lot Of Things In Perspective"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/these-photos-of-200-calories-of-junk-food-vs-healthy-food-put-a-lot-of-things-in-perspective-245652.html" title="These Photos Of 200 Calories Of Junk Food VS Healthy Food Put A Lot Of Things In Perspective">
                            These Photos Of 200 Calories Of Junk Food VS Healthy Food Put A Lot Of Things In Perspective                        </a>
                    </figcaption>
                </div>
                        </div><!--trending-panel end-->
    </section><!--container end-->

<!-- -------------------    end Block 1  ------------------------------------- -->
<section class="big-ads" id="ad1"> 
<div id="bigAd1_slot"></div>
    <script>        
        showBigAD1('bigAd1_slot');
    </script> 
</section>
<!-- -------------------    start Block 2  ------------------------------------- -->

    <section id="hp_block_2" class="container cf" style="border: 0px solid red;"><!--container start-->
        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
                            <div class="news-panel-list cf" id="column1_4"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            13 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/sports/british-boxer-talks-trash-to-vijender-before-his-first-pro-fight-promises-to-send-a-brutal-message-home-245708.html" title="British Boxer Talks Trash To Vijender Before His First Pro Fight. Promises To 'Send A Brutal Message Home'" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/vijendersonny_1443525340_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/sports/british-boxer-talks-trash-to-vijender-before-his-first-pro-fight-promises-to-send-a-brutal-message-home-245708.html" title="British Boxer Talks Trash To Vijender Before His First Pro Fight. Promises To 'Send A Brutal Message Home'">
                            British Boxer Talks Trash To Vijender Before His First Pro Fight. Promises To 'Send A Brutal Message Home'                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_5"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            14 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/2-hilarious-crime-cases-prove-beyond-doubt-that-uttar-pradesh-has-the-funniest-thieves-245706.html" title="2 Hilarious Crime Cases Prove Beyond Doubt That Uttar Pradesh Has The Funniest Thieves" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1443524595_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/2-hilarious-crime-cases-prove-beyond-doubt-that-uttar-pradesh-has-the-funniest-thieves-245706.html" title="2 Hilarious Crime Cases Prove Beyond Doubt That Uttar Pradesh Has The Funniest Thieves">
                            2 Hilarious Crime Cases Prove Beyond Doubt That Uttar Pradesh Has The Funniest Thieves                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_6"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            14 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/indian-mps-have-some-of-the-lowest-salaries-in-the-world-but-no-worries-they-may-get-a-raise-soon-245702.html" title="Indian MPs Have Some Of The Lowest Salaries In The World. But No Worries They May Get A Raise Soon" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1443523882_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/indian-mps-have-some-of-the-lowest-salaries-in-the-world-but-no-worries-they-may-get-a-raise-soon-245702.html" title="Indian MPs Have Some Of The Lowest Salaries In The World. But No Worries They May Get A Raise Soon">
                            Indian MPs Have Some Of The Lowest Salaries In The World. But No Worries They May Get A Raise Soon                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_7"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            14 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/rs-15-crore-per-annum-thats-a-common-package-for-iitians-even-before-final-placements-begin-245703.html" title="Rs 1.5 Crore Per Annum - That's A Common Package For IITians Even Before Final Placements Begin" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/iit-grad-502_1443525546_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/rs-15-crore-per-annum-thats-a-common-package-for-iitians-even-before-final-placements-begin-245703.html" title="Rs 1.5 Crore Per Annum - That's A Common Package For IITians Even Before Final Placements Begin">
                            Rs 1.5 Crore Per Annum - That's A Common Package For IITians Even Before Final Placements Begin                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_8"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            15 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/world/an-atmosphere-to-breathe-in-+-other-resources-that-will-ensure-that-mars-becomes-a-human-colony-in-future-245695.html" title="An Atmosphere To Breathe In + Other Resources That Will Ensure That Mars Becomes A Human Colony In Future" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/mars5_1443520648_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/world/an-atmosphere-to-breathe-in-+-other-resources-that-will-ensure-that-mars-becomes-a-human-colony-in-future-245695.html" title="An Atmosphere To Breathe In + Other Resources That Will Ensure That Mars Becomes A Human Colony In Future">
                            An Atmosphere To Breathe In + Other Resources That Will Ensure That Mars Becomes A Human Colony In Future                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/bollywood/11-things-well-really-miss-in-hera-pheri-3-since-akshay-kumar-is-not-a-part-of-it-245688.html" class="tint" title="11 Things We'll Really Miss In 'Hera Pheri 3' Since Akshay Kumar Is Not A Part Of It">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage-cd_1443521177_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/11-things-well-really-miss-in-hera-pheri-3-since-akshay-kumar-is-not-a-part-of-it-245688.html" title="11 Things We'll Really Miss In 'Hera Pheri 3' Since Akshay Kumar Is Not A Part Of It">
                            11 Things We'll Really Miss In 'Hera Pheri 3' Since Akshay Kumar Is Not A Part Of It                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/lifestyle/a-rigged-game-of-monopoly-and-a-chance-card-this-girl-really-couldnt-say-no-to-this-proposal-245704.html" class="tint" title="A Rigged Game Of Monopoly And A Chance Card - This Girl Really Couldn't Say 'No' To This Proposal">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage2_1443523542_1443523547_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/a-rigged-game-of-monopoly-and-a-chance-card-this-girl-really-couldnt-say-no-to-this-proposal-245704.html" title="A Rigged Game Of Monopoly And A Chance Card - This Girl Really Couldn't Say 'No' To This Proposal">
                            A Rigged Game Of Monopoly And A Chance Card - This Girl Really Couldn't Say 'No' To This Proposal                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/nawazuddin-to-play-psycho-killer-in-kashyaps-next-+-7-facts-about-the-criminal-who-inspired-this-film-245690.html" class="tint" title="Nawazuddin Siddiqui To Play Psycho Killer In Kashyap's Next + 7 Facts About The Criminal Who Inspired This Film">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1443515794_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/nawazuddin-to-play-psycho-killer-in-kashyaps-next-+-7-facts-about-the-criminal-who-inspired-this-film-245690.html" title="Nawazuddin Siddiqui To Play Psycho Killer In Kashyap's Next + 7 Facts About The Criminal Who Inspired This Film">
                            Nawazuddin Siddiqui To Play Psycho Killer In Kashyap's Next + 7 Facts About The Criminal Who Inspired This Film                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_5"><!--trending-panel-list start-->
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/nothing-can-come-between-narendra-modi-and-a-camera-not-even-mark-zuckerberg-245663.html'>video</a>
                        <a href="http://www.indiatimes.com/videocafe/nothing-can-come-between-narendra-modi-and-a-camera-not-even-mark-zuckerberg-245663.html" class="tint" title="Nothing Can Come Between Narendra Modi And A Camera. Not Even Mark Zuckerberg!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/modi_cam_card_1443442344_218x102.jpg" border="0" alt="Nothing Can Come Between Narendra Modi And A Camera. Not Even Mark Zuckerberg!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/nothing-can-come-between-narendra-modi-and-a-camera-not-even-mark-zuckerberg-245663.html" title="Nothing Can Come Between Narendra Modi And A Camera. Not Even Mark Zuckerberg!">
                            Nothing Can Come Between Narendra Modi And A Camera. Not Even Mark Zuckerberg!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_6"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/culture/who-we-are/10-bizzare-coffeemaking-techniques-from-all-over-the-world-245624.html" class="tint" title="10 Bizzare Coffee-Making Techniques From All Over The World">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/coffee-and-cheese2_1443419643_1443419654_218x102.jpg" border="0" alt="10 Bizzare Coffee-Making Techniques From All Over The World"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/10-bizzare-coffeemaking-techniques-from-all-over-the-world-245624.html" title="10 Bizzare Coffee-Making Techniques From All Over The World">
                            10 Bizzare Coffee-Making Techniques From All Over The World                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_7"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/health/healthyliving/5-staircase-exercises-that-are-your-stairway-to-fitness-and-weight-loss-245648.html" class="tint" title="5 Staircase Exercises That Are Your Stairway To Fitness And Weight Loss">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card-1_1443433289_218x102.jpg" border="0" alt="5 Staircase Exercises That Are Your Stairway To Fitness And Weight Loss"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/5-staircase-exercises-that-are-your-stairway-to-fitness-and-weight-loss-245648.html" title="5 Staircase Exercises That Are Your Stairway To Fitness And Weight Loss">
                            5 Staircase Exercises That Are Your Stairway To Fitness And Weight Loss                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_8"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/health/healthyliving/7-health-conditions-that-your-feet-can-warn-you-about-245647.html" class="tint" title="7 Health Conditions That Your Feet Can Warn You About!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1443429737_218x102.jpg" border="0" alt="7 Health Conditions That Your Feet Can Warn You About!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/7-health-conditions-that-your-feet-can-warn-you-about-245647.html" title="7 Health Conditions That Your Feet Can Warn You About!">
                            7 Health Conditions That Your Feet Can Warn You About!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_9"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/lifestyle/technology/nasa-finds-liquid-water-on-mars-could-life-also-exist-on-the-red-planet-245668.html" class="tint" title="NASA Finds Liquid Water On Mars. Could Life Also Exist On The Red Planet?">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/mars---card2_1443459621_218x102.jpg" border="0" alt="NASA Finds Liquid Water On Mars. Could Life Also Exist On The Red Planet?"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/technology/nasa-finds-liquid-water-on-mars-could-life-also-exist-on-the-red-planet-245668.html" title="NASA Finds Liquid Water On Mars. Could Life Also Exist On The Red Planet?">
                            NASA Finds Liquid Water On Mars. Could Life Also Exist On The Red Planet?                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div>

    </section><!--container end-->

<!--------------------------- end Block2 ------------------------------------------>
<section class="big-ads" id="ad2"> 
<div id="bigAd2_slot"></div>
</section>
<!--------------------------- start Block3 ------------------------------------------>

    <section id="hp_block_3" class="container cf"><!--container start-->

        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
          
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_9">
                    <figure>
                        <div class="hash-tag">
                            15 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/government-plays-flip-flop-over-ethical-hacker-ankit-fadias-appointment-as-digital-india-brand-ambassador-245694.html" title="Government Plays Flip Flop Over 'Ethical Hacker' Ankit Fadia's Appointment As Digital India Brand Ambassador" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/ankit-fadia-502_1443520542_236x111.jpg" border="0" alt="Government Plays Flip Flop Over 'Ethical Hacker' Ankit Fadia's Appointment As Digital India Brand Ambassador"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/government-plays-flip-flop-over-ethical-hacker-ankit-fadias-appointment-as-digital-india-brand-ambassador-245694.html" title="Government Plays Flip Flop Over 'Ethical Hacker' Ankit Fadia's Appointment As Digital India Brand Ambassador">
                            Government Plays Flip Flop Over 'Ethical Hacker' Ankit Fadia's Appointment As Digital India Brand Ambassador                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_10">
                    <figure>
                        <div class="hash-tag">
                            16 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/one-car-per-family-aaps-new-strategy-to-control-traffic-congestion-and-air-pollution-in-the-capital-245691.html" title="One Car Per Family, AAP's New Strategy To Control Traffic Congestion And Air Pollution In The Capital" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/delhi-traffic-502_1443515530_236x111.jpg" border="0" alt="One Car Per Family, AAP's New Strategy To Control Traffic Congestion And Air Pollution In The Capital"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/one-car-per-family-aaps-new-strategy-to-control-traffic-congestion-and-air-pollution-in-the-capital-245691.html" title="One Car Per Family, AAP's New Strategy To Control Traffic Congestion And Air Pollution In The Capital">
                            One Car Per Family, AAP's New Strategy To Control Traffic Congestion And Air Pollution In The Capital                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_11">
                    <figure>
                        <div class="hash-tag">
                            16 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/sports/still-in-the-hunt-for-an-olympic-gold-mary-kom-assures-she-is-not-retiring-before-rio-2016_-245684.html" title="Still In The Hunt For An Olympic Gold, Mary Kom Assures She Is Not Retiring Before Rio 2016" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/marykompractice_1443511675_236x111.jpg" border="0" alt="Still In The Hunt For An Olympic Gold, Mary Kom Assures She Is Not Retiring Before Rio 2016"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/still-in-the-hunt-for-an-olympic-gold-mary-kom-assures-she-is-not-retiring-before-rio-2016_-245684.html" title="Still In The Hunt For An Olympic Gold, Mary Kom Assures She Is Not Retiring Before Rio 2016">
                            Still In The Hunt For An Olympic Gold, Mary Kom Assures She Is Not Retiring Before Rio 2016                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_12">
                    <figure>
                        <div class="hash-tag">
                            17 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/a-year-after-cat-introduces-3rd-gender-category-80-transgenders-prepare-to-enter-bschools-245686.html" title="A Year After CAT Introduces 3rd Gender Category, 80 Transgenders Prepare To Enter B-Schools" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage5_1443512671_1443512679_236x111.jpg" border="0" alt="A Year After CAT Introduces 3rd Gender Category, 80 Transgenders Prepare To Enter B-Schools"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/a-year-after-cat-introduces-3rd-gender-category-80-transgenders-prepare-to-enter-bschools-245686.html" title="A Year After CAT Introduces 3rd Gender Category, 80 Transgenders Prepare To Enter B-Schools">
                            A Year After CAT Introduces 3rd Gender Category, 80 Transgenders Prepare To Enter B-Schools                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_13">
                    <figure>
                        <div class="hash-tag">
                            17 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/up-has-only-3656-traffic-cops-against-a-population-of-20-crore-reason-enough-to-not-follow-traffic-rules-245683.html" title="UP Has Only 3,656 Traffic Cops Against A Population Of 20 Crore, Reason Enough To Not Follow Traffic Rules?" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1443512202_236x111.jpg" border="0" alt="UP Has Only 3,656 Traffic Cops Against A Population Of 20 Crore, Reason Enough To Not Follow Traffic Rules?"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/up-has-only-3656-traffic-cops-against-a-population-of-20-crore-reason-enough-to-not-follow-traffic-rules-245683.html" title="UP Has Only 3,656 Traffic Cops Against A Population Of 20 Crore, Reason Enough To Not Follow Traffic Rules?">
                            UP Has Only 3,656 Traffic Cops Against A Population Of 20 Crore, Reason Enough To Not Follow Traffic Rules?                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                     
                <div class="life-panel-list cf">
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/priyanka-chopra-was-just-on-jimmy-kimmel-live-and-ended-up-revealing-some-really-disturbing-sh*t-245709.html'>video</a>                          
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/videocafe/priyanka-chopra-was-just-on-jimmy-kimmel-live-and-ended-up-revealing-some-really-disturbing-sh*t-245709.html" class="tint" title="Priyanka Chopra Was Just On 'Jimmy Kimmel Live!' And Ended Up Revealing Some Really Disturbing Sh*t">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/pri_dennis_card_1443526947_502x234.jpg" border="0" alt="http://www.indiatimes.com/videocafe/priyanka-chopra-was-just-on-jimmy-kimmel-live-and-ended-up-revealing-some-really-disturbing-sh*t-245709.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/priyanka-chopra-was-just-on-jimmy-kimmel-live-and-ended-up-revealing-some-really-disturbing-sh*t-245709.html" title="Priyanka Chopra Was Just On 'Jimmy Kimmel Live!' And Ended Up Revealing Some Really Disturbing Sh*t">
                            Priyanka Chopra Was Just On 'Jimmy Kimmel Live!' And Ended Up Revealing Some Really Disturbing Sh*t                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/this-video-about-the-4-heart-breakers-is-exactly-what-you-need-to-watch-on-world-heart-day-245700.html'>video</a>                          
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/healthyliving/this-video-about-the-4-heart-breakers-is-exactly-what-you-need-to-watch-on-world-heart-day-245700.html" class="tint" title="This Video About The 4 Heart Breakers Is Exactly What You Need To Watch On World Heart Day">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/2_1443523749_502x234.jpg" border="0" alt="http://www.indiatimes.com/health/healthyliving/this-video-about-the-4-heart-breakers-is-exactly-what-you-need-to-watch-on-world-heart-day-245700.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/this-video-about-the-4-heart-breakers-is-exactly-what-you-need-to-watch-on-world-heart-day-245700.html" title="This Video About The 4 Heart Breakers Is Exactly What You Need To Watch On World Heart Day">
                            This Video About The 4 Heart Breakers Is Exactly What You Need To Watch On World Heart Day                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/malala-yousafzai-trolls-american-tv-host-stephen-colbert-with-her-amazing-card-trick-genius-245692.html'>video</a>                          
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/videocafe/malala-yousafzai-trolls-american-tv-host-stephen-colbert-with-her-amazing-card-trick-genius-245692.html" class="tint" title="Malala Yousafzai Trolls American TV Host Stephen Colbert With Her Amazing Card Trick! #Genius">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/malala_card_1443516817_502x234.jpg" border="0" alt="http://www.indiatimes.com/videocafe/malala-yousafzai-trolls-american-tv-host-stephen-colbert-with-her-amazing-card-trick-genius-245692.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/malala-yousafzai-trolls-american-tv-host-stephen-colbert-with-her-amazing-card-trick-genius-245692.html" title="Malala Yousafzai Trolls American TV Host Stephen Colbert With Her Amazing Card Trick! #Genius">
                            Malala Yousafzai Trolls American TV Host Stephen Colbert With Her Amazing Card Trick! #Genius                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_10"><!--trending-panel-list start-->
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/priyanka-chopra-was-just-on-jimmy-kimmel-live-and-ended-up-revealing-some-really-disturbing-sh*t-245709.html'>video</a>		

                        <a href="http://www.indiatimes.com/videocafe/priyanka-chopra-was-just-on-jimmy-kimmel-live-and-ended-up-revealing-some-really-disturbing-sh*t-245709.html" class="tint" title="Priyanka Chopra Was Just On 'Jimmy Kimmel Live!' And Ended Up Revealing Some Really Disturbing Sh*t">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/pri_dennis_card_1443526947_218x102.jpg" border="0" alt="Priyanka Chopra Was Just On 'Jimmy Kimmel Live!' And Ended Up Revealing Some Really Disturbing Sh*t"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/priyanka-chopra-was-just-on-jimmy-kimmel-live-and-ended-up-revealing-some-really-disturbing-sh*t-245709.html" title="Priyanka Chopra Was Just On 'Jimmy Kimmel Live!' And Ended Up Revealing Some Really Disturbing Sh*t">
                            Priyanka Chopra Was Just On 'Jimmy Kimmel Live!' And Ended Up Revealing Some Really Disturbing Sh*t                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_11"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/culture/who-we-are/7-occasions-in-life-when-weâre-more-than-happy-to-say-âthe-sweets-are-on-usâ-245675.html" class="tint" title="7 Occasions In Life When Weâre More Than Happy To Say âThe Sweets Are On Usâ">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1443528573_1443528576_218x102.jpg" border="0" alt="7 Occasions In Life When Weâre More Than Happy To Say âThe Sweets Are On Usâ"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/7-occasions-in-life-when-weâre-more-than-happy-to-say-âthe-sweets-are-on-usâ-245675.html" title="7 Occasions In Life When Weâre More Than Happy To Say âThe Sweets Are On Usâ">
                            7 Occasions In Life When Weâre More Than Happy To Say âThe Sweets Are On Usâ                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_12"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/hollywood/all-fast-furious-fans-rejoice-vin-diesel-announces-a-trilogy-to-end-this-epic-saga-245671.html" class="tint" title="All 'Fast & Furious' Fans Rejoice! Vin Diesel Announces A Trilogy To End This Epic Saga!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/ffcard_1443505608_1443505617_218x102.jpg" border="0" alt="All 'Fast & Furious' Fans Rejoice! Vin Diesel Announces A Trilogy To End This Epic Saga!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/hollywood/all-fast-furious-fans-rejoice-vin-diesel-announces-a-trilogy-to-end-this-epic-saga-245671.html" title="All 'Fast & Furious' Fans Rejoice! Vin Diesel Announces A Trilogy To End This Epic Saga!">
                            All 'Fast & Furious' Fans Rejoice! Vin Diesel Announces A Trilogy To End This Epic Saga!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_13"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/culture/who-we-are/12-facts-about-bhagat-singh-that-you-still-didnt-know-231217.html" class="tint" title="12 Facts About Bhagat Singh That You Still Didn't Know">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Mar/bhagat-singh-original-photo_502_1427262412_218x102.jpg" border="0" alt="12 Facts About Bhagat Singh That You Still Didn't Know"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/12-facts-about-bhagat-singh-that-you-still-didnt-know-231217.html" title="12 Facts About Bhagat Singh That You Still Didn't Know">
                            12 Facts About Bhagat Singh That You Still Didn't Know                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_14"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/health/healthyliving/7-reasosn-you-should-include-wheat-chapattis-in-your-daily-everyday-245553.html" class="tint" title="7 Reasons You Should Include Wheat Chapattis In Your Daily Diet">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/chapati-card_1443090177_218x102.jpg" border="0" alt="7 Reasons You Should Include Wheat Chapattis In Your Daily Diet"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/7-reasosn-you-should-include-wheat-chapattis-in-your-daily-everyday-245553.html" title="7 Reasons You Should Include Wheat Chapattis In Your Daily Diet">
                            7 Reasons You Should Include Wheat Chapattis In Your Daily Diet                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div><!--trending-panel end-->

    </section><!--container end-->

<!------------------------------- end Block 3 ---------------------------------------->
<section class="big-ads">
<div id="bigAd3_slot"></div>
</section>

<!------------------------------- start Block4  -------------------------------------->

    <section class="container cf" id="container4"><!--container start-->
        <div class="news-panel cf ">
            <h2>news</h2>

                            <div class="news-panel-list cf" id="column1_14">
                    <figure>
                        <div class="hash-tag">
                            18 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/foamy-lake-on-fire-creates-havoc-in-bengaluru-citizens-come-forward-in-protest-245676.html" title="Foamy 'Lake On Fire' Creates Havoc In Bengaluru, Citizens Come Forward In Protest" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/502_1443508180_236x111.jpg" border="0" alt="Foamy 'Lake On Fire' Creates Havoc In Bengaluru, Citizens Come Forward In Protest"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/foamy-lake-on-fire-creates-havoc-in-bengaluru-citizens-come-forward-in-protest-245676.html" title="Foamy 'Lake On Fire' Creates Havoc In Bengaluru, Citizens Come Forward In Protest">
                            Foamy 'Lake On Fire' Creates Havoc In Bengaluru, Citizens Come Forward In Protest                        </a>
                    </figcaption> 
                </div>
                <div class='container1'>                <div class="news-panel-list cf" id="column1_15">
                    <figure>
                        <div class="hash-tag">
                            18 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/indian-origin-doctor-fashions-makeshift-inhaler-onboard-flight-saves-asthmatic-toddlers-life-245672.html" title="Indian Origin Doctor Fashions Makeshift Inhaler Onboard Flight, Saves Asthmatic Toddler's Life" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/dr-khurshid-502_1443507033_236x111.jpg" border="0" alt="Indian Origin Doctor Fashions Makeshift Inhaler Onboard Flight, Saves Asthmatic Toddler's Life"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/indian-origin-doctor-fashions-makeshift-inhaler-onboard-flight-saves-asthmatic-toddlers-life-245672.html" title="Indian Origin Doctor Fashions Makeshift Inhaler Onboard Flight, Saves Asthmatic Toddler's Life">
                            Indian Origin Doctor Fashions Makeshift Inhaler Onboard Flight, Saves Asthmatic Toddler's Life                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_16">
                    <figure>
                        <div class="hash-tag">
                            19 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/meet-lujendra-ojha-the-nepalese-rockstar-astrogeek-who-helped-nasa-find-water-on-mars-245673.html" title="Meet Lujendra Ojha - The Nepalese Rockstar Astrogeek Who Helped NASA Find Water On Mars" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/astro-music-502_1443511640_236x111.jpg" border="0" alt="Meet Lujendra Ojha - The Nepalese Rockstar Astrogeek Who Helped NASA Find Water On Mars"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/meet-lujendra-ojha-the-nepalese-rockstar-astrogeek-who-helped-nasa-find-water-on-mars-245673.html" title="Meet Lujendra Ojha - The Nepalese Rockstar Astrogeek Who Helped NASA Find Water On Mars">
                            Meet Lujendra Ojha - The Nepalese Rockstar Astrogeek Who Helped NASA Find Water On Mars                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_17">
                    <figure>
                        <div class="hash-tag">
                            20 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/not-tracking-your-dp-says-facebook-blames-confusion-on-engineer-error-245669.html" title="Not Tracking Your DP, Facebook Says Confusion Happened Due To An Engineer Error" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/modi502_1443504174_236x111.jpg" border="0" alt="Not Tracking Your DP, Facebook Says Confusion Happened Due To An Engineer Error"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/not-tracking-your-dp-says-facebook-blames-confusion-on-engineer-error-245669.html" title="Not Tracking Your DP, Facebook Says Confusion Happened Due To An Engineer Error">
                            Not Tracking Your DP, Facebook Says Confusion Happened Due To An Engineer Error                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_18">
                    <figure>
                        <div class="hash-tag">
                            20 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/mumbai-women-cops-brutal-assault-on-a-young-girl-outside-lalbaugcha-raja-caught-on-video-245665.html" title="Mumbai Women Cops' Brutal Assault On A Young Girl Outside Lalbaugcha Raja Caught On Video" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/lal-502_1443444783_236x111.jpg" border="0" alt="Mumbai Women Cops' Brutal Assault On A Young Girl Outside Lalbaugcha Raja Caught On Video"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/mumbai-women-cops-brutal-assault-on-a-young-girl-outside-lalbaugcha-raja-caught-on-video-245665.html" title="Mumbai Women Cops' Brutal Assault On A Young Girl Outside Lalbaugcha Raja Caught On Video">
                            Mumbai Women Cops' Brutal Assault On A Young Girl Outside Lalbaugcha Raja Caught On Video                        </a>
                    </figcaption> 
                </div>
                 
        </div>
    </div><!--news-panel end-->

    <div class="life-panel cf container2" id="b4c2"><!--life-panel start-->
        <h2>lifestyle</h2>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/srk-to-make-an-appearance-on-bigg-boss-+-3-things-you-need-to-know-about-bigg-boss-9-right-now-245680.html" class="tint" title="SRK To Make An Appearance On Bigg Boss? + 3 Things You Need To Know About Bigg Boss 9 Right Now!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/18-srk-salman_1443510979_1443510985_502x234.jpg" border="0" alt="SRK To Make An Appearance On Bigg Boss? + 3 Things You Need To Know About Bigg Boss 9 Right Now!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/srk-to-make-an-appearance-on-bigg-boss-+-3-things-you-need-to-know-about-bigg-boss-9-right-now-245680.html" title="SRK To Make An Appearance On Bigg Boss? + 3 Things You Need To Know About Bigg Boss 9 Right Now!">
                        SRK To Make An Appearance On Bigg Boss? + 3 Things You Need To Know About Bigg Boss 9 Right Now!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/10-bizzare-coffeemaking-techniques-from-all-over-the-world-245624.html" class="tint" title="10 Bizzare Coffee-Making Techniques From All Over The World">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/coffee-and-cheese2_1443419643_1443419654_502x234.jpg" border="0" alt="10 Bizzare Coffee-Making Techniques From All Over The World" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/10-bizzare-coffeemaking-techniques-from-all-over-the-world-245624.html" title="10 Bizzare Coffee-Making Techniques From All Over The World">
                        10 Bizzare Coffee-Making Techniques From All Over The World                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/when-salman-gave-a-befitting-reply-to-a-jazbaati-question-about-exflame-aishwarya-rai-245682.html" class="tint" title="When Salman Gave A Befitting Reply To A 'Jazbaati' Question About Ex-Flame Aishwarya Rai!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card-salman_1443512466_1443512479_502x234.jpg" border="0" alt="When Salman Gave A Befitting Reply To A 'Jazbaati' Question About Ex-Flame Aishwarya Rai!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/when-salman-gave-a-befitting-reply-to-a-jazbaati-question-about-exflame-aishwarya-rai-245682.html" title="When Salman Gave A Befitting Reply To A 'Jazbaati' Question About Ex-Flame Aishwarya Rai!">
                        When Salman Gave A Befitting Reply To A 'Jazbaati' Question About Ex-Flame Aishwarya Rai!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/7-health-conditions-that-your-feet-can-warn-you-about-245647.html" class="tint" title="7 Health Conditions That Your Feet Can Warn You About!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1443429737_502x234.jpg" border="0" alt="7 Health Conditions That Your Feet Can Warn You About!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/7-health-conditions-that-your-feet-can-warn-you-about-245647.html" title="7 Health Conditions That Your Feet Can Warn You About!">
                        7 Health Conditions That Your Feet Can Warn You About!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/embarrassing-video-surfaces-showing-satya-nadella-wiping-his-hands-after-handshake-with-pm-modi-245681.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/embarrassing-video-surfaces-showing-satya-nadella-wiping-his-hands-after-handshake-with-pm-modi-245681.html" class="tint" title="Embarrassing Video Surfaces Showing Satya Nadella Wiping His Hands After Handshake With PM Modi">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/modi_card_1443510874_502x234.jpg" border="0" alt="Embarrassing Video Surfaces Showing Satya Nadella Wiping His Hands After Handshake With PM Modi" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/embarrassing-video-surfaces-showing-satya-nadella-wiping-his-hands-after-handshake-with-pm-modi-245681.html" title="Embarrassing Video Surfaces Showing Satya Nadella Wiping His Hands After Handshake With PM Modi">
                        Embarrassing Video Surfaces Showing Satya Nadella Wiping His Hands After Handshake With PM Modi                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-classical-indian-version-of-harry-potter-soundtrack-will-make-you-happy-in-an-instant-245677.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-classical-indian-version-of-harry-potter-soundtrack-will-make-you-happy-in-an-instant-245677.html" class="tint" title="This Classical Indian Version Of Harry Potter Soundtrack Will Make You Happy In An Instant">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/harrypotter_card_1443513466_502x234.jpg" border="0" alt="This Classical Indian Version Of Harry Potter Soundtrack Will Make You Happy In An Instant" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-classical-indian-version-of-harry-potter-soundtrack-will-make-you-happy-in-an-instant-245677.html" title="This Classical Indian Version Of Harry Potter Soundtrack Will Make You Happy In An Instant">
                        This Classical Indian Version Of Harry Potter Soundtrack Will Make You Happy In An Instant                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/hollywood/paul-walkers-daughter-sues-porsche-over-the-accident-that-killed-her-dad-245679.html" class="tint" title="Paul Walker's Daughter Sues Porsche Over The Accident That Killed Her Dad">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage-5_1443510382_1443510389_502x234.jpg" border="0" alt="Paul Walker's Daughter Sues Porsche Over The Accident That Killed Her Dad" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/hollywood/paul-walkers-daughter-sues-porsche-over-the-accident-that-killed-her-dad-245679.html" title="Paul Walker's Daughter Sues Porsche Over The Accident That Killed Her Dad">
                        Paul Walker's Daughter Sues Porsche Over The Accident That Killed Her Dad                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/hollywood/all-fast-furious-fans-rejoice-vin-diesel-announces-a-trilogy-to-end-this-epic-saga-245671.html" class="tint" title="All 'Fast & Furious' Fans Rejoice! Vin Diesel Announces A Trilogy To End This Epic Saga!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/ffcard_1443505608_1443505617_502x234.jpg" border="0" alt="All 'Fast & Furious' Fans Rejoice! Vin Diesel Announces A Trilogy To End This Epic Saga!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/hollywood/all-fast-furious-fans-rejoice-vin-diesel-announces-a-trilogy-to-end-this-epic-saga-245671.html" title="All 'Fast & Furious' Fans Rejoice! Vin Diesel Announces A Trilogy To End This Epic Saga!">
                        All 'Fast & Furious' Fans Rejoice! Vin Diesel Announces A Trilogy To End This Epic Saga!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/5-staircase-exercises-that-are-your-stairway-to-fitness-and-weight-loss-245648.html" class="tint" title="5 Staircase Exercises That Are Your Stairway To Fitness And Weight Loss">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card-1_1443433289_502x234.jpg" border="0" alt="5 Staircase Exercises That Are Your Stairway To Fitness And Weight Loss" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/5-staircase-exercises-that-are-your-stairway-to-fitness-and-weight-loss-245648.html" title="5 Staircase Exercises That Are Your Stairway To Fitness And Weight Loss">
                        5 Staircase Exercises That Are Your Stairway To Fitness And Weight Loss                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/11-lesser-known-facts-about-mehmood-that-prove-he-was-so-much-more-than-just-a-comedian-245554.html" class="tint" title="11 Lesser Known Facts About Mehmood That Prove He Was So Much More Than Just A Comedian">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/18dfr_mehmood_839031g_1443090146_1443090152_502x234.jpg" border="0" alt="11 Lesser Known Facts About Mehmood That Prove He Was So Much More Than Just A Comedian" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/11-lesser-known-facts-about-mehmood-that-prove-he-was-so-much-more-than-just-a-comedian-245554.html" title="11 Lesser Known Facts About Mehmood That Prove He Was So Much More Than Just A Comedian">
                        11 Lesser Known Facts About Mehmood That Prove He Was So Much More Than Just A Comedian                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/nasa-finds-liquid-water-on-mars-could-life-also-exist-on-the-red-planet-245668.html" class="tint" title="NASA Finds Liquid Water On Mars. Could Life Also Exist On The Red Planet?">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/mars---card2_1443459621_502x234.jpg" border="0" alt="NASA Finds Liquid Water On Mars. Could Life Also Exist On The Red Planet?" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/nasa-finds-liquid-water-on-mars-could-life-also-exist-on-the-red-planet-245668.html" title="NASA Finds Liquid Water On Mars. Could Life Also Exist On The Red Planet?">
                        NASA Finds Liquid Water On Mars. Could Life Also Exist On The Red Planet?                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/travel/7-reasons-twoweeksbeforewinter-is-officially-the-best-time-to-be-in-delhi-245588.html" class="tint" title="7 Reasons Two-Weeks-Before-Winter Is Officially The Best Time To Be In Delhi">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/cp1_1443262451_502x234.jpg" border="0" alt="7 Reasons Two-Weeks-Before-Winter Is Officially The Best Time To Be In Delhi" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/travel/7-reasons-twoweeksbeforewinter-is-officially-the-best-time-to-be-in-delhi-245588.html" title="7 Reasons Two-Weeks-Before-Winter Is Officially The Best Time To Be In Delhi">
                        7 Reasons Two-Weeks-Before-Winter Is Officially The Best Time To Be In Delhi                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/nothing-can-come-between-narendra-modi-and-a-camera-not-even-mark-zuckerberg-245663.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/nothing-can-come-between-narendra-modi-and-a-camera-not-even-mark-zuckerberg-245663.html" class="tint" title="Nothing Can Come Between Narendra Modi And A Camera. Not Even Mark Zuckerberg!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/modi_cam_card_1443442344_502x234.jpg" border="0" alt="Nothing Can Come Between Narendra Modi And A Camera. Not Even Mark Zuckerberg!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/nothing-can-come-between-narendra-modi-and-a-camera-not-even-mark-zuckerberg-245663.html" title="Nothing Can Come Between Narendra Modi And A Camera. Not Even Mark Zuckerberg!">
                        Nothing Can Come Between Narendra Modi And A Camera. Not Even Mark Zuckerberg!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/health/tips-tricks/17-beauty-hacks-in-under-7-minutes-that-will-make-your-beauty-regime-simpler-245478.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/17-beauty-hacks-in-under-7-minutes-that-will-make-your-beauty-regime-simpler-245478.html" class="tint" title="17 Beauty Hacks In Under 7 Minutes That Will Make Your Beauty Regime Simpler">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/beauty-card_1442923956_502x234.jpg" border="0" alt="17 Beauty Hacks In Under 7 Minutes That Will Make Your Beauty Regime Simpler" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/17-beauty-hacks-in-under-7-minutes-that-will-make-your-beauty-regime-simpler-245478.html" title="17 Beauty Hacks In Under 7 Minutes That Will Make Your Beauty Regime Simpler">
                        17 Beauty Hacks In Under 7 Minutes That Will Make Your Beauty Regime Simpler                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/14-realistically-beautiful-quotes-about-life-love-from-celine-jesses-before-trilogy-245660.html" class="tint" title="14 Realistically Beautiful Quotes About Life & Love From Celine & Jesse's 'Before' Trilogy!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card-04_1443443970_1443443976_502x234.jpg" border="0" alt="14 Realistically Beautiful Quotes About Life & Love From Celine & Jesse's 'Before' Trilogy!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/14-realistically-beautiful-quotes-about-life-love-from-celine-jesses-before-trilogy-245660.html" title="14 Realistically Beautiful Quotes About Life & Love From Celine & Jesse's 'Before' Trilogy!">
                        14 Realistically Beautiful Quotes About Life & Love From Celine & Jesse's 'Before' Trilogy!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/13-shows-from-the-90s-that-you-were-not-supposed-to-watch-as-a-kid-245530.html" class="tint" title="13 Shows From The 90s That You Were Not Supposed To Watch As A Kid">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/cp_1443089343_502x234.jpg" border="0" alt="13 Shows From The 90s That You Were Not Supposed To Watch As A Kid" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/13-shows-from-the-90s-that-you-were-not-supposed-to-watch-as-a-kid-245530.html" title="13 Shows From The 90s That You Were Not Supposed To Watch As A Kid">
                        13 Shows From The 90s That You Were Not Supposed To Watch As A Kid                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/these-touching-illustrations-explain-why-living-with-depression-is-not-the-end-of-the-world-245557.html" class="tint" title="These Touching Illustrations Explain Exactly What It Feels Like To Live With Depression">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/cover_1443093037_502x234.jpg" border="0" alt="These Touching Illustrations Explain Exactly What It Feels Like To Live With Depression" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/these-touching-illustrations-explain-why-living-with-depression-is-not-the-end-of-the-world-245557.html" title="These Touching Illustrations Explain Exactly What It Feels Like To Live With Depression">
                        These Touching Illustrations Explain Exactly What It Feels Like To Live With Depression                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/travel/10-pictures-that-prove-autumn-in-india-is-just-as-beautiful-as-it-is-in-colder-countries-245560.html" class="tint" title="10 Pictures That Prove Autumn In India Is Just As Beautiful As It Is In Colder Countries">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/cp_1443098781_502x234.jpg" border="0" alt="10 Pictures That Prove Autumn In India Is Just As Beautiful As It Is In Colder Countries" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/travel/10-pictures-that-prove-autumn-in-india-is-just-as-beautiful-as-it-is-in-colder-countries-245560.html" title="10 Pictures That Prove Autumn In India Is Just As Beautiful As It Is In Colder Countries">
                        10 Pictures That Prove Autumn In India Is Just As Beautiful As It Is In Colder Countries                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/7-facts-about-mathematician-srinivasa-ramanujan-the-man-who-knew-infinity-245643.html" class="tint" title="7 Facts About Mathematician Srinivasa Ramanujan - The Man Who Knew Infinity">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/dev-patel_1443425056_1443425072_502x234.jpg" border="0" alt="7 Facts About Mathematician Srinivasa Ramanujan - The Man Who Knew Infinity" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/7-facts-about-mathematician-srinivasa-ramanujan-the-man-who-knew-infinity-245643.html" title="7 Facts About Mathematician Srinivasa Ramanujan - The Man Who Knew Infinity">
                        7 Facts About Mathematician Srinivasa Ramanujan - The Man Who Knew Infinity                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/heres-a-19-step-guide-to-detox-your-body-in-just-two-weeks-245501.html" class="tint" title="Here's A 19 Step Guide To Detox Your Body In Just Two Weeks">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1442999613_502x234.jpg" border="0" alt="Here's A 19 Step Guide To Detox Your Body In Just Two Weeks" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/heres-a-19-step-guide-to-detox-your-body-in-just-two-weeks-245501.html" title="Here's A 19 Step Guide To Detox Your Body In Just Two Weeks">
                        Here's A 19 Step Guide To Detox Your Body In Just Two Weeks                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-video-explains-how-the-world-changes-in-every-four-minutes-245645.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-video-explains-how-the-world-changes-in-every-four-minutes-245645.html" class="tint" title="This Video Explains How The World Changes In Every Four Minutes!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/fourmins_card_1443426238_502x234.jpg" border="0" alt="This Video Explains How The World Changes In Every Four Minutes!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-video-explains-how-the-world-changes-in-every-four-minutes-245645.html" title="This Video Explains How The World Changes In Every Four Minutes!">
                        This Video Explains How The World Changes In Every Four Minutes!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/6-reasons-why-bollywood-doesnt-need-scifi-films-at-all-245642.html" class="tint" title="6 Reasons Why Bollywood Doesn't Need Sci-Fi Films At All!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1443426849_502x234.jpg" border="0" alt="6 Reasons Why Bollywood Doesn't Need Sci-Fi Films At All!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/6-reasons-why-bollywood-doesnt-need-scifi-films-at-all-245642.html" title="6 Reasons Why Bollywood Doesn't Need Sci-Fi Films At All!">
                        6 Reasons Why Bollywood Doesn't Need Sci-Fi Films At All!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/11-gamechanging-moneysaving-tips-thatll-help-you-save-for-that-house-in-the-hills-245561.html" class="tint" title="11 Game-Changing Money-Saving Tips That'll Help You Save For That House In The Hills">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/srk1_1443097512_1443097519_502x234.jpg" border="0" alt="11 Game-Changing Money-Saving Tips That'll Help You Save For That House In The Hills" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/11-gamechanging-moneysaving-tips-thatll-help-you-save-for-that-house-in-the-hills-245561.html" title="11 Game-Changing Money-Saving Tips That'll Help You Save For That House In The Hills">
                        11 Game-Changing Money-Saving Tips That'll Help You Save For That House In The Hills                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/srk-takes-a-jibe-at-himself-asks-boys-not-to-stalk-girls-like-he-did-in-darr-245637.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/srk-takes-a-jibe-at-himself-asks-boys-not-to-stalk-girls-like-he-did-in-darr-245637.html" class="tint" title="SRK Takes A Jibe At Himself, Asks Boys Not To Stalk Girls Like He Did In Darr!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/sa_1443421295_1443421300_502x234.jpg" border="0" alt="SRK Takes A Jibe At Himself, Asks Boys Not To Stalk Girls Like He Did In Darr!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/srk-takes-a-jibe-at-himself-asks-boys-not-to-stalk-girls-like-he-did-in-darr-245637.html" title="SRK Takes A Jibe At Himself, Asks Boys Not To Stalk Girls Like He Did In Darr!">
                        SRK Takes A Jibe At Himself, Asks Boys Not To Stalk Girls Like He Did In Darr!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/7-reasosn-you-should-include-wheat-chapattis-in-your-daily-everyday-245553.html" class="tint" title="7 Reasons You Should Include Wheat Chapattis In Your Daily Diet">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/chapati-card_1443090177_502x234.jpg" border="0" alt="7 Reasons You Should Include Wheat Chapattis In Your Daily Diet" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/7-reasosn-you-should-include-wheat-chapattis-in-your-daily-everyday-245553.html" title="7 Reasons You Should Include Wheat Chapattis In Your Daily Diet">
                        7 Reasons You Should Include Wheat Chapattis In Your Daily Diet                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/14-awesome-things-which-filmy-people-must-add-to-their-bucket-list-245627.html" class="tint" title="14 Awesome Things Which 'Filmy' People Must Add To Their Bucket List!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1443348542_1443348552_502x234.jpg" border="0" alt="14 Awesome Things Which 'Filmy' People Must Add To Their Bucket List!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/14-awesome-things-which-filmy-people-must-add-to-their-bucket-list-245627.html" title="14 Awesome Things Which 'Filmy' People Must Add To Their Bucket List!">
                        14 Awesome Things Which 'Filmy' People Must Add To Their Bucket List!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/bhopal-man-rescues-8yearold-girl-from-an-alcoholic-father-who-forced-her-in-to-begging-245632.html" class="tint" title="Bhopal Man Rescues 10-Year-Old Girl From An Alcoholic Father Who Forced Her Into Begging">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/untitled_1443354793_1443354805_1443354809_502x234.jpg" border="0" alt="Bhopal Man Rescues 10-Year-Old Girl From An Alcoholic Father Who Forced Her Into Begging" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/bhopal-man-rescues-8yearold-girl-from-an-alcoholic-father-who-forced-her-in-to-begging-245632.html" title="Bhopal Man Rescues 10-Year-Old Girl From An Alcoholic Father Who Forced Her Into Begging">
                        Bhopal Man Rescues 10-Year-Old Girl From An Alcoholic Father Who Forced Her Into Begging                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/12-signs-youre-never-going-back-on-a-second-date-with-that-person-again-245492.html" class="tint" title="12 Signs You're Never Going Back On A Second Date With That Person Again">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/cp_1443011279_1443011290_502x234.jpg" border="0" alt="12 Signs You're Never Going Back On A Second Date With That Person Again" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/12-signs-youre-never-going-back-on-a-second-date-with-that-person-again-245492.html" title="12 Signs You're Never Going Back On A Second Date With That Person Again">
                        12 Signs You're Never Going Back On A Second Date With That Person Again                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/12-facts-about-bhagat-singh-that-you-still-didnt-know-231217.html" class="tint" title="12 Facts About Bhagat Singh That You Still Didn't Know">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Mar/bhagat-singh-original-photo_502_1427262412_502x234.jpg" border="0" alt="12 Facts About Bhagat Singh That You Still Didn't Know" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/12-facts-about-bhagat-singh-that-you-still-didnt-know-231217.html" title="12 Facts About Bhagat Singh That You Still Didn't Know">
                        12 Facts About Bhagat Singh That You Still Didn't Know                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/15-reasons-sleeping-is-the-most-underestimated-activity-of-them-all-245564.html" class="tint" title="15 Reasons Sleeping Is The Most Underestimated Activity Of Them All">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/sall_1443098614_1443098631_502x234.jpg" border="0" alt="15 Reasons Sleeping Is The Most Underestimated Activity Of Them All" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/15-reasons-sleeping-is-the-most-underestimated-activity-of-them-all-245564.html" title="15 Reasons Sleeping Is The Most Underestimated Activity Of Them All">
                        15 Reasons Sleeping Is The Most Underestimated Activity Of Them All                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/5-common-myths-about-tuberculosis-that-need-to-be-busted-right-now-245473.html" class="tint" title="5 Common Myths About Tuberculosis That Need To Be Busted Right Now">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card1_1442922596_502x234.jpg" border="0" alt="5 Common Myths About Tuberculosis That Need To Be Busted Right Now" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/5-common-myths-about-tuberculosis-that-need-to-be-busted-right-now-245473.html" title="5 Common Myths About Tuberculosis That Need To Be Busted Right Now">
                        5 Common Myths About Tuberculosis That Need To Be Busted Right Now                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/whoa-this-crazy-man-sets-a-gas-station-on-fire-to-kill-a-spider-245628.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/whoa-this-crazy-man-sets-a-gas-station-on-fire-to-kill-a-spider-245628.html" class="tint" title="Whoa! This Crazy Man Sets A Gas Station On Fire To Kill A Spider.">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/gas_pump_card_1443349128_502x234.jpg" border="0" alt="Whoa! This Crazy Man Sets A Gas Station On Fire To Kill A Spider." class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/whoa-this-crazy-man-sets-a-gas-station-on-fire-to-kill-a-spider-245628.html" title="Whoa! This Crazy Man Sets A Gas Station On Fire To Kill A Spider.">
                        Whoa! This Crazy Man Sets A Gas Station On Fire To Kill A Spider.                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/6-super-seeds-that-work-wonders-on-your-body-245471.html" class="tint" title="6 Super Seeds That Work Wonders On Your Body">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card-1_1442921556_502x234.jpg" border="0" alt="6 Super Seeds That Work Wonders On Your Body" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/6-super-seeds-that-work-wonders-on-your-body-245471.html" title="6 Super Seeds That Work Wonders On Your Body">
                        6 Super Seeds That Work Wonders On Your Body                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/video-shows-the-shocking-reality-of-the-way-our-gods-are-treated-after-the-festivities-are-over-245630.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/video-shows-the-shocking-reality-of-the-way-our-gods-are-treated-after-the-festivities-are-over-245630.html" class="tint" title="Video Shows The Shocking Reality Of The Way Our Gods Are Treated After The Festivities Are Over">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/ganesh_visarjan_card_1443352831_502x234.jpg" border="0" alt="Video Shows The Shocking Reality Of The Way Our Gods Are Treated After The Festivities Are Over" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/video-shows-the-shocking-reality-of-the-way-our-gods-are-treated-after-the-festivities-are-over-245630.html" title="Video Shows The Shocking Reality Of The Way Our Gods Are Treated After The Festivities Are Over">
                        Video Shows The Shocking Reality Of The Way Our Gods Are Treated After The Festivities Are Over                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/these-indian-men-try-womens-diy-hairstyle-and-end-up-making-a-fool-of-themselves-lol-245621.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/these-indian-men-try-womens-diy-hairstyle-and-end-up-making-a-fool-of-themselves-lol-245621.html" class="tint" title="These Indian Men Try Women's DIY Hairstyle And End Up Making a Fool Of Themselves! #LOL">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/men_haristyle_card_1443340388_502x234.jpg" border="0" alt="These Indian Men Try Women's DIY Hairstyle And End Up Making a Fool Of Themselves! #LOL" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/these-indian-men-try-womens-diy-hairstyle-and-end-up-making-a-fool-of-themselves-lol-245621.html" title="These Indian Men Try Women's DIY Hairstyle And End Up Making a Fool Of Themselves! #LOL">
                        These Indian Men Try Women's DIY Hairstyle And End Up Making a Fool Of Themselves! #LOL                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/11-misconceptions-we-had-about-the-world-when-we-were-kids-245574.html" class="tint" title="11 Misconceptions We Had About The World When We Were Kids">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/cp_1443167867_502x234.jpg" border="0" alt="11 Misconceptions We Had About The World When We Were Kids" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/11-misconceptions-we-had-about-the-world-when-we-were-kids-245574.html" title="11 Misconceptions We Had About The World When We Were Kids">
                        11 Misconceptions We Had About The World When We Were Kids                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/9-funniest-bloopers-in-bollywood-movies-you-probably-missed-245623.html" class="tint" title="9 Funniest Bloopers In Bollywood Movies You Probably Missed">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage_1443341648_1443341664_502x234.jpg" border="0" alt="9 Funniest Bloopers In Bollywood Movies You Probably Missed" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/9-funniest-bloopers-in-bollywood-movies-you-probably-missed-245623.html" title="9 Funniest Bloopers In Bollywood Movies You Probably Missed">
                        9 Funniest Bloopers In Bollywood Movies You Probably Missed                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/hollywood/deleted-scene-from-avengers-age-of-ultron-+-9-neverseenbefore-clips-from-superhero-movies-245619.html" class="tint" title="Deleted Scene From 'Avengers: Age of Ultron' + 9 Never Seen Before Clips From Superhero Movies">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/superheros_card_1443335876_502x234.jpg" border="0" alt="Deleted Scene From 'Avengers: Age of Ultron' + 9 Never Seen Before Clips From Superhero Movies" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/hollywood/deleted-scene-from-avengers-age-of-ultron-+-9-neverseenbefore-clips-from-superhero-movies-245619.html" title="Deleted Scene From 'Avengers: Age of Ultron' + 9 Never Seen Before Clips From Superhero Movies">
                        Deleted Scene From 'Avengers: Age of Ultron' + 9 Never Seen Before Clips From Superhero Movies                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/this-quick-test-can-tell-you-whether-your-weight-is-healthy-or-not-245427.html" class="tint" title="This Quick Test Can Tell You Whether Your Weight Is Healthy Or Not!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/weight_1442831165_502x234.jpg" border="0" alt="This Quick Test Can Tell You Whether Your Weight Is Healthy Or Not!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/this-quick-test-can-tell-you-whether-your-weight-is-healthy-or-not-245427.html" title="This Quick Test Can Tell You Whether Your Weight Is Healthy Or Not!">
                        This Quick Test Can Tell You Whether Your Weight Is Healthy Or Not!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/lengthy-films-are-back-here-are-12-superlong-bollywood-films-worth-watching-243531.html" class="tint" title="Lengthy Films Are Back! Here Are 12 Super-Long Bollywood Films Worth Watching!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/1111_1438075868_1438075874_502x234.jpg" border="0" alt="Lengthy Films Are Back! Here Are 12 Super-Long Bollywood Films Worth Watching!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/lengthy-films-are-back-here-are-12-superlong-bollywood-films-worth-watching-243531.html" title="Lengthy Films Are Back! Here Are 12 Super-Long Bollywood Films Worth Watching!">
                        Lengthy Films Are Back! Here Are 12 Super-Long Bollywood Films Worth Watching!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/lol-after-blocking-srk-salman-hrithik-on-twitter-krk-now-demands-rs-25k-to-unblock-245618.html" class="tint" title="LOL! After Blocking SRK, Salman & Hrithik On Twitter, KRK Now Demands Rs 25K To Unblock!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/krk-story-card_1443335105_1443335118_502x234.jpg" border="0" alt="LOL! After Blocking SRK, Salman & Hrithik On Twitter, KRK Now Demands Rs 25K To Unblock!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/lol-after-blocking-srk-salman-hrithik-on-twitter-krk-now-demands-rs-25k-to-unblock-245618.html" title="LOL! After Blocking SRK, Salman & Hrithik On Twitter, KRK Now Demands Rs 25K To Unblock!">
                        LOL! After Blocking SRK, Salman & Hrithik On Twitter, KRK Now Demands Rs 25K To Unblock!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/artists-create-worldâs-longest-gif-file-that-takes-a-1000-years-to-loop-245616.html" class="tint" title="Artists Create Worldâs Longest GIF File That Takes A 1000 Years To Loop!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/gif---card_1443336732_502x234.jpg" border="0" alt="Artists Create Worldâs Longest GIF File That Takes A 1000 Years To Loop!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/artists-create-worldâs-longest-gif-file-that-takes-a-1000-years-to-loop-245616.html" title="Artists Create Worldâs Longest GIF File That Takes A 1000 Years To Loop!">
                        Artists Create Worldâs Longest GIF File That Takes A 1000 Years To Loop!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/after-helping-113-drought-hit-families-nana-patekar-opens-up-on-how-charity-made-him-more-human-245614.html" class="tint" title="After Helping 113 Drought Hit Families, Nana Patekar Opens Up On How Charity Made Him More Human">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/n-nana-patekar-large570_1443331280_1443331284_502x234.jpg" border="0" alt="After Helping 113 Drought Hit Families, Nana Patekar Opens Up On How Charity Made Him More Human" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/after-helping-113-drought-hit-families-nana-patekar-opens-up-on-how-charity-made-him-more-human-245614.html" title="After Helping 113 Drought Hit Families, Nana Patekar Opens Up On How Charity Made Him More Human">
                        After Helping 113 Drought Hit Families, Nana Patekar Opens Up On How Charity Made Him More Human                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/travel/5-indian-luxury-trains-that-will-make-sure-you-start-saving-for-a-ride-on-one-of-them-245482.html" class="tint" title="5 Indian Luxury Trains That Will Make Sure You Start Saving For A Ride On One Of Them">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1442988704_1442988712_502x234.jpg" border="0" alt="5 Indian Luxury Trains That Will Make Sure You Start Saving For A Ride On One Of Them" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/travel/5-indian-luxury-trains-that-will-make-sure-you-start-saving-for-a-ride-on-one-of-them-245482.html" title="5 Indian Luxury Trains That Will Make Sure You Start Saving For A Ride On One Of Them">
                        5 Indian Luxury Trains That Will Make Sure You Start Saving For A Ride On One Of Them                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/humans-of-new-york-to-share-stories-of-syrian-refugees-in-search-of-new-beginnings-245615.html" class="tint" title="These Heart Breaking Stories Of Syrian Refugees In Search Of A New Life Will Bring Tears To Your Eyes">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage1_1443331445_1443331450_502x234.jpg" border="0" alt="These Heart Breaking Stories Of Syrian Refugees In Search Of A New Life Will Bring Tears To Your Eyes" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/humans-of-new-york-to-share-stories-of-syrian-refugees-in-search-of-new-beginnings-245615.html" title="These Heart Breaking Stories Of Syrian Refugees In Search Of A New Life Will Bring Tears To Your Eyes">
                        These Heart Breaking Stories Of Syrian Refugees In Search Of A New Life Will Bring Tears To Your Eyes                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-video-is-a-wake-up-call-for-everyone-who-is-spreading-pollution-in-the-name-of-god-245346.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-video-is-a-wake-up-call-for-everyone-who-is-spreading-pollution-in-the-name-of-god-245346.html" class="tint" title="This Video Is A Wake Up Call For Everyone Who Is Spreading Pollution In The Name Of God!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/pollution_card_1442574823_502x234.jpg" border="0" alt="This Video Is A Wake Up Call For Everyone Who Is Spreading Pollution In The Name Of God!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-video-is-a-wake-up-call-for-everyone-who-is-spreading-pollution-in-the-name-of-god-245346.html" title="This Video Is A Wake Up Call For Everyone Who Is Spreading Pollution In The Name Of God!">
                        This Video Is A Wake Up Call For Everyone Who Is Spreading Pollution In The Name Of God!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/7-times-medical-students-took-their-course-material-too-seriously-245496.html" class="tint" title="7 Times Medical Students Took Their Course Material Too Seriously">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card-3_1442994025_502x234.jpg" border="0" alt="7 Times Medical Students Took Their Course Material Too Seriously" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/7-times-medical-students-took-their-course-material-too-seriously-245496.html" title="7 Times Medical Students Took Their Course Material Too Seriously">
                        7 Times Medical Students Took Their Course Material Too Seriously                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/12-reasons-loneliness-is-the-best-teacher-ever-245508.html" class="tint" title="12 Reasons Loneliness Is The Best Teacher Ever">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/rkcard1_1443003098_1443003104_502x234.jpg" border="0" alt="12 Reasons Loneliness Is The Best Teacher Ever" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/12-reasons-loneliness-is-the-best-teacher-ever-245508.html" title="12 Reasons Loneliness Is The Best Teacher Ever">
                        12 Reasons Loneliness Is The Best Teacher Ever                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/9-times-actors-took-their-evil-roles-way-too-seriously-and-goofed-up-their-costumes-245610.html" class="tint" title="9 Times Actors Took Their Evil Roles Way Too Seriously And Goofed Up Their Costumes">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/crd_1443273104_502x234.jpg" border="0" alt="9 Times Actors Took Their Evil Roles Way Too Seriously And Goofed Up Their Costumes" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/9-times-actors-took-their-evil-roles-way-too-seriously-and-goofed-up-their-costumes-245610.html" title="9 Times Actors Took Their Evil Roles Way Too Seriously And Goofed Up Their Costumes">
                        9 Times Actors Took Their Evil Roles Way Too Seriously And Goofed Up Their Costumes                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/hollywood/you-cant-miss-priyanka-chopra-teaching-america-how-to-shake-a-leg-bollywood-style-245600.html" class="tint" title="You Can't Miss Priyanka Chopra Teaching America How To Shake A Leg - Bollywood Style!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1443258459_502x234.jpg" border="0" alt="You Can't Miss Priyanka Chopra Teaching America How To Shake A Leg - Bollywood Style!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/hollywood/you-cant-miss-priyanka-chopra-teaching-america-how-to-shake-a-leg-bollywood-style-245600.html" title="You Can't Miss Priyanka Chopra Teaching America How To Shake A Leg - Bollywood Style!">
                        You Can't Miss Priyanka Chopra Teaching America How To Shake A Leg - Bollywood Style!                    </a>
                </figcaption>
            </div>
        
        <script id="HpMiddleBlock_tpl" type="text/x-handlebars-template">
            {{#if MoreData}}
            {{#each MoreData}} 
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>


                    <a  class='video-btn sprite' href="{{guid}}" style="display:{{card_id}}"> </a>

    				{{#if label_name.length}}
                    <a href="{{guid}}" class="{{label_name}} sticker">&nbsp;</a>
    						 {{/if}} 
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="{{guid}}" title="{{carousal_headline}}" class="tint">
                        <img class="greyBg lazy" data-original="{{thumbnail}}" border="0" alt="{{carousal_headline}}" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="{{guid}}" title="{{carousal_headline}}">
                        {{carousal_headline}}
                    </a>
                </figcaption>
            </div>  
            {{/each}} 
            {{/if}}

            </script>

        </div><!--life-panel end-->

        <div class="trending-panel cf "><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_15"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/culture/travel/7-reasons-twoweeksbeforewinter-is-officially-the-best-time-to-be-in-delhi-245588.html" class="tint" title="7 Reasons Two-Weeks-Before-Winter Is Officially The Best Time To Be In Delhi">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/cp1_1443262451_218x102.jpg" border="0" alt="7 Reasons Two-Weeks-Before-Winter Is Officially The Best Time To Be In Delhi"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/travel/7-reasons-twoweeksbeforewinter-is-officially-the-best-time-to-be-in-delhi-245588.html" title="7 Reasons Two-Weeks-Before-Winter Is Officially The Best Time To Be In Delhi">
                            7 Reasons Two-Weeks-Before-Winter Is Officially The Best Time To Be In Delhi                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                <div class='container3'>                <div class="trending-panel-list cf" id="column3_16"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/culture/travel/10-pictures-that-prove-autumn-in-india-is-just-as-beautiful-as-it-is-in-colder-countries-245560.html" class="tint" title="10 Pictures That Prove Autumn In India Is Just As Beautiful As It Is In Colder Countries">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/cp_1443098781_218x102.jpg" border="0" alt="10 Pictures That Prove Autumn In India Is Just As Beautiful As It Is In Colder Countries"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/travel/10-pictures-that-prove-autumn-in-india-is-just-as-beautiful-as-it-is-in-colder-countries-245560.html" title="10 Pictures That Prove Autumn In India Is Just As Beautiful As It Is In Colder Countries">
                            10 Pictures That Prove Autumn In India Is Just As Beautiful As It Is In Colder Countries                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_17"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/health/healthyliving/heres-a-19-step-guide-to-detox-your-body-in-just-two-weeks-245501.html" class="tint" title="Here's A 19 Step Guide To Detox Your Body In Just Two Weeks">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/card_1442999613_218x102.jpg" border="0" alt="Here's A 19 Step Guide To Detox Your Body In Just Two Weeks"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/heres-a-19-step-guide-to-detox-your-body-in-just-two-weeks-245501.html" title="Here's A 19 Step Guide To Detox Your Body In Just Two Weeks">
                            Here's A 19 Step Guide To Detox Your Body In Just Two Weeks                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_18"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/bollywood/11-lesser-known-facts-about-mehmood-that-prove-he-was-so-much-more-than-just-a-comedian-245554.html" class="tint" title="11 Lesser Known Facts About Mehmood That Prove He Was So Much More Than Just A Comedian">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Sep/18dfr_mehmood_839031g_1443090146_1443090152_218x102.jpg" border="0" alt="11 Lesser Known Facts About Mehmood That Prove He Was So Much More Than Just A Comedian"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/11-lesser-known-facts-about-mehmood-that-prove-he-was-so-much-more-than-just-a-comedian-245554.html" title="11 Lesser Known Facts About Mehmood That Prove He Was So Much More Than Just A Comedian">
                            11 Lesser Known Facts About Mehmood That Prove He Was So Much More Than Just A Comedian                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_19"><!--trending-panel-list start-->
                                        <figure>
                                
                        <a class='video-btn sprite' href='http://www.indiatimes.com/health/tips-tricks/17-beauty-hacks-in-under-7-minutes-that-will-make-your-beauty-regime-simpler-245478.html'>video</a>					
                        <a href="http://www.indiatimes.com/health/tips-tricks/17-beauty-hacks-in-under-7-minutes-that-will-make-your-beauty-regime-simpler-245478.html" class="tint" title="17 Beauty Hacks In Under 7 Minutes That Will Make Your Beauty Regime Simpler">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Sep/beauty-card_1442923956_218x102.jpg" border="0" alt="17 Beauty Hacks In Under 7 Minutes That Will Make Your Beauty Regime Simpler"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/tips-tricks/17-beauty-hacks-in-under-7-minutes-that-will-make-your-beauty-regime-simpler-245478.html" title="17 Beauty Hacks In Under 7 Minutes That Will Make Your Beauty Regime Simpler">
                            17 Beauty Hacks In Under 7 Minutes That Will Make Your Beauty Regime Simpler                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                
        </div>

    </div><!--trending-panel end-->

    </section>
    <section class="container cf" id="loader" style="display: none;border: 0px solid red;">
    <div class="news-panel cf ">&nbsp;</div>
    <div class="life-panel cf" style="text-align: center;"><img src="http://media.indiatimes.in/resources/images/loading.gif" border="0" /></div>
    <div class="trending-panel cf ">&nbsp;</div>
</section>
<section class="big-ads remove-fixed-home" id="adfooter">
<div id="badRos_slot"></div> 
</section>   

<!------------------------ end Block 4------------------------------------------------>
<!--container end-->

<script type="text/javascript">
   
    $('#bigAd1_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    showBigAD2('bigAd2_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible     
       
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#bigAd2_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    showBigAD3('bigAd3_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible

    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible  
      
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#bigAd3_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    BADros('badRos_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});

$(document).ready(function() {
	/* spotlight onload tracking homepage */
	//console.log("homepage");
			ga('send', 'event', 'OnLoad Partner Stories', '245698', 'homepage', {'nonInteraction': 1});
			ga('send', 'event', 'OnLoad Partner Stories', '245675', 'homepage', {'nonInteraction': 1});
	           
});

$(document).ready(function(){
        var trigger_depth={b2:99, b3:499, b4:1199};
        var is_trigger_active={b2:true, b3:true, b4:true};
        var call_on_scroll=true;
        
            $(window).on("scroll",function(){
            if((!(is_trigger_active.b2)&&!(is_trigger_active.b3)&&!(is_trigger_active.b4))==false)
            {
                callPreload(is_trigger_active,trigger_depth);
                
            }
    });
});

function callPreload(active,depth)
{
    var scroll_top = $(window).scrollTop();
    if((active.b2)&&(scroll_top > depth.b2))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Sep/vijendersonny_1443525340_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1443524595_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1443523882_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/iit-grad-502_1443525546_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/mars5_1443520648_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage-cd_1443521177_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage2_1443523542_1443523547_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1443515794_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/modi_cam_card_1443442344_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/coffee-and-cheese2_1443419643_1443419654_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/card-1_1443433289_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1443429737_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/mars---card2_1443459621_218x102.jpg");
        active.b2=false;
    }
    if((active.b3)&&(scroll_top > depth.b3))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Sep/ankit-fadia-502_1443520542_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/delhi-traffic-502_1443515530_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/marykompractice_1443511675_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage5_1443512671_1443512679_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1443512202_236x111.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/pri_dennis_card_1443526947_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/2_1443523749_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/malala_card_1443516817_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/pri_dennis_card_1443526947_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1443528573_1443528576_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/ffcard_1443505608_1443505617_218x102.jpg","http://media.indiatimes.in/media/content/2015/Mar/bhagat-singh-original-photo_502_1427262412_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/chapati-card_1443090177_218x102.jpg");
        active.b3=false;
    }
    
    if((active.b4)&&(scroll_top > depth.b4))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Sep/502_1443508180_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/dr-khurshid-502_1443507033_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/astro-music-502_1443511640_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/modi502_1443504174_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/lal-502_1443444783_236x111.jpg","http://media.indiatimes.in/media/content/2015/Sep/18-srk-salman_1443510979_1443510985_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/coffee-and-cheese2_1443419643_1443419654_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card-salman_1443512466_1443512479_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1443429737_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/modi_card_1443510874_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/harrypotter_card_1443513466_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage-5_1443510382_1443510389_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/ffcard_1443505608_1443505617_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card-1_1443433289_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/18dfr_mehmood_839031g_1443090146_1443090152_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/mars---card2_1443459621_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/cp1_1443262451_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/modi_cam_card_1443442344_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/beauty-card_1442923956_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card-04_1443443970_1443443976_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/cp_1443089343_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/cover_1443093037_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/cp_1443098781_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/dev-patel_1443425056_1443425072_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1442999613_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/fourmins_card_1443426238_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1443426849_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/srk1_1443097512_1443097519_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/sa_1443421295_1443421300_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/chapati-card_1443090177_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1443348542_1443348552_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/untitled_1443354793_1443354805_1443354809_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/cp_1443011279_1443011290_502x234.jpg","http://media.indiatimes.in/media/content/2015/Mar/bhagat-singh-original-photo_502_1427262412_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/sall_1443098614_1443098631_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card1_1442922596_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/gas_pump_card_1443349128_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card-1_1442921556_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/ganesh_visarjan_card_1443352831_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/men_haristyle_card_1443340388_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/cp_1443167867_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage_1443341648_1443341664_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/superheros_card_1443335876_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/weight_1442831165_502x234.jpg","http://media.indiatimes.in/media/content/2015/Jul/1111_1438075868_1438075874_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/krk-story-card_1443335105_1443335118_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/gif---card_1443336732_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/n-nana-patekar-large570_1443331280_1443331284_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1442988704_1442988712_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/picmonkey-collage1_1443331445_1443331450_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/pollution_card_1442574823_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card-3_1442994025_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/rkcard1_1443003098_1443003104_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/crd_1443273104_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1443258459_502x234.jpg","http://media.indiatimes.in/media/content/2015/Sep/cp1_1443262451_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/cp_1443098781_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/card_1442999613_218x102.jpg","http://media.indiatimes.in/media/content/2015/Sep/18dfr_mehmood_839031g_1443090146_1443090152_218x102.jpg","http://media.indiatimes.in/media/videocafe/2015/Sep/beauty-card_1442923956_218x102.jpg");
        active.b4=false;
    }
}
</script>    <div class="clr"></div>
      
	
                 <script> 
				                    showFooterCode();
					            </script>
	           <style>
.bottom-strip{background:#ebebeb;display:block;padding:15px 0;overflow:hidden;}.bottom-strip-box{margin:0 auto;display:table;max-width:410px;width:100%;}.square-logo{width:66px;height:64px;float:left;margin-right:10px;}.bottom-strip-content{float:left;margin-top:14px;}.bottom-strip-content span{display:block;color:#737272;font:12px/1 "RobotoRegular", Arial, Helvetica, sans-serif;margin-bottom:2px;}.bottom-strip-content a{color:#1b85dd;font:24px/1 "RobotoRegular", Arial, Helvetica, sans-serif;text-decoration:none;}
</style>
<div class="last-container">
    <!--<br>--> 
    <!--social-panel start-->
    <div class="social-panel red-bg">
        <div class="soc-inner"><!--soc-inner start-->
            <p class="soc-title">Be a Part of The New & Next</p>
            <div class="soc-count"><!--soc-count start-->
                <a href="https://www.facebook.com/indiatimes" target="_blank" title="facebook" class="sprite soc-fb"></a>
                <p>
                    3,615,680<span class="black"> FRIENDS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://twitter.com/indiatimes" target="_blank" title="twitter" class="sprite soc-twt"></a>
                <p>10343  <span class="black">FOLLOWERS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://plus.google.com/+indiatimes" target="_blank" title="gplus" class="sprite soc-gplus"></a>
                <p>104,163 <span class="black">MEMBERS</span></p>
            </div>
            <div class="soc-count"><!--soc-count start-->
                <a href="javascript:void(0);" title="E-mail" class="sprite soc-mail"></a>
                <p>49,706 <span class="black">SUBSCRIBERS</span></p>
            </div>
            <!--soc-count end-->
        </div>
        <!--soc-inner end-->
    </div>
    <!--social-panel end-->
    <div class="clr"></div>

    <footer>
        <!-- social container start-->
        <div class="socical">
            <div class="containers">
                <div class="fl left_cont"> Get your weekly dose of virality sent to your inbox! </div>
                <div class="fr right_cont">

                    <input type="text" value="Please Enter Email Address" class="Email" name="UserEmail" id="UserEmail" defaultValue="Please Enter Email Address"/>
                    <input type="submit" id="btn-submit" value="SIGN UP" name="btn-submit" class="Subscribe">
                    <span id="subsEmal" style='color: red; padding-left: 66px; text-align: left;'></span>
                </div>

            </div>
        </div><!-- social containers end-->
        <div class="footer"><!-- footer grey start-->
            <div class="sub_container">
                <!-- container start-->
                <div class="cont_new">
                    <h2>Our Channels</h2>
                                                <a href='http://www.indiatimes.com/news/' class="blue size">News</a> 
                                                    <a href='http://www.indiatimes.com/lifestyle/' class="purple size">Lifestyle</a> 
                                                    <a href='http://www.indiatimes.com/entertainment/' class="red size">Entertainment</a> 
                                                    <a href='http://www.indiatimes.com/videocafe/' class="orange size">Videos</a> 
                                                    <a href='http://www.indiatimes.com/health/' class="green size">Health</a> 
                        					<a href='http://www.indiatimes.com/trending' class="size" style="color:white">Trending</a> 
                </div><!-- container closed-->

                <div class="cont"> <!-- container start-->
                    <h2>Sections</h2>
                                            <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/news/india/'>india</a>
                                             
                                    <a href='http://www.indiatimes.com/news/world/'>world</a>
                                             
                                    <a href='http://www.indiatimes.com/news/sports/'>sports</a>
                                             
                                    <a href='http://www.indiatimes.com/news/weird/'>weird</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/lifestyle/self/'>self</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/style/'>style</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/who-we-are/'>who we are</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/travel/'>travel</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/food/'>food</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/entertainment/bollywood/'>bollywood</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/celebs/'>celebscoop</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/hollywood/'>hollywood</a>
                                
                             </div>                        <div class="sub_link">
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                             
                                    <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                             
                                    <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                             
                                    <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                             
                                    <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                
                             </div>                    <div class="sub_link"><a href="http://www.indiatimes.com/photogallery">Photogallery</a></div>
                </div><!-- container closed-->
                <div class="cont_new"> <!-- container start-->
                    <h2>Indiatimes Lifestyle Network</h2>

                    <div class="sub_link">                             <a href="http://www.mensxp.com"   target="_blank" >MENS XP</a>
                                                    <a href="http://www.idiva.com"   target="_blank" >iDiva</a>
                                                    <a href="http://luxpresso.com"   target="_blank" >Luxury</a>
                                                    <a href="http://in.askmen.com/"   target="_blank" >Ask Men</a>
                                                    <a href="http://www.pursuitist.in/"   target="_blank" >Pursuitist</a>
                        </div>
                </div><!-- container closed-->
                <div class="cont"> <!-- container start-->
                    <h2>Other Sites</h2>
                    <div class="sub_link"> 
                        
                            <a href="http://www.cricbuzz.com"  target="_blank">CricBuzz</a> 
                        
                            <a href="http://www.happytrips.com/"  target="_blank">Happy Trips</a> 
                        
                            <a href="http://www.businessinsider.in/"  target="_blank">Business Insider</a> 
                        
                            <a href="http://www.zoomtv.com"  target="_blank">ZoomTv</a> 
                        
                            <a href="http://www.gizmodo.in/"  target="_blank">Gizmodo</a> 
                        
                            <a href="http://www.lifehacker.co.in/"  target="_blank">Lifehacker</a> 
                        
                            <a href="http://in.ign.com/"  target="_blank">IGN</a> 
                        
                            <a href="http://boxtv.com"  target="_blank">BoxTV</a> 
                        
                            <a href="http://www.gaana.com"  target="_blank">Gaana</a> 
                        
                            <a href="http://shopping.indiatimes.com"  target="_blank">Online Shopping</a> 
                        
                            <a href="http://www.in.techradar.com/"  target="_blank">Techradar</a> 
                        
                            <a href="http://timesdeal.com"  target="_blank">Daily Deals</a> 
                        
                            <a href="http://www.gitanjalishop.com/"  target="_blank">Gitanjali Shop</a> 
                        
                            <a href="http://www.satvikshop.com/"  target="_blank">Satvik Shop</a> 
                        
                            <a href="http://whatshot.in"  target="_blank">WHAT'S HOT</a> 
                        
                            <a href="http://zigwheels.com"  target="_blank">ZigWheels</a> 
                        
                            <a href="http://filmipop.com"  target="_blank">FILMIPOP</a> 
                        
                            <a href="http://www.filmfare.com/"  target="_blank">Filmfare</a> 
                        
                            <a href="http://www.femina.in"  target="_blank">Femina</a> 
                        
                            <a href="http://mobile.indiatimes.com"  target="_blank">Mobile</a> 
                        
                            <a href="http://www.greetzap.com"  target="_blank">Greetings</a> 
                        
                            <a href="http://www.follo.co.in"  target="_blank">Follo</a> 
                        
                            <a href="http://timesjobs.com"  target="_blank">Jobs</a> 
                        
                            <a href="http://magicbricks.com"  target="_blank">Property</a> 
                        
                            <a href="http://www.astrospeak.com"  target="_blank">Astrology</a> 
                        
                            <a href="http://speakingtree.in"  target="_blank">Speaking Tree</a> 
                        
                            <a href="http://simplymarry.com"  target="_blank">Matrimonial</a> 
                        
                            <a href="http://itimes.com"  target="_blank">iTimes</a> 
                        
                            <a href="http://www.ads2book.com/"  target="_blank">Ads2book</a> 
                        
                            <a href="http://www.tcnext.com/"  target="_blank">TcNext</a> 
                                            </div>
                </div><!-- container closed-->
                <div class="cont border_none"><!-- container start-->
                    <h2>About us</h2>
                    <div class="sub_link">
                                                    <a href='http://www.indiatimes.com/privacypolicy' >Privacy Policy</a> 
                                                    <a href='http://www.indiatimes.com/sitemap.html' >sitemap</a> 
                                                    <a href='http://www.indiatimes.com/termsandcondition' >Terms & CONDITIONS</a> 
                                                    <a href='http://www.indiatimes.com/seoarchive' >ARCHIVES</a> 
                                                    <a href='http://www.indiatimes.com/contactus' >Contact us</a> 
                                                    <a href='http://www.indiatimes.com/aboutus' >ABOUT INDIATIMES</a> 
                                            </div>
                </div><!-- container closed-->
            </div><!-- footer grey closed-->
        </div><!-- footer black closed-->

        <div class="black_footer"><!-- footer black start-->
            <div class="containers">
                <p><a href="javascript:void(0)" class="copyrt">Copyright Â© 2015 Times Internet Limited. Powered by Indiatimes Lifestyle Network. All rights reserved</a></p>
                <span><a href="http://www.indiatimes.com/seoarchive/">Archive</a> | <a href="http://www.indiatimes.com/sitemap.html">Sitemap</a> | <a href="http://www.indiatimes.com/aboutus/">About Us</a> | <a href="http://www.indiatimes.com/contactus/">Contact Us</a> | <a href="http://www.indiatimes.com/privacypolicy/">Privacy Policy</a> | <a target="_blank" href="http://m.indiatimes.com">Indiatimes Mobile</a> | <a href="http://www.indiatimes.com/termsandcondition/">Terms of Use and Grievance Redressal Policy </a> </span> 
            </div>
        </div><!-- footer black end-->
    </footer>
</div>

    <script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.lazyload.min.js?v=100.26" charset="utf-8"></script>
<script defer type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.easing.1.3.min.js?v=100.26"></script>

<input type="hidden" name="currentId" id="currentId" value="">
    <div class="animate_cont dawn floating delay5">
        <span class="animate_arrow sprite_image"></span>
    </div>



<script type="text/javascript" src="http://media.indiatimes.in/resources/js/main.js?v=100.26"></script>    
<script type="text/javascript" src="http://media.indiatimes.in/resources/js/common.js?v=100.26"></script>
<script  defer type="text/javascript" src="http://media.indiatimes.in/resources/js/jquery-scrolltofixed.js?v=100.26"></script>

<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://cdn.mouseflow.com/projects/1e238441-c30e-4d1b-8edd-c0f14d8fa6b7.js' type='text/javascript'%3E%3C/script%3E"));</script>
<!-- mouse flow code ends-->
<script  src="http://tags.crwdcntrl.net/c/2818/cc.js?ns=_cc2818" id="LOTCC_2818"></script> 

<script> 
    _cc2818.bcp(); 
</script>

	 <p id="back-top" style="display:none;"><a href="#top"><span></span></a> </p>
</div><!--wrap end-->


</body>
</html>