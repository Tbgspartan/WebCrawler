



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>

                    
                    
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "420-0379728-1480026";
                var ue_id = "14F9JZ6GXV9BJKVXZ0QC";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        
    
        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />
    
    <meta property='fb:app_id' content='115109575169727' />
    
    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="14F9JZ6GXV9BJKVXZ0QC" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1a-c3-2xl-i-f6fd650a.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-1379726009._CB293124761_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-3118624260._CB293333852_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-2454701167._CB293329573_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-1180111305._CB293333875_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-520887519._CB293333839_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
(function(){var d=function(o){return Object.prototype.toString.call(o)==="[object Array]";},g=function(q,p){var o;for(o=0;o<q.length;o++){if(o in q){p.call(null,q[o],o);}}},h=[],k,b,l=false,n=false,f=function(){var o=[],p=[],q={};g(h,function(s){var r="";g(s.dartsite.split("/"),function(t){if(t!==""){if(t in q){}else{q[t]=o.length;o.push(t);}r+="/"+q[t];}});p.push(r);});return{iu_parts:o,enc_prev_ius:p};},c=function(){var o=[];g(h,function(q){var p=[];g(q.sizes,function(r){p.push(r.join("x"));});o.push(p.join("|"));});return o;},m=function(){var o=[];g(h,function(p){o.push(a(p.targeting));});return o.join("|");},a=function(r,o){var q,p,s=[];for(q in r){p=[];for(j=0;j<r[q].length;j++){p.push(encodeURIComponent(r[q][j]));}if(o){s.push(q+"="+encodeURIComponent(p.join(",")));}else{s.push(q+"="+p.join(","));}}return s.join("&");},e=function(){var o=+new Date();if(n){return;}if(!this.readyState||"loaded"===this.readyState){n=true;if(l){imdbads.cmd.push(function(){for(i=0;i<h.length;i++){generic.monitoring.record_metric(h[i].name+".fail",csm.duration(o));}});}}};window.tinygpt={define_slot:function(r,q,o,p){h.push({dartsite:r.replace(/\/$/,""),sizes:q,name:o,targeting:p});},set_targeting:function(o){k=o;},callback:function(q){var r,p={},t,o,s=+new Date();l=false;for(r=0;r<h.length;r++){t=h[r].dartsite;o=h[r].name;if(q[r][t]){p[o]=q[r][t];}else{window.console&&console.error&&console.error("Unable to correlate GPT response for "+o);}}imdbads.cmd.push(function(){for(r=0;r<h.length;r++){ad_utils.slot_events.trigger(h[r].name,"request",{timestamp:b});ad_utils.slot_events.trigger(h[r].name,"tagdeliver",{timestamp:s});}ad_utils.gpt.handle_response(p);});},send:function(){var r=[],q=function(s,t){if(d(t)){t=t.join(",");}if(t){r.push(s+"="+encodeURIComponent(""+t));}},o,p;if(h.length===0){tinygpt.callback({});return;}q("gdfp_req","1");q("correlator",Math.floor(4503599627370496*Math.random()));q("output","json_html");q("callback","tinygpt.callback");q("impl","fifs");q("json_a","1");result=f();q("iu_parts",result.iu_parts);q("enc_prev_ius",result.enc_prev_ius);q("prev_iu_szs",c());q("prev_scp",m());q("cust_params",a(k,true));o=document.createElement("script");p=document.getElementsByTagName("script")[0];o.async=true;o.type="text/javascript";o.src="http://pubads.g.doubleclick.net/gampad/ads?"+r.join("&");o.id="tinygpt";o.onload=o.onerror=o.onreadystatechange=e;l=true;p.parentNode.insertBefore(o,p);b=+new Date();}};})();</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['a'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['2'],
'u': ['626184888514'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-tarnhelm-1244366189._CB292973052_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"97f676e0dc3aa0d219b01b7509975428820d6c7a",
"2015-09-30T00%3A15%3A47GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 24253;
generic.days_to_midnight = 0.2807060182094574;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-1582251138._CB317085808_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'a']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm === undefined || window.generic === undefined || window.consoleLog === undefined)) {
                    if (console !== undefined && console.log !== undefined) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=a;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=626184888514;ord=626184888514?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=a;bpx=2;c=0;s=3075;s=32;ord=626184888514?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=a;bpx=2;c=0;s=3075;s=32;ord=626184888514?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
onsubmit="(new Image()).src='/rg/SEARCH-BOX/HEADER/images/b.gif?link=/find';"
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/chart/toptv/?ref_=nv_tp_tv250_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                            <li><a href="/search/title?count=100&title_type=feature,tv_series,tv_movie&explore=title_type,genres,year,countries&ref_=nv_ch_mm_1"
> Popular Movies & TV
</a>                            </li>
                        <li><a href="/chart/?ref_=nv_ch_cht_2"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_3"
>Oscar Winners</a></li>
                        <li><a href="/chart/top?ref_=nv_ch_250_4"
>Top 250 Movies</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_5"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/falltv/?ref_=nv_tvv_fall_1"
>Fall TV</a></li>
                        <li><a href="/list/ls074418362/?ref_=nv_tvv_picks_2"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_3"
>On Tonight</a></li>
                        <li><a href="/chart/toptv/?ref_=nv_tvv_250_4"
>Top 250 TV</a></li>
                        <li><a href="/list/ls074093523/?ref_=nv_tvv_wn_5"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_6"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/family-entertainment-guide/?ref_=nv_sf_feg_1"
>Family Entertainment Guide</a></li>
                        <li><a href="/best-of/?ref_=nv_sf_bo_2"
>Best of 2014</a></li>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_3"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/whattowatch/?ref_=nv_sf_wtw_4"
>What to Watch</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=09-30&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_2"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_3"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_4"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_5"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_6"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_7"
>Emmy Awards</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_8"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_9"
>Toronto Film Festival</a></li>
                        <li><a href="/awards-central/?ref_=nv_ev_all_10"
>More Popular Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni59057012/?ref_=nv_nw_tn_1"
> âPacific Rim 2â Delayed, âPitch Perfect 3â Gets New Release Date
</a><br />
                        <span class="time">2 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59055746/?ref_=nv_nw_tn_2"
> Matthew Vaughn to Direct Spy Thriller âI Am Pilgrimâ
</a><br />
                        <span class="time">7 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59056242/?ref_=nv_nw_tn_3"
> Jim Carrey âShocked and Saddenedâ Over Girlfriendâs Apparent Suicide
</a><br />
                        <span class="time">5 hours ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/games/guess?ref_=nv_cm_qz_3"
>Quiz Game</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_4"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5> 
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0116282/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTQwNTAzMTU4M15BMl5BanBnXkFtZTcwMDgzNDY3Mw@@._V1._SY325_CR80,5,410,315_.jpg",
            titleYears : "1996",
            rank : 152,
                    headline : "Fargo"
    },
    nameAd : {
            clickThru : "/name/nm2225369/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTQ1NzU2NTkxNl5BMl5BanBnXkFtZTcwNzMxOTUxOQ@@._V1._SX250_CR0,0,250,315_.jpg",
            rank : 43,
            headline : "Jennifer Lawrence"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYlcmqUUHLMjBx7XtHa8n3Ux2dV_4T3lqgGZpy2gJ0L6KAYMMd6g5GCP7H6kD10pYL6XwP2X0Ti%0D%0AT2i5650cUok_s_1GHZJpipznRe8Gwu15Eb4RD_xrZXUibBJjmkOeW5265LrYfUhKHQQ-Manb2Frt%0D%0AcPbwWO53rb0NDrH7_sEA_9t3xYKwJZVIM_ggCmDk1TsseVN4CR2DBCUUGHlGkJ5-6Q%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYgavZQ32VGEeX2aVH3RGznsUo2-pKELhte_Ns8enBlTHM-3p1RyfSFIvXC9eYr8389iZ_LlANi%0D%0A0eMLwyGemPrrrJxLBf_eUbDn95o4qih3w3Eu0HfsqWgJCW7qsyJWA1b0l3s_eyMXp0yGmljvSevI%0D%0AJrFFSWK-KPxe1dDAFx6CcxIR15zGJ8UTangIZf4ddEM1%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYlLH56ymQNmj5jduohYYjU0495Bd3CqpkECQolFGVKz3m73guHFxEpwT8GK9CXpcUI2hbu8Y_l%0D%0A8WNRulJZQ1zIUybPnb7I0hFVGGvPC9sNhdO7bOq9stMua79FZCQrhXoDDFcoKPihcwCtCIcXgqoo%0D%0AmE9HlcphBi9XTpIB6jjCp1RKqx72O1adYALKdxdY0ww42f2lWPFbUoV5Majt8eTDYw%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=626184888514;ord=626184888514?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>

    <div id="pagecontent">
    <div class="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=626184888514;ord=626184888514?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	
    </div>
    <div id="top-slot-wrapper" class="pagecontent">
                    
                    
    </div>
    <div class="pagecontent">
        <div id="content-2-wide">
            <div id="main">
                    
        <div class="heroWidget">
        <span class="ab_widget"
        >
            <div class="ab_hero">
<div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2642129689?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223556582&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223556582&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi2642129689" data-source="bylist" data-id="ls002653141" data-rid="14F9JZ6GXV9BJKVXZ0QC" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="Count down 10 trivia items about the world of James Bond." alt="Count down 10 trivia items about the world of James Bond." src="http://ia.media-imdb.com/images/M/MV5BMTQ4MTg0MzQ2MV5BMl5BanBnXkFtZTgwMDMzMzk4NjE@._V1_SY298_CR10,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ4MTg0MzQ2MV5BMl5BanBnXkFtZTgwMDMzMzk4NjE@._V1_SY298_CR10,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Count down 10 trivia items about the world of James Bond." title="Count down 10 trivia items about the world of James Bond." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Count down 10 trivia items about the world of James Bond." title="Count down 10 trivia items about the world of James Bond." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2379713/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223556582&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223556582&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > "The World of Bond" </a> </div> </div> <div class="secondary ellipsis"> 10 James Bond Trivia Facts </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi595309337?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223556582&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223556582&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi595309337" data-source="bylist" data-id="ls002309697" data-rid="14F9JZ6GXV9BJKVXZ0QC" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title=".In an expedition of the uncharted American wilderness, explorer Hugh Glass is brutally attacked by a bear and left for dead by members of his own hunting team. In a quest to survive, Glass endures unimaginable grief as well as the betrayal of his confidant John Fitzgerald. Guided by sheer will and the love of his family, Glass must navigate a vicious winter in a relentless pursuit to live and find redemption." alt=".In an expedition of the uncharted American wilderness, explorer Hugh Glass is brutally attacked by a bear and left for dead by members of his own hunting team. In a quest to survive, Glass endures unimaginable grief as well as the betrayal of his confidant John Fitzgerald. Guided by sheer will and the love of his family, Glass must navigate a vicious winter in a relentless pursuit to live and find redemption." src="http://ia.media-imdb.com/images/M/MV5BOTIwMzA4OTAwMl5BMl5BanBnXkFtZTgwNDQ3MTkyNjE@._V1_SY298_CR131,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BOTIwMzA4OTAwMl5BMl5BanBnXkFtZTgwNDQ3MTkyNjE@._V1_SY298_CR131,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt=".In an expedition of the uncharted American wilderness, explorer Hugh Glass is brutally attacked by a bear and left for dead by members of his own hunting team. In a quest to survive, Glass endures unimaginable grief as well as the betrayal of his confidant John Fitzgerald. Guided by sheer will and the love of his family, Glass must navigate a vicious winter in a relentless pursuit to live and find redemption." title=".In an expedition of the uncharted American wilderness, explorer Hugh Glass is brutally attacked by a bear and left for dead by members of his own hunting team. In a quest to survive, Glass endures unimaginable grief as well as the betrayal of his confidant John Fitzgerald. Guided by sheer will and the love of his family, Glass must navigate a vicious winter in a relentless pursuit to live and find redemption." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt=".In an expedition of the uncharted American wilderness, explorer Hugh Glass is brutally attacked by a bear and left for dead by members of his own hunting team. In a quest to survive, Glass endures unimaginable grief as well as the betrayal of his confidant John Fitzgerald. Guided by sheer will and the love of his family, Glass must navigate a vicious winter in a relentless pursuit to live and find redemption." title=".In an expedition of the uncharted American wilderness, explorer Hugh Glass is brutally attacked by a bear and left for dead by members of his own hunting team. In a quest to survive, Glass endures unimaginable grief as well as the betrayal of his confidant John Fitzgerald. Guided by sheer will and the love of his family, Glass must navigate a vicious winter in a relentless pursuit to live and find redemption." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt1663202/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223556582&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223556582&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > The Revenant </a> </div> </div> <div class="secondary ellipsis"> Trailer #2 </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi108573465?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223556582&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223556582&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi108573465" data-source="bylist" data-id="ls002922459" data-rid="14F9JZ6GXV9BJKVXZ0QC" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="Adam Jones is a chef who destroyed his career with drugs and diva behavior. He cleans up and returns to London, determined to redeem himself by spearheading a top restaurant that can gain three Michelin stars." alt="Adam Jones is a chef who destroyed his career with drugs and diva behavior. He cleans up and returns to London, determined to redeem himself by spearheading a top restaurant that can gain three Michelin stars." src="http://ia.media-imdb.com/images/M/MV5BNjEzNTk2OTEwNF5BMl5BanBnXkFtZTgwNzExMTg0NjE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjEzNTk2OTEwNF5BMl5BanBnXkFtZTgwNzExMTg0NjE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Adam Jones is a chef who destroyed his career with drugs and diva behavior. He cleans up and returns to London, determined to redeem himself by spearheading a top restaurant that can gain three Michelin stars." title="Adam Jones is a chef who destroyed his career with drugs and diva behavior. He cleans up and returns to London, determined to redeem himself by spearheading a top restaurant that can gain three Michelin stars." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Adam Jones is a chef who destroyed his career with drugs and diva behavior. He cleans up and returns to London, determined to redeem himself by spearheading a top restaurant that can gain three Michelin stars." title="Adam Jones is a chef who destroyed his career with drugs and diva behavior. He cleans up and returns to London, determined to redeem himself by spearheading a top restaurant that can gain three Michelin stars." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2503944/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223556582&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223556582&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > Burnt </a> </div> </div> <div class="secondary ellipsis"> Trailer #2 </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/trailers?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223556582&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223556582&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Browse more trailers </a> </p>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/falltv/stephen-moyer-reveals-things-you-need-to-know-about-the-bastard-executioner?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223274242&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_ftwd_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223274242&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Fall TV Spotlight: Stephen Moyer Reveals 7 Things You Need to Know about "Bastard Executioner"</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/falltv/stephen-moyer-reveals-things-you-need-to-know-about-the-bastard-executioner?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223274242&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_ftwd_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223274242&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Bastard Executioner (2015-)" alt="The Bastard Executioner (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMjM4NzY3MDUzNl5BMl5BanBnXkFtZTgwMjkzNzg0NjE@._V1_SX170_CR0,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjM4NzY3MDUzNl5BMl5BanBnXkFtZTgwMjkzNzg0NjE@._V1_SX170_CR0,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/falltv/stephen-moyer-reveals-things-you-need-to-know-about-the-bastard-executioner?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223274242&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_ftwd_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223274242&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Bastard Executioner (2015-)" alt="The Bastard Executioner (2015-)" src="http://ia.media-imdb.com/images/M/MV5BNTkzMTg4MTc1MF5BMl5BanBnXkFtZTgwNTEzMTk4NjE@._V1_SX444_CR0,0,444,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNTkzMTg4MTc1MF5BMl5BanBnXkFtZTgwNTEzMTk4NjE@._V1_SX444_CR0,0,444,250_AL_UY500_UX888_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">He's gone from playing a good vampire on <a href="/title/tt0844441?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223274242&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_ftwd_lk1">"True Blood"</a> to an evil human being on FX's <a href="/title/tt4218618?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223274242&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_ftwd_lk2">"The Bastard Executioner."</a> <a href="/name/nm0610459?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223274242&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_ftwd_lk3">Stephen Moyer</a> reveals seven intriguing facts about his new role on FX's new <a href="/name/nm1176676?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223274242&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_ftwd_lk4">Kurt Sutter</a> drama.</p> <p class="seemore"> <a href="/falltv/stephen-moyer-reveals-things-you-need-to-know-about-the-bastard-executioner?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223274242&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_ftwd_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223274242&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Read more </a> </p>    </div>

        </span>
        </div>
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59057012?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTY3MTI5NjQ4Nl5BMl5BanBnXkFtZTcwOTU1OTU0OQ@@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59057012?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1"
class="headlines" >âPacific Rim 2â Delayed, âPitch Perfect 3â Gets New Release Date</a>
    <div class="infobar">
            <span class="text-muted">2 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_src"
>Variety - Film News</a></span>
    </div>
                                </div>
<p> <a href="/company/co0005073?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk1">Universal Pictures</a> has removed â<a href="/title/tt1663662?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk2">Pacific Rim</a> 2â from the release calendar, but the studio insists that it is committed to making a sequel to the monsters versus robots film. In a statement, Universal and Legendary Entertainment, the filmâs primary financial backer, says that it is delaying the film...                                        <span class="nobr"><a href="/news/ni59057012?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59055746?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2"
class="headlines" >Matthew Vaughn to Direct Spy Thriller âI Am Pilgrimâ</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2_src"
>The Wrap</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59056242?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3"
class="headlines" >Jim Carrey âShocked and Saddenedâ Over Girlfriendâs Apparent Suicide</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59053544?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4"
class="headlines" >Catherine E. Coulson Dies: âTwin Peaksâ Log Lady Was 71</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004913?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4_src"
>Deadline TV</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59055808?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5"
class="headlines" >ABC Family Orders Nicki Minaj Scripted Comedy Based on Her Family's Immigration From Trinidad</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0032055?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5_src"
>Indiewire Television</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59057012?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTY3MTI5NjQ4Nl5BMl5BanBnXkFtZTcwOTU1OTU0OQ@@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59057012?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1"
class="headlines" >âPacific Rim 2â Delayed, âPitch Perfect 3â Gets New Release Date</a>
    <div class="infobar">
            <span class="text-muted">2 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_src"
>Variety - Film News</a></span>
    </div>
                                </div>
<p> <a href="/company/co0005073?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk1">Universal Pictures</a> has removed â<a href="/title/tt1663662?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk2">Pacific Rim</a> 2â from the release calendar, but the studio insists that it is committed to making a sequel to the monsters versus robots film. In a statement, Universal and Legendary Entertainment, the filmâs primary financial backer, says that it is delaying the film...                                        <span class="nobr"><a href="/news/ni59057012?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59056300?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2"
class="headlines" >âPaper Townsâ Nat Wolff to Star in Adam Wingardâs âDeath Noteâ</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59055584?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3"
class="headlines" >Ellar Coltrane Gets First Post-Boyhood Role</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3_src"
>Vulture</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59055746?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4"
class="headlines" >Matthew Vaughn to Direct Spy Thriller âI Am Pilgrimâ</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4_src"
>The Wrap</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59055624?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5"
class="headlines" >Mathieu Amalric To Star In BenoÃ®t Jacquot's Adaptation Of Don DeLillo's 'The Body Artist'</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0011867?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5_src"
>The Playlist</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59053544?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMzc0MTg5OTc4N15BMl5BanBnXkFtZTgwNzQyNzE5NjE@._V1_SY150_CR7,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59053544?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1"
class="headlines" >Catherine E. Coulson Dies: âTwin Peaksâ Log Lady Was 71</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004913?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_src"
>Deadline TV</a></span>
    </div>
                                </div>
<p>Updated with statement from <a href="/name/nm0000186?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk1">David Lynch</a>:Â <a href="/name/nm0183466?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk2">Catherine E. Coulson</a>, a camera assistant and actress best known for playing the Log Lady on <a href="/title/tt0098936?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk3">Twin Peaks</a>, died today of cancer. She was 71. âToday I lost one of my dearest friends,â <a href="/title/tt0098936?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk4">Twin Peaks</a>Â creatorÂ <a href="/name/nm0000186?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk5">David Lynch</a> said in a statement. âCatherine was solid gold....                                        <span class="nobr"><a href="/news/ni59053544?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59055808?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2"
class="headlines" >ABC Family Orders Nicki Minaj Scripted Comedy Based on Her Family's Immigration From Trinidad</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0032055?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2_src"
>Indiewire Television</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59055617?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3"
class="headlines" >Allen Bainâs Bainframe Harnesses Robert Heinleinâs âThe Man Who Sold The Moonâ</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004913?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3_src"
>Deadline TV</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59055484?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4"
class="headlines" >Why Marvel Changed Daredevil From A Movie To A Netflix Show</a>
    <div class="infobar">
            <span class="text-muted">9 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000098?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4_src"
>cinemablend.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59055898?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5"
class="headlines" >Curtis â50 Centâ Jackson Inks Overall Deal With Starz</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004913?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5_src"
>Deadline TV</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59057230?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTU3Njg5MDUwNV5BMl5BanBnXkFtZTcwNzMyMzMwMw@@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59057230?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1"
class="headlines" >Ralph Lauren Is Stepping Down as CEO of His Fashion EmpireâAll the Details!</a>
    <div class="infobar">
            <span class="text-muted">2 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_src"
>E! Online</a></span>
    </div>
                                </div>
<p><a href="/name/nm0491073?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk1">Ralph Lauren</a> is handing over the CEO reigns of his namesake company. Butâ¦Mr. Lauren isn't exactly exiting the picture. The 75-year-old may be relinquishing his office as CEO, but he will still retain the post of executive chairman and chief creative director of the multi-brand company. Meaning that...                                        <span class="nobr"><a href="/news/ni59057230?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59056242?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2"
class="headlines" >Jim Carrey âShocked and Saddenedâ Over Girlfriendâs Apparent Suicide</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59055831?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3"
class="headlines" >LL Cool J's Son Najee Smith Arrested and Charged With Trespassing After Altercation at NYC Restaurant</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3_src"
>E! Online</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59055768?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4"
class="headlines" >Tom Hanks and Amy Poehler Dated and Lived Together in the '80s . . . Sort Of</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000088?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4_src"
>Popsugar.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59055438?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5"
class="headlines" >Bill Cosbyâs Honorary Degree Yanked by Brown University in Wake of Sex Scandal</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5_src"
>The Wrap</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <h3>IMDb Snapshot</h3> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1447750656/rg1372363520?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2222778042&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2222778042&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Macbeth (2015)" alt="Macbeth (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTg4NzYyMDM5MF5BMl5BanBnXkFtZTgwODEwMzE5NjE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg4NzYyMDM5MF5BMl5BanBnXkFtZTgwODEwMzE5NjE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm1447750656/rg1372363520?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2222778042&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2222778042&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <i>Macbeth</i> UK Premiere </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm4081708032/rg1422695168?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2222778042&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2222778042&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Revenant (2015)" alt="The Revenant (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTkxMzE2NDA4OV5BMl5BanBnXkFtZTgwNDE3MjE5NjE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTkxMzE2NDA4OV5BMl5BanBnXkFtZTgwNDE3MjE5NjE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm4081708032/rg1422695168?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2222778042&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2222778042&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <i>The Revenant</i> </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1497426944/rg1573690112?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2222778042&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2222778042&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjM0MjEzNjc2Ml5BMl5BanBnXkFtZTgwNTQ4NjE5NjE@._V1._CR0,0,1470,1470_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjM0MjEzNjc2Ml5BMl5BanBnXkFtZTgwNTQ4NjE5NjE@._V1._CR0,0,1470,1470_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm1497426944/rg1573690112?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2222778042&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2222778042&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > 53rd New York Film Festival </a> </div> </div> </div> </div> </div> </div> </div>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
                <div class="ab_borntoday">
<span class="ninja_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=9-29&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Born Today</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1157048?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Zachary Levi" alt="Zachary Levi" src="http://ia.media-imdb.com/images/M/MV5BMTAyNTAzMTA4OTJeQTJeQWpwZ15BbWU3MDA4NDI2Njk@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTAyNTAzMTA4OTJeQTJeQWpwZ15BbWU3MDA4NDI2Njk@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1157048?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_1">Zachary Levi</a> (35) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0574534?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Ian McShane" alt="Ian McShane" src="http://ia.media-imdb.com/images/M/MV5BMTQwMzAzMTIyMF5BMl5BanBnXkFtZTcwMjM1MDM2OQ@@._V1_SY172_CR18,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQwMzAzMTIyMF5BMl5BanBnXkFtZTcwMjM1MDM2OQ@@._V1_SY172_CR18,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0574534?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_2">Ian McShane</a> (73) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0000143?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Erika Eleniak" alt="Erika Eleniak" src="http://ia.media-imdb.com/images/M/MV5BMTk4NDEyNzE1M15BMl5BanBnXkFtZTcwODE5OTQ1NA@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk4NDEyNzE1M15BMl5BanBnXkFtZTcwODE5OTQ1NA@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0000143?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_3">Erika Eleniak</a> (46) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0331577?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Luke Goss" alt="Luke Goss" src="http://ia.media-imdb.com/images/M/MV5BMTc3NDU3Mjk2OV5BMl5BanBnXkFtZTgwOTQxNDA2NjE@._V1_SY172_CR16,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc3NDU3Mjk2OV5BMl5BanBnXkFtZTgwOTQxNDA2NjE@._V1_SY172_CR16,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0331577?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_4">Luke Goss</a> (47) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0002093?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Greer Garson" alt="Greer Garson" src="http://ia.media-imdb.com/images/M/MV5BMjE4MDg5MjE5NV5BMl5BanBnXkFtZTcwNTcwMDYzOA@@._V1_SY172_CR5,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE4MDg5MjE5NV5BMl5BanBnXkFtZTcwNTcwMDYzOA@@._V1_SY172_CR5,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0002093?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_5">Greer Garson</a> (1904-1996) </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/search/name?refine=birth_monthday&birth_monthday=9-29&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See all birthdays </a> </p>        </div>

        </span>
        </div>
                    
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/title/tt3186946/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2222787642&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2222787642&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Indie Focus: 'Xenia' - U.S. Trailer</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm798288896/tt3186946?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2222787642&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2222787642&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Xenia (2014)" alt="Xenia (2014)" src="http://ia.media-imdb.com/images/M/MV5BMTYyNzEwMDg1Nl5BMl5BanBnXkFtZTgwMjQ2Mjk4NjE@._V1_SX170_CR0,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTYyNzEwMDg1Nl5BMl5BanBnXkFtZTgwMjQ2Mjk4NjE@._V1_SX170_CR0,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi50967321?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2222787642&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2222787642&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi50967321" data-rid="14F9JZ6GXV9BJKVXZ0QC" data-type="single" class="video-colorbox" data-refsuffix="hm_if" data-ref="hm_if_i_2"> <img itemprop="image" class="pri_image" title="Xenia (2014)" alt="Xenia (2014)" src="http://ia.media-imdb.com/images/M/MV5BMjM2NTk2OTEzMl5BMl5BanBnXkFtZTgwMTExMzE2MjE@._V1_SX444_CR0,0,444,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjM2NTk2OTEzMl5BMl5BanBnXkFtZTgwMTExMzE2MjE@._V1_SX444_CR0,0,444,250_AL_UY500_UX888_AL_.jpg" /> <img alt="Xenia (2014)" title="Xenia (2014)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Xenia (2014)" title="Xenia (2014)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">Strangers in their own birthplace, Danny and Odysseus cross the country in search of their Greek father, after their Albanian mother passes away.</p> <p class="seemore"> <a href="/title/tt3186946/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2222787642&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2222787642&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Learn more about <i>Xenia</i> </a> </p>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_trivia">
<span class="ninja_header"> <span class="oneline"> <a href="/title/tt0804503/trivia?item=tr1665661&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Did You Know?</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt0804503?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Mad Men (2007-2015)" alt="Mad Men (2007-2015)" src="http://ia.media-imdb.com/images/M/MV5BMjMwNzk5Nzg2OV5BMl5BanBnXkFtZTgwMjg1OTk1NDE@._V1_SY132_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjMwNzk5Nzg2OV5BMl5BanBnXkFtZTgwMjg1OTk1NDE@._V1_SY132_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" > <strong class="text-large"><a href="/title/tt0804503?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Mad Men</a></strong> <p class="blurb">In 2012, costume designer <a href="/name/nm0117124?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Janie Bryant</a> told Slate magazine that she always repeats one of Peggy's costumes from the previous season during the next season's premiere. Bryant said that she "[loves] that tradition for Peggy because I think that this all is really based in reality. That's what we would do in real life: We repeat our clothes."</p></div> </div> </div> <p class="seemore"> <a href="/title/tt0804503/trivia?item=tr1665661&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See more trivia </a> </p>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;wPKFb8jB5fc&quot;}"
        >
            <div class="ab_poll poll">
<span class="ninja_header"> <span class="oneline"> <a href="/poll/wPKFb8jB5fc/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2221808002&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2221808002&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Poll: IMDb Birthday Poll</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2221808002&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk">More Polls</a></h4> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/wPKFb8jB5fc/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2221808002&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2221808002&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Goodfellas (1990)" alt="Goodfellas (1990)" src="http://ia.media-imdb.com/images/M/MV5BMTY2OTE5MzQ3MV5BMl5BanBnXkFtZTgwMTY2NTYxMTE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY2OTE5MzQ3MV5BMl5BanBnXkFtZTgwMTY2NTYxMTE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/wPKFb8jB5fc/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2221808002&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2221808002&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Ghost (1990)" alt="Ghost (1990)" src="http://ia.media-imdb.com/images/M/MV5BMTU0NzQzODUzNl5BMl5BanBnXkFtZTgwMjc5NTYxMTE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU0NzQzODUzNl5BMl5BanBnXkFtZTgwMjc5NTYxMTE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/wPKFb8jB5fc/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2221808002&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2221808002&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Pretty Woman (1990)" alt="Pretty Woman (1990)" src="http://ia.media-imdb.com/images/M/MV5BNjk2ODQzNDYxNV5BMl5BanBnXkFtZTgwMTcyNDg4NjE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjk2ODQzNDYxNV5BMl5BanBnXkFtZTgwMTcyNDg4NjE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/wPKFb8jB5fc/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2221808002&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2221808002&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Misery (1990)" alt="Misery (1990)" src="http://ia.media-imdb.com/images/M/MV5BNzY0ODQ3MTMxN15BMl5BanBnXkFtZTgwMDkwNTg4NjE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNzY0ODQ3MTMxN15BMl5BanBnXkFtZTgwMDkwNTg4NjE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/wPKFb8jB5fc/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2221808002&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2221808002&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Dances with Wolves (1990)" alt="Dances with Wolves (1990)" src="http://ia.media-imdb.com/images/M/MV5BMTY3OTI5NDczN15BMl5BanBnXkFtZTcwNDA0NDY3Mw@@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY3OTI5NDczN15BMl5BanBnXkFtZTcwNDA0NDY3Mw@@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">From this list of movies that earned at least one Oscar nomination and were released the year that IMDb was born which one is your favorite? Discuss <a href="http://www.imdb.com/board/bd0000088/nest/221031856?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2221808002&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk1">here</a> after voting.</p> <p class="seemore"> <a href="/poll/wPKFb8jB5fc/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2221808002&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2221808002&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Vote now </a> </p>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
            </div>
            <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,11x1;p=tr;fv=1;ab=a;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=626184888514;ord=626184888514?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,11x1;p=tr;fv=1;ab=a;bpx=2;c=0;s=3075;s=32;ord=626184888514?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,11x1;p=tr;fv=1;ab=a;bpx=2;c=0;s=3075;s=32;ord=626184888514?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	
                    
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223413762&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223413762&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Opening This Week</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3659388"></div> <div class="title"> <a href="/title/tt3659388?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223413762&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t0"> The Martian </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3488710"></div> <div class="title"> <a href="/title/tt3488710?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223413762&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t1"> The Walk </a> <span class="secondary-text"></span> </div> <div class="action"> Opens on 9/30 on IMAX Only </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1658801"></div> <div class="title"> <a href="/title/tt1658801?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223413762&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t2"> Freeheld </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3065132"></div> <div class="title"> <a href="/title/tt3065132?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223413762&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t3"> He Named Me Malala </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1092634"></div> <div class="title"> <a href="/title/tt1092634?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223413762&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t4"> Shanghai </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3155242"></div> <div class="title"> <a href="/title/tt3155242?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223413762&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t5"> Partisan </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4000870"></div> <div class="title"> <a href="/title/tt4000870?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223413762&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t6"> Addicted to Fresno </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3397884"></div> <div class="title"> <a href="/title/tt3397884?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223413762&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t7"> Sicario </a> <span class="secondary-text"></span> </div> <div class="action"> Nationwide Expansion </div> </div> </div> </div> </div> <div><a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223413762&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223413762&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more opening this week</p></a></div>
        </span>
        </div>
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
            <div class="ab_ninja showtimesMessagingWidget">
<p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2006955922&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p>    </div>

        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/chart/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Now Playing (Box Office)</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2510894"></div> <div class="title"> <a href="/title/tt2510894?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t0"> Hotel Transylvania 2 </a> <span class="secondary-text">$47.5M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2361509"></div> <div class="title"> <a href="/title/tt2361509?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t1"> The Intern </a> <span class="secondary-text">$18.2M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4046784"></div> <div class="title"> <a href="/title/tt4046784?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t2"> Maze Runner: The Scorch Trials </a> <span class="secondary-text">$14.0M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2719848"></div> <div class="title"> <a href="/title/tt2719848?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t3"> Everest </a> <span class="secondary-text">$13.1M</span> </div> <div class="action"> <a href="/showtimes/title/tt2719848?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1355683"></div> <div class="title"> <a href="/title/tt1355683?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t4"> Black Mass </a> <span class="secondary-text">$11.5M</span> </div> </div> </div> </div> </div> <div><a href="/chart/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more box office results</p></a></div>
        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Coming Soon</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3332064"></div> <div class="title"> <a href="/title/tt3332064?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t0"> Pan </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3488710"></div> <div class="title"> <a href="/title/tt3488710?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t1"> The Walk </a> <span class="secondary-text"></span> </div> <div class="action"> Nationwide Expansion </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3254796"></div> <div class="title"> <a href="/title/tt3254796?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t2"> Big Stone Gap </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2080374"></div> <div class="title"> <a href="/title/tt2080374?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t3"> Steve Jobs </a> <span class="secondary-text"></span> </div> <div class="action"> Opens in LA/NY </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2118624"></div> <div class="title"> <a href="/title/tt2118624?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t4"> The Final Girls </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> </div> </div> </div> <div><a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more coming soon</p></a></div>
        </span>
        </div>
                    
                    
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
            <div class="ab_twitter">
<span class="widget_header"> <h3>Follow Us On Twitter</h3> </span> <div class="widget_content"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3776329187._CB305760180_.html#{"screen-name":"IMDb","height":"500px","width":"300px"}'></iframe> </div> </div>    </div>

        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
            <div class="ab_facebook">
<span class="widget_header"> <h3>Find Us On Facebook</h3> </span> <div class="widget_content"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
        <div class="aux-content-widget-2 sticky-widget">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="http://www.imdb.com/family-entertainment-guide/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223264822&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_feg_rhs_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223264822&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <h3>10 Best Family Movies for Parents</h3> </a> </span> </span> <p class="blurb">It's time to pick a family film that you'll enjoy at least as much as the kids.</p> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/family-entertainment-guide/best-movies-for-parents?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223264822&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_feg_rhs_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223264822&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Honey, I Shrunk the Kids (1989)" alt="Honey, I Shrunk the Kids (1989)" src="http://ia.media-imdb.com/images/M/MV5BMTUyMzk5NDMxMF5BMl5BanBnXkFtZTcwNTQ1MDkwMw@@._V1_SY525_CR76,0,700,525_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUyMzk5NDMxMF5BMl5BanBnXkFtZTcwNTQ1MDkwMw@@._V1_SY525_CR76,0,700,525_AL_UY1050_UX1400_AL_.jpg" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="http://www.imdb.com/family-entertainment-guide/best-movies-for-parents?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223264822&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_feg_rhs_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223264822&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > 10 Best Family Movies for Parents </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="http://www.imdb.com/family-entertainment-guide/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223264822&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_feg_rhs_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2223264822&pf_rd_r=14F9JZ6GXV9BJKVXZ0QC&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Visit our Family Entertainment Guide </a> </p>    </div>

        </span>
        </div>
            </div>
        </div>
        
                    
                    
        
    
        <br class="clear" />
    </div>
</div>
                





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYljBFk1IVe3fFOKjP1OFel7ngAk4mXlRWtSc38WxwXT8-eV7g87ktdPRUBesHLz3XG_B4sgzFG%0D%0AMa2kF-jYDIq35WnAjoG6zewrCpwXt5hl7zwAxMJRGFCTMfA7wYYK7Z4VGD2AuzfOIrMqKD62wUFN%0D%0ACwWfgyJ9_wTaLRKVuYqVmiS4gC6T-1DEoUXLTHaISTkgVb-Fc37Z0qSslWqBtTrWH12r4KvVALFh%0D%0AE5_cpUneSSM%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYleBNGmFmOrpjKHcTYlIs0IXOeLsSULPHDUSWx6V9WyKBiTVh3Xdh1Y4Y7FhjB8aRqNRKu77gL%0D%0AhktoPIQ4Mm_kCIpymfFUPIr8ZubL4KBDQN9rFzOExnvRlM3ryecENDBa7QuFDOXvMQNaDF1gN7yV%0D%0As_sT6RXLglEXtMuIohAl8bbyUoHRkqP0d8FvmZU3mEZFj6trEPgApRvvw3QPOQBgjg%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYrKWCdmtwPhHTk2GaINwG2DKpyueQ8Rd-PRHpIfqucnUdxbbJX7h2GeOFwRxoy7t4_FBsJUSAu%0D%0AMyvWk-0tw2t0XrApAhmomg3FZIBhOxS4_hmo56SsTSJuCO7co-QQDG9xZopR2Gh15DycsV5V9DnQ%0D%0AGNAI5Bt3limQdKb8XkheMZ81Fp5K0_2uxVv46lom2yKdUUsKQm-ntjUuG6GU_xbhWoR2gBqnXDYH%0D%0AGLSALPksA0pkX3bF2XPZdy8tJlS1u9r2V5dgP78CAWHBty6b2V8BTMB2-NKy9rMHWyO1TGJI2C47%0D%0Anj2PEnkOTNWIoFM9cdM5iA4a3QqnaXkYNXPTWyTikDKJ5ik78VKKf9grR_0wcz5l8ztYI0CUKmtr%0D%0AlcCQBT0YMMBT6Y9yCnC9iMkHD5E6Pg%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYqMsgQWxz5-pV-6BhhN7WUNWzY8LiGXT4t4IMajg1lnLG01S8zLGH_74XGsFw7CzGwNcvxU4Kj%0D%0A0G53ROk1QTnjtCG0cwkHvMczM7NTygQv-f7fGOZOTmUgVuGKcwEwLyXL-RE2s2Vz344_l1yL5MME%0D%0AnNUv9aU7F_GaqFt9LHz9CuQWvtJSvrLVivwkL5iVdvOIBv1W_CYkalDXGafhnfC4vA%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYvz1tOd3Xk7b1tj0bB4ONFkVgg3cBCn5gZEdHncXaUygGRgzUnNXUV4ermu2mei3qonQbrgHjt%0D%0AtJh62FVPcPuTB1zICg11PZEf7hFmSeRe3onC7umBV7JyYMHk6TqLKqiguoQHmgintIgXSx_FLqpJ%0D%0Am8tBQYJPeqK34mTdKLzxpZeh0LVwFcD8ErOTAVCiecCF%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYk1wTjw9MkCpuWbMz5AtyhluHCGkEMWX39Xc8acnu1ydNNn25UjmUqj759L_b0RXlFMfnAYLXh%0D%0Ao023npNTRYyVJg04i-kCBGUq59lRWItBsV6HMYdMZRHp3FixQM8XaK_MBNak0Nb1vetQ7j-wZ_NI%0D%0Asq0L-QIU0atNKyzqeHT0OsMUlJjoULRjwp43mF0nu-VRpU-yfConbjH7ZBgXZgVmEA%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top 250</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Top Movies</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li>    <a href="/offsite/?page-action=ft_ad&token=BCYpX04x1wwIOAlQAk096Krr-lbLNPyhMkuVlKtvIDDXACW_hnzjs8dQDMXQrR9IBuIu4f_GF_Rd%0D%0AfTPQ1vJ3OQB1WBrFA5IJw7mo7ACO1VkZKVphkUdnjSoyfwwt3cbQduCl5fhjA_mAQeRgg0v4xyWS%0D%0AnkOGzAnEB0-LCQmhPcP8ORpIMg0YpMZSnfRJf-X3Ofca9cEcVdGRXaLBL2VxhQP2ehexDCdxWAt6%0D%0AVmEg-7_VwGTYPErHNKurKxXwyBZckTJN6pBUBsTjMZKeb6tgUjj2oQ%0D%0A&ref_=ft_ad"
itemprop='url'>Advertising</a>
</li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYl7lo5_PkBjassQnztja0uY5k0-CFUIP5GgVh7_hrN5IW4VFQBfv9qaTaMR86JXNh4qx7LhyTd%0D%0A5x5XFD84CFzSMAKmnC5rRhRAbfHyiwbyysvZHb4rjgX4wMxOATeppqiGHTpvc8pID7HJ4z2DNdos%0D%0AhSCWComIEeFdldKhPikEdWdTNRQuEykAIe9sfX8GqS_eHkBaxuK2DlrdURbd4FgOPA%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYupKN9esux5h9cgWlhYwigfl6hDAfN1QnWM9NdUxw9Z3T-SsQnpd21osrUAon85RFbMQabIS44%0D%0A5-xV_IAS1whuLXIoTbj9LS4Yw163Olw4S73X6-yoeAYsOQsNc2VRLPOqT_ymTVd1aGv-JrRy1bsB%0D%0AVbL3uazif5PUyOi8zRKBB4t9yHd1FY120gqMxC2w8X79%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYoaR4jjwiotvG_EyxC315bLlzv5D5qkeGvK7WwdwEMGkai53qiil3sH3EdQNEnn4JRTq0cKDxR%0D%0AH80ZcB3n-e-4LJ7qoyc7Ec3kluZK3cp9DvboOhB996XAVyO8xdcONlQNySPUi3ziEXA35oZnVoAj%0D%0A3LEYNvcXMZQkTamtntQjptlA48zpvgdZ0truT-vrNIlTvSc_z6d6MHrsjjxMYhK0x21YfxTvl-Gk%0D%0A4DGOH6FZZJsn6_1-t4DFkkxp_ggyKwlK%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/show_article?conditions';"
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2015
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/';"
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYj-NCmrFNMl-42yS76nxEOqLXugLEOZ_NTJnyiFmI0toxAsCAgYiAzpqJwwymHn5CvuFZ5eGXY%0D%0AKKrDwJXC7_ylX8DT772iV16QyPGxdHEo9U_k6kAESrTtkzNXsjny5DNy8OmRS7ZuBxkL9jwa2JFu%0D%0A8KJ79Ja2u8SHKwoBzNT79VdnhTydWCc5aj-wOsXyel6sQKszR4nxHa7Gml3hUbOdoPY-n2ecP4Mq%0D%0AF2S2wrOnwVM%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYv5OiquoUXz8TIyY5cskGtpeoMYb9c6x6Hack9iQwt0-aZ0cgwkCnuW0mPfwCHzL-UmNvDC3AU%0D%0AQ8lPrPSxx7sVeUJzizTYBlSH27T3ReBfzvs5q2VUWIpNJRycTRoGlGkXsOOyL23ZH8zy46sqk8Uc%0D%0AIbyGVGxKO09gt2CkgUQBGXUx_hpaAioybp5z_EaiERZuq2B4tWVEqAsF2iiDyLBEMDi6QcWE-SSV%0D%0Anj09-HzRHxg%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYt0uFq7CyJYBQRa9kulMPw6XUzNfT5vhh5NdxbddyT2QNMa7phuK2Vcv9xPau1ptByWFESSgBS%0D%0AN5aAEB49Ze-hO_RWfFC7MSLORGA9APtySvbm6pg9TLckfKtYusxpUxgsF7vImiPzzW9M4Sk7V4JB%0D%0AV4XlqubUprBz4oYkSAI6wJiQclyVZZ8aHY7fc5qR3fhxoQGZ6agkwSURhFsRHnFzHK4VoIPzn4y_%0D%0AKSA2LaVTz_I%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYghUyqpXKiOzpOXieJ9zvqcm4Qm_iipqIveHS7tTEFjNtit4GWCNGyS90epp6Mw01A8nUKbg1g%0D%0AumoVyj2KCo2hJQ_8--qxJjLiWzggqxpYpud1OVQL-2Oo-jraM_EaJkZycnLnYEaXkQ5rdRKY1654%0D%0AvjlhRGxQ8FoZnYuS8h8hDX1oQbNHkLIBPImxVUzP5RmSy2C_Md42sGpY6HR9O7Y6rSLy1IrllVBa%0D%0AMWSoVvn1CtA%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYqQ7fMDM8Rq2HU6beuwxwyCFFT1YvBWuCk0oJVDQlsqjjQAkfKLG60dAhESOUUk0YSe4HPBuWD%0D%0AT2LRle07baySJnB039Q8F5un3u07iQ5zAZfxqnmGRHoU5chU981DLbAeGSFnyPxtmzKkA_EoSnGL%0D%0AiHUruxLXccqmOdXBdTjVHLXNjGnPBpUkuE4VDAnI3PkVPzCrCGlDeXM343IuPz5FFmlmTYipHYyf%0D%0ABeeGjW4chH8%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYnqk56FxjkdzFGwOImEnFrzsF29Kh8uSukSDfheUsjl6qDGTMHTE6rBCzH1Fv9_OjsTLUmBrlv%0D%0Auzdsuap0Qq_-ga4MuBQhLpCc3A79jyt5HIZmyPFD3Y96SWpZhach10_iSQnlXHHZ7RhMHimWKDOi%0D%0ADDFKj3JH-7hN2vPbLMYYPgDH5bxQEB4OXbJoTyF2HS2LGRPVvgjzP96sSniIZoAnDMEfLJp_CHZH%0D%0Amh3P7Vd5bWQRL-lHZHRgrhhBSYe_zo7W%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYl7RXPTgcJDRSMpC8g-FO3AlJfbqb0h5Aqg_DoVWT30QB9Ol0dTc4_M9fG6gMWksWxykurcNLD%0D%0AFfjNL76hW0akBXLDIGSewEqpz49GklRCBAUQqKnUm1Z0FOPcz0g8Ndl_W7iaZCcKk45ES9HYP6fi%0D%0AIfYPbsg69g6mMgXnzTfTAiA%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYoSm_LrH1kn_aitzpWjRpPIJnI73Ywo1cxqsdSDT3bE-lY69HyGOKIiols1-ZMxxtUAhJEZN4x%0D%0Av6rc4x8s0Q_eiUBKHbCmNAFjG8FCYh9F3Wftu_WfB1-arBjgytSr0cSXOBBdUsh67Sbb70wZrfN5%0D%0AITo4Q73WL8vPzoJ51ULLeuk%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/common-1055245832._CB292594158_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-2902142289._CB292800893_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-1705459340._CB292800891_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-3920146857._CB292796746_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-3229803205._CB292800882_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=0101ff9fac2b86ce5dbbef8fe7b019f0263b5564d689d2a53aadda4adb994dd7f7fd",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=626184888514"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-232398347._CB349580400_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=626184888514&ord=626184888514";
    },"unable to request AAN pixel");
</script>

<div id="flashContent" style="width: 0px; height: 0px; overflow:hidden;">
    <script type="text/javascript">
        if (generic && generic.monitoring.record_metric && flashAdUtils.canPlayFlash) {
            var flashLoaded = 0;
            function swfLoaded() {
                flashLoaded = 1;
                generic.monitoring.record_metric('ads_flash_did_play', 1, true);
            }
            window.onload = function() {
                // Flash was not loaded
                if (flashLoaded === 0) {
                    // Flash could be loaded but wasn't
                    if (flashAdUtils.canPlayFlash()) {
                        generic.monitoring.record_metric('ads_flash_can_but_did_not_play', 0, true);
                    } else {
                        generic.monitoring.record_metric('ads_flash_cannot_play', 0, true);
                    }
                }
            }
            /**
             * There is a rare possibility of window.onload getting called before swfLoaded.
             * Firing this metric to keep track of such calls.
             */
             generic.monitoring.record_metric('ads_flash_page_loaded', 1, true);
         }
    </script>
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="1x1" align="middle">
        <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
        <param name="quality" value="high" />
        <param name="play" value="true" />
        <param name="loop" value="true" />
        <param name="wmode" value="transparent" />
        <param name="scale" value="showall" />
        <param name="menu" value="true" />
        <param name="devicefont" value="false" />
        <param name="salign" value="" />
        <param name="allowScriptAccess" value="always" />

        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" width="1" height="1">
            <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
            <param name="quality" value="high" />
            <param name="play" value="true" />
            <param name="loop" value="true" />
            <param name="wmode" value="transparent" />
            <param name="scale" value="showall" />
            <param name="menu" value="true" />
            <param name="devicefont" value="false" />
            <param name="salign" value="" />
            <param name="allowScriptAccess" value="always" />
        </object>
        <!--<![endif]-->
    </object>
</div>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="814"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
