<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-773a99c.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-773a99c.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-773a99c.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-773a99c.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kat.cr/sc-773a99c.js');

        sc('setHost', 'a.kat.cr');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: '773a99c',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-773a99c.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<div id="_60318cd4e8d28f6fb76fe34e9bd9c498"></div>
<div id="_39ecb76dd457e5ac33776fdf11500d56"></div>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka">e</i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka">g</i></button></div>
			</form>
		</div>
        <div id="_277923e5f9d753c5b0630c28e641790c"></div>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080p/" class="tag6">1080p</a>
	<a href="/search/2015/" class="tag10">2015</a>
	<a href="/search/3d/" class="tag3">3d</a>
	<a href="/search/android/" class="tag2">android</a>
	<a href="/search/ant%20man/" class="tag2">ant man</a>
	<a href="/search/discography/" class="tag3">discography</a>
	<a href="/search/drake%20time%20to%20be%20alive/" class="tag2">drake time to be alive</a>
	<a href="/search/empire/" class="tag4">empire</a>
	<a href="/search/empire%20s02e01/" class="tag3">empire s02e01</a>
	<a href="/search/everest/" class="tag2">everest</a>
	<a href="/search/fear%20the%20walking%20dead/" class="tag3">fear the walking dead</a>
	<a href="/search/fifa%2016/" class="tag2">fifa 16</a>
	<a href="/search/flac/" class="tag2">flac</a>
	<a href="/search/french/" class="tag3">french</a>
	<a href="/search/gotham/" class="tag2">gotham</a>
	<a href="/search/hindi/" class="tag9">hindi</a>
	<a href="/search/hindi%202015/" class="tag5">hindi 2015</a>
	<a href="/search/inside%20out/" class="tag2">inside out</a>
	<a href="/search/ita/" class="tag2">ita</a>
	<a href="/search/juego%20de%20tronos%202/" class="tag3">juego de tronos 2</a>
	<a href="/search/kat%20ph%20com/" class="tag6">kat ph com</a>
	<a href="/search/katti%20batti/" class="tag2">katti batti</a>
	<a href="/search/limitless/" class="tag2">limitless</a>
	<a href="/search/mad%20max/" class="tag3">mad max</a>
	<a href="/search/malayalam/" class="tag3">malayalam</a>
	<a href="/search/mission%20impossible/" class="tag2">mission impossible</a>
	<a href="/search/mission%20impossible%20rogue%20nation/" class="tag2">mission impossible rogue nation</a>
	<a href="/search/modern%20family/" class="tag2">modern family</a>
	<a href="/search/modern%20family%20s07e01/" class="tag2">modern family s07e01</a>
	<a href="/search/movies/" class="tag2">movies</a>
	<a href="/search/narcos/" class="tag2">narcos</a>
	<a href="/search/nezu/" class="tag10">nezu</a>
	<a href="/search/nl/" class="tag2">nl</a>
	<a href="/search/pixels/" class="tag2">pixels</a>
	<a href="/search/ripsalot/" class="tag5">ripsalot</a>
	<a href="/search/scream%20queens/" class="tag2">scream queens</a>
	<a href="/search/south%20park/" class="tag4">south park</a>
	<a href="/search/south%20park%20s19e02/" class="tag2">south park s19e02</a>
	<a href="/search/survivor/" class="tag2">survivor</a>
	<a href="/search/tamil/" class="tag5">tamil</a>
	<a href="/search/telugu/" class="tag4">telugu</a>
	<a href="/search/telugu%202015/" class="tag2">telugu 2015</a>
	<a href="/search/the%20visit/" class="tag2">the visit</a>
	<a href="/search/the%20walking%20dead/" class="tag2">the walking dead</a>
	<a href="/search/tomorrowland/" class="tag2">tomorrowland</a>
	<a href="/search/yify/" class="tag10">yify</a>
	<a href="/search/yify%201080p/" class="tag4">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag4">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11300779,0" class="icommentjs icon16" href="/tomorrowland-2015-720p-brrip-x264-yify-t11300779.html#comment"> <em class="iconvalue">194</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/tomorrowland-2015-720p-brrip-x264-yify-t11300779.html" class="cellMainLink">Tomorrowland (2015) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="914684200">872.31 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="22 Sep 2015, 16:26">3&nbsp;days</span></td>
			<td class="green center">24760</td>
			<td class="red lasttd center">14774</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11307505,0" class="icommentjs icon16" href="/mission-impossible-rogue-nation-2015-hdrip-xvid-ac3-evo-t11307505.html#comment"> <em class="iconvalue">143</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/mission-impossible-rogue-nation-2015-hdrip-xvid-ac3-evo-t11307505.html" class="cellMainLink">Mission Impossible Rogue Nation 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1548288970">1.44 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="23 Sep 2015, 21:51">2&nbsp;days</span></td>
			<td class="green center">11963</td>
			<td class="red lasttd center">11659</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11298258,0" class="icommentjs icon16" href="/me-and-earl-and-the-dying-girl-2015-1080p-brrip-x264-yify-t11298258.html#comment"> <em class="iconvalue">62</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/me-and-earl-and-the-dying-girl-2015-1080p-brrip-x264-yify-t11298258.html" class="cellMainLink">Me and Earl and the Dying Girl (2015) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="1758365755">1.64 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="22 Sep 2015, 05:06">3&nbsp;days</span></td>
			<td class="green center">10367</td>
			<td class="red lasttd center">6464</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11304935,0" class="icommentjs icon16" href="/maze-runner-the-scorch-trials-2015-hd-ts-xvid-ac3-hq-hive-cm8-t11304935.html#comment"> <em class="iconvalue">98</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/maze-runner-the-scorch-trials-2015-hd-ts-xvid-ac3-hq-hive-cm8-t11304935.html" class="cellMainLink">Maze Runner-The Scorch Trials 2015 HD-TS XVID AC3 HQ Hive-CM8</a></div>
			</td>
			<td class="nobr center" data-sort="1946160859">1.81 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="23 Sep 2015, 12:44">2&nbsp;days</span></td>
			<td class="green center">6706</td>
			<td class="red lasttd center">8410</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11312756,0" class="icommentjs icon16" href="/san-andreas-2015-720p-brrip-x264-yify-t11312756.html#comment"> <em class="iconvalue">52</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/san-andreas-2015-720p-brrip-x264-yify-t11312756.html" class="cellMainLink">San Andreas (2015) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="854987861">815.38 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="24 Sep 2015, 22:41">1&nbsp;day</span></td>
			<td class="green center">6000</td>
			<td class="red lasttd center">8796</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11304106,0" class="icommentjs icon16" href="/magic-mike-xxl-2015-720p-brrip-x264-yify-t11304106.html#comment"> <em class="iconvalue">39</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/magic-mike-xxl-2015-720p-brrip-x264-yify-t11304106.html" class="cellMainLink">Magic Mike XXL (2015) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="854521732">814.94 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="23 Sep 2015, 08:10">2&nbsp;days</span></td>
			<td class="green center">7384</td>
			<td class="red lasttd center">5778</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11305976,0" class="icommentjs icon16" href="/navy-seals-vs-zombies-2015-bdrip-xvid-ac3-evo-t11305976.html#comment"> <em class="iconvalue">64</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/navy-seals-vs-zombies-2015-bdrip-xvid-ac3-evo-t11305976.html" class="cellMainLink">Navy Seals vs Zombies 2015 BDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1535001666">1.43 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="23 Sep 2015, 16:21">2&nbsp;days</span></td>
			<td class="green center">4355</td>
			<td class="red lasttd center">4941</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11299151,0" class="icommentjs icon16" href="/the-messenger-2015-hdrip-xvid-ac3-evo-t11299151.html#comment"> <em class="iconvalue">33</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/the-messenger-2015-hdrip-xvid-ac3-evo-t11299151.html" class="cellMainLink">The Messenger 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1512399261">1.41 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="22 Sep 2015, 10:13">3&nbsp;days</span></td>
			<td class="green center">4045</td>
			<td class="red lasttd center">3459</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11304167,0" class="icommentjs icon16" href="/war-pigs-2015-1080p-brrip-x264-yify-t11304167.html#comment"> <em class="iconvalue">26</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/war-pigs-2015-1080p-brrip-x264-yify-t11304167.html" class="cellMainLink">War Pigs (2015) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="1541415327">1.44 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="23 Sep 2015, 08:32">2&nbsp;days</span></td>
			<td class="green center">3987</td>
			<td class="red lasttd center">2735</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11299578,0" class="icommentjs icon16" href="/hidden-2015-hdrip-xvid-etrg-t11299578.html#comment"> <em class="iconvalue">75</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/hidden-2015-hdrip-xvid-etrg-t11299578.html" class="cellMainLink">Hidden 2015 HDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="739297533">705.05 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="22 Sep 2015, 12:20">3&nbsp;days</span></td>
			<td class="green center">3376</td>
			<td class="red lasttd center">2229</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11308701,0" class="icommentjs icon16" href="/she-s-funny-that-way-2014-720p-brrip-x264-yify-t11308701.html#comment"> <em class="iconvalue">16</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/she-s-funny-that-way-2014-720p-brrip-x264-yify-t11308701.html" class="cellMainLink">She&#039;s Funny That Way (2014) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="788946796">752.4 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="24 Sep 2015, 04:55">1&nbsp;day</span></td>
			<td class="green center">3063</td>
			<td class="red lasttd center">2016</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11304152,0" class="icommentjs icon16" href="/every-thing-will-be-fine-2015-1080p-brrip-x264-yify-t11304152.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/every-thing-will-be-fine-2015-1080p-brrip-x264-yify-t11304152.html" class="cellMainLink">Every Thing Will Be Fine (2015) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="1976045044">1.84 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="23 Sep 2015, 08:28">2&nbsp;days</span></td>
			<td class="green center">1844</td>
			<td class="red lasttd center">1607</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11298624,0" class="icommentjs icon16" href="/sword-of-vengeance-2015-1080p-brrip-x264-dts-jyk-t11298624.html#comment"> <em class="iconvalue">24</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/sword-of-vengeance-2015-1080p-brrip-x264-dts-jyk-t11298624.html" class="cellMainLink">Sword of Vengeance 2015 1080p BRRip x264 DTS-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="2348903057">2.19 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="22 Sep 2015, 06:53">3&nbsp;days</span></td>
			<td class="green center">1603</td>
			<td class="red lasttd center">1413</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11301768,0" class="icommentjs icon16" href="/the-goob-2014-720p-brrip-x264-yify-t11301768.html#comment"> <em class="iconvalue">22</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/the-goob-2014-720p-brrip-x264-yify-t11301768.html" class="cellMainLink">The Goob (2014) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="728573137">694.82 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="22 Sep 2015, 18:55">3&nbsp;days</span></td>
			<td class="green center">1714</td>
			<td class="red lasttd center">679</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11309328,0" class="icommentjs icon16" href="/ted-2-2015-1080p-web-dl-x264-ac3-jyk-t11309328.html#comment"> <em class="iconvalue">14</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/ted-2-2015-1080p-web-dl-x264-ac3-jyk-t11309328.html" class="cellMainLink">Ted 2 2015 1080p WEB-DL x264 AC3-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="3218854847">3 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="24 Sep 2015, 07:41">1&nbsp;day</span></td>
			<td class="green center">1278</td>
			<td class="red lasttd center">643</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11308176,0" class="icommentjs icon16" href="/empire-2015-s02e01-hdtv-x264-killers-ettv-t11308176.html#comment"> <em class="iconvalue">80</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/empire-2015-s02e01-hdtv-x264-killers-ettv-t11308176.html" class="cellMainLink">Empire 2015 S02E01 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="412569307">393.46 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="24 Sep 2015, 02:02">1&nbsp;day</span></td>
			<td class="green center">7007</td>
			<td class="red lasttd center">3227</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11308305,0" class="icommentjs icon16" href="/south-park-s19e02-hdtv-x264-killers-ettv-t11308305.html#comment"> <em class="iconvalue">51</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/south-park-s19e02-hdtv-x264-killers-ettv-t11308305.html" class="cellMainLink">South Park S19E02 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="97045201">92.55 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="24 Sep 2015, 02:37">1&nbsp;day</span></td>
			<td class="green center">6972</td>
			<td class="red lasttd center">650</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11302838,0" class="icommentjs icon16" href="/scream-queens-2015-s01e01-hdtv-x264-killers-ettv-t11302838.html#comment"> <em class="iconvalue">77</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/scream-queens-2015-s01e01-hdtv-x264-killers-ettv-t11302838.html" class="cellMainLink">Scream Queens 2015 S01E01 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="385290730">367.44 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="23 Sep 2015, 01:37">2&nbsp;days</span></td>
			<td class="green center">6471</td>
			<td class="red lasttd center">1183</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11297482,0" class="icommentjs icon16" href="/gotham-s02e01-hdtv-x264-lol-ettv-t11297482.html#comment"> <em class="iconvalue">151</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/gotham-s02e01-hdtv-x264-lol-ettv-t11297482.html" class="cellMainLink">Gotham S02E01 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="269222943">256.75 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="22 Sep 2015, 00:59">3&nbsp;days</span></td>
			<td class="green center">5243</td>
			<td class="red lasttd center">1647</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11297704,0" class="icommentjs icon16" href="/blindspot-s01e01-hdtv-x264-lol-ettv-t11297704.html#comment"> <em class="iconvalue">116</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/blindspot-s01e01-hdtv-x264-lol-ettv-t11297704.html" class="cellMainLink">Blindspot S01E01 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="278301743">265.41 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="22 Sep 2015, 02:02">3&nbsp;days</span></td>
			<td class="green center">4795</td>
			<td class="red lasttd center">1497</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11308109,0" class="icommentjs icon16" href="/modern-family-s07e01-hdtv-x264-batv-ettv-t11308109.html#comment"> <em class="iconvalue">42</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/modern-family-s07e01-hdtv-x264-batv-ettv-t11308109.html" class="cellMainLink">Modern Family S07E01 HDTV x264-BATV[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="233715085">222.89 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="24 Sep 2015, 01:36">1&nbsp;day</span></td>
			<td class="green center">4346</td>
			<td class="red lasttd center">1922</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11297744,0" class="icommentjs icon16" href="/minority-report-s01e01-hdtv-x264-killers-ettv-t11297744.html#comment"> <em class="iconvalue">106</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/minority-report-s01e01-hdtv-x264-killers-ettv-t11297744.html" class="cellMainLink">Minority Report S01E01 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="445145314">424.52 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="22 Sep 2015, 02:14">3&nbsp;days</span></td>
			<td class="green center">4964</td>
			<td class="red lasttd center">590</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11297698,0" class="icommentjs icon16" href="/scorpion-s02e01-hdtv-x264-lol-ettv-t11297698.html#comment"> <em class="iconvalue">91</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/scorpion-s02e01-hdtv-x264-lol-ettv-t11297698.html" class="cellMainLink">Scorpion S02E01 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="326036376">310.93 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="22 Sep 2015, 01:58">3&nbsp;days</span></td>
			<td class="green center">4003</td>
			<td class="red lasttd center">1218</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11303211,0" class="icommentjs icon16" href="/the-bastard-executioner-s01e03-hdtv-x264-batv-ettv-t11303211.html#comment"> <em class="iconvalue">60</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/the-bastard-executioner-s01e03-hdtv-x264-batv-ettv-t11303211.html" class="cellMainLink">The Bastard Executioner S01E03 HDTV x264-BATV[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="539221863">514.24 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="23 Sep 2015, 03:29">2&nbsp;days</span></td>
			<td class="green center">4081</td>
			<td class="red lasttd center">747</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11308461,0" class="icommentjs icon16" href="/survivor-s31e01-hdtv-x264-uav-ettv-t11308461.html#comment"> <em class="iconvalue">35</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/survivor-s31e01-hdtv-x264-uav-ettv-t11308461.html" class="cellMainLink">Survivor S31E01 HDTV x264-UAV[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="903012730">861.18 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="24 Sep 2015, 03:24">1&nbsp;day</span></td>
			<td class="green center">3463</td>
			<td class="red lasttd center">996</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11313491,0" class="icommentjs icon16" href="/how-to-get-away-with-murder-s02e01-hdtv-x264-lol-rartv-t11313491.html#comment"> <em class="iconvalue">50</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/how-to-get-away-with-murder-s02e01-hdtv-x264-lol-rartv-t11313491.html" class="cellMainLink">How to Get Away with Murder S02E01 HDTV x264-LOL[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="260372990">248.31 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="25 Sep 2015, 02:55">20&nbsp;hours</span></td>
			<td class="green center">2152</td>
			<td class="red lasttd center">2520</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11313286,0" class="icommentjs icon16" href="/greys-anatomy-s12e01-hdtv-x264-killers-rartv-t11313286.html#comment"> <em class="iconvalue">21</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/greys-anatomy-s12e01-hdtv-x264-killers-rartv-t11313286.html" class="cellMainLink">Greys Anatomy S12E01 HDTV x264-KILLERS[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="315426145">300.81 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="25 Sep 2015, 01:55">21&nbsp;hours</span></td>
			<td class="green center">2413</td>
			<td class="red lasttd center">1837</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11297420,0" class="icommentjs icon16" href="/the-muppets-s01e01-hdtv-x264-killers-ettv-t11297420.html#comment"> <em class="iconvalue">71</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/the-muppets-s01e01-hdtv-x264-killers-ettv-t11297420.html" class="cellMainLink">The Muppets S01E01 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="218826347">208.69 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="22 Sep 2015, 00:38">3&nbsp;days</span></td>
			<td class="green center">2833</td>
			<td class="red lasttd center">701</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11297469,0" class="icommentjs icon16" href="/castle-2009-s08e01-hdtv-x264-lol-ettv-t11297469.html#comment"> <em class="iconvalue">63</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/castle-2009-s08e01-hdtv-x264-lol-ettv-t11297469.html" class="cellMainLink">Castle 2009 S08E01 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="280160931">267.18 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="22 Sep 2015, 00:55">3&nbsp;days</span></td>
			<td class="green center">2745</td>
			<td class="red lasttd center">810</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11313516,0" class="icommentjs icon16" href="/the-player-2015-s01e01-hdtv-x264-lol-ettv-t11313516.html#comment"> <em class="iconvalue">32</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/the-player-2015-s01e01-hdtv-x264-lol-ettv-t11313516.html" class="cellMainLink">The Player 2015 S01E01 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="312445583">297.97 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="25 Sep 2015, 03:01">20&nbsp;hours</span></td>
			<td class="green center">1647</td>
			<td class="red lasttd center">2629</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11307693,0" class="icommentjs icon16" href="/mp3-new-releases-2015-week-38-amazeballz-glodls-t11307693.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/mp3-new-releases-2015-week-38-amazeballz-glodls-t11307693.html" class="cellMainLink">MP3 NEW RELEASES 2015 WEEK 38 - AMAZEBALLZ [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="3339252204">3.11 <span>GB</span></td>
			<td class="center">466</td>
			<td class="center"><span title="23 Sep 2015, 23:13">2&nbsp;days</span></td>
			<td class="green center">429</td>
			<td class="red lasttd center">321</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11305316,0" class="icommentjs icon16" href="/va-ultimate-pop-4cd-2015-mp3-320kbps-h4ckus-glodls-t11305316.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/va-ultimate-pop-4cd-2015-mp3-320kbps-h4ckus-glodls-t11305316.html" class="cellMainLink">VA - Ultimate Pop [4CD] [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="557583083">531.75 <span>MB</span></td>
			<td class="center">74</td>
			<td class="center"><span title="23 Sep 2015, 13:36">2&nbsp;days</span></td>
			<td class="green center">352</td>
			<td class="red lasttd center">132</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11301323,0" class="icommentjs icon16" href="/b-b-king-lonely-and-blue-2015-mp3-320kbps-h4ckus-glodls-t11301323.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/b-b-king-lonely-and-blue-2015-mp3-320kbps-h4ckus-glodls-t11301323.html" class="cellMainLink">B.B. King - Lonely And Blue [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="340026737">324.27 <span>MB</span></td>
			<td class="center">52</td>
			<td class="center"><span title="22 Sep 2015, 17:30">3&nbsp;days</span></td>
			<td class="green center">341</td>
			<td class="red lasttd center">61</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11298979,0" class="icommentjs icon16" href="/va-guitar-gods-of-80-s-and-90-s-2007-320ak-t11298979.html#comment"> <em class="iconvalue">21</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/va-guitar-gods-of-80-s-and-90-s-2007-320ak-t11298979.html" class="cellMainLink">VA - Guitar Gods Of 80&#039;s and 90&#039;s 2007 320ak</a></div>
			</td>
			<td class="nobr center" data-sort="362375417">345.59 <span>MB</span></td>
			<td class="center">34</td>
			<td class="center"><span title="22 Sep 2015, 09:04">3&nbsp;days</span></td>
			<td class="green center">339</td>
			<td class="red lasttd center">47</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11310889,0" class="icommentjs icon16" href="/disclosure-caracal-deluxe-edition-320-kbps-edm-enigmaticabhi-t11310889.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/disclosure-caracal-deluxe-edition-320-kbps-edm-enigmaticabhi-t11310889.html" class="cellMainLink">Disclosure - Caracal (Deluxe Edition) [320 KBPS] [EDM] *EnigmaticAbhi*</a></div>
			</td>
			<td class="nobr center" data-sort="162018959">154.51 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span title="24 Sep 2015, 14:47">1&nbsp;day</span></td>
			<td class="green center">273</td>
			<td class="red lasttd center">102</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11304110,0" class="icommentjs icon16" href="/va-ministry-of-sound-vegas-sessions-2015-mp3-320kbps-h4ckus-glodls-t11304110.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/va-ministry-of-sound-vegas-sessions-2015-mp3-320kbps-h4ckus-glodls-t11304110.html" class="cellMainLink">VA - Ministry of Sound : Vegas Sessions [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="388411058">370.42 <span>MB</span></td>
			<td class="center">36</td>
			<td class="center"><span title="23 Sep 2015, 08:13">2&nbsp;days</span></td>
			<td class="green center">249</td>
			<td class="red lasttd center">52</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11299573,0" class="icommentjs icon16" href="/new-order-music-complete-chattchitto-rg-t11299573.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/new-order-music-complete-chattchitto-rg-t11299573.html" class="cellMainLink">New Order - Music Complete [ChattChitto RG]</a></div>
			</td>
			<td class="nobr center" data-sort="175130749">167.02 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span title="22 Sep 2015, 12:19">3&nbsp;days</span></td>
			<td class="green center">259</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11305493,0" class="icommentjs icon16" href="/ugly-kid-joe-uglier-than-they-used-ta-be-2015-320ak-t11305493.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/ugly-kid-joe-uglier-than-they-used-ta-be-2015-320ak-t11305493.html" class="cellMainLink">Ugly Kid Joe - Uglier Than They Used Ta Be 2015 320ak</a></div>
			</td>
			<td class="nobr center" data-sort="117197461">111.77 <span>MB</span></td>
			<td class="center">11</td>
			<td class="center"><span title="23 Sep 2015, 14:16">2&nbsp;days</span></td>
			<td class="green center">248</td>
			<td class="red lasttd center">31</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11298907,0" class="icommentjs icon16" href="/silversun-pickups-better-nature-2015-320-kbps-freak37-t11298907.html#comment"> <em class="iconvalue">14</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/silversun-pickups-better-nature-2015-320-kbps-freak37-t11298907.html" class="cellMainLink">Silversun Pickups â Better Nature (2015) 320 KBPS - Freak37</a></div>
			</td>
			<td class="nobr center" data-sort="123398982">117.68 <span>MB</span></td>
			<td class="center">11</td>
			<td class="center"><span title="22 Sep 2015, 08:38">3&nbsp;days</span></td>
			<td class="green center">208</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11301407,0" class="icommentjs icon16" href="/alabama-southern-drawl-2015-mp3-320kbps-freak37-t11301407.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/alabama-southern-drawl-2015-mp3-320kbps-freak37-t11301407.html" class="cellMainLink">Alabama - Southern Drawl (2015) [ MP3 320kbps ] - Freak37</a></div>
			</td>
			<td class="nobr center" data-sort="129950453">123.93 <span>MB</span></td>
			<td class="center">14</td>
			<td class="center"><span title="22 Sep 2015, 17:51">3&nbsp;days</span></td>
			<td class="green center">180</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11311928,0" class="icommentjs icon16" href="/fetty-wap-fetty-wap-deluxe-explicit-2015-mp3-album-vbuc-t11311928.html#comment"> <em class="iconvalue">27</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/fetty-wap-fetty-wap-deluxe-explicit-2015-mp3-album-vbuc-t11311928.html" class="cellMainLink">Fetty Wap - Fetty Wap (Deluxe) [Explicit] 2015 {MP3 Album}~{VBUc}</a></div>
			</td>
			<td class="nobr center" data-sort="189336474">180.57 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center"><span title="24 Sep 2015, 18:16">1&nbsp;day</span></td>
			<td class="green center">160</td>
			<td class="red lasttd center">47</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11310100,0" class="icommentjs icon16" href="/eagles-of-death-metal-zipper-down-2015-mp3-320kpbs-freak37-t11310100.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/eagles-of-death-metal-zipper-down-2015-mp3-320kpbs-freak37-t11310100.html" class="cellMainLink">Eagles Of Death Metal â Zipper Down (2015) [ mp3 320kpbs ] - Freak37</a></div>
			</td>
			<td class="nobr center" data-sort="108731114">103.69 <span>MB</span></td>
			<td class="center">12</td>
			<td class="center"><span title="24 Sep 2015, 11:29">1&nbsp;day</span></td>
			<td class="green center">132</td>
			<td class="red lasttd center">25</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11301448,0" class="icommentjs icon16" href="/howard-stern-show-wrap-up-09-22-15-t11301448.html#comment"> <em class="iconvalue">17</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/howard-stern-show-wrap-up-09-22-15-t11301448.html" class="cellMainLink">Howard Stern Show + Wrap Up 09-22-15</a></div>
			</td>
			<td class="nobr center" data-sort="267934666">255.52 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="22 Sep 2015, 18:02">3&nbsp;days</span></td>
			<td class="green center">124</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11298853,0" class="icommentjs icon16" href="/latest-malayalam-movie-songs-320kbps-sep-2015-kanal-pathemari-life-of-josutty-kohinoor-Î©mega39-t11298853.html#comment"> <em class="iconvalue">14</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/latest-malayalam-movie-songs-320kbps-sep-2015-kanal-pathemari-life-of-josutty-kohinoor-Î©mega39-t11298853.html" class="cellMainLink">Latest Malayalam Movie Songs 320Kbps_[sep 2015]_(Kanal,Pathemari,Life of Josutty,Kohinoor)_Î©mega39</a></div>
			</td>
			<td class="nobr center" data-sort="164968117">157.33 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center"><span title="22 Sep 2015, 08:13">3&nbsp;days</span></td>
			<td class="green center">99</td>
			<td class="red lasttd center">31</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11307119,0" class="icommentjs icon16" href="/los-lobos-gates-of-gold-2015-mp3-320kpbs-freak37-t11307119.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/los-lobos-gates-of-gold-2015-mp3-320kpbs-freak37-t11307119.html" class="cellMainLink">Los Lobos â Gates Of Gold (2015) [ mp3 320kpbs ] - Freak37</a></div>
			</td>
			<td class="nobr center" data-sort="103239231">98.46 <span>MB</span></td>
			<td class="center">12</td>
			<td class="center"><span title="23 Sep 2015, 19:44">2&nbsp;days</span></td>
			<td class="green center">100</td>
			<td class="red lasttd center">14</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11299165,0" class="icommentjs icon16" href="/mad-max-r-g-mechanics-t11299165.html#comment"> <em class="iconvalue">282</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/mad-max-r-g-mechanics-t11299165.html" class="cellMainLink">Mad Max [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center" data-sort="4283230728">3.99 <span>GB</span></td>
			<td class="center">8</td>
			<td class="center"><span title="22 Sep 2015, 10:19">3&nbsp;days</span></td>
			<td class="green center">1550</td>
			<td class="red lasttd center">802</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11299044,0" class="icommentjs icon16" href="/soma-reloaded-t11299044.html#comment"> <em class="iconvalue">107</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/soma-reloaded-t11299044.html" class="cellMainLink">SOMA-RELOADED</a></div>
			</td>
			<td class="nobr center" data-sort="11877323228">11.06 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="22 Sep 2015, 09:32">3&nbsp;days</span></td>
			<td class="green center">1054</td>
			<td class="red lasttd center">1189</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11298886,0" class="icommentjs icon16" href="/blood-bowl-2-codex-t11298886.html#comment"> <em class="iconvalue">44</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/blood-bowl-2-codex-t11298886.html" class="cellMainLink">Blood.Bowl.2-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="4175632014">3.89 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="22 Sep 2015, 08:29">3&nbsp;days</span></td>
			<td class="green center">390</td>
			<td class="red lasttd center">192</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11300700,0" class="icommentjs icon16" href="/fifa-16-xbox360-complex-t11300700.html#comment"> <em class="iconvalue">65</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/fifa-16-xbox360-complex-t11300700.html" class="cellMainLink">FIFA.16.XBOX360-COMPLEX</a></div>
			</td>
			<td class="nobr center" data-sort="8738849184">8.14 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="22 Sep 2015, 16:09">3&nbsp;days</span></td>
			<td class="green center">326</td>
			<td class="red lasttd center">277</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11303272,0" class="icommentjs icon16" href="/wolfenstein-the-old-blood-r-g-mechanics-t11303272.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/wolfenstein-the-old-blood-r-g-mechanics-t11303272.html" class="cellMainLink">Wolfenstein: The Old Blood [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center" data-sort="33520003710">31.22 <span>GB</span></td>
			<td class="center">17</td>
			<td class="center"><span title="23 Sep 2015, 03:52">2&nbsp;days</span></td>
			<td class="green center">277</td>
			<td class="red lasttd center">309</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11313596,0" class="icommentjs icon16" href="/metal-gear-solid-v-the-phantom-pain-v-1-0-0-5-2015-pc-repack-Ð¾Ñ-seyter-t11313596.html#comment"> <em class="iconvalue">58</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/metal-gear-solid-v-the-phantom-pain-v-1-0-0-5-2015-pc-repack-Ð¾Ñ-seyter-t11313596.html" class="cellMainLink">Metal Gear Solid V: The Phantom Pain [v 1.0.0.5] (2015) PC | RePack Ð¾Ñ SEYTER</a></div>
			</td>
			<td class="nobr center" data-sort="12916813534">12.03 <span>GB</span></td>
			<td class="center">62</td>
			<td class="center"><span title="25 Sep 2015, 03:21">20&nbsp;hours</span></td>
			<td class="green center">153</td>
			<td class="red lasttd center">255</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11306515,0" class="icommentjs icon16" href="/nba-2k16-michael-jordan-edition-steam-preload-ali213-t11306515.html#comment"> <em class="iconvalue">36</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/nba-2k16-michael-jordan-edition-steam-preload-ali213-t11306515.html" class="cellMainLink">NBA 2K16 Michael Jordan Edition Steam Preload-ALI213</a></div>
			</td>
			<td class="nobr center" data-sort="42721271600">39.79 <span>GB</span></td>
			<td class="center">42</td>
			<td class="center"><span title="23 Sep 2015, 17:25">2&nbsp;days</span></td>
			<td class="green center">39</td>
			<td class="red lasttd center">408</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11301516,0" class="icommentjs icon16" href="/afro-samurai-2-revenge-of-kuma-volume-one-codex-t11301516.html#comment"> <em class="iconvalue">19</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/afro-samurai-2-revenge-of-kuma-volume-one-codex-t11301516.html" class="cellMainLink">Afro Samurai 2 Revenge of Kuma Volume One-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="1843632098">1.72 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="22 Sep 2015, 18:17">3&nbsp;days</span></td>
			<td class="green center">156</td>
			<td class="red lasttd center">104</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11298318,0" class="icommentjs icon16" href="/terratech-v0-5-12-t11298318.html#comment"> <em class="iconvalue">26</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/terratech-v0-5-12-t11298318.html" class="cellMainLink">TerraTech v0.5.12</a></div>
			</td>
			<td class="nobr center" data-sort="233256426">222.45 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="22 Sep 2015, 05:19">3&nbsp;days</span></td>
			<td class="green center">169</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11311587,0" class="icommentjs icon16" href="/final-fantasy-v-reloaded-t11311587.html#comment"> <em class="iconvalue">23</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/final-fantasy-v-reloaded-t11311587.html" class="cellMainLink">FINAL.FANTASY.V-RELOADED</a></div>
			</td>
			<td class="nobr center" data-sort="1011061208">964.22 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="24 Sep 2015, 16:30">1&nbsp;day</span></td>
			<td class="green center">149</td>
			<td class="red lasttd center">42</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11303306,0" class="icommentjs icon16" href="/men-of-war-assault-squad-2-r-g-mechanics-t11303306.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/men-of-war-assault-squad-2-r-g-mechanics-t11303306.html" class="cellMainLink">Men of War: Assault Squad 2 [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center" data-sort="1598967316">1.49 <span>GB</span></td>
			<td class="center">16</td>
			<td class="center"><span title="23 Sep 2015, 04:08">2&nbsp;days</span></td>
			<td class="green center">138</td>
			<td class="red lasttd center">63</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11302060,0" class="icommentjs icon16" href="/system-shock-enhanced-edition-gog-t11302060.html#comment"> <em class="iconvalue">22</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/system-shock-enhanced-edition-gog-t11302060.html" class="cellMainLink">System Shock: Enhanced Edition (GOG)</a></div>
			</td>
			<td class="nobr center" data-sort="933334512">890.1 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span title="22 Sep 2015, 20:20">3&nbsp;days</span></td>
			<td class="green center">136</td>
			<td class="red lasttd center">37</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11310832,0" class="icommentjs icon16" href="/cities-skylines-after-dark-codex-t11310832.html#comment"> <em class="iconvalue">36</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/cities-skylines-after-dark-codex-t11310832.html" class="cellMainLink">Cities Skylines After Dark-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="2807761861">2.61 <span>GB</span></td>
			<td class="center">59</td>
			<td class="center"><span title="24 Sep 2015, 14:27">1&nbsp;day</span></td>
			<td class="green center">112</td>
			<td class="red lasttd center">78</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11312525,0" class="icommentjs icon16" href="/redemption-cemetery-7-clock-of-fate-ce-2015-pc-final-t11312525.html#comment"> <em class="iconvalue">13</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/redemption-cemetery-7-clock-of-fate-ce-2015-pc-final-t11312525.html" class="cellMainLink">Redemption Cemetery 7: Clock of Fate CE (2015) PC [FINAL]</a></div>
			</td>
			<td class="nobr center" data-sort="1030440350">982.7 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="24 Sep 2015, 21:12">1&nbsp;day</span></td>
			<td class="green center">100</td>
			<td class="red lasttd center">52</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11305501,0" class="icommentjs icon16" href="/assault-android-cactus-skidrow-t11305501.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/assault-android-cactus-skidrow-t11305501.html" class="cellMainLink">Assault.Android.Cactus-SKIDROW</a></div>
			</td>
			<td class="nobr center" data-sort="1062552003">1013.33 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="23 Sep 2015, 14:18">2&nbsp;days</span></td>
			<td class="green center">64</td>
			<td class="red lasttd center">34</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11301993,0" class="icommentjs icon16" href="/microsoft-office-professional-rtm-2016-original-msdn-english-x86-x64-team-os-t11301993.html#comment"> <em class="iconvalue">171</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/microsoft-office-professional-rtm-2016-original-msdn-english-x86-x64-team-os-t11301993.html" class="cellMainLink">Microsoft Office Professional Rtm 2016 Original Msdn English X86-x64-=TEAM OS=-</a></div>
			</td>
			<td class="nobr center" data-sort="2421989478">2.26 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="22 Sep 2015, 19:55">3&nbsp;days</span></td>
			<td class="green center">1143</td>
			<td class="red lasttd center">1086</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11308013,0" class="icommentjs icon16" href="/microsoft-ms-office-2016-pro-plus-rtm-16-0-4266-1003-32-64-bit-ratiborus-3-2-appzdam-t11308013.html#comment"> <em class="iconvalue">29</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-Type"></i>
                    <a href="/microsoft-ms-office-2016-pro-plus-rtm-16-0-4266-1003-32-64-bit-ratiborus-3-2-appzdam-t11308013.html" class="cellMainLink">Microsoft MS Office 2016 Pro Plus RTM 16.0.4266.1003 [32-64 bit] (Ratiborus 3.2) - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="2724702760">2.54 <span>GB</span></td>
			<td class="center">36</td>
			<td class="center"><span title="24 Sep 2015, 00:48">1&nbsp;day</span></td>
			<td class="green center">274</td>
			<td class="red lasttd center">251</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11305756,0" class="icommentjs icon16" href="/internet-download-manager-idm-6-23-build-22-crack-cpul-ctrc-t11305756.html#comment"> <em class="iconvalue">36</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/internet-download-manager-idm-6-23-build-22-crack-cpul-ctrc-t11305756.html" class="cellMainLink">Internet Download Manager (IDM) 6.23 Build 22 + Crack ~ [CPUL][CTRC]</a></div>
			</td>
			<td class="nobr center" data-sort="10598654">10.11 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="23 Sep 2015, 15:22">2&nbsp;days</span></td>
			<td class="green center">275</td>
			<td class="red lasttd center">51</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11304907,0" class="icommentjs icon16" href="/ms-microsoft-office-2016-pro-plus-rtm-16-0-4266-1003-retail-37-languages-iso-32-64-bit-appzdam-t11304907.html#comment"> <em class="iconvalue">42</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/ms-microsoft-office-2016-pro-plus-rtm-16-0-4266-1003-retail-37-languages-iso-32-64-bit-appzdam-t11304907.html" class="cellMainLink">MS (Microsoft) Office 2016 Pro Plus RTM 16.0.4266.1003 (Retail) + 37 Languages ISO [32-64 bit] - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="86104532992">80.19 <span>GB</span></td>
			<td class="center">37</td>
			<td class="center"><span title="23 Sep 2015, 12:36">2&nbsp;days</span></td>
			<td class="green center">33</td>
			<td class="red lasttd center">468</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11306522,0" class="icommentjs icon16" href="/iobit-driver-booster-pro-3-0-3-257-final-2015-frank-t11306522.html#comment"> <em class="iconvalue">33</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/iobit-driver-booster-pro-3-0-3-257-final-2015-frank-t11306522.html" class="cellMainLink">IObit Driver Booster PRO 3.0.3.257 Final(2015)-FRANK</a></div>
			</td>
			<td class="nobr center" data-sort="15292416">14.58 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="23 Sep 2015, 17:26">2&nbsp;days</span></td>
			<td class="green center">182</td>
			<td class="red lasttd center">19</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11310751,0" class="icommentjs icon16" href="/kms-office-activator-2016-ultimate-1-1-appzdam-t11310751.html#comment"> <em class="iconvalue">17</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/kms-office-activator-2016-ultimate-1-1-appzdam-t11310751.html" class="cellMainLink">KMS Office Activator 2016 Ultimate 1.1 - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="16798417">16.02 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="24 Sep 2015, 13:55">1&nbsp;day</span></td>
			<td class="green center">156</td>
			<td class="red lasttd center">20</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11304440,0" class="icommentjs icon16" href="/the-keys-for-eset-kaspersky-avast-dr-web-avira-23-sep-2015-pc-t11304440.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/the-keys-for-eset-kaspersky-avast-dr-web-avira-23-sep-2015-pc-t11304440.html" class="cellMainLink">The keys for ESET, Kaspersky, Avast, Dr.Web, Avira [23 sep] (2015) PC</a></div>
			</td>
			<td class="nobr center" data-sort="3536362">3.37 <span>MB</span></td>
			<td class="center">330</td>
			<td class="center"><span title="23 Sep 2015, 09:57">2&nbsp;days</span></td>
			<td class="green center">118</td>
			<td class="red lasttd center">53</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11305899,0" class="icommentjs icon16" href="/visio-professional-2016-x86-x64-en-us-msdn-t11305899.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/visio-professional-2016-x86-x64-en-us-msdn-t11305899.html" class="cellMainLink">Visio Professional 2016 | x86 x64 | en-us - MSDN</a></div>
			</td>
			<td class="nobr center" data-sort="2421987328">2.26 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="23 Sep 2015, 15:56">2&nbsp;days</span></td>
			<td class="green center">88</td>
			<td class="red lasttd center">50</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11307328,0" class="icommentjs icon16" href="/tubemate-2-2-6-645-apk-adfree-material-design-mod-best-youtube-downloader-osmdroid-t11307328.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/tubemate-2-2-6-645-apk-adfree-material-design-mod-best-youtube-downloader-osmdroid-t11307328.html" class="cellMainLink">TubeMate 2.2.6.645 apk AdFree Material Design Mod (Best YouTube Downloader) {OsmDroid}</a></div>
			</td>
			<td class="nobr center" data-sort="2499984">2.38 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="23 Sep 2015, 20:43">2&nbsp;days</span></td>
			<td class="green center">97</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11301892,0" class="icommentjs icon16" href="/acronis-true-image-bootable-iso-v2016-19-0-5628-multilang-deepstatus-t11301892.html#comment"> <em class="iconvalue">13</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/acronis-true-image-bootable-iso-v2016-19-0-5628-multilang-deepstatus-t11301892.html" class="cellMainLink">Acronis True Image + Bootable ISO v2016 19.0.5628 Multilang [deepstatus]</a></div>
			</td>
			<td class="nobr center" data-sort="919177634">876.6 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="22 Sep 2015, 19:24">3&nbsp;days</span></td>
			<td class="green center">81</td>
			<td class="red lasttd center">25</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11302693,0" class="icommentjs icon16" href="/office-professional-plus-rtm-danish-french-spanish-dutch-x86-x64-2016-part1-by-teamos-whitedeath-t11302693.html#comment"> <em class="iconvalue">17</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/office-professional-plus-rtm-danish-french-spanish-dutch-x86-x64-2016-part1-by-teamos-whitedeath-t11302693.html" class="cellMainLink">Office Professional Plus RTM Danish/French/Spanish/Dutch x86-x64 2016 PART1 by:TeamOS[WhiteDeath]</a></div>
			</td>
			<td class="nobr center" data-sort="11770376294">10.96 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="23 Sep 2015, 00:43">2&nbsp;days</span></td>
			<td class="green center">37</td>
			<td class="red lasttd center">95</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11311802,0" class="icommentjs icon16" href="/ccleaner-5-10-5373-crack-inc-pro-business-addition-by-cpul-ctrc-t11311802.html#comment"> <em class="iconvalue">14</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-exeType"></i>
                    <a href="/ccleaner-5-10-5373-crack-inc-pro-business-addition-by-cpul-ctrc-t11311802.html" class="cellMainLink">CCleaner 5.10.5373-Crack Inc.Pro &amp; Business Addition - By~[CPUL] [CTRC]</a></div>
			</td>
			<td class="nobr center" data-sort="7356594">7.02 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="24 Sep 2015, 17:43">1&nbsp;day</span></td>
			<td class="green center">59</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11304980,0" class="icommentjs icon16" href="/microsoft-visio-office-pro-2016-rtm-16-0-4266-1003-27-languages-iso-32-64-bit-appzdam-t11304980.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/microsoft-visio-office-pro-2016-rtm-16-0-4266-1003-27-languages-iso-32-64-bit-appzdam-t11304980.html" class="cellMainLink">Microsoft Visio Office Pro 2016 RTM 16.0.4266.1003 + 27 Languages ISO [32-64 bit] - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="64007913472">59.61 <span>GB</span></td>
			<td class="center">27</td>
			<td class="center"><span title="23 Sep 2015, 12:54">2&nbsp;days</span></td>
			<td class="green center">2</td>
			<td class="red lasttd center">135</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11305504,0" class="icommentjs icon16" href="/unity-asset-ultimate-game-music-collection-v1-3-akd-t11305504.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-Type"></i>
                    <a href="/unity-asset-ultimate-game-music-collection-v1-3-akd-t11305504.html" class="cellMainLink">Unity Asset - Ultimate Game Music Collection v1.3[AKD]</a></div>
			</td>
			<td class="nobr center" data-sort="2021772595">1.88 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="23 Sep 2015, 14:19">2&nbsp;days</span></td>
			<td class="green center">52</td>
			<td class="red lasttd center">32</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11307757,0" class="icommentjs icon16" href="/easeus-partition-master-technician-v10-8-multilang-deepstatus-t11307757.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-zipType"></i>
                    <a href="/easeus-partition-master-technician-v10-8-multilang-deepstatus-t11307757.html" class="cellMainLink">EaseUS Partition Master Technician v10.8 Multilang [deepstatus]</a></div>
			</td>
			<td class="nobr center" data-sort="29750804">28.37 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="23 Sep 2015, 23:33">2&nbsp;days</span></td>
			<td class="green center">65</td>
			<td class="red lasttd center">5</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/fansub-resistance-naruto-shippuuden-431-french-subbed-1280x720-mp4-t11312191.html" class="cellMainLink">[Fansub-Resistance]Naruto Shippuuden 431 [French Subbed] (1280x720).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="240050124">228.93 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="24 Sep 2015, 19:22">1&nbsp;day</span></td>
			<td class="green center">1383</td>
			<td class="red lasttd center">213</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/leopard-raws-himouto-umaru-chan-12-end-abc-1280x720-x264-aac-mp4-t11308450.html" class="cellMainLink">[Leopard-Raws] Himouto! Umaru-chan - 12 END (ABC 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="363507515">346.67 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="24 Sep 2015, 03:20">1&nbsp;day</span></td>
			<td class="green center">946</td>
			<td class="red lasttd center">112</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11306987,0" class="icommentjs icon16" href="/horriblesubs-himouto-umaru-chan-12-720p-mkv-t11306987.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/horriblesubs-himouto-umaru-chan-12-720p-mkv-t11306987.html" class="cellMainLink">[HorribleSubs] Himouto! Umaru-chan - 12 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="340384076">324.62 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="23 Sep 2015, 19:10">2&nbsp;days</span></td>
			<td class="green center">716</td>
			<td class="red lasttd center">47</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11307059,0" class="icommentjs icon16" href="/leopard-raws-kuusen-madoushi-kouhosei-no-kyoukan-12-end-sun-1280x720-x264-aac-mp4-t11307059.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/leopard-raws-kuusen-madoushi-kouhosei-no-kyoukan-12-end-sun-1280x720-x264-aac-mp4-t11307059.html" class="cellMainLink">[Leopard-Raws] Kuusen Madoushi Kouhosei no Kyoukan - 12 END (SUN 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="599451761">571.68 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="23 Sep 2015, 19:30">2&nbsp;days</span></td>
			<td class="green center">573</td>
			<td class="red lasttd center">47</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/leopard-raws-ore-monogatari-24-end-ntv-1280x720-x264-aac-mp4-t11308446.html" class="cellMainLink">[Leopard-Raws] Ore Monogatari!! - 24 END (NTV 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="349735346">333.53 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="24 Sep 2015, 03:20">1&nbsp;day</span></td>
			<td class="green center">362</td>
			<td class="red lasttd center">40</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/leopard-raws-aquarion-logos-13-raw-kbs-1280x720-x264-aac-mp4-t11312053.html" class="cellMainLink">[Leopard-Raws] Aquarion Logos - 13 RAW (KBS 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="654248476">623.94 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="24 Sep 2015, 18:50">1&nbsp;day</span></td>
			<td class="green center">305</td>
			<td class="red lasttd center">86</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11310322,0" class="icommentjs icon16" href="/naruto-shippuden-431-eng-sub-480p-l-mbert-t11310322.html#comment"> <em class="iconvalue">21</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/naruto-shippuden-431-eng-sub-480p-l-mbert-t11310322.html" class="cellMainLink">Naruto Shippuden 431 [EnG SuB] 480p L@mBerT</a></div>
			</td>
			<td class="nobr center" data-sort="59710672">56.94 <span>MB</span></td>
			<td class="center">9</td>
			<td class="center"><span title="24 Sep 2015, 12:15">1&nbsp;day</span></td>
			<td class="green center">281</td>
			<td class="red lasttd center">122</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/leopard-raws-monster-musume-no-iru-nichijou-12-end-kbs-1280x720-x264-aac-mp4-t11301817.html" class="cellMainLink">[Leopard-Raws] Monster Musume no Iru Nichijou - 12 END (KBS 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="396790277">378.41 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="22 Sep 2015, 19:05">3&nbsp;days</span></td>
			<td class="green center">309</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/leopard-raws-suzakinishi-the-animation-12-end-mx-1280x720-x264-aac-mp4-t11308438.html" class="cellMainLink">[Leopard-Raws] Suzakinishi The Animation - 12 END (MX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="64998673">61.99 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="24 Sep 2015, 03:15">1&nbsp;day</span></td>
			<td class="green center">217</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/vivid-non-non-biyori-repeat-12-3e3cb332-mkv-t11310794.html" class="cellMainLink">[Vivid] Non Non Biyori Repeat - 12 [3E3CB332].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="329972617">314.69 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="24 Sep 2015, 14:15">1&nbsp;day</span></td>
			<td class="green center">209</td>
			<td class="red lasttd center">19</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/leopard-raws-bikini-warriors-12-end-mx-1280x720-x264-aac-mp4-t11301819.html" class="cellMainLink">[Leopard-Raws] Bikini Warriors - 12 END (MX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="89828876">85.67 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="22 Sep 2015, 19:05">3&nbsp;days</span></td>
			<td class="green center">176</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/leopard-raws-naruto-shippuuden-431-raw-tx-1280x720-x264-aac-mp4-t11312054.html" class="cellMainLink">[Leopard-Raws] Naruto Shippuuden - 431 RAW (TX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="363865610">347.01 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="24 Sep 2015, 18:50">1&nbsp;day</span></td>
			<td class="green center">162</td>
			<td class="red lasttd center">25</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/commie-ore-monogatari-24-f3620fc6-mkv-t11309254.html" class="cellMainLink">[Commie] Ore Monogatari!! - 24 [F3620FC6].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="206865527">197.28 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="24 Sep 2015, 07:15">1&nbsp;day</span></td>
			<td class="green center">147</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11304876,0" class="icommentjs icon16" href="/animerg-dragon-ball-super-11-1080p-mkv-t11304876.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/animerg-dragon-ball-super-11-1080p-mkv-t11304876.html" class="cellMainLink">[AnimeRG] Dragon Ball Super - 11 [1080p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="575317387">548.67 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="23 Sep 2015, 12:27">2&nbsp;days</span></td>
			<td class="green center">133</td>
			<td class="red lasttd center">27</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11300735,0" class="icommentjs icon16" href="/bakedfish-overlord-12-720p-aac-mp4-t11300735.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-filmType"></i>
                    <a href="/bakedfish-overlord-12-720p-aac-mp4-t11300735.html" class="cellMainLink">[BakedFish] Overlord - 12 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="327777042">312.59 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="22 Sep 2015, 16:16">3&nbsp;days</span></td>
			<td class="green center">90</td>
			<td class="red lasttd center">6</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11306424,0" class="icommentjs icon16" href="/marvel-week-09-23-2015-nem-t11306424.html#comment"> <em class="iconvalue">25</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/marvel-week-09-23-2015-nem-t11306424.html" class="cellMainLink">Marvel Week+ (09-23-2015) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="433365925">413.29 <span>MB</span></td>
			<td class="center">12</td>
			<td class="center"><span title="23 Sep 2015, 17:08">2&nbsp;days</span></td>
			<td class="green center">747</td>
			<td class="red lasttd center">287</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11305303,0" class="icommentjs icon16" href="/dc-week-09-23-2015-aka-dc-you-week-17-nem-t11305303.html#comment"> <em class="iconvalue">18</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/dc-week-09-23-2015-aka-dc-you-week-17-nem-t11305303.html" class="cellMainLink">DC Week+ (09-23-2015) (aka DC YOU Week 17) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="512210774">488.48 <span>MB</span></td>
			<td class="center">13</td>
			<td class="center"><span title="23 Sep 2015, 13:34">2&nbsp;days</span></td>
			<td class="green center">551</td>
			<td class="red lasttd center">249</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11302716,0" class="icommentjs icon16" href="/photography-books-collection-for-beginners-professionals-mantesh-t11302716.html#comment"> <em class="iconvalue">15</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/photography-books-collection-for-beginners-professionals-mantesh-t11302716.html" class="cellMainLink">Photography Books Collection for Beginners &amp; Professionals - Mantesh</a></div>
			</td>
			<td class="nobr center" data-sort="2487965372">2.32 <span>GB</span></td>
			<td class="center">93</td>
			<td class="center"><span title="23 Sep 2015, 00:54">2&nbsp;days</span></td>
			<td class="green center">383</td>
			<td class="red lasttd center">305</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11298618,0" class="icommentjs icon16" href="/j-k-rowling-harry-potter-audio-books-1-7-read-by-stephen-fry-mp3-t11298618.html#comment"> <em class="iconvalue">14</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/j-k-rowling-harry-potter-audio-books-1-7-read-by-stephen-fry-mp3-t11298618.html" class="cellMainLink">J. K. Rowling - Harry Potter Audio Books 1-7; Read by Stephen Fry [MP3]</a></div>
			</td>
			<td class="nobr center" data-sort="1350620034">1.26 <span>GB</span></td>
			<td class="center">202</td>
			<td class="center"><span title="22 Sep 2015, 06:51">3&nbsp;days</span></td>
			<td class="green center">271</td>
			<td class="red lasttd center">154</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11303061,0" class="icommentjs icon16" href="/home-garden-magazines-sept-23-2015-true-pdf-t11303061.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/home-garden-magazines-sept-23-2015-true-pdf-t11303061.html" class="cellMainLink">Home &amp; Garden Magazines - Sept 23 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="1117004255">1.04 <span>GB</span></td>
			<td class="center">48</td>
			<td class="center"><span title="23 Sep 2015, 02:37">2&nbsp;days</span></td>
			<td class="green center">253</td>
			<td class="red lasttd center">185</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11301185,0" class="icommentjs icon16" href="/injustice-gods-among-us-year-four-021-2015-digital-son-of-ultron-empire-cbr-nem-t11301185.html#comment"> <em class="iconvalue">25</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/injustice-gods-among-us-year-four-021-2015-digital-son-of-ultron-empire-cbr-nem-t11301185.html" class="cellMainLink">Injustice - Gods Among Us - Year Four 021 (2015) (digital) (Son of Ultron-Empire).cbr (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="24838866">23.69 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="22 Sep 2015, 17:01">3&nbsp;days</span></td>
			<td class="green center">335</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11306475,0" class="icommentjs icon16" href="/how-to-create-a-new-vegetable-garden-producing-a-beautiful-and-fruitful-garden-from-scratch-2015-pdf-gooner-t11306475.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/how-to-create-a-new-vegetable-garden-producing-a-beautiful-and-fruitful-garden-from-scratch-2015-pdf-gooner-t11306475.html" class="cellMainLink">How to Create a New Vegetable Garden - Producing a Beautiful and Fruitful Garden from Scratch (2015).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="12139542">11.58 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="23 Sep 2015, 17:16">2&nbsp;days</span></td>
			<td class="green center">326</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11303415,0" class="icommentjs icon16" href="/assorted-magazines-bundle-sept-23-2015-true-pdf-t11303415.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/assorted-magazines-bundle-sept-23-2015-true-pdf-t11303415.html" class="cellMainLink">Assorted Magazines Bundle - Sept 23 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="257048577">245.14 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span title="23 Sep 2015, 04:38">2&nbsp;days</span></td>
			<td class="green center">251</td>
			<td class="red lasttd center">137</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11306624,0" class="icommentjs icon16" href="/how-to-attack-and-defend-your-website-1st-edition-2015-pdf-gooner-t11306624.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/how-to-attack-and-defend-your-website-1st-edition-2015-pdf-gooner-t11306624.html" class="cellMainLink">How to Attack and Defend Your Website - 1st Edition (2015).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="24600547">23.46 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="23 Sep 2015, 17:44">2&nbsp;days</span></td>
			<td class="green center">261</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11308263,0" class="icommentjs icon16" href="/womens-magazines-bundle-sept-24-2015-true-pdf-t11308263.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/womens-magazines-bundle-sept-24-2015-true-pdf-t11308263.html" class="cellMainLink">Womens Magazines Bundle - Sept 24 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="652070814">621.86 <span>MB</span></td>
			<td class="center">30</td>
			<td class="center"><span title="24 Sep 2015, 02:22">1&nbsp;day</span></td>
			<td class="green center">176</td>
			<td class="red lasttd center">110</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11299684,0" class="icommentjs icon16" href="/maximum-pc-november-2015-t11299684.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/maximum-pc-november-2015-t11299684.html" class="cellMainLink">Maximum PC â November 2015</a></div>
			</td>
			<td class="nobr center" data-sort="31430432">29.97 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="22 Sep 2015, 12:50">3&nbsp;days</span></td>
			<td class="green center">215</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11308512,0" class="icommentjs icon16" href="/mens-magazines-bundle-september-24-2015-true-pdf-t11308512.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/mens-magazines-bundle-september-24-2015-true-pdf-t11308512.html" class="cellMainLink">Mens Magazines Bundle - September 24 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="187130146">178.46 <span>MB</span></td>
			<td class="center">10</td>
			<td class="center"><span title="24 Sep 2015, 03:48">1&nbsp;day</span></td>
			<td class="green center">188</td>
			<td class="red lasttd center">68</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11309647,0" class="icommentjs icon16" href="/hydroponics-for-the-home-grower-2015-pdf-gooner-t11309647.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/hydroponics-for-the-home-grower-2015-pdf-gooner-t11309647.html" class="cellMainLink">Hydroponics for the Home Grower (2015).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="35781637">34.12 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="24 Sep 2015, 09:18">1&nbsp;day</span></td>
			<td class="green center">188</td>
			<td class="red lasttd center">40</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11308773,0" class="icommentjs icon16" href="/computer-gadget-gamer-mags-sept-24-2015-true-pdf-t11308773.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/computer-gadget-gamer-mags-sept-24-2015-true-pdf-t11308773.html" class="cellMainLink">Computer Gadget &amp; Gamer Mags - Sept 24 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="160306587">152.88 <span>MB</span></td>
			<td class="center">14</td>
			<td class="center"><span title="24 Sep 2015, 05:16">1&nbsp;day</span></td>
			<td class="green center">160</td>
			<td class="red lasttd center">60</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11303990,0" class="icommentjs icon16" href="/0-day-week-of-2015-09-16-t11303990.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-pdfType"></i>
                    <a href="/0-day-week-of-2015-09-16-t11303990.html" class="cellMainLink">0-Day Week of 2015.09.16</a></div>
			</td>
			<td class="nobr center" data-sort="9313891593">8.67 <span>GB</span></td>
			<td class="center">162</td>
			<td class="center"><span title="23 Sep 2015, 07:37">2&nbsp;days</span></td>
			<td class="green center">100</td>
			<td class="red lasttd center">161</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11304381,0" class="icommentjs icon16" href="/james-brown-20-all-time-greatest-hits-2014-24-96-hd-flac-t11304381.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/james-brown-20-all-time-greatest-hits-2014-24-96-hd-flac-t11304381.html" class="cellMainLink">James Brown - 20 All-Time Greatest Hits! (2014) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1734013155">1.61 <span>GB</span></td>
			<td class="center">46</td>
			<td class="center"><span title="23 Sep 2015, 09:32">2&nbsp;days</span></td>
			<td class="green center">148</td>
			<td class="red lasttd center">78</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11305987,0" class="icommentjs icon16" href="/nina-simone-i-put-a-spell-on-you-2013-24-96-hd-flac-t11305987.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/nina-simone-i-put-a-spell-on-you-2013-24-96-hd-flac-t11305987.html" class="cellMainLink">Nina Simone - I Put A Spell On You (2013) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="739114149">704.87 <span>MB</span></td>
			<td class="center">30</td>
			<td class="center"><span title="23 Sep 2015, 16:25">2&nbsp;days</span></td>
			<td class="green center">107</td>
			<td class="red lasttd center">35</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11299074,0" class="icommentjs icon16" href="/santana-welcome-2014-24-96-hd-flac-t11299074.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/santana-welcome-2014-24-96-hd-flac-t11299074.html" class="cellMainLink">Santana - Welcome (2014) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1202782311">1.12 <span>GB</span></td>
			<td class="center">24</td>
			<td class="center"><span title="22 Sep 2015, 09:43">3&nbsp;days</span></td>
			<td class="green center">100</td>
			<td class="red lasttd center">30</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11299576,0" class="icommentjs icon16" href="/new-order-music-complete-japanese-edition-2015-flac-sn3h1t87-glodls-t11299576.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/new-order-music-complete-japanese-edition-2015-flac-sn3h1t87-glodls-t11299576.html" class="cellMainLink">New Order - Music Complete (Japanese Edition) [2015] [FLAC] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="536392947">511.54 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span title="22 Sep 2015, 12:19">3&nbsp;days</span></td>
			<td class="green center">98</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11299723,0" class="icommentjs icon16" href="/john-coltrane-blue-train-2012-24-192-hd-flac-t11299723.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/john-coltrane-blue-train-2012-24-192-hd-flac-t11299723.html" class="cellMainLink">John Coltrane - Blue Train (2012) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="2146708144">2 <span>GB</span></td>
			<td class="center">17</td>
			<td class="center"><span title="22 Sep 2015, 13:01">3&nbsp;days</span></td>
			<td class="green center">74</td>
			<td class="red lasttd center">50</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/frank-sinatra-past-present-future-vinyl-yeraycito-master-series-t11300470.html" class="cellMainLink">Frank Sinatra - Past, Present &amp; Future (VINYL) YERAYCITO MASTER SERIES</a></div>
			</td>
			<td class="nobr center" data-sort="2189285811">2.04 <span>GB</span></td>
			<td class="center">29</td>
			<td class="center"><span title="22 Sep 2015, 15:32">3&nbsp;days</span></td>
			<td class="green center">75</td>
			<td class="red lasttd center">35</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11297407,0" class="icommentjs icon16" href="/eric-clapton-live-at-budokan-flac-tntvillage-t11297407.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/eric-clapton-live-at-budokan-flac-tntvillage-t11297407.html" class="cellMainLink">Eric Clapton - Live at Budokan [Flac][TntVillage]</a></div>
			</td>
			<td class="nobr center" data-sort="405327350">386.55 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center"><span title="22 Sep 2015, 00:30">3&nbsp;days</span></td>
			<td class="green center">85</td>
			<td class="red lasttd center">9</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11300467,0" class="icommentjs icon16" href="/joan-baez-the-best-of-vinyl-yeraycito-master-series-t11300467.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/joan-baez-the-best-of-vinyl-yeraycito-master-series-t11300467.html" class="cellMainLink">Joan Baez - The Best Of (VINYL) YERAYCITO MASTER SERIES</a></div>
			</td>
			<td class="nobr center" data-sort="1121449364">1.04 <span>GB</span></td>
			<td class="center">15</td>
			<td class="center"><span title="22 Sep 2015, 15:31">3&nbsp;days</span></td>
			<td class="green center">70</td>
			<td class="red lasttd center">33</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11307277,0" class="icommentjs icon16" href="/cecil-taylor-unit-structures-2014-24-192-hd-flac-t11307277.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/cecil-taylor-unit-structures-2014-24-192-hd-flac-t11307277.html" class="cellMainLink">Cecil Taylor - Unit Structures (2014) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1761842371">1.64 <span>GB</span></td>
			<td class="center">14</td>
			<td class="center"><span title="23 Sep 2015, 20:26">2&nbsp;days</span></td>
			<td class="green center">62</td>
			<td class="red lasttd center">30</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11301745,0" class="icommentjs icon16" href="/rolling-stones-out-of-our-heads-us-uk-2005-24-88-hd-flac-t11301745.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/rolling-stones-out-of-our-heads-us-uk-2005-24-88-hd-flac-t11301745.html" class="cellMainLink">Rolling Stones - Out Of Our Heads US + UK (2005) [24-88 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="870844725">830.5 <span>MB</span></td>
			<td class="center">68</td>
			<td class="center"><span title="22 Sep 2015, 18:50">3&nbsp;days</span></td>
			<td class="green center">59</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11309964,0" class="icommentjs icon16" href="/west-bruce-laing-discography-flac-t11309964.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/west-bruce-laing-discography-flac-t11309964.html" class="cellMainLink">West, Bruce &amp; Laing - Discography [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1332442143">1.24 <span>GB</span></td>
			<td class="center">109</td>
			<td class="center"><span title="24 Sep 2015, 10:37">1&nbsp;day</span></td>
			<td class="green center">37</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11312546,0" class="icommentjs icon16" href="/john-mclaughlin-black-light-2015-flac-t11312546.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/john-mclaughlin-black-light-2015-flac-t11312546.html" class="cellMainLink">John McLaughlin - Black Light (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="352581658">336.25 <span>MB</span></td>
			<td class="center">13</td>
			<td class="center"><span title="24 Sep 2015, 21:20">1&nbsp;day</span></td>
			<td class="green center">36</td>
			<td class="red lasttd center">13</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11308594,0" class="icommentjs icon16" href="/va-latin-music-flac-mp3-big-papi-japanese-import-victor-tokyo-1991-t11308594.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/va-latin-music-flac-mp3-big-papi-japanese-import-victor-tokyo-1991-t11308594.html" class="cellMainLink">VA - Latin Music [FLAC+MP3](Big Papi) Japanese Import Victor Tokyo 1991</a></div>
			</td>
			<td class="nobr center" data-sort="349021387">332.85 <span>MB</span></td>
			<td class="center">35</td>
			<td class="center"><span title="24 Sep 2015, 04:19">1&nbsp;day</span></td>
			<td class="green center">31</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/va-the-best-of-80-s-2cd-2015-flac-t11307212.html" class="cellMainLink">VA - The Best of 80&#039;s [2CD] (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="773231974">737.41 <span>MB</span></td>
			<td class="center">39</td>
			<td class="center"><span title="23 Sep 2015, 20:01">2&nbsp;days</span></td>
			<td class="green center">31</td>
			<td class="red lasttd center">6</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock">
					<i class="ka kaTorType ka-musicType"></i>
                    <a href="/world-chillout-bestsellers-2015-t11307122.html" class="cellMainLink">World Chillout (Bestsellers) (2015)</a></div>
			</td>
			<td class="nobr center" data-sort="322651968">307.7 <span>MB</span></td>
			<td class="center">11</td>
			<td class="center"><span title="23 Sep 2015, 19:44">2&nbsp;days</span></td>
			<td class="green center">32</td>
			<td class="red lasttd center">4</td>
        </tr>
			</table>


	<script type="text/javascript">
		if ($.cookie('kat_settings[fpw_column]') != null && $.cookie('kat_settings[fpw_order]')) {
			var colIndex = $.cookie('kat_settings[fpw_column]')
			var ascend = $.cookie('kat_settings[fpw_order]');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			sortTables(colIndex, ascend);
		}
		$('.frontPageWidget .firstr th .fpw-sort').click(function() {
			var colIndex = $(this).parent().index();
			var ascend = !$(this).hasClass('sortedAsc')?1:0;
			$('.fpw-sort').removeClass('sortedAsc sortedDesc');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			updateFPWsettings(colIndex, ascend);
			sortTables(colIndex, ascend);
		});
		function sortTables(colIndex, ascend) {	
			$('.frontPageWidget').each(function() {
				var data = [];
				$('tr:not(.firstr)', $(this)).each(function() {
					var name = $('td:first .cellMainLink', $(this)).text();
					var id = $('td:first .cellMainLink', $(this)).attr('href').match(/.*-t(\d+)\.html/)[1];
					var fileSize = $('td:eq(1)', $(this)).attr('data-sort');
					data.push({
						'col0':name, 'col1':padNum(fileSize), 'col2':padNum($('td:eq(2)',$(this)).text()), 'col3':padNum(id), 'col4':padNum($('td:eq(4)',$(this)).text()), 'col5':padNum($('td:eq(5)',$(this)).text()), 'html':$(this).html()
					});
				});
				data.sort(function(a, b) {
					var x = a['col'+colIndex].toLowerCase(); var y = b['col'+colIndex].toLowerCase();
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
				if (!ascend||ascend=='0') data.reverse();
				$(this).find('tr:not(.firstr)').remove();
				for (var i=0;i<data.length;i++) {
					$(this).append('<tr class="'+(i+1%2==0?'even':'odd')+'">'+data[i].html+'</tr>');
				}
			});
		}
		function padNum(val) {
			val = '0000000000000000'+val;
			return val.substring(val.length-15);
		}
		function updateFPWsettings(t, o) {
			$.cookie('kat_settings[fpw_column]', t, { expires: 365, path: '/' });
			$.cookie('kat_settings[fpw_order]', o, { expires: 365, path: '/' });
		}
	</script>
		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
    
            
        <div id="_119b0a17fab5493361a252d04bf527db"></div>
    
                
    	    <div class="advertising">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <div id="_7063408f1c01d50e0dc2d833186ce962"></div>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/where-are-my-manners/?unread=16933561">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Where are my manners?
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/5%5Bc%5D4LlYW%5B4%5D9/">5[c]4LlYW[4]9</a></span></span> <span title="25 Sep 2015, 23:43">30&nbsp;sec.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/utorrent-ask-anything-plus-tips-tricks/?unread=16933559">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Utorrent: Ask Anything, plus Tips &amp; Tricks
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/Twistty/">Twistty</a></span></span> <span title="25 Sep 2015, 23:43">51&nbsp;sec.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/adopt-uploader-program-v11-all-users-help/?unread=16933557">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				**Adopt an uploader Program v11-- For all Users to Help**
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/yivlx/">yivlx</a></span></span> <span title="25 Sep 2015, 23:42">1&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/book-yaang3rs-bya-academic-it-engineering-fiction-non-fictio-thread-103734/?unread=16933555">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				BÃÃK Ð¯ANGÎRS[BÐ¯] : Academic, IT, Engineering, Fiction &amp; Non-Fiction Releases
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_2"><a class="plain" href="/user/ira-1969/">ira-1969</a></span></span> <span title="25 Sep 2015, 23:41">2&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/translation-team-refresh-all-translators-read-sign/?unread=16933546">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Translation Team Refresh *All Translators Read &amp; Sign
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_4"><a class="plain" href="/user/Salad_Fingers/">Salad_Fingers</a></span></span> <span title="25 Sep 2015, 23:39">5&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/my-first-torrent-please-post-here-and-help-me/?unread=16933544">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				My First Torrent - Please POST Here! and Help Me..
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/yivlx/">yivlx</a></span></span> <span title="25 Sep 2015, 23:36">7&nbsp;min.&nbsp;ago</span></span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/summer-updates-september-1/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Summer updates [September, 1]
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="01 Sep 2015, 16:13">3&nbsp;weeks&nbsp;ago</span></span>
	</li>
	<li>
		<a href="/blog/post/kickasstorrents-is-moving-to-kat-cr-domain/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				KickassTorrents is moving to kat.cr domain
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="24 Apr 2015, 10:13">5&nbsp;months&nbsp;ago</span></span>
	</li>
	<li>
		<a href="/blog/post/happy-torrents-day-2015/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Happy Torrents Day 2015!
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="30 Mar 2015, 10:06">5&nbsp;months&nbsp;ago</span></span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/CualiTochtli/post/torrenting-we-do-it-yet-some-still-complain-about-something-or-look-askance-yammer-impute-or-growl/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Torrenting - we do it, yet some still complain about something or look askance, yammer, impute or growl</p></a><span class="explanation">by <a class="plain aclColor_8" href="/user/CualiTochtli/">CualiTochtli</a> <span title="25 Sep 2015, 22:34">1&nbsp;hour&nbsp;ago</span></span></li>
	<li><a href="/blog/olderthangod/post/the-gym-and-the-cork/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> The Gym and the Cork</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/olderthangod/">olderthangod</a> <span title="25 Sep 2015, 19:45">3&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/Smittech/post/smittech-and-his-evil-liver/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Smittech and his Evil Liver.....</p></a><span class="explanation">by <a class="plain aclColor_5" href="/user/Smittech/">Smittech</a> <span title="25 Sep 2015, 02:05">21&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/Secludish/post/hosted-for-a-wonderful-new-user/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Hosted for a wonderful new user! :)</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/Secludish/">Secludish</a> <span title="24 Sep 2015, 00:18">yesterday</span></span></li>
	<li><a href="/blog/olderthangod/post/lucy/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Lucy</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/olderthangod/">olderthangod</a> <span title="23 Sep 2015, 22:25">2&nbsp;days&nbsp;ago</span></span></li>
	<li><a href="/blog/magicpotions/post/on-a-swiss-revolution-by-watertiger/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> On a Swiss revolution by watertiger</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/magicpotions/">magicpotions</a> <span title="23 Sep 2015, 17:43">2&nbsp;days&nbsp;ago</span></span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/drake%20future/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				drake future
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/max%20lauren%20graham/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Max lauren graham
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/nezu/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				nezu
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/soccermom%20charlie/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				soccermom charlie
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/inches%20of%20black%20meat/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				inches of black meat
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/wanderlust/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				wanderlust
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/hereafter%20-%20outra%20vida%20%282010%29/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Hereafter - Outra Vida (2010)
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/this%20was%20your%20idea/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				this was your idea
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/stage%20fright%202005/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				stage fright 2005
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/itil/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				itil
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/evil%20dead/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				evil dead
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		<li>
			<a data-nop href="http://www.torrentreactor.net/" target="_blank" rel="external">
				<span class="itorreact thirdPartIcons"></span>TorrentReactor
			</a>
		</li>
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a onclick="setLanguage('bsc', '.kat.cr');return false;" class="plain">Bosnian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a onclick="setLanguage('ko', '.kat.cr');return false;" class="plain">Korean</a></li>
                                <li><a onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
		<li><a href="/welcome/">welcome</a></li>
		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <div id="_673e31f53f8166159b8e996c4124765b"></div>
        <div id="_e7050fb15fd39b3e4e99a5be4a57b6ea"></div>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script><script>
 sc('addGlobal', 'pagetype', 'front');
 sc('addSlot', '_60318cd4e8d28f6fb76fe34e9bd9c498');
sc('addSlot', '_39ecb76dd457e5ac33776fdf11500d56');
sc('addSlot', '_277923e5f9d753c5b0630c28e641790c');
sc('addSlot', '_119b0a17fab5493361a252d04bf527db');
sc('addSlot', '_7063408f1c01d50e0dc2d833186ce962', { 'searchQuery': '' });
sc('addSlot', '_673e31f53f8166159b8e996c4124765b');
sc('addSlot', '_e7050fb15fd39b3e4e99a5be4a57b6ea');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>

</body>
</html>
