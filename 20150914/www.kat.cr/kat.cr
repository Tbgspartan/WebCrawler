<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-46aa35a.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-46aa35a.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-46aa35a.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-46aa35a.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kat.cr/sc-46aa35a.js');

        sc('setHost', 'a.kat.cr');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: '46aa35a',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-46aa35a.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<div id="_60318cd4e8d28f6fb76fe34e9bd9c498"></div>
<div id="_39ecb76dd457e5ac33776fdf11500d56"></div>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka">e</i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka">g</i></button></div>
			</form>
		</div>
        <div id="_277923e5f9d753c5b0630c28e641790c"></div>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080p/" class="tag6">1080p</a>
	<a href="/search/2014/" class="tag2">2014</a>
	<a href="/search/2015/" class="tag10">2015</a>
	<a href="/search/3d/" class="tag4">3d</a>
	<a href="/search/american%20ultra/" class="tag2">american ultra</a>
	<a href="/search/android/" class="tag2">android</a>
	<a href="/search/ant%20man/" class="tag2">ant man</a>
	<a href="/search/apk/" class="tag2">apk</a>
	<a href="/search/avengers/" class="tag2">avengers</a>
	<a href="/search/bajrangi%20bhaijaan/" class="tag2">bajrangi bhaijaan</a>
	<a href="/search/discography/" class="tag3">discography</a>
	<a href="/search/fantastic%20four/" class="tag2">fantastic four</a>
	<a href="/search/fear%20the%20walking%20dead/" class="tag2">fear the walking dead</a>
	<a href="/search/french/" class="tag3">french</a>
	<a href="/search/hand%20of%20god/" class="tag2">hand of god</a>
	<a href="/search/hero/" class="tag2">hero</a>
	<a href="/search/hindi/" class="tag10">hindi</a>
	<a href="/search/hindi%202015/" class="tag6">hindi 2015</a>
	<a href="/search/hitman%20agent%2047/" class="tag2">hitman agent 47</a>
	<a href="/search/inside%20out/" class="tag2">inside out</a>
	<a href="/search/ita/" class="tag2">ita</a>
	<a href="/search/longmire/" class="tag2">longmire</a>
	<a href="/search/mad%20max/" class="tag5">mad max</a>
	<a href="/search/malayalam/" class="tag3">malayalam</a>
	<a href="/search/metal%20gear%20solid/" class="tag2">metal gear solid</a>
	<a href="/search/metal%20gear%20solid%20v/" class="tag2">metal gear solid v</a>
	<a href="/search/minions/" class="tag2">minions</a>
	<a href="/search/movies/" class="tag2">movies</a>
	<a href="/search/mr%20robot/" class="tag2">mr robot</a>
	<a href="/search/narcos/" class="tag2">narcos</a>
	<a href="/search/nezu/" class="tag10">nezu</a>
	<a href="/search/nl/" class="tag2">nl</a>
	<a href="/search/pixels/" class="tag2">pixels</a>
	<a href="/search/ripsalot/" class="tag4">ripsalot</a>
	<a href="/search/star%20wars/" class="tag2">star wars</a>
	<a href="/search/straight%20outta%20compton/" class="tag2">straight outta compton</a>
	<a href="/search/tamil/" class="tag4">tamil</a>
	<a href="/search/telugu/" class="tag4">telugu</a>
	<a href="/search/telugu%202015/" class="tag2">telugu 2015</a>
	<a href="/search/the%20visit/" class="tag2">the visit</a>
	<a href="/search/the%20walking%20dead/" class="tag2">the walking dead</a>
	<a href="/search/under%20the%20dome/" class="tag2">under the dome</a>
	<a href="/search/welcome%20back/" class="tag2">welcome back</a>
	<a href="/search/welcome%20back%202015/" class="tag6">welcome back 2015</a>
	<a href="/search/windows%2010/" class="tag2">windows 10</a>
	<a href="/search/yify/" class="tag10">yify</a>
	<a href="/search/yify%201080p/" class="tag4">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag5">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11235757,0" class="icommentjs icon16" href="/entourage-2015-720p-brrip-x264-yify-t11235757.html#comment"> <em class="iconvalue">96</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/entourage-2015-720p-brrip-x264-yify-t11235757.html" class="cellMainLink">Entourage (2015) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="848380549">809.08 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="10 Sep 2015, 20:10">3&nbsp;days</span></td>
			<td class="green center">12946</td>
			<td class="red lasttd center">6730</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11233043,0" class="icommentjs icon16" href="/jurassic-world-2015-hdrip-xvid-ac3-etrg-t11233043.html#comment"> <em class="iconvalue">108</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/jurassic-world-2015-hdrip-xvid-ac3-etrg-t11233043.html" class="cellMainLink">Jurassic World 2015 HDRip XViD AC3-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="1505408603">1.4 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="10 Sep 2015, 09:28">3&nbsp;days</span></td>
			<td class="green center">8325</td>
			<td class="red lasttd center">6965</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11237868,0" class="icommentjs icon16" href="/poltergeist-2015-1080p-brrip-x264-yify-t11237868.html#comment"> <em class="iconvalue">55</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/poltergeist-2015-1080p-brrip-x264-yify-t11237868.html" class="cellMainLink">Poltergeist (2015) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="1554603645">1.45 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="11 Sep 2015, 04:26">2&nbsp;days</span></td>
			<td class="green center">8359</td>
			<td class="red lasttd center">5470</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11231663,0" class="icommentjs icon16" href="/fantastic-four-2015-korsub-1080p-hdrip-x264-aac-jyk-t11231663.html#comment"> <em class="iconvalue">31</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fantastic-four-2015-korsub-1080p-hdrip-x264-aac-jyk-t11231663.html" class="cellMainLink">Fantastic Four 2015 KORSUB 1080p HDRip x264 AAC-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="2844591571">2.65 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="10 Sep 2015, 03:19">3&nbsp;days</span></td>
			<td class="green center">5872</td>
			<td class="red lasttd center">5702</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11233453,0" class="icommentjs icon16" href="/beyond-the-mask-2015-hdrip-xvid-ac3-evo-t11233453.html#comment"> <em class="iconvalue">47</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/beyond-the-mask-2015-hdrip-xvid-ac3-evo-t11233453.html" class="cellMainLink">Beyond The Mask 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1540307317">1.43 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="10 Sep 2015, 11:30">3&nbsp;days</span></td>
			<td class="green center">4599</td>
			<td class="red lasttd center">4057</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11245376,0" class="icommentjs icon16" href="/monster-hunt-2015-hd720p-x264-aac-mandarin-chs-eng-mp4ba-t11245376.html#comment"> <em class="iconvalue">34</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/monster-hunt-2015-hd720p-x264-aac-mandarin-chs-eng-mp4ba-t11245376.html" class="cellMainLink">Monster Hunt 2015 HD720P X264 AAC Mandarin CHS-ENG Mp4Ba</a></div>
			</td>
			<td class="nobr center" data-sort="1783627898">1.66 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span title="12 Sep 2015, 12:05">1&nbsp;day</span></td>
			<td class="green center">2106</td>
			<td class="red lasttd center">5827</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11241173,0" class="icommentjs icon16" href="/self-less-2015-1080p-web-dl-x264-aac-jyk-t11241173.html#comment"> <em class="iconvalue">14</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/self-less-2015-1080p-web-dl-x264-aac-jyk-t11241173.html" class="cellMainLink">Self less 2015 1080p WEB-DL x264 AAC-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="3154108558">2.94 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="11 Sep 2015, 19:55">2&nbsp;days</span></td>
			<td class="green center">4098</td>
			<td class="red lasttd center">2306</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11230921,0" class="icommentjs icon16" href="/underworld-the-legacy-collection-1080p-bluray-ac3-x264-etrg-t11230921.html#comment"> <em class="iconvalue">75</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/underworld-the-legacy-collection-1080p-bluray-ac3-x264-etrg-t11230921.html" class="cellMainLink">Underworld The Legacy Collection 1080p BluRay AC3 x264-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="14263004774">13.28 <span>GB</span></td>
			<td class="center">100</td>
			<td class="center"><span title="09 Sep 2015, 23:23">3&nbsp;days</span></td>
			<td class="green center">1777</td>
			<td class="red lasttd center">3882</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11237729,0" class="icommentjs icon16" href="/posthumous-2014-720p-brrip-x264-yify-t11237729.html#comment"> <em class="iconvalue">26</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/posthumous-2014-720p-brrip-x264-yify-t11237729.html" class="cellMainLink">Posthumous (2014) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="793744058">756.97 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="11 Sep 2015, 03:27">2&nbsp;days</span></td>
			<td class="green center">3230</td>
			<td class="red lasttd center">1542</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11245585,0" class="icommentjs icon16" href="/swat-unit-887-2015-dvdrip-xvid-etrg-t11245585.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/swat-unit-887-2015-dvdrip-xvid-etrg-t11245585.html" class="cellMainLink">SWAT Unit 887 2015 DVDRip XviD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="739772779">705.5 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="12 Sep 2015, 12:55">1&nbsp;day</span></td>
			<td class="green center">1471</td>
			<td class="red lasttd center">2025</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11238451,0" class="icommentjs icon16" href="/listening-2014-hdrip-xvid-ac3-evo-t11238451.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/listening-2014-hdrip-xvid-ac3-evo-t11238451.html" class="cellMainLink">Listening 2014 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1480142570">1.38 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="11 Sep 2015, 07:09">2&nbsp;days</span></td>
			<td class="green center">1328</td>
			<td class="red lasttd center">1856</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11233758,0" class="icommentjs icon16" href="/avengers-age-of-ultron-2015-3d-brrip-x264-yify-t11233758.html#comment"> <em class="iconvalue">14</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/avengers-age-of-ultron-2015-3d-brrip-x264-yify-t11233758.html" class="cellMainLink">Avengers: Age of Ultron (2015) 3D BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="2202225640">2.05 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="10 Sep 2015, 13:11">3&nbsp;days</span></td>
			<td class="green center">1869</td>
			<td class="red lasttd center">1046</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11239824,0" class="icommentjs icon16" href="/san-andreas-2015-1080p-webrip-ac3-x264-etrg-t11239824.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/san-andreas-2015-1080p-webrip-ac3-x264-etrg-t11239824.html" class="cellMainLink">San Andreas 2015 1080p WEBRip AC3 x264-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="3145504996">2.93 <span>GB</span></td>
			<td class="center">13</td>
			<td class="center"><span title="11 Sep 2015, 13:45">2&nbsp;days</span></td>
			<td class="green center">891</td>
			<td class="red lasttd center">1736</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11247152,0" class="icommentjs icon16" href="/forbidden-kingdom-2015-dvdrip-x264-ac3-5-1-ddr-t11247152.html#comment"> <em class="iconvalue">19</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/forbidden-kingdom-2015-dvdrip-x264-ac3-5-1-ddr-t11247152.html" class="cellMainLink">Forbidden Kingdom (2015) DvDRip x264 AC3 5.1 [DDR]</a></div>
			</td>
			<td class="nobr center" data-sort="1582007539">1.47 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="12 Sep 2015, 21:20">1&nbsp;day</span></td>
			<td class="green center">315</td>
			<td class="red lasttd center">1618</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11244022,0" class="icommentjs icon16" href="/to-the-fore-2015-hd720p-x264-aac-mandarin-chs-mp4ba-hyprz-t11244022.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/to-the-fore-2015-hd720p-x264-aac-mandarin-chs-mp4ba-hyprz-t11244022.html" class="cellMainLink">To The Fore 2015 HD720P X264 AAC Mandarin CHS Mp4Ba [HyprZ]</a></div>
			</td>
			<td class="nobr center" data-sort="2454716538">2.29 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span title="12 Sep 2015, 06:48">1&nbsp;day</span></td>
			<td class="green center">616</td>
			<td class="red lasttd center">1077</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11243089,0" class="icommentjs icon16" href="/z-nation-s02e01-hdtv-x264-killers-ettv-t11243089.html#comment"> <em class="iconvalue">95</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/z-nation-s02e01-hdtv-x264-killers-ettv-t11243089.html" class="cellMainLink">Z Nation S02E01 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="477606441">455.48 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="12 Sep 2015, 03:10">1&nbsp;day</span></td>
			<td class="green center">3610</td>
			<td class="red lasttd center">1675</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11237213,0" class="icommentjs icon16" href="/under-the-dome-s03e13-hdtv-x264-lol-ettv-t11237213.html#comment"> <em class="iconvalue">105</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/under-the-dome-s03e13-hdtv-x264-lol-ettv-t11237213.html" class="cellMainLink">Under the Dome S03E13 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="357367562">340.81 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="11 Sep 2015, 00:01">2&nbsp;days</span></td>
			<td class="green center">4221</td>
			<td class="red lasttd center">442</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11242601,0" class="icommentjs icon16" href="/continuum-s04e02-hdtv-x264-killers-ettv-t11242601.html#comment"> <em class="iconvalue">42</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/continuum-s04e02-hdtv-x264-killers-ettv-t11242601.html" class="cellMainLink">Continuum S04E02 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="360064685">343.38 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="12 Sep 2015, 02:12">1&nbsp;day</span></td>
			<td class="green center">2620</td>
			<td class="red lasttd center">1041</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11237658,0" class="icommentjs icon16" href="/dominion-s02e10-hdtv-x264-killers-ettv-t11237658.html#comment"> <em class="iconvalue">53</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dominion-s02e10-hdtv-x264-killers-ettv-t11237658.html" class="cellMainLink">Dominion S02E10 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="266363116">254.02 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="11 Sep 2015, 03:09">2&nbsp;days</span></td>
			<td class="green center">3166</td>
			<td class="red lasttd center">314</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11243553,0" class="icommentjs icon16" href="/hand-of-god-s01-web-dl-xvid-fum-ettv-t11243553.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/hand-of-god-s01-web-dl-xvid-fum-ettv-t11243553.html" class="cellMainLink">Hand of God S01 WEB-DL XviD-FUM[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="4623584535">4.31 <span>GB</span></td>
			<td class="center">11</td>
			<td class="center"><span title="12 Sep 2015, 05:05">1&nbsp;day</span></td>
			<td class="green center">1519</td>
			<td class="red lasttd center">1590</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11231418,0" class="icommentjs icon16" href="/extant-s02e12e13-hdtv-x264-lol-ettv-t11231418.html#comment"> <em class="iconvalue">68</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/extant-s02e12e13-hdtv-x264-lol-ettv-t11231418.html" class="cellMainLink">Extant S02E12E13 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="497240845">474.21 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="10 Sep 2015, 02:01">3&nbsp;days</span></td>
			<td class="green center">2118</td>
			<td class="red lasttd center">211</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11233017,0" class="icommentjs icon16" href="/the-ultimate-fighter-s22e01-hdtv-x264-fight-bb-t11233017.html#comment"> <em class="iconvalue">35</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-ultimate-fighter-s22e01-hdtv-x264-fight-bb-t11233017.html" class="cellMainLink">The Ultimate Fighter S22E01 HDTV x264 Fight-BB</a></div>
			</td>
			<td class="nobr center" data-sort="1033483556">985.61 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="10 Sep 2015, 09:24">3&nbsp;days</span></td>
			<td class="green center">1758</td>
			<td class="red lasttd center">245</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11237677,0" class="icommentjs icon16" href="/wwe-smackdown-2015-09-10-hdtv-x264-ebi-sparrow-t11237677.html#comment"> <em class="iconvalue">33</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/wwe-smackdown-2015-09-10-hdtv-x264-ebi-sparrow-t11237677.html" class="cellMainLink">WWE Smackdown 2015 09 10 HDTV x264-Ebi -={SPARROW}=-</a></div>
			</td>
			<td class="nobr center" data-sort="902192460">860.4 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="11 Sep 2015, 03:14">2&nbsp;days</span></td>
			<td class="green center">1369</td>
			<td class="red lasttd center">271</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11231541,0" class="icommentjs icon16" href="/the-league-s07e01-hdtv-x264-batv-ettv-t11231541.html#comment"> <em class="iconvalue">18</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-league-s07e01-hdtv-x264-batv-ettv-t11231541.html" class="cellMainLink">The League S07E01 HDTV x264-BATV[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="290715520">277.25 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="10 Sep 2015, 02:42">3&nbsp;days</span></td>
			<td class="green center">1131</td>
			<td class="red lasttd center">296</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11235812,0" class="icommentjs icon16" href="/longmire-s04e01-webrip-x264-2hd-rartv-t11235812.html#comment"> <em class="iconvalue">53</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/longmire-s04e01-webrip-x264-2hd-rartv-t11235812.html" class="cellMainLink">Longmire S04E01 WEBRip x264-2HD[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="630919776">601.69 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="10 Sep 2015, 20:35">3&nbsp;days</span></td>
			<td class="green center">917</td>
			<td class="red lasttd center">185</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11247498,0" class="icommentjs icon16" href="/match-of-the-day-s51e06-12th-september-2015-720p-x264-t11247498.html#comment"> <em class="iconvalue">16</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/match-of-the-day-s51e06-12th-september-2015-720p-x264-t11247498.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/match-of-the-day-s51e06-12th-september-2015-720p-x264-t11247498.html" class="cellMainLink">Match of the Day - S51E06 - 12th September 2015 - 720p x264</a></div>
			</td>
			<td class="nobr center" data-sort="1795177461">1.67 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="12 Sep 2015, 23:24">23&nbsp;hours</span></td>
			<td class="green center">511</td>
			<td class="red lasttd center">421</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11242950,0" class="icommentjs icon16" href="/bering-sea-gold-s05e02-girl-drama-hdtv-x264-w4f-ettv-t11242950.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/bering-sea-gold-s05e02-girl-drama-hdtv-x264-w4f-ettv-t11242950.html" class="cellMainLink">Bering Sea Gold S05E02 Girl Drama HDTV x264-W4F[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="349672954">333.47 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="12 Sep 2015, 03:02">1&nbsp;day</span></td>
			<td class="green center">682</td>
			<td class="red lasttd center">228</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11247145,0" class="icommentjs icon16" href="/the-x-factor-uk-s12e05-hdtv-x264-4yeo-mp4-t11247145.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-x-factor-uk-s12e05-hdtv-x264-4yeo-mp4-t11247145.html" class="cellMainLink">The X Factor UK S12E05 HDTV x264-4yEo mp4</a></div>
			</td>
			<td class="nobr center" data-sort="745021277">710.51 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="12 Sep 2015, 21:16">1&nbsp;day</span></td>
			<td class="green center">405</td>
			<td class="red lasttd center">240</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11247996,0" class="icommentjs icon16" href="/blunt-talk-s01e04-hdtv-x264-batv-ettv-t11247996.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/blunt-talk-s01e04-hdtv-x264-batv-ettv-t11247996.html" class="cellMainLink">Blunt Talk S01E04 HDTV x264-BATV[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="200155632">190.88 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="13 Sep 2015, 01:41">21&nbsp;hours</span></td>
			<td class="green center">277</td>
			<td class="red lasttd center">207</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11248593,0" class="icommentjs icon16" href="/ë°¤ì-ê±·ë-ì ë¹-scholar-who-walks-the-night-720p-hdtv-x264-aac-sodihd-complete-english-subtitles-added-to-description-t11248593.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ë°¤ì-ê±·ë-ì ë¹-scholar-who-walks-the-night-720p-hdtv-x264-aac-sodihd-complete-english-subtitles-added-to-description-t11248593.html" class="cellMainLink">ë°¤ì ê±·ë ì ë¹ (Scholar Who Walks the Night) 720p HDTV x264 AAC-SODiHD Complete (English subtitles added to description)</a></div>
			</td>
			<td class="nobr center" data-sort="20566125630">19.15 <span>GB</span></td>
			<td class="center">20</td>
			<td class="center"><span title="13 Sep 2015, 03:48">19&nbsp;hours</span></td>
			<td class="green center">44</td>
			<td class="red lasttd center">250</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11236782,0" class="icommentjs icon16" href="/jay-rock-90059-2015-320-kbps-t11236782.html#comment"> <em class="iconvalue">29</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/jay-rock-90059-2015-320-kbps-t11236782.html" class="cellMainLink">Jay Rock â 90059 (2015) 320 KBPS</a></div>
			</td>
			<td class="nobr center" data-sort="111696262">106.52 <span>MB</span></td>
			<td class="center">12</td>
			<td class="center"><span title="10 Sep 2015, 22:50">3&nbsp;days</span></td>
			<td class="green center">714</td>
			<td class="red lasttd center">75</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11231584,0" class="icommentjs icon16" href="/mp3-new-releases-2015-week-36-glodls-t11231584.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/mp3-new-releases-2015-week-36-glodls-t11231584.html" class="cellMainLink">MP3 NEW RELEASES 2015 WEEK 36 - [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="4285512645">3.99 <span>GB</span></td>
			<td class="center">451</td>
			<td class="center"><span title="10 Sep 2015, 02:55">3&nbsp;days</span></td>
			<td class="green center">425</td>
			<td class="red lasttd center">342</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11233651,0" class="icommentjs icon16" href="/stereophonics-keep-the-village-alive-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11233651.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/stereophonics-keep-the-village-alive-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11233651.html" class="cellMainLink">Stereophonics - Keep The Village Alive [2015] [MP3-320Kbps] [CBR] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="101657072">96.95 <span>MB</span></td>
			<td class="center">23</td>
			<td class="center"><span title="10 Sep 2015, 12:24">3&nbsp;days</span></td>
			<td class="green center">301</td>
			<td class="red lasttd center">41</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11238596,0" class="icommentjs icon16" href="/ryan-adams-heartbreaker-t11238596.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/ryan-adams-heartbreaker-t11238596.html" class="cellMainLink">Ryan Adams - Heartbreaker</a></div>
			</td>
			<td class="nobr center" data-sort="104919514">100.06 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span title="11 Sep 2015, 07:49">2&nbsp;days</span></td>
			<td class="green center">1</td>
			<td class="red lasttd center">290</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11236255,0" class="icommentjs icon16" href="/armin-van-buuren-a-state-of-trance-730-2015-09-10-split-clubtime-t11236255.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/armin-van-buuren-a-state-of-trance-730-2015-09-10-split-clubtime-t11236255.html" class="cellMainLink">Armin Van Buuren - A State Of Trance 730 (2015-09-10) (Split - ClubTime)</a></div>
			</td>
			<td class="nobr center" data-sort="313006455">298.51 <span>MB</span></td>
			<td class="center">37</td>
			<td class="center"><span title="10 Sep 2015, 21:47">3&nbsp;days</span></td>
			<td class="green center">235</td>
			<td class="red lasttd center">39</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11232045,0" class="icommentjs icon16" href="/top-100-60-s-rock-albums-by-ultimateclassicrock-mp3-cd-s-51-75-t11232045.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/top-100-60-s-rock-albums-by-ultimateclassicrock-mp3-cd-s-51-75-t11232045.html" class="cellMainLink">Top 100 60&#039;s Rock Albums By ultimateclassicrock (MP3)CD&#039;s 51-75</a></div>
			</td>
			<td class="nobr center" data-sort="3431580542">3.2 <span>GB</span></td>
			<td class="center">477</td>
			<td class="center"><span title="10 Sep 2015, 05:01">3&nbsp;days</span></td>
			<td class="green center">126</td>
			<td class="red lasttd center">141</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11240951,0" class="icommentjs icon16" href="/yo-gotti-the-return-320kbps-mixjoint-t11240951.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/yo-gotti-the-return-320kbps-mixjoint-t11240951.html" class="cellMainLink">Yo Gotti - The Return [320Kbps] (MixJoint)</a></div>
			</td>
			<td class="nobr center" data-sort="163505682">155.93 <span>MB</span></td>
			<td class="center">26</td>
			<td class="center"><span title="11 Sep 2015, 19:11">2&nbsp;days</span></td>
			<td class="green center">207</td>
			<td class="red lasttd center">53</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11238291,0" class="icommentjs icon16" href="/bring-me-the-horizon-that-s-the-spirit-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11238291.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bring-me-the-horizon-that-s-the-spirit-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11238291.html" class="cellMainLink">Bring Me the Horizon - That&#039;s The Spirit (2015) [MP3-320Kbps] [CBR] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="108646707">103.61 <span>MB</span></td>
			<td class="center">13</td>
			<td class="center"><span title="11 Sep 2015, 06:08">2&nbsp;days</span></td>
			<td class="green center">173</td>
			<td class="red lasttd center">43</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11243199,0" class="icommentjs icon16" href="/slayer-repentless-limited-box-set-2-cd-2015-320ak-t11243199.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/slayer-repentless-limited-box-set-2-cd-2015-320ak-t11243199.html" class="cellMainLink">Slayer - Repentless [Limited Box Set 2-CD] (2015)320ak</a></div>
			</td>
			<td class="nobr center" data-sort="267267350">254.89 <span>MB</span></td>
			<td class="center">32</td>
			<td class="center"><span title="12 Sep 2015, 03:31">1&nbsp;day</span></td>
			<td class="green center">150</td>
			<td class="red lasttd center">53</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11246418,0" class="icommentjs icon16" href="/billboard-top-100-singles-80s-1980-1989-mp3-vbr-h4ckus-glodls-t11246418.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/billboard-top-100-singles-80s-1980-1989-mp3-vbr-h4ckus-glodls-t11246418.html" class="cellMainLink">Billboard - Top 100 Singles 80s (1980 - 1989) [MP3-VBR] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="5149102873">4.8 <span>GB</span></td>
			<td class="center">1006</td>
			<td class="center"><span title="12 Sep 2015, 17:05">1&nbsp;day</span></td>
			<td class="green center">31</td>
			<td class="red lasttd center">146</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11235729,0" class="icommentjs icon16" href="/roots-reggae-third-world-discography-1976-2014-jamal-the-moroccan-t11235729.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/roots-reggae-third-world-discography-1976-2014-jamal-the-moroccan-t11235729.html" class="cellMainLink">[Roots Reggae] Third World - Discography 1976-2014 (Jamal The Moroccan)</a></div>
			</td>
			<td class="nobr center" data-sort="2879831137">2.68 <span>GB</span></td>
			<td class="center">309</td>
			<td class="center"><span title="10 Sep 2015, 19:57">3&nbsp;days</span></td>
			<td class="green center">76</td>
			<td class="red lasttd center">91</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11233118,0" class="icommentjs icon16" href="/selena-gomez-revival-deluxe-edition-2015-2-pre-order-singles-mp3-320kbps-h4ckus-glodls-t11233118.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/selena-gomez-revival-deluxe-edition-2015-2-pre-order-singles-mp3-320kbps-h4ckus-glodls-t11233118.html" class="cellMainLink">Selena Gomez - Revival [Deluxe Edition] [2015] - 2 Pre-order Singles [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="18116777">17.28 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="10 Sep 2015, 10:00">3&nbsp;days</span></td>
			<td class="green center">108</td>
			<td class="red lasttd center">42</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11238589,0" class="icommentjs icon16" href="/anjunadeep-07-mixed-by-james-grant-jody-wistenoff-mixed-unmixed-320kbps-inspiron-t11238589.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/anjunadeep-07-mixed-by-james-grant-jody-wistenoff-mixed-unmixed-320kbps-inspiron-t11238589.html" class="cellMainLink">Anjunadeep 07 (Mixed by James Grant &amp; Jody Wistenoff) (Mixed + Unmixed) (320kbps) (Inspiron)</a></div>
			</td>
			<td class="nobr center" data-sort="761394356">726.12 <span>MB</span></td>
			<td class="center">37</td>
			<td class="center"><span title="11 Sep 2015, 07:49">2&nbsp;days</span></td>
			<td class="green center">69</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/taqdeer-sibte-hassan-2015-full-album-mp3-groo-t11244597.html" class="cellMainLink">Taqdeer - Sibte Hassan (2015) Full Album Mp3 Groo</a></div>
			</td>
			<td class="nobr center" data-sort="92349338">88.07 <span>MB</span></td>
			<td class="center">11</td>
			<td class="center"><span title="12 Sep 2015, 08:52">1&nbsp;day</span></td>
			<td class="green center">20</td>
			<td class="red lasttd center">44</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/ufo-too-hot-to-handle-2-cd-2012-320ak-t11246585.html" class="cellMainLink">UFO - Too Hot To Handle(2-CD) 2012 320ak</a></div>
			</td>
			<td class="nobr center" data-sort="378183062">360.66 <span>MB</span></td>
			<td class="center">40</td>
			<td class="center"><span title="12 Sep 2015, 17:53">1&nbsp;day</span></td>
			<td class="green center">33</td>
			<td class="red lasttd center">26</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11237232,0" class="icommentjs icon16" href="/metal-gear-solid-v-the-phantom-pain-v1-0-0-3-update-2-multi8-not-cracked-fitgirl-repack-100-lossless-t11237232.html#comment"> <em class="iconvalue">365</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/metal-gear-solid-v-the-phantom-pain-v1-0-0-3-update-2-multi8-not-cracked-fitgirl-repack-100-lossless-t11237232.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/metal-gear-solid-v-the-phantom-pain-v1-0-0-3-update-2-multi8-not-cracked-fitgirl-repack-100-lossless-t11237232.html" class="cellMainLink">Metal Gear Solid V: The Phantom Pain (v1.0.0.3/Update 2, MULTI8 - NOT CRACKED) [FitGirl Repack, 100% Lossless]</a></div>
			</td>
			<td class="nobr center" data-sort="12363797840">11.51 <span>GB</span></td>
			<td class="center">13</td>
			<td class="center"><span title="11 Sep 2015, 00:09">2&nbsp;days</span></td>
			<td class="green center">263</td>
			<td class="red lasttd center">657</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11232066,0" class="icommentjs icon16" href="/the-witcher-3-wild-hunt-v-1-08-4-16-dlc-2015-pc-repack-Ð¾Ñ-r-g-games-t11232066.html#comment"> <em class="iconvalue">37</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/the-witcher-3-wild-hunt-v-1-08-4-16-dlc-2015-pc-repack-Ð¾Ñ-r-g-games-t11232066.html" class="cellMainLink">The Witcher 3: Wild Hunt [v 1.08.4 + 16 DLC] (2015) PC | RePack Ð¾Ñ R.G. Games</a></div>
			</td>
			<td class="nobr center" data-sort="27977253390">26.06 <span>GB</span></td>
			<td class="center">12</td>
			<td class="center"><span title="10 Sep 2015, 05:05">3&nbsp;days</span></td>
			<td class="green center">288</td>
			<td class="red lasttd center">584</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11240819,0" class="icommentjs icon16" href="/the-vanishing-of-ethan-carter-redux-reloaded-t11240819.html#comment"> <em class="iconvalue">32</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/the-vanishing-of-ethan-carter-redux-reloaded-t11240819.html" class="cellMainLink">The Vanishing of Ethan Carter Redux-RELOADED</a></div>
			</td>
			<td class="nobr center" data-sort="3027047390">2.82 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="11 Sep 2015, 18:22">2&nbsp;days</span></td>
			<td class="green center">175</td>
			<td class="red lasttd center">143</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11233514,0" class="icommentjs icon16" href="/minecraft-pocket-edition-v0-12-1-apk-android-t11233514.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/minecraft-pocket-edition-v0-12-1-apk-android-t11233514.html" class="cellMainLink">Minecraft - Pocket Edition v0.12.1 APK (Android)</a></div>
			</td>
			<td class="nobr center" data-sort="16665730">15.89 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="10 Sep 2015, 11:44">3&nbsp;days</span></td>
			<td class="green center">202</td>
			<td class="red lasttd center">11</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11235773,0" class="icommentjs icon16" href="/league-of-light-3-silent-mountain-ce-2015-pc-final-t11235773.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/league-of-light-3-silent-mountain-ce-2015-pc-final-t11235773.html" class="cellMainLink">League of Light 3: Silent Mountain CE (2015) PC [FINAL]</a></div>
			</td>
			<td class="nobr center" data-sort="890432122">849.18 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="10 Sep 2015, 20:16">3&nbsp;days</span></td>
			<td class="green center">170</td>
			<td class="red lasttd center">33</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11235798,0" class="icommentjs icon16" href="/hatred-survival-reloaded-t11235798.html#comment"> <em class="iconvalue">18</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/hatred-survival-reloaded-t11235798.html" class="cellMainLink">Hatred.Survival-RELOADED</a></div>
			</td>
			<td class="nobr center" data-sort="2306899293">2.15 <span>GB</span></td>
			<td class="center">49</td>
			<td class="center"><span title="10 Sep 2015, 20:27">3&nbsp;days</span></td>
			<td class="green center">121</td>
			<td class="red lasttd center">64</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11243586,0" class="icommentjs icon16" href="/nevertales-4-legends-ce-2015-pc-final-t11243586.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/nevertales-4-legends-ce-2015-pc-final-t11243586.html" class="cellMainLink">Nevertales 4: Legends CE (2015) PC [FINAL]</a></div>
			</td>
			<td class="nobr center" data-sort="814067441">776.36 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="12 Sep 2015, 05:10">1&nbsp;day</span></td>
			<td class="green center">135</td>
			<td class="red lasttd center">34</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11239230,0" class="icommentjs icon16" href="/victor-vran-by-xatab-t11239230.html#comment"> <em class="iconvalue">13</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/victor-vran-by-xatab-t11239230.html" class="cellMainLink">Victor Vran by XATAB</a></div>
			</td>
			<td class="nobr center" data-sort="2834919072">2.64 <span>GB</span></td>
			<td class="center">9</td>
			<td class="center"><span title="11 Sep 2015, 10:46">2&nbsp;days</span></td>
			<td class="green center">96</td>
			<td class="red lasttd center">35</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11240881,0" class="icommentjs icon16" href="/destiny-the-taken-king-legendary-edition-ps3-imars-t11240881.html#comment"> <em class="iconvalue">21</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/destiny-the-taken-king-legendary-edition-ps3-imars-t11240881.html" class="cellMainLink">Destiny The Taken King Legendary Edition PS3-iMARS</a></div>
			</td>
			<td class="nobr center" data-sort="7697498603">7.17 <span>GB</span></td>
			<td class="center">80</td>
			<td class="center"><span title="11 Sep 2015, 18:45">2&nbsp;days</span></td>
			<td class="green center">28</td>
			<td class="red lasttd center">89</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11246456,0" class="icommentjs icon16" href="/the-incredible-adventures-of-van-helsing-ii-complete-pack-gog-t11246456.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/the-incredible-adventures-of-van-helsing-ii-complete-pack-gog-t11246456.html" class="cellMainLink">The Incredible Adventures of Van Helsing II - Complete Pack (GOG)</a></div>
			</td>
			<td class="nobr center" data-sort="13175285474">12.27 <span>GB</span></td>
			<td class="center">9</td>
			<td class="center"><span title="12 Sep 2015, 17:18">1&nbsp;day</span></td>
			<td class="green center">12</td>
			<td class="red lasttd center">72</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11237674,0" class="icommentjs icon16" href="/nhl-legacy-edition-xbox360-imars-t11237674.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/nhl-legacy-edition-xbox360-imars-t11237674.html" class="cellMainLink">NHL Legacy Edition XBOX360-iMARS</a></div>
			</td>
			<td class="nobr center" data-sort="8738859105">8.14 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="11 Sep 2015, 03:14">2&nbsp;days</span></td>
			<td class="green center">31</td>
			<td class="red lasttd center">47</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11245776,0" class="icommentjs icon16" href="/lovers-in-a-dangerous-spacetime-v1-1-3-t11245776.html#comment"> <em class="iconvalue">15</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/lovers-in-a-dangerous-spacetime-v1-1-3-t11245776.html" class="cellMainLink">Lovers in a Dangerous Spacetime v1.1.3</a></div>
			</td>
			<td class="nobr center" data-sort="449734612">428.9 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="12 Sep 2015, 13:54">1&nbsp;day</span></td>
			<td class="green center">52</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11246377,0" class="icommentjs icon16" href="/ravenmark-scourge-of-estellion-reloaded-t11246377.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/ravenmark-scourge-of-estellion-reloaded-t11246377.html" class="cellMainLink">Ravenmark Scourge of Estellion-RELOADED</a></div>
			</td>
			<td class="nobr center" data-sort="715168651">682.04 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span title="12 Sep 2015, 16:52">1&nbsp;day</span></td>
			<td class="green center">1</td>
			<td class="red lasttd center">46</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11240446,0" class="icommentjs icon16" href="/solar-shifter-ex-codex-t11240446.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/solar-shifter-ex-codex-t11240446.html" class="cellMainLink">Solar.Shifter.EX-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="2325255865">2.17 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="11 Sep 2015, 16:34">2&nbsp;days</span></td>
			<td class="green center">20</td>
			<td class="red lasttd center">13</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11245775,0" class="icommentjs icon16" href="/subnautica-beta-2287-t11245775.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/subnautica-beta-2287-t11245775.html" class="cellMainLink">Subnautica - Beta 2287</a></div>
			</td>
			<td class="nobr center" data-sort="3587182781">3.34 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="12 Sep 2015, 13:54">1&nbsp;day</span></td>
			<td class="green center">15</td>
			<td class="red lasttd center">15</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11233359,0" class="icommentjs icon16" href="/os-x-el-capitan-gm-candidate-t11233359.html#comment"> <em class="iconvalue">19</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/os-x-el-capitan-gm-candidate-t11233359.html" class="cellMainLink">OS X El Capitan GM Candidate</a></div>
			</td>
			<td class="nobr center" data-sort="6066073703">5.65 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="10 Sep 2015, 11:11">3&nbsp;days</span></td>
			<td class="green center">283</td>
			<td class="red lasttd center">105</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11237316,0" class="icommentjs icon16" href="/windows-8-1-pro-vl-update-3-x64-en-us-esd-sept2015-pre-activated-team-os-t11237316.html#comment"> <em class="iconvalue">26</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-8-1-pro-vl-update-3-x64-en-us-esd-sept2015-pre-activated-team-os-t11237316.html" class="cellMainLink">Windows 8.1 Pro Vl Update 3 x64 En-Us ESD Sept2015 Pre-activated-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="4046848824">3.77 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span title="11 Sep 2015, 00:46">2&nbsp;days</span></td>
			<td class="green center">140</td>
			<td class="red lasttd center">218</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11232558,0" class="icommentjs icon16" href="/windows-7-ultimate-sp1-x64-en-us-oem-esd-sept2015-pre-activation-team-os-t11232558.html#comment"> <em class="iconvalue">47</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-7-ultimate-sp1-x64-en-us-oem-esd-sept2015-pre-activation-team-os-t11232558.html" class="cellMainLink">Windows 7 Ultimate Sp1 x64 En-Us OEM ESD Sept2015 Pre-Activation=-{TEAM OS}=</a></div>
			</td>
			<td class="nobr center" data-sort="3261522712">3.04 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="10 Sep 2015, 07:30">3&nbsp;days</span></td>
			<td class="green center">166</td>
			<td class="red lasttd center">173</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11241194,0" class="icommentjs icon16" href="/driverpack-solution-17-0-1-rc2-codename-camaro-team-os-t11241194.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/driverpack-solution-17-0-1-rc2-codename-camaro-team-os-t11241194.html" class="cellMainLink">DriverPack Solution 17.0.1 RC2 - Codename Camaro-=TEAM OS=-</a></div>
			</td>
			<td class="nobr center" data-sort="11397557611">10.61 <span>GB</span></td>
			<td class="center">1915</td>
			<td class="center"><span title="11 Sep 2015, 20:02">2&nbsp;days</span></td>
			<td class="green center">102</td>
			<td class="red lasttd center">134</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11238530,0" class="icommentjs icon16" href="/windows-10-numix-x86-2015-en-us-pre-activated-axeswy-tomecar-t11238530.html#comment"> <em class="iconvalue">38</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-10-numix-x86-2015-en-us-pre-activated-axeswy-tomecar-t11238530.html" class="cellMainLink">Windows 10 Numix x86 2015 En-Us Pre-Activated (AxeSwY &amp; TomeCar)</a></div>
			</td>
			<td class="nobr center" data-sort="2660554009">2.48 <span>GB</span></td>
			<td class="center">8</td>
			<td class="center"><span title="11 Sep 2015, 07:33">2&nbsp;days</span></td>
			<td class="green center">82</td>
			<td class="red lasttd center">103</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11239394,0" class="icommentjs icon16" href="/wintoflash-professional-1-2-0007-final-portable-key-4realtorrentz-t11239394.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/wintoflash-professional-1-2-0007-final-portable-key-4realtorrentz-t11239394.html" class="cellMainLink">WinToFlash Professional 1.2.0007 Final Portable + Key [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="38137590">36.37 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="11 Sep 2015, 11:43">2&nbsp;days</span></td>
			<td class="green center">156</td>
			<td class="red lasttd center">19</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11236753,0" class="icommentjs icon16" href="/flashboot-2-3-x32-x64-eng-license-key-t11236753.html#comment"> <em class="iconvalue">14</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/flashboot-2-3-x32-x64-eng-license-key-t11236753.html" class="cellMainLink">FlashBoot 2.3 (x32/x64)[ENG][License Key]</a></div>
			</td>
			<td class="nobr center" data-sort="23943411">22.83 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="10 Sep 2015, 22:47">3&nbsp;days</span></td>
			<td class="green center">116</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11240572,0" class="icommentjs icon16" href="/windows-7-aio-22in1-esd-pt-br-sep-2015-generation2-t11240572.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-7-aio-22in1-esd-pt-br-sep-2015-generation2-t11240572.html" class="cellMainLink">Windows 7 AIO 22in1 ESD pt-BR Sep 2015 {Generation2}</a></div>
			</td>
			<td class="nobr center" data-sort="4640329216">4.32 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="11 Sep 2015, 17:16">2&nbsp;days</span></td>
			<td class="green center">48</td>
			<td class="red lasttd center">75</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11240863,0" class="icommentjs icon16" href="/inkydeals-the-big-bang-bundle-fluck3r-t11240863.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/inkydeals-the-big-bang-bundle-fluck3r-t11240863.html" class="cellMainLink">[Inkydeals] The Big Bang Bundle [Fluck3r]</a></div>
			</td>
			<td class="nobr center" data-sort="27168943975">25.3 <span>GB</span></td>
			<td class="center">29</td>
			<td class="center"><span title="11 Sep 2015, 18:38">2&nbsp;days</span></td>
			<td class="green center">1</td>
			<td class="red lasttd center">111</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11245009,0" class="icommentjs icon16" href="/native-instruments-reaktor-6-os-x-pitchshifter-dada-t11245009.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/native-instruments-reaktor-6-os-x-pitchshifter-dada-t11245009.html" class="cellMainLink">Native Instruments - Reaktor 6 OS X [PiTcHsHiFTeR][dada]</a></div>
			</td>
			<td class="nobr center" data-sort="813630586">775.94 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="12 Sep 2015, 10:37">1&nbsp;day</span></td>
			<td class="green center">75</td>
			<td class="red lasttd center">20</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11245222,0" class="icommentjs icon16" href="/apple-final-cut-pro-x-10-2-2-mac-os-x-coque599-t11245222.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/apple-final-cut-pro-x-10-2-2-mac-os-x-coque599-t11245222.html" class="cellMainLink">Apple Final Cut Pro X 10.2.2 [Mac Os X] [coque599]</a></div>
			</td>
			<td class="nobr center" data-sort="2837576464">2.64 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="12 Sep 2015, 11:30">1&nbsp;day</span></td>
			<td class="green center">1</td>
			<td class="red lasttd center">91</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11245069,0" class="icommentjs icon16" href="/collection-of-programs-portableapps-v-12-1-multilanguage-appzdam-t11245069.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/collection-of-programs-portableapps-v-12-1-multilanguage-appzdam-t11245069.html" class="cellMainLink">Collection of programs PortableApps v.12.1 Multilanguage - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="6471215087">6.03 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="12 Sep 2015, 10:53">1&nbsp;day</span></td>
			<td class="green center">44</td>
			<td class="red lasttd center">31</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11245481,0" class="icommentjs icon16" href="/microsoft-office-2013-sp1-x64-pro-plus-vl-multi-15-generation2-t11245481.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/microsoft-office-2013-sp1-x64-pro-plus-vl-multi-15-generation2-t11245481.html" class="cellMainLink">Microsoft Office 2013 SP1 X64 Pro Plus VL MULTi-15 {Generation2}</a></div>
			</td>
			<td class="nobr center" data-sort="3939252362">3.67 <span>GB</span></td>
			<td class="center">18</td>
			<td class="center"><span title="12 Sep 2015, 12:38">1&nbsp;day</span></td>
			<td class="green center">17</td>
			<td class="red lasttd center">42</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11241302,0" class="icommentjs icon16" href="/8dio-adagio-strings-bundle-9-vst-au-aax-kontakt-oddsox-t11241302.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/8dio-adagio-strings-bundle-9-vst-au-aax-kontakt-oddsox-t11241302.html" class="cellMainLink">8dio Adagio Strings Bundle 9 VST-AU-AAX KONTAKT [oddsox]</a></div>
			</td>
			<td class="nobr center" data-sort="112709683611">104.97 <span>GB</span></td>
			<td class="center">25</td>
			<td class="center"><span title="11 Sep 2015, 20:32">2&nbsp;days</span></td>
			<td class="green center">0</td>
			<td class="red lasttd center">48</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11230996,0" class="icommentjs icon16" href="/ios9-gm-ipod-touch-5th-generation-13a340-t11230996.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/ios9-gm-ipod-touch-5th-generation-13a340-t11230996.html" class="cellMainLink">iOS9 GM iPod touch 5th generation 13A340</a></div>
			</td>
			<td class="nobr center" data-sort="3143097771">2.93 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="09 Sep 2015, 23:43">3&nbsp;days</span></td>
			<td class="green center">0</td>
			<td class="red lasttd center">24</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11237155,0" class="icommentjs icon16" href="/fansub-resistance-naruto-shippuuden-429-french-subbed-1280x720-mp4-t11237155.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fansub-resistance-naruto-shippuuden-429-french-subbed-1280x720-mp4-t11237155.html" class="cellMainLink">[Fansub-Resistance]Naruto Shippuuden 429 [French Subbed] (1280x720).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="254827394">243.02 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="10 Sep 2015, 23:26">2&nbsp;days</span></td>
			<td class="green center">1512</td>
			<td class="red lasttd center">83</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-charlotte-11-raw-bs11-1280x720-x264-aac-mp4-t11247032.html" class="cellMainLink">[Leopard-Raws] Charlotte - 11 RAW (BS11 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="370954151">353.77 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="12 Sep 2015, 20:25">1&nbsp;day</span></td>
			<td class="green center">1132</td>
			<td class="red lasttd center">305</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-arslan-senki-23-720p-mkv-t11249618.html" class="cellMainLink">[HorribleSubs] Arslan Senki - 23 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="450868514">429.98 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="13 Sep 2015, 09:10">14&nbsp;hours</span></td>
			<td class="green center">75</td>
			<td class="red lasttd center">595</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/commie-senki-zesshou-symphogear-gx-11-b071b8c3-mkv-t11246009.html" class="cellMainLink">[Commie] Senki Zesshou Symphogear GX - 11 [B071B8C3].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="440232440">419.84 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="12 Sep 2015, 14:55">1&nbsp;day</span></td>
			<td class="green center">177</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/bakedfish-rokka-no-yuusha-11-720p-aac-mp4-t11247200.html" class="cellMainLink">[BakedFish] Rokka no Yuusha - 11 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="276128906">263.34 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="12 Sep 2015, 21:37">1&nbsp;day</span></td>
			<td class="green center">142</td>
			<td class="red lasttd center">48</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/vivid-gakkou-gurashi-10-2c60e915-mkv-t11240113.html" class="cellMainLink">[Vivid] Gakkou Gurashi! - 10 [2C60E915].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="389299514">371.26 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="11 Sep 2015, 15:10">2&nbsp;days</span></td>
			<td class="green center">167</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/commie-gatchaman-crowds-insight-10-546d903e-mkv-t11247987.html" class="cellMainLink">[Commie] Gatchaman Crowds insight - 10 [546D903E].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="375582092">358.18 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="13 Sep 2015, 01:40">21&nbsp;hours</span></td>
			<td class="green center">130</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11231077,0" class="icommentjs icon16" href="/ghost-in-the-shell-arise-border-3-ghost-tears-2014-bdrip-1080p-hevc-sub-ita-eng-nahom-t11231077.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ghost-in-the-shell-arise-border-3-ghost-tears-2014-bdrip-1080p-hevc-sub-ita-eng-nahom-t11231077.html" class="cellMainLink">Ghost in the Shell Arise Border 3 Ghost Tears 2014 BDRip 1080p HEVC SUB ITA ENG-NAHOM</a></div>
			</td>
			<td class="nobr center" data-sort="1472832000">1.37 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="10 Sep 2015, 00:09">3&nbsp;days</span></td>
			<td class="green center">100</td>
			<td class="red lasttd center">49</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11234514,0" class="icommentjs icon16" href="/naruto-shippuden-episode-429-english-subbed-720p-arizone-t11234514.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/naruto-shippuden-episode-429-english-subbed-720p-arizone-t11234514.html" class="cellMainLink">Naruto Shippuden Episode 429 [English Subbed] 720p ~ARIZONE</a></div>
			</td>
			<td class="nobr center" data-sort="134846967">128.6 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="10 Sep 2015, 16:10">3&nbsp;days</span></td>
			<td class="green center">71</td>
			<td class="red lasttd center">33</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					<a class="icon16" href="/ipunisher-one-piece-709-720p-aac-mp4-t11248613.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ipunisher-one-piece-709-720p-aac-mp4-t11248613.html" class="cellMainLink">[iPUNISHER] One Piece - 709 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="313438154">298.92 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="13 Sep 2015, 04:00">19&nbsp;hours</span></td>
			<td class="green center">30</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11248642,0" class="icommentjs icon16" href="/one-piece-709-480p-eng-sub-72mb-iorchid-t11248642.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/one-piece-709-480p-eng-sub-72mb-iorchid-t11248642.html" class="cellMainLink">One Piece - 709 [480p][Eng Sub][72mb]_iORcHiD</a></div>
			</td>
			<td class="nobr center" data-sort="74788983">71.32 <span>MB</span></td>
			<td class="center">8</td>
			<td class="center"><span title="13 Sep 2015, 04:09">19&nbsp;hours</span></td>
			<td class="green center">13</td>
			<td class="red lasttd center">39</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/deadfish-prison-school-09-720p-aac-mp4-t11247297.html" class="cellMainLink">[DeadFish] Prison School - 09 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="499467496">476.33 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="12 Sep 2015, 22:18">1&nbsp;day</span></td>
			<td class="green center">35</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11232411,0" class="icommentjs icon16" href="/reinforce-juuou-mujin-no-fafnir-bdrip-1920x1080-x264-flac-t11232411.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/reinforce-juuou-mujin-no-fafnir-bdrip-1920x1080-x264-flac-t11232411.html" class="cellMainLink">[ReinForce] Juuou Mujin no Fafnir (BDRip 1920x1080 x264 FLAC)</a></div>
			</td>
			<td class="nobr center" data-sort="17488018976">16.29 <span>GB</span></td>
			<td class="center">149</td>
			<td class="center"><span title="10 Sep 2015, 06:35">3&nbsp;days</span></td>
			<td class="green center">4</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/reinforce-toaru-hikuushi-e-no-koiuta-bdrip-1920x1080-x264-flac-t11239298.html" class="cellMainLink">[ReinForce] Toaru Hikuushi e no Koiuta (BDRip 1920x1080 x264 FLAC)</a></div>
			</td>
			<td class="nobr center" data-sort="34668199264">32.29 <span>GB</span></td>
			<td class="center">70</td>
			<td class="center"><span title="11 Sep 2015, 11:10">2&nbsp;days</span></td>
			<td class="green center">2</td>
			<td class="red lasttd center">9</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11235923,0" class="icommentjs icon16" href="/wolverine-collection-1974-2015-nem-t11235923.html#comment"> <em class="iconvalue">22</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/wolverine-collection-1974-2015-nem-t11235923.html" class="cellMainLink">Wolverine Collection (1974-2015) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="39601024675">36.88 <span>GB</span></td>
			<td class="center">1418</td>
			<td class="center"><span title="10 Sep 2015, 21:10">3&nbsp;days</span></td>
			<td class="green center">106</td>
			<td class="red lasttd center">519</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11237389,0" class="icommentjs icon16" href="/mens-magazines-bundle-september-11-2015-true-pdf-t11237389.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/mens-magazines-bundle-september-11-2015-true-pdf-t11237389.html" class="cellMainLink">Mens Magazines Bundle - September 11 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="218988399">208.84 <span>MB</span></td>
			<td class="center">10</td>
			<td class="center"><span title="11 Sep 2015, 01:16">2&nbsp;days</span></td>
			<td class="green center">450</td>
			<td class="red lasttd center">122</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11234307,0" class="icommentjs icon16" href="/the-new-york-times-bestsellers-september-13-2015-fiction-non-fiction-t11234307.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-new-york-times-bestsellers-september-13-2015-fiction-non-fiction-t11234307.html" class="cellMainLink">The New York Times Bestsellers - September 13, 2015 [Fiction / Non-Fiction]</a></div>
			</td>
			<td class="nobr center" data-sort="71794752">68.47 <span>MB</span></td>
			<td class="center">25</td>
			<td class="center"><span title="10 Sep 2015, 15:38">3&nbsp;days</span></td>
			<td class="green center">371</td>
			<td class="red lasttd center">66</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11237214,0" class="icommentjs icon16" href="/excel-vba-24-hour-trainer-2nd-edition-2015-pdf-epub-gooner-t11237214.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/excel-vba-24-hour-trainer-2nd-edition-2015-pdf-epub-gooner-t11237214.html" class="cellMainLink">Excel VBA 24-Hour Trainer - 2nd Edition (2015) (Pdf &amp; Epub) Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="39511547">37.68 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="11 Sep 2015, 00:01">2&nbsp;days</span></td>
			<td class="green center">343</td>
			<td class="red lasttd center">51</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11233369,0" class="icommentjs icon16" href="/windows-10-for-dummies-t11233369.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/windows-10-for-dummies-t11233369.html" class="cellMainLink">Windows 10 For Dummies</a></div>
			</td>
			<td class="nobr center" data-sort="58765875">56.04 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="10 Sep 2015, 11:14">3&nbsp;days</span></td>
			<td class="green center">315</td>
			<td class="red lasttd center">49</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11235158,0" class="icommentjs icon16" href="/explosive-calisthenics-superhuman-power-maximum-speed-and-agility-plus-combat-ready-reflexes-using-bodyweight-only-methods-2015-pdf-epub-gooner-t11235158.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/explosive-calisthenics-superhuman-power-maximum-speed-and-agility-plus-combat-ready-reflexes-using-bodyweight-only-methods-2015-pdf-epub-gooner-t11235158.html" class="cellMainLink">Explosive Calisthenics, Superhuman Power, Maximum Speed and Agility, Plus Combat-Ready Reflexes Using Bodyweight-Only Methods (2015) (Pdf &amp; Epub) Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="58265763">55.57 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="10 Sep 2015, 19:02">3&nbsp;days</span></td>
			<td class="green center">301</td>
			<td class="red lasttd center">53</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11241381,0" class="icommentjs icon16" href="/fluent-python-1st-edition-2015-pdf-epub-mobi-gooner-t11241381.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/fluent-python-1st-edition-2015-pdf-epub-mobi-gooner-t11241381.html" class="cellMainLink">Fluent Python - 1st Edition (2015) (Pdf, Epub &amp; Mobi) Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="44155120">42.11 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="11 Sep 2015, 20:52">2&nbsp;days</span></td>
			<td class="green center">262</td>
			<td class="red lasttd center">53</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11243406,0" class="icommentjs icon16" href="/numerical-methods-for-engineers-and-scientists-3rd-ed-wiley-2014-pdf-t11243406.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/numerical-methods-for-engineers-and-scientists-3rd-ed-wiley-2014-pdf-t11243406.html" class="cellMainLink">Numerical Methods for Engineers and Scientists 3rd ed (Wiley, 2014).pdf</a></div>
			</td>
			<td class="nobr center" data-sort="44830825">42.75 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="12 Sep 2015, 04:34">1&nbsp;day</span></td>
			<td class="green center">178</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11244205,0" class="icommentjs icon16" href="/fitness-steps-to-success-1st-edition-2015-pdf-gooner-t11244205.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/fitness-steps-to-success-1st-edition-2015-pdf-gooner-t11244205.html" class="cellMainLink">Fitness - Steps to Success - 1st Edition (2015).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="17030457">16.24 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="12 Sep 2015, 07:27">1&nbsp;day</span></td>
			<td class="green center">169</td>
			<td class="red lasttd center">25</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11234378,0" class="icommentjs icon16" href="/image-week-09-09-2015-nem-t11234378.html#comment"> <em class="iconvalue">13</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/image-week-09-09-2015-nem-t11234378.html" class="cellMainLink">Image Week (09-09-2015) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="709151293">676.3 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span title="10 Sep 2015, 15:50">3&nbsp;days</span></td>
			<td class="green center">115</td>
			<td class="red lasttd center">50</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11239573,0" class="icommentjs icon16" href="/the-economist-12-september-18-september-2015-audio-edition-t11239573.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-economist-12-september-18-september-2015-audio-edition-t11239573.html" class="cellMainLink">The Economist â 12 September - 18 September 2015 (Audio Edition)</a></div>
			</td>
			<td class="nobr center" data-sort="195696293">186.63 <span>MB</span></td>
			<td class="center">97</td>
			<td class="center"><span title="11 Sep 2015, 12:29">2&nbsp;days</span></td>
			<td class="green center">93</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11246343,0" class="icommentjs icon16" href="/packtpub-publication-big-data-forensics-learning-hadoop-investigations-perform-forensic-investigations-on-hadoop-clusters-with-cutting-edge-tools-and-techniques-by-joe-sremack-pradyutvam2-pdf-t11246343.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/packtpub-publication-big-data-forensics-learning-hadoop-investigations-perform-forensic-investigations-on-hadoop-clusters-with-cutting-edge-tools-and-techniques-by-joe-sremack-pradyutvam2-pdf-t11246343.html" class="cellMainLink">(packtpub publication)Big Data Forensics Learning Hadoop Investigations --Perform forensic investigations on Hadoop clusters with cutting-edge tools and techniques by Joe Sremack(pradyutvam2).pdf</a></div>
			</td>
			<td class="nobr center" data-sort="3391864">3.23 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="12 Sep 2015, 16:41">1&nbsp;day</span></td>
			<td class="green center">1</td>
			<td class="red lasttd center">75</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11247455,0" class="icommentjs icon16" href="/hercules-collection-1965-2012-nem-t11247455.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/hercules-collection-1965-2012-nem-t11247455.html" class="cellMainLink">Hercules Collection (1965-2012) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="2187153194">2.04 <span>GB</span></td>
			<td class="center">94</td>
			<td class="center"><span title="12 Sep 2015, 23:10">1&nbsp;day</span></td>
			<td class="green center">42</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11247737,0" class="icommentjs icon16" href="/i-invented-the-modern-age-the-rise-of-henry-ford-richard-snow-t11247737.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/i-invented-the-modern-age-the-rise-of-henry-ford-richard-snow-t11247737.html" class="cellMainLink">I Invented the Modern Age - The Rise of Henry Ford - Richard Snow</a></div>
			</td>
			<td class="nobr center" data-sort="368009997">350.96 <span>MB</span></td>
			<td class="center">267</td>
			<td class="center"><span title="13 Sep 2015, 00:16">23&nbsp;hours</span></td>
			<td class="green center">17</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/what-s-up-1-tests-pdf-a0b10c110-t11245774.html" class="cellMainLink">What&#039;s Up 1 Tests - pdf [a0b10c110]</a></div>
			</td>
			<td class="nobr center" data-sort="636310">621.4 <span>KB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="12 Sep 2015, 13:54">1&nbsp;day</span></td>
			<td class="green center">0</td>
			<td class="red lasttd center">14</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11233852,0" class="icommentjs icon16" href="/pink-floyd-studio-albums-remastered-1967-2014-flac-ikar911-t11233852.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/pink-floyd-studio-albums-remastered-1967-2014-flac-ikar911-t11233852.html" class="cellMainLink">Pink Floyd - Studio albums [Remastered] (1967-2014) FLAC ikar911</a></div>
			</td>
			<td class="nobr center" data-sort="9539620798">8.88 <span>GB</span></td>
			<td class="center">309</td>
			<td class="center"><span title="10 Sep 2015, 13:42">3&nbsp;days</span></td>
			<td class="green center">155</td>
			<td class="red lasttd center">176</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11233857,0" class="icommentjs icon16" href="/rock-collection-1985-flac-t11233857.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/rock-collection-1985-flac-t11233857.html" class="cellMainLink">Rock Collection 1985 (FLAC)</a></div>
			</td>
			<td class="nobr center" data-sort="10021176388">9.33 <span>GB</span></td>
			<td class="center">330</td>
			<td class="center"><span title="10 Sep 2015, 13:44">3&nbsp;days</span></td>
			<td class="green center">81</td>
			<td class="red lasttd center">117</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11240941,0" class="icommentjs icon16" href="/va-one-night-at-the-opera-5-cd-set-2000-flac-tfm-t11240941.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-one-night-at-the-opera-5-cd-set-2000-flac-tfm-t11240941.html" class="cellMainLink">VA - One Night at the Opera - 5-CD-Set - (2000)-[FLAC]-[TFM]</a></div>
			</td>
			<td class="nobr center" data-sort="1867472837">1.74 <span>GB</span></td>
			<td class="center">98</td>
			<td class="center"><span title="11 Sep 2015, 19:09">2&nbsp;days</span></td>
			<td class="green center">133</td>
			<td class="red lasttd center">56</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11235066,0" class="icommentjs icon16" href="/isaac-hayes-shaft-soundtrack-2015-24-192-hd-flac-t11235066.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/isaac-hayes-shaft-soundtrack-2015-24-192-hd-flac-t11235066.html" class="cellMainLink">Isaac Hayes - Shaft Soundtrack (2015) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="2791907345">2.6 <span>GB</span></td>
			<td class="center">30</td>
			<td class="center"><span title="10 Sep 2015, 18:36">3&nbsp;days</span></td>
			<td class="green center">100</td>
			<td class="red lasttd center">37</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11239702,0" class="icommentjs icon16" href="/crosby-stills-nash-csn-2012-2012-24-48-hd-flac-t11239702.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/crosby-stills-nash-csn-2012-2012-24-48-hd-flac-t11239702.html" class="cellMainLink">Crosby, Stills &amp; Nash - CSN 2012 (2012) [24-48 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1807177812">1.68 <span>GB</span></td>
			<td class="center">56</td>
			<td class="center"><span title="11 Sep 2015, 13:04">2&nbsp;days</span></td>
			<td class="green center">73</td>
			<td class="red lasttd center">47</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11236469,0" class="icommentjs icon16" href="/slayer-repentless-limited-edition-2015-bbm-flac-t11236469.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/slayer-repentless-limited-edition-2015-bbm-flac-t11236469.html" class="cellMainLink">Slayer - Repentless (Limited Edition) (2015) BBM-FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="491827013">469.04 <span>MB</span></td>
			<td class="center">40</td>
			<td class="center"><span title="10 Sep 2015, 22:14">3&nbsp;days</span></td>
			<td class="green center">103</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11240084,0" class="icommentjs icon16" href="/duran-duran-paper-gods-deluxe-version-2015-flac-sn3h1t87-glodls-t11240084.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/duran-duran-paper-gods-deluxe-version-2015-flac-sn3h1t87-glodls-t11240084.html" class="cellMainLink">Duran Duran - Paper Gods [Deluxe Version] (2015) [FLAC] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="489912245">467.22 <span>MB</span></td>
			<td class="center">21</td>
			<td class="center"><span title="11 Sep 2015, 15:00">2&nbsp;days</span></td>
			<td class="green center">93</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11246055,0" class="icommentjs icon16" href="/linda-ronstadt-hasten-down-the-wind-2014-24-192-hd-flac-t11246055.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/linda-ronstadt-hasten-down-the-wind-2014-24-192-hd-flac-t11246055.html" class="cellMainLink">Linda Ronstadt - Hasten Down The Wind (2014) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1630198546">1.52 <span>GB</span></td>
			<td class="center">30</td>
			<td class="center"><span title="12 Sep 2015, 15:09">1&nbsp;day</span></td>
			<td class="green center">82</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11245384,0" class="icommentjs icon16" href="/santana-santana-iii-2014-24-96-hd-flac-t11245384.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/santana-santana-iii-2014-24-96-hd-flac-t11245384.html" class="cellMainLink">Santana - Santana III (2014) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="947965948">904.05 <span>MB</span></td>
			<td class="center">24</td>
			<td class="center"><span title="12 Sep 2015, 12:08">1&nbsp;day</span></td>
			<td class="green center">85</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/aretha-franklin-amazing-grace-vinyl-yeraycito-master-series-t11240124.html" class="cellMainLink">Aretha Franklin Amazing Grace (VINYL) YERAYCITO MASTER SERIES</a></div>
			</td>
			<td class="nobr center" data-sort="1772729697">1.65 <span>GB</span></td>
			<td class="center">17</td>
			<td class="center"><span title="11 Sep 2015, 15:13">2&nbsp;days</span></td>
			<td class="green center">57</td>
			<td class="red lasttd center">33</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11238619,0" class="icommentjs icon16" href="/metallica-live-at-olimpiyskiy-arena-moscow-rus-08-27-2015-24-48-hd-flac-t11238619.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/metallica-live-at-olimpiyskiy-arena-moscow-rus-08-27-2015-24-48-hd-flac-t11238619.html" class="cellMainLink">Metallica - Live at Olimpiyskiy Arena, Moscow, RUS - 08-27-2015 [24-48 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1675581133">1.56 <span>GB</span></td>
			<td class="center">80</td>
			<td class="center"><span title="11 Sep 2015, 07:55">2&nbsp;days</span></td>
			<td class="green center">61</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11234126,0" class="icommentjs icon16" href="/paul-simon-2011-so-beautiful-or-so-what-deluxe-limited-edition-cd-dvd-iso-eac-flac-t11234126.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/paul-simon-2011-so-beautiful-or-so-what-deluxe-limited-edition-cd-dvd-iso-eac-flac-t11234126.html" class="cellMainLink">Paul Simon - 2011 - So Beautiful Or So What (Deluxe Limited Edition) [CD+DVD ISO] [EAC FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1272256065">1.18 <span>GB</span></td>
			<td class="center">40</td>
			<td class="center"><span title="10 Sep 2015, 14:53">3&nbsp;days</span></td>
			<td class="green center">41</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11245716,0" class="icommentjs icon16" href="/chick-corea-bela-fleck-two-2015-flac-24-92-t11245716.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/chick-corea-bela-fleck-two-2015-flac-24-92-t11245716.html" class="cellMainLink">Chick Corea &amp; Bela Fleck - Two (2015) FLAC 24-92</a></div>
			</td>
			<td class="nobr center" data-sort="2182639745">2.03 <span>GB</span></td>
			<td class="center">21</td>
			<td class="center"><span title="12 Sep 2015, 13:33">1&nbsp;day</span></td>
			<td class="green center">48</td>
			<td class="red lasttd center">13</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11234024,0" class="icommentjs icon16" href="/amorphis-discography-1992-2015-t11234024.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/amorphis-discography-1992-2015-t11234024.html" class="cellMainLink">Amorphis - Discography 1992-2015</a></div>
			</td>
			<td class="nobr center" data-sort="8517701789">7.93 <span>GB</span></td>
			<td class="center">87</td>
			<td class="center"><span title="10 Sep 2015, 14:24">3&nbsp;days</span></td>
			<td class="green center">15</td>
			<td class="red lasttd center">27</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/oriental-jazz-gnawa-majid-bekkas-al-qantara-2013-flac-jamal-the-moroccan-t11247897.html" class="cellMainLink">[Oriental Jazz, Gnawa] Majid Bekkas - Al Qantara 2013 FLAC (Jamal The Moroccan)</a></div>
			</td>
			<td class="nobr center" data-sort="400888471">382.32 <span>MB</span></td>
			<td class="center">19</td>
			<td class="center"><span title="13 Sep 2015, 01:07">22&nbsp;hours</span></td>
			<td class="green center">1</td>
			<td class="red lasttd center">17</td>
        </tr>
			</table>


	<script type="text/javascript">
		if ($.cookie('kat_settings[fpw_column]') != null && $.cookie('kat_settings[fpw_order]')) {
			var colIndex = $.cookie('kat_settings[fpw_column]')
			var ascend = $.cookie('kat_settings[fpw_order]');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			sortTables(colIndex, ascend);
		}
		$('.frontPageWidget .firstr th .fpw-sort').click(function() {
			var colIndex = $(this).parent().index();
			var ascend = !$(this).hasClass('sortedAsc')?1:0;
			$('.fpw-sort').removeClass('sortedAsc sortedDesc');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			updateFPWsettings(colIndex, ascend);
			sortTables(colIndex, ascend);
		});
		function sortTables(colIndex, ascend) {	
			$('.frontPageWidget').each(function() {
				var data = [];
				$('tr:not(.firstr)', $(this)).each(function() {
					var name = $('td:first .cellMainLink', $(this)).text();
					var id = $('td:first .cellMainLink', $(this)).attr('href').match(/.*-t(\d+)\.html/)[1];
					var fileSize = $('td:eq(1)', $(this)).attr('data-sort');
					data.push({
						'col0':name, 'col1':padNum(fileSize), 'col2':padNum($('td:eq(2)',$(this)).text()), 'col3':padNum(id), 'col4':padNum($('td:eq(4)',$(this)).text()), 'col5':padNum($('td:eq(5)',$(this)).text()), 'html':$(this).html()
					});
				});
				data.sort(function(a, b) {
					var x = a['col'+colIndex].toLowerCase(); var y = b['col'+colIndex].toLowerCase();
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
				if (!ascend||ascend=='0') data.reverse();
				$(this).find('tr:not(.firstr)').remove();
				for (var i=0;i<data.length;i++) {
					$(this).append('<tr class="'+(i+1%2==0?'even':'odd')+'">'+data[i].html+'</tr>');
				}
			});
		}
		function padNum(val) {
			val = '0000000000000000'+val;
			return val.substring(val.length-15);
		}
		function updateFPWsettings(t, o) {
			$.cookie('kat_settings[fpw_column]', t, { expires: 365, path: '/' });
			$.cookie('kat_settings[fpw_order]', o, { expires: 365, path: '/' });
		}
	</script>
		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
    
            
        <div id="_119b0a17fab5493361a252d04bf527db"></div>
    
                
    	    <div class="advertising">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <div id="_7063408f1c01d50e0dc2d833186ce962"></div>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/kickass-anime-community-v-6/?unread=16892565">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Kickass Anime Community V.6!
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/marrrawrr/">marrrawrr</a></span></span> <span title="13 Sep 2015, 23:20">47&nbsp;sec.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/quiz-can-you-guess-movie-v14/?unread=16892564">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Quiz; Can you guess this movie? V14
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/anrkeytek/">anrkeytek</a></span></span> <span title="13 Sep 2015, 23:19">1&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/poll-if-you-had-enough-money-would-you-still-pirate/?unread=16892563">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				[Poll] If you had enough money, would you still pirate?
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/ConnorMan95/">ConnorMan95</a></span></span> <span title="13 Sep 2015, 23:17">3&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/tv-show-request-v2-thread-99183/?unread=16892561">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				TV Show Request V2
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/q1k--/">q1k--</a></span></span> <span title="13 Sep 2015, 23:16">4&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/how-spot-fake-torrent-files/?unread=16892560">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				How to Spot Fake Torrent Files
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/Fast_Upload/">Fast_Upload</a></span></span> <span title="13 Sep 2015, 23:16">4&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/what-games-are-you-playing-right-now-v2/?unread=16892558">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What Games are you playing right now? V2
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/TVGod/">TVGod</a></span></span> <span title="13 Sep 2015, 23:16">5&nbsp;min.&nbsp;ago</span></span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/summer-updates-september-1/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Summer updates [September, 1]
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="01 Sep 2015, 16:13">1&nbsp;week&nbsp;ago</span></span>
	</li>
	<li>
		<a href="/blog/post/kickasstorrents-is-moving-to-kat-cr-domain/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				KickassTorrents is moving to kat.cr domain
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="24 Apr 2015, 10:13">4&nbsp;months&nbsp;ago</span></span>
	</li>
	<li>
		<a href="/blog/post/happy-torrents-day-2015/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Happy Torrents Day 2015!
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="30 Mar 2015, 10:06">5&nbsp;months&nbsp;ago</span></span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/analogkid6103/post/uploaders-dumpers-and-elites-who-s-who/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Uploaders Dumpers and Elites Who&#039;s Who</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/analogkid6103/">analogkid6103</a> <span title="13 Sep 2015, 22:18">1&nbsp;hour&nbsp;ago</span></span></li>
	<li><a href="/blog/RuneMagle/post/my-disney-and-nickelodeon-uploads/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> My Disney and Nickelodeon Uploads</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/RuneMagle/">RuneMagle</a> <span title="13 Sep 2015, 21:03">2&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/politux/post/i-thought-elite-uploader-meant-original-content/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> I Thought Elite Uploader Meant Original Content</p></a><span class="explanation">by <a class="plain aclColor_3" href="/user/politux/">politux</a> <span title="13 Sep 2015, 14:23">8&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/olderthangod/post/can-you-handle-it/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Can you handle it ??</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/olderthangod/">olderthangod</a> <span title="12 Sep 2015, 23:07">yesterday</span></span></li>
	<li><a href="/blog/.X0RN./post/piracy-popcorns-sony-pictures/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Piracy Popcorns - Sony Pictures</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/.X0RN./">.X0RN.</a> <span title="12 Sep 2015, 20:17">yesterday</span></span></li>
	<li><a href="/blog/BulgariaHD/post/introduction/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Introduction</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/BulgariaHD/">BulgariaHD</a> <span title="12 Sep 2015, 12:28">yesterday</span></span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/the%20sound%20of%20rescue%20aperture/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				the sound of rescue aperture
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/quality%20masturbation%20anjelica/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				quality masturbation anjelica
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/ableton%208.1/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				ableton 8.1
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/sheeran/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				sheeran
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/edge%20of%20tomorrow/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				edge of tomorrow
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/multitracks%20is_safe%3A1/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				multitracks is_safe:1
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/mr.robot.s01e07.hdtv.x264-killers/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Mr.Robot.S01E07.HDTV.x264-KILLERS
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/kelly%20elliott/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				kelly elliott
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/%3Ddeep%20and%20house/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				=deep and house
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/far%20cry/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				far cry
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/hellsing%20ultimate%20complete/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				hellsing ultimate complete
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		<li>
			<a data-nop href="http://www.torrentreactor.net/" target="_blank" rel="external">
				<span class="itorreact thirdPartIcons"></span>TorrentReactor
			</a>
		</li>
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a onclick="setLanguage('ko', '.kat.cr');return false;" class="plain">Korean</a></li>
                                <li><a onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
		<li><a href="/welcome/">welcome</a></li>
		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <div id="_673e31f53f8166159b8e996c4124765b"></div>
        <div id="_e7050fb15fd39b3e4e99a5be4a57b6ea"></div>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script><script>
 sc('addGlobal', 'pagetype', 'front');
 sc('addSlot', '_60318cd4e8d28f6fb76fe34e9bd9c498');
sc('addSlot', '_39ecb76dd457e5ac33776fdf11500d56');
sc('addSlot', '_277923e5f9d753c5b0630c28e641790c');
sc('addSlot', '_119b0a17fab5493361a252d04bf527db');
sc('addSlot', '_7063408f1c01d50e0dc2d833186ce962', { 'searchQuery': '' });
sc('addSlot', '_673e31f53f8166159b8e996c4124765b');
sc('addSlot', '_e7050fb15fd39b3e4e99a5be4a57b6ea');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>

</body>
</html>
