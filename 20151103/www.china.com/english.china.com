ï»¿<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>China.com - Your guide on traveling and living in China</title>
<meta name="keywords" content="China,travel,lifestyle,learn Chinese,news,videos,business,films,sports,reports" />
<meta name="description" content="English.china.com is a one-stop shop for everything about China â news, events, culture, people, lifestyle, language. It also provides information about traveling and living in China." />
<meta name="auther" content="F7 13489" />
<!-- /etc/htmlhead.shtml Start -->
<link href="/css/style.css?20141205.1" rel="stylesheet" />
<!--[if lte IE 6]> 
<script src="/js/DD_belatedPNG_0.0.8a-min.js" type="text/javascript"></script>
<script type="text/javascript">
DD_belatedPNG.fix('.page-select-language-list em, .page-nav .page-openSearch em, .page-focus-prevnext a, .page-video-right .item-text, .page-watched .play, .page-video-right .item-icon, .page-video-right .item-watch'); 
</script>
<![endif]-->
<!-- /etc/htmlhead.shtml End -->
<!-- /etc/goto3g.shtml Start -->

<!-- /etc/goto3g.shtml End -->
</head>

<body>
<!-- /etc/channelhomehead.shtml Start -->
<div class="page-head">
  <div class="page-top-bg" id="page-top">
    <div class="maxWidth page-top">
      <div class="page-logo"><a href="/index.html"><img src="/img/logo.png" alt="china.com" width="208" height="46" /></a></div>
      <div class="page-language">
        <em>language:English</em>
      </div>
      <div class="page-top-right">
          <div class="page-top-time">Thursday, October 24, 2013</div>
          <div class="page-select-language" id="page-select-language">
              <div class="page-show-language" style="display: block;">Language</div>
              <div class="page-select-language-tit" style="display: none;"><i>Language</i></div>
              <div class="page-select-language-list" style="display: none;">
                <a href="http://www.china.com/index.html" class="langCn"><em>www</em></a>
                <a href="http://english.china.com/index.html" class="langEn"><em>english</em></a>
                <a href="http://german.china.com/index.html" class="langDe"><em>german</em></a>
                <a href="http://italy.china.com/index.html" class="langIt"><em>italy</em></a>
                <a href="http://portuguese.china.com/index.html" class="langPt"><em>portuguese</em></a>
                <a href="http://french.china.com/index.html" class="langFr"><em>french</em></a>
                <a href="http://russian.china.com/index.html" class="langRu"><em>russian</em></a>
                <a href="http://espanol.china.com/index.html" class="langEs"><em>espanol</em></a>
                <a href="http://malay.china.com/index.html" class="langMy"><em>malay</em></a>
                <a href="http://vietnamese.china.com/index.html" class="langVn"><em>vietnamese</em></a>
                <a href="http://laos.china.com/index.html" class="langLa"><em>laos</em></a>
                <a href="http://cambodian.china.com/index.html" class="langKh"><em>cambodian</em></a>
                <a href="http://thai.china.com/index.html" class="langTh"><em>thai</em></a>
                <a href="http://indonesian.china.com/index.html" class="langId"><em>indonesian</em></a>
                <a href="http://filipino.china.com/index.html" class="langPh"><em>filipino</em></a>
                <a href="http://myanmar.china.com/index.html" class="langMm"><em>myanmar</em></a>
                <a href="http://japanese.china.com/index.html" class="langJp"><em>japanese</em></a>
                <a href="http://korean.china.com/index.html" class="langKr"><em>korean</em></a>
                <a href="http://mongol.china.com/index.html" class="langMn"><em>mongol</em></a>
                <a href="http://nepal.china.com/index.html" class="langNp"><em>nepal</em></a>
                <a href="http://hindi.china.com/index.html" class="langIn"><em>hindi</em></a>
                <a href="http://bengali.china.com/index.html" class="langMd"><em>bengali</em></a>
                <a href="http://turkish.china.com/index.html" class="langTr"><em>turkish</em></a>
                <a href="http://persian.china.com/index.html" class="langIr"><em>persian</em></a>
                <a href="http://arabic.china.com/index.html" class="langAe"><em>arabic</em></a>
              </div>
            </div>
            <div class="page-search">
              <form id="web-search" name="web-search" method="get" action="http://www.google.com/search">
                <input type="hidden" name="sitesearch" value="english.china.com" />
                <input type="text" name="q" class="web-search-keyword" placeholder="Search here..." />
                <input type="submit" name="button" class="web-search-but" value="Search Site" />
              </form>
            </div>
      </div>
    </div>
  </div>
  <div class="page-nav-bg" id="page-nav">
    <ul class="page-nav maxWidth">
      <li class="page-openSearch"><a href="#"><em>Open search</em></a></li>
      <li class="small-logo"><a href="/index.html"><img src="/img/small-logo.png" /></a></li>
      <li><a href="/news/index.html">News</a></li>
      <li><a href="/video/index.html">Video</a></li>
      <!--<li><a href="/audio/index.html">Audio</a></li>-->
      <!--<li><a href="/photos/index.html">Photos</a></li>-->
      <li><a href="/travel/index.html">Travel</a></li>
      <!--<li><a href="/lifestyle/index.html">Lifestyle</a></li>-->
      <li><a href="/chinese/index.html">Learn Chinese</a></li>
	  <li><a href="http://english.china.com/cityguide/">City Guide</a></li>
      <li><a href="http://mail.china.com/en/" class="ext" target="_blank">Free Mail</a></li>
    </ul>
  </div>
</div><!-- page-head End -->
<!-- /etc/channelhomehead.shtml End -->

<div class="maxWidth">
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µè¶éå¹¿å1 #17286  Begin -->
<script type="text/javascript" src="http://dvser.china.com/s?z=china&c=206" charset="gbk" ></script>
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µè¶éå¹¿å1 End -->
</div>

<div class="page-main maxWidth">
  <div class="page-left">
    <div class="page-focus" id="page-focus">
      <div class="page-focus-body" id="page-focus-body">
        <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12394/2015/11/03/3521s902474.htm" title="PLA Holds Demining Drill along China-Vietnam Border"><img src="http://img03.mini.abroad.imgcdc.com/english/news/topphotos/china/189/20151103/496506_128854.jpg.680x330.jpg" width="680" height="330" alt="PLA Holds Demining Drill along China-Vietnam Border" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12394/2015/11/03/3521s902474.htm" title="PLA Holds Demining Drill along China-Vietnam Border" class="title_default">PLA Holds Demining Drill along China-Vietnam Border</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12394/2015/11/01/4021s902187.htm" title="Premier: Trilateral Talks Must be Based on Understanding"><img src="http://img03.mini.abroad.imgcdc.com/english/home/topphoto/1295/20151101/494990_128326.jpg.680x330.jpg" width="680" height="330" alt="Premier: Trilateral Talks Must be Based on Understanding" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12394/2015/11/01/4021s902187.htm" title="Premier: Trilateral Talks Must be Based on Understanding" class="title_default">Premier: Trilateral Talks Must be Based on Understanding</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12394/2015/11/02/4201s902333.htm" title="120-meter-long Romantic Wall in NE China Attracts Scores of Citizens"><img src="http://img02.mini.abroad.imgcdc.com/english/news/topphotos/china/189/20151103/495933_128649.jpg.680x330.jpg" width="680" height="330" alt="120-meter-long Romantic Wall in NE China Attracts Scores of Citizens" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12394/2015/11/02/4201s902333.htm" title="120-meter-long Romantic Wall in NE China Attracts Scores of Citizens" class="title_default">120-meter-long Romantic Wall in NE China Attracts Scores of Citizens</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://news.xinhuanet.com/english/photo/2015-11/03/c_134777423.htm" title=""The Day of the Dead" marked in Panama"><img src="http://img01.mini.abroad.imgcdc.com/english/news/topphotos/world/1208/20151103/495932_128648.jpg.680x330.jpg" width="565" height="250" alt=""The Day of the Dead" marked in Panama" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://news.xinhuanet.com/english/photo/2015-11/03/c_134777423.htm" title=""The Day of the Dead" marked in Panama" class="title_default">"The Day of the Dead" marked in Panama</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://www.chinadaily.com.cn/sports/2015-11/02/content_22341087.htm" title="Radwanska masters art of finishing on a high note"><img src="http://img01.mini.abroad.imgcdc.com/english/photos/sports/96/20151102/495064_128352.jpg.680x330.jpg" width="680" height="330" alt="Radwanska masters art of finishing on a high note" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://www.chinadaily.com.cn/sports/2015-11/02/content_22341087.htm" title="Radwanska masters art of finishing on a high note" class="title_default">Radwanska masters art of finishing on a high note</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div>
      </div>
      <div id="page-focus-console"></div>
      <div class="page-focus-prevnext">
        <a href="#" id="page-focus-prev">Previous focus</a>
        <a href="#" id="page-focus-next">Next focus</a>
      </div>
    </div><!-- page-focus End -->
    
    <div class="page-latest">
      <h2 class="modTit"><strong><a href="/news/index.html">LATEST NEWS</a></strong></h2>
      <div class="page-latest-body" id="page-latest">
        <div class="item-phototext ">
          <div class="item-photo"><a href="http://en.people.cn/n/2015/1103/c98649-8970827.html" title="Wingsuit Pilots Fly in Zhaotong"><img src="http://img04.abroad.imgcdc.com/english/news/sports/57/20151103/496509_128855_200x120.jpg" width="200" height="120" alt="Wingsuit Pilots Fly in Zhaotong" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2015/11/03 17:56:45</em><em class="hide">November 03 2015 18:29:59</em></div>
            <h3 class="item-tit"><a href="http://en.people.cn/n/2015/1103/c98649-8970827.html" title="Wingsuit Pilots Fly in Zhaotong" class="title_default">Wingsuit Pilots Fly in Zhaotong</a></h3>
            <p class="item-infor" title="Wingsuit pilots who attend the 4th AOPO International Fly Contest fly in Zhaotong, southwest Chinaâs Yunnan province on Nov. 2, 2015.">Wingsuit pilots who attend the 4th AOPO International Fly Contest fly in Zhaotong, southwest Chinaâs Yunnan province on Nov. 2, 2015.</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2015-11/03/c_134776501.htm" title="British Companies Anticipate Participation in Iran's Energy Industry"><img src="" width="" height="" alt="British Companies Anticipate Participation in Iran's Energy Industry" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2015/11/03 17:55:51</em><em class="hide">November 03 2015 18:29:59</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2015-11/03/c_134776501.htm" title="British Companies Anticipate Participation in Iran's Energy Industry" class="title_default">British Companies Anticipate Participation in Iran's Energy Industry</a></h3>
            <p class="item-infor" title="British Petroleum's (BP) head of Middle East operations, Michael Townshend, said Monday that BP is willing to return to Iran and resume its activities in oil and gas exploration, in addition to production projects.">British Petroleum's (BP) head of Middle East operations, Michael Townshend, said Monday that BP is willing to return to Iran and resume its activities in oil and gas exploration, in addition to production projects.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2015-11/03/c_134776504.htm" title="Chinese, French presidents reach deal on climate change"><img src="http://img03.abroad.imgcdc.com/english/news/china/54/20151103/496153_128726_200x120.jpg" width="200" height="120" alt="Chinese, French presidents reach deal on climate change" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/11/03 11:25:18</em><em class="hide">November 03 2015 18:29:59</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2015-11/03/c_134776504.htm" title="Chinese, French presidents reach deal on climate change" class="title_default">Chinese, French presidents reach deal on climate change</a></h3>
            <p class="item-infor" title="President Xi Jinping and his French counterpart Francois Hollande issued a joint statement on climate change, vowing to promote a working program to accelerate pre-2020 efforts in mitigation, adaptation and support during the Paris climate summit.">President Xi Jinping and his French counterpart Francois Hollande issued a joint statement on climate change, vowing to promote a working program to accelerate pre-2020 efforts in mitigation, adaptation and support during the Paris climate summit.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/11/03/2941s902404.htm" title="Guangzhou-based Company Acquires La Liga's Espanyol"><img src="http://img04.abroad.imgcdc.com/english/news/sports/57/20151103/496147_128723_200x120.jpg" width="200" height="120" alt="Guangzhou-based Company Acquires La Liga's Espanyol" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2015/11/03 11:23:26</em><em class="hide">November 03 2015 18:29:59</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/11/03/2941s902404.htm" title="Guangzhou-based Company Acquires La Liga's Espanyol" class="title_default">Guangzhou-based Company Acquires La Liga's Espanyol</a></h3>
            <p class="item-infor" title="Guangzhou-based tech company Rastar Group has acquired La Liga club Espanyol for 65 milllion Euros.">Guangzhou-based tech company Rastar Group has acquired La Liga club Espanyol for 65 milllion Euros.</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://www.shanghaidaily.com/world/Australia-bids-adieu-to-knights-and-dames/shdaily.shtml" title="Australia bids adieu to knights and dames"><img src="" width="" height="" alt="Australia bids adieu to knights and dames" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2015/11/03 11:20:30</em><em class="hide">November 03 2015 18:29:59</em></div>
            <h3 class="item-tit"><a href="http://www.shanghaidaily.com/world/Australia-bids-adieu-to-knights-and-dames/shdaily.shtml" title="Australia bids adieu to knights and dames" class="title_default">Australia bids adieu to knights and dames</a></h3>
            <p class="item-infor" title="Australiaâs pro-republic Prime Minister Malcolm Turnbull yesterday scrapped knights and dames from the nationâs honors system. ">Australiaâs pro-republic Prime Minister Malcolm Turnbull yesterday scrapped knights and dames from the nationâs honors system. </p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/business/2015-11/03/content_22352252.htm" title="ICBC to promote China's first homegrown passenger plane"><img src="http://img04.mini.abroad.imgcdc.com/english/news/business/56/20151103/496092_128699.jpg.200x120.jpg" width="160" height="103" alt="ICBC to promote China's first homegrown passenger plane" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2015/11/03 11:01:53</em><em class="hide">November 03 2015 18:29:59</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/business/2015-11/03/content_22352252.htm" title="ICBC to promote China's first homegrown passenger plane" class="title_default">ICBC to promote China's first homegrown passenger plane</a></h3>
            <p class="item-infor" title="China's biggest financial leasing company is to start promoting sales of the domestically built C919 airliner internationally.">China's biggest financial leasing company is to start promoting sales of the domestically built C919 airliner internationally.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://en.people.cn/n/2015/1102/c98649-8970404.html" title="Second Chinese Canoeing Contest Held in Hangzhou"><img src="http://img04.mini.abroad.imgcdc.com/english/news/sports/57/20151102/495827_128619.jpg.200x120.jpg" width="200" height="120" alt="Second Chinese Canoeing Contest Held in Hangzhou" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2015/11/02 21:09:04</em><em class="hide">November 03 2015 18:29:59</em></div>
            <h3 class="item-tit"><a href="http://en.people.cn/n/2015/1102/c98649-8970404.html" title="Second Chinese Canoeing Contest Held in Hangzhou" class="title_default">Second Chinese Canoeing Contest Held in Hangzhou</a></h3>
            <p class="item-infor" title="The second Chinese Canoeing Contest was held in the West Lake in Hangzhou, capital of east China's Zhejiang province lately. Many athletes participated in this contest and experienced the excitements and challenges.">The second Chinese Canoeing Contest was held in the West Lake in Hangzhou, capital of east China's Zhejiang province lately. Many athletes participated in this contest and experienced the excitements and challenges.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/world/2015livisitrok/2015-11/02/content_22341151.htm" title="Deal with History Responsibly, Li Urges Abe"><img src="http://img03.mini.abroad.imgcdc.com/english/news/world/55/20151102/495818_128610.jpg.200x120.jpg" width="200" height="120" alt="Deal with History Responsibly, Li Urges Abe" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2015/11/02 21:05:14</em><em class="hide">November 03 2015 18:29:59</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/world/2015livisitrok/2015-11/02/content_22341151.htm" title="Deal with History Responsibly, Li Urges Abe" class="title_default">Deal with History Responsibly, Li Urges Abe</a></h3>
            <p class="item-infor" title="Premier Li Keqiang has urged Japan to properly tackle the issue of history "in a responsible manner" and to "undertake positive policies toward China".">Premier Li Keqiang has urged Japan to properly tackle the issue of history "in a responsible manner" and to "undertake positive policies toward China".</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://en.people.cn/n/2015/1102/c90000-8970189.html" title="First China-developed Large Passenger Jet C919 Rolls off Line"><img src="http://img02.mini.abroad.imgcdc.com/english/news/china/54/20151102/495823_128613.jpg.200x120.jpg" width="200" height="120" alt="First China-developed Large Passenger Jet C919 Rolls off Line" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/11/02 21:04:47</em><em class="hide">November 03 2015 18:29:59</em></div>
            <h3 class="item-tit"><a href="http://en.people.cn/n/2015/1102/c90000-8970189.html" title="First China-developed Large Passenger Jet C919 Rolls off Line" class="title_default">First China-developed Large Passenger Jet C919 Rolls off Line</a></h3>
            <p class="item-infor" title="The first China-developed large passenger jet, the C919, built by the Commercial Aircraft Corporation of China Ltd. (COMAC), has rolled off the production line today, which marks a breakthrough in the history of China's aviation industry and new level of China's high-end equipment manufacturing. A grand ceremony is held in COMAC's factory in Shanghai to celebrate the great moment.">The first China-developed large passenger jet, the C919, built by the Commercial Aircraft Corporation of China Ltd. (COMAC), has rolled off the production line today, which marks a breakthrough in the history of China's aviation industry and new level of China's high-end equipment manufacturing. A grand ceremony is held in COMAC's factory in Shanghai to celebrate the great moment.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/11/02/4083s902339.htm" title=""The Last Women Standing" Premieres in Beijing"><img src="http://img03.mini.abroad.imgcdc.com/english/news/showbiz/58/20151102/495826_128614.jpg.200x120.jpg" width="200" height="120" alt=""The Last Women Standing" Premieres in Beijing" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/showbiz">Showbiz</a></strong><em>2015/11/02 21:01:03</em><em class="hide">November 03 2015 18:29:59</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/11/02/4083s902339.htm" title=""The Last Women Standing" Premieres in Beijing" class="title_default">"The Last Women Standing" Premieres in Beijing</a></h3>
            <p class="item-infor" title="The Chinese premiere for romance film "The Last Women Standing" was held in Beijing yesterday, with the leading stars Eddie Peng and Shu Qi showing up to promote their movie.">The Chinese premiere for romance film "The Last Women Standing" was held in Beijing yesterday, with the leading stars Eddie Peng and Shu Qi showing up to promote their movie.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/photo/2015-11/02/c_134773014_2.htm" title="Child models shine at China Fashion Week"><img src="http://img03.abroad.imgcdc.com/english/news/showbiz/58/20151102/495098_128358_200x120.jpg" width="200" height="120" alt="Child models shine at China Fashion Week" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/showbiz">Showbiz</a></strong><em>2015/11/02 09:09:51</em><em class="hide">November 03 2015 18:29:59</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/photo/2015-11/02/c_134773014_2.htm" title="Child models shine at China Fashion Week" class="title_default">Child models shine at China Fashion Week</a></h3>
            <p class="item-infor" title="Child models show creations of Showkids Chen Juanhong Children's Wear Collection during China Fashion Week in Beijing. ">Child models show creations of Showkids Chen Juanhong Children's Wear Collection during China Fashion Week in Beijing. </p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/11/02/2941s902243.htm" title="You Hao Wins Gold Medal in Men's Parallel Bars"><img src="http://img04.abroad.imgcdc.com/english/news/sports/57/20151102/495075_128355_200x120.jpg" width="200" height="120" alt="You Hao Wins Gold Medal in Men's Parallel Bars" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2015/11/02 09:00:17</em><em class="hide">November 03 2015 18:29:59</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/11/02/2941s902243.htm" title="You Hao Wins Gold Medal in Men's Parallel Bars" class="title_default">You Hao Wins Gold Medal in Men's Parallel Bars</a></h3>
            <p class="item-infor" title="You Hao has won the gold medal in men's parallel bars at the World Gymnastics Championships in Glasgow and exerted China's dominance at this event.">You Hao has won the gold medal in men's parallel bars at the World Gymnastics Championships in Glasgow and exerted China's dominance at this event.</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://www.shanghaidaily.com/metro/society/Courier-firms-ignore-notice-amid-fears-over-personal-data-leak/shdaily.shtml" title="Courier firms ignore notice amid fears over personal data leak"><img src="" width="" height="" alt="Courier firms ignore notice amid fears over personal data leak" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/11/02 08:59:16</em><em class="hide">November 03 2015 18:29:59</em></div>
            <h3 class="item-tit"><a href="http://www.shanghaidaily.com/metro/society/Courier-firms-ignore-notice-amid-fears-over-personal-data-leak/shdaily.shtml" title="Courier firms ignore notice amid fears over personal data leak" class="title_default">Courier firms ignore notice amid fears over personal data leak</a></h3>
            <p class="item-infor" title="Courier firms in Shanghai are ignoring Chinaâs directive to only accept packages with real names and ID for deliveries even as a survey found that most people were hesitant in releasing their personal details.">Courier firms in Shanghai are ignoring Chinaâs directive to only accept packages with real names and ID for deliveries even as a survey found that most people were hesitant in releasing their personal details.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/11/02/3561s902233.htm" title="China will Buy 100 Helicopters from Airbus"><img src="http://img03.abroad.imgcdc.com/english/news/business/56/20151102/495067_128354_200x120.jpg" width="200" height="120" alt="China will Buy 100 Helicopters from Airbus" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2015/11/02 08:57:04</em><em class="hide">November 03 2015 18:29:59</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/11/02/3561s902233.htm" title="China will Buy 100 Helicopters from Airbus" class="title_default">China will Buy 100 Helicopters from Airbus</a></h3>
            <p class="item-infor" title="Airbus Helicopters, a unit of Airbus Group SE, said that China will buy 100 helicopters over the next 10 years from them.">Airbus Helicopters, a unit of Airbus Group SE, said that China will buy 100 helicopters over the next 10 years from them.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2015-11/01/c_134772157.htm" title="Investigations on Russian plane crash in Egypt start"><img src="http://img02.abroad.imgcdc.com/english/news/world/55/20151102/495065_128353_200x120.jpg" width="200" height="120" alt="Investigations on Russian plane crash in Egypt start" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2015/11/02 08:55:20</em><em class="hide">November 03 2015 18:29:59</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2015-11/01/c_134772157.htm" title="Investigations on Russian plane crash in Egypt start" class="title_default">Investigations on Russian plane crash in Egypt start</a></h3>
            <p class="item-infor" title="Egyptian and international investigators have begun probing the reasons of a Russian passenger plane that crashed in Egypt's Sinai Peninsula on Saturday. ">Egyptian and international investigators have begun probing the reasons of a Russian passenger plane that crashed in Egypt's Sinai Peninsula on Saturday. </p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/11/01/3821s902184.htm" title="Hebei China Fortune Elevated to the Chinese Super League"><img src="http://img04.mini.abroad.imgcdc.com/english/news/sports/57/20151101/495001_128339.jpg.200x120.jpg" width="200" height="120" alt="Hebei China Fortune Elevated to the Chinese Super League" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2015/11/01 21:04:11</em><em class="hide">November 03 2015 18:29:59</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/11/01/3821s902184.htm" title="Hebei China Fortune Elevated to the Chinese Super League" class="title_default">Hebei China Fortune Elevated to the Chinese Super League</a></h3>
            <p class="item-infor" title="The Chinese Super League slots vacated by Guizhou Renhe and Shanghai Shenxin have now been filled.2nd division side Hebei China Fortune is moving up to the Super League next season.
">The Chinese Super League slots vacated by Guizhou Renhe and Shanghai Shenxin have now been filled.2nd division side Hebei China Fortune is moving up to the Super League next season.
</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/11/01/3123s902164.htm" title=""Blind Message" Wins Big at 15th Chinese Film Media Awards"><img src="http://img02.mini.abroad.imgcdc.com/english/news/showbiz/58/20151101/494999_128337.jpg.200x120.jpg" width="200" height="120" alt=""Blind Message" Wins Big at 15th Chinese Film Media Awards" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/showbiz">Showbiz</a></strong><em>2015/11/01 21:01:20</em><em class="hide">November 03 2015 18:29:59</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/11/01/3123s902164.htm" title=""Blind Message" Wins Big at 15th Chinese Film Media Awards" class="title_default">"Blind Message" Wins Big at 15th Chinese Film Media Awards</a></h3>
            <p class="item-infor" title="Director Lou Ye's film "Blind Message" has emerged the big winner from last night's Chinese Film Media Awards.">Director Lou Ye's film "Blind Message" has emerged the big winner from last night's Chinese Film Media Awards.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/11/01/3441s902155.htm" title="China's Manufacturing PMI Holds Steady in October"><img src="http://img01.mini.abroad.imgcdc.com/english/news/business/56/20151101/494998_128336.jpg.200x120.jpg" width="200" height="120" alt="China's Manufacturing PMI Holds Steady in October" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2015/11/01 21:00:06</em><em class="hide">November 03 2015 18:29:59</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/11/01/3441s902155.htm" title="China's Manufacturing PMI Holds Steady in October" class="title_default">China's Manufacturing PMI Holds Steady in October</a></h3>
            <p class="item-infor" title="China's factory activity held steady in October as the country's manufacturing sector saw improved but still weak demand, official data showed Sunday. ">China's factory activity held steady in October as the country's manufacturing sector saw improved but still weak demand, official data showed Sunday. </p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/11/01/4082s902181.htm" title="Investigation Underway for Russian Airliner Crash"><img src="http://img01.mini.abroad.imgcdc.com/english/news/world/55/20151101/494994_128332.jpg.200x120.jpg" width="200" height="120" alt="Investigation Underway for Russian Airliner Crash" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2015/11/01 20:55:00</em><em class="hide">November 03 2015 18:29:59</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/11/01/4082s902181.htm" title="Investigation Underway for Russian Airliner Crash" class="title_default">Investigation Underway for Russian Airliner Crash</a></h3>
            <p class="item-infor" title="An official day of mourning is underway in Russia following the crash of a Russian airliner on Saturday in the Egyptian Sinai.">An official day of mourning is underway in Russia following the crash of a Russian airliner on Saturday in the Egyptian Sinai.</p>
          </div>
        </div><div class="item-phototext item-onlytext">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/11/01/4061s902179.htm" title="China-Japan-South Korea Leaders Holds Talks in Seoul"><img src="" width="" height="" alt="China-Japan-South Korea Leaders Holds Talks in Seoul" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/11/01 20:53:59</em><em class="hide">November 03 2015 18:29:59</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/11/01/4061s902179.htm" title="China-Japan-South Korea Leaders Holds Talks in Seoul" class="title_default">China-Japan-South Korea Leaders Holds Talks in Seoul</a></h3>
            <p class="item-infor" title="This weekend's China-Japan-South Korea leaders' meetings have taken place in Seoul.">This weekend's China-Japan-South Korea leaders' meetings have taken place in Seoul.</p>
          </div>
        </div>
      </div>
      <div class="page-latest-more">
        <a href="#" id="page-latest-show-more"><em class="page-latest-more-icon">&nbsp;</em></a>
        <a href="/news/index.html" id="page-latest-click-more"><em class="page-latest-more-icon">Show More</em></a>
      </div>
    </div><!-- page-latest End -->
    <div class="page-left-ad">
	<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µéæ å¹¿å1 #17286 Begin -->
<script type="text/javascript" src="http://dvser.china.com/s?z=china&c=207" charset="gbk" ></script>
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µéæ å¹¿å1 End -->
	</div> 
    <div class="page-video">
      <h2 class="modTit"><strong><a href="/video/index.html">VIDEO</a></strong></h2>
      <div class="page-video-body">
        <div class="page-video-left">
          <script type="text/javascript" src="http://c.wrating.com/v2_pre.js"></script>
<!--noscript-->
          <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="420" height="234">
            <param name="movie" value="http://english.china.com/videoPlayer/video.swf"/>
            <param name="quality" value="high"/>
            <param name="bgcolor" value="#ffffff"/>
            <param name="allowScriptAccess" value="sameDomain"/>
            <param name="allowFullScreen" value="true"/>
            <param name="wmode" value="Opaque">
            <param name="flashvars" value="sw=420&sh=234&url=http://mod.cri.cn/eng/video/chinarevealed/2015/10/1019todayinhistory.mp4&loop=1&autoplay=0"/>
            <!--[if!IE]>
            -->
            <object type="application/x-shockwave-flash" data="http://english.china.com/videoPlayer/video.swf" width="420" height="234">
              <param name="quality" value="high"/>
              <param name="bgcolor" value="#ffffff"/>
              <param name="allowScriptAccess" value="sameDomain"/>
              <param name="allowFullScreen" value="true"/>
              <param name="wmode" value="Opaque">
              <param name="flashvars" value="sw=420&sh=234&url=http://mod.cri.cn/eng/video/chinarevealed/2015/10/1019todayinhistory.mp4&loop=1&autoplay=0"/>
              <!--<![endif]-->
              <!--[if gte IE 6]>
              -->
              <p>
                Either scripts and active content are not permitted to run or Adobe Flash Player version 11.4.0 or greater is not installed.
              </p>
              <!--<![endif]-->
              <a href="http://www.adobe.com/go/getflashplayer">
                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash Player"/>
              </a>
              <!--[if!IE]>--></object>
            <!--<![endif]-->
          </object>
          <!--/noscript-->
          <a href="/home/videobig/1299/20151019/485195.html" class="video-tit">Today in History: The War against Japanese Aggression, 1937.10.19</a>
        </div>
        <div class="page-video-right" id="page-video-right">
          <div class="item-video">
            <div class="item-photo">
              <a href="http://english.china.com/video/life/2649/20151028/491957.html"><img src="http://img04.abroad.imgcdc.com/english/home/videosmall/1301/20151028/491968_127479.jpg" width="245" height="125" alt="Along the Silk Road:Dev Raturi--Indian Ingenuity" /></a>
            </div>
            <a href="http://english.china.com/video/life/2649/20151028/491957.html" class="item-text">
              <h3 class="item-tit"><strong class="title_default">Along the Silk Road:Dev Raturi--Indian Ingenuity</strong></h3>
              <p class="item-infor">Dev brings India's unique culture to China, showcasing its food, and rediscovering Silk Road ties.</p>
              <div class="item-type">
                <span class="item-icon"></span>
                <span class="item-watch">watch</span>
              </div>
            </a>
          </div><div class="item-video">
            <div class="item-photo">
              <a href="http://english.china.com/video/chinarevealed/72/20150916/462850.html"><img src="http://img04.abroad.imgcdc.com/english/home/videosmall/1301/20150916/462873_118431.jpg" width="245" height="125" alt="China-USA: Building Careers" /></a>
            </div>
            <a href="http://english.china.com/video/chinarevealed/72/20150916/462850.html" class="item-text">
              <h3 class="item-tit"><strong class="title_default">China-USA: Building Careers</strong></h3>
              <p class="item-infor">American and Chinese professionals explain the benefits of working and studying in each other's countries.</p>
              <div class="item-type">
                <span class="item-icon"></span>
                <span class="item-watch">watch</span>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div><!-- page-video End -->
    <div class="page-mods">
      <div class="page-mod-item">
        <h2 class="modTit"><strong><a href="/travel/index.html">TRAVEL</a></strong></h2>
        <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://www.timeoutbeijing.com/features/Food__Drink-Food_awards_2015/38233/Food-Awards-2015-the-winners.html" title="Food Awards 2015: the winners"><img src="http://img03.mini.abroad.imgcdc.com/english/travel/listright/mostpopular/1534/20150506/366181_88898.jpg.330x190.jpg" width="330" height="190" alt="Food Awards 2015: the winners" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://www.timeoutbeijing.com/features/Food__Drink-Food_awards_2015/38233/Food-Awards-2015-the-winners.html" title="Food Awards 2015: the winners" class="title_default">Food Awards 2015: the winners</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div>
        <div class="newslist">
          <ul>
            <li><a href="http://english.cri.cn/12394/2015/08/11/2743s891245.htm" title="Japanese War Orphan Recounts Past" class="title_default">Japanese War Orphan Recounts Past</a></li><li><a href="http://english.cri.cn/6566/2014/12/25/44s858261.htm" title="Winter Nadam Kicks Off in North China" class="title_default">Winter Nadam Kicks Off in North China</a></li><li><a href="http://english.cri.cn/6566/2014/09/29/44s845986.htm" title="To Experience Authentic Taiwan Folk Art in Beijing" class="title_default">To Experience Authentic Taiwan Folk Art in Beijing</a></li>
          </ul>
        </div>

      </div>
      <div class="page-mod-item">
        <h2 class="modTit"><strong><a href="/chinese/index.html">LEARN CHINESE</a></strong></h2>
        <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12514/2014/11/24/2001s853655.htm" title="China's First Love"><img src="http://img03.mini.abroad.imgcdc.com/english/home/learnpic/1315/20141124/211729_51886.jpg.330x190.jpg" width="330" height="190" alt="China's First Love" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12514/2014/11/24/2001s853655.htm" title="China's First Love" class="title_default">China's First Love</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div>
        <div class="newslist">
          <ul>
            <li><a href="http://english.cri.cn/12514/2014/10/16/2001s848110.htm" title="Top 10 Popular Chinese TV Dramas Overseas" class="title_default">Top 10 Popular Chinese TV Dramas Overseas</a></li><li><a href="http://english.cri.cn/12514/2014/10/17/2001s848240.htm" title="çµç¶ Chinese Pipa" class="title_default">çµç¶ Chinese Pipa</a></li><li><a href="http://english.cri.cn/12514/2014/09/25/2001s845407.htm" title="Useful Shopping Sentences in Chinese" class="title_default">Useful Shopping Sentences in Chinese</a></li>
          </ul>
        </div>

      </div>
      
      
    </div><!-- page-mods End -->
    <!--<div class="page-left-ad"><a href="#"><img src="/file/left-ad-2.jpg" /></a></div> page-left-ad End -->
    <!-- page-photos End -->
  </div><!-- page-left End -->
  <div class="page-right">
    
    <div class="page-right-ad noMarginTop">
      <!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µç»ä¸­ç»1 #17286 Begin -->
<script type="text/javascript" src="http://dvser.china.com/s?z=china&c=208" charset="gbk" ></script>
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µç»ä¸­ç»1 End -->
    </div><!-- page-right-ad End -->
    <!--include virtual="/etc/right_top_ad_index.shtml" -->
      
    ï»¿<!-- /home/imgtj/index.html CMSID:5828 Start -->
<div class="page-right-ad">
	<a href="http://english.cri.cn/12394/2015/09/21/Zt2821s896890.htm"><img src="http://img04.abroad.imgcdc.com/english/home/imgtj/5829/20151014/482283_124639.jpg" width="293" height="88" alt="20151014" /></a><a href="http://english.cri.cn/12394/2015/09/02/Zt2821s894283.htm"><img src="http://img01.abroad.imgcdc.com/english/home/imgtj/5829/20150908/482287_124640.jpg" width="293" height="88" alt="20150908" /></a>
</div>
<!-- /home/imgtj/index.html CMSID:5828 End --><!-- #15734 -->

    <!-- page-right-ad End -->
    
    <div class="page-watched">
      <h2 class="modTit"><strong>Most Watched</strong></h2>
      <div class="page-watched-body" id="rank-video">
      </div>
    </div><!-- page-watched End -->
    <div class="page-popular">
      <h2 class="modTit"><strong>Most Popular</strong></h2>
      <ul class="page-popular-body" id="rank-list"></ul>
    </div><!-- page-popular End -->
    <div class="page-right-ad">
	<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µæé®å¹¿å1 #17286 Begin -->
<script type="text/javascript" src="http://dvser.china.com/s?z=china&c=209" charset="gbk" ></script>
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µæé®å¹¿å1 End -->
	</div> 
    <div class="page-tochina">
      <h2 class="modTit"><strong>Tune in to China</strong></h2>
      <div class="page-tochina-body">
        <div class="item radio-news">
          <div class="item-title"></div>
          <div class="item-body">
            <div class="item-infor">
              <h3 id="title">-</h3>
              <h4 id="playtime">-</h4>
              <a href="http://english.cri.cn/7146/2012/12/03/301s736372.htm" class="full"></a>
            </div>
            <a href="mms://livexwb.cri.com.cn/am846.wsx" class="item-play"></a>
          </div>
        </div>
        <div class="item radio-ez">
          <div class="item-title"></div>
          <div class="item-body">
            <div class="item-infor">
              <h3 id="nexttitle">-</h3>
              <h4 id="nexttime">-</h4>
              <a href="http://english.cri.cn/easyfm/ezplaytime.html" class="full"></a>
            </div>
            <a href="mms://livexwb.cri.com.cn/fm915.wsx" class="item-play"></a>
          </div>
        </div>
      </div>
    </div><!-- page-tochina End -->

    <!-- /home/imgtj/index.html CMSID:5828 Start -->
<div class="page-right-ad">
	<a href="http://www.chinahighlights.com/tour/beijingtour/forbidden-city-heritage-walk.htm"><img src="http://img02.abroad.imgcdc.com/english/home/imgtj2/11771/20150729/427302_107797.gif" width="300" height="250" alt="ææ¸¸åä½æ¨è" /></a>
</div>
<!-- /home/imgtj/index.html CMSID:5828 End --><!-- #15734 -->
    <div class="page-hotListening">
      <h2 class="page-hotListening-tit"><strong>Hot Listening</strong></h2>
      <div class="page-hotListening-body">
        <div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/cribb/plus/pik.htm"><img src="http://img03.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4933_1370.jpg" width="120" height="90" alt="People in the Know" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/cribb/plus/pik.htm" class="title_default">People in the Know</a><a href="http://english.cri.cn/cribb/plus/pik.htm" class="icon-horn"></a></h3>
            <p class="item-infor">PIK gives you insights to the world through interviews.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/cribb/plus/today.htm"><img src="http://img02.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4931_1369.jpg" width="120" height="90" alt="Today" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/cribb/plus/today.htm" class="title_default">Today</a><a href="http://english.cri.cn/cribb/plus/today.htm" class="icon-horn"></a></h3>
            <p class="item-infor">A news magazine show with in-depth panel discussions.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/easyfm/easymorning.html"><img src="http://img01.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4928_1368.jpg" width="120" height="90" alt="EZ Morning" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/easyfm/easymorning.html" class="title_default">EZ Morning</a><a href="http://english.cri.cn/easyfm/easymorning.html" class="icon-horn"></a></h3>
            <p class="item-infor">It lights up your mornings with interesting chit-chats.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/easyfm/ezwheel.html"><img src="http://img04.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4927_1367.jpg" width="120" height="90" alt="More to Learn" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/easyfm/ezwheel.html" class="title_default">More to Learn</a><a href="http://english.cri.cn/easyfm/ezwheel.html" class="icon-horn"></a></h3>
            <p class="item-infor">More to Learn is filled up with English stories and anecdotes.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/easyfm/hour.html"><img src="http://img03.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4923_1366.jpg" width="120" height="90" alt="The Beijing Hour" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/easyfm/hour.html" class="title_default">The Beijing Hour</a><a href="http://english.cri.cn/easyfm/hour.html" class="icon-horn"></a></h3>
            <p class="item-infor">It opens up the world to you with latest news updates.</p>
          </div>
        </div>
      </div>
    </div><!-- page-hotListening End -->

    <div class="page-mobile">
      <h2 class="modTit"><strong>Mobile</strong></h2>
      <div class="page-mobile-body">
        <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=539404062&mt=8&s=143441" class="iphone" title="Mobile Iphone">Iphone</a>
      </div>
    </div><!-- page-mobile End -->

    <div class="page-cooperation">
      <h2 class="modTit"><strong>Cooperation</strong></h2>
      <div class="page-cooperation-body">
        <a href="http://gc.wrating.com/click.php?a=&c=860099-1000099998&cs=341_285_2559_860010_400000000&ds=354_355_356_357_358&url=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps%2Fdetails%3Fid%3Dmobi.mgeek.TunnyBrowser%26referrer%3Dchannel_id%253Dchinacom%2526utm_source%253Dchinacom"><img src="/file/logo-dolphin-140-90.png" height="90" width="140"></a>
      </div>
    </div><!-- page-cooperation End -->

  </div><!-- page-right End -->
</div><!-- page-main End -->

<script>
window.collectMethod_rank = window.collectMethod_rank || [];
collectMethod_rank.push(function () {
  setRank("rank-video", 3, "http://english.china.com/js/english_43_day.js", "video", function(){
    setRank("rank-list", 5, "http://english.china.com/js/english_22_day.js", "list");
  });
});
</script>

<!-- /etc/channelsitemap.shtml Start -->
<div class="page-map">
  <div class="page-map-body maxWidth">
  <dl class="item">
      <dt><a href="http://english.china.com/news/index.html">News:</a></dt>
      <dd>
        <a href="http://english.china.com/news/china/index.html">China</a>
        <a href="http://english.china.com/news/world/index.html">World</a>
        <a href="http://english.china.com/news/business/index.html">Business</a>
        <a href="http://english.china.com/news/sports/index.html">Sports</a>
        <a href="http://english.china.com/news/showbiz/index.html">Showbiz</a>
		<a href="http://english.china.com/audio/index.html">Audio</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/video/index.html">Video:</a></dt>
      <dd>
        <a href="http://english.china.com/video/c4/index.html">C4</a>
        <a href="http://english.china.com/video/life/index.html">My Chinese Life</a>
        <a href="http://english.china.com/video/thesoundstage/index.html">The Sound Stage</a>
        <a href="http://english.china.com/video/chinarevealed/index.html">China Revealed</a>
        <a href="http://english.china.com/video/showbiz/index.html">Showbiz Video</a>
        <a href="http://english.china.com/video/tour/index.html">Travel Video</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/photos/index.html">Photos:</a></dt>
      <dd>
        <a href="http://english.china.com/photos/china/index.html">China</a>
        <a href="http://english.china.com/photos/world/index.html">World</a>
        <a href="http://english.china.com/photos/fun/index.html">Fun</a>
        <a href="http://english.china.com/photos/travel/index.html">Travel</a>
        <a href="http://english.china.com/photos/entertainment/index.html">Entertainment</a>
        <a href="http://english.china.com/photos/sports/index.html">Sports</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/travel/index.html">Travel:</a></dt>
      <dd>
        <a href="http://english.china.com/travel/beijing/index.html">Beijing</a>
        <a href="http://english.china.com/travel/shanghai/index.html">Shanghai</a>
        <a href="http://english.china.com/travel/guangzhou/index.html">Guangzhou</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/lifestyle/index.html">Lifestyle:</a></dt>
      <dd>        
        <a href="http://english.china.com/lifestyle/livemusic/index.html">Live Music</a>
        <a href="http://english.china.com/lifestyle/opera/index.html">Opera & Classical</a>
        <a href="http://english.china.com/lifestyle/movies/index.html">Movies</a>
        <a href="http://english.china.com/lifestyle/traditionalshows/index.html">Traditional Shows</a>
        <a href="http://english.china.com/lifestyle/exhibitions/index.html">Exhibitions</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/chinese/index.html">Learn Chinese:</a></dt>
      <dd>
        <a href="http://english.china.com/chinese/studio/index.html">Chinese Studio</a>
        <a href="http://english.china.com/chinese/living/index.html">Living Chinese</a>
        <a href="http://english.china.com/chinese/everyday/index.html">Everyday Chinese</a>
        <a href="http://english.china.com/chinese/justforfun/index.html">Just For Fun</a>
        <a href="http://english.china.com/chinese/culture/index.html">Chinese Culture</a>
        <a href="http://english.china.com/chinese/buzzwords/index.html">Buzzwords</a>        
      </dd>
    </dl>      
  </div>
</div><!-- page-map End -->
<!-- /etc/channelsitemap.shtml End -->

<div class="page-link">
  <div class="page-link-body maxWidth">
    <a href="http://english.cri.cn/">CRIENGLISH.com</a>|<a href="http://www.chinadaily.com.cn/">China Daily</a>|<a href="http://www.xinhuanet.com/english/">Xinhua</a>|<a href="http://www.china.org.cn/index.htm">China.org.cn</a>|<a href="http://english.cntv.cn/">CNTV</a>
  </div>
</div><!-- page-link End -->

<!-- /etc/channelcopyright.shtml Start -->
<div class="page-footer">
  <div class="page-foot-link">
    <a href="/about/">About China.com</a>|<a href="/about/gmg.html">About GMG</a>|<a href="/ad/">Ad Services</a>|<a href="/contact/">Contact Information</a>|<a href="/copyright/">Copyright Notice</a>
  </div>
  <p><!--E-mail to:<a href="mailto:english@bj.china.com">english@bj.china.com</a><br />-->Copyright &copy; China.com All Rights Reserved</p>
</div><!-- page-footer End -->

<script src="/js/require.min.js" data-main="/js/main"></script>

<!-- START WRating v1.0 -->
<script type="text/javascript" src="http://c.wrating.com/a1.js">
</script>
<script type="text/javascript">
var vjAcc="860010-0446010000";
var wrUrl="http://c.wrating.com/";
vjTrack("");
</script>
<noscript><img src="http://c.wrating.com/a.gif?a=&c=860010-0446010000" width="1" 
height="1"/></noscript>
<!-- END WRating v1.0 -->

<!-- Start Alexa Certify Javascript #13481-->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"S6Upi1awA+00a/", domain:"china.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=S6Upi1awA+00a/" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript #13481-->


<!-- Start Google Analytics #16010-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60581848-2', 'auto');
  ga('send', 'pageview');

</script>
<!-- End Google Analytics #16010-->

<!-- /etc/channelcopyright.shtml End -->
</body>
</html>