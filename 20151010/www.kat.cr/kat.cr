<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-fe96285.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-fe96285.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-fe96285.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-fe96285.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kat.cr/sc-fe96285.js');

        sc('setHost', 'a.kat.cr');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: 'fe96285',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-fe96285.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<div id="_60318cd4e8d28f6fb76fe34e9bd9c498"></div>
<div id="_39ecb76dd457e5ac33776fdf11500d56"></div>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka">e</i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka">g</i></button></div>
			</form>
		</div>
        <div id="_277923e5f9d753c5b0630c28e641790c"></div>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080p/" class="tag5">1080p</a>
	<a href="/search/2014/" class="tag2">2014</a>
	<a href="/search/2015/" class="tag10">2015</a>
	<a href="/search/3d/" class="tag3">3d</a>
	<a href="/search/american%20horror%20story/" class="tag6">american horror story</a>
	<a href="/search/american%20horror%20story%20s05e01/" class="tag3">american horror story s05e01</a>
	<a href="/search/android/" class="tag2">android</a>
	<a href="/search/ant%20man/" class="tag2">ant man</a>
	<a href="/search/arrow/" class="tag5">arrow</a>
	<a href="/search/arrow%20s04e01/" class="tag8">arrow s04e01</a>
	<a href="/search/bahubali/" class="tag2">bahubali</a>
	<a href="/search/breaking/" class="tag2">breaking</a>
	<a href="/search/criminal%20minds/" class="tag2">criminal minds</a>
	<a href="/search/discography/" class="tag3">discography</a>
	<a href="/search/empire/" class="tag3">empire</a>
	<a href="/search/everest/" class="tag2">everest</a>
	<a href="/search/fear%20the%20walking%20dead/" class="tag2">fear the walking dead</a>
	<a href="/search/french/" class="tag2">french</a>
	<a href="/search/heroes%20reborn/" class="tag2">heroes reborn</a>
	<a href="/search/hindi/" class="tag7">hindi</a>
	<a href="/search/hindi%202015/" class="tag4">hindi 2015</a>
	<a href="/search/hindi%20movies%202015/" class="tag5">hindi movies 2015</a>
	<a href="/search/hotel%20transylvania/" class="tag2">hotel transylvania</a>
	<a href="/search/kat%20ph%20com/" class="tag10">kat ph com</a>
	<a href="/search/limitless/" class="tag2">limitless</a>
	<a href="/search/mad%20max/" class="tag2">mad max</a>
	<a href="/search/malayalam/" class="tag3">malayalam</a>
	<a href="/search/modern%20family/" class="tag2">modern family</a>
	<a href="/search/nezu/" class="tag9">nezu</a>
	<a href="/search/ripsalot/" class="tag5">ripsalot</a>
	<a href="/search/season%204%20arrow/" class="tag4">season 4 arrow</a>
	<a href="/search/sicario/" class="tag2">sicario</a>
	<a href="/search/south%20park/" class="tag3">south park</a>
	<a href="/search/supernatural/" class="tag2">supernatural</a>
	<a href="/search/supernatural%20s11e01/" class="tag2">supernatural s11e01</a>
	<a href="/search/supernatural%20season%2011/" class="tag2">supernatural season 11</a>
	<a href="/search/tamil/" class="tag4">tamil</a>
	<a href="/search/telugu/" class="tag3">telugu</a>
	<a href="/search/telugu%202015/" class="tag2">telugu 2015</a>
	<a href="/search/the%20blacklist/" class="tag2">the blacklist</a>
	<a href="/search/the%20flash/" class="tag2">the flash</a>
	<a href="/search/the%20flash%20s02e01%20hdtv%20x264/" class="tag2">the flash s02e01 hdtv x264</a>
	<a href="/search/the%20martian/" class="tag3">the martian</a>
	<a href="/search/the%20walking%20dead/" class="tag3">the walking dead</a>
	<a href="/search/undefined/" class="tag5">undefined</a>
	<a href="/search/yify/" class="tag8">yify</a>
	<a href="/search/yify%201080p/" class="tag3">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag4">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11376021,0" class="icommentjs icon16" href="/dope-2015-720p-brrip-x264-yify-t11376021.html#comment"> <em class="iconvalue">113</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dope-2015-720p-brrip-x264-yify-t11376021.html" class="cellMainLink">Dope (2015) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="846106977">806.91 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T01:43:02+00:00">08 Oct 2015, 01:43:02</span></td>
			<td class="green center">25521</td>
			<td class="red lasttd center">18451</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11366577,0" class="icommentjs icon16" href="/paper-towns-2015-1080p-brrip-x264-yify-t11366577.html#comment"> <em class="iconvalue">92</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/paper-towns-2015-1080p-brrip-x264-yify-t11366577.html" class="cellMainLink">Paper Towns (2015) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="1758831743">1.64 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T02:57:07+00:00">06 Oct 2015, 02:57:07</span></td>
			<td class="green center">12855</td>
			<td class="red lasttd center">11717</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11382078,0" class="icommentjs icon16" href="/knock-knock-2015-hdrip-xvid-etrg-t11382078.html#comment"> <em class="iconvalue">45</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/knock-knock-2015-hdrip-xvid-etrg-t11382078.html" class="cellMainLink">Knock Knock 2015 HDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="743049711">708.63 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-09T07:29:19+00:00">09 Oct 2015, 07:29:19</span></td>
			<td class="green center">8901</td>
			<td class="red lasttd center">9709</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11380908,0" class="icommentjs icon16" href="/max-2015-720p-brrip-x264-yify-t11380908.html#comment"> <em class="iconvalue">37</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/max-2015-720p-brrip-x264-yify-t11380908.html" class="cellMainLink">Max (2015) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="856319829">816.65 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-09T01:56:44+00:00">09 Oct 2015, 01:56:44</span></td>
			<td class="green center">6865</td>
			<td class="red lasttd center">6839</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11378853,0" class="icommentjs icon16" href="/cooties-2014-720p-brrip-x264-yify-t11378853.html#comment"> <em class="iconvalue">47</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/cooties-2014-720p-brrip-x264-yify-t11378853.html" class="cellMainLink">Cooties (2014) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="733107570">699.15 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T15:15:17+00:00">08 Oct 2015, 15:15:17</span></td>
			<td class="green center">7597</td>
			<td class="red lasttd center">4576</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11366784,0" class="icommentjs icon16" href="/z-for-zachariah-2015-720p-brrip-x264-yify-t11366784.html#comment"> <em class="iconvalue">62</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/z-for-zachariah-2015-720p-brrip-x264-yify-t11366784.html" class="cellMainLink">Z for Zachariah (2015) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="795037351">758.21 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T04:08:01+00:00">06 Oct 2015, 04:08:01</span></td>
			<td class="green center">6455</td>
			<td class="red lasttd center">3180</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11368232,0" class="icommentjs icon16" href="/the-vatican-tapes-2015-720p-brrip-x264-yify-t11368232.html#comment"> <em class="iconvalue">102</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-vatican-tapes-2015-720p-brrip-x264-yify-t11368232.html" class="cellMainLink">The Vatican Tapes (2015) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="738651170">704.43 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T11:45:52+00:00">06 Oct 2015, 11:45:52</span></td>
			<td class="green center">6001</td>
			<td class="red lasttd center">2363</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11377850,0" class="icommentjs icon16" href="/matchstick-men-2003-1080p-brrip-x264-yify-t11377850.html#comment"> <em class="iconvalue">16</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/matchstick-men-2003-1080p-brrip-x264-yify-t11377850.html" class="cellMainLink">Matchstick Men (2003) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="1968114053">1.83 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T10:50:34+00:00">08 Oct 2015, 10:50:34</span></td>
			<td class="green center">3980</td>
			<td class="red lasttd center">3915</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11370856,0" class="icommentjs icon16" href="/the-exorcism-of-molly-hartley-2015-bdrip-xvid-ac3-evo-t11370856.html#comment"> <em class="iconvalue">54</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-exorcism-of-molly-hartley-2015-bdrip-xvid-ac3-evo-t11370856.html" class="cellMainLink">The Exorcism of Molly Hartley 2015 BDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1483859900">1.38 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T00:15:23+00:00">07 Oct 2015, 00:15:23</span></td>
			<td class="green center">4151</td>
			<td class="red lasttd center">3023</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11375775,0" class="icommentjs icon16" href="/the-contract-2015-hdrip-xvid-ac3-evo-t11375775.html#comment"> <em class="iconvalue">13</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-contract-2015-hdrip-xvid-ac3-evo-t11375775.html" class="cellMainLink">The Contract 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1403620231">1.31 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T00:20:03+00:00">08 Oct 2015, 00:20:03</span></td>
			<td class="green center">3357</td>
			<td class="red lasttd center">2059</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11373832,0" class="icommentjs icon16" href="/the-jungle-book-howl-at-the-moon-2015-hdrip-xvid-ac3-evo-t11373832.html#comment"> <em class="iconvalue">20</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-jungle-book-howl-at-the-moon-2015-hdrip-xvid-ac3-evo-t11373832.html" class="cellMainLink">The Jungle Book Howl at the Moon 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1504273342">1.4 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T15:16:19+00:00">07 Oct 2015, 15:16:19</span></td>
			<td class="green center">2160</td>
			<td class="red lasttd center">2044</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11369097,0" class="icommentjs icon16" href="/max-2015-hdrip-xvid-ac3-evo-t11369097.html#comment"> <em class="iconvalue">36</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/max-2015-hdrip-xvid-ac3-evo-t11369097.html" class="cellMainLink">Max 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1516852503">1.41 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T15:47:55+00:00">06 Oct 2015, 15:47:55</span></td>
			<td class="green center">2257</td>
			<td class="red lasttd center">1398</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11384021,0" class="icommentjs icon16" href="/zero-tolerance-2015-hdrip-xvid-ac3-evo-t11384021.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/zero-tolerance-2015-hdrip-xvid-ac3-evo-t11384021.html" class="cellMainLink">Zero Tolerance 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1490183925">1.39 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-09T15:35:28+00:00">09 Oct 2015, 15:35:28</span></td>
			<td class="green center">1177</td>
			<td class="red lasttd center">3294</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11367918,0" class="icommentjs icon16" href="/berkshire-county-2014-720p-brrip-x264-yify-t11367918.html#comment"> <em class="iconvalue">31</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/berkshire-county-2014-720p-brrip-x264-yify-t11367918.html" class="cellMainLink">Berkshire County (2014) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="725894935">692.27 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T10:03:49+00:00">06 Oct 2015, 10:03:49</span></td>
			<td class="green center">2234</td>
			<td class="red lasttd center">854</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11383031,0" class="icommentjs icon16" href="/dragon-ball-z-resurrection-f-2015-hdrip-xvid-ac3-evo-t11383031.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dragon-ball-z-resurrection-f-2015-hdrip-xvid-ac3-evo-t11383031.html" class="cellMainLink">Dragon Ball Z Resurrection F 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1527497907">1.42 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-09T11:41:22+00:00">09 Oct 2015, 11:41:22</span></td>
			<td class="green center">1370</td>
			<td class="red lasttd center">1942</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11375899,0" class="icommentjs icon16" href="/arrow-s04e01-hdtv-x264-lol-ettv-t11375899.html#comment"> <em class="iconvalue">242</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/arrow-s04e01-hdtv-x264-lol-ettv-t11375899.html" class="cellMainLink">Arrow S04E01 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="278303976">265.41 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T01:01:14+00:00">08 Oct 2015, 01:01:14</span></td>
			<td class="green center">21831</td>
			<td class="red lasttd center">3208</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11370984,0" class="icommentjs icon16" href="/the-flash-2014-s02e01-hdtv-x264-lol-ettv-t11370984.html#comment"> <em class="iconvalue">371</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-flash-2014-s02e01-hdtv-x264-lol-ettv-t11370984.html" class="cellMainLink">The Flash 2014 S02E01 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="269859762">257.36 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T01:02:14+00:00">07 Oct 2015, 01:02:14</span></td>
			<td class="green center">18524</td>
			<td class="red lasttd center">2063</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11376323,0" class="icommentjs icon16" href="/american-horror-story-s05e01-hdtv-x264-killers-ettv-t11376323.html#comment"> <em class="iconvalue">146</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/american-horror-story-s05e01-hdtv-x264-killers-ettv-t11376323.html" class="cellMainLink">American Horror Story S05E01 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="466641021">445.02 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T03:33:14+00:00">08 Oct 2015, 03:33:14</span></td>
			<td class="green center">13939</td>
			<td class="red lasttd center">1973</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11370821,0" class="icommentjs icon16" href="/limitless-s01e03-hdtv-x264-lol-ettv-t11370821.html#comment"> <em class="iconvalue">170</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/limitless-s01e03-hdtv-x264-lol-ettv-t11370821.html" class="cellMainLink">Limitless S01E03 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="306373412">292.18 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T00:00:13+00:00">07 Oct 2015, 00:00:13</span></td>
			<td class="green center">12588</td>
			<td class="red lasttd center">1493</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11380752,0" class="icommentjs icon16" href="/the-vampire-diaries-s07e01-hdtv-x264-lol-rartv-t11380752.html#comment"> <em class="iconvalue">71</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-vampire-diaries-s07e01-hdtv-x264-lol-rartv-t11380752.html" class="cellMainLink">The Vampire Diaries S07E01 HDTV x264-LOL[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="285145662">271.94 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-09T00:57:05+00:00">09 Oct 2015, 00:57:05</span></td>
			<td class="green center">9486</td>
			<td class="red lasttd center">2516</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11376068,0" class="icommentjs icon16" href="/supernatural-s11e01-hdtv-x264-lol-ettv-t11376068.html#comment"> <em class="iconvalue">148</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/supernatural-s11e01-hdtv-x264-lol-ettv-t11376068.html" class="cellMainLink">Supernatural S11E01 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="269848369">257.35 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T01:59:15+00:00">08 Oct 2015, 01:59:15</span></td>
			<td class="green center">9923</td>
			<td class="red lasttd center">1302</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11366248,0" class="icommentjs icon16" href="/gotham-s02e03-hdtv-x264-lol-ettv-t11366248.html#comment"> <em class="iconvalue">171</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/gotham-s02e03-hdtv-x264-lol-ettv-t11366248.html" class="cellMainLink">Gotham S02E03 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="271833846">259.24 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T01:01:11+00:00">06 Oct 2015, 01:01:11</span></td>
			<td class="green center">9940</td>
			<td class="red lasttd center">1073</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11380953,0" class="icommentjs icon16" href="/the-blacklist-s03e02-hdtv-x264-killers-rartv-t11380953.html#comment"> <em class="iconvalue">54</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-blacklist-s03e02-hdtv-x264-killers-rartv-t11380953.html" class="cellMainLink">The Blacklist S03E02 HDTV x264-KILLERS[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="281354990">268.32 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-09T02:02:38+00:00">09 Oct 2015, 02:02:38</span></td>
			<td class="green center">9269</td>
			<td class="red lasttd center">2162</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11371158,0" class="icommentjs icon16" href="/marvels-agents-of-s-h-i-e-l-d-s03e02-hdtv-x264-killers-ettv-t11371158.html#comment"> <em class="iconvalue">154</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/marvels-agents-of-s-h-i-e-l-d-s03e02-hdtv-x264-killers-ettv-t11371158.html" class="cellMainLink">Marvels Agents of S H I E L D S03E02 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="289793968">276.37 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T02:04:15+00:00">07 Oct 2015, 02:04:15</span></td>
			<td class="green center">8875</td>
			<td class="red lasttd center">1176</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11376062,0" class="icommentjs icon16" href="/empire-2015-s02e03-hdtv-x264-killers-ettv-t11376062.html#comment"> <em class="iconvalue">37</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/empire-2015-s02e03-hdtv-x264-killers-ettv-t11376062.html" class="cellMainLink">Empire 2015 S02E03 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="395025476">376.73 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T01:58:13+00:00">08 Oct 2015, 01:58:13</span></td>
			<td class="green center">8013</td>
			<td class="red lasttd center">1580</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11376003,0" class="icommentjs icon16" href="/modern-family-s07e03-hdtv-x264-killers-ettv-t11376003.html#comment"> <em class="iconvalue">53</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/modern-family-s07e03-hdtv-x264-killers-ettv-t11376003.html" class="cellMainLink">Modern Family S07E03 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="162715443">155.18 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T01:35:14+00:00">08 Oct 2015, 01:35:14</span></td>
			<td class="green center">8306</td>
			<td class="red lasttd center">883</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11381288,0" class="icommentjs icon16" href="/heroes-reborn-s01e04-hdtv-x264-killers-ettv-t11381288.html#comment"> <em class="iconvalue">25</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/heroes-reborn-s01e04-hdtv-x264-killers-ettv-t11381288.html" class="cellMainLink">Heroes Reborn S01E04 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="321653248">306.75 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-09T04:02:11+00:00">09 Oct 2015, 04:02:11</span></td>
			<td class="green center">6881</td>
			<td class="red lasttd center">2202</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11380763,0" class="icommentjs icon16" href="/greys-anatomy-s12e03-hdtv-x264-fleet-rartv-t11380763.html#comment"> <em class="iconvalue">14</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/greys-anatomy-s12e03-hdtv-x264-fleet-rartv-t11380763.html" class="cellMainLink">Greys Anatomy S12E03 HDTV x264-FLEET[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="274860471">262.13 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-09T01:01:05+00:00">09 Oct 2015, 01:01:05</span></td>
			<td class="green center">7059</td>
			<td class="red lasttd center">1494</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11366396,0" class="icommentjs icon16" href="/blindspot-s01e03-hdtv-x264-lol-ettv-t11366396.html#comment"> <em class="iconvalue">121</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/blindspot-s01e03-hdtv-x264-lol-ettv-t11366396.html" class="cellMainLink">Blindspot S01E03 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="295582165">281.89 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T02:02:16+00:00">06 Oct 2015, 02:02:16</span></td>
			<td class="green center">6713</td>
			<td class="red lasttd center">809</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11380952,0" class="icommentjs icon16" href="/scandal-s05e03-hdtv-x264-fleet-rartv-t11380952.html#comment"> <em class="iconvalue">22</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/scandal-s05e03-hdtv-x264-fleet-rartv-t11380952.html" class="cellMainLink">Scandal S05E03 HDTV x264-FLEET[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="295588215">281.89 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-09T02:02:31+00:00">09 Oct 2015, 02:02:31</span></td>
			<td class="green center">5920</td>
			<td class="red lasttd center">1471</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11377762,0" class="icommentjs icon16" href="/selena-gomez-revival-deluxe-edition-2015-mp3-320kbps-h4ckus-glodls-t11377762.html#comment"> <em class="iconvalue">22</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/selena-gomez-revival-deluxe-edition-2015-mp3-320kbps-h4ckus-glodls-t11377762.html" class="cellMainLink">Selena Gomez - Revival [Deluxe Edition] [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="122571233">116.89 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T10:31:14+00:00">08 Oct 2015, 10:31:14</span></td>
			<td class="green center">1128</td>
			<td class="red lasttd center">331</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11380139,0" class="icommentjs icon16" href="/mp3-new-releases-2015-week-40-glodls-t11380139.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/mp3-new-releases-2015-week-40-glodls-t11380139.html" class="cellMainLink">MP3 NEW RELEASES 2015 WEEK 40 [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="3544566949">3.3 <span>GB</span></td>
			<td class="center">488</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T20:49:20+00:00">08 Oct 2015, 20:49:20</span></td>
			<td class="green center">615</td>
			<td class="red lasttd center">849</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11379542,0" class="icommentjs icon16" href="/va-best-70s-of-my-love-2015-mp3-320-kbps-t11379542.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-best-70s-of-my-love-2015-mp3-320-kbps-t11379542.html" class="cellMainLink">VA - Best 70s Of My Love (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="923178920">880.41 <span>MB</span></td>
			<td class="center">101</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T18:07:48+00:00">08 Oct 2015, 18:07:48</span></td>
			<td class="green center">420</td>
			<td class="red lasttd center">286</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11369227,0" class="icommentjs icon16" href="/va-now-that-s-what-i-call-halloween-2015-compilation-mp3-edm-rg-t11369227.html#comment"> <em class="iconvalue">24</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-now-that-s-what-i-call-halloween-2015-compilation-mp3-edm-rg-t11369227.html" class="cellMainLink">VA - NOW That&#039;s What I Call Halloween (2015) Compilation @ MP3 [EDM RG]</a></div>
			</td>
			<td class="nobr center" data-sort="164062214">156.46 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T16:34:07+00:00">06 Oct 2015, 16:34:07</span></td>
			<td class="green center">521</td>
			<td class="red lasttd center">61</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11366200,0" class="icommentjs icon16" href="/paul-mccartney-pipes-of-peace-deluxe-2015-freak37-t11366200.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/paul-mccartney-pipes-of-peace-deluxe-2015-freak37-t11366200.html" class="cellMainLink">Paul McCartney â Pipes of Peace (Deluxe) ( 2015 ) ...-Freak37</a></div>
			</td>
			<td class="nobr center" data-sort="171310678">163.37 <span>MB</span></td>
			<td class="center">21</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T00:44:16+00:00">06 Oct 2015, 00:44:16</span></td>
			<td class="green center">500</td>
			<td class="red lasttd center">61</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11368748,0" class="icommentjs icon16" href="/disclosure-caracal-limited-deluxe-edition-2015-album-mp3-cdda-cd-rip-edm-rg-t11368748.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/disclosure-caracal-limited-deluxe-edition-2015-album-mp3-cdda-cd-rip-edm-rg-t11368748.html" class="cellMainLink">Disclosure - Caracal [Limited Deluxe Edition] 2015 Album @ MP3 / CDDA CD Rip [EDM RG]</a></div>
			</td>
			<td class="nobr center" data-sort="157853398">150.54 <span>MB</span></td>
			<td class="center">26</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T14:08:39+00:00">06 Oct 2015, 14:08:39</span></td>
			<td class="green center">443</td>
			<td class="red lasttd center">74</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11367666,0" class="icommentjs icon16" href="/va-ministry-of-sound-mash-up-mix-mixed-by-the-cut-up-boys-2015-mp3-320-kbps-t11367666.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-ministry-of-sound-mash-up-mix-mixed-by-the-cut-up-boys-2015-mp3-320-kbps-t11367666.html" class="cellMainLink">VA - Ministry Of Sound Mash Up Mix (Mixed by The Cut Up Boys) (2015) Mp3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="731129438">697.26 <span>MB</span></td>
			<td class="center">42</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T08:46:55+00:00">06 Oct 2015, 08:46:55</span></td>
			<td class="green center">414</td>
			<td class="red lasttd center">125</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11378207,0" class="icommentjs icon16" href="/faithless-faithless-2-0-2015-mp3-vbr-h4ckus-glodls-t11378207.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/faithless-faithless-2-0-2015-mp3-vbr-h4ckus-glodls-t11378207.html" class="cellMainLink">Faithless - Faithless 2.0 [2015] [MP3-VBR] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="294059689">280.44 <span>MB</span></td>
			<td class="center">33</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T12:20:55+00:00">08 Oct 2015, 12:20:55</span></td>
			<td class="green center">370</td>
			<td class="red lasttd center">102</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11366760,0" class="icommentjs icon16" href="/beatport-singles-05-10-2015-edm-electro-house-future-house-hardstyle-trance-progressive-house-t11366760.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/beatport-singles-05-10-2015-edm-electro-house-future-house-hardstyle-trance-progressive-house-t11366760.html" class="cellMainLink">Beatport Singles - 05.10.2015 (EDM.Electro House,Future House,Hardstyle,Trance,Progressive House)</a></div>
			</td>
			<td class="nobr center" data-sort="1894952923">1.76 <span>GB</span></td>
			<td class="center">141</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T03:56:48+00:00">06 Oct 2015, 03:56:48</span></td>
			<td class="green center">232</td>
			<td class="red lasttd center">181</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11367686,0" class="icommentjs icon16" href="/va-deep-house-party-mix-2015-mp3-320-kbps-t11367686.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-deep-house-party-mix-2015-mp3-320-kbps-t11367686.html" class="cellMainLink">VA - Deep House Party Mix (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="783106703">746.83 <span>MB</span></td>
			<td class="center">71</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T08:54:07+00:00">06 Oct 2015, 08:54:07</span></td>
			<td class="green center">237</td>
			<td class="red lasttd center">104</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11379586,0" class="icommentjs icon16" href="/hurts-surrender-deluxe-edition-2015-mp3-320kbps-h4ckus-glodls-t11379586.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/hurts-surrender-deluxe-edition-2015-mp3-320kbps-h4ckus-glodls-t11379586.html" class="cellMainLink">Hurts - Surrender [Deluxe Edition] [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="108158174">103.15 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T18:18:33+00:00">08 Oct 2015, 18:18:33</span></td>
			<td class="green center">201</td>
			<td class="red lasttd center">48</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11381391,0" class="icommentjs icon16" href="/armin-van-buuren-a-state-of-trance-734-2015-10-08-split-inspiron-t11381391.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/armin-van-buuren-a-state-of-trance-734-2015-10-08-split-inspiron-t11381391.html" class="cellMainLink">Armin Van Buuren - A State Of Trance 734 (2015-10-08) (Split) (Inspiron)</a></div>
			</td>
			<td class="nobr center" data-sort="321009399">306.14 <span>MB</span></td>
			<td class="center">38</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-09T04:23:23+00:00">09 Oct 2015, 04:23:23</span></td>
			<td class="green center">161</td>
			<td class="red lasttd center">74</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11373096,0" class="icommentjs icon16" href="/cliff-richard-75-at-75-3cd-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11373096.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/cliff-richard-75-at-75-3cd-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11373096.html" class="cellMainLink">Cliff Richard - 75 At 75 [3CD] (2015) [MP3-320Kbps] [CBR] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="581203134">554.28 <span>MB</span></td>
			<td class="center">77</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T12:12:36+00:00">07 Oct 2015, 12:12:36</span></td>
			<td class="green center">164</td>
			<td class="red lasttd center">56</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11374602,0" class="icommentjs icon16" href="/va-recognition-of-the-heart-2015-mp3-320-kbps-t11374602.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-recognition-of-the-heart-2015-mp3-320-kbps-t11374602.html" class="cellMainLink">VA - Recognition Of The Heart (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="967897123">923.06 <span>MB</span></td>
			<td class="center">102</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T17:48:36+00:00">07 Oct 2015, 17:48:36</span></td>
			<td class="green center">132</td>
			<td class="red lasttd center">87</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11383676,0" class="icommentjs icon16" href="/bryan-adams-get-up-deluxe-edition-2015-mp3-320kbps-h4ckus-glodls-t11383676.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bryan-adams-get-up-deluxe-edition-2015-mp3-320kbps-h4ckus-glodls-t11383676.html" class="cellMainLink">Bryan Adams - Get Up [Deluxe Edition] [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="117695727">112.24 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-09T14:15:30+00:00">09 Oct 2015, 14:15:30</span></td>
			<td class="green center">101</td>
			<td class="red lasttd center">46</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11380347,0" class="icommentjs icon16" href="/wrc-5-fia-world-rally-championship-reloaded-t11380347.html#comment"> <em class="iconvalue">16</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/wrc-5-fia-world-rally-championship-reloaded-t11380347.html" class="cellMainLink">WRC 5 FIA World Rally Championship-RELOADED</a></div>
			</td>
			<td class="nobr center" data-sort="18432088177">17.17 <span>GB</span></td>
			<td class="center">95</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T22:31:54+00:00">08 Oct 2015, 22:31:54</span></td>
			<td class="green center">299</td>
			<td class="red lasttd center">2431</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11383574,0" class="icommentjs icon16" href="/the-witcher-3-wild-hunt-patch-v1-10-gog-t11383574.html#comment"> <em class="iconvalue">75</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/the-witcher-3-wild-hunt-patch-v1-10-gog-t11383574.html" class="cellMainLink">The Witcher 3: Wild Hunt (Patch v1.10) (GOG)</a></div>
			</td>
			<td class="nobr center" data-sort="10705115834">9.97 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-09T13:44:49+00:00">09 Oct 2015, 13:44:49</span></td>
			<td class="green center">370</td>
			<td class="red lasttd center">2149</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11369383,0" class="icommentjs icon16" href="/transformers-devastation-codex-t11369383.html#comment"> <em class="iconvalue">67</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/transformers-devastation-codex-t11369383.html" class="cellMainLink">Transformers.Devastation-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="6884103764">6.41 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T17:12:18+00:00">06 Oct 2015, 17:12:18</span></td>
			<td class="green center">917</td>
			<td class="red lasttd center">895</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11367436,0" class="icommentjs icon16" href="/assassin-s-creed-anthology-the-complete-edition-repack-by-corepack-t11367436.html#comment"> <em class="iconvalue">195</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/assassin-s-creed-anthology-the-complete-edition-repack-by-corepack-t11367436.html" class="cellMainLink">Assassin&#039;s Creed Anthology - The Complete Edition - RePack by CorePack</a></div>
			</td>
			<td class="nobr center" data-sort="57015842781">53.1 <span>GB</span></td>
			<td class="center">18</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T07:25:44+00:00">06 Oct 2015, 07:25:44</span></td>
			<td class="green center">66</td>
			<td class="red lasttd center">1444</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11369316,0" class="icommentjs icon16" href="/prison-architect-v1-0-t11369316.html#comment"> <em class="iconvalue">53</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/prison-architect-v1-0-t11369316.html" class="cellMainLink">Prison Architect v1.0</a></div>
			</td>
			<td class="nobr center" data-sort="281857194">268.8 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T16:56:47+00:00">06 Oct 2015, 16:56:47</span></td>
			<td class="green center">760</td>
			<td class="red lasttd center">53</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11380130,0" class="icommentjs icon16" href="/sid-meiers-civilization-beyond-earth-rising-tide-addon-reloaded-t11380130.html#comment"> <em class="iconvalue">50</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/sid-meiers-civilization-beyond-earth-rising-tide-addon-reloaded-t11380130.html" class="cellMainLink">Sid Meiers Civilization Beyond Earth Rising Tide Addon-RELOADED</a></div>
			</td>
			<td class="nobr center" data-sort="3294513815">3.07 <span>GB</span></td>
			<td class="center">68</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T20:46:42+00:00">08 Oct 2015, 20:46:42</span></td>
			<td class="green center">492</td>
			<td class="red lasttd center">501</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11371553,0" class="icommentjs icon16" href="/dragon-age-inquisition-xatab-t11371553.html#comment"> <em class="iconvalue">21</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/dragon-age-inquisition-xatab-t11371553.html" class="cellMainLink">Dragon Age: Inquisition- XATAB</a></div>
			</td>
			<td class="nobr center" data-sort="23305439949">21.7 <span>GB</span></td>
			<td class="center">13</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T04:09:17+00:00">07 Oct 2015, 04:09:17</span></td>
			<td class="green center">375</td>
			<td class="red lasttd center">297</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11372642,0" class="icommentjs icon16" href="/star-wars-battlefront-pc-beta-3dm-t11372642.html#comment"> <em class="iconvalue">68</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/star-wars-battlefront-pc-beta-3dm-t11372642.html" class="cellMainLink">Star Wars Battlefront PC Beta-3DM</a></div>
			</td>
			<td class="nobr center" data-sort="10088623216">9.4 <span>GB</span></td>
			<td class="center">13</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T09:33:35+00:00">07 Oct 2015, 09:33:35</span></td>
			<td class="green center">174</td>
			<td class="red lasttd center">434</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11378717,0" class="icommentjs icon16" href="/arma-3-v-1-52-2013-pc-repack-by-xatab-t11378717.html#comment"> <em class="iconvalue">14</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/arma-3-v-1-52-2013-pc-repack-by-xatab-t11378717.html" class="cellMainLink">Arma 3 [v 1.52] (2013) PC | RePack by xatab</a></div>
			</td>
			<td class="nobr center" data-sort="10274868666">9.57 <span>GB</span></td>
			<td class="center">9</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T14:50:50+00:00">08 Oct 2015, 14:50:50</span></td>
			<td class="green center">160</td>
			<td class="red lasttd center">167</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11375093,0" class="icommentjs icon16" href="/grim-tales-9-threads-of-destiny-collector-s-edition-asg-t11375093.html#comment"> <em class="iconvalue">13</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/grim-tales-9-threads-of-destiny-collector-s-edition-asg-t11375093.html" class="cellMainLink">Grim Tales 9 â Threads of Destiny Collectorâs Edition [ASG]</a></div>
			</td>
			<td class="nobr center" data-sort="771702574">735.95 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T20:08:45+00:00">07 Oct 2015, 20:08:45</span></td>
			<td class="green center">197</td>
			<td class="red lasttd center">47</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11368979,0" class="icommentjs icon16" href="/skyhill-hi2u-full-game-crack-t11368979.html#comment"> <em class="iconvalue">13</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/skyhill-hi2u-full-game-crack-t11368979.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/skyhill-hi2u-full-game-crack-t11368979.html" class="cellMainLink">SKYHILL-HI2U [Full Game+crack]</a></div>
			</td>
			<td class="nobr center" data-sort="226238847">215.76 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T15:18:25+00:00">06 Oct 2015, 15:18:25</span></td>
			<td class="green center">142</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11366770,0" class="icommentjs icon16" href="/tales-from-the-borderlands-r-g-mechanics-t11366770.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/tales-from-the-borderlands-r-g-mechanics-t11366770.html" class="cellMainLink">Tales from the Borderlands [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center" data-sort="2901476233">2.7 <span>GB</span></td>
			<td class="center">15</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T04:03:50+00:00">06 Oct 2015, 04:03:50</span></td>
			<td class="green center">108</td>
			<td class="red lasttd center">43</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11366684,0" class="icommentjs icon16" href="/3dmgame-dragon-age-inquisition-update-1-10-incl-dlc-and-crack-cpy-t11366684.html#comment"> <em class="iconvalue">72</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/3dmgame-dragon-age-inquisition-update-1-10-incl-dlc-and-crack-cpy-t11366684.html" class="cellMainLink">3DMGAME-Dragon Age Inquisition Update 1-10 Incl DLC and Crack-CPY</a></div>
			</td>
			<td class="nobr center" data-sort="9267570472">8.63 <span>GB</span></td>
			<td class="center">12</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T03:31:39+00:00">06 Oct 2015, 03:31:39</span></td>
			<td class="green center">53</td>
			<td class="red lasttd center">118</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11382889,0" class="icommentjs icon16" href="/assetto-corsa-v1-3-incl-dream-pack-1-and-2-dlc-cracked-rldgames-t11382889.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/assetto-corsa-v1-3-incl-dream-pack-1-and-2-dlc-cracked-rldgames-t11382889.html" class="cellMainLink">Assetto Corsa v1.3 Incl Dream Pack 1 and 2 DLC Cracked-RLDGAMES</a></div>
			</td>
			<td class="nobr center" data-sort="11259145249">10.49 <span>GB</span></td>
			<td class="center">9</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-09T11:09:12+00:00">09 Oct 2015, 11:09:12</span></td>
			<td class="green center">27</td>
			<td class="red lasttd center">102</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11379295,0" class="icommentjs icon16" href="/sublevel-zero-skidrow-t11379295.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/sublevel-zero-skidrow-t11379295.html" class="cellMainLink">Sublevel.Zero-SKIDROW</a></div>
			</td>
			<td class="nobr center" data-sort="881955231">841.1 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T17:18:02+00:00">08 Oct 2015, 17:18:02</span></td>
			<td class="green center">42</td>
			<td class="red lasttd center">15</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11379065,0" class="icommentjs icon16" href="/internet-download-manager-idm-6-25-build-1-crack-cpul-ctrc-t11379065.html#comment"> <em class="iconvalue">27</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/internet-download-manager-idm-6-25-build-1-crack-cpul-ctrc-t11379065.html" class="cellMainLink">Internet Download Manager (IDM) 6.25 Build 1 + Crack ~ [CPUL][CTRC]</a></div>
			</td>
			<td class="nobr center" data-sort="10603515">10.11 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T16:22:43+00:00">08 Oct 2015, 16:22:43</span></td>
			<td class="green center">322</td>
			<td class="red lasttd center">35</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11367355,0" class="icommentjs icon16" href="/pinnacle-studio-ultimate-v19-0-1-245-64-bit-32bit-content-pack-team-os-t11367355.html#comment"> <em class="iconvalue">15</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/pinnacle-studio-ultimate-v19-0-1-245-64-bit-32bit-content-pack-team-os-t11367355.html" class="cellMainLink">Pinnacle Studio Ultimate v19.0.1.245 64 Bit + 32bit +Content Pack-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="7541308066">7.02 <span>GB</span></td>
			<td class="center">381</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T07:03:39+00:00">06 Oct 2015, 07:03:39</span></td>
			<td class="green center">180</td>
			<td class="red lasttd center">277</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11371060,0" class="icommentjs icon16" href="/adobe-photoshop-lightroom-cc-6-2-multilingual-patch-appzdam-t11371060.html#comment"> <em class="iconvalue">38</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/adobe-photoshop-lightroom-cc-6-2-multilingual-patch-appzdam-t11371060.html" class="cellMainLink">Adobe Photoshop Lightroom CC 6.2 Multilingual + Patch - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="1018249464">971.08 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T01:29:18+00:00">07 Oct 2015, 01:29:18</span></td>
			<td class="green center">258</td>
			<td class="red lasttd center">99</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11378038,0" class="icommentjs icon16" href="/unhackme-7-80-build-480-crack-4realtorrentz-t11378038.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/unhackme-7-80-build-480-crack-4realtorrentz-t11378038.html" class="cellMainLink">UnHackMe 7.80 Build 480 + Crack [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="21522268">20.53 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T11:33:34+00:00">08 Oct 2015, 11:33:34</span></td>
			<td class="green center">251</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11378537,0" class="icommentjs icon16" href="/iobit-driver-booster-pro-3-0-3-261-final-2015-frank-t11378537.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/iobit-driver-booster-pro-3-0-3-261-final-2015-frank-t11378537.html" class="cellMainLink">IObit Driver Booster PRO 3.0.3.261 Final (2015)-FRANK</a></div>
			</td>
			<td class="nobr center" data-sort="15293651">14.59 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T13:57:49+00:00">08 Oct 2015, 13:57:49</span></td>
			<td class="green center">153</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11367178,0" class="icommentjs icon16" href="/google-play-store-v5-9-12-mod-xpoz-t11367178.html#comment"> <em class="iconvalue">62</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/google-play-store-v5-9-12-mod-xpoz-t11367178.html" class="cellMainLink">Google Play Store v5.9.12 Mod-XpoZ</a></div>
			</td>
			<td class="nobr center" data-sort="8053920">7.68 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T06:06:05+00:00">06 Oct 2015, 06:06:05</span></td>
			<td class="green center">139</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11373142,0" class="icommentjs icon16" href="/mixcraft-pro-studio-v7-5-287-incl-keygen-air-r2r-deepstatus-t11373142.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/mixcraft-pro-studio-v7-5-287-incl-keygen-air-r2r-deepstatus-t11373142.html" class="cellMainLink">Mixcraft Pro Studio v7.5.287 Incl.Keygen-AiR-R2R [deepstatus]</a></div>
			</td>
			<td class="nobr center" data-sort="355277588">338.82 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T12:25:09+00:00">07 Oct 2015, 12:25:09</span></td>
			<td class="green center">126</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11369898,0" class="icommentjs icon16" href="/presonus-studio-one-3-professional-v3-1-0-35191-incl-patch-and-keygen-r2r-t11369898.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/presonus-studio-one-3-professional-v3-1-0-35191-incl-patch-and-keygen-r2r-t11369898.html" class="cellMainLink">PreSonus Studio One 3 Professional v3.1.0.35191 Incl.Patch and Keygen-R2R</a></div>
			</td>
			<td class="nobr center" data-sort="185523264">176.93 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T19:20:53+00:00">06 Oct 2015, 19:20:53</span></td>
			<td class="green center">117</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11379028,0" class="icommentjs icon16" href="/latest-idm-internet-download-manager-6-25-build-1-retail-crack-s0ft4pc-t11379028.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/latest-idm-internet-download-manager-6-25-build-1-retail-crack-s0ft4pc-t11379028.html" class="cellMainLink">Latest IDM (Internet Download Manager) 6.25 Build 1 Retail + Crack [S0ft4PC]</a></div>
			</td>
			<td class="nobr center" data-sort="7143749">6.81 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T16:09:14+00:00">08 Oct 2015, 16:09:14</span></td>
			<td class="green center">98</td>
			<td class="red lasttd center">6</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/daz3d-dz22410-lend-her-grace-poses-for-g3f-rar-t11372891.html" class="cellMainLink">Daz3D DZ22410 - Lend Her Grace Poses for G3F.rar</a></div>
			</td>
			<td class="nobr center" data-sort="56760431">54.13 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T10:56:46+00:00">07 Oct 2015, 10:56:46</span></td>
			<td class="green center">78</td>
			<td class="red lasttd center">0</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/daz3d-dz22131-dreamy-lingerie-for-g3f-zip-t11372876.html" class="cellMainLink">Daz3D DZ22131 - Dreamy Lingerie for G3F.zip</a></div>
			</td>
			<td class="nobr center" data-sort="48321611">46.08 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T10:53:30+00:00">07 Oct 2015, 10:53:30</span></td>
			<td class="green center">73</td>
			<td class="red lasttd center">3</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/daz3d-poser-dz22879-roma-heels-bundle-for-g2f-g3f-rar-t11373422.html" class="cellMainLink">Daz3D Poser DZ22879 - Roma Heels Bundle for G2F G3F.rar</a></div>
			</td>
			<td class="nobr center" data-sort="50673177">48.33 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T13:25:53+00:00">07 Oct 2015, 13:25:53</span></td>
			<td class="green center">66</td>
			<td class="red lasttd center">1</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11374151,0" class="icommentjs icon16" href="/daz3d-ro111063-yoga-for-g3f-zip-t11374151.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/daz3d-ro111063-yoga-for-g3f-zip-t11374151.html" class="cellMainLink">Daz3D RO111063 - Yoga For G3F.zip</a></div>
			</td>
			<td class="nobr center" data-sort="70553890">67.29 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T16:14:07+00:00">07 Oct 2015, 16:14:07</span></td>
			<td class="green center">63</td>
			<td class="red lasttd center">2</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11382800,0" class="icommentjs icon16" href="/driver-booster-pro-3-0-3-261-crack-s0ft4pc-t11382800.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/driver-booster-pro-3-0-3-261-crack-s0ft4pc-t11382800.html" class="cellMainLink">Driver Booster Pro 3.0.3.261 + Crack [S0ft4PC]</a></div>
			</td>
			<td class="nobr center" data-sort="14753498">14.07 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-09T10:43:57+00:00">09 Oct 2015, 10:43:57</span></td>
			<td class="green center">51</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11374258,0" class="icommentjs icon16" href="/daz3d-subway-train-22560-t11374258.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/daz3d-subway-train-22560-t11374258.html" class="cellMainLink">DAZ3D : Subway Train [22560]</a></div>
			</td>
			<td class="nobr center" data-sort="169764894">161.9 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T16:35:52+00:00">07 Oct 2015, 16:35:52</span></td>
			<td class="green center">51</td>
			<td class="red lasttd center">0</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fansub-resistance-naruto-shippuuden-433-french-subbed-1280x720-mp4-t11379227.html" class="cellMainLink">[Fansub-Resistance]Naruto Shippuuden 433 [ French Subbed] (1280x720).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="295448466">281.76 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T17:03:17+00:00">08 Oct 2015, 17:03:17</span></td>
			<td class="green center">2318</td>
			<td class="red lasttd center">395</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11384054,0" class="icommentjs icon16" href="/horriblesubs-shomin-sample-01-720p-mkv-t11384054.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-shomin-sample-01-720p-mkv-t11384054.html" class="cellMainLink">[HorribleSubs] Shomin Sample - 01 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="458535542">437.29 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-09T15:45:03+00:00">09 Oct 2015, 15:45:03</span></td>
			<td class="green center">686</td>
			<td class="red lasttd center">263</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11379779,0" class="icommentjs icon16" href="/leopard-raws-subete-ga-f-ni-naru-the-perfect-insider-01-raw-cx-1280x720-x264-aac-mp4-t11379779.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-subete-ga-f-ni-naru-the-perfect-insider-01-raw-cx-1280x720-x264-aac-mp4-t11379779.html" class="cellMainLink">[Leopard-Raws] Subete ga F ni Naru - The Perfect Insider - 01 RAW (CX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="489886503">467.19 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T19:05:02+00:00">08 Oct 2015, 19:05:02</span></td>
			<td class="green center">491</td>
			<td class="red lasttd center">101</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-lance-n-39-masques-02-raw-tbs-1280x720-x264-aac-mp4-t11380172.html" class="cellMainLink">[Leopard-Raws] Lance N&#39; Masques - 02 RAW (TBS 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="363182538">346.36 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T21:05:03+00:00">08 Oct 2015, 21:05:03</span></td>
			<td class="green center">447</td>
			<td class="red lasttd center">107</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11374883,0" class="icommentjs icon16" href="/leopard-raws-sakurako-san-no-ashimoto-ni-wa-shitai-ga-umatteiru-01-raw-kbs-1280x720-x264-aac-mp4-t11374883.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-sakurako-san-no-ashimoto-ni-wa-shitai-ga-umatteiru-01-raw-kbs-1280x720-x264-aac-mp4-t11374883.html" class="cellMainLink">[Leopard-Raws] Sakurako-san no Ashimoto ni wa Shitai ga Umatteiru - 01 RAW (KBS 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="286982866">273.69 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T19:05:02+00:00">07 Oct 2015, 19:05:02</span></td>
			<td class="green center">443</td>
			<td class="red lasttd center">27</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11382011,0" class="icommentjs icon16" href="/leopard-raws-young-black-jack-02-raw-tbs-1280x720-x264-aac-mp4-t11382011.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-young-black-jack-02-raw-tbs-1280x720-x264-aac-mp4-t11382011.html" class="cellMainLink">[Leopard-Raws] Young Black Jack - 02 RAW (TBS 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="367647926">350.62 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-09T07:05:03+00:00">09 Oct 2015, 07:05:03</span></td>
			<td class="green center">364</td>
			<td class="red lasttd center">85</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11378571,0" class="icommentjs icon16" href="/animerg-naruto-shippuuden-433-english-subbed-480p-kami-t11378571.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-naruto-shippuuden-433-english-subbed-480p-kami-t11378571.html" class="cellMainLink">[AnimeRG] Naruto Shippuuden - 433 [English Subbed][480p] [KaMi]</a></div>
			</td>
			<td class="nobr center" data-sort="60099903">57.32 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T14:05:11+00:00">08 Oct 2015, 14:05:11</span></td>
			<td class="green center">275</td>
			<td class="red lasttd center">39</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-hacka-doll-the-animation-02-raw-mx-1280x720-x264-aac-mp4-t11385165.html" class="cellMainLink">[Leopard-Raws] Hacka Doll The Animation - 02 RAW (MX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="197880803">188.71 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-09T17:15:03+00:00">09 Oct 2015, 17:15:03</span></td>
			<td class="green center">180</td>
			<td class="red lasttd center">124</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-aquarion-logos-15-raw-kbs-1280x720-x264-aac-mp4-t11379567.html" class="cellMainLink">[Leopard-Raws] Aquarion Logos - 15 RAW (KBS 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="467373673">445.72 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T18:15:03+00:00">08 Oct 2015, 18:15:03</span></td>
			<td class="green center">208</td>
			<td class="red lasttd center">40</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-tantei-team-kz-jiken-note-01-raw-nhke-1280x720-x264-aac-mp4-t11374386.html" class="cellMainLink">[Leopard-Raws] Tantei Team KZ - Jiken Note - 01 RAW (NHKE 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="106248223">101.33 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T17:00:02+00:00">07 Oct 2015, 17:00:02</span></td>
			<td class="green center">189</td>
			<td class="red lasttd center">6</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11378643,0" class="icommentjs icon16" href="/naruto-shippuden-shippuuden-433-480p-eng-sub-iorchid-t11378643.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/naruto-shippuden-shippuuden-433-480p-eng-sub-iorchid-t11378643.html" class="cellMainLink">Naruto Shippuden (Shippuuden) - 433 [480p][Eng Sub]_iORcHiD</a></div>
			</td>
			<td class="nobr center" data-sort="102730151">97.97 <span>MB</span></td>
			<td class="center">8</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T14:31:01+00:00">08 Oct 2015, 14:31:01</span></td>
			<td class="green center">167</td>
			<td class="red lasttd center">27</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fff-yuru-yuri-san-hai-01-1b453753-mkv-t11375947.html" class="cellMainLink">[FFF] Yuru Yuri San Hai! - 01 [1B453753].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="304925290">290.8 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T01:15:02+00:00">08 Oct 2015, 01:15:02</span></td>
			<td class="green center">161</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/commie-little-witch-academia-the-enchanted-parade-bd-720p-aac-e4692780-mkv-t11376845.html" class="cellMainLink">[Commie] Little Witch Academia The Enchanted Parade [BD 720p AAC] [E4692780].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="981579360">936.11 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T06:15:03+00:00">08 Oct 2015, 06:15:03</span></td>
			<td class="green center">150</td>
			<td class="red lasttd center">18</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-miss-monochrome-the-animation-3-02-raw-mx-1280x720-x264-aac-mp4-t11385167.html" class="cellMainLink">[Leopard-Raws] Miss Monochrome The Animation 3 - 02 RAW (MX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="153902949">146.77 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-09T17:15:03+00:00">09 Oct 2015, 17:15:03</span></td>
			<td class="green center">107</td>
			<td class="red lasttd center">90</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-fushigi-na-somera-chan-01-raw-tvs-1280x720-x264-aac-mp4-t11379548.html" class="cellMainLink">[Leopard-Raws] Fushigi na Somera-chan - 01 RAW (TVS 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="54340663">51.82 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T18:10:03+00:00">08 Oct 2015, 18:10:03</span></td>
			<td class="green center">135</td>
			<td class="red lasttd center">10</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11374097,0" class="icommentjs icon16" href="/marvel-week-10-07-2015-nem-t11374097.html#comment"> <em class="iconvalue">38</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/marvel-week-10-07-2015-nem-t11374097.html" class="cellMainLink">Marvel Week+ (10-07-2015) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="1082985089">1.01 <span>GB</span></td>
			<td class="center">22</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T16:02:26+00:00">07 Oct 2015, 16:02:26</span></td>
			<td class="green center">845</td>
			<td class="red lasttd center">437</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11373561,0" class="icommentjs icon16" href="/dc-week-10-07-2015-aka-dc-you-week-19-nem-t11373561.html#comment"> <em class="iconvalue">28</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/dc-week-10-07-2015-aka-dc-you-week-19-nem-t11373561.html" class="cellMainLink">DC Week+ (10-07-2015) (aka DC YOU Week 19) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="555392735">529.66 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T14:08:01+00:00">07 Oct 2015, 14:08:01</span></td>
			<td class="green center">572</td>
			<td class="red lasttd center">227</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11369812,0" class="icommentjs icon16" href="/national-geographic-november-2015-usa-t11369812.html#comment"> <em class="iconvalue">16</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/national-geographic-november-2015-usa-t11369812.html" class="cellMainLink">National Geographic - November 2015 USA</a></div>
			</td>
			<td class="nobr center" data-sort="17674379">16.86 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T18:52:21+00:00">06 Oct 2015, 18:52:21</span></td>
			<td class="green center">407</td>
			<td class="red lasttd center">11</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11368805,0" class="icommentjs icon16" href="/injustice-gods-among-us-year-four-023-2015-digital-son-of-ultron-empire-cbr-nem-t11368805.html#comment"> <em class="iconvalue">22</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/injustice-gods-among-us-year-four-023-2015-digital-son-of-ultron-empire-cbr-nem-t11368805.html" class="cellMainLink">Injustice - Gods Among Us - Year Four 023 (2015) (digital) (Son of Ultron-Empire).cbr (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="22943984">21.88 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T14:21:32+00:00">06 Oct 2015, 14:21:32</span></td>
			<td class="green center">360</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11372892,0" class="icommentjs icon16" href="/secret-wars-06-of-08-2015-digital-zone-empire-cbr-nem-t11372892.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/secret-wars-06-of-08-2015-digital-zone-empire-cbr-nem-t11372892.html" class="cellMainLink">Secret Wars 06 (of 08) (2015) (Digital) (Zone-Empire).cbr (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="47676027">45.47 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T10:58:07+00:00">07 Oct 2015, 10:58:07</span></td>
			<td class="green center">283</td>
			<td class="red lasttd center">9</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11373150,0" class="icommentjs icon16" href="/introduction-to-mathematical-logic-6th-edition-2015-pdf-gooner-t11373150.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/introduction-to-mathematical-logic-6th-edition-2015-pdf-gooner-t11373150.html" class="cellMainLink">Introduction to Mathematical Logic - 6th Edition (2015).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="10472707">9.99 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T12:27:26+00:00">07 Oct 2015, 12:27:26</span></td>
			<td class="green center">273</td>
			<td class="red lasttd center">19</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11372636,0" class="icommentjs icon16" href="/invincible-iron-man-001-2015-digital-minutemen-thoth-cbz-nem-t11372636.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/invincible-iron-man-001-2015-digital-minutemen-thoth-cbz-nem-t11372636.html" class="cellMainLink">Invincible Iron Man 001 (2015) (digital) (Minutemen-Thoth).cbz (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="36038531">34.37 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T09:32:38+00:00">07 Oct 2015, 09:32:38</span></td>
			<td class="green center">270</td>
			<td class="red lasttd center">5</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11372778,0" class="icommentjs icon16" href="/interpreting-engineering-drawings-8th-edition-2015-pdf-gooner-t11372778.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/interpreting-engineering-drawings-8th-edition-2015-pdf-gooner-t11372778.html" class="cellMainLink">Interpreting Engineering Drawings - 8th Edition (2015).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="25298424">24.13 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T10:21:23+00:00">07 Oct 2015, 10:21:23</span></td>
			<td class="green center">235</td>
			<td class="red lasttd center">19</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11372377,0" class="icommentjs icon16" href="/journey-to-star-wars-the-force-awakens-shattered-empire-02-of-04-2015-digital-kileko-empire-cbr-nem-t11372377.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/journey-to-star-wars-the-force-awakens-shattered-empire-02-of-04-2015-digital-kileko-empire-cbr-nem-t11372377.html" class="cellMainLink">Journey to Star Wars - The Force Awakens - Shattered Empire 02 (of 04) (2015) (Digital) (Kileko-Empire).cbr (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="40179683">38.32 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T08:02:54+00:00">07 Oct 2015, 08:02:54</span></td>
			<td class="green center">223</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11373549,0" class="icommentjs icon16" href="/batman-robin-eternal-001-2015-digital-zone-empire-cbr-t11373549.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/batman-robin-eternal-001-2015-digital-zone-empire-cbr-t11373549.html" class="cellMainLink">Batman &amp; Robin Eternal 001 (2015) (Digital) (Zone-Empire).cbr</a></div>
			</td>
			<td class="nobr center" data-sort="51685657">49.29 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T14:05:06+00:00">07 Oct 2015, 14:05:06</span></td>
			<td class="green center">203</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11373977,0" class="icommentjs icon16" href="/doctor-strange-001-2015-digital-oroboros-dcp-cbr-t11373977.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/doctor-strange-001-2015-digital-oroboros-dcp-cbr-t11373977.html" class="cellMainLink">Doctor Strange 001 (2015) (digital) (Oroboros-DCP).cbr</a></div>
			</td>
			<td class="nobr center" data-sort="54726794">52.19 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T15:43:44+00:00">07 Oct 2015, 15:43:44</span></td>
			<td class="green center">200</td>
			<td class="red lasttd center">13</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11372948,0" class="icommentjs icon16" href="/the-amazing-spider-man-001-2015-digital-zone-empire-cbr-nem-t11372948.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-amazing-spider-man-001-2015-digital-zone-empire-cbr-nem-t11372948.html" class="cellMainLink">The Amazing Spider-Man 001 (2015) (Digital) (Zone-Empire).cbr (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="126475290">120.62 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T11:21:58+00:00">07 Oct 2015, 11:21:58</span></td>
			<td class="green center">195</td>
			<td class="red lasttd center">13</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11379240,0" class="icommentjs icon16" href="/image-week-10-07-2015-nem-t11379240.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/image-week-10-07-2015-nem-t11379240.html" class="cellMainLink">Image Week (10-07-2015) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="815216271">777.45 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T17:06:03+00:00">08 Oct 2015, 17:06:03</span></td>
			<td class="green center">155</td>
			<td class="red lasttd center">79</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11372631,0" class="icommentjs icon16" href="/avengers-0-2015-digital-minutemen-thoth-cbz-nem-t11372631.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/avengers-0-2015-digital-minutemen-thoth-cbz-nem-t11372631.html" class="cellMainLink">Avengers 0 (2015) (digital) (Minutemen-Thoth).cbz (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="95529881">91.1 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T09:31:56+00:00">07 Oct 2015, 09:31:56</span></td>
			<td class="green center">176</td>
			<td class="red lasttd center">7</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11372049,0" class="icommentjs icon16" href="/0-day-week-of-2015-09-30-t11372049.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/0-day-week-of-2015-09-30-t11372049.html" class="cellMainLink">0-Day Week of 2015.09.30</a></div>
			</td>
			<td class="nobr center" data-sort="7774805688">7.24 <span>GB</span></td>
			<td class="center">149</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T06:40:21+00:00">07 Oct 2015, 06:40:21</span></td>
			<td class="green center">112</td>
			<td class="red lasttd center">134</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11376763,0" class="icommentjs icon16" href="/va-now-that-s-what-i-call-classic-rock-3cd-2015-flac-sn3h1t87-glodls-t11376763.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-now-that-s-what-i-call-classic-rock-3cd-2015-flac-sn3h1t87-glodls-t11376763.html" class="cellMainLink">VA - NOW That&#039;s What I Call Classic Rock [3CD] (2015) [FLAC] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="1641061478">1.53 <span>GB</span></td>
			<td class="center">78</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T05:47:49+00:00">08 Oct 2015, 05:47:49</span></td>
			<td class="green center">473</td>
			<td class="red lasttd center">268</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11380343,0" class="icommentjs icon16" href="/ella-fitzgerald-billie-holiday-at-newport-2015-24-192-hd-flac-t11380343.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/ella-fitzgerald-billie-holiday-at-newport-2015-24-192-hd-flac-t11380343.html" class="cellMainLink">Ella Fitzgerald &amp; Billie Holiday At Newport (2015) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="2430565354">2.26 <span>GB</span></td>
			<td class="center">32</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T22:30:39+00:00">08 Oct 2015, 22:30:39</span></td>
			<td class="green center">186</td>
			<td class="red lasttd center">99</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11379126,0" class="icommentjs icon16" href="/billie-holiday-billie-holiday-2015-192-24-hd-flac-t11379126.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/billie-holiday-billie-holiday-2015-192-24-hd-flac-t11379126.html" class="cellMainLink">Billie Holiday - Billie Holiday (2015) [192-24 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1052815999">1004.04 <span>MB</span></td>
			<td class="center">30</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T16:38:54+00:00">08 Oct 2015, 16:38:54</span></td>
			<td class="green center">145</td>
			<td class="red lasttd center">50</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11375383,0" class="icommentjs icon16" href="/van-morrison-moondance-2013-24-192-hd-flac-t11375383.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/van-morrison-moondance-2013-24-192-hd-flac-t11375383.html" class="cellMainLink">Van Morrison - Moondance (2013) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1509306729">1.41 <span>GB</span></td>
			<td class="center">26</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T21:38:00+00:00">07 Oct 2015, 21:38:00</span></td>
			<td class="green center">129</td>
			<td class="red lasttd center">40</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11377506,0" class="icommentjs icon16" href="/cassandra-wilson-coming-forth-by-day-2015-24-96-hd-flac-t11377506.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/cassandra-wilson-coming-forth-by-day-2015-24-96-hd-flac-t11377506.html" class="cellMainLink">Cassandra Wilson - Coming Forth by Day (2015) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1287151404">1.2 <span>GB</span></td>
			<td class="center">31</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T09:26:44+00:00">08 Oct 2015, 09:26:44</span></td>
			<td class="green center">113</td>
			<td class="red lasttd center">36</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11380681,0" class="icommentjs icon16" href="/america-history-greatest-hits-flac-tntvillage-t11380681.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/america-history-greatest-hits-flac-tntvillage-t11380681.html" class="cellMainLink">America - History Greatest Hits [Flac][TntVillage]</a></div>
			</td>
			<td class="nobr center" data-sort="223212646">212.87 <span>MB</span></td>
			<td class="center">19</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-09T00:33:05+00:00">09 Oct 2015, 00:33:05</span></td>
			<td class="green center">113</td>
			<td class="red lasttd center">31</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11378063,0" class="icommentjs icon16" href="/eartha-kitt-everybody-needs-somebody-sometimes-2015-flac-t11378063.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/eartha-kitt-everybody-needs-somebody-sometimes-2015-flac-t11378063.html" class="cellMainLink">Eartha Kitt - Everybody Needs Somebody Sometimes (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="468328852">446.63 <span>MB</span></td>
			<td class="center">33</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T11:40:59+00:00">08 Oct 2015, 11:40:59</span></td>
			<td class="green center">113</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11382212,0" class="icommentjs icon16" href="/santana-inner-secrets-2014-24-96-hd-flac-t11382212.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/santana-inner-secrets-2014-24-96-hd-flac-t11382212.html" class="cellMainLink">Santana - Inner Secrets (2014) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="953166337">909.01 <span>MB</span></td>
			<td class="center">25</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-09T08:03:07+00:00">09 Oct 2015, 08:03:07</span></td>
			<td class="green center">95</td>
			<td class="red lasttd center">55</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11380116,0" class="icommentjs icon16" href="/fine-young-cannibals-the-finest-flac-tntvillage-t11380116.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/fine-young-cannibals-the-finest-flac-tntvillage-t11380116.html" class="cellMainLink">Fine Young Cannibals - The Finest [Flac][TntVillage]</a></div>
			</td>
			<td class="nobr center" data-sort="410153634">391.15 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T20:41:21+00:00">08 Oct 2015, 20:41:21</span></td>
			<td class="green center">106</td>
			<td class="red lasttd center">32</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/george-benson-the-ultimate-collection-2cd-2015-flac-t11383079.html" class="cellMainLink">George Benson - The Ultimate Collection (2CD 2015) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1069931782">1020.37 <span>MB</span></td>
			<td class="center">53</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-09T11:53:11+00:00">09 Oct 2015, 11:53:11</span></td>
			<td class="green center">89</td>
			<td class="red lasttd center">56</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11368311,0" class="icommentjs icon16" href="/eric-clapton-slowhand-2012-24-96-hd-flac-t11368311.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/eric-clapton-slowhand-2012-24-96-hd-flac-t11368311.html" class="cellMainLink">Eric Clapton - Slowhand (2012) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="834654743">795.99 <span>MB</span></td>
			<td class="center">30</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T12:13:54+00:00">06 Oct 2015, 12:13:54</span></td>
			<td class="green center">105</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11372607,0" class="icommentjs icon16" href="/miles-davis-on-the-corner-2014-24-96-hd-flac-t11372607.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/miles-davis-on-the-corner-2014-24-96-hd-flac-t11372607.html" class="cellMainLink">Miles Davis - On The Corner (2014) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1314448505">1.22 <span>GB</span></td>
			<td class="center">15</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-07T09:24:20+00:00">07 Oct 2015, 09:24:20</span></td>
			<td class="green center">98</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-q-music-top-500-van-90-s-6cd-2015-flac-t11367901.html" class="cellMainLink">VA - Q-Music Top 500 Van 90&#039;s [6CD] (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="3121491991">2.91 <span>GB</span></td>
			<td class="center">147</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-06T09:56:43+00:00">06 Oct 2015, 09:56:43</span></td>
			<td class="green center">85</td>
			<td class="red lasttd center">43</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/john-fogerty-discography-1973-2013-flac-reworked-reseeded-t11378792.html" class="cellMainLink">John Fogerty - Discography (1973-2013) [FLAC] (reworked &amp; reseeded)</a></div>
			</td>
			<td class="nobr center" data-sort="5218611400">4.86 <span>GB</span></td>
			<td class="center">399</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-08T15:03:49+00:00">08 Oct 2015, 15:03:49</span></td>
			<td class="green center">63</td>
			<td class="red lasttd center">42</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11383627,0" class="icommentjs icon16" href="/linda-ronstadt-what-s-new-2014-24-96-hd-flac-t11383627.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/linda-ronstadt-what-s-new-2014-24-96-hd-flac-t11383627.html" class="cellMainLink">Linda Ronstadt - What&#039;s New (2014) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="796227345">759.34 <span>MB</span></td>
			<td class="center">24</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-10-09T14:01:51+00:00">09 Oct 2015, 14:01:51</span></td>
			<td class="green center">65</td>
			<td class="red lasttd center">21</td>
        </tr>
			</table>


	<script type="text/javascript">
		if ($.cookie('kat_settings[fpw_column]') != null && $.cookie('kat_settings[fpw_order]')) {
			var colIndex = $.cookie('kat_settings[fpw_column]')
			var ascend = $.cookie('kat_settings[fpw_order]');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			sortTables(colIndex, ascend);
		}
		$('.frontPageWidget .firstr th .fpw-sort').click(function() {
			var colIndex = $(this).parent().index();
			var ascend = !$(this).hasClass('sortedAsc')?1:0;
			$('.fpw-sort').removeClass('sortedAsc sortedDesc');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			updateFPWsettings(colIndex, ascend);
			sortTables(colIndex, ascend);
		});
		function sortTables(colIndex, ascend) {	
			$('.frontPageWidget').each(function() {
				var data = [];
				$('tr:not(.firstr)', $(this)).each(function() {
					var name = $('td:first .cellMainLink', $(this)).text();
					var id = $('td:first .cellMainLink', $(this)).attr('href').match(/.*-t(\d+)\.html/)[1];
					var fileSize = $('td:eq(1)', $(this)).attr('data-sort');
					data.push({
						'col0':name, 'col1':padNum(fileSize), 'col2':padNum($('td:eq(2)',$(this)).text()), 'col3':padNum(id), 'col4':padNum($('td:eq(4)',$(this)).text()), 'col5':padNum($('td:eq(5)',$(this)).text()), 'html':$(this).html()
					});
				});
				data.sort(function(a, b) {
					var x = a['col'+colIndex].toLowerCase(); var y = b['col'+colIndex].toLowerCase();
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
				if (!ascend||ascend=='0') data.reverse();
				$(this).find('tr:not(.firstr)').remove();
				for (var i=0;i<data.length;i++) {
					$(this).append('<tr class="'+((i+1)%2==0?'even':'odd')+'">'+data[i].html+'</tr>');
				}
			});
		}
		function padNum(val) {
			val = '0000000000000000'+val;
			return val.substring(val.length-15);
		}
		function updateFPWsettings(t, o) {
			$.cookie('kat_settings[fpw_column]', t, { expires: 365, path: '/' });
			$.cookie('kat_settings[fpw_order]', o, { expires: 365, path: '/' });
		}
	</script>
		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
    
            
        <div id="_119b0a17fab5493361a252d04bf527db"></div>
    
                
    	    <div class="advertising">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <div id="_7063408f1c01d50e0dc2d833186ce962"></div>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/quiz-can-you-guess-movie-v15/?unread=16980554">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Quiz; Can you guess this movie? V15
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/titovicanjoey/">titovicanjoey</a></span></span> <time class="timeago" datetime="2015-10-09T23:59:55+00:00">09 Oct 2015, 23:59</time></span>
	</li>
		<li>
		<a href="/community/show/torrents-need-updating-kat-changing-mod-work-thread-only-v5/?unread=16980553">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Torrents that need updating / KAT changing/ Mod Work Thread Only..V5
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_4"><a class="plain" href="/user/Judas/">Judas</a></span></span> <time class="timeago" datetime="2015-10-09T23:59:40+00:00">09 Oct 2015, 23:59</time></span>
	</li>
		<li>
		<a href="/community/show/get-me-out-here-started-poping/?unread=16980552">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Get me out of here started poping up
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/anthemic/">anthemic</a></span></span> <time class="timeago" datetime="2015-10-09T23:59:32+00:00">09 Oct 2015, 23:59</time></span>
	</li>
		<li>
		<a href="/community/show/book-yaang3rs-bya-academic-it-engineering-fiction-non-fictio-thread-103734/?unread=16980550">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				BÃÃK Ð¯ANGÎRS[BÐ¯] : Academic, IT, Engineering, Fiction &amp; Non-Fiction Releases
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_2"><a class="plain" href="/user/ira-1969/">ira-1969</a></span></span> <time class="timeago" datetime="2015-10-09T23:57:29+00:00">09 Oct 2015, 23:57</time></span>
	</li>
		<li>
		<a href="/community/show/what-are-you-listening-right-now-v4-thread-108003/?unread=16980540">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What are you listening to right now? V4
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/Yivlx/">Yivlx</a></span></span> <time class="timeago" datetime="2015-10-09T23:52:43+00:00">09 Oct 2015, 23:52</time></span>
	</li>
		<li>
		<a href="/community/show/music-requests-new-v2/?unread=16980534">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Music Requests - New (V2)
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/scarred13/">scarred13</a></span></span> <time class="timeago" datetime="2015-10-09T23:51:07+00:00">09 Oct 2015, 23:51</time></span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/look-mama-i-m-popular/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Look, mama, I&#039;m popular!
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-05T17:42:40+00:00">05 Oct 2015, 17:42</time></span>
	</li>
	<li>
		<a href="/blog/post/summer-updates-september-1/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Summer updates [September, 1]
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-09-01T16:13:36+00:00">01 Sep 2015, 16:13</time></span>
	</li>
	<li>
		<a href="/blog/post/kickasstorrents-is-moving-to-kat-cr-domain/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				KickassTorrents is moving to kat.cr domain
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-04-24T10:13:38+00:00">24 Apr 2015, 10:13</time></span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/CtShoN/post/in-memory-of-ct-shon-s-father/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> In Memory of Ct.ShoN&#039;s Father</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/CtShoN/">CtShoN</a> <time class="timeago" datetime="2015-10-09T20:17:12+00:00">09 Oct 2015, 20:17</time></span></li>
	<li><a href="/blog/.kristine./post/look-mama-i-m-at-kat/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Look, mama, I&#039;m at KAT</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/.kristine./">.kristine.</a> <time class="timeago" datetime="2015-10-09T10:36:49+00:00">09 Oct 2015, 10:36</time></span></li>
	<li><a href="/blog/WarhoundOne/post/the-history-of-me-part-1/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> The History of Me..Part 1</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/WarhoundOne/">WarhoundOne</a> <time class="timeago" datetime="2015-10-09T08:36:04+00:00">09 Oct 2015, 08:36</time></span></li>
	<li><a href="/blog/SirSeedsAlot/post/can-a-broken-heart-ever-be-fixed-pirate-mixtape/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Can a Broken Heart Ever Be Fixed? Pirate Mixtape</p></a><span class="explanation">by <a class="plain aclColor_3" href="/user/SirSeedsAlot/">SirSeedsAlot</a> <time class="timeago" datetime="2015-10-09T05:46:28+00:00">09 Oct 2015, 05:46</time></span></li>
	<li><a href="/blog/SushiKushi/post/sushikushi-recruitment-drive-encoders-sub-re-timers-and-xdcc-bot-hosts-needed/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> SushiKushi Recruitment Drive: Encoders, Sub Re-timers, and XDCC Bot hosts needed!</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/SushiKushi/">SushiKushi</a> <time class="timeago" datetime="2015-10-09T02:31:52+00:00">09 Oct 2015, 02:31</time></span></li>
	<li><a href="/blog/OptimusPr1me/post/crianÃ§a/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> CrianÃ§a</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/OptimusPr1me/">OptimusPr1me</a> <time class="timeago" datetime="2015-10-08T20:55:42+00:00">08 Oct 2015, 20:55</time></span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/hot%20and%20mean/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				hot and mean
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/the%20martian/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				the martian
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/the%20battle%20new%20york%20day%202/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				the battle new york day 2
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/dani%20moneytalks/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				dani moneytalks
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/gay%20xxx/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				gay xxx
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/turtix/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				turtix
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/atreyu/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				atreyu
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/zombies/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				zombies
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/keka/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				keka
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/nims.island/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				nims.island
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/pit/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				pit
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		<li>
			<a data-nop href="http://www.torrentreactor.net/" target="_blank" rel="external">
				<span class="itorreact thirdPartIcons"></span>TorrentReactor
			</a>
		</li>
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a onclick="setLanguage('bsc', '.kat.cr');return false;" class="plain">Bosnian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a onclick="setLanguage('ko', '.kat.cr');return false;" class="plain">Korean</a></li>
                                <li><a onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a onclick="setLanguage('sl', '.kat.cr');return false;" class="plain">Slovenian</a></li>
                                <li><a onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
		<li><a href="/welcome/">welcome</a></li>
		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
        <li><a href="https://kastatus.com">KAT status</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <div id="_673e31f53f8166159b8e996c4124765b"></div>
        <div id="_e7050fb15fd39b3e4e99a5be4a57b6ea"></div>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script><script>
 sc('addGlobal', 'pagetype', 'front');
 sc('addSlot', '_60318cd4e8d28f6fb76fe34e9bd9c498');
sc('addSlot', '_39ecb76dd457e5ac33776fdf11500d56');
sc('addSlot', '_277923e5f9d753c5b0630c28e641790c');
sc('addSlot', '_119b0a17fab5493361a252d04bf527db');
sc('addSlot', '_7063408f1c01d50e0dc2d833186ce962', { 'searchQuery': '' });
sc('addSlot', '_673e31f53f8166159b8e996c4124765b');
sc('addSlot', '_e7050fb15fd39b3e4e99a5be4a57b6ea');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>

</body>
</html>
