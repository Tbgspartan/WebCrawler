<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trending  stories on Indian Lifestyle, Culture, Relationships, Food, Travel, Entertainment, News & New Technology News - Indiatimes.com</title>

<meta name="keywords" content="" />
<meta name="description" content="Indiatimes.com brings you the news, articles, stories and videos on entertainment, latest lifestyle, culture & new technologies emerging worldwide." />



		

<link rel="image_src" href="http://media.indiatimes.in/resources/images/fbimage.png" />
<meta property="og:title" content="" />    
<meta property="og:url" content="http://www.indiatimes.com" />
<meta property="og:image" content="http://media.indiatimes.in/resources/images/fbimage.png" />

<meta property="og:site_name" content="indiatimes.com" />
<meta property="fb:app_id" content="117787264903013" />
<meta property="og:description" content="" />
<meta name="google-site-verification" content="ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw" />
<meta name="msvalidate.01" content="BAB71AAED7CCC01DB106A58E99625EB4" />
<link href="https://plus.google.com/+indiatimes/posts" rel="publisher" />

<!--<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />-->

<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">    <link rel="shortcut icon" href="http://media.indiatimes.in/resources/images/favicon.ico?v=25" type="image/x-icon"><link rel="stylesheet" href="http://media.indiatimes.in/resources/css/site.css?v=100.9" media="screen"><link rel="stylesheet" href="http://www.indiatimes.com/fonts/fonts.min.css?v=100.9" media="screen"><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery-1.11.1.min.js?v=100.9"></script><script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/handlebars-v1.3.0.js?v=100.9"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.inview.min.js?v=100.9"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.preload.min.js?v=100.9"></script><script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script><script>(function() {var _fbq = window._fbq || (window._fbq = []);if (!_fbq.loaded) {var fbds = document.createElement('script');fbds.async = true;fbds.src = '//connect.facebook.net/en_US/fbds.js';var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(fbds, s);_fbq.loaded = true;}_fbq.push(['addPixelId', '624029644406124']);})();window._fbq = window._fbq || [];window._fbq.push(['track', 'PixelInitialized', {}]);</script><noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=624029644406124&amp;ev=PixelInitialized" /></noscript>           <!--  Analytics Code Begin -->    <!-- Begin comScore Tag -->    <script>        var _comscore = _comscore || [];        _comscore.push({ c1: "2", c2: "6036484" });        (function() {            var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";            el.parentNode.insertBefore(s, el);        })();    </script>    <noscript>    <img src="http://b.scorecardresearch.com/p?c1=2&c2=6036484&cv=2.0&cj=1" />    </noscript>    <!-- End comScore Tag -->    <!-- Begin Google Analytics Tag -->    <script>        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');        ga('create', 'UA-198011-6', 'indiatimes.com');            ga('require', 'displayfeatures');        ga('send', 'pageview');    </script>    <!-- End Google Analytics Tag -->    <!--  Analytics Code Begin --><!--adcode common script--><script type='text/javascript' src="http://ad.crwdcntrl.net/5/c=2800/pe=y/var=_ccaud"></script><script type="text/javascript">    var googletag = googletag || {};    googletag.cmd = googletag.cmd || [];    (function() {        var gads = document.createElement("script");        gads.async = true;        gads.type = "text/javascript";        var useSSL = "https:" == document.location.protocol;        gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";        var node =document.getElementsByTagName("script")[0];        node.parentNode.insertBefore(gads, node);    })();</script> <script type="text/javascript">        var nid='';        var cid='';        var sid='';        var brand_name='';                                                        </script>            <script type="text/javascript" src="http://media.indiatimes.in/resources/js/banner/home.js?v=100.9"></script>
            
<!--[if lt IE 9]>
  <script type="text/javascript" src="http://media.indiatimes.in/js/html5.js?v1.1"></script>
  <![endif]-->
<script>
var isDetailPage = 0</script>
</head>
<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];   
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=117787264903013&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script> 	
	
<div id="wrap" class="cf"><!--wrap start-->
    <div class='darkDisabledBg' id='disabledBg'></div>
<header>
    <div class="header-inner">
        <div class="comm-header ">
            <nav>
                <div class='forTop'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>

                    <div class="lnks">
                                                        <a href="http://www.indiatimes.com/news/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','News');">News</a> 
                                                               <a href="http://www.indiatimes.com/lifestyle/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Lifestyle');">Lifestyle</a> 
                                                               <a href="http://www.indiatimes.com/entertainment/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Entertainment');">Entertainment</a> 
                                                               <a href="http://www.indiatimes.com/videocafe/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Videos');">Videos</a> 
                                                               <a href="http://www.indiatimes.com/health/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Health');">Health</a> 
                                                                                          <a href='http://www.indiatimes.com/trending'  target="_blank" class="#fffff" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Trending');">Trending</a> 
                                                            <a href='http://www.indiatimes.com/specials/freedom-blogs/'  target="_blank" class="best" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','#FreedomBlogs');">#FreedomBlogs</a> 
                                                </div>
                </div>
                <div class='onScrolled'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>
                    <a href="javascript:;" class="menu rc sprite" id="headerMenu">Menu</a>
                    <div class="has-tag">
                                                       <a href='http://www.indiatimes.com/specials/freedom-blogs/'  target="_blank" class="best" >#FreedomBlogs</a> 
                                                </div>
                </div>

                                
            </nav>
			                 
	    <div id="sticker" style="display:none;"><span class="readTitle"></span></div>
                    <div class="socls fr share">
            </div>
                        
            
			 <div class="social fr">
                            <a title="facebook" target="_blank"  class="sprite fb" href="https://www.facebook.com/indiatimes">facebook</a>
                            <a title="twitter" target="_blank"  class="sprite twt" href="https://twitter.com/indiatimes">twitter</a>
                            <a title="search"  class="sprite serach" onclick="$('body').showSearch();" href="javascript:void(0);">search</a>
                        </div>
        </div>
    </div>
</header>
<div class="clr"></div>
<div id="overlay" class="animated bounceOut"><!--overlay start-->
    <div class="search_conts"><!--search-cont start-->
        <form onsubmit="return false;" class="serach-form">
            <input type="text" onkeyup="search_data();" class="input" onfocus="if(this.value=='Search'){this.value=''}" onblur="if(this.value==''){this.value='Search'};" value="Search" name="q" id="q" autocomplete="off">
            <input type="button" class="sprite src-btn" onclick="search_data();">
            <a class="sprite sclose" id="close" href="javascript:void(0);">X</a>
        </form>
        <div class="scroll-panes" id="scroll-panes">
            <div class="clr"></div>
            <div class="res-msg" id="res-msg"></div>
            <div class="search-list cf" id="results"></div>
            <div class="clr"></div>
            <div class="loadmore" id="progressBar" style="display: none;">Loading......</div>
            <a class="loadmore" style="display:none;" onclick="load_search_data(); " id="load_more_button" href="javascript:void(0);">Load more</a>
        </div>
    </div>
</div>
<div class="blk">
    <div class="inner">
        <dl id="leftMenu" class="leftMenu accordion">
            <dt data-color="red-bg" class=""><a href="http://www.indiatimes.com">Home</a></dt>
            <dd> </dd>
                            <dt  data-color="blue-bg" ><a href="http://www.indiatimes.com/news/">News</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/news/india/'>India</a>
                                     
                            <a href='http://www.indiatimes.com/news/world/'>World</a>
                                     
                            <a href='http://www.indiatimes.com/news/sports/'>Sports</a>
                                     
                            <a href='http://www.indiatimes.com/news/weird/'>Weird</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="purple-bg" ><a href="http://www.indiatimes.com/lifestyle/">Lifestyle</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/lifestyle/self/'>Self</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/style/'>Style</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                     
                            <a href='http://www.indiatimes.com/culture/who-we-are/'>Who we are</a>
                                     
                            <a href='http://www.indiatimes.com/culture/travel/'>Travel</a>
                                     
                            <a href='http://www.indiatimes.com/culture/food/'>Food</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="red-bg" ><a href="http://www.indiatimes.com/entertainment/">Entertainment</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/entertainment/bollywood/'>Bollywood</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/celebs/'>Celebscoop</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/hollywood/'>Hollywood</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="orange-bg" ><a href="http://www.indiatimes.com/videocafe/">Videos</a>
                                </dt>
                                <dt  data-color="green-bg" ><a href="http://www.indiatimes.com/health/">Health</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                     
                            <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                     
                            <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                     
                            <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                     
                            <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                            </dd>
                    <div class="clr"></div>
                            <dt class="pink-bg">
            <div class="follow">Follow indiatimes </div>
            <div class="follow_cont"> 
                <a href="https://www.facebook.com/indiatimes" class="fbleft sprite"> facebook</a> 
                <a href="https://twitter.com/indiatimes" class="twtleft sprite"> twitter</a> 
            </div>
            </dt>

            <dd>&nbsp;</dd>
            <div class="clr"></div>
        </dl>
    </div>
</div>


<div class="clr"></div>
<div class="dummy-cont">&nbsp;</div>
<div class="clr"></div>


<div id="pushdown">
    <div class="pull-ad"><!--pull-ad start-->   
	
                <script type="text/javascript"> 
                    showHeaderCode();
                </script>
	    </div><!--pull-ad end-->
</div>
    <!--testing 15-08-24 06:10:02-->    <section class="topslide-cont cf"><!--container start-->

         
 <div class="top-slide cf" id="top-slide" style="display:none;"><!--top-slide start-->
        <div class="top-slide-left pink-bg"><span class="yellow">TRENDING TODAY</span></div>
        <div class="top-slide-right skyblue-bg"><!--top-slide-right start-->
            <div class="flexslider">
                <div class="slides marqueeSlides" style=" max-width:800px;margin:auto; overflow:hidden">
				      
                    <span style="padding-right:50px;"><a title="11 Best Discoveries Of Bollywood That Helped Revive Our Faith In Quality Cinema" href="http://www.indiatimes.com/entertainment/11-best-discoveries-of-bollywood-that-helped-us-revive-our-faith-in-quality-cinema-243651.html">11 Best Discoveries Of Bollywood That Helped Revive Our Faith In Quality Cinema</a></span>
					 
                    <span style="padding-right:50px;"><a title="Travellers Beware! 40 Different Ways Con Artists Will Cheat You Out Of Cash And Valuables" href="http://www.indiatimes.com/culture/travel/travellers-beware-40-different-ways-con-artists-will-cheat-you-out-of-cash-and-valuables-244452.html">Travellers Beware! 40 Different Ways Con Artists Will Cheat You Out Of Cash And Valuables</a></span>
					 
                    <span style="padding-right:50px;"><a title="17 Important Facts About The Pill You Need To Know Before Popping One" href="http://www.indiatimes.com/health/healthyliving/17-important-facts-about-the-pill-you-need-to-know-before-popping-one-244400.html">17 Important Facts About The Pill You Need To Know Before Popping One</a></span>
					 
                    <span style="padding-right:50px;"><a title="9 Best Tracks Of KK You Must Have In Your Playlist" href="http://www.indiatimes.com/entertainment/9-best-songs-of-kk-you-must-have-in-your-playlist-244401.html">9 Best Tracks Of KK You Must Have In Your Playlist</a></span>
					 
                    <span style="padding-right:50px;"><a title="This Hangover + Zindagi Na Milegi Dobara Mashup Will Make You Laugh So Hard You'll Cry!" href="http://www.indiatimes.com/videocafe/this-hangover-+-zindagi-na-milegi-dobara-mashup-will-make-you-laugh-so-hard-youll-cry-244443.html">This Hangover + Zindagi Na Milegi Dobara Mashup Will Make You Laugh So Hard You'll Cry!</a></span>
					 
                    <span style="padding-right:50px;"><a title="11 Item Numbers Of The Decade That We Loved Dancing To" href="http://www.indiatimes.com/entertainment/bollywood/11-item-numbers-of-the-decade-that-we-loved-dancing-to-244377.html">11 Item Numbers Of The Decade That We Loved Dancing To</a></span>
					 
                    <span style="padding-right:50px;"><a title="These Are the Most Hilarious Questions India's 'Sexperts' Have Ever Been Asked" href="http://www.indiatimes.com/videocafe/these-are-the-most-hilarious-questions-indias-sexperts-have-ever-been-asked-244455.html">These Are the Most Hilarious Questions India's 'Sexperts' Have Ever Been Asked</a></span>
					 
                    <span style="padding-right:50px;"><a title="11 Make-In-A-Mug Recipes That Will Get You Into The Kitchen" href="http://www.indiatimes.com/culture/food/11-makeinamug-recipes-that-will-get-you-into-the-kitchen-244382.html">11 Make-In-A-Mug Recipes That Will Get You Into The Kitchen</a></span>
					 
                    <span style="padding-right:50px;"><a title="This Is The Ultimate Guide To Saying 'Sorry' Like You Actually Mean It." href="http://www.indiatimes.com/lifestyle/self/this-is-the-ultimate-guide-to-saying-sorry-like-you-actually-mean-it-244307.html">This Is The Ultimate Guide To Saying 'Sorry' Like You Actually Mean It.</a></span>
					 
                    <span style="padding-right:50px;"><a title="Just So You Don't Forget, Here's What India LOLed At In The Past 8 Months" href="http://www.indiatimes.com/videocafe/just-so-you-dont-forget-heres-what-india-loled-at-in-the-past-8-months-244440.html">Just So You Don't Forget, Here's What India LOLed At In The Past 8 Months</a></span>
					 
                    <span style="padding-right:50px;"><a title="Shopkeeper Gaurav Gera Gives Sunny Leone Exactly What She Asks For" href="http://www.indiatimes.com/videocafe/shopkeeper-gaurav-gera-gives-sunny-leone-exactly-what-she-asks-for-244450.html">Shopkeeper Gaurav Gera Gives Sunny Leone Exactly What She Asks For</a></span>
					 
                    <span style="padding-right:50px;"><a title="10 Common English Mistakes We All Make And How To NOT Make Them" href="http://www.indiatimes.com/lifestyle/self/10-simple-english-mistakes-we-all-make-and-how-to-not-make-them-232873.html">10 Common English Mistakes We All Make And How To NOT Make Them</a></span>
					 
                    <span style="padding-right:50px;"><a title="35 Weight Loss Exercises You Can Do At Home With Zero Equipment" href="http://www.indiatimes.com/health/35-weight-loss-exercises-you-can-do-at-home-with-zero-equipment-244337.html">35 Weight Loss Exercises You Can Do At Home With Zero Equipment</a></span>
					 
                    <span style="padding-right:50px;"><a title="India's Bravest Dancer Set To Have Both Feet Flying And Her Head Held High" href="http://www.indiatimes.com/entertainment/celebs/indias-bravest-dancer-set-to-have-both-feet-flying-and-her-head-held-high-244426.html">India's Bravest Dancer Set To Have Both Feet Flying And Her Head Held High</a></span>
					 
                    <span style="padding-right:50px;"><a title="This Delhi Policeman Is So Drunk On The Delhi Metro, He Can't Even Stand. And All This In His Uniform!" href="http://www.indiatimes.com/videocafe/alcohol-isnt-allowed-on-delhi-metro-but-drunk-cops-totally-legit-244429.html">This Delhi Policeman Is So Drunk On The Delhi Metro, He Can't Even Stand. And All This In His Uniform!</a></span>
					 
                    <span style="padding-right:50px;"><a title="13 Crazy Things You Didn't Know Could Happen On Facebook" href="http://www.indiatimes.com/lifestyle/technology/13-crazy-things-you-didnt-know-could-happen-on-facebook-244422.html">13 Crazy Things You Didn't Know Could Happen On Facebook</a></span>
					 
                    <span style="padding-right:50px;"><a title="This Photographer Captured The Past And Present Of Madras And Chennai And It Is Stunning" href="http://www.indiatimes.com/culture/travel/this-photographer-captured-the-past-and-present-of-madras-and-chennai-and-it-is-stunning-244419.html">This Photographer Captured The Past And Present Of Madras And Chennai And It Is Stunning</a></span>
					 
                    <span style="padding-right:50px;"><a title="11 Then-And-Now Pictures Of Hill Stations That Prove Change Is The Only Constant" href="http://www.indiatimes.com/culture/travel/11-thenandnow-pictures-of-hill-stations-that-prove-change-is-the-only-constant-244343.html">11 Then-And-Now Pictures Of Hill Stations That Prove Change Is The Only Constant</a></span>
					 
                    <span style="padding-right:50px;"><a title="9 Ridiculous College And Hostel Rules That Will Make You Pull Out Your Hair In Frustration" href="http://www.indiatimes.com/culture/who-we-are/9-ridiculous-college-and-hostel-rules-that-will-make-you-pull-out-your-hair-in-frustration-244348.html">9 Ridiculous College And Hostel Rules That Will Make You Pull Out Your Hair In Frustration</a></span>
					 
                    <span style="padding-right:50px;"><a title="17 Years After Dil Se, King Khan Keeps The Spirit Alive With A Dubsmash Of Chhaiyya Chhaiyya" href="http://www.indiatimes.com/entertainment/bollywood/17-years-after-dil-se-king-khan-keeps-the-spirit-alive-with-a-dubsmash-of-chhaiyya-chhaiyya-244428.html">17 Years After Dil Se, King Khan Keeps The Spirit Alive With A Dubsmash Of Chhaiyya Chhaiyya</a></span>
					 
                    <span style="padding-right:50px;"><a title="7 Low-Fat Egg Recipes That Are Yummy, Healthy And Diabetic-Friendly!" href="http://www.indiatimes.com/health/recipes/7-lowfat-egg-recipes-that-are-yummy-healthy-and-diabeticfriendly-244406.html">7 Low-Fat Egg Recipes That Are Yummy, Healthy And Diabetic-Friendly!</a></span>
					 
                    <span style="padding-right:50px;"><a title="10 Haunted Road Routes To Avoid If You're Scared Of The Paranormal" href="http://www.indiatimes.com/lifestyle/self/10-haunted-road-routes-to-avoid-if-youre-scared-of-the-paranormal-244309.html">10 Haunted Road Routes To Avoid If You're Scared Of The Paranormal</a></span>
					 
                    <span style="padding-right:50px;"><a title="9 Reasons You Shouldn't Worry About Saving Money" href="http://www.indiatimes.com/lifestyle/self/9-reasons-you-shouldnt-worry-about-saving-money-244283.html">9 Reasons You Shouldn't Worry About Saving Money</a></span>
					 
                    <span style="padding-right:50px;"><a title="Methi Seed Extract Can Sort Out All Your Menstrual Woes!" href="http://www.indiatimes.com/health/buzz/methi-seed-extract-can-sort-out-all-your-menstrual-woes-244329.html">Methi Seed Extract Can Sort Out All Your Menstrual Woes!</a></span>
					 
                    <span style="padding-right:50px;"><a title="14 Romantic Illustrations That Will Give You Some Serious Relationship Goals" href="http://www.indiatimes.com/culture/who-we-are/14-romantic-illustrations-that-will-give-you-some-serious-relationship-goals-244364.html">14 Romantic Illustrations That Will Give You Some Serious Relationship Goals</a></span>
					                    
                </div>
            </div>
            <a title="close" class="sprite close" href="javascript:void(0);">close</a>
        </div><!--top-slide-right end-->
    </div><!--top-slide end-->
    </section>

    <section id="hp_block_1" class="container cf" style="border: 0px solid red;"><!--container start-->
                

        <div class="news-panel cf"><!--news-panel start-->
            <h2>Big news</h2>

            <div class="feature-list cf"><!--feature-list start-->	
                                    <figure>
                        <div class="hash-tag">
                             2 days ago                         </div>
						 						          
                        <a href="http://www.indiatimes.com/news/world/70-years-after-it-went-missing-nazi-gold-train-found-in-poland-with-treasure-worth-rs-1317-crore-244407.html" class=" tint" title="70 Years After It Went Missing, Nazi Gold Train Found In Poland With Treasure Worth Rs 1317 Crore">
                            <img  class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Aug/nazi-collage-502_1440156910_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/nazi-collage-502_1440156910_236x111.jpg"  border="0" alt="70 Years After It Went Missing, Nazi Gold Train Found In Poland With Treasure Worth Rs 1317 Crore"/></a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/70-years-after-it-went-missing-nazi-gold-train-found-in-poland-with-treasure-worth-rs-1317-crore-244407.html" title="70 Years After It Went Missing, Nazi Gold Train Found In Poland With Treasure Worth Rs 1317 Crore">
                            70 Years After It Went Missing, Nazi Gold Train Found In Poland With Treasure Worth Rs 1317 Crore                        </a>
                    </figcaption>
                            </div><!--feature-list end-->

                            <div class="news-panel-list cf" id="column1_0"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/world/for-the-first-time-in-saudi-history-women-are-allowed-to-vote-244458.html" title="For The First Time In Saudi History, Women Are Allowed To Vote" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Aug/agencies-6_1440333208_1440333212_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/agencies-6_1440333208_1440333212_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/for-the-first-time-in-saudi-history-women-are-allowed-to-vote-244458.html" title="For The First Time In Saudi History, Women Are Allowed To Vote">
                            For The First Time In Saudi History, Women Are Allowed To Vote                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_1"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/world/indian-origin-businessman-is-the-new-ceo-of-the-dubai-gold-exchange-244456.html" title="Indian Origin Businessman Is The New CEO Of The Dubai Gold Exchange" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage-c666_1440330194_1440330201_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage-c666_1440330194_1440330201_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/indian-origin-businessman-is-the-new-ceo-of-the-dubai-gold-exchange-244456.html" title="Indian Origin Businessman Is The New CEO Of The Dubai Gold Exchange">
                            Indian Origin Businessman Is The New CEO Of The Dubai Gold Exchange                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_2"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/world/two-baby-pandas-are-the-reason-why-the-world-is-breaking-into-a-happy-dance-today-244454.html" title="Two Baby Pandas Are The Reason Why The World Is Breaking Into A Happy Dance Today" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Aug/panda5_1440327391_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/panda5_1440327391_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/two-baby-pandas-are-the-reason-why-the-world-is-breaking-into-a-happy-dance-today-244454.html" title="Two Baby Pandas Are The Reason Why The World Is Breaking Into A Happy Dance Today">
                            Two Baby Pandas Are The Reason Why The World Is Breaking Into A Happy Dance Today                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_3"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            13 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/india/need-for-sex-education-heightens-study-reveals-14-years-is-the-new-sex-age-for-indian-kids-244451.html" title="Need For Sex Education Heightens. Study Reveals 14 Years Is The New 'Sex Age' For Indian Kids" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Aug/sex502_1440327768_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/sex502_1440327768_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/need-for-sex-education-heightens-study-reveals-14-years-is-the-new-sex-age-for-indian-kids-244451.html" title="Need For Sex Education Heightens. Study Reveals 14 Years Is The New 'Sex Age' For Indian Kids">
                            Need For Sex Education Heightens. Study Reveals 14 Years Is The New 'Sex Age' For Indian Kids                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                    </div><!--news-panel end-->

        <div class="life-panel cf" id="middleBlock1"><!--life-panel start-->
            <h2>lifestyle</h2>

            

                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                   
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/buzz/rejoice-viagra-for-women-to-hit-the-shelves-in-india-in-october-244403.html" class="tint" title="Rejoice! Viagra For Women To Hit The Shelves In India In October!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/health/buzz/rejoice-viagra-for-women-to-hit-the-shelves-in-india-in-october-244403.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Aug/pill-card_1440156868_502x234.gif" data-original23="http://media.indiatimes.in/media/content/2015/Aug/pill-card_1440156868_502x234.gif"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/buzz/rejoice-viagra-for-women-to-hit-the-shelves-in-india-in-october-244403.html" title="Rejoice! Viagra For Women To Hit The Shelves In India In October!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/health/buzz/rejoice-viagra-for-women-to-hit-the-shelves-in-india-in-october-244403.html');">
                            Rejoice! Viagra For Women To Hit The Shelves In India In October!                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                   
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/bollywood/11-item-numbers-of-the-decade-that-we-loved-dancing-to-244377.html" class="tint" title="11 Item Numbers Of The Decade That We Loved Dancing To" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/entertainment/bollywood/11-item-numbers-of-the-decade-that-we-loved-dancing-to-244377.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Aug/sunny_1440064012_1440064028_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/sunny_1440064012_1440064028_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/11-item-numbers-of-the-decade-that-we-loved-dancing-to-244377.html" title="11 Item Numbers Of The Decade That We Loved Dancing To" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/entertainment/bollywood/11-item-numbers-of-the-decade-that-we-loved-dancing-to-244377.html');">
                            11 Item Numbers Of The Decade That We Loved Dancing To                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                   
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/buzz/the-latest-health-fad-is-out-and-it-involves-drinking-fatwater-seriously-244411.html" class="tint" title="The Latest Health Fad Is Out And It Involves Drinking FATWater. Seriously!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/health/buzz/the-latest-health-fad-is-out-and-it-involves-drinking-fatwater-seriously-244411.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Aug/fatwater-card_1440159701_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/fatwater-card_1440159701_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/buzz/the-latest-health-fad-is-out-and-it-involves-drinking-fatwater-seriously-244411.html" title="The Latest Health Fad Is Out And It Involves Drinking FATWater. Seriously!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/health/buzz/the-latest-health-fad-is-out-and-it-involves-drinking-fatwater-seriously-244411.html');">
                            The Latest Health Fad Is Out And It Involves Drinking FATWater. Seriously!                        </a>
                    </figcaption>
                </div>
                        
        </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_0">
                    <span class="strip skyblue-bg"></span>                    <figure>

                                                <a href="http://www.indiatimes.com/entertainment/11-best-discoveries-of-bollywood-that-helped-us-revive-our-faith-in-quality-cinema-243651.html" class="tint" title="11 Best Discoveries Of Bollywood That Helped Revive Our Faith In Quality Cinema">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Jul/card_1438323459_1438323470_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Jul/card_1438323459_1438323470_218x102.jpg" border="0" alt="11 Best Discoveries Of Bollywood That Helped Revive Our Faith In Quality Cinema"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/11-best-discoveries-of-bollywood-that-helped-us-revive-our-faith-in-quality-cinema-243651.html" title="11 Best Discoveries Of Bollywood That Helped Revive Our Faith In Quality Cinema">
                            11 Best Discoveries Of Bollywood That Helped Revive Our Faith In Quality Cinema                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_1">
                                        <figure>

                                                <a href="http://www.indiatimes.com/culture/travel/travellers-beware-40-different-ways-con-artists-will-cheat-you-out-of-cash-and-valuables-244452.html" class="tint" title="Travellers Beware! 40 Different Ways Con Artists Will Cheat You Out Of Cash And Valuables">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage-555_1440323864_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage-555_1440323864_218x102.jpg" border="0" alt="Travellers Beware! 40 Different Ways Con Artists Will Cheat You Out Of Cash And Valuables"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/travel/travellers-beware-40-different-ways-con-artists-will-cheat-you-out-of-cash-and-valuables-244452.html" title="Travellers Beware! 40 Different Ways Con Artists Will Cheat You Out Of Cash And Valuables">
                            Travellers Beware! 40 Different Ways Con Artists Will Cheat You Out Of Cash And Valuables                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_2">
                                        <figure>

                                                <a href="http://www.indiatimes.com/health/healthyliving/17-important-facts-about-the-pill-you-need-to-know-before-popping-one-244400.html" class="tint" title="17 Important Facts About The Pill You Need To Know Before Popping One">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Aug/card-1_1440150623_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/card-1_1440150623_218x102.jpg" border="0" alt="17 Important Facts About The Pill You Need To Know Before Popping One"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/17-important-facts-about-the-pill-you-need-to-know-before-popping-one-244400.html" title="17 Important Facts About The Pill You Need To Know Before Popping One">
                            17 Important Facts About The Pill You Need To Know Before Popping One                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_3">
                                        <figure>

                                                <a href="http://www.indiatimes.com/entertainment/9-best-songs-of-kk-you-must-have-in-your-playlist-244401.html" class="tint" title="9 Best Tracks Of KK You Must Have In Your Playlist">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Aug/card_1440159923_1440159931_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/card_1440159923_1440159931_218x102.jpg" border="0" alt="9 Best Tracks Of KK You Must Have In Your Playlist"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/9-best-songs-of-kk-you-must-have-in-your-playlist-244401.html" title="9 Best Tracks Of KK You Must Have In Your Playlist">
                            9 Best Tracks Of KK You Must Have In Your Playlist                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_4">
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-hangover-+-zindagi-na-milegi-dobara-mashup-will-make-you-laugh-so-hard-youll-cry-244443.html'>video</a>                        <a href="http://www.indiatimes.com/videocafe/this-hangover-+-zindagi-na-milegi-dobara-mashup-will-make-you-laugh-so-hard-youll-cry-244443.html" class="tint" title="This Hangover + Zindagi Na Milegi Dobara Mashup Will Make You Laugh So Hard You'll Cry!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/videocafe/2015/Aug/hang_card_1440308985_218x102.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Aug/hang_card_1440308985_218x102.jpg" border="0" alt="This Hangover + Zindagi Na Milegi Dobara Mashup Will Make You Laugh So Hard You'll Cry!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/this-hangover-+-zindagi-na-milegi-dobara-mashup-will-make-you-laugh-so-hard-youll-cry-244443.html" title="This Hangover + Zindagi Na Milegi Dobara Mashup Will Make You Laugh So Hard You'll Cry!">
                            This Hangover + Zindagi Na Milegi Dobara Mashup Will Make You Laugh So Hard You'll Cry!                        </a>
                    </figcaption>
                </div>
                        </div><!--trending-panel end-->
    </section><!--container end-->

<!-- -------------------    end Block 1  ------------------------------------- -->
<section class="big-ads" id="ad1"> 
<div id="bigAd1_slot"></div>
    <script>        
        showBigAD1('bigAd1_slot');
    </script> 
</section>
<!-- -------------------    start Block 2  ------------------------------------- -->

    <section id="hp_block_2" class="container cf" style="border: 0px solid red;"><!--container start-->
        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
                            <div class="news-panel-list cf" id="column1_4"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            15 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/world/now-ammo-to-shoot-down-drones-from-the-country-that-gave-us-drone-attacks-244448.html" title="Now, Ammo To Shoot Down Drones, From The Country That Gave Us Drone Attacks!" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/dron502_1440334741_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/world/now-ammo-to-shoot-down-drones-from-the-country-that-gave-us-drone-attacks-244448.html" title="Now, Ammo To Shoot Down Drones, From The Country That Gave Us Drone Attacks!">
                            Now, Ammo To Shoot Down Drones, From The Country That Gave Us Drone Attacks!                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_5"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            16 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/archeological-survey-of-india-just-found-a-2500-year-old-city-and-its-full-of-riches-244449.html" title="Archeological Survey Of India Just Found A 2500 Year Old City, And It's Full Of Riches" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/1-6_1440317196_1440317204_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/archeological-survey-of-india-just-found-a-2500-year-old-city-and-its-full-of-riches-244449.html" title="Archeological Survey Of India Just Found A 2500 Year Old City, And It's Full Of Riches">
                            Archeological Survey Of India Just Found A 2500 Year Old City, And It's Full Of Riches                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_6"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            16 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/chetan-bhagat-tells-indian-mps-how-to-use-tech-to-resolve-the-parliament-dramas-244447.html" title="Chetan Bhagat Tells Indian MPs How To Use Tech To Resolve The Parliament Dramas" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage-555_1440315391_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/chetan-bhagat-tells-indian-mps-how-to-use-tech-to-resolve-the-parliament-dramas-244447.html" title="Chetan Bhagat Tells Indian MPs How To Use Tech To Resolve The Parliament Dramas">
                            Chetan Bhagat Tells Indian MPs How To Use Tech To Resolve The Parliament Dramas                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_7"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            17 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/world/us-confirms-cannabis-kills-cancer-cells-may-set-the-ball-rolling-for-legalisation-of-weed-244445.html" title="U.S. Confirms Cannabis Kills Cancer Cells, May Set The Ball Rolling For Legalisation Of Weed" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/weed502_1440313182_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/world/us-confirms-cannabis-kills-cancer-cells-may-set-the-ball-rolling-for-legalisation-of-weed-244445.html" title="U.S. Confirms Cannabis Kills Cancer Cells, May Set The Ball Rolling For Legalisation Of Weed">
                            U.S. Confirms Cannabis Kills Cancer Cells, May Set The Ball Rolling For Legalisation Of Weed                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_8"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            17 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/indias-angry-exes-are-taking-revenge-on-their-girlfriends-with-porn-videos-revengeporn-244444.html" title="India's Angry Exes Are Taking Revenge On Their Girlfriends With Porn Videos" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/awdnews-5_1440313856_1440313860_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/indias-angry-exes-are-taking-revenge-on-their-girlfriends-with-porn-videos-revengeporn-244444.html" title="India's Angry Exes Are Taking Revenge On Their Girlfriends With Porn Videos">
                            India's Angry Exes Are Taking Revenge On Their Girlfriends With Porn Videos                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/these-are-the-most-hilarious-questions-indias-sexperts-have-ever-been-asked-244455.html'>video</a>                          
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/videocafe/these-are-the-most-hilarious-questions-indias-sexperts-have-ever-been-asked-244455.html" class="tint" title="These Are the Most Hilarious Questions India's 'Sexperts' Have Ever Been Asked">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/picmonkey-collage-555_1440329475_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/these-are-the-most-hilarious-questions-indias-sexperts-have-ever-been-asked-244455.html" title="These Are the Most Hilarious Questions India's 'Sexperts' Have Ever Been Asked">
                            These Are the Most Hilarious Questions India's 'Sexperts' Have Ever Been Asked                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/11-best-discoveries-of-bollywood-that-helped-us-revive-our-faith-in-quality-cinema-243651.html" class="tint" title="11 Best Discoveries Of Bollywood That Helped Revive Our Faith In Quality Cinema">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/card_1438323459_1438323470_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/11-best-discoveries-of-bollywood-that-helped-us-revive-our-faith-in-quality-cinema-243651.html" title="11 Best Discoveries Of Bollywood That Helped Revive Our Faith In Quality Cinema">
                            11 Best Discoveries Of Bollywood That Helped Revive Our Faith In Quality Cinema                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/culture/travel/travellers-beware-40-different-ways-con-artists-will-cheat-you-out-of-cash-and-valuables-244452.html" class="tint" title="Travellers Beware! 40 Different Ways Con Artists Will Cheat You Out Of Cash And Valuables">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage-555_1440323864_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/travel/travellers-beware-40-different-ways-con-artists-will-cheat-you-out-of-cash-and-valuables-244452.html" title="Travellers Beware! 40 Different Ways Con Artists Will Cheat You Out Of Cash And Valuables">
                            Travellers Beware! 40 Different Ways Con Artists Will Cheat You Out Of Cash And Valuables                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_5"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/bollywood/11-item-numbers-of-the-decade-that-we-loved-dancing-to-244377.html" class="tint" title="11 Item Numbers Of The Decade That We Loved Dancing To">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/sunny_1440064012_1440064028_218x102.jpg" border="0" alt="11 Item Numbers Of The Decade That We Loved Dancing To"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/11-item-numbers-of-the-decade-that-we-loved-dancing-to-244377.html" title="11 Item Numbers Of The Decade That We Loved Dancing To">
                            11 Item Numbers Of The Decade That We Loved Dancing To                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_6"><!--trending-panel-list start-->
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/these-are-the-most-hilarious-questions-indias-sexperts-have-ever-been-asked-244455.html'>video</a>
                        <a href="http://www.indiatimes.com/videocafe/these-are-the-most-hilarious-questions-indias-sexperts-have-ever-been-asked-244455.html" class="tint" title="These Are the Most Hilarious Questions India's 'Sexperts' Have Ever Been Asked">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/picmonkey-collage-555_1440329475_218x102.jpg" border="0" alt="These Are the Most Hilarious Questions India's 'Sexperts' Have Ever Been Asked"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/these-are-the-most-hilarious-questions-indias-sexperts-have-ever-been-asked-244455.html" title="These Are the Most Hilarious Questions India's 'Sexperts' Have Ever Been Asked">
                            These Are the Most Hilarious Questions India's 'Sexperts' Have Ever Been Asked                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_7"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/culture/food/11-makeinamug-recipes-that-will-get-you-into-the-kitchen-244382.html" class="tint" title="11 Make-In-A-Mug Recipes That Will Get You Into The Kitchen">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/cp_1440067403_218x102.jpg" border="0" alt="11 Make-In-A-Mug Recipes That Will Get You Into The Kitchen"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/food/11-makeinamug-recipes-that-will-get-you-into-the-kitchen-244382.html" title="11 Make-In-A-Mug Recipes That Will Get You Into The Kitchen">
                            11 Make-In-A-Mug Recipes That Will Get You Into The Kitchen                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_8"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/lifestyle/self/this-is-the-ultimate-guide-to-saying-sorry-like-you-actually-mean-it-244307.html" class="tint" title="This Is The Ultimate Guide To Saying 'Sorry' Like You Actually Mean It.">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1439897570_218x102.jpg" border="0" alt="This Is The Ultimate Guide To Saying 'Sorry' Like You Actually Mean It."/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/this-is-the-ultimate-guide-to-saying-sorry-like-you-actually-mean-it-244307.html" title="This Is The Ultimate Guide To Saying 'Sorry' Like You Actually Mean It.">
                            This Is The Ultimate Guide To Saying 'Sorry' Like You Actually Mean It.                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_9"><!--trending-panel-list start-->
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/just-so-you-dont-forget-heres-what-india-loled-at-in-the-past-8-months-244440.html'>video</a>
                        <a href="http://www.indiatimes.com/videocafe/just-so-you-dont-forget-heres-what-india-loled-at-in-the-past-8-months-244440.html" class="tint" title="Just So You Don't Forget, Here's What India LOLed At In The Past 8 Months">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/gandutv_card_1440268970_218x102.jpg" border="0" alt="Just So You Don't Forget, Here's What India LOLed At In The Past 8 Months"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/just-so-you-dont-forget-heres-what-india-loled-at-in-the-past-8-months-244440.html" title="Just So You Don't Forget, Here's What India LOLed At In The Past 8 Months">
                            Just So You Don't Forget, Here's What India LOLed At In The Past 8 Months                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div>

    </section><!--container end-->

<!--------------------------- end Block2 ------------------------------------------>
<section class="big-ads" id="ad2"> 
<div id="bigAd2_slot"></div>
</section>
<!--------------------------- start Block3 ------------------------------------------>

    <section id="hp_block_3" class="container cf"><!--container start-->

        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
          
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_9">
                    <figure>
                        <div class="hash-tag">
                            18 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/world/uk-airshow-ends-in-tragedy-7-people-dead-as-military-jet-crashes-into-a-busy-road-244442.html" title="UK Airshow Ends In Tragedy, 7 People Dead As Military Jet Crashes Into A Busy Road" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/airshow502_1440308306_236x111.jpg" border="0" alt="UK Airshow Ends In Tragedy, 7 People Dead As Military Jet Crashes Into A Busy Road"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/uk-airshow-ends-in-tragedy-7-people-dead-as-military-jet-crashes-into-a-busy-road-244442.html" title="UK Airshow Ends In Tragedy, 7 People Dead As Military Jet Crashes Into A Busy Road">
                            UK Airshow Ends In Tragedy, 7 People Dead As Military Jet Crashes Into A Busy Road                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_10">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/world/we-tracked-dawood-ibrahims-house-on-google-maps-he-shares-a-neighbourhood-with-some-of-pakistans-hottest-restaurants-244432.html" title="We Tracked Dawood Ibrahim's House On Google Maps. He Shares A Neighbourhood With Some Of Pakistan's Hottest Restaurants!" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/block-4_1440247618_1440247622_236x111.jpg" border="0" alt="We Tracked Dawood Ibrahim's House On Google Maps. He Shares A Neighbourhood With Some Of Pakistan's Hottest Restaurants!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/we-tracked-dawood-ibrahims-house-on-google-maps-he-shares-a-neighbourhood-with-some-of-pakistans-hottest-restaurants-244432.html" title="We Tracked Dawood Ibrahim's House On Google Maps. He Shares A Neighbourhood With Some Of Pakistan's Hottest Restaurants!">
                            We Tracked Dawood Ibrahim's House On Google Maps. He Shares A Neighbourhood With Some Of Pakistan's Hottest Restaurants!                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_11">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/india-might-have-more-women-cheaters-than-men-according-to-leading-adultery-website-244433.html" title="Ashley Madison Hack Throws Up An Interesting Statistic: India Has More Women Cheaters Than Men" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/capture2-fb_1440307834_1440307848_236x111.jpg" border="0" alt="Ashley Madison Hack Throws Up An Interesting Statistic: India Has More Women Cheaters Than Men"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/india-might-have-more-women-cheaters-than-men-according-to-leading-adultery-website-244433.html" title="Ashley Madison Hack Throws Up An Interesting Statistic: India Has More Women Cheaters Than Men">
                            Ashley Madison Hack Throws Up An Interesting Statistic: India Has More Women Cheaters Than Men                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_12">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/delhi-is-officially-indias-rape-capital-and-nagaland-is-the-safest-state-for-women-244434.html" title="Nagaland Is The Safest State For Women While Delhi Is Officially India's Rape Capital" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage-5555_1440251464_236x111.jpg" border="0" alt="Nagaland Is The Safest State For Women While Delhi Is Officially India's Rape Capital"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/delhi-is-officially-indias-rape-capital-and-nagaland-is-the-safest-state-for-women-244434.html" title="Nagaland Is The Safest State For Women While Delhi Is Officially India's Rape Capital">
                            Nagaland Is The Safest State For Women While Delhi Is Officially India's Rape Capital                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_13">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/huntington-batman-comes-to-the-rescue-of-a-child-after-baltimore-batman-passes-away-244430.html" title="Huntington Batman Comes To The Rescue Of A Child, After Baltimore Batman Passes Away" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440245913_236x111.jpg" border="0" alt="Huntington Batman Comes To The Rescue Of A Child, After Baltimore Batman Passes Away"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/huntington-batman-comes-to-the-rescue-of-a-child-after-baltimore-batman-passes-away-244430.html" title="Huntington Batman Comes To The Rescue Of A Child, After Baltimore Batman Passes Away">
                            Huntington Batman Comes To The Rescue Of A Child, After Baltimore Batman Passes Away                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                     
                <div class="life-panel-list cf">
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/shopkeeper-gaurav-gera-gives-sunny-leone-exactly-what-she-asks-for-244450.html'>video</a>                          
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/videocafe/shopkeeper-gaurav-gera-gives-sunny-leone-exactly-what-she-asks-for-244450.html" class="tint" title="Shopkeeper Gaurav Gera Gives Sunny Leone Exactly What She Asks For">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/sunnydubsmash_card_1440317772_502x234.jpg" border="0" alt="http://www.indiatimes.com/videocafe/shopkeeper-gaurav-gera-gives-sunny-leone-exactly-what-she-asks-for-244450.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/shopkeeper-gaurav-gera-gives-sunny-leone-exactly-what-she-asks-for-244450.html" title="Shopkeeper Gaurav Gera Gives Sunny Leone Exactly What She Asks For">
                            Shopkeeper Gaurav Gera Gives Sunny Leone Exactly What She Asks For                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/lifestyle/9-traits-of-the-urban-dog-lover-and-his-love-for-mans-best-friend-244340.html" class="tint" title="9 Traits Of The Urban Dog Lover And His Love For Man's Best Friend">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/cp_1439980516_502x234.jpg" border="0" alt="http://www.indiatimes.com/lifestyle/9-traits-of-the-urban-dog-lover-and-his-love-for-mans-best-friend-244340.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/9-traits-of-the-urban-dog-lover-and-his-love-for-mans-best-friend-244340.html" title="9 Traits Of The Urban Dog Lover And His Love For Man's Best Friend">
                            9 Traits Of The Urban Dog Lover And His Love For Man's Best Friend                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/healthyliving/17-important-facts-about-the-pill-you-need-to-know-before-popping-one-244400.html" class="tint" title="17 Important Facts About The Pill You Need To Know Before Popping One">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card-1_1440150623_502x234.jpg" border="0" alt="http://www.indiatimes.com/health/healthyliving/17-important-facts-about-the-pill-you-need-to-know-before-popping-one-244400.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/17-important-facts-about-the-pill-you-need-to-know-before-popping-one-244400.html" title="17 Important Facts About The Pill You Need To Know Before Popping One">
                            17 Important Facts About The Pill You Need To Know Before Popping One                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_10"><!--trending-panel-list start-->
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/shopkeeper-gaurav-gera-gives-sunny-leone-exactly-what-she-asks-for-244450.html'>video</a>		

                        <a href="http://www.indiatimes.com/videocafe/shopkeeper-gaurav-gera-gives-sunny-leone-exactly-what-she-asks-for-244450.html" class="tint" title="Shopkeeper Gaurav Gera Gives Sunny Leone Exactly What She Asks For">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/sunnydubsmash_card_1440317772_218x102.jpg" border="0" alt="Shopkeeper Gaurav Gera Gives Sunny Leone Exactly What She Asks For"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/shopkeeper-gaurav-gera-gives-sunny-leone-exactly-what-she-asks-for-244450.html" title="Shopkeeper Gaurav Gera Gives Sunny Leone Exactly What She Asks For">
                            Shopkeeper Gaurav Gera Gives Sunny Leone Exactly What She Asks For                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_11"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/lifestyle/self/10-simple-english-mistakes-we-all-make-and-how-to-not-make-them-232873.html" class="tint" title="10 Common English Mistakes We All Make And How To NOT Make Them">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/May/502_1432274983_218x102.jpg" border="0" alt="10 Common English Mistakes We All Make And How To NOT Make Them"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/10-simple-english-mistakes-we-all-make-and-how-to-not-make-them-232873.html" title="10 Common English Mistakes We All Make And How To NOT Make Them">
                            10 Common English Mistakes We All Make And How To NOT Make Them                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_12"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/health/35-weight-loss-exercises-you-can-do-at-home-with-zero-equipment-244337.html" class="tint" title="35 Weight Loss Exercises You Can Do At Home With Zero Equipment">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/weight-loss-fb_1439979652_218x102.gif" border="0" alt="35 Weight Loss Exercises You Can Do At Home With Zero Equipment"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/35-weight-loss-exercises-you-can-do-at-home-with-zero-equipment-244337.html" title="35 Weight Loss Exercises You Can Do At Home With Zero Equipment">
                            35 Weight Loss Exercises You Can Do At Home With Zero Equipment                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_13"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/celebs/indias-bravest-dancer-set-to-have-both-feet-flying-and-her-head-held-high-244426.html" class="tint" title="India's Bravest Dancer Set To Have Both Feet Flying And Her Head Held High">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage-55_1440237864_218x102.jpg" border="0" alt="India's Bravest Dancer Set To Have Both Feet Flying And Her Head Held High"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/indias-bravest-dancer-set-to-have-both-feet-flying-and-her-head-held-high-244426.html" title="India's Bravest Dancer Set To Have Both Feet Flying And Her Head Held High">
                            India's Bravest Dancer Set To Have Both Feet Flying And Her Head Held High                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_14"><!--trending-panel-list start-->
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/alcohol-isnt-allowed-on-delhi-metro-but-drunk-cops-totally-legit-244429.html'>video</a>		

                        <a href="http://www.indiatimes.com/videocafe/alcohol-isnt-allowed-on-delhi-metro-but-drunk-cops-totally-legit-244429.html" class="tint" title="This Delhi Policeman Is So Drunk On The Delhi Metro, He Can't Even Stand. And All This In His Uniform!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/cop-6_1440242816_1440242820_218x102.jpg" border="0" alt="This Delhi Policeman Is So Drunk On The Delhi Metro, He Can't Even Stand. And All This In His Uniform!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/alcohol-isnt-allowed-on-delhi-metro-but-drunk-cops-totally-legit-244429.html" title="This Delhi Policeman Is So Drunk On The Delhi Metro, He Can't Even Stand. And All This In His Uniform!">
                            This Delhi Policeman Is So Drunk On The Delhi Metro, He Can't Even Stand. And All This In His Uniform!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div><!--trending-panel end-->

    </section><!--container end-->

<!------------------------------- end Block 3 ---------------------------------------->
<section class="big-ads">
<div id="bigAd3_slot"></div>
</section>

<!------------------------------- start Block4  -------------------------------------->

    <section class="container cf" id="container4"><!--container start-->
        <div class="news-panel cf ">
            <h2>news</h2>

                            <div class="news-panel-list cf" id="column1_14">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/sports/daily-wage-earner-iftekhar-khan-enters-kickboxing-nationals-dreams-of-serving-the-army-244431.html" title="Daily-Wage Earner Iftekhar Khan Enters Kickboxing Nationals, Dreams Of Serving The Army" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/iftekharkickboxing_1440246733_236x111.jpg" border="0" alt="Daily-Wage Earner Iftekhar Khan Enters Kickboxing Nationals, Dreams Of Serving The Army"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/daily-wage-earner-iftekhar-khan-enters-kickboxing-nationals-dreams-of-serving-the-army-244431.html" title="Daily-Wage Earner Iftekhar Khan Enters Kickboxing Nationals, Dreams Of Serving The Army">
                            Daily-Wage Earner Iftekhar Khan Enters Kickboxing Nationals, Dreams Of Serving The Army                        </a>
                    </figcaption> 
                </div>
                <div class='container1'>                <div class="news-panel-list cf" id="column1_15">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/weird/7-innovative-drug-smuggling-attempts-which-will-make-you-jealous-of-criminal-masterminds-243476.html" title="Hiding Cocaine In Sarees + 7 Other Innovative Drug Smuggling Attempts Which Will Make You Jealous Of Criminals" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/card-card_1437988321_236x111.jpg" border="0" alt="Hiding Cocaine In Sarees + 7 Other Innovative Drug Smuggling Attempts Which Will Make You Jealous Of Criminals"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/weird/7-innovative-drug-smuggling-attempts-which-will-make-you-jealous-of-criminal-masterminds-243476.html" title="Hiding Cocaine In Sarees + 7 Other Innovative Drug Smuggling Attempts Which Will Make You Jealous Of Criminals">
                            Hiding Cocaine In Sarees + 7 Other Innovative Drug Smuggling Attempts Which Will Make You Jealous Of Criminals                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_16">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/weird/youll-never-believe-chinas-incentive-for-male-programmers-pretty-girls-244424.html" title="You'll Never Believe China's Incentive For Male Programmers - Office Cheerleaders." class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/1_1440233875_1440233882_236x111.jpg" border="0" alt="You'll Never Believe China's Incentive For Male Programmers - Office Cheerleaders."/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/weird/youll-never-believe-chinas-incentive-for-male-programmers-pretty-girls-244424.html" title="You'll Never Believe China's Incentive For Male Programmers - Office Cheerleaders.">
                            You'll Never Believe China's Incentive For Male Programmers - Office Cheerleaders.                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_17">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india-has-proof-that-dawood-ibrahim-is-in-pakistan-after-yakub-will-we-hang-the-mumbai-blast-mastermind-244423.html" title="India Has Proof That Dawood Ibrahim Is In Pakistan. After Yakub, Will We Hang The Mumbai Blast Mastermind?" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/ht-5_1440231698_1440231700_236x111.jpg" border="0" alt="India Has Proof That Dawood Ibrahim Is In Pakistan. After Yakub, Will We Hang The Mumbai Blast Mastermind?"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india-has-proof-that-dawood-ibrahim-is-in-pakistan-after-yakub-will-we-hang-the-mumbai-blast-mastermind-244423.html" title="India Has Proof That Dawood Ibrahim Is In Pakistan. After Yakub, Will We Hang The Mumbai Blast Mastermind?">
                            India Has Proof That Dawood Ibrahim Is In Pakistan. After Yakub, Will We Hang The Mumbai Blast Mastermind?                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_18">
                    <figure>
                        <div class="hash-tag">
                                                        <a href="http://www.indiatimes.com/news/sports/harbhajan-singh-to-marry-starlet-girlfriend-geeta-basra-in-october-244418.html" title="Harbhajan Singh To Marry Starlet Girlfriend Geeta Basra In October" class="pink">
                                Waah Bhai Waah!                            </a>1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/sports/harbhajan-singh-to-marry-starlet-girlfriend-geeta-basra-in-october-244418.html" title="Harbhajan Singh To Marry Starlet Girlfriend Geeta Basra In October" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/harbhajangeeta_1440226360_236x111.jpg" border="0" alt="Harbhajan Singh To Marry Starlet Girlfriend Geeta Basra In October"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/harbhajan-singh-to-marry-starlet-girlfriend-geeta-basra-in-october-244418.html" title="Harbhajan Singh To Marry Starlet Girlfriend Geeta Basra In October">
                            Harbhajan Singh To Marry Starlet Girlfriend Geeta Basra In October                        </a>
                    </figcaption> 
                </div>
                 
        </div>
    </div><!--news-panel end-->

    <div class="life-panel cf container2" id="b4c2"><!--life-panel start-->
        <h2>lifestyle</h2>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/just-so-you-dont-forget-heres-what-india-loled-at-in-the-past-8-months-244440.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/just-so-you-dont-forget-heres-what-india-loled-at-in-the-past-8-months-244440.html" class="tint" title="Just So You Don't Forget, Here's What India LOLed At In The Past 8 Months">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/gandutv_card_1440268970_502x234.jpg" border="0" alt="Just So You Don't Forget, Here's What India LOLed At In The Past 8 Months" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/just-so-you-dont-forget-heres-what-india-loled-at-in-the-past-8-months-244440.html" title="Just So You Don't Forget, Here's What India LOLed At In The Past 8 Months">
                        Just So You Don't Forget, Here's What India LOLed At In The Past 8 Months                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-hangover-+-zindagi-na-milegi-dobara-mashup-will-make-you-laugh-so-hard-youll-cry-244443.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-hangover-+-zindagi-na-milegi-dobara-mashup-will-make-you-laugh-so-hard-youll-cry-244443.html" class="tint" title="This Hangover + Zindagi Na Milegi Dobara Mashup Will Make You Laugh So Hard You'll Cry!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/hang_card_1440308985_502x234.jpg" border="0" alt="This Hangover + Zindagi Na Milegi Dobara Mashup Will Make You Laugh So Hard You'll Cry!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-hangover-+-zindagi-na-milegi-dobara-mashup-will-make-you-laugh-so-hard-youll-cry-244443.html" title="This Hangover + Zindagi Na Milegi Dobara Mashup Will Make You Laugh So Hard You'll Cry!">
                        This Hangover + Zindagi Na Milegi Dobara Mashup Will Make You Laugh So Hard You'll Cry!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/food/11-makeinamug-recipes-that-will-get-you-into-the-kitchen-244382.html" class="tint" title="11 Make-In-A-Mug Recipes That Will Get You Into The Kitchen">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/cp_1440067403_502x234.jpg" border="0" alt="11 Make-In-A-Mug Recipes That Will Get You Into The Kitchen" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/food/11-makeinamug-recipes-that-will-get-you-into-the-kitchen-244382.html" title="11 Make-In-A-Mug Recipes That Will Get You Into The Kitchen">
                        11 Make-In-A-Mug Recipes That Will Get You Into The Kitchen                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/9-best-songs-of-kk-you-must-have-in-your-playlist-244401.html" class="tint" title="9 Best Tracks Of KK You Must Have In Your Playlist">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440159923_1440159931_502x234.jpg" border="0" alt="9 Best Tracks Of KK You Must Have In Your Playlist" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/9-best-songs-of-kk-you-must-have-in-your-playlist-244401.html" title="9 Best Tracks Of KK You Must Have In Your Playlist">
                        9 Best Tracks Of KK You Must Have In Your Playlist                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/14-sneaky-ways-technology-is-hurting-your-health-and-how-to-stop-it-244339.html" class="tint" title="14 Sneaky Ways Technology Is Hurting Your Health, And How To Stop It">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/tech_1440062735_502x234.jpg" border="0" alt="14 Sneaky Ways Technology Is Hurting Your Health, And How To Stop It" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/14-sneaky-ways-technology-is-hurting-your-health-and-how-to-stop-it-244339.html" title="14 Sneaky Ways Technology Is Hurting Your Health, And How To Stop It">
                        14 Sneaky Ways Technology Is Hurting Your Health, And How To Stop It                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/the-trailer-of-talvar-based-on-aarushi-murder-case-is-out-and-it-promises-to-unravel-the-mystery-244441.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/the-trailer-of-talvar-based-on-aarushi-murder-case-is-out-and-it-promises-to-unravel-the-mystery-244441.html" class="tint" title="The Trailer Of 'Talvar' Based On Aarushi Murder Case Is Out And It Promises To Unravel The Mystery!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/talvar_card_1440307194_502x234.jpg" border="0" alt="The Trailer Of 'Talvar' Based On Aarushi Murder Case Is Out And It Promises To Unravel The Mystery!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/the-trailer-of-talvar-based-on-aarushi-murder-case-is-out-and-it-promises-to-unravel-the-mystery-244441.html" title="The Trailer Of 'Talvar' Based On Aarushi Murder Case Is Out And It Promises To Unravel The Mystery!">
                        The Trailer Of 'Talvar' Based On Aarushi Murder Case Is Out And It Promises To Unravel The Mystery!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/this-is-the-ultimate-guide-to-saying-sorry-like-you-actually-mean-it-244307.html" class="tint" title="This Is The Ultimate Guide To Saying 'Sorry' Like You Actually Mean It.">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1439897570_502x234.jpg" border="0" alt="This Is The Ultimate Guide To Saying 'Sorry' Like You Actually Mean It." class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/this-is-the-ultimate-guide-to-saying-sorry-like-you-actually-mean-it-244307.html" title="This Is The Ultimate Guide To Saying 'Sorry' Like You Actually Mean It.">
                        This Is The Ultimate Guide To Saying 'Sorry' Like You Actually Mean It.                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/methi-seed-extract-can-sort-out-all-your-menstrual-woes-244329.html" class="tint" title="Methi Seed Extract Can Sort Out All Your Menstrual Woes!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/fenugr_1439972587_502x234.gif" border="0" alt="Methi Seed Extract Can Sort Out All Your Menstrual Woes!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/methi-seed-extract-can-sort-out-all-your-menstrual-woes-244329.html" title="Methi Seed Extract Can Sort Out All Your Menstrual Woes!">
                        Methi Seed Extract Can Sort Out All Your Menstrual Woes!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/17-years-after-dil-se-king-khan-keeps-the-spirit-alive-with-a-dubsmash-of-chhaiyya-chhaiyya-244428.html" class="tint" title="17 Years After Dil Se, King Khan Keeps The Spirit Alive With A Dubsmash Of Chhaiyya Chhaiyya">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/cp_1440242764_502x234.jpg" border="0" alt="17 Years After Dil Se, King Khan Keeps The Spirit Alive With A Dubsmash Of Chhaiyya Chhaiyya" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/17-years-after-dil-se-king-khan-keeps-the-spirit-alive-with-a-dubsmash-of-chhaiyya-chhaiyya-244428.html" title="17 Years After Dil Se, King Khan Keeps The Spirit Alive With A Dubsmash Of Chhaiyya Chhaiyya">
                        17 Years After Dil Se, King Khan Keeps The Spirit Alive With A Dubsmash Of Chhaiyya Chhaiyya                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/alcohol-isnt-allowed-on-delhi-metro-but-drunk-cops-totally-legit-244429.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/alcohol-isnt-allowed-on-delhi-metro-but-drunk-cops-totally-legit-244429.html" class="tint" title="This Delhi Policeman Is So Drunk On The Delhi Metro, He Can't Even Stand. And All This In His Uniform!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/cop-6_1440242816_1440242820_502x234.jpg" border="0" alt="This Delhi Policeman Is So Drunk On The Delhi Metro, He Can't Even Stand. And All This In His Uniform!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/alcohol-isnt-allowed-on-delhi-metro-but-drunk-cops-totally-legit-244429.html" title="This Delhi Policeman Is So Drunk On The Delhi Metro, He Can't Even Stand. And All This In His Uniform!">
                        This Delhi Policeman Is So Drunk On The Delhi Metro, He Can't Even Stand. And All This In His Uniform!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/pure-and-pious-spoof-of-radhe-ma-lade-ma-is-all-that-you-need-to-watch-today-244425.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/pure-and-pious-spoof-of-radhe-ma-lade-ma-is-all-that-you-need-to-watch-today-244425.html" class="tint" title="Pure And Pious Spoof Of Radhe Ma, La-De Ma Is All That You Need To Watch Today">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/card_1440236592_502x234.jpg" border="0" alt="Pure And Pious Spoof Of Radhe Ma, La-De Ma Is All That You Need To Watch Today" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/pure-and-pious-spoof-of-radhe-ma-lade-ma-is-all-that-you-need-to-watch-today-244425.html" title="Pure And Pious Spoof Of Radhe Ma, La-De Ma Is All That You Need To Watch Today">
                        Pure And Pious Spoof Of Radhe Ma, La-De Ma Is All That You Need To Watch Today                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/13-crazy-things-you-didnt-know-could-happen-on-facebook-244422.html" class="tint" title="13 Crazy Things You Didn't Know Could Happen On Facebook">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440233694_502x234.jpg" border="0" alt="13 Crazy Things You Didn't Know Could Happen On Facebook" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/13-crazy-things-you-didnt-know-could-happen-on-facebook-244422.html" title="13 Crazy Things You Didn't Know Could Happen On Facebook">
                        13 Crazy Things You Didn't Know Could Happen On Facebook                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/indias-bravest-dancer-set-to-have-both-feet-flying-and-her-head-held-high-244426.html" class="tint" title="India's Bravest Dancer Set To Have Both Feet Flying And Her Head Held High">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage-55_1440237864_502x234.jpg" border="0" alt="India's Bravest Dancer Set To Have Both Feet Flying And Her Head Held High" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/indias-bravest-dancer-set-to-have-both-feet-flying-and-her-head-held-high-244426.html" title="India's Bravest Dancer Set To Have Both Feet Flying And Her Head Held High">
                        India's Bravest Dancer Set To Have Both Feet Flying And Her Head Held High                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/35-weight-loss-exercises-you-can-do-at-home-with-zero-equipment-244337.html" class="tint" title="35 Weight Loss Exercises You Can Do At Home With Zero Equipment">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/weight-loss-fb_1439979652_502x234.gif" border="0" alt="35 Weight Loss Exercises You Can Do At Home With Zero Equipment" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/35-weight-loss-exercises-you-can-do-at-home-with-zero-equipment-244337.html" title="35 Weight Loss Exercises You Can Do At Home With Zero Equipment">
                        35 Weight Loss Exercises You Can Do At Home With Zero Equipment                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/travel/11-thenandnow-pictures-of-hill-stations-that-prove-change-is-the-only-constant-244343.html" class="tint" title="11 Then-And-Now Pictures Of Hill Stations That Prove Change Is The Only Constant">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage1_1440048298_1440048312_502x234.jpg" border="0" alt="11 Then-And-Now Pictures Of Hill Stations That Prove Change Is The Only Constant" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/travel/11-thenandnow-pictures-of-hill-stations-that-prove-change-is-the-only-constant-244343.html" title="11 Then-And-Now Pictures Of Hill Stations That Prove Change Is The Only Constant">
                        11 Then-And-Now Pictures Of Hill Stations That Prove Change Is The Only Constant                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/travel/this-photographer-captured-the-past-and-present-of-madras-and-chennai-and-it-is-stunning-244419.html" class="tint" title="This Photographer Captured The Past And Present Of Madras And Chennai And It Is Stunning">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/cp_1440228362_1440228367_502x234.jpg" border="0" alt="This Photographer Captured The Past And Present Of Madras And Chennai And It Is Stunning" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/travel/this-photographer-captured-the-past-and-present-of-madras-and-chennai-and-it-is-stunning-244419.html" title="This Photographer Captured The Past And Present Of Madras And Chennai And It Is Stunning">
                        This Photographer Captured The Past And Present Of Madras And Chennai And It Is Stunning                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/recipes/7-lowfat-egg-recipes-that-are-yummy-healthy-and-diabeticfriendly-244406.html" class="tint" title="7 Low-Fat Egg Recipes That Are Yummy, Healthy And Diabetic-Friendly!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/egg_1440157005_502x234.jpg" border="0" alt="7 Low-Fat Egg Recipes That Are Yummy, Healthy And Diabetic-Friendly!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/recipes/7-lowfat-egg-recipes-that-are-yummy-healthy-and-diabeticfriendly-244406.html" title="7 Low-Fat Egg Recipes That Are Yummy, Healthy And Diabetic-Friendly!">
                        7 Low-Fat Egg Recipes That Are Yummy, Healthy And Diabetic-Friendly!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/karan-johar-spends-15-crore-on-roping-in-academy-winning-makeup-artist-for-rishi-kapoors-look-in-kapoor-sons-244417.html" class="tint" title="Karan Johar Spends 1.5 Crore On Roping In Academy Winning Make-Up Artist For Rishi Kapoor's Look In Kapoor & Sons">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/karan-johar-and-rishi-kapoor_1440222263_1440222305_502x234.jpg" border="0" alt="Karan Johar Spends 1.5 Crore On Roping In Academy Winning Make-Up Artist For Rishi Kapoor's Look In Kapoor & Sons" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/karan-johar-spends-15-crore-on-roping-in-academy-winning-makeup-artist-for-rishi-kapoors-look-in-kapoor-sons-244417.html" title="Karan Johar Spends 1.5 Crore On Roping In Academy Winning Make-Up Artist For Rishi Kapoor's Look In Kapoor & Sons">
                        Karan Johar Spends 1.5 Crore On Roping In Academy Winning Make-Up Artist For Rishi Kapoor's Look In Kapoor & Sons                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/10-simple-english-mistakes-we-all-make-and-how-to-not-make-them-232873.html" class="tint" title="10 Common English Mistakes We All Make And How To NOT Make Them">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/May/502_1432274983_502x234.jpg" border="0" alt="10 Common English Mistakes We All Make And How To NOT Make Them" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/10-simple-english-mistakes-we-all-make-and-how-to-not-make-them-232873.html" title="10 Common English Mistakes We All Make And How To NOT Make Them">
                        10 Common English Mistakes We All Make And How To NOT Make Them                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/9-ridiculous-college-and-hostel-rules-that-will-make-you-pull-out-your-hair-in-frustration-244348.html" class="tint" title="9 Ridiculous College And Hostel Rules That Will Make You Pull Out Your Hair In Frustration">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/kk_1439984079_1439984095_502x234.jpg" border="0" alt="9 Ridiculous College And Hostel Rules That Will Make You Pull Out Your Hair In Frustration" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/9-ridiculous-college-and-hostel-rules-that-will-make-you-pull-out-your-hair-in-frustration-244348.html" title="9 Ridiculous College And Hostel Rules That Will Make You Pull Out Your Hair In Frustration">
                        9 Ridiculous College And Hostel Rules That Will Make You Pull Out Your Hair In Frustration                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/ecigarettes-are-95-less-harmful-than-what-youre-currently-smoking-244389.html" class="tint" title="E-Cigarettes Are 95% Less Harmful Than What You're Currently Smoking!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/smoking_1440072756_502x234.jpg" border="0" alt="E-Cigarettes Are 95% Less Harmful Than What You're Currently Smoking!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/ecigarettes-are-95-less-harmful-than-what-youre-currently-smoking-244389.html" title="E-Cigarettes Are 95% Less Harmful Than What You're Currently Smoking!">
                        E-Cigarettes Are 95% Less Harmful Than What You're Currently Smoking!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/9-reasons-you-shouldnt-worry-about-saving-money-244283.html" class="tint" title="9 Reasons You Shouldn't Worry About Saving Money">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1439882668_502x234.jpg" border="0" alt="9 Reasons You Shouldn't Worry About Saving Money" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/9-reasons-you-shouldnt-worry-about-saving-money-244283.html" title="9 Reasons You Shouldn't Worry About Saving Money">
                        9 Reasons You Shouldn't Worry About Saving Money                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/desi-couples-try-out-kamasutra-positions-for-the-first-time-on-camera-the-results-are-hilarious-244415.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/desi-couples-try-out-kamasutra-positions-for-the-first-time-on-camera-the-results-are-hilarious-244415.html" class="tint" title="Desi Couples Try Out Kamasutra Positions For The First Time On Camera - The Results Are Hilarious!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/yoga502_1440179383_502x234.jpg" border="0" alt="Desi Couples Try Out Kamasutra Positions For The First Time On Camera - The Results Are Hilarious!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/desi-couples-try-out-kamasutra-positions-for-the-first-time-on-camera-the-results-are-hilarious-244415.html" title="Desi Couples Try Out Kamasutra Positions For The First Time On Camera - The Results Are Hilarious!">
                        Desi Couples Try Out Kamasutra Positions For The First Time On Camera - The Results Are Hilarious!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/recipes/8-heart-healthy-recipes-that-can-be-made-within-30minutes_-244306.html" class="tint" title="8 Heart Healthy Recipes That Can Be Made Within 30-Minutes">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/food-card_1439895635_502x234.gif" border="0" alt="8 Heart Healthy Recipes That Can Be Made Within 30-Minutes" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/recipes/8-heart-healthy-recipes-that-can-be-made-within-30minutes_-244306.html" title="8 Heart Healthy Recipes That Can Be Made Within 30-Minutes">
                        8 Heart Healthy Recipes That Can Be Made Within 30-Minutes                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/10-haunted-road-routes-to-avoid-if-youre-scared-of-the-paranormal-244309.html" class="tint" title="10 Haunted Road Routes To Avoid If You're Scared Of The Paranormal">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1439899870_502x234.jpg" border="0" alt="10 Haunted Road Routes To Avoid If You're Scared Of The Paranormal" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/10-haunted-road-routes-to-avoid-if-youre-scared-of-the-paranormal-244309.html" title="10 Haunted Road Routes To Avoid If You're Scared Of The Paranormal">
                        10 Haunted Road Routes To Avoid If You're Scared Of The Paranormal                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/muscle-breakdown-+-3-more-ways-the-body-reacts-when-you-skip-meals-244383.html" class="tint" title="Muscle Breakdown + 3 More Ways The Body Reacts When You Skip Meals">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440072284_502x234.jpg" border="0" alt="Muscle Breakdown + 3 More Ways The Body Reacts When You Skip Meals" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/muscle-breakdown-+-3-more-ways-the-body-reacts-when-you-skip-meals-244383.html" title="Muscle Breakdown + 3 More Ways The Body Reacts When You Skip Meals">
                        Muscle Breakdown + 3 More Ways The Body Reacts When You Skip Meals                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/nasas-mars-rover-uses-a-selfie-stick-to-send-earth-its-love-and-a-selfie-244398.html" class="tint" title="Now NASA's Mars Rover Goes Selfie-Crazy, Takes A Low Angle One Using Its Robotic Arm">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440149893_502x234.jpg" border="0" alt="Now NASA's Mars Rover Goes Selfie-Crazy, Takes A Low Angle One Using Its Robotic Arm" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/nasas-mars-rover-uses-a-selfie-stick-to-send-earth-its-love-and-a-selfie-244398.html" title="Now NASA's Mars Rover Goes Selfie-Crazy, Takes A Low Angle One Using Its Robotic Arm">
                        Now NASA's Mars Rover Goes Selfie-Crazy, Takes A Low Angle One Using Its Robotic Arm                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/14-romantic-illustrations-that-will-give-you-some-serious-relationship-goals-244364.html" class="tint" title="14 Romantic Illustrations That Will Give You Some Serious Relationship Goals">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/cooking_1440053461_1440053479_502x234.jpg" border="0" alt="14 Romantic Illustrations That Will Give You Some Serious Relationship Goals" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/14-romantic-illustrations-that-will-give-you-some-serious-relationship-goals-244364.html" title="14 Romantic Illustrations That Will Give You Some Serious Relationship Goals">
                        14 Romantic Illustrations That Will Give You Some Serious Relationship Goals                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/16-simple-ways-to-balance-your-hormones-naturally-244386.html" class="tint" title="16 Simple Ways To Balance Your Hormones Naturally">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/balance_1440070324_502x234.jpg" border="0" alt="16 Simple Ways To Balance Your Hormones Naturally" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/16-simple-ways-to-balance-your-hormones-naturally-244386.html" title="16 Simple Ways To Balance Your Hormones Naturally">
                        16 Simple Ways To Balance Your Hormones Naturally                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/9-things-about-being-broke-and-only-wanting-the-best-things-in-life-244361.html" class="tint" title="9 Things About Being Broke And Only Wanting The Best Things In Life">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/cp_1440049341_1440049351_502x234.jpg" border="0" alt="9 Things About Being Broke And Only Wanting The Best Things In Life" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/9-things-about-being-broke-and-only-wanting-the-best-things-in-life-244361.html" title="9 Things About Being Broke And Only Wanting The Best Things In Life">
                        9 Things About Being Broke And Only Wanting The Best Things In Life                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/17yearsofdilse-7-reasons-why-srks-dil-se-is-nothing-short-of-a-masterpiece-244359.html" class="tint" title="#17YearsOfDilSe, 7 Reasons Why SRK's Dil Se Is Nothing Short Of A Masterpiece">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/dil-se-card_1440048375_1440048382_502x234.jpg" border="0" alt="#17YearsOfDilSe, 7 Reasons Why SRK's Dil Se Is Nothing Short Of A Masterpiece" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/17yearsofdilse-7-reasons-why-srks-dil-se-is-nothing-short-of-a-masterpiece-244359.html" title="#17YearsOfDilSe, 7 Reasons Why SRK's Dil Se Is Nothing Short Of A Masterpiece">
                        #17YearsOfDilSe, 7 Reasons Why SRK's Dil Se Is Nothing Short Of A Masterpiece                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/11-reasons-good-employees-quit-their-companies-244252.html" class="tint" title="11 Reasons Good Employees Quit Their Companies">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/jobs_1439809094_1439809104_502x234.jpg" border="0" alt="11 Reasons Good Employees Quit Their Companies" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/11-reasons-good-employees-quit-their-companies-244252.html" title="11 Reasons Good Employees Quit Their Companies">
                        11 Reasons Good Employees Quit Their Companies                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/videos/these-palestinians-use-the-war-zone-of-gaza-for-a-gym-whats-your-excuse-244344.html" class="tint" title="These Palestinians Use The War Zone Of Gaza For A Gym! What's Your Excuse?">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/palestine_1440066152_502x234.jpg" border="0" alt="These Palestinians Use The War Zone Of Gaza For A Gym! What's Your Excuse?" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/videos/these-palestinians-use-the-war-zone-of-gaza-for-a-gym-whats-your-excuse-244344.html" title="These Palestinians Use The War Zone Of Gaza For A Gym! What's Your Excuse?">
                        These Palestinians Use The War Zone Of Gaza For A Gym! What's Your Excuse?                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/research-proves-becoming-a-parent-is-easily-the-hardest-thing-youâll-ever-do-244292.html" class="tint" title="Research Proves Becoming A Parent Is Easily The Hardest Thing Youâll Ever Do!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/parenting_1439890022_502x234.jpg" border="0" alt="Research Proves Becoming A Parent Is Easily The Hardest Thing Youâll Ever Do!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/research-proves-becoming-a-parent-is-easily-the-hardest-thing-youâll-ever-do-244292.html" title="Research Proves Becoming A Parent Is Easily The Hardest Thing Youâll Ever Do!">
                        Research Proves Becoming A Parent Is Easily The Hardest Thing Youâll Ever Do!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-woman-makes-ultimate-sacrifice-for-the-nation-not-once-but-twice-244391.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-woman-makes-ultimate-sacrifice-for-the-nation-not-once-but-twice-244391.html" class="tint" title="This Woman Makes Ultimate Sacrifice For The Nation. Not Once But Twice!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/soldier_card_1440075342_502x234.jpg" border="0" alt="This Woman Makes Ultimate Sacrifice For The Nation. Not Once But Twice!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-woman-makes-ultimate-sacrifice-for-the-nation-not-once-but-twice-244391.html" title="This Woman Makes Ultimate Sacrifice For The Nation. Not Once But Twice!">
                        This Woman Makes Ultimate Sacrifice For The Nation. Not Once But Twice!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/the-best-and-worst-of-your-24-hours-according-to-science-244378.html" class="tint" title="The Best And Worst Of Your 24 Hours, According To Science">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card7_1440067237_1440067252_502x234.jpg" border="0" alt="The Best And Worst Of Your 24 Hours, According To Science" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/the-best-and-worst-of-your-24-hours-according-to-science-244378.html" title="The Best And Worst Of Your 24 Hours, According To Science">
                        The Best And Worst Of Your 24 Hours, According To Science                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-video-of-anushka-sharma-proves-that-karaoke-is-not-for-everyone-244381.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-video-of-anushka-sharma-proves-that-karaoke-is-not-for-everyone-244381.html" class="tint" title="This Video Of Anushka Sharma Proves That Karaoke Is Not For Everyone!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/anushkasharma_card_1440074392_502x234.jpg" border="0" alt="This Video Of Anushka Sharma Proves That Karaoke Is Not For Everyone!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-video-of-anushka-sharma-proves-that-karaoke-is-not-for-everyone-244381.html" title="This Video Of Anushka Sharma Proves That Karaoke Is Not For Everyone!">
                        This Video Of Anushka Sharma Proves That Karaoke Is Not For Everyone!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/man-honours-late-wife-by-planting-400-acres-of-sunflowers-+-an-indian-love-saga-that-will-warm-your-heart-immensely-244380.html" class="tint" title="Man Honours Late Wife By Planting 400 Acres Of Sunflowers + An Indian Love Saga That Will Warm Your Heart Immensely">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/502_1440069404_502x234.jpg" border="0" alt="Man Honours Late Wife By Planting 400 Acres Of Sunflowers + An Indian Love Saga That Will Warm Your Heart Immensely" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/man-honours-late-wife-by-planting-400-acres-of-sunflowers-+-an-indian-love-saga-that-will-warm-your-heart-immensely-244380.html" title="Man Honours Late Wife By Planting 400 Acres Of Sunflowers + An Indian Love Saga That Will Warm Your Heart Immensely">
                        Man Honours Late Wife By Planting 400 Acres Of Sunflowers + An Indian Love Saga That Will Warm Your Heart Immensely                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/ranveerdeepika-celebrate-bajiraos-315th-birthday-with-his-descendants-244366.html" class="tint" title="Ranveer-Deepika Celebrate Bajirao's 315th Birthday With His Descendants!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collageggg_1440057099_1440057106_502x234.jpg" border="0" alt="Ranveer-Deepika Celebrate Bajirao's 315th Birthday With His Descendants!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/ranveerdeepika-celebrate-bajiraos-315th-birthday-with-his-descendants-244366.html" title="Ranveer-Deepika Celebrate Bajirao's 315th Birthday With His Descendants!">
                        Ranveer-Deepika Celebrate Bajirao's 315th Birthday With His Descendants!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/lalu-prasad-yadavs-ribtickling-mimicry-of-pm-narendra-modi-+-10-performances-which-are-worthy-of-an-oscar-244375.html" class="tint" title="Lalu Prasad Yadav's Rib-Tickling Mimicry Of PM Narendra Modi + 10 Performances Which Are Worthy Of An Oscar!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/modi_card_1440068402_502x234.jpg" border="0" alt="Lalu Prasad Yadav's Rib-Tickling Mimicry Of PM Narendra Modi + 10 Performances Which Are Worthy Of An Oscar!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/lalu-prasad-yadavs-ribtickling-mimicry-of-pm-narendra-modi-+-10-performances-which-are-worthy-of-an-oscar-244375.html" title="Lalu Prasad Yadav's Rib-Tickling Mimicry Of PM Narendra Modi + 10 Performances Which Are Worthy Of An Oscar!">
                        Lalu Prasad Yadav's Rib-Tickling Mimicry Of PM Narendra Modi + 10 Performances Which Are Worthy Of An Oscar!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/14-natural-ways-to-get-rid-of-acne-forever-244295.html" class="tint" title="14 Natural Ways To Get Rid of Acne Forever!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/acne-card_1439893051_502x234.gif" border="0" alt="14 Natural Ways To Get Rid of Acne Forever!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/14-natural-ways-to-get-rid-of-acne-forever-244295.html" title="14 Natural Ways To Get Rid of Acne Forever!">
                        14 Natural Ways To Get Rid of Acne Forever!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/11-things-you-will-love-and-hate-about-dating-a-shy-girl-244322.html" class="tint" title="11 Things You Will Love And Hate About Dating A Shy Girl">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/dpcard_1439974979_1439975010_1439975017_502x234.jpg" border="0" alt="11 Things You Will Love And Hate About Dating A Shy Girl" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/11-things-you-will-love-and-hate-about-dating-a-shy-girl-244322.html" title="11 Things You Will Love And Hate About Dating A Shy Girl">
                        11 Things You Will Love And Hate About Dating A Shy Girl                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/videos/6-indian-health-concepts-that-are-taking-the-west-by-storm-244328.html" class="tint" title="6 Indian Health Concepts That Are Taking The West By Storm">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card-1_1439971166_502x234.jpg" border="0" alt="6 Indian Health Concepts That Are Taking The West By Storm" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/videos/6-indian-health-concepts-that-are-taking-the-west-by-storm-244328.html" title="6 Indian Health Concepts That Are Taking The West By Storm">
                        6 Indian Health Concepts That Are Taking The West By Storm                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/12-gmail-hacks-that-will-sort-out-your-email-woes-for-good-244313.html" class="tint" title="12 Gmail Hacks That Will Sort Out Your Email Woes For Good">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/email_1439973333_1439973344_502x234.jpg" border="0" alt="12 Gmail Hacks That Will Sort Out Your Email Woes For Good" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/12-gmail-hacks-that-will-sort-out-your-email-woes-for-good-244313.html" title="12 Gmail Hacks That Will Sort Out Your Email Woes For Good">
                        12 Gmail Hacks That Will Sort Out Your Email Woes For Good                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/jacqueline-to-replace-katrina-in-bang-bang-sequel-another-katfight-in-the-future-yes-probably-244299.html" class="tint" title="9 Times When Film-Makers Found Easy Replacement For Their Actresses">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1439892464_1439892480_502x234.jpg" border="0" alt="9 Times When Film-Makers Found Easy Replacement For Their Actresses" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/jacqueline-to-replace-katrina-in-bang-bang-sequel-another-katfight-in-the-future-yes-probably-244299.html" title="9 Times When Film-Makers Found Easy Replacement For Their Actresses">
                        9 Times When Film-Makers Found Easy Replacement For Their Actresses                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/these-9-foods-are-turning-your-pearly-whites-yellow-244261.html" class="tint" title="These 9 Foods Are Turning Your Pearly Whites Yellow">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/teeth_1439815967_502x234.jpg" border="0" alt="These 9 Foods Are Turning Your Pearly Whites Yellow" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/these-9-foods-are-turning-your-pearly-whites-yellow-244261.html" title="These 9 Foods Are Turning Your Pearly Whites Yellow">
                        These 9 Foods Are Turning Your Pearly Whites Yellow                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/lalbaugcha-raja-how-indias-most-famous-ganapati-idol-has-evolved-over-the-past-81-years-244356.html" class="tint" title="77 Incarnations Of India's Most Famous Ganapati, Mumbai's Lalbaugcha Raja">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/lalbaugh_1439989679_1439989691_502x234.jpg" border="0" alt="77 Incarnations Of India's Most Famous Ganapati, Mumbai's Lalbaugcha Raja" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/lalbaugcha-raja-how-indias-most-famous-ganapati-idol-has-evolved-over-the-past-81-years-244356.html" title="77 Incarnations Of India's Most Famous Ganapati, Mumbai's Lalbaugcha Raja">
                        77 Incarnations Of India's Most Famous Ganapati, Mumbai's Lalbaugcha Raja                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/specials/transgender-project/breakingbarriers-manabi-bandhopadhyay-set-to-be-worlds-first-transgender-principal-244153.html" class="tint" title="#BreakingBarriers Manabi Bandhopadhyay Became World's First Transgender Principal">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/bigimage-cardimg_1439550589_502x234.jpg" border="0" alt="#BreakingBarriers Manabi Bandhopadhyay Became World's First Transgender Principal" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/specials/transgender-project/breakingbarriers-manabi-bandhopadhyay-set-to-be-worlds-first-transgender-principal-244153.html" title="#BreakingBarriers Manabi Bandhopadhyay Became World's First Transgender Principal">
                        #BreakingBarriers Manabi Bandhopadhyay Became World's First Transgender Principal                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/7-reasons-planning-in-advance-isnt-always-the-best-plan-youve-had-244200.html" class="tint" title="7 Reasons Planning In Advance Isn't Always The Best Plan You've Had">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/cp_1439645274_502x234.jpg" border="0" alt="7 Reasons Planning In Advance Isn't Always The Best Plan You've Had" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/7-reasons-planning-in-advance-isnt-always-the-best-plan-youve-had-244200.html" title="7 Reasons Planning In Advance Isn't Always The Best Plan You've Had">
                        7 Reasons Planning In Advance Isn't Always The Best Plan You've Had                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/work-is-painful-enough-doesnt-mean-your-back-should-bear-the-brunt-244285.html" class="tint" title="Work Is Painful Enough. Doesn't Mean Your Back Should Bear The Brunt">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/posture-card_1439885032_502x234.gif" border="0" alt="Work Is Painful Enough. Doesn't Mean Your Back Should Bear The Brunt" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/work-is-painful-enough-doesnt-mean-your-back-should-bear-the-brunt-244285.html" title="Work Is Painful Enough. Doesn't Mean Your Back Should Bear The Brunt">
                        Work Is Painful Enough. Doesn't Mean Your Back Should Bear The Brunt                    </a>
                </figcaption>
            </div>
        
        <script id="HpMiddleBlock_tpl" type="text/x-handlebars-template">
            {{#if MoreData}}
            {{#each MoreData}} 
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>


                    <a  class='video-btn sprite' href="{{guid}}" style="display:{{card_id}}"> </a>

    				{{#if label_name.length}}
                    <a href="{{guid}}" class="{{label_name}} sticker">&nbsp;</a>
    						 {{/if}} 
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="{{guid}}" title="{{carousal_headline}}" class="tint">
                        <img class="greyBg lazy" data-original="{{thumbnail}}" border="0" alt="{{carousal_headline}}" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="{{guid}}" title="{{carousal_headline}}">
                        {{carousal_headline}}
                    </a>
                </figcaption>
            </div>  
            {{/each}} 
            {{/if}}

            </script>

        </div><!--life-panel end-->

        <div class="trending-panel cf "><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_15"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/lifestyle/technology/13-crazy-things-you-didnt-know-could-happen-on-facebook-244422.html" class="tint" title="13 Crazy Things You Didn't Know Could Happen On Facebook">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1440233694_218x102.jpg" border="0" alt="13 Crazy Things You Didn't Know Could Happen On Facebook"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/technology/13-crazy-things-you-didnt-know-could-happen-on-facebook-244422.html" title="13 Crazy Things You Didn't Know Could Happen On Facebook">
                            13 Crazy Things You Didn't Know Could Happen On Facebook                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                <div class='container3'>                <div class="trending-panel-list cf" id="column3_16"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/culture/travel/this-photographer-captured-the-past-and-present-of-madras-and-chennai-and-it-is-stunning-244419.html" class="tint" title="This Photographer Captured The Past And Present Of Madras And Chennai And It Is Stunning">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/cp_1440228362_1440228367_218x102.jpg" border="0" alt="This Photographer Captured The Past And Present Of Madras And Chennai And It Is Stunning"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/travel/this-photographer-captured-the-past-and-present-of-madras-and-chennai-and-it-is-stunning-244419.html" title="This Photographer Captured The Past And Present Of Madras And Chennai And It Is Stunning">
                            This Photographer Captured The Past And Present Of Madras And Chennai And It Is Stunning                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_17"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/culture/travel/11-thenandnow-pictures-of-hill-stations-that-prove-change-is-the-only-constant-244343.html" class="tint" title="11 Then-And-Now Pictures Of Hill Stations That Prove Change Is The Only Constant">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage1_1440048298_1440048312_218x102.jpg" border="0" alt="11 Then-And-Now Pictures Of Hill Stations That Prove Change Is The Only Constant"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/travel/11-thenandnow-pictures-of-hill-stations-that-prove-change-is-the-only-constant-244343.html" title="11 Then-And-Now Pictures Of Hill Stations That Prove Change Is The Only Constant">
                            11 Then-And-Now Pictures Of Hill Stations That Prove Change Is The Only Constant                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_18"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/culture/who-we-are/9-ridiculous-college-and-hostel-rules-that-will-make-you-pull-out-your-hair-in-frustration-244348.html" class="tint" title="9 Ridiculous College And Hostel Rules That Will Make You Pull Out Your Hair In Frustration">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/kk_1439984079_1439984095_218x102.jpg" border="0" alt="9 Ridiculous College And Hostel Rules That Will Make You Pull Out Your Hair In Frustration"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/9-ridiculous-college-and-hostel-rules-that-will-make-you-pull-out-your-hair-in-frustration-244348.html" title="9 Ridiculous College And Hostel Rules That Will Make You Pull Out Your Hair In Frustration">
                            9 Ridiculous College And Hostel Rules That Will Make You Pull Out Your Hair In Frustration                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_19"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/bollywood/17-years-after-dil-se-king-khan-keeps-the-spirit-alive-with-a-dubsmash-of-chhaiyya-chhaiyya-244428.html" class="tint" title="17 Years After Dil Se, King Khan Keeps The Spirit Alive With A Dubsmash Of Chhaiyya Chhaiyya">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/cp_1440242764_218x102.jpg" border="0" alt="17 Years After Dil Se, King Khan Keeps The Spirit Alive With A Dubsmash Of Chhaiyya Chhaiyya"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/17-years-after-dil-se-king-khan-keeps-the-spirit-alive-with-a-dubsmash-of-chhaiyya-chhaiyya-244428.html" title="17 Years After Dil Se, King Khan Keeps The Spirit Alive With A Dubsmash Of Chhaiyya Chhaiyya">
                            17 Years After Dil Se, King Khan Keeps The Spirit Alive With A Dubsmash Of Chhaiyya Chhaiyya                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                
        </div>

    </div><!--trending-panel end-->

    </section>
    <section class="container cf" id="loader" style="display: none;border: 0px solid red;">
    <div class="news-panel cf ">&nbsp;</div>
    <div class="life-panel cf" style="text-align: center;"><img src="http://media.indiatimes.in/resources/images/loading.gif" border="0" /></div>
    <div class="trending-panel cf ">&nbsp;</div>
</section>
<section class="big-ads remove-fixed-home" id="adfooter">
<div id="badRos_slot"></div> 
</section>   

<!------------------------ end Block 4------------------------------------------------>
<!--container end-->

<script type="text/javascript">
   
    $('#hp_block_2').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible     
       showBigAD2('bigAd2_slot');
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#hp_block_3').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    if (visiblePartY == 'top') {
      // top part of element is visible

    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible  
      showBigAD3('bigAd3_slot');
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#container4').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible

       BADros('badRos_slot');
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});

$(document).ready(function(){
        var trigger_depth={b2:99, b3:499, b4:1199};
        var is_trigger_active={b2:true, b3:true, b4:true};
        var call_on_scroll=true;
        
            $(window).on("scroll",function(){
            if((!(is_trigger_active.b2)&&!(is_trigger_active.b3)&&!(is_trigger_active.b4))==false)
            {
                callPreload(is_trigger_active,trigger_depth);
                
            }
    });
});

function callPreload(active,depth)
{
    var scroll_top = $(window).scrollTop();
    if((active.b2)&&(scroll_top > depth.b2))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Aug/dron502_1440334741_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/1-6_1440317196_1440317204_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage-555_1440315391_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/weed502_1440313182_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/awdnews-5_1440313856_1440313860_236x111.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/picmonkey-collage-555_1440329475_502x234.jpg","http://media.indiatimes.in/media/content/2015/Jul/card_1438323459_1438323470_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage-555_1440323864_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/sunny_1440064012_1440064028_218x102.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/picmonkey-collage-555_1440329475_218x102.jpg","http://media.indiatimes.in/media/content/2015/Aug/cp_1440067403_218x102.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1439897570_218x102.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/gandutv_card_1440268970_218x102.jpg");
        active.b2=false;
    }
    if((active.b3)&&(scroll_top > depth.b3))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Aug/airshow502_1440308306_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/block-4_1440247618_1440247622_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/capture2-fb_1440307834_1440307848_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage-5555_1440251464_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1440245913_236x111.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/sunnydubsmash_card_1440317772_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/cp_1439980516_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card-1_1440150623_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/sunnydubsmash_card_1440317772_218x102.jpg","http://media.indiatimes.in/media/content/2015/May/502_1432274983_218x102.jpg","http://media.indiatimes.in/media/content/2015/Aug/weight-loss-fb_1439979652_218x102.gif","http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage-55_1440237864_218x102.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/cop-6_1440242816_1440242820_218x102.jpg");
        active.b3=false;
    }
    
    if((active.b4)&&(scroll_top > depth.b4))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Aug/iftekharkickboxing_1440246733_236x111.jpg","http://media.indiatimes.in/media/content/2015/Jul/card-card_1437988321_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/1_1440233875_1440233882_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/ht-5_1440231698_1440231700_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/harbhajangeeta_1440226360_236x111.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/gandutv_card_1440268970_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/hang_card_1440308985_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/cp_1440067403_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1440159923_1440159931_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/tech_1440062735_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/talvar_card_1440307194_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1439897570_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/fenugr_1439972587_502x234.gif","http://media.indiatimes.in/media/content/2015/Aug/cp_1440242764_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/cop-6_1440242816_1440242820_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/card_1440236592_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1440233694_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage-55_1440237864_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/weight-loss-fb_1439979652_502x234.gif","http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage1_1440048298_1440048312_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/cp_1440228362_1440228367_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/egg_1440157005_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/karan-johar-and-rishi-kapoor_1440222263_1440222305_502x234.jpg","http://media.indiatimes.in/media/content/2015/May/502_1432274983_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/kk_1439984079_1439984095_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/smoking_1440072756_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1439882668_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/yoga502_1440179383_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/food-card_1439895635_502x234.gif","http://media.indiatimes.in/media/content/2015/Aug/card_1439899870_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1440072284_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1440149893_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/cooking_1440053461_1440053479_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/balance_1440070324_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/cp_1440049341_1440049351_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/dil-se-card_1440048375_1440048382_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/jobs_1439809094_1439809104_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/palestine_1440066152_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/parenting_1439890022_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/soldier_card_1440075342_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card7_1440067237_1440067252_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/anushkasharma_card_1440074392_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/502_1440069404_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collageggg_1440057099_1440057106_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/modi_card_1440068402_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/acne-card_1439893051_502x234.gif","http://media.indiatimes.in/media/content/2015/Aug/dpcard_1439974979_1439975010_1439975017_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card-1_1439971166_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/email_1439973333_1439973344_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1439892464_1439892480_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/teeth_1439815967_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/lalbaugh_1439989679_1439989691_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/bigimage-cardimg_1439550589_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/cp_1439645274_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/posture-card_1439885032_502x234.gif","http://media.indiatimes.in/media/content/2015/Aug/card_1440233694_218x102.jpg","http://media.indiatimes.in/media/content/2015/Aug/cp_1440228362_1440228367_218x102.jpg","http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage1_1440048298_1440048312_218x102.jpg","http://media.indiatimes.in/media/content/2015/Aug/kk_1439984079_1439984095_218x102.jpg","http://media.indiatimes.in/media/content/2015/Aug/cp_1440242764_218x102.jpg");
        active.b4=false;
    }
}
</script>





    <div class="clr"></div>
      
	
                 <script> 
				                    showFooterCode();
					            </script>
	           <style>
.bottom-strip{background:#ebebeb;display:block;padding:15px 0;overflow:hidden;}.bottom-strip-box{margin:0 auto;display:table;max-width:410px;width:100%;}.square-logo{width:66px;height:64px;float:left;margin-right:10px;}.bottom-strip-content{float:left;margin-top:14px;}.bottom-strip-content span{display:block;color:#737272;font:12px/1 "RobotoRegular", Arial, Helvetica, sans-serif;margin-bottom:2px;}.bottom-strip-content a{color:#1b85dd;font:24px/1 "RobotoRegular", Arial, Helvetica, sans-serif;text-decoration:none;}
</style>
<div class="last-container">
    <!--<br>--> 
    <!--social-panel start-->
    <div class="social-panel red-bg">
        <div class="soc-inner"><!--soc-inner start-->
            <p class="soc-title">Be a Part of The New & Next</p>
            <div class="soc-count"><!--soc-count start-->
                <a href="https://www.facebook.com/indiatimes" target="_blank" title="facebook" class="sprite soc-fb"></a>
                <p>
                    3,482,600<span class="black"> FRIENDS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://twitter.com/indiatimes" target="_blank" title="twitter" class="sprite soc-twt"></a>
                <p>10343  <span class="black">FOLLOWERS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://plus.google.com/+indiatimes" target="_blank" title="gplus" class="sprite soc-gplus"></a>
                <p>104,163 <span class="black">MEMBERS</span></p>
            </div>
            <div class="soc-count"><!--soc-count start-->
                <a href="javascript:void(0);" title="E-mail" class="sprite soc-mail"></a>
                <p>48,541 <span class="black">SUBSCRIBERS</span></p>
            </div>
            <!--soc-count end-->
        </div>
        <!--soc-inner end-->
    </div>
    <!--social-panel end-->
    <div class="clr"></div>

    <footer>
        <!-- social container start-->
        <div class="socical">
            <div class="containers">
                <div class="fl left_cont"> Get your weekly dose of virality sent to your inbox! </div>
                <div class="fr right_cont">

                    <input type="text" value="Please Enter Email Address" class="Email" name="UserEmail" id="UserEmail" defaultValue="Please Enter Email Address"/>
                    <input type="submit" id="btn-submit" value="SIGN UP" name="btn-submit" class="Subscribe">
                    <span id="subsEmal" style='color: red; padding-left: 66px; text-align: left;'></span>
                </div>

            </div>
        </div><!-- social containers end-->
        <div class="footer"><!-- footer grey start-->
            <div class="sub_container">
                <!-- container start-->
                <div class="cont_new">
                    <h2>Our Channels</h2>
                                                <a href='http://www.indiatimes.com/news/' class="blue size">News</a> 
                                                    <a href='http://www.indiatimes.com/lifestyle/' class="purple size">Lifestyle</a> 
                                                    <a href='http://www.indiatimes.com/entertainment/' class="red size">Entertainment</a> 
                                                    <a href='http://www.indiatimes.com/videocafe/' class="orange size">Videos</a> 
                                                    <a href='http://www.indiatimes.com/health/' class="green size">Health</a> 
                        
                </div><!-- container closed-->

                <div class="cont"> <!-- container start-->
                    <h2>Sections</h2>
                                            <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/news/india/'>india</a>
                                             
                                    <a href='http://www.indiatimes.com/news/world/'>world</a>
                                             
                                    <a href='http://www.indiatimes.com/news/sports/'>sports</a>
                                             
                                    <a href='http://www.indiatimes.com/news/weird/'>weird</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/lifestyle/self/'>self</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/style/'>style</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/who-we-are/'>who we are</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/travel/'>travel</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/food/'>food</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/entertainment/bollywood/'>bollywood</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/celebs/'>celebscoop</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/hollywood/'>hollywood</a>
                                
                             </div>                        <div class="sub_link">
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                             
                                    <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                             
                                    <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                             
                                    <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                             
                                    <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                
                             </div>                    <div class="sub_link"><a href="http://www.indiatimes.com/photogallery">Photogallery</a></div>
                </div><!-- container closed-->
                <div class="cont_new"> <!-- container start-->
                    <h2>Indiatimes Lifestyle Network</h2>

                    <div class="sub_link">                             <a href="http://www.mensxp.com"   target="_blank" >MENS XP</a>
                                                    <a href="http://www.idiva.com"   target="_blank" >iDiva</a>
                                                    <a href="http://luxpresso.com"   target="_blank" >Luxury</a>
                                                    <a href="http://in.askmen.com/"   target="_blank" >Ask Men</a>
                                                    <a href="http://www.pursuitist.in/"   target="_blank" >Pursuitist</a>
                        </div>
                </div><!-- container closed-->
                <div class="cont"> <!-- container start-->
                    <h2>Other Sites</h2>
                    <div class="sub_link"> 
                        
                            <a href="http://www.cricbuzz.com"  target="_blank">CricBuzz</a> 
                        
                            <a href="http://www.happytrips.com/"  target="_blank">Happy Trips</a> 
                        
                            <a href="http://www.businessinsider.in/"  target="_blank">Business Insider</a> 
                        
                            <a href="http://www.zoomtv.com"  target="_blank">ZoomTv</a> 
                        
                            <a href="http://www.gizmodo.in/"  target="_blank">Gizmodo</a> 
                        
                            <a href="http://www.lifehacker.co.in/"  target="_blank">Lifehacker</a> 
                        
                            <a href="http://in.ign.com/"  target="_blank">IGN</a> 
                        
                            <a href="http://boxtv.com"  target="_blank">BoxTV</a> 
                        
                            <a href="http://www.gaana.com"  target="_blank">Gaana</a> 
                        
                            <a href="http://shopping.indiatimes.com"  target="_blank">Online Shopping</a> 
                        
                            <a href="http://www.in.techradar.com/"  target="_blank">Techradar</a> 
                        
                            <a href="http://timesdeal.com"  target="_blank">Daily Deals</a> 
                        
                            <a href="http://www.gitanjalishop.com/"  target="_blank">Gitanjali Shop</a> 
                        
                            <a href="http://www.satvikshop.com/"  target="_blank">Satvik Shop</a> 
                        
                            <a href="http://whatshot.in"  target="_blank">WHAT'S HOT</a> 
                        
                            <a href="http://zigwheels.com"  target="_blank">ZigWheels</a> 
                        
                            <a href="http://filmipop.com"  target="_blank">FILMIPOP</a> 
                        
                            <a href="http://www.filmfare.com/"  target="_blank">Filmfare</a> 
                        
                            <a href="http://www.femina.in"  target="_blank">Femina</a> 
                        
                            <a href="http://mobile.indiatimes.com"  target="_blank">Mobile</a> 
                        
                            <a href="http://www.greetzap.com"  target="_blank">Greetings</a> 
                        
                            <a href="http://www.follo.co.in"  target="_blank">Follo</a> 
                        
                            <a href="http://timesjobs.com"  target="_blank">Jobs</a> 
                        
                            <a href="http://magicbricks.com"  target="_blank">Property</a> 
                        
                            <a href="http://www.astrospeak.com"  target="_blank">Astrology</a> 
                        
                            <a href="http://speakingtree.in"  target="_blank">Speaking Tree</a> 
                        
                            <a href="http://simplymarry.com"  target="_blank">Matrimonial</a> 
                        
                            <a href="http://itimes.com"  target="_blank">iTimes</a> 
                        
                            <a href="http://www.ads2book.com/"  target="_blank">Ads2book</a> 
                        
                            <a href="http://www.tcnext.com/"  target="_blank">TcNext</a> 
                                            </div>
                </div><!-- container closed-->
                <div class="cont border_none"><!-- container start-->
                    <h2>About us</h2>
                    <div class="sub_link">
                                                    <a href='http://www.indiatimes.com/privacypolicy' >Privacy Policy</a> 
                                                    <a href='http://www.indiatimes.com/sitemap.html' >sitemap</a> 
                                                    <a href='http://www.indiatimes.com/termsandcondition' >Terms & CONDITIONS</a> 
                                                    <a href='http://www.indiatimes.com/seoarchive' >ARCHIVES</a> 
                                                    <a href='http://www.indiatimes.com/contactus' >Contact us</a> 
                                                    <a href='http://www.indiatimes.com/aboutus' >ABOUT INDIATIMES</a> 
                                            </div>
                </div><!-- container closed-->
            </div><!-- footer grey closed-->
        </div><!-- footer black closed-->

        <div class="black_footer"><!-- footer black start-->
            <div class="containers">
                <p><a href="javascript:void(0)" class="copyrt">Copyright Â© 2015 Times Internet Limited. Powered by Indiatimes Lifestyle Network. All rights reserved</a></p>
                <span><a href="http://www.indiatimes.com/seoarchive/">Archive</a> | <a href="http://www.indiatimes.com/sitemap.html">Sitemap</a> | <a href="http://www.indiatimes.com/aboutus/">About Us</a> | <a href="http://www.indiatimes.com/contactus/">Contact Us</a> | <a href="http://www.indiatimes.com/privacypolicy/">Privacy Policy</a> | <a target="_blank" href="http://m.indiatimes.com">Indiatimes Mobile</a> | <a href="http://www.indiatimes.com/termsandcondition/">Terms of Use and Grievance Redressal Policy </a> </span> 
            </div>
        </div><!-- footer black end-->
    </footer>
</div>

    <script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.lazyload.min.js?v=100.9" charset="utf-8"></script>
<script defer type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.easing.1.3.min.js?v=100.9"></script>

<input type="hidden" name="currentId" id="currentId" value="">
    <div class="animate_cont dawn floating delay5">
        <span class="animate_arrow sprite_image"></span>
    </div>
<script defer src="http://media.indiatimes.in/resources/js/jquery.flexslider-min.js?v=1421222060"></script>



<script type="text/javascript" src="http://media.indiatimes.in/resources/js/main.js?v=100.9"></script>    
<script type="text/javascript" src="http://media.indiatimes.in/resources/js/common.js?v=100.9"></script>
<script  defer type="text/javascript" src="http://media.indiatimes.in/resources/js/jquery-scrolltofixed.js?v=100.9"></script>

<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://cdn.mouseflow.com/projects/1e238441-c30e-4d1b-8edd-c0f14d8fa6b7.js' type='text/javascript'%3E%3C/script%3E"));</script>
<!-- mouse flow code ends-->
<script  src="http://tags.crwdcntrl.net/c/2818/cc.js?ns=_cc2818" id="LOTCC_2818"></script> 

<script> 
    _cc2818.bcp(); 
</script>

	 <p id="back-top" style="display:none;"><a href="#top"><span></span></a> </p>
</div><!--wrap end-->


</body>
</html>