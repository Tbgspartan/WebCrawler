



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>

                    
                    
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "996-2042987-2156585";
                var ue_id = "11X8ADAAJJ3TKYEXXQ43";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        
    
        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />
    
    <meta property='fb:app_id' content='115109575169727' />
    
    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="11X8ADAAJJ3TKYEXXQ43" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1d-c3-2xl-i-17e737ea.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-3697064203._CB315760060_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-485555676._CB318510215_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-247421025._CB318510254_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-3978168775._CB318510288_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-1541130848._CB318510255_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
(function(){var d=function(o){return Object.prototype.toString.call(o)==="[object Array]";},g=function(q,p){var o;for(o=0;o<q.length;o++){if(o in q){p.call(null,q[o],o);}}},h=[],k,b,l=false,n=false,f=function(){var o=[],p=[],q={};g(h,function(s){var r="";g(s.dartsite.split("/"),function(t){if(t!==""){if(t in q){}else{q[t]=o.length;o.push(t);}r+="/"+q[t];}});p.push(r);});return{iu_parts:o,enc_prev_ius:p};},c=function(){var o=[];g(h,function(q){var p=[];g(q.sizes,function(r){p.push(r.join("x"));});o.push(p.join("|"));});return o;},m=function(){var o=[];g(h,function(p){o.push(a(p.targeting));});return o.join("|");},a=function(r,o){var q,p,s=[];for(q in r){p=[];for(j=0;j<r[q].length;j++){p.push(encodeURIComponent(r[q][j]));}if(o){s.push(q+"="+encodeURIComponent(p.join(",")));}else{s.push(q+"="+p.join(","));}}return s.join("&");},e=function(){var o=+new Date();if(n){return;}if(!this.readyState||"loaded"===this.readyState){n=true;if(l){imdbads.cmd.push(function(){for(i=0;i<h.length;i++){generic.monitoring.record_metric(h[i].name+".fail",csm.duration(o));}});}}};window.tinygpt={define_slot:function(r,q,o,p){h.push({dartsite:r.replace(/\/$/,""),sizes:q,name:o,targeting:p});},set_targeting:function(o){k=o;},callback:function(q){var r,p={},t,o,s=+new Date();l=false;for(r=0;r<h.length;r++){t=h[r].dartsite;o=h[r].name;if(q[r][t]){p[o]=q[r][t];}else{window.console&&console.error&&console.error("Unable to correlate GPT response for "+o);}}imdbads.cmd.push(function(){for(r=0;r<h.length;r++){ad_utils.slot_events.trigger(h[r].name,"request",{timestamp:b});ad_utils.slot_events.trigger(h[r].name,"tagdeliver",{timestamp:s});}ad_utils.gpt.handle_response(p);});},send:function(){var r=[],q=function(s,t){if(d(t)){t=t.join(",");}if(t){r.push(s+"="+encodeURIComponent(""+t));}},o,p;if(h.length===0){tinygpt.callback({});return;}q("gdfp_req","1");q("correlator",Math.floor(4503599627370496*Math.random()));q("output","json_html");q("callback","tinygpt.callback");q("impl","fifs");q("json_a","1");result=f();q("iu_parts",result.iu_parts);q("enc_prev_ius",result.enc_prev_ius);q("prev_iu_szs",c());q("prev_scp",m());q("cust_params",a(k,true));o=document.createElement("script");p=document.getElementsByTagName("script")[0];o.async=true;o.type="text/javascript";o.src="http://pubads.g.doubleclick.net/gampad/ads?"+r.join("&");o.id="tinygpt";o.onload=o.onerror=o.onreadystatechange=e;l=true;p.parentNode.insertBefore(o,p);b=+new Date();}};})();</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['b'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['1'],
'u': ['377258321678'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-3196534337._CB302695131_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"2ff7bcaec10b9e428f9e4f6c09d84cdf2bbbb3c3",
"2015-07-11T23%3A41%3A32GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 26308;
generic.days_to_midnight = 0.3044907450675964;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-1582251138._CB317085808_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'b']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm === undefined || window.generic === undefined || window.consoleLog === undefined)) {
                    if (console !== undefined && console.log !== undefined) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-1949688977._CB306861155_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-3293651389._CB317087773_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=377258321678;ord=377258321678?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=377258321678?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=377258321678?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
onsubmit="(new Image()).src='/rg/SEARCH-BOX/HEADER/images/b.gif?link=/find';"
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/imdbpicks/monthly-tv-picks/?ref_=nv_tp_tvpicks_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/search/title?count=100&title_type=feature,tv_series,tv_movie&explore=title_type,genres,year,countries&ref_=nv_ch_mm_1"
>Popular Movies & TV</a></li>
                        <li><a href="/chart/?ref_=nv_ch_cht_2"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_3"
>Oscar Winners</a></li>
                        <li><a href="/chart/top?ref_=nv_ch_250_4"
>Top 250</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_5"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/imdbpicks/monthly-tv-picks/?ref_=nv_tvv_picks_1"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_2"
>On Tonight</a></li>
                        <li><a href="/list/ls074093523/?ref_=nv_tvv_wn_3"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_4"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/academymuseum/?ref_=nv_sf_am_1"
>Academy Museum</a></li>
                        <li><a href="/imdbpicks/?ref_=nv_sf_pks_2"
>IMDb Picks</a></li>
                        <li><a href="/best-of/?ref_=nv_sf_bo_3"
>Best of 2014</a></li>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_4"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/whattowatch/?ref_=nv_sf_wtw_5"
>What to Watch</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=07-11&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_2"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_3"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_4"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_5"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_6"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_7"
>Road to the Emmys</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_8"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_9"
>Toronto Film Festival</a></li>
                        <li><a href="/awards-central/?ref_=nv_ev_all_10"
>More Popular Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni58785464/?ref_=nv_nw_tn_1"
> Saturday Report: 'Minions' Cool Millions
</a><br />
                        <span class="time">10 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni58786374/?ref_=nv_nw_tn_2"
> What if we lost World War II? âThe Man in the High Castleâ Panelâs Big Questions
</a><br />
                        <span class="time">41 minutes ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni58786363/?ref_=nv_nw_tn_3"
> Donald Trump Draws Protesters In L.A., Meets Hollywood Conservatives And Victimsâ Families
</a><br />
                        <span class="time">54 minutes ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/games/guess?ref_=nv_cm_qz_3"
>Quiz Game</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_4"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5> 
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0083658/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTYyNDg5MjE2M15BMl5BanBnXkFtZTYwMTE3Nzc4._V1._SY315_CR20,0,410,315_.jpg",
            titleYears : "1982",
            rank : 135,
                    headline : "Blade Runner"
    },
    nameAd : {
            clickThru : "/name/nm0006969/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTUzNzkzOTUyOV5BMl5BanBnXkFtZTcwNDE0MzEzNw@@._V1._SX250_CR0,0,250,315_.jpg",
            rank : 90,
            headline : "Elizabeth Banks"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYibOXbeNB6wtLe3tIRCZkN7onOohBsT_DSjhRgJbVsOiramyD8IMyWWwmxA1l6HMUFMKqZmBbM%0D%0AoFNdbLkwrgcdz2jCr6i3EoMXFSYJrwN0q65zSDN13Ey7EI92F4LJvZfxTToo_cxSEvcLUjDBTqRo%0D%0AUc3B9oQM1t6pT9MXEKXwVIkm1dregjOJ0DS9wEpcDsoQvEJp28Hu9zLtAUy5gKoTIg%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYpctarkwSH27-MheIofWjeIg9-yWjoPAA8fYJcuWQDDyCSA3ejQ-WOvGjfNzCHt6ELJz0ixNNe%0D%0AOPAp2aaiubozeyIFRZfnrQ6sZwLcsAVhMy1NZ676jV_-xpb65JSK3QS0854X470cnalIiyk1BrpY%0D%0AoWAvLwhjDY-BEvPpBt75MCddAricKKquJxFuNUdcVvtk%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYkax4OvIWo3jBLEP_nofuo3pRwGFJBC_OJ3IVt4efVDuhUeVg0_hZrhjGqV0EQ3KO3b1g9u1r0%0D%0Avm31tS7_DOAUHusIucKCgSYkkLApyy0SqUAjUz0QcyHUSyYM0IgZaxdObULm96gcv-LQjcvYopIx%0D%0A7gyhxFUKodgGzIAOrP6rY-d_EpJDaO35sgjqUlBl9U6MoPh1pU-48n70VP-HKE7ylQ%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=377258321678;ord=377258321678?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>
                    <div id="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=377258321678;ord=377258321678?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	

                    
                    

<div id="content-2-wide">
    <div id="main">
                    
        <div class="heroWidget">
        <span class="ab_widget"
        >
            <div class="ab_hero">
<div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3451171609?ref_=hm_hp_i_1" data-video="vi3451171609" data-source="bylist" data-id="ls074765188" data-rid="11X8ADAAJJ3TKYEXXQ43" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="Watch the new reel for Star Wars: Episode VII - The Force Awakens that was released at Comic-Con 2015." alt="Watch the new reel for Star Wars: Episode VII - The Force Awakens that was released at Comic-Con 2015." src="http://ia.media-imdb.com/images/M/MV5BMTUwMjU0MzQwNV5BMl5BanBnXkFtZTgwNzQwODUzNTE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUwMjU0MzQwNV5BMl5BanBnXkFtZTgwNzQwODUzNTE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Watch the new reel for Star Wars: Episode VII - The Force Awakens that was released at Comic-Con 2015." title="Watch the new reel for Star Wars: Episode VII - The Force Awakens that was released at Comic-Con 2015." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Watch the new reel for Star Wars: Episode VII - The Force Awakens that was released at Comic-Con 2015." title="Watch the new reel for Star Wars: Episode VII - The Force Awakens that was released at Comic-Con 2015." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2488496/?ref_=hm_hp_cap_pri_1" > Star Wars: Episode VII - The Force Awakens </a> </div> </div> <div class="secondary ellipsis"> Comic-Con Reel </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2578821913?ref_=hm_hp_i_2" data-video="vi2578821913" data-source="bylist" data-id="ls074765188" data-rid="11X8ADAAJJ3TKYEXXQ43" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="In the early 1960s, CIA agent Napoleon Solo and KGB operative Illya Kuryakin participate in a joint mission against a mysterious criminal organization, which is working to proliferate nuclear weapons." alt="In the early 1960s, CIA agent Napoleon Solo and KGB operative Illya Kuryakin participate in a joint mission against a mysterious criminal organization, which is working to proliferate nuclear weapons." src="http://ia.media-imdb.com/images/M/MV5BMTc4NDQ1MTI3OF5BMl5BanBnXkFtZTgwNjk5MzUxNjE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc4NDQ1MTI3OF5BMl5BanBnXkFtZTgwNjk5MzUxNjE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="In the early 1960s, CIA agent Napoleon Solo and KGB operative Illya Kuryakin participate in a joint mission against a mysterious criminal organization, which is working to proliferate nuclear weapons." title="In the early 1960s, CIA agent Napoleon Solo and KGB operative Illya Kuryakin participate in a joint mission against a mysterious criminal organization, which is working to proliferate nuclear weapons." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="In the early 1960s, CIA agent Napoleon Solo and KGB operative Illya Kuryakin participate in a joint mission against a mysterious criminal organization, which is working to proliferate nuclear weapons." title="In the early 1960s, CIA agent Napoleon Solo and KGB operative Illya Kuryakin participate in a joint mission against a mysterious criminal organization, which is working to proliferate nuclear weapons." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt1638355/?ref_=hm_hp_cap_pri_2" > The Man from U.N.C.L.E. </a> </div> </div> <div class="secondary ellipsis"> Comic:Con Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2343940889?ref_=hm_hp_i_3" data-video="vi2343940889" data-source="bylist" data-id="ls074765188" data-rid="11X8ADAAJJ3TKYEXXQ43" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="Fearing the actions of a god-like Super Hero left unchecked, Gotham City's own formidable, forceful vigilante takes on Metropolis' most revered, modern-day savior, while the world wrestles with what sort of hero it really needs. And with Batman and Superman at war with one another, a new threat quickly arises, putting mankind in greater danger than it's ever known before." alt="Fearing the actions of a god-like Super Hero left unchecked, Gotham City's own formidable, forceful vigilante takes on Metropolis' most revered, modern-day savior, while the world wrestles with what sort of hero it really needs. And with Batman and Superman at war with one another, a new threat quickly arises, putting mankind in greater danger than it's ever known before." src="http://ia.media-imdb.com/images/M/MV5BMTUyMTIxNzE1M15BMl5BanBnXkFtZTgwOTU0ODQ4MTE@._V1_SY298_CR1,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUyMTIxNzE1M15BMl5BanBnXkFtZTgwOTU0ODQ4MTE@._V1_SY298_CR1,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Fearing the actions of a god-like Super Hero left unchecked, Gotham City's own formidable, forceful vigilante takes on Metropolis' most revered, modern-day savior, while the world wrestles with what sort of hero it really needs. And with Batman and Superman at war with one another, a new threat quickly arises, putting mankind in greater danger than it's ever known before." title="Fearing the actions of a god-like Super Hero left unchecked, Gotham City's own formidable, forceful vigilante takes on Metropolis' most revered, modern-day savior, while the world wrestles with what sort of hero it really needs. And with Batman and Superman at war with one another, a new threat quickly arises, putting mankind in greater danger than it's ever known before." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Fearing the actions of a god-like Super Hero left unchecked, Gotham City's own formidable, forceful vigilante takes on Metropolis' most revered, modern-day savior, while the world wrestles with what sort of hero it really needs. And with Batman and Superman at war with one another, a new threat quickly arises, putting mankind in greater danger than it's ever known before." title="Fearing the actions of a god-like Super Hero left unchecked, Gotham City's own formidable, forceful vigilante takes on Metropolis' most revered, modern-day savior, while the world wrestles with what sort of hero it really needs. And with Batman and Superman at war with one another, a new threat quickly arises, putting mankind in greater danger than it's ever known before." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2975590/?ref_=hm_hp_cap_pri_3" > Batman v Superman: Dawn of Justice </a> </div> </div> <div class="secondary ellipsis"> Comic-Con Trailer </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/trailers?ref_=hm_hp_sm" class="position_bottom supplemental" > Browse more trailers </a> </p>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="http://www.imdb.com/comic-con/?ref_=hm_ccn_hm_sa_hd" > <h3>Comic-Con 2015</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/comic-con/blog?ref_=hm_ccn_hm_sa_i_1#COMICCON-15-DOCTORWHOPANEL" > <img itemprop="image" class="pri_image" title="Doctor Who (2005-)" alt="Doctor Who (2005-)" src="http://ia.media-imdb.com/images/M/MV5BMjE2NjQxMTQ4NF5BMl5BanBnXkFtZTgwOTI0OTEyNjE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE2NjQxMTQ4NF5BMl5BanBnXkFtZTgwOTI0OTEyNjE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="http://www.imdb.com/comic-con/blog?ref_=hm_ccn_hm_sa_cap_pri_1#COMICCON-15-DOCTORWHOPANEL" > "Doctor Who" Teases Season 9 </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/comic-con/photos/comic-con-2015-friday-rm1283975936?ref_=hm_ccn_hm_sa_i_2" > <img itemprop="image" class="pri_image" title="Star Wars: EpisÃ³dio VII - O Despertar da ForÃ§a (2015)" alt="Star Wars: EpisÃ³dio VII - O Despertar da ForÃ§a (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTkxMjQ5Nzk4OF5BMl5BanBnXkFtZTgwOTcwMTMyNjE@._V1_SY201_CR53,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTkxMjQ5Nzk4OF5BMl5BanBnXkFtZTgwOTcwMTMyNjE@._V1_SY201_CR53,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="http://www.imdb.com/comic-con/photos/comic-con-2015-friday-rm1283975936?ref_=hm_ccn_hm_sa_cap_pri_2" > Friday Photos </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/comic-con/blog?ref_=hm_ccn_hm_sa_i_3#COMICCON-15-STARWARSPANEL" > <img itemprop="image" class="pri_image" title="Star Wars: Episode VII - The Force Awakens (2015)" alt="Star Wars: Episode VII - The Force Awakens (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTQ0OTEyOTU4OF5BMl5BanBnXkFtZTgwNjcwMTMyNjE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ0OTEyOTU4OF5BMl5BanBnXkFtZTgwNjcwMTMyNjE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="http://www.imdb.com/comic-con/blog?ref_=hm_ccn_hm_sa_cap_pri_3#COMICCON-15-STARWARSPANEL" > Quick Hits - Star Wars: The Force Awakens Panel </a> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Check out our section for our handy <a href="http://www.imdb.com/comic-con/schedule/thursday?ref_=hm_ccn_hm_sa_lk1">schedule</a>, read about <a href="http://www.imdb.com/comic-con/comic-con-agenda/?ref_=hm_ccn_hm_sa_lk2">the 13 Things You Don't Want to Miss at Comic-Con</a>, and <a href="http://www.imdb.com/comic-con/trailers?ref_=hm_ccn_hm_sa_lk3">watch trailers</a> from select movies and TV shows presenting this year. We'll be updating daily from the Con with video interviews, photos, panel roundups, and more!</p> <p class="seemore"> <a href="http://www.imdb.com/comic-con/?ref_=hm_ccn_hm_sa_sm" class="position_bottom supplemental" > Visit our Comic-Con section </a> </p>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <h3>Comic-Con 2015: IMDb Video Interviews</h3> </span> </span> <p class="blurb">IMDb's Chako Suzuki is on the street at Comic-Con 2015 in San Diego chatting with cosplayers (like Jedi Elvis!) and interviewing the stars. Watch more videos in our <a href="http://www.imdb.com/comic-con/video?ref_=hm_ccn_video_lk1">Comic-Con video gallery</a>.</p> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2142614297?ref_=hm_ccn_video_i_1" data-video="vi2142614297" data-source="bylist" data-id="ls074183483" data-rid="11X8ADAAJJ3TKYEXXQ43" data-type="playlist" class="video-colorbox" data-refsuffix="hm_ccn_video" data-ref="hm_ccn_video_i_1"> <img itemprop="image" class="pri_image" title="IMDb roams the halls of Comic-Con 2015 and asks the burning question: What are you dressed as?" alt="IMDb roams the halls of Comic-Con 2015 and asks the burning question: What are you dressed as?" src="http://ia.media-imdb.com/images/M/MV5BMjI2Nzc1NzUwN15BMl5BanBnXkFtZTgwMzc1NDMyNjE@._V1._CR0,50,1200,700_SY230_SX307_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI2Nzc1NzUwN15BMl5BanBnXkFtZTgwMzc1NDMyNjE@._V1._CR0,50,1200,700_SY230_SX307_AL_UY460_UX614_AL_.jpg" /> <img alt="IMDb roams the halls of Comic-Con 2015 and asks the burning question: What are you dressed as?" title="IMDb roams the halls of Comic-Con 2015 and asks the burning question: What are you dressed as?" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="IMDb roams the halls of Comic-Con 2015 and asks the burning question: What are you dressed as?" title="IMDb roams the halls of Comic-Con 2015 and asks the burning question: What are you dressed as?" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/comic-con/video/?ref_=hm_ccn_video_cap_pri_1" > IMDb on the Street: Cosplay </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2075505433?ref_=hm_ccn_video_i_2" data-video="vi2075505433" data-source="bylist" data-id="ls074183483" data-rid="11X8ADAAJJ3TKYEXXQ43" data-type="playlist" class="video-colorbox" data-refsuffix="hm_ccn_video" data-ref="hm_ccn_video_i_2"> <img itemprop="image" class="pri_image" title="Zoo (2015-)" alt="Zoo (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMjE4NjE5Njg5OF5BMl5BanBnXkFtZTgwODY1NDMyNjE@._V1._SY700_CR0,0,1200,700_SY230_SX307_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE4NjE5Njg5OF5BMl5BanBnXkFtZTgwODY1NDMyNjE@._V1._SY700_CR0,0,1200,700_SY230_SX307_AL_UY460_UX614_AL_.jpg" /> <img alt="Zoo (2015-)" title="Zoo (2015-)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Zoo (2015-)" title="Zoo (2015-)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/comic-con/video/?ref_=hm_ccn_video_cap_pri_2" > IMDb Interview: The cast of "Zoo" </a> </div> </div> </div> </div> </div> </div> </div>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58785464?ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTg2MTMyMzU0M15BMl5BanBnXkFtZTgwOTU3ODk4NTE@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58785464?ref_=hm_nw_tp1"
class="headlines" >Saturday Report: 'Minions' Cool Millions</a>
    <div class="infobar">
            <span class="text-muted">10 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000159?ref_=hm_nw_tp1_src"
>Box Office Mojo</a></span>
    </div>
                                </div>
<p>Universal is estimating that <a href="/title/tt2293640?ref_=hm_nw_tp1_lk1">Minions</a>, the 3-D prequel to their <a href="/title/tt1323594?ref_=hm_nw_tp1_lk2">Despicable Me</a> franchise is heading to an astonishing $120.9M opening weekend. The CG animated flick from Illumination Entertainment marched into 4,301 venues Friday, the most North American venues in Universal's history, and left with ...                                        <span class="nobr"><a href="/news/ni58785464?ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58786374?ref_=hm_nw_tp2"
class="headlines" >What if we lost World War II? âThe Man in the High Castleâ Panelâs Big Questions</a>
    <div class="infobar">
            <span class="text-muted">41 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000940?ref_=hm_nw_tp2_src"
>IMDb Blog - All the Latest</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58786363?ref_=hm_nw_tp3"
class="headlines" >Donald Trump Draws Protesters In L.A., Meets Hollywood Conservatives And Victimsâ Families</a>
    <div class="infobar">
            <span class="text-muted">54 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?ref_=hm_nw_tp3_src"
>The Wrap</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58786362?ref_=hm_nw_tp4"
class="headlines" >'Hateful Eight': Quentin Tarantino says Ennio Morricone is scoring his Western</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000173?ref_=hm_nw_tp4_src"
>Hitfix</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58786358?ref_=hm_nw_tp5"
class="headlines" >Orphan Black's Tatiana Maslany Has One Flaw</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?ref_=hm_nw_tp5_src"
>Vulture</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58785464?ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTg2MTMyMzU0M15BMl5BanBnXkFtZTgwOTU3ODk4NTE@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58785464?ref_=hm_nw_mv1"
class="headlines" >Saturday Report: 'Minions' Cool Millions</a>
    <div class="infobar">
            <span class="text-muted">10 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000159?ref_=hm_nw_mv1_src"
>Box Office Mojo</a></span>
    </div>
                                </div>
<p>Universal is estimating that <a href="/title/tt2293640?ref_=hm_nw_mv1_lk1">Minions</a>, the 3-D prequel to their <a href="/title/tt1323594?ref_=hm_nw_mv1_lk2">Despicable Me</a> franchise is heading to an astonishing $120.9M opening weekend. The CG animated flick from Illumination Entertainment marched into 4,301 venues Friday, the most North American venues in Universal's history, and left with ...                                        <span class="nobr"><a href="/news/ni58785464?ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58786492?ref_=hm_nw_mv2"
class="headlines" >Stone rewatched: the Australian bikie movie that inspired Mad Max</a>
    <div class="infobar">
            <span class="text-muted">37 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000680?ref_=hm_nw_mv2_src"
>The Guardian - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58786449?ref_=hm_nw_mv3"
class="headlines" >âCrimson Peakâ Maze Coming to Universal Halloween Horror Nights [Comic-Con 2015]</a>
    <div class="infobar">
            <span class="text-muted">41 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000151?ref_=hm_nw_mv3_src"
>Slash Film</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58786357?ref_=hm_nw_mv4"
class="headlines" >Quentin Tarantino Delivers Mind-Blowing Look At âHateful Eightâ â Comic Con</a>
    <div class="infobar">
            <span class="text-muted">59 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?ref_=hm_nw_mv4_src"
>Deadline</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58786350?ref_=hm_nw_mv5"
class="headlines" >Quentin Tarantino says The Hateful Eight will have Ennio Morricone score</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000680?ref_=hm_nw_mv5_src"
>The Guardian - Film News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58784919?ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjM5NzUxOTI4MV5BMl5BanBnXkFtZTgwMjkwNzAxNjE@._V1_SY150_CR4,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58784919?ref_=hm_nw_tv1"
class="headlines" >âScreamâs First Victim Bella Thorne To Reappear In Flashbacks â Comic Con</a>
    <div class="infobar">
            <span class="text-muted">18 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004913?ref_=hm_nw_tv1_src"
>Deadline TV</a></span>
    </div>
                                </div>
<p><a href="/name/nm2254074?ref_=hm_nw_tv1_lk1">Bella Thorne</a> will return to this season of MTV's cult classic remake <a href="/title/tt3921180?ref_=hm_nw_tv1_lk2">Scream</a>,Â producers announced during the showâs panel on Day 2 of Comic-Con. Thorneâs character Nina, theÂ seriesâ first causality, was brutally killed in the pilot episode. The producers and Thorne were mum on the timing but said ...                                        <span class="nobr"><a href="/news/ni58784919?ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58785490?ref_=hm_nw_tv2"
class="headlines" >The Ash vs Evil Dead Trailer is Groovy, Baby</a>
    <div class="infobar">
            <span class="text-muted">9 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?ref_=hm_nw_tv2_src"
>Vulture</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58784514?ref_=hm_nw_tv3"
class="headlines" >Comic-Con Day 2: âGame of Thrones,â âBig Bang Theory,â âStar Warsâ & More</a>
    <div class="infobar">
            <span class="text-muted">19 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?ref_=hm_nw_tv3_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58785507?ref_=hm_nw_tv4"
class="headlines" >Paul Rudd Voiced Tina's Horse, and Other Thrilling Bob's Burgers Casting News from Comic-Con</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?ref_=hm_nw_tv4_src"
>Vulture</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58784500?ref_=hm_nw_tv5"
class="headlines" >Dan Harmon Dishes Community Movie Prospects, Confirms Big-Screen Title</a>
    <div class="infobar">
            <span class="text-muted">20 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?ref_=hm_nw_tv5_src"
>TVLine.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58786349?ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BODQyNTQyNzY4MV5BMl5BanBnXkFtZTcwODg5MDA3MQ@@._V1_SY150_CR12,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58786349?ref_=hm_nw_cel1"
class="headlines" >Mila Kunis On Ashton's Parenting</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000936?ref_=hm_nw_cel1_src"
>Access Hollywood</a></span>
    </div>
                                </div>
<p>It seems like <a href="/name/nm0005109?ref_=hm_nw_cel1_lk1">Mila Kunis</a> hit the husband jackpot when it comes <a href="/name/nm0005110?ref_=hm_nw_cel1_lk2">Ashton Kutcher</a>.Not only is the actor a very involved father to their 9-month-old daughter, Wyatt, he insists on taking care of diaper duty."My husband is an incredibly hands-on dad," Mila told the UK's Telegraph in a new interview. "...                                        <span class="nobr"><a href="/news/ni58786349?ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58786352?ref_=hm_nw_cel2"
class="headlines" >Mila Kunis & Ashton Kutcher Keep a Gun at Their HouseâFind Out Why and What She Said About Baby Wyatt</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?ref_=hm_nw_cel2_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58775527?ref_=hm_nw_cel3"
class="headlines" >You Have to See Ben Affleck's Lower Back Tattoo</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000088?ref_=hm_nw_cel3_src"
>Popsugar.com</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58786335?ref_=hm_nw_cel4"
class="headlines" >Kylie Jenner & Kendall Jenner Rock Near-Identical Looks, Tyga Steps Out Amid ScandalâSee Pics</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?ref_=hm_nw_cel4_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58786336?ref_=hm_nw_cel5"
class="headlines" >Carrie Underwood Breaks Into Her Car After Singer's 4-Month-Old Son and Dogs Are Accidentally Locked Inside</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?ref_=hm_nw_cel5_src"
>E! Online</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <h3>IMDb Snapshot - Comic-Con 2015</h3> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/comic-con/photos/comic-con-2015-friday-rm4119325440?ref_=hm_snp_i_1" > <img itemprop="image" class="pri_image" title="Game of Thrones (2011-)" alt="Game of Thrones (2011-)" src="http://ia.media-imdb.com/images/M/MV5BMTQ0Njk3NTE1Ml5BMl5BanBnXkFtZTgwNTAyMTMyNjE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ0Njk3NTE1Ml5BMl5BanBnXkFtZTgwNTAyMTMyNjE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="http://www.imdb.com/comic-con/photos/comic-con-2015-friday-rm4119325440?ref_=hm_snp_cap_pri_1" > Friday Photos </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/comic-con/photos/comic-con-2015-photos-we-love-rm227011328?ref_=hm_snp_i_2" > <img itemprop="image" class="pri_image" title="The Walking Dead (2010-)" alt="The Walking Dead (2010-)" src="http://ia.media-imdb.com/images/M/MV5BMTQ1MDM3MTM4M15BMl5BanBnXkFtZTgwMjk5MDMyNjE@._V1_SY201_CR48,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ1MDM3MTM4M15BMl5BanBnXkFtZTgwMjk5MDMyNjE@._V1_SY201_CR48,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="http://www.imdb.com/comic-con/photos/comic-con-2015-photos-we-love-rm227011328?ref_=hm_snp_cap_pri_2" > Photos We Love </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/comic-con/photos/comic-con-2015-cute-crazy-creepy-rm313715456?ref_=hm_snp_i_3" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTg5MjU2MTg5OV5BMl5BanBnXkFtZTgwNzc0NjEyNjE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg5MjU2MTg5OV5BMl5BanBnXkFtZTgwNzc0NjEyNjE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="http://www.imdb.com/comic-con/photos/comic-con-2015-cute-crazy-creepy-rm313715456?ref_=hm_snp_cap_pri_3" > Cute, Crazy & Creepy Cosplay </a> </div> </div> </div> </div> </div> </div> </div>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <h3>Comic-Con 2015: Trailers & Clips</h3> </span> </span> <p class="blurb">There's so many new trailers and teasers coming out of Comic-Con 2015 , we've compiled a list so they're easy to find! Check out our <a href="http://www.imdb.com/comic-con/trailers/?ref_=hm_ccn_video_lk1">Comic-Con video gallery</a> to see the full list.</p> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1270199065?ref_=hm_ccn_video_i_1" data-video="vi1270199065" data-source="bylist" data-id="ls074765188" data-rid="11X8ADAAJJ3TKYEXXQ43" data-type="playlist" class="video-colorbox" data-refsuffix="hm_ccn_video" data-ref="hm_ccn_video_i_1"> <img itemprop="image" class="pri_image" title="Ash has spent the last 30 years avoiding responsibility, maturity and the terrors of the Evil Dead until a Deadite plague threatens to destroy all of mankind and Ash becomes mankind's only hope." alt="Ash has spent the last 30 years avoiding responsibility, maturity and the terrors of the Evil Dead until a Deadite plague threatens to destroy all of mankind and Ash becomes mankind's only hope." src="http://ia.media-imdb.com/images/M/MV5BMTg1MzIzNjY5MF5BMl5BanBnXkFtZTgwNTA1MzQxNTE@._V1_SX148_CR0,0,148,219_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg1MzIzNjY5MF5BMl5BanBnXkFtZTgwNTA1MzQxNTE@._V1_SX148_CR0,0,148,219_AL_UY438_UX296_AL_.jpg" /> <img alt="Ash has spent the last 30 years avoiding responsibility, maturity and the terrors of the Evil Dead until a Deadite plague threatens to destroy all of mankind and Ash becomes mankind's only hope." title="Ash has spent the last 30 years avoiding responsibility, maturity and the terrors of the Evil Dead until a Deadite plague threatens to destroy all of mankind and Ash becomes mankind's only hope." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Ash has spent the last 30 years avoiding responsibility, maturity and the terrors of the Evil Dead until a Deadite plague threatens to destroy all of mankind and Ash becomes mankind's only hope." title="Ash has spent the last 30 years avoiding responsibility, maturity and the terrors of the Evil Dead until a Deadite plague threatens to destroy all of mankind and Ash becomes mankind's only hope." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt4189022/?ref_=hm_ccn_video_cap_pri_1" > Ash vs Evil Dead </a> </div> </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1303687961?ref_=hm_ccn_video_i_2" data-video="vi1303687961" data-source="bylist" data-id="ls074765188" data-rid="11X8ADAAJJ3TKYEXXQ43" data-type="playlist" class="video-colorbox" data-refsuffix="hm_ccn_video" data-ref="hm_ccn_video_i_2"> <img itemprop="image" class="pri_image" title="Here is the official Season 4 trailer which premiered at Comic-Con 2015." alt="Here is the official Season 4 trailer which premiered at Comic-Con 2015." src="http://ia.media-imdb.com/images/M/MV5BMTcyMTUzMzMxNV5BMl5BanBnXkFtZTgwMDY3NTUzNDE@._V1_SX148_CR0,0,148,219_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTcyMTUzMzMxNV5BMl5BanBnXkFtZTgwMDY3NTUzNDE@._V1_SX148_CR0,0,148,219_AL_UY438_UX296_AL_.jpg" /> <img alt="Here is the official Season 4 trailer which premiered at Comic-Con 2015." title="Here is the official Season 4 trailer which premiered at Comic-Con 2015." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Here is the official Season 4 trailer which premiered at Comic-Con 2015." title="Here is the official Season 4 trailer which premiered at Comic-Con 2015." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2306299/?ref_=hm_ccn_video_cap_pri_2" > Vikings </a> </div> </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2964566809?ref_=hm_ccn_video_i_3" data-video="vi2964566809" data-source="bylist" data-id="ls074765188" data-rid="11X8ADAAJJ3TKYEXXQ43" data-type="playlist" class="video-colorbox" data-refsuffix="hm_ccn_video" data-ref="hm_ccn_video_i_3"> <img itemprop="image" class="pri_image" title="Here is the official Season 6 trailer which premiered at Comic-Con 2015." alt="Here is the official Season 6 trailer which premiered at Comic-Con 2015." src="http://ia.media-imdb.com/images/M/MV5BOTg1MjgwMDQ5Ml5BMl5BanBnXkFtZTgwOTA3MDM5MzE@._V1_SY219_CR0,0,148,219_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BOTg1MjgwMDQ5Ml5BMl5BanBnXkFtZTgwOTA3MDM5MzE@._V1_SY219_CR0,0,148,219_AL_UY438_UX296_AL_.jpg" /> <img alt="Here is the official Season 6 trailer which premiered at Comic-Con 2015." title="Here is the official Season 6 trailer which premiered at Comic-Con 2015." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Here is the official Season 6 trailer which premiered at Comic-Con 2015." title="Here is the official Season 6 trailer which premiered at Comic-Con 2015." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt1520211/?ref_=hm_ccn_video_cap_pri_3" > The Walking Dead </a> </div> </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1068741401?ref_=hm_ccn_video_i_4" data-video="vi1068741401" data-source="bylist" data-id="ls074765188" data-rid="11X8ADAAJJ3TKYEXXQ43" data-type="playlist" class="video-colorbox" data-refsuffix="hm_ccn_video" data-ref="hm_ccn_video_i_4"> <img itemprop="image" class="pri_image" title="What did the world look like as it was transforming into the horrifying apocalypse depicted in &quot;The Walking Dead&quot;? This spin-off set in Los Angeles, following new characters as they face the beginning of the end of the world, will answer that question." alt="What did the world look like as it was transforming into the horrifying apocalypse depicted in &quot;The Walking Dead&quot;? This spin-off set in Los Angeles, following new characters as they face the beginning of the end of the world, will answer that question." src="http://ia.media-imdb.com/images/M/MV5BMjMxMDA0NDM5NV5BMl5BanBnXkFtZTgwNDMwNTIxNjE@._V1_SY219_CR8,0,148,219_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjMxMDA0NDM5NV5BMl5BanBnXkFtZTgwNDMwNTIxNjE@._V1_SY219_CR8,0,148,219_AL_UY438_UX296_AL_.jpg" /> <img alt="What did the world look like as it was transforming into the horrifying apocalypse depicted in &quot;The Walking Dead&quot;? This spin-off set in Los Angeles, following new characters as they face the beginning of the end of the world, will answer that question." title="What did the world look like as it was transforming into the horrifying apocalypse depicted in &quot;The Walking Dead&quot;? This spin-off set in Los Angeles, following new characters as they face the beginning of the end of the world, will answer that question." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="What did the world look like as it was transforming into the horrifying apocalypse depicted in &quot;The Walking Dead&quot;? This spin-off set in Los Angeles, following new characters as they face the beginning of the end of the world, will answer that question." title="What did the world look like as it was transforming into the horrifying apocalypse depicted in &quot;The Walking Dead&quot;? This spin-off set in Los Angeles, following new characters as they face the beginning of the end of the world, will answer that question." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt3743822/?ref_=hm_ccn_video_cap_pri_4" > Fear the Walking Dead </a> </div> </div> </div> </div> </div> </div> </div> </div> </div>    </div>

        </span>
        </div>
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/title/tt3997238/?ref_=hm_if_gay_hd" > <h3>Indie Focus: <i>Do I Sound Gay?</i> - Exclusive Clip</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3377919232/tt3997238?ref_=hm_if_gay_i_1" > <img itemprop="image" class="pri_image" title="Do I Sound Gay? (2014)" alt="Do I Sound Gay? (2014)" src="http://ia.media-imdb.com/images/M/MV5BNDQ2MzAzNzYzMV5BMl5BanBnXkFtZTgwODc3OTU5NTE@._V1_SX170_CR0,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNDQ2MzAzNzYzMV5BMl5BanBnXkFtZTgwODc3OTU5NTE@._V1_SX170_CR0,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3148919577?ref_=hm_if_gay_i_2" data-video="vi3148919577" data-rid="11X8ADAAJJ3TKYEXXQ43" data-type="single" class="video-colorbox" data-refsuffix="hm_if_gay" data-ref="hm_if_gay_i_2"> <img itemprop="image" class="pri_image" title="Do I Sound Gay? (2014)" alt="Do I Sound Gay? (2014)" src="http://ia.media-imdb.com/images/M/MV5BMTQ1Mjk0NTI4N15BMl5BanBnXkFtZTgwMTY2MzIyNjE@._V1._SY250_SX444_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ1Mjk0NTI4N15BMl5BanBnXkFtZTgwMTY2MzIyNjE@._V1._SY250_SX444_AL_UY500_UX888_AL_.jpg" /> <img alt="Do I Sound Gay? (2014)" title="Do I Sound Gay? (2014)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Do I Sound Gay? (2014)" title="Do I Sound Gay? (2014)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">George Takei shares the story of what inspired him to finally speak openly about this sexual orientation in David Thorpe's documentary about the stereotype of the "gay voice".</p> <p class="seemore"> <a href="/title/tt3997238/?ref_=hm_if_gay_sm" class="position_bottom supplemental" > Learn more about <i>Do I Sound Gay?</i> </a> </p>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
                <div class="ab_borntoday">
<span class="ninja_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=7-11&ref_=hm_brn_hd" > <h3>Born Today</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm2100657?ref_=hm_brn_i_1" > <img itemprop="image" class="pri_image" title="Serinda Swan" alt="Serinda Swan" src="http://ia.media-imdb.com/images/M/MV5BMTg2MjQ1NzE2MF5BMl5BanBnXkFtZTcwNjcxMDg4OA@@._V1_SY172_CR12,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg2MjQ1NzE2MF5BMl5BanBnXkFtZTcwNjcxMDg4OA@@._V1_SY172_CR12,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm2100657?ref_=hm_brn_cap_pri_lk1_1">Serinda Swan</a> (31) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1592225?ref_=hm_brn_i_2" > <img itemprop="image" class="pri_image" title="Rachael Taylor" alt="Rachael Taylor" src="http://ia.media-imdb.com/images/M/MV5BMTkzOTk5Mjg4N15BMl5BanBnXkFtZTcwNjMxNTE4MQ@@._V1_SY172_CR6,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTkzOTk5Mjg4N15BMl5BanBnXkFtZTcwNjMxNTE4MQ@@._V1_SY172_CR6,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1592225?ref_=hm_brn_cap_pri_lk1_2">Rachael Taylor</a> (31) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0150362?ref_=hm_brn_i_3" > <img itemprop="image" class="pri_image" title="Justin Chambers" alt="Justin Chambers" src="http://ia.media-imdb.com/images/M/MV5BMTI5ODA1MDc1M15BMl5BanBnXkFtZTcwNjk5ODk5Mg@@._V1_SY172_CR3,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTI5ODA1MDc1M15BMl5BanBnXkFtZTcwNjk5ODk5Mg@@._V1_SY172_CR3,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0150362?ref_=hm_brn_cap_pri_lk1_3">Justin Chambers</a> (45) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0742146?ref_=hm_brn_i_4" > <img itemprop="image" class="pri_image" title="Michael Rosenbaum" alt="Michael Rosenbaum" src="http://ia.media-imdb.com/images/M/MV5BMTUyNTc5Nzk2N15BMl5BanBnXkFtZTgwNDIxNTA4NTE@._V1_SY172_CR28,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUyNTc5Nzk2N15BMl5BanBnXkFtZTgwNDIxNTA4NTE@._V1_SY172_CR28,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0742146?ref_=hm_brn_cap_pri_lk1_4">Michael Rosenbaum</a> (43) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0000688?ref_=hm_brn_i_5" > <img itemprop="image" class="pri_image" title="Sela Ward" alt="Sela Ward" src="http://ia.media-imdb.com/images/M/MV5BMTI4NjYzNDAwN15BMl5BanBnXkFtZTYwNzMwMjA2._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTI4NjYzNDAwN15BMl5BanBnXkFtZTYwNzMwMjA2._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0000688?ref_=hm_brn_cap_pri_lk1_5">Sela Ward</a> (59) </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/search/name?refine=birth_monthday&birth_monthday=7-11&ref_=hm_brn_sm" class="position_bottom supplemental" > See all birthdays </a> </p>        </div>

        </span>
        </div>
                    
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_trivia">
<span class="ninja_header"> <span class="oneline"> <a href="/title/tt3314218/trivia?item=tr2513694&ref_=hm_trv_hd" > <h3>Did You Know?</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt3314218?ref_=hm_trv_i_1" > <img itemprop="image" class="pri_image" title="UnREAL (2015-)" alt="UnREAL (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMjMzNTA3NTY0NF5BMl5BanBnXkFtZTgwODMyODE3NDE@._V1_SY132_CR54,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjMzNTA3NTY0NF5BMl5BanBnXkFtZTgwODMyODE3NDE@._V1_SY132_CR54,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" > <strong class="text-large"><a href="/title/tt3314218?ref_=hm_trv_lk1">UnREAL</a></strong> <p class="blurb">This show is based on the short film "Sequin Raze," which was written and directed by Sarah Gertrude Shapiro. She was a producer on "The Bachelor" from 2002-2004, and is now the co-creator, writer, and supervising producer for UnREAL</p></div> </div> </div> <p class="seemore"> <a href="/title/tt3314218/trivia?item=tr2513694&ref_=hm_trv_sm" class="position_bottom supplemental" > See more trivia </a> </p>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
    </div>
    <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-1949688977._CB306861155_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-3293651389._CB317087773_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=377258321678;ord=377258321678?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=377258321678?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=377258321678?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	
                    
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/movies-in-theaters/?ref_=hm_otw_hd" > <h3>Opening This Week</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2293640"></div> <div class="title"> <a href="/title/tt2293640?ref_=hm_otw_t0"> Minions </a> <span class="secondary-text"></span> </div> <div class="action"> <a href="/showtimes/title/tt2293640?ref_=hm_otw_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2309260"></div> <div class="title"> <a href="/title/tt2309260?ref_=hm_otw_t1"> The Gallows </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2140379"></div> <div class="title"> <a href="/title/tt2140379?ref_=hm_otw_t2"> Self/less </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2624412"></div> <div class="title"> <a href="/title/tt2624412?ref_=hm_otw_t3"> Boulevard </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3997238"></div> <div class="title"> <a href="/title/tt3997238?ref_=hm_otw_t4"> Do I Sound Gay? </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3824458"></div> <div class="title"> <a href="/title/tt3824458?ref_=hm_otw_t5"> Tangerine </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3626804"></div> <div class="title"> <a href="/title/tt3626804?ref_=hm_otw_t6"> Nowitzki: The Perfect Shot </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2151739"></div> <div class="title"> <a href="/title/tt2151739?ref_=hm_otw_t7"> Meet Me in Montenegro </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> </div> </div> </div> <div><a href="/movies-in-theaters/?ref_=hm_otw_sm" ><p class="seemore position_bottom">See more opening this week</p></a></div>
        </span>
        </div>
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
            <div class="ab_ninja showtimesMessagingWidget">
<p class="blurb"><a href="/showtimes?ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p>    </div>

        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/chart/?ref_=hm_cht_hd" > <h3>Now Playing (Box Office)</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2096673"></div> <div class="title"> <a href="/title/tt2096673?ref_=hm_cht_t0"> Inside Out </a> <span class="secondary-text">$29.8M</span> </div> <div class="action"> <a href="/showtimes/title/tt2096673?ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt0369610"></div> <div class="title"> <a href="/title/tt0369610?ref_=hm_cht_t1"> Jurassic World </a> <span class="secondary-text">$29.2M</span> </div> <div class="action"> <a href="/showtimes/title/tt0369610?ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1340138"></div> <div class="title"> <a href="/title/tt1340138?ref_=hm_cht_t2"> Terminator Genisys </a> <span class="secondary-text">$27.0M</span> </div> <div class="action"> <a href="/showtimes/title/tt1340138?ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2268016"></div> <div class="title"> <a href="/title/tt2268016?ref_=hm_cht_t3"> Magic Mike XXL </a> <span class="secondary-text">$12.9M</span> </div> <div class="action"> <a href="/showtimes/title/tt2268016?ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2637276"></div> <div class="title"> <a href="/title/tt2637276?ref_=hm_cht_t4"> Ted 2 </a> <span class="secondary-text">$11.2M</span> </div> </div> </div> </div> </div> <div><a href="/chart/?ref_=hm_cht_sm" ><p class="seemore position_bottom">See more box office results</p></a></div>
        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/movies-coming-soon/?ref_=hm_cs_hd" > <h3>Coming Soon</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt0478970"></div> <div class="title"> <a href="/title/tt0478970?ref_=hm_cs_t0"> Ant-Man </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3152624"></div> <div class="title"> <a href="/title/tt3152624?ref_=hm_cs_t1"> Trainwreck </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3715320"></div> <div class="title"> <a href="/title/tt3715320?ref_=hm_cs_t2"> Irrational Man </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3521134"></div> <div class="title"> <a href="/title/tt3521134?ref_=hm_cs_t3"> The Look of Silence </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3168230"></div> <div class="title"> <a href="/title/tt3168230?ref_=hm_cs_t4"> Mr. Holmes </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <div><a href="/movies-coming-soon/?ref_=hm_cs_sm" ><p class="seemore position_bottom">See more coming soon</p></a></div>
        </span>
        </div>
                    
                    
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
                <h3>Follow Us On Twitter</h3>
    <iframe class="twitter-frame" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3776329187._CB305760180_.html#{"screen-name":"IMDb","height":"500px","width":"300px"}'></iframe>

 

        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
            <div id="fb-root"></div>

    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    
    <h3>Find Us On Facebook</h3>
    <div class="fb-like-box" data-href="http://www.facebook.com/imdb" data-width="285" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
        <div class="aux-content-widget-2 sticky-widget">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/imdbpicks/?ref_=hm_pks_hd" > <h3>IMDb Picks: July</h3> </a> </span> </span> <p class="blurb">Visit our IMDb Picks section to see our recommendations of movies and TV shows coming out in July.</p> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/?ref_=hm_pks_i_1" > <img itemprop="image" class="pri_image" title="Ray Donovan (2013-)" alt="Ray Donovan (2013-)" src="http://ia.media-imdb.com/images/M/MV5BMTg1MzA1MTY5MF5BMl5BanBnXkFtZTgwMTk4NzYwNjE@._V1_SY525_CR44,0,700,525_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg1MzA1MTY5MF5BMl5BanBnXkFtZTgwMTk4NzYwNjE@._V1_SY525_CR44,0,700,525_AL_UY1050_UX1400_AL_.jpg" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/imdbpicks/?ref_=hm_pks_cap_pri_1" > Ray Donovan </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/imdbpicks/?ref_=hm_pks_sm" class="position_bottom supplemental" > Visit the IMDb Picks section </a> </p>    </div>

        </span>
        </div>
    </div>
</div>

                    
                    


                   <br class="clear" />
                </div>





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYjeITD9Xlao9ZH9R7-Y0XJQjfHNAxCJgZpgBqdCSI8HbluBkdT39kY3MKAmcuZHGmank_ipv57%0D%0Av3bAHctZxAmBDQeUl5J6yacHqt3-f6wIegN3DQXCiJKmaA4TX-PpEhrnzFaFxNx-zvLuYHZ-fhDU%0D%0AbiK3mgF0q_PcCiD_fCkfUXXBXZBW5TSWkOWM5KhEKTfLJdBvIt9UrYAqfRG21XQ66eVPp4PbiKC8%0D%0AHu_A8UlmBtc%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYqYY8dfrRNmcpFtCqtHUlM-PgFkCJhrdvi5O7bOd6x4i9RpZURfStlsc2x7zcDdwgLjgiqOoSt%0D%0ANXGPcU3b6e0U4Yrhc-2Jba4gLiHL8aHdT-Np8gJPmciI-lXfoqaWJj31lA8B7jjQ08srRQvueNp3%0D%0AOjOKcLbW6hbpJz7m2U4otNh1UsMeTLmCZ2eiiVEuA5H1rpvH9v2X9USy9PJf-XmxXQ%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYmk26iV8cm7MLMkBUlkPR1stPWahQjiJ8iUDSod-WUbWa2QIp0dleJYjULqOBM9McOcRyC3X-n%0D%0AKgfbDaDVFLIXFiSBreUPzWA8zxBZ5KN66Nm-LmLlZfXdG0xOKaYQPX11VoNK21wQOdTWWNo4nihN%0D%0A8nG6yUzH7YhEDGla1CMEPr2soShZOh6_M2Ako9XuGUk7Qi3RQH1g_u5vy8o9Hq9A24nB3cGlTepy%0D%0AJNsyJ5gsYZTLpEjUvyn_Ro3FYup2VOhn7tsaj8YIpU4XLTM7hPxmMZup8JDD4PvNQd2F-2dhkFnR%0D%0AwEdRJrlXeJRGXTzQiGRvt6prKDXnX6TI6DlY80Nr3Q2IyyFpUf4w7pRQdbNjiREh78RbLAa2OW1O%0D%0AXfVMTwO6GgktQiST82k1eFbBl9itAg%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYsexdD-s4A8-3qLM3o-TPngRjMDLlp3ZbKB94P1Lcung2LhNCyTCz_fwMGX13eLKR5g7zD-8Jz%0D%0A8Cxgv4PwMmnbdWqynTcpjTUXnAo0nw_aKt7L0Q8zHe4XJz2qR0YVqidR3cin51QMSwM5xApM1x6i%0D%0ACbl_wnhHDEm7moQoJhYbviJLSH_lDKy283EW6DsKquuq34_rdNYyxNRAkyT74VWgNg%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYkfhVicy8CxCr0yDhb8XBVgjPEeEV-oo2glqmntrMI5Pz09YL2sX2Trf3cman376cEPRnx9U7o%0D%0AjVxCNABkwgRQAS9PGrIxKHfFj9Hjb33Cufr0Wz6DrT5VJ59EX0Q05iPP-_nZiP9WCCZWInNrulCW%0D%0A3foX40vMMD9v7RhNeQitLlmmZDlhSvTPQD38BKwR65tJ%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYuw4nyDG-VH8H1qPJI7Tgk1fiyuWhdTg02UoRLSLB8iaRGmJ4k3UBcgAH5zse788n-QVQH3PgW%0D%0AVyvsPf2sfCS5ToRDlRoIF078umo4oe2x6sCLT1I2cDP0y4t5AoazTcg5KmtkPLigItg1oft57xy3%0D%0AbUvHOyif6po7ug1YdGJk74_uKw9I2brPyES8Ep10JFBwbSgWpO2DIwHaDg4cdfWN-g%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top 250</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Top Movies</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li>    <a href="/offsite/?page-action=ft_ad&token=BCYvLNr03IcaCTISE6VxvJAFfNh66hijS52DeAGjpIMVg8rfDM9mNzcQSM8-7oL1rmgrNG2fvOqa%0D%0A2yEeiRejyfDDtFmOBNWyYxiCcldvmjmf1cLCBXt3IH3GYGeGawO-C_3WVxasq_Sj-RzJ-7WqVH_P%0D%0Aug_cxs2pf9niPNa7_2yxZ0CRtL9by1BYLdeIgj40xa3uXZzXzyRm11tzNU1k4Zrvo9oSgXH5GSgF%0D%0A8rbnJDYxw5V9WfHecbOPX_2BBu-w8Q30a6rg0xKwIb1qbpJYAtJIOg%0D%0A&ref_=ft_ad"
itemprop='url'>Advertising</a>
</li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYtp_DqRrAZj3wJAQp6sEtwwyyt5wTzO9G1ss2H4llLmkwn89cgov1X4NumqwZtccKN6dUXYfoP%0D%0AypeEAexUtGzNpXZ24a8ICkfx6b-xlKJsGTn8tnrj78uCweeu4VXlbQr8jvQOAt_SLgY1laIIqmLh%0D%0AgGWgye9ZN8oHUFZz-Gb_Vu_R7rH4KCNtiK63R-zkzo5AmGaeNS95ysk5h0ZoFBh3kw%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYnbmjjpBTMKvjPD2LEeolmU8LCgPGQ6ElINddHkjsQHLe1I83xhtZDra5lgE3WfPjFN1haTIVJ%0D%0AN98XvRzYQEAXMUb4edrXSzbmfl7OKKDnQiN7arSyysP_USgWic0wgvz1mndVZjtOq0vIXgEOgVB6%0D%0A7-n8v9NCstBwau_L00xTpeNTHRlEHTjIp9vQMfq2JoUU%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYjz4GTT-FwZZVenxI-yCYu2PXjA1PuPEgZSCSRIzBIRwIHb2DMwd85Q3J3ZvYnSv2yyPhQ9vWK%0D%0AjnpZQVBmlKxgB5eNJfZ9nwW8tTF870rD7C6nvWyzpPtdyOYfUsiPE5hbCBhiRKgRtEWuukJmuNXM%0D%0AxpclysbGdbCI589JY5RlBRfJPCrQMBdIyfAI0F6JE0pcY_Q4FZAbxENHZbH5hCi9YdPV0Z6kgVLC%0D%0Agbb4om-XgXqsqTzaMDqkcsR2Z9s3EPKf%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/show_article?conditions';"
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2015
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/';"
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYjg2QYNELjanQiJzwHMFhPBdnI3G2DHr_CIu1drMTfWmmSbEZPQ5o8ROIlqC19g6lSPySVGeuc%0D%0AYIhAjs3_lPoubhoajFR2spc8SJcJ3YMS06TGWMJKJSJu0zjkHYPj0arPxnL0fSTBONiQv1ki4wz_%0D%0AIzWZ-Wue8jBNjBPyLjnJuFlVtD2mDwKxVfJc91vV_YNWDWJGdsl_zZsr-E8083IatjpBXbzGJD2i%0D%0A4nzEbVQo_vM%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Instant Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYjHOfJBRhDaICg1KkOqVjAZHpeouKHYBVxZD1DNS4mK3ibie7DzgnyuQK5xJnL0qA6PYu13Ujj%0D%0At1_G2TYLk303E2-abcmHH_4V_I3oeJsSHepNIwFs8p5iqDW9r49sj3IeJb9oZ9kxrSqMexUQkfRf%0D%0AjDWCT6Iyk0WBhqekIwU9R37JzagiaStPViqT4NyXb_FxKQlerOIyaxDSjRYRXax73BehNic-fyUs%0D%0AGx7YWkRFiC8%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Instant Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYr5-DXeUXp7NekcNuEdIjPBzikOA8a2RuLsHCRi7FyUCx6SjObO5iIuQbGZ1UF7J2qqXE56MBc%0D%0AIO1PbxvI92B6jzlUL-CDFJ4hROn6tl_xE4nVxmdEvJX0isnlyh2S6zW1vZAJ_RX0me5XjhU78PcX%0D%0Als4BfKcSMqpFpFQHRJjsR0i5ZNDtSV1uX2kQ_V8r70RPLSPI1zrXSr61AXY4ufFGVF6SQvzydDr8%0D%0AqclM9U1hjHs%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYvCGHfnA6LGM9Ec1D4SE5cSnvsuMbcJlX8BRhQZX2LM9g5tlSxJrmvuA_UbykPR30rB3nGK-Ne%0D%0AqbV62q1rsvDYkgENVN_Ii7LvfEVJRPRAgPDv5MXR4Fl-h626wDQiMNn6Jk9qKoILopufNadLNC8I%0D%0AFDQHkAEWF8xcWbXBKCDPXdl_9X9GiyPtW7ReWlJ3CpI899azzFwBHaGWsmEAlAeZ1hwZJzPyHPbw%0D%0AerAfk2-1xfc%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYha9iTS7OmZ2GGbZl2rInIadOKXsITJNnJtTzz0oA4yhLkumJnsW3mb67yhLoYfakkL8nGARhL%0D%0Acj4KRPF83QDti76xjOixHGNhWlsPAPYeOdUBRGlOs6or-UDWDHieXat4-xbjfkv_NPoIIhZRgKEk%0D%0Andyuov0Uci6mZCm_5gv_xpH963f5i-QxG0Bf3VjQ19yitUHjt0kocwZkS-XGe1ijUXZpDT3Gpb-5%0D%0AA-WHvx4p4n8%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYuNTbwBbUESZpCFRNVWtT_UK69oa0qUn3ckrBd5fk9140XdXtNX7DSgZVD93op2iKpcOzJkz7e%0D%0A_QvDLnIzKBaFwDQXtqQebBWE-6mcgX-oqbEUGAUVqe0deL4EG6O97V70Xa6l2jJnpeLCQT-4M6p-%0D%0AS-u4rZzIJeqEpLaUe7SIGAWkRPoNC0-Y29Z53l0ssk72gEkfvbAm0f7n5zZ7NUDnlhw5RXrd6sxK%0D%0AgoGSR41KK5O6Ke0dmuOuxtctEQl4iig-%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYrcNGl3wS9gqMJBYNLdNQHFxEEHszTAF7jGaGQ0VGY7jPtkrhHh5lMyLPUdddnQFgmZs_JXtJy%0D%0AqJwCqmHZkQm_xzqz8qDLcBtXRbiqTw_AQip9YsGtcOwoQMMh8PTmxKZvCCApoTFCgDNIw3PLZB3F%0D%0AFMJy8MtTcGn38T1678Zg248%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYglm-TQJ_gJmV4A3G334kTI_PKSIDJKPb4UvXw_ZXE0iES17fGU2efHWQSOh0Au6qcvpU18sUP%0D%0AJei-i1jtbFAY9CGYRNyv4V_y3zg5F6oA9HvXi4QJQL3nAJQXkvrPGwbVXp4SXdlQL3E19AlfX217%0D%0AO_YRWIHZagALEiR1CKrNiJM%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-2395339203._CB315760582_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-135080496._CB318528481_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-3627981178._CB318528484_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-2430112694._CB318528507_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=0101f7fb9b2089ca9ed7aff8a8f76849fbbf9ebbceb91426df3580788aebb9f7043a",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=377258321678"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-232398347._CB349580400_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=377258321678&ord=377258321678";
    },"unable to request AAN pixel");
</script>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="702"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
