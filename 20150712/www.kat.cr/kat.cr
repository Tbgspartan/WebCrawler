<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-0531997.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-0531997.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-0531997.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-0531997.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kat.cr/sc-0531997.js');

        sc('setHost', 'a.kat.cr');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: '0531997',
            detect_lang: 0,
            spare_click: 1        };
    </script>
    <script src="//kastatic.com/js/all-0531997.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<div id="_60318cd4e8d28f6fb76fe34e9bd9c498"></div>
<div id="_39ecb76dd457e5ac33776fdf11500d56"></div>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom" style="color: #ffeeb4; font-size: 34px; float:left; height: 50px; line-height: 50px;"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka">e</i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka">g</i></button></div>
			</form>
		</div>
        <div id="_277923e5f9d753c5b0630c28e641790c"></div>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080p/" class="tag7">1080p</a>
	<a href="/search/1080p/" class="tag5">1080p</a>
	<a href="/search/2014/" class="tag3">2014</a>
	<a href="/search/2015/" class="tag8">2015</a>
	<a href="/search/2015/" class="tag7">2015</a>
	<a href="/search/3d/" class="tag4">3d</a>
	<a href="/search/3d%20remux/" class="tag2">3d remux</a>
	<a href="/search/abcd%202%202015/" class="tag2">abcd 2 2015</a>
	<a href="/search/android/" class="tag9">android</a>
	<a href="/search/android/" class="tag4">android</a>
	<a href="/search/avengers%20age%20of%20ultron/" class="tag2">avengers age of ultron</a>
	<a href="/search/baahubali/" class="tag6">baahubali</a>
	<a href="/search/bahubali/" class="tag2">bahubali</a>
	<a href="/search/bahubali%20trailer/" class="tag2">bahubali trailer</a>
	<a href="/search/batman/" class="tag3">batman</a>
	<a href="/search/discography/" class="tag2">discography</a>
	<a href="/search/fast%20and%20furious%207/" class="tag2">fast and furious 7</a>
	<a href="/search/fast%20and%20furious%207%201080/" class="tag2">fast and furious 7 1080</a>
	<a href="/search/flac/" class="tag2">flac</a>
	<a href="/search/french/" class="tag2">french</a>
	<a href="/search/game%20of%20thrones/" class="tag3">game of thrones</a>
	<a href="/search/hamari%20adhuri%20kahani/" class="tag2">hamari adhuri kahani</a>
	<a href="/search/hannibal/" class="tag2">hannibal</a>
	<a href="/search/hindi/" class="tag8">hindi</a>
	<a href="/search/hindi/" class="tag7">hindi</a>
	<a href="/search/hindi%202015/" class="tag3">hindi 2015</a>
	<a href="/search/inside%20out/" class="tag2">inside out</a>
	<a href="/search/insurgent/" class="tag3">insurgent</a>
	<a href="/search/ita/" class="tag4">ita</a>
	<a href="/search/malayalam/" class="tag2">malayalam</a>
	<a href="/search/man%20of%20steel/" class="tag2">man of steel</a>
	<a href="/search/minions/" class="tag2">minions</a>
	<a href="/search/movies/" class="tag4">movies</a>
	<a href="/search/mr%20robot/" class="tag2">mr robot</a>
	<a href="/search/nezu/" class="tag8">nezu</a>
	<a href="/search/nl/" class="tag2">nl</a>
	<a href="/search/san%20andreas/" class="tag2">san andreas</a>
	<a href="/search/tamil/" class="tag4">tamil</a>
	<a href="/search/tanu%20weds%20manu/" class="tag2">tanu weds manu</a>
	<a href="/search/ted%202/" class="tag2">ted 2</a>
	<a href="/search/telugu/" class="tag2">telugu</a>
	<a href="/search/terminator%20genisys/" class="tag2">terminator genisys</a>
	<a href="/search/the%20walking%20dead/" class="tag2">the walking dead</a>
	<a href="/search/under%20the%20dome/" class="tag2">under the dome</a>
	<a href="/search/untouchable/" class="tag5">untouchable</a>
	<a href="/search/yify/" class="tag9">yify</a>
	<a href="/search/yify%201080p/" class="tag3">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag4">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="rsssign" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10917129,0" class="icomment icommentjs icon16" href="/jurassic-world-2015-1080p-hdrip-korsub-x264-aac2-0-rarbg-t10917129.html#comment"> <em class="iconvalue">386</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/jurassic-world-2015-1080p-hdrip-korsub-x264-aac2-0-rarbg-t10917129.html" class="cellMainLink">Jurassic World 2015 1080p HDRip KORSUB x264 AAC2 0-RARBG</a></div>
			</td>
			<td class="nobr center">4 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">20174</td>
			<td class="red lasttd center">24421</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10917731,0" class="icomment icommentjs icon16" href="/ted-2-2015-uncensored-1080p-hc-webrip-x264-aac2-0-rarbg-t10917731.html#comment"> <em class="iconvalue">119</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ted-2-2015-uncensored-1080p-hc-webrip-x264-aac2-0-rarbg-t10917731.html" class="cellMainLink">Ted 2 2015 UNCENSORED 1080p HC WEBRip x264 AAC2 0-RARBG</a></div>
			</td>
			<td class="nobr center">3.79 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">14824</td>
			<td class="red lasttd center">12487</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10916632,0" class="icomment icommentjs icon16" href="/ted-2-2015-hc-hdrip-xvid-etrg-t10916632.html#comment"> <em class="iconvalue">62</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ted-2-2015-hc-hdrip-xvid-etrg-t10916632.html" class="cellMainLink">Ted 2.2015.HC.HDRip.XViD-ETRG</a></div>
			</td>
			<td class="nobr center">1.39 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">15273</td>
			<td class="red lasttd center">11691</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10917557,0" class="icomment icommentjs icon16" href="/insurgent-2015-1080p-hdrip-x264-dd5-1-rarbg-t10917557.html#comment"> <em class="iconvalue">56</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/insurgent-2015-1080p-hdrip-x264-dd5-1-rarbg-t10917557.html" class="cellMainLink">Insurgent 2015 1080p HDRip x264 DD5 1-RARBG</a></div>
			</td>
			<td class="nobr center">4.04 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">9888</td>
			<td class="red lasttd center">12426</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10912960,0" class="icomment icommentjs icon16" href="/final-girl-2015-brrip-xvid-ac3-evo-t10912960.html#comment"> <em class="iconvalue">53</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/final-girl-2015-brrip-xvid-ac3-evo-t10912960.html" class="cellMainLink">Final Girl 2015 BRRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center">1.43 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">8537</td>
			<td class="red lasttd center">7413</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10922696,0" class="icomment icommentjs icon16" href="/justice-league-gods-and-monsters-2015-hdrip-xvid-ac3-evo-t10922696.html#comment"> <em class="iconvalue">23</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/justice-league-gods-and-monsters-2015-hdrip-xvid-ac3-evo-t10922696.html" class="cellMainLink">Justice League Gods and Monsters 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center">1.39 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center">13&nbsp;hours</td>
			<td class="green center">4960</td>
			<td class="red lasttd center">10233</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/spy-2015-truefrench-md-hdrip-xvid-t10923263.html" class="cellMainLink">Spy 2015 TRUEFRENCH MD HDRip XviD</a></div>
			</td>
			<td class="nobr center">696.72 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">11&nbsp;hours</td>
			<td class="green center">6771</td>
			<td class="red lasttd center">5140</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10914215,0" class="icomment icommentjs icon16" href="/strangerland-2015-hdrip-xvid-etrg-t10914215.html#comment"> <em class="iconvalue">65</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/strangerland-2015-hdrip-xvid-etrg-t10914215.html" class="cellMainLink">Strangerland 2015 HDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center">708.49 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">7006</td>
			<td class="red lasttd center">4654</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10918465,0" class="icomment icommentjs icon16" href="/american-heist-2014-truefrench-bdrip-xvid-avitech-avi-t10918465.html#comment"> <em class="iconvalue">9</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/american-heist-2014-truefrench-bdrip-xvid-avitech-avi-t10918465.html" class="cellMainLink">American Heist 2014 TRUEFRENCH BDRiP XViD-AViTECH avi</a></div>
			</td>
			<td class="nobr center">700.71 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">7823</td>
			<td class="red lasttd center">3323</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10917218,0" class="icomment icommentjs icon16" href="/eng-jurassic-world-2015-720p-hc-hdrip-x264-ac3-evo-t10917218.html#comment"> <em class="iconvalue">51</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/eng-jurassic-world-2015-720p-hc-hdrip-x264-ac3-evo-t10917218.html" class="cellMainLink">ENG - Jurassic World 2015 720p HC HDRip X264 AC3-EVO</a></div>
			</td>
			<td class="nobr center">2.56 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">5266</td>
			<td class="red lasttd center">4778</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10916774,0" class="icomment icommentjs icon16" href="/true-story-2015-1080p-web-dl-dd5-1-h-264-rarbg-t10916774.html#comment"> <em class="iconvalue">39</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/true-story-2015-1080p-web-dl-dd5-1-h-264-rarbg-t10916774.html" class="cellMainLink">True Story 2015 1080p WEB-DL DD5 1 H 264 RARBG</a></div>
			</td>
			<td class="nobr center">3.75 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">4748</td>
			<td class="red lasttd center">3749</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10907814,0" class="icomment icommentjs icon16" href="/terminator-genisys-2015-new-hdts-xvid-ac3-readnfo-mrg-t10907814.html#comment"> <em class="iconvalue">52</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/terminator-genisys-2015-new-hdts-xvid-ac3-readnfo-mrg-t10907814.html" class="cellMainLink">Terminator Genisys {2015} NEW HDTS XVID AC3 READNFO-MRG</a></div>
			</td>
			<td class="nobr center">1.77 <span>GB</span></td>
			<td class="center">15</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">3358</td>
			<td class="red lasttd center">3839</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10923718,0" class="icomment icommentjs icon16" href="/max-2015-hc-hdrip-xvid-evo-t10923718.html#comment"> <em class="iconvalue">9</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/max-2015-hc-hdrip-xvid-evo-t10923718.html" class="cellMainLink">Max 2015 HC HDRip XviD-EVO</a></div>
			</td>
			<td class="nobr center">1.33 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center">9&nbsp;hours</td>
			<td class="green center">2046</td>
			<td class="red lasttd center">5149</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10922333,0" class="icomment icommentjs icon16" href="/burying-the-ex-2014-720p-brrip-x264-yify-t10922333.html#comment"> <em class="iconvalue">12</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/burying-the-ex-2014-720p-brrip-x264-yify-t10922333.html" class="cellMainLink">Burying the Ex (2014) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center">700.95 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center">15&nbsp;hours</td>
			<td class="green center">3821</td>
			<td class="red lasttd center">3258</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10910226,0" class="icomment icommentjs icon16" href="/x-men-days-of-future-past-2014-the-rogue-cut-1080p-bluray-x264-sadpanda-rarbg-t10910226.html#comment"> <em class="iconvalue">66</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/x-men-days-of-future-past-2014-the-rogue-cut-1080p-bluray-x264-sadpanda-rarbg-t10910226.html" class="cellMainLink">X-Men Days of Future Past 2014 THE ROGUE CUT 1080p BluRay x264-SADPANDA[rarbg]</a></div>
			</td>
			<td class="nobr center">10.93 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">3073</td>
			<td class="red lasttd center">3981</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="rsssign" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10910673,0" class="icomment icommentjs icon16" href="/mr-robot-s01e03-720p-hdtv-x264-immerse-rartv-t10910673.html#comment"> <em class="iconvalue">209</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/mr-robot-s01e03-720p-hdtv-x264-immerse-rartv-t10910673.html" class="cellMainLink">Mr Robot S01E03 720p HDTV x264-IMMERSE[rartv]</a></div>
			</td>
			<td class="nobr center">658.77 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">9766</td>
			<td class="red lasttd center">1376</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10910461,0" class="icomment icommentjs icon16" href="/suits-s05e03-hdtv-x264-asap-rarbg-t10910461.html#comment"> <em class="iconvalue">79</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/suits-s05e03-hdtv-x264-asap-rarbg-t10910461.html" class="cellMainLink">Suits S05E03 HDTV x264-ASAP[rarbg]</a></div>
			</td>
			<td class="nobr center">258.75 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">8630</td>
			<td class="red lasttd center">2324</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10913799,0" class="icomment icommentjs icon16" href="/wayward-pines-s01e08-hdtv-x264-lol-ettv-t10913799.html#comment"> <em class="iconvalue">90</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/wayward-pines-s01e08-hdtv-x264-lol-ettv-t10913799.html" class="cellMainLink">Wayward Pines S01E08 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center">242.23 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">7590</td>
			<td class="red lasttd center">1010</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10921488,0" class="icomment icommentjs icon16" href="/dark-matter-s01e05-hdtv-x264-killers-rartv-t10921488.html#comment"> <em class="iconvalue">59</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dark-matter-s01e05-hdtv-x264-killers-rartv-t10921488.html" class="cellMainLink">Dark Matter S01E05 HDTV x264-KILLERS[rartv]</a></div>
			</td>
			<td class="nobr center">239.35 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">20&nbsp;hours</td>
			<td class="green center">6027</td>
			<td class="red lasttd center">1380</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10915446,0" class="icomment icommentjs icon16" href="/under-the-dome-s03e04-hdtv-x264-lol-rartv-t10915446.html#comment"> <em class="iconvalue">75</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/under-the-dome-s03e04-hdtv-x264-lol-rartv-t10915446.html" class="cellMainLink">Under the Dome S03E04 HDTV x264-LOL[rartv]</a></div>
			</td>
			<td class="nobr center">293.97 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">5136</td>
			<td class="red lasttd center">739</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10921299,0" class="icomment icommentjs icon16" href="/killjoys-s01e04-hdtv-x264-killers-rartv-t10921299.html#comment"> <em class="iconvalue">41</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/killjoys-s01e04-hdtv-x264-killers-rartv-t10921299.html" class="cellMainLink">Killjoys S01E04 HDTV x264-KILLERS[rartv]</a></div>
			</td>
			<td class="nobr center">378.28 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">21&nbsp;hours</td>
			<td class="green center">3467</td>
			<td class="red lasttd center">953</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10915998,0" class="icomment icommentjs icon16" href="/dominion-s02e01-hdtv-x264-asap-rartv-t10915998.html#comment"> <em class="iconvalue">69</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dominion-s02e01-hdtv-x264-asap-rartv-t10915998.html" class="cellMainLink">Dominion S02E01 HDTV x264-ASAP[rartv]</a></div>
			</td>
			<td class="nobr center">408.55 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">3568</td>
			<td class="red lasttd center">631</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10921088,0" class="icomment icommentjs icon16" href="/defiance-s03e06-hdtv-x264-asap-rartv-t10921088.html#comment"> <em class="iconvalue">34</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/defiance-s03e06-hdtv-x264-asap-rartv-t10921088.html" class="cellMainLink">Defiance S03E06 HDTV x264-ASAP[rartv]</a></div>
			</td>
			<td class="nobr center">335.89 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">22&nbsp;hours</td>
			<td class="green center">3307</td>
			<td class="red lasttd center">732</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10910204,0" class="icomment icommentjs icon16" href="/extant-s02e02-hdtv-x264-lol-rartv-t10910204.html#comment"> <em class="iconvalue">59</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/extant-s02e02-hdtv-x264-lol-rartv-t10910204.html" class="cellMainLink">Extant S02E02 HDTV x264-LOL[rartv]</a></div>
			</td>
			<td class="nobr center">245.16 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">3223</td>
			<td class="red lasttd center">375</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10921472,0" class="icomment icommentjs icon16" href="/the-messengers-2015-s01e11-hdtv-x264-lol-rartv-t10921472.html#comment"> <em class="iconvalue">29</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-messengers-2015-s01e11-hdtv-x264-lol-rartv-t10921472.html" class="cellMainLink">The Messengers 2015 S01E11 HDTV x264-LOL[rartv]</a></div>
			</td>
			<td class="nobr center">286.72 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">20&nbsp;hours</td>
			<td class="green center">2397</td>
			<td class="red lasttd center">820</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10905993,0" class="icomment icommentjs icon16" href="/tyrant-s02e04-720p-hdtv-x264-killers-rartv-t10905993.html#comment"> <em class="iconvalue">9</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/tyrant-s02e04-720p-hdtv-x264-killers-rartv-t10905993.html" class="cellMainLink">Tyrant S02E04 720p HDTV x264-KILLERS[rartv]</a></div>
			</td>
			<td class="nobr center">930.26 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">1533</td>
			<td class="red lasttd center">1029</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10918862,0" class="icomment icommentjs icon16" href="/hannibal-s03e06-hdtv-xvid-fum-ettv-t10918862.html#comment"> <em class="iconvalue">11</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/hannibal-s03e06-hdtv-xvid-fum-ettv-t10918862.html" class="cellMainLink">Hannibal S03E06 HDTV XviD-FUM[ettv]</a></div>
			</td>
			<td class="nobr center">364.72 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">1801</td>
			<td class="red lasttd center">695</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/marvels-agent-carter-s01e01-french-ld-web-dl-xvid-asphixias-avi-t10920422.html" class="cellMainLink">Marvels Agent Carter S01E01 FRENCH LD WEB-DL XviD-ASPHiXiAS avi</a></div>
			</td>
			<td class="nobr center">350.87 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">23&nbsp;hours</td>
			<td class="green center">1479</td>
			<td class="red lasttd center">418</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10921830,0" class="icomment icommentjs icon16" href="/orange-is-the-new-black-s03-complete-season-3-w-eng-fra-softsubs-720p-webrip-x264-mkv-ac3-5-1-ehhhh-t10921830.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					<a class="istill icon16" href="/orange-is-the-new-black-s03-complete-season-3-w-eng-fra-softsubs-720p-webrip-x264-mkv-ac3-5-1-ehhhh-t10921830.html#stills" title="Torrent Has Screenshots"><span></span></a>
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/orange-is-the-new-black-s03-complete-season-3-w-eng-fra-softsubs-720p-webrip-x264-mkv-ac3-5-1-ehhhh-t10921830.html" class="cellMainLink">Orange Is The New Black S03 COMPLETE Season 3 w Eng + Fra Softsubs - 720p WEBRip x264 [MKV,AC3,5.1] Ehhhh</a></div>
			</td>
			<td class="nobr center">7.48 <span>GB</span></td>
			<td class="center">42</td>
			<td class="center">18&nbsp;hours</td>
			<td class="green center">95</td>
			<td class="red lasttd center">602</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fortitude-s01e02-spanish-espaÃol-720p-hdtv-x264-newpct-t10909744.html" class="cellMainLink">Fortitude S01E02 SPANISH ESPAÃOL 720p HDTV x264-NEWPCT</a></div>
			</td>
			<td class="nobr center">1.74 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">263</td>
			<td class="red lasttd center">369</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="rsssign" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10914974,0" class="icomment icommentjs icon16" href="/mp3-new-releases-2015-week-27-glodls-t10914974.html#comment"> <em class="iconvalue">15</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/mp3-new-releases-2015-week-27-glodls-t10914974.html" class="cellMainLink">MP3 NEW RELEASES 2015 WEEK 27 [GloDLS]</a></div>
			</td>
			<td class="nobr center">4.03 <span>GB</span></td>
			<td class="center">607</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">722</td>
			<td class="red lasttd center">812</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10918108,0" class="icomment icommentjs icon16" href="/billboard-hot-100-singles-chart-18th-july-2015-cbr-320-kbps-aryan-l33t-t10918108.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/billboard-hot-100-singles-chart-18th-july-2015-cbr-320-kbps-aryan-l33t-t10918108.html" class="cellMainLink">Billboard Hot 100 Singles Chart (18th July 2015) CBR 320 Kbps [AryaN_L33T]</a></div>
			</td>
			<td class="nobr center">838.11 <span>MB</span></td>
			<td class="center">103</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">742</td>
			<td class="red lasttd center">546</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10917161,0" class="icomment icommentjs icon16" href="/tyrese-black-rose-2015-l-audio-l-albumtrack-l-320kbps-l-cbr-l-mp3-l-sn3h1t87-t10917161.html#comment"> <em class="iconvalue">12</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/tyrese-black-rose-2015-l-audio-l-albumtrack-l-320kbps-l-cbr-l-mp3-l-sn3h1t87-t10917161.html" class="cellMainLink">Tyrese - Black Rose (2015) l Audio l AlbumTrack l 320Kbps l CBR l Mp3 l sn3h1t87</a></div>
			</td>
			<td class="nobr center">142.49 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">1067</td>
			<td class="red lasttd center">157</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10908541,0" class="icomment icommentjs icon16" href="/va-100-magic-tracks-chillout-2015-mp3-320-kbps-t10908541.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-100-magic-tracks-chillout-2015-mp3-320-kbps-t10908541.html" class="cellMainLink">VA - 100 Magic Tracks Chillout (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center">1.06 <span>GB</span></td>
			<td class="center">102</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">312</td>
			<td class="red lasttd center">212</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10922642,0" class="icomment icommentjs icon16" href="/the-official-uk-top-40-singles-chart-10th-july-2015-cbr-320-kbps-aryan-l33t-t10922642.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-official-uk-top-40-singles-chart-10th-july-2015-cbr-320-kbps-aryan-l33t-t10922642.html" class="cellMainLink">The Official UK TOP 40 Singles Chart (10th July 2015) CBR 320 Kbps [AryaN_L33T]</a></div>
			</td>
			<td class="nobr center">342.91 <span>MB</span></td>
			<td class="center">44</td>
			<td class="center">13&nbsp;hours</td>
			<td class="green center">291</td>
			<td class="red lasttd center">214</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10908474,0" class="icomment icommentjs icon16" href="/va-drum-bass-summer-slammers-2015-mp3-320-kbps-t10908474.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-drum-bass-summer-slammers-2015-mp3-320-kbps-t10908474.html" class="cellMainLink">VA - Drum &amp; Bass Summer Slammers (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center">457.35 <span>MB</span></td>
			<td class="center">33</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">297</td>
			<td class="red lasttd center">114</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10920044,0" class="icomment icommentjs icon16" href="/house-electro-house-progressive-deep-house-tech-house-techno-trance-indie-dance-nu-disco-beatport-top-100-downloads-june-2015-mp3-320-kbps-edm-rg-t10920044.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/house-electro-house-progressive-deep-house-tech-house-techno-trance-indie-dance-nu-disco-beatport-top-100-downloads-june-2015-mp3-320-kbps-edm-rg-t10920044.html" class="cellMainLink">(House, Electro House, Progressive, Deep House, Tech House, Techno, Trance, Indie Dance / Nu Disco) Beatport Top 100 Downloads June (2015) MP3, 320 Kbps [EDM RG]</a></div>
			</td>
			<td class="nobr center">1.28 <span>GB</span></td>
			<td class="center">101</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">145</td>
			<td class="red lasttd center">258</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10910528,0" class="icomment icommentjs icon16" href="/va-urban-dance-13-2015-mp3-divxtotal-t10910528.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-urban-dance-13-2015-mp3-divxtotal-t10910528.html" class="cellMainLink">VA - Urban Dance 13 (2015) MP3-DIVXTOTAL</a></div>
			</td>
			<td class="nobr center">509.89 <span>MB</span></td>
			<td class="center">67</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">291</td>
			<td class="red lasttd center">91</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10906591,0" class="icomment icommentjs icon16" href="/paul-mccartney-discography-1967-2014-bbm-t10906591.html#comment"> <em class="iconvalue">8</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/paul-mccartney-discography-1967-2014-bbm-t10906591.html" class="cellMainLink">Paul McCartney - Discography (1967 - 2014) BBM</a></div>
			</td>
			<td class="nobr center">14.12 <span>GB</span></td>
			<td class="center">2294</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">103</td>
			<td class="red lasttd center">269</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-ministry-of-sound-future-bass-2015-mp3-320-kbps-t10919459.html" class="cellMainLink">VA - Ministry Of Sound - Future Bass (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center">712.45 <span>MB</span></td>
			<td class="center">34</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">145</td>
			<td class="red lasttd center">163</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-fab-four-beatles-solo-greatest-hits-bubanee-t10922428.html" class="cellMainLink">The Fab Four (Beatles) - Solo Greatest Hits [Bubanee]</a></div>
			</td>
			<td class="nobr center">598.06 <span>MB</span></td>
			<td class="center">95</td>
			<td class="center">14&nbsp;hours</td>
			<td class="green center">160</td>
			<td class="red lasttd center">127</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10919800,0" class="icomment icommentjs icon16" href="/va-538-hitzone-74-2015-mp3-t10919800.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-538-hitzone-74-2015-mp3-t10919800.html" class="cellMainLink">VA - 538 Hitzone 74 (2015) [MP3]</a></div>
			</td>
			<td class="nobr center">388.8 <span>MB</span></td>
			<td class="center">51</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">150</td>
			<td class="red lasttd center">65</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-chillout-the-beauty-of-lounge-jazz-music-2015-mp3-divxtotal-t10924626.html" class="cellMainLink">VA - Chillout The Beauty of Lounge &amp; Jazz Music (2015) MP3-DIVXTOTAL</a></div>
			</td>
			<td class="nobr center">451.23 <span>MB</span></td>
			<td class="center">54</td>
			<td class="center">5&nbsp;hours</td>
			<td class="green center">1</td>
			<td class="red lasttd center">173</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-30-pop-classics-on-piano-2015-mp3-divxtotal-t10924640.html" class="cellMainLink">VA - 30 Pop Classics on Piano (2015) MP3-DIVXTOTAL</a></div>
			</td>
			<td class="nobr center">270.6 <span>MB</span></td>
			<td class="center">31</td>
			<td class="center">5&nbsp;hours</td>
			<td class="green center">108</td>
			<td class="red lasttd center">66</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-chillout-for-weddings-great-chillout-music-for-a-perfect-wedding-mp3-divxtotal-t10924632.html" class="cellMainLink">VA - Chillout for Weddings - Great Chillout Music for a Perfect Wedding MP3-DIVXTOTAL</a></div>
			</td>
			<td class="nobr center">259.62 <span>MB</span></td>
			<td class="center">26</td>
			<td class="center">5&nbsp;hours</td>
			<td class="green center">60</td>
			<td class="red lasttd center">65</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="rsssign" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10917416,0" class="icomment icommentjs icon16" href="/pc-f1-2015-full-unlocked-rldgames-t10917416.html#comment"> <em class="iconvalue">43</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/pc-f1-2015-full-unlocked-rldgames-t10917416.html" class="cellMainLink">[PC] F1.2015.FULL.UNLOCKED-RLDGAMES</a></div>
			</td>
			<td class="nobr center">9.87 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">246</td>
			<td class="red lasttd center">1233</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10917246,0" class="icomment icommentjs icon16" href="/mortal-kombat-x-premium-edition-update-10-2015-pc-repack-Ð¾Ñ-nemos-t10917246.html#comment"> <em class="iconvalue">36</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/mortal-kombat-x-premium-edition-update-10-2015-pc-repack-Ð¾Ñ-nemos-t10917246.html" class="cellMainLink">Mortal Kombat X: Premium Edition [Update 10] (2015) PC | RePack Ð¾Ñ =nemos=</a></div>
			</td>
			<td class="nobr center">25.69 <span>GB</span></td>
			<td class="center">10</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">280</td>
			<td class="red lasttd center">713</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10909971,0" class="icomment icommentjs icon16" href="/60-seconds-v1-042-2015-pc-repack-by-rg-freedom-t10909971.html#comment"> <em class="iconvalue">9</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/60-seconds-v1-042-2015-pc-repack-by-rg-freedom-t10909971.html" class="cellMainLink">60 Seconds! [v1.042] (2015) PC | RePack by RG Freedom</a></div>
			</td>
			<td class="nobr center">217.45 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">770</td>
			<td class="red lasttd center">51</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10915551,0" class="icomment icommentjs icon16" href="/f1-2015-3dm-not-yet-cracked-t10915551.html#comment"> <em class="iconvalue">58</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/f1-2015-3dm-not-yet-cracked-t10915551.html" class="cellMainLink">F1 2015-3DM [NOT YET CRACKED]</a></div>
			</td>
			<td class="nobr center">10.16 <span>GB</span></td>
			<td class="center">14</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">203</td>
			<td class="red lasttd center">545</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10922870,0" class="icomment icommentjs icon16" href="/just-cause-2-rus-eng-multi6-w-all-dlcs-repack-by-nemos-t10922870.html#comment"> <em class="iconvalue">18</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/just-cause-2-rus-eng-multi6-w-all-dlcs-repack-by-nemos-t10922870.html" class="cellMainLink">Just Cause 2 [RUS | ENG| MULTi6] w/ All DLCs RePack by nemos</a></div>
			</td>
			<td class="nobr center">1.27 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center">12&nbsp;hours</td>
			<td class="green center">320</td>
			<td class="red lasttd center">170</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10916806,0" class="icomment icommentjs icon16" href="/the-amazing-spider-man-2-bundle-plaza-t10916806.html#comment"> <em class="iconvalue">7</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/the-amazing-spider-man-2-bundle-plaza-t10916806.html" class="cellMainLink">The Amazing Spider Man 2 Bundle-PLAZA</a></div>
			</td>
			<td class="nobr center">7.7 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">96</td>
			<td class="red lasttd center">318</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/final-fantasy-xiii-2010-2014-pc-repack-by-karame1ka-t10912219.html" class="cellMainLink">Final Fantasy XIII (2010-2014) PC | RePack by Karame1ka</a></div>
			</td>
			<td class="nobr center">27.53 <span>GB</span></td>
			<td class="center">19</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">232</td>
			<td class="red lasttd center">109</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10914682,0" class="icomment icommentjs icon16" href="/the-red-solstice-reloaded-t10914682.html#comment"> <em class="iconvalue">20</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/the-red-solstice-reloaded-t10914682.html" class="cellMainLink">The.Red.Solstice-RELOADED</a></div>
			</td>
			<td class="nobr center">1 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">176</td>
			<td class="red lasttd center">116</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10923191,0" class="icomment icommentjs icon16" href="/titan-quest-duology-rus-eng-repack-by-rg-mechanics-t10923191.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/titan-quest-duology-rus-eng-repack-by-rg-mechanics-t10923191.html" class="cellMainLink">Titan Quest [DUOLOGY] [RUS | ENG] RePack by RG Mechanics</a></div>
			</td>
			<td class="nobr center">2.98 <span>GB</span></td>
			<td class="center">15</td>
			<td class="center">11&nbsp;hours</td>
			<td class="green center">150</td>
			<td class="red lasttd center">96</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10907416,0" class="icomment icommentjs icon16" href="/legends-of-eisenwald-update-4-2015-pc-repack-Ð¾Ñ-xatab-t10907416.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/legends-of-eisenwald-update-4-2015-pc-repack-Ð¾Ñ-xatab-t10907416.html" class="cellMainLink">Legends of Eisenwald [Update 4] (2015) PC | RePack Ð¾Ñ xatab</a></div>
			</td>
			<td class="nobr center">683.87 <span>MB</span></td>
			<td class="center">8</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">183</td>
			<td class="red lasttd center">50</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10906020,0" class="icomment icommentjs icon16" href="/battle-fantasia-reloaded-t10906020.html#comment"> <em class="iconvalue">17</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/battle-fantasia-reloaded-t10906020.html" class="cellMainLink">Battle.Fantasia-RELOADED</a></div>
			</td>
			<td class="nobr center">2.25 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">146</td>
			<td class="red lasttd center">78</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10919523,0" class="icomment icommentjs icon16" href="/age-of-empires-ii-hd-the-forgotten-version-4-1-1-repack-mr-dj-t10919523.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					<a class="istill icon16" href="/age-of-empires-ii-hd-the-forgotten-version-4-1-1-repack-mr-dj-t10919523.html#stills" title="Torrent Has Screenshots"><span></span></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/age-of-empires-ii-hd-the-forgotten-version-4-1-1-repack-mr-dj-t10919523.html" class="cellMainLink">Age Of Empires II HD The Forgotten version 4.1.1 repack Mr DJ</a></div>
			</td>
			<td class="nobr center">1.15 <span>GB</span></td>
			<td class="center">11</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">80</td>
			<td class="red lasttd center">131</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10922523,0" class="icomment icommentjs icon16" href="/contradiction-spot-the-liar-reloaded-t10922523.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/contradiction-spot-the-liar-reloaded-t10922523.html" class="cellMainLink">Contradiction Spot The Liar-RELOADED</a></div>
			</td>
			<td class="nobr center">2.34 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center">14&nbsp;hours</td>
			<td class="green center">49</td>
			<td class="red lasttd center">69</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10914343,0" class="icomment icommentjs icon16" href="/traverser-flt-t10914343.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/traverser-flt-t10914343.html" class="cellMainLink">Traverser-FLT</a></div>
			</td>
			<td class="nobr center">2.14 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">71</td>
			<td class="red lasttd center">45</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10919489,0" class="icomment icommentjs icon16" href="/tomb-raider-2013-goty-edition-repack-by-corepack-t10919489.html#comment"> <em class="iconvalue">10</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/tomb-raider-2013-goty-edition-repack-by-corepack-t10919489.html" class="cellMainLink">Tomb Raider (2013) GOTY Edition - RePack by CorePack</a></div>
			</td>
			<td class="nobr center">6.56 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">14</td>
			<td class="red lasttd center">44</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="rsssign" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10908502,0" class="icomment icommentjs icon16" href="/internet-download-manager-idm-6-23-build-14-registered-32bit-64bit-patch-crackingpatching-t10908502.html#comment"> <em class="iconvalue">111</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/internet-download-manager-idm-6-23-build-14-registered-32bit-64bit-patch-crackingpatching-t10908502.html" class="cellMainLink">Internet Download Manager (IDM) 6.23 Build 14 Registered (32bit + 64bit Patch) [CrackingPatching]</a></div>
			</td>
			<td class="nobr center">9.66 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">1107</td>
			<td class="red lasttd center">475</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10906060,0" class="icomment icommentjs icon16" href="/microsoft-office-2016-professional-plus-16-0-4229-1002-preview-32-64-bit-ratiborus-2-8-activator-appzdam-t10906060.html#comment"> <em class="iconvalue">89</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/microsoft-office-2016-professional-plus-16-0-4229-1002-preview-32-64-bit-ratiborus-2-8-activator-appzdam-t10906060.html" class="cellMainLink">Microsoft Office 2016 Professional Plus 16.0.4229.1002 Preview [32-64 bit] (Ratiborus 2.8) + Activator - AppzDam</a></div>
			</td>
			<td class="nobr center">2.55 <span>GB</span></td>
			<td class="center">14</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">735</td>
			<td class="red lasttd center">659</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10914163,0" class="icomment icommentjs icon16" href="/windows-10-pro-insider-preview-build-10166-en-us-x64-by-whitedeath-t10914163.html#comment"> <em class="iconvalue">52</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-10-pro-insider-preview-build-10166-en-us-x64-by-whitedeath-t10914163.html" class="cellMainLink">Windows 10 Pro Insider Preview Build 10166 En-us X64 By:WhiteDeath</a></div>
			</td>
			<td class="nobr center">3.73 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">348</td>
			<td class="red lasttd center">333</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10922671,0" class="icomment icommentjs icon16" href="/tails-1-4-1-i386-iso-multilang-tntvillage-t10922671.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/tails-1-4-1-i386-iso-multilang-tntvillage-t10922671.html" class="cellMainLink">Tails 1.4.1 i386, [Iso - MultiLang] [TNTVillage]</a></div>
			</td>
			<td class="nobr center">925.62 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center">13&nbsp;hours</td>
			<td class="green center">470</td>
			<td class="red lasttd center">45</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10912089,0" class="icomment icommentjs icon16" href="/adobe-photoshop-elements-13-1-multilingual-keygen-patch-32-64-bit-appzdam-t10912089.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/adobe-photoshop-elements-13-1-multilingual-keygen-patch-32-64-bit-appzdam-t10912089.html" class="cellMainLink">Adobe Photoshop Elements 13.1 Multilingual + Keygen/Patch [32-64 bit] - AppzDam</a></div>
			</td>
			<td class="nobr center">3.25 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">123</td>
			<td class="red lasttd center">89</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10907172,0" class="icomment icommentjs icon16" href="/k-lite-codec-pack-11-2-0-mega-full-standard-basic-update-t10907172.html#comment"> <em class="iconvalue">15</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/k-lite-codec-pack-11-2-0-mega-full-standard-basic-update-t10907172.html" class="cellMainLink">K-Lite Codec Pack 11.2.0 Mega_Full_Standard_Basic + Update</a></div>
			</td>
			<td class="nobr center">150.82 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">149</td>
			<td class="red lasttd center">48</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10907222,0" class="icomment icommentjs icon16" href="/atomix-virtualdj-pro-infinity-v8-0-2345-all-skins-samples-soundeffects-incl-crack-m4master-teamos-hkrg-t10907222.html#comment"> <em class="iconvalue">27</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/atomix-virtualdj-pro-infinity-v8-0-2345-all-skins-samples-soundeffects-incl-crack-m4master-teamos-hkrg-t10907222.html" class="cellMainLink">Atomix VirtualDJ Pro Infinity v8.0.2345 [All Skins, Samples &amp; SoundEffects] Incl. Crack [M4Master][TeamOS-HKRG]</a></div>
			</td>
			<td class="nobr center">120.03 <span>MB</span></td>
			<td class="center">9</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">144</td>
			<td class="red lasttd center">25</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10913801,0" class="icomment icommentjs icon16" href="/amd-catalyst-display-drivers-15-7-whql-t10913801.html#comment"> <em class="iconvalue">38</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/amd-catalyst-display-drivers-15-7-whql-t10913801.html" class="cellMainLink">AMD Catalyst Display Drivers 15.7 WHQL</a></div>
			</td>
			<td class="nobr center">1.37 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">90</td>
			<td class="red lasttd center">79</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10922388,0" class="icomment icommentjs icon16" href="/utorrent-pro-3-4-3-build-40633-stable-incl-crack-portable-4realtorrentz-t10922388.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/utorrent-pro-3-4-3-build-40633-stable-incl-crack-portable-4realtorrentz-t10922388.html" class="cellMainLink">uTorrent Pro 3.4.3 Build 40633 Stable Incl Crack + Portable [4realtorrentz]</a></div>
			</td>
			<td class="nobr center">7.05 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">15&nbsp;hours</td>
			<td class="green center">114</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10917361,0" class="icomment icommentjs icon16" href="/windows-10-build-10166-x86-aio-en-us-by-whitedeath-teamos-t10917361.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-10-build-10166-x86-aio-en-us-by-whitedeath-teamos-t10917361.html" class="cellMainLink">Windows 10 Build 10166 x86 AIO En-us By:WhiteDeath[TeamOS]</a></div>
			</td>
			<td class="nobr center">2.91 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">56</td>
			<td class="red lasttd center">70</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10912712,0" class="icomment icommentjs icon16" href="/unity3d-pro-5-1-1f1-eng-patch-32-64-bit-appzdam-t10912712.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/unity3d-pro-5-1-1f1-eng-patch-32-64-bit-appzdam-t10912712.html" class="cellMainLink">Unity3D Pro 5.1.1f1 [Eng] + Patch [32-64 bit] - AppzDam</a></div>
			</td>
			<td class="nobr center">3.01 <span>GB</span></td>
			<td class="center">177</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">26</td>
			<td class="red lasttd center">62</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10918308,0" class="icomment icommentjs icon16" href="/58-hand-drawn-animal-pack-cm-189620-t10918308.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/58-hand-drawn-animal-pack-cm-189620-t10918308.html" class="cellMainLink">58 Hand Drawn Animal Pack - CM 189620</a></div>
			</td>
			<td class="nobr center">66.59 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">63</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10923956,0" class="icomment icommentjs icon16" href="/skypr-the-adblocker-for-skype-techtools-t10923956.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/skypr-the-adblocker-for-skype-techtools-t10923956.html" class="cellMainLink">Skypr - The adblocker for Skype [TechTools]</a></div>
			</td>
			<td class="nobr center">6.53 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">7&nbsp;hours</td>
			<td class="green center">53</td>
			<td class="red lasttd center">19</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/ez-cd-audio-converter-3-0-8-x32-x64-multi-portable-t10915406.html" class="cellMainLink">EZ CD Audio Converter 3.0.8.(x32/x64)[Multi][Portable]</a></div>
			</td>
			<td class="nobr center">47.48 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">49</td>
			<td class="red lasttd center">18</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10910181,0" class="icomment icommentjs icon16" href="/ios-9-beta-3-iphone-5s-model-a1453-a1533-13a4293f-zip-t10910181.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/ios-9-beta-3-iphone-5s-model-a1453-a1533-13a4293f-zip-t10910181.html" class="cellMainLink">iOS 9 beta 3 iPhone 5s Model A1453 A1533 13A4293f.zip</a></div>
			</td>
			<td class="nobr center">1.89 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">21</td>
			<td class="red lasttd center">42</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="rsssign" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-working-02-720p-mkv-t10924678.html" class="cellMainLink">[HorribleSubs] Working!!! - 02 [720p].mkv</a></div>
			</td>
			<td class="nobr center">322.56 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">4&nbsp;hours</td>
			<td class="green center">1551</td>
			<td class="red lasttd center">573</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10914360,0" class="icomment icommentjs icon16" href="/fansub-resistance-naruto-shippuuden-419-1280x720-mp4-t10914360.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fansub-resistance-naruto-shippuuden-419-1280x720-mp4-t10914360.html" class="cellMainLink">[Fansub-Resistance]Naruto Shippuuden 419 (1280x720).mp4</a></div>
			</td>
			<td class="nobr center">201.35 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">1686</td>
			<td class="red lasttd center">175</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-himouto-umaru-chan-01-raw-abc-1280x720-x264-aac-mp4-t10909578.html" class="cellMainLink">[Leopard-Raws] Himouto! Umaru-chan - 01 RAW (ABC 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">351.46 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">1303</td>
			<td class="red lasttd center">135</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10909064,0" class="icomment icommentjs icon16" href="/leopard-raws-okusama-ga-seito-kaichou-02-raw-atx-1280x720-x264-aac-mp4-t10909064.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-okusama-ga-seito-kaichou-02-raw-atx-1280x720-x264-aac-mp4-t10909064.html" class="cellMainLink">[Leopard-Raws] Okusama ga Seito Kaichou! - 02 RAW (ATX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">125.07 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">1318</td>
			<td class="red lasttd center">76</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-classroom-crisis-02-raw-tbs-1280x720-x264-aac-mp4-t10922033.html" class="cellMainLink">[Leopard-Raws] Classroom Crisis - 02 RAW (TBS 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">311.04 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">17&nbsp;hours</td>
			<td class="green center">900</td>
			<td class="red lasttd center">182</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/commie-senki-zesshou-symphogear-gx-02-d9f17490-mkv-t10924278.html" class="cellMainLink">[Commie] Senki Zesshou Symphogear GX - 02 [D9F17490].mkv</a></div>
			</td>
			<td class="nobr center">518.4 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">6&nbsp;hours</td>
			<td class="green center">634</td>
			<td class="red lasttd center">346</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-shokugeki-no-souma-14-5-raw-tbs-1280x720-x264-aac-mp4-t10922297.html" class="cellMainLink">[Leopard-Raws] Shokugeki no Souma - 14.5 RAW (TBS 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">546.08 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">15&nbsp;hours</td>
			<td class="green center">694</td>
			<td class="red lasttd center">218</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-the-disappearance-of-nagato-yuki-chan-15-raw-mx-1280x720-x264-aac-mp4-t10922018.html" class="cellMainLink">[Leopard-Raws] The Disappearance of Nagato Yuki-chan - 15 RAW (MX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">418.14 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">17&nbsp;hours</td>
			<td class="green center">701</td>
			<td class="red lasttd center">208</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-senki-zesshou-symphogear-gx-02-raw-mbs-1280x720-x264-aac-mp4-t10922017.html" class="cellMainLink">[Leopard-Raws] Senki Zesshou Symphogear GX - 02 RAW (MBS 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">531.68 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">17&nbsp;hours</td>
			<td class="green center">691</td>
			<td class="red lasttd center">206</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-kuusen-madoushi-kouhosei-no-kyoukan-01-raw-sun-1280x720-x264-aac-mp4-t10913899.html" class="cellMainLink">[Leopard-Raws] Kuusen Madoushi Kouhosei no Kyoukan - 01 RAW (SUN 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">362.21 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">340</td>
			<td class="red lasttd center">137</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10912831,0" class="icomment icommentjs icon16" href="/naruto-shippuden-episode-419-english-subbed-480p-arizone-t10912831.html#comment"> <em class="iconvalue">17</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/naruto-shippuden-episode-419-english-subbed-480p-arizone-t10912831.html" class="cellMainLink">Naruto Shippuden Episode 419 [English Subbed] 480p ~ARIZONE</a></div>
			</td>
			<td class="nobr center">53.34 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">293</td>
			<td class="red lasttd center">80</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10908691,0" class="icomment icommentjs icon16" href="/dragon-ball-z-gt-all-episodes-all-movies-specials-400-480p-engdub-eng-sub-60-90mb-iorchid-t10908691.html#comment"> <em class="iconvalue">16</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dragon-ball-z-gt-all-episodes-all-movies-specials-400-480p-engdub-eng-sub-60-90mb-iorchid-t10908691.html" class="cellMainLink">Dragon Ball / Z / GT All Episodes ,All Movies + Specials [400 - 480p][EngDub-Eng Sub][60 - 90mb]_iORcHiD</a></div>
			</td>
			<td class="nobr center">42.08 <span>GB</span></td>
			<td class="center">559</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">1</td>
			<td class="red lasttd center">271</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10913340,0" class="icomment icommentjs icon16" href="/animerg-noragami-season-1-complete-eng-dub-720p-scavvykid-t10913340.html#comment"> <em class="iconvalue">9</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-noragami-season-1-complete-eng-dub-720p-scavvykid-t10913340.html" class="cellMainLink">[AnimeRG] Noragami Season 1 complete [Eng Dub] [720p] [ScavvyKiD]</a></div>
			</td>
			<td class="nobr center">5.42 <span>GB</span></td>
			<td class="center">14</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">106</td>
			<td class="red lasttd center">128</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10906239,0" class="icomment icommentjs icon16" href="/animerg-cardcaptor-sakura-complete-480p-ep-1-70-scavvykid-t10906239.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-cardcaptor-sakura-complete-480p-ep-1-70-scavvykid-t10906239.html" class="cellMainLink">[AnimeRG] Cardcaptor Sakura Complete [480p] [EP 1 - 70] [ScavvyKiD]</a></div>
			</td>
			<td class="nobr center">9.97 <span>GB</span></td>
			<td class="center">72</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">27</td>
			<td class="red lasttd center">146</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-aquarion-logos-02-raw-kbs-1280x720-x264-aac-mp4-t10919161.html" class="cellMainLink">[Leopard-Raws] Aquarion Logos - 02 RAW (KBS 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">469.62 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">8</td>
			<td class="red lasttd center">73</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="rsssign" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10909372,0" class="icomment icommentjs icon16" href="/marvel-week-07-08-2015-nem-t10909372.html#comment"> <em class="iconvalue">40</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/marvel-week-07-08-2015-nem-t10909372.html" class="cellMainLink">Marvel Week+ (07-08-2015) (- Nem -)</a></div>
			</td>
			<td class="nobr center">647.48 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">797</td>
			<td class="red lasttd center">287</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10914826,0" class="icomment icommentjs icon16" href="/dc-week-07-08-2015-vertigo-aka-dc-you-week-06-nem-t10914826.html#comment"> <em class="iconvalue">30</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/dc-week-07-08-2015-vertigo-aka-dc-you-week-06-nem-t10914826.html" class="cellMainLink">DC Week+ (07-08-2015) (+ Vertigo) (aka DC YOU Week 06) (- Nem -)</a></div>
			</td>
			<td class="nobr center">474.11 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">561</td>
			<td class="red lasttd center">233</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10914188,0" class="icomment icommentjs icon16" href="/the-complete-photographer-a-masterclass-in-every-genre-dk-publishing-2010-pdf-gooner-t10914188.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-complete-photographer-a-masterclass-in-every-genre-dk-publishing-2010-pdf-gooner-t10914188.html" class="cellMainLink">The Complete Photographer - A Masterclass In Every Genre (DK Publishing) (2010).pdf Gooner</a></div>
			</td>
			<td class="nobr center">105.09 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">577</td>
			<td class="red lasttd center">53</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10924250,0" class="icomment icommentjs icon16" href="/assorted-magazines-bundle-july-11-2015-true-pdf-t10924250.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/assorted-magazines-bundle-july-11-2015-true-pdf-t10924250.html" class="cellMainLink">Assorted Magazines Bundle - July 11 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center">520.19 <span>MB</span></td>
			<td class="center">29</td>
			<td class="center">6&nbsp;hours</td>
			<td class="green center">240</td>
			<td class="red lasttd center">289</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10918746,0" class="icomment icommentjs icon16" href="/woodwork-a-step-by-step-photographic-guide-dk-publishing-2010-pdf-gooner-t10918746.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/woodwork-a-step-by-step-photographic-guide-dk-publishing-2010-pdf-gooner-t10918746.html" class="cellMainLink">Woodwork - A Step-by-Step Photographic Guide (DK Publishing) (2010).pdf Gooner</a></div>
			</td>
			<td class="nobr center">71.95 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">454</td>
			<td class="red lasttd center">65</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10914361,0" class="icomment icommentjs icon16" href="/fashion-photography-101-a-complete-course-for-the-new-fashion-photographers-2012-pdf-gooner-t10914361.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/fashion-photography-101-a-complete-course-for-the-new-fashion-photographers-2012-pdf-gooner-t10914361.html" class="cellMainLink">Fashion Photography 101 - A Complete Course for the New Fashion Photographers (2012).pdf Gooner</a></div>
			</td>
			<td class="nobr center">121.16 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">245</td>
			<td class="red lasttd center">63</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10910796,0" class="icomment icommentjs icon16" href="/home-garden-magazines-july-9-2015-true-pdf-t10910796.html#comment"> <em class="iconvalue">7</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/home-garden-magazines-july-9-2015-true-pdf-t10910796.html" class="cellMainLink">Home &amp; Garden Magazines - July 9 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center">383.15 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">236</td>
			<td class="red lasttd center">68</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10910437,0" class="icomment icommentjs icon16" href="/automobile-magazines-bundle-july-9-2015-true-pdf-t10910437.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/automobile-magazines-bundle-july-9-2015-true-pdf-t10910437.html" class="cellMainLink">Automobile Magazines Bundle - July 9 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center">277.02 <span>MB</span></td>
			<td class="center">10</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">187</td>
			<td class="red lasttd center">60</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10913764,0" class="icomment icommentjs icon16" href="/god-is-dead-001-038-extras-2013-ongoing-digital-empire-nem-t10913764.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/god-is-dead-001-038-extras-2013-ongoing-digital-empire-nem-t10913764.html" class="cellMainLink">God is Dead (001-038+Extras)(2013-Ongoing)(Digital) (Empire) (- Nem -)</a></div>
			</td>
			<td class="nobr center">1.86 <span>GB</span></td>
			<td class="center">41</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">148</td>
			<td class="red lasttd center">70</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10924534,0" class="icomment icommentjs icon16" href="/tabloid-magazines-bundle-july-11-2015-true-pdf-t10924534.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/tabloid-magazines-bundle-july-11-2015-true-pdf-t10924534.html" class="cellMainLink">Tabloid Magazines Bundle - July 11 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center">316.64 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center">5&nbsp;hours</td>
			<td class="green center">144</td>
			<td class="red lasttd center">70</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10906600,0" class="icomment icommentjs icon16" href="/0-day-week-of-2015-07-01-t10906600.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/0-day-week-of-2015-07-01-t10906600.html" class="cellMainLink">0-Day Week of 2015.07.01</a></div>
			</td>
			<td class="nobr center">7 <span>GB</span></td>
			<td class="center">157</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">86</td>
			<td class="red lasttd center">109</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10921209,0" class="icomment icommentjs icon16" href="/warhammer-40k-7th-edition-codex-dark-angels-t10921209.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/warhammer-40k-7th-edition-codex-dark-angels-t10921209.html" class="cellMainLink">Warhammer 40k - 7th Edition Codex - Dark Angels</a></div>
			</td>
			<td class="nobr center">353.51 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">21&nbsp;hours</td>
			<td class="green center">139</td>
			<td class="red lasttd center">47</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10908443,0" class="icomment icommentjs icon16" href="/anne-mccaffrey-dragonriders-of-pern-1-12-t10908443.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/anne-mccaffrey-dragonriders-of-pern-1-12-t10908443.html" class="cellMainLink">Anne McCaffrey - Dragonriders of Pern [1-12]</a></div>
			</td>
			<td class="nobr center">4.58 <span>GB</span></td>
			<td class="center">27</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">96</td>
			<td class="red lasttd center">82</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10906622,0" class="icomment icommentjs icon16" href="/hitlist-week-of-2015-07-01-t10906622.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/hitlist-week-of-2015-07-01-t10906622.html" class="cellMainLink">Hitlist Week of 2015.07.01</a></div>
			</td>
			<td class="nobr center">42.75 <span>GB</span></td>
			<td class="center">397</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">9</td>
			<td class="red lasttd center">115</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/ramayan-3392-a-d-001-004-origins-2014-2015-digital-empire-nem-t10925150.html" class="cellMainLink">Ramayan 3392 A.D. (001-004+Origins) (2014-2015) (digital-Empire) (- Nem -)</a></div>
			</td>
			<td class="nobr center">382.38 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center">2&nbsp;hours</td>
			<td class="green center">1</td>
			<td class="red lasttd center">53</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="rsssign" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10919406,0" class="icomment icommentjs icon16" href="/buena-vista-social-club-discography-flac-vtwin88cube-t10919406.html#comment"> <em class="iconvalue">7</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/buena-vista-social-club-discography-flac-vtwin88cube-t10919406.html" class="cellMainLink">Buena Vista Social Club - Discography {FLAC} vtwin88cube</a></div>
			</td>
			<td class="nobr center">2.29 <span>GB</span></td>
			<td class="center">142</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">309</td>
			<td class="red lasttd center">213</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10908559,0" class="icomment icommentjs icon16" href="/sting-and-the-police-the-very-best-of-2002-flac-soup-t10908559.html#comment"> <em class="iconvalue">10</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/sting-and-the-police-the-very-best-of-2002-flac-soup-t10908559.html" class="cellMainLink">Sting and The Police - The Very Best Of (2002) FLAC Soup</a></div>
			</td>
			<td class="nobr center">493.07 <span>MB</span></td>
			<td class="center">29</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">155</td>
			<td class="red lasttd center">38</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10912252,0" class="icomment icommentjs icon16" href="/iron-maiden-1980-2010-discography-flac-24-bit-t10912252.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/iron-maiden-1980-2010-discography-flac-24-bit-t10912252.html" class="cellMainLink">[Iron Maiden] [1980-2010] Discography [FLAC 24-bit]</a></div>
			</td>
			<td class="nobr center">13.21 <span>GB</span></td>
			<td class="center">156</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">56</td>
			<td class="red lasttd center">122</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10907149,0" class="icomment icommentjs icon16" href="/fleetwood-mac-fleetwood-mac-2011-hdtracks-24bit-96-flac-beolab1700-t10907149.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/fleetwood-mac-fleetwood-mac-2011-hdtracks-24bit-96-flac-beolab1700-t10907149.html" class="cellMainLink">Fleetwood Mac - Fleetwood Mac - (2011) [HDTracks] 24bit 96 FLAC Beolab1700</a></div>
			</td>
			<td class="nobr center">902.07 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">114</td>
			<td class="red lasttd center">30</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10922065,0" class="icomment icommentjs icon16" href="/blood-sweat-tears-1970-blood-sweat-tears-3-2003-mfsl-sacd-24-88-flac-t10922065.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/blood-sweat-tears-1970-blood-sweat-tears-3-2003-mfsl-sacd-24-88-flac-t10922065.html" class="cellMainLink">Blood, Sweat &amp; Tears 1970 Blood, Sweat &amp; Tears 3 (2003 MFSL SACD 24-88) [FLAC]</a></div>
			</td>
			<td class="nobr center">855.73 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center">17&nbsp;hours</td>
			<td class="green center">117</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10917742,0" class="icomment icommentjs icon16" href="/canned-heat-with-john-lee-hooker-carnegie-hall-1971-2015-flac-t10917742.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/canned-heat-with-john-lee-hooker-carnegie-hall-1971-2015-flac-t10917742.html" class="cellMainLink">Canned Heat with John Lee Hooker - Carnegie Hall 1971 (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center">339.59 <span>MB</span></td>
			<td class="center">27</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">121</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/mozart-piano-concertos-brendel-t10921248.html" class="cellMainLink">Mozart - Piano Concertos - Brendel</a></div>
			</td>
			<td class="nobr center">3.18 <span>GB</span></td>
			<td class="center">112</td>
			<td class="center">21&nbsp;hours</td>
			<td class="green center">30</td>
			<td class="red lasttd center">108</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10917093,0" class="icomment icommentjs icon16" href="/bill-withers-live-at-carnegie-hall-2014-mfsl-sacd-24bit-flac-beolab1700-t10917093.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bill-withers-live-at-carnegie-hall-2014-mfsl-sacd-24bit-flac-beolab1700-t10917093.html" class="cellMainLink">Bill Withers - Live at Carnegie Hall (2014) MFSL SACD 24bit FLAC Beolab1700</a></div>
			</td>
			<td class="nobr center">1.52 <span>GB</span></td>
			<td class="center">32</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">96</td>
			<td class="red lasttd center">41</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10909228,0" class="icomment icommentjs icon16" href="/stevie-ray-vaughan-and-double-trouble-couldn-t-stand-the-weather-2013-hdtracks-24bit-176-beolab1700-t10909228.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/stevie-ray-vaughan-and-double-trouble-couldn-t-stand-the-weather-2013-hdtracks-24bit-176-beolab1700-t10909228.html" class="cellMainLink">Stevie Ray Vaughan and Double Trouble - Couldn&#039;t Stand the Weather (2013)[HDtracks] 24Bit 176 Beolab1700</a></div>
			</td>
			<td class="nobr center">1.64 <span>GB</span></td>
			<td class="center">14</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">69</td>
			<td class="red lasttd center">48</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10915681,0" class="icomment icommentjs icon16" href="/bon-jovi-the-bon-jovi-remasters-flac-mp3-big-papi-80-s-rock-t10915681.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bon-jovi-the-bon-jovi-remasters-flac-mp3-big-papi-80-s-rock-t10915681.html" class="cellMainLink">Bon Jovi - The Bon Jovi Remasters [FLAC+MP3](Big Papi) 80&#039;s Rock</a></div>
			</td>
			<td class="nobr center">357.96 <span>MB</span></td>
			<td class="center">24</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">65</td>
			<td class="red lasttd center">28</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10908091,0" class="icomment icommentjs icon16" href="/the-rides-stephen-stills-can-t-get-enough-vinyl-yeraycito-master-series-t10908091.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-rides-stephen-stills-can-t-get-enough-vinyl-yeraycito-master-series-t10908091.html" class="cellMainLink">The Rides (Stephen Stills) - Can&#039;t Get Enough (VINYL) YERAYCITO MASTER SERIES</a></div>
			</td>
			<td class="nobr center">1.02 <span>GB</span></td>
			<td class="center">13</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">50</td>
			<td class="red lasttd center">37</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/calexico-spiritoso-2013-flac-t10922687.html" class="cellMainLink">Calexico - Spiritoso (2013) [FLAC]</a></div>
			</td>
			<td class="nobr center">349.14 <span>MB</span></td>
			<td class="center">23</td>
			<td class="center">13&nbsp;hours</td>
			<td class="green center">46</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10919270,0" class="icomment icommentjs icon16" href="/rod-stewart-picture-in-a-frame-the-best-ballads-2015-flac-t10919270.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/rod-stewart-picture-in-a-frame-the-best-ballads-2015-flac-t10919270.html" class="cellMainLink">Rod Stewart - Picture In A Frame (The Best Ballads) (2015) FLAC</a></div>
			</td>
			<td class="nobr center">454.27 <span>MB</span></td>
			<td class="center">24</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">43</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/roger-waters-amused-to-death-1992-flac-vtwin88cube-t10920483.html" class="cellMainLink">Roger Waters - Amused To Death (1992) {FLAC} vtwin88cube</a></div>
			</td>
			<td class="nobr center">370.6 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center">23&nbsp;hours</td>
			<td class="green center">26</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10919783,0" class="icomment icommentjs icon16" href="/va-q-music-top-500-van-de-90s-2015-flac-t10919783.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-q-music-top-500-van-de-90s-2015-flac-t10919783.html" class="cellMainLink">VA - Q-Music - Top 500 van de 90s (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center">4.28 <span>GB</span></td>
			<td class="center">117</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">10</td>
			<td class="red lasttd center">17</td>
        </tr>
			</table>


		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
                
        <div id="_119b0a17fab5493361a252d04bf527db"></div>
    
                
    	    <div class="advertising">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <div id="_7063408f1c01d50e0dc2d833186ce962"></div>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/hello-guise/?unread=16660911">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Hello guise
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/phaed/">phaed</a></span></span> 57&nbsp;sec.&nbsp;ago</span>
	</li>
		<li>
		<a href="/community/show/kat-s-problems/?unread=16660908">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				KAT&#039;s Problems
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_verified"><a class="plain" href="/user/KayOs/">KayOs</a></span></span> 2&nbsp;min.&nbsp;ago</span>
	</li>
		<li>
		<a href="/community/show/dedicate-song-thread/?unread=16660907">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Dedicate A Song Thread
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_verified"><a class="plain" href="/user/andyt1000/">andyt1000</a></span></span> 3&nbsp;min.&nbsp;ago</span>
	</li>
		<li>
		<a href="/community/show/please-request-ebooks-and-audio-books-here-v10/?unread=16660906">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Please request ebooks and audio books here. V10
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/akoakoako/">akoakoako</a></span></span> 3&nbsp;min.&nbsp;ago</span>
	</li>
		<li>
		<a href="/community/show/tell-lie-day/?unread=16660905">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Tell A Lie Day!
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/curious-os/">curious-os</a></span></span> 4&nbsp;min.&nbsp;ago</span>
	</li>
		<li>
		<a href="/community/show/what-movie-are-you-watching-right-now/?unread=16660901">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What Movie are you watching right now?
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/curious-os/">curious-os</a></span></span> 5&nbsp;min.&nbsp;ago</span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/kickasstorrents-is-moving-to-kat-cr-domain/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				KickassTorrents is moving to kat.cr domain
			</p>
		</a>
		<span class="explanation">by KickassTorrents 2&nbsp;months&nbsp;ago</span>
	</li>
	<li>
		<a href="/blog/post/happy-torrents-day-2015/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Happy Torrents Day 2015!
			</p>
		</a>
		<span class="explanation">by KickassTorrents 3&nbsp;months&nbsp;ago</span>
	</li>
	<li>
		<a href="/blog/post/technical-maintenance-march-17/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Technical maintenance (March, 17)
			</p>
		</a>
		<span class="explanation">by KickassTorrents 3&nbsp;months&nbsp;ago</span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/steelballz/post/what-i-think-by-balcommon-a-hosted-blog/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> What I Think - by BalCommon - a hosted blog</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/steelballz/">steelballz</a> 2&nbsp;hours&nbsp;ago</span></li>
	<li><a href="/blog/Vampire_rox./post/it-was-a-cold-night-of-1915/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> It was a cold night of 1915</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/Vampire_rox./">Vampire_rox.</a> 7&nbsp;hours&nbsp;ago</span></li>
	<li><a href="/blog/miok2cup/post/useless-facts-all-or-none-of-which-may-be-true/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> Useless Facts (All or none of which may be true)</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/miok2cup/">miok2cup</a> 8&nbsp;hours&nbsp;ago</span></li>
	<li><a href="/blog/Touro1973/post/new-movie-releases-and-blu-ray-releases-week-29-july-13-july-19/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> New Movie releases and Blu-ray releases Week 29 (July 13 - July 19)</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/Touro1973/">Touro1973</a> 9&nbsp;hours&nbsp;ago</span></li>
	<li><a href="/blog/DickCheny/post/kat-cr-notification-from-google-the-site-ahead-contains-harmful-programs/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> Kat.cr notification from Google &quot;The Site Ahead Contains Harmful Programs!!!&quot;</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/DickCheny/">DickCheny</a> 11&nbsp;hours&nbsp;ago</span></li>
	<li><a href="/blog/Dotexone/post/my-pearls-of-wisdom-now-with-more-pussycats/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> My Pearls of Wisdom (now with more pussycats)</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/Dotexone/">Dotexone</a> 16&nbsp;hours&nbsp;ago</span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/10musume/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				10musume
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/true%20detective/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				true detective
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/70s%2Bdisco%2B/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				70s+disco+
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/calista%20carmichael/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				calista carmichael
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/grey%20s06/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				grey s06
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/r.kelly%20jay%20z%20best%20both%20worlds/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				r.kelly jay z best both worlds
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/big%20cyn/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				big cyn
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/punjabi%20song%20by%20satinder%20sartaj/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				punjabi song by satinder sartaj
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/ac9/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				ac9
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/superfast.2015/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				superfast.2015
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/anatomy%20of/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				anatomy of
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		<li>
			<a data-nop href="http://www.torrentreactor.net/" target="_blank" rel="external">
				<span class="itorreact thirdPartIcons"></span>TorrentReactor
			</a>
		</li>
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
		<li><a href="/welcome/">welcome</a></li>
		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <div id="_673e31f53f8166159b8e996c4124765b"></div>
        <div id="_e7050fb15fd39b3e4e99a5be4a57b6ea"></div>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script><script>
 sc('addGlobal', 'pagetype', 'front');
 sc('addSlot', '_60318cd4e8d28f6fb76fe34e9bd9c498');
sc('addSlot', '_39ecb76dd457e5ac33776fdf11500d56');
sc('addSlot', '_277923e5f9d753c5b0630c28e641790c');
sc('addSlot', '_119b0a17fab5493361a252d04bf527db');
sc('addSlot', '_7063408f1c01d50e0dc2d833186ce962', { 'searchQuery': '' });
sc('addSlot', '_673e31f53f8166159b8e996c4124765b');
sc('addSlot', '_e7050fb15fd39b3e4e99a5be4a57b6ea');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>
</body>
</html>
