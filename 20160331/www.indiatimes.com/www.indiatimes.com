<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trending  stories on Indian Lifestyle, Culture, Relationships, Food, Travel, Entertainment, News & New Technology News - Indiatimes.com</title>

<meta name="keywords" content="" />
<meta name="description" content="Indiatimes.com brings you the news, articles, stories and videos on entertainment, latest lifestyle, culture & new technologies emerging worldwide." />



		

<link rel="image_src" href="http://media.indiatimes.in/resources/images/fbimage.png" />
<meta property="og:title" content="" />    
<meta property="og:url" content="http://www.indiatimes.com" />
<meta property="og:image" content="http://media.indiatimes.in/resources/images/fbimage.png" />

<meta property="og:site_name" content="indiatimes.com" />
<meta property="fb:app_id" content="117787264903013" />
<meta property="og:description" content="" />
<meta name="google-site-verification" content="ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw" />
<meta name="msvalidate.01" content="BAB71AAED7CCC01DB106A58E99625EB4" />
<link href="https://plus.google.com/+indiatimes/posts" rel="publisher" />

<meta property="og:type" content="website" />
    
<!--<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />-->

<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">    <link rel="shortcut icon" href="http://media.indiatimes.in/resources/images/favicon.ico?v=25" type="image/x-icon"><link rel="stylesheet" href="http://www.indiatimes.com/fonts/fonts.min.css?v=118.1" media="screen"><link rel="stylesheet" href="http://media.indiatimes.in/resources/css/site.css?v=118.1" media="screen"><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery-1.11.1.min.js?v=118.1"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.inview.min.js?v=118.1"></script><script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script><!-- Facebook Pixel Code --><script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');fbq('init', '853339751421540');fbq('track', 'PageView');fbq("track","ViewContent");</script><noscript><img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=853339751421540&ev=PageView&noscript=1"/></noscript><!-- End Facebook Pixel Code -->           <!--  Analytics Code Begin -->    <!-- Begin comScore Tag -->    <script>        var _comscore = _comscore || [];        _comscore.push({ c1: "2", c2: "6036484" });        (function() {            var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";            el.parentNode.insertBefore(s, el);        })();    </script>    <noscript>    <img src="http://b.scorecardresearch.com/p?c1=2&c2=6036484&cv=2.0&cj=1" />    </noscript>    <!-- End comScore Tag -->    <!-- Begin Google Analytics Tag -->    <script>        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');        ga('create', 'UA-198011-6', 'indiatimes.com');            ga('require', 'displayfeatures');        ga('send', 'pageview');    </script>    <!-- End Google Analytics Tag -->    <!--  Analytics Code Begin --><!--adcode common script--><script type='text/javascript' src="http://ad.crwdcntrl.net/5/c=2800/pe=y/var=_ccaud"></script><script type="text/javascript">    var googletag = googletag || {};    googletag.cmd = googletag.cmd || [];    (function() {        var gads = document.createElement("script");        gads.async = true;        gads.type = "text/javascript";        var useSSL = "https:" == document.location.protocol;        gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";        var node =document.getElementsByTagName("script")[0];        node.parentNode.insertBefore(gads, node);    })();</script> <script type="text/javascript">        var nid='';        var cid='';        var sid='';        var brand_name='';                                                             </script>    <script>
    var contType = "";
    </script>
        <script type="text/javascript" src="http://media.indiatimes.in/resources/js/banner/home.js?v=118.1"></script>
            
<!--[if lt IE 9]>
  <script type="text/javascript" src="http://media.indiatimes.in/js/html5.js?v1.1"></script>
  <![endif]-->
<script>
var isDetailPage = 0;
var isNewYear = 1;
</script>
</head>
<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];   
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=117787264903013&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script> 	
	
<div id="wrap" class="cf"><!--wrap start-->
    <div class='darkDisabledBg' id='disabledBg'></div>
<header>
    <div class="header-inner">
        <div class="comm-header ">
            <nav>
                <div class='forTop'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>

                    <div class="lnks">
                                <a href="http://www.indiatimes.com/news/"  onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'News');">News</a> 
                                                               <a href="http://www.indiatimes.com/lifestyle/"  onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'Lifestyle');">Lifestyle</a> 
                                                               <a href="http://www.indiatimes.com/entertainment/"  onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'Entertainment');">Entertainment</a> 
                                                               <a href="http://www.indiatimes.com/health/"  onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'Health');">Health</a> 
                                                               <a href="http://www.indiatimes.com/videocafe/"  onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'Videos');">Videos</a> 
                                                               <a href="http://www.indiatimes.com/play/"  onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'Slog Overs');">Slog Overs</a> 
                                                                                          <a href='http://www.indiatimes.com/trending'  class="#fffff" onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'Trending');">Trending</a> 
                                                </div>
                </div>
                    <div class='onScrolled'> 
                        <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>
                        <a href="javascript:;" class="menu rc sprite" id="headerMenu">Menu</a>
                        <div class="has-tag">
                                                    </div>
                    </div>

            
            </nav>
            
            <div id="sticker" style="display:none;"><span class="readTitle">
                                    </span></div>
                <div class="socls fr share">
                </div>


            <div class="social fr">
                <a title="facebook" target="_blank"  class="sprite fb" href="https://www.facebook.com/indiatimes">facebook</a>
                <a title="twitter" target="_blank"  class="sprite twt" href="https://twitter.com/indiatimes">twitter</a>
                <a title="search"  class="sprite serach" onclick="$('body').showSearch();" href="javascript:void(0);">search</a>
            </div>
        </div>
    </div>
</header>
<div class="clr"></div>
<div id="overlay" class="animated bounceOut"><!--overlay start-->
    <div class="search_conts"><!--search-cont start-->
        <form onsubmit="return false;" class="serach-form">
            <input type="text" onkeyup="search_data();" class="input" onfocus="if (this.value == 'Search') {
                        this.value = ''
                    }" onblur="if (this.value == '') {
                                this.value = 'Search'
                            }
                            ;" value="Search" name="q" id="q" autocomplete="off">
            <input type="button" class="sprite src-btn" onclick="search_data();">
            <a class="sprite sclose" id="close" href="javascript:void(0);">X</a>
        </form>
        <div class="scroll-panes" id="scroll-panes">
            <div class="clr"></div>
            <div class="res-msg" id="res-msg"></div>
            <div class="search-list cf" id="results"></div>
            <div class="clr"></div>
            <div class="loadmore" id="progressBar" style="display: none;">Loading......</div>
            <a class="loadmore" style="display:none;" onclick="load_search_data();" id="load_more_button" href="javascript:void(0);">Load more</a>
        </div>
    </div>
</div>
<div class="blk">
    <div  id="leftMenu" class="leftMenu accordion inner">
        <dl >
            <dt data-color="red-bg" class=""><a href="http://www.indiatimes.com">Home</a></dt>
            <dd> </dd>
                            <dt  data-color="blue-bg" ><a href="http://www.indiatimes.com/news/">News</a>
					<a href="javascript:void(0)" class="arrow sprite ">
					</a>                </dt>
                                        <dd >
                     
                            <a href='http://www.indiatimes.com/news/india/'>India</a>
                     
                            <a href='http://www.indiatimes.com/news/world/'>World</a>
                     
                            <a href='http://www.indiatimes.com/news/sports/'>Sports</a>
                     
                            <a href='http://www.indiatimes.com/news/weird/'>Weird</a>
                            </dd>
                    <div class="clr"></div>
                    <dt  data-color="purple-bg" ><a href="http://www.indiatimes.com/lifestyle/">Lifestyle</a>
					<a href="javascript:void(0)" class="arrow sprite ">
					</a>                </dt>
                                        <dd >
                     
                            <a href='http://www.indiatimes.com/lifestyle/self/'>Self</a>
                     
                            <a href='http://www.indiatimes.com/lifestyle/style/'>Style</a>
                     
                            <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                     
                            <a href='http://www.indiatimes.com/culture/who-we-are/'>Who we are</a>
                     
                            <a href='http://www.indiatimes.com/culture/travel/'>Travel</a>
                     
                            <a href='http://www.indiatimes.com/culture/food/'>Food</a>
                            </dd>
                    <div class="clr"></div>
                    <dt  data-color="red-bg" ><a href="http://www.indiatimes.com/entertainment/">Entertainment</a>
					<a href="javascript:void(0)" class="arrow sprite ">
					</a>                </dt>
                                        <dd >
                     
                            <a href='http://www.indiatimes.com/entertainment/bollywood/'>Bollywood</a>
                     
                            <a href='http://www.indiatimes.com/entertainment/celebs/'>Celebscoop</a>
                     
                            <a href='http://www.indiatimes.com/entertainment/hollywood/'>Hollywood</a>
                            </dd>
                    <div class="clr"></div>
                    <dt  data-color="green-bg" ><a href="http://www.indiatimes.com/health/">Health</a>
					<a href="javascript:void(0)" class="arrow sprite ">
					</a>                </dt>
                                        <dd >
                     
                            <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                     
                            <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                     
                            <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                     
                            <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                     
                            <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                            </dd>
                    <div class="clr"></div>
                    <dt  data-color="orange-bg" ><a href="http://www.indiatimes.com/videocafe/">Videos</a>
					                </dt>
                                    <dt  data-color="yellow-bg" ><a href="http://www.indiatimes.com/play/">Slog Overs</a>
					                </dt>
                                <dt data-color="" class=""><a href="http://www.indiatimes.com/trending/">Trending</a>
            </dt>
            <dt class="pink-bg">
                <div class="follow">Follow indiatimes </div>
                <div class="follow_cont"> 
                    <a href="https://www.facebook.com/indiatimes" class="fbleft sprite"> facebook</a> 
                    <a href="https://twitter.com/indiatimes" class="twtleft sprite"> twitter</a> 
                </div>
            </dt>

            <dd>&nbsp;</dd>
            <div class="clr"></div>
        </dl>
    </div>
</div>


<div class="clr"></div>
<div class="dummy-cont">&nbsp;</div>
<div class="clr"></div>


<div id="pushdown">
    <div class="pull-ad"><!--pull-ad start-->   
                            <div id="OOP_Inter" style="display: none;">
                        <script type='text/javascript'>
                            googletag.cmd.push(function () {
                                googletag.display('OOP_Inter'); });
                        </script>
                    </div>
                    <div id="PPD" style="display: none;">
                        <script type='text/javascript'>
                            googletag.cmd.push(function () {
                                googletag.display('PPD');
                            });
                        </script>
                    </div>
            
    </div><!--pull-ad end-->
</div>
    <!--testing 16-03-31 23:00:04-->

<section id="hp_block_1" class="container cf" style="border: 0px solid red;"><!--container start-->
    
<div class="big-image">
        	<div class="gradient-b"></div>
		<a href="http://www.indiatimes.com/news/india/after-a-10-year-long-wait-indian-army-to-finally-get-50-000-bullet-proof-vests-252781.html" class="gradient-patten"></a>
            <div class="featureds"><span class="border-left">&nbsp;</span> Featured <span class="border-right">&nbsp;</span></div>
            <div class="bid-card-txt">
                                <a href="http://www.indiatimes.com/news/india/after-a-10-year-long-wait-indian-army-to-finally-get-50-000-bullet-proof-vests-252781.html" class="big-card-small">
                    After A 10 Year Long Wait, Indian Army To Finally Get 50,000 Bullet-Proof Vests                </a>
              <span class="card-line"></span>  
            </div>
            <a href="http://www.indiatimes.com/news/india/after-a-10-year-long-wait-indian-army-to-finally-get-50-000-bullet-proof-vests-252781.html" class="tint"><img src="http://media.indiatimes.in/media/content/2016/Mar/rtx27vns_1459418477_1024x477.jpg"/></a>
</div>
    
        

    <div class="news-panel cf"><!--news-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/news/">news</a></h2>

        <div class="feature-list cf"><!--feature-list start-->	
                            <figure>

                        <a href="http://www.indiatimes.com/news/world/a-300-year-old-gurdwara-in-pakistan-opens-its-doors-for-the-first-time-in-nearly-70-years-252787.html" class=" tint" title="A 300-Year-Old Gurdwara In Pakistan Opens Its Doors For The First Time In Nearly 70 Years">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1459403811_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/card_1459403811_236x111.jpg"  border="0" alt="A 300-Year-Old Gurdwara In Pakistan Opens Its Doors For The First Time In Nearly 70 Years"/></a>
                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/a-300-year-old-gurdwara-in-pakistan-opens-its-doors-for-the-first-time-in-nearly-70-years-252787.html" title="A 300-Year-Old Gurdwara In Pakistan Opens Its Doors For The First Time In Nearly 70 Years">
    A 300-Year-Old Gurdwara In Pakistan Opens Its Doors For The First Time In Nearly 70 Years                    </a>
                </figcaption>
                </div><!--feature-list end-->

                    <div class="news-panel-list cf" id="column1_0"><!--news-panel-list start-->
                <figure>
                        
                        <a href="http://www.indiatimes.com/news/world/2-months-after-he-almost-starved-disowned-witch-boy-is-now-a-healthy-happy-child-252842.html" title="2 Months After He Almost Starved, Disowned 'Witch Boy' Is Now A Healthy Happy Child!" class=" tint">                       
                        <img src="http://media.indiatimes.in/media/content/2016/Mar/kid64_1459432436_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/kid64_1459432436_236x111.jpg" border="0" alt=""/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/2-months-after-he-almost-starved-disowned-witch-boy-is-now-a-healthy-happy-child-252842.html" title="2 Months After He Almost Starved, Disowned 'Witch Boy' Is Now A Healthy Happy Child!">
    2 Months After He Almost Starved, Disowned 'Witch Boy' Is Now A Healthy Happy Child!                    </a>
                </figcaption> 
            </div><!--news-panel-list end-->
                    <div class="news-panel-list cf" id="column1_1"><!--news-panel-list start-->
                <figure>
                        
                        <a href="http://www.indiatimes.com/news/world/russian-oil-tycoon-spends-a-billion-dollars-on-son-s-wedding-gets-sting-jlo-and-enrique-to-perform-together-252789.html" title="Russian Oil Tycoon Spends A Billion Dollars On Son's Wedding, Gets Sting, JLO And Enrique To Perform Together" class=" tint">                       
                        <img src="http://media.indiatimes.in/media/content/2016/Mar/fb_1459408762_1459408774_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/fb_1459408762_1459408774_236x111.jpg" border="0" alt=""/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/russian-oil-tycoon-spends-a-billion-dollars-on-son-s-wedding-gets-sting-jlo-and-enrique-to-perform-together-252789.html" title="Russian Oil Tycoon Spends A Billion Dollars On Son's Wedding, Gets Sting, JLO And Enrique To Perform Together">
    Russian Oil Tycoon Spends A Billion Dollars On Son's Wedding, Gets Sting, JLO And Enrique To Perform Together                    </a>
                </figcaption> 
            </div><!--news-panel-list end-->
                    <div class="news-panel-list cf" id="column1_2"><!--news-panel-list start-->
                <figure>
                        
                        <a href="http://www.indiatimes.com/news/weird/man-tries-yoga-on-a-flight-to-calm-down-is-arrested-for-getting-violent-instead-252824.html" title="Man Tries Yoga On A Flight To Calm Down, Is Arrested For Getting Violent Instead" class=" tint">                       
                        <img src="http://media.indiatimes.in/media/content/2016/Mar/cp4_1459419829_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/cp4_1459419829_236x111.jpg" border="0" alt=""/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/weird/man-tries-yoga-on-a-flight-to-calm-down-is-arrested-for-getting-violent-instead-252824.html" title="Man Tries Yoga On A Flight To Calm Down, Is Arrested For Getting Violent Instead">
    Man Tries Yoga On A Flight To Calm Down, Is Arrested For Getting Violent Instead                    </a>
                </figcaption> 
            </div><!--news-panel-list end-->
            </div><!--news-panel end-->

    <div class="life-panel cf" id="middleBlock1"><!--life-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/lifestyle/">lifestyle</a></h2>

                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/model-carol-gracias-breaks-all-stereotypes-walks-the-ramp-with-her-pregnant-belly-252835.html" class="tint" title="Model Carol Gracias Breaks All Stereotypes, Walks The Ramp With Her Pregnant Belly!">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/12512784_10154059730956823_2430825994770317294_n_1459431857_1459431862_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/12512784_10154059730956823_2430825994770317294_n_1459431857_1459431862_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/model-carol-gracias-breaks-all-stereotypes-walks-the-ramp-with-her-pregnant-belly-252835.html" title="Model Carol Gracias Breaks All Stereotypes, Walks The Ramp With Her Pregnant Belly!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories', 'http://www.indiatimes.com/entertainment/celebs/model-carol-gracias-breaks-all-stereotypes-walks-the-ramp-with-her-pregnant-belly-252835.html');">

    Model Carol Gracias Breaks All Stereotypes, Walks The Ramp With Her Pregnant Belly!                        </a>
                    </div>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

    <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/celebs/this-minion-mashup-of-afghan-jalebi-is-the-cutest-thing-you-ll-watch-today-252839.html'>video</a>                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/this-minion-mashup-of-afghan-jalebi-is-the-cutest-thing-you-ll-watch-today-252839.html" class="tint" title="This Minion Mashup Of 'Afghan Jalebi' Is The Cutest Thing You'll Watch Today!">
                        <img  src="http://media.indiatimes.in/media/videocafe/2016/Mar/card4_1459430029_1459430039_502x234.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2016/Mar/card4_1459430029_1459430039_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/this-minion-mashup-of-afghan-jalebi-is-the-cutest-thing-you-ll-watch-today-252839.html" title="This Minion Mashup Of 'Afghan Jalebi' Is The Cutest Thing You'll Watch Today!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories', 'http://www.indiatimes.com/entertainment/celebs/this-minion-mashup-of-afghan-jalebi-is-the-cutest-thing-you-ll-watch-today-252839.html');">

    This Minion Mashup Of 'Afghan Jalebi' Is The Cutest Thing You'll Watch Today!                        </a>
                    </div>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/despite-virat-kohli-s-tweet-slamming-haters-anushka-sharma-is-in-no-mood-for-a-patch-up-252840.html" class="tint" title="Despite Virat Kohli's Tweet Slamming Haters, Anushka Sharma Is In No Mood For A Patch Up">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/virat-card_1459431025_1459431029_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/virat-card_1459431025_1459431029_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/despite-virat-kohli-s-tweet-slamming-haters-anushka-sharma-is-in-no-mood-for-a-patch-up-252840.html" title="Despite Virat Kohli's Tweet Slamming Haters, Anushka Sharma Is In No Mood For A Patch Up" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories', 'http://www.indiatimes.com/entertainment/celebs/despite-virat-kohli-s-tweet-slamming-haters-anushka-sharma-is-in-no-mood-for-a-patch-up-252840.html');">

    Despite Virat Kohli's Tweet Slamming Haters, Anushka Sharma Is In No Mood For A Patch Up                        </a>
                    </div>
                </figcaption>
            </div>
                    
    </div><!--life-panel end-->

    <div class="trending-panel cf"><!--trending-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/trending/">trending</a></h2>
        
                    
            <div class="trending-panel-list cf" id="column3_0">
                <figure>
                                <a href="http://www.indiatimes.com/culture/who-we-are/these-examples-of-how-things-were-done-just-a-few-decades-ago-prove-technology-has-come-such-a-long-way-252841.html" class="tint" title="These Examples Of How Things Were Done Just A Few Decades Ago Prove Technology Has Come Such A Long Way!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/these-examples-of-how-things-were-done-just-a-few-decades-ago-prove-technology-has-come-such-a-long-way-252841.html');">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1459441553_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/card_1459441553_236x111.jpg" border="0" alt="These Examples Of How Things Were Done Just A Few Decades Ago Prove Technology Has Come Such A Long Way!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container partner-cont">
                         <div class="partner"><a href="javascript:void(0);" title="">PARTNER</a></div>
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/these-examples-of-how-things-were-done-just-a-few-decades-ago-prove-technology-has-come-such-a-long-way-252841.html" title="These Examples Of How Things Were Done Just A Few Decades Ago Prove Technology Has Come Such A Long Way!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/these-examples-of-how-things-were-done-just-a-few-decades-ago-prove-technology-has-come-such-a-long-way-252841.html');">
    These Examples Of How Things Were Done Just A Few Decades Ago Prove Technology Has Come Such A Long Way!                    </a>
                         </div>
                </figcaption>
            </div>        
        
        
                
            <div class="trending-panel-list cf" id="column3_1">
                <figure>
                                <a href="http://www.indiatimes.com/culture/who-we-are/10-telltale-signs-that-you-need-to-spend-more-time-with-your-loved-ones-252721.html" class="tint" title="10 Telltale Signs That You Need To Spend More Time With Your Loved Ones." onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/10-telltale-signs-that-you-need-to-spend-more-time-with-your-loved-ones-252721.html');">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1459428720_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/card_1459428720_236x111.jpg" border="0" alt="10 Telltale Signs That You Need To Spend More Time With Your Loved Ones."/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container partner-cont">
                         <div class="partner"><a href="javascript:void(0);" title="">PARTNER</a></div>
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/10-telltale-signs-that-you-need-to-spend-more-time-with-your-loved-ones-252721.html" title="10 Telltale Signs That You Need To Spend More Time With Your Loved Ones." onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/10-telltale-signs-that-you-need-to-spend-more-time-with-your-loved-ones-252721.html');">
    10 Telltale Signs That You Need To Spend More Time With Your Loved Ones.                    </a>
                         </div>
                </figcaption>
            </div>        
        
        
                
            <div class="trending-panel-list cf" id="column3_2">
                <figure>
            <a class='video-btn sprite' href='http://www.indiatimes.com/lifestyle/self/everyone-needs-a-messiah-and-it-is-no-different-for-your-deodorant-confused-watch-this-video-to-see-what-we-are-talking-about-252727.html'>video</a>                    <a href="http://www.indiatimes.com/lifestyle/self/everyone-needs-a-messiah-and-it-is-no-different-for-your-deodorant-confused-watch-this-video-to-see-what-we-are-talking-about-252727.html" class="tint" title="Everyone Needs A Messiah And It Is No Different For Your Deodorant. Confused? Watch This Video To Unravel The Mystery." onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/self/everyone-needs-a-messiah-and-it-is-no-different-for-your-deodorant-confused-watch-this-video-to-see-what-we-are-talking-about-252727.html');">
                        <img  src="http://media.indiatimes.in/media/videocafe/2016/Mar/card1_1459400416_236x111.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2016/Mar/card1_1459400416_236x111.jpg" border="0" alt="Everyone Needs A Messiah And It Is No Different For Your Deodorant. Confused? Watch This Video To Unravel The Mystery."/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container partner-cont">
                         <div class="partner"><a href="javascript:void(0);" title="">PARTNER</a></div>
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/self/everyone-needs-a-messiah-and-it-is-no-different-for-your-deodorant-confused-watch-this-video-to-see-what-we-are-talking-about-252727.html" title="Everyone Needs A Messiah And It Is No Different For Your Deodorant. Confused? Watch This Video To Unravel The Mystery." onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/self/everyone-needs-a-messiah-and-it-is-no-different-for-your-deodorant-confused-watch-this-video-to-see-what-we-are-talking-about-252727.html');">
    Everyone Needs A Messiah And It Is No Different For Your Deodorant. Confused? Watch This Video To Unravel The Mystery.                    </a>
                         </div>
                </figcaption>
            </div>        
        
        
                
                        <div class="trending-panel-list cf" id="column3_0">
    <span class="strip skyblue-bg"></span>                <figure>
                        <a href="http://www.indiatimes.com/entertainment/celebs/model-carol-gracias-breaks-all-stereotypes-walks-the-ramp-with-her-pregnant-belly-252835.html" class="tint" title="Model Carol Gracias Breaks All Stereotypes, Walks The Ramp With Her Pregnant Belly!">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/12512784_10154059730956823_2430825994770317294_n_1459431857_1459431862_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/12512784_10154059730956823_2430825994770317294_n_1459431857_1459431862_236x111.jpg" border="0" alt="Model Carol Gracias Breaks All Stereotypes, Walks The Ramp With Her Pregnant Belly!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/model-carol-gracias-breaks-all-stereotypes-walks-the-ramp-with-her-pregnant-belly-252835.html" title="Model Carol Gracias Breaks All Stereotypes, Walks The Ramp With Her Pregnant Belly! " onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/self/everyone-needs-a-messiah-and-it-is-no-different-for-your-deodorant-confused-watch-this-video-to-see-what-we-are-talking-about-252727.html');">
    Model Carol Gracias Breaks All Stereotypes, Walks The Ramp With Her Pregnant Belly!                    </a>
                   </div>     
                </figcaption>
            </div>
        </div><!--trending-panel end-->
</section><!--container end-->

<!-- -------------------    end Block 1  ------------------------------------- -->
<section class="big-ads" id="ad1"> 
    <div id="bigAd1_slot"></div>
    <script>
        showBigAD1('bigAd1_slot');
    </script> 
</section>
<!-- -------------------    start Block 2  ------------------------------------- -->

<section id="hp_block_2" class="container cf" style="border: 0px solid red;"><!--container start-->
    <div class="news-panel cf"><!--news-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/news/">news</a></h2>
                    <div class="news-panel-list cf " ><!--news-panel-list start-->
                <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/news/india/caught-on-camera-under-construction-flyover-collapsing-in-kolkata-s-crowded-bara-bazaar-252829.html'>video</a>                    <a href="http://www.indiatimes.com/news/india/caught-on-camera-under-construction-flyover-collapsing-in-kolkata-s-crowded-bara-bazaar-252829.html" title="Caught On Camera: Under Construction Flyover Collapsing In Kolkata's Crowded Bara Bazaar" class=" tint">


                        <img  src="http://media.indiatimes.in/media/videocafe/2016/Mar/kolkataflyover_card_1459425326_236x111.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Mar/kolkataflyover_card_1459425326_236x111.jpg" border="0" alt=""/>
                    </a>
                </figure>
                <figcaption>

                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/caught-on-camera-under-construction-flyover-collapsing-in-kolkata-s-crowded-bara-bazaar-252829.html" title="Caught On Camera: Under Construction Flyover Collapsing In Kolkata's Crowded Bara Bazaar">
    Caught On Camera: Under Construction Flyover Collapsing In Kolkata's Crowded Bara Bazaar                    </a>
                </figcaption> 
            </div>
                        <div class="news-panel-list cf colombia" id="div-clmb-ctn-129061-4"  data-slot="129061" data-position="4" data-section="0" data-cb="adwidgetNew"><!--news-panel-list start-->
                <figure>

                                            <a href="http://www.indiatimes.com/news/india/100-percent-fdi-in-online-retail-could-mean-the-end-of-discounts-on-amazon-flipkart-and-snapdeal-252830.html" title="100 Percent FDI In Online Retail Could Mean The End Of Discounts On Amazon, Flipkart And Snapdeal" class=" tint">


                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/snapdeal-amazon-flipkart-640_1459425492_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/snapdeal-amazon-flipkart-640_1459425492_236x111.jpg" border="0" alt=""/>
                    </a>
                </figure>
                <figcaption>

                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/100-percent-fdi-in-online-retail-could-mean-the-end-of-discounts-on-amazon-flipkart-and-snapdeal-252830.html" title="100 Percent FDI In Online Retail Could Mean The End Of Discounts On Amazon, Flipkart And Snapdeal">
    100 Percent FDI In Online Retail Could Mean The End Of Discounts On Amazon, Flipkart And Snapdeal                    </a>
                </figcaption> 
            </div>
                        <div class="news-panel-list cf " ><!--news-panel-list start-->
                <figure>

                                            <a href="http://www.indiatimes.com/news/weird/this-creepy-selfie-is-making-the-internet-lose-it-and-it-will-send-shivers-down-your-spine-252815.html" title="This Creepy Selfie Is Making The Internet Lose It And It Will Send Shivers Down Your Spine" class=" tint">


                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/cp3_1459414759_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cp3_1459414759_236x111.jpg" border="0" alt=""/>
                    </a>
                </figure>
                <figcaption>

                    <a class="bold-txt" href="http://www.indiatimes.com/news/weird/this-creepy-selfie-is-making-the-internet-lose-it-and-it-will-send-shivers-down-your-spine-252815.html" title="This Creepy Selfie Is Making The Internet Lose It And It Will Send Shivers Down Your Spine">
    This Creepy Selfie Is Making The Internet Lose It And It Will Send Shivers Down Your Spine                    </a>
                </figcaption> 
            </div>
                        <div class="news-panel-list cf " ><!--news-panel-list start-->
                <figure>

                                            <a href="http://www.indiatimes.com/news/world/oddeven-pioneer-mexico-has-banned-all-cars-after-heavy-pollution-252825.html" title="#OddEven Pioneer Mexico Has Banned All Cars For A Day Each Week After Heavy Pollution" class=" tint">


                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/6921496914_276d411c27_z-640_1459420763_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/6921496914_276d411c27_z-640_1459420763_236x111.jpg" border="0" alt=""/>
                    </a>
                </figure>
                <figcaption>

                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/oddeven-pioneer-mexico-has-banned-all-cars-after-heavy-pollution-252825.html" title="#OddEven Pioneer Mexico Has Banned All Cars For A Day Each Week After Heavy Pollution">
    #OddEven Pioneer Mexico Has Banned All Cars For A Day Each Week After Heavy Pollution                    </a>
                </figcaption> 
            </div>
                </div><!--news-panel end-->

    <div class="life-panel cf"><!--life-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/lifestyle/">lifestyle</a></h2>
                        
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/play/slog-overs/virat-kohli-once-chose-to-save-his-team-from-defeat-over-attending-his-father-s-funeral-what-a-player-252837.html" class="tint" title="Virat Kohli Once Chose To Save His Team From Defeat Over Attending His Father's Funeral. What A Player!">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/father640_1459429500_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/father640_1459429500_502x234.jpg" border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/virat-kohli-once-chose-to-save-his-team-from-defeat-over-attending-his-father-s-funeral-what-a-player-252837.html" title="Virat Kohli Once Chose To Save His Team From Defeat Over Attending His Father's Funeral. What A Player!">
    Virat Kohli Once Chose To Save His Team From Defeat Over Attending His Father's Funeral. What A Player!                        </a>
                    </div>
                </figcaption>
            </div>
                    
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/of-course-indian-women-watch-porn-and-they-prefer-girl-on-girl-action-according-to-this-map-252828.html" class="tint" title="Of Course Indian Women Watch Porn And They Prefer Girl-On-Girl Action According To This Map">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage2_1459423365_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage2_1459423365_502x234.jpg" border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/of-course-indian-women-watch-porn-and-they-prefer-girl-on-girl-action-according-to-this-map-252828.html" title="Of Course Indian Women Watch Porn And They Prefer Girl-On-Girl Action According To This Map">
    Of Course Indian Women Watch Porn And They Prefer Girl-On-Girl Action According To This Map                        </a>
                    </div>
                </figcaption>
            </div>
                    
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/adele-invited-her-doppelganger-on-the-stage-they-clicked-the-selfie-of-a-lifetime-252827.html" class="tint" title="Adele Invited Her DoppelgÃ¤nger On The Stage & They Clicked The Selfie Of A Lifetime!">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/adele4_1459422456_1459422462_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/adele4_1459422456_1459422462_502x234.jpg" border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/adele-invited-her-doppelganger-on-the-stage-they-clicked-the-selfie-of-a-lifetime-252827.html" title="Adele Invited Her DoppelgÃ¤nger On The Stage & They Clicked The Selfie Of A Lifetime!">
    Adele Invited Her DoppelgÃ¤nger On The Stage & They Clicked The Selfie Of A Lifetime!                        </a>
                    </div>
                </figcaption>
            </div>
                </div><!--life-panel end-->

    <div class="trending-panel cf"><!--trending-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/trending/">trending</a></h2>
        
            <div class="trending-panel-list cf "  ><!--trending-panel-list start-->
                                        <figure>

    
                    <a href="http://www.indiatimes.com/play/slog-overs/ravi-shastri-s-warning-to-indian-batsmen-ahead-of-semis-virat-kohli-can-t-always-bail-you-out-252788.html" class="tint" title="Ravi Shastri's Warning To Indian Batsmen Ahead Of Semis - Virat Kohli Can't Always Bail You Out">

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/rv640_1459404058_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/rv640_1459404058_236x111.jpg" border="0" alt="Ravi Shastri's Warning To Indian Batsmen Ahead Of Semis - Virat Kohli Can't Always Bail You Out"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/ravi-shastri-s-warning-to-indian-batsmen-ahead-of-semis-virat-kohli-can-t-always-bail-you-out-252788.html" title="Ravi Shastri's Warning To Indian Batsmen Ahead Of Semis - Virat Kohli Can't Always Bail You Out">
            Ravi Shastri's Warning To Indian Batsmen Ahead Of Semis - Virat Kohli Can't Always Bail You Out                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
    
            <div class="trending-panel-list cf colombia"  id="div-clmb-ctn-129061-2"  data-slot="129061" data-position="2" data-section="0" data-cb="adwidgetNew"><!--trending-panel-list start-->
                                        <figure>

    
                    <a href="http://www.indiatimes.com/health/buzz/india-is-suffering-at-36-we-have-the-highest-rate-of-depression-in-the-world-252755.html" class="tint" title="India Is Suffering. At 36%, We Have The Highest Rate Of Depression In The World!">

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/cover_1459331916_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cover_1459331916_236x111.jpg" border="0" alt="India Is Suffering. At 36%, We Have The Highest Rate Of Depression In The World!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/buzz/india-is-suffering-at-36-we-have-the-highest-rate-of-depression-in-the-world-252755.html" title="India Is Suffering. At 36%, We Have The Highest Rate Of Depression In The World!">
            India Is Suffering. At 36%, We Have The Highest Rate Of Depression In The World!                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
    
            <div class="trending-panel-list cf "  ><!--trending-panel-list start-->
                                        <figure>

    
                    <a href="http://www.indiatimes.com/play/slog-overs/andrew-flintoff-has-nothing-better-to-do-trolls-mate-big-b-asks-for-tickets-to-t20-finals-252780.html" class="tint" title="Flintoff Has Nothing Better To Do, Trolls 'Mate' Big B Again Asking For Tickets To T20 Finals!">

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/andrewbachchan640_1459398795_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/andrewbachchan640_1459398795_236x111.jpg" border="0" alt="Flintoff Has Nothing Better To Do, Trolls 'Mate' Big B Again Asking For Tickets To T20 Finals!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/andrew-flintoff-has-nothing-better-to-do-trolls-mate-big-b-asks-for-tickets-to-t20-finals-252780.html" title="Flintoff Has Nothing Better To Do, Trolls 'Mate' Big B Again Asking For Tickets To T20 Finals!">
            Flintoff Has Nothing Better To Do, Trolls 'Mate' Big B Again Asking For Tickets To T20 Finals!                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
    
            <div class="trending-panel-list cf "  ><!--trending-panel-list start-->
                                        <figure>

    
                    <a href="http://www.indiatimes.com/play/slog-overs/harbhajan-singh-gives-a-fitting-reply-to-a-guy-who-trolled-him-for-being-a-passenger-in-the-indian-cricket-team-252814.html" class="tint" title="Harbhajan Singh Gives A Fitting Reply To A Guy Who Trolled Him For Being A Passenger In The Indian Cricket Team">

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/harbhajanmusclesafp_1459414292_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/harbhajanmusclesafp_1459414292_236x111.jpg" border="0" alt="Harbhajan Singh Gives A Fitting Reply To A Guy Who Trolled Him For Being A Passenger In The Indian Cricket Team"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/harbhajan-singh-gives-a-fitting-reply-to-a-guy-who-trolled-him-for-being-a-passenger-in-the-indian-cricket-team-252814.html" title="Harbhajan Singh Gives A Fitting Reply To A Guy Who Trolled Him For Being A Passenger In The Indian Cricket Team">
            Harbhajan Singh Gives A Fitting Reply To A Guy Who Trolled Him For Being A Passenger In The Indian Cricket Team                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
        </div>

</section><!--container end-->

<!--------------------------- end Block2 ------------------------------------------>
<section class="big-ads" id="ad2"> 
    <div id="bigAd2_slot"></div>
</section>
<!--------------------------- start Block3 ------------------------------------------>

<section id="hp_block_3" class="container cf"><!--container start-->

    <div class="news-panel cf"><!--news-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/news/">news</a></h2>

                    <div class="news-panel-list cf " >

                <figure>

                    <a href="http://www.indiatimes.com/news/india/netaji-subhas-chandra-bose-made-three-broadcasts-after-the-fateful-plane-crash-reveals-declassified-documents-252823.html" title="Netaji Subhas Chandra Bose Made Three 'Broadcasts' After The Fateful Plane Crash, Reveals Declassified Documents" class=" tint">
            

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1459419681_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1459419681_236x111.jpg" border="0" alt="Netaji Subhas Chandra Bose Made Three 'Broadcasts' After The Fateful Plane Crash, Reveals Declassified Documents"/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/netaji-subhas-chandra-bose-made-three-broadcasts-after-the-fateful-plane-crash-reveals-declassified-documents-252823.html" title="Netaji Subhas Chandra Bose Made Three 'Broadcasts' After The Fateful Plane Crash, Reveals Declassified Documents">
            Netaji Subhas Chandra Bose Made Three 'Broadcasts' After The Fateful Plane Crash, Reveals Declassified Documents                    </a>
                </figcaption> 
            </div>
                <div class="news-panel-list cf colombia" id="div-clmb-ctn-129061-8"  data-slot="129061" data-position="8" data-section="0" data-cb="adwidgetNew">

                <figure>

                    <a href="http://www.indiatimes.com/news/sports/mary-kom-can-still-make-it-to-rio-despite-losing-in-the-asian-olympics-qualification-semis-252818.html" title="Mary Kom Can Still Make It To Rio, Despite Losing In The Asian Olympics Qualification Semis" class=" tint">
            

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/marykom640_1459418035_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/marykom640_1459418035_236x111.jpg" border="0" alt="Mary Kom Can Still Make It To Rio, Despite Losing In The Asian Olympics Qualification Semis"/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/sports/mary-kom-can-still-make-it-to-rio-despite-losing-in-the-asian-olympics-qualification-semis-252818.html" title="Mary Kom Can Still Make It To Rio, Despite Losing In The Asian Olympics Qualification Semis">
            Mary Kom Can Still Make It To Rio, Despite Losing In The Asian Olympics Qualification Semis                    </a>
                </figcaption> 
            </div>
                <div class="news-panel-list cf " >

                <figure>

                    <a href="http://www.indiatimes.com/news/world/apple-still-can-t-figure-out-how-fbi-hacked-the-san-bernardino-attacker-s-iphone-252813.html" title="Apple Still Can't Figure Out How FBI Hacked The San Bernardino Attacker's iPhone!" class=" tint">
            

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/afp-640_1459417799_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/afp-640_1459417799_236x111.jpg" border="0" alt="Apple Still Can't Figure Out How FBI Hacked The San Bernardino Attacker's iPhone!"/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/apple-still-can-t-figure-out-how-fbi-hacked-the-san-bernardino-attacker-s-iphone-252813.html" title="Apple Still Can't Figure Out How FBI Hacked The San Bernardino Attacker's iPhone!">
            Apple Still Can't Figure Out How FBI Hacked The San Bernardino Attacker's iPhone!                    </a>
                </figcaption> 
            </div>
                <div class="news-panel-list cf " >

                <figure>

                    <a href="http://www.indiatimes.com/news/sports/which-cricket-player-are-you-234343.html" title="Which Cricket Player Are You?" class=" tint">
            

                        <img  src="http://media.indiatimes.in/media/quiz/2015/Jul/cricket-card_1435915048_236x111.jpg" data-original="http://media.indiatimes.in/media/quiz/2015/Jul/cricket-card_1435915048_236x111.jpg" border="0" alt="Which Cricket Player Are You?"/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/sports/which-cricket-player-are-you-234343.html" title="Which Cricket Player Are You?">
            Which Cricket Player Are You?                    </a>
                </figcaption> 
            </div>
        </div><!--news-panel end-->

    <div class="life-panel cf"><!--life-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/lifestyle/">lifestyle</a></h2>
         
            <div class="life-panel-list cf">
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/hollywood/amidst-reports-of-kim-kardashian-leaking-her-own-sex-tape-the-lady-posts-another-nude-selfie-252817.html" class="tint" title="Amidst Reports Of Kim Kardashian Leaking Her Own Sex Tape, The Lady Posts Another Nude Selfie!">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/kim-kardashian-has-fixed-that-viral-instagram-post-that-the-fda-demanded-she-take-down_1459417452_1459417456_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/kim-kardashian-has-fixed-that-viral-instagram-post-that-the-fda-demanded-she-take-down_1459417452_1459417456_502x234.jpg" border="0" alt="http://www.indiatimes.com/entertainment/hollywood/amidst-reports-of-kim-kardashian-leaking-her-own-sex-tape-the-lady-posts-another-nude-selfie-252817.html" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/entertainment/hollywood/amidst-reports-of-kim-kardashian-leaking-her-own-sex-tape-the-lady-posts-another-nude-selfie-252817.html" title="Amidst Reports Of Kim Kardashian Leaking Her Own Sex Tape, The Lady Posts Another Nude Selfie!">
            Amidst Reports Of Kim Kardashian Leaking Her Own Sex Tape, The Lady Posts Another Nude Selfie!                        </a>
                    </div>
                </figcaption>
            </div>
                     
            <div class="life-panel-list cf">
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/indian-origin-scientists-believe-rotten-tomatoes-could-be-a-source-of-generating-electricity-252450.html" class="tint" title="Indian-Origin Scientists Believe Rotten Tomatoes Could Be A Source Of Generating Electricity">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458719574_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1458719574_502x234.jpg" border="0" alt="http://www.indiatimes.com/culture/who-we-are/indian-origin-scientists-believe-rotten-tomatoes-could-be-a-source-of-generating-electricity-252450.html" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/indian-origin-scientists-believe-rotten-tomatoes-could-be-a-source-of-generating-electricity-252450.html" title="Indian-Origin Scientists Believe Rotten Tomatoes Could Be A Source Of Generating Electricity">
            Indian-Origin Scientists Believe Rotten Tomatoes Could Be A Source Of Generating Electricity                        </a>
                    </div>
                </figcaption>
            </div>
                     
            <div class="life-panel-list cf">
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/travel/there-is-a-place-known-as-the-land-of-stray-dogs-and-more-than-500-dogs-stay-there-252717.html" class="tint" title="There Is A Place Known As The Land Of Stray Dogs And More Than 500 Dogs Stay There!">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/1_1459258814_1459258826_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/1_1459258814_1459258826_502x234.jpg" border="0" alt="http://www.indiatimes.com/culture/travel/there-is-a-place-known-as-the-land-of-stray-dogs-and-more-than-500-dogs-stay-there-252717.html" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/culture/travel/there-is-a-place-known-as-the-land-of-stray-dogs-and-more-than-500-dogs-stay-there-252717.html" title="There Is A Place Known As The Land Of Stray Dogs And More Than 500 Dogs Stay There!">
            There Is A Place Known As The Land Of Stray Dogs And More Than 500 Dogs Stay There!                        </a>
                    </div>
                </figcaption>
            </div>
                </div><!--life-panel end-->

    <div class="trending-panel cf"><!--trending-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/trending/">trending</a></h2>
                    <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                            <figure>

            		

                    <a href="http://www.indiatimes.com/culture/who-we-are/the-way-people-reacted-to-this-boy-playing-holi-in-pakistan-is-exactly-what-s-going-to-change-the-world-252756.html" class="tint" title="The Way People Reacted To This Boy Playing Holi In Pakistan Is Exactly What's Going To Change The World">

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/holi2_1459332138_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/holi2_1459332138_236x111.jpg" border="0" alt="The Way People Reacted To This Boy Playing Holi In Pakistan Is Exactly What's Going To Change The World"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/the-way-people-reacted-to-this-boy-playing-holi-in-pakistan-is-exactly-what-s-going-to-change-the-world-252756.html" title="The Way People Reacted To This Boy Playing Holi In Pakistan Is Exactly What's Going To Change The World">
    The Way People Reacted To This Boy Playing Holi In Pakistan Is Exactly What's Going To Change The World                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
                <div class="trending-panel-list cf colombia" id="div-clmb-ctn-129061-6"  data-slot="129061" data-position="6" data-section="0" data-cb="adwidgetNew"><!--trending-panel-list start-->
                            <figure>

            		

                    <a href="http://www.indiatimes.com/entertainment/celebs/here-s-why-karisma-kapoor-withdrew-mutual-consent-plea-for-divorce-from-husband-sunjay-kapur-252804.html" class="tint" title="Here's Why Karisma Kapoor Withdrew Mutual Consent Plea For Divorce From Husband Sunjay Kapur">

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/karisma-card_1459410355_1459410359_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/karisma-card_1459410355_1459410359_236x111.jpg" border="0" alt="Here's Why Karisma Kapoor Withdrew Mutual Consent Plea For Divorce From Husband Sunjay Kapur"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/here-s-why-karisma-kapoor-withdrew-mutual-consent-plea-for-divorce-from-husband-sunjay-kapur-252804.html" title="Here's Why Karisma Kapoor Withdrew Mutual Consent Plea For Divorce From Husband Sunjay Kapur">
    Here's Why Karisma Kapoor Withdrew Mutual Consent Plea For Divorce From Husband Sunjay Kapur                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
                <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                            <figure>

            		

                    <a href="http://www.indiatimes.com/entertainment/celebs/rumours-say-modi-may-nominate-big-b-for-president-wait-what-252774.html" class="tint" title="Rumours Say Modi May Nominate Big B For President. Wait, What?">

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/abb_1459341715_1459341726_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/abb_1459341715_1459341726_236x111.jpg" border="0" alt="Rumours Say Modi May Nominate Big B For President. Wait, What?"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/rumours-say-modi-may-nominate-big-b-for-president-wait-what-252774.html" title="Rumours Say Modi May Nominate Big B For President. Wait, What?">
    Rumours Say Modi May Nominate Big B For President. Wait, What?                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
                <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                            <figure>

            		

                    <a href="http://www.indiatimes.com/entertainment/hollywood/9-hints-from-batman-vs-superman-dawn-of-justice-about-what-lies-ahead-in-the-dc-universe-252744.html" class="tint" title="9 Hints From Batman Vs Superman: Dawn Of Justice About What Lies Ahead In The DC Universe!">

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/batman-card_1459326186_1459326190_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/batman-card_1459326186_1459326190_236x111.jpg" border="0" alt="9 Hints From Batman Vs Superman: Dawn Of Justice About What Lies Ahead In The DC Universe!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/hollywood/9-hints-from-batman-vs-superman-dawn-of-justice-about-what-lies-ahead-in-the-dc-universe-252744.html" title="9 Hints From Batman Vs Superman: Dawn Of Justice About What Lies Ahead In The DC Universe!">
    9 Hints From Batman Vs Superman: Dawn Of Justice About What Lies Ahead In The DC Universe!                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
        </div><!--trending-panel end-->

</section><!--container end-->

<!------------------------------- end Block 3 ---------------------------------------->
<section class="big-ads">
    <div id="bigAd3_slot"></div>
</section>

<!------------------------------- start Block4  -------------------------------------->

<section class="container cf" id="container4"><!--container start-->
    <div class="news-panel cf ">
        <h2><a target="_blank" href="http://www.indiatimes.com/news/">news</a></h2>

                    <div class="news-panel-list cf " >
                <figure>                      
                    <a href="http://www.indiatimes.com/news/india/shaktiman-can-now-stand-on-his-feet-will-walk-in-months-252811.html" title="Shaktiman Can Now Stand On His Feet, Will Walk In Months!" class=" tint">
                                                <img  src="http://media.indiatimes.in/media/content/2016/Mar/fb-640_1459413158_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/fb-640_1459413158_236x111.jpg" border="0" alt="Shaktiman Can Now Stand On His Feet, Will Walk In Months!"/>
                    </a>
                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/shaktiman-can-now-stand-on-his-feet-will-walk-in-months-252811.html" title="Shaktiman Can Now Stand On His Feet, Will Walk In Months!">
        Shaktiman Can Now Stand On His Feet, Will Walk In Months!                    </a>
                </figcaption> 
            </div>
                <div class='container1'>            <div class="news-panel-list cf colombia" id="div-clmb-ctn-129061-12"  data-slot="129061" data-position="12" data-section="0" data-cb="adwidgetNew">
                <figure>                      
                    <a href="http://www.indiatimes.com/news/india/under-construction-flyover-collapses-in-kolkata-at-least-10-killed-150-feared-trapped-says-reports-252810.html" title="Under Construction Flyover Collapses In Kolkata, At Least 10 Killed, 150 Feared Trapped, Says Reports" class=" tint">
                                                <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1459412544_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1459412544_236x111.jpg" border="0" alt="Under Construction Flyover Collapses In Kolkata, At Least 10 Killed, 150 Feared Trapped, Says Reports"/>
                    </a>
                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/under-construction-flyover-collapses-in-kolkata-at-least-10-killed-150-feared-trapped-says-reports-252810.html" title="Under Construction Flyover Collapses In Kolkata, At Least 10 Killed, 150 Feared Trapped, Says Reports">
        Under Construction Flyover Collapses In Kolkata, At Least 10 Killed, 150 Feared Trapped, Says Reports                    </a>
                </figcaption> 
            </div>
                            <div class="news-panel-list cf " >
                <figure>                      
                    <a href="http://www.indiatimes.com/news/world/architects-want-to-dig-up-new-york-s-central-park-to-create-world-s-first-underground-skyline-252784.html" title="Architects Want To Dig Up New York's Central Park To Create World's First Underground Skyline!" class=" tint">
                                                <img  src="http://media.indiatimes.in/media/content/2016/Mar/cp1_1459401769_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cp1_1459401769_236x111.jpg" border="0" alt="Architects Want To Dig Up New York's Central Park To Create World's First Underground Skyline!"/>
                    </a>
                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/architects-want-to-dig-up-new-york-s-central-park-to-create-world-s-first-underground-skyline-252784.html" title="Architects Want To Dig Up New York's Central Park To Create World's First Underground Skyline!">
        Architects Want To Dig Up New York's Central Park To Create World's First Underground Skyline!                    </a>
                </figcaption> 
            </div>
                            <div class="news-panel-list cf " >
                <figure>                      
                    <a href="http://www.indiatimes.com/news/sports/15-years-ago-today-sachin-tendulkar-became-the-first-player-to-score-10-000-runs-in-odis-252791.html" title="15 Years Ago Today, Sachin Tendulkar Became The First Player To Score 10,000 Runs In ODIs" class=" tint">
                                                <img  src="http://media.indiatimes.in/media/content/2016/Mar/sachin1640_1459406693_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/sachin1640_1459406693_236x111.jpg" border="0" alt="15 Years Ago Today, Sachin Tendulkar Became The First Player To Score 10,000 Runs In ODIs"/>
                    </a>
                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/sports/15-years-ago-today-sachin-tendulkar-became-the-first-player-to-score-10-000-runs-in-odis-252791.html" title="15 Years Ago Today, Sachin Tendulkar Became The First Player To Score 10,000 Runs In ODIs">
        15 Years Ago Today, Sachin Tendulkar Became The First Player To Score 10,000 Runs In ODIs                    </a>
                </figcaption> 
            </div>
                 
    </div>
</div><!--news-panel end-->

<div class="life-panel cf container2" id="b4c2"><!--life-panel start-->
    <h2><a target="_blank" href="http://www.indiatimes.com/lifestyle/">lifestyle</a></h2>
        
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/gurmeet-ram-rahim-singh-s-song-love-charger-tops-jimmy-fallon-s-list-do-not-play-list-252826.html" class="tint" title="Gurmeet Ram Rahim Singh's Song 'Love Charger' Tops Jimmy Fallon's List. Do Not Play, List!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/msg-card_1459420966_1459420971_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/msg-card_1459420966_1459420971_502x234.jpg" border="0" alt="Gurmeet Ram Rahim Singh's Song 'Love Charger' Tops Jimmy Fallon's List. Do Not Play, List!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/gurmeet-ram-rahim-singh-s-song-love-charger-tops-jimmy-fallon-s-list-do-not-play-list-252826.html" title="Gurmeet Ram Rahim Singh's Song 'Love Charger' Tops Jimmy Fallon's List. Do Not Play, List!">
    Gurmeet Ram Rahim Singh's Song 'Love Charger' Tops Jimmy Fallon's List. Do Not Play, List!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/7-ways-to-get-a-flat-belly-in-a-week-252718.html" class="tint" title="7 Ways To Get A Flat Belly In A Week">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/fb_1459259206_1459259219_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/fb_1459259206_1459259219_502x234.jpg" border="0" alt="7 Ways To Get A Flat Belly In A Week" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/7-ways-to-get-a-flat-belly-in-a-week-252718.html" title="7 Ways To Get A Flat Belly In A Week">
    7 Ways To Get A Flat Belly In A Week                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/uday-chopra-s-heroic-efforts-to-take-on-his-haters-gets-him-trolled-left-right-centre-252816.html" class="tint" title="Uday Chopra's Heroic Efforts To Take On His Haters Gets Him Trolled Left, Right & Centre!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/uday-chopra-card_1459417140_1459417171_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/uday-chopra-card_1459417140_1459417171_502x234.jpg" border="0" alt="Uday Chopra's Heroic Efforts To Take On His Haters Gets Him Trolled Left, Right & Centre!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/uday-chopra-s-heroic-efforts-to-take-on-his-haters-gets-him-trolled-left-right-centre-252816.html" title="Uday Chopra's Heroic Efforts To Take On His Haters Gets Him Trolled Left, Right & Centre!">
    Uday Chopra's Heroic Efforts To Take On His Haters Gets Him Trolled Left, Right & Centre!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/7-fun-facts-about-bollywood-which-we-bet-you-didn-t-know-252805.html" class="tint" title="7 Fun Facts About Bollywood Which We Bet You Didn't Know!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/p0_1459412125_1459412133_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/p0_1459412125_1459412133_502x234.jpg" border="0" alt="7 Fun Facts About Bollywood Which We Bet You Didn't Know!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/7-fun-facts-about-bollywood-which-we-bet-you-didn-t-know-252805.html" title="7 Fun Facts About Bollywood Which We Bet You Didn't Know!">
    7 Fun Facts About Bollywood Which We Bet You Didn't Know!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/food/scottish-brewers-launch-beer-that-you-can-spread-on-toast-252795.html" class="tint" title="Scottish Brewers Launch Beer That You Can Spread On Toast!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/cp2_1459408089_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cp2_1459408089_502x234.jpg" border="0" alt="Scottish Brewers Launch Beer That You Can Spread On Toast!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/food/scottish-brewers-launch-beer-that-you-can-spread-on-toast-252795.html" title="Scottish Brewers Launch Beer That You Can Spread On Toast!">
    Scottish Brewers Launch Beer That You Can Spread On Toast!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/another-twist-in-the-tale-hrithik-roshan-s-lawyers-say-that-his-pope-tweet-was-about-a-fish-252801.html" class="tint" title="Another Twist In The Tale, Hrithik Roshan's Lawyers Say That His 'Pope' Tweet Was About A Fish!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/karis02_1459409358_1459409362_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/karis02_1459409358_1459409362_502x234.jpg" border="0" alt="Another Twist In The Tale, Hrithik Roshan's Lawyers Say That His 'Pope' Tweet Was About A Fish!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/another-twist-in-the-tale-hrithik-roshan-s-lawyers-say-that-his-pope-tweet-was-about-a-fish-252801.html" title="Another Twist In The Tale, Hrithik Roshan's Lawyers Say That His 'Pope' Tweet Was About A Fish!">
    Another Twist In The Tale, Hrithik Roshan's Lawyers Say That His 'Pope' Tweet Was About A Fish!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/buzz/22-comics-that-every-new-mother-will-relate-to-252008.html" class="tint" title="22 Comics That Every New Mother Will Relate To!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/cover_1458022228_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cover_1458022228_502x234.jpg" border="0" alt="22 Comics That Every New Mother Will Relate To!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/buzz/22-comics-that-every-new-mother-will-relate-to-252008.html" title="22 Comics That Every New Mother Will Relate To!">
    22 Comics That Every New Mother Will Relate To!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/here-s-why-karisma-kapoor-withdrew-mutual-consent-plea-for-divorce-from-husband-sunjay-kapur-252804.html" class="tint" title="Here's Why Karisma Kapoor Withdrew Mutual Consent Plea For Divorce From Husband Sunjay Kapur">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/karisma-card_1459410355_1459410359_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/karisma-card_1459410355_1459410359_502x234.jpg" border="0" alt="Here's Why Karisma Kapoor Withdrew Mutual Consent Plea For Divorce From Husband Sunjay Kapur" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/here-s-why-karisma-kapoor-withdrew-mutual-consent-plea-for-divorce-from-husband-sunjay-kapur-252804.html" title="Here's Why Karisma Kapoor Withdrew Mutual Consent Plea For Divorce From Husband Sunjay Kapur">
    Here's Why Karisma Kapoor Withdrew Mutual Consent Plea For Divorce From Husband Sunjay Kapur                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/bollywood-celebs-extend-support-to-aamir-khan-for-spreading-awareness-about-water-conservation-252792.html" class="tint" title="Bollywood Celebs Extend Support To Aamir Khan For Spreading Awareness About Water-Conservation">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/card1_1459407015_1459407021_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card1_1459407015_1459407021_502x234.jpg" border="0" alt="Bollywood Celebs Extend Support To Aamir Khan For Spreading Awareness About Water-Conservation" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/bollywood-celebs-extend-support-to-aamir-khan-for-spreading-awareness-about-water-conservation-252792.html" title="Bollywood Celebs Extend Support To Aamir Khan For Spreading Awareness About Water-Conservation">
    Bollywood Celebs Extend Support To Aamir Khan For Spreading Awareness About Water-Conservation                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/harbhajan-singh-gives-a-fitting-reply-to-a-guy-who-trolled-him-for-being-a-passenger-in-the-indian-cricket-team-252814.html" class="tint" title="Harbhajan Singh Gives A Fitting Reply To A Guy Who Trolled Him For Being A Passenger In The Indian Cricket Team">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/harbhajanmusclesafp_1459414292_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/harbhajanmusclesafp_1459414292_502x234.jpg" border="0" alt="Harbhajan Singh Gives A Fitting Reply To A Guy Who Trolled Him For Being A Passenger In The Indian Cricket Team" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/harbhajan-singh-gives-a-fitting-reply-to-a-guy-who-trolled-him-for-being-a-passenger-in-the-indian-cricket-team-252814.html" title="Harbhajan Singh Gives A Fitting Reply To A Guy Who Trolled Him For Being A Passenger In The Indian Cricket Team">
    Harbhajan Singh Gives A Fitting Reply To A Guy Who Trolled Him For Being A Passenger In The Indian Cricket Team                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/hrithik-roshan-names-kangana-ranaut-for-the-first-time-in-the-fir-against-email-impostor-252793.html" class="tint" title="Hrithik Roshan Names Kangana Ranaut For The First Time In The FIR Against Email Impostor!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage_1459406896_1459406902_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage_1459406896_1459406902_502x234.jpg" border="0" alt="Hrithik Roshan Names Kangana Ranaut For The First Time In The FIR Against Email Impostor!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/hrithik-roshan-names-kangana-ranaut-for-the-first-time-in-the-fir-against-email-impostor-252793.html" title="Hrithik Roshan Names Kangana Ranaut For The First Time In The FIR Against Email Impostor!">
    Hrithik Roshan Names Kangana Ranaut For The First Time In The FIR Against Email Impostor!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/this-video-of-real-azhar-giving-training-to-reel-azhar-emraan-will-make-you-green-with-envy-252786.html" class="tint" title="This Video Of Real Azhar Giving Training To Reel Azhar Emraan Will Make You Green With Envy!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/azh-cd_1459404380_1459404385_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/azh-cd_1459404380_1459404385_502x234.jpg" border="0" alt="This Video Of Real Azhar Giving Training To Reel Azhar Emraan Will Make You Green With Envy!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/this-video-of-real-azhar-giving-training-to-reel-azhar-emraan-will-make-you-green-with-envy-252786.html" title="This Video Of Real Azhar Giving Training To Reel Azhar Emraan Will Make You Green With Envy!">
    This Video Of Real Azhar Giving Training To Reel Azhar Emraan Will Make You Green With Envy!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/ravi-shastri-s-warning-to-indian-batsmen-ahead-of-semis-virat-kohli-can-t-always-bail-you-out-252788.html" class="tint" title="Ravi Shastri's Warning To Indian Batsmen Ahead Of Semis - Virat Kohli Can't Always Bail You Out">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/rv640_1459404058_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/rv640_1459404058_502x234.jpg" border="0" alt="Ravi Shastri's Warning To Indian Batsmen Ahead Of Semis - Virat Kohli Can't Always Bail You Out" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/ravi-shastri-s-warning-to-indian-batsmen-ahead-of-semis-virat-kohli-can-t-always-bail-you-out-252788.html" title="Ravi Shastri's Warning To Indian Batsmen Ahead Of Semis - Virat Kohli Can't Always Bail You Out">
    Ravi Shastri's Warning To Indian Batsmen Ahead Of Semis - Virat Kohli Can't Always Bail You Out                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/technology/scientists-claim-unknown-planet-9-could-be-responsible-for-mass-extinction-of-life-on-earth-252782.html" class="tint" title="Scientists Claim Unknown 'Planet 9' Could Be Responsible For Mass Extinction Of Life On Earth">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/cp_1459401597_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cp_1459401597_502x234.jpg" border="0" alt="Scientists Claim Unknown 'Planet 9' Could Be Responsible For Mass Extinction Of Life On Earth" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/technology/scientists-claim-unknown-planet-9-could-be-responsible-for-mass-extinction-of-life-on-earth-252782.html" title="Scientists Claim Unknown 'Planet 9' Could Be Responsible For Mass Extinction Of Life On Earth">
    Scientists Claim Unknown 'Planet 9' Could Be Responsible For Mass Extinction Of Life On Earth                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/step-motherly-treatment-with-india-s-women-cricket-team-has-left-sonakshi-sinha-disappointed-252783.html" class="tint" title="Step-Motherly Treatment With India's Women Cricket Team Has Left Sonakshi Sinha Disappointed!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/l1_1459401979_1459401991_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/l1_1459401979_1459401991_502x234.jpg" border="0" alt="Step-Motherly Treatment With India's Women Cricket Team Has Left Sonakshi Sinha Disappointed!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/step-motherly-treatment-with-india-s-women-cricket-team-has-left-sonakshi-sinha-disappointed-252783.html" title="Step-Motherly Treatment With India's Women Cricket Team Has Left Sonakshi Sinha Disappointed!">
    Step-Motherly Treatment With India's Women Cricket Team Has Left Sonakshi Sinha Disappointed!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/andrew-flintoff-has-nothing-better-to-do-trolls-mate-big-b-asks-for-tickets-to-t20-finals-252780.html" class="tint" title="Flintoff Has Nothing Better To Do, Trolls 'Mate' Big B Again Asking For Tickets To T20 Finals!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/andrewbachchan640_1459398795_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/andrewbachchan640_1459398795_502x234.jpg" border="0" alt="Flintoff Has Nothing Better To Do, Trolls 'Mate' Big B Again Asking For Tickets To T20 Finals!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/andrew-flintoff-has-nothing-better-to-do-trolls-mate-big-b-asks-for-tickets-to-t20-finals-252780.html" title="Flintoff Has Nothing Better To Do, Trolls 'Mate' Big B Again Asking For Tickets To T20 Finals!">
    Flintoff Has Nothing Better To Do, Trolls 'Mate' Big B Again Asking For Tickets To T20 Finals!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/buzz/india-is-suffering-at-36-we-have-the-highest-rate-of-depression-in-the-world-252755.html" class="tint" title="India Is Suffering. At 36%, We Have The Highest Rate Of Depression In The World!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/cover_1459331916_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cover_1459331916_502x234.jpg" border="0" alt="India Is Suffering. At 36%, We Have The Highest Rate Of Depression In The World!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/buzz/india-is-suffering-at-36-we-have-the-highest-rate-of-depression-in-the-world-252755.html" title="India Is Suffering. At 36%, We Have The Highest Rate Of Depression In The World!">
    India Is Suffering. At 36%, We Have The Highest Rate Of Depression In The World!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/rumours-say-modi-may-nominate-big-b-for-president-wait-what-252774.html" class="tint" title="Rumours Say Modi May Nominate Big B For President. Wait, What?">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/abb_1459341715_1459341726_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/abb_1459341715_1459341726_502x234.jpg" border="0" alt="Rumours Say Modi May Nominate Big B For President. Wait, What?" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/rumours-say-modi-may-nominate-big-b-for-president-wait-what-252774.html" title="Rumours Say Modi May Nominate Big B For President. Wait, What?">
    Rumours Say Modi May Nominate Big B For President. Wait, What?                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/who-we-are/the-way-people-reacted-to-this-boy-playing-holi-in-pakistan-is-exactly-what-s-going-to-change-the-world-252756.html" class="tint" title="The Way People Reacted To This Boy Playing Holi In Pakistan Is Exactly What's Going To Change The World">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/holi2_1459332138_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/holi2_1459332138_502x234.jpg" border="0" alt="The Way People Reacted To This Boy Playing Holi In Pakistan Is Exactly What's Going To Change The World" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/the-way-people-reacted-to-this-boy-playing-holi-in-pakistan-is-exactly-what-s-going-to-change-the-world-252756.html" title="The Way People Reacted To This Boy Playing Holi In Pakistan Is Exactly What's Going To Change The World">
    The Way People Reacted To This Boy Playing Holi In Pakistan Is Exactly What's Going To Change The World                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/randeep-hooda-turns-haryanvi-blood-mafia-in-laal-rang-it-s-trailer-looks-really-intriguing-252771.html" class="tint" title="Randeep Hooda Turns Haryanvi Blood Mafia In 'Laal Rang' & It's Trailer Looks Really Intriguing!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/screenshot_3_1459339743_1459339747_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/screenshot_3_1459339743_1459339747_502x234.jpg" border="0" alt="Randeep Hooda Turns Haryanvi Blood Mafia In 'Laal Rang' & It's Trailer Looks Really Intriguing!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/randeep-hooda-turns-haryanvi-blood-mafia-in-laal-rang-it-s-trailer-looks-really-intriguing-252771.html" title="Randeep Hooda Turns Haryanvi Blood Mafia In 'Laal Rang' & It's Trailer Looks Really Intriguing!">
    Randeep Hooda Turns Haryanvi Blood Mafia In 'Laal Rang' & It's Trailer Looks Really Intriguing!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/hollywood/9-hints-from-batman-vs-superman-dawn-of-justice-about-what-lies-ahead-in-the-dc-universe-252744.html" class="tint" title="9 Hints From Batman Vs Superman: Dawn Of Justice About What Lies Ahead In The DC Universe!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/batman-card_1459326186_1459326190_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/batman-card_1459326186_1459326190_502x234.jpg" border="0" alt="9 Hints From Batman Vs Superman: Dawn Of Justice About What Lies Ahead In The DC Universe!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/hollywood/9-hints-from-batman-vs-superman-dawn-of-justice-about-what-lies-ahead-in-the-dc-universe-252744.html" title="9 Hints From Batman Vs Superman: Dawn Of Justice About What Lies Ahead In The DC Universe!">
    9 Hints From Batman Vs Superman: Dawn Of Justice About What Lies Ahead In The DC Universe!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/who-we-are/this-image-shows-our-hypocrisy-in-reacting-to-terrorist-attacks-in-different-parts-of-the-world-252751.html" class="tint" title="This Image Shows Our Hypocrisy In Reacting To Terrorist Attacks In Different Parts Of The World">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/cp3_1459329043_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cp3_1459329043_502x234.jpg" border="0" alt="This Image Shows Our Hypocrisy In Reacting To Terrorist Attacks In Different Parts Of The World" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/this-image-shows-our-hypocrisy-in-reacting-to-terrorist-attacks-in-different-parts-of-the-world-252751.html" title="This Image Shows Our Hypocrisy In Reacting To Terrorist Attacks In Different Parts Of The World">
    This Image Shows Our Hypocrisy In Reacting To Terrorist Attacks In Different Parts Of The World                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/confirmed-karan-singh-grover-bipasha-basu-are-tying-the-knot-next-month-here-s-all-the-dope-252757.html" class="tint" title="Confirmed! Karan Singh Grover & Bipasha Basu Are Tying The Knot Next Month. Here's All The Dope">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/wedding1_1459333231_1459333237_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/wedding1_1459333231_1459333237_502x234.jpg" border="0" alt="Confirmed! Karan Singh Grover & Bipasha Basu Are Tying The Knot Next Month. Here's All The Dope" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/confirmed-karan-singh-grover-bipasha-basu-are-tying-the-knot-next-month-here-s-all-the-dope-252757.html" title="Confirmed! Karan Singh Grover & Bipasha Basu Are Tying The Knot Next Month. Here's All The Dope">
    Confirmed! Karan Singh Grover & Bipasha Basu Are Tying The Knot Next Month. Here's All The Dope                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/buzz/scientists-identify-a-new-universal-facial-expression-the-not-face-252688.html" class="tint" title="Scientists Identify A New Universal Facial Expression: The Not Face">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/card-4_1459321178_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card-4_1459321178_502x234.jpg" border="0" alt="Scientists Identify A New Universal Facial Expression: The Not Face" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/buzz/scientists-identify-a-new-universal-facial-expression-the-not-face-252688.html" title="Scientists Identify A New Universal Facial Expression: The Not Face">
    Scientists Identify A New Universal Facial Expression: The Not Face                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/injured-yuvraj-singh-ruled-out-of-world-t20-here-are-three-guys-who-can-replace-him-in-semis-vs-wi-252762.html" class="tint" title="Injured Yuvraj Singh Ruled Out Of World T20. Here Are Three Guys Who Can Replace Him In Semis Vs WI">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/yuvrajinjuredreuters_1459333684_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/yuvrajinjuredreuters_1459333684_502x234.jpg" border="0" alt="Injured Yuvraj Singh Ruled Out Of World T20. Here Are Three Guys Who Can Replace Him In Semis Vs WI" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/injured-yuvraj-singh-ruled-out-of-world-t20-here-are-three-guys-who-can-replace-him-in-semis-vs-wi-252762.html" title="Injured Yuvraj Singh Ruled Out Of World T20. Here Are Three Guys Who Can Replace Him In Semis Vs WI">
    Injured Yuvraj Singh Ruled Out Of World T20. Here Are Three Guys Who Can Replace Him In Semis Vs WI                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/move-over-arjun-ranveer-singh-to-bromance-with-ranbir-kapoor-in-zoya-akhtar-s-next-252741.html" class="tint" title="Move Over Arjun, Ranveer Singh To Bromance With Ranbir Kapoor In Zoya Akhtar's Next!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/ccrd_1459325291_1459325296_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/ccrd_1459325291_1459325296_502x234.jpg" border="0" alt="Move Over Arjun, Ranveer Singh To Bromance With Ranbir Kapoor In Zoya Akhtar's Next!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/move-over-arjun-ranveer-singh-to-bromance-with-ranbir-kapoor-in-zoya-akhtar-s-next-252741.html" title="Move Over Arjun, Ranveer Singh To Bromance With Ranbir Kapoor In Zoya Akhtar's Next!">
    Move Over Arjun, Ranveer Singh To Bromance With Ranbir Kapoor In Zoya Akhtar's Next!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/who-we-are/with-odd-even-returning-to-delhi-here-s-what-you-will-need-to-do-again-to-make-your-commute-easy-252627.html" class="tint" title="With Odd Even Returning To Delhi, Here's What You Will Need To Do Again To Make Your Commute Easy">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1459318472_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1459318472_502x234.jpg" border="0" alt="With Odd Even Returning To Delhi, Here's What You Will Need To Do Again To Make Your Commute Easy" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/with-odd-even-returning-to-delhi-here-s-what-you-will-need-to-do-again-to-make-your-commute-easy-252627.html" title="With Odd Even Returning To Delhi, Here's What You Will Need To Do Again To Make Your Commute Easy">
    With Odd Even Returning To Delhi, Here's What You Will Need To Do Again To Make Your Commute Easy                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/buzz/yoga-based-technique-for-prevention-and-cure-of-cancer-may-just-be-a-year-away-252696.html" class="tint" title="Yoga Based Technique For Prevention And Cure Of Cancer May Just Be A Year Away!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/rtx1ocvb-600_1459248226_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/rtx1ocvb-600_1459248226_502x234.jpg" border="0" alt="Yoga Based Technique For Prevention And Cure Of Cancer May Just Be A Year Away!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/buzz/yoga-based-technique-for-prevention-and-cure-of-cancer-may-just-be-a-year-away-252696.html" title="Yoga Based Technique For Prevention And Cure Of Cancer May Just Be A Year Away!">
    Yoga Based Technique For Prevention And Cure Of Cancer May Just Be A Year Away!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/theon-greyjoy-just-dropped-a-major-spoiler-for-got-season-6-shares-what-s-in-store-for-him-and-sansa-252746.html" class="tint" title="Theon Greyjoy Just Dropped A Major Spoiler For GoT Season 6, Shares What's In Store For Him And Sansa">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/allen-card_1459326978_1459326981_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/allen-card_1459326978_1459326981_502x234.jpg" border="0" alt="Theon Greyjoy Just Dropped A Major Spoiler For GoT Season 6, Shares What's In Store For Him And Sansa" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/theon-greyjoy-just-dropped-a-major-spoiler-for-got-season-6-shares-what-s-in-store-for-him-and-sansa-252746.html" title="Theon Greyjoy Just Dropped A Major Spoiler For GoT Season 6, Shares What's In Store For Him And Sansa">
    Theon Greyjoy Just Dropped A Major Spoiler For GoT Season 6, Shares What's In Store For Him And Sansa                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/this-heartfelt-apology-by-shahid-afridi-shows-what-playing-for-pakistan-actually-means-to-him-252748.html" class="tint" title="This Heartfelt Apology By Shahid Afridi Shows What Playing For Pakistan Actually Means To Him">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/afridivideo_1459325240_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/afridivideo_1459325240_502x234.jpg" border="0" alt="This Heartfelt Apology By Shahid Afridi Shows What Playing For Pakistan Actually Means To Him" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/this-heartfelt-apology-by-shahid-afridi-shows-what-playing-for-pakistan-actually-means-to-him-252748.html" title="This Heartfelt Apology By Shahid Afridi Shows What Playing For Pakistan Actually Means To Him">
    This Heartfelt Apology By Shahid Afridi Shows What Playing For Pakistan Actually Means To Him                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/technology/life-just-got-a-bit-more-dangerous-man-invents-a-gun-that-disguises-itself-as-a-smartphone-252732.html" class="tint" title="Life Just Got A Bit More Dangerous. Man Invents A Gun That Disguises Itself As A Smartphone!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/640x299_1459323351_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/640x299_1459323351_502x234.jpg" border="0" alt="Life Just Got A Bit More Dangerous. Man Invents A Gun That Disguises Itself As A Smartphone!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/technology/life-just-got-a-bit-more-dangerous-man-invents-a-gun-that-disguises-itself-as-a-smartphone-252732.html" title="Life Just Got A Bit More Dangerous. Man Invents A Gun That Disguises Itself As A Smartphone!">
    Life Just Got A Bit More Dangerous. Man Invents A Gun That Disguises Itself As A Smartphone!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/the-jungle-book-makers-take-a-dig-at-censor-board-after-receiving-a-u-a-certificate-252734.html" class="tint" title="'The Jungle Book' Makers Take A Dig At Censor Board After Receiving A U/A Certificate">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/jungle-card_1459320189_1459320196_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/jungle-card_1459320189_1459320196_502x234.jpg" border="0" alt="'The Jungle Book' Makers Take A Dig At Censor Board After Receiving A U/A Certificate" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/the-jungle-book-makers-take-a-dig-at-censor-board-after-receiving-a-u-a-certificate-252734.html" title="'The Jungle Book' Makers Take A Dig At Censor Board After Receiving A U/A Certificate">
    'The Jungle Book' Makers Take A Dig At Censor Board After Receiving A U/A Certificate                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/refuting-all-rumors-ankita-lokhande-confirms-that-sushant-her-are-very-much-together-252725.html" class="tint" title="Refuting All Rumors, Ankita Lokhande Confirms That Sushant & Her Are Very Much Together">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/sushant-ankita2_1459315609_1459315621_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/sushant-ankita2_1459315609_1459315621_502x234.jpg" border="0" alt="Refuting All Rumors, Ankita Lokhande Confirms That Sushant & Her Are Very Much Together" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/refuting-all-rumors-ankita-lokhande-confirms-that-sushant-her-are-very-much-together-252725.html" title="Refuting All Rumors, Ankita Lokhande Confirms That Sushant & Her Are Very Much Together">
    Refuting All Rumors, Ankita Lokhande Confirms That Sushant & Her Are Very Much Together                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/hrithik-roshan-s-affair-with-the-pope-tweet-lands-him-in-a-legal-soup-252724.html" class="tint" title="Hrithik Roshanâs 'Affair With The Pope' Tweet Lands Him In A Legal Soup!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/10hrithik-roshan15_1459314568_1459314574_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/10hrithik-roshan15_1459314568_1459314574_502x234.jpg" border="0" alt="Hrithik Roshanâs 'Affair With The Pope' Tweet Lands Him In A Legal Soup!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/hrithik-roshan-s-affair-with-the-pope-tweet-lands-him-in-a-legal-soup-252724.html" title="Hrithik Roshanâs 'Affair With The Pope' Tweet Lands Him In A Legal Soup!">
    Hrithik Roshanâs 'Affair With The Pope' Tweet Lands Him In A Legal Soup!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/buzz/smartphone-assistants-siri-and-cortana-not-so-smart-when-it-comes-to-urgent-medical-queries-252161.html" class="tint" title="Smartphone Assistants Siri And Cortana Not So Smart When It Comes To Urgent Medical Queries">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/lybrate-user-640_1458212031_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/lybrate-user-640_1458212031_502x234.jpg" border="0" alt="Smartphone Assistants Siri And Cortana Not So Smart When It Comes To Urgent Medical Queries" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/buzz/smartphone-assistants-siri-and-cortana-not-so-smart-when-it-comes-to-urgent-medical-queries-252161.html" title="Smartphone Assistants Siri And Cortana Not So Smart When It Comes To Urgent Medical Queries">
    Smartphone Assistants Siri And Cortana Not So Smart When It Comes To Urgent Medical Queries                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/this-pic-of-virat-kohli-hanging-with-baby-ziva-dhoni-is-so-cute-you-should-hide-it-from-your-gf-252720.html" class="tint" title="This Pic Of Virat Kohli Hanging With Baby Ziva Dhoni Is So Cute You Should Hide It From Your GF!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/viratkohli640_1459273428_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/viratkohli640_1459273428_502x234.jpg" border="0" alt="This Pic Of Virat Kohli Hanging With Baby Ziva Dhoni Is So Cute You Should Hide It From Your GF!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/this-pic-of-virat-kohli-hanging-with-baby-ziva-dhoni-is-so-cute-you-should-hide-it-from-your-gf-252720.html" title="This Pic Of Virat Kohli Hanging With Baby Ziva Dhoni Is So Cute You Should Hide It From Your GF!">
    This Pic Of Virat Kohli Hanging With Baby Ziva Dhoni Is So Cute You Should Hide It From Your GF!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
        <a class='video-btn sprite' href='http://www.indiatimes.com/play/slog-overs/this-maukamauka-parody-shows-why-chris-gayle-won-t-be-a-hit-at-wankhede-stadium-vs-india-252719.html'>video</a>                <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/this-maukamauka-parody-shows-why-chris-gayle-won-t-be-a-hit-at-wankhede-stadium-vs-india-252719.html" class="tint" title="This #MaukaMauka Parody Shows Why Chris Gayle Won't Be A Hit At Wankhede Stadium Vs India!">
                    <img  src="http://media.indiatimes.in/media/videocafe/2016/Mar/mauka_wi_card_1459270166_502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Mar/mauka_wi_card_1459270166_502x234.jpg" border="0" alt="This #MaukaMauka Parody Shows Why Chris Gayle Won't Be A Hit At Wankhede Stadium Vs India!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/this-maukamauka-parody-shows-why-chris-gayle-won-t-be-a-hit-at-wankhede-stadium-vs-india-252719.html" title="This #MaukaMauka Parody Shows Why Chris Gayle Won't Be A Hit At Wankhede Stadium Vs India!">
    This #MaukaMauka Parody Shows Why Chris Gayle Won't Be A Hit At Wankhede Stadium Vs India!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/who-we-are/if-you-ever-wonder-what-intimacy-looks-like-then-take-a-look-at-these-pictures-252624.html" class="tint" title="If You Ever Wonder What Intimacy Looks Like Then Take A Look At These Pictures">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/couples_1459152188_1459152199_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/couples_1459152188_1459152199_502x234.jpg" border="0" alt="If You Ever Wonder What Intimacy Looks Like Then Take A Look At These Pictures" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/if-you-ever-wonder-what-intimacy-looks-like-then-take-a-look-at-these-pictures-252624.html" title="If You Ever Wonder What Intimacy Looks Like Then Take A Look At These Pictures">
    If You Ever Wonder What Intimacy Looks Like Then Take A Look At These Pictures                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/5-natural-supplements-that-can-actually-help-you-stay-young-forever-252652.html" class="tint" title="5 Natural Supplements That Can Actually Help You Stay Young Forever!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/fb_1459173812_1459173827_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/fb_1459173812_1459173827_502x234.jpg" border="0" alt="5 Natural Supplements That Can Actually Help You Stay Young Forever!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/5-natural-supplements-that-can-actually-help-you-stay-young-forever-252652.html" title="5 Natural Supplements That Can Actually Help You Stay Young Forever!">
    5 Natural Supplements That Can Actually Help You Stay Young Forever!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/prison-break-star-wentworth-miller-gives-a-powerful-response-to-his-body-shamers-252707.html" class="tint" title="Prison Break Star Wentworth Miller Gives A Powerful Response To His Body-Shamers!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/wentworth-miller-a-l-epoque-de-prison-break_exact1024x768_l_1459252169_1459252174_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/wentworth-miller-a-l-epoque-de-prison-break_exact1024x768_l_1459252169_1459252174_502x234.jpg" border="0" alt="Prison Break Star Wentworth Miller Gives A Powerful Response To His Body-Shamers!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/prison-break-star-wentworth-miller-gives-a-powerful-response-to-his-body-shamers-252707.html" title="Prison Break Star Wentworth Miller Gives A Powerful Response To His Body-Shamers!">
    Prison Break Star Wentworth Miller Gives A Powerful Response To His Body-Shamers!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/virat-kohli-displaces-aaron-finch-to-regain-no-1-spot-in-t20-rankings-india-retain-top-spot-252712.html" class="tint" title="Virat Kohli Displaces Aaron Finch To Regain No 1 Spot In T20 Rankings. India Retain Top Spot">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/kohliausno1reuters_1459256465_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/kohliausno1reuters_1459256465_502x234.jpg" border="0" alt="Virat Kohli Displaces Aaron Finch To Regain No 1 Spot In T20 Rankings. India Retain Top Spot" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/virat-kohli-displaces-aaron-finch-to-regain-no-1-spot-in-t20-rankings-india-retain-top-spot-252712.html" title="Virat Kohli Displaces Aaron Finch To Regain No 1 Spot In T20 Rankings. India Retain Top Spot">
    Virat Kohli Displaces Aaron Finch To Regain No 1 Spot In T20 Rankings. India Retain Top Spot                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/technology/this-parody-commercial-tears-into-apple-s-new-iphone-se-makes-a-lot-of-sense-252694.html" class="tint" title="This Parody Commercial Tears Into Apple's New iPhone SE, Makes A Lot Of Sense">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/cp1_1459248003_1459248011_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cp1_1459248003_1459248011_502x234.jpg" border="0" alt="This Parody Commercial Tears Into Apple's New iPhone SE, Makes A Lot Of Sense" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/technology/this-parody-commercial-tears-into-apple-s-new-iphone-se-makes-a-lot-of-sense-252694.html" title="This Parody Commercial Tears Into Apple's New iPhone SE, Makes A Lot Of Sense">
    This Parody Commercial Tears Into Apple's New iPhone SE, Makes A Lot Of Sense                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/arjun-kapoor-salutes-virat-kohli-for-shutting-down-all-those-who-were-trolling-anushka-sharma-252701.html" class="tint" title="Arjun Kapoor Salutes Virat Kohli For Shutting Down All Those Who Were Trolling Anushka Sharma">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage_1459249821_1459249830_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage_1459249821_1459249830_502x234.jpg" border="0" alt="Arjun Kapoor Salutes Virat Kohli For Shutting Down All Those Who Were Trolling Anushka Sharma" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/arjun-kapoor-salutes-virat-kohli-for-shutting-down-all-those-who-were-trolling-anushka-sharma-252701.html" title="Arjun Kapoor Salutes Virat Kohli For Shutting Down All Those Who Were Trolling Anushka Sharma">
    Arjun Kapoor Salutes Virat Kohli For Shutting Down All Those Who Were Trolling Anushka Sharma                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/sunil-gavaskar-tears-into-anushka-sharma-haters-says-she-only-helped-virat-kohli-become-a-better-cricketer-252708.html" class="tint" title="Sunil Gavaskar Tears Into Anushka Sharma Haters, Says She Only Helped Virat Kohli Become A Better Cricketer">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/gavaskarkohlianushka_1459251945_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/gavaskarkohlianushka_1459251945_502x234.jpg" border="0" alt="Sunil Gavaskar Tears Into Anushka Sharma Haters, Says She Only Helped Virat Kohli Become A Better Cricketer" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/sunil-gavaskar-tears-into-anushka-sharma-haters-says-she-only-helped-virat-kohli-become-a-better-cricketer-252708.html" title="Sunil Gavaskar Tears Into Anushka Sharma Haters, Says She Only Helped Virat Kohli Become A Better Cricketer">
    Sunil Gavaskar Tears Into Anushka Sharma Haters, Says She Only Helped Virat Kohli Become A Better Cricketer                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/vinod-kambli-gets-trolled-mercilessly-after-apparently-taking-a-dig-at-sachin-tendulkar-252703.html" class="tint" title="Vinod Kambli Gets Trolled Mercilessly After Apparently Taking A Dig At Sachin Tendulkar">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/picture640_1459255307_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/picture640_1459255307_502x234.jpg" border="0" alt="Vinod Kambli Gets Trolled Mercilessly After Apparently Taking A Dig At Sachin Tendulkar" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/vinod-kambli-gets-trolled-mercilessly-after-apparently-taking-a-dig-at-sachin-tendulkar-252703.html" title="Vinod Kambli Gets Trolled Mercilessly After Apparently Taking A Dig At Sachin Tendulkar">
    Vinod Kambli Gets Trolled Mercilessly After Apparently Taking A Dig At Sachin Tendulkar                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/who-we-are/remember-thomas-the-tank-engine-he-s-now-getting-a-new-indian-friend-252686.html" class="tint" title="Remember Thomas The Tank Engine? He's Now Getting A New Indian Friend">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/cp_1459242188_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cp_1459242188_502x234.jpg" border="0" alt="Remember Thomas The Tank Engine? He's Now Getting A New Indian Friend" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/remember-thomas-the-tank-engine-he-s-now-getting-a-new-indian-friend-252686.html" title="Remember Thomas The Tank Engine? He's Now Getting A New Indian Friend">
    Remember Thomas The Tank Engine? He's Now Getting A New Indian Friend                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/buzz/a-mysterious-breast-infection-is-affecting-healthy-women-in-pune-252684.html" class="tint" title="A Mysterious Breast Infection Is Affecting Healthy Women In Pune">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/cover_1459242041_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cover_1459242041_502x234.jpg" border="0" alt="A Mysterious Breast Infection Is Affecting Healthy Women In Pune" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/buzz/a-mysterious-breast-infection-is-affecting-healthy-women-in-pune-252684.html" title="A Mysterious Breast Infection Is Affecting Healthy Women In Pune">
    A Mysterious Breast Infection Is Affecting Healthy Women In Pune                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/the-lighter-side-of-david-beckham-shown-in-a-photo-where-he-is-stitching-a-dress-for-his-daughter-s-doll-252692.html" class="tint" title="This Is How Legendary Footballer David Beckham Spends His Retirement, Stitching Dresses For His Daughter's Dolls">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/beckhanm640_1459247088_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/beckhanm640_1459247088_502x234.jpg" border="0" alt="This Is How Legendary Footballer David Beckham Spends His Retirement, Stitching Dresses For His Daughter's Dolls" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/the-lighter-side-of-david-beckham-shown-in-a-photo-where-he-is-stitching-a-dress-for-his-daughter-s-doll-252692.html" title="This Is How Legendary Footballer David Beckham Spends His Retirement, Stitching Dresses For His Daughter's Dolls">
    This Is How Legendary Footballer David Beckham Spends His Retirement, Stitching Dresses For His Daughter's Dolls                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
        <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/celebs/this-short-film-is-a-proof-that-true-love-is-all-about-conversations-hot-chai-biskoots-252689.html'>video</a>                <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/this-short-film-is-a-proof-that-true-love-is-all-about-conversations-hot-chai-biskoots-252689.html" class="tint" title="This Short Film Is A Proof That True Love Is All About Conversations, Hot Chai & Biskoots!">
                    <img  src="http://media.indiatimes.in/media/videocafe/2016/Mar/a123_1459245484_1459245490_502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Mar/a123_1459245484_1459245490_502x234.jpg" border="0" alt="This Short Film Is A Proof That True Love Is All About Conversations, Hot Chai & Biskoots!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/this-short-film-is-a-proof-that-true-love-is-all-about-conversations-hot-chai-biskoots-252689.html" title="This Short Film Is A Proof That True Love Is All About Conversations, Hot Chai & Biskoots!">
    This Short Film Is A Proof That True Love Is All About Conversations, Hot Chai & Biskoots!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/mowgli-bidding-goodbye-to-raksha-in-this-new-jungle-book-clip-will-leave-you-sobbing-252678.html" class="tint" title="Mowgli Bidding Goodbye To Raksha In This New Jungle Book Clip Will Leave You Sobbing!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/mowgli1_1459237810_1459237816_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/mowgli1_1459237810_1459237816_502x234.jpg" border="0" alt="Mowgli Bidding Goodbye To Raksha In This New Jungle Book Clip Will Leave You Sobbing!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/mowgli-bidding-goodbye-to-raksha-in-this-new-jungle-book-clip-will-leave-you-sobbing-252678.html" title="Mowgli Bidding Goodbye To Raksha In This New Jungle Book Clip Will Leave You Sobbing!">
    Mowgli Bidding Goodbye To Raksha In This New Jungle Book Clip Will Leave You Sobbing!                    </a>
                </div>    
            </figcaption>
        </div>
    
</div><!--life-panel end-->

<div class="trending-panel cf "><!--trending-panel start-->
    <h2><a target="_blank" href="http://www.indiatimes.com/trending/">trending</a></h2>
                    <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                    <figure>
      
    					
                <a href="http://www.indiatimes.com/lifestyle/technology/life-just-got-a-bit-more-dangerous-man-invents-a-gun-that-disguises-itself-as-a-smartphone-252732.html" class="tint" title="Life Just Got A Bit More Dangerous. Man Invents A Gun That Disguises Itself As A Smartphone!">

                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/640x299_1459323351_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/640x299_1459323351_236x111.jpg" border="0" alt="Life Just Got A Bit More Dangerous. Man Invents A Gun That Disguises Itself As A Smartphone!"/>
                </a>
            </figure>
            <figcaption>
            <div class="white-container">
                <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/technology/life-just-got-a-bit-more-dangerous-man-invents-a-gun-that-disguises-itself-as-a-smartphone-252732.html" title="Life Just Got A Bit More Dangerous. Man Invents A Gun That Disguises Itself As A Smartphone!">
    Life Just Got A Bit More Dangerous. Man Invents A Gun That Disguises Itself As A Smartphone!                </a>
            </div>
            </figcaption>
        </div><!--trending-panel-list end-->
    <div class='container3'>        <div class="trending-panel-list cf colombia" id="div-clmb-ctn-129061-10"  data-slot="129061" data-position="10" data-section="0" data-cb="adwidgetNew"><!--trending-panel-list start-->
                    <figure>
      
    					
                <a href="http://www.indiatimes.com/play/slog-overs/injured-yuvraj-singh-ruled-out-of-world-t20-here-are-three-guys-who-can-replace-him-in-semis-vs-wi-252762.html" class="tint" title="Injured Yuvraj Singh Ruled Out Of World T20. Here Are Three Guys Who Can Replace Him In Semis Vs WI">

                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/yuvrajinjuredreuters_1459333684_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/yuvrajinjuredreuters_1459333684_236x111.jpg" border="0" alt="Injured Yuvraj Singh Ruled Out Of World T20. Here Are Three Guys Who Can Replace Him In Semis Vs WI"/>
                </a>
            </figure>
            <figcaption>
            <div class="white-container">
                <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/injured-yuvraj-singh-ruled-out-of-world-t20-here-are-three-guys-who-can-replace-him-in-semis-vs-wi-252762.html" title="Injured Yuvraj Singh Ruled Out Of World T20. Here Are Three Guys Who Can Replace Him In Semis Vs WI">
    Injured Yuvraj Singh Ruled Out Of World T20. Here Are Three Guys Who Can Replace Him In Semis Vs WI                </a>
            </div>
            </figcaption>
        </div><!--trending-panel-list end-->
            <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                    <figure>
      
    					
                <a href="http://www.indiatimes.com/entertainment/celebs/confirmed-karan-singh-grover-bipasha-basu-are-tying-the-knot-next-month-here-s-all-the-dope-252757.html" class="tint" title="Confirmed! Karan Singh Grover & Bipasha Basu Are Tying The Knot Next Month. Here's All The Dope">

                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/wedding1_1459333231_1459333237_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/wedding1_1459333231_1459333237_236x111.jpg" border="0" alt="Confirmed! Karan Singh Grover & Bipasha Basu Are Tying The Knot Next Month. Here's All The Dope"/>
                </a>
            </figure>
            <figcaption>
            <div class="white-container">
                <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/confirmed-karan-singh-grover-bipasha-basu-are-tying-the-knot-next-month-here-s-all-the-dope-252757.html" title="Confirmed! Karan Singh Grover & Bipasha Basu Are Tying The Knot Next Month. Here's All The Dope">
    Confirmed! Karan Singh Grover & Bipasha Basu Are Tying The Knot Next Month. Here's All The Dope                </a>
            </div>
            </figcaption>
        </div><!--trending-panel-list end-->
            <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                    <figure>
      
    					
                <a href="http://www.indiatimes.com/entertainment/bollywood/move-over-arjun-ranveer-singh-to-bromance-with-ranbir-kapoor-in-zoya-akhtar-s-next-252741.html" class="tint" title="Move Over Arjun, Ranveer Singh To Bromance With Ranbir Kapoor In Zoya Akhtar's Next!">

                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/ccrd_1459325291_1459325296_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/ccrd_1459325291_1459325296_236x111.jpg" border="0" alt="Move Over Arjun, Ranveer Singh To Bromance With Ranbir Kapoor In Zoya Akhtar's Next!"/>
                </a>
            </figure>
            <figcaption>
            <div class="white-container">
                <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/move-over-arjun-ranveer-singh-to-bromance-with-ranbir-kapoor-in-zoya-akhtar-s-next-252741.html" title="Move Over Arjun, Ranveer Singh To Bromance With Ranbir Kapoor In Zoya Akhtar's Next!">
    Move Over Arjun, Ranveer Singh To Bromance With Ranbir Kapoor In Zoya Akhtar's Next!                </a>
            </div>
            </figcaption>
        </div><!--trending-panel-list end-->
    
</div>

</div><!--trending-panel end-->

</section>
<section class="container cf" id="loader" style="display: none;border: 0px solid red;">
    <div class="news-panel cf ">&nbsp;</div>
    <div class="life-panel cf" style="text-align: center;"><img src="http://media.indiatimes.in/resources/images/loading.gif" border="0" /></div>
    <div class="trending-panel cf ">&nbsp;</div>
</section>
<div class="remove-fixed-home">&nbsp;</div>
<section class="big-ads" id="adfooter">
    <div id="badRos_slot"></div> 
</section>   

<!------------------------ end Block 4------------------------------------------------>
<!--container end-->

<script type="text/javascript">

    $('#bigAd1_slot').bind('inview', function (event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            // element is now visible in the viewport
            showBigAD2('bigAd2_slot');
            if (visiblePartY == 'top') {
                // top part of element is visible
            } else if (visiblePartY == 'bottom') {
                // bottom part of element is visible     

            } else {
                // whole part of element is visible
            }
        } else {
            // element has gone out of viewport
        }
    });
    $('#bigAd2_slot').bind('inview', function (event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            // element is now visible in the viewport
            showBigAD3('bigAd3_slot');
            if (visiblePartY == 'top') {
                // top part of element is visible

            } else if (visiblePartY == 'bottom') {
                // bottom part of element is visible  

            } else {
                // whole part of element is visible
            }
        } else {
            // element has gone out of viewport
        }
    });
    $('#bigAd3_slot').bind('inview', function (event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            // element is now visible in the viewport
            BADros('badRos_slot');
            if (visiblePartY == 'top') {
                // top part of element is visible
            } else if (visiblePartY == 'bottom') {
                // bottom part of element is visible
            } else {
                // whole part of element is visible
            }
        } else {
            // element has gone out of viewport
        }
    });

    $(document).ready(function () {
        /* spotlight onload tracking homepage */
        //console.log("homepage");
            ga('send', 'event', 'OnLoad Partner Stories', '252841', 'homepage', {'nonInteraction': 1});
            ga('send', 'event', 'OnLoad Partner Stories', '252721', 'homepage', {'nonInteraction': 1});
            ga('send', 'event', 'OnLoad Partner Stories', '252727', 'homepage', {'nonInteraction': 1});
    });

</script>    <div class="clr"></div>

	
    <script>
                showFooterCode();
        </script>
<style>
    .bottom-strip{background:#ebebeb;display:block;padding:15px 0;overflow:hidden;}.bottom-strip-box{margin:0 auto;display:table;max-width:410px;width:100%;}.square-logo{width:66px;height:64px;float:left;margin-right:10px;}.bottom-strip-content{float:left;margin-top:14px;}.bottom-strip-content span{display:block;color:#737272;font:12px/1 "RobotoRegular", Arial, Helvetica, sans-serif;margin-bottom:2px;}.bottom-strip-content a{color:#1b85dd;font:24px/1 "RobotoRegular", Arial, Helvetica, sans-serif;text-decoration:none;}
</style>
<div class="last-container">
    <!--131<br>News--> 
    <!--social-panel start-->
    <div class="social-panel red-bg">
        <div class="soc-inner"><!--soc-inner start-->
            <p class="soc-title">Be a Part of The New & Next</p>
            <div class="soc-count"><!--soc-count start-->
                <a href="https://www.facebook.com/indiatimes" target="_blank" title="facebook" class="sprite soc-fb"></a>
                <p>
                    4,730,461<span class="black"> FRIENDS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://twitter.com/indiatimes" target="_blank" title="twitter" class="sprite soc-twt"></a>
                <p>10343  <span class="black">FOLLOWERS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://plus.google.com/+indiatimes" target="_blank" title="gplus" class="sprite soc-gplus"></a>
                <p>104,163 <span class="black">MEMBERS</span></p>
            </div>
            <div class="soc-count"><!--soc-count start-->
                <a href="javascript:void(0);" title="E-mail" class="sprite soc-mail" id="subscribers_id"></a>
                <p>60,390 <span class="black">SUBSCRIBERS</span></p>
            </div>
            <!--soc-count end-->
        </div>
        <!--soc-inner end-->
    </div>
    <!--social-panel end-->
    <div class="clr"></div>

    <footer>

        <div class="footer"><!-- footer grey start-->
            <div class="sub_container">
                <!-- container start-->
                <div class="cont_new">
                    <h2>Our Channels</h2>
                                                    <a href='http://www.indiatimes.com/news/' class="blue size">News</a> 
                                                            <a href='http://www.indiatimes.com/lifestyle/' class="purple size">Lifestyle</a> 
                                                            <a href='http://www.indiatimes.com/entertainment/' class="red size">Entertainment</a> 
                                                            <a href='http://www.indiatimes.com/health/' class="green size">Health</a> 
                                                            <a href='http://www.indiatimes.com/videocafe/' class="orange size">Videos</a> 
                                                            <a href='http://www.indiatimes.com/play/' class="yellow size">Slog Overs</a> 
                                                <a href='http://www.indiatimes.com/trending' class="size" style="color:white">Trending</a> 
                </div><!-- container closed-->

                <div class="cont"> <!-- container start-->
                    <h2>Sections</h2>
                    						
                            <div class="sub_link">

                     
                                        <a href='http://www.indiatimes.com/news/india/'>india</a>
                         
                                        <a href='http://www.indiatimes.com/news/world/'>world</a>
                         
                                        <a href='http://www.indiatimes.com/news/sports/'>sports</a>
                         
                                        <a href='http://www.indiatimes.com/news/weird/'>weird</a>
            
                            </div>						
                            <div class="sub_link">

                     
                                        <a href='http://www.indiatimes.com/lifestyle/self/'>self</a>
                         
                                        <a href='http://www.indiatimes.com/lifestyle/style/'>style</a>
                         
                                        <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                         
                                        <a href='http://www.indiatimes.com/culture/who-we-are/'>who we are</a>
                         
                                        <a href='http://www.indiatimes.com/culture/travel/'>travel</a>
                         
                                        <a href='http://www.indiatimes.com/culture/food/'>food</a>
            
                            </div>						
                            <div class="sub_link">

                     
                                        <a href='http://www.indiatimes.com/entertainment/bollywood/'>bollywood</a>
                         
                                        <a href='http://www.indiatimes.com/entertainment/celebs/'>celebscoop</a>
                         
                                        <a href='http://www.indiatimes.com/entertainment/hollywood/'>hollywood</a>
            
                            </div>						
                            <div class="sub_link">

                     
                                        <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                         
                                        <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                         
                                        <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                         
                                        <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                         
                                        <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
            
                            </div>                    <div class="sub_link"><a href="http://www.indiatimes.com/photogallery">Photogallery</a></div>
                </div><!-- container closed-->
                <div class="cont_new"> <!-- container start-->
                    <h2>Indiatimes Lifestyle Network</h2>

                    <div class="sub_link">                             <a href="http://www.mensxp.com"   target="_blank" >MENS XP</a>
                                                    <a href="http://www.idiva.com"   target="_blank" >iDiva</a>
                                                    <a href="http://luxpresso.com"   target="_blank" >Luxury</a>
                                                    <a href="http://in.askmen.com/"   target="_blank" >Ask Men</a>
                                                    <a href="http://www.pursuitist.in/"   target="_blank" >Pursuitist</a>
                        </div>
                </div><!-- container closed-->
                <div class="cont"> <!-- container start-->
                    <h2>Other Sites</h2>
                    <div class="sub_link"> 

                            <a href="http://timesofindia.indiatimes.com/budgetspecial.cms"  target="_blank">Budget 2016</a> 
                        
                            <a href="http://timesofindia.indiatimes.com/budget-2016/rail-budget-2016/indiabudget/50867848.cms"  target="_blank">Rail Budget 2016</a> 
                        
                            <a href="http://www.cricbuzz.com"  target="_blank">CricBuzz</a> 
                        
                            <a href="http://www.happytrips.com/"  target="_blank">Happy Trips</a> 
                        
                            <a href="http://www.businessinsider.in/"  target="_blank">Business Insider</a> 
                        
                            <a href="http://www.zoomtv.com"  target="_blank">ZoomTv</a> 
                        
                            <a href="http://www.gizmodo.in/"  target="_blank">Gizmodo</a> 
                        
                            <a href="http://www.lifehacker.co.in/"  target="_blank">Lifehacker</a> 
                        
                            <a href="http://in.ign.com/"  target="_blank">IGN</a> 
                        
                            <a href="http://boxtv.com"  target="_blank">BoxTV</a> 
                        
                            <a href="http://www.gaana.com"  target="_blank">Gaana</a> 
                        
                            <a href="http://shopping.indiatimes.com"  target="_blank">Online Shopping</a> 
                        
                            <a href="http://www.in.techradar.com/"  target="_blank">Techradar</a> 
                        
                            <a href="http://timesdeal.com"  target="_blank">Daily Deals</a> 
                        
                            <a href="http://www.gitanjalishop.com/"  target="_blank">Gitanjali Shop</a> 
                        
                            <a href="http://www.satvikshop.com/"  target="_blank">Satvik Shop</a> 
                        
                            <a href="http://whatshot.in"  target="_blank">WHAT'S HOT</a> 
                        
                            <a href="http://zigwheels.com"  target="_blank">ZigWheels</a> 
                        
                            <a href="http://filmipop.com"  target="_blank">FILMIPOP</a> 
                        
                            <a href="http://www.filmfare.com/"  target="_blank">Filmfare</a> 
                        
                            <a href="http://www.femina.in"  target="_blank">Femina</a> 
                        
                            <a href="http://mobile.indiatimes.com"  target="_blank">Mobile</a> 
                        
                            <a href="http://www.greetzap.com"  target="_blank">Greetings</a> 
                        
                            <a href="http://www.follo.co.in"  target="_blank">Follo</a> 
                        
                            <a href="http://timesjobs.com"  target="_blank">Jobs</a> 
                        
                            <a href="http://magicbricks.com"  target="_blank">Property</a> 
                        
                            <a href="http://www.astrospeak.com"  target="_blank">Astrology</a> 
                        
                            <a href="http://speakingtree.in"  target="_blank">Speaking Tree</a> 
                        
                            <a href="http://simplymarry.com"  target="_blank">Matrimonial</a> 
                        
                            <a href="http://itimes.com"  target="_blank">iTimes</a> 
                        
                            <a href="http://www.ads2book.com/"  target="_blank">Ads2book</a> 
                        
                            <a href="http://www.tcnext.com/"  target="_blank">TcNext</a> 
                        
                            <a href="http://www.romedynow.com"  target="_blank">Romedy Now</a> 
                                            </div>
                </div><!-- container closed-->
                <div class="cont border_none"><!-- container start-->
                    <h2>About us</h2>
                    <div class="sub_link">
                            <a href='http://www.indiatimes.com/privacypolicy' >Privacy Policy</a> 
                            <a href='http://www.indiatimes.com/sitemap.html' >sitemap</a> 
                            <a href='http://www.indiatimes.com/termsandcondition' >Terms & CONDITIONS</a> 
                            <a href='http://www.indiatimes.com/seoarchive' >ARCHIVES</a> 
                            <a href='http://www.indiatimes.com/contactus' >Contact us</a> 
                            <a href='http://www.indiatimes.com/aboutus' >ABOUT INDIATIMES</a> 
                    </div>
                </div><!-- container closed-->
            </div><!-- footer grey closed-->
        </div><!-- footer black closed-->

        <div class="black_footer"><!-- footer black start-->
            <div class="containers">
                <p><a href="javascript:void(0)" class="copyrt">Copyright Â© 2016 Times Internet Limited. Powered by Indiatimes Lifestyle Network. All rights reserved</a></p>
                <span><a href="http://www.indiatimes.com/seoarchive/">Archive</a> | <a href="http://www.indiatimes.com/sitemap.html">Sitemap</a> | <a href="http://www.indiatimes.com/aboutus/">About Us</a> | <a href="http://www.indiatimes.com/contactus/">Contact Us</a> | <a href="http://www.indiatimes.com/privacypolicy/">Privacy Policy</a> | <a target="_blank" href="http://m.indiatimes.com">Indiatimes Mobile</a> | <a href="http://www.indiatimes.com/termsandcondition/">Terms of Use and Grievance Redressal Policy </a> </span> 
            </div>
        </div><!-- footer black end-->
    </footer>
</div>
<script>
    $(document).ready(function () {
        $("#subscribers_id").click(function () {
            $("#UserEmail").focus();
        });
    });
    function socialRHSHide() {}
	

</script>

    
<script defer type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.easing.1.3.min.js?v=118.1"></script>

<input type="hidden" name="currentId" id="currentId" value="">
    <div class="animate_cont dawn floating delay5">
        <span class="animate_arrow sprite_image"></span>
    </div>




<script type="text/javascript" src="http://media.indiatimes.in/resources/js/main.js?v=118.1"></script>    
<script type="text/javascript" src="http://media.indiatimes.in/resources/js/common.js?v=118.1"></script>
<script  defer type="text/javascript" src="http://media.indiatimes.in/resources/js/jquery-scrolltofixed.js?v=118.1"></script>

<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://cdn.mouseflow.com/projects/1e238441-c30e-4d1b-8edd-c0f14d8fa6b7.js' type='text/javascript'%3E%3C/script%3E"));</script>
<!-- mouse flow code ends-->
<script  src="http://tags.crwdcntrl.net/c/2818/cc.js?ns=_cc2818" id="LOTCC_2818"></script> 

<script> 
    _cc2818.bcp(); 
</script>

	 <p id="back-top" style="display:none;"><a href="#top"><span></span></a> </p>
</div><!--wrap end-->


</body>
</html>