



<!DOCTYPE html>

<!--[if IE]><![endif]--><html><head><meta charset="utf-8" /><meta http-equiv="Content-Type" content="text/html;charset=UTF-8" /><title>globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento</title><meta name="google-site-verification" content="BKmmuVQac1JM6sKlj3IoXQvffyIRJvJfbicMouA2a88" /><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.5" /><meta property="og:title" content="globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento"/><meta property="og:type" content="website"/><meta property="og:url" content="http://www.globo.com/"/><meta property="og:image" content="http://s.glbimg.com/en/ho/static/globocom2012/img/fb_marca.png"/><meta property="og:site_name" content="globo.com"/><meta property="og:description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta property="fb:page_id" content="224969370851736" /><meta property='busca:title' content='Globo.com' /><meta property='busca:species' content='Home' /><meta property='busca:issued' content='30/12/2015 16:44:07' /><meta property='busca:modified' content='30/12/2015 16:44:07' /><meta property='busca:publisher' content='www.globo.com' /><meta name="description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta name="keywords" content="NotÃ­cias, Entretenimento, Esporte, Tecnologia, Portal, ConteÃºdo, Rede Globo, TV Globo, VÃ­deos, TelevisÃ£o"/><meta name="application-name" content="Globo.com"/><meta name="msapplication-TileColor" content="#0669DE"/><meta name="msapplication-TileImage" content="http://s.glbimg.com/en/ho/static/globocom2012/img/globo-win-tile.png"/><link rel="canonical" href="http://www.globo.com/" /><link rel="shortcut icon" href="http://s.glbimg.com/en/ho/static/globocom2012/img/favicon.png" type="image/ico" /><link rel="apple-touch-icon-precomposed" sizes="57x57" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone.png" /><link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-ipad.png" /><link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone-retina.png" /><link rel="stylesheet" href="http://s.glbimg.com/en/ho/static/CACHE/css/496b20dbaace.css" type="text/css" media="screen" /><!--[if lt IE 10]><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/d783f8b1c48f.js"></script><![endif]--><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/87daf046b449.js"></script></head><!--[if lt IE 7 ]><body class="ie ie6"><![endif]--><!--[if IE 7 ]><body class="ie ie7"><![endif]--><!--[if IE 8 ]><body class="ie ie8"><![endif]--><!--[if IE 9 ]><body class="ie ie9gt ie9"><![endif]--><!--[if gt IE 9 ]><body class="ie ie9gt ie10gt"><![endif]--><!--[if !IE]><!--><body><!--<![endif]--><div id="home-push-menu"><div class="home-push-menu-container"><div class="home-push-menu-initial"><div class="home-push-menu-header">NAVEGUE</div><ul class="barra-itens" data-analytics-context="Link Produto"><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://g1.globo.com" accesskey="n" class="barra-item-g1 link-produto analytics-area analytics-id-T">g1</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://globoesporte.globo.com" accesskey="e" class="barra-item-globoesporte link-produto analytics-area analytics-id-T">globoesporte</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://gshow.globo.com" accesskey="t" class="barra-item-gshow link-produto analytics-area analytics-id-T">gshow</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://famosos.globo.com" accesskey="t" class="barra-item-famosos-etc link-produto analytics-area analytics-id-T">famosos &amp; etc</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://techtudo.com.br" accesskey="b" class="barra-item-tech link-produto analytics-area analytics-id-T">tecnologia</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://video.globo.com" accesskey="v" class="barra-item-videos link-produto analytics-area analytics-id-T">vÃ­deos</a></li><li><a href="#" id="home-push-menu-show-email">e-mail</a><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="2.423,0 0,2.399 5.153,7.5 0,12.602 2.423,15 10,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-right.png"></image></svg></span></li><li><a href="https://meuperfil.globo.com/">central globo.com</a></li></ul></div><div class="home-push-menu-email"><ul><li><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="7.577,0 10,2.399 4.847,7.5 10,12.602 7.577,15 0,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-left.png"></image></svg></span><a id="home-push-menu-hide-email" href="#">e-mail</a></li><li><a href="https://login.globo.com/login/1948">globomail free</a></li><li><a href="https://login.globo.com/login/1">globomail pro</a></li></ul></div></div></div><div id="home-pagecontent" class=" home-menu-rendered" style=""><script type="text/javascript">
function comScore(C){var A="comScore",j=document,y=j.location,B="",z="undefined",x=2048,D,v,i,w,c="characterSet",l="defaultCharset",u=(typeof encodeURIComponent!=z?encodeURIComponent:escape);if(j.cookie.indexOf(A+"=")!=-1){i=j.cookie.split(";");for(w=0,y=i.length;w<y;w++){var d=i[w].indexOf(A+"=");if(d!=-1){B="&"+unescape(i[w].substring(d+A.length+1))}}}C=C+"&ns__t="+(new Date().getTime());C=C+"&ns_c="+(j[c]?j[c]:(j[l]?j[l]:""))+"&c8="+u(j.title)+B+"&c7="+u(y&&y.href?y.href:j.URL)+"&c9="+u(j.referrer);if(C.length>x&&C.indexOf("&")>0){D=C.substr(0,x-8).lastIndexOf("&");C=(C.substring(0,D)+"&ns_cut="+u(C.substring(D+1))).substr(0,x)}if(j.images){v=new Image();if(typeof ns_p==z){ns_p=v}v.src=C}else{j.write('<p><img src="'+C+'" height="1" width="1" alt="*"/></p>')}}comScore("http"+(document.location.href.charAt(4)=="s"?"s://sb":"://b")+".scorecardresearch.com/p?c1=2&c2=6035227");</script><noscript><p><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6035227&amp;c4=http%3A%2F%2Fwww.globo.com%2Findex.html" height="1" width="1" alt="*"/></p></noscript><script>
libby.loadScript('http://b.scorecardresearch.com/c2/6035227/cs.js');</script><script type="text/javascript">
var utag_data = {"structure_tree": "[\"globocom\"]", "ad_site_page": "{\"adUnit\":\"tvg_Globo.com.Home\", \"adPositionsDesktop\": [\"banner_slim_topo\",\"banner_slb_meio\",\"banner_slb_fim\",\"banner_selo4\",\"banner_floating\"], \"adPositionsMobile\":[\"banner_mobile_topo\",\"banner_mobile_meio\",\"banner_mobile_fim\"]}", "page_name": "index"};</script><script type="text/javascript">
(function (a,b,c,d) {a = '//tags.globo.com/utag/globo/home/prod/utag.js';b=document;c = 'script';d=b.createElement(c);d.src=a;d.type = 'text/java' + c;d.async=true;a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);})();
</script><header><div id="base-container-width-element" class="container"><div class="header-mobile-top analytics-area analytics-id-H"><a id="open-menu" class="open-menu" href="#"><svg version="1.1" class="burger-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19px" height="13px" viewBox="0 0 19 13" enable-background="new 0 0 19 13" xml:space="preserve"><g><path fill="#1063E0" d="M17.493,5H1.485C0.665,5,0,5.665,0,6.485C0,7.305,0.665,7.97,1.485,7.97h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,5.665,18.313,5,17.493,5z M1.485,2.971h16.008c0.82,0,1.486-0.666,1.486-1.486C18.979,0.666,18.313,0,17.493,0H1.485C0.665,0,0,0.666,0,1.484C0,2.305,0.665,2.971,1.485,2.971z M17.493,10H1.485C0.665,10,0,10.666,0,11.485c0,0.82,0.665,1.485,1.485,1.485h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,10.666,18.313,10,17.493,10z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/menu-button.png"></image></svg></a><a class="logo-globo analytics-area analytics-id-T" href="http://globo.com" title="globo.com"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 98 22" enable-background="new 0 0 98 22" xml:space="preserve"><g><path fill="#1063E0" d="M92.982,5.033c-1.115,0-2.549,0.601-3.459,1.625c-0.865-1.069-2.026-1.625-3.392-1.625c-1.205,0-2.252,0.489-3.049,1.336c-0.114-0.757-0.661-1.203-1.502-1.203c-0.957,0-1.639,0.712-1.639,1.781v8.551c0,1.069,0.682,1.781,1.639,1.781s1.638-0.712,1.638-1.781V9.909c0-1.292,0.592-2.205,1.936-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.682,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.909c0-1.292,0.728-2.205,1.934-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.683,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.352C97.466,6.547,95.645,5.033,92.982,5.033z M10.036,5.166c-0.91,0-1.434,0.49-1.639,1.336c-0.614-0.98-1.889-1.469-3.004-1.469C2.093,5.033,0,8.039,0,11.268c0,3.184,1.616,6.146,5.052,6.146c1.502,0,2.617-0.535,3.299-1.515h0.045v0.757c0,1.625-1.183,2.538-2.776,2.538c-1.889,0-3.049-1.001-4.097-1.001c-0.637,0-1.319,0.8-1.319,1.403C0.205,21.354,4.028,22,5.371,22c3.732,0,6.304-1.938,6.304-5.791V6.947C11.674,5.878,10.991,5.166,10.036,5.166z M5.826,14.474c-1.707,0-2.549-1.559-2.549-3.273c0-1.492,0.842-3.229,2.549-3.229c1.797,0,2.571,1.781,2.571,3.339C8.397,12.871,7.578,14.474,5.826,14.474z M14.318,0c-0.956,0-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781s1.639-0.712,1.639-1.781V1.782C15.957,0.712,15.274,0,14.318,0z M22.479,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169c3.527,0,5.872-2.829,5.872-6.169C28.35,7.994,25.938,5.033,22.479,5.033z M22.479,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.797-3.273,2.594-3.273c1.798,0,2.594,1.737,2.594,3.273C25.073,12.782,24.254,14.474,22.479,14.474z M35.371,5.033c-1.138,0-2.253,0.378-3.118,1.113V1.782C32.253,0.712,31.57,0,30.614,0s-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781c0.911,0,1.434-0.49,1.639-1.336c0.614,0.979,1.889,1.471,3.004,1.471c3.3,0,5.393-3.008,5.393-6.036C40.65,8.261,38.897,5.033,35.371,5.033z M34.825,14.474c-1.798,0-2.572-1.781-2.572-3.34c0-1.559,0.819-3.162,2.572-3.162c1.707,0,2.548,1.759,2.548,3.273C37.373,12.737,36.531,14.474,34.825,14.474z M46.863,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169s5.872-2.829,5.872-6.169C52.735,7.994,50.322,5.033,46.863,5.033z M46.863,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273s2.594,1.737,2.594,3.273C49.458,12.782,48.638,14.474,46.863,14.474z M55.425,13.939c-0.978,0-1.774,0.779-1.774,1.736s0.796,1.738,1.774,1.738c0.979,0,1.775-0.781,1.775-1.738S56.404,13.939,55.425,13.939z M66.583,13.805c-0.386,0-1.616,0.669-2.436,0.669c-1.774,0-2.753-1.648-2.753-3.229c0-1.648,0.888-3.273,2.753-3.273c1.048,0,1.776,0.623,2.594,0.623c0.775,0,1.253-0.868,1.253-1.536c0-1.56-2.709-2.026-4.006-2.026c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.343,6.169,5.871,6.169c1.184,0,4.006-0.558,4.006-2.206C67.994,14.451,67.355,13.805,66.583,13.805z M73.446,5.033c-3.459,0-5.872,2.961-5.872,6.212c0,3.34,2.344,6.169,5.872,6.169c3.527,0,5.871-2.829,5.871-6.169C79.317,7.994,76.904,5.033,73.446,5.033z M73.446,14.474c-1.774,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273c1.797,0,2.594,1.737,2.594,3.273C76.04,12.782,75.221,14.474,73.446,14.474z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/logo.png"></image></svg></a><form id="search-form" action="http://www.globo.com/busca/" method="get"><div id="search-container" class="search-container"><span><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="12px" height="12px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><g><path fill="#999999" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/search-small.png"></image></svg></span><div class="search-internal-container"><input type="text" id="search-globo" name="q" placeholder="encontre na globo.com"></div><a id="close-search" class="close-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="11px" height="11px" viewBox="0 0 11 11" enable-background="new 0 0 11 11" xml:space="preserve"><g><path fill="#666766" d="M6.884,5.501c1.955-1.955,3.733-3.732,3.809-3.808c0.388-0.388,0.388-1.015,0-1.403 c-0.387-0.386-1.015-0.386-1.402,0C9.215,0.365,7.437,2.144,5.482,4.099c-1.947-1.947-3.714-3.714-3.79-3.79 c-0.387-0.388-1.015-0.388-1.402,0c-0.387,0.387-0.387,1.016,0,1.401c0.076,0.076,1.842,1.843,3.79,3.791 C2.135,7.446,0.372,9.209,0.297,9.285c-0.387,0.387-0.387,1.015,0,1.401c0.387,0.389,1.015,0.389,1.402,0 c0.075-0.075,1.838-1.838,3.784-3.783c1.953,1.952,3.727,3.727,3.802,3.802c0.387,0.387,1.015,0.387,1.402,0 c0.387-0.387,0.387-1.015,0-1.402C10.611,9.228,8.837,7.454,6.884,5.501z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/close.png"></image></svg></a></div><a id="open-search" class="open-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><path fill="#1063e0" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path><image src="http://s.glbimg.com/gl/ba/img/common/search.png"></image></svg></a></form></div><h1 class="analytics-area analytics-id-H"><a class="logo-topo analytics-area analytics-id-T" href="/" title="globo.com">
                globo.com
            </a></h1><div class="complementos analytics-area analytics-id-T"><div id="busca-padrao"><form action="http://www.globo.com/busca/" method="get" accept-charset="utf-8"><fieldset><legend>buscar</legend><label for="busca-campo" class="label-for-search">buscar</label><input id="busca-campo" type="text" name="q" autocomplete="off" accesskey="s" lang="pt-BR" x-webkit-speech speech /><button type="submit">buscar</button></fieldset></form></div><div id="libby-box-previsao-tempo" class="analytics-area analytics-id-L"></div></div><nav><div class="spacer"></div><header id="header-produto" class="header-navegacao header-home" data-analytics="T"><div id="glbbarrawidget"></div><div class="floating-bar"><div class="header-bar"><div class="grid-12 clearfix area-principal row"><div class="column"><div class="menu-area to-left" data-analytics="S"><div class="menu-button"><div class="burger"><b></b><b></b><b></b></div><span class="menu-label">MENU</span></div><span class="menu-produto"></span></div><a class="logo-area" href="http://www.globo.com/"><h1 class="logo icon-produto">Home</h1></a><div class="to-right"><div class="search-area"><form id="frmBuscaScroll" action="#"><input id="search-input-floating" type="text" name="q" placeholder="BUSCAR" class="search" /><label for="search-input-floating" class="glass-container"><div class="glass"><div class="iglass css3pie"></div></div></label></form></div></div></div></div></div></div><div class="cube-container"><div class="cube"><div class="face front"><div class="grid-12 clearfix area-principal row"><div class="column"><div class="menu-area to-left" data-analytics="S"><div class="menu-button"><div class="burger"><b></b><b></b><b></b></div><span class="menu-label">MENU</span></div><span class="menu-produto"></span></div><a class="logo-area" href="http://www.globo.com/"><h1 class="logo icon-produto">Home</h1></a><div class="to-right"><div class="search-area"><form id="frmBuscaScroll" action="#"><input id="search-input-top" type="text" name="q" placeholder="BUSCAR" class="search" /><label for="search-input-top" class="glass-container"><div class="glass"><div class="iglass css3pie"></div></div></label></form></div><div id="busca-padrao"></div></div></div></div></div></div></div></header><script>
(function(window,document) {try{new CustomEvent("test");} catch(e){var CustomEvent=function(event,params) {var evt;params=params || {bubbles: false,
cancelable: false,
detail: undefined};evt=document.createEvent("CustomEvent");evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);return evt;};
CustomEvent.prototype=window.Event.prototype;window.CustomEvent=CustomEvent;}
document.dispatchEvent(new CustomEvent('glb.headerDom.ready'));})(window,document);</script><style type="text/css">
    /*
       evita que o markup do menu seja exibido sem estilo enquanto
       ele nÃ£o foi carregado
    */
    #menu-container {
        display: none;
    }
    #menu-fonts {
        height: 1px;
        width: 1px;
        color: transparent;
        overflow: hidden;
        position: absolute;
    }
</style>

<nav id="menu-container" class="regua-navegacao-tab menu-no-animation">
    

<script id="menu-carousel-template" type="x-tmpl-mustache">

<div id="menu-carousel-header-sizes-infos" class="menu-carousel-header-sizes-infos">
</div>
<div id="menu-carousel-header" class="menu-carousel menu-carousel-header swiper-container">
    <ul class="menu-brands swiper-wrapper">
        {{#portals}}
        <li data-produto="{{name}}" class="menu-carousel-item menu-carousel-{{name}} {{#active}}menu-carousel-item-active{{/active}} swiper-slide">
            <a href="{{link}}" class="menu-carousel-link" data-index="{{index}}">
                <svg class="brand-icon">
                    <use class="menu-logo" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#eixo-icone-{{name}}"></use>
                </svg>
            </a>
            <div class="menu-carousel-link-shadow"></div>
        </li>
        {{/portals}}
    </ul>
</div>
<div id="menu-carousel-body" class="menu-carousel menu-carousel-body swiper-container">
    <ul class="menu-brands swiper-wrapper">
        {{#portals}}
        <li data-produto="{{name}}" class="menu-carousel-item menu-carousel-{{name}} {{#active}}menu-carousel-item-active{{/active}} swiper-slide">
            <div id="menu-rounder" class="menu-rounder">{{{menu}}}</div>
        </li>
        {{/portals}}
    </ul>
</div>

</script>

<script id="menu-rounder-template" type="x-tmpl-mustache">

  <div id="menu-cascade" class="menu-cascade" data-menu-index="{{index}}">
    <ul class="menu-root menu-level menu-level-0">{{#items}}{{>recursive_partial}}{{/items}}</ul>
  </div>
  <div id="menu-addon" class="menu-addon"></div>

</script>

<script id="menu-custom-template" type="x-tmpl-mustache">

    {{#items}}{{>recursive_partial}}{{/items}}

</script>


<script id="menu-dinamico-template" type="x-tmpl-mustache">

<li id="menu-{{deepness}}-{{tituloSlug}}" class="menu-item {{#separador}} menu-item-separator{{/separador}}{{#destacado}} menu-item-highlighted{{/destacado}}{{#agrupador}} menu-item-grouped{{/agrupador}}{{#hasChildren}} is-father{{/hasChildren}}{{#hasSubmenuBroken}} menu-item-submenu-broken{{/hasSubmenuBroken}}">

    {{#shouldBeLink}}<a href="{{link}}" class="menu-item-link">{{/shouldBeLink}}
    {{^shouldBeLink}}<span class="menu-item-link">{{/shouldBeLink}}
        <span class="menu-item-title">{{titulo}}<span class="menu-item-arrow"><svg class="regua-navegacao-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#regua-arrow"></use></svg></span></span>
    {{#shouldBeLink}}</a>{{/shouldBeLink}}
    {{^shouldBeLink}}</span>{{/shouldBeLink}}

    {{#hasChildren}}
        <div class="menu-level menu-submenu menu-submenu-level{{deepness}}{{#hasSubmenuBroken}} menu-item-submenu-broken{{/hasSubmenuBroken}} menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back"><span class="menu-item-arrow"><svg class="regua-navegacao-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#regua-arrow"></use></svg></span><span class="regua-navegacao-menu-item">{{titulo}}</span></a>
            <ul class="menu-submenu-vertical-scroll">
            {{#children}}
                {{>recursive_partial}}
            {{/children}}
            <ul>
        </div>
    {{/hasChildren}}
</li>

</script>


    <div id="menu-rounder" class="menu-rounder">
        <div id="menu-cascade" class="menu-cascade" data-menu-index="0">
            

<ul class="menu-root menu-level menu-level-0">
    
        




<li class="menu-item is-father " id="menu-1-famosos">

    <a class="menu-item-link">
        <span class="menu-item-title">Famosos</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Famosos</a>
            
                

                




<li class="menu-item " id="menu-2-ego">

    <a href="http://ego.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-paparazzo">

    <a href="http://ego.globo.com/paparazzo/index.html" class="menu-item-link">
        <span class="menu-item-title">Paparazzo</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-patricia-kogut">

    <a href="http://kogut.oglobo.globo.com/noticias-da-tv/index.html" class="menu-item-link">
        <span class="menu-item-title">Patricia Kogut</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-quem">

    <a href="http://revistaquem.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Quem</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-retratos-da-vida">

    <a href="http://extra.globo.com/famosos/" class="menu-item-link">
        <span class="menu-item-title">Retratos da Vida</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-moda-estilo">

    <a class="menu-item-link">
        <span class="menu-item-title">Moda &amp; Estilo</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Moda &amp; Estilo</a>
            
                

                




<li class="menu-item " id="menu-2-ela-no-o-globo">

    <a href="http://ela.oglobo.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Ela no O Globo</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-estilo-no-gshow">

    <a href="http://gshow.globo.com/Estilo/" class="menu-item-link">
        <span class="menu-item-title">Estilo no Gshow</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-moda-no-ego">

    <a href="http://ego.globo.com/moda/index.html" class="menu-item-link">
        <span class="menu-item-title">Moda no Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-moda-no-gnt">

    <a href="http://gnt.globo.com/moda/" class="menu-item-link">
        <span class="menu-item-title">Moda no GNT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-glamour">

    <a href="http://revistaglamour.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Glamour</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-gq-brasil">

    <a href="http://gq.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista GQ Brasil</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-marie-claire">

    <a href="http://revistamarieclaire.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Marie Claire</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-vogue">

    <a href="http://vogue.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Vogue</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-bem-estar-saude">

    <a class="menu-item-link">
        <span class="menu-item-title">Bem-Estar &amp; SaÃºde</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Bem-Estar &amp; SaÃºde</a>
            
                

                




<li class="menu-item " id="menu-2-beleza-no-ego">

    <a href="http://ego.globo.com/beleza/index.html" class="menu-item-link">
        <span class="menu-item-title">Beleza no Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-beleza-no-gnt">

    <a href="http://gnt.globo.com/beleza/" class="menu-item-link">
        <span class="menu-item-title">Beleza no GNT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-beleza-no-gshow">

    <a href="http://gshow.globo.com/Estilo/Beleza/" class="menu-item-link">
        <span class="menu-item-title">Beleza no Gshow</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-bem-estar-no-g1">

    <a href="http://g1.globo.com/bemestar/" class="menu-item-link">
        <span class="menu-item-title">Bem Estar no G1</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-bem-estar-no-gnt">

    <a href="http://gnt.com.br/bemestar" class="menu-item-link">
        <span class="menu-item-title">Bem Estar no GNT </span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-horoscopo-no-ego">

    <a href="http://horoscopo.ego.globo.com/" class="menu-item-link">
        <span class="menu-item-title">HorÃ³scopo no Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-maes-no-gnt">

    <a href="http://gnt.globo.com/maes-e-filhos/" class="menu-item-link">
        <span class="menu-item-title">MÃ£es no GNT </span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-meus-5-minutos">

    <a href="http://meus5minutos.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Meus 5 Minutos</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-crescer">

    <a href="http://revistacrescer.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Crescer</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-casa-decoracao">

    <a class="menu-item-link">
        <span class="menu-item-title">Casa &amp; DecoraÃ§Ã£o</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Casa &amp; DecoraÃ§Ã£o</a>
            
                

                




<li class="menu-item " id="menu-2-casa-no-gnt">

    <a href="http://gnt.globo.com/casa-e-decoracao/" class="menu-item-link">
        <span class="menu-item-title">Casa no GNT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-casa-vogue">

    <a href="http://casavogue.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Casa Vogue</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-casa-e-jardim">

    <a href="http://revistacasaejardim.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Casa e Jardim</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-do-zap">

    <a href="http://revista.zapimoveis.com.br/?utm_source=globo.com-etc&amp;utm_medium=botao-casa-decoracao&amp;utm_campaign=revista-imoveis" class="menu-item-link">
        <span class="menu-item-title">Revista do Zap</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-gastronomia">

    <a class="menu-item-link">
        <span class="menu-item-title">Gastronomia</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Gastronomia</a>
            
                

                




<li class="menu-item " id="menu-2-casa-e-comida">

    <a href="http://revistacasaejardim.globo.com/Casa-e-Comida/" class="menu-item-link">
        <span class="menu-item-title">Casa e Comida</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-colheradas">

    <a href="http://meus5minutos.globo.com/blogs/Colheradas/" class="menu-item-link">
        <span class="menu-item-title">Colheradas</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-receitas-da-ana-maria">

    <a href="http://gshow.globo.com/programas/mais-voce/Receitas-da-Ana-Maria/" class="menu-item-link">
        <span class="menu-item-title">Receitas da Ana Maria</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-receitascom">

    <a href="http://gshow.globo.com/receitas/" class="menu-item-link">
        <span class="menu-item-title">Receitas.com</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-receitas-no-gnt">

    <a href="http://gnt.globo.com/receitas/" class="menu-item-link">
        <span class="menu-item-title">Receitas no GNT</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-musica">

    <a class="menu-item-link">
        <span class="menu-item-title">MÃºsica</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">MÃºsica</a>
            
                

                




<li class="menu-item " id="menu-2-altas-horas">

    <a href=" http://gshow.globo.com/programas/altas-horas/" class="menu-item-link">
        <span class="menu-item-title">Altas Horas </span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-g1-musica">

    <a href="http://g1.globo.com/musica/" class="menu-item-link">
        <span class="menu-item-title">G1 MÃºsica</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-gshow-musica">

    <a href="http://gshow.globo.com/Musica/" class="menu-item-link">
        <span class="menu-item-title">Gshow MÃºsica</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-musica-no-multishow">

    <a href="http://multishow.globo.com/musica/" class="menu-item-link">
        <span class="menu-item-title">MÃºsica no Multishow</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-musicacombr">

    <a href="http://musica.com.br/" class="menu-item-link">
        <span class="menu-item-title">MÃºsica.com.br</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-radiobeat">

    <a href="http://radiobeat.com.br/" class="menu-item-link">
        <span class="menu-item-title">RADIOBEAT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-som-brasil">

    <a href="http://globotv.globo.com/rede-globo/som-brasil/" class="menu-item-link">
        <span class="menu-item-title">Som Brasil</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-cinema-teatro">

    <a class="menu-item-link">
        <span class="menu-item-title">Cinema &amp; Teatro</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Cinema &amp; Teatro</a>
            
                

                




<li class="menu-item " id="menu-2-canal-brasil">

    <a href="http://canalbrasil.globo.com/ " class="menu-item-link">
        <span class="menu-item-title">Canal Brasil</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-canal-universal">

    <a href="http://universal.globo.com" class="menu-item-link">
        <span class="menu-item-title">Canal Universal</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-filmes-na-globo">

    <a href="http://redeglobo.globo.com/filmes/index.html" class="menu-item-link">
        <span class="menu-item-title">Filmes na Globo</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-g1-cinema">

    <a href="http://g1.globo.com/pop-arte/cinema/" class="menu-item-link">
        <span class="menu-item-title">G1 Cinema</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-globo-filmes">

    <a href=" http://globofilmes.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Globo Filmes</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-megapix">

    <a href="http://megapix.globo.com/" class="menu-item-link">
        <span class="menu-item-title">MegaPix</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-monet">

    <a href="http://revistamonet.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Monet</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-teatro">

    <a href="http://www.agentesevenoteatro.com.br/" class="menu-item-link">
        <span class="menu-item-title">Teatro</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-telecine">

    <a href="http://telecine.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Telecine</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item menu-item-highlighted " id="menu-1-ego">

    <a href="http://ego.globo.com/" class="menu-item-link">
        <span class="menu-item-title">ego</span>
    </a>

    
</li>

    
        




<li class="menu-item menu-item-highlighted " id="menu-1-quem">

    <a href="http://revistaquem.globo.com/" class="menu-item-link">
        <span class="menu-item-title">quem</span>
    </a>

    
</li>

    
        




<li class="menu-item menu-item-highlighted " id="menu-1-patricia-kogut">

    <a href="http://kogut.oglobo.globo.com/" class="menu-item-link">
        <span class="menu-item-title">patricia kogut</span>
    </a>

    
</li>

    
</ul>

        </div>
        <div id="menu-addon" class="menu-addon"></div>
    </div>

    
</nav>

<div id="menu-fonts">
    
    
    <span class="open-sans">BESbswy</span>
    <span class="open-sans-bold">BESbswy</span>
    <span class="open-sans-light">BESbswy</span>
    <span class="roboto-slab">BESbswy</span>
    <span class="proximanova-semibold">BESbswy</span>
</div>

<div id="menu-content-overlay"></div>
<style>
.regua-svg-container{height:0;width:0;position:absolute;visibility:hidden}</style><script>
(function (window) {window.REGUA_SETTINGS=window.REGUA_SETTINGS || {};window.REGUA_SETTINGS.portalName = "home";window.REGUA_SETTINGS.portalHome = "http://www.globo.com/";window.REGUA_SETTINGS.portalsList = ["home","g1","ge","gshow","famosos","techtudo","globotv","globoplay","globosatplay"];window.REGUA_SETTINGS.portalsLinkList = ["http://www.globo.com/","http://g1.globo.com/index.html","http://globoesporte.globo.com/","http://gshow.globo.com/","http://famosos.globo.com/","http://www.techtudo.com.br/","http://globotv.globo.com/","http://globoplay.globo.com/","http://globosatplay.globo.com/"];window.REGUA_SETTINGS.staticUrl = "http://s.glbimg.com/en/ho/static/";window.REGUA_SETTINGS.svgSpriteName = "sprite-e737468ca1.svg";window.REGUA_SETTINGS.suggestUrl = "";window.REGUA_SETTINGS.version = "1.3.7";})(window);
</script><script>(function(){var e,t,n,o;e=function(){function e(){this.name="Android",this.bridge=window.GloboBridge||{navigated:function(e){},menuAction:function(e){},reguaAction:function(e){}}}return e.prototype.navigateTo=function(e){this.bridge.navigated(JSON.stringify(e))},e.prototype.menuAction=function(e){this.bridge.menuAction(JSON.stringify(e))},e.prototype.reguaAction=function(e){this.bridge.reguaAction(JSON.stringify(e))},e}(),t=function(){function e(){this.name="iOS",this.connectWebViewJavascriptBridge(function(e){return e.init(function(e,t){return window.glb.masterApp.manageMessage(e)})})}return e.prototype.connectWebViewJavascriptBridge=function(e){window.WebViewJavascriptBridge?e(WebViewJavascriptBridge):document.addEventListener("WebViewJavascriptBridgeReady",function(){e(WebViewJavascriptBridge)},!1)},e.prototype.brigdeSendJson=function(e){return this.connectWebViewJavascriptBridge(function(t){t.send(JSON.stringify(e))})},e.prototype.navigateTo=function(e){this.brigdeSendJson(e)},e.prototype.menuAction=function(e){this.brigdeSendJson(e)},e.prototype.reguaAction=function(e){this.brigdeSendJson(e)},e}(),n=function(){function n(e){this.platformApp=e,this.initCalled=!1,null!=this.platformApp&&this.bind()}return n.prototype.init=function(){this.initCalled||(this.initCalled=!0,this.header&&(this.processHeaderInfo(),this.bindAfterInit()))},n.prototype.processHeaderInfo=function(){this.countHeaderTries=0,this.maxHeaderTries=100,this.headerCubeContainer=this.header.querySelector(".cube-container"),this.headerCubeContainer?this.processHeaderInfoAction():setTimeout(function(e){return function(){return e.processHeaderInfoAction()}}(this),1)},n.prototype.processHeaderInfoAction=function(){var e;if(this.countHeaderTries++,this.headerCubeContainer=this.header.querySelector(".cube-container"),this.headerCubeContainer)this.cacheVariables(),e=this.getHeaderData(),this.navigateTo(e);else{if(this.countHeaderTries>this.maxHeaderTries)return void console.log("Nao achou o markup interno do header");setTimeout(function(e){return function(){return e.processHeaderInfoAction()}}(this),1)}},n.prototype.bind=function(){var e;return this.bindAppEvents(),this.header=document.querySelector("#header-produto"),e=function(e){return function(){return e.header||(e.header=document.querySelector("#header-produto")),e.init()}}(this),this.header?this.init():(document.addEventListener("glb.headerDom.ready",e),document.addEventListener("DOMContentLoaded",e))},n.prototype.cacheVariables=function(){return this.elements={},this.elements.header=this.header,this.elements.headerCubeContainer=this.headerCubeContainer,this.elements.headerFront=this.elements.headerCubeContainer?this.elements.headerCubeContainer.querySelector(".front"):null,this.elements.headerLogoProduto=this.elements.headerCubeContainer?this.elements.headerCubeContainer.querySelector(".logo-produto-container"):null,this.elements.headerLogoProdutoLink=this.elements.headerLogoProduto?this.elements.headerLogoProduto.parentNode:null,this.elements.headerLogoArea=this.elements.headerCubeContainer?this.elements.headerCubeContainer.querySelector(".logo-area .logo"):null,this.elements.headerLogoAreaLink=this.elements.headerLogoArea?this.elements.headerLogoArea.parentNode:null,this.elements.headerSubeditoria=this.elements.headerFront?this.elements.headerFront.querySelector(".menu-subeditoria"):null,this.elements.headerSubeditoriaLink=this.elements.headerSubeditoria?this.elements.headerSubeditoria.querySelector("a"):null,!0},n.prototype.bindAppEvents=function(){document.addEventListener("glb.menu-carousel.ready",function(e){return function(){return e.platformApp.menuAction({action:"menuReady"})}}(this)),document.addEventListener("glb.menu-carousel.shown.before",function(e){return function(){return e.platformApp.menuAction({action:"menuShownBefore"})}}(this)),document.addEventListener("glb.menu-carousel.shown.after",function(e){return function(){return e.platformApp.menuAction({action:"menuShownAfter"})}}(this)),document.addEventListener("glb.menu-carousel.off.before",function(e){return function(){return e.platformApp.menuAction({action:"menuOffBefore"})}}(this)),document.addEventListener("glb.menu-carousel.off.after",function(e){return function(){return e.platformApp.menuAction({action:"menuOffAfter"})}}(this)),document.addEventListener("glb.regua.exists",function(e){return function(){return e.platformApp.reguaAction({action:"reguaExists"})}}(this))},n.prototype.bindAfterInit=function(){},n.prototype._getText=function(e){return e?(e.innerText||e.textContent).trim()||"":""},n.prototype._getHref=function(e){return e?e.getAttribute("href")||"":""},n.prototype._getCssProp=function(e,t){var n;return e?(n=window.getComputedStyle(e),n.getPropertyValue(t)||""):""},n.prototype._getBackgroundColor=function(e){return this._getCssProp(e,"background-color")},n.prototype.getHeaderData=function(){var e;return e={color:this._getBackgroundColor(this.elements.headerFront),editoria:"",editoriaHref:"",subeditoria:"",subeditoriaHref:"",logo:"",logoHref:""},this.elements.headerLogoProduto?(e.logo=this._getText(this.elements.headerLogoProduto),e.logoHref=this._getHref(this.elements.headerLogoProdutoLink),e.editoria=this._getText(this.elements.headerLogoArea),e.editoriaHref=this._getHref(this.elements.headerLogoArea),this.elements.headerSubeditoria&&(e.subeditoria=this._getText(this.elements.headerSubeditoria),e.subeditoriaHref=this._getHref(this.elements.headerSubeditoriaLink))):(e.logo=this._getText(this.elements.headerLogoArea),e.logoHref=this._getHref(this.elements.headerLogoAreaLink)),e},n.prototype.navigateTo=function(e){e.action="navigateTo",e.url=window.location.href,this.platformApp?this.platformApp.navigateTo(e):console.log("Sem Plataforma")},n.prototype.manageMessage=function(e){var t,n;return e&&(t=JSON.parse(e),t.action="regua-change")?(n=t.rulerItem||"home",null!=window.$?$(document).trigger("glb.regua.change",[n]):console.log("Sem jQuery")):void 0},n.prototype.getUserAgent=function(){return window.getUserAgent?window.getUserAgent():window.navigator.userAgent},n.prototype.getPlatformApp=function(){var n;return n=this.getUserAgent(),n.match(/(iPad|iPhone|iPod)/g)?new t:n.match(/(Android)/g)?new e:void 0},n}(),window.glb=window.glb||{},window.glb.MasterApp=n,window.glb.AndroidApp=e,window.glb.IosApp=t,window.initMasterApp=function(){var e;if(!window.glb.masterApp)return e=n.prototype.getPlatformApp(),window.glb.masterApp=new n(e)},o=null!=window.noAutoLoadReguaNavegacao&&window.noAutoLoadReguaNavegacao,o||window.initMasterApp()}).call(this);</script><script>(function(){var n,o,e,t;e=function(){return/MobApp_Android|MobApp_iOS|MobApp_WP/},o=function(){return window.getUserAgent?window.getUserAgent():window.navigator.userAgent},n=function(){var n,t,a,i,r;i=o(),r=i.split(" ");for(a in r)if(n=r[a],e().test(n))return t=n.split("/"),{name:t[0].replace("_"," "),version:t[1],frameworkVersion:t[2]};return null},window.addGlbOnAppClass=function(){var o;window.glb=window.glb||{},window.glb.extractAppFromUserAgent||(window.glb.extractAppFromUserAgent=n,window.glb.nativeAppInfo=n(),null!==window.glb.nativeAppInfo&&(window.glb.nativeAppInfo.frameworkVersion?(o=parseInt(window.glb.nativeAppInfo.frameworkVersion,10),o>=1&&(document.documentElement.className+=" glb-on-app")):document.documentElement.className+=" glb-on-app-comnio"))},t=null!=window.noAutoLoadReguaNavegacao&&window.noAutoLoadReguaNavegacao,t||window.addGlbOnAppClass()}).call(this);</script><script>(function(){var n,e,o,i,t,w,d,r,a,s,l,u,c;try{new CustomEvent("test")}catch(m){o=m,n=function(n,e){var o;return o=void 0,e=e||{bubbles:!1,cancelable:!1,detail:void 0},o=document.createEvent("CustomEvent"),o.initCustomEvent(n,e.bubbles,e.cancelable,e.detail),o},n.prototype=window.Event.prototype,window.CustomEvent=n}s=function(n,e){var o;return null==n||null==e?!1:(o=new RegExp("(?:^|\\s)"+e+"(?!\\S)"),!!n.className.match(o))},e=function(n,e){s(n,e)||(n.className+=" "+e)},u=function(n,e){var o;s(n,e)&&(o=new RegExp("(?:^|\\s)"+e+"(?!\\S)","g"),n.className=n.className.replace(o,""))},window.viewportSize={},window.viewportSize.getHeight=function(){return a("Height")},window.viewportSize.getWidth=function(){return a("Width")},a=function(n){var e,o,i,t,w,d;return d=void 0,w=n.toLowerCase(),i=window.document,t=i.documentElement,void 0===window["inner"+n]?d=t["client"+n]:window["inner"+n]!==t["client"+n]?(e=i.createElement("body"),e.id="vpw-test-b",e.style.cssText="overflow:scroll",o=i.createElement("div"),o.id="vpw-test-d",o.style.cssText="position:absolute;top:-1000px",o.innerHTML="<style>@media("+w+":"+t["client"+n]+"px){body#vpw-test-b div#vpw-test-d{"+w+":7px!important}}</style>",e.appendChild(o),t.insertBefore(e,i.head),d=7===o["offset"+n]?t["client"+n]:window["inner"+n],t.removeChild(e)):d=window["inner"+n],d},window.myInnerWidth=viewportSize.getWidth(),window.myInnerHeight=viewportSize.getHeight(),t=function(){return window.myInnerWidth||window.innerWidth},i=function(){return window.myInnerHeight||window.innerHeight},w=function(){var n;return null!=window.isAndroidBrowser?window.isAndroidBrowser:(n=navigator.userAgent,window.isAndroidBrowser=n.indexOf("Mozilla/5.0")>-1&&n.indexOf("Android ")>-1&&n.indexOf("AppleWebKit")>-1&&-1===n.indexOf("Chrome"),window.isAndroidBrowser)},d=function(){return null!=window.isPortrait?window.isPortrait:(window.isPortrait=t()<=i(),window.isPortrait)},r=function(){return null!=window.isTouchable?window.isTouchable:(window.isTouchable="ontouchstart"in window||navigator.msMaxTouchPoints||window.DocumentTouch&&document instanceof window.DocumentTouch,window.isTouchable)},l=function(){var n;return window.REGUAMAXWIDTHPORTRAIT=window.REGUAMAXWIDTHPORTRAIT||640,window.REGUAMAXWIDTHLANDSCAPE=window.REGUAMAXWIDTHLANDSCAPE||767,window.isPortrait=d(),window.isTouchable=r(),window.isAndroidBrowser=w(),n=t(),(window.isPortrait&&n<=window.REGUAMAXWIDTHPORTRAIT||!window.isPortrait&&n<=window.REGUAMAXWIDTHLANDSCAPE)&&!window.isAndroidBrowser&&document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image","1.1")&&(null==window.glb.nativeAppInfo||window.glb.nativeAppInfo.frameworkVersion)},window.glb=window.glb||{},window.glb.hasClass=s,window.glb.addClass=e,window.glb.removeClass=u,window.addHasReguaClass=function(){window.glb.reguaShouldStart||(window.glb.reguaShouldStart=l,window.glb.reguaShouldStart()?(window.glb.addClass(document.documentElement,"has-regua"),window.glb.addClass(document.documentElement,"svg-support"),window.glb.removeClass(document.documentElement,"has-not-regua"),document.dispatchEvent(new CustomEvent("glb.regua.exists"))):(window.glb.removeClass(document.documentElement,"has-regua"),window.glb.removeClass(document.documentElement,"svg-support"),window.glb.addClass(document.documentElement,"has-not-regua")))},c=null!=window.noAutoLoadReguaNavegacao&&window.noAutoLoadReguaNavegacao,c||window.addHasReguaClass()}).call(this);</script><nav id="regua-navegacao" class="regua-navegacao "><div id="regua-svg-container" class="regua-svg-container"><svg xmlns="http://www.w3.org/2000/svg"><symbol id="eixo-icone-busca" viewBox="0 0 24 24"><path d="M21.67 20.27l-5.72-5.73c.98-1.26 1.55-2.83 1.55-4.54 0-4.14-3.36-7.5-7.5-7.5-4.15 0-7.5 3.36-7.5 7.5 0 4.14 3.35 7.5 7.5 7.5 1.7 0 3.27-.57 4.53-1.54l5.73 5.73c.42.41 1.07.43 1.46.04.39-.39.37-1.04-.05-1.46zM10 15.5c-3.04 0-5.5-2.46-5.5-5.5S6.96 4.5 10 4.5c3.03 0 5.5 2.46 5.5 5.5s-2.47 5.5-5.5 5.5z"/></symbol><symbol id="eixo-icone-feed" viewBox="0 0 24 24"><path d="M17 14c0 .55-.45 1-1 1h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1zM19 10c0 .55-.45 1-1 1h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1z"/><path fill="#FFF" d="M17 14c0 .55-.45 1-1 1h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1zM19 10c0 .55-.45 1-1 1h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1z"/><path d="M19 10c0 .55-.45 1-1 1h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1zM17 14c0 .55-.45 1-1 1h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1z"/><path d="M4 3v4H0v11c0 1.66 1.34 3 3 3h19c1.1 0 2-.9 2-2V3H4zm0 15.09c0 .5-.45.91-1 .91s-1-.41-1-.91V9h2v9.09zM22 19H6V5h16v14z"/><path fill="#FFF" d="M4 9v9.09c0 .5-.45.91-1 .91s-1-.41-1-.91V9h2zM6 5v14h16V5H6zm10 10h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1s-.45 1-1 1zm2-4h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1s-.45 1-1 1z"/></symbol><symbol id="eixo-icone-menu" viewBox="0 0 24 24"><path d="M22 6c0 .552-.447 1-1 1H3c-.552 0-1-.448-1-1 0-.553.448-1 1-1h18c.553 0 1 .447 1 1zM22 12c0 .552-.447 1-1 1H3c-.552 0-1-.448-1-1 0-.553.448-1 1-1h18c.553 0 1 .447 1 1zM22 18c0 .552-.447 1-1 1H3c-.552 0-1-.448-1-1 0-.553.448-1 1-1h18c.553 0 1 .447 1 1z"/></symbol><symbol id="eixo-icone-usuario" viewBox="0 0 24 24"><path d="M12 2C8.4 2 5.5 4.9 5.5 8.5c0 3.61 2.9 6.5 6.5 6.5s6.5-2.89 6.5-6.5C18.5 4.9 15.6 2 12 2zm0 11.07c-2.49 0-4.5-2.02-4.5-4.5s2.01-4.5 4.5-4.5 4.5 2.02 4.5 4.5-2.01 4.5-4.5 4.5zM22.893 23c-.733 0-1.074-.455-1.074-.86C21.817 20.26 18.22 19 12 19s-9.818 1.136-9.818 3.14c0 .406-.41.86-1.092.86C.358 23 0 22.513 0 22.108 0 18.958 4.598 17 12 17s12 1.958 12 5.108c0 .405-.398.892-1.107.892z"/></symbol><symbol id="regua-arrow" viewBox="0 0 5 14"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.902 6.64L1.422.386C1.217.017.762-.11.402.104.05.317-.074.79.133 1.16l3.25 5.843L.1 12.847c-.208.368-.085.838.274 1.05.357.212.814.086 1.02-.28l3.412-6.074c.21-.243.26-.606.096-.904z"/></symbol></svg></div><div class="regua-navegacao-container"><ul class="regua-lista regua-lista-home"><li id="regua-navegacao-item-home" class="regua-navegacao-item active"><span data-href="http://www.globo.com/"
                                            data-target="regua-tab-home" class="home-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-feed"/></svg></span></li><li id="regua-navegacao-item-menu" class="regua-navegacao-item "><span data-target="regua-tab-menu" class="menu-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-menu"/></svg></span></li><li id="regua-navegacao-item-busca" class="regua-navegacao-item "><span data-target="regua-tab-busca" class="busca-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-busca"/></svg></span></li><li id="regua-navegacao-item-usuario" class="regua-navegacao-item "><span data-target="regua-tab-usuario" class="usuario-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-usuario"/></svg></span></li></ul></div></nav><nav id="regua-user-container" class="regua-user-container regua-user-home regua-navegacao-tab"><div id="regua-user-controls" class="regua-user-controls"></div><div id="regua-user-tabs" class="regua-tabs regua-user-tab-2"><div id="tab-labels" class="tab-labels"><span class="tab-label tab-label-1" data-index="1">notificaÃ§Ãµes</span><span class="tab-label tab-label-2" data-index="2">minha conta</span></div><div class="regua-tab regua-tab-1" data-index="1"><div class="regua-content"><ul id="regua-user-profile-notifications" class="regua-user-row regua-user-profile-notifications"></ul></div></div><div class="regua-tab regua-tab-2" data-index="2"><div class="regua-content"><ul id="regua-user-profile-list" class="regua-user-row regua-user-profile-list"></ul><div id="regua-user-logout" class="regua-user-logout"></div></div></div></div></nav><div id="regua-tab-busca" class="regua-navegacao-tab regua-tab-busca regua-busca-home"><div class="regua-search-header"><div class="regua-search-box"><form class="regua-search-form" action="http://globo.com/busca/" method="GET"><div class="regua-search-form-container regua-table"><div class="regua-search-input-box regua-table-cell"><input class="regua-search-input" name="q" type="search" placeholder="o que vocÃª procura?" autocorrect="off" autocapitalize="off" autocomplete="off"/></div><div class="regua-search-buttons-container regua-table-cell"><div class="regua-search-clear-container"><a href="#" class="regua-search-clear-button">clear</a></div><div class="search-button-icon"><input type="submit" value="go" class="regua-search-submit"/><div class="regua-icon-block"><svg class="regua-navegacao-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#eixo-icone-busca"></use></svg></div><a href="#" class="regua-icon-block search-button-go"><svg class="regua-navegacao-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#eixo-icone-busca"></use></svg></a></div></div></div></form></div></div><div class="regua-container-search-body"><ul class="regua-pre-suggest"></ul><div class="regua-container-suggest"><ul class="regua-suggest"></ul></div><div class="regua-container-results"></div></div></div>



<ul id="home-menu" class="nested widget-menu">
    
        <li class="g1 analytics-product analytics-multi-product">
            <a href="http://g1.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">g1</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://g1.globo.com/" data-menu-id="g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">g1</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="jornais-revistas-e-radio">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">jornais, revistas e rÃ¡dio</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="servicos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">serviÃ§os</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-g1">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-g1" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/economia/">
                                                <span class="titulo">Economia</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/carros/">
                                                <span class="titulo">Carros</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/">
                                                <span class="titulo">G1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/mundo/">
                                                <span class="titulo">Mundo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/">
                                                <span class="titulo">Jornal Extra</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/politica/">
                                                <span class="titulo">PolÃ­tica</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/concursos-e-emprego/">
                                                <span class="titulo">Concursos e Empregos</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/">
                                                <span class="titulo">Jornal O Globo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/planeta-bizarro/">
                                                <span class="titulo">Planeta Bizarro</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/jornal-hoje/">
                                                <span class="titulo">Jornal Hoje</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="g1">
                                        <div class="submenu-title">g1</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Carros</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/concursos-e-emprego/">
                                                    <span class="titulo">Concursos e Empregos</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/">
                                                    <span class="titulo">Economia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/educacao/">
                                                    <span class="titulo">EducaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/imposto-de-renda/2015/index.html">
                                                    <span class="titulo">Imposto de Renda 2015</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/mundo/">
                                                    <span class="titulo">Mundo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/planeta-bizarro/">
                                                    <span class="titulo">Planeta Bizarro</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/politica/">
                                                    <span class="titulo">PolÃ­tica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/">
                                                    <span class="titulo">Pop &amp; Arte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/tecnologia/">
                                                    <span class="titulo">Tecnologia &amp; Games</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bom-dia-brasil/">
                                                    <span class="titulo">Bom dia Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://fantastico.globo.com/">
                                                    <span class="titulo">FantÃ¡stico</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/agronegocios/">
                                                    <span class="titulo">Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/globo-news/">
                                                    <span class="titulo">GloboNews</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/hora1">
                                                    <span class="titulo">Hora 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-da-globo/">
                                                    <span class="titulo">Jornal da Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-hoje/">
                                                    <span class="titulo">Jornal Hoje</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-nacional/">
                                                    <span class="titulo">Jornal Nacional</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="jornais-revistas-e-radio">
                                        <div class="submenu-title">jornais, revistas e rÃ¡dio</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/">
                                                    <span class="titulo">Jornal Extra</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/">
                                                    <span class="titulo">Jornal O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://cbn.globoradio.globo.com/home/HOME.htm">
                                                    <span class="titulo">RÃ¡dio CBN</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaautoesporte.globo.com/">
                                                    <span class="titulo">Revista Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaepoca.globo.com/">
                                                    <span class="titulo">Revista Ãpoca</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagalileu.globo.com/">
                                                    <span class="titulo">Revista Galileu</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagloborural.globo.com/">
                                                    <span class="titulo">Revista Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radioglobo.globoradio.globo.com/">
                                                    <span class="titulo">RÃ¡dio Globo</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="servicos">
                                        <div class="submenu-title">serviÃ§os</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/agenda.html">
                                                    <span class="titulo">Agenda de Shows</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://especiais.g1.globo.com/educacao/app-g1-enem/">
                                                    <span class="titulo">Aplicativo G1 Enem</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/">
                                                    <span class="titulo">Conversor de Moedas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">
                                                    <span class="titulo">CotaÃ§Ãµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/">
                                                    <span class="titulo">Enem e Vestibular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/loteria/">
                                                    <span class="titulo">Loterias</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/previsao-do-tempo.html">
                                                    <span class="titulo">PrevisÃ£o do tempo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/tabela-fipe/index.html">
                                                    <span class="titulo">Tabela FIPE</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/telecurso/">
                                                    <span class="titulo">Telecurso</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.techtudo.com.br/velocimetro.html">
                                                    <span class="titulo">VelocÃ­metro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-g1"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="globoesporte analytics-product analytics-multi-product">
            <a href="http://globoesporte.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">globoesporte</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-globoesporte">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/" data-menu-id="globoesportecom">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globoesporte.com</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://sportv.globo.com/" data-menu-id="sportv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sportv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/futebol/" data-menu-id="futebol">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">futebol</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="mais-esportes">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mais esportes</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-globoesporte">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-globoesporte" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                <span class="titulo">Futebol Internacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://sportv.globo.com/site/combate/">
                                                <span class="titulo">MMA</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                <span class="titulo">FÃ³rmula 1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/">
                                                <span class="titulo">Globo Esporte</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/">
                                                <span class="titulo">Futebol Nacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/volei/">
                                                <span class="titulo">VÃ´lei</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/atletismo/">
                                                <span class="titulo">Atletismo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/basquete/">
                                                <span class="titulo">Basquete</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/tenis/">
                                                <span class="titulo">TÃªnis</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="globoesportecom">
                                        <div class="submenu-title">globoesporte.com</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://futpedia.globo.com/">
                                                    <span class="titulo">FutpÃ©dia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/cartola-fc/">
                                                    <span class="titulo">Cartola FC</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="sportv">
                                        <div class="submenu-title">sportv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/nba/">
                                                    <span class="titulo">NBA</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/eventos/combate/">
                                                    <span class="titulo">Combate</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/corujao-do-esporte/">
                                                    <span class="titulo">CorujÃ£o do Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/esporte-espetacular/">
                                                    <span class="titulo">Esporte Espetacular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/">
                                                    <span class="titulo">Globo Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://app.globoesporte.globo.com/tv/planeta-extremo/">
                                                    <span class="titulo">Planeta Extremo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sociopremiere.globo.com/">
                                                    <span class="titulo">Premiere</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/">
                                                    <span class="titulo">SporTV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/lutas/tuf-brasil/">
                                                    <span class="titulo">The Ultimate Fighter Brasil</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="futebol">
                                        <div class="submenu-title">futebol</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie B</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-c/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie C</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-do-brasil/">
                                                    <span class="titulo">Copa do Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-sul-americana/">
                                                    <span class="titulo">Copa Sul-Americana</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                    <span class="titulo">Futebol Internacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol Nacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">
                                                    <span class="titulo">Liga dos CampeÃµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/selecao-brasileira/">
                                                    <span class="titulo">SeleÃ§Ã£o Brasileira</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="mais-esportes">
                                        <div class="submenu-title">mais esportes</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/atletismo/">
                                                    <span class="titulo">Atletismo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/basquete/">
                                                    <span class="titulo">Basquete</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eu-atleta/">
                                                    <span class="titulo">Eu Atleta</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                    <span class="titulo">FÃ³rmula 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eventos/futsal/">
                                                    <span class="titulo">Futsal</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/combate/">
                                                    <span class="titulo">MMA</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/natacao/">
                                                    <span class="titulo">NataÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/stock-car/">
                                                    <span class="titulo">StockCar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/tenis/">
                                                    <span class="titulo">TÃªnis</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/volei/">
                                                    <span class="titulo">VÃ´lei</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-globoesporte"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="gshow analytics-product analytics-multi-product">
            <a href="http://gshow.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">gshow</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/" data-menu-id="gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gshow</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="programas">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">programas</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="series">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sÃ©ries</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/programas/webseries/" data-menu-id="series-originais">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sÃ©ries originais</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com/" data-menu-id="rede-globo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">rede globo</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-gshow">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-gshow" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/mais-voce/">
                                                <span class="titulo">Mais VocÃª</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/realities/the-voice-brasil/">
                                                <span class="titulo">The Voice</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                <span class="titulo">Encontro com FÃ¡tima</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://redeglobo.globo.com/programacao.html">
                                                <span class="titulo">ProgramaÃ§Ã£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                <span class="titulo">VÃ­deo Show</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/">
                                                <span class="titulo">Gshow</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://redeglobo.globo.com/">
                                                <span class="titulo">Rede globo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/series/mister-brau/2015/">
                                                <span class="titulo">Mister Brau</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                <span class="titulo">CaldeirÃ£o do Huck</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="gshow">
                                        <div class="submenu-title">gshow</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Bastidores/">
                                                    <span class="titulo">Bastidores</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/como-fazer/">
                                                    <span class="titulo">Como Fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo">
                                                    <span class="titulo">Estilo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/especial-blog/gshow-troll/1.html">
                                                    <span class="titulo">Gshow Troll</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/participe/">
                                                    <span class="titulo">Participe</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas-da-Ana-Maria/">
                                                    <span class="titulo">Receitas da Ana Maria</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/tv/plantao/">
                                                    <span class="titulo">TV</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="programas">
                                        <div class="submenu-title">programas</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://gshow.globo.com/programas/altas-horas/">
                                                    <span class="titulo">Altas Horas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                    <span class="titulo">CaldeirÃ£o do Huck</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                    <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/e-de-casa/">
                                                    <span class="titulo">Ã de Casa</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                    <span class="titulo">Encontro com FÃ¡tima</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/estrelas/index.html">
                                                    <span class="titulo">Estrelas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/">
                                                    <span class="titulo">Mais VocÃª</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/programa-do-jo/">
                                                    <span class="titulo">Programa do JÃ´</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/realities/the-voice-brasil/">
                                                    <span class="titulo">The Voice</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                    <span class="titulo">VÃ­deo Show</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="series">
                                        <div class="submenu-title">sÃ©ries</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/amorteamo/">
                                                    <span class="titulo">Amorteamo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/chapa-quente/">
                                                    <span class="titulo">Chapa Quente</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/ligacoes-perigosas/">
                                                    <span class="titulo">LigaÃ§Ãµes Perigosas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/mister-brau/2015/">
                                                    <span class="titulo">Mister Brau</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/pe-na-cova/2015/">
                                                    <span class="titulo">PÃ© na Cova</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/especial-blog/ta-no-ar-a-tv-na-tv/1.html">
                                                    <span class="titulo">TÃ¡ no ar: a TV na TV </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/zorra/">
                                                    <span class="titulo">Zorra</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="series-originais">
                                        <div class="submenu-title">sÃ©ries originais</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/bichos/no-ar.html">
                                                    <span class="titulo">Bichos</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/como-fazer/no-ar.html">
                                                    <span class="titulo">Como fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/deborah-secco-apresenta/no-ar.html">
                                                    <span class="titulo">Deborah Secco apresenta</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/dulce-delight/">
                                                    <span class="titulo">Dulce Delight</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/laboratorio-do-som/no-ar.html">
                                                    <span class="titulo">LaboratÃ³rio do som</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/morri-na-tv/no-ar.html">
                                                    <span class="titulo">Morri na TV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/os-desatinados-malhacao-seu-lugar-no-mundo/no-ar.html">
                                                    <span class="titulo">Os desatinados</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/sebastiana-quebra-galho/no-ar.html">
                                                    <span class="titulo">Sebastiana Quebra-Galho</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/verdades-secretasdoc/no-ar.html">
                                                    <span class="titulo">Verdades Secretas.doc</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="rede-globo">
                                        <div class="submenu-title">rede globo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/programacao.html">
                                                    <span class="titulo">ProgramaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-gshow"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="famosos-etc analytics-product analytics-multi-product">
            <a href="http://famosos.globo.com" class="analytics-area analytics-id-T">
                <span class="titulo">famosos &amp; etc</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-famosos-etc">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://famosos.globo.com/" data-menu-id="famosos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">famosos</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="moda-estilo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">moda & estilo</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="bem-estar-saude">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">bem-estar & saÃºde</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="casa-decoracao">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">casa & decoraÃ§Ã£o</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="gastronomia">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gastronomia</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="musica-cinema-e-teatro">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mÃºsica, cinema e teatro</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="canais-de-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">canais de tv</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-famosos-etc">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-famosos-etc" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://ego.globo.com/">
                                                <span class="titulo">Ego</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://paparazzo.globo.com/">
                                                <span class="titulo">Paparazzo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://revistaquem.globo.com/">
                                                <span class="titulo">Quem</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/famosos/">
                                                <span class="titulo">Retratos da Vida</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/cultura/kogut/">
                                                <span class="titulo">Patricia Kogut</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://famosos.globo.com/">
                                                <span class="titulo">Famosos</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="famosos">
                                        <div class="submenu-title">famosos</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/">
                                                    <span class="titulo">Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://paparazzo.globo.com/">
                                                    <span class="titulo">Paparazzo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/cultura/kogut/">
                                                    <span class="titulo">Patricia Kogut</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaquem.globo.com/">
                                                    <span class="titulo">Quem</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/famosos/">
                                                    <span class="titulo">Retratos da Vida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="moda-estilo">
                                        <div class="submenu-title">moda &amp; estilo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/moda/index.html">
                                                    <span class="titulo">Moda no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ela.oglobo.globo.com/">
                                                    <span class="titulo">Ela no O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo/">
                                                    <span class="titulo">Estilo no Gshow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaglamour.globo.com/">
                                                    <span class="titulo">Revista Glamour</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gq.globo.com/">
                                                    <span class="titulo">Revista GQ Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistamarieclaire.globo.com/">
                                                    <span class="titulo">Revista Marie Claire </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/moda/">
                                                    <span class="titulo">Moda no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://vogue.globo.com/">
                                                    <span class="titulo">Revista Vogue</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="bem-estar-saude">
                                        <div class="submenu-title">bem-estar &amp; saÃºde</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/beleza/index.html">
                                                    <span class="titulo">Beleza no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/beleza/">
                                                    <span class="titulo">Beleza no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.com.br/bemestar">
                                                    <span class="titulo">Bem Estar no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar no G1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/maes-e-filhos/">
                                                    <span class="titulo">MÃ£es no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/">
                                                    <span class="titulo">Meus 5 Minutos </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://horoscopo.ego.globo.com/">
                                                    <span class="titulo">HorÃ³scopo no Ego</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="casa-decoracao">
                                        <div class="submenu-title">casa &amp; decoraÃ§Ã£o</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/">
                                                    <span class="titulo">Revista Casa e Jardim</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/casa-e-decoracao/">
                                                    <span class="titulo">Casa no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://casavogue.globo.com/">
                                                    <span class="titulo">Casa Vogue </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revista.zapimoveis.com.br/?utm_source=globo.com-etc&amp;utm_medium=botao-casa-decoracao&amp;utm_campaign=Revista-Home">
                                                    <span class="titulo">Revista do Zap</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="gastronomia">
                                        <div class="submenu-title">gastronomia</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/receitas/">
                                                    <span class="titulo">Receitas no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas/index.html">
                                                    <span class="titulo">Receitas no Mais VocÃª </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/blogs/Colheradas/">
                                                    <span class="titulo">Colheradas </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/Casa-e-Comida/">
                                                    <span class="titulo">Casa e Comida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="musica-cinema-e-teatro">
                                        <div class="submenu-title">mÃºsica, cinema e teatro</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/cinema/">
                                                    <span class="titulo">G1 Cinema</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/">
                                                    <span class="titulo">G1 MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">Gshow MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://musica.com.br/">
                                                    <span class="titulo">MÃºsica.com.br</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radiobeat.com.br/">
                                                    <span class="titulo">RADIOBEAT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.agentesevenoteatro.com.br/">
                                                    <span class="titulo">Teatro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="canais-de-tv">
                                        <div class="submenu-title">canais de tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/">
                                                    <span class="titulo">Rede Globo </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalbrasil.globo.com/">
                                                    <span class="titulo">Canal Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://mundogloob.globo.com/">
                                                    <span class="titulo">Gloob</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/">
                                                    <span class="titulo">GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://megapix.globo.com/">
                                                    <span class="titulo">Megapix</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://multishow.globo.com/">
                                                    <span class="titulo">Multishow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://telecine.globo.com/">
                                                    <span class="titulo">Telecine</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://uc.globo.com/">
                                                    <span class="titulo">Universal Channel</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalviva.globo.com/">
                                                    <span class="titulo">Viva</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://canaloff.globo.com">
                                                    <span class="titulo">Off</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-famosos-etc"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="tecnologia analytics-product analytics-multi-product">
            <a href="http://www.techtudo.com.br/" class="analytics-area analytics-id-T">
                <span class="titulo">tecnologia</span>

        
        
            </a>
        </li>
        
    
        <li class="videos analytics-product analytics-multi-product">
            <a href="http://globoplay.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">vÃ­deos</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                            
                                <li class="has-link ">
                                    <a href="http://globoplay.globo.com/" data-menu-id="globo-play">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globo play</span>
                                    </a>
                                </li>
                            
                                <li class="has-link ">
                                    <a href="http://globosatplay.globo.com/" data-menu-id="globosat-play">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globosat play</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-videos">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
</ul>
<!-- gerado em: 2015-12-3016:36:15Z --><div id="assinante-menu"></div></nav></div><div class="header-bottom"></div></header><script>
libby.loadScript('http://s.glbimg.com/en/ho/static/etc/busca/js/jquery.buscaPadrao.v2.compressed.js', function(){var searchArea=document.getElementById('busca-padrao'),
placeholderValue = 'encontre na globo.com';if(typeof searchWidget !== "undefined" && searchWidget){searchWidget(searchArea, {suggestionsEnabled: true,
baseSearchUrl: 'http://www.globo.com',
addClass:'home-search',
placeholder: placeholderValue,
buscaHome: true,
qtd_min_letras_pro_suggest: 2,
qtd_min_letras_pra_busca: 2,
addStyleSheet: ''});}
searchArea.style.display = "block";if(searchArea && searchArea.querySelectorAll){var inputElement=searchArea.querySelectorAll('input')[0];inputElement.onwebkitspeechchange=inputElement.onspeechchange=function (ev) {inputElement.value=inputElement.value.replace(placeholderValue, '');inputElement.form.submit();};}});</script><div id="x60" class="opec-area opec-mobile opec-x60 grid-12"><div id="banner_mobile_topo" class="tag-manager-publicidade-container"></div></div><div id="urgente" class="container urgente analytics-area analytics-id-U "><div class="grid-base super-wide"></div></div><div id="bloco-principal" class="container first-scroll-container clearfix"><div class="grid-base wide analytics-area analytics-id-A"><div class="grid-base narrow pull-left primeira-area hide-mobile"><div class="destaque-principal-vertical-foto-topo destaque destaque-primeiro-scroll principal glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/economia/noticia/2015/12/tesouro-diz-que-pagou-r-724-bilhoes-de-pedaladas-devidas-em-2015.html" class=" " title="Governo anuncia que todas as &#39;pedaladas&#39; devidas foram pagas"><div class="conteudo"><h2>Governo anuncia que todas as &#39;pedaladas&#39; devidas foram pagas</h2></div></a></div></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://especiais.g1.globo.com/sao-paulo/2015/as-promessas-de-haddad/" class=" " title="Haddad cumpre 31 de suas 79 promessas"><div class="conteudo"><h2>Haddad cumpre 31 de suas 79 promessas</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Paes cumpre 10 de 40" href="http://especiais.g1.globo.com/rio-de-janeiro/2015/as-promessas-de-paes/">Paes cumpre 10 de 40</a></div></li></ul></div><div class="direita"><div class="mobile-grid-partial"><a href="http://g1.globo.com/economia/seu-dinheiro/noticia/2015/12/salario-minimo-em-2016-veja-o-que-muda-com-o-novo-valor.html" class=" " title="SalÃ¡rio mÃ­nimo a R$ 880 altera benefÃ­cios; veja"><div class="conteudo"><h2>SalÃ¡rio mÃ­nimo a R$ 880 altera benefÃ­cios; veja</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="DÃ³lar encosta em R$ 4" href="http://g1.globo.com/economia/mercados/noticia/2015/12/cotacao-do-dolar-301215.html">DÃ³lar encosta em R$ 4</a></div></li></ul></div></div><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/operacao-lava-jato/noticia/2015/12/delator-afirma-que-fez-entrega-de-dinheiro-destinado-aecio-diz-jornal.html" class=" " title="Jornal: delator diz que fez entrega de dinheiro destinado a AÃ©cio"><div class="conteudo"><h2>Jornal: delator diz que fez entrega<br /> de dinheiro destinado a AÃ©cio</h2></div></a></div></div><div class="ultimo-destaque"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-entretenimento analytics-area  analytics-id-M
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/vem-por-ai/noticia/2015/12/dante-captura-peca-chave-da-faccao.html" class="foto " title="&#39;A Regra&#39;: Dante prende Guerra (TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/7Wq7FY6vTkn6rzvkxb4YGIsTzh4=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/az3XV7oTtD89_Ze6hT4sCURxzng=/0x35:720x360/155x70/s3.glbimg.com/v1/AUTH_e84042ef78cb4708aeebdf1c68c6cbd6/photos/apis/75be310beb2a47b4bf36cd73bc01aa0f/dante.jpg" alt="&#39;A Regra&#39;: Dante prende Guerra (TV Globo)" title="&#39;A Regra&#39;: Dante prende Guerra (TV Globo)"
         data-original-image="s2.glbimg.com/az3XV7oTtD89_Ze6hT4sCURxzng=/0x35:720x360/155x70/s3.glbimg.com/v1/AUTH_e84042ef78cb4708aeebdf1c68c6cbd6/photos/apis/75be310beb2a47b4bf36cd73bc01aa0f/dante.jpg" data-url-smart_horizontal="dsRShH-0brWSiayTBU_HArWb4g0=/90x56/smart/filters:strip_icc()/" data-url-smart="dsRShH-0brWSiayTBU_HArWb4g0=/90x56/smart/filters:strip_icc()/" data-url-feature="dsRShH-0brWSiayTBU_HArWb4g0=/90x56/smart/filters:strip_icc()/" data-url-tablet="34a_Zpep4MXply3rY5D07G6ZjS8=/160xorig/smart/filters:strip_icc()/" data-url-desktop="kAlHnK2BH2kI70qpJ0fD0C_3gaI=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>&#39;A Regra&#39;: Dante prende Guerra</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/totalmente-demais/vem-por-ai/noticia/2015/12/jonatas-e-salvo-por-jamaica-e-nao-cai-em-armacao-de-fabinho.html" class="foto " title="Jonatas escapa da armaÃ§Ã£o (Artur Meninea/Gshow)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/T3lHYOW4xZbDOhbaKt9KXInALJE=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/b18MdGfkICaUVjp1sMXlGBxbjqI=/132x129:711x391/155x70/s3.glbimg.com/v1/AUTH_e84042ef78cb4708aeebdf1c68c6cbd6/photos/apis/69b3381f48db46f6b9f8261914560cea/germano-jonatas-lili-leila.jpg" alt="Jonatas escapa da armaÃ§Ã£o (Artur Meninea/Gshow)" title="Jonatas escapa da armaÃ§Ã£o (Artur Meninea/Gshow)"
         data-original-image="s2.glbimg.com/b18MdGfkICaUVjp1sMXlGBxbjqI=/132x129:711x391/155x70/s3.glbimg.com/v1/AUTH_e84042ef78cb4708aeebdf1c68c6cbd6/photos/apis/69b3381f48db46f6b9f8261914560cea/germano-jonatas-lili-leila.jpg" data-url-smart_horizontal="2LMKA7_S7cyTw7LzCM4B46EIFaU=/90x56/smart/filters:strip_icc()/" data-url-smart="2LMKA7_S7cyTw7LzCM4B46EIFaU=/90x56/smart/filters:strip_icc()/" data-url-feature="2LMKA7_S7cyTw7LzCM4B46EIFaU=/90x56/smart/filters:strip_icc()/" data-url-tablet="ANyHtlD_Trpuzup-PcOWQq_1W1k=/160xorig/smart/filters:strip_icc()/" data-url-desktop="Y4QWdGZz9iYcuRBCjVa0oSFQ3hQ=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Jonatas escapa da armaÃ§Ã£o</h2></div></a></div></div></div></div></div><div class="grid-base narrow ultimo hide-mobile"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://oglobo.globo.com/economia/capa-da-economist-alerta-para-queda-do-brasil-preve-desastre-em-2016-18384376" class="foto " title="Capa de revista prevÃª desastre para o Brasil (ReproduÃ§Ã£o)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/kyUwc9kI50fGtPU05gWZhGzmJ4M=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/rWhFq4dT8hWUR5ylqSAssEqQ7tY=/0x215:459x571/155x120/s.glbimg.com/en/ho/f/original/2015/12/30/economist.jpg" alt="Capa de revista prevÃª desastre para o Brasil (ReproduÃ§Ã£o)" title="Capa de revista prevÃª desastre para o Brasil (ReproduÃ§Ã£o)"
         data-original-image="s2.glbimg.com/rWhFq4dT8hWUR5ylqSAssEqQ7tY=/0x215:459x571/155x120/s.glbimg.com/en/ho/f/original/2015/12/30/economist.jpg" data-url-smart_horizontal="Eg0uV0-x6ueDdfP0noSP-V2-8l8=/90x56/smart/filters:strip_icc()/" data-url-smart="Eg0uV0-x6ueDdfP0noSP-V2-8l8=/90x56/smart/filters:strip_icc()/" data-url-feature="Eg0uV0-x6ueDdfP0noSP-V2-8l8=/90x56/smart/filters:strip_icc()/" data-url-tablet="9AYSo7pOCqeOO7uN4Ptb-6e9adI=/160xorig/smart/filters:strip_icc()/" data-url-desktop="dLvY4FTJWEj0Fa6x1e91JmGWCuw=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Capa de revista prevÃª desastre para o Brasil</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/radicais/noticia/2015/12/skatista-paulista-digo-menezes-faz-contato-com-familia-apos-sumico.html" class="foto " title="CampeÃ£o de skate reaparece em SP: &#39;Abalado&#39; (ReproduÃ§Ã£o Facebook)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/pWv2TdOzFEfHouq2bYoXKPUUGdY=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/9MZiZYZ_G69righmgOMV769AIwE=/23x237:203x377/155x120/s.glbimg.com/es/ge/f/original/2015/12/30/print_digo_menezes.jpg" alt="CampeÃ£o de skate reaparece em SP: &#39;Abalado&#39; (ReproduÃ§Ã£o Facebook)" title="CampeÃ£o de skate reaparece em SP: &#39;Abalado&#39; (ReproduÃ§Ã£o Facebook)"
         data-original-image="s2.glbimg.com/9MZiZYZ_G69righmgOMV769AIwE=/23x237:203x377/155x120/s.glbimg.com/es/ge/f/original/2015/12/30/print_digo_menezes.jpg" data-url-smart_horizontal="9L2YnaYkEOMQSO_xBNsY9SJBLHI=/90x56/smart/filters:strip_icc()/" data-url-smart="9L2YnaYkEOMQSO_xBNsY9SJBLHI=/90x56/smart/filters:strip_icc()/" data-url-feature="9L2YnaYkEOMQSO_xBNsY9SJBLHI=/90x56/smart/filters:strip_icc()/" data-url-tablet="O4QHFsJsw85KhGjR0__koKwGb2E=/160xorig/smart/filters:strip_icc()/" data-url-desktop="Yn1uMkEfSCbcexmaG8nf2QMLldg=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>CampeÃ£o de skate reaparece em SP: &#39;Abalado&#39;</h2></div></a></div></div></div><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/rio-de-janeiro/noticia/2015/12/mulher-que-acusa-pms-de-estupro-na-rocinha-diz-que-nao-consegue-dormir.html" class="foto " title="No Rio, jovem que acusa PMs relata estupro (reproduÃ§Ã£o / g1)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/vpCvEx0VtAU0hGRFFW6bLTxCwg0=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/U9tCxHuVyz3mImFRiXNVAAlswUQ=/0x5:155x75/155x70/s.glbimg.com/en/ho/f/original/2015/12/30/vitima-estupro.jpg" alt="No Rio, jovem que acusa PMs relata estupro (reproduÃ§Ã£o / g1)" title="No Rio, jovem que acusa PMs relata estupro (reproduÃ§Ã£o / g1)"
         data-original-image="s2.glbimg.com/U9tCxHuVyz3mImFRiXNVAAlswUQ=/0x5:155x75/155x70/s.glbimg.com/en/ho/f/original/2015/12/30/vitima-estupro.jpg" data-url-smart_horizontal="qTmldGteQWmDDP6qayB3pJQFKJo=/90x56/smart/filters:strip_icc()/" data-url-smart="qTmldGteQWmDDP6qayB3pJQFKJo=/90x56/smart/filters:strip_icc()/" data-url-feature="qTmldGteQWmDDP6qayB3pJQFKJo=/90x56/smart/filters:strip_icc()/" data-url-tablet="PMX21iykF4gfEKRnVq-Q4m-_DBM=/160xorig/smart/filters:strip_icc()/" data-url-desktop="CGvsD7uRI5wJ0ixOtii42vXbN-g=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>No Rio, jovem que acusa PMs relata  estupro</h2></div></a></div></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/futebol-internacional/jogo/30-12-2015/realmadrid-realsociedad/" class=" " title="CR7 marca 2 no Real Sociedad e garante vitÃ³ria (REUTERS/Juan Medina)"><div class="conteudo"><h2>CR7 marca 2 no Real Sociedad e garante vitÃ³ria</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/central-do-mercado/cobertura.html#/glb-feed-post/5683f86dc22d625fbead282f" class=" " title="Siga o Mercado: VerdÃ£o desiste de Jean, do Flu (BRUNO HADDAD/FLUMINENSE F.C.)"><div class="conteudo"><h2>Siga o Mercado: VerdÃ£o desiste de Jean, do Flu</h2></div></a></div></div></div><div class="ultimo-destaque"><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/times/fluminense/noticia/2015/12/fluminense-confirma-rescisao-de-contrato-com-adidas.html" class=" " title="Flu confirma rescisÃ£o com a Adidas e acerto com nova fornecedora
"><div class="conteudo"><h2>Flu confirma rescisÃ£o com a Adidas e acerto com nova fornecedora<br /></h2></div></a></div></div></div></div><div class="grid-base wide analytics-area analytics-id-A mobile-grid-base"><div class="destaque-principal-vertical-foto-topo destaque destaque-primeiro-scroll principal glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/economia/noticia/2015/12/tesouro-diz-que-pagou-r-724-bilhoes-de-pedaladas-devidas-em-2015.html" class=" " title="Governo anuncia que todas as &#39;pedaladas&#39; devidas foram pagas"><div class="conteudo"><h2>Governo anuncia que todas as &#39;pedaladas&#39; devidas foram pagas</h2></div></a></div></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://especiais.g1.globo.com/sao-paulo/2015/as-promessas-de-haddad/" class=" " title="Haddad cumpre 31 de suas 79 promessas"><div class="conteudo"><h2>Haddad cumpre 31 de suas 79 promessas</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Paes cumpre 10 de 40" href="http://especiais.g1.globo.com/rio-de-janeiro/2015/as-promessas-de-paes/">Paes cumpre 10 de 40</a></div></li></ul></div><div class="direita"><div class="mobile-grid-partial"><a href="http://g1.globo.com/economia/seu-dinheiro/noticia/2015/12/salario-minimo-em-2016-veja-o-que-muda-com-o-novo-valor.html" class=" " title="SalÃ¡rio mÃ­nimo a R$ 880 altera benefÃ­cios; veja"><div class="conteudo"><h2>SalÃ¡rio mÃ­nimo a R$ 880 altera benefÃ­cios; veja</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="DÃ³lar encosta em R$ 4" href="http://g1.globo.com/economia/mercados/noticia/2015/12/cotacao-do-dolar-301215.html">DÃ³lar encosta em R$ 4</a></div></li></ul></div></div><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/operacao-lava-jato/noticia/2015/12/delator-afirma-que-fez-entrega-de-dinheiro-destinado-aecio-diz-jornal.html" class=" " title="Jornal: delator diz que fez entrega de dinheiro destinado a AÃ©cio"><div class="conteudo"><h2>Jornal: delator diz que fez entrega<br /> de dinheiro destinado a AÃ©cio</h2></div></a></div></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://oglobo.globo.com/economia/capa-da-economist-alerta-para-queda-do-brasil-preve-desastre-em-2016-18384376" class="foto " title="Capa de revista prevÃª desastre para o Brasil (ReproduÃ§Ã£o)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/kyUwc9kI50fGtPU05gWZhGzmJ4M=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/rWhFq4dT8hWUR5ylqSAssEqQ7tY=/0x215:459x571/155x120/s.glbimg.com/en/ho/f/original/2015/12/30/economist.jpg" alt="Capa de revista prevÃª desastre para o Brasil (ReproduÃ§Ã£o)" title="Capa de revista prevÃª desastre para o Brasil (ReproduÃ§Ã£o)"
         data-original-image="s2.glbimg.com/rWhFq4dT8hWUR5ylqSAssEqQ7tY=/0x215:459x571/155x120/s.glbimg.com/en/ho/f/original/2015/12/30/economist.jpg" data-url-smart_horizontal="Eg0uV0-x6ueDdfP0noSP-V2-8l8=/90x56/smart/filters:strip_icc()/" data-url-smart="Eg0uV0-x6ueDdfP0noSP-V2-8l8=/90x56/smart/filters:strip_icc()/" data-url-feature="Eg0uV0-x6ueDdfP0noSP-V2-8l8=/90x56/smart/filters:strip_icc()/" data-url-tablet="9AYSo7pOCqeOO7uN4Ptb-6e9adI=/160xorig/smart/filters:strip_icc()/" data-url-desktop="dLvY4FTJWEj0Fa6x1e91JmGWCuw=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Capa de revista prevÃª desastre para o Brasil</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/radicais/noticia/2015/12/skatista-paulista-digo-menezes-faz-contato-com-familia-apos-sumico.html" class="foto " title="CampeÃ£o de skate reaparece em SP: &#39;Abalado&#39; (ReproduÃ§Ã£o Facebook)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/pWv2TdOzFEfHouq2bYoXKPUUGdY=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/9MZiZYZ_G69righmgOMV769AIwE=/23x237:203x377/155x120/s.glbimg.com/es/ge/f/original/2015/12/30/print_digo_menezes.jpg" alt="CampeÃ£o de skate reaparece em SP: &#39;Abalado&#39; (ReproduÃ§Ã£o Facebook)" title="CampeÃ£o de skate reaparece em SP: &#39;Abalado&#39; (ReproduÃ§Ã£o Facebook)"
         data-original-image="s2.glbimg.com/9MZiZYZ_G69righmgOMV769AIwE=/23x237:203x377/155x120/s.glbimg.com/es/ge/f/original/2015/12/30/print_digo_menezes.jpg" data-url-smart_horizontal="9L2YnaYkEOMQSO_xBNsY9SJBLHI=/90x56/smart/filters:strip_icc()/" data-url-smart="9L2YnaYkEOMQSO_xBNsY9SJBLHI=/90x56/smart/filters:strip_icc()/" data-url-feature="9L2YnaYkEOMQSO_xBNsY9SJBLHI=/90x56/smart/filters:strip_icc()/" data-url-tablet="O4QHFsJsw85KhGjR0__koKwGb2E=/160xorig/smart/filters:strip_icc()/" data-url-desktop="Yn1uMkEfSCbcexmaG8nf2QMLldg=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>CampeÃ£o de skate reaparece em SP: &#39;Abalado&#39;</h2></div></a></div></div></div><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/rio-de-janeiro/noticia/2015/12/mulher-que-acusa-pms-de-estupro-na-rocinha-diz-que-nao-consegue-dormir.html" class="foto " title="No Rio, jovem que acusa PMs relata estupro (reproduÃ§Ã£o / g1)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/vpCvEx0VtAU0hGRFFW6bLTxCwg0=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/U9tCxHuVyz3mImFRiXNVAAlswUQ=/0x5:155x75/155x70/s.glbimg.com/en/ho/f/original/2015/12/30/vitima-estupro.jpg" alt="No Rio, jovem que acusa PMs relata estupro (reproduÃ§Ã£o / g1)" title="No Rio, jovem que acusa PMs relata estupro (reproduÃ§Ã£o / g1)"
         data-original-image="s2.glbimg.com/U9tCxHuVyz3mImFRiXNVAAlswUQ=/0x5:155x75/155x70/s.glbimg.com/en/ho/f/original/2015/12/30/vitima-estupro.jpg" data-url-smart_horizontal="qTmldGteQWmDDP6qayB3pJQFKJo=/90x56/smart/filters:strip_icc()/" data-url-smart="qTmldGteQWmDDP6qayB3pJQFKJo=/90x56/smart/filters:strip_icc()/" data-url-feature="qTmldGteQWmDDP6qayB3pJQFKJo=/90x56/smart/filters:strip_icc()/" data-url-tablet="PMX21iykF4gfEKRnVq-Q4m-_DBM=/160xorig/smart/filters:strip_icc()/" data-url-desktop="CGvsD7uRI5wJ0ixOtii42vXbN-g=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>No Rio, jovem que acusa PMs relata  estupro</h2></div></a></div></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/futebol-internacional/jogo/30-12-2015/realmadrid-realsociedad/" class=" " title="CR7 marca 2 no Real Sociedad e garante vitÃ³ria (REUTERS/Juan Medina)"><div class="conteudo"><h2>CR7 marca 2 no Real Sociedad e garante vitÃ³ria</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/central-do-mercado/cobertura.html#/glb-feed-post/5683f86dc22d625fbead282f" class=" " title="Siga o Mercado: VerdÃ£o desiste de Jean, do Flu (BRUNO HADDAD/FLUMINENSE F.C.)"><div class="conteudo"><h2>Siga o Mercado: VerdÃ£o desiste de Jean, do Flu</h2></div></a></div></div></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/times/fluminense/noticia/2015/12/fluminense-confirma-rescisao-de-contrato-com-adidas.html" class=" " title="Flu confirma rescisÃ£o com a Adidas e acerto com nova fornecedora
"><div class="conteudo"><h2>Flu confirma rescisÃ£o com a Adidas e acerto com nova fornecedora<br /></h2></div></a></div></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-entretenimento analytics-area  analytics-id-M
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/vem-por-ai/noticia/2015/12/dante-captura-peca-chave-da-faccao.html" class="foto " title="&#39;A Regra&#39;: Dante prende Guerra (TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/7Wq7FY6vTkn6rzvkxb4YGIsTzh4=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/az3XV7oTtD89_Ze6hT4sCURxzng=/0x35:720x360/155x70/s3.glbimg.com/v1/AUTH_e84042ef78cb4708aeebdf1c68c6cbd6/photos/apis/75be310beb2a47b4bf36cd73bc01aa0f/dante.jpg" alt="&#39;A Regra&#39;: Dante prende Guerra (TV Globo)" title="&#39;A Regra&#39;: Dante prende Guerra (TV Globo)"
         data-original-image="s2.glbimg.com/az3XV7oTtD89_Ze6hT4sCURxzng=/0x35:720x360/155x70/s3.glbimg.com/v1/AUTH_e84042ef78cb4708aeebdf1c68c6cbd6/photos/apis/75be310beb2a47b4bf36cd73bc01aa0f/dante.jpg" data-url-smart_horizontal="dsRShH-0brWSiayTBU_HArWb4g0=/90x56/smart/filters:strip_icc()/" data-url-smart="dsRShH-0brWSiayTBU_HArWb4g0=/90x56/smart/filters:strip_icc()/" data-url-feature="dsRShH-0brWSiayTBU_HArWb4g0=/90x56/smart/filters:strip_icc()/" data-url-tablet="34a_Zpep4MXply3rY5D07G6ZjS8=/160xorig/smart/filters:strip_icc()/" data-url-desktop="kAlHnK2BH2kI70qpJ0fD0C_3gaI=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>&#39;A Regra&#39;: Dante prende Guerra</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/totalmente-demais/vem-por-ai/noticia/2015/12/jonatas-e-salvo-por-jamaica-e-nao-cai-em-armacao-de-fabinho.html" class="foto " title="Jonatas escapa da armaÃ§Ã£o (Artur Meninea/Gshow)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/T3lHYOW4xZbDOhbaKt9KXInALJE=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/b18MdGfkICaUVjp1sMXlGBxbjqI=/132x129:711x391/155x70/s3.glbimg.com/v1/AUTH_e84042ef78cb4708aeebdf1c68c6cbd6/photos/apis/69b3381f48db46f6b9f8261914560cea/germano-jonatas-lili-leila.jpg" alt="Jonatas escapa da armaÃ§Ã£o (Artur Meninea/Gshow)" title="Jonatas escapa da armaÃ§Ã£o (Artur Meninea/Gshow)"
         data-original-image="s2.glbimg.com/b18MdGfkICaUVjp1sMXlGBxbjqI=/132x129:711x391/155x70/s3.glbimg.com/v1/AUTH_e84042ef78cb4708aeebdf1c68c6cbd6/photos/apis/69b3381f48db46f6b9f8261914560cea/germano-jonatas-lili-leila.jpg" data-url-smart_horizontal="2LMKA7_S7cyTw7LzCM4B46EIFaU=/90x56/smart/filters:strip_icc()/" data-url-smart="2LMKA7_S7cyTw7LzCM4B46EIFaU=/90x56/smart/filters:strip_icc()/" data-url-feature="2LMKA7_S7cyTw7LzCM4B46EIFaU=/90x56/smart/filters:strip_icc()/" data-url-tablet="ANyHtlD_Trpuzup-PcOWQq_1W1k=/160xorig/smart/filters:strip_icc()/" data-url-desktop="Y4QWdGZz9iYcuRBCjVa0oSFQ3hQ=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Jonatas escapa da armaÃ§Ã£o</h2></div></a></div></div></div></div><div class="grid-base wide pull-left wide-inferior"></div></div><div id="area-widgets-direita-topo" class="grid-base narrow area-widgets-direta-topo analytics-area analytics-id-V"><div id="ad-position-top1" class="opec"><div id="banner_slim_topo" class="tag-manager-publicidade-container"></div></div><div class="destaque destaque-agrupador-globoplay mobile-grid-full"><a href="http://globoplay.globo.com/" class="go-to analytics-area analytics-id-header">GloboPlay</a><div class="destaque-agrupador-globoplay-container"><div class="stage-container"><ul class="stage analytics-area analytics-id-1 analytics-id-I"><li class="showing"><a href="http://globoplay.globo.com/v/4707273/" title="Apresentador do &#39;Bem Estar&#39; correrÃ¡ na SÃ£o Silvestre" class=""><div class="foto"><div class="foto-overlay"><img src="http://s2.glbimg.com/Cs5VsUuaNkjOfFQTn-0u4K42xBo=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/QmdljHVqDyFIpUSvt1NiMWrUWIc=/335x188/filters:max_age(3600)/s02.video.glbimg.com/deo/vi/73/72/4707273" alt="Apresentador do &#39;Bem Estar&#39; correrÃ¡ na SÃ£o Silvestre" title="Apresentador do &#39;Bem Estar&#39; correrÃ¡ na SÃ£o Silvestre"
                 data-original-image="s2.glbimg.com/QmdljHVqDyFIpUSvt1NiMWrUWIc=/335x188/filters:max_age(3600)/s02.video.glbimg.com/deo/vi/73/72/4707273" data-url-smart_horizontal="DUnrfMUJPLxeN0yp09WE9UxZVkM=/90x56/smart/filters:strip_icc()/" data-url-smart="DUnrfMUJPLxeN0yp09WE9UxZVkM=/90x56/smart/filters:strip_icc()/" data-url-feature="DUnrfMUJPLxeN0yp09WE9UxZVkM=/90x56/smart/filters:strip_icc()/" data-url-tablet="AgdEVv5OYbfq5vfIS31x6ym60Hw=/160xorig/smart/filters:strip_icc()/" data-url-desktop="o6octJHfZ0DWH5ax5W32rM40O_o=/335x188/smart/filters:strip_icc()/"
            /></div><p>2 min</p></div><div class="conteudo"><h3>Globo Esporte SP</h3><h2>Apresentador do &#39;Bem Estar&#39; correrÃ¡ na SÃ£o Silvestre</h2></div></a></li><li><a href="http://globoplay.globo.com/v/4707041/" title="Ana Furtado lÃª pedido de desculpas de FÃª Souza" class="next"><div class="foto"><div class="foto-overlay"><img src="http://s2.glbimg.com/4KPTX9msP8mOb3helkmK2PnIjvk=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/X1GWQ3V-BbXmUtSDkl9Z3KZVrY4=/335x188/filters:max_age(3600)/s02.video.glbimg.com/deo/vi/41/70/4707041" alt="Ana Furtado lÃª pedido de desculpas de FÃª Souza" title="Ana Furtado lÃª pedido de desculpas de FÃª Souza"
                 data-original-image="s2.glbimg.com/X1GWQ3V-BbXmUtSDkl9Z3KZVrY4=/335x188/filters:max_age(3600)/s02.video.glbimg.com/deo/vi/41/70/4707041" data-url-smart_horizontal="7FQa4zGjoSfGkdamtA_6B8oy5Hw=/90x56/smart/filters:strip_icc()/" data-url-smart="7FQa4zGjoSfGkdamtA_6B8oy5Hw=/90x56/smart/filters:strip_icc()/" data-url-feature="7FQa4zGjoSfGkdamtA_6B8oy5Hw=/90x56/smart/filters:strip_icc()/" data-url-tablet="KiPLEs54d7P2OOjY2yckoiDC9f0=/160xorig/smart/filters:strip_icc()/" data-url-desktop="ZI_cjpsthK9LglW4rV6uns01dBQ=/335x188/smart/filters:strip_icc()/"
            /></div><p>1 min</p></div><div class="conteudo"><h3>ENCONTRO COM FÃTIMA</h3><h2>Ana Furtado lÃª pedido de desculpas de FÃª Souza</h2></div></a></li><li><a href="http://globoplay.globo.com/v/4706755/" title="Para trazer dinheiro, roupas amarelas viram preferÃªncia" class="hidden"><div class="foto"><div class="foto-overlay"><img src="http://s2.glbimg.com/9tRJrnoFlz_eoP0eXnCOE8uidi4=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/N-54mmQKlmXxGvRuARbUq6iUISw=/335x188/filters:max_age(3600)/s04.video.glbimg.com/deo/vi/55/67/4706755" alt="Para trazer dinheiro, roupas amarelas viram preferÃªncia" title="Para trazer dinheiro, roupas amarelas viram preferÃªncia"
                 data-original-image="s2.glbimg.com/N-54mmQKlmXxGvRuARbUq6iUISw=/335x188/filters:max_age(3600)/s04.video.glbimg.com/deo/vi/55/67/4706755" data-url-smart_horizontal="mZqk57ERXGvajdR7migCkk5upbs=/90x56/smart/filters:strip_icc()/" data-url-smart="mZqk57ERXGvajdR7migCkk5upbs=/90x56/smart/filters:strip_icc()/" data-url-feature="mZqk57ERXGvajdR7migCkk5upbs=/90x56/smart/filters:strip_icc()/" data-url-tablet="EWQprJUvwAwMbmKWI8dcNTW5pT0=/160xorig/smart/filters:strip_icc()/" data-url-desktop="f29vPs1HvTRis13SZKLbwV_4H74=/335x188/smart/filters:strip_icc()/"
            /></div><p>4 min</p></div><div class="conteudo"><h3>BOM DIA BRASIL</h3><h2>Para trazer dinheiro, roupas amarelas viram preferÃªncia</h2></div></a></li></ul></div><div class="destaque-agrupador-globoplay-bottom-container"><div class="videos-list-container"><ul class="videos-list analytics-area analytics-id-2 analytics-id-I"><li><div class="destaque"><a href="http://globoplay.globo.com/v/4707041/" title="Ana Furtado lÃª pedido de desculpas de FÃª Souza"><div class="foto"><div class="foto-overlay"><img src="http://s2.glbimg.com/4KPTX9msP8mOb3helkmK2PnIjvk=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/X1GWQ3V-BbXmUtSDkl9Z3KZVrY4=/335x188/filters:max_age(3600)/s02.video.glbimg.com/deo/vi/41/70/4707041" alt="Ana Furtado lÃª pedido de desculpas de FÃª Souza" title="Ana Furtado lÃª pedido de desculpas de FÃª Souza"
                         data-original-image="s2.glbimg.com/X1GWQ3V-BbXmUtSDkl9Z3KZVrY4=/335x188/filters:max_age(3600)/s02.video.glbimg.com/deo/vi/41/70/4707041" data-url-smart_horizontal="7FQa4zGjoSfGkdamtA_6B8oy5Hw=/90x56/smart/filters:strip_icc()/" data-url-smart="7FQa4zGjoSfGkdamtA_6B8oy5Hw=/90x56/smart/filters:strip_icc()/" data-url-feature="7FQa4zGjoSfGkdamtA_6B8oy5Hw=/90x56/smart/filters:strip_icc()/" data-url-tablet="1iMZKI96vlWVKs8QggqMC12ROc8=/85x60/smart/filters:strip_icc()/" data-url-desktop="pzCz-rI_B3FIW8k-tna52uD6P7I=/138x78/smart/filters:strip_icc()/"
                    /><p>1 min</p></div></div><div class="conteudo"><h3>ENCONTRO COM FÃTIMA</h3><h2>Ana Furtado lÃª pedido de desculpas de FÃª Souza</h2></div></a></div></li><li><div class="destaque"><a href="http://globoplay.globo.com/v/4706755/" title="Para trazer dinheiro, roupas amarelas viram preferÃªncia"><div class="foto"><div class="foto-overlay"><img src="http://s2.glbimg.com/9tRJrnoFlz_eoP0eXnCOE8uidi4=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/N-54mmQKlmXxGvRuARbUq6iUISw=/335x188/filters:max_age(3600)/s04.video.glbimg.com/deo/vi/55/67/4706755" alt="Para trazer dinheiro, roupas amarelas viram preferÃªncia" title="Para trazer dinheiro, roupas amarelas viram preferÃªncia"
                         data-original-image="s2.glbimg.com/N-54mmQKlmXxGvRuARbUq6iUISw=/335x188/filters:max_age(3600)/s04.video.glbimg.com/deo/vi/55/67/4706755" data-url-smart_horizontal="mZqk57ERXGvajdR7migCkk5upbs=/90x56/smart/filters:strip_icc()/" data-url-smart="mZqk57ERXGvajdR7migCkk5upbs=/90x56/smart/filters:strip_icc()/" data-url-feature="mZqk57ERXGvajdR7migCkk5upbs=/90x56/smart/filters:strip_icc()/" data-url-tablet="98GzXz_T0yn9Pry0sf_pMzjWHyo=/85x60/smart/filters:strip_icc()/" data-url-desktop="O_UrKIocHeUJV8HrxrQfDJyMqiI=/138x78/smart/filters:strip_icc()/"
                    /><p>4 min</p></div></div><div class="conteudo"><h3>BOM DIA BRASIL</h3><h2>Para trazer dinheiro, roupas amarelas viram preferÃªncia</h2></div></a></div></li><li><div class="destaque"><a href="http://globoplay.globo.com/v/4707273/" title="Apresentador do &#39;Bem Estar&#39; correrÃ¡ na SÃ£o Silvestre"><div class="foto"><div class="foto-overlay"><img src="http://s2.glbimg.com/Cs5VsUuaNkjOfFQTn-0u4K42xBo=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/QmdljHVqDyFIpUSvt1NiMWrUWIc=/335x188/filters:max_age(3600)/s02.video.glbimg.com/deo/vi/73/72/4707273" alt="Apresentador do &#39;Bem Estar&#39; correrÃ¡ na SÃ£o Silvestre" title="Apresentador do &#39;Bem Estar&#39; correrÃ¡ na SÃ£o Silvestre"
                         data-original-image="s2.glbimg.com/QmdljHVqDyFIpUSvt1NiMWrUWIc=/335x188/filters:max_age(3600)/s02.video.glbimg.com/deo/vi/73/72/4707273" data-url-smart_horizontal="DUnrfMUJPLxeN0yp09WE9UxZVkM=/90x56/smart/filters:strip_icc()/" data-url-smart="DUnrfMUJPLxeN0yp09WE9UxZVkM=/90x56/smart/filters:strip_icc()/" data-url-feature="DUnrfMUJPLxeN0yp09WE9UxZVkM=/90x56/smart/filters:strip_icc()/" data-url-tablet="pgPzSySew0U_9ACG8ExyzgagdZI=/85x60/smart/filters:strip_icc()/" data-url-desktop="wyoGRzFFYB6m_5jSfdQ_teAmaMA=/138x78/smart/filters:strip_icc()/"
                    /><p>2 min</p></div></div><div class="conteudo"><h3>Globo Esporte SP</h3><h2>Apresentador do &#39;Bem Estar&#39; correrÃ¡ na SÃ£o Silvestre</h2></div></a></div></li></ul></div><a class="more" href="http://globoplay.globo.com/">
            MAIS VÃDEOS
              <svg width="9px" height="6px" viewBox="0 0 9 6" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M8.176,2.996 L4.242,0.14 L4.41,1.946 L0.658,1.946 L0.658,4.046 L4.41,4.046 L4.242,5.866 L8.176,2.996 Z"></path></svg></a></div></div><div class="destaque-agrupador-globoplay-container-responsive"><div class="slider"><ul><li class="analytics-area analytics-id-1 analytics-id-I"><div class="destaque"><a href="http://globoplay.globo.com/v/4707273/" title="Apresentador do &#39;Bem Estar&#39; correrÃ¡ na SÃ£o Silvestre"><div class="foto"><div class="foto-overlay"><img src="http://s2.glbimg.com/Cs5VsUuaNkjOfFQTn-0u4K42xBo=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/QmdljHVqDyFIpUSvt1NiMWrUWIc=/335x188/filters:max_age(3600)/s02.video.glbimg.com/deo/vi/73/72/4707273" alt="Apresentador do &#39;Bem Estar&#39; correrÃ¡ na SÃ£o Silvestre" title="Apresentador do &#39;Bem Estar&#39; correrÃ¡ na SÃ£o Silvestre"
                         data-original-image="s2.glbimg.com/QmdljHVqDyFIpUSvt1NiMWrUWIc=/335x188/filters:max_age(3600)/s02.video.glbimg.com/deo/vi/73/72/4707273" data-url-smart_horizontal="DUnrfMUJPLxeN0yp09WE9UxZVkM=/90x56/smart/filters:strip_icc()/" data-url-smart="DUnrfMUJPLxeN0yp09WE9UxZVkM=/90x56/smart/filters:strip_icc()/" data-url-feature="DUnrfMUJPLxeN0yp09WE9UxZVkM=/90x56/smart/filters:strip_icc()/" data-url-tablet="pgPzSySew0U_9ACG8ExyzgagdZI=/85x60/smart/filters:strip_icc()/" data-url-desktop="wyoGRzFFYB6m_5jSfdQ_teAmaMA=/138x78/smart/filters:strip_icc()/"
                    /><p>2 min</p></div></div><div class="conteudo"><h3>Globo Esporte SP</h3><h2>Apresentador do &#39;Bem Estar&#39; correrÃ¡ na SÃ£o Silvestre</h2></div></a></div></li><li class="analytics-area analytics-id-2 analytics-id-I"><div class="destaque"><a href="http://globoplay.globo.com/v/4707041/" title="Ana Furtado lÃª pedido de desculpas de FÃª Souza"><div class="foto"><div class="foto-overlay"><img src="http://s2.glbimg.com/4KPTX9msP8mOb3helkmK2PnIjvk=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/X1GWQ3V-BbXmUtSDkl9Z3KZVrY4=/335x188/filters:max_age(3600)/s02.video.glbimg.com/deo/vi/41/70/4707041" alt="Ana Furtado lÃª pedido de desculpas de FÃª Souza" title="Ana Furtado lÃª pedido de desculpas de FÃª Souza"
                         data-original-image="s2.glbimg.com/X1GWQ3V-BbXmUtSDkl9Z3KZVrY4=/335x188/filters:max_age(3600)/s02.video.glbimg.com/deo/vi/41/70/4707041" data-url-smart_horizontal="7FQa4zGjoSfGkdamtA_6B8oy5Hw=/90x56/smart/filters:strip_icc()/" data-url-smart="7FQa4zGjoSfGkdamtA_6B8oy5Hw=/90x56/smart/filters:strip_icc()/" data-url-feature="7FQa4zGjoSfGkdamtA_6B8oy5Hw=/90x56/smart/filters:strip_icc()/" data-url-tablet="1iMZKI96vlWVKs8QggqMC12ROc8=/85x60/smart/filters:strip_icc()/" data-url-desktop="pzCz-rI_B3FIW8k-tna52uD6P7I=/138x78/smart/filters:strip_icc()/"
                    /><p>1 min</p></div></div><div class="conteudo"><h3>ENCONTRO COM FÃTIMA</h3><h2>Ana Furtado lÃª pedido de desculpas de FÃª Souza</h2></div></a></div></li><li class="analytics-area analytics-id-3 analytics-id-I"><div class="destaque"><a href="http://globoplay.globo.com/v/4706755/" title="Para trazer dinheiro, roupas amarelas viram preferÃªncia"><div class="foto"><div class="foto-overlay"><img src="http://s2.glbimg.com/9tRJrnoFlz_eoP0eXnCOE8uidi4=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/N-54mmQKlmXxGvRuARbUq6iUISw=/335x188/filters:max_age(3600)/s04.video.glbimg.com/deo/vi/55/67/4706755" alt="Para trazer dinheiro, roupas amarelas viram preferÃªncia" title="Para trazer dinheiro, roupas amarelas viram preferÃªncia"
                         data-original-image="s2.glbimg.com/N-54mmQKlmXxGvRuARbUq6iUISw=/335x188/filters:max_age(3600)/s04.video.glbimg.com/deo/vi/55/67/4706755" data-url-smart_horizontal="mZqk57ERXGvajdR7migCkk5upbs=/90x56/smart/filters:strip_icc()/" data-url-smart="mZqk57ERXGvajdR7migCkk5upbs=/90x56/smart/filters:strip_icc()/" data-url-feature="mZqk57ERXGvajdR7migCkk5upbs=/90x56/smart/filters:strip_icc()/" data-url-tablet="98GzXz_T0yn9Pry0sf_pMzjWHyo=/85x60/smart/filters:strip_icc()/" data-url-desktop="O_UrKIocHeUJV8HrxrQfDJyMqiI=/138x78/smart/filters:strip_icc()/"
                    /><p>4 min</p></div></div><div class="conteudo"><h3>BOM DIA BRASIL</h3><h2>Para trazer dinheiro, roupas amarelas viram preferÃªncia</h2></div></a></div></li></ul></div><div class="page-marker"><li class="active"><div></div></li><li><div></div></li><li><div></div></li></div></div></div></div></div><div class="separator-first-scroll-container"><section><div><div id="opec-banner-middle-container" class="opec-banner-middle-container"><div id="ad-position-middle" class="opec"><div id="banner_slb_meio" class="tag-manager-publicidade-container"></div></div></div></div></section></div><div id="x62" class="opec-area opec-mobile opec-x62 grid-12"><div id="banner_mobile_meio" class="tag-manager-publicidade-container"></div></div><div id="container-columns" class="container columns clearfix glb-area-colunas"><section class="area news-column   last-has-photo
            
        
        "><div class="destaques-container closed analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/rio-de-janeiro/noticia/2015/12/rio-registra-sensacao-termica-de-quase-39c-na-manha-de-quarta.html" class="foto" title="Temperatura no Rio jÃ¡ passa de 41ÂºC,
 e sensaÃ§Ã£o tÃ©rmica deve superar 50ÂºC (Marcus Victorio/Brazil Photo Press/EstadÃ£o ConteÃºdo)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/vR7Hdv5LzfUB07Cw3u9U8Om3OgI=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/k3ymRdgxyUMeW3F_mXZsCbTN3CE=/0x135:1700x1048/335x180/s.glbimg.com/jo/g1/f/original/2015/12/30/clima-rj_marcus_victorio_estadao_conteudo.jpg" alt="Temperatura no Rio jÃ¡ passa de 41ÂºC,
 e sensaÃ§Ã£o tÃ©rmica deve superar 50ÂºC (Marcus Victorio/Brazil Photo Press/EstadÃ£o ConteÃºdo)" title="Temperatura no Rio jÃ¡ passa de 41ÂºC,
 e sensaÃ§Ã£o tÃ©rmica deve superar 50ÂºC (Marcus Victorio/Brazil Photo Press/EstadÃ£o ConteÃºdo)"
                data-original-image="s2.glbimg.com/k3ymRdgxyUMeW3F_mXZsCbTN3CE=/0x135:1700x1048/335x180/s.glbimg.com/jo/g1/f/original/2015/12/30/clima-rj_marcus_victorio_estadao_conteudo.jpg" data-url-smart_horizontal="l4OoKuDT5lmLd0XyUI2JlGanckg=/90x0/smart/filters:strip_icc()/" data-url-smart="l4OoKuDT5lmLd0XyUI2JlGanckg=/90x0/smart/filters:strip_icc()/" data-url-feature="l4OoKuDT5lmLd0XyUI2JlGanckg=/90x0/smart/filters:strip_icc()/" data-url-tablet="-aKcvdnq8HGpEWF2pS3i2VDdlpA=/220x125/smart/filters:strip_icc()/" data-url-desktop="dx0sXlNTUpjvgHwDEXpSkVDprQc=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Temperatura no Rio jÃ¡ passa de 41ÂºC,<br /> e sensaÃ§Ã£o tÃ©rmica deve superar 50ÂºC</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-30 14:36:10" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/pi/noticia/2015/12/gerente-de-futebol-do-river-pi-e-preso-por-assassinato-de-ex-namorada.html" class="foto" title="Gerente de futebol de clube do PiauÃ­ Ã© preso pelo assassinato da ex (Renan Morais )" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/7gA2GRn-hEA7UrkJSy9zI1bpshA=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/lNdNfc6zYqC62bF53wlTPLSQ4G8=/38x77:957x691/120x80/s.glbimg.com/es/ge/f/original/2015/12/08/boiadeiro.jpg" alt="Gerente de futebol de clube do PiauÃ­ Ã© preso pelo assassinato da ex (Renan Morais )" title="Gerente de futebol de clube do PiauÃ­ Ã© preso pelo assassinato da ex (Renan Morais )"
                data-original-image="s2.glbimg.com/lNdNfc6zYqC62bF53wlTPLSQ4G8=/38x77:957x691/120x80/s.glbimg.com/es/ge/f/original/2015/12/08/boiadeiro.jpg" data-url-smart_horizontal="ud1YjNgspmhYM6fWF25HXq-c4YQ=/90x0/smart/filters:strip_icc()/" data-url-smart="ud1YjNgspmhYM6fWF25HXq-c4YQ=/90x0/smart/filters:strip_icc()/" data-url-feature="ud1YjNgspmhYM6fWF25HXq-c4YQ=/90x0/smart/filters:strip_icc()/" data-url-tablet="IX3ian64wouMhBQkM32TyBfsmhc=/70x50/smart/filters:strip_icc()/" data-url-desktop="Je4__EzoT7Lm9PMydp0p1g6djbc=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Gerente de futebol de clube do PiauÃ­ Ã© preso<br /> pelo assassinato da ex</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-30 08:34:41" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/tv-e-lazer/telinha/alem-do-tempo-flora-diegues-opera-cabeca-as-pressas-deixa-novela-18380482.html" class="foto" title="Atriz Flora Diegues opera cabeÃ§a por causa de aneurisma aos 29 anos (Estevam Avellar/Rede Globo/DivulgaÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/TUZS0jLAS04JKxRHj1O0ILrNtOo=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/ZQgiUhxn1Bu4GODC49G9_gBMrH8=/150x62:363x204/120x80/s.glbimg.com/en/ho/f/original/2015/12/30/flora_atriz_de_alem.jpg" alt="Atriz Flora Diegues opera cabeÃ§a por causa de aneurisma aos 29 anos (Estevam Avellar/Rede Globo/DivulgaÃ§Ã£o)" title="Atriz Flora Diegues opera cabeÃ§a por causa de aneurisma aos 29 anos (Estevam Avellar/Rede Globo/DivulgaÃ§Ã£o)"
                data-original-image="s2.glbimg.com/ZQgiUhxn1Bu4GODC49G9_gBMrH8=/150x62:363x204/120x80/s.glbimg.com/en/ho/f/original/2015/12/30/flora_atriz_de_alem.jpg" data-url-smart_horizontal="nhHG78dqSF4FnUatPCAEKLSmd1M=/90x0/smart/filters:strip_icc()/" data-url-smart="nhHG78dqSF4FnUatPCAEKLSmd1M=/90x0/smart/filters:strip_icc()/" data-url-feature="nhHG78dqSF4FnUatPCAEKLSmd1M=/90x0/smart/filters:strip_icc()/" data-url-tablet="nl-2cV9WJhBPn3Sikcmii4mAl0o=/70x50/smart/filters:strip_icc()/" data-url-desktop="v79yCGJHH_XrPRis-aB2SSgOeQQ=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Atriz Flora Diegues opera cabeÃ§a por causa de aneurisma aos 29 anos</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://oglobo.globo.com/mundo/turquia-detem-dois-suspeitos-de-planejar-ataque-durantes-as-celebracoes-do-ano-novo-18383648" class="" title="Turquia detÃ©m dois suspeitos de planejar ataque durantes as celebraÃ§Ãµes do Ano Novo" rel="bookmark"><span class="conteudo"><p>Turquia detÃ©m dois suspeitos de planejar ataque durantes as celebraÃ§Ãµes do Ano Novo</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-30 16:30:27" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/noticias/rio/jovem-cortado-por-linha-chilena-em-noite-de-natal-sobrevive-18385732.html" class="foto" title="Jovem de 16 anos pega a moto do pai e tem pescoÃ§o cortado por linha chilena (Extra)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/WdgLBNSiXWtloKAWjsWOA_-Te_E=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/xp0uAL_Z5n61PMBNso0xyTJxlj4=/0x181:533x536/120x80/s.glbimg.com/en/ho/f/original/2015/12/30/linhachilena.png" alt="Jovem de 16 anos pega a moto do pai e tem pescoÃ§o cortado por linha chilena (Extra)" title="Jovem de 16 anos pega a moto do pai e tem pescoÃ§o cortado por linha chilena (Extra)"
                data-original-image="s2.glbimg.com/xp0uAL_Z5n61PMBNso0xyTJxlj4=/0x181:533x536/120x80/s.glbimg.com/en/ho/f/original/2015/12/30/linhachilena.png" data-url-smart_horizontal="fBlJyBjzGKHlvuGzP8ZvOTZ1NGc=/90x0/smart/filters:strip_icc()/" data-url-smart="fBlJyBjzGKHlvuGzP8ZvOTZ1NGc=/90x0/smart/filters:strip_icc()/" data-url-feature="fBlJyBjzGKHlvuGzP8ZvOTZ1NGc=/90x0/smart/filters:strip_icc()/" data-url-tablet="UFy_9On4tK5PwxtMtktC8Smz8H8=/70x50/smart/filters:strip_icc()/" data-url-desktop="qkQtYUD25OKxi6_-Jgo1GJ_vlI8=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Jovem de 16 anos pega a moto do pai e tem pescoÃ§o cortado por linha chilena</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-30 10:18:59" class="chamada chamada-principal mobile-grid-full"><a href="http://www.techtudo.com.br/listas/noticia/2015/12/gta-5-need-speed-confira-os-jogos-com-melhores-graficos-de-2015.html" class="foto" title="Veja os jogos com grÃ¡ficos mais &#39;perfeitos&#39; que foram lanÃ§ados ao longo de 2015 (DivulgaÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/tSxHLOVPBs4t0obT-DsjWuVcSEE=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/W3AvbPMZwWI7B3TJLz2ehkEG_28=/13x0:467x302/120x80/s.glbimg.com/po/tt2/f/original/2015/12/30/the-witcher-3-wild-hunt.jpg" alt="Veja os jogos com grÃ¡ficos mais &#39;perfeitos&#39; que foram lanÃ§ados ao longo de 2015 (DivulgaÃ§Ã£o)" title="Veja os jogos com grÃ¡ficos mais &#39;perfeitos&#39; que foram lanÃ§ados ao longo de 2015 (DivulgaÃ§Ã£o)"
                data-original-image="s2.glbimg.com/W3AvbPMZwWI7B3TJLz2ehkEG_28=/13x0:467x302/120x80/s.glbimg.com/po/tt2/f/original/2015/12/30/the-witcher-3-wild-hunt.jpg" data-url-smart_horizontal="RABdA1Xm5udcYLy-3PSavYX1q0A=/90x0/smart/filters:strip_icc()/" data-url-smart="RABdA1Xm5udcYLy-3PSavYX1q0A=/90x0/smart/filters:strip_icc()/" data-url-feature="RABdA1Xm5udcYLy-3PSavYX1q0A=/90x0/smart/filters:strip_icc()/" data-url-tablet="nGF4RalofZCYqmi1sZYm4mKUe5M=/70x50/smart/filters:strip_icc()/" data-url-desktop="gft-6rg2lc9wtsm1iq2o5ix3M8U=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Veja os jogos com grÃ¡ficos <br />mais &#39;perfeitos&#39; que foram <br />lanÃ§ados ao longo de 2015</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-30 16:01:41" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/sp/santos-regiao/noticia/2015/12/homem-e-preso-apos-esfaquear-ex-mulher-dentro-de-igreja-em-santos.html" class="" title="Homem Ã© preso apÃ³s esfaquear a ex-mulher dentro de igreja evangÃ©lica em Santos (Arquivo Pessoal)" rel="bookmark"><span class="conteudo"><p>Homem Ã© preso apÃ³s esfaquear a ex-mulher dentro de igreja evangÃ©lica em Santos</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-30 13:12:41" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/bahia/noticia/2015/12/turista-de-brasilia-morre-e-namorado-fica-ferido-em-acidente-no-sul-da-bahia.html" class="foto" title="BA: carro perde controle, mata turista do DF de 23 anos e fere namorado dela (Aurea Catharina/Arquivo Pessoal)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/ovY0uybXxCvO4VALKlQD6OUy9tQ=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/8796XTsM3MqVPF_RbM4AVAoUqBc=/0x44:620x458/120x80/s.glbimg.com/jo/g1/f/original/2015/12/30/acidente_portoseguro_2.jpg" alt="BA: carro perde controle, mata turista do DF de 23 anos e fere namorado dela (Aurea Catharina/Arquivo Pessoal)" title="BA: carro perde controle, mata turista do DF de 23 anos e fere namorado dela (Aurea Catharina/Arquivo Pessoal)"
                data-original-image="s2.glbimg.com/8796XTsM3MqVPF_RbM4AVAoUqBc=/0x44:620x458/120x80/s.glbimg.com/jo/g1/f/original/2015/12/30/acidente_portoseguro_2.jpg" data-url-smart_horizontal="Tp0tw0J5AdMZHz8RAAb2Bv1IcB4=/90x0/smart/filters:strip_icc()/" data-url-smart="Tp0tw0J5AdMZHz8RAAb2Bv1IcB4=/90x0/smart/filters:strip_icc()/" data-url-feature="Tp0tw0J5AdMZHz8RAAb2Bv1IcB4=/90x0/smart/filters:strip_icc()/" data-url-tablet="qVdTd7RWqSP0Dt4ZaqEaTshhm2E=/70x50/smart/filters:strip_icc()/" data-url-desktop="5_2pE5kwnTe67ET26pJKaCgRuKI=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>BA: carro perde controle, mata turista do DF de 23 anos e fere namorado dela</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-30 10:46:22" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/rn/rio-grande-do-norte/noticia/2015/12/precisamos-de-ajuda-diz-irma-de-mulher-que-pesa-360-kg-em-natal.html" class="foto" title="IrmÃ£ de mulher que pesa 360 kg em Natal pede ajuda: &#39;Chegamos ao limite&#39; (Corpo de Bombeiros/DivulgaÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/ljL5pPkzxpG-JzNW9tMJtGCYsxg=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/VHGOdY31S5Wya1EvWi4RhU_LC6w=/511x88:1167x525/120x80/s.glbimg.com/jo/g1/f/original/2015/12/29/resgate.jpg" alt="IrmÃ£ de mulher que pesa 360 kg em Natal pede ajuda: &#39;Chegamos ao limite&#39; (Corpo de Bombeiros/DivulgaÃ§Ã£o)" title="IrmÃ£ de mulher que pesa 360 kg em Natal pede ajuda: &#39;Chegamos ao limite&#39; (Corpo de Bombeiros/DivulgaÃ§Ã£o)"
                data-original-image="s2.glbimg.com/VHGOdY31S5Wya1EvWi4RhU_LC6w=/511x88:1167x525/120x80/s.glbimg.com/jo/g1/f/original/2015/12/29/resgate.jpg" data-url-smart_horizontal="Ls0CsiHLJsDMdcZE4HxA6SvVfXc=/90x0/smart/filters:strip_icc()/" data-url-smart="Ls0CsiHLJsDMdcZE4HxA6SvVfXc=/90x0/smart/filters:strip_icc()/" data-url-feature="Ls0CsiHLJsDMdcZE4HxA6SvVfXc=/90x0/smart/filters:strip_icc()/" data-url-tablet="h8wN_w4mdRLXYfWdVtOaILnS-yE=/70x50/smart/filters:strip_icc()/" data-url-desktop="Gz5bROEwCBLbBOOoJkW-lZeUZ5o=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>IrmÃ£ de mulher que pesa 360 kg em Natal pede ajuda: &#39;Chegamos ao limite&#39;</p></span></a></div></div><div class="show-more"><span class="setinha-show-more"></span></div></div><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-noticias
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "mogi-e-suzano": {"ordering": 1, "url": "http://g1.globo.com/sp/mogi-das-cruzes-suzano", "name": "Mogi e Suzano"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "mogi-e-suzano": {"ordering": 1, "url": "http://g1.globo.com/sp/mogi-das-cruzes-suzano", "name": "Mogi e Suzano"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}});}
</script><div id="libby-home-widget-economia" class="libby-home-widgetBase libby-home-widget-economia analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais notÃ­cias" href="http://g1.globo.com/"><span class="text">mais notÃ­cias</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area sports-column   last-has-photo
            
        
        "><div class="destaques-container closed analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/pe/noticia/2015/12/mourinho-tenta-mas-nao-consegue-escapar-dos-fas-em-pernambuco.html" class="foto" title="Mourinho faz de tudo para evitar, mas Ã© clicado por sÃ£o-paulino no Nordeste (Arquivo Pessoal)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/1sMpH4NAvD8rYqY8MJ1f1Hu_XqU=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/vBalXgvQyEnD2dMVD4_IFON3yAY=/20x102:520x371/335x180/s.glbimg.com/es/ge/f/original/2015/12/30/mourinho.jpg" alt="Mourinho faz de tudo para evitar, mas Ã© clicado por sÃ£o-paulino no Nordeste (Arquivo Pessoal)" title="Mourinho faz de tudo para evitar, mas Ã© clicado por sÃ£o-paulino no Nordeste (Arquivo Pessoal)"
                data-original-image="s2.glbimg.com/vBalXgvQyEnD2dMVD4_IFON3yAY=/20x102:520x371/335x180/s.glbimg.com/es/ge/f/original/2015/12/30/mourinho.jpg" data-url-smart_horizontal="3hgeydy54tj0D5eyNFlwkswQqA0=/90x0/smart/filters:strip_icc()/" data-url-smart="3hgeydy54tj0D5eyNFlwkswQqA0=/90x0/smart/filters:strip_icc()/" data-url-feature="3hgeydy54tj0D5eyNFlwkswQqA0=/90x0/smart/filters:strip_icc()/" data-url-tablet="SwhBph3lhBXm6cFyrzcIjQO-yAU=/220x125/smart/filters:strip_icc()/" data-url-desktop="OdK8s1_ApRiKrY_QLhDMxwKp1qY=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Mourinho faz de tudo para evitar, mas Ã© clicado por sÃ£o-paulino no Nordeste</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-30 16:22:06" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/futebol-internacional/futebol-ingles/noticia/2015/12/oscar-entra-em-selecao-de-piores-do-ano-feita-por-jornal-ingles.html" class="foto" title="Brasileiro Oscar entra em seleÃ§Ã£o de piores do ano feita por jornal inglÃªs (Reuters)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/deYz84Ffz6wu1mAwHcyzYMfgErk=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/u7YEeuZg8HracLRrWrTgOj_HUrs=/1034x161:1725x621/120x80/s.glbimg.com/es/ge/f/original/2015/12/26/2015-12-26t164719z_94150039_mt1aci14231252_rtrmadp_3_soccer-england-che-wat_1.jpg" alt="Brasileiro Oscar entra em seleÃ§Ã£o de piores do ano feita por jornal inglÃªs (Reuters)" title="Brasileiro Oscar entra em seleÃ§Ã£o de piores do ano feita por jornal inglÃªs (Reuters)"
                data-original-image="s2.glbimg.com/u7YEeuZg8HracLRrWrTgOj_HUrs=/1034x161:1725x621/120x80/s.glbimg.com/es/ge/f/original/2015/12/26/2015-12-26t164719z_94150039_mt1aci14231252_rtrmadp_3_soccer-england-che-wat_1.jpg" data-url-smart_horizontal="jWpJvj30W_4FXVLR1xiUvojA0IE=/90x0/smart/filters:strip_icc()/" data-url-smart="jWpJvj30W_4FXVLR1xiUvojA0IE=/90x0/smart/filters:strip_icc()/" data-url-feature="jWpJvj30W_4FXVLR1xiUvojA0IE=/90x0/smart/filters:strip_icc()/" data-url-tablet="Z97EMKg_yitMSZryVfaYAiwec4c=/70x50/smart/filters:strip_icc()/" data-url-desktop="qzgT1zRGOCL307u2jd36XgBgo9k=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Brasileiro Oscar entra em seleÃ§Ã£o de piores do ano feita por jornal inglÃªs</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-30 16:34:16" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/motor/stock-car/noticia/2015/12/vencedor-da-ultima-etapa-do-ano-atila-curte-ferias-com-renata-fan-na-disney.html" class="foto" title="Piloto da Stock Car passa fÃ©rias na Disney ao lado da apresentadora Renata Fan (DivulgaÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/EYR4y07JObfjBipd-XgD6b5W-wA=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/e8vjzOfkpP20Azf0cW31Vz_C_qc=/0x111:656x548/120x80/s.glbimg.com/es/ge/f/original/2015/12/30/atila_2.png" alt="Piloto da Stock Car passa fÃ©rias na Disney ao lado da apresentadora Renata Fan (DivulgaÃ§Ã£o)" title="Piloto da Stock Car passa fÃ©rias na Disney ao lado da apresentadora Renata Fan (DivulgaÃ§Ã£o)"
                data-original-image="s2.glbimg.com/e8vjzOfkpP20Azf0cW31Vz_C_qc=/0x111:656x548/120x80/s.glbimg.com/es/ge/f/original/2015/12/30/atila_2.png" data-url-smart_horizontal="l986zfCIn9TWCNLdFaHCfxIrAyU=/90x0/smart/filters:strip_icc()/" data-url-smart="l986zfCIn9TWCNLdFaHCfxIrAyU=/90x0/smart/filters:strip_icc()/" data-url-feature="l986zfCIn9TWCNLdFaHCfxIrAyU=/90x0/smart/filters:strip_icc()/" data-url-tablet="y5t49ZLzqhkgqXveAoyHDOZz_ak=/70x50/smart/filters:strip_icc()/" data-url-desktop="UH9pIVwG1qdXD9zeOZnBv-zfOSA=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Piloto da Stock Car passa fÃ©rias na Disney ao lado da apresentadora Renata Fan</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-30 14:13:33" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/blogs/especial-blog/bastidores-fc/post/pedido-dos-eua-suica-bloqueia-us-80-milhoes-em-13-contas-de-cartolas-da-fifa.html" class="" title="A pedido dos EUA, SuÃ­Ã§a bloqueia US$ 80 milhÃµes em 13 contas de cartolas da Fifa" rel="bookmark"><span class="conteudo"><p>A pedido dos EUA, SuÃ­Ã§a bloqueia US$ 80 milhÃµes em 13 contas de cartolas da Fifa</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-30 08:02:38" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/flamengo/noticia/2015/12/fla-emprega-r-2-milhoes-de-novos-titulos-da-torcida-para-avancos-no-ct.html" class="foto" title="Fla emprega R$ 2 milhÃµes de novos tÃ­tulos da torcida para avanÃ§os no CT (Ivan Raupp)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/l23ntAJC4gynidAGkIEXSBALIoc=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/hwUsU7dClMWeQL-Zs2Hywcv21sA=/0x95:3264x2273/120x80/s.glbimg.com/es/ge/f/original/2015/12/14/modulos_16_e_17.jpg" alt="Fla emprega R$ 2 milhÃµes de novos tÃ­tulos da torcida para avanÃ§os no CT (Ivan Raupp)" title="Fla emprega R$ 2 milhÃµes de novos tÃ­tulos da torcida para avanÃ§os no CT (Ivan Raupp)"
                data-original-image="s2.glbimg.com/hwUsU7dClMWeQL-Zs2Hywcv21sA=/0x95:3264x2273/120x80/s.glbimg.com/es/ge/f/original/2015/12/14/modulos_16_e_17.jpg" data-url-smart_horizontal="KG3dj8FP1h7UtzQDKtBtt27VgXs=/90x0/smart/filters:strip_icc()/" data-url-smart="KG3dj8FP1h7UtzQDKtBtt27VgXs=/90x0/smart/filters:strip_icc()/" data-url-feature="KG3dj8FP1h7UtzQDKtBtt27VgXs=/90x0/smart/filters:strip_icc()/" data-url-tablet="Xn_Iq6_4dxS0ydjm6c6euNhUxf8=/70x50/smart/filters:strip_icc()/" data-url-desktop="k-3D0CWXsvkOpxNid41ItOS57OE=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Fla emprega R$ 2 milhÃµes de novos tÃ­tulos da torcida para avanÃ§os no CT</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-30 08:02:38" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/esporte/vice-campeao-na-india-leo-moura-curte-ferias-em-angra-dos-reis-com-mulher-as-filhas-18384646.html" class="foto" title="Vice-campeÃ£o na Ãndia, Leo Moura viaja para Angra com a mulher e as filhas (ReproduÃ§Ã£o/Instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/k--yryblbWpfVFyTEVrAkLHAAIg=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/TMcR-6u-Bv2Ive45FIOpXp4jPkM=/52x129:346x325/120x80/s.glbimg.com/en/ho/f/original/2015/12/30/leo-moura-angra-1.jpg" alt="Vice-campeÃ£o na Ãndia, Leo Moura viaja para Angra com a mulher e as filhas (ReproduÃ§Ã£o/Instagram)" title="Vice-campeÃ£o na Ãndia, Leo Moura viaja para Angra com a mulher e as filhas (ReproduÃ§Ã£o/Instagram)"
                data-original-image="s2.glbimg.com/TMcR-6u-Bv2Ive45FIOpXp4jPkM=/52x129:346x325/120x80/s.glbimg.com/en/ho/f/original/2015/12/30/leo-moura-angra-1.jpg" data-url-smart_horizontal="k64FNXuImvZjZf5RbWhhXnh3Kc8=/90x0/smart/filters:strip_icc()/" data-url-smart="k64FNXuImvZjZf5RbWhhXnh3Kc8=/90x0/smart/filters:strip_icc()/" data-url-feature="k64FNXuImvZjZf5RbWhhXnh3Kc8=/90x0/smart/filters:strip_icc()/" data-url-tablet="beXzL-6Ace5y98eAvaMDeRC4D9s=/70x50/smart/filters:strip_icc()/" data-url-desktop="DzfzERYEM0ds-rtS8FapPX4v-n4=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Vice-campeÃ£o na Ãndia, Leo Moura viaja para Angra com a mulher e as filhas</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/cruzeiro/noticia/2015/12/talisca-eder-cruzeiro-tenta-repatriar-atacantes-para-vaga-de-damiao.html" class="" title="Talisca, Ãder... Cruzeiro tenta repatriar atacantes para a vaga de DamiÃ£o" rel="bookmark"><span class="conteudo"><p>Talisca, Ãder... Cruzeiro tenta repatriar atacantes para a vaga de DamiÃ£o</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/esporte/campeao-mundial-de-surfe-adriano-de-souza-curte-almoco-em-florianopolis-com-familia-de-guga-18384819.html" class="foto" title="CampeÃ£o Mineirinho curte FlorianÃ³polis com a famÃ­lia de Guga (ReproduÃ§Ã£o/Instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/z7oWBBK3f4HBjC1X7nuSmW1nDsY=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/YXoRd9W7f-3pRRxE774hMmFeAdc=/152x0:976x549/120x80/s.glbimg.com/en/ho/f/original/2015/12/30/guga-surfe.jpg" alt="CampeÃ£o Mineirinho curte FlorianÃ³polis com a famÃ­lia de Guga (ReproduÃ§Ã£o/Instagram)" title="CampeÃ£o Mineirinho curte FlorianÃ³polis com a famÃ­lia de Guga (ReproduÃ§Ã£o/Instagram)"
                data-original-image="s2.glbimg.com/YXoRd9W7f-3pRRxE774hMmFeAdc=/152x0:976x549/120x80/s.glbimg.com/en/ho/f/original/2015/12/30/guga-surfe.jpg" data-url-smart_horizontal="YFve8IldsWUxBsaGPsISxFGpwow=/90x0/smart/filters:strip_icc()/" data-url-smart="YFve8IldsWUxBsaGPsISxFGpwow=/90x0/smart/filters:strip_icc()/" data-url-feature="YFve8IldsWUxBsaGPsISxFGpwow=/90x0/smart/filters:strip_icc()/" data-url-tablet="JW5EuO6szv-dA6_wYAefnY-1dts=/70x50/smart/filters:strip_icc()/" data-url-desktop="VVcY3IUmC5dcOGgyYjEWQc6maHM=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>CampeÃ£o Mineirinho<br /> curte FlorianÃ³polis <br />com a famÃ­lia de Guga</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-30 13:55:34" class="chamada chamada-principal mobile-grid-full"><a href="http://sportv.globo.com/site/combate/noticia/2015/12/irma-afirma-que-uma-parte-de-ronda-morreu-na-derrota-para-holly-holm.html" class="foto" title="IrmÃ£ de Ronda diz que parte da lutadora &#39;morreu&#39; na derrota para Holly Holm (reproduÃ§Ã£o/Youtube)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/doXatRtjUp_GXd_-VD1BY9qit6I=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/khdy6-WhU7m9gCMuUjUXi_LcgkQ=/0x89:421x369/120x80/s.glbimg.com/es/ge/f/original/2015/11/20/rr_1.jpg" alt="IrmÃ£ de Ronda diz que parte da lutadora &#39;morreu&#39; na derrota para Holly Holm (reproduÃ§Ã£o/Youtube)" title="IrmÃ£ de Ronda diz que parte da lutadora &#39;morreu&#39; na derrota para Holly Holm (reproduÃ§Ã£o/Youtube)"
                data-original-image="s2.glbimg.com/khdy6-WhU7m9gCMuUjUXi_LcgkQ=/0x89:421x369/120x80/s.glbimg.com/es/ge/f/original/2015/11/20/rr_1.jpg" data-url-smart_horizontal="U4pwxbyTP8oKCv9jy1q5qJF53VU=/90x0/smart/filters:strip_icc()/" data-url-smart="U4pwxbyTP8oKCv9jy1q5qJF53VU=/90x0/smart/filters:strip_icc()/" data-url-feature="U4pwxbyTP8oKCv9jy1q5qJF53VU=/90x0/smart/filters:strip_icc()/" data-url-tablet="gfClCbwm94FZCFFhPJ7oFFcOCDs=/70x50/smart/filters:strip_icc()/" data-url-desktop="VQdjoczyD5dmUyW34iiDEiqfs3c=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>IrmÃ£ de Ronda diz que parte da lutadora &#39;morreu&#39; na derrota para Holly Holm</p></span></a></div></div><div class="show-more"><span class="setinha-show-more"></span></div></div><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-times soccer-teams
"></div><div id="libby-home-widget-brasileirao" class="libby-home-widgetBase libby-home-widget-brasileirao analytics-area analytics-id-W"><input type="hidden" name="rodada_selecionada" value="current"></div><a class="link-see-more analytics-area analytics-id-L" title="mais esportes" href="http://globoesporte.globo.com/"><span class="text">mais esportes</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area etc-column analytics-area   last-has-photo
            
        
        "><div class="destaques-container closed analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/famosos/zilu-godoi-posa-de-biquini-durante-ferias-em-praia-enquanto-zeze-viaja-com-graciele-lacerda-18383858.html" class="foto" title="Zilu posa de fio dental em fÃ©rias na praia e mostra estar podendo aos 54 (reproduÃ§Ã£o / instagram )" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/FBZMTYS_6h7EZrBx26yLEgl8v9U=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/Tj1ulAFN1oW3UwgzSfxIUaNMZ4o=/0x17:448x258/335x180/s.glbimg.com/en/ho/f/original/2015/12/30/zilu.jpg" alt="Zilu posa de fio dental em fÃ©rias na praia e mostra estar podendo aos 54 (reproduÃ§Ã£o / instagram )" title="Zilu posa de fio dental em fÃ©rias na praia e mostra estar podendo aos 54 (reproduÃ§Ã£o / instagram )"
                data-original-image="s2.glbimg.com/Tj1ulAFN1oW3UwgzSfxIUaNMZ4o=/0x17:448x258/335x180/s.glbimg.com/en/ho/f/original/2015/12/30/zilu.jpg" data-url-smart_horizontal="0eSLRQnc9Xh0skODerKomoBgbOs=/90x0/smart/filters:strip_icc()/" data-url-smart="0eSLRQnc9Xh0skODerKomoBgbOs=/90x0/smart/filters:strip_icc()/" data-url-feature="0eSLRQnc9Xh0skODerKomoBgbOs=/90x0/smart/filters:strip_icc()/" data-url-tablet="zuNQVQR289QJXiyzxbAWmGLnKmY=/220x125/smart/filters:strip_icc()/" data-url-desktop="Iz8j8Fff31_xqy2a9Q7-trpLqiM=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Zilu posa de fio dental em fÃ©rias na praia e mostra estar podendo aos 54</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-30 15:46:22" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/12/famosos-celebram-2016-em-fernando-de-noronha.html" class="foto" title="Paula Fernandes chega a Noronha para festa de Ano Novo com outros famosos (DivulgaÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/HmMg9rLGFb0PGVKOTNq24AAZliI=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/L6rJyE8aIQQ3tMXjiJ0-RQVbAGY=/174x626:1234x1333/120x80/s.glbimg.com/jo/eg/f/original/2015/12/30/paula_fernandes_-_foto_keila_castro_6.jpg" alt="Paula Fernandes chega a Noronha para festa de Ano Novo com outros famosos (DivulgaÃ§Ã£o)" title="Paula Fernandes chega a Noronha para festa de Ano Novo com outros famosos (DivulgaÃ§Ã£o)"
                data-original-image="s2.glbimg.com/L6rJyE8aIQQ3tMXjiJ0-RQVbAGY=/174x626:1234x1333/120x80/s.glbimg.com/jo/eg/f/original/2015/12/30/paula_fernandes_-_foto_keila_castro_6.jpg" data-url-smart_horizontal="fMSBWGikTk5Ibolfyy04DIs5Jck=/90x0/smart/filters:strip_icc()/" data-url-smart="fMSBWGikTk5Ibolfyy04DIs5Jck=/90x0/smart/filters:strip_icc()/" data-url-feature="fMSBWGikTk5Ibolfyy04DIs5Jck=/90x0/smart/filters:strip_icc()/" data-url-tablet="MglxeFrJXMkWQq2d-RDNB_7e6wY=/70x50/smart/filters:strip_icc()/" data-url-desktop="WouzNxqhiecr2XTUsPzacZbxxvo=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Paula Fernandes chega a Noronha para festa de Ano Novo com outros famosos</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-30 12:39:10" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/12/deborah-secco-compartilha-1-foto-de-seu-casamento-com-hugo-moura.html" class="foto" title="Deborah Secco revela que se casou e mostra foto: &#39;Foi um dia muito especial&#39; (ReproduÃ§Ã£o/Instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/vRjlvKI_EOdVHjLYlHTik1JpPLc=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/ensbfUmeDOVCTDan-fLP2iaE3Aw=/495x33:907x308/120x80/s.glbimg.com/jo/eg/f/original/2015/12/30/deborah_secco_e_hugo_moura.jpg" alt="Deborah Secco revela que se casou e mostra foto: &#39;Foi um dia muito especial&#39; (ReproduÃ§Ã£o/Instagram)" title="Deborah Secco revela que se casou e mostra foto: &#39;Foi um dia muito especial&#39; (ReproduÃ§Ã£o/Instagram)"
                data-original-image="s2.glbimg.com/ensbfUmeDOVCTDan-fLP2iaE3Aw=/495x33:907x308/120x80/s.glbimg.com/jo/eg/f/original/2015/12/30/deborah_secco_e_hugo_moura.jpg" data-url-smart_horizontal="1Oob8ECJfQrW2dKrmqDwvhR-hEE=/90x0/smart/filters:strip_icc()/" data-url-smart="1Oob8ECJfQrW2dKrmqDwvhR-hEE=/90x0/smart/filters:strip_icc()/" data-url-feature="1Oob8ECJfQrW2dKrmqDwvhR-hEE=/90x0/smart/filters:strip_icc()/" data-url-tablet="8YAvuveXgkfQry1RysPFb_S84D0=/70x50/smart/filters:strip_icc()/" data-url-desktop="qdr86ycUWaecaccSaGhA-9o-WHM=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Deborah Secco revela que se casou e mostra foto: &#39;Foi um dia muito especial&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-30 15:28:48" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/12/ex-bbb-aline-gotschalg-e-eleita-musa-de-2015-pelos-internautas.html" class="" title="Ex-BBB Aline Ã© eleita musa de 2015 por internautas, superando Marquezine e Grazi (ReproduÃ§Ã£o)" rel="bookmark"><span class="conteudo"><p>Ex-BBB Aline Ã© eleita musa de 2015 por internautas, superando Marquezine e Grazi</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-30 13:51:50" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/verao/noticia/2015/12/ana-paula-evangelista-sobre-calor-no-rio-durmo-vestida-e-acordo-pelada.html" class="foto" title="Musa do carnaval exibe corpaÃ§o e reclama do calor no Rio: &#39;Acordo pelada&#39; (Marcos Serra Lima / EGO)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/DbCwjCSBiIBeMMa7uJfw6ufWktE=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/dnsHB_RT9O7kDNF6s92pRcobljQ=/31x647:1580x1680/120x80/s.glbimg.com/jo/eg/f/original/2015/12/29/6aa.jpg" alt="Musa do carnaval exibe corpaÃ§o e reclama do calor no Rio: &#39;Acordo pelada&#39; (Marcos Serra Lima / EGO)" title="Musa do carnaval exibe corpaÃ§o e reclama do calor no Rio: &#39;Acordo pelada&#39; (Marcos Serra Lima / EGO)"
                data-original-image="s2.glbimg.com/dnsHB_RT9O7kDNF6s92pRcobljQ=/31x647:1580x1680/120x80/s.glbimg.com/jo/eg/f/original/2015/12/29/6aa.jpg" data-url-smart_horizontal="mI7zZ4kFKK17xHf98ceUIlWuOms=/90x0/smart/filters:strip_icc()/" data-url-smart="mI7zZ4kFKK17xHf98ceUIlWuOms=/90x0/smart/filters:strip_icc()/" data-url-feature="mI7zZ4kFKK17xHf98ceUIlWuOms=/90x0/smart/filters:strip_icc()/" data-url-tablet="othC0Ge3WsV9Kk-bWRsjRBqjY4w=/70x50/smart/filters:strip_icc()/" data-url-desktop="BeMO365evqrW-iThY9rf9hGuUQo=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Musa do carnaval exibe corpaÃ§o e reclama do calor no Rio: &#39;Acordo pelada&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-30 11:04:42" class="chamada chamada-principal mobile-grid-full"><a href="http://revistaglamour.globo.com/Celebridades/noticia/2015/12/flavia-alessandra-posa-de-biquini-e-fala-sobre-o-corpo-e-truques-de-beleza.html" class="foto" title="Aos 41, FlÃ¡via Alessandra faz bonito ao posar de biquÃ­ni e entrega &#39;segredo&#39; (Fabio Bartelt)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/5Q6xsrbriy5fAzMki7UPrfyQQ5o=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/zTROmJ8QiEMfW7NpiU7zduxNGTQ=/176x254:811x677/120x80/e.glbimg.com/og/ed/f/original/2015/12/26/151206_glamour-0241.png" alt="Aos 41, FlÃ¡via Alessandra faz bonito ao posar de biquÃ­ni e entrega &#39;segredo&#39; (Fabio Bartelt)" title="Aos 41, FlÃ¡via Alessandra faz bonito ao posar de biquÃ­ni e entrega &#39;segredo&#39; (Fabio Bartelt)"
                data-original-image="s2.glbimg.com/zTROmJ8QiEMfW7NpiU7zduxNGTQ=/176x254:811x677/120x80/e.glbimg.com/og/ed/f/original/2015/12/26/151206_glamour-0241.png" data-url-smart_horizontal="kf6xVl5kY6zUc1JX4xgTiIFY1I0=/90x0/smart/filters:strip_icc()/" data-url-smart="kf6xVl5kY6zUc1JX4xgTiIFY1I0=/90x0/smart/filters:strip_icc()/" data-url-feature="kf6xVl5kY6zUc1JX4xgTiIFY1I0=/90x0/smart/filters:strip_icc()/" data-url-tablet="hd8ioy8pw2-28e2HSMzVw_Jl32c=/70x50/smart/filters:strip_icc()/" data-url-desktop="AIecsP9VZLAManmhpTFrbAQtf8c=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Aos 41, FlÃ¡via Alessandra faz bonito ao posar de biquÃ­ni e entrega &#39;segredo&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-30 12:39:10" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/famosos/juliana-paes-compra-quatro-casas-de-veraneio-para-familia-na-regiao-dos-lagos-por-cerca-de-800-mil-18383044.html" class="" title="Juliana Paes compra quatro casinhas em Ã¡rea discreta do RJ no total de R$ 800 mil (reproduÃ§Ã£o)" rel="bookmark"><span class="conteudo"><p>Juliana Paes compra quatro casinhas em Ã¡rea discreta do RJ no total de R$ 800 mil</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-30 14:46:06" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/famosos/thammy-miranda-se-aborrece-com-criticas-faz-desabafo-acha-que-deus-entao-nao-enche-saco-que-eu-nao-tenho-18384934.html" class="foto" title="Thammy se irrita com as crÃ­ticas na web: &#39;NÃ£o enche o saco que eu nÃ£o tenho&#39; (Instagram / ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/zIgxLvcOGryepm8VFeQjjt5OmPM=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/qQNCAkrnv-DTZSut2lAXCsY9XwA=/0x113:640x540/120x80/s.glbimg.com/jo/eg/f/original/2015/12/29/thammy_miranda_e_andressa_ferreira.jpg" alt="Thammy se irrita com as crÃ­ticas na web: &#39;NÃ£o enche o saco que eu nÃ£o tenho&#39; (Instagram / ReproduÃ§Ã£o)" title="Thammy se irrita com as crÃ­ticas na web: &#39;NÃ£o enche o saco que eu nÃ£o tenho&#39; (Instagram / ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/qQNCAkrnv-DTZSut2lAXCsY9XwA=/0x113:640x540/120x80/s.glbimg.com/jo/eg/f/original/2015/12/29/thammy_miranda_e_andressa_ferreira.jpg" data-url-smart_horizontal="-x1A6dTh2dRby3NKcmnSiyU5IF0=/90x0/smart/filters:strip_icc()/" data-url-smart="-x1A6dTh2dRby3NKcmnSiyU5IF0=/90x0/smart/filters:strip_icc()/" data-url-feature="-x1A6dTh2dRby3NKcmnSiyU5IF0=/90x0/smart/filters:strip_icc()/" data-url-tablet="nC7NOQC-11kFRZ2oAlVyHZoW3hk=/70x50/smart/filters:strip_icc()/" data-url-desktop="MvkpkUcNMkSJxE2Tc9gH1eyhCO4=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Thammy se irrita com as crÃ­ticas na web: &#39;NÃ£o enche o saco que eu nÃ£o tenho&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-30 13:55:34" class="chamada chamada-principal mobile-grid-full"><a href="http://gnt.globo.com/receitas/receitas/file-wellington-com-shitake-e-presunto-de-parma.htm" class="foto" title="Carolina Ferraz e a filha preparam filÃ© especial para o Ano Novo; receita (reproduÃ§Ã£o GNT)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/hh7Pma1GibaLMHK5Gdf9mjnzah0=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/37XVfej5IrWWTWWeSsdUMGa3FBE=/510x49:1296x572/120x80/s.glbimg.com/en/ho/f/original/2015/12/30/carolina_ferraz_cozinha_com_a_filha.jpg" alt="Carolina Ferraz e a filha preparam filÃ© especial para o Ano Novo; receita (reproduÃ§Ã£o GNT)" title="Carolina Ferraz e a filha preparam filÃ© especial para o Ano Novo; receita (reproduÃ§Ã£o GNT)"
                data-original-image="s2.glbimg.com/37XVfej5IrWWTWWeSsdUMGa3FBE=/510x49:1296x572/120x80/s.glbimg.com/en/ho/f/original/2015/12/30/carolina_ferraz_cozinha_com_a_filha.jpg" data-url-smart_horizontal="xZfxM_ZU59JM9UBBOMgQQjn5gj8=/90x0/smart/filters:strip_icc()/" data-url-smart="xZfxM_ZU59JM9UBBOMgQQjn5gj8=/90x0/smart/filters:strip_icc()/" data-url-feature="xZfxM_ZU59JM9UBBOMgQQjn5gj8=/90x0/smart/filters:strip_icc()/" data-url-tablet="6Mq3XTA5e1zfeK5x8i3P0y52UoA=/70x50/smart/filters:strip_icc()/" data-url-desktop="wYEMLZMTMP80wUWAq6dnpe8Kyrs=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Carolina Ferraz e a filha preparam filÃ© especial para o Ano Novo; receita</p></span></a></div></div><div class="show-more"><span class="setinha-show-more"></span></div></div><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-novelas
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2015/", "logo": "http://s2.glbimg.com/tjsQl1SMvddbD-BGiSoC2Nb6kqg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-36x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2015/rss/", "logo_tv": "http://s2.glbimg.com/y8thjGY4IQuSvc4VfunyXi0d8C4=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-45x30.jpg"}, {"ordering": 34, "default": false, "cor": "#FF7F00", "name": "\u00cata Mundo Bom! ", "url": "http://gshow.globo.com/novelas/eta-mundo-bom/", "logo": "http://s2.glbimg.com/03_kBb44AvSMInRXaxaDxbqybTY=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/12/11/eta_36x20.jpg", "slug": "eta-mundo-bom", "url_feed": "http://gshow.globo.com/novelas/eta-mundo-bom/rss/", "logo_tv": "http://s2.glbimg.com/4yiquDhiaRXleK9H_vmDHGDDiLM=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/12/11/eta_45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/FRxoFZaNszGUmyoDZALOiIC_SOY=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/10/22/alem_do_tempo_segunda_fase-36x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/3Z-Sw1ITHzu4-EEfXG-zRbCs_dY=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/10/22/alem_g_45x30_1.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "Totalmente Demais", "url": "http://gshow.globo.com/novelas/totalmente-demais/", "logo": "http://s2.glbimg.com/R8T_N-bSmq23FCuZwf27cTTmjzg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/11/06/logo_36x20.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/totalmente-demais/rss/", "logo_tv": "http://s2.glbimg.com/DOKGjWF1k80-ujyw_Q-OT1IX65M=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/11/06/logo_45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "A Regra do Jogo", "url": "http://gshow.globo.com/novelas/a-regra-do-jogo/", "logo": "http://s2.glbimg.com/K-Nvvvm7zp2F7zS8TcY6TkIcxoE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-36x20.jpg", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/a-regra-do-jogo/rss/", "logo_tv": "http://s2.glbimg.com/4B7Jk0R6K1HlAAhuZIrDOJp8hbs=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-45x30.jpg"}]);}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2015/", "logo": "http://s2.glbimg.com/tjsQl1SMvddbD-BGiSoC2Nb6kqg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-36x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2015/rss/", "logo_tv": "http://s2.glbimg.com/y8thjGY4IQuSvc4VfunyXi0d8C4=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-45x30.jpg"}, {"ordering": 34, "default": false, "cor": "#FF7F00", "name": "\u00cata Mundo Bom! ", "url": "http://gshow.globo.com/novelas/eta-mundo-bom/", "logo": "http://s2.glbimg.com/03_kBb44AvSMInRXaxaDxbqybTY=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/12/11/eta_36x20.jpg", "slug": "eta-mundo-bom", "url_feed": "http://gshow.globo.com/novelas/eta-mundo-bom/rss/", "logo_tv": "http://s2.glbimg.com/4yiquDhiaRXleK9H_vmDHGDDiLM=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/12/11/eta_45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/FRxoFZaNszGUmyoDZALOiIC_SOY=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/10/22/alem_do_tempo_segunda_fase-36x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/3Z-Sw1ITHzu4-EEfXG-zRbCs_dY=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/10/22/alem_g_45x30_1.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "Totalmente Demais", "url": "http://gshow.globo.com/novelas/totalmente-demais/", "logo": "http://s2.glbimg.com/R8T_N-bSmq23FCuZwf27cTTmjzg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/11/06/logo_36x20.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/totalmente-demais/rss/", "logo_tv": "http://s2.glbimg.com/DOKGjWF1k80-ujyw_Q-OT1IX65M=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/11/06/logo_45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "A Regra do Jogo", "url": "http://gshow.globo.com/novelas/a-regra-do-jogo/", "logo": "http://s2.glbimg.com/K-Nvvvm7zp2F7zS8TcY6TkIcxoE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-36x20.jpg", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/a-regra-do-jogo/rss/", "logo_tv": "http://s2.glbimg.com/4B7Jk0R6K1HlAAhuZIrDOJp8hbs=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-45x30.jpg"}]);}});}
</script><div id="libby-home-widget-horoscopo" class="libby-home-widgetBase libby-home-widget-horoscopo analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais entretenimento" href="http://variedades.globo.com/"><span class="text">mais variedades</span><span class="arrow">&rsaquo;</span></a></div></section></div><div id="ad-position-middle3" class="opec"><div id="banner_slb_fim" class="tag-manager-publicidade-container"></div></div><div class="container columns clearfix glb-area-colunas second-area"><section class="area news-column"></section><section class="area sports-column"></section><section class="area etc-column analytics-area"></section></div><section class="area central container analytics-area analytics-id-O"><div class="separator"></div><section class="agrupador-quadruplo-tecnologia analytics-area analytics-id-T clearfix franja-inferior tecnologia"><div class="cabecalho"><h2><a href="http://www.techtudo.com.br/" title="TECNOLOGIA &amp; GAMES"><span class="word word-0">TECNOLOGIA</span><span class="word word-1">&</span><span class="word word-2">GAMES</span></a></h2><div id="ad-position-x34" class="opec"><div id="banner_selo4" class="tag-manager-publicidade-container"></div></div><aside class="links analytics-area analytics-id-T"><ul><li><a href="http://forum.techtudo.com.br/" title="fÃ³rum">fÃ³rum</a></li><li><a href="http://www.techtudo.com.br/velocimetro.html" title="velocÃ­metro">velocÃ­metro</a></li><li><a href="http://www.techtudo.com.br/videos/" title="vÃ­deos">vÃ­deos</a></li><li><a href="http://www.techtudo.com.br/jogos/" title="jogos">jogos</a></li><li><a href="http://www.techtudo.com.br/mobile/" title="celulares">celulares</a></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaque"><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/listas/noticia/2015/12/capa-para-iphone-6-dez-opcoes-mais-baratas-para-enfeitar-o-celular-da-apple.html" title="ConheÃ§a capinhas mais baratas e interessantes para seu iPhone 6"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/z70DU-DfBhLvtR0z8fJG0aupAnE=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/Kw5uAw0teT-HTMPwj93bwcvZ15U=/0x0:444x236/245x130/s.glbimg.com/po/tt2/f/original/2015/12/30/case-a-prova-dagua-permite-ate-usar-o-sensor-de-digitais.jpg" alt="ConheÃ§a capinhas mais baratas e interessantes para seu iPhone 6 (DivulgaÃ§Ã£o)" title="ConheÃ§a capinhas mais baratas e interessantes para seu iPhone 6 (DivulgaÃ§Ã£o)"
             data-original-image="s2.glbimg.com/Kw5uAw0teT-HTMPwj93bwcvZ15U=/0x0:444x236/245x130/s.glbimg.com/po/tt2/f/original/2015/12/30/case-a-prova-dagua-permite-ate-usar-o-sensor-de-digitais.jpg" data-url-smart_horizontal="ZrYUy9PFitnxitJMkVxiab_EBPw=/90x56/smart/filters:strip_icc()/" data-url-smart="ZrYUy9PFitnxitJMkVxiab_EBPw=/90x56/smart/filters:strip_icc()/" data-url-feature="ZrYUy9PFitnxitJMkVxiab_EBPw=/90x56/smart/filters:strip_icc()/" data-url-tablet="2s2jyq0CnLxa5yr5qz2ynUWNbfc=/160x96/smart/filters:strip_icc()/" data-url-desktop="ozA2q385v5afe9LEIhaFW56VSas=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>ConheÃ§a capinhas mais baratas e interessantes para seu iPhone 6</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/listas/noticia/2015/12/dubsmash-uber-e-mais-lista-reune-dez-apps-da-modinha-em-2015.html" title="Relembre as modas de 2015 que bombaram nos smarts e Internet"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/tGtkBQeHthWCyEsHf086Y_9QC2c=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/jcVNX0QFaoTv27Sy5Bk-MWHOVg4=/3x13:431x240/245x130/s.glbimg.com/po/tt2/f/original/2015/04/09/captura_de_tela_2015-04-09_as_16.10.32.png" alt="Relembre as modas de 2015 que bombaram nos smarts e Internet (ReproduÃ§Ã£o / YouTube)" title="Relembre as modas de 2015 que bombaram nos smarts e Internet (ReproduÃ§Ã£o / YouTube)"
             data-original-image="s2.glbimg.com/jcVNX0QFaoTv27Sy5Bk-MWHOVg4=/3x13:431x240/245x130/s.glbimg.com/po/tt2/f/original/2015/04/09/captura_de_tela_2015-04-09_as_16.10.32.png" data-url-smart_horizontal="oL9L3NI4XtOdOpsXew1-8_CAEPE=/90x56/smart/filters:strip_icc()/" data-url-smart="oL9L3NI4XtOdOpsXew1-8_CAEPE=/90x56/smart/filters:strip_icc()/" data-url-feature="oL9L3NI4XtOdOpsXew1-8_CAEPE=/90x56/smart/filters:strip_icc()/" data-url-tablet="oJuqcVolUqVXR1fybrjd0-32Zss=/160x96/smart/filters:strip_icc()/" data-url-desktop="qTnnm8_Sr-e_g-RbDn99BhWe66I=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Relembre as modas de 2015 que bombaram nos smarts e Internet</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/listas/noticia/2015/12/descubra-quais-sao-os-jogos-de-xbox-one-mais-esperados-para-2016.html" title="Xbox One: veja os games mais esperados para o prÃ³ximo ano"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/g6swj_t0l-8o44Pk2bkisfJcn7w=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/ZvebpoS3xQFQBJTt1-7ti51oTzU=/0x89:1528x900/245x130/s.glbimg.com/po/tt2/f/original/2015/08/05/2916109-quantum-break-time-stop.jpg" alt="Xbox One: veja os games mais esperados para o prÃ³ximo ano (DivulgaÃ§Ã£o)" title="Xbox One: veja os games mais esperados para o prÃ³ximo ano (DivulgaÃ§Ã£o)"
             data-original-image="s2.glbimg.com/ZvebpoS3xQFQBJTt1-7ti51oTzU=/0x89:1528x900/245x130/s.glbimg.com/po/tt2/f/original/2015/08/05/2916109-quantum-break-time-stop.jpg" data-url-smart_horizontal="-_M6F4geFZagBau5rz_GRvdOoJk=/90x56/smart/filters:strip_icc()/" data-url-smart="-_M6F4geFZagBau5rz_GRvdOoJk=/90x56/smart/filters:strip_icc()/" data-url-feature="-_M6F4geFZagBau5rz_GRvdOoJk=/90x56/smart/filters:strip_icc()/" data-url-tablet="wxrKK1IezKQcqKL6aF5Y4dvBKZ0=/160x96/smart/filters:strip_icc()/" data-url-desktop="yqtMWnIOj4x05IruFdnrMiFagjs=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Xbox One: veja os games mais esperados para o prÃ³ximo ano</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/dicas-e-tutoriais/noticia/2015/12/windows-10-usa-seus-dados-pessoais-em-anuncios-saiba-como-evitar.html" title="Windows 10 pode &#39;roubar&#39; seus dados sem vocÃª saber; entenda"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/d5XQTEZWJST2c59xiHvOeMjg4F8=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/yUQOnBWQMSWvQUIhIVAkXrQZTGE=/14x48:612x365/245x130/s.glbimg.com/po/tt2/f/original/2015/12/16/6-windows-10-papel-de-parede.jpg" alt="Windows 10 pode &#39;roubar&#39; seus dados sem vocÃª saber; entenda (Windows 10 teve novo papel de parede apresentado pela Microsoft em junho (Foto: ReproduÃ§Ã£o/Microsoft))" title="Windows 10 pode &#39;roubar&#39; seus dados sem vocÃª saber; entenda (Windows 10 teve novo papel de parede apresentado pela Microsoft em junho (Foto: ReproduÃ§Ã£o/Microsoft))"
             data-original-image="s2.glbimg.com/yUQOnBWQMSWvQUIhIVAkXrQZTGE=/14x48:612x365/245x130/s.glbimg.com/po/tt2/f/original/2015/12/16/6-windows-10-papel-de-parede.jpg" data-url-smart_horizontal="VgBobt6NFfhcrb0pxffaYfjgh0E=/90x56/smart/filters:strip_icc()/" data-url-smart="VgBobt6NFfhcrb0pxffaYfjgh0E=/90x56/smart/filters:strip_icc()/" data-url-feature="VgBobt6NFfhcrb0pxffaYfjgh0E=/90x56/smart/filters:strip_icc()/" data-url-tablet="BmN07ayKdb_x-gIrsV6GgEgrHyg=/160x96/smart/filters:strip_icc()/" data-url-desktop="yW4DoFj3Zw0RrcXxTZc13c_mCCI=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Windows 10 pode &#39;roubar&#39; seus <br />dados sem vocÃª saber; entenda</p></span></a></li></ul></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-tecnologia']);</script><div class="separator"></div><section class="agrupador-sextuplo analytics-area analytics-id-S"><section class="analytics-area analytics-id-E esquerda tvg"><div class="cabecalho"><h2><span class="word word-0">moda</span><span class="word word-1">&amp;</span><span class="word word-2">beleza</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://vogue.globo.com/moda/guia-de-estilo/noticia/2015/12/prata-ou-ouro-saiba-como-combinar-os-tons-metalicos-para-o-reveillon.html" alt="Prata ou ouro? Stylist da Vogue ensina a combinar tons metÃ¡licos"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/bnOce9WWrYslGLERi3lqJIGjzJA=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/yIheq0r5jNgKj_5zqfzlG01Rjqg=/0x31:1500x999/155x100/e.glbimg.com/og/ed/f/original/2015/12/17/look-2.jpg" alt="Prata ou ouro? Stylist da Vogue ensina a combinar tons metÃ¡licos (ReproduÃ§Ã£o)" title="Prata ou ouro? Stylist da Vogue ensina a combinar tons metÃ¡licos (ReproduÃ§Ã£o)"
            data-original-image="s2.glbimg.com/yIheq0r5jNgKj_5zqfzlG01Rjqg=/0x31:1500x999/155x100/e.glbimg.com/og/ed/f/original/2015/12/17/look-2.jpg" data-url-smart_horizontal="MRAT7r7splHk_WDb6cj-n21NxxE=/90x56/smart/filters:strip_icc()/" data-url-smart="MRAT7r7splHk_WDb6cj-n21NxxE=/90x56/smart/filters:strip_icc()/" data-url-feature="MRAT7r7splHk_WDb6cj-n21NxxE=/90x56/smart/filters:strip_icc()/" data-url-tablet="vqztuD-Gk720OuWpGLJZimnHJ5c=/122x75/smart/filters:strip_icc()/" data-url-desktop="j5-veVKiad0s3YTBu2nehOw6St4=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>roupa de festa</h3><p>Prata ou ouro? Stylist da Vogue ensina a combinar tons metÃ¡licos</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revistamarieclaire.globo.com/Beleza/fotos/2015/12/labios-quentes-10-sugestoes-de-batons-que-prometem-fazer-sucesso-no-verao.html" alt="Veja 10 sugestÃµes de batons que prometem fazer sucesso no verÃ£o"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/nCasRpaQU_C_uz8AOdCeTFecEvs=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/hXYpYvDbxRQB-lUcKnlAzmsyCHY=/0x48:800x565/155x100/e.glbimg.com/og/ed/f/original/2015/12/30/bocao.jpg" alt="Veja 10 sugestÃµes de batons que prometem fazer sucesso no verÃ£o (DivulgaÃ§Ã£o)" title="Veja 10 sugestÃµes de batons que prometem fazer sucesso no verÃ£o (DivulgaÃ§Ã£o)"
            data-original-image="s2.glbimg.com/hXYpYvDbxRQB-lUcKnlAzmsyCHY=/0x48:800x565/155x100/e.glbimg.com/og/ed/f/original/2015/12/30/bocao.jpg" data-url-smart_horizontal="2yOfxlJmZuQ5FINqLLM9-YJ6_Lo=/90x56/smart/filters:strip_icc()/" data-url-smart="2yOfxlJmZuQ5FINqLLM9-YJ6_Lo=/90x56/smart/filters:strip_icc()/" data-url-feature="2yOfxlJmZuQ5FINqLLM9-YJ6_Lo=/90x56/smart/filters:strip_icc()/" data-url-tablet="RrAfdmLatOkYY1bCq6G7bI8ylDo=/122x75/smart/filters:strip_icc()/" data-url-desktop="QfIfpv2MS5KjCAJTr46CJVkis0g=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>LÃ¡bios quentes</h3><p>Veja 10 sugestÃµes de batons que prometem fazer sucesso no verÃ£o</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://gshow.globo.com/como-fazer/noticia/2015/12/maquiagem-de-ano-novo-confira-dicas-com-dany-bananinha.html" alt="Bananinha mostra passo a passo de make especial para o RÃ©veillon"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/7wOoWqQWm6BGNjAEn0R7aAwwHsA=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/DDENB7GeKnKFeubbEDyHbSsm2yw=/0x14:720x479/155x100/s3.glbimg.com/v1/AUTH_e84042ef78cb4708aeebdf1c68c6cbd6/photos/apis/905cbae24fee4dadbe7abed111142616/maquiagem-dany-sombra-clara.jpg" alt="Bananinha mostra passo a passo de make especial para o RÃ©veillon (FÃ¡bio Rocha / Gshow)" title="Bananinha mostra passo a passo de make especial para o RÃ©veillon (FÃ¡bio Rocha / Gshow)"
            data-original-image="s2.glbimg.com/DDENB7GeKnKFeubbEDyHbSsm2yw=/0x14:720x479/155x100/s3.glbimg.com/v1/AUTH_e84042ef78cb4708aeebdf1c68c6cbd6/photos/apis/905cbae24fee4dadbe7abed111142616/maquiagem-dany-sombra-clara.jpg" data-url-smart_horizontal="Fpoib3FGFVPxUfXZjeUezlCTQ2g=/90x56/smart/filters:strip_icc()/" data-url-smart="Fpoib3FGFVPxUfXZjeUezlCTQ2g=/90x56/smart/filters:strip_icc()/" data-url-feature="Fpoib3FGFVPxUfXZjeUezlCTQ2g=/90x56/smart/filters:strip_icc()/" data-url-tablet="8hsWPERE80oTSXBzHhbo8Rp0Rrk=/122x75/smart/filters:strip_icc()/" data-url-desktop="WIRc2s6Hmp2sOWvXj22JTAYFYx8=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>prepare-se para o ano novo</h3><p>Bananinha mostra passo a passo de make especial para o RÃ©veillon</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://gnt.globo.com/" title="GNT">GNT</a></h3></li><li><h3><a href="http://vogue.globo.com/" title="vogue">vogue</a></h3></li><li></li></ul></aside></section><div class="separator"></div><section class="analytics-area analytics-id-D direita tvg"><div class="cabecalho"><h2><span class="word word-0">CASA</span><span class="word word-1">&amp;</span><span class="word word-2">DECORAÃÃO</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://casavogue.globo.com/Interiores/casas/noticia/2015/12/cores-claras-e-madeira-pontuam-o-decor.html" alt="Cores claras e madeira pontuam casa com ambientes integrados"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/btbez8iu5eEYfO-NyjpAmJPuDvM=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/uYDbWVeo58mP1DKWpkQDC75lnrY=/0x319:620x719/155x100/e.glbimg.com/og/ed/f/original/2015/12/15/apartamento-nvmd-int2architecture03.jpg" alt="Cores claras e madeira pontuam casa com ambientes integrados (DivulgaÃ§Ã£o)" title="Cores claras e madeira pontuam casa com ambientes integrados (DivulgaÃ§Ã£o)"
            data-original-image="s2.glbimg.com/uYDbWVeo58mP1DKWpkQDC75lnrY=/0x319:620x719/155x100/e.glbimg.com/og/ed/f/original/2015/12/15/apartamento-nvmd-int2architecture03.jpg" data-url-smart_horizontal="oMStd8f1D-YYTH7GKEvpPywVpT0=/90x56/smart/filters:strip_icc()/" data-url-smart="oMStd8f1D-YYTH7GKEvpPywVpT0=/90x56/smart/filters:strip_icc()/" data-url-feature="oMStd8f1D-YYTH7GKEvpPywVpT0=/90x56/smart/filters:strip_icc()/" data-url-tablet="d2ZR4WsLiIb8Oup3Lckmt9ZerYo=/122x75/smart/filters:strip_icc()/" data-url-desktop="fHRTYvgaU_pQQTWrmT8ihg7g_J0=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>cozinha gourmet</h3><p>Cores claras e madeira pontuam casa com ambientes integrados</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revista.zapimoveis.com.br/ambiente-fresco-durante-o-verao/" alt="Saiba quais caracterÃ­sticas do imÃ³vel ajudam a aliviar o calor"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/vcCwDJxs59yCvB1blKNIOiUgCPU=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/BqKxzASXrBM7RGy_6EnObHjRw0I=/0x13:599x400/155x100/s.glbimg.com/en/ho/f/original/2015/12/30/casa-verao.jpg" alt="Saiba quais caracterÃ­sticas do imÃ³vel ajudam a aliviar o calor (Shutterstock)" title="Saiba quais caracterÃ­sticas do imÃ³vel ajudam a aliviar o calor (Shutterstock)"
            data-original-image="s2.glbimg.com/BqKxzASXrBM7RGy_6EnObHjRw0I=/0x13:599x400/155x100/s.glbimg.com/en/ho/f/original/2015/12/30/casa-verao.jpg" data-url-smart_horizontal="EBZIDMK0SJUeHxYMXhC2RsAdgwc=/90x56/smart/filters:strip_icc()/" data-url-smart="EBZIDMK0SJUeHxYMXhC2RsAdgwc=/90x56/smart/filters:strip_icc()/" data-url-feature="EBZIDMK0SJUeHxYMXhC2RsAdgwc=/90x56/smart/filters:strip_icc()/" data-url-tablet="wuabRH7wlAtyYhEFBv23dEhUCXU=/122x75/smart/filters:strip_icc()/" data-url-desktop="gi_F3FuhQSlIVvZzSld-PMQtUR0=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>escolhas importantes</h3><p>Saiba quais caracterÃ­sticas do imÃ³vel ajudam a aliviar o calor</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revistacrescer.globo.com/Festa-de-aniversario/Temas/fotos/2015/12/tema-de-festa-festa-no-ceu.html" alt="AnimaÃ§Ã£o vira tema de festa bem colorida; inspire-se nos detalhes"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/VR9NNKIUxutFbclvqcTNgmnJ6NQ=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/2Iqs6O2pnsXvTed5-zJXUjUZTjc=/104x0:1871x1140/155x100/s.glbimg.com/en/ho/f/original/2015/12/30/decoracao_festa_dKXZzcP.jpg" alt="AnimaÃ§Ã£o vira tema de festa bem colorida; inspire-se nos detalhes (DIVULGAÃÃO/ FRAN MATOS)" title="AnimaÃ§Ã£o vira tema de festa bem colorida; inspire-se nos detalhes (DIVULGAÃÃO/ FRAN MATOS)"
            data-original-image="s2.glbimg.com/2Iqs6O2pnsXvTed5-zJXUjUZTjc=/104x0:1871x1140/155x100/s.glbimg.com/en/ho/f/original/2015/12/30/decoracao_festa_dKXZzcP.jpg" data-url-smart_horizontal="mG0ZsIZdabmkq5Vs6wHHmg2U6lU=/90x56/smart/filters:strip_icc()/" data-url-smart="mG0ZsIZdabmkq5Vs6wHHmg2U6lU=/90x56/smart/filters:strip_icc()/" data-url-feature="mG0ZsIZdabmkq5Vs6wHHmg2U6lU=/90x56/smart/filters:strip_icc()/" data-url-tablet="3Pv4UZ53hI9HqQNA6iiSfbsZlTY=/122x75/smart/filters:strip_icc()/" data-url-desktop="ktUzkRdNeDQ6A5BqXEbRGsnZYzM=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>que capricho!</h3><p>AnimaÃ§Ã£o vira tema de festa bem colorida; inspire-se nos detalhes</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://revistacasaejardim.globo.com/" title="CASA E JARDIM">CASA E JARDIM</a></h3></li><li><h3><a href="http://casavogue.globo.com/" title="CASA VOGUE">CASA VOGUE</a></h3></li><li></li></ul></aside></section></section><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior famosos-style"><div class="cabecalho"><h2><a title="famosos"><span class='logo'>famosos</span><span class='texto'>famosos</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://ego.globo.com/">ego</a></h3></li><li class=""><h3><a href="http://revistaquem.globo.com/">quem</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://ego.globo.com/famosos/noticia/2015/12/preta-gil-aparece-sem-maquiagem-e-em-momento-vo-babona-em-foto.html" title="Madrasta posta foto e mostra Preta em momento &#39;vÃ³ babona&#39;"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/h3yu9lQg9wERN9cZxHFSVlx8myQ=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/HRHkeDqkDMWtORMK4tpzWXUGWsQ=/0x137:640x476/245x130/s.glbimg.com/jo/eg/f/original/2015/12/30/flora_gil_sandra_gadelha_e_preta_gil.jpg" alt="Madrasta posta foto e mostra Preta em momento &#39;vÃ³ babona&#39; (Instagram / ReproduÃ§Ã£o)" title="Madrasta posta foto e mostra Preta em momento &#39;vÃ³ babona&#39; (Instagram / ReproduÃ§Ã£o)"
                    data-original-image="s2.glbimg.com/HRHkeDqkDMWtORMK4tpzWXUGWsQ=/0x137:640x476/245x130/s.glbimg.com/jo/eg/f/original/2015/12/30/flora_gil_sandra_gadelha_e_preta_gil.jpg" data-url-smart_horizontal="FbbaKy9ycjcR4jQAYEzZ29RT430=/90x56/smart/filters:strip_icc()/" data-url-smart="FbbaKy9ycjcR4jQAYEzZ29RT430=/90x56/smart/filters:strip_icc()/" data-url-feature="FbbaKy9ycjcR4jQAYEzZ29RT430=/90x56/smart/filters:strip_icc()/" data-url-tablet="LuiLz8NWtNzbqmgYzoL9JhE_qG4=/160x95/smart/filters:strip_icc()/" data-url-desktop="8qpr68aUbiq0QnjsW5aSHHyXTDc=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Madrasta posta foto e mostra Preta em momento &#39;vÃ³ babona&#39;</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://epoca.globo.com/colunas-e-blogs/bruno-astuto/noticia/2015/12/22-personalidades-falam-de-seus-desejos-para-2016.html" title="22 personalidades revelam o que desejam para 2016"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/fgoaPtYGhjEwf0XxCSEFWmDvsZQ=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/a9oSlukmuV7P-NZ_5w9nLphTjpc=/303x288:604x448/245x130/s.glbimg.com/en/ho/f/original/2015/12/30/claudia_raia.jpg" alt="22 personalidades revelam o que desejam para 2016 (DIVULGAÃÃO)" title="22 personalidades revelam o que desejam para 2016 (DIVULGAÃÃO)"
                    data-original-image="s2.glbimg.com/a9oSlukmuV7P-NZ_5w9nLphTjpc=/303x288:604x448/245x130/s.glbimg.com/en/ho/f/original/2015/12/30/claudia_raia.jpg" data-url-smart_horizontal="9lg-OiqHmdY4qrATebEKbGbOePo=/90x56/smart/filters:strip_icc()/" data-url-smart="9lg-OiqHmdY4qrATebEKbGbOePo=/90x56/smart/filters:strip_icc()/" data-url-feature="9lg-OiqHmdY4qrATebEKbGbOePo=/90x56/smart/filters:strip_icc()/" data-url-tablet="tmoTDgDBpAKe94FJyoJMemAqojI=/160x95/smart/filters:strip_icc()/" data-url-desktop="hMyzw5loSa7AKdcrUI67xGd9Q0U=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>22 personalidades revelam <br />o que desejam para 2016</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://ego.globo.com/famosos/noticia/2015/12/renata-davilla-exibe-silhueta-enxuta-durante-tarde-em-miami.html" title="Ex-BBB Renatinha volta a exibir corpo escultural em Miami"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/iHU4nO45ZRrEhXWekaheBcD4HlI=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/pE6pFU_5X3yeaisxfDdWsJZ5gJs=/289x172:608x341/245x130/s.glbimg.com/jo/eg/f/original/2015/12/30/916447_425544804308879_1713078597_n.jpg" alt="Ex-BBB Renatinha volta a exibir corpo escultural em Miami (ReproduÃ§Ã£o/Instagram)" title="Ex-BBB Renatinha volta a exibir corpo escultural em Miami (ReproduÃ§Ã£o/Instagram)"
                    data-original-image="s2.glbimg.com/pE6pFU_5X3yeaisxfDdWsJZ5gJs=/289x172:608x341/245x130/s.glbimg.com/jo/eg/f/original/2015/12/30/916447_425544804308879_1713078597_n.jpg" data-url-smart_horizontal="0Jc5bsc_eB3miRb9VBDK8o9FbIM=/90x56/smart/filters:strip_icc()/" data-url-smart="0Jc5bsc_eB3miRb9VBDK8o9FbIM=/90x56/smart/filters:strip_icc()/" data-url-feature="0Jc5bsc_eB3miRb9VBDK8o9FbIM=/90x56/smart/filters:strip_icc()/" data-url-tablet="1qQnJgHr47OvHi-qk-07gpHLsxc=/160x95/smart/filters:strip_icc()/" data-url-desktop="UWCJpbPshvK7PXpnp21Vvg7fm7Q=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Ex-BBB Renatinha volta a exibir corpo escultural em Miami</p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://ego.globo.com/biquini/noticia/2015/12/sabrina-sato-curte-sol-em-iate-de-biquini-e-oculos-estilosos.html" title="Sato exibe corpaÃ§o e faz &#39;carÃ£o&#39; em passeio no mar da TailÃ¢ndia"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/BW_gEQUwb90qG0siqO04ZJoZWiw=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/G670HIL1v90JUpBF5f7QaHlg3Zw=/0x17:605x338/245x130/s.glbimg.com/jo/eg/f/original/2015/12/30/sabrina_sato.jpg" alt="Sato exibe corpaÃ§o e faz &#39;carÃ£o&#39; em passeio no mar da TailÃ¢ndia (Instagram / ReproduÃ§Ã£o)" title="Sato exibe corpaÃ§o e faz &#39;carÃ£o&#39; em passeio no mar da TailÃ¢ndia (Instagram / ReproduÃ§Ã£o)"
                    data-original-image="s2.glbimg.com/G670HIL1v90JUpBF5f7QaHlg3Zw=/0x17:605x338/245x130/s.glbimg.com/jo/eg/f/original/2015/12/30/sabrina_sato.jpg" data-url-smart_horizontal="lc8E3PaTtuO8CfTy86UCN6Na7u4=/90x56/smart/filters:strip_icc()/" data-url-smart="lc8E3PaTtuO8CfTy86UCN6Na7u4=/90x56/smart/filters:strip_icc()/" data-url-feature="lc8E3PaTtuO8CfTy86UCN6Na7u4=/90x56/smart/filters:strip_icc()/" data-url-tablet="2ibHbG6-thOezGRHeCgegxovPIk=/160x95/smart/filters:strip_icc()/" data-url-desktop="fJ87F5bGj4BxsQWaKKjZQ_qkh1I=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Sato exibe corpaÃ§o e faz &#39;carÃ£o&#39; em passeio no mar da TailÃ¢ndia</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_famosos-style']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior gshow"><div class="cabecalho"><h2><a href="http://gshow.globo.com/" title="novelas, sÃ©ries, programas e muito mais"><span class='logo'>novelas, sÃ©ries, programas e muito mais</span><span class='texto'>novelas, sÃ©ries, programas e muito mais</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://gshow.globo.com/estilo-tv/">ESTILO TV</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/">A REGRA DO JOGO</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/totalmente-demais/">TOTALMENTE DEMAIS</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/alem-do-tempo/">ALÃM DO TEMPO</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/malhacao/2015/">MALHAÃÃO</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/alem-do-tempo/vem-por-ai/noticia/2015/12/pedro-e-melissa-armam-contra-felipe.html" title="Em &#39;AlÃ©m do Tempo&#39;, Melissa e Pedro armam contra casal"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/b4Dy1YB2mycNhFLpaJ9DrsjJ3uc=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/7g3IoG61bozDTpi9gCbn9tFfG-k=/311x67:720x284/245x130/s3.glbimg.com/v1/AUTH_e84042ef78cb4708aeebdf1c68c6cbd6/photos/apis/36d89d45263b43b0a501644dcaa71435/melissa-escuta-os-planos-de.jpg" alt="Em &#39;AlÃ©m do Tempo&#39;, Melissa e Pedro armam contra casal (TV Globo)" title="Em &#39;AlÃ©m do Tempo&#39;, Melissa e Pedro armam contra casal (TV Globo)"
                    data-original-image="s2.glbimg.com/7g3IoG61bozDTpi9gCbn9tFfG-k=/311x67:720x284/245x130/s3.glbimg.com/v1/AUTH_e84042ef78cb4708aeebdf1c68c6cbd6/photos/apis/36d89d45263b43b0a501644dcaa71435/melissa-escuta-os-planos-de.jpg" data-url-smart_horizontal="_SepjZ0mpdCUJDnsOCtJkqFpgYE=/90x56/smart/filters:strip_icc()/" data-url-smart="_SepjZ0mpdCUJDnsOCtJkqFpgYE=/90x56/smart/filters:strip_icc()/" data-url-feature="_SepjZ0mpdCUJDnsOCtJkqFpgYE=/90x56/smart/filters:strip_icc()/" data-url-tablet="c_A9oLKXvj5EXWQTFKtd1uADZgk=/160x95/smart/filters:strip_icc()/" data-url-desktop="QJqRzMp8FY9TGb30wBhXKqbmn90=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Em &#39;AlÃ©m do Tempo&#39;, Melissa <br />e Pedro armam contra casal</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/malhacao/2015/Vem-por-ai/noticia/2015/12/rodrigo-e-luciana-ficam-de-novo-e-ela-abre-o-jogo-sobre-coquetel.html" title="&#39;MalhaÃ§Ã£o&#39;: Rodrigo e Luciana ficam e ela conversa sobre aids"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/e_WtZfTuLKPRvK-7R_4Ja5dds3Y=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/fW7KvHFgUL3uI2AXt5_F8YkSUuY=/0x137:720x519/245x130/s3.glbimg.com/v1/AUTH_e84042ef78cb4708aeebdf1c68c6cbd6/photos/apis/be0e9b89867f4318b8620d42aa2b3cc5/beijo-luciana-rodrigo.jpg" alt="&#39;MalhaÃ§Ã£o&#39;: Rodrigo e Luciana ficam e ela conversa sobre aids (TV Globo)" title="&#39;MalhaÃ§Ã£o&#39;: Rodrigo e Luciana ficam e ela conversa sobre aids (TV Globo)"
                    data-original-image="s2.glbimg.com/fW7KvHFgUL3uI2AXt5_F8YkSUuY=/0x137:720x519/245x130/s3.glbimg.com/v1/AUTH_e84042ef78cb4708aeebdf1c68c6cbd6/photos/apis/be0e9b89867f4318b8620d42aa2b3cc5/beijo-luciana-rodrigo.jpg" data-url-smart_horizontal="UG1Dp_DTEbL9Moby9xazdBr1xVM=/90x56/smart/filters:strip_icc()/" data-url-smart="UG1Dp_DTEbL9Moby9xazdBr1xVM=/90x56/smart/filters:strip_icc()/" data-url-feature="UG1Dp_DTEbL9Moby9xazdBr1xVM=/90x56/smart/filters:strip_icc()/" data-url-tablet="uwA65gisV4q-unDvbMT3TQo1reE=/160x95/smart/filters:strip_icc()/" data-url-desktop="BqDCyTjMIyWkX3GoVugAJdrUaro=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>&#39;MalhaÃ§Ã£o&#39;: Rodrigo e Luciana ficam e ela conversa sobre aids</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/Bastidores/noticia/2015/12/manuela-llerena-ja-perdeu-amigo-soropositivo-consigo-trazer-o-sofrimento-para-cena.html" title="&#39;Consigo trazer sofrimento para a cena&#39;, diz atriz sobre doenÃ§a"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/RgM085cvXAIRu6CRlvt8mGOcMBA=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/VpEKVN1MAWbqG1HjqjQuV1Arjv8=/148x29:542x237/245x130/s3.glbimg.com/v1/AUTH_e84042ef78cb4708aeebdf1c68c6cbd6/photos/apis/8bdf844ed3a4456fbc85499e6136725e/manuela-llerena-malhacao-2.jpg" alt="&#39;Consigo trazer sofrimento para a cena&#39;, diz atriz sobre doenÃ§a (Fabiano Battaglin/Gshow)" title="&#39;Consigo trazer sofrimento para a cena&#39;, diz atriz sobre doenÃ§a (Fabiano Battaglin/Gshow)"
                    data-original-image="s2.glbimg.com/VpEKVN1MAWbqG1HjqjQuV1Arjv8=/148x29:542x237/245x130/s3.glbimg.com/v1/AUTH_e84042ef78cb4708aeebdf1c68c6cbd6/photos/apis/8bdf844ed3a4456fbc85499e6136725e/manuela-llerena-malhacao-2.jpg" data-url-smart_horizontal="5aQAnh2SPKImjztFIGFAwXE2kp8=/90x56/smart/filters:strip_icc()/" data-url-smart="5aQAnh2SPKImjztFIGFAwXE2kp8=/90x56/smart/filters:strip_icc()/" data-url-feature="5aQAnh2SPKImjztFIGFAwXE2kp8=/90x56/smart/filters:strip_icc()/" data-url-tablet="Ly28btMhhCc3GPwwiR7M9TrKEjc=/160x95/smart/filters:strip_icc()/" data-url-desktop="S61j8jhCU7GzwSq7HDlb2O2MJKs=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>&#39;Consigo trazer sofrimento para a cena&#39;, diz atriz sobre doenÃ§a</p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://gshow.globo.com/Bastidores/noticia/2015/12/paolla-oliveira-ama-supersticoes-no-ano-novo-roupa-branca-por-cima-da-calcinha-colorida.html" title="Atriz, Paolla Oliveira fala de superstiÃ§Ãµes para o Ano Novo"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/Tiw4B2B665ZiFFB9YBN4cJvEEwU=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/j8wRdJtEUKQXXvUJv-3X9JXrbwA=/0x73:570x375/245x130/s.glbimg.com/et/gs/f/original/2015/11/23/car_1711.jpg" alt="Atriz, Paolla Oliveira fala de superstiÃ§Ãµes para o Ano Novo (Carol Caminha/Gshow)" title="Atriz, Paolla Oliveira fala de superstiÃ§Ãµes para o Ano Novo (Carol Caminha/Gshow)"
                    data-original-image="s2.glbimg.com/j8wRdJtEUKQXXvUJv-3X9JXrbwA=/0x73:570x375/245x130/s.glbimg.com/et/gs/f/original/2015/11/23/car_1711.jpg" data-url-smart_horizontal="HbJCkL0iU9AUTlX1mPucqzd9WK8=/90x56/smart/filters:strip_icc()/" data-url-smart="HbJCkL0iU9AUTlX1mPucqzd9WK8=/90x56/smart/filters:strip_icc()/" data-url-feature="HbJCkL0iU9AUTlX1mPucqzd9WK8=/90x56/smart/filters:strip_icc()/" data-url-tablet="Nu898235w1rEKNMjnmisuUrBye0=/160x95/smart/filters:strip_icc()/" data-url-desktop="kaWMlVYHXqChK318ts2VjxnLEDc=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Atriz, Paolla Oliveira fala de superstiÃ§Ãµes para o Ano Novo</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_gshow']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior globoplay"><div class="cabecalho"><h2><a href="http://globoplay.globo.com/" title="globoplay"><span class='logo'>globoplay</span><span class='texto'>globoplay</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://globoplay.globo.com/a-regra-do-jogo/p/8887/">A REGRA DO JOGO</a></h3></li><li class=""><h3><a href="http://globoplay.globo.com/jornal-nacional/p/819/">JORNAL NACIONAL</a></h3></li><li class=""><h3><a href="http://globoplay.globo.com/totalmente-demais/p/8943/">TOTALMENTE DEMAIS</a></h3></li><li class=""><h3><a href="http://globoplay.globo.com/alem-do-tempo/p/8811/">ALÃM DO TEMPO</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://globoplay.globo.com/v/4706747/" title="SÃ©rie apresenta os grandes desafios das OlimpÃ­adas do Rio"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/OceqBCewhMvTvUSTOnhZDndrr3Q=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/flxTv6q2r4o9P7Ky7n8qAp1Kqa8=/245x130/filters:max_age(3600)/s04.video.glbimg.com/deo/vi/47/67/4706747" alt="SÃ©rie apresenta os grandes desafios das OlimpÃ­adas do Rio" title="SÃ©rie apresenta os grandes desafios das OlimpÃ­adas do Rio"
                    data-original-image="s2.glbimg.com/flxTv6q2r4o9P7Ky7n8qAp1Kqa8=/245x130/filters:max_age(3600)/s04.video.glbimg.com/deo/vi/47/67/4706747" data-url-smart_horizontal="VYXw78qdjDwFA0IeIjbAma8ZWNI=/90x56/smart/filters:strip_icc()/" data-url-smart="VYXw78qdjDwFA0IeIjbAma8ZWNI=/90x56/smart/filters:strip_icc()/" data-url-feature="VYXw78qdjDwFA0IeIjbAma8ZWNI=/90x56/smart/filters:strip_icc()/" data-url-tablet="3NINgsfPeUXLUyqY5fSega9D3BU=/160x95/smart/filters:strip_icc()/" data-url-desktop="EC3GGz5E577uethvQQJ5htwh660=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><h3>bom dia brasil</h3><p>SÃ©rie apresenta os grandes desafios das OlimpÃ­adas do Rio</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://globoplay.globo.com/v/4706759/" title="Famosos revelam simpatias que fazem no Ano Novo"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/1qayasDIBj_5Q3ZNKlfogGCZjGc=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/VEDRLKGbahj219b1q-EDvGZh1f8=/245x130/filters:max_age(3600)/s04.video.glbimg.com/deo/vi/59/67/4706759" alt="Famosos revelam simpatias que fazem no Ano Novo" title="Famosos revelam simpatias que fazem no Ano Novo"
                    data-original-image="s2.glbimg.com/VEDRLKGbahj219b1q-EDvGZh1f8=/245x130/filters:max_age(3600)/s04.video.glbimg.com/deo/vi/59/67/4706759" data-url-smart_horizontal="Jj4SWnDewDcTBuVxa14PdbbNwjM=/90x56/smart/filters:strip_icc()/" data-url-smart="Jj4SWnDewDcTBuVxa14PdbbNwjM=/90x56/smart/filters:strip_icc()/" data-url-feature="Jj4SWnDewDcTBuVxa14PdbbNwjM=/90x56/smart/filters:strip_icc()/" data-url-tablet="X2U99WLgLad2tWHgRBkYLLlGP1E=/160x95/smart/filters:strip_icc()/" data-url-desktop="V-IA7UdmvoSEMMe5zxL_Mx0AmPQ=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><h3>mais vocÃª</h3><p>Famosos revelam simpatias que fazem no Ano Novo</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://globoplay.globo.com/v/4707084/" title="Maria Prata dÃ¡ dica de looks para usar na virada"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/1YBa0GxcdT9sB5VO8aRPIsnBUM8=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/NFoxGm_yS2oF_pyMqaFikpXW_zI=/335x188/filters:max_age(3600)/s01.video.glbimg.com/deo/vi/84/70/4707084" alt="Maria Prata dÃ¡ dica de looks para usar na virada" title="Maria Prata dÃ¡ dica de looks para usar na virada"
                    data-original-image="s2.glbimg.com/NFoxGm_yS2oF_pyMqaFikpXW_zI=/335x188/filters:max_age(3600)/s01.video.glbimg.com/deo/vi/84/70/4707084" data-url-smart_horizontal="imlWgY5xIatQlHsqK7uDJyTBhU4=/90x56/smart/filters:strip_icc()/" data-url-smart="imlWgY5xIatQlHsqK7uDJyTBhU4=/90x56/smart/filters:strip_icc()/" data-url-feature="imlWgY5xIatQlHsqK7uDJyTBhU4=/90x56/smart/filters:strip_icc()/" data-url-tablet="RIlx96pJ8gbWyAPHKufK-Cd88vk=/160x95/smart/filters:strip_icc()/" data-url-desktop="-N77ee8BPdrr2X-gO_xmvidCZQ4=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><h3>Encontro com FÃ¡tima</h3><p>Maria Prata dÃ¡ dica de looks para usar na virada</p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://globoplay.globo.com/v/4706317/" title="Dante consegue desmascarar infiltrado da facÃ§Ã£o"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/bzARL2ZmTdwIYaelfFRgNmVk56o=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/583MLilVMCuC6uLozpH__rkA-74=/0x3:720x407/335x188/s3.glbimg.com/v1/AUTH_e84042ef78cb4708aeebdf1c68c6cbd6/photos/apis/82a69aed13814970b5daac5b75a204bf/dante.jpg" alt="Dante consegue desmascarar infiltrado da facÃ§Ã£o (TV Globo)" title="Dante consegue desmascarar infiltrado da facÃ§Ã£o (TV Globo)"
                    data-original-image="s2.glbimg.com/583MLilVMCuC6uLozpH__rkA-74=/0x3:720x407/335x188/s3.glbimg.com/v1/AUTH_e84042ef78cb4708aeebdf1c68c6cbd6/photos/apis/82a69aed13814970b5daac5b75a204bf/dante.jpg" data-url-smart_horizontal="JyU7DeWKLiuXkg246X4WXLs_Jko=/90x56/smart/filters:strip_icc()/" data-url-smart="JyU7DeWKLiuXkg246X4WXLs_Jko=/90x56/smart/filters:strip_icc()/" data-url-feature="JyU7DeWKLiuXkg246X4WXLs_Jko=/90x56/smart/filters:strip_icc()/" data-url-tablet="gndloace016iydZf48SwfjUa59A=/160x95/smart/filters:strip_icc()/" data-url-desktop="Anb01xuIMezHvb93uTM2-LkAyuI=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><h3>A REGRA DO JOGO</h3><p>Dante consegue desmascarar infiltrado da facÃ§Ã£o</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_globoplay']);</script></section><section class="area topglobocom"><div class="container area"><div class="separator"></div><div id="topglobocom3" class="clearfix destaque topglobocom3"><div class="topo"><h3 class="titulo">
            top <strong>globo</strong><span class="subtitulo">tÃ¡ todo mundo clicando...</span></h3></div><div id="mais-vistas" class="clearfix mais-vistas tab-content analytics-area analytics-id-R"><div class="slider"><ol class="noticias analytics-area analytics-id-J"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://extra.globo.com/tv-e-lazer/telinha/alem-do-tempo-flora-diegues-opera-cabeca-as-pressas-deixa-novela-18380482.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Atriz Flora Diegues, de &#39;AlÃ©m do tempo&#39;, opera cabeÃ§a Ã s pressas e deixa a novela</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://extra.globo.com/noticias/rio/jovens-desaparecidas-na-zona-oeste-do-rio-sao-encontradas-em-buzios-18382064.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Jovens desaparecidas na Zona Oeste do Rio sÃ£o encontradas em BÃºzios</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/sp/santos-regiao/noticia/2015/12/mae-de-adolescente-apreendido-pede-perdao-apos-filho-assaltar-turistas.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">MÃ£e de menor apreendido pede perdÃ£o apÃ³s filho assaltar turistas no litoral de SP</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/economia/seu-dinheiro/noticia/2015/12/salario-minimo-em-2016-veja-o-que-muda-com-o-novo-valor.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Veja o que muda com o novo valor do salÃ¡rio mÃ­nimo em 2016: R$ 880</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/sao-paulo/noticia/2015/12/chuva-intensa-antecipa-recuperacao-e-cantareira-sai-do-volume-morto.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Chuva intensa ajuda e Sistema Cantareira sai do volume morto apÃ³s 19 meses</span></span></a></div></div></li></ol><ol class="esportes analytics-area analytics-id-E"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://sportv.globo.com/site/combate/noticia/2015/12/vou-pegar-meu-cinturao-de-novo-afirma-anderson-silva-em-rede-social.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">&#39;Vou pegar meu cinturÃ£o de novo&#39;, afirma Anderson Silva em rede social</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/futebol/central-do-mercado/cobertura.html#/glb-feed-post/5683b9ca1133931f71786e86" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Meia Bernardo Ã© apresentado com camisa do Ulsan, equipe da Coreia do Sul </span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/futebol/futebol-internacional/noticia/2015/12/jornal-diz-que-guardiola-pediu-contratacao-de-neymar-ao-bayern.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Guardiola pediu contrataÃ§Ã£o de Neymar ao Bayern, afirma jornal</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/futebol/futebol-internacional/futebol-espanhol/noticia/2015/12/no-ultimo-jogo-do-ano-barcelona-pode-quebrar-recorde-do-real-madrid.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">No Ãºltimo jogo do ano, Barcelona<br />pode quebrar recorde do Real Madrid</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/futebol/times/santos/noticia/2015/12/no-limite-modesto-e-pessimista-em-manter-marquinhos-gabriel-no-santos.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">No limite, Modesto Ã© pessimista em manter Marquinhos Gabriel no Santos</span></span></a></div></div></li></ol><ol class="entretenimento analytics-area analytics-id-M"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://revistaglamour.globo.com/Celebridades/noticia/2015/12/flavia-alessandra-posa-de-biquini-e-fala-sobre-o-corpo-e-truques-de-beleza.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Aos 41 anos, FlÃ¡via Alessandra faz bonito ao posar de biquÃ­ni e entrega truques de beleza</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://gshow.globo.com/Estilo/noticia/2015/12/laura-keller-ensina-como-manter-o-bumbum-durinho-no-verao.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Laura Keller mostra treino que a ajuda a manter o bumbum durinho no verÃ£o</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/famosos/noticia/2015/12/renata-davilla-exibe-silhueta-enxuta-durante-tarde-em-miami.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Ex-BBB Renatinha exibe silhueta perfeita durante tarde em Miami</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://extra.globo.com/famosos/juliana-paes-compra-quatro-casas-de-veraneio-para-familia-na-regiao-dos-lagos-por-cerca-de-800-mil-18383044.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Juliana Paes compra 4 casas de veraneio em Ã¡rea discreta do RJ no total de R$ 800 mil</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/vem-por-ai/noticia/2015/12/domingas-desconfia-de-cesar.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Em &#39;Regra do Jogo&#39;, CÃ©sar aparece com dinheiro e Domingas desconfia</span></span></a></div></div></li></ol></div></div><div class="controls-area"><ul class="step-marker"><li class=" glb-hl-style-noticias"><div class=""><a href="#" data-related=""><div class="ball active"></div></a></div></li><li class=" glb-hl-style-esporte"><div class=""><a href="#" data-related=""><div class="ball"></div></a></div></li><li class=" glb-hl-style-entretenimento"><div class=""><a href="#" data-related=""><div class="ball "></div></a></div></li></ul></div></div></div></section><section class="area area-pre-servicos analytics-area analytics-id-O"><div class="container area"><section class="franja-globosatplay franja-inferior analytics-area analytics-id-A"><div style="height: 0; width: 0; position: absolute; visibility: hidden"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><path id="a" d="M324.327 4.747c-13.334 0-24.22 10.88-24.22 24.216s10.886 24.215 24.22 24.215c13.333 0 24.213-10.88 24.213-24.215S337.66 4.747 324.327 4.747zm9.425 26.764l-13.615 7.875c-.694.403-1.355.594-1.94.597h-.033c-1.438-.008-2.43-1.16-2.43-3.142V21.088c0-1.977.99-3.132 2.425-3.144h.046c.583.005 1.24.198 1.932.6l13.615 7.874c2.425 1.4 2.425 3.692 0 5.093z"/></defs><symbol id="globosatplay__bis_play" viewBox="0 0 295.891 167.449"><path fill="#25B5E9" d="M136.175 18.26h24.027v78.755h-24.027zM111.892 48.27c1.396-2.798 2.19-5.947 2.19-9.274 0-11.324-9.066-20.522-20.336-20.735H68.48v78.755l29.61-.025c14.408-.19 26.07-11.97 26.07-26.42.002-9.177-4.64-17.483-12.268-22.3M200.714 26.49c-10.937 10.22-11.853 20.946-11.853 30.126l-.002.798c0 6.963-.676 8.996-4.01 12.1l-.098.1c-1.638 1.562-4.397 2.6-12.53 2.6v24.8c10.222 0 20.89-1.144 29.592-9.4 10.928-10.227 11.845-20.953 11.845-30.126l.003-.796c0-6.968 2.073-10.346 4.03-12.114 1.948-1.76 7.288-1.5 9.64-1.5V18.26c-9.91.245-19.553 1.526-26.616 8.23M87.782 149.156c10.597 0 19.245-8.65 19.245-19.248 0-10.6-8.648-19.246-19.245-19.246-10.6 0-19.25 8.647-19.25 19.246 0 10.598 8.652 19.248 19.25 19.248m-6.83-25.506c0-1.57.786-2.49 1.927-2.498h.036c.464.004.985.157 1.535.475l10.823 6.26c1.928 1.114 1.928 2.935 0 4.048l-10.822 6.257c-.55.322-1.075.473-1.54.476h-.027c-1.143-.008-1.932-.922-1.932-2.5V123.65z"/><path fill="#99B1C8" d="M115.258 149.124V110.63h14.46c1.85 0 3.497.298 4.944.894 1.446.597 2.662 1.414 3.645 2.452.982 1.04 1.734 2.242 2.256 3.61.52 1.364.78 2.797.78 4.3 0 1.5-.26 2.933-.78 4.297-.52 1.367-1.283 2.568-2.285 3.606-1.004 1.04-2.22 1.857-3.645 2.453-1.43.597-3.068.895-4.92.895h-11.166v15.986h-3.29zm14.082-18.988c1.27 0 2.424-.202 3.463-.606 1.04-.405 1.933-.972 2.684-1.702.75-.73 1.326-1.598 1.73-2.597.405-1 .605-2.116.605-3.346 0-1.193-.2-2.3-.605-3.32-.404-1.02-.98-1.894-1.73-2.625-.75-.732-1.645-1.3-2.684-1.704-1.04-.403-2.193-.605-3.463-.605h-10.793v16.507h10.793zM185.072 110.63h-4.105l-14.55 35.553h-18.474V110.63h-3.295v38.558h24.354l3.815-9.48h20.405l3.814 9.48h3.814l-15.778-38.558zm-11.156 26.07l9.075-22.544 9.133 22.545h-18.207zM208.795 149.183v-16.475l-15.32-22.084h3.99l12.978 18.96 12.98-18.96h3.987l-15.32 22.084v16.475"/></symbol><symbol id="globosatplay__brasil_play" viewBox="0 0 295.889 167.449"><path fill="#2C3488" d="M182.486.168h-39.88c1.502.426 2.98 1.017 4.396 1.835l34.226 19.762-16.834 30.95-4.677-10.166c6.427-2.052 8.255-5.49 8.808-9.033 1.442-9.142-4.873-12.247-12.63-12.247h-14.798l-5.762 36.57h11.083l2.327-14.794 6.04 14.795h17.791l3.822-7.368h10.14l1.33 7.37h11.583l-6.788-29.475 4.113 2.375c4.496 2.587 7.53 6.73 8.842 11.355V23.3C205.62 10.527 195.262.17 182.487.17m-25.374 33.515c-.446 3.1-2.66 3.768-4.986 3.768h-2.496l1.33-8.37h2.442c2.437.002 4.154 1.722 3.71 4.603m22.168 10.86l5.044-10.472 1.22 10.472h-6.264zM105.473 35.645l-3.498 22.198h16.954c9.09 0 14.02-3.602 14.963-9.698.83-5.098-2.107-8.424-5.6-9.586 3.492-.832 6.098-2.826 6.762-7.148 1.22-7.76-5.21-10.14-11.47-10.14h-9.812l7.078-12.26c2.59-4.5 6.734-7.533 11.363-8.843H113.41c-12.774 0-23.137 10.356-23.137 23.13V63.2c.424-1.51 1.018-2.994 1.838-4.413l13.363-23.14zm12.516-6.948h2.436c1.94 0 3.437 1.664 3.05 4.1-.39 2.605-2.274 3.602-4.602 3.602h-2.104l1.22-7.703zm-2.107 13.3h2.493c2.77 0 4.433 1.825 3.99 4.597-.444 2.77-2.882 3.768-5.263 3.768h-2.552l1.333-8.366zM122.128 98.228c.312.006.62.014.948.014 9.752 0 14.904-4.212 16.127-10.638 1.107-5.544-2.607-8.593-6.54-10.75-1.995-1.108-4.045-1.995-5.54-2.88-1.44-.833-2.553-1.775-2.33-2.94.167-1.33 2.162-2.437 4.214-2.437 4.156 0 6.76 1.33 9.64 3.27l4.877-8.645c-3.714-2.05-6.87-3.435-14.02-3.435-6.703 0-15.35 2.38-16.623 10.527-.886 5.594 2.938 8.365 6.985 10.417 1.992 1 4.043 1.83 5.54 2.718 1.495.886 2.546 1.937 2.325 3.323-.277 1.608-2.937 2.383-4.71 2.383-3.49 0-6.318-1.33-9.863-4.1l-3.464 5.993-10.576-6.107c-4.5-2.595-7.535-6.745-8.844-11.38v18.822c0 12.774 10.36 23.128 23.137 23.128h39.863c-1.498-.422-2.97-1.014-4.382-1.83L122.13 98.23z"/><path fill="#2C3488" d="M185.002 89.43h-13.834l4.547-28.756H164.63l-5.76 36.57h21.62l-5.445 9.427c-2.588 4.497-6.73 7.53-11.354 8.84h18.796c12.775 0 23.133-10.354 23.133-23.128V52.518c-.426 1.497-1.015 2.97-1.827 4.38l-18.79 32.532zM158.924 60.674H147.84l-5.762 36.57h11.082M87.756 167.247c10.597 0 19.243-8.65 19.243-19.246 0-10.598-8.647-19.247-19.244-19.247-10.6 0-19.25 8.65-19.25 19.248s8.65 19.247 19.25 19.247m-6.832-25.505c0-1.572.79-2.49 1.928-2.498h.038c.464.004.985.158 1.535.475l10.82 6.26c1.93 1.113 1.93 2.934 0 4.046l-10.82 6.258c-.552.32-1.077.473-1.542.475h-.027c-1.143-.007-1.932-.923-1.932-2.498v-12.52z"/><path fill="#99B1C8" d="M115.23 167.215V128.72h14.46c1.85 0 3.498.298 4.944.894 1.447.597 2.662 1.415 3.645 2.453.98 1.04 1.733 2.242 2.255 3.61.52 1.364.78 2.798.78 4.298s-.26 2.934-.78 4.3c-.52 1.366-1.283 2.567-2.285 3.605-1.003 1.038-2.22 1.858-3.645 2.454-1.43.596-3.067.895-4.918.895H118.52v15.985h-3.29zm14.082-18.988c1.27 0 2.424-.2 3.463-.605 1.038-.404 1.934-.972 2.684-1.702.75-.73 1.327-1.597 1.73-2.598.403-1 .604-2.116.604-3.347 0-1.193-.2-2.3-.605-3.32-.403-1.018-.98-1.892-1.73-2.625-.75-.73-1.647-1.298-2.685-1.702-1.04-.404-2.193-.605-3.463-.605H118.52v16.505h10.792zM185.045 128.722h-4.104l-14.55 35.552h-18.475v-35.552h-3.295v38.56h24.354l3.816-9.482h20.404l3.814 9.48h3.814l-15.777-38.558zm-11.156 26.072l9.074-22.545 9.132 22.544H173.89zM208.77 167.274V150.8l-15.32-22.085h3.988l12.98 18.962 12.977-18.962h3.988l-15.32 22.085v16.474"/></symbol><symbol id="globosatplay__combate_play" viewBox="0 0 295.89 167.449"><path fill="#CF2028" d="M68.502 61.48l-2.438 9.318c-.38 1.386-1.807 2.562-3.273 2.562H43.644c-1.975 0-3.19-1.43-2.686-3.403l6.424-25.02c.335-1.387 1.808-2.563 3.274-2.563h19.148c1.973 0 3.19 1.552 2.686 3.444l-2.18 8.438H62.83l.925-3.566c.168-.63-.21-1.136-.84-1.136h-8.018c-.676 0-1.387.632-1.555 1.26l-3.61 14.234c-.17.632.206 1.092.837 1.092h7.978c.67 0 1.387-.545 1.553-1.175l.886-3.483h7.516zM94.53 70.797c-.335 1.386-1.806 2.562-3.272 2.562h-19.23c-1.975 0-3.11-1.43-2.604-3.404l6.383-25.02c.334-1.387 1.846-2.563 3.314-2.563h19.147c1.975 0 3.148 1.552 2.69 3.443l-6.426 24.98zm-16.415-5.754c-.168.633.293 1.093.922 1.093h7.98c.675 0 1.34-.545 1.513-1.175l3.695-14.272c.166-.63-.213-1.138-.84-1.138h-8.02c-.715 0-1.39.632-1.557 1.26l-3.693 14.233zM128.12 42.372h12.26c1.978 0 3.15 1.47 2.647 3.444l-7.055 27.543h-7.475l5.795-22.673c.17-.675-.252-1.136-.88-1.136h-5.542c-.672 0-1.387.59-1.555 1.26l-5.752 22.55h-7.516l5.793-22.672c.17-.675-.21-1.136-.838-1.136H111.2l-6.09 23.81h-7.474l7.895-30.988h15.495c1.594 0 2.727.502 3.398 1.3.97-.8 2.186-1.302 3.698-1.302M168.936 60.132l-2.564 10.246c-.46 1.888-2.1 2.98-4.28 2.98H139.96l7.855-30.987h21.035c2.938 0 4.197 1.682 3.486 4.37l-1.514 5.667c-.336 1.218-1.05 2.1-2.057 2.645l-1.637.88c1.722.885 2.224 2.44 1.808 4.2m-18.352 6.004h7.98c.67 0 1.34-.505 1.51-1.175l1.09-4.616c.17-.714-.253-1.133-1.048-.714l-9.785 5.162c-.713.38-.627 1.344.254 1.344m4.494-16.586c-.672 0-1.387.507-1.557 1.178l-1.173 4.703c-.168.67.21.965.838.63l9.826-5.166c.632-.336.757-.46.8-.755.04-.295-.21-.59-.547-.59h-8.185zM176.488 73.36h-7.516l7.307-28.426c.337-1.386 1.85-2.562 3.276-2.562h19.23c1.89 0 3.106 1.47 2.604 3.444l-7.012 27.543h-7.56l2.964-11.29-10.465-.03-2.83 11.32zm4.74-18.417l10.33-.017 1.06-4.238c.164-.63-.213-1.138-.8-1.138h-8.022c-.672 0-1.385.632-1.553 1.26 0 0-1.016 4.093-1.016 4.133M202.814 49.55l1.85-7.178h24.73l-1.847 7.178h-8.65l-6.088 23.81h-7.472l6.086-23.81M248.244 54.253L246.4 61.48h-12.22l-.927 3.564c-.164.632.213 1.092.844 1.092h14.863l-1.848 7.223h-19.988c-1.97 0-3.104-1.43-2.602-3.404l6.34-25.02c.336-1.387 1.893-2.563 3.36-2.563h20.823L253.2 49.55h-14.74c-.67 0-1.384.633-1.554 1.26l-.88 3.444h12.218zM87.752 125.044c10.598 0 19.244-8.65 19.244-19.246 0-10.6-8.646-19.247-19.244-19.247S68.504 95.2 68.504 105.8c0 10.597 8.65 19.246 19.248 19.246m-6.83-25.505c0-1.573.787-2.49 1.928-2.5h.037c.463.005.984.158 1.535.476l10.822 6.26c1.928 1.114 1.928 2.935 0 4.05l-10.822 6.255c-.553.322-1.078.474-1.54.476h-.03c-1.143-.005-1.93-.922-1.93-2.497V99.54z"/><path fill="#99B1C8" d="M115.227 125.012V86.52h14.46c1.85 0 3.5.297 4.946.892 1.445.597 2.66 1.415 3.643 2.454.984 1.04 1.736 2.24 2.258 3.607.52 1.367.78 2.798.78 4.3 0 1.5-.26 2.935-.78 4.3-.52 1.366-1.285 2.567-2.287 3.607-1.002 1.038-2.22 1.856-3.645 2.453-1.428.598-3.066.896-4.918.896h-11.166v15.984h-3.29zm14.082-18.988c1.27 0 2.423-.202 3.462-.604 1.04-.405 1.934-.972 2.686-1.703.748-.73 1.326-1.597 1.73-2.597.405-1 .606-2.116.606-3.348 0-1.192-.2-2.3-.607-3.318-.402-1.02-.98-1.894-1.73-2.626-.75-.73-1.645-1.298-2.685-1.703-1.04-.403-2.193-.604-3.463-.604h-10.792v16.506h10.79zM185.043 86.52h-4.105l-14.55 35.55h-18.476V86.52h-3.295v38.558h24.355l3.814-9.48h20.404l3.814 9.48h3.816l-15.777-38.56zm-11.156 26.07l9.074-22.545 9.132 22.545h-18.205zM208.766 125.072v-16.476l-15.32-22.083h3.99l12.978 18.96 12.977-18.96h3.99l-15.32 22.083v16.476"/></symbol><symbol id="globosatplay__globonews_play" viewBox="0 0 295.889 167.449"><path fill="#EC1C24" d="M235.632 53.06c-6.222-1.726-12.504-2.404-12.752-5.545.005-1.905 1.71-2.287 3.144-2.287 1.052 0 2.55.31 3.432.896.915.61 1.565 1.482 1.587 2.725v-.002h15.11c-.31-10.797-11.337-15.436-21.76-13.81h-15.767l-11.443 21.018V35.04h-14.385l-13.312 24.424V35.04h-50.29v19.27l-10.482-19.27h-16.25v45.65h15.13V60.62l11.27 20.07h50.762l13.185-23.317V80.69h14.754l13.56-24.124c2.805 2.973 7.504 4.17 11.813 5.39 7.29 2.08 9.128 2.67 9.128 4.864 0 2.146-2.86 3.265-4.317 3.265-1.66 0-2.97-.998-3.616-2.74-.167-.434-.312-1.056-.312-1.997h-16.875c.304 13.797 15.784 15.562 20.744 15.562 9.793 0 20.27-3.698 20.27-15.158 0-8.068-6.106-10.97-12.328-12.692m-79.557 15.12h-22.14V63.3h17.147V52.43h-17.146v-4.883h22.14V68.18z"/><path fill-rule="evenodd" clip-rule="evenodd" fill="#B1B3B5" d="M91.036 57.867c0 3.104-.016 6.17-.314 9.137-.164 1.617-.973 1.988-2.475 2.148-5.2.544-10.955.67-16.344.653-5.388.018-11.145-.11-16.344-.653-1.507-.16-2.312-.53-2.48-2.148-.295-2.968-.312-6.033-.312-9.137 0-3.106.018-6.173.313-9.14.168-1.615.973-1.986 2.48-2.147 5.198-.543 10.955-.67 16.343-.655 5.39-.014 11.145.112 16.344.655 1.502.16 2.31.532 2.475 2.148.3 2.966.314 6.032.314 9.14m4.84 0c0 13.167-10.74 23.91-23.973 23.91-13.232 0-23.975-10.743-23.975-23.91 0-13.173 10.742-23.914 23.975-23.914 13.232 0 23.973 10.74 23.973 23.913m-33.715 0c0-5.33 4.348-9.678 9.743-9.678 5.394 0 9.742 4.346 9.742 9.677 0 5.33-4.35 9.677-9.742 9.677-5.395 0-9.742-4.348-9.742-9.677"/><path fill="#EC1C24" d="M87.753 133.464c10.598 0 19.245-8.65 19.245-19.248 0-10.6-8.647-19.246-19.245-19.246s-19.248 8.647-19.248 19.246c0 10.598 8.65 19.248 19.248 19.248m-6.83-25.506c0-1.57.787-2.49 1.928-2.498h.038c.464.004.986.157 1.535.475l10.822 6.26c1.928 1.114 1.928 2.935 0 4.048L84.423 122.5c-.552.322-1.077.473-1.54.476h-.03c-1.143-.008-1.93-.922-1.93-2.5V107.96z"/><path fill="#99B1C8" d="M115.23 133.43V94.94h14.458c1.85 0 3.5.298 4.945.894 1.446.597 2.66 1.414 3.645 2.452.982 1.04 1.734 2.242 2.256 3.61.52 1.364.78 2.797.78 4.3 0 1.5-.26 2.933-.78 4.297-.52 1.368-1.283 2.57-2.286 3.607-1.003 1.04-2.218 1.858-3.644 2.454-1.43.598-3.068.896-4.92.896h-11.166v15.986h-3.29zm14.08-18.987c1.27 0 2.426-.202 3.465-.606 1.038-.405 1.933-.972 2.684-1.702.75-.73 1.325-1.598 1.73-2.597.403-1 .604-2.117.604-3.347 0-1.192-.2-2.3-.605-3.32-.405-1.018-.98-1.893-1.73-2.624-.752-.73-1.647-1.298-2.685-1.703-1.04-.403-2.194-.605-3.464-.605h-10.79v16.506h10.79zM185.044 94.938h-4.105l-14.55 35.553h-18.475V94.94h-3.295v38.558h24.354l3.815-9.48h20.403l3.814 9.48h3.815l-15.778-38.558zm-11.156 26.07l9.074-22.544 9.132 22.545h-18.206zM208.767 133.49v-16.474l-15.32-22.085h3.99l12.978 18.963 12.978-18.962h3.99l-15.32 22.086v16.475"/></symbol><symbol id="globosatplay__globosat_play" viewBox="0 0 295.889 167.449"><path fill="#4576A1" d="M72.195 51.673l2.387-5.126-2.867-7.704-5.62 11.38M41.076 53.863l5.092 6.374 3.543.98 2.335-4.477M79.79 54.882l-3.915 8.346-14.404-3.836-7.087 14.202-7.865-2.624 4.87 5.28 7.717 2.682 6.258-13.048 13.33 3.825 3.504-7.972"/><path fill="#4576A1" d="M61.016 58.384l14.386 3.83 3.76-8.012-7.12-1.686v.003l-7.192-1.712 6.222-12.6-7.64-1.42-6.362 12.118-12.332-2.93-3.894 6.94L53.3 56.184l-2.743 5.267.007.002-4.445 8.48 7.827 2.613"/><path fill-rule="evenodd" clip-rule="evenodd" fill="#4576A1" d="M149.352 44.935h8.602c5.793 0 7.998 3.66 8.014 6.914.012 2.36-.64 3.944-2.365 5.22 2.195 1.144 3.326 3.445 3.326 5.805 0 4.464-2.935 7.912-8.17 7.912h-9.407V44.935zm8.462 5.658l-1.857-.003v4.367h1.83c1.25 0 2.152-.937 2.148-2.258-.003-1.37-1.152-2.107-2.12-2.107m.218 9.316h-2.076v5.162h2.23c1.417 0 2.505-1.262 2.505-2.56 0-1.41-1.092-2.603-2.66-2.603m-23.553-8.964c3.804 0 6.91 3.11 6.91 6.917s-3.106 6.915-6.91 6.915c-3.813 0-6.92-3.107-6.92-6.915s3.106-6.917 6.92-6.917m0-6.726c7.51 0 13.638 6.13 13.638 13.644 0 7.508-6.13 13.637-13.64 13.637-7.513 0-13.64-6.128-13.64-13.636 0-7.514 6.127-13.644 13.64-13.644m46.797 6.726c3.81 0 6.918 3.11 6.918 6.917s-3.11 6.915-6.918 6.915-6.916-3.107-6.916-6.915 3.107-6.917 6.915-6.917m0-6.726c7.512 0 13.64 6.13 13.64 13.644 0 7.508-6.128 13.637-13.64 13.637-7.508 0-13.64-6.128-13.64-13.636 0-7.514 6.133-13.644 13.64-13.644m-64.048.715l-6.57 10.92h-9.43v5.653h6.428c-1.225 1.82-2.89 3.12-5.902 3.1-3.482-.02-6.803-2.312-6.803-7.13 0-4.092 3.525-6.837 6.662-6.833 2.396 0 5.32.718 7.3 3.998l3.526-5.84c-2.938-3.11-6.19-4.575-11.21-4.575-7.265.157-12.962 6.37-12.98 13.073 0 8.202 6.57 13.486 13.188 13.486h19.375V64.97h-7.897l12.084-20.035h-7.77z"/><path fill="#4576A1" d="M205.102 71.498c-5.293 0-9.484-2.736-9.484-8.345v-.855h6.68c0 1.706.78 3.588 2.732 3.588 1.385 0 2.555-1.103 2.555-2.52 0-1.707-1.42-2.274-2.803-2.88-.78-.355-1.562-.673-2.346-.99-3.41-1.426-6.428-3.41-6.428-7.532 0-4.9 4.727-7.744 9.2-7.744 2.556 0 5.433.96 7.1 2.985 1.353 1.67 1.673 3.125 1.74 5.184h-6.64c-.213-1.454-.71-2.56-2.414-2.56-1.172 0-2.31.816-2.31 2.062 0 .39.036.782.25 1.1.638 1.065 4.083 2.417 5.187 2.915 3.478 1.598 6.146 3.405 6.146 7.53 0 5.504-3.98 8.06-9.164 8.06M249.898 50.615v20.17h-6.712v-20.17h-5.184v-5.68h17.045v5.68M227.77 44.946l-15.587 25.84h7.768l2.157-3.576h7.94v3.572h6.72V44.946h-8.996zm2.277 17.35h-4.973l4.973-8.238v8.237zM87.74 130.626c10.598 0 19.244-8.648 19.244-19.247 0-10.6-8.646-19.247-19.244-19.247s-19.25 8.648-19.25 19.246c0 10.598 8.653 19.246 19.25 19.246m-6.83-25.506c0-1.57.787-2.49 1.926-2.498h.037c.465.005.984.157 1.535.475l10.822 6.26c1.928 1.114 1.928 2.937 0 4.05l-10.822 6.256c-.55.32-1.076.473-1.54.475h-.028c-1.143-.006-1.93-.922-1.93-2.497v-12.52z"/><path fill="#99B1C8" d="M115.215 130.594V92.102h14.46c1.85 0 3.5.297 4.946.893 1.446.598 2.66 1.414 3.644 2.453.982 1.038 1.734 2.242 2.256 3.606.52 1.366.78 2.8.78 4.3 0 1.502-.26 2.935-.78 4.3-.52 1.365-1.283 2.566-2.285 3.606-1.002 1.038-2.22 1.857-3.645 2.453-1.43.598-3.068.896-4.92.896h-11.166v15.984h-3.29zm14.082-18.988c1.27 0 2.424-.202 3.463-.604 1.04-.406 1.934-.972 2.684-1.703s1.326-1.6 1.73-2.6.605-2.116.605-3.347c0-1.192-.202-2.3-.606-3.318-.404-1.02-.98-1.894-1.73-2.626-.75-.73-1.645-1.3-2.684-1.705-1.04-.403-2.193-.604-3.463-.604h-10.793v16.507h10.793zM185.03 92.102h-4.105l-14.55 35.55H147.9v-35.55h-3.295v38.56h24.355l3.814-9.483h20.404l3.814 9.48h3.814L185.03 92.103zm-11.157 26.07l9.076-22.545 9.13 22.546h-18.207zM208.752 130.653V114.18l-15.318-22.085h3.988l12.978 18.96 12.98-18.96h3.987L212.05 114.18v16.473"/></symbol><symbol id="globosatplay__globosatplay" viewBox="0 0 500 58"><path fill-rule="evenodd" clip-rule="evenodd" fill="#98B0C7" d="M108.42 6.036h15.26c10.282 0 14.192 6.485 14.224 12.26.022 4.193-1.14 7.007-4.202 9.262 3.9 2.035 5.91 6.114 5.91 10.305 0 7.92-5.208 14.037-14.503 14.037h-16.69V6.036zm15.017 10.032l-3.3-.003v7.746h3.253c2.216 0 3.812-1.666 3.81-4.006-.005-2.423-2.043-3.735-3.763-3.736m.392 16.54h-3.692v9.155h3.958c2.52 0 4.443-2.236 4.443-4.545 0-2.505-1.936-4.61-4.71-4.61M82.026 16.695c6.758 0 12.27 5.515 12.27 12.272 0 6.757-5.512 12.273-12.27 12.273-6.76 0-12.272-5.517-12.272-12.273 0-6.757 5.513-12.272 12.272-12.272m0-11.928c13.326 0 24.2 10.874 24.2 24.2s-10.874 24.204-24.2 24.204c-13.328 0-24.205-10.875-24.205-24.202s10.877-24.2 24.205-24.2m83.038 11.927c6.757 0 12.27 5.515 12.27 12.272 0 6.757-5.514 12.273-12.27 12.273-6.758 0-12.274-5.517-12.274-12.273 0-6.757 5.518-12.272 12.275-12.272m0-11.928c13.325 0 24.2 10.874 24.2 24.2s-10.875 24.204-24.2 24.204c-13.327 0-24.206-10.875-24.206-24.202s10.878-24.2 24.205-24.2M51.417 6.035L39.764 25.4H23.027v10.035h11.408c-2.175 3.23-5.125 5.537-10.474 5.505-6.18-.035-12.07-4.104-12.07-12.655 0-7.258 6.262-12.128 11.824-12.125 4.25.004 9.437 1.27 12.948 7.096l6.26-10.362c-5.21-5.52-10.983-8.116-19.895-8.116C10.143 5.05.032 16.08 0 27.972 0 42.526 11.66 51.9 23.398 51.9h34.38V41.577H43.764l21.44-35.54H51.418z"/><path fill="#98B0C7" d="M207.336 53.162c-9.39 0-16.824-4.853-16.824-14.81V36.84h11.848c0 3.023 1.385 6.365 4.852 6.365 2.458 0 4.538-1.954 4.538-4.475 0-3.025-2.52-4.032-4.98-5.104-1.386-.63-2.77-1.197-4.158-1.766-6.048-2.52-11.405-6.048-11.405-13.36 0-8.693 8.38-13.734 16.32-13.734 4.538 0 9.644 1.7 12.604 5.292 2.397 2.96 2.963 5.546 3.09 9.2h-11.785c-.38-2.583-1.26-4.537-4.286-4.537-2.08 0-4.096 1.45-4.096 3.655 0 .694.063 1.388.44 1.955 1.134 1.89 7.248 4.285 9.2 5.167 6.177 2.834 10.902 6.05 10.902 13.36 0 9.768-7.06 14.305-16.26 14.305M286.824 16.108v35.79h-11.907V16.11h-9.203V6.036h30.25v10.072M247.565 6.053l-27.66 45.85h13.784l3.824-6.346h14.083v6.337h11.926V6.054h-15.958zm4.033 30.783h-8.816l8.816-14.615v14.616zM358.895 53.18V4.747h18.193c2.328 0 4.4.374 6.222 1.124 1.82.75 3.35 1.78 4.585 3.086 1.236 1.308 2.183 2.82 2.838 4.54s.983 3.52.983 5.408c0 1.888-.328 3.69-.983 5.41-.655 1.718-1.614 3.23-2.875 4.536-1.262 1.308-2.792 2.338-4.586 3.087-1.797.75-3.86 1.126-6.188 1.126h-14.05v20.112h-4.14zm17.718-23.89c1.597 0 3.05-.255 4.355-.763 1.307-.51 2.434-1.223 3.377-2.143.944-.92 1.67-2.01 2.18-3.268.507-1.258.76-2.662.76-4.21 0-1.5-.253-2.894-.76-4.177-.51-1.283-1.235-2.384-2.18-3.305-.943-.92-2.07-1.634-3.377-2.143-1.307-.508-2.76-.76-4.355-.76h-13.578v20.765h13.578zM446.732 4.748h-5.163l-18.307 44.73h-23.245V4.748h-4.146V53.26h30.642l4.8-11.928h25.672l4.8 11.928h4.8L446.732 4.748zM432.697 37.55l11.418-28.365 11.488 28.365h-22.906zM476.58 53.254v-20.73L457.307 4.74h5.018l16.33 23.857L494.98 4.74H500l-19.273 27.785v20.73"/><clipPath id="b"><use xlink:href="#a" overflow="visible"/></clipPath><linearGradient id="c" gradientUnits="userSpaceOnUse" x1="-281.142" y1="327.715" x2="-280.55" y2="327.715" gradientTransform="scale(81.6953) rotate(-45 257.577 -507.484)"><stop offset="0" stop-color="#7B61A6"/><stop offset=".061" stop-color="#8E5599"/><stop offset=".198" stop-color="#B53C80"/><stop offset=".324" stop-color="#D12B6E"/><stop offset=".435" stop-color="#E22063"/><stop offset=".518" stop-color="#E81C5F"/><stop offset=".587" stop-color="#E92859"/><stop offset=".714" stop-color="#ED484A"/><stop offset=".882" stop-color="#F37C31"/><stop offset="1" stop-color="#F7A51E"/></linearGradient><path clip-path="url(#b)" fill="url(#c)" d="M275.894 28.963l48.43-48.432 48.43 48.433-48.43 48.432"/></symbol><symbol id="globosatplay__gloob_play" viewBox="0 0 295.891 167.449"><path fill="#A3CC39" d="M165.266 39.822c-14.754 0-26.715 11.96-26.715 26.715 0 14.755 11.962 26.712 26.716 26.712 14.752 0 26.713-11.958 26.713-26.713 0-14.754-11.962-26.715-26.714-26.715m0 31.61c-2.71 0-4.898-2.19-4.898-4.895 0-2.703 2.19-4.9 4.898-4.9 2.703 0 4.896 2.197 4.896 4.9 0 2.704-2.193 4.894-4.896 4.894"/><path fill="#8375B5" d="M198.432 28.245c-7.867 0-14.246 6.38-14.246 14.247 0 7.87 6.38 14.247 14.246 14.247 7.87 0 14.248-6.378 14.248-14.248 0-7.868-6.377-14.247-14.248-14.247m.004 17.362c-1.723 0-3.12-1.393-3.12-3.115 0-1.72 1.397-3.117 3.12-3.117 1.72 0 3.113 1.397 3.113 3.117 0 1.723-1.396 3.115-3.114 3.115"/><path fill="#FFCA07" d="M124.137 22.9l21.046 3.71-11.755 66.644-21.045-3.712z"/><path fill="#EE4065" d="M238.043 54.372l-4.803-2.246 5.27-11.297-19.367-9.032-22.58 48.423 23.973 11.18c.006 0 .01.006.014.006l.184.085.004-.003c10.21 4.632 22.268.19 27.016-9.992 4.746-10.187.396-22.278-9.71-27.123M229.39 72.93c-.624 1.338-2.214 1.915-3.55 1.293-.03-.016-.057-.03-.084-.047l-.004.006-2.328-1.085 2.258-4.847 2.332 1.09-.004.004c.03.014.055.022.086.037 1.338.624 1.914 2.213 1.295 3.55"/><path fill="#25B2E7" d="M109.906 35.702l-31.45 14.666 6.772 14.525 11.64-5.427c-.618 5.004-3.716 9.596-8.626 11.882-7.578 3.535-16.586.26-20.12-7.32-3.53-7.575-.255-16.585 7.325-20.118 4.05-1.89 8.514-1.828 12.324-.204l7.827-18.933c-8.932-3.732-19.344-3.838-28.807.576-17.832 8.312-25.543 29.505-17.23 47.335 8.313 17.827 29.505 25.54 47.335 17.225 17.828-8.312 25.54-29.504 17.227-47.33-1.163-2.502-2.59-4.792-4.216-6.878M87.756 145.41c10.596 0 19.242-8.646 19.242-19.245s-8.646-19.246-19.242-19.246c-10.6 0-19.25 8.646-19.25 19.245s8.65 19.245 19.25 19.245m-6.832-25.504c0-1.572.787-2.49 1.928-2.498h.037c.464.004.983.156 1.534.474l10.822 6.26c1.928 1.114 1.928 2.936 0 4.05l-10.822 6.257c-.553.32-1.076.47-1.54.474h-.028c-1.143-.008-1.932-.922-1.932-2.497v-12.52z"/><path fill="#99B1C8" d="M115.23 145.38v-38.493h14.46c1.85 0 3.5.297 4.944.893 1.447.598 2.662 1.414 3.645 2.453s1.733 2.24 2.255 3.608c.52 1.366.78 2.8.78 4.3s-.26 2.933-.78 4.3c-.52 1.364-1.285 2.566-2.285 3.605-1.004 1.038-2.22 1.857-3.645 2.454-1.43.596-3.068.894-4.918.894H118.52v15.986h-3.29zm14.082-18.988c1.268 0 2.424-.2 3.463-.605 1.037-.403 1.932-.97 2.684-1.703.75-.73 1.325-1.597 1.73-2.596.403-1 .604-2.117.604-3.348 0-1.192-.2-2.3-.605-3.32s-.98-1.894-1.73-2.626c-.753-.73-1.647-1.298-2.685-1.7-1.04-.405-2.195-.606-3.463-.606H118.52v16.505h10.792zM185.043 106.887h-4.104l-14.55 35.552h-18.475v-35.553h-3.295v38.558h24.354l3.814-9.48h20.406l3.814 9.48h3.814l-15.78-38.558zm-11.154 26.07l9.073-22.545 9.133 22.545H173.89zM208.768 145.44v-16.476l-15.32-22.084h3.99l12.978 18.962 12.98-18.962h3.987l-15.32 22.084v16.475"/></symbol><symbol id="globosatplay__gnt_play" viewBox="0 0 295.89 167.449"><path fill-rule="evenodd" clip-rule="evenodd" fill="#6B4894" d="M60.37 41.958c-12.593 0-34.388 3.143-34.388 25.488 0 22.342 24.965 25.507 34.39 25.702 7.757.15 13.917-.502 20.405-2.636 0 0 .15 4.52-.1 8.215-.424 6.172-1.56 10.05-4.258 13.426-5.208 6.504-15.85 6.457-22.312 6.54-3.02-.003-8.455-.505-8.455-.505l.015 4.837c25.136 2.344 48.06.826 51.542-20.86.51-3.136.566-6.45.576-12.136.02-4.083 0-48.072 0-48.072H60.37zm20.407 42.488c0 .427-7.857 3.79-17.635 2.742-11.37-1.215-18.895-8.914-18.895-19.753 0-11.458 7.33-18.604 16.337-19.987 3.458-.53 8.563-.777 12.552-.62 4.072.152 7.64.616 7.64.616v37.002zM157.954 66.478c0-14.182-6.327-19.558-22.784-19.558-4.518 0-12.164.585-12.235.585V91.9H105.77V41.96h33.434c12.59 0 35.92-1.047 35.92 27.06V91.9h-17.17V66.478zM204.188 86.27c-6.637-4.058-6.014-12.093-6.01-20.095.016-1.473 0-6.177 0-16.838h19.528v-5.393h-19.528c.016-6.61-.004-15.295 0-22.087h-16.666v46.21c0 17.42 9.27 25.803 31.027 25.803 2.04 0 3.78-.068 6.077-.216v-5.418c-3.886.082-10.706.308-14.43-1.966"/><path fill="#6B4894" d="M130.28 145.56c10.597 0 19.244-8.65 19.244-19.247 0-10.6-8.647-19.247-19.245-19.247s-19.25 8.648-19.25 19.247c0 10.597 8.652 19.246 19.25 19.246m-6.832-25.506c0-1.572.788-2.49 1.928-2.498h.037c.464.004.985.157 1.535.475l10.82 6.26c1.93 1.115 1.93 2.936 0 4.05l-10.82 6.256c-.552.32-1.077.473-1.54.475h-.03c-1.142-.004-1.93-.92-1.93-2.496v-12.52z"/><path fill="#99B1C8" d="M157.755 145.527v-38.494h14.46c1.85 0 3.497.298 4.944.893 1.445.597 2.66 1.415 3.644 2.454.982 1.04 1.734 2.24 2.256 3.607.52 1.367.78 2.798.78 4.3 0 1.5-.26 2.935-.78 4.3-.52 1.366-1.283 2.567-2.285 3.607-1.004 1.038-2.22 1.856-3.645 2.453-1.43.598-3.068.896-4.92.896h-11.166v15.985h-3.29zm14.082-18.987c1.27 0 2.424-.202 3.463-.604 1.04-.405 1.933-.972 2.684-1.703.75-.73 1.326-1.597 1.73-2.597.405-1 .605-2.116.605-3.348 0-1.192-.2-2.3-.606-3.318-.404-1.02-.98-1.894-1.73-2.626-.75-.73-1.645-1.298-2.684-1.703-1.04-.402-2.193-.603-3.463-.603h-10.793v16.505h10.793zM227.57 107.034h-4.106l-14.55 35.552H190.44v-35.552h-3.295v38.56H211.5l3.814-9.482h20.405l3.813 9.48h3.814l-15.778-38.558zm-11.157 26.07l9.075-22.544 9.132 22.545h-18.207zM251.292 145.587V129.11l-15.32-22.082h3.99l12.978 18.96 12.98-18.96h3.987l-15.32 22.083v16.477"/></symbol><symbol id="globosatplay__megapix_play" viewBox="0 0 295.889 167.449"><path fill="#5DDB24" d="M82.494 126.062c7.5 7.5 19.7 7.5 27.2 0s7.5-19.7 0-27.1c-7.5-7.5-19.7-7.5-27.1 0-7.6 7.4-7.6 19.6-.1 27.1"/><path fill="#FFF" d="M91.994 103.762c-1.1 0-1.9.9-1.9 2.5v12.5c0 1.6.8 2.5 1.9 2.5.5 0 1-.2 1.5-.5l10.8-6.2c1.9-1.1 1.9-2.9 0-4l-10.8-6.2c-.5-.5-1-.6-1.5-.6z"/><path fill="#99B1C8" d="M123.395 131.662v-38.4h14.4c1.8 0 3.5.3 4.9.9s2.7 1.4 3.6 2.4c1 1 1.7 2.2 2.2 3.6s.8 2.8.8 4.3-.3 2.9-.8 4.3-1.3 2.6-2.3 3.6-2.2 1.9-3.6 2.4c-1.4.6-3.102.9-4.9.9h-11.1v15.9h-3.2v.1zm14.1-18.9c1.3 0 2.4-.2 3.5-.6 1-.4 1.9-1 2.7-1.7.7-.7 1.3-1.6 1.7-2.6.398-1 .6-2.1.6-3.3 0-1.2-.2-2.3-.6-3.3-.4-1-1-1.9-1.7-2.6s-1.6-1.3-2.7-1.7c-1-.4-2.2-.6-3.5-.6h-10.8v16.5l10.8-.1zM153.294 131.662v-38.4h3.3v35.4h18.5v3"/><path fill="#99B1C8" d="M205.494 131.662l-3.8-9.4h-20.3l-3.8 9.4h-3.8l15.7-38.4h4.1l15.7 38.4h-3.8zm-14-34.9l-9 22.4h18.1l-9.1-22.4zM217.694 131.662v-16.4l-15.3-22h4l12.9 18.9 12.9-18.9h4l-15.3 22v16.4"/><g><path fill="#453767" d="M92.494 78.162v-23.9l-9.3 23.9h-3.7l-9.2-24v24h-8.5v-34.5h12.1l7.4 19.5 7.6-19.5h12.1v34.5M108.194 78.162v-34.5h25.1v7.4h-16.6v6h16.2v7.3h-16.2v6.5h16.6v7.3M156.395 78.762c-2.6 0-5-.4-7.3-1.2-2.3-.8-4.2-2-5.9-3.6-1.7-1.5-3-3.4-4-5.6s-1.5-4.7-1.5-7.4.5-5.2 1.5-7.4 2.3-4.1 4-5.6 3.7-2.7 5.9-3.6c2.2-.8 4.7-1.2 7.3-1.2 1.9 0 3.7.2 5.2.7s2.9 1.1 4.1 1.9c1.2.8 2.3 1.7 3.1 2.7.8.9 1.5 1.9 2.1 2.9l-7.1 3.7c-.7-1.102-1.6-2.102-2.9-3-1.3-.9-2.9-1.4-4.6-1.4-1.5 0-2.8.3-4 .8-1.2.5-2.3 1.3-3.2 2.2-.9.9-1.6 2-2.1 3.3s-.7 2.6-.7 4.1.2 2.8.7 4.1 1.2 2.4 2.1 3.3c.9.9 2 1.7 3.2 2.2 1.2.5 2.6.8 4 .8 1.3 0 2.6-.2 3.8-.7 1.2-.4 2.2-.9 2.8-1.5l.1-.1v-3.2h-7.8v-7.4h16.4v13.6c-1.8 2-4 3.7-6.5 4.9-2.6 1.1-5.5 1.7-8.7 1.7M200.694 78.162l-1.7-5h-14.2l-1.7 5h-9.7l13-34.5h11l13 34.5h-9.7zm-13.6-12.4h9.7l-4.8-14.3-4.9 14.3zM213.694 78.162v-34.5h17.3c1.9 0 3.6.3 5.1.9 1.5.6 2.7 1.4 3.7 2.5 1 1 1.8 2.3 2.3 3.6.5 1.4.8 2.9.8 4.4s-.3 3-.8 4.4c-.5 1.4-1.3 2.6-2.3 3.6s-2.3 1.9-3.7 2.5c-1.5.6-3.2.9-5.1.9h-8.8v11.8l-8.5-.1zm8.5-19.1h7.6c1.3 0 2.4-.3 3.2-1 .9-.7 1.3-1.7 1.3-3s-.4-2.3-1.3-3c-.8-.7-1.9-1-3.2-1h-7.6v8zM247.095 43.662h8.5v34.5h-8.5zM284.694 78.162l-7.8-12-7.7 12h-10.1l12.1-17.7-11.3-16.8h10l7 11.2 6.9-11.2h10.1l-11.2 16.8 12 17.7"/><path fill="#97DE32" d="M51.494 60.962l-25.1-25.2v25.2"/><path fill="#BDF950" d="M26.395 35.762l-25.2 25.2h25.2"/><path fill="#5DDB24" d="M1.194 60.962l25.2 25.1v-25.1"/><path fill="#38BB27" d="M26.395 60.962v25.1l25.1-25.1"/></g></symbol><symbol id="globosatplay__multishow_play" viewBox="0 0 295.89 167.449"><path fill="#E7525A" d="M155.005 17.89h11.906v26.237h17.466V54.46h-29.373M215.34 17.89h11.905v36.57H215.34zM189.14 28.222h-10.323V17.886h32.55v10.336h-10.323V54.46H189.14M81.302 54.062H69.24v-36.25h12.728l7.94 12.72 7.94-12.72h12.288v36.25H98.66V36.974L89.96 49.69l-8.656-12.716M138.087 17.812v19.724c0 4.707-2.053 6.588-5.295 6.588-3.238 0-5.295-2.05-5.295-6.87V17.813h-12.95v20.126c0 12.246 7.265 17.314 18.284 17.314 11 0 18.5-4.938 18.5-17.49v-19.95h-13.243zM128.083 59.665v12.96h-12.06v-12.96H104.2l-.008 37.42h11.83V84.12h12.06v12.966h11.476v-37.42M170.524 78.417v-.102c0-4.336-2.957-8.263-7.635-8.263-4.624 0-7.532 3.875-7.532 8.16v.103c0 4.34 2.96 8.264 7.635 8.264 4.627-.002 7.53-3.87 7.53-8.163m-27.428 0v-.102c0-10.616 8.678-19.08 19.898-19.08 11.223 0 19.795 8.36 19.795 18.978v.102c0 10.614-8.678 19.085-19.898 19.085-11.223 0-19.795-8.366-19.795-18.983M215.175 59.665h12.36v37.42H214.54l-7.94-12.72-7.937 12.72H186.34v-37.42h11.77V77.52l8.553-12.726 8.512 12.727M89.67 73.864c-.968-.232-2.923-.672-2.923-.672-3.926-.796-4.562-1.448-4.562-2.462v-.102c0-.905.807-1.81 2.664-1.81 2.642 0 5.31.93 8.183 2.394l8.02-7.23c-4.208-3.165-9.712-4.748-16.606-4.748-10.227 0-16.09 5.484-16.09 12.565v.103c0 6.354 5.066 9.13 11.27 10.87.97.23 3.256.668 3.256.668 3.922.794 4.893 1.446 4.893 2.457v.106c0 .905-1.037 1.812-2.895 1.812-2.642 0-5.43-.934-8.3-2.396l-8.08 7.228c4.207 3.166 9.486 4.75 16.38 4.75 10.228 0 15.84-5.486 15.84-12.563v-.105c0-6.36-4.85-9.133-11.05-10.866M87.604 149.604c10.598 0 19.246-8.65 19.246-19.247s-8.648-19.247-19.246-19.247-19.248 8.65-19.248 19.247 8.65 19.247 19.248 19.247m-6.83-25.506c0-1.57.787-2.49 1.928-2.5h.037c.462.006.983.16 1.534.478l10.82 6.26c1.93 1.113 1.93 2.934 0 4.047l-10.82 6.256c-.553.32-1.078.473-1.54.475h-.03c-1.143-.006-1.93-.922-1.93-2.497v-12.52z"/><path fill="#99B1C8" d="M115.08 149.57V111.08h14.46c1.85 0 3.498.3 4.945.893 1.445.6 2.66 1.416 3.645 2.454.982 1.04 1.734 2.242 2.256 3.607.52 1.368.78 2.8.78 4.3 0 1.502-.26 2.936-.78 4.3-.52 1.367-1.283 2.568-2.287 3.608-1.003 1.038-2.22 1.857-3.644 2.45-1.43.6-3.068.897-4.92.897H118.37v15.985h-3.29zm14.083-18.986c1.27 0 2.424-.202 3.46-.606 1.04-.405 1.935-.97 2.685-1.702.752-.73 1.328-1.597 1.732-2.598.404-1 .605-2.115.605-3.347 0-1.192-.2-2.297-.605-3.318-.404-1.02-.98-1.894-1.732-2.625-.75-.73-1.645-1.3-2.684-1.703-1.037-.403-2.19-.604-3.46-.604H118.37v16.505h10.793zM184.896 111.078h-4.105l-14.548 35.552h-18.477v-35.552h-3.295v38.56h24.355l3.814-9.483h20.405l3.814 9.482h3.813l-15.777-38.56zm-11.157 26.07l9.073-22.543 9.133 22.544H173.74zM208.618 149.63v-16.474l-15.32-22.083h3.988l12.98 18.96 12.98-18.96h3.987l-15.32 22.083v16.475"/></symbol><symbol id="globosatplay__off_play" viewBox="0 0 295.891 167.449"><path fill="#010101" d="M91.217 30.085c-.89 1.124-2.383 2.06-4.496 2.06-3 0-5.05-1.95-5.05-4.816 0-3.883 2.986-6.97 6.72-6.97 1.65 0 3.146.696 3.78 1.79l-1.033.68c-.348-.728-1.303-1.488-2.78-1.488-3.224 0-5.48 2.882-5.48 5.876 0 2.186 1.382 3.943 3.91 3.943 1.35 0 2.62-.522 3.59-1.678l.84.603zM92.678 31.86h-1.303l7.244-11.216h1.11l2.336 11.215h-1.19l-.605-2.947h-5.75l-1.842 2.946zm2.494-3.993h4.924l-1.176-5.955-3.748 5.955zM111.898 30.26h.032l1.7-9.616h1.128L112.77 31.86h-1.413l-4.605-9.742h-.033l-1.716 9.74h-1.127l1.984-11.214h1.415M115.63 31.86h-1.304l7.246-11.216h1.11l2.337 11.215h-1.194l-.602-2.947h-5.752l-1.84 2.946zm2.493-3.993h4.926l-1.177-5.955-3.75 5.955zM128.1 30.86h5.257l-.158 1h-6.388l1.987-11.216h1.126"/><path fill="#010101" d="M206.432 19.693c2.826-.432 6.475.43 8.252 3.34l13.168-8.007c-4.816-7.874-14.143-12.007-23.768-10.53-9.895 1.52-17.598 8.74-20.098 18.84-.998 4.03-1.834 8.14-2.598 12.24h-28.615c.596-2.902 1.22-5.775 1.91-8.554 1.385-5.59 5.277-6.992 7.473-7.33 2.826-.43 6.475.43 8.252 3.34l13.168-8.006c-4.816-7.874-14.143-12.007-23.768-10.53-9.895 1.52-17.596 8.74-20.098 18.842-.996 4.023-1.86 8.14-2.662 12.24H94.634c-14.664 0-26.596 11.895-26.596 26.516 0 14.62 11.932 26.516 26.596 26.516s26.594-11.896 26.594-26.516c0-4.56-1.16-8.854-3.2-12.606h16.368c-5.484 27.887-11.51 45.205-35.625 46.643l-3.27 15.474c19.36-.02 33.115-7.25 42.045-22.095 6.824-11.34 9.87-25.775 12.562-40.022h28.83c-2.2 12.528-4.607 24.155-9.76 32.625-4.86 7.988-11.79 12.354-21.988 13.68l-3.323 15.712c17.672-.9 30.29-7.928 38.496-21.417 6.973-11.464 9.734-26.182 12.23-40.6h16.868l2.942-13.91h-17.314c.57-2.906 1.184-5.777 1.87-8.555 1.384-5.592 5.276-6.994 7.474-7.33M94.635 74.698c-6.97 0-12.643-5.654-12.643-12.604 0-6.952 5.672-12.606 12.643-12.606s12.643 5.654 12.643 12.606c0 6.95-5.673 12.604-12.643 12.604M87.754 163.215c10.598 0 19.246-8.65 19.246-19.247s-8.648-19.247-19.246-19.247c-10.596 0-19.248 8.65-19.248 19.248s8.652 19.247 19.248 19.247m-6.83-25.506c0-1.572.787-2.49 1.928-2.5h.037c.464.005.983.158 1.534.477l10.822 6.26c1.926 1.113 1.926 2.934 0 4.047l-10.822 6.256c-.553.322-1.076.474-1.54.476h-.028c-1.145-.006-1.932-.92-1.932-2.497v-12.52z"/><path fill="#99B1C8" d="M115.23 163.183V124.69h14.46c1.85 0 3.5.3 4.944.893 1.447.598 2.662 1.415 3.645 2.453.98 1.04 1.733 2.242 2.255 3.607.52 1.367.78 2.8.78 4.3s-.26 2.935-.78 4.3c-.52 1.366-1.283 2.567-2.287 3.607-1.002 1.038-2.217 1.857-3.643 2.45-1.43.6-3.068.897-4.92.897H118.52v15.985h-3.29zm14.082-18.987c1.268 0 2.424-.202 3.46-.606 1.04-.405 1.937-.97 2.685-1.702.75-.73 1.328-1.597 1.732-2.598.403-1 .604-2.115.604-3.347 0-1.193-.2-2.298-.605-3.32-.405-1.02-.983-1.893-1.733-2.624-.748-.73-1.645-1.3-2.684-1.704-1.037-.403-2.193-.604-3.46-.604H118.52v16.505h10.792zM185.045 124.69h-4.105l-14.55 35.552h-18.474V124.69h-3.295v38.56h24.355l3.816-9.483h20.405l3.814 9.482h3.813l-15.778-38.56zm-11.156 26.07l9.073-22.543 9.133 22.544H173.89zM208.77 163.243v-16.475l-15.32-22.083h3.988l12.978 18.96 12.98-18.96h3.987l-15.32 22.083v16.475"/></symbol><symbol id="globosatplay__premiere_play" viewBox="0 0 295.889 167.449"><path fill="#2E8F44" d="M98.773 56.455v-3.06c0-.416-.342-.76-.76-.76h-13.42c-.42 0-.758.344-.758.76v3.878c.245-.04.5-.06.76-.06l13.42.004c.42-.005.758-.342.758-.762M214.036 56.455v-3.06c0-.416-.342-.76-.76-.76h-13.42c-.42 0-.76.344-.76.76v3.878c.248-.04.503-.06.76-.06l13.42.004c.42-.005.76-.342.76-.762M70.845 52.625H57.297c-.416 0-.76.34-.76.755v3.86c.25-.04.503-.06.76-.06h13.548c.42 0 .756-.34.758-.754V53.38c-.002-.415-.34-.755-.758-.755"/><path fill="#2E8F44" d="M251.837 40.888H44.05c-1.77 0-3.207 1.434-3.207 3.205v27.544c0 1.773 1.436 3.207 3.207 3.207h207.787c1.775 0 3.21-1.434 3.21-3.207V44.093c0-1.77-1.435-3.205-3.21-3.205M75.407 56.426c-.003 2.62-2.127 4.748-4.75 4.748h-13.36c-.42 0-.76.337-.76.75v5.178H52.55v-1.59h-.003V53.38c.003-2.622 2.124-4.75 4.75-4.75h13.36c2.625 0 4.747 2.126 4.75 4.75v3.046zm22.378 10.682l-5.89-5.882-7.3-.003c-.41.003-.745.318-.76.723V67.104h-4.013V53.397c.002-2.634 2.133-4.768 4.77-4.77h13.42c2.638.002 4.77 2.136 4.77 4.77v3.06c0 2.64-2.136 4.768-4.77 4.77h-.445l5.92 5.882h-5.702zm30.294-14.63h-15.745c-.504 0-.77.306-.77.663v2.462l16.514.02v3.877l-16.514.016v2.946c0 .41.396.647.754.647h15.76v3.972h-15.823c-3.267 0-5.05-2.344-5.05-4.653v-8.983c0-1.992 1.575-4.79 4.743-4.79h16.13v3.82zm28.552 14.637h-4.176v-13.15l-5.943 10.93h-3.605l-6.237-10.978v13.197h-4.174V48.64h5.108l6.958 12.134 7.097-12.15h4.973v18.49zm8.75.05h-4.334V48.57h4.334v18.594zm25.29-14.687h-15.744c-.502 0-.77.306-.77.663v2.462l16.513.02v3.877l-16.512.016v2.946c0 .41.395.647.754.647h15.76v3.972h-15.82c-3.27 0-5.054-2.344-5.054-4.653v-8.983c0-1.992 1.574-4.79 4.745-4.79h16.128v3.82zm22.376 14.63l-5.89-5.882-7.3-.003c-.412.003-.743.318-.762.723V67.104h-4.01V53.397c.002-2.634 2.133-4.768 4.77-4.77h13.42c2.636.002 4.768 2.136 4.77 4.77v3.06c-.002 2.64-2.136 4.768-4.77 4.77h-.446l5.918 5.882h-5.7zm30.293-14.63H227.6c-.505 0-.77.306-.77.663v2.462l16.513.02v3.877l-16.513.016v2.946c0 .41.396.647.756.647h15.758v3.972h-15.82c-3.267 0-5.053-2.344-5.053-4.653v-8.983c0-1.992 1.577-4.79 4.747-4.79h16.127v3.82zM87.76 126.53c10.597 0 19.245-8.65 19.245-19.248 0-10.6-8.648-19.246-19.244-19.246-10.597 0-19.25 8.647-19.25 19.246 0 10.597 8.653 19.247 19.25 19.247m-6.83-25.507c0-1.57.786-2.49 1.927-2.498h.037c.465.004.986.156 1.535.475l10.82 6.26c1.93 1.114 1.93 2.935 0 4.048l-10.82 6.257c-.553.322-1.077.473-1.54.476h-.03c-1.143-.007-1.93-.92-1.93-2.497v-12.52z"/><path fill="#99B1C8" d="M115.235 126.497V88.002h14.46c1.85 0 3.5.298 4.946.895 1.446.597 2.66 1.413 3.644 2.45.982 1.042 1.734 2.243 2.257 3.61.522 1.365.78 2.798.78 4.3 0 1.5-.26 2.934-.78 4.298-.52 1.366-1.283 2.568-2.285 3.606-1.004 1.04-2.22 1.858-3.645 2.455-1.43.597-3.067.895-4.92.895h-11.164v15.986h-3.29zm14.082-18.99c1.27 0 2.424-.2 3.462-.604 1.04-.405 1.934-.972 2.684-1.702.75-.73 1.328-1.597 1.732-2.596.402-1 .604-2.117.604-3.347 0-1.193-.2-2.3-.604-3.32-.404-1.02-.982-1.895-1.732-2.626s-1.645-1.297-2.685-1.702c-1.04-.402-2.193-.605-3.463-.605h-10.79v16.506h10.79zM185.05 88.002h-4.104l-14.55 35.554h-18.475V88.002h-3.294v38.56h24.354l3.816-9.48H193.2l3.814 9.48h3.814L185.05 88zm-11.155 26.072l9.074-22.545 9.13 22.544h-18.205zM208.774 126.556V110.08l-15.32-22.083h3.988l12.98 18.96 12.977-18.96h3.988l-15.32 22.084v16.476"/></symbol><symbol id="globosatplay__sportv_play" viewBox="0 0 295.89 167.449"><path fill="#010101" d="M172.26 57.55v-3.12c0-.423-.346-.77-.773-.77h-13.664c-.43 0-.775.347-.775.77v3.947c.252-.04.51-.058.775-.058h13.664c.427 0 .773-.346.773-.77M138.236 53.667h-13.71c-.413 0-.75.34-.75.758v9.124c0 .41.337.75.75.75h13.71c.414 0 .752-.34.752-.75v-9.125c0-.418-.34-.758-.752-.758M106.328 53.662H92.664c-.428 0-.773.346-.773.77v3.946c.253-.04.513-.058.774-.058h13.664c.428 0 .77-.348.775-.77v-3.12c-.003-.423-.35-.768-.775-.768"/><path fill="#205EAB" d="M86.965 129.52c10.596 0 19.244-8.648 19.244-19.245 0-10.6-8.65-19.247-19.245-19.247-10.598 0-19.25 8.648-19.25 19.247 0 10.597 8.652 19.246 19.25 19.246m-6.83-25.505c0-1.572.785-2.49 1.926-2.498h.038c.465.004.986.157 1.535.475l10.822 6.26c1.928 1.114 1.928 2.935 0 4.05l-10.822 6.255c-.55.32-1.076.473-1.54.475h-.028c-1.143-.005-1.93-.922-1.93-2.497v-12.52z"/><path fill="#99B1C8" d="M114.44 129.49V90.994h14.46c1.85 0 3.498.298 4.945.893 1.445.597 2.66 1.415 3.643 2.454.982 1.04 1.734 2.24 2.258 3.607.52 1.366.78 2.797.78 4.3 0 1.5-.26 2.934-.78 4.298-.523 1.367-1.285 2.568-2.287 3.608-1.005 1.038-2.22 1.856-3.646 2.453-1.428.597-3.066.895-4.918.895H117.73v15.985h-3.29zm14.08-18.99c1.27 0 2.425-.2 3.464-.603 1.04-.405 1.934-.972 2.684-1.703.75-.73 1.328-1.597 1.732-2.597.402-1 .604-2.116.604-3.348 0-1.193-.2-2.3-.604-3.32-.404-1.02-.982-1.893-1.732-2.625-.75-.73-1.645-1.298-2.684-1.703-1.04-.403-2.193-.604-3.463-.604h-10.79v16.505h10.79zM184.254 90.995h-4.104l-14.55 35.552h-18.476V90.995h-3.295v38.56h24.353l3.816-9.482h20.403l3.814 9.48h3.814l-15.776-38.558zm-11.154 26.07l9.074-22.544 9.13 22.546H173.1zM207.98 129.548v-16.476L192.657 90.99h3.988l12.98 18.96 12.978-18.96h3.988l-15.32 22.082v16.476"/><path fill="#205EAB" d="M255.355 41.225c0-1.84-1.566-3.326-3.514-3.326H44.046c-1.938 0-3.512 1.484-3.512 3.325v31.782c0 1.84 1.574 3.33 3.512 3.33h207.797c1.947 0 3.514-1.488 3.514-3.33V41.225z"/><path fill="#C12D3F" d="M251.85 37.895h-57.307v38.44h57.307c1.938 0 3.506-1.485 3.506-3.33v-31.78c0-1.845-1.57-3.33-3.506-3.33"/><path fill="#FFF" d="M107.783 57.527c-.002.396-.346.73-.775.73H93.3c-.26 0-.52.016-.772.054v-3.75c0-.402.346-.732.773-.732h13.708c.43 0 .773.33.775.732v2.967zm-.775-7.585H93.3c-2.69.006-4.866 2.07-4.868 4.617v11.796h.002v1.484h4.094v-4.972c0-.402.342-.734.773-.734h13.708c2.69 0 4.865-2.063 4.87-4.608l.003-2.967c-.007-2.546-2.18-4.61-4.872-4.618M61.383 51.164H75.13c.43 0 .772.334.776.737h4.105c0-2.553-2.187-4.626-4.88-4.626H61.383c-2.695 0-4.88 2.073-4.88 4.627v2.97c0 2.555 2.185 4.626 4.88 4.626H75.13c.43 0 .772.33.776.74v2.962c-.004.408-.348.74-.775.74H61.384c-.43 0-.773-.332-.773-.74h-4.107c0 2.558 2.186 4.627 4.88 4.627h13.75c2.692 0 4.88-2.07 4.88-4.627v-2.962c0-2.564-2.19-4.628-4.88-4.628h-13.75c-.43 0-.773-.333-.773-.738l-.004-2.968c.003-.404.347-.738.777-.738M158.65 58.257c-.268 0-.527.016-.777.054v-3.75c0-.402.348-.73.777-.73h13.7c.433 0 .774.328.774.73v2.967c0 .4-.342.73-.773.73h-13.7zm13.702 3.878c2.69 0 4.87-2.063 4.875-4.608V54.56c-.004-2.546-2.178-4.61-4.875-4.61l-13.7-.007c-2.7.006-4.874 2.07-4.874 4.617v13.272h4.096V62.84c.016-.396.357-.707.777-.707h7.458l6.01 5.697h5.824l-6.045-5.697h.455zM139.764 63.223c0 .394-.342.717-.762.717H125.26c-.418 0-.756-.323-.756-.717v-8.67c0-.396.338-.716.756-.716h13.742c.416 0 .762.32.762.716v8.67zm-.67-13.28h-13.93c-2.65.005-4.795 2.038-4.8 4.55v8.787c.005 2.512 2.15 4.552 4.8 4.552h13.93c2.652 0 4.797-2.04 4.803-4.552v-8.787c-.006-2.512-2.145-4.544-4.803-4.55M237.844 47.29l-9.817 14.825-9.806-14.826h-5.064l13.098 20.542h3.564l13.16-20.543M182.666 51.155h9.625v16.677h4.093V51.155h9.627v-3.872h-23.344"/></symbol><symbol id="globosatplay__syfy_play" viewBox="0 0 295.891 167.449"><path fill="#42145F" d="M87.756 151.34c10.598 0 19.244-8.65 19.244-19.248s-8.646-19.246-19.244-19.246-19.25 8.647-19.25 19.246 8.652 19.248 19.25 19.248m-6.83-25.507c0-1.57.787-2.49 1.928-2.498h.037c.464.004.985.156 1.536.475l10.822 6.26c1.928 1.114 1.928 2.935 0 4.048l-10.822 6.256c-.553.32-1.078.473-1.54.476h-.03c-1.14-.008-1.93-.923-1.93-2.5v-12.517z"/><g fill="#99B1C8"><path d="M115.232 151.308v-38.494h14.46c1.85 0 3.497.298 4.944.894 1.447.598 2.662 1.414 3.645 2.452.983 1.04 1.735 2.242 2.255 3.608.52 1.367.783 2.8.783 4.3s-.262 2.934-.783 4.3c-.52 1.365-1.28 2.567-2.283 3.605-1.004 1.04-2.22 1.857-3.646 2.453-1.428.6-3.066.895-4.918.895h-11.166v15.988h-3.29zm14.082-18.99c1.27 0 2.424-.2 3.463-.604 1.037-.405 1.932-.97 2.684-1.702.75-.73 1.327-1.597 1.73-2.597.405-1 .606-2.116.606-3.348 0-1.192-.2-2.298-.605-3.32-.403-1.018-.98-1.894-1.73-2.625-.75-.73-1.645-1.298-2.683-1.703-1.04-.404-2.193-.605-3.463-.605H118.52v16.505h10.794zM185.047 112.813h-4.105l-14.55 35.553H147.92v-35.553h-3.295v38.56h24.354l3.814-9.482h20.407l3.814 9.483h3.815l-15.778-38.56zm-11.154 26.072l9.072-22.545 9.133 22.545h-18.205zM208.77 151.367v-16.475l-15.32-22.083h3.99l12.978 18.96 12.978-18.96h3.99l-15.32 22.082v16.475"/></g><g fill="#3D2661"><path d="M185.246 38.926H171.87V37.46c-.005-.886.032-1.842.19-2.706.11-.64.285-1.2.49-1.625.336-.647.57-.968 1.3-1.413.733-.426 2.23-.998 5.21-1.002h9.125v-14.64h-9.125c-4.336 0-8.145.77-11.377 2.35-2.412 1.176-4.445 2.82-5.953 4.645-2.28 2.753-3.373 5.76-3.918 8.288-.543 2.537-.578 4.712-.58 6.103l.002 39.56h14.64l-.005-24.248h13.376V38.926zM207.684 99.98c12.875-.01 19.676-9.424 19.7-18.274v-42.78h-13.757v21.302c0 .973-.123 2.302-1.174 3.452-1.027 1.13-2.365 1.994-4.566 1.983-2.293.015-3.922-.836-4.748-1.998-.83-1.152-.99-2.474-.993-3.438v-21.3h-13.96V60.53c.015 9.007 6.677 18.257 17.872 18.28 2.183 0 4.35-.45 6.458-1.288l1.018-.404-.004 1.094-.004 3.136c0 .655-.014 1.566-.707 2.78-.68 1.225-2.19 2.42-4.728 2.4-2.29.01-3.764-1.002-4.543-2.04-.796-1.033-.974-2.043-1-2.185l-14.118-.057.285 2.25c1.633 7.725 8.67 15.5 18.967 15.483M134.562 99.98c12.877-.01 19.678-9.424 19.7-18.274v-42.78h-13.755v21.302c0 .973-.066 2.27-1.176 3.452-1.046 1.12-2.366 1.994-4.565 1.983-2.295.015-3.922-.836-4.748-1.998-.832-1.152-.988-2.474-.992-3.438v-21.3h-13.96V60.53c.015 9.007 6.677 18.257 17.87 18.28 2.184 0 4.348-.45 6.46-1.288l1.017-.404v1.094l-.006 3.136c0 .645-.012 1.566-.71 2.78-.68 1.225-2.19 2.42-4.728 2.4-2.29.01-3.764-1.002-4.543-2.04-.79-1.033-.973-2.043-1-2.185l-14.115-.057.28 2.25c1.64 7.725 8.674 15.5 18.972 15.483M71.736 59.12s.01-1.202.057-1.91l13.826.036-.042.846c-.014 2.472 1.49 4.827 2.812 5.796 1.316.977 3.207 1.293 4.357 1.293 2.96-.002 5.66-1.808 5.695-4.587l-.006-.255c-.094-2.168-1.467-3.355-3.432-4.274-1.762-.826-4.78-2.105-8.05-3.726-3.274-1.623-6.81-3.592-9.6-5.794-4.026-3.19-5.84-7.3-5.62-13.015.188-4.87 2.307-9.06 6.26-12.532 3.957-3.474 8.832-4.86 14-4.858 6.566 0 12.053 2.648 15.682 6.964 3.676 4.522 4.385 8.806 4.54 11.96l.028.604H97.9c-.146-1.476-.844-3.243-1.682-4.203-.986-1.14-2.506-1.88-4.408-1.876-1.098.007-2.383.33-3.465 1.016-1.068.672-1.953 1.784-1.945 3.25-.018 1.582.723 2.817 2.756 3.85 1.998 1.036 8.023 3.7 8.87 4.027 8.597 3.35 14.03 8.77 14.647 16.955.014.178.016.368.016.57.01 2.767-.82 7.555-3.99 11.732-3.18 4.18-8.68 7.83-15.888 7.83-14.25-.05-21.042-10.4-21.076-19.7"/></g></symbol><symbol id="globosatplay__telecine_play" viewBox="0 0 295.891 167.449"><path fill="#E53A33" d="M234.4 109.892H61.49V51.95H234.4v57.942zm-139-22.92c-1 .696-2.27 1.052-3.84 1.052-2.162 0-3.88-.63-5.143-1.88-1.3-1.242-1.967-2.936-1.967-5.085 0-2.147.6-3.884 1.816-5.175 1.164-1.297 2.867-1.944 5.145-1.944 1.806 0 3.073.688 3.83 1.17.858.504 1.478 1.307 1.81 1.658l9.722-6.68c-1.076-1.566-2.902-3.624-4.55-4.846-2.82-2.176-6.173-3.278-10.73-3.304-5.902.072-10.553 1.9-13.975 5.51-3.48 3.614-5.246 8.125-5.287 13.558.04 5.715 1.807 10.258 5.334 13.648 3.44 3.444 8.025 5.18 13.738 5.272 4.475-.092 8.174-1.3 11.156-3.674 1.662-1.312 3.04-2.758 4.197-4.33l-9.553-6.77c-.34.362-.666 1.118-1.702 1.82m37.002-24.09h-11.846c-.066.03-.094.185-.094.445V98.28c0 .243.027.383.094.435.043.026.176.03.434.03h11.412c.297 0 .46-.03.508-.092.033-.04.033-.15.033-.354V63.36c.055-.358-.14-.514-.54-.48m48.398.2c-.026-.062-.194-.092-.5-.092h-9.843c-.242 0-.38.014-.42.062-.033.056-.045.25-.045.568v13.648c-1.63-1.177-8.943-6.196-21.965-15.073-.205-.17-.346-.23-.453-.15-.082.01-.117.173-.117.447-.146 12.188-.146 23.683 0 35.848-.04.303.15.474.57.53h9.727c.27 0 .426-.057.453-.122.033-.03.062-.145.062-.357V84.665c1.45 1.126 8.767 6.132 21.923 15 .305.15.494.204.566.16.048-.073.087-.317.087-.704.156-13.8.156-25.19 0-35.504 0-.248-.013-.416-.046-.54m42.182 26.93c-.05-.035-.225-.057-.54-.057h-14.808v-4.367h14.068c.46.03.684-.2.656-.733v-7.938c0-.348-.02-.548-.068-.668-.074-.08-.256-.123-.588-.123h-14.068V71.56h14.332c.44.06.65-.223.635-.76v-7.03c0-.353-.024-.592-.065-.7-.062-.017-.264-.048-.695-.048h-25.818c-.17 0-.34.03-.36.048-.08.04-.1.2-.1.5v34.466c0 .306.03.513.14.62.088.105.26.16.502.16h26.238c.256 0 .432-.055.54-.16.032-.107.046-.314.046-.62V90.69c0-.382-.014-.594-.046-.68"/><path fill="#010101" d="M141.854 32.34h-14.72v-4.422h13.882c.453.048.676-.212.637-.712v-7.834c0-.312-.027-.537-.04-.638-.08-.09-.278-.135-.597-.135h-13.88v-4.527H141.4c.426.04.63-.21.588-.728V6.228c0-.33 0-.578-.04-.683-.075-.02-.25-.05-.548-.05h-25.818c-.29 0-.45.028-.488.05-.072.035-.092.205-.092.48V40.47c0 .295.05.507.148.6.072.103.242.164.48.164h26.223c.258 0 .42-.062.527-.165.05-.092.055-.304.055-.6V33.06c0-.37-.006-.59-.055-.673-.04-.022-.21-.047-.526-.047M222.182 32.34H207.48v-4.422h13.88c.445.048.674-.212.64-.712v-7.834c0-.312-.024-.537-.06-.638-.074-.09-.264-.135-.58-.135h-13.88v-4.527h14.27c.426.04.643-.21.61-.728V6.228c0-.33-.028-.578-.058-.683-.08-.02-.254-.05-.553-.05h-25.827c-.277 0-.438.028-.486.05-.06.035-.102.205-.102.48V40.47c0 .295.06.507.15.6.086.103.264.164.494.164h26.2c.27 0 .45-.062.54-.165.034-.092.063-.304.063-.6V33.06c0-.37-.03-.59-.062-.673-.023-.022-.206-.047-.538-.047M103.717 5.43H72.99c-.305 0-.44.168-.398.52v8.967c-.04.303.08.43.354.398h9.44v25.492c0 .242.03.386.097.42.04.035.176.053.418.053h11.26c.255 0 .425-.044.446-.108.033-.035.06-.16.06-.364V15.315h9.12c.242.03.357-.096.357-.398V5.95c0-.352-.135-.52-.426-.52M181.436 31.432h-12.682V5.958c.066-.36-.115-.512-.512-.48h-11.666c-.06.038-.1.193-.1.46v34.826c0 .233.04.382.1.408.03.044.178.06.428.06h24.492c.27 0 .436-.03.467-.103.043-.033.06-.154.06-.366v-8.78c0-.363-.207-.552-.587-.552"/><path fill="#99B1C8" d="M115.23 161.956v-38.493h14.46c1.85 0 3.497.298 4.944.894 1.445.598 2.66 1.414 3.645 2.452.98 1.04 1.733 2.24 2.255 3.607.52 1.365.78 2.8.78 4.3s-.26 2.934-.78 4.298c-.52 1.367-1.283 2.568-2.285 3.606-1.004 1.04-2.22 1.86-3.646 2.455-1.428.598-3.066.896-4.916.896H118.52v15.986h-3.29zm14.082-18.987c1.27 0 2.424-.203 3.463-.607 1.037-.404 1.932-.972 2.684-1.7.75-.732 1.325-1.6 1.73-2.6s.604-2.116.604-3.346c0-1.193-.2-2.3-.605-3.32-.405-1.02-.98-1.894-1.73-2.625-.753-.73-1.647-1.298-2.685-1.703-1.04-.404-2.193-.606-3.463-.606H118.52v16.506h10.792zM185.043 123.463h-4.104l-14.55 35.553h-18.475v-35.553h-3.295v38.558h24.354l3.816-9.48h20.404l3.812 9.48h3.816l-15.78-38.557zm-11.154 26.07l9.073-22.545 9.133 22.546H173.89zM208.768 162.016V145.54l-15.32-22.084h3.99l12.978 18.962 12.98-18.962h3.985l-15.318 22.085v16.476"/><path fill="#010101" d="M141.854 32.34h-14.72v-4.422h13.882c.453.048.676-.212.637-.712v-7.834c0-.312-.027-.537-.04-.638-.08-.09-.278-.135-.597-.135h-13.88v-4.527H141.4c.426.04.63-.21.588-.728V6.228c0-.33 0-.578-.04-.683-.075-.02-.25-.05-.548-.05h-25.818c-.29 0-.45.028-.488.05-.072.035-.092.205-.092.48V40.47c0 .295.05.507.148.6.072.103.242.164.48.164h26.223c.258 0 .42-.062.527-.165.05-.092.055-.304.055-.6V33.06c0-.37-.006-.59-.055-.673-.04-.022-.21-.047-.526-.047M222.182 32.34H207.48v-4.422h13.88c.445.048.674-.212.64-.712v-7.834c0-.312-.024-.537-.06-.638-.074-.09-.264-.135-.58-.135h-13.88v-4.527h14.27c.426.04.643-.21.61-.728V6.228c0-.33-.028-.578-.058-.683-.08-.02-.254-.05-.553-.05h-25.827c-.277 0-.438.028-.486.05-.06.035-.102.205-.102.48V40.47c0 .295.06.507.15.6.086.103.264.164.494.164h26.2c.27 0 .45-.062.54-.165.034-.092.063-.304.063-.6V33.06c0-.37-.03-.59-.062-.673-.023-.022-.206-.047-.538-.047M103.717 5.43H72.99c-.305 0-.44.168-.398.52v8.967c-.04.303.08.43.354.398h9.44v25.492c0 .242.03.386.097.42.04.035.176.053.418.053h11.26c.255 0 .425-.044.446-.108.033-.035.06-.16.06-.364V15.315h9.12c.242.03.357-.096.357-.398V5.95c0-.352-.135-.52-.426-.52M181.436 31.432h-12.682V5.958c.066-.36-.115-.512-.512-.48h-11.666c-.06.038-.1.193-.1.46v34.826c0 .233.04.382.1.408.03.044.178.06.428.06h24.492c.27 0 .436-.03.467-.103.043-.033.06-.154.06-.366v-8.78c0-.363-.207-.552-.587-.552"/><path fill="#99B1C8" d="M115.23 161.956v-38.493h14.46c1.85 0 3.497.298 4.944.894 1.445.598 2.66 1.414 3.645 2.452.98 1.04 1.733 2.24 2.255 3.607.52 1.365.78 2.8.78 4.3s-.26 2.934-.78 4.298c-.52 1.367-1.283 2.568-2.285 3.606-1.004 1.04-2.22 1.86-3.646 2.455-1.428.598-3.066.896-4.916.896H118.52v15.986h-3.29zm14.082-18.987c1.27 0 2.424-.203 3.463-.607 1.037-.404 1.932-.972 2.684-1.7.75-.732 1.325-1.6 1.73-2.6s.604-2.116.604-3.346c0-1.193-.2-2.3-.605-3.32-.405-1.02-.98-1.894-1.73-2.625-.753-.73-1.647-1.298-2.685-1.703-1.04-.404-2.193-.606-3.463-.606H118.52v16.506h10.792zM185.043 123.463h-4.104l-14.55 35.553h-18.475v-35.553h-3.295v38.558h24.354l3.816-9.48h20.404l3.812 9.48h3.816l-15.78-38.557zm-11.154 26.07l9.073-22.545 9.133 22.546H173.89zM208.768 162.016V145.54l-15.32-22.084h3.99l12.978 18.962 12.98-18.962h3.985l-15.318 22.085v16.476"/><path fill="#E53A33" d="M87.756 161.99c10.596 0 19.242-8.65 19.242-19.25 0-10.598-8.646-19.245-19.242-19.245-10.6 0-19.25 8.647-19.25 19.246 0 10.6 8.65 19.25 19.25 19.25m-6.832-25.508c0-1.57.787-2.488 1.928-2.498h.037c.462.004.985.157 1.534.475l10.822 6.258c1.928 1.115 1.928 2.936 0 4.05l-10.822 6.257c-.553.322-1.076.473-1.54.476h-.028c-1.143-.008-1.932-.922-1.932-2.498v-12.52z"/></symbol><symbol id="globosatplay__universal_play" viewBox="0 0 295.89 167.449"><path fill="#531C63" d="M98.925 43.127h-8.43C96.92 18.26 119.353 0 146.427 0c26.846 0 49.416 18.313 55.902 43.127h-8.405c-6.254-20.296-25.154-35.04-47.498-35.04-22.346 0-41.242 14.744-47.502 35.04m47.502 64.353c-23.297 0-42.852-16.034-48.23-37.667h-8.296c1.964 9.278 6.155 17.74 11.99 24.784l7.95-.56-.84 7.77c10.082 8.576 23.148 13.757 37.426 13.757 27.787 0 50.988-19.61 56.525-45.75h-8.298c-5.378 21.632-24.926 37.666-48.225 37.666"/><path fill="#531C63" d="M106.415 75.304v-.03c0-3 2.262-5.46 5.504-5.46 1.987 0 3.185.66 4.16 1.63l-1.474 1.702c-.816-.736-1.645-1.19-2.703-1.19-1.777 0-3.062 1.48-3.062 3.29v.028c0 1.81 1.255 3.32 3.063 3.32 1.21 0 1.947-.483 2.777-1.236l1.48 1.492c-1.09 1.16-2.297 1.883-4.334 1.883-3.104 0-5.41-2.395-5.41-5.43M118.565 69.994h2.32v4.185h4.282v-4.186h2.326v10.56h-2.326v-4.235h-4.283v4.233h-2.32M134.118 69.92h2.145l4.52 10.633h-2.424l-.966-2.37h-4.463l-.964 2.37H129.6l4.518-10.632zm2.444 6.214l-1.4-3.423-1.404 3.424h2.804zM142.655 69.994h2.143l4.947 6.502v-6.502h2.29v10.56h-1.98l-5.105-6.71v6.71h-2.295M155.288 69.994h2.14l4.948 6.502v-6.502h2.295v10.56h-1.976l-5.113-6.71v6.71h-2.292M167.917 69.994h7.96v2.068h-5.653v2.14h4.976v2.066h-4.976v2.216h5.732v2.07h-8.04M178.987 69.994h2.32v8.45h5.26v2.11h-7.58M79.49 57.725c0 .68-.09 1.268-.25 1.77-.507 1.56-1.735 2.302-3.42 2.302-1.067 0-1.954-.32-2.587-.975-.672-.696-1.06-1.767-1.06-3.236v-10.99H66.63v11.103c0 1.803.308 3.328.874 4.576 1.283 2.833 3.922 4.267 7.488 4.443h1.543c4.936-.242 8.17-2.845 8.47-8.33.01-.27.022-.548.022-.835v-10.96h-5.54v11.13zM100.55 56.18v.87l-.613-.8-7.526-9.655h-5.114v19.779h5.426V55.523l1.19 1.52 7.284 9.33h4.776V46.596h-5.422M108.46 55.423v10.95h5.48V46.596h-5.48M127.552 54.222l-1.768 4.89-1.7-4.748-2.79-7.77h-6.104l3.243 8.08 4.754 11.836h5.087l5.018-12.46 3.002-7.455h-5.986M143.187 58.576h9.49V54.25h-9.49v-3.023h10.478v-4.632H137.76v19.778h16.05V61.71h-10.623M173.304 55.044c.117-.515.178-1.074.178-1.673v-.05c0-1.95-.594-3.45-1.754-4.61-1.322-1.324-3.416-2.115-6.44-2.115h-9.35v19.78h5.48V60.38h2.43l3.983 5.994h6.302l-4.72-6.9c1.978-.836 3.405-2.302 3.892-4.43m-8.24 1.042h-3.647v-4.774h3.62c1.806 0 2.966.79 2.966 2.372v.06c0 .347-.066.665-.19.945-.388.874-1.36 1.396-2.748 1.396M191.292 60.213c0-1.615-.537-2.8-1.516-3.71-1.27-1.18-3.283-1.895-5.834-2.45-2.77-.622-3.447-1.017-3.447-1.92v-.056c0-.74.652-1.272 1.98-1.272 1.753 0 3.73.65 5.536 1.946l2.745-3.867c-2.15-1.723-4.77-2.63-8.14-2.63-4.747 0-7.657 2.66-7.657 6.326v.06c0 1.017.205 1.85.576 2.545 1.113 2.076 3.73 2.925 6.883 3.645 2.717.646 3.363 1.07 3.363 1.92v.057c0 .848-.795 1.36-2.29 1.36-2.286 0-4.43-.823-6.413-2.377l-3.053 3.644c2.455 2.18 5.766 3.277 9.27 3.277 4.803 0 7.998-2.403 7.998-6.44v-.057zM204.41 46.455h-5.284l-4.45 10.558-3.942 9.36h5.732l1.41-3.563h7.658l1.416 3.564h5.877l-3.066-7.258-5.35-12.66zm-4.917 12.09l.35-.877 1.88-4.714 2.07 5.263.136.327h-4.437zM223.866 61.568h-4.185V46.595h-5.484v19.779h15.06v-4.806M87.608 167.417c10.615 0 19.283-8.668 19.283-19.286 0-10.618-8.667-19.285-19.282-19.285-10.62 0-19.29 8.667-19.29 19.286 0 10.62 8.67 19.287 19.29 19.287m-6.843-25.56c0-1.576.787-2.493 1.932-2.5h.035c.467.003.99.153 1.54.474l10.843 6.273c1.932 1.116 1.932 2.938 0 4.056l-10.844 6.267c-.552.324-1.08.477-1.544.48H82.7c-1.146-.008-1.935-.93-1.935-2.506V141.86z"/><path fill="#99B1C8" d="M115.138 167.382v-38.57h14.486c1.855 0 3.508.3 4.96.897 1.444.596 2.663 1.415 3.647 2.456.985 1.043 1.74 2.247 2.26 3.615.524 1.366.784 2.806.784 4.31 0 1.5-.26 2.94-.783 4.306-.52 1.368-1.286 2.573-2.286 3.615-1.006 1.042-2.227 1.858-3.656 2.456-1.43.6-3.072.9-4.926.9h-11.19v16.016h-3.294zm14.11-19.025c1.27 0 2.427-.2 3.466-.61 1.043-.4 1.94-.97 2.69-1.703.75-.73 1.333-1.6 1.735-2.6.403-1.002.604-2.123.604-3.355 0-1.198-.2-2.304-.605-3.328-.403-1.02-.985-1.898-1.735-2.628-.752-.73-1.648-1.304-2.69-1.706-1.04-.406-2.198-.607-3.468-.607h-10.814v16.538h10.814zM185.09 128.812h-4.112l-14.582 35.625h-18.512v-35.625h-3.297v38.636h24.4l3.824-9.5h20.444l3.82 9.5h3.824l-15.81-38.636zm-11.18 26.12l9.095-22.584 9.146 22.585h-18.24zM208.858 167.44v-16.504l-15.347-22.13h3.993l13.004 19 13.006-19h3.998l-15.35 22.13v16.505"/></symbol><symbol id="globosatplay__viva_play" viewBox="0 0 295.891 167.449"><path fill="#F79522" d="M147.947.19C116.093.19 90.27 26.008 90.27 57.86s25.823 57.672 57.677 57.672c31.85 0 57.668-25.82 57.668-57.673 0-31.852-25.818-57.67-57.668-57.67M116.86 74.61h-6.51c-1.267-3.695-5.185-14.42-7.597-20.987-1.08-2.938-3.26-9.54-6.42-12.67 6.828.53 9.19 2.208 11.578 7.77 1.526 3.544 3.864 10.196 5.753 15.535l7.387-23.395h6.924L116.86 74.61zm21.97 0h-6.65V40.863h6.65V74.61zm16.457 0l-12.22-33.747h6.827l8.465 23.928 7.593-23.927h6.75L161.756 74.61h-6.47zm23.547-10.277c.4-.002 5.822-1.483 10.686.857l-6.236-15.244c-3.12 9.912-7.58 24.664-7.58 24.664h-6.695l10.753-33.747h6.508l13.286 33.747h-7.11c-2.766-5.237-7.38-8.826-13.612-10.277M87.756 167.227c10.598 0 19.244-8.65 19.244-19.248 0-10.6-8.646-19.247-19.244-19.247s-19.25 8.648-19.25 19.246c0 10.598 8.652 19.247 19.25 19.247m-6.832-25.507c0-1.57.79-2.49 1.928-2.498h.04c.462.004.983.157 1.534.476l10.82 6.26c1.928 1.113 1.928 2.934 0 4.048l-10.82 6.255c-.553.323-1.078.475-1.543.478h-.027c-1.143-.007-1.932-.923-1.932-2.498v-12.52z"/><path fill="#99B1C8" d="M115.23 167.194V128.7h14.46c1.85 0 3.5.298 4.944.894 1.447.598 2.662 1.414 3.645 2.452.983 1.04 1.733 2.242 2.255 3.61.52 1.364.78 2.797.78 4.298 0 1.5-.26 2.934-.78 4.3-.52 1.366-1.283 2.567-2.285 3.606-1.002 1.04-2.22 1.858-3.645 2.453-1.428.6-3.068.896-4.918.896H118.52v15.985h-3.29zm14.082-18.988c1.27 0 2.424-.202 3.463-.605 1.04-.403 1.934-.97 2.684-1.7s1.327-1.598 1.73-2.598c.405-1 .606-2.116.606-3.348 0-1.19-.2-2.3-.607-3.318-.403-1.02-.98-1.896-1.73-2.627-.75-.73-1.646-1.3-2.685-1.704-1.04-.403-2.193-.606-3.463-.606H118.52v16.506h10.792zM185.045 128.7h-4.104l-14.548 35.552h-18.477V128.7h-3.295v38.56h24.354l3.816-9.482h20.404l3.814 9.48h3.814L185.045 128.7zm-11.154 26.07l9.075-22.543 9.133 22.544H173.89zM208.77 167.254v-16.476l-15.32-22.083h3.988l12.98 18.96 12.977-18.96h3.99l-15.32 22.083v16.476"/></symbol></svg></div><header class="cabecalho"><h2 class="globosatplay__title"><a href="http://globosatplay.globo.com/" class="globosatplay__title-link"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="500" height="58" viewBox="0 0 500 58"><path fill-rule="evenodd" clip-rule="evenodd" fill="#98B0C7" d="M108.42 6.036h15.26c10.282 0 14.192 6.485 14.224 12.26.022 4.193-1.14 7.007-4.202 9.262 3.9 2.035 5.91 6.114 5.91 10.305 0 7.92-5.208 14.037-14.503 14.037h-16.69V6.036zm15.017 10.032l-3.3-.003v7.746h3.253c2.216 0 3.812-1.666 3.81-4.006-.005-2.423-2.043-3.735-3.763-3.736m.392 16.54h-3.692v9.155h3.958c2.52 0 4.443-2.236 4.443-4.545 0-2.505-1.936-4.61-4.71-4.61M82.026 16.695c6.758 0 12.27 5.515 12.27 12.272 0 6.757-5.512 12.273-12.27 12.273-6.76 0-12.272-5.517-12.272-12.273 0-6.757 5.513-12.272 12.272-12.272m0-11.928c13.326 0 24.2 10.874 24.2 24.2s-10.874 24.204-24.2 24.204c-13.328 0-24.205-10.875-24.205-24.202s10.877-24.2 24.205-24.2m83.038 11.927c6.757 0 12.27 5.515 12.27 12.272 0 6.757-5.514 12.273-12.27 12.273-6.758 0-12.274-5.517-12.274-12.273 0-6.757 5.518-12.272 12.275-12.272m0-11.928c13.325 0 24.2 10.874 24.2 24.2s-10.875 24.204-24.2 24.204c-13.327 0-24.206-10.875-24.206-24.202s10.878-24.2 24.205-24.2M51.417 6.035L39.764 25.4H23.027v10.035h11.408c-2.175 3.23-5.125 5.537-10.474 5.505-6.18-.035-12.07-4.104-12.07-12.655 0-7.258 6.262-12.128 11.824-12.125 4.25.004 9.437 1.27 12.948 7.096l6.26-10.362c-5.21-5.52-10.983-8.116-19.895-8.116C10.143 5.05.032 16.08 0 27.972 0 42.526 11.66 51.9 23.398 51.9h34.38V41.577H43.764l21.44-35.54H51.418z"/><path fill="#98B0C7" d="M207.336 53.162c-9.39 0-16.824-4.853-16.824-14.81V36.84h11.848c0 3.023 1.385 6.365 4.852 6.365 2.458 0 4.538-1.954 4.538-4.475 0-3.025-2.52-4.032-4.98-5.104-1.386-.63-2.77-1.197-4.158-1.766-6.048-2.52-11.405-6.048-11.405-13.36 0-8.693 8.38-13.734 16.32-13.734 4.538 0 9.644 1.7 12.604 5.292 2.397 2.96 2.963 5.546 3.09 9.2h-11.785c-.38-2.583-1.26-4.537-4.286-4.537-2.08 0-4.096 1.45-4.096 3.655 0 .694.063 1.388.44 1.955 1.134 1.89 7.248 4.285 9.2 5.167 6.177 2.834 10.902 6.05 10.902 13.36 0 9.768-7.06 14.305-16.26 14.305M286.824 16.108v35.79h-11.907V16.11h-9.203V6.036h30.25v10.072M247.565 6.053l-27.66 45.85h13.784l3.824-6.346h14.083v6.337h11.926V6.054h-15.958zm4.033 30.783h-8.816l8.816-14.615v14.616zM358.895 53.18V4.747h18.193c2.328 0 4.4.374 6.222 1.124 1.82.75 3.35 1.78 4.585 3.086 1.236 1.308 2.183 2.82 2.838 4.54s.983 3.52.983 5.408c0 1.888-.328 3.69-.983 5.41-.655 1.718-1.614 3.23-2.875 4.536-1.262 1.308-2.792 2.338-4.586 3.087-1.797.75-3.86 1.126-6.188 1.126h-14.05v20.112h-4.14zm17.718-23.89c1.597 0 3.05-.255 4.355-.763 1.307-.51 2.434-1.223 3.377-2.143.944-.92 1.67-2.01 2.18-3.268.507-1.258.76-2.662.76-4.21 0-1.5-.253-2.894-.76-4.177-.51-1.283-1.235-2.384-2.18-3.305-.943-.92-2.07-1.634-3.377-2.143-1.307-.508-2.76-.76-4.355-.76h-13.578v20.765h13.578zM446.732 4.748h-5.163l-18.307 44.73h-23.245V4.748h-4.146V53.26h30.642l4.8-11.928h25.672l4.8 11.928h4.8L446.732 4.748zM432.697 37.55l11.418-28.365 11.488 28.365h-22.906zM476.58 53.254v-20.73L457.307 4.74h5.018l16.33 23.857L494.98 4.74H500l-19.273 27.785v20.73"/><defs><path id="a" d="M324.327 4.747c-13.334 0-24.22 10.88-24.22 24.216s10.886 24.215 24.22 24.215c13.333 0 24.213-10.88 24.213-24.215S337.66 4.747 324.327 4.747zm9.425 26.764l-13.615 7.875c-.694.403-1.355.594-1.94.597h-.033c-1.438-.008-2.43-1.16-2.43-3.142V21.088c0-1.977.99-3.132 2.425-3.144h.046c.583.005 1.24.198 1.932.6l13.615 7.874c2.425 1.4 2.425 3.692 0 5.093z"/></defs><clipPath id="b"><use xlink:href="#a" overflow="visible"/></clipPath><linearGradient id="c" gradientUnits="userSpaceOnUse" x1="-281.142" y1="327.715" x2="-280.55" y2="327.715" gradientTransform="scale(81.6953) rotate(-45 257.577 -507.484)"><stop offset="0" stop-color="#7B61A6"/><stop offset=".061" stop-color="#8E5599"/><stop offset=".198" stop-color="#B53C80"/><stop offset=".324" stop-color="#D12B6E"/><stop offset=".435" stop-color="#E22063"/><stop offset=".518" stop-color="#E81C5F"/><stop offset=".587" stop-color="#E92859"/><stop offset=".714" stop-color="#ED484A"/><stop offset=".882" stop-color="#F37C31"/><stop offset="1" stop-color="#F7A51E"/></linearGradient><path clip-path="url(#b)" fill="url(#c)" d="M275.894 28.963l48.43-48.432 48.43 48.433-48.43 48.432"/></svg></a></h2><aside class="action analytics-area analytics-id-T"><a class="globosatplay__side-link" href="http://globosatplay.globo.com/conheca/">
        ConheÃ§a o GlobosatPlay
        <span class="arrow">âº</span></a></aside></header><div class="borda"><div class="rodape"></div></div><div class="globosatplay__logo-list-wrapper"><ul class="globosatplay__logo-list analytics-link-list"><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/gnt/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__gnt_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/sportv/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__sportv_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/multishow/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__multishow_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/globosat/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__globosat_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/bis/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__bis_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/viva/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__viva_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/canal-off/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__off_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/gloob/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__gloob_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/telecine/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__telecine_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/universal/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__universal_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/syfy/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__syfy_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/globonews/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__globonews_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/canal-brasil/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__brasil_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/megapix/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__megapix_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/combate/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__combate_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/premierefc/ao-vivo/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__premiere_play'></use></svg></a></li></ul></div></section><div class="separator "></div></div></section><section class="area globo-servicos container analytics-area analytics-id-C"><h2 class="titulo">serviÃ§os</h2><div class="borda"><span class="rodape"></span></div><div class="content"><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-loterias
" 
    data-lotteries-url="http://api.globo.com/lotteries/megasena.jsonp"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-fipe
"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-zap
" 
    data-zap-url="http://api.globo.com/zap/search/"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"></div></div></section><section class="area widgets_inferior franja-inferior container analytics-area analytics-id-C"><div class="box-links-servicos"><div class="links"><p>MAIS SERVIÃOS</p><a href="http://revistapegn.globo.com/Franquias/compare/">Abra uma franquia</a><a href="http://globoesporte.globo.com/eu-atleta/calculo-imc.html">AvaliaÃ§Ã£o fÃ­sica</a><a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">Conversor de moedas</a><a href="http://www.techtudo.com.br/softwares/">Downloads</a><a href="http://g1.globo.com/previsao-do-tempo/">PrevisÃ£o do tempo</a><a href="http://g1.globo.com/radar-g1/platb/">TrÃ¢nsito</a></div></div><div id="glb-shopping-widget" class="area globoshopping container" data-url="http://vitrines.globo.com/vitrine/vitrine.min.js" data-sitepage="globocom/home" data-responsive="0" data-branding-color="0669DE"></div></section><section class="area diretorio"><div class="container analytics-area analytics-id-Z"><div class="diretorio-responsive"><a href="/todos-os-sites.html">veja todos os sites da <strong>globo.com</strong> âº</a></div><div id="glb-diretorio"><div class="glb-conteudo"><div class="glb-bloco"><div class="glb-grid-12"><ul class="diretorio-indice"><li class="diretorio-linha diretorio-first "><ul><li class="diretorio-first-level analytics-product glb-menu-g1 diretorio-quebra "><a title="g1" href="http://g1.globo.com/">g1</a><ul><li class="diretorio-second-level"><a title="Acre" href="http://g1.globo.com/ac/acre/">Acre</a></li><li class="diretorio-second-level"><a title="Alagoas" href="http://g1.globo.com/al/alagoas/">Alagoas</a></li><li class="diretorio-second-level"><a title="Amazonas" href="http://g1.globo.com/am/amazonas/">Amazonas</a></li><li class="diretorio-second-level"><a title="Bahia" href="http://g1.globo.com/bahia/?noAudience=true">Bahia</a></li><li class="diretorio-second-level"><a title="Bem Estar" href="http://g1.globo.com/bemestar/">Bem Estar</a></li><li class="diretorio-second-level"><a title="Bom Dia Brasil" href="http://g1.globo.com/bom-dia-brasil/">Bom Dia Brasil</a></li><li class="diretorio-second-level"><a title="Brasil" href="http://g1.globo.com/brasil/">Brasil</a></li><li class="diretorio-second-level"><a title="Carnaval" href="http://g1.globo.com/carnaval/2013/">Carnaval</a></li><li class="diretorio-second-level"><a title="Carros" href="http://g1.globo.com/carros/">Carros</a></li><li class="diretorio-second-level"><a title="CearÃ¡" href="http://g1.globo.com/ceara/">CearÃ¡</a></li><li class="diretorio-second-level"><a title="CiÃªncia e SaÃºde" href="http://g1.globo.com/ciencia-e-saude/">CiÃªncia e SaÃºde</a></li><li class="diretorio-second-level"><a title="Como SerÃ¡?" href="http://redeglobo.globo.com/como-sera/index.html">Como SerÃ¡?</a></li><li class="diretorio-second-level"><a title="Concursos e Emprego" href="http://g1.globo.com/concursos-e-emprego/">Concursos e Emprego</a></li><li class="diretorio-second-level"><a title="Distrito Federal" href="http://g1.globo.com/distrito-federal/">Distrito Federal</a></li><li class="diretorio-second-level"><a title="Economia" href="http://g1.globo.com/economia/">Economia</a></li><li class="diretorio-second-level"><a title="EducaÃ§Ã£o" href="http://www.educacao.globo.com/">EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="EspÃ­rito Santo" href="http://g1.globo.com/espirito-santo/">EspÃ­rito Santo</a></li><li class="diretorio-second-level"><a title="FantÃ¡stico" href="http://fantastico.globo.com/">FantÃ¡stico</a></li><li class="diretorio-second-level"><a title="G1 EducaÃ§Ã£o" href="http://g1.globo.com/educacao/">G1 EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Globo Natureza" href="http://g1.globo.com/natureza/">Globo Natureza</a></li><li class="diretorio-second-level"><a title="Globo News" href="http://globonews.globo.com/">Globo News</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://g1.globo.com/economia/agronegocios/">Globo Rural</a></li><li class="diretorio-second-level"><a title="Globo RepÃ³rter" href="http://g1.globo.com/globo-reporter/">Globo RepÃ³rter</a></li><li class="diretorio-second-level"><a title="GoiÃ¡s" href="http://g1.globo.com/goias/">GoiÃ¡s</a></li><li class="diretorio-second-level"><a title="Jornal da Globo" href="http://g1.globo.com/jornal-da-globo/">Jornal da Globo</a></li><li class="diretorio-second-level"><a title="Jornal Hoje" href="http://g1.globo.com/jornal-hoje/">Jornal Hoje</a></li><li class="diretorio-second-level"><a title="Jornal Nacional" href="http://g1.globo.com/jornal-nacional/">Jornal Nacional</a></li><li class="diretorio-second-level"><a title="MaranhÃ£o" href="http://g1.globo.com/ma/maranhao/">MaranhÃ£o</a></li><li class="diretorio-second-level"><a title="Mato Grosso" href="http://g1.globo.com/mato-grosso/">Mato Grosso</a></li><li class="diretorio-second-level"><a title="Mato Grosso do Sul" href="http://g1.globo.com/mato-grosso-do-sul/">Mato Grosso do Sul</a></li><li class="diretorio-second-level"><a title="Minas Gerais" href="http://g1.globo.com/minas-gerais/">Minas Gerais</a></li><li class="diretorio-second-level"><a title="Mundo" href="http://g1.globo.com/mundo/">Mundo</a></li><li class="diretorio-second-level"><a title="ParÃ¡" href="http://g1.globo.com/pa/para/">ParÃ¡</a></li><li class="diretorio-second-level"><a title="ParaÃ­ba" href="http://g1.globo.com/paraiba/">ParaÃ­ba</a></li><li class="diretorio-second-level"><a title="ParanÃ¡" href="http://g1.globo.com/parana/">ParanÃ¡</a></li><li class="diretorio-second-level"><a title="Pernambuco" href="http://g1.globo.com/pernambuco/">Pernambuco</a></li><li class="diretorio-second-level"><a title="PiauÃ­" href="http://g1.globo.com/pi/piaui/">PiauÃ­</a></li><li class="diretorio-second-level"><a title="Planeta Bizarro" href="http://g1.globo.com/planeta-bizarro/">Planeta Bizarro</a></li><li class="diretorio-second-level"><a title="PolÃ­tica" href="http://g1.globo.com/politica/">PolÃ­tica</a></li><li class="diretorio-second-level"><a title="Pop &amp; Arte" href="http://g1.globo.com/pop-arte/">Pop &amp; Arte</a></li><li class="diretorio-second-level"><a title="PrevisÃ£o do Tempo" href="http://g1.globo.com/previsao-do-tempo.html">PrevisÃ£o do Tempo</a></li><li class="diretorio-second-level"><a title="ProfissÃ£o RepÃ³rter" href="http://g1.globo.com/profissao-reporter/">ProfissÃ£o RepÃ³rter</a></li><li class="diretorio-second-level"><a title="Rio de Janeiro" href="http://g1.globo.com/rio-de-janeiro/">Rio de Janeiro</a></li><li class="diretorio-second-level"><a title="Rio Grande do Norte" href="http://g1.globo.com/rn/rio-grande-do-norte/">Rio Grande do Norte</a></li><li class="diretorio-second-level"><a title="Rio Grande do Sul" href="http://g1.globo.com/rs/rio-grande-do-sul/">Rio Grande do Sul</a></li><li class="diretorio-second-level"><a title="RondÃ´nia" href="http://g1.globo.com/ro/rondonia/">RondÃ´nia</a></li><li class="diretorio-second-level"><a title="Santa Catarina" href="http://g1.globo.com/sc/santa-catarina/">Santa Catarina</a></li><li class="diretorio-second-level"><a title="SÃ£o Paulo" href="http://g1.globo.com/sao-paulo/">SÃ£o Paulo</a></li><li class="diretorio-second-level"><a title="Sergipe" href="http://g1.globo.com/se/sergipe/">Sergipe</a></li><li class="diretorio-second-level"><a title="Serra, Lagos e Norte" href="http://g1.globo.com/rj/serra-lagos-norte/">Serra, Lagos e Norte</a></li><li class="diretorio-second-level"><a title="Tecnologia e Games" href="http://g1.globo.com/tecnologia/">Tecnologia e Games</a></li><li class="diretorio-second-level"><a title="Turismo e Viagem" href="http://g1.globo.com/turismo-e-viagem/">Turismo e Viagem</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-globoesporte diretorio-quebra "><a title="globoesporte" href="http://globoesporte.globo.com/">globoesporte</a><ul><li class="diretorio-second-level"><a title="Atletismo" href="http://globoesporte.globo.com/atletismo/">Atletismo</a></li><li class="diretorio-second-level"><a title="Basquete" href="http://globoesporte.globo.com/basquete/">Basquete</a></li><li class="diretorio-second-level"><a title="Basquete/NBB" href="http://globoesporte.globo.com/basquete/nbb/">Basquete/NBB</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie A" href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">BrasileirÃ£o SÃ©rie A</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie B" href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">BrasileirÃ£o SÃ©rie B</a></li><li class="diretorio-second-level"><a title="Campeonato Carioca" href="http://globoesporte.globo.com/rj/futebol/campeonato-carioca/index.html">Campeonato Carioca</a></li><li class="diretorio-second-level"><a title="Campeonato GaÃºcho" href="http://globoesporte.globo.com/rs/futebol/campeonato-gaucho/index.html">Campeonato GaÃºcho</a></li><li class="diretorio-second-level"><a title="Campeonato Mineiro" href="http://globoesporte.globo.com/mg/futebol/campeonato-mineiro/index.html">Campeonato Mineiro</a></li><li class="diretorio-second-level"><a title="Campeonato Paulista" href="http://globoesporte.globo.com/sp/futebol/campeonato-paulista/index.html">Campeonato Paulista</a></li><li class="diretorio-second-level"><a title="Cartola FC" href="http://globoesporte.globo.com/cartola-fc/">Cartola FC</a></li><li class="diretorio-second-level"><a title="Copa do Mundo da Fifa" href="http://globoesporte.globo.com/futebol/copa-do-mundo/index.html">Copa do Mundo da Fifa</a></li><li class="diretorio-second-level"><a title="Copa AmÃ©rica" href="http://globoesporte.globo.com/futebol/copa-america/">Copa AmÃ©rica</a></li><li class="diretorio-second-level"><a title="Copa Sul-Americana" href="http://globoesporte.globo.com/futebol/copa-sul-americana/index.html">Copa Sul-Americana</a></li><li class="diretorio-second-level"><a title="Desafio de Futsal" href="http://globoesporte.globo.com/eventos/desafio-de-futsal/">Desafio de Futsal</a></li><li class="diretorio-second-level"><a title="Esporte Espetacular" href="http://globoesporte.globo.com/programas/esporte-espetacular/">Esporte Espetacular</a></li><li class="diretorio-second-level"><a title="Esportes AquÃ¡ticos" href="http://globoesporte.globo.com/aquaticos/">Esportes AquÃ¡ticos</a></li><li class="diretorio-second-level"><a title="Esportes Radicais" href="http://globoesporte.globo.com/radicais/">Esportes Radicais</a></li><li class="diretorio-second-level"><a title="Eu Atleta" href="http://globoesporte.globo.com/eu-atleta/">Eu Atleta</a></li><li class="diretorio-second-level"><a title="FÃ³rmula 1" href="http://globoesporte.globo.com/motor/formula-1/">FÃ³rmula 1</a></li><li class="diretorio-second-level"><a title="Futebol" href="http://globoesporte.globo.com/futebol/">Futebol</a></li><li class="diretorio-second-level"><a title="Futebol Internacional" href="http://globoesporte.globo.com/futebol/futebol-internacional/">Futebol Internacional</a></li><li class="diretorio-second-level"><a title="FutpÃ©dia" href="http://futpedia.globo.com/">FutpÃ©dia</a></li><li class="diretorio-second-level"><a title="Game Futebol" href="http://gamefutebol.globoesporte.globo.com/">Game Futebol</a></li><li class="diretorio-second-level"><a title="Liga dos CampeÃµes" href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">Liga dos CampeÃµes</a></li><li class="diretorio-second-level"><a title="OlimpÃ­adas" href="http://globoesporte.globo.com/olimpiadas/">OlimpÃ­adas</a></li><li class="diretorio-second-level"><a title="SeleÃ§Ã£o Brasileira" href="http://globoesporte.globo.com/futebol/selecao-brasileira/">SeleÃ§Ã£o Brasileira</a></li><li class="diretorio-second-level"><a title="StockCar" href="http://globoesporte.globo.com/motor/stock-car/">StockCar</a></li><li class="diretorio-second-level"><a title="Surfe" href="http://globoesporte.globo.com/radicais/surfe/">Surfe</a></li><li class="diretorio-second-level"><a title="TÃªnis" href="http://globoesporte.globo.com/tenis/">TÃªnis</a></li><li class="diretorio-second-level"><a title="Vai-e-vem do Mercado" href="http://globoesporte.globo.com/futebol/vai-e-vem-do-mercado/">Vai-e-vem do Mercado</a></li><li class="diretorio-second-level"><a title="VerÃ£o Espetacular" href="http://globoesporte.globo.com/programas/verao-espetacular/">VerÃ£o Espetacular</a></li><li class="diretorio-second-level"><a title="VÃ´lei" href="http://globoesporte.globo.com/volei/">VÃ´lei</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-gshow diretorio-quebra "><a title="gshow" href="http://gshow.globo.com/">gshow</a><ul><li class="diretorio-second-level"><a title="A Regra do Jogo" href="http://gshow.globo.com/novelas/a-regra-do-jogo/">A Regra do Jogo</a></li><li class="diretorio-second-level"><a title="AlÃ©m do Tempo" href="http://gshow.globo.com/novelas/alem-do-tempo/">AlÃ©m do Tempo</a></li><li class="diretorio-second-level"><a title="Altas Horas" href="http://altashoras.globo.com/">Altas Horas</a></li><li class="diretorio-second-level"><a title="Amor &amp; Sexo" href="http://amoresexo.globo.com/">Amor &amp; Sexo</a></li><li class="diretorio-second-level"><a title="Bastidores" href="http://gshow.globo.com/Bastidores/">Bastidores</a></li><li class="diretorio-second-level"><a title="BBB" href="http://bbb.globo.com/">BBB</a></li><li class="diretorio-second-level"><a title="CaldeirÃ£o do Huck" href="http://caldeiraodohuck.globo.com/">CaldeirÃ£o do Huck</a></li><li class="diretorio-second-level"><a title="Caminho das Ãndias" href="http://gshow.globo.com/novelas/caminho-das-indias/">Caminho das Ãndias</a></li><li class="diretorio-second-level"><a title="Como Fazer" href="http://gshow.globo.com/como-fazer/">Como Fazer</a></li><li class="diretorio-second-level"><a title="DomingÃ£o do FaustÃ£o" href="http://domingaodofaustao.globo.com/">DomingÃ£o do FaustÃ£o</a></li><li class="diretorio-second-level"><a title="Ã de Casa" href="http://gshow.globo.com/programas/e-de-casa/">Ã de Casa</a></li><li class="diretorio-second-level"><a title="Encontro com FÃ¡tima Bernardes" href="http://tvg.globo.com/programas/encontro-com-fatima-bernardes/">Encontro com FÃ¡tima Bernardes</a></li><li class="diretorio-second-level"><a title="Escolinha do Professor Raimundo" href="http://gshow.globo.com/programas/escolinha-do-professor-raimundo/">Escolinha do Professor Raimundo</a></li><li class="diretorio-second-level"><a title="Esquenta!" href="http://tvg.globo.com/programas/esquenta/">Esquenta!</a></li><li class="diretorio-second-level"><a title="Estilo" href="http://gshow.globo.com/Estilo/">Estilo</a></li><li class="diretorio-second-level"><a title="Estrelas" href="http://estrelas.globo.com/">Estrelas</a></li><li class="diretorio-second-level"><a title="Ãta Mundo Bom!" href="http://gshow.globo.com/novelas/eta-mundo-bom/">Ãta Mundo Bom!</a></li><li class="diretorio-second-level"><a title="LigaÃ§Ãµes Perigosas" href="http://gshow.globo.com/series/ligacoes-perigosas/">LigaÃ§Ãµes Perigosas</a></li><li class="diretorio-second-level"><a title="Mais VocÃª" href="http://maisvoce.globo.com/">Mais VocÃª</a></li><li class="diretorio-second-level"><a title="MalhaÃ§Ã£o" href="http://malhacao.globo.com/">MalhaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Mister Brau" href="http://gshow.globo.com/series/mister-brau/2015/">Mister Brau</a></li><li class="diretorio-second-level"><a title="MÃºsica" href="http://gshow.globo.com/Musica/">MÃºsica</a></li><li class="diretorio-second-level"><a title="PÃ© na Cova" href="http://gshow.globo.com/series/pe-na-cova/2015/">PÃ© na Cova</a></li><li class="diretorio-second-level"><a title="Programa do JÃ´" href="http://programadojo.globo.com/">Programa do JÃ´</a></li><li class="diretorio-second-level"><a title="Receitas.com" href="http://www.receitas.com/">Receitas.com</a></li><li class="diretorio-second-level"><a title="Receitas da Ana Maria" href="http://gshow.globo.com/programas/mais-voce/Receitas-da-Ana-Maria/">Receitas da Ana Maria</a></li><li class="diretorio-second-level"><a title="SÃ©ries Originais" href="http://gshow.globo.com/webseries/">SÃ©ries Originais</a></li><li class="diretorio-second-level"><a title="SessÃ£o ComÃ©dia" href="http://gshow.globo.com/sessao-comedia/videos/">SessÃ£o ComÃ©dia</a></li><li class="diretorio-second-level"><a title="TÃ¡ no Ar" href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/index.html">TÃ¡ no Ar</a></li><li class="diretorio-second-level"><a title="The Voice Brasil" href="http://gshow.globo.com/realities/the-voice-brasil/">The Voice Brasil</a></li><li class="diretorio-second-level"><a title="The Voice Kids" href="http://gshow.globo.com/programas/the-voice-kids/">The Voice Kids</a></li><li class="diretorio-second-level"><a title="Totalmente Demais" href="http://gshow.globo.com/novelas/totalmente-demais/">Totalmente Demais</a></li><li class="diretorio-second-level"><a title="Tomara que Caia" href="http://gshow.globo.com/programas/tomara-que-caia/">Tomara que Caia</a></li><li class="diretorio-second-level"><a title="Video Show" href="http://videoshow.globo.com/">Video Show</a></li><li class="diretorio-second-level"><a title="Zorra" href="http://gshow.globo.com/programas/zorra/">Zorra</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-famosos-etc diretorio-quebra "><a title="famosos &amp; etc" href="http://famosos.globo.com/">famosos &amp; etc</a><ul><li class="diretorio-second-level"><a title="EGO" href="http://ego.globo.com/">EGO</a></li><li class="diretorio-second-level"><a title="G1 Cinema" href="http://g1.globo.com/pop-arte/cinema/">G1 Cinema</a></li><li class="diretorio-second-level"><a title="G1 MÃºsica" href="http://g1.globo.com/pop-arte/musica/">G1 MÃºsica</a></li><li class="diretorio-second-level"><a title="Globo Jogos" href="http://jogos.globo.com/">Globo Jogos</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="HorÃ³scopo" href="http://horoscopo.ego.globo.com/ne/home.html">HorÃ³scopo</a></li><li class="diretorio-second-level"><a title="Meus 5 minutos" href="http://www.meus5minutos.com.br/">Meus 5 minutos</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="Namoro na Web" href="http://tracking.parperfeito.com.br/ppbanner/bannerTracker?originId=13&amp;identifierId=41763&amp;actionId=1">Namoro na Web</a></li><li class="diretorio-second-level"><a title="Paparazzo" href="http://paparazzo.globo.com/">Paparazzo</a></li><li class="diretorio-second-level"><a title="Patricia Kogut" href="http://oglobo.globo.com/cultura/kogut/">Patricia Kogut</a></li><li class="diretorio-second-level"><a title="Retratos da Vida" href="http://extra.globo.com/lazer/retratosdavida/">Retratos da Vida</a></li><li class="diretorio-second-level"><a title="Senninha" href="http://senninha.globo.com/">Senninha</a></li><li class="diretorio-second-level"><a title="Teatro" href="http://www.agentesevenoteatro.com.br/">Teatro</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-globo-play diretorio-sem-quebra "><a title="globo play" href="http://globoplay.globo.com/">globo play</a></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-grupo-globo diretorio-quebra "><a title="Grupo Globo" href="http://grupoglobo.globo.com/">Grupo Globo</a><ul><li class="diretorio-second-level"><a title="princÃ­pios editoriais" href="http://g1.globo.com/principios-editoriais-do-grupo-globo.html">princÃ­pios editoriais</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-editora-globo diretorio-sem-quebra "><a title="editora globo" href="http://editoraglobo.globo.com/">editora globo</a><ul><li class="diretorio-second-level"><a title="Auto Esporte" href="http://revistaautoesporte.globo.com/">Auto Esporte</a></li><li class="diretorio-second-level"><a title="Casa e Comida" href="http://revistacasaejardim.globo.com/Casa-e-Comida/">Casa e Comida</a></li><li class="diretorio-second-level"><a title="Casa e Jardim" href="http://revistacasaejardim.globo.com/">Casa e Jardim</a></li><li class="diretorio-second-level"><a title="Casa Vogue" href="http://casavogue.globo.com/">Casa Vogue</a></li><li class="diretorio-second-level"><a title="Crescer" href="http://revistacrescer.globo.com/">Crescer</a></li><li class="diretorio-second-level"><a title="Ãpoca" href="http://revistaepoca.globo.com/">Ãpoca</a></li><li class="diretorio-second-level"><a title="Ãpoca NegÃ³cios" href="http://epocanegocios.globo.com/">Ãpoca NegÃ³cios</a></li><li class="diretorio-second-level"><a title="Galileu" href="http://revistagalileu.globo.com/">Galileu</a></li><li class="diretorio-second-level"><a title="Glamour" href="http://revistaglamour.globo.com/">Glamour</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://revistagloborural.globo.com/">Globo Rural</a></li><li class="diretorio-second-level"><a title="GQ Brasil" href="http://gq.globo.com/">GQ Brasil</a></li><li class="diretorio-second-level"><a title="Marie Claire" href="http://revistamarieclaire.globo.com/">Marie Claire</a></li><li class="diretorio-second-level"><a title="Meus 5 Minutos" href="http://meus5minutos.globo.com/">Meus 5 Minutos</a></li><li class="diretorio-second-level"><a title="Monet" href="http://revistamonet.globo.com/">Monet</a></li><li class="diretorio-second-level"><a title="Mundo do SÃ­tio" href="http://mundodositio.globo.com/">Mundo do SÃ­tio</a></li><li class="diretorio-second-level"><a title="PEGN" href="http://revistapegn.globo.com/">PEGN</a></li><li class="diretorio-second-level"><a title="Quem" href="http://revistaquem.globo.com/">Quem</a></li><li class="diretorio-second-level"><a title="Vogue" href="http://vogue.globo.com/">Vogue</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-infoglobo diretorio-sem-quebra "><a title="infoglobo" href="http://infoglobo.com.br/Anuncie/Home.aspx">infoglobo</a><ul><li class="diretorio-second-level"><a title="Ela Digital" href="http://ela.oglobo.globo.com/">Ela Digital</a></li><li class="diretorio-second-level"><a title="Extra" href="http://extra.globo.com/">Extra</a></li><li class="diretorio-second-level"><a title="O Globo" href="http://oglobo.globo.com/">O Globo</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-last "><ul><li class="diretorio-first-level analytics-product glb-menu-globosat diretorio-quebra "><a title="globosat" href="http://canaisglobosat.globo.com/">globosat</a><ul><li class="diretorio-second-level"><a title="Globosat Play" href="http://globosatplay.globo.com/">Globosat Play</a></li><li class="diretorio-second-level"><a title="+Globosat" href="http://maisglobosat.globo.com/">+Globosat</a></li><li class="diretorio-second-level"><a title="BIS" href="http://canalbis.globo.com/">BIS</a></li><li class="diretorio-second-level"><a title="Canal Brasil" href="http://canalbrasil.globo.com/">Canal Brasil</a></li><li class="diretorio-second-level"><a title="Canal Futura" href="http://www.futura.org.br/">Canal Futura</a></li><li class="diretorio-second-level"><a title="Combate" href="http://combate.globo.com/">Combate</a></li><li class="diretorio-second-level"><a title="GloboNews" href="http://globonews.globo.com/">GloboNews</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="Megapix" href="http://megapix.globo.com/">Megapix</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="Off" href="http://canaloff.globo.com/">Off</a></li><li class="diretorio-second-level"><a title="Premiere" href="http://sociopremiere.globo.com/">Premiere</a></li><li class="diretorio-second-level"><a title="SporTV" href="http://sportv.globo.com/">SporTV</a></li><li class="diretorio-second-level"><a title="Studio Universal" href="http://studiouniversal.globo.com/">Studio Universal</a></li><li class="diretorio-second-level"><a title="Syfy" href="http://syfy.globo.com/">Syfy</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Universal Channel" href="http://uc.globo.com/">Universal Channel</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-sistema-globo-de-radio diretorio-sem-quebra "><a title="sistema globo de rÃ¡dio" href="http://globoradio.globo.com/home/HOME.htm">sistema globo de rÃ¡dio</a><ul><li class="diretorio-second-level"><a title="BHFM" href="http://bhfm.globoradio.globo.com/home/HOME.htm">BHFM</a></li><li class="diretorio-second-level"><a title="CBN" href="http://cbn.globoradio.globo.com/home/HOME.htm">CBN</a></li><li class="diretorio-second-level"><a title="RADIOBEAT" href="http://radiobeat.com.br/">RADIOBEAT</a></li><li class="diretorio-second-level"><a title="RÃ¡dio Globo" href="http://radioglobo.globoradio.globo.com/home/HOME.htm">RÃ¡dio Globo</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-globo-filmes diretorio-sem-quebra "><a title="globo filmes" href="http://globofilmes.globo.com/">globo filmes</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-som-livre diretorio-sem-quebra "><a title="som livre" href="http://www.somlivre.com/">som livre</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-blog-globocom diretorio-sem-quebra "><a title="blog globo.com" href="http://blog.globo.com/1.html">blog globo.com</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-zap-imoveis diretorio-sem-quebra "><a title="zap imÃ³veis" href="http://www.zapimoveis.com.br/">zap imÃ³veis</a></li></ul></li></ul></div></div></div></div></div></section><div id="x61" class="opec-area opec-mobile opec-x61 grid-12"><div id="banner_mobile_fim" class="tag-manager-publicidade-container"></div></div><footer><div class="container analytics-area analytics-id-B"><a href="http://www.globo.com/" title="Globo.com">Globo.com</a><span class="copyright">&copy; Copyright 2000-2015 Globo ComunicaÃ§Ã£o e ParticipaÃ§Ãµes S.A.</span><span class="copyright-480 hidden"><p>&copy; 2000-2015.</p><p>Todos os direitos reservados.</p></span><ul class="footer-menu"><li><a href="/privacidade.html">polÃ­tica de privacidade e seguranÃ§a</a></li><li><a href="/todos-os-sites.html">todos os sites</a></li><li><a href="http://anuncie.globo.com/">anuncie conosco</a></li><li><a href="http://meuperfil.globo.com">central globo.com</a></li></ul></div></footer></div><!--[if lt IE 9]><script src="http://s.glbimg.com/en/ho/static/libby/css3pie/js/PIE.js" type="text/javascript"></script><![endif]--><script>
responsiveHub.updateImages();</script><script>
(function(){var scr=document.createElement('script');scr.type = 'text/javascript';scr.async=true;scr.src = 'http://s.glbimg.com/en/ho/static/CACHE/js/5fd02574e217.js';document.getElementsByTagName('head')[0].appendChild(scr);}());</script><div id="opec-frame1"><div id="banner_floating" class="tag-manager-publicidade-container"></div></div><script>if(document.body && document.body.offsetWidth <= 711){}else{setTimeout(function(){var bannerMiddleTop = $("#ad-position-middle"),
bannerMiddleBottom = $('#ad-position-middle3');if(bannerMiddleTop.height() < 15){bannerMiddleTop.addClass('without-opec');}if(bannerMiddleBottom.height() < 15){bannerMiddleBottom.addClass('without-opec');}}, 6000);}
</script><script>
window.glb=window.glb || {};window.glb.analyticsConfig=window.glb.analyticsConfig  || {};window.glb.analyticsConfig = {scrollEventChance: 0,
setDomainVar: true};</script><script>
document.cookie = 'globoapi.geo.geolocation=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.estado=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.novela=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.time=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';setTimeout(function(){window.glb.barra=window.glb.barra || {};window.glb.barra.component=window.glb.barra.component || {};window.glb.barra.component.barraDiv = {className: ''};}, 1000);</script><style>
#assinante-menu .barra-notifications-area .float-box.config-active .save-area{bottom:0;position:absolute;width:100%}#assinante-menu .barra-notifications-area .float-box.config-active .config-box{padding-bottom:44px}</style><style>
header .container{height:150px}.box-destaque-uber .destaques-primario .titulo{font-family:"ProximaNovaA-Bold"}@media (min-width:1600px){.conteudo-uber .box-destaque-uber{position:static;max-width:1600px;margin:0 auto}}</style><style>
.agrupador-quadruplo-tecnologia .destaque-inferior ul{display:none;// css de folder}</style><link type="text/css" rel="stylesheet" href="http://c.api.globo.com/soccer_teams/b_45x45.css"></body></html>
<!-- PÃ¡gina gerada em 30/12/2015 16:44:09 -->
