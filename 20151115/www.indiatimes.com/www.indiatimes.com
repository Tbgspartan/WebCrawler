<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trending  stories on Indian Lifestyle, Culture, Relationships, Food, Travel, Entertainment, News & New Technology News - Indiatimes.com</title>

<meta name="keywords" content="" />
<meta name="description" content="Indiatimes.com brings you the news, articles, stories and videos on entertainment, latest lifestyle, culture & new technologies emerging worldwide." />



		

<link rel="image_src" href="http://media.indiatimes.in/resources/images/fbimage.png" />
<meta property="og:title" content="" />    
<meta property="og:url" content="http://www.indiatimes.com" />
<meta property="og:image" content="http://media.indiatimes.in/resources/images/fbimage.png" />

<meta property="og:site_name" content="indiatimes.com" />
<meta property="fb:app_id" content="117787264903013" />
<meta property="og:description" content="" />
<meta name="google-site-verification" content="ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw" />
<meta name="msvalidate.01" content="BAB71AAED7CCC01DB106A58E99625EB4" />
<link href="https://plus.google.com/+indiatimes/posts" rel="publisher" />

<meta property="og:type" content="website" />
    
<!--<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />-->

<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">    <link rel="shortcut icon" href="http://media.indiatimes.in/resources/images/favicon.ico?v=25" type="image/x-icon"><link rel="stylesheet" href="http://media.indiatimes.in/resources/css/site.css?v=100.53" media="screen"><link rel="stylesheet" href="http://www.indiatimes.com/fonts/fonts.min.css?v=100.53" media="screen"><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery-1.11.1.min.js?v=100.53"></script><script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/handlebars-v1.3.0.js?v=100.53"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.inview.min.js?v=100.53"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.preload.min.js?v=100.53"></script><script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script><!-- Facebook Pixel Code --><script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');fbq('init', '853339751421540');fbq('track', 'PageView');</script><noscript><img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=853339751421540&ev=PageView&noscript=1"/></noscript><!-- End Facebook Pixel Code -->           <!--  Analytics Code Begin -->    <!-- Begin comScore Tag -->    <script>        var _comscore = _comscore || [];        _comscore.push({ c1: "2", c2: "6036484" });        (function() {            var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";            el.parentNode.insertBefore(s, el);        })();    </script>    <noscript>    <img src="http://b.scorecardresearch.com/p?c1=2&c2=6036484&cv=2.0&cj=1" />    </noscript>    <!-- End comScore Tag -->    <!-- Begin Google Analytics Tag -->    <script>        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');        ga('create', 'UA-198011-6', 'indiatimes.com');            ga('require', 'displayfeatures');        ga('send', 'pageview');    </script>    <!-- End Google Analytics Tag -->    <!--  Analytics Code Begin --><!--adcode common script--><script type='text/javascript' src="http://ad.crwdcntrl.net/5/c=2800/pe=y/var=_ccaud"></script><script type="text/javascript">    var googletag = googletag || {};    googletag.cmd = googletag.cmd || [];    (function() {        var gads = document.createElement("script");        gads.async = true;        gads.type = "text/javascript";        var useSSL = "https:" == document.location.protocol;        gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";        var node =document.getElementsByTagName("script")[0];        node.parentNode.insertBefore(gads, node);    })();</script> <script type="text/javascript">        var nid='';        var cid='';        var sid='';        var brand_name='';                                                             </script>    <script>
    var contType = "";
    </script>
        <script type="text/javascript" src="http://media.indiatimes.in/resources/js/banner/home.js?v=100.53"></script>
            
<!--[if lt IE 9]>
  <script type="text/javascript" src="http://media.indiatimes.in/js/html5.js?v1.1"></script>
  <![endif]-->
<script>
var isDetailPage = 0</script>
</head>
<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];   
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=117787264903013&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script> 	
	
<div id="wrap" class="cf"><!--wrap start-->
    <div class='darkDisabledBg' id='disabledBg'></div>
<header>
    <div class="header-inner">
        <div class="comm-header ">
            <nav>
                <div class='forTop'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>

                    <div class="lnks">
                                                        <a href="http://www.indiatimes.com/news/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','News');">News</a> 
                                                               <a href="http://www.indiatimes.com/lifestyle/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Lifestyle');">Lifestyle</a> 
                                                               <a href="http://www.indiatimes.com/entertainment/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Entertainment');">Entertainment</a> 
                                                               <a href="http://www.indiatimes.com/health/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Health');">Health</a> 
                                                               <a href="http://www.indiatimes.com/videocafe/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Videos');">Videos</a> 
                                                                                          <a href='http://www.indiatimes.com/trending'  target="_blank" class="#fffff" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Trending');">Trending</a> 
                                                </div>
                </div>
                <div class='onScrolled'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>
                    <a href="javascript:;" class="menu rc sprite" id="headerMenu">Menu</a>
                    <div class="has-tag">
                                           </div>
                </div>

                                
            </nav>
			                 
	    <div id="sticker" style="display:none;"><span class="readTitle"></span></div>
                    <div class="socls fr share">
            </div>
                        
            
			 <div class="social fr">
                            <a title="facebook" target="_blank"  class="sprite fb" href="https://www.facebook.com/indiatimes">facebook</a>
                            <a title="twitter" target="_blank"  class="sprite twt" href="https://twitter.com/indiatimes">twitter</a>
                            <a title="search"  class="sprite serach" onclick="$('body').showSearch();" href="javascript:void(0);">search</a>
                        </div>
        </div>
    </div>
</header>
<div class="clr"></div>
<div id="overlay" class="animated bounceOut"><!--overlay start-->
    <div class="search_conts"><!--search-cont start-->
        <form onsubmit="return false;" class="serach-form">
            <input type="text" onkeyup="search_data();" class="input" onfocus="if(this.value=='Search'){this.value=''}" onblur="if(this.value==''){this.value='Search'};" value="Search" name="q" id="q" autocomplete="off">
            <input type="button" class="sprite src-btn" onclick="search_data();">
            <a class="sprite sclose" id="close" href="javascript:void(0);">X</a>
        </form>
        <div class="scroll-panes" id="scroll-panes">
            <div class="clr"></div>
            <div class="res-msg" id="res-msg"></div>
            <div class="search-list cf" id="results"></div>
            <div class="clr"></div>
            <div class="loadmore" id="progressBar" style="display: none;">Loading......</div>
            <a class="loadmore" style="display:none;" onclick="load_search_data(); " id="load_more_button" href="javascript:void(0);">Load more</a>
        </div>
    </div>
</div>
<div class="blk">
    <div  id="leftMenu" class="leftMenu accordion inner">
        <dl >
            <dt data-color="red-bg" class=""><a href="http://www.indiatimes.com">Home</a></dt>
            <dd> </dd>
                            <dt  data-color="blue-bg" ><a href="http://www.indiatimes.com/news/">News</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/news/india/'>India</a>
                                     
                            <a href='http://www.indiatimes.com/news/world/'>World</a>
                                     
                            <a href='http://www.indiatimes.com/news/sports/'>Sports</a>
                                     
                            <a href='http://www.indiatimes.com/news/weird/'>Weird</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="purple-bg" ><a href="http://www.indiatimes.com/lifestyle/">Lifestyle</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/lifestyle/self/'>Self</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/style/'>Style</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                     
                            <a href='http://www.indiatimes.com/culture/who-we-are/'>Who we are</a>
                                     
                            <a href='http://www.indiatimes.com/culture/travel/'>Travel</a>
                                     
                            <a href='http://www.indiatimes.com/culture/food/'>Food</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="red-bg" ><a href="http://www.indiatimes.com/entertainment/">Entertainment</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/entertainment/bollywood/'>Bollywood</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/celebs/'>Celebscoop</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/hollywood/'>Hollywood</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="green-bg" ><a href="http://www.indiatimes.com/health/">Health</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                     
                            <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                     
                            <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                     
                            <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                     
                            <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="orange-bg" ><a href="http://www.indiatimes.com/videocafe/">Videos</a>
                                </dt>
                				<dt data-color="" class=""><a href="http://www.indiatimes.com/trending/">Trending</a>
                           </dt>
            <dt class="pink-bg">
            <div class="follow">Follow indiatimes </div>
            <div class="follow_cont"> 
                <a href="https://www.facebook.com/indiatimes" class="fbleft sprite"> facebook</a> 
                <a href="https://twitter.com/indiatimes" class="twtleft sprite"> twitter</a> 
            </div>
            </dt>

            <dd>&nbsp;</dd>
            <div class="clr"></div>
        </dl>
    </div>
</div>


<div class="clr"></div>
<div class="dummy-cont">&nbsp;</div>
<div class="clr"></div>


<div id="pushdown">
    <div class="pull-ad"><!--pull-ad start-->   
	
                <script type="text/javascript"> 
                    showHeaderCode();
                </script>
	    </div><!--pull-ad end-->
</div>
    <!--testing 15-11-16 00:00:05-->  

    <section id="hp_block_1" class="container cf" style="border: 0px solid red;"><!--container start-->
        <div class="top-heading">
    <a href="http://www.indiatimes.com/news/world/a-sikh-man-was-photoshopped-to-resemble-a-paris-terrorist-was-even-shared-by-an-isis-channel-247316.html" class="big-card-top red"></a>
</div>

<div class="clr"></div>
<div class="big-image">
	<div class="gradient-b"></div>
	<a href="http://www.indiatimes.com/news/world/a-sikh-man-was-photoshopped-to-resemble-a-paris-terrorist-was-even-shared-by-an-isis-channel-247316.html" class="big-card-small">A Sikh Man's Selfie Was Photoshopped To Resemble A Paris Attack Terrorist, It Was Then Shared By An ISIS Channel</a>
                <a href="http://www.indiatimes.com/news/world/a-sikh-man-was-photoshopped-to-resemble-a-paris-terrorist-was-even-shared-by-an-isis-channel-247316.html" class="tint"><img src="http://media.indiatimes.in/media/content/2015/Nov/980_1447580189_980x457.jpg"/></a>
</div>
<div class="clr"></div>
<div class="top-heading">
    
</div>

        

        <div class="news-panel cf"><!--news-panel start-->
            <h2>Big news</h2>

            <div class="feature-list cf"><!--feature-list start-->	
                                    <figure>
                        <div class="hash-tag">
                             12 hours ago                         </div>
						 						          
                        <a href="http://www.indiatimes.com/news/world/muslims-worldwide-are-condemning-the-paris-attacks-say-islam-doesn-t-teach-terrorism-247321.html" class=" tint" title="Muslims Worldwide Are Condemning The Paris Attacks, Say Islam Doesn't Teach Terrorism">
                            <img  class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Nov/paris502_1447571100_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/paris502_1447571100_236x111.jpg"  border="0" alt="Muslims Worldwide Are Condemning The Paris Attacks, Say Islam Doesn't Teach Terrorism"/></a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/muslims-worldwide-are-condemning-the-paris-attacks-say-islam-doesn-t-teach-terrorism-247321.html" title="Muslims Worldwide Are Condemning The Paris Attacks, Say Islam Doesn't Teach Terrorism">
                            Muslims Worldwide Are Condemning The Paris Attacks, Say Islam Doesn't Teach Terrorism                        </a>
                    </figcaption>
                            </div><!--feature-list end-->

                            <div class="news-panel-list cf" id="column1_0"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            6 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/world/after-losing-her-boyfriend-in-the-paris-terror-attacks-this-girl-just-posted-an-emotional-tribute-to-him-247345.html" title="After Losing Her Boyfriend In The Paris Terror Attacks, This Girl Just Posted An Emotional Tribute To Him!" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Nov/metal502_1447590934_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/metal502_1447590934_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/after-losing-her-boyfriend-in-the-paris-terror-attacks-this-girl-just-posted-an-emotional-tribute-to-him-247345.html" title="After Losing Her Boyfriend In The Paris Terror Attacks, This Girl Just Posted An Emotional Tribute To Him!">
                            After Losing Her Boyfriend In The Paris Terror Attacks, This Girl Just Posted An Emotional Tribute To Him!                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_1"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            6 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/india/bombay-high-court-tells-ex-tata-employee-to-stop-defaming-his-employer-on-facebook-247344.html" title="Bombay High Court Tells Ex-TATA Employee To Stop Defaming His Employer On Facebook!" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Nov/main-5_1447588456_1447588462_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/main-5_1447588456_1447588462_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/bombay-high-court-tells-ex-tata-employee-to-stop-defaming-his-employer-on-facebook-247344.html" title="Bombay High Court Tells Ex-TATA Employee To Stop Defaming His Employer On Facebook!">
                            Bombay High Court Tells Ex-TATA Employee To Stop Defaming His Employer On Facebook!                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_2"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            7 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/india/here-s-why-this-business-class-corporate-only-wears-a-white-vest-and-gandhi-cap-to-work-everyday-247342.html" title="Here's Why This Business Class Corporate Only Wears A White Vest And Gandhi Cap To Work Everyday" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Nov/ffff_1447586999_1447587002_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/ffff_1447586999_1447587002_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/here-s-why-this-business-class-corporate-only-wears-a-white-vest-and-gandhi-cap-to-work-everyday-247342.html" title="Here's Why This Business Class Corporate Only Wears A White Vest And Gandhi Cap To Work Everyday">
                            Here's Why This Business Class Corporate Only Wears A White Vest And Gandhi Cap To Work Everyday                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_3"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            7 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/world/france-names-first-parisattacks-terrorist-and-vows-ruthless-action-247341.html" title="France Names First #ParisAttacks Terrorist, And Vows Ruthless Action" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Nov/rts6xdm-5_1447586314_1447586319_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/rts6xdm-5_1447586314_1447586319_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/france-names-first-parisattacks-terrorist-and-vows-ruthless-action-247341.html" title="France Names First #ParisAttacks Terrorist, And Vows Ruthless Action">
                            France Names First #ParisAttacks Terrorist, And Vows Ruthless Action                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                    </div><!--news-panel end-->

        <div class="life-panel cf" id="middleBlock1"><!--life-panel start-->
            <h2>lifestyle</h2>

                                <div class="life-panel-list cf"><!--life-panel-list start-->
                        <figure><span class="label-tag spon">Partner</span>
                            <a class='video-btn sprite' href='http://www.indiatimes.com/culture/who-we-are/the-perfect-video-to-go-with-your-morning-cup-of-coffee-247284.html'>video</a>
                               
                            <a href="http://www.indiatimes.com/culture/who-we-are/the-perfect-video-to-go-with-your-morning-cup-of-coffee-247284.html" class=" tint" title="" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/the-perfect-video-to-go-with-your-morning-cup-of-coffee-247284.html');">
                                <img  class="greyBg23 lazy23"  src="http://media.indiatimes.in/media/videocafe/2015/Nov/nescafe502_1447477868_502x234.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Nov/nescafe502_1447477868_502x234.jpg"  border="0" alt="The Perfect Video To Go With Your Morning Cup Of Coffee!" class="img-responsive"/>
                            </a>
                        </figure>
                        <figcaption>
                            <a href="http://www.indiatimes.com/culture/who-we-are/the-perfect-video-to-go-with-your-morning-cup-of-coffee-247284.html" title="The Perfect Video To Go With Your Morning Cup Of Coffee!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/the-perfect-video-to-go-with-your-morning-cup-of-coffee-247284.html');">The Perfect Video To Go With Your Morning Cup Of Coffee!</a>
                        </figcaption>
                    </div><!--life-panel-list end-->
                

                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                   
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/healthyliving/9-healthy-spices-that-you-should-be-eating-for-their-medicinal-benefits-247254.html" class="tint" title="9 Healthy Spices That You Should Be Eating For Their Medicinal Benefits" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/health/healthyliving/9-healthy-spices-that-you-should-be-eating-for-their-medicinal-benefits-247254.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Nov/card-1_1447396921_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/card-1_1447396921_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/9-healthy-spices-that-you-should-be-eating-for-their-medicinal-benefits-247254.html" title="9 Healthy Spices That You Should Be Eating For Their Medicinal Benefits" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/health/healthyliving/9-healthy-spices-that-you-should-be-eating-for-their-medicinal-benefits-247254.html');">
                            9 Healthy Spices That You Should Be Eating For Their Medicinal Benefits                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                   
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/culture/travel/9-reasons-why-taking-a-gap-year-before-college-might-be-the-best-choice-you-ever-make-247251.html" class="tint" title="9 Reasons Why Taking A Gap Year Before College Might Be The Best Choice You Ever Make!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/travel/9-reasons-why-taking-a-gap-year-before-college-might-be-the-best-choice-you-ever-make-247251.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Nov/cp_1447392609_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/cp_1447392609_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/travel/9-reasons-why-taking-a-gap-year-before-college-might-be-the-best-choice-you-ever-make-247251.html" title="9 Reasons Why Taking A Gap Year Before College Might Be The Best Choice You Ever Make!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/travel/9-reasons-why-taking-a-gap-year-before-college-might-be-the-best-choice-you-ever-make-247251.html');">
                            9 Reasons Why Taking A Gap Year Before College Might Be The Best Choice You Ever Make!                        </a>
                    </figcaption>
                </div>
                        
        </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_0">
                    <span class="strip skyblue-bg"></span>                    <figure>

                                                <a href="http://www.indiatimes.com/videocafe/when-virender-sehwag-hits-a-six-while-singing-tu-jaane-na-that-is-surely-the-best-boundary-ever-247340.html" class="tint" title="When Virender Sehwag Hits A Six While Singing 'Tu Jaane Na' That Is Surely The Best Boundary Ever">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Nov/sehwag_card1a_1447585185_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/sehwag_card1a_1447585185_218x102.jpg" border="0" alt="When Virender Sehwag Hits A Six While Singing 'Tu Jaane Na' That Is Surely The Best Boundary Ever"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/when-virender-sehwag-hits-a-six-while-singing-tu-jaane-na-that-is-surely-the-best-boundary-ever-247340.html" title="When Virender Sehwag Hits A Six While Singing 'Tu Jaane Na' That Is Surely The Best Boundary Ever">
                            When Virender Sehwag Hits A Six While Singing 'Tu Jaane Na' That Is Surely The Best Boundary Ever                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_1">
                                        <figure>

                                                <a href="http://www.indiatimes.com/culture/who-we-are/14-fitting-replies-to-stalkers-who-would-stop-stalking-you-for-good-247247.html" class="tint" title="14 Fitting Replies To Stalkers Who Would Stop Stalking You For Good">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Nov/o-teen-texting-facebook_1447575685_1447575753_1447575773_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/o-teen-texting-facebook_1447575685_1447575753_1447575773_218x102.jpg" border="0" alt="14 Fitting Replies To Stalkers Who Would Stop Stalking You For Good"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/14-fitting-replies-to-stalkers-who-would-stop-stalking-you-for-good-247247.html" title="14 Fitting Replies To Stalkers Who Would Stop Stalking You For Good">
                            14 Fitting Replies To Stalkers Who Would Stop Stalking You For Good                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_2">
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/pahlaj-nihalani-s-thank-you-modi-video-plays-during-prem-ratan-dhan-payo-intervals-and-it-s-hilarious-247343.html'>video</a>                        <a href="http://www.indiatimes.com/entertainment/bollywood/pahlaj-nihalani-s-thank-you-modi-video-plays-during-prem-ratan-dhan-payo-intervals-and-it-s-hilarious-247343.html" class="tint" title="Pahlaj Nihalani's 'Thank You Modi' Video Plays During Prem Ratan Dhan Payo Intervals, And It's Hilarious!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/videocafe/2015/Nov/prdp_card_1447587606_218x102.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Nov/prdp_card_1447587606_218x102.jpg" border="0" alt="Pahlaj Nihalani's 'Thank You Modi' Video Plays During Prem Ratan Dhan Payo Intervals, And It's Hilarious!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/pahlaj-nihalani-s-thank-you-modi-video-plays-during-prem-ratan-dhan-payo-intervals-and-it-s-hilarious-247343.html" title="Pahlaj Nihalani's 'Thank You Modi' Video Plays During Prem Ratan Dhan Payo Intervals, And It's Hilarious!">
                            Pahlaj Nihalani's 'Thank You Modi' Video Plays During Prem Ratan Dhan Payo Intervals, And It's Hilarious!                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_3">
                                        <figure>

                                                <a href="http://www.indiatimes.com/lifestyle/self/14-simple-ways-to-show-love-without-actually-using-the-word-247276.html" class="tint" title="14 Simple Ways To Show Love Without Actually Using The Word">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Nov/1_1447413239_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/1_1447413239_218x102.jpg" border="0" alt="14 Simple Ways To Show Love Without Actually Using The Word"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/14-simple-ways-to-show-love-without-actually-using-the-word-247276.html" title="14 Simple Ways To Show Love Without Actually Using The Word">
                            14 Simple Ways To Show Love Without Actually Using The Word                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_4">
                                        <figure>

                                                <a href="http://www.indiatimes.com/entertainment/salman-khan-s-prdp-shatters-bajrangi-bhaijaan-s-record-mints-rs-100-crore-in-just-3-days-247327.html" class="tint" title="Salman Khan's PRDP Shatters Bajrangi Bhaijaan's Record, Mints Rs.100 Crore In Just 3 Days!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Nov/pr_1447572361_1447572384_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/pr_1447572361_1447572384_218x102.jpg" border="0" alt="Salman Khan's PRDP Shatters Bajrangi Bhaijaan's Record, Mints Rs.100 Crore In Just 3 Days!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/salman-khan-s-prdp-shatters-bajrangi-bhaijaan-s-record-mints-rs-100-crore-in-just-3-days-247327.html" title="Salman Khan's PRDP Shatters Bajrangi Bhaijaan's Record, Mints Rs.100 Crore In Just 3 Days!">
                            Salman Khan's PRDP Shatters Bajrangi Bhaijaan's Record, Mints Rs.100 Crore In Just 3 Days!                        </a>
                    </figcaption>
                </div>
                        </div><!--trending-panel end-->
    </section><!--container end-->

<!-- -------------------    end Block 1  ------------------------------------- -->
<section class="big-ads" id="ad1"> 
<div id="bigAd1_slot"></div>
    <script>        
        showBigAD1('bigAd1_slot');
    </script> 
</section>
<!-- -------------------    start Block 2  ------------------------------------- -->

    <section id="hp_block_2" class="container cf" style="border: 0px solid red;"><!--container start-->
        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
                            <div class="news-panel-list cf" id="column1_4"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            7 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/how-france-used-the-mumbai-attacks-to-plan-security-and-how-india-can-learn-from-parisattacks-247339.html" title="How France Used The Mumbai Attacks To Plan Security, And How India Can Learn From #ParisAttacks" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/main-5_1447584960_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/how-france-used-the-mumbai-attacks-to-plan-security-and-how-india-can-learn-from-parisattacks-247339.html" title="How France Used The Mumbai Attacks To Plan Security, And How India Can Learn From #ParisAttacks">
                            How France Used The Mumbai Attacks To Plan Security, And How India Can Learn From #ParisAttacks                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_5"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            8 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/world/here-s-the-116-photos-that-nasa-sent-aliens-via-the-voyager-spacecraft-247336.html" title="Here's The 116 Photos That NASA Sent Aliens Via The Voyager Spacecraft" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/nasa-5_1447583030_1447583033_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/world/here-s-the-116-photos-that-nasa-sent-aliens-via-the-voyager-spacecraft-247336.html" title="Here's The 116 Photos That NASA Sent Aliens Via The Voyager Spacecraft">
                            Here's The 116 Photos That NASA Sent Aliens Via The Voyager Spacecraft                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_6"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            8 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/world/it-wasn-t-just-a-terrorist-attack-it-was-a-massacre-says-this-paris-attack-survivor-247335.html" title="'It Wasn't Just A Terrorist Attack, It Was A Massacre', Says This Paris Attack Survivor" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/viral502_1447582260_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/world/it-wasn-t-just-a-terrorist-attack-it-was-a-massacre-says-this-paris-attack-survivor-247335.html" title="'It Wasn't Just A Terrorist Attack, It Was A Massacre', Says This Paris Attack Survivor">
                            'It Wasn't Just A Terrorist Attack, It Was A Massacre', Says This Paris Attack Survivor                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_7"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            8 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/captain-ashwani-kumar-killed-by-avalanche-at-siachen-glacier-the-highest-battlefield-on-earth-247334.html" title="Captain Ashwani Kumar Killed By Avalanche At Siachen Glacier, The Highest Battlefield On Earth" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/army-5_1447580868_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/captain-ashwani-kumar-killed-by-avalanche-at-siachen-glacier-the-highest-battlefield-on-earth-247334.html" title="Captain Ashwani Kumar Killed By Avalanche At Siachen Glacier, The Highest Battlefield On Earth">
                            Captain Ashwani Kumar Killed By Avalanche At Siachen Glacier, The Highest Battlefield On Earth                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_8"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            9 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/sports/holly-holm-stuns-the-world-hands-rowdy-ronda-rousey-her-first-mma-loss-ever-247333.html" title="Holly Holm Stuns The World, Hands 'Rowdy' Ronda Rousey Her First MMA Loss Ever" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/ufc-193-holly-beats-ronda-502_1447580549_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/sports/holly-holm-stuns-the-world-hands-rowdy-ronda-rousey-her-first-mma-loss-ever-247333.html" title="Holly Holm Stuns The World, Hands 'Rowdy' Ronda Rousey Her First MMA Loss Ever">
                            Holly Holm Stuns The World, Hands 'Rowdy' Ronda Rousey Her First MMA Loss Ever                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/from-badlapur-to-tamasha-2015-has-been-a-great-year-of-bollywood-s-love-sagas-247337.html" class="tint" title="From Badlapur to Tamasha, 2015 Has Been A Great Year Of Bollywood's Love Sagas!">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cad_1447583815_1447583835_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/from-badlapur-to-tamasha-2015-has-been-a-great-year-of-bollywood-s-love-sagas-247337.html" title="From Badlapur to Tamasha, 2015 Has Been A Great Year Of Bollywood's Love Sagas!">
                            From Badlapur to Tamasha, 2015 Has Been A Great Year Of Bollywood's Love Sagas!                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/pahlaj-nihalani-s-thank-you-modi-video-plays-during-prem-ratan-dhan-payo-intervals-and-it-s-hilarious-247343.html'>video</a>                          
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/bollywood/pahlaj-nihalani-s-thank-you-modi-video-plays-during-prem-ratan-dhan-payo-intervals-and-it-s-hilarious-247343.html" class="tint" title="Pahlaj Nihalani's 'Thank You Modi' Video Plays During Prem Ratan Dhan Payo Intervals, And It's Hilarious!">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/prdp_card_1447587606_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/pahlaj-nihalani-s-thank-you-modi-video-plays-during-prem-ratan-dhan-payo-intervals-and-it-s-hilarious-247343.html" title="Pahlaj Nihalani's 'Thank You Modi' Video Plays During Prem Ratan Dhan Payo Intervals, And It's Hilarious!">
                            Pahlaj Nihalani's 'Thank You Modi' Video Plays During Prem Ratan Dhan Payo Intervals, And It's Hilarious!                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/do-you-know-how-vitamins-work-in-your-body-247268.html'>video</a>                          
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/healthyliving/do-you-know-how-vitamins-work-in-your-body-247268.html" class="tint" title="Do You Know How Vitamins Work In Your Body?">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/card-cgg_1447405418_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/do-you-know-how-vitamins-work-in-your-body-247268.html" title="Do You Know How Vitamins Work In Your Body?">
                            Do You Know How Vitamins Work In Your Body?                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_5"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/health/healthyliving/10-health-problems-that-you-may-have-to-face-if-you-don-t-manage-your-weight-247280.html" class="tint" title="10 Health Problems That You May Have To Face If You Don't Manage Your Weight">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cover_1447416795_218x102.jpg" border="0" alt="10 Health Problems That You May Have To Face If You Don't Manage Your Weight"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/10-health-problems-that-you-may-have-to-face-if-you-don-t-manage-your-weight-247280.html" title="10 Health Problems That You May Have To Face If You Don't Manage Your Weight">
                            10 Health Problems That You May Have To Face If You Don't Manage Your Weight                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_6"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/mandana-rochelle-are-my-favorites-can-win-the-show-says-gautam-gulati-we-re-not-surprised-247319.html" class="tint" title="Mandana, Rochelle Are My Favorites & Can Win The Show, Says Gautam Gulati! We're Not Surprised!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/gauti-card_1447565098_1447565113_218x102.jpg" border="0" alt="Mandana, Rochelle Are My Favorites & Can Win The Show, Says Gautam Gulati! We're Not Surprised!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/mandana-rochelle-are-my-favorites-can-win-the-show-says-gautam-gulati-we-re-not-surprised-247319.html" title="Mandana, Rochelle Are My Favorites & Can Win The Show, Says Gautam Gulati! We're Not Surprised!">
                            Mandana, Rochelle Are My Favorites & Can Win The Show, Says Gautam Gulati! We're Not Surprised!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_7"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/lifestyle/self/15-facts-that-will-leave-you-happy-for-the-rest-of-the-day-247222.html" class="tint" title="15 Facts That Will Leave You Happy For The Rest Of The Day">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/1_1447309231_218x102.jpg" border="0" alt="15 Facts That Will Leave You Happy For The Rest Of The Day"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/15-facts-that-will-leave-you-happy-for-the-rest-of-the-day-247222.html" title="15 Facts That Will Leave You Happy For The Rest Of The Day">
                            15 Facts That Will Leave You Happy For The Rest Of The Day                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div>

    </section><!--container end-->

<!--------------------------- end Block2 ------------------------------------------>
<section class="big-ads" id="ad2"> 
<div id="bigAd2_slot"></div>
</section>
<!--------------------------- start Block3 ------------------------------------------>

    <section id="hp_block_3" class="container cf"><!--container start-->

        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
          
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_9">
                    <figure>
                        <div class="hash-tag">
                            9 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/world/someone-might-have-predicted-the-parisattacks-on-a-french-videogame-forum-almost-a-week-ago-247325.html" title="Did A Spooky Post On A Videogame Forum Predict The Paris Attacks A Week Before They Happened?" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/rts6vx05_1447569768_1447569771_236x111.jpg" border="0" alt="Did A Spooky Post On A Videogame Forum Predict The Paris Attacks A Week Before They Happened?"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/someone-might-have-predicted-the-parisattacks-on-a-french-videogame-forum-almost-a-week-ago-247325.html" title="Did A Spooky Post On A Videogame Forum Predict The Paris Attacks A Week Before They Happened?">
                            Did A Spooky Post On A Videogame Forum Predict The Paris Attacks A Week Before They Happened?                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_10">
                    <figure>
                        <div class="hash-tag">
                            10 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/this-iisc-student-was-blackmailed-threatened-because-he-was-gay-his-fightback-inspires-us-all-247329.html" title="This IISC Student Was Blackmailed, Threatened Because He Was Gay, His Fightback Inspires Us All" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/gay-5_1447572932_236x111.jpg" border="0" alt="This IISC Student Was Blackmailed, Threatened Because He Was Gay, His Fightback Inspires Us All"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/this-iisc-student-was-blackmailed-threatened-because-he-was-gay-his-fightback-inspires-us-all-247329.html" title="This IISC Student Was Blackmailed, Threatened Because He Was Gay, His Fightback Inspires Us All">
                            This IISC Student Was Blackmailed, Threatened Because He Was Gay, His Fightback Inspires Us All                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_11">
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/world/this-father-saved-hundreds-of-lives-during-beirut-bombings-while-sacrificing-his-life-and-his-daughter-s-247323.html" title="This Father Sacrificed His And His Daughterâs Life To Save Hundreds During Beirut Bombings" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/man-daighter-502_1447568116_236x111.jpg" border="0" alt="This Father Sacrificed His And His Daughterâs Life To Save Hundreds During Beirut Bombings"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/this-father-saved-hundreds-of-lives-during-beirut-bombings-while-sacrificing-his-life-and-his-daughter-s-247323.html" title="This Father Sacrificed His And His Daughterâs Life To Save Hundreds During Beirut Bombings">
                            This Father Sacrificed His And His Daughterâs Life To Save Hundreds During Beirut Bombings                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_12">
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/after-paris-attacks-conservatives-find-new-voice-european-nations-quickly-slamming-doors-on-syrian-refugees-247320.html" title="After Paris Attacks Conservatives Find New Voice, European Nations Quickly Slamming Doors On Syrian Refugees" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/refugee-502_1447572157_236x111.jpg" border="0" alt="After Paris Attacks Conservatives Find New Voice, European Nations Quickly Slamming Doors On Syrian Refugees"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/after-paris-attacks-conservatives-find-new-voice-european-nations-quickly-slamming-doors-on-syrian-refugees-247320.html" title="After Paris Attacks Conservatives Find New Voice, European Nations Quickly Slamming Doors On Syrian Refugees">
                            After Paris Attacks Conservatives Find New Voice, European Nations Quickly Slamming Doors On Syrian Refugees                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_13">
                    <figure>
                        <div class="hash-tag">
                            13 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/world/irish-rock-band-u2-s-frontman-bono-calls-paris-attack-first-direct-hit-on-music-247318.html" title="Irish Rock Band U2's Frontman Bono Calls Paris Attack 'First Direct Hit On Music'!" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/u2_1447563239_1447563267_236x111.jpg" border="0" alt="Irish Rock Band U2's Frontman Bono Calls Paris Attack 'First Direct Hit On Music'!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/irish-rock-band-u2-s-frontman-bono-calls-paris-attack-first-direct-hit-on-music-247318.html" title="Irish Rock Band U2's Frontman Bono Calls Paris Attack 'First Direct Hit On Music'!">
                            Irish Rock Band U2's Frontman Bono Calls Paris Attack 'First Direct Hit On Music'!                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/videocafe/when-virender-sehwag-hits-a-six-while-singing-tu-jaane-na-that-is-surely-the-best-boundary-ever-247340.html" class="tint" title="When Virender Sehwag Hits A Six While Singing 'Tu Jaane Na' That Is Surely The Best Boundary Ever">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/sehwag_card1a_1447585185_502x234.jpg" border="0" alt="http://www.indiatimes.com/videocafe/when-virender-sehwag-hits-a-six-while-singing-tu-jaane-na-that-is-surely-the-best-boundary-ever-247340.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/when-virender-sehwag-hits-a-six-while-singing-tu-jaane-na-that-is-surely-the-best-boundary-ever-247340.html" title="When Virender Sehwag Hits A Six While Singing 'Tu Jaane Na' That Is Surely The Best Boundary Ever">
                            When Virender Sehwag Hits A Six While Singing 'Tu Jaane Na' That Is Surely The Best Boundary Ever                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/after-introducing-break-up-parties-in-love-aaj-kal-imtiaz-ali-is-back-with-one-holiday-stand-in-tamasha-247331.html" class="tint" title="After Introducing Break-Up Parties In Love Aaj Kal, Imtiaz Ali Is Back With 'One Holiday Stand' In Tamasha!">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/tamasha-card_1447577508_1447577519_502x234.jpg" border="0" alt="http://www.indiatimes.com/entertainment/after-introducing-break-up-parties-in-love-aaj-kal-imtiaz-ali-is-back-with-one-holiday-stand-in-tamasha-247331.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/after-introducing-break-up-parties-in-love-aaj-kal-imtiaz-ali-is-back-with-one-holiday-stand-in-tamasha-247331.html" title="After Introducing Break-Up Parties In Love Aaj Kal, Imtiaz Ali Is Back With 'One Holiday Stand' In Tamasha!">
                            After Introducing Break-Up Parties In Love Aaj Kal, Imtiaz Ali Is Back With 'One Holiday Stand' In Tamasha!                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/culture/who-we-are/14-fitting-replies-to-stalkers-who-would-stop-stalking-you-for-good-247247.html" class="tint" title="14 Fitting Replies To Stalkers Who Would Stop Stalking You For Good">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/o-teen-texting-facebook_1447575685_1447575753_1447575773_502x234.jpg" border="0" alt="http://www.indiatimes.com/culture/who-we-are/14-fitting-replies-to-stalkers-who-would-stop-stalking-you-for-good-247247.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/14-fitting-replies-to-stalkers-who-would-stop-stalking-you-for-good-247247.html" title="14 Fitting Replies To Stalkers Who Would Stop Stalking You For Good">
                            14 Fitting Replies To Stalkers Who Would Stop Stalking You For Good                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                    </div><!--trending-panel end-->

    </section><!--container end-->

<!------------------------------- end Block 3 ---------------------------------------->
<section class="big-ads">
<div id="bigAd3_slot"></div>
</section>

<!------------------------------- start Block4  -------------------------------------->

    <section class="container cf" id="container4"><!--container start-->
        <div class="news-panel cf ">
            <h2>news</h2>

                            <div class="news-panel-list cf" id="column1_14">
                    <figure>
                        <div class="hash-tag">
                            13 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/india-s-first-railways-university-on-its-way-will-teach-rail-operations-and-management-246982.html" title="India's First Railways University On Its Way, Will Teach Rail Operations And Management" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/rail5_1446634347_236x111.jpg" border="0" alt="India's First Railways University On Its Way, Will Teach Rail Operations And Management"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/india-s-first-railways-university-on-its-way-will-teach-rail-operations-and-management-246982.html" title="India's First Railways University On Its Way, Will Teach Rail Operations And Management">
                            India's First Railways University On Its Way, Will Teach Rail Operations And Management                        </a>
                    </figcaption> 
                </div>
                <div class='container1'>                <div class="news-panel-list cf" id="column1_15">
                    <figure>
                        <div class="hash-tag">
                            13 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/this-chilling-map-shows-isis-plans-for-world-domination-by-2020_-244010.html" title="This Chilling Map Shows ISIS' Plans For World Domination By 2020!" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/isis-world-map=-502_1439215143_236x111.jpg" border="0" alt="This Chilling Map Shows ISIS' Plans For World Domination By 2020!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/this-chilling-map-shows-isis-plans-for-world-domination-by-2020_-244010.html" title="This Chilling Map Shows ISIS' Plans For World Domination By 2020!">
                            This Chilling Map Shows ISIS' Plans For World Domination By 2020!                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_16">
                    <figure>
                        <div class="hash-tag">
                            13 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/8-gripping-before-and-after-images-that-map-isis-s-reign-of-terror-in-syria-one-destruction-at-a-time-246654.html" title="8 Gripping Before-And-After Images That Map ISIS's Reign Of Terror In Syria, One Destruction At A Time" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card_1445926341_236x111.jpg" border="0" alt="8 Gripping Before-And-After Images That Map ISIS's Reign Of Terror In Syria, One Destruction At A Time"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/8-gripping-before-and-after-images-that-map-isis-s-reign-of-terror-in-syria-one-destruction-at-a-time-246654.html" title="8 Gripping Before-And-After Images That Map ISIS's Reign Of Terror In Syria, One Destruction At A Time">
                            8 Gripping Before-And-After Images That Map ISIS's Reign Of Terror In Syria, One Destruction At A Time                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_17">
                    <figure>
                        <div class="hash-tag">
                            14 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/shamed-on-facebook-by-his-vengeful-wife-bengaluru-techie-attempts-suicide-247309.html" title="Shamed On Facebook By His Vengeful Wife, Bengaluru Techie Attempts Suicide" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/shame-card_1447503371_236x111.jpg" border="0" alt="Shamed On Facebook By His Vengeful Wife, Bengaluru Techie Attempts Suicide"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/shamed-on-facebook-by-his-vengeful-wife-bengaluru-techie-attempts-suicide-247309.html" title="Shamed On Facebook By His Vengeful Wife, Bengaluru Techie Attempts Suicide">
                            Shamed On Facebook By His Vengeful Wife, Bengaluru Techie Attempts Suicide                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_18">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/paris-terror-attacks-this-video-of-football-fans-singing-the-french-national-anthem-after-the-terror-attack-will-move-you-247315.html" title="This Video Of Football Fans Singing The French National Anthem After The Terror Attack Will Move You!" class=" tint">
                            <a class='video-btn sprite' href='http://www.indiatimes.com/news/world/paris-terror-attacks-this-video-of-football-fans-singing-the-french-national-anthem-after-the-terror-attack-will-move-you-247315.html'>video</a>
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/parisfootball502_1447518030_236x111.jpg" border="0" alt="This Video Of Football Fans Singing The French National Anthem After The Terror Attack Will Move You!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/paris-terror-attacks-this-video-of-football-fans-singing-the-french-national-anthem-after-the-terror-attack-will-move-you-247315.html" title="This Video Of Football Fans Singing The French National Anthem After The Terror Attack Will Move You!">
                            This Video Of Football Fans Singing The French National Anthem After The Terror Attack Will Move You!                        </a>
                    </figcaption> 
                </div>
                 
        </div>
    </div><!--news-panel end-->

    <div class="life-panel cf container2" id="b4c2"><!--life-panel start-->
        <h2>lifestyle</h2>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/salman-khan-s-prdp-shatters-bajrangi-bhaijaan-s-record-mints-rs-100-crore-in-just-3-days-247327.html" class="tint" title="Salman Khan's PRDP Shatters Bajrangi Bhaijaan's Record, Mints Rs.100 Crore In Just 3 Days!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/pr_1447572361_1447572384_502x234.jpg" border="0" alt="Salman Khan's PRDP Shatters Bajrangi Bhaijaan's Record, Mints Rs.100 Crore In Just 3 Days!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/salman-khan-s-prdp-shatters-bajrangi-bhaijaan-s-record-mints-rs-100-crore-in-just-3-days-247327.html" title="Salman Khan's PRDP Shatters Bajrangi Bhaijaan's Record, Mints Rs.100 Crore In Just 3 Days!">
                        Salman Khan's PRDP Shatters Bajrangi Bhaijaan's Record, Mints Rs.100 Crore In Just 3 Days!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/when-maggi-comes-back-what-will-happen-to-all-those-other-noodles-unke-toh-m-lag-gaye-maggireturns-247322.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/when-maggi-comes-back-what-will-happen-to-all-those-other-noodles-unke-toh-m-lag-gaye-maggireturns-247322.html" class="tint" title="When Maggi Comes Back, What Will Happen To All Those Other Noodles. Unke Toh 'M Lag Gaye' #MaggiReturns">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/maggi_card_1447567228_502x234.jpg" border="0" alt="When Maggi Comes Back, What Will Happen To All Those Other Noodles. Unke Toh 'M Lag Gaye' #MaggiReturns" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/when-maggi-comes-back-what-will-happen-to-all-those-other-noodles-unke-toh-m-lag-gaye-maggireturns-247322.html" title="When Maggi Comes Back, What Will Happen To All Those Other Noodles. Unke Toh 'M Lag Gaye' #MaggiReturns">
                        When Maggi Comes Back, What Will Happen To All Those Other Noodles. Unke Toh 'M Lag Gaye' #MaggiReturns                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/hollywood/spectre-is-out-here-are-00-7-reasons-you-should-not-miss-the-latest-james-bond-extravaganza-246737.html" class="tint" title="Spectre Is Out! Here Are (00)7 Reasons You Should Not Miss The Latest James Bond Extravaganza">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/bond-502_1446037458_502x234.jpg" border="0" alt="Spectre Is Out! Here Are (00)7 Reasons You Should Not Miss The Latest James Bond Extravaganza" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/hollywood/spectre-is-out-here-are-00-7-reasons-you-should-not-miss-the-latest-james-bond-extravaganza-246737.html" title="Spectre Is Out! Here Are (00)7 Reasons You Should Not Miss The Latest James Bond Extravaganza">
                        Spectre Is Out! Here Are (00)7 Reasons You Should Not Miss The Latest James Bond Extravaganza                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/10-health-problems-that-you-may-have-to-face-if-you-don-t-manage-your-weight-247280.html" class="tint" title="10 Health Problems That You May Have To Face If You Don't Manage Your Weight">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cover_1447416795_502x234.jpg" border="0" alt="10 Health Problems That You May Have To Face If You Don't Manage Your Weight" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/10-health-problems-that-you-may-have-to-face-if-you-don-t-manage-your-weight-247280.html" title="10 Health Problems That You May Have To Face If You Don't Manage Your Weight">
                        10 Health Problems That You May Have To Face If You Don't Manage Your Weight                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/prejudices-against-communities-are-dividing-the-world-says-amitabh-bachchan-247326.html" class="tint" title="Prejudices Against Communities Are Dividing The World, Says Amitabh Bachchan">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/ab1_1447570409_1447570419_502x234.jpg" border="0" alt="Prejudices Against Communities Are Dividing The World, Says Amitabh Bachchan" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/prejudices-against-communities-are-dividing-the-world-says-amitabh-bachchan-247326.html" title="Prejudices Against Communities Are Dividing The World, Says Amitabh Bachchan">
                        Prejudices Against Communities Are Dividing The World, Says Amitabh Bachchan                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/14-simple-ways-to-show-love-without-actually-using-the-word-247276.html" class="tint" title="14 Simple Ways To Show Love Without Actually Using The Word">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/1_1447413239_502x234.jpg" border="0" alt="14 Simple Ways To Show Love Without Actually Using The Word" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/14-simple-ways-to-show-love-without-actually-using-the-word-247276.html" title="14 Simple Ways To Show Love Without Actually Using The Word">
                        14 Simple Ways To Show Love Without Actually Using The Word                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/mandana-rochelle-are-my-favorites-can-win-the-show-says-gautam-gulati-we-re-not-surprised-247319.html" class="tint" title="Mandana, Rochelle Are My Favorites & Can Win The Show, Says Gautam Gulati! We're Not Surprised!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/gauti-card_1447565098_1447565113_502x234.jpg" border="0" alt="Mandana, Rochelle Are My Favorites & Can Win The Show, Says Gautam Gulati! We're Not Surprised!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/mandana-rochelle-are-my-favorites-can-win-the-show-says-gautam-gulati-we-re-not-surprised-247319.html" title="Mandana, Rochelle Are My Favorites & Can Win The Show, Says Gautam Gulati! We're Not Surprised!">
                        Mandana, Rochelle Are My Favorites & Can Win The Show, Says Gautam Gulati! We're Not Surprised!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/if-bollywood-dance-is-your-thing-this-quick-cardio-workout-is-the-perfect-way-to-burn-calories-247281.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/if-bollywood-dance-is-your-thing-this-quick-cardio-workout-is-the-perfect-way-to-burn-calories-247281.html" class="tint" title="If Bollywood Dance Is Your Thing, This Quick Cardio Workout Is The Perfect Way To Burn Calories!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/cover_1447418596_502x234.jpg" border="0" alt="If Bollywood Dance Is Your Thing, This Quick Cardio Workout Is The Perfect Way To Burn Calories!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/if-bollywood-dance-is-your-thing-this-quick-cardio-workout-is-the-perfect-way-to-burn-calories-247281.html" title="If Bollywood Dance Is Your Thing, This Quick Cardio Workout Is The Perfect Way To Burn Calories!">
                        If Bollywood Dance Is Your Thing, This Quick Cardio Workout Is The Perfect Way To Burn Calories!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/four-guys-share-their-stories-over-a-game-of-carrom-it-s-as-horrifying-as-the-paris-attacks-247317.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/four-guys-share-their-stories-over-a-game-of-carrom-it-s-as-horrifying-as-the-paris-attacks-247317.html" class="tint" title="Four Guys Share Their Stories Over A Game Of Carrom & It's As Horrifying As The Paris Attacks!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/carrom_card_1447562603_502x234.jpg" border="0" alt="Four Guys Share Their Stories Over A Game Of Carrom & It's As Horrifying As The Paris Attacks!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/four-guys-share-their-stories-over-a-game-of-carrom-it-s-as-horrifying-as-the-paris-attacks-247317.html" title="Four Guys Share Their Stories Over A Game Of Carrom & It's As Horrifying As The Paris Attacks!">
                        Four Guys Share Their Stories Over A Game Of Carrom & It's As Horrifying As The Paris Attacks!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/15-facts-that-will-leave-you-happy-for-the-rest-of-the-day-247222.html" class="tint" title="15 Facts That Will Leave You Happy For The Rest Of The Day">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/1_1447309231_502x234.jpg" border="0" alt="15 Facts That Will Leave You Happy For The Rest Of The Day" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/15-facts-that-will-leave-you-happy-for-the-rest-of-the-day-247222.html" title="15 Facts That Will Leave You Happy For The Rest Of The Day">
                        15 Facts That Will Leave You Happy For The Rest Of The Day                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/this-is-how-salman-khan-became-prem-his-first-screen-test-for-maine-pyar-kiya-247308.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/this-is-how-salman-khan-became-prem-his-first-screen-test-for-maine-pyar-kiya-247308.html" class="tint" title="This Is How Salman Khan Became 'Prem'. His First Screen Test For Maine Pyar Kiya!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/picmonkey-collage_1447502380_1447502386_502x234.jpg" border="0" alt="This Is How Salman Khan Became 'Prem'. His First Screen Test For Maine Pyar Kiya!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/this-is-how-salman-khan-became-prem-his-first-screen-test-for-maine-pyar-kiya-247308.html" title="This Is How Salman Khan Became 'Prem'. His First Screen Test For Maine Pyar Kiya!">
                        This Is How Salman Khan Became 'Prem'. His First Screen Test For Maine Pyar Kiya!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/whoa-sylvester-stallone-might-replace-sanjay-dutt-in-salman-khan-s-upcoming-film-sultan-247304.html" class="tint" title="Whoa! Sylvester Stallone Might Replace Sanjay Dutt In Salman Khan's Upcoming Film Sultan!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1447497588_1447497594_502x234.jpg" border="0" alt="Whoa! Sylvester Stallone Might Replace Sanjay Dutt In Salman Khan's Upcoming Film Sultan!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/whoa-sylvester-stallone-might-replace-sanjay-dutt-in-salman-khan-s-upcoming-film-sultan-247304.html" title="Whoa! Sylvester Stallone Might Replace Sanjay Dutt In Salman Khan's Upcoming Film Sultan!">
                        Whoa! Sylvester Stallone Might Replace Sanjay Dutt In Salman Khan's Upcoming Film Sultan!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/these-dutch-cops-play-cook-and-cleaner-to-five-children-when-their-mother-fell-sick-respect-247301.html" class="tint" title="These Dutch Cops Play Cook And Cleaner To Five Children When Their Mother Fell Sick #Respect">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1447495182_502x234.jpg" border="0" alt="These Dutch Cops Play Cook And Cleaner To Five Children When Their Mother Fell Sick #Respect" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/these-dutch-cops-play-cook-and-cleaner-to-five-children-when-their-mother-fell-sick-respect-247301.html" title="These Dutch Cops Play Cook And Cleaner To Five Children When Their Mother Fell Sick #Respect">
                        These Dutch Cops Play Cook And Cleaner To Five Children When Their Mother Fell Sick #Respect                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/katrina-kaif-feels-talking-about-salman-disrespects-her-existing-relationship-okay-then-247299.html" class="tint" title="Katrina Kaif Feels Talking About Salman Disrespects Her 'Existing Relationship'. Okay Then!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1447492798_1447492804_502x234.jpg" border="0" alt="Katrina Kaif Feels Talking About Salman Disrespects Her 'Existing Relationship'. Okay Then!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/katrina-kaif-feels-talking-about-salman-disrespects-her-existing-relationship-okay-then-247299.html" title="Katrina Kaif Feels Talking About Salman Disrespects Her 'Existing Relationship'. Okay Then!">
                        Katrina Kaif Feels Talking About Salman Disrespects Her 'Existing Relationship'. Okay Then!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/travel/17-scenic-images-of-winters-in-kashmir-that-could-easily-model-as-landscape-artwork-247228.html" class="tint" title="17 Scenic Images Of Winters In Kashmir That Could Easily Model As Landscape Artwork">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cp_1447312040_1447312045_502x234.jpg" border="0" alt="17 Scenic Images Of Winters In Kashmir That Could Easily Model As Landscape Artwork" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/travel/17-scenic-images-of-winters-in-kashmir-that-could-easily-model-as-landscape-artwork-247228.html" title="17 Scenic Images Of Winters In Kashmir That Could Easily Model As Landscape Artwork">
                        17 Scenic Images Of Winters In Kashmir That Could Easily Model As Landscape Artwork                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/8-bollywood-films-where-the-hero-whisked-away-the-girl-from-the-airport-in-the-climax-247267.html" class="tint" title="8 Bollywood Films Where The Hero Whisked Away The Girl From The Airport In The Climax!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1447404461_1447404469_502x234.jpg" border="0" alt="8 Bollywood Films Where The Hero Whisked Away The Girl From The Airport In The Climax!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/8-bollywood-films-where-the-hero-whisked-away-the-girl-from-the-airport-in-the-climax-247267.html" title="8 Bollywood Films Where The Hero Whisked Away The Girl From The Airport In The Climax!">
                        8 Bollywood Films Where The Hero Whisked Away The Girl From The Airport In The Climax!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/this-harmless-image-of-actress-shruthi-menon-is-taking-the-internet-by-storm-but-why-247291.html" class="tint" title="This Harmless Image Of Actress Shruthi Menon Is Taking The Internet By Storm. But Why?">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/shruti_0_1447481347_1447481353_502x234.jpg" border="0" alt="This Harmless Image Of Actress Shruthi Menon Is Taking The Internet By Storm. But Why?" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/this-harmless-image-of-actress-shruthi-menon-is-taking-the-internet-by-storm-but-why-247291.html" title="This Harmless Image Of Actress Shruthi Menon Is Taking The Internet By Storm. But Why?">
                        This Harmless Image Of Actress Shruthi Menon Is Taking The Internet By Storm. But Why?                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/you-won-t-believe-how-fascinating-it-is-to-watch-soan-papdi-being-made-from-scratch-247294.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/you-won-t-believe-how-fascinating-it-is-to-watch-soan-papdi-being-made-from-scratch-247294.html" class="tint" title="You Won't Believe How Fascinating It Is To Watch Soan Papdi Being Made From Scratch">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/card_1447483759_1447483768_502x234.jpg" border="0" alt="You Won't Believe How Fascinating It Is To Watch Soan Papdi Being Made From Scratch" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/you-won-t-believe-how-fascinating-it-is-to-watch-soan-papdi-being-made-from-scratch-247294.html" title="You Won't Believe How Fascinating It Is To Watch Soan Papdi Being Made From Scratch">
                        You Won't Believe How Fascinating It Is To Watch Soan Papdi Being Made From Scratch                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/style/11-struggles-of-men-who-just-can-t-seem-to-grow-a-full-beard-247274.html" class="tint" title="11 Struggles Of Men Who Just Can't Seem To Grow A Full Beard">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1447487183_502x234.jpg" border="0" alt="11 Struggles Of Men Who Just Can't Seem To Grow A Full Beard" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/style/11-struggles-of-men-who-just-can-t-seem-to-grow-a-full-beard-247274.html" title="11 Struggles Of Men Who Just Can't Seem To Grow A Full Beard">
                        11 Struggles Of Men Who Just Can't Seem To Grow A Full Beard                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/10-rare-photos-that-show-chacha-nehrus-love-for-children-228391.html" class="tint" title="#RememberingNehru 13 Pictures That Show Chacha Nehru's Love For Children">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2014/Nov/nehru_1415864366_1415864371_502x234.jpg" border="0" alt="#RememberingNehru 13 Pictures That Show Chacha Nehru's Love For Children" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/10-rare-photos-that-show-chacha-nehrus-love-for-children-228391.html" title="#RememberingNehru 13 Pictures That Show Chacha Nehru's Love For Children">
                        #RememberingNehru 13 Pictures That Show Chacha Nehru's Love For Children                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/the-knife-you-use-in-the-kitchen-is-spreading-bacteria-between-foods-247231.html" class="tint" title="The Knife You Use In The Kitchen Is Spreading Bacteria Between Foods">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card-2_1447316526_502x234.jpg" border="0" alt="The Knife You Use In The Kitchen Is Spreading Bacteria Between Foods" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/the-knife-you-use-in-the-kitchen-is-spreading-bacteria-between-foods-247231.html" title="The Knife You Use In The Kitchen Is Spreading Bacteria Between Foods">
                        The Knife You Use In The Kitchen Is Spreading Bacteria Between Foods                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/this-is-the-ultimate-14-day-post-diwali-detox-challenge-think-you-re-up-for-it-247257.html" class="tint" title="This Is The Ultimate 14 Day Post Diwali Detox Challenge - Think Youâre Up For It?">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cover_1447399512_502x234.jpg" border="0" alt="This Is The Ultimate 14 Day Post Diwali Detox Challenge - Think Youâre Up For It?" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/this-is-the-ultimate-14-day-post-diwali-detox-challenge-think-you-re-up-for-it-247257.html" title="This Is The Ultimate 14 Day Post Diwali Detox Challenge - Think Youâre Up For It?">
                        This Is The Ultimate 14 Day Post Diwali Detox Challenge - Think Youâre Up For It?                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/9-simple-things-you-can-do-to-make-a-good-first-impression-247235.html" class="tint" title="9 Simple Things You Can Do To Make A Good First Impression!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card-7_1447321599_502x234.jpg" border="0" alt="9 Simple Things You Can Do To Make A Good First Impression!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/9-simple-things-you-can-do-to-make-a-good-first-impression-247235.html" title="9 Simple Things You Can Do To Make A Good First Impression!">
                        9 Simple Things You Can Do To Make A Good First Impression!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/want-to-make-a-lasting-impression-in-that-office-meeting-just-use-the-word-yeah-more-often-247273.html" class="tint" title="Want To Make A Lasting Impression In That Office Meeting? Just Use The Word 'Yeah' More Often">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1447409426_502x234.jpg" border="0" alt="Want To Make A Lasting Impression In That Office Meeting? Just Use The Word 'Yeah' More Often" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/want-to-make-a-lasting-impression-in-that-office-meeting-just-use-the-word-yeah-more-often-247273.html" title="Want To Make A Lasting Impression In That Office Meeting? Just Use The Word 'Yeah' More Often">
                        Want To Make A Lasting Impression In That Office Meeting? Just Use The Word 'Yeah' More Often                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/videos/these-12-fabulous-photos-of-people-doing-yoga-in-the-mountains-will-take-your-breath-away-247272.html" class="tint" title="These 12 Fabulous Photos Of People Doing Yoga In The Mountains Will Take Your Breath Away!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cover_1447407652_502x234.jpg" border="0" alt="These 12 Fabulous Photos Of People Doing Yoga In The Mountains Will Take Your Breath Away!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/videos/these-12-fabulous-photos-of-people-doing-yoga-in-the-mountains-will-take-your-breath-away-247272.html" title="These 12 Fabulous Photos Of People Doing Yoga In The Mountains Will Take Your Breath Away!">
                        These 12 Fabulous Photos Of People Doing Yoga In The Mountains Will Take Your Breath Away!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/29-fantastic-pieces-of-random-advice-that-you-should-remember-at-all-times-247183.html" class="tint" title="29 Fantastic Pieces Of Random Advice That You Should Remember At All Times">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1447158317_502x234.jpg" border="0" alt="29 Fantastic Pieces Of Random Advice That You Should Remember At All Times" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/29-fantastic-pieces-of-random-advice-that-you-should-remember-at-all-times-247183.html" title="29 Fantastic Pieces Of Random Advice That You Should Remember At All Times">
                        29 Fantastic Pieces Of Random Advice That You Should Remember At All Times                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/here-s-why-you-feel-feverish-when-you-re-under-a-lot-of-stress-247227.html" class="tint" title="Here's Why You Feel Feverish When You're Under A Lot Of Stress">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cover_1447313201_502x234.jpg" border="0" alt="Here's Why You Feel Feverish When You're Under A Lot Of Stress" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/here-s-why-you-feel-feverish-when-you-re-under-a-lot-of-stress-247227.html" title="Here's Why You Feel Feverish When You're Under A Lot Of Stress">
                        Here's Why You Feel Feverish When You're Under A Lot Of Stress                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/adnan-sami-s-indian-citizenship-dreams-shattered-as-pakistan-refuses-to-revoke-his-nationality-247256.html" class="tint" title="Adnan Sami's Indian Citizenship Dreams Shattered As Pakistan Refuses To Revoke His Nationality">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cacc_1447397375_1447397381_502x234.jpg" border="0" alt="Adnan Sami's Indian Citizenship Dreams Shattered As Pakistan Refuses To Revoke His Nationality" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/adnan-sami-s-indian-citizenship-dreams-shattered-as-pakistan-refuses-to-revoke-his-nationality-247256.html" title="Adnan Sami's Indian Citizenship Dreams Shattered As Pakistan Refuses To Revoke His Nationality">
                        Adnan Sami's Indian Citizenship Dreams Shattered As Pakistan Refuses To Revoke His Nationality                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/6-epic-dialogues-from-gulaal-that-prove-bollywood-needs-more-such-kickass-films-247243.html" class="tint" title="6 Epic Dialogues From 'Gulaal' That Prove Bollywood Needs More Such Kickass Films!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/nn_1447330687_502x234.jpg" border="0" alt="6 Epic Dialogues From 'Gulaal' That Prove Bollywood Needs More Such Kickass Films!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/6-epic-dialogues-from-gulaal-that-prove-bollywood-needs-more-such-kickass-films-247243.html" title="6 Epic Dialogues From 'Gulaal' That Prove Bollywood Needs More Such Kickass Films!">
                        6 Epic Dialogues From 'Gulaal' That Prove Bollywood Needs More Such Kickass Films!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/5-yoga-poses-that-promises-beautiful-skin-and-glowing-skin-240220.html" class="tint" title="9 Yoga Poses That Promise Beautiful And Glowing Skin">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card-3_1447320868_502x234.jpg" border="0" alt="9 Yoga Poses That Promise Beautiful And Glowing Skin" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/5-yoga-poses-that-promises-beautiful-skin-and-glowing-skin-240220.html" title="9 Yoga Poses That Promise Beautiful And Glowing Skin">
                        9 Yoga Poses That Promise Beautiful And Glowing Skin                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/12-life-struggles-turned-into-hard-hitting-nursery-rhymes-247162.html" class="tint" title="12 Life Struggles Turned Into Hard-Hitting Nursery Rhymes">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage2_1447144362_1447144371_502x234.jpg" border="0" alt="12 Life Struggles Turned Into Hard-Hitting Nursery Rhymes" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/12-life-struggles-turned-into-hard-hitting-nursery-rhymes-247162.html" title="12 Life Struggles Turned Into Hard-Hitting Nursery Rhymes">
                        12 Life Struggles Turned Into Hard-Hitting Nursery Rhymes                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/6-food-replacements-you-should-make-on-your-dining-table-this-festive-season-247155.html" class="tint" title="6 Food Replacements You Should Make On Your Dining Table This Festive Season">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card-1_1447139452_502x234.jpg" border="0" alt="6 Food Replacements You Should Make On Your Dining Table This Festive Season" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/6-food-replacements-you-should-make-on-your-dining-table-this-festive-season-247155.html" title="6 Food Replacements You Should Make On Your Dining Table This Festive Season">
                        6 Food Replacements You Should Make On Your Dining Table This Festive Season                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/7-thoughts-you-have-when-you-are-considering-further-studies-247022.html" class="tint" title="7 Thoughts You Have When You Are Considering Further Studies">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cp_1446723291_502x234.jpg" border="0" alt="7 Thoughts You Have When You Are Considering Further Studies" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/7-thoughts-you-have-when-you-are-considering-further-studies-247022.html" title="7 Thoughts You Have When You Are Considering Further Studies">
                        7 Thoughts You Have When You Are Considering Further Studies                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/14-everyday-things-that-look-like-a-herculean-task-in-space-247167.html" class="tint" title="14 Everyday Things That Look Like A Herculean Task In Space">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card-2_1447149332_502x234.jpg" border="0" alt="14 Everyday Things That Look Like A Herculean Task In Space" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/14-everyday-things-that-look-like-a-herculean-task-in-space-247167.html" title="14 Everyday Things That Look Like A Herculean Task In Space">
                        14 Everyday Things That Look Like A Herculean Task In Space                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/12-small-changes-that-will-help-you-keep-diabetes-at-bay-247225.html" class="tint" title="12 Small Changes That Will Help You Keep Diabetes At Bay">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card1_1447314932_502x234.jpg" border="0" alt="12 Small Changes That Will Help You Keep Diabetes At Bay" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/12-small-changes-that-will-help-you-keep-diabetes-at-bay-247225.html" title="12 Small Changes That Will Help You Keep Diabetes At Bay">
                        12 Small Changes That Will Help You Keep Diabetes At Bay                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/11-foolproof-ways-that-will-help-you-keep-secrets-to-yourself-247134.html" class="tint" title="11 Foolproof Ways That Will Help You Keep Secrets To Yourself">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/fb_1447071401_1447071413_502x234.jpg" border="0" alt="11 Foolproof Ways That Will Help You Keep Secrets To Yourself" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/11-foolproof-ways-that-will-help-you-keep-secrets-to-yourself-247134.html" title="11 Foolproof Ways That Will Help You Keep Secrets To Yourself">
                        11 Foolproof Ways That Will Help You Keep Secrets To Yourself                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/nawazuddin-siddiqui-and-sunny-leone-might-star-together-in-a-film-now-that-s-going-to-be-quite-a-couple-247226.html" class="tint" title="Nawazuddin Siddiqui And Sunny Leone Might Star Together In A Film. Now That's Going To Be Quite A Couple!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1447311020_1447311023_502x234.jpg" border="0" alt="Nawazuddin Siddiqui And Sunny Leone Might Star Together In A Film. Now That's Going To Be Quite A Couple!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/nawazuddin-siddiqui-and-sunny-leone-might-star-together-in-a-film-now-that-s-going-to-be-quite-a-couple-247226.html" title="Nawazuddin Siddiqui And Sunny Leone Might Star Together In A Film. Now That's Going To Be Quite A Couple!">
                        Nawazuddin Siddiqui And Sunny Leone Might Star Together In A Film. Now That's Going To Be Quite A Couple!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/the-effects-of-energy-drinks-on-your-blood-pressure-and-heart-rate-are-worse-than-you-thought-247151.html" class="tint" title="The Effects Of Energy Drinks On Your Blood Pressure And Heart Rate Are Worse Than You Thought!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cover_1447155488_502x234.jpg" border="0" alt="The Effects Of Energy Drinks On Your Blood Pressure And Heart Rate Are Worse Than You Thought!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/the-effects-of-energy-drinks-on-your-blood-pressure-and-heart-rate-are-worse-than-you-thought-247151.html" title="The Effects Of Energy Drinks On Your Blood Pressure And Heart Rate Are Worse Than You Thought!">
                        The Effects Of Energy Drinks On Your Blood Pressure And Heart Rate Are Worse Than You Thought!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/world-s-first-twitter-lyric-video-features-baaki-baatein-peene-baad-it-s-bloody-brilliant-247180.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/world-s-first-twitter-lyric-video-features-baaki-baatein-peene-baad-it-s-bloody-brilliant-247180.html" class="tint" title="World's First Twitter Lyric Video Features 'Baaki Baatein Peene Baad' & It's Bloody Brilliant!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/badshah-card-image_1447317179_1447317189_502x234.jpg" border="0" alt="World's First Twitter Lyric Video Features 'Baaki Baatein Peene Baad' & It's Bloody Brilliant!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/world-s-first-twitter-lyric-video-features-baaki-baatein-peene-baad-it-s-bloody-brilliant-247180.html" title="World's First Twitter Lyric Video Features 'Baaki Baatein Peene Baad' & It's Bloody Brilliant!">
                        World's First Twitter Lyric Video Features 'Baaki Baatein Peene Baad' & It's Bloody Brilliant!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-1-minute-video-will-remind-you-why-judging-a-book-by-its-cover-is-always-a-bad-idea-247168.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-1-minute-video-will-remind-you-why-judging-a-book-by-its-cover-is-always-a-bad-idea-247168.html" class="tint" title="This 1-Minute Video Will Remind You Why Judging A Book By Its Cover Is Always A Bad Idea!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/judgemental_card_1447148310_502x234.jpg" border="0" alt="This 1-Minute Video Will Remind You Why Judging A Book By Its Cover Is Always A Bad Idea!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-1-minute-video-will-remind-you-why-judging-a-book-by-its-cover-is-always-a-bad-idea-247168.html" title="This 1-Minute Video Will Remind You Why Judging A Book By Its Cover Is Always A Bad Idea!">
                        This 1-Minute Video Will Remind You Why Judging A Book By Its Cover Is Always A Bad Idea!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/you-can-t-speak-your-mind-in-this-country-says-ranbir-14-things-ranbir-deepika-said-at-tamasha-diwali-party-247218.html" class="tint" title="You Can't Speak Your Mind In This Country, Says Ranbir + 14 Things Ranbir-Deepika Said At 'Tamasha' Diwali Party">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/rd_1447316485_1447316492_502x234.jpg" border="0" alt="You Can't Speak Your Mind In This Country, Says Ranbir + 14 Things Ranbir-Deepika Said At 'Tamasha' Diwali Party" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/you-can-t-speak-your-mind-in-this-country-says-ranbir-14-things-ranbir-deepika-said-at-tamasha-diwali-party-247218.html" title="You Can't Speak Your Mind In This Country, Says Ranbir + 14 Things Ranbir-Deepika Said At 'Tamasha' Diwali Party">
                        You Can't Speak Your Mind In This Country, Says Ranbir + 14 Things Ranbir-Deepika Said At 'Tamasha' Diwali Party                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/this-parody-video-of-prem-ratan-dhan-payo-is-brutally-honest-and-bloody-hilarious-247221.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/this-parody-video-of-prem-ratan-dhan-payo-is-brutally-honest-and-bloody-hilarious-247221.html" class="tint" title="You Might Actually Fall Off Laughing After Watching This Parody Video Of Prem Ratan Dhan Payo!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/screenshot_3_1447308407_1447308415_502x234.jpg" border="0" alt="You Might Actually Fall Off Laughing After Watching This Parody Video Of Prem Ratan Dhan Payo!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/this-parody-video-of-prem-ratan-dhan-payo-is-brutally-honest-and-bloody-hilarious-247221.html" title="You Might Actually Fall Off Laughing After Watching This Parody Video Of Prem Ratan Dhan Payo!">
                        You Might Actually Fall Off Laughing After Watching This Parody Video Of Prem Ratan Dhan Payo!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/6-things-you-can-stop-doing-this-diwali-247122.html" class="tint" title="6 Things You Can Stop Doing This Diwali!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1447058308_502x234.jpg" border="0" alt="6 Things You Can Stop Doing This Diwali!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/6-things-you-can-stop-doing-this-diwali-247122.html" title="6 Things You Can Stop Doing This Diwali!">
                        6 Things You Can Stop Doing This Diwali!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/7-reasons-why-jamie-oliver-s-anti-sugar-campaign-makes-sense-246806.html" class="tint" title="7 Reasons Why Jamie Oliver's Anti-Sugar Campaign Makes Sense">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/cover_1446201271_502x234.jpg" border="0" alt="7 Reasons Why Jamie Oliver's Anti-Sugar Campaign Makes Sense" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/7-reasons-why-jamie-oliver-s-anti-sugar-campaign-makes-sense-246806.html" title="7 Reasons Why Jamie Oliver's Anti-Sugar Campaign Makes Sense">
                        7 Reasons Why Jamie Oliver's Anti-Sugar Campaign Makes Sense                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/a-family-in-mumbai-is-left-homeless-because-of-a-diwali-rocket-at-least-now-say-no-to-crackers-247215.html" class="tint" title="A Family In Mumbai Is Left Homeless Because Of A Diwali Rocket. At Least Now Say No To Crackers!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/fire-bu_1447250507_1447250516_1447250528_502x234.jpg" border="0" alt="A Family In Mumbai Is Left Homeless Because Of A Diwali Rocket. At Least Now Say No To Crackers!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/a-family-in-mumbai-is-left-homeless-because-of-a-diwali-rocket-at-least-now-say-no-to-crackers-247215.html" title="A Family In Mumbai Is Left Homeless Because Of A Diwali Rocket. At Least Now Say No To Crackers!">
                        A Family In Mumbai Is Left Homeless Because Of A Diwali Rocket. At Least Now Say No To Crackers!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/15-pictures-that-show-what-struggles-of-real-life-are-like-247135.html" class="tint" title="15 Pictures That Show What Struggles Of Real Life Are Like">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/srk_1447394969_502x234.jpg" border="0" alt="15 Pictures That Show What Struggles Of Real Life Are Like" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/15-pictures-that-show-what-struggles-of-real-life-are-like-247135.html" title="15 Pictures That Show What Struggles Of Real Life Are Like">
                        15 Pictures That Show What Struggles Of Real Life Are Like                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/7-things-you-won-t-believe-actually-contain-alcohol-247187.html" class="tint" title="7 Things You Wonât Believe Actually Contain Alcohol">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cough_1447159956_1447159967_502x234.jpg" border="0" alt="7 Things You Wonât Believe Actually Contain Alcohol" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/7-things-you-won-t-believe-actually-contain-alcohol-247187.html" title="7 Things You Wonât Believe Actually Contain Alcohol">
                        7 Things You Wonât Believe Actually Contain Alcohol                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/beau-virat-kohli-is-backing-anushka-sharma-for-an-animal-friendly-pawsitive-diwali-247209.html" class="tint" title="Beau Virat Kohli Is Backing Anushka Sharma For An 'Animal Friendly' & #Pawsitive Diwali!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/nkn_1447238180_1447238190_502x234.jpg" border="0" alt="Beau Virat Kohli Is Backing Anushka Sharma For An 'Animal Friendly' & #Pawsitive Diwali!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/beau-virat-kohli-is-backing-anushka-sharma-for-an-animal-friendly-pawsitive-diwali-247209.html" title="Beau Virat Kohli Is Backing Anushka Sharma For An 'Animal Friendly' & #Pawsitive Diwali!">
                        Beau Virat Kohli Is Backing Anushka Sharma For An 'Animal Friendly' & #Pawsitive Diwali!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/enforcement-directorate-quizzes-shah-rukh-khan-for-3-hours-for-financial-irregularities-fema-violations-247207.html" class="tint" title="Enforcement Directorate Quizzes Shah Rukh Khan For 3 Hours For Financial Irregularities & FEMA Violations!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/srk-card_1447314540_1447314547_502x234.jpg" border="0" alt="Enforcement Directorate Quizzes Shah Rukh Khan For 3 Hours For Financial Irregularities & FEMA Violations!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/enforcement-directorate-quizzes-shah-rukh-khan-for-3-hours-for-financial-irregularities-fema-violations-247207.html" title="Enforcement Directorate Quizzes Shah Rukh Khan For 3 Hours For Financial Irregularities & FEMA Violations!">
                        Enforcement Directorate Quizzes Shah Rukh Khan For 3 Hours For Financial Irregularities & FEMA Violations!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/ranbir-katrina-hrithik-other-stars-are-still-quiet-a-pathaka-they-re-helping-sell-firecrackers-247202.html" class="tint" title="Ranbir, Katrina, Hrithik & Other Stars Are Still Quite A 'Pathaka' & They're Helping Sell Firecrackers">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/fire-crackers_1447228369_1447228388_502x234.jpg" border="0" alt="Ranbir, Katrina, Hrithik & Other Stars Are Still Quite A 'Pathaka' & They're Helping Sell Firecrackers" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/ranbir-katrina-hrithik-other-stars-are-still-quiet-a-pathaka-they-re-helping-sell-firecrackers-247202.html" title="Ranbir, Katrina, Hrithik & Other Stars Are Still Quite A 'Pathaka' & They're Helping Sell Firecrackers">
                        Ranbir, Katrina, Hrithik & Other Stars Are Still Quite A 'Pathaka' & They're Helping Sell Firecrackers                    </a>
                </figcaption>
            </div>
        
        <script id="HpMiddleBlock_tpl" type="text/x-handlebars-template">
            {{#if MoreData}}
            {{#each MoreData}} 
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>


                    <a  class='video-btn sprite' href="{{guid}}" style="display:{{card_id}}"> </a>

    				{{#if label_name.length}}
                    <a href="{{guid}}" class="{{label_name}} sticker">&nbsp;</a>
    						 {{/if}} 
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="{{guid}}" title="{{carousal_headline}}" class="tint">
                        <img class="greyBg lazy" data-original="{{thumbnail}}" border="0" alt="{{carousal_headline}}" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="{{guid}}" title="{{carousal_headline}}">
                        {{carousal_headline}}
                    </a>
                </figcaption>
            </div>  
            {{/each}} 
            {{/if}}

            </script>

        </div><!--life-panel end-->

        <div class="trending-panel cf "><!--trending-panel start-->
            <h2>trending</h2>
            
        </div>

    </div><!--trending-panel end-->

    </section>
    <section class="container cf" id="loader" style="display: none;border: 0px solid red;">
    <div class="news-panel cf ">&nbsp;</div>
    <div class="life-panel cf" style="text-align: center;"><img src="http://media.indiatimes.in/resources/images/loading.gif" border="0" /></div>
    <div class="trending-panel cf ">&nbsp;</div>
</section>
<section class="big-ads remove-fixed-home" id="adfooter">
<div id="badRos_slot"></div> 
</section>   

<!------------------------ end Block 4------------------------------------------------>
<!--container end-->

<script type="text/javascript">
   
    $('#bigAd1_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    showBigAD2('bigAd2_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible     
       
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#bigAd2_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    showBigAD3('bigAd3_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible

    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible  
      
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#bigAd3_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    BADros('badRos_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});

$(document).ready(function() {
	/* spotlight onload tracking homepage */
	//console.log("homepage");
			ga('send', 'event', 'OnLoad Partner Stories', '247284', 'homepage', {'nonInteraction': 1});
	           
});

$(document).ready(function(){
        var trigger_depth={b2:99, b3:499, b4:1199};
        var is_trigger_active={b2:true, b3:true, b4:true};
        var call_on_scroll=true;
        
            $(window).on("scroll",function(){
            if((!(is_trigger_active.b2)&&!(is_trigger_active.b3)&&!(is_trigger_active.b4))==false)
            {
                callPreload(is_trigger_active,trigger_depth);
                
            }
    });
});

function callPreload(active,depth)
{
    var scroll_top = $(window).scrollTop();
    if((active.b2)&&(scroll_top > depth.b2))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Nov/main-5_1447584960_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/nasa-5_1447583030_1447583033_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/viral502_1447582260_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/army-5_1447580868_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/ufc-193-holly-beats-ronda-502_1447580549_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/cad_1447583815_1447583835_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/prdp_card_1447587606_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/card-cgg_1447405418_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/cover_1447416795_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/gauti-card_1447565098_1447565113_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/1_1447309231_218x102.jpg");
        active.b2=false;
    }
    if((active.b3)&&(scroll_top > depth.b3))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Nov/rts6vx05_1447569768_1447569771_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/gay-5_1447572932_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/man-daighter-502_1447568116_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/refugee-502_1447572157_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/u2_1447563239_1447563267_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/sehwag_card1a_1447585185_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/tamasha-card_1447577508_1447577519_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/o-teen-texting-facebook_1447575685_1447575753_1447575773_502x234.jpg");
        active.b3=false;
    }
    
    if((active.b4)&&(scroll_top > depth.b4))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Nov/rail5_1446634347_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/isis-world-map=-502_1439215143_236x111.jpg","http://media.indiatimes.in/media/content/2015/Oct/card_1445926341_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/shame-card_1447503371_236x111.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/parisfootball502_1447518030_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/pr_1447572361_1447572384_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/maggi_card_1447567228_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/bond-502_1446037458_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/cover_1447416795_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/ab1_1447570409_1447570419_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/1_1447413239_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/gauti-card_1447565098_1447565113_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/cover_1447418596_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/carrom_card_1447562603_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/1_1447309231_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/picmonkey-collage_1447502380_1447502386_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1447497588_1447497594_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1447495182_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1447492798_1447492804_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/cp_1447312040_1447312045_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1447404461_1447404469_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/shruti_0_1447481347_1447481353_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/card_1447483759_1447483768_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1447487183_502x234.jpg","http://media.indiatimes.in/media/content/2014/Nov/nehru_1415864366_1415864371_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card-2_1447316526_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/cover_1447399512_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card-7_1447321599_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1447409426_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/cover_1447407652_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1447158317_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/cover_1447313201_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/cacc_1447397375_1447397381_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/nn_1447330687_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card-3_1447320868_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage2_1447144362_1447144371_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card-1_1447139452_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/cp_1446723291_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card-2_1447149332_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card1_1447314932_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/fb_1447071401_1447071413_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1447311020_1447311023_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/cover_1447155488_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/badshah-card-image_1447317179_1447317189_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/judgemental_card_1447148310_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/rd_1447316485_1447316492_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/screenshot_3_1447308407_1447308415_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1447058308_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/cover_1446201271_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/fire-bu_1447250507_1447250516_1447250528_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/srk_1447394969_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/cough_1447159956_1447159967_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/nkn_1447238180_1447238190_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/srk-card_1447314540_1447314547_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/fire-crackers_1447228369_1447228388_502x234.jpg");
        active.b4=false;
    }
}
</script>    <div class="clr"></div>
      
	
                 <script> 
				                    showFooterCode();
					            </script>
	           <style>
.bottom-strip{background:#ebebeb;display:block;padding:15px 0;overflow:hidden;}.bottom-strip-box{margin:0 auto;display:table;max-width:410px;width:100%;}.square-logo{width:66px;height:64px;float:left;margin-right:10px;}.bottom-strip-content{float:left;margin-top:14px;}.bottom-strip-content span{display:block;color:#737272;font:12px/1 "RobotoRegular", Arial, Helvetica, sans-serif;margin-bottom:2px;}.bottom-strip-content a{color:#1b85dd;font:24px/1 "RobotoRegular", Arial, Helvetica, sans-serif;text-decoration:none;}
</style>
<div class="last-container">
    <!--<br>--> 
    <!--social-panel start-->
    <div class="social-panel red-bg">
        <div class="soc-inner"><!--soc-inner start-->
            <p class="soc-title">Be a Part of The New & Next</p>
            <div class="soc-count"><!--soc-count start-->
                <a href="https://www.facebook.com/indiatimes" target="_blank" title="facebook" class="sprite soc-fb"></a>
                <p>
                    3,892,619<span class="black"> FRIENDS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://twitter.com/indiatimes" target="_blank" title="twitter" class="sprite soc-twt"></a>
                <p>10343  <span class="black">FOLLOWERS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://plus.google.com/+indiatimes" target="_blank" title="gplus" class="sprite soc-gplus"></a>
                <p>104,163 <span class="black">MEMBERS</span></p>
            </div>
            <div class="soc-count"><!--soc-count start-->
                <a href="javascript:void(0);" title="E-mail" class="sprite soc-mail" id="subscribers_id"></a>
                <p>50,612 <span class="black">SUBSCRIBERS</span></p>
            </div>
            <!--soc-count end-->
        </div>
        <!--soc-inner end-->
    </div>
    <!--social-panel end-->
    <div class="clr"></div>

    <footer>
        <!-- social container start-->
        <div class="socical">
            <div class="containers">
                <div class="fl left_cont"> Get your weekly dose of virality sent to your inbox! </div>
                <div class="fr right_cont">

                    <input type="text" value="Please Enter Email Address" class="Email" name="UserEmail" id="UserEmail" defaultValue="Please Enter Email Address"/>
                    <input type="submit" id="btn-submit" value="SIGN UP" name="btn-submit" class="Subscribe">
                    <span id="subsEmal" style='color: red; padding-left: 66px; text-align: left;'></span>
                </div>

            </div>
        </div><!-- social containers end-->
        <div class="footer"><!-- footer grey start-->
            <div class="sub_container">
                <!-- container start-->
                <div class="cont_new">
                    <h2>Our Channels</h2>
                                                <a href='http://www.indiatimes.com/news/' class="blue size">News</a> 
                                                    <a href='http://www.indiatimes.com/lifestyle/' class="purple size">Lifestyle</a> 
                                                    <a href='http://www.indiatimes.com/entertainment/' class="red size">Entertainment</a> 
                                                    <a href='http://www.indiatimes.com/health/' class="green size">Health</a> 
                                                    <a href='http://www.indiatimes.com/videocafe/' class="orange size">Videos</a> 
                        					<a href='http://www.indiatimes.com/trending' class="size" style="color:white">Trending</a> 
                </div><!-- container closed-->

                <div class="cont"> <!-- container start-->
                    <h2>Sections</h2>
                                            <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/news/india/'>india</a>
                                             
                                    <a href='http://www.indiatimes.com/news/world/'>world</a>
                                             
                                    <a href='http://www.indiatimes.com/news/sports/'>sports</a>
                                             
                                    <a href='http://www.indiatimes.com/news/weird/'>weird</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/lifestyle/self/'>self</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/style/'>style</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/who-we-are/'>who we are</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/travel/'>travel</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/food/'>food</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/entertainment/bollywood/'>bollywood</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/celebs/'>celebscoop</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/hollywood/'>hollywood</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                             
                                    <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                             
                                    <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                             
                                    <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                             
                                    <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                
                             </div>                        <div class="sub_link">
                             </div>                    <div class="sub_link"><a href="http://www.indiatimes.com/photogallery">Photogallery</a></div>
                </div><!-- container closed-->
                <div class="cont_new"> <!-- container start-->
                    <h2>Indiatimes Lifestyle Network</h2>

                    <div class="sub_link">                             <a href="http://www.mensxp.com"   target="_blank" >MENS XP</a>
                                                    <a href="http://www.idiva.com"   target="_blank" >iDiva</a>
                                                    <a href="http://luxpresso.com"   target="_blank" >Luxury</a>
                                                    <a href="http://in.askmen.com/"   target="_blank" >Ask Men</a>
                                                    <a href="http://www.pursuitist.in/"   target="_blank" >Pursuitist</a>
                        </div>
                </div><!-- container closed-->
                <div class="cont"> <!-- container start-->
                    <h2>Other Sites</h2>
                    <div class="sub_link"> 
                        
                            <a href="http://www.cricbuzz.com"  target="_blank">CricBuzz</a> 
                        
                            <a href="http://www.happytrips.com/"  target="_blank">Happy Trips</a> 
                        
                            <a href="http://www.businessinsider.in/"  target="_blank">Business Insider</a> 
                        
                            <a href="http://www.zoomtv.com"  target="_blank">ZoomTv</a> 
                        
                            <a href="http://www.gizmodo.in/"  target="_blank">Gizmodo</a> 
                        
                            <a href="http://www.lifehacker.co.in/"  target="_blank">Lifehacker</a> 
                        
                            <a href="http://in.ign.com/"  target="_blank">IGN</a> 
                        
                            <a href="http://boxtv.com"  target="_blank">BoxTV</a> 
                        
                            <a href="http://www.gaana.com"  target="_blank">Gaana</a> 
                        
                            <a href="http://shopping.indiatimes.com"  target="_blank">Online Shopping</a> 
                        
                            <a href="http://www.in.techradar.com/"  target="_blank">Techradar</a> 
                        
                            <a href="http://timesdeal.com"  target="_blank">Daily Deals</a> 
                        
                            <a href="http://www.gitanjalishop.com/"  target="_blank">Gitanjali Shop</a> 
                        
                            <a href="http://www.satvikshop.com/"  target="_blank">Satvik Shop</a> 
                        
                            <a href="http://whatshot.in"  target="_blank">WHAT'S HOT</a> 
                        
                            <a href="http://zigwheels.com"  target="_blank">ZigWheels</a> 
                        
                            <a href="http://filmipop.com"  target="_blank">FILMIPOP</a> 
                        
                            <a href="http://www.filmfare.com/"  target="_blank">Filmfare</a> 
                        
                            <a href="http://www.femina.in"  target="_blank">Femina</a> 
                        
                            <a href="http://mobile.indiatimes.com"  target="_blank">Mobile</a> 
                        
                            <a href="http://www.greetzap.com"  target="_blank">Greetings</a> 
                        
                            <a href="http://www.follo.co.in"  target="_blank">Follo</a> 
                        
                            <a href="http://timesjobs.com"  target="_blank">Jobs</a> 
                        
                            <a href="http://magicbricks.com"  target="_blank">Property</a> 
                        
                            <a href="http://www.astrospeak.com"  target="_blank">Astrology</a> 
                        
                            <a href="http://speakingtree.in"  target="_blank">Speaking Tree</a> 
                        
                            <a href="http://simplymarry.com"  target="_blank">Matrimonial</a> 
                        
                            <a href="http://itimes.com"  target="_blank">iTimes</a> 
                        
                            <a href="http://www.ads2book.com/"  target="_blank">Ads2book</a> 
                        
                            <a href="http://www.tcnext.com/"  target="_blank">TcNext</a> 
                        
                            <a href="http://www.romedynow.com"  target="_blank">Romedy Now</a> 
                                            </div>
                </div><!-- container closed-->
                <div class="cont border_none"><!-- container start-->
                    <h2>About us</h2>
                    <div class="sub_link">
                                                    <a href='http://www.indiatimes.com/privacypolicy' >Privacy Policy</a> 
                                                    <a href='http://www.indiatimes.com/sitemap.html' >sitemap</a> 
                                                    <a href='http://www.indiatimes.com/termsandcondition' >Terms & CONDITIONS</a> 
                                                    <a href='http://www.indiatimes.com/seoarchive' >ARCHIVES</a> 
                                                    <a href='http://www.indiatimes.com/contactus' >Contact us</a> 
                                                    <a href='http://www.indiatimes.com/aboutus' >ABOUT INDIATIMES</a> 
                                            </div>
                </div><!-- container closed-->
            </div><!-- footer grey closed-->
        </div><!-- footer black closed-->

        <div class="black_footer"><!-- footer black start-->
            <div class="containers">
                <p><a href="javascript:void(0)" class="copyrt">Copyright Â© 2015 Times Internet Limited. Powered by Indiatimes Lifestyle Network. All rights reserved</a></p>
                <span><a href="http://www.indiatimes.com/seoarchive/">Archive</a> | <a href="http://www.indiatimes.com/sitemap.html">Sitemap</a> | <a href="http://www.indiatimes.com/aboutus/">About Us</a> | <a href="http://www.indiatimes.com/contactus/">Contact Us</a> | <a href="http://www.indiatimes.com/privacypolicy/">Privacy Policy</a> | <a target="_blank" href="http://m.indiatimes.com">Indiatimes Mobile</a> | <a href="http://www.indiatimes.com/termsandcondition/">Terms of Use and Grievance Redressal Policy </a> </span> 
            </div>
        </div><!-- footer black end-->
    </footer>
</div>
<script>
    $( document ).ready(function() {
        $("#subscribers_id").click(function(){
            $("#UserEmail").focus();
        });
    });
    
</script>

    <script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.lazyload.min.js?v=100.53" charset="utf-8"></script>
<script defer type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.easing.1.3.min.js?v=100.53"></script>

<input type="hidden" name="currentId" id="currentId" value="">
    <div class="animate_cont dawn floating delay5">
        <span class="animate_arrow sprite_image"></span>
    </div>



<script type="text/javascript" src="http://media.indiatimes.in/resources/js/main.js?v=100.53"></script>    
<script type="text/javascript" src="http://media.indiatimes.in/resources/js/common.js?v=100.53"></script>
<script  defer type="text/javascript" src="http://media.indiatimes.in/resources/js/jquery-scrolltofixed.js?v=100.53"></script>

<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://cdn.mouseflow.com/projects/1e238441-c30e-4d1b-8edd-c0f14d8fa6b7.js' type='text/javascript'%3E%3C/script%3E"));</script>
<!-- mouse flow code ends-->
<script  src="http://tags.crwdcntrl.net/c/2818/cc.js?ns=_cc2818" id="LOTCC_2818"></script> 

<script> 
    _cc2818.bcp(); 
</script>

	 <p id="back-top" style="display:none;"><a href="#top"><span></span></a> </p>
</div><!--wrap end-->


</body>
</html>