



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>

                    
                    
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "099-0151959-3287656";
                var ue_id = "0J3QFW0VBQYEZ5DQK110";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        
    
        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />
    
    <meta property='fb:app_id' content='115109575169727' />
    
    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="0J3QFW0VBQYEZ5DQK110" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1e-c3-2xl-i-53087a84.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-422682513._CB312123354_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-485555676._CB318510215_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-247421025._CB318510254_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-3978168775._CB318510288_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-1541130848._CB318510255_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
(function(){var d=function(o){return Object.prototype.toString.call(o)==="[object Array]";},g=function(q,p){var o;for(o=0;o<q.length;o++){if(o in q){p.call(null,q[o],o);}}},h=[],k,b,l=false,n=false,f=function(){var o=[],p=[],q={};g(h,function(s){var r="";g(s.dartsite.split("/"),function(t){if(t!==""){if(t in q){}else{q[t]=o.length;o.push(t);}r+="/"+q[t];}});p.push(r);});return{iu_parts:o,enc_prev_ius:p};},c=function(){var o=[];g(h,function(q){var p=[];g(q.sizes,function(r){p.push(r.join("x"));});o.push(p.join("|"));});return o;},m=function(){var o=[];g(h,function(p){o.push(a(p.targeting));});return o.join("|");},a=function(r,o){var q,p,s=[];for(q in r){p=[];for(j=0;j<r[q].length;j++){p.push(encodeURIComponent(r[q][j]));}if(o){s.push(q+"="+encodeURIComponent(p.join(",")));}else{s.push(q+"="+p.join(","));}}return s.join("&");},e=function(){var o=+new Date();if(n){return;}if(!this.readyState||"loaded"===this.readyState){n=true;if(l){imdbads.cmd.push(function(){for(i=0;i<h.length;i++){generic.monitoring.record_metric(h[i].name+".fail",csm.duration(o));}});}}};window.tinygpt={define_slot:function(r,q,o,p){h.push({dartsite:r.replace(/\/$/,""),sizes:q,name:o,targeting:p});},set_targeting:function(o){k=o;},callback:function(q){var r,p={},t,o,s=+new Date();l=false;for(r=0;r<h.length;r++){t=h[r].dartsite;o=h[r].name;if(q[r][t]){p[o]=q[r][t];}else{window.console&&console.error&&console.error("Unable to correlate GPT response for "+o);}}imdbads.cmd.push(function(){for(r=0;r<h.length;r++){ad_utils.slot_events.trigger(h[r].name,"request",{timestamp:b});ad_utils.slot_events.trigger(h[r].name,"tagdeliver",{timestamp:s});}ad_utils.gpt.handle_response(p);});},send:function(){var r=[],q=function(s,t){if(d(t)){t=t.join(",");}if(t){r.push(s+"="+encodeURIComponent(""+t));}},o,p;if(h.length===0){tinygpt.callback({});return;}q("gdfp_req","1");q("correlator",Math.floor(4503599627370496*Math.random()));q("output","json_html");q("callback","tinygpt.callback");q("impl","fifs");q("json_a","1");result=f();q("iu_parts",result.iu_parts);q("enc_prev_ius",result.enc_prev_ius);q("prev_iu_szs",c());q("prev_scp",m());q("cust_params",a(k,true));o=document.createElement("script");p=document.getElementsByTagName("script")[0];o.async=true;o.type="text/javascript";o.src="http://pubads.g.doubleclick.net/gampad/ads?"+r.join("&");o.id="tinygpt";o.onload=o.onerror=o.onreadystatechange=e;l=true;p.parentNode.insertBefore(o,p);b=+new Date();}};})();</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['e'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['2'],
'u': ['987789831504'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-tarnhelm-4026715392._CB312195511_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"375b7ac5c509373210f9e24d8756229c3d9c430e",
"2015-09-04T23%3A58%3A18GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 25302;
generic.days_to_midnight = 0.292847216129303;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-1582251138._CB317085808_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'e']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm === undefined || window.generic === undefined || window.consoleLog === undefined)) {
                    if (console !== undefined && console.log !== undefined) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=e;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=987789831504;ord=987789831504?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=e;bpx=2;c=0;s=3075;s=32;ord=987789831504?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=e;bpx=2;c=0;s=3075;s=32;ord=987789831504?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
onsubmit="(new Image()).src='/rg/SEARCH-BOX/HEADER/images/b.gif?link=/find';"
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/imdbpicks/monthly-tv-picks/?ref_=nv_tp_tvpicks_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/search/title?count=100&title_type=feature,tv_series,tv_movie&explore=title_type,genres,year,countries&ref_=nv_ch_mm_1"
>Popular Movies & TV</a></li>
                        <li><a href="/chart/?ref_=nv_ch_cht_2"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_3"
>Oscar Winners</a></li>
                        <li><a href="/chart/top?ref_=nv_ch_250_4"
>Top 250 Movies</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_5"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/imdbpicks/monthly-tv-picks/?ref_=nv_tvv_picks_1"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_2"
>On Tonight</a></li>
                            <li><a href="/chart/toptv?ref_=nv_tvv_250_3"
>Top 250 TV</a></li>
                        <li><a href="/list/ls074093523/?ref_=nv_tvv_wn_4"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_5"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/academymuseum/?ref_=nv_sf_am_1"
>Academy Museum</a></li>
                        <li><a href="/imdbpicks/?ref_=nv_sf_pks_2"
>IMDb Picks</a></li>
                        <li><a href="/best-of/?ref_=nv_sf_bo_3"
>Best of 2014</a></li>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_4"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/whattowatch/?ref_=nv_sf_wtw_5"
>What to Watch</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=09-04&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_2"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_3"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_4"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_5"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_6"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_7"
>Road to the Emmys</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_8"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_9"
>Toronto Film Festival</a></li>
                        <li><a href="/awards-central/?ref_=nv_ev_all_10"
>More Popular Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni58970456/?ref_=nv_nw_tn_1"
> âGame of Thronesâ Casts âUnREALâ Star Freddie Stroma for Season 6
</a><br />
                        <span class="time">7 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni58970461/?ref_=nv_nw_tn_2"
> âThe Transporter Refueledâ Revs Up $365,000 at Thursday Night Box Office
</a><br />
                        <span class="time">7 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni58970129/?ref_=nv_nw_tn_3"
> Can âBlack Massâ Get Johnny Depp Back in the Awards Game?
</a><br />
                        <span class="time">8 hours ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/games/guess?ref_=nv_cm_qz_3"
>Quiz Game</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_4"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5> 
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0060196/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTMxMDM1OTQxM15BMl5BanBnXkFtZTcwNDc0Mzg4Mw@@._V1._SX420_CR5,5,410,315_.jpg",
            titleYears : "1966",
            rank : 8,
                    headline : "The Good, the Bad and the Ugly"
    },
    nameAd : {
            clickThru : "/name/nm0424060/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTc1ODIxOTA1OV5BMl5BanBnXkFtZTcwNDY5MDc1NA@@._V1._SX300_CR15,30,250,315_BR10_CT10_.jpg",
            rank : 23,
            headline : "Scarlett Johansson"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYmFYn6FOtuIdnuTMmYCZ1T0hzGWk6h6zqbPFlgEmV87t3wwgwtUyRoFyAd0bQalKaSUVdPact-%0D%0AxqPjEBEbXOpufb7qHntGPgtTRbaN77uh3ZM2nrhvt8M86_Nredr7jbao5C8apiarAaNEStbgl1Qc%0D%0AUa72MjdzBwVmqjGUAoHCQr8ZWBCXaT2Gx8kOSxaiz_EX5HpWaPzDfK-BK4X2cBD2Rw%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYlth_iStfv1yZvVlUsjWXL4VSBLatuZhJ7hEOGBUbA3HmSulMHCh-hZU7y4LxukqK2Be_iSxvL%0D%0A8HFvf-VgpuVnWMseeUsYrPSB3icPTE_LW7DGcEF7PT4x17xH0EelkeUvm-ll9YmsbYphLllDmWiD%0D%0AdNwYqhD06IzA-mfh43l0sUsHgNB-BJhWMKJCF0fhXvpt%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYnDBztFvOXlZtCFWZO7TB1IswOClSHvYq4rKQpyfb60WaiPcTZ_UPM-Uz8cQO8jT5l0lux3y5A%0D%0AvOE0QWRup77WAk2PqEeX6vsLeSNVq6lcgq6T21zYoE1-tfx-d0Jj8dOJy11Nt87n23a8VSxwnXzx%0D%0AdBXMnaqfpKecjg_aXXiKJRP1J9GiM6S7AqspQqKUYgXFnJVK5iPQz8gDFtI5IKcPiA%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=987789831504;ord=987789831504?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>
                    <div id="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=987789831504;ord=987789831504?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	

                    
                    

<div id="content-2-wide">
    <div id="main">
                    
        <div class="heroWidget">
        <span class="ab_widget"
        >
            <div class="ab_hero">
<div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1111339801?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191224802&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191224802&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi1111339801" data-source="bylist" data-id="ls054268624" data-rid="0J3QFW0VBQYEZ5DQK110" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="IMDb sits down with the cast of the thriller drama &quot;Hand of God,&quot; including Ron Perlman, Julian Morris, Garret Dillahunt, and Alona Tal." alt="IMDb sits down with the cast of the thriller drama &quot;Hand of God,&quot; including Ron Perlman, Julian Morris, Garret Dillahunt, and Alona Tal." src="http://ia.media-imdb.com/images/M/MV5BMjA0NzI4NTk1NF5BMl5BanBnXkFtZTgwODcwMTI3NjE@._V1_SY298_CR21,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA0NzI4NTk1NF5BMl5BanBnXkFtZTgwODcwMTI3NjE@._V1_SY298_CR21,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="IMDb sits down with the cast of the thriller drama &quot;Hand of God,&quot; including Ron Perlman, Julian Morris, Garret Dillahunt, and Alona Tal." title="IMDb sits down with the cast of the thriller drama &quot;Hand of God,&quot; including Ron Perlman, Julian Morris, Garret Dillahunt, and Alona Tal." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="IMDb sits down with the cast of the thriller drama &quot;Hand of God,&quot; including Ron Perlman, Julian Morris, Garret Dillahunt, and Alona Tal." title="IMDb sits down with the cast of the thriller drama &quot;Hand of God,&quot; including Ron Perlman, Julian Morris, Garret Dillahunt, and Alona Tal." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt3973768/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191224802&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191224802&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > "Hand of God" </a> </div> </div> <div class="secondary ellipsis"> What to Watch </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2703471385?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191224802&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191224802&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi2703471385" data-source="bylist" data-id="ls002309697" data-rid="0J3QFW0VBQYEZ5DQK110" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="In a dystopian near future, single people, according to the laws of The City, are taken to The Hotel, where they are obliged to find a romantic partner in forty-five days or are transformed into beasts and sent off into The Woods." alt="In a dystopian near future, single people, according to the laws of The City, are taken to The Hotel, where they are obliged to find a romantic partner in forty-five days or are transformed into beasts and sent off into The Woods." src="http://ia.media-imdb.com/images/M/MV5BNDQ1NDE5NzQ1NF5BMl5BanBnXkFtZTgwNzA5OTM2NTE@._V1_SY298_CR4,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNDQ1NDE5NzQ1NF5BMl5BanBnXkFtZTgwNzA5OTM2NTE@._V1_SY298_CR4,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="In a dystopian near future, single people, according to the laws of The City, are taken to The Hotel, where they are obliged to find a romantic partner in forty-five days or are transformed into beasts and sent off into The Woods." title="In a dystopian near future, single people, according to the laws of The City, are taken to The Hotel, where they are obliged to find a romantic partner in forty-five days or are transformed into beasts and sent off into The Woods." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="In a dystopian near future, single people, according to the laws of The City, are taken to The Hotel, where they are obliged to find a romantic partner in forty-five days or are transformed into beasts and sent off into The Woods." title="In a dystopian near future, single people, according to the laws of The City, are taken to The Hotel, where they are obliged to find a romantic partner in forty-five days or are transformed into beasts and sent off into The Woods." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt3464902/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191224802&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191224802&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > The Lobster </a> </div> </div> <div class="secondary ellipsis"> International Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3777082137?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191224802&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191224802&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi3777082137" data-source="bylist" data-id="ls002397163" data-rid="0J3QFW0VBQYEZ5DQK110" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="A young kid teams up with the niece of young adult horror author R.L. Stine after the writer's imaginary demons are set free on the town of Greendale, Maryland." alt="A young kid teams up with the niece of young adult horror author R.L. Stine after the writer's imaginary demons are set free on the town of Greendale, Maryland." src="http://ia.media-imdb.com/images/M/MV5BMjA1OTUzNTQ5Ml5BMl5BanBnXkFtZTgwODQ4NDkxNjE@._V1_SX201_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA1OTUzNTQ5Ml5BMl5BanBnXkFtZTgwODQ4NDkxNjE@._V1_SX201_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="A young kid teams up with the niece of young adult horror author R.L. Stine after the writer's imaginary demons are set free on the town of Greendale, Maryland." title="A young kid teams up with the niece of young adult horror author R.L. Stine after the writer's imaginary demons are set free on the town of Greendale, Maryland." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="A young kid teams up with the niece of young adult horror author R.L. Stine after the writer's imaginary demons are set free on the town of Greendale, Maryland." title="A young kid teams up with the niece of young adult horror author R.L. Stine after the writer's imaginary demons are set free on the town of Greendale, Maryland." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt1051904/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191224802&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191224802&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > Goosebumps </a> </div> </div> <div class="secondary ellipsis"> Exclusive Trailer </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/trailers?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191224802&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191224802&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Browse more trailers </a> </p>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="http://www.imdb.com/list/ls079029897/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191129882&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_il_hog_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191129882&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <h3>"Hand of God": 5 Things to Expect from Ron Perlman's Next Act</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:624px;height:auto;" > <div style="width:624px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/list/ls079029897/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191129882&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_il_hog_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191129882&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Hand of God (2014-)" alt="Hand of God (2014-)" src="http://ia.media-imdb.com/images/M/MV5BMTgzODIwMzgyMl5BMl5BanBnXkFtZTgwMzcxMDE3NjE@._V1_SX624_CR0,0,624,351_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTgzODIwMzgyMl5BMl5BanBnXkFtZTgwMzcxMDE3NjE@._V1_SX624_CR0,0,624,351_AL_UY702_UX1248_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">In his first series since "Sons of Anarchy," Ron Perlman plays a corrupt judge who is either channeling a higher power...or losing his mind. Here are 5 things to expect from "Hand of God," Amazon Prime's newest series.</p> <p class="seemore"> <a href="http://www.imdb.com/list/ls079029897/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191129882&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_il_hog_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191129882&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Read More </a> </p>    </div>

        </span>
        </div>
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58970456?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTgzMDY4MjU5M15BMl5BanBnXkFtZTcwMjA3ODc3Nw@@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58970456?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1"
class="headlines" >âGame of Thronesâ Casts âUnREALâ Star Freddie Stroma for Season 6</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_src"
>Variety - TV News</a></span>
    </div>
                                </div>
<p> â<a href="/title/tt0944947?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk1">Game of Thrones</a>â has added <a href="/name/nm2238815?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk2">Freddie Stroma</a>, star of Lifetimeâs â<a href="/title/tt3314218?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk3">UnREAL</a>,â for season six, Variety has confirmed. Stroma will play Dickon Tarly, brother of Samwell (<a href="/name/nm4263213?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk4">John Bradley</a>), in the HBO seriesâ upcoming season. Weâre expected to meet the rest of Samâs family, including his father, the ruthless ...                                        <span class="nobr"><a href="/news/ni58970456?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58970461?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2"
class="headlines" >âThe Transporter Refueledâ Revs Up $365,000 at Thursday Night Box Office</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2_src"
>The Wrap</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58970129?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3"
class="headlines" >Can âBlack Massâ Get Johnny Depp Back in the Awards Game?</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58970012?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4"
class="headlines" >Venice Reviews: Is Tom Hardy Bad or Brilliant As Twin Mobsters in 'Legend'?</a>
    <div class="infobar">
            <span class="text-muted">9 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000353?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4_src"
>Thompson on Hollywood</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58970096?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5"
class="headlines" >How Captain Americaâs Stuffed Cast Caused a Marvel Civil War</a>
    <div class="infobar">
            <span class="text-muted">9 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5_src"
>Vulture</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58970146?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTMwMTE2ODk0NV5BMl5BanBnXkFtZTYwMTE5MzQ3._V1_SY150_CR9,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58970146?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1"
class="headlines" >Welcome to Death Row being shopped as a sequel to Straight Outta Compton</a>
    <div class="infobar">
            <span class="text-muted">9 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0003561?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_src"
>Flickeringmyth</a></span>
    </div>
                                </div>
<p>Given the box office success of <a href="/title/tt1398426?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk1">Straight Outta Compton</a>, itâs no surprise that Hollywood is looking to capitalise on the hip-hop market, and according to THR, agency Apa has put together a package based upon <a href="/name/nm0767655?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk2">S. Leigh Savidge</a>âs book and documentary <a href="/title/tt0275066?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk3">Welcome to Death Row</a>, which it is shopping around as...                                        <span class="nobr"><a href="/news/ni58970146?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58970461?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2"
class="headlines" >âThe Transporter Refueledâ Revs Up $365,000 at Thursday Night Box Office</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2_src"
>The Wrap</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58970070?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3"
class="headlines" >Johnny Depp Holds Court as âBlack Massâ Lights Up Venice Film Festival</a>
    <div class="infobar">
            <span class="text-muted">9 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58970117?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4"
class="headlines" >Sequel Bits: âMad Max,â âMaze Runner,â âWolverine,â James Bond</a>
    <div class="infobar">
            <span class="text-muted">9 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000151?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4_src"
>Slash Film</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58970330?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5"
class="headlines" >11 Craziest Things That Have Happened During the Making of Werner Herzog's Films</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000139?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5_src"
>Indiewire</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58970903?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjE3MDM4NTg0NV5BMl5BanBnXkFtZTcwNjI4MTczMw@@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58970903?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1"
class="headlines" >Netflixâs Lemony Snicket Series Taps âTrue Bloodâsâ Mark Hudis, Barry Sonnenfeld (Exclusive)</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_src"
>Variety - TV News</a></span>
    </div>
                                </div>
<p> Former â<a href="/title/tt0844441?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk1">True Blood</a>â showrunner <a href="/name/nm0399725?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk2">Mark Hudis</a> and director <a href="/name/nm0001756?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk3">Barry Sonnenfeld</a> have signed on to steer Netflixâs adaptation of the Lemony Snicket â<a href="/title/tt0339291?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk4">A Series of Unfortunate Events</a>â fantasy book series. Hudis will serve as showrunner and exec producer of the series, while Sonnenfeld will direct and exec ...                                        <span class="nobr"><a href="/news/ni58970903?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58970342?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2"
class="headlines" >Ellen Pompeo Is Offended Fans Think âGreyâs Anatomyâ Canât Continue Without Patrick Dempseyâs McDreamy</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2_src"
>The Wrap</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58970319?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3"
class="headlines" >âBastard Executionerâ Review: Kurt Sutterâs Strong Series Cuts Sharp & Deep</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004913?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3_src"
>Deadline TV</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58970456?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4"
class="headlines" >âGame of Thronesâ Casts âUnREALâ Star Freddie Stroma for Season 6</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58970326?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5"
class="headlines" >Scott Porter Joins Robert Kirkman's Exorcism Drama Outcast</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5_src"
>TVLine.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58969826?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjMxMzg3MDI5NV5BMl5BanBnXkFtZTcwOTAxODc0Ng@@._V1_SY150_CR15,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58969826?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1"
class="headlines" >Happy 34th Birthday, BeyoncÃ©! All Hail the Queen of Everything (Especially Fierce Fashion)</a>
    <div class="infobar">
            <span class="text-muted">10 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_src"
>E! Online</a></span>
    </div>
                                </div>
<p>There's no official holiday that marks today, but there might as well be. Why, you ask? Because it's <a href="/name/nm0461498?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk1">BeyoncÃ©</a>'s birthday, people! They don't call her Queen Bey for nothingâwhether it's fabulous hairdos, unbelievably sexy red carpet looks or just her morning I-woke-up-like-this selfies, the Grammy ...                                        <span class="nobr"><a href="/news/ni58969826?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58970076?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2"
class="headlines" >Jessica Alba's Honest Company Sued, Accused of Being Deceptive: Details</a>
    <div class="infobar">
            <span class="text-muted">13 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0045136?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2_src"
>Us Weekly</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58969825?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3"
class="headlines" >Ben Affleck and Jennifer Garner Are All Smiles in L.A.</a>
    <div class="infobar">
            <span class="text-muted">10 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3_src"
>E! Online</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58969987?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4"
class="headlines" >Sanaa Lathan's Diversity Plea</a>
    <div class="infobar">
            <span class="text-muted">10 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000936?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4_src"
>Access Hollywood</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58969920?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5"
class="headlines" >P!nk Trashes the VMAs -- and Demi Lovato Claps Back!</a>
    <div class="infobar">
            <span class="text-muted">10 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004699?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5_src"
>TooFab</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <h3>IMDb Snapshot</h3> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm2582769408/rg1657510656?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2190838462&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2190838462&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTgyMzU3MTQwMl5BMl5BanBnXkFtZTgwMjYxMjI3NjE@._V1._CR0,0,1362,1362_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTgyMzU3MTQwMl5BMl5BanBnXkFtZTgwMjYxMjI3NjE@._V1._CR0,0,1362,1362_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm2582769408/rg1657510656?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2190838462&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2190838462&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > Venice Film Festival: Day 2 </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm553643776/rg1528338944?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2190838462&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2190838462&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Carol (2015)" alt="Carol (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTczNTQ4OTEyNV5BMl5BanBnXkFtZTgwNDgyMDI3NjE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTczNTQ4OTEyNV5BMl5BanBnXkFtZTgwNDgyMDI3NjE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm553643776/rg1528338944?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2190838462&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2190838462&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > Latest Posters </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm4109496064/rg1858837248?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2190838462&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2190838462&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Legend (2015)" alt="Legend (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTg2OTA1MDczN15BMl5BanBnXkFtZTgwMTEyMjI3NjE@._V1_SY201_CR51,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg2OTA1MDczN15BMl5BanBnXkFtZTgwMTEyMjI3NjE@._V1_SY201_CR51,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm4109496064/rg1858837248?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2190838462&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2190838462&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <i>Legend</i> Premiere </a> </div> </div> </div> </div> </div> </div> </div>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
                <div class="ab_borntoday">
<span class="ninja_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=9-4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Born Today</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0004747?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Wes Bentley" alt="Wes Bentley" src="http://ia.media-imdb.com/images/M/MV5BOTgyOTY5OTA5OF5BMl5BanBnXkFtZTcwNzM1MjM1Nw@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BOTgyOTY5OTA5OF5BMl5BanBnXkFtZTcwNzM1MjM1Nw@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0004747?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_1">Wes Bentley</a> (37) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0339011?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Max Greenfield" alt="Max Greenfield" src="http://ia.media-imdb.com/images/M/MV5BMjA5NjQxMzQyM15BMl5BanBnXkFtZTgwMzUxMjE3MTE@._V1_SY172_CR6,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA5NjQxMzQyM15BMl5BanBnXkFtZTgwMzUxMjE3MTE@._V1_SY172_CR6,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0339011?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_2">Max Greenfield</a> (35) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0461498?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="BeyoncÃ© Knowles" alt="BeyoncÃ© Knowles" src="http://ia.media-imdb.com/images/M/MV5BMjMxMzg3MDI5NV5BMl5BanBnXkFtZTcwOTAxODc0Ng@@._V1_SY172_CR17,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjMxMzg3MDI5NV5BMl5BanBnXkFtZTcwOTAxODc0Ng@@._V1_SY172_CR17,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0461498?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_3">BeyoncÃ© Knowles</a> (34) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm3114649?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Charlotte Le Bon" alt="Charlotte Le Bon" src="http://ia.media-imdb.com/images/M/MV5BMTkwODIzODkzMF5BMl5BanBnXkFtZTgwMzg4NTEwMDE@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTkwODIzODkzMF5BMl5BanBnXkFtZTgwMzg4NTEwMDE@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm3114649?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_4">Charlotte Le Bon</a> (29) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0001834?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Damon Wayans" alt="Damon Wayans" src="http://ia.media-imdb.com/images/M/MV5BMTk0NTQ2OTU4MF5BMl5BanBnXkFtZTYwMTMxNTM1._V1_SY172_CR3,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk0NTQ2OTU4MF5BMl5BanBnXkFtZTYwMTMxNTM1._V1_SY172_CR3,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0001834?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_5">Damon Wayans</a> (55) </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/search/name?refine=birth_monthday&birth_monthday=9-4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See all birthdays </a> </p>        </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/title/tt3973768/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191113762&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191113762&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage" > <h3>TV Spotlight: "Hand of God" - First Look</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3103189248/tt3973768?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191113762&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191113762&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Hand of God (2014-)" alt="Hand of God (2014-)" src="http://ia.media-imdb.com/images/M/MV5BMTUyODc0NzM4Nl5BMl5BanBnXkFtZTgwODk5MjY0NjE@._V1_SY250_CR0,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUyODc0NzM4Nl5BMl5BanBnXkFtZTgwODk5MjY0NjE@._V1_SY250_CR0,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi455258905?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191113762&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191113762&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi455258905" data-rid="0J3QFW0VBQYEZ5DQK110" data-type="single" class="video-colorbox" data-refsuffix="hm_tv" data-ref="hm_tv_i_2"> <img itemprop="image" class="pri_image" title="Hand of God (2014-)" alt="Hand of God (2014-)" src="http://ia.media-imdb.com/images/M/MV5BMjM0MDQ0MTIwMl5BMl5BanBnXkFtZTgwODYxMDE3NjE@._V1_SX444_CR0,0,444,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjM0MDQ0MTIwMl5BMl5BanBnXkFtZTgwODYxMDE3NjE@._V1_SX444_CR0,0,444,250_AL_UY500_UX888_AL_.jpg" /> <img alt="Hand of God (2014-)" title="Hand of God (2014-)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Hand of God (2014-)" title="Hand of God (2014-)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">Created by <a href="/name/nm1219905/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191113762&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_lk1">Ben Watkins</a> and directed by <a href="/name/nm0286975/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191113762&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_lk2">Marc Forster</a>, Amazon Prime's newest series stars <a href="/name/nm0000579/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191113762&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_lk3">Ron Perlman</a> as a morally corrupt judge who suffers a breakdown and believes God is compelling him onto a path of vigilante justice.</p> <p class="seemore"> <a href="/title/tt3973768/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191113762&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191113762&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Learn more about "Hand of God" </a> </p>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_trivia">
<span class="ninja_header"> <span class="oneline"> <a href="/title/tt2719848/trivia?item=tr2506990&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Did You Know?</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt2719848?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Everest (2015)" alt="Everest (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjMzMjIxOTIxMl5BMl5BanBnXkFtZTgwNTk4NDI0NjE@._V1_SX89_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjMzMjIxOTIxMl5BMl5BanBnXkFtZTgwNTk4NDI0NjE@._V1_SX89_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" > <strong class="text-large"><a href="/title/tt2719848?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Everest</a></strong> <p class="blurb"><a href="/name/nm0000705?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Robin Wright</a> and <a href="/name/nm0446672?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk2">Michael Kelly</a> are co-stars in the Netflix series <a href="/title/tt1856010?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk3">House of Cards</a> (2013)</p></div> </div> </div> <p class="seemore"> <a href="/title/tt2719848/trivia?item=tr2506990&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See more trivia </a> </p>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;g9yhEc39XRo&quot;}"
        >
            <div class="ab_poll poll">
<span class="ninja_header"> <span class="oneline"> <a href="/poll/g9yhEc39XRo/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191425042&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191425042&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Poll: Wes Craven (1939-2015) RIP</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191425042&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk">More Polls</a></h4> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/g9yhEc39XRo/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191425042&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191425042&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Last House on the Left (1972)" alt="The Last House on the Left (1972)" src="http://ia.media-imdb.com/images/M/MV5BMTQ2MzgyNTQxN15BMl5BanBnXkFtZTcwMjYyNDMyMQ@@._V1_SY207_CR2,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ2MzgyNTQxN15BMl5BanBnXkFtZTcwMjYyNDMyMQ@@._V1_SY207_CR2,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/g9yhEc39XRo/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191425042&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191425042&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Invitation to Hell (1984)" alt="Invitation to Hell (1984)" src="http://ia.media-imdb.com/images/M/MV5BMTU2MTQ1MTMxMF5BMl5BanBnXkFtZTcwMTQ4NTQyMQ@@._V1_SY207_CR2,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU2MTQ1MTMxMF5BMl5BanBnXkFtZTcwMTQ4NTQyMQ@@._V1_SY207_CR2,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/g9yhEc39XRo/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191425042&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191425042&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Hills Have Eyes (1977)" alt="The Hills Have Eyes (1977)" src="http://ia.media-imdb.com/images/M/MV5BMTYxNTYzODU4MV5BMl5BanBnXkFtZTcwNjEzNzIzMQ@@._V1_SY207_CR3,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTYxNTYzODU4MV5BMl5BanBnXkFtZTcwNjEzNzIzMQ@@._V1_SY207_CR3,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/g9yhEc39XRo/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191425042&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191425042&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Stranger in Our House (1978)" alt="Stranger in Our House (1978)" src="http://ia.media-imdb.com/images/M/MV5BMTY2MTU5NDQxOF5BMl5BanBnXkFtZTcwNjU3MDUxMQ@@._V1_SY207_CR1,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY2MTU5NDQxOF5BMl5BanBnXkFtZTcwNjU3MDUxMQ@@._V1_SY207_CR1,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/g9yhEc39XRo/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191425042&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191425042&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="BenÃ§Ã£o Mortal (1981)" alt="BenÃ§Ã£o Mortal (1981)" src="http://ia.media-imdb.com/images/M/MV5BMTUwNjQ0NTEyMV5BMl5BanBnXkFtZTYwOTc3NDg5._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUwNjQ0NTEyMV5BMl5BanBnXkFtZTYwOTc3NDg5._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">Which Wes Craven movie is your favorite? <a href="http://www.imdb.com/board/bd0000088/nest/247833090/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191425042&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk1">Discuss here</a> after voting.</p> <p class="seemore"> <a href="/poll/g9yhEc39XRo/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191425042&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191425042&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Vote now </a> </p>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
    </div>
    <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,11x1;p=tr;fv=1;ab=e;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=987789831504;ord=987789831504?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,11x1;p=tr;fv=1;ab=e;bpx=2;c=0;s=3075;s=32;ord=987789831504?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,11x1;p=tr;fv=1;ab=e;bpx=2;c=0;s=3075;s=32;ord=987789831504?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	
                    
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2074322302&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2074322302&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Opening This Week</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2938956"></div> <div class="title"> <a href="/title/tt2938956?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2074322302&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t0"> Le Transporteur: HÃ©ritage </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1178665"></div> <div class="title"> <a href="/title/tt1178665?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2074322302&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t1"> A Walk in the Woods </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt0443465"></div> <div class="title"> <a href="/title/tt0443465?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2074322302&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t2"> Before We Go </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4643580"></div> <div class="title"> <a href="/title/tt4643580?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2074322302&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t3"> Un gallo con muchos huevos </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> </div> </div> </div> <div><a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2074322302&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2074322302&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more opening this week</p></a></div>
        </span>
        </div>
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
            <div class="ab_ninja showtimesMessagingWidget">
<p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2006955922&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p>    </div>

        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/chart/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Now Playing (Box Office)</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1398426"></div> <div class="title"> <a href="/title/tt1398426?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t0"> Straight Outta Compton </a> <span class="secondary-text">$13.1M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3832914"></div> <div class="title"> <a href="/title/tt3832914?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t1"> War Room </a> <span class="secondary-text">$11.4M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2381249"></div> <div class="title"> <a href="/title/tt2381249?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t2"> Mission: Impossible - Rogue Nation </a> <span class="secondary-text">$8.2M</span> </div> <div class="action"> <a href="/showtimes/title/tt2381249?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1781922"></div> <div class="title"> <a href="/title/tt1781922?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t3"> No Escape </a> <span class="secondary-text">$8.1M</span> </div> <div class="action"> <a href="/showtimes/title/tt1781922?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2752772"></div> <div class="title"> <a href="/title/tt2752772?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t4"> Sinister 2 </a> <span class="secondary-text">$4.7M</span> </div> <div class="action"> <a href="/showtimes/title/tt2752772?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> </div> </div> </div> <div><a href="/chart/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more box office results</p></a></div>
        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Coming Soon</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3567288"></div> <div class="title"> <a href="/title/tt3567288?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t0"> The Visit </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3862750"></div> <div class="title"> <a href="/title/tt3862750?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t1"> The Perfect Guy </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4337690"></div> <div class="title"> <a href="/title/tt4337690?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t2"> 90 Minutes in Heaven </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3125472"></div> <div class="title"> <a href="/title/tt3125472?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t3"> Coming Home </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2909116"></div> <div class="title"> <a href="/title/tt2909116?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t4"> Le dernier loup </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <div><a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more coming soon</p></a></div>
        </span>
        </div>
                    
                    
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
            <div class="ab_twitter">
<span class="widget_header"> <h3>Follow Us On Twitter</h3> </span> <div class="widget_content"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3776329187._CB305760180_.html#{"screen-name":"IMDb","height":"500px","width":"300px"}'></iframe> </div> </div>    </div>

        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
            <div class="ab_facebook">
<span class="widget_header"> <h3>Find Us On Facebook</h3> </span> <div class="widget_content"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
        <div class="aux-content-widget-2 sticky-widget">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/gallery/rg1842060032/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191130002&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pwh_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191130002&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <h3>"Hand of God" Photo Gallery</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/media/rm734588672/rg1842060032?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191130002&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pwh_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191130002&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Hand of God (2014-)" alt="Hand of God (2014-)" src="http://ia.media-imdb.com/images/M/MV5BMTYyMTQ4MjM3MV5BMl5BanBnXkFtZTgwNzAwNTI3NjE@._V1_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTYyMTQ4MjM3MV5BMl5BanBnXkFtZTgwNzAwNTI3NjE@._V1_UY750_UX1000_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">See photos from the first season of "Hand of God" in our special gallery</p> <p class="seemore"> <a href="/gallery/rg1842060032/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191130002&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pwh_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2191130002&pf_rd_r=0J3QFW0VBQYEZ5DQK110&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See the photo gallery </a> </p>    </div>

        </span>
        </div>
    </div>
</div>

                    
                    


                   <br class="clear" />
                </div>





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYr3yB2AnwbIRw10aXdb-_ZjQNMyXmyKjhfqgHUFVGrTKU5dRwiABrIws7eRXQxHSzgyQ-VsecY%0D%0A9zj7UklEYVhJReGN0q3HRCErxTE1npw6kamr7YR3hu6g27WrYGYx4HmB0QGdI6evBX02K5LC94-a%0D%0AzWATd8FIKwYFb3HxJ-4ACUfH8lYxHm-O8FOQfPwqfBYoJna2SaVdRKc2Fs2TCdq_wFczbPETCVq3%0D%0ABpfFd2lUBbk%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYuqILfRFkSxH0lQiAa0ci7tWX8tTyZ8cLmLnjc4QutCCQImFLyLB4X4N3XDA5wl7tm3VsODB8R%0D%0AXXsdZKoM72TjWBWdebyKNxW-5ktfh0fBySdEmJ1-fhheEiDFe-FNWVkv-KwFy3Jcs6QUFmgWcQMJ%0D%0AoKi0r_hlald9_WVsE9P6J_OdjvzzIHnqhTH5Espq3gzMZQ2_rguvXZSiOb93jQhQ-g%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYt6Xka7C01r32zC0j-l_ULOoVOtwM3YDw3paMyW4N0ov3zqZHz8XoKfrbEWYNPPb5Xtj2uCS-5%0D%0ApCftSe7TfndyKjI5QTWR7qWo51vnCh6USiNEMldx4HeYIPjqLtBQ3pbbajTaC7FwHnuyhok0I-jd%0D%0ATBB5nFUYHKRBGGA7tmMzELsilP6OJn6a9t_T0QnZCxFCz3HWpwHdGjCYyYeMnPkIOM225qoaIMxg%0D%0AtAx1k0e1WiE2c_4UvBEAoMrp6zXoubNRqwZdf6SGtvh1P4PvL-sebz40vrJljsn6nCtWTSOCVRyu%0D%0AukddW-YfYcJilzpBdL-9QEMjpdYwFpi4wKNeKuJoWnI5dH_apeQvJUASikMemCkqNZINtCiWB2x4%0D%0AVQQKdH9WbkqS7klMh5kooaYE_w_25A%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYqSbSxfG-eltIBzNIivm1BWvgejpCYj8I8FBA6-IMnQGIZARm7CS_XGUqgjO1KGKFXb84Y3tiP%0D%0AEd_1cZm3HSHMyCyRV9UuA8527D2_IxapzwVJsqXQIjkgEE6tAfzKBVR9XNsTK2Cta0bRMXd3AfYh%0D%0AR5vqukxRM9jrgSG93sNwyXOb1g-zrFYZBrsFEoVzbddUC_dkj0PqjNRt5sILEd-5Uw%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYplRIIQ6bpykeGOGUWPWbyhrB363o_RBjCZfttf4KRdfl_J_edlfg55wSR07DCMcfT6r_XCFf2%0D%0AltJK6pfL3pBjyGFTcuJerV4E3VMYMpip_p7HGyvtDVsY1O2bZB0SBZyeiZMxAyS0KXodFqm7jBik%0D%0AaZE-29m1hxZpTaQ9j5XVMj2X2pewhbxnNXAO-WCJGFPE%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYt1GVCHwS-5dJ9P0nG0oXmCHwYLtqfAVh02hAcfOAr3gDZwKpvqYUFkn2Xdcl7CgH2ryf3Fq6X%0D%0AmW2URcdfjKk9dOLGbHHT9rKz_fNjLyKUYaiPAyleHydTG71S4fSUybQRDN9W1He81n2NFDivODs_%0D%0ApEqrx-yjtmS8HL0jI9WMFJiu9IFj0r-9v17-m_DY4wrRlxiezX-yYN-E9Lu_Q4fdAw%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top 250</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Top Movies</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li>    <a href="/offsite/?page-action=ft_ad&token=BCYv-Y_5X_-8ELa8A_sRtS0-zZ3-u1WVyiunZKGgeCChGF0VLGy5E2duDOOIDBRSzICfpX8AGxS4%0D%0AGIR8XaslPcK99CujskaiR4jLEW8Aozw6GPdhIHMx0KBuQG3H5skEgLS_wazlIK_avRtmWD_SmRuk%0D%0AuvPyZl0y-ugAUQlscxIBuqEgndPKEBjToxJiD3svk3buSmjL3txfCv5gcytQLLrn3vU_G3ceXilY%0D%0AYPYXNiig1rI00vfhbGMz8cnw0hPvfZK6C0t14acrVgx0hwLfzQPN8A%0D%0A&ref_=ft_ad"
itemprop='url'>Advertising</a>
</li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYp6pjkx__u5jA_qLL0nSRZ0JfQrzCpQv1aXN_GTMSSP9bwcIp9Nf8JK0e-LXc09hk903z0X2mJ%0D%0AW5LPl0MCFkYih2VB_t7Qltmr_Aedb6jiwGz7eKScM4fWqY5UqYDdlmk_6RzAdj0sDqH707yrsuDo%0D%0A3jyg7t605W_2ziAA2v2hI5bp8LQKKXuxhOHgZfV7_Q3hJYtPK7DlwWQ2hcDjLJtkPA%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYinNwTIcvLvZUjCVr4j1VaSEC9Zs7N8jndF8RHA8d5v8EkaHdN1AtnrrZND9aBndoN_B-6GS0t%0D%0AhnXequmLIl2KAnHmPhef1cFphhywdsjZwZAFohXcA4Ey_yGteL9WVxLVxuQS7dzuwGR0Jb0HOWEb%0D%0Aqgz-WDDQVAK2johmqFp8NbYO4iAa4lnO_dfTZHQutHbj%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYoXlx3317cUycPvurEJBbikKUwb1WRi4onUCORMvNwRkvOVR2rEkjW_Zh1fYbayLBLqZKeTwGK%0D%0AkQ7cAWoBXMzh_BLHpq_a_wh7FwssKHOeBnBb6mSaGGzPIY20x6bzaRtuIi-xQ_B9YK2dfDyjT0FI%0D%0AHmkN9611z8WJQMmPogext6efve789LTXovQEm6RNhpT9XWuwFbBTncKvEi4Fw7csNsKqAiUNICAw%0D%0AD2Pfcu4LCobjbj3GkBYu8oUb5WMs87Nl%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/show_article?conditions';"
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2015
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/';"
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYkP165QuaQyLjEkyXyYYbq379DNEwC4yi7_lBYqSBHgjwaOgMzr2QL4hl5VH02Z2JKG1EpP7ct%0D%0AMQhyg2C7tbyliI0VtYvraJYjnroQ4s3CCRCy_3gTlFR_C9CqZG2G5md_p3-OBd2liseY7z_xu_Wz%0D%0A07FNWP9o37n6oS5KWdVVnWl9pZW7TwDxMnmQwRm25zqJa_V8lGThOFlICr8GBQ9HEhh7V1SwK4qP%0D%0Ar4XHPm5AS9Y%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Instant Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYnqjLkyqWR8L5BdG0W-c4P2zvNptX5hYF9-vFvD2iCdmLwBV-ZxJ9YkTDzBqXqqOJyU31JdZQy%0D%0AlJLtRK8zMizw1NfF-L1NlSGGWy0X1HrRuHxTD6S88axX4XRZKX5OOBuuC8lmWgIySh1qHKVE-uiS%0D%0AtWZwLdvCaiDkeg0PxacvLDHQb7BcODLz6_5ZAiSjP4PTWrL8Mk-cgY7-4rh09LQ69FbAY0ZZgFfc%0D%0AaLBqz5QOaOI%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Instant Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYqMsxYXyN2idFdAOJ20g_riMvHDtzPdxOUTiGzQLLeCo_o2Orh_pH8I-kGt01-U0fGPk5q_4XX%0D%0Adg0yzUbAxA-4Hmw4W_J4DpuCPk8T3dGU2buWtpaE3KRRnwQ0aZEoFvfMtx_tjvcP7_ZKU4-BTrKB%0D%0ACkMEz5e1JbHQ98_cqiwX4IxAvzRMk0Z2eNrzjOi5bp-n920mMO2OJNWY---Qw79TcWdHpImLuToU%0D%0A2CfFEdP5CDo%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYtyu2WsMlZ0dXu3ikFK1toGaOUw1FUsz4jo8hMzcI1HnG1wH2oW8o-JKgbL1US-lEGezcvHyOE%0D%0AKfl5oGQcdtvAAe9YN71LGcDqZ5vInpO-P-8MbW-zScERcCF4QtcZ75e_NfiiPl2kTc4VUQcmaD-W%0D%0Az2E_VzGGSCp0HX-hVLYwJf_qhH2xEdVawNrRjQYlCh1B6baxdHre7FMtRdbgpza9CvyfmPD681gG%0D%0AgD9ZxqfkMJ4%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYvtWeFBmYpFbvygj4r4HdQKbzYcBHNWLHX9eC_pr4OMfrMUsDwYwC7yoCe7GBDnbXPG_CKYixn%0D%0AHJi-SUOdzJz-eNL1r4cUf9-DdFQYKJdOnGN4BLVxvgf9fLdc5mHC72xGf-iL1XbBkbGpy-YxFMB0%0D%0A9wfQz47KQiOxxu7WMSJJAwjz7hXrhK_V-FvBy5aSBAW2WFXZ82vwj43qcp1bFhfYnpBUqE_USdv1%0D%0AHGk7khP2WsQ%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYk6TB8jOke0oG9HRBewro6rBs_E1V5pvjrCyp9W1jKDz9kdOtj3yl_Seq8FZLmRoQsId1pr1oO%0D%0AL689mGjuXFpoD16Lkl3dAHOPyhTmvInwhJyN2X0KL-8cT0Tkg7DTDLD3bKlecKySymNvXxd3r1DB%0D%0AVu6aIrXu9yrigxczp0ZpS8CwEblYA8HVwwy5jR-B6D6e_zX5fovqjVKgLaZ2QbJQGvXPRKriqpJB%0D%0Auqd-ICRPPk8XRPCh0_57Uj9AeJW5Kcku%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYnTqGOUXx4SakEVMLprowXtRUIp1m0oYsxdW7HTe0yN3SnQl_1RGMB5y43SyoK6DH-OdViDm_Z%0D%0AwQLuKKxxnAREEnlHJil_MLZekOO8cWOnpJToLg2wQbMdMU6ic6hJodeb-44GRq0H1oXkUJyn5IGm%0D%0AqmeWcOn5qHBZwK5wYQWud3M%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYkEjVzCFfSuKIQy5VlRU7dhGk-QapWbM5S2NqKqB99uRtUvl-TcnkrdOQM9qtB_iiOgKCtFO-f%0D%0ATuAf2KUfyxSFXvMNq4B-IcDRj5ufUJQepEx7xPiNppO_blL7Te_y1rBXwCOAGKtzyvkPEUDOXzY4%0D%0Aw0s86d5mbUx9IcRy89NgbFU%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-369064624._CB313453487_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-379116075._CB313453520_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-3031882257._CB313453528_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-2041056462._CB313453507_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=01014985a8f956888b1b3dcc656bef2c96af8f765988210906a5f1e70bc7f85e3c33",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=987789831504"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-232398347._CB349580400_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=987789831504&ord=987789831504";
    },"unable to request AAN pixel");
</script>

<div id="flashContent" style="width: 0px; height: 0px; overflow:hidden;">
    <script type="text/javascript">
        if (generic && generic.monitoring.record_metric && flashAdUtils.canPlayFlash) {
            var flashLoaded = 0;
            function swfLoaded() {
                flashLoaded = 1;
                generic.monitoring.record_metric('ads_flash_did_play', 1, true);
            }
            window.onload = function() {
                // Flash was not loaded
                if (flashLoaded === 0) {
                    // Flash could be loaded but wasn't
                    if (flashAdUtils.canPlayFlash()) {
                        generic.monitoring.record_metric('ads_flash_can_but_did_not_play', 0, true);
                    } else {
                        generic.monitoring.record_metric('ads_flash_cannot_play', 0, true);
                    }
                }
            }
            /**
             * There is a rare possibility of window.onload getting called before swfLoaded.
             * Firing this metric to keep track of such calls.
             */
             generic.monitoring.record_metric('ads_flash_page_loaded', 1, true);
         }
    </script>
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="1x1" align="middle">
        <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
        <param name="quality" value="high" />
        <param name="play" value="true" />
        <param name="loop" value="true" />
        <param name="wmode" value="transparent" />
        <param name="scale" value="showall" />
        <param name="menu" value="true" />
        <param name="devicefont" value="false" />
        <param name="salign" value="" />
        <param name="allowScriptAccess" value="always" />

        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" width="1" height="1">
            <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
            <param name="quality" value="high" />
            <param name="play" value="true" />
            <param name="loop" value="true" />
            <param name="wmode" value="transparent" />
            <param name="scale" value="showall" />
            <param name="menu" value="true" />
            <param name="devicefont" value="false" />
            <param name="salign" value="" />
            <param name="allowScriptAccess" value="always" />
        </object>
        <!--<![endif]-->
    </object>
</div>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="587"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
