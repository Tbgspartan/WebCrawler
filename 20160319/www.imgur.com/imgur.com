<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>    Imgur: The most awesome images on the Internet</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="robots" content="follow,index" />
    <meta name="keywords" content="images, funny, image host, image sharing, reaction gif, viral images, current events, cute, visual storytelling, gif" />
    <meta name="description" content="Imgur is the best place to share and enjoy the most awesome images on the Internet. Every day, millions of people use Imgur to be entertained and inspired by funny, heartwarming and helpful images and stories from all around the world." />
    <meta name="copyright" content="Copyright 2016 Imgur, Inc." />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge;" />
    
    <link rel="alternate" type="application/rss+xml" title="Imgur Gallery" href="http://feeds.feedburner.com/ImgurGallery?format=xml" />
    <link rel="icon" type="image/png" href="//s.imgur.com/images/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="//s.imgur.com/images/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="//s.imgur.com/images/favicon-16x16.png" sizes="16x16">
    <link rel="apple-touch-icon-precomposed" href="//s.imgur.com/images/favicon-152.png">
    <meta name="msapplication-TileColor" content="#2cd63c">
    <meta name="msapplication-TileImage" content="//s.imgur.com/images/favicon-144.png">
            <link rel="stylesheet" type="text/css" href="//s.imgur.com/min/global.css?1458322880" />

    
        <link rel="stylesheet" type="text/css" href="//s.imgur.com/min/gallery.css?1458322880" />

            <!--[if IE 9]><link rel="stylesheet" href="//s.imgur.com/include/css/ie-sucks.css?0" type="text/css" /><![endif]-->
    
            <link rel="canonical" href="http://imgur.com/" />
        <meta property="og:url" content="http://imgur.com/" />
    
    <meta name="p:domain_verify" content="a1e7abe8af908cc6dfaf935dd9a20384"/>
    <meta property="og:site_name" content="Imgur" />
    <meta property="fb:admins" content="12331492,12301369" />
    <meta property="fb:app_id" content="127621437303857" />
    <meta property="al:android:url" content="imgur://imgur.com/?from=fbreferral" />
    <meta property="al:android:app_name" content="Imgur" />
    <meta property="al:android:package" content="com.imgur.mobile" />
    <meta property="al:ios:url" content="imgur://imgur.com/?from=fbreferral" />
    <meta property="al:ios:app_store_id" content="639881495" />
    <meta property="al:ios:app_name" content="Imgur" />
    <meta property="al:web:url" content="http://imgur.com/" />
    <meta name="twitter:site" content="@imgur" />
    <meta name="twitter:domain" content="imgur.com" />
    <meta name="twitter:creator" content="@imgur" />
    <meta name="twitter:app:id:googleplay" content="com.imgur.mobile" />
        <meta name="twitter:card" content="summary_large_image" />
    <meta property="og:type" content="website" />
            <meta property="og:title" content="Imgur" />
        <meta name="twitter:title" content="Imgur" />
    
            <meta property="og:description" content="Imgur: The most awesome images on the Internet." />
        <meta name="twitter:description" content="Imgur: The most awesome images on the Internet." />
    
    <meta property="og:image" content="http://s.imgur.com/images/logo-1200-630.jpg?2" />
    <meta property="og:image:height" content="630" />
    <meta property="og:image:width" content="1200" />
    <meta name="twitter:image:src" content="http://s.imgur.com/images/logo-1200-630.jpg?2" />
    <meta name="twitter:image:height" content="630" />
    <meta name="twitter:image:width" content="1200" />
    <!--[if lte IE 8]><script type="text/javascript" src="//s.imgur.com/min/iepoly.js?1458322880"></script>
<![endif]-->
    
        
</head>
<body class="">
                
    <script type='text/javascript'>
    var googletag = googletag || { };
    googletag.cmd = googletag.cmd || [];

    (function() {
        var gads = document.createElement('script');
        gads.async = true;
        gads.type = 'text/javascript';
        var useSSL = 'https:' == document.location.protocol;
        gads.src = (useSSL ? 'https:' : 'http:') + '//www.googletagservices.com/tag/js/gpt.js';
        var node = document.getElementsByTagName('script')[0];
        node.parentNode.insertBefore(gads, node);
    })();
</script>



<div id="topbar">
    <div class="header-center">
        <div id="header-logo">

            <a href="//imgur.com" class="logo " data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@logo@@}}">
                
                <span class="logo-icon"></span>
            </a>
        </div>
        <div id="main-nav">
            <ul>
                <li class="menu-btn">
                    <div class="menu-icon">
                        <div class="bar"></div>
                        <div class="bar"></div>
                        <div class="bar"></div>
                        
                    </div>
                    <div class="link-menu">
                        <div class="test-1868-show">
                            <ul class="topics cf">
                                <li class="item bold" data-value="hot"><a href="//imgur.com/hot" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@hot@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Most Viral</a></li>
                                <li class="item bold" data-value="new"><a href="//imgur.com/new" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@new@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">User Submitted</a></li>
                                                                                                            <li class="item" data-value="Staff Picks">
                                            <a href="//imgur.com/topic/Staff_Picks" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Staff Picks@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Staff Picks</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Funny">
                                            <a href="//imgur.com/topic/Funny" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Funny@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Funny</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Awesome">
                                            <a href="//imgur.com/topic/Awesome" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Awesome@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Awesome</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Aww">
                                            <a href="//imgur.com/topic/Aww" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Aww@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Aww</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="The More You Know">
                                            <a href="//imgur.com/topic/The_More_You_Know" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@The More You Know@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">The More You Know</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Storytime">
                                            <a href="//imgur.com/topic/Storytime" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Storytime@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Storytime</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Current Events">
                                            <a href="//imgur.com/topic/Current_Events" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Current Events@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Current Events</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Design &amp; Art">
                                            <a href="//imgur.com/topic/Design_&amp;_Art" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Design &amp;amp; Art@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Design &amp; Art</a>
                                        </li>
                                    
                                                                                                            <li class="item" data-value="Reaction">
                                            <a href="//imgur.com/topic/Reaction" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Reaction@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Reaction</a>
                                        </li>
                                    
                                                                    
                                                                                                            <li class="item" data-value="Inspiring">
                                            <a href="//imgur.com/topic/Inspiring" class="name" data-jafo="{@@event@@:@@hamburgerTopicClick@@,@@meta@@:{@@location@@:@@header@@,@@galleryType@@:@@topic@@,@@topicTag@@:@@Inspiring@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Inspiring</a>
                                        </li>
                                    
                                
                            </ul>
                            <ul class="dropdown-footer cf">
                                <li><a href="//imgur.com/blog" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@blog@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">blog</a></li>
                                <li><a href="//imgur.com/about"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@about@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">about</a></li>
                                <li><a href="//store.imgur.com" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@store@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">store</a></li>
                                <li><a href="//imgur.com/jobs"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@jobs@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">jobs</a></li>
                                <li><a href="//help.imgur.com/hc/en-us" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@help@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">help</a></li>
                                <li><a href="//imgur.com/apps"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@apps@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">apps</a></li>
                                <li><a href="//imgur.com/tos"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@terms@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">terms</a></li>
                                <li><a href="//imgur.com/privacy"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@privacy@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">privacy</a></li>
                                <li><a href="//imgur.com/removalrequest" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@deletion@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">request deletion</a></li>
                                <li><a href="//api.imgur.com" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@api@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">api</a></li>
                                <li><a href="//imgur.com/privacy#adchoices" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@adchoices@@,@@optExperimentId@@:@@2998040157@@,@@optExperimentVar@@:@@3000540343@@}}">ad choices</a></li>
                            </ul>
                        </div>
                        <ul class="test-1868-hide">
                            <li><a href="//imgur.com/jobs" class="new" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@jobs@@}}">we're hiring!</a></li>
                                                            <li><a href="//imgur.com/blog" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@blog@@,@@new@@:false}}">blog</a></li>
                            
                            <li><a href="//imgur.com/about"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@about@@}}">about imgur</a></li>
                            <li><a href="//store.imgur.com" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@store@@}}">imgur store</a></li>
                            <li><a href="//imgur.com/apps"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@apps@@}}">imgur apps</a></li>
                            <li><a href="//api.imgur.com" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@api@@}}">developer api</a></li>
                            <li><a href="//help.imgur.com/hc/en-us" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@help@@}}">need help?</a></li>
                        </ul>
                        <div class="clear test-1868-hide"></div>
                        <div class="dropdown-footer hamburger-menu test-1868-hide">
                            <div class="terms-footer">
                                <a href="//imgur.com/tos"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@terms@@}}">terms</a>
                                <a href="//imgur.com/privacy"  data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@privacy@@}}">privacy</a>
                                <a class="small-margin-top" href="//imgur.com/privacy#adchoices" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@adchoices@@}}">ad choices</a>
                            </div>
                             
                                <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Ffacebook.com%2Fimgur&amp;width&amp;layout=button&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=35" scrolling="no" frameborder="0" style="display:inline-block; border:none; overflow:hidden; height:20px; width:50px; margin-right:7px;" allowTransparency="true"></iframe>
                                <a href="https://twitter.com/imgur" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false"></a>
                            
                        </div>
                    </div>
                </li>
                <li class="upload-button-container">
                    <a class="upload-button disabled upload-blue" href="javascript:;">
                        <div class="icon icon-upload"></div>
                        <span class="upload-btn-text">upload images</span>
                        <span class="upload-queue-length">23</span>
                        <div id="upload-global-top-bar-mini-progress"></div>
                    </a>
                    <div class="creation-dropdown upload-blue">
                        <div class="selection icon icon-arrow-down"></div>
                        <div class="options">
                            <ul>
                                <li class="item">
                                    <a class="creation-dropdown-upload" href="javascript:;">Upload Images</a>
                                </li>
                                <li class="item">
                                    <a href="//imgur.com/vidgif">Video to GIF</a>
                                </li>
                                <li class="item">
                                    <a href="//imgur.com/memegen" data-jafo="{@@event@@:@@makeAMeme@@,@@meta@@:{@@control@@:@@header@@}}">Make a Meme</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="random-button title-n " title="random mode">
                    <a href="//imgur.com/random" data-jafo="{@@event@@:@@header@@,@@meta@@:{@@link@@:@@random@@}}">
                        <div class="random-icon"></div>
                    </a>
                </li>
            </ul>
        </div>
        <div id="secondary-nav">
            <ul class="user-nav">
                <li id="global-search-container">
                    <form class="search-form" method="get" action="//imgur.com/search">
                        <input id="search-global-input" type="text" name="q" class="search" blur placeholder="Search images"  />
                    </form>

                    <div class="search-icon-container">
                        <div class="search-icon-container-icon"></div>
                    </div>
                </li>
                                    <li class="signin-link"><a class="" href="https://imgur.com/signin?invokedBy=regularSignIn">sign in</a></li>
                    <li id="register"><a class="" href="https://imgur.com/register?invokedBy=regularSignIn">sign up</a></li>
                
            </ul>
        </div> <!-- #secondary-nav -->
    </div> <!-- .header-center -->
</div> <!-- #topbar -->
<div id="cta-container-placeholder"></div>
<div id="cta-lightbox-placeholder"></div>
    



    

    

            

        
    

            <a href="javascript:;" id="upload-global-album-tipsy" class="title-nw nodisplay" title="You can't create an album with just one image"></a>

<div id="upload-global" class="popup">
    <form method="post" id="upload-global-form" enctype="multipart/form-data" action="http://imgur.com/upload">
        

        <div id="upload-global-buttons">
            <div id="upload-global-upload-loader" class="textbox">
                Loading...
            </div>

            <div id="upload-global-file-wrapper">
                <span class="computerIcon"></span>
                <span class="green">browse</span> your computer

                <input accept="image/*" type="file" name="Filedata" multiple="multiple" id="upload-global-file">
            </div>

            <div class="left">
                <div id="upload-global-url">
                    <span class="webIcon"></span>
                    <input type="text" id="upload-global-link-input" class="placeholder-onkeydown" title="enter image URLs" placeholder="enter image URLs" />
                </div>
            </div>

            <div class="clear"></div>

            <div id="upload-global-dragdrop">
                <span class="dragdropIcon"></span>
                <span class="green">drag and drop</span> here
            </div>

            <div class="left">
                <div id="upload-global-clipboard">
                    <div style="text-align: center"><span class="clipboardIcons">Ctrl + V</span></div>
                    <span class="green">paste</span> from your clipboard
                </div>
            </div>

            <div class="clear"></div>
        </div>

        <div class="textbox center" id="upload-global-instructions"></div>

        <!-- gallery submit vars -->
        <input type="hidden" name="current_upload" value="0"/>
        <input type="hidden" name="total_uploads" value="0"/>
        <input type="hidden" name="terms" value="0"/>
        <input type="hidden" name="gallery_type" value=""/>
        <input type="hidden" name="location" value=""/>


        <div id="upload-global-errors" class="textbox error" style="display: none;"></div>

        <div id="upload-global-queue-description" class="textbox nodisplay"></div>

        <div id="upload-global-start-upload">
            <div class="panel">
                <div>

                    <table id="upload-global-actions">
                        <tbody>
                            <tr>

                                <td width="50.4%">

                                    <div id="upload-global-album-checkbox-div">
                                        <input type="hidden" name="gallery_submit" value="0"/>

                                        <button id="upload-global-gallery-checkbox" type="button">
                                            <span class="gallery-icon"></span>share with community
                                        </button>

                                        <div id="upload-global-gallery-checkbox-overlay"></div>

                                        <input type="hidden" name="create_album" value="0"/>

                                        <button id="upload-global-album-checkbox" type="button">
                                            <span class="createAlbum-icon"></span>create album
                                        </button>

                                    </div>

                                </td>
                                <td width="49.6%">
                                    <button type="button" id="upload-global-start-button" class="button-big green" type="submit">
                                        <span class="start-btn-icon"></span>
                                        Start Upload
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>

                <div id="upload-global-album-options">
                    <table id="upload-global-album-layout">
    <tr>
        <td id="upload-global-album-td">
            <input name="album_title" type="text" title="Optional Album Title" class="placeholder-onkeydown" id="upload-global-album-title" maxlength="55" />
        </td>
    </tr>
</table>

                </div>

                <div class="textbox list">
                    <table id="upload-global-file-list-header">
                        <tr class="top-tr">
                            <td width="45" class="edit-td"><h2>Edit</h2></td>
                            <td><h2>Images <span class="upload-queue-length">(0)</span></h2></td>
                            <td width="40"></td>
                        </tr>
                    </table>

                    <table id="upload-global-file-list">
                    </table>
                                            <div class="agreement-notice">By uploading, you agree to our <a href="//imgur.com/tos" target="_blank">terms of service</a></div>
                    
                </div>
            </div>
        </div>

        <div id="upload-global-progress-wrapper">
            <div id="upload-global-progressbar">
                <div class="progressBarBG">
                    <div class="progressBar"></div>
                </div>
            </div>
            <div id="upload-global-upload-queue"></div>

            <div id="upload-global-converting">
                <p>
                    Optimizing your large GIFs...
                </p>
            </div>
        </div>
    </form>
</div>

<div id="upload-global-FF-paste-box"></div>

<div id="hiddenDropZone" class="nodisplay"></div>

<div class="nodisplay">
    <div id="upload-global-image-formats">
         That file type is not supported!

        <div class="textbox supported-formats">
            Supported formats: JPEG, GIF, PNG, APNG, TIFF, BMP, PDF, XCF
        </div>
    </div>
</div>

<div id="upload-global-flash-container" title="upload images from your computer">
    <div id="upload-global-flash-browse"></div>
</div>

    

        <div class="nodisplay">
	<div id="colorbox-confirm" class="popup">
		<h3 id="colorbox-confirm-title"></h3>
		
		<div class="textbox" id="colorbox-confirm-message"></div>

		<div class="margin-top">
			<div class="left">
				<a href="javascript:;" class="colorbox-no-confirm" id="colorbox-confirm-no">No way!</a>
			</div>
			
			<div class="right">
				<div class="small-loader"></div>
				<a href="javascript:;" class="colorbox-confirm" id="colorbox-confirm-yes">I'm sure</a>
			</div>
			
			<div class="clear"></div>
		</div>
	</div>
</div>


    
            
    

    
    
    
    

    <div id="fullbleed-bg"></div>
    <div id="content" class="outside main">
        
                    <div class="sentence-sorting">
            <span class="before-text">
    The

</span>
<div id="section" class="combobox front-page-section sorting-text-align" name="section">
    <div class="selection">
                <div data-type="base" data-value="hot">
            <span class="name">Most Viral</span>
        </div>
        
        
    </div>

    <div class="options nano">
        <div class="js-galleries content">
            <div class="combobox-header-current">current: 
                <div class="combobox-current green bold">
                                            Most Viral
                    
                </div>
            </div>
            <ul>
                <li class="bold item nodisplay" data-type="base" data-value="hot">
                    <a href="/hot/viral" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@hot@@,@@gallerySort@@:@@viral@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Most Viral</a>
                </li>
                <li class="bold item" data-type="base" data-value="new">
                                            <a href="/new/viral" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@new@@,@@gallerySort@@:@@viral@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">User Submitted</a>
                    
                </li>
                <li class="combobox-hairline"></li>
                                                            <li class="item" data-value="Staff Picks">
                            <a href="/topic/Staff_Picks" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Staff Picks@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Staff Picks</a>
                        </li>
                    
                                                            <li class="item" data-value="Funny">
                            <a href="/topic/Funny" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Funny@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Funny</a>
                        </li>
                    
                                                            <li class="item" data-value="Awesome">
                            <a href="/topic/Awesome" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Awesome@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Awesome</a>
                        </li>
                    
                                                            <li class="item" data-value="Aww">
                            <a href="/topic/Aww" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Aww@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Aww</a>
                        </li>
                    
                                                            <li class="item" data-value="The More You Know">
                            <a href="/topic/The_More_You_Know" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@The More You Know@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">The More You Know</a>
                        </li>
                    
                                                            <li class="item" data-value="Storytime">
                            <a href="/topic/Storytime" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Storytime@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Storytime</a>
                        </li>
                    
                                                            <li class="item" data-value="Current Events">
                            <a href="/topic/Current_Events" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Current Events@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Current Events</a>
                        </li>
                    
                                                            <li class="item" data-value="Design &amp; Art">
                            <a href="/topic/Design_&amp;_Art" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Design &amp;amp; Art@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Design &amp; Art</a>
                        </li>
                    
                                                            <li class="item" data-value="Reaction">
                            <a href="/topic/Reaction" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Reaction@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Reaction</a>
                        </li>
                    
                                    
                                                            <li class="item" data-value="Inspiring">
                            <a href="/topic/Inspiring" class="name" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@topic@@,@@gallerySort@@:@@viral@@,@@topicTag@@:@@Inspiring@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Inspiring</a>
                        </li>
                    
                
            </ul>
        </div>
        <div id="custom-group" class="item new-group"><a href="/gallery/custom" data-jafo="{@@event@@:@@gallerySortChange@@,@@meta@@:{@@location@@:@@outside@@,@@galleryType@@:@@custom@@,@@gallerySort@@:@@viral@@,@@previousSort@@:@@viral@@,@@previousType@@:@@hot@@,@@inSearch@@:@@@@}}">Custom Gallery</a></div>
    </div>
    <input type="hidden" name="section" value="hot">
</div><span class="middle-text"> <!-- intentionally not closed -->

    images on the Internet, 


        sorted by
    
    </span> <!-- .middle-text -->
    
    
        
            
    

            
    
    <div id="sort" class="combobox">
        <div class="selection">
            popularity
        </div>
        <div class="options sorting-text-align">
            <div class="combobox-header-current">current: 
                <div class="combobox-current green bold">
                    popularity
                </div>
            </div>
            <ul>
                                    <li class="item nodisplay" value="viral">
                                                    <a href="/hot/viral">popularity</a>
                        
                    </li>
                    <li class="item" value="time">
                                                    <a href="/hot/time">newest first</a>
                        
                    </li>
                                        <li class="item" value="top">
                                                    <a href="/top">highest scoring</a>
                        
                    </li>
                    
                
            </ul>
        </div>
        <input type="hidden" name="sort" value="viral">
    </div>

    

    <div class="sort-options">
                                    <ul>
                                            <li><a href="/gallery/random" id="random-button" class="title-n" title="random mode"><span></span></a></li>
                    
                                            <li><a href="javascript:void(0)" id="past-link" class="title-n" title="past images"><span></span></a></li>
                    
                </ul>
            
        
    </div>
    <div id="user-gallery-message">
        
    </div>
</div>

               
        

                    <div class="panel hidden">
                <div id="imagelist" class="home-gallery">
                                            <script type="text/tag-group-data">[]</script>


<div class="posts br5 first-child">
    

            
        <div class="outside-loader"></div>
        
                    
        

        <div class="cards">
                            <div id="HJKAF" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/HJKAF" data-page="0">
        <img alt="" src="//i.imgur.com/Hvv8DXcb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="HJKAF" type="image" data-up="6192">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="HJKAF" type="image" data-downs="99">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-HJKAF">6,093</span>
                            <span class="points-text-HJKAF">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Kodi Streaming - An Idiot&#039;s guide (by confirmed Idiot)</p>
        
        
        <div class="post-info">
            album &middot; 37,707 views
        </div>
    </div>
    
</div>

                            <div id="zu03v5V" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/zu03v5V" data-page="0">
        <img alt="" src="//i.imgur.com/zu03v5Vb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="zu03v5V" type="image" data-up="10240">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="zu03v5V" type="image" data-downs="97">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-zu03v5V">10,143</span>
                            <span class="points-text-zu03v5V">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Science BITCH!</p>
        
        
        <div class="post-info">
            animated &middot; 4,928,413 views
        </div>
    </div>
    
</div>

                            <div id="AV7IG5T" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/AV7IG5T" data-page="0">
        <img alt="" src="//i.imgur.com/AV7IG5Tb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="AV7IG5T" type="image" data-up="3812">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="AV7IG5T" type="image" data-downs="27">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-AV7IG5T">3,785</span>
                            <span class="points-text-AV7IG5T">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>This man has insane skills</p>
        
        
        <div class="post-info">
            animated &middot; 279,680 views
        </div>
    </div>
    
</div>

                            <div id="GbEeH8a" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/GbEeH8a" data-page="0">
        <img alt="" src="//i.imgur.com/GbEeH8ab.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="GbEeH8a" type="image" data-up="7629">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="GbEeH8a" type="image" data-downs="234">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-GbEeH8a">7,395</span>
                            <span class="points-text-GbEeH8a">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Zoom</p>
        
        
        <div class="post-info">
            image &middot; 353,374 views
        </div>
    </div>
    
</div>

                            <div id="SML2f" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/SML2f" data-page="0">
        <img alt="" src="//i.imgur.com/WRLXXTbb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="SML2f" type="image" data-up="9902">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="SML2f" type="image" data-downs="219">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-SML2f">9,683</span>
                            <span class="points-text-SML2f">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>How to open locks with a bobby pin.</p>
        
        
        <div class="post-info">
            album &middot; 101,791 views
        </div>
    </div>
    
</div>

                            <div id="GLzT0Kg" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/GLzT0Kg" data-page="0">
        <img alt="" src="//i.imgur.com/GLzT0Kgb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="GLzT0Kg" type="image" data-up="8255">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="GLzT0Kg" type="image" data-downs="230">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-GLzT0Kg">8,025</span>
                            <span class="points-text-GLzT0Kg">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>As a farmer, MRW the crops are looking bad this year...</p>
        
        
        <div class="post-info">
            image &middot; 297,796 views
        </div>
    </div>
    
</div>

                            <div id="ApfG5" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/ApfG5" data-page="0">
        <img alt="" src="//i.imgur.com/4rg52ujb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="ApfG5" type="image" data-up="6387">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="ApfG5" type="image" data-downs="86">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-ApfG5">6,301</span>
                            <span class="points-text-ApfG5">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>favorite for when you&#039;re sad</p>
        
        
        <div class="post-info">
            album &middot; 59,119 views
        </div>
    </div>
    
</div>

                            <div id="ALVdJ0J" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/ALVdJ0J" data-page="0">
        <img alt="" src="//i.imgur.com/ALVdJ0Jb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="ALVdJ0J" type="image" data-up="1258">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="ALVdJ0J" type="image" data-downs="22">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-ALVdJ0J">1,236</span>
                            <span class="points-text-ALVdJ0J">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>People have always said that I look exactly like my mum when she was younger, apparently they were right...</p>
        
        
        <div class="post-info">
            image &middot; 910,390 views
        </div>
    </div>
    
</div>

                            <div id="jzApUmK" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/jzApUmK" data-page="0">
        <img alt="" src="//i.imgur.com/jzApUmKb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="jzApUmK" type="image" data-up="3777">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="jzApUmK" type="image" data-downs="81">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-jzApUmK">3,696</span>
                            <span class="points-text-jzApUmK">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>This song always makes me hungry</p>
        
        
        <div class="post-info">
            animated &middot; 273,118 views
        </div>
    </div>
    
</div>

                            <div id="umITXqB" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/umITXqB" data-page="0">
        <img alt="" src="//i.imgur.com/umITXqBb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="umITXqB" type="image" data-up="9494">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="umITXqB" type="image" data-downs="496">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-umITXqB">8,998</span>
                            <span class="points-text-umITXqB">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Double tap THIS</p>
        
        
        <div class="post-info">
            animated &middot; 573,250 views
        </div>
    </div>
    
</div>

                            <div id="KfWW5ie" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/KfWW5ie" data-page="0">
        <img alt="" src="//i.imgur.com/KfWW5ieb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="KfWW5ie" type="image" data-up="606">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="KfWW5ie" type="image" data-downs="29">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-KfWW5ie">577</span>
                            <span class="points-text-KfWW5ie">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Trying to take drunk people home</p>
        
        
        <div class="post-info">
            image &middot; 761,835 views
        </div>
    </div>
    
</div>

                            <div id="adGDBMJ" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/adGDBMJ" data-page="0">
        <img alt="" src="//i.imgur.com/adGDBMJb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="adGDBMJ" type="image" data-up="2403">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="adGDBMJ" type="image" data-downs="50">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-adGDBMJ">2,353</span>
                            <span class="points-text-adGDBMJ">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Still love NASA</p>
        
        
        <div class="post-info">
            image &middot; 67,976 views
        </div>
    </div>
    
</div>

                            <div id="K6PK6" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/K6PK6" data-page="0">
        <img alt="" src="//i.imgur.com/K5xiLSxb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="K6PK6" type="image" data-up="7041">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="K6PK6" type="image" data-downs="128">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-K6PK6">6,913</span>
                            <span class="points-text-K6PK6">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>I sell propane and propane accessories</p>
        
        
        <div class="post-info">
            album &middot; 75,106 views
        </div>
    </div>
    
</div>

                            <div id="vC0JirR" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/vC0JirR" data-page="0">
        <img alt="" src="//i.imgur.com/vC0JirRb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="vC0JirR" type="image" data-up="3520">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="vC0JirR" type="image" data-downs="98">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-vC0JirR">3,422</span>
                            <span class="points-text-vC0JirR">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>20 ft tall graffiti mural of Kanye west kissing himself.</p>
        
        
        <div class="post-info">
            image &middot; 1,440,193 views
        </div>
    </div>
    
</div>

                            <div id="v6bNZld" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/v6bNZld" data-page="0">
        <img alt="" src="//i.imgur.com/v6bNZldb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="v6bNZld" type="image" data-up="2409">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="v6bNZld" type="image" data-downs="73">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-v6bNZld">2,336</span>
                            <span class="points-text-v6bNZld">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Motherly Love?</p>
        
        
        <div class="post-info">
            image &middot; 97,298 views
        </div>
    </div>
    
</div>

                            <div id="diuDf" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/diuDf" data-page="0">
        <img alt="" src="//i.imgur.com/tRTiwIjb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="diuDf" type="image" data-up="2960">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="diuDf" type="image" data-downs="127">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-diuDf">2,833</span>
                            <span class="points-text-diuDf">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>When you feel like the entire world is against you, but dad has your back</p>
        
        
        <div class="post-info">
            album &middot; 33,504 views
        </div>
    </div>
    
</div>

                            <div id="T29jt" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/T29jt" data-page="0">
        <img alt="" src="//i.imgur.com/SSMlpEfb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="T29jt" type="image" data-up="2376">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="T29jt" type="image" data-downs="88">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-T29jt">2,288</span>
                            <span class="points-text-T29jt">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Satan told me to never let this die</p>
        
        
        <div class="post-info">
            album &middot; 18,021 views
        </div>
    </div>
    
</div>

                            <div id="Uwob4cP" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/Uwob4cP" data-page="0">
        <img alt="" src="//i.imgur.com/Uwob4cPb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="Uwob4cP" type="image" data-up="5653">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="Uwob4cP" type="image" data-downs="321">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-Uwob4cP">5,332</span>
                            <span class="points-text-Uwob4cP">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>MRW a buddy asks me why I still have a beard after I borrowed his trimmer.</p>
        
        
        <div class="post-info">
            animated &middot; 376,532 views
        </div>
    </div>
    
</div>

                            <div id="f2s5YaP" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/f2s5YaP" data-page="0">
        <img alt="" src="//i.imgur.com/f2s5YaPb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="f2s5YaP" type="image" data-up="4066">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="f2s5YaP" type="image" data-downs="68">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-f2s5YaP">3,998</span>
                            <span class="points-text-f2s5YaP">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Puss in Boots</p>
        
        
        <div class="post-info">
            animated &middot; 448,487 views
        </div>
    </div>
    
</div>

                            <div id="4go91wj" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/4go91wj" data-page="0">
        <img alt="" src="//i.imgur.com/4go91wjb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="4go91wj" type="image" data-up="3008">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="4go91wj" type="image" data-downs="43">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-4go91wj">2,965</span>
                            <span class="points-text-4go91wj">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>They know they&#039;re going to the park</p>
        
        
        <div class="post-info">
            animated &middot; 236,391 views
        </div>
    </div>
    
</div>

                            <div id="GCTTD" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/GCTTD" data-page="0">
        <img alt="" src="//i.imgur.com/9ROpvahb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="GCTTD" type="image" data-up="3621">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="GCTTD" type="image" data-downs="72">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-GCTTD">3,549</span>
                            <span class="points-text-GCTTD">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>At least one of these should be helpful.</p>
        
        
        <div class="post-info">
            album &middot; 41,461 views
        </div>
    </div>
    
</div>

                            <div id="VMfF8" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/VMfF8" data-page="0">
        <img alt="" src="//i.imgur.com/ZAKaDmmb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="VMfF8" type="image" data-up="3797">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="VMfF8" type="image" data-downs="33">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-VMfF8">3,764</span>
                            <span class="points-text-VMfF8">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Lieutenant Columbo</p>
        
        
        <div class="post-info">
            album &middot; 47,486 views
        </div>
    </div>
    
</div>

                            <div id="ldLpWK4" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/ldLpWK4" data-page="0">
        <img alt="" src="//i.imgur.com/ldLpWK4b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="ldLpWK4" type="image" data-up="2626">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="ldLpWK4" type="image" data-downs="398">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-ldLpWK4">2,228</span>
                            <span class="points-text-ldLpWK4">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>makes sense</p>
        
        
        <div class="post-info">
            image &middot; 138,087 views
        </div>
    </div>
    
</div>

                            <div id="kFbPI5z" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/kFbPI5z" data-page="0">
        <img alt="" src="//i.imgur.com/kFbPI5zb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="kFbPI5z" type="image" data-up="11832">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="kFbPI5z" type="image" data-downs="1019">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-kFbPI5z">10,813</span>
                            <span class="points-text-kFbPI5z">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Who&#039;s wearing pants here?</p>
        
        
        <div class="post-info">
            image &middot; 481,018 views
        </div>
    </div>
    
</div>

                            <div id="qQmsy4w" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/qQmsy4w" data-page="0">
        <img alt="" src="//i.imgur.com/qQmsy4wb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="qQmsy4w" type="image" data-up="2251">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="qQmsy4w" type="image" data-downs="95">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-qQmsy4w">2,156</span>
                            <span class="points-text-qQmsy4w">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Welcome to Colorado</p>
        
        
        <div class="post-info">
            image &middot; 134,043 views
        </div>
    </div>
    
</div>

                            <div id="DEhkMmQ" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/DEhkMmQ" data-page="0">
        <img alt="" src="//i.imgur.com/DEhkMmQb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="DEhkMmQ" type="image" data-up="335">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="DEhkMmQ" type="image" data-downs="188">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-DEhkMmQ">147</span>
                            <span class="points-text-DEhkMmQ">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Bizarro America</p>
        
        
        <div class="post-info">
            image &middot; 238,291 views
        </div>
    </div>
    
</div>

                            <div id="99kIC" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/99kIC" data-page="0">
        <img alt="" src="//i.imgur.com/xU8VZGfb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="99kIC" type="image" data-up="18452">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="99kIC" type="image" data-downs="214">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-99kIC">18,238</span>
                            <span class="points-text-99kIC">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Lord of the Facts</p>
        
        
        <div class="post-info">
            album &middot; 197,311 views
        </div>
    </div>
    
</div>

                            <div id="WoF55A1" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/WoF55A1" data-page="0">
        <img alt="" src="//i.imgur.com/WoF55A1b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="WoF55A1" type="image" data-up="8093">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="WoF55A1" type="image" data-downs="128">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-WoF55A1">7,965</span>
                            <span class="points-text-WoF55A1">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Release the Aww...</p>
        
        
        <div class="post-info">
            animated &middot; 723,467 views
        </div>
    </div>
    
</div>

                            <div id="Di1kezV" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/Di1kezV" data-page="0">
        <img alt="" src="//i.imgur.com/Di1kezVb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="Di1kezV" type="image" data-up="1901">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="Di1kezV" type="image" data-downs="41">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-Di1kezV">1,860</span>
                            <span class="points-text-Di1kezV">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Action corgi</p>
        
        
        <div class="post-info">
            animated &middot; 133,312 views
        </div>
    </div>
    
</div>

                            <div id="17aU19L" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/17aU19L" data-page="0">
        <img alt="" src="//i.imgur.com/17aU19Lb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="17aU19L" type="image" data-up="3002">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="17aU19L" type="image" data-downs="44">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-17aU19L">2,958</span>
                            <span class="points-text-17aU19L">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>guess what we did</p>
        
        
        <div class="post-info">
            image &middot; 191,891 views
        </div>
    </div>
    
</div>

                            <div id="4Wny9cg" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/4Wny9cg" data-page="0">
        <img alt="" src="//i.imgur.com/4Wny9cgb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="4Wny9cg" type="image" data-up="1679">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="4Wny9cg" type="image" data-downs="38">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-4Wny9cg">1,641</span>
                            <span class="points-text-4Wny9cg">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>domestic animals</p>
        
        
        <div class="post-info">
            image &middot; 61,466 views
        </div>
    </div>
    
</div>

                            <div id="Aitp4yD" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/Aitp4yD" data-page="0">
        <img alt="" src="//i.imgur.com/Aitp4yDb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="Aitp4yD" type="image" data-up="1802">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="Aitp4yD" type="image" data-downs="21">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-Aitp4yD">1,781</span>
                            <span class="points-text-Aitp4yD">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Everyone stay back!</p>
        
        
        <div class="post-info">
            image &middot; 1,255,104 views
        </div>
    </div>
    
</div>

                            <div id="04dya" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/04dya" data-page="0">
        <img alt="" src="//i.imgur.com/jWWsKfcb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="04dya" type="image" data-up="7504">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="04dya" type="image" data-downs="103">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-04dya">7,401</span>
                            <span class="points-text-04dya">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>*groans in a very classy and sophisticated kinda way*</p>
        
        
        <div class="post-info">
            album &middot; 90,387 views
        </div>
    </div>
    
</div>

                            <div id="LSaPT" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/LSaPT" data-page="0">
        <img alt="" src="//i.imgur.com/Zi03UTXb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="LSaPT" type="image" data-up="2083">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="LSaPT" type="image" data-downs="185">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-LSaPT">1,898</span>
                            <span class="points-text-LSaPT">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Today makes 4 years I&#039;ve wasted with you faggots. Here&#039;s a photo dump</p>
        
        
        <div class="post-info">
            album &middot; 23,808 views
        </div>
    </div>
    
</div>

                            <div id="e4CiRNb" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/e4CiRNb" data-page="0">
        <img alt="" src="//i.imgur.com/e4CiRNbb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="e4CiRNb" type="image" data-up="1328">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="e4CiRNb" type="image" data-downs="42">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-e4CiRNb">1,286</span>
                            <span class="points-text-e4CiRNb">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Diver finds cinder blocks and shackles at the bottom of a lake.</p>
        
        
        <div class="post-info">
            animated &middot; 93,575 views
        </div>
    </div>
    
</div>

                            <div id="B9c94sa" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/B9c94sa" data-page="0">
        <img alt="" src="//i.imgur.com/B9c94sab.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="B9c94sa" type="image" data-up="3668">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="B9c94sa" type="image" data-downs="82">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-B9c94sa">3,586</span>
                            <span class="points-text-B9c94sa">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Pibble &lt;3</p>
        
        
        <div class="post-info">
            animated &middot; 293,473 views
        </div>
    </div>
    
</div>

                            <div id="rd9Em" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/rd9Em" data-page="0">
        <img alt="" src="//i.imgur.com/lqdHrsgb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="rd9Em" type="image" data-up="1489">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="rd9Em" type="image" data-downs="131">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-rd9Em">1,358</span>
                            <span class="points-text-rd9Em">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Excelling at Excel</p>
        
        
        <div class="post-info">
            album &middot; 10,267 views
        </div>
    </div>
    
</div>

                            <div id="TVdS1" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/TVdS1" data-page="0">
        <img alt="" src="//i.imgur.com/rlZSISMb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="TVdS1" type="image" data-up="1936">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="TVdS1" type="image" data-downs="75">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-TVdS1">1,861</span>
                            <span class="points-text-TVdS1">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>RIP Gawker</p>
        
        
        <div class="post-info">
            album &middot; 23,187 views
        </div>
    </div>
    
</div>

                            <div id="uA8mAoy" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/uA8mAoy" data-page="0">
        <img alt="" src="//i.imgur.com/uA8mAoyb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="uA8mAoy" type="image" data-up="5974">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="uA8mAoy" type="image" data-downs="43">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-uA8mAoy">5,931</span>
                            <span class="points-text-uA8mAoy">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>shark</p>
        
        
        <div class="post-info">
            animated &middot; 463,510 views
        </div>
    </div>
    
</div>

                            <div id="4KA272u" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/4KA272u" data-page="0">
        <img alt="" src="//i.imgur.com/4KA272ub.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="4KA272u" type="image" data-up="1098">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="4KA272u" type="image" data-downs="5">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-4KA272u">1,093</span>
                            <span class="points-text-4KA272u">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Venusaur vs Caterpie</p>
        
        
        <div class="post-info">
            animated &middot; 473,269 views
        </div>
    </div>
    
</div>

                            <div id="usWeh" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/usWeh" data-page="0">
        <img alt="" src="//i.imgur.com/xz6mtxZb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="usWeh" type="image" data-up="2435">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="usWeh" type="image" data-downs="47">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-usWeh">2,388</span>
                            <span class="points-text-usWeh">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Odeith graffiti artist - Lisboa, Portugal</p>
        
        
        <div class="post-info">
            album &middot; 39,754 views
        </div>
    </div>
    
</div>

                            <div id="iUomnHz" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/iUomnHz" data-page="0">
        <img alt="" src="//i.imgur.com/iUomnHzb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="iUomnHz" type="image" data-up="1326">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="iUomnHz" type="image" data-downs="17">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-iUomnHz">1,309</span>
                            <span class="points-text-iUomnHz">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Waait for it...</p>
        
        
        <div class="post-info">
            animated &middot; 221,803 views
        </div>
    </div>
    
</div>

                            <div id="uybbj" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/uybbj" data-page="0">
        <img alt="" src="//i.imgur.com/bXvlNw0b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="uybbj" type="image" data-up="1056">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="uybbj" type="image" data-downs="21">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-uybbj">1,035</span>
                            <span class="points-text-uybbj">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>You&#039;ve got mail</p>
        
        
        <div class="post-info">
            album &middot; 4,737 views
        </div>
    </div>
    
</div>

                            <div id="8qhB140" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/8qhB140" data-page="0">
        <img alt="" src="//i.imgur.com/8qhB140b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="8qhB140" type="image" data-up="12075">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="8qhB140" type="image" data-downs="908">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-8qhB140">11,167</span>
                            <span class="points-text-8qhB140">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>How I see the US presidential election as a nerd.</p>
        
        
        <div class="post-info">
            image &middot; 1,878,846 views
        </div>
    </div>
    
</div>

                            <div id="9VZU1g7" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/9VZU1g7" data-page="0">
        <img alt="" src="//i.imgur.com/9VZU1g7b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="9VZU1g7" type="image" data-up="1245">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="9VZU1g7" type="image" data-downs="36">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-9VZU1g7">1,209</span>
                            <span class="points-text-9VZU1g7">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Cats are liquid</p>
        
        
        <div class="post-info">
            animated &middot; 90,772 views
        </div>
    </div>
    
</div>

                            <div id="YkIzGCN" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/YkIzGCN" data-page="0">
        <img alt="" src="//i.imgur.com/YkIzGCNb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="YkIzGCN" type="image" data-up="1583">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="YkIzGCN" type="image" data-downs="90">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-YkIzGCN">1,493</span>
                            <span class="points-text-YkIzGCN">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Welcome</p>
        
        
        <div class="post-info">
            animated &middot; 163,198 views
        </div>
    </div>
    
</div>

                            <div id="c4phTO2" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/c4phTO2" data-page="0">
        <img alt="" src="//i.imgur.com/c4phTO2b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="c4phTO2" type="image" data-up="1267">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="c4phTO2" type="image" data-downs="32">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-c4phTO2">1,235</span>
                            <span class="points-text-c4phTO2">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Curious  kittys</p>
        
        
        <div class="post-info">
            animated &middot; 83,323 views
        </div>
    </div>
    
</div>

                            <div id="xoZx0oY" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/xoZx0oY" data-page="0">
        <img alt="" src="//i.imgur.com/xoZx0oYb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="xoZx0oY" type="image" data-up="2765">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="xoZx0oY" type="image" data-downs="122">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-xoZx0oY">2,643</span>
                            <span class="points-text-xoZx0oY">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>OP not included in the picture</p>
        
        
        <div class="post-info">
            image &middot; 146,963 views
        </div>
    </div>
    
</div>

                            <div id="JXcXq7E" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/JXcXq7E" data-page="0">
        <img alt="" src="//i.imgur.com/JXcXq7Eb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="JXcXq7E" type="image" data-up="6891">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="JXcXq7E" type="image" data-downs="82">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-JXcXq7E">6,809</span>
                            <span class="points-text-JXcXq7E">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Human, what are this?</p>
        
        
        <div class="post-info">
            animated &middot; 493,331 views
        </div>
    </div>
    
</div>

                            <div id="cQrXWWy" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/cQrXWWy" data-page="0">
        <img alt="" src="//i.imgur.com/cQrXWWyb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="cQrXWWy" type="image" data-up="1295">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="cQrXWWy" type="image" data-downs="23">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-cQrXWWy">1,272</span>
                            <span class="points-text-cQrXWWy">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>No time to explain!</p>
        
        
        <div class="post-info">
            animated &middot; 148,253 views
        </div>
    </div>
    
</div>

                            <div id="hOC4E" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/hOC4E" data-page="0">
        <img alt="" src="//i.imgur.com/GqElMsQb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="hOC4E" type="image" data-up="4626">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="hOC4E" type="image" data-downs="116">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-hOC4E">4,510</span>
                            <span class="points-text-hOC4E">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Why didn&#039;t we know about this earlier?</p>
        
        
        <div class="post-info">
            album &middot; 57,166 views
        </div>
    </div>
    
</div>

                            <div id="7mlle" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/7mlle" data-page="0">
        <img alt="" src="//i.imgur.com/xa4fZd5b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="7mlle" type="image" data-up="1573">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="7mlle" type="image" data-downs="327">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-7mlle">1,246</span>
                            <span class="points-text-7mlle">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>With dare devil season 2 and all I thought I&#039;d give you a little punisher!</p>
        
        
        <div class="post-info">
            album &middot; 16,259 views
        </div>
    </div>
    
</div>

                            <div id="0Il46WT" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/0Il46WT" data-page="0">
        <img alt="" src="//i.imgur.com/0Il46WTb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="0Il46WT" type="image" data-up="844">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="0Il46WT" type="image" data-downs="47">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-0Il46WT">797</span>
                            <span class="points-text-0Il46WT">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Not gonna volunteer as tribute for sure</p>
        
        
        <div class="post-info">
            image &middot; 27,944 views
        </div>
    </div>
    
</div>

                            <div id="mZnCFgz" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/mZnCFgz" data-page="0">
        <img alt="" src="//i.imgur.com/mZnCFgzb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="mZnCFgz" type="image" data-up="1801">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="mZnCFgz" type="image" data-downs="64">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-mZnCFgz">1,737</span>
                            <span class="points-text-mZnCFgz">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>My favorite magic trick i ever seen.</p>
        
        
        <div class="post-info">
            image &middot; 108,734 views
        </div>
    </div>
    
</div>

                            <div id="ZlFaU" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/ZlFaU" data-page="0">
        <img alt="" src="//i.imgur.com/Rlmucuob.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="ZlFaU" type="image" data-up="2146">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="ZlFaU" type="image" data-downs="71">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-ZlFaU">2,075</span>
                            <span class="points-text-ZlFaU">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Bobby</p>
        
        
        <div class="post-info">
            album &middot; 34,694 views
        </div>
    </div>
    
</div>

                            <div id="Ls76sb5" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/Ls76sb5" data-page="0">
        <img alt="" src="//i.imgur.com/Ls76sb5b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="Ls76sb5" type="image" data-up="20810">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="Ls76sb5" type="image" data-downs="313">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-Ls76sb5">20,497</span>
                            <span class="points-text-Ls76sb5">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Clothes shopping done correctly</p>
        
        
        <div class="post-info">
            image &middot; 3,468,890 views
        </div>
    </div>
    
</div>

                            <div id="zyGKdm4" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/zyGKdm4" data-page="0">
        <img alt="" src="//i.imgur.com/zyGKdm4b.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="zyGKdm4" type="image" data-up="1429">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="zyGKdm4" type="image" data-downs="93">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-zyGKdm4">1,336</span>
                            <span class="points-text-zyGKdm4">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Great feature and all but ...</p>
        
        
        <div class="post-info">
            image &middot; 69,829 views
        </div>
    </div>
    
</div>

                            <div id="zY6tjyI" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/zY6tjyI" data-page="0">
        <img alt="" src="//i.imgur.com/zY6tjyIb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="zY6tjyI" type="image" data-up="1067">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="zY6tjyI" type="image" data-downs="87">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-zY6tjyI">980</span>
                            <span class="points-text-zY6tjyI">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>Me browsing user-sub.</p>
        
        
        <div class="post-info">
            animated &middot; 79,045 views
        </div>
    </div>
    
</div>

                            <div id="pDFAE" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/pDFAE" data-page="0">
        <img alt="" src="//i.imgur.com/ahP9kuTb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="pDFAE" type="image" data-up="12852">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="pDFAE" type="image" data-downs="230">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-pDFAE">12,622</span>
                            <span class="points-text-pDFAE">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>It won&#039;t be the same without Joe</p>
        
        
        <div class="post-info">
            album &middot; 137,646 views
        </div>
    </div>
    
</div>

                            <div id="0tGdcOG" class="post" data-tag1="" data-tag2="">
    <a class="image-list-link" href="/gallery/0tGdcOG" data-page="0">
        <img alt="" src="//i.imgur.com/0tGdcOGb.jpg" />
        
                    
            <div class="point-info gradient-transparent-black transition">
                <div class="relative">
                    <div class="pa-bottom">
                        <div class="arrows">
                            <div title="like" class="arrow up " data="0tGdcOG" type="image" data-up="1002">
                                <span class="icon-upvote"></span>
                            </div>
                            <div title="dislike" class="arrow down " data="0tGdcOG" type="image" data-downs="96">
                                <span class="icon-downvote"></span>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="point-info-points" title="points">
                            <span class="points-0tGdcOG">906</span>
                            <span class="points-text-0tGdcOG">points</span>
                        </div>
                    </div>
                </div>
            </div>
        
    </a>
    <div class="hover">
                    <p>@Sarah pls</p>
        
        
        <div class="post-info">
            image &middot; 45,438 views
        </div>
    </div>
    
</div>

            
        </div>

        <div class="clear"></div>

        <div class="imagelist-loader textbox center lineheight small-margin-top">
            <img src="//s.imgur.com/images/loaders/ddddd1_181817/48.gif" />
        </div>
    


    </div>
            <div class="page-divider"><a href="javascript:;" class="scroll-top-text" data-jafo="{@@event@@:@@galleryBackToTop@@,@@meta@@:{@@galleryType@@:@@hot@@,@@gallerySort@@:@@viral@@,@@pageNum@@:0}}">back to top</a></div>
    


                    
                </div>
            
                                                            <div id="top-comments" class="left-column">
                                                                                    <div id="outside-tagging" class="br5">
                                    <div class="group-add-done nodisplay"></div>
        
                                    <div id="tags-current-list" class="tag-list tag-list-immutable">
                                                                                        <div class="panel">
        <div class="textbox">
            <h2>Today&#039;s Best Comments</h2>
        </div>
    
        <div class="captions best-captions">
                            <div class="caption-container rank-0">
                    <p class="clickability">
                        <a href="/gallery/pDFAE/comment/612185536"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="It won&#039;t be the same without Joe" src="//i.imgur.com/ahP9kuTb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/TheQuack">TheQuack</a> 3,559 points
                        </div>
        
                                                    I think Obama should resign a few days early just so Joe gets to be the 45th President for a little while.
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container rank-1">
                    <p class="clickability">
                        <a href="/gallery/8qhB140/comment/612224893"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="How I see the US presidential election as a nerd." src="//i.imgur.com/8qhB140b.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/speakhands">speakhands</a> 3,326 points
                        </div>
        
                                                    HOW THE &#9829; IS CRUZ NOT THE PENGUIN
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container rank-2">
                    <p class="clickability">
                        <a href="/gallery/99kIC/comment/612163804"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="Lord of the Facts" src="//i.imgur.com/xU8VZGfb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/DeathclawSanctuary">DeathclawSanctuary</a> 2,734 points
                        </div>
        
                                                    If this gets love, more will come :)
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container">
                    <p class="clickability">
                        <a href="/gallery/99kIC/comment/612165256"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="Lord of the Facts" src="//i.imgur.com/xU8VZGfb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/themobileappisbroken">themobileappisbroken</a> 2,566 points
                        </div>
        
                                                    <a class="image-link" href="https://media.licdn.com/mpr/mpr/AAEAAQAAAAAAAAYQAAAAJDZmZjM2NDllLTZhYzgtNDhiZS05ZTRlLWQ3M2FiMGY5NDM2OQ.jpg">https://media.licdn.com/mpr/mpr/AAEAAQAAAAAAAAYQAAAAJDZmZjM2NDllLTZhYzgtNDhiZS05ZTRlLWQ3M2FiMGY5NDM2OQ.jpg</a>
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container">
                    <p class="clickability">
                        <a href="/gallery/iTSf1/comment/612128410"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="True fact" src="//i.imgur.com/IULD4Hib.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/TheTipTopTopHat">TheTipTopTopHat</a> 2,424 points
                        </div>
        
                                                    Well duh, that&#039;s plane to sea.
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container">
                    <p class="clickability">
                        <a href="/gallery/zu03v5V/comment/612336520"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="Science BITCH!" src="//i.imgur.com/zu03v5Vb.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/TheLoneliestGiraffe">TheLoneliestGiraffe</a> 2,268 points
                        </div>
        
                                                    His logic is deeply flawed
                        
                    </div>
                </div>

                <div class="clear hr"></div>
                            <div class="caption-container last-child">
                    <p class="clickability">
                        <a href="/gallery/K5QwJ/comment/612124771"></a>
                    </p>

                    <div class="caption">
                    <p>
                        <img original-title="Its not always a journey of self discovery... sometimes it just is" src="//i.imgur.com/TjwleE9b.jpg" />
                    </p>

                        <div class="author">
                            <a href="/user/FaultySage">FaultySage</a> 2,241 points
                        </div>
        
                                                    I feel like most straight girls would think the same thing and still not want to clash clams with said female.
                        
                    </div>
                </div>

                <div class="clear"></div>
            
    
            
        </div>
    </div>


                                        
                                    </div>
                                </div>
        
                                <div id="outside-tagging-showhide" class="no-select title-e short" title=" Show Sidebar">
                                                                            <div id="comment-tab">
                                            <div id="comment-icon-one"></div>
                                            <div id="comment-icon-two"></div>
                                            <div id="comment-icon-three"></div>
                                        </div>
                                    
                                </div>
                            
                        
                        </div>
                    
                
            </div>
        

        <div class="clear"></div>
    </div>

    <div class="nodisplay">
        <div id="past-wrapper" class="popup">
            <div>
                <div class="textbox left">
                    

                    <input type="text" id="days" />

                                            days ago
                    
                </div>
                
                <div id="past-calendar-wrapper" class="textbox left">
                    <div id="past-calendar"></div>
                </div>

                <div id="slider-wrapper" class="textbox left">
                    <div class="ticker negative">&laquo;</div>
                    <div id="slider"></div>
                    <div class="ticker positive">&raquo;</div>
                </div>

                <div id="button-wrapper" class="textbox left">
                    <input type="button" class="button-medium" value="Go" id="past-submit" />
                </div>

                <div class="clear"></div>
            </div>

            <div id="past-loader"></div>
            <div id="past-preview"></div>
        </div>
    </div>

    <div class="tag-group-selector-container"></div>
            <div id="top-tag-container"></div>
    

    <input id="sid" type="hidden" value="fb824b4aa76973a909178f80ed25cde3" />
        

    

            
    

    

            <script type="text/javascript">
            (function() {
                var roll = Math.random();
                if(roll < 0.05) {
                    
                    (function(_,e,rr,s){
                        _errs=[s];var c=_.onerror;_.onerror=function(){var a=arguments;_errs.push(a);
                        c&&c.apply(this,a)};var b=function(){var c=e.createElement(rr),b=e.getElementsByTagName(rr)[0];
                        c.src="//beacon.errorception.com/"+s+".js";c.async=!0;b.parentNode.insertBefore(c,b)};
                        _.addEventListener?_.addEventListener("load",b,!1):_.attachEvent("onload",b)
                    })(window,document,"script","51d1e360b05ccb10310000d2");
                    
                }
            })();
        </script>

        
<!--[if lte IE 8]>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript">
if(typeof jQuery === 'undefined') {
    document.write('<script type="text/javascript" src="//s.imgur.com/include/js/ext/jquery-1.10.2.min.js"><' + '/script>');
}
</script>
<![endif]-->

<!--[if gt IE 8]>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
if(typeof jQuery === 'undefined') {
    document.write('<script type="text/javascript" src="//s.imgur.com/include/js/ext/jquery.2.1.1.min.js"><' + '/script>');
}
</script>
<![endif]-->

<!--[if !IE]> -->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
if(typeof jQuery === 'undefined') {
    document.write('<script type="text/javascript" src="//s.imgur.com/include/js/ext/jquery.2.1.1.min.js"><' + '/script>');
}
</script>
<!--<![endif]-->



        <script type="text/javascript" src="//s.imgur.com/min/global.js?1458322880"></script>

        <script type="text/javascript">
            Imgur.Environment = {
                isDev:         false,
                isSubdomain:   Imgur.Util.isSubdomain('imgur.com'),
                host:          'imgur.com',
                cdnUrl:        '//i.imgur.com',
                staticUrl:     '//s.imgur.com',
                signed:        false,
                auth:          {},
                recaptcha_key: '6LeZbt4SAAAAAKEsafT3QzEFp5vJ1-Z23uy5mPDz',
                msid:          'fb824b4aa76973a909178f80ed25cde3',
                beta:          {
                    enabled:   false,
                    inBeta:    false
                },
                
                ctaConfig:     {"anonymous":[{"active":false,"type":"button","trackingName":"anonymous","localStorageName":"cta-anonymous","url":"\/","buttonText":"start exploring","title":"Welcome to Imgur!","description":"Your new home for the Internet's best visual stories.","background":"{STATIC}\/images\/house-cta\/cta-background.jpg","newTab":false,"buttonColor":"#77ac21"}],"user":[{"active":false,"type":"button","trackingName":"campImgur","localStorageName":"cta-camp1418760143","customClass":"u-pl95","url":"\/\/imgur.com\/camp","buttonText":"Get More Details","title":"Camp Imgur, August 2015","description":"We're 50% sold out! Reserve your space now.","background":"{STATIC}\/images\/house-cta\/cta-camp-imgur.jpg","newTab":true,"buttonColor":"#cf5033"}],"gaming":[{"active":true,"type":"button","trackingName":"gaming","localStorageName":"cta-gaming","url":"\/\/imgur.com\/t\/gaming\/top\/week","buttonText":"Check it out!","title":"You are now browsing the Internet on God Mode.","description":"Awesome unlocked! Imgur has gaming content like you wouldn't believe.","background":"{STATIC}\/images\/house-cta\/cta-twitch.jpg","newTab":false,"buttonColor":"#4d3280"}],"facebook":[{"active":true,"type":"custom","jsReactClassName":"Facebook","trackingName":"facebook","localStorageName":"cta-fb141008","title":"Like us on Facebook!","description":"We're bringing funny to your newsfeed. Like Imgur on Facebook!","background":"{STATIC}\/images\/house-cta\/facebook-day.jpg"}],"twitter":[{"active":true,"type":"custom","jsReactClassName":"Twitter","url":"https:\/\/twitter.com\/imgur","trackingName":"twitter","localStorageName":"cta-tw141008","newTab":true,"title":"Follow us on Twitter!","description":""}],"pinterest":[{"active":true,"type":"custom","jsReactClassName":"Pinterest","trackingName":"pinterest","localStorageName":"cta-pinterest-2015-07-14","title":"Follow us on Pinterest!","background":"{STATIC}\/images\/house-cta\/pinterest.png","customClass":"pinterest"}],"promoted":[{"active":true,"type":"custom","jsReactClassName":"Promoted","trackingName":"promoted","localStorageName":false,"buttonText":"Learn More","title":"What Are Promoted Posts?","description":"And why am I seeing them?","background":"{STATIC}\/images\/house-cta\/cta-promoted.jpg","url":"\/\/imgur.com\/blog\/?p=6101","newTab":true,"customClass":"pp-banner"}],"global":[{"active":false,"trackingName":"cta-survey-2016-03-17","localStorageName":"cta-survey-2016-03-17","type":"button","customClass":"u-pl260 cta-dark-text","url":"https:\/\/www.surveymonkey.com\/r\/ImgurMovies","title":"Hey Imgur!","description":"Have time to take a quick survey?","newTab":true,"buttonText":"YES","buttonColor":"#000000","background":"{STATIC}\/images\/house-cta\/cta-survey.jpg","backgroundColor":"#ffe9d3"},{"active":false,"trackingName":"inCaseYouMissedIt","localStorageName":"cta-icymi-2015-07-14","type":"button","subtype":"app","background":"{STATIC}\/images\/house-cta\/cta-icymi.png","url":"\/\/imgur.com\/topic\/Hand_Picked\/","buttonText":"Take me there","title":"Catch up on the awesome posts you might have missed!","description":"Now featured in the Hand Picked topic.","newTab":false,"customClass":"u-pl260 icymi","buttonColor":"#252525","frontPageOnly":true},{"active":true,"type":"custom","jsReactClassName":"Newsletter","trackingName":"inCaseYouMissedItForm","localStorageName":"apps1440530195","background":"{STATIC}\/images\/house-cta\/cta-icymi-form.png?1440530195","newTab":true,"customClass":"u-pl200"}],"global-lightbox":[{"active":true,"trackingName":"mobileApps","localStorageName":"apps1442873156","type":"custom","jsReactClassName":"GetTheApp","background":"{STATIC}\/images\/space-med-bg.png?1433176979","newTab":true}],"side-gallery":[{"active":false,"localStorageName":"cta-side-whatisimgur","hash":"2gUGa","is_album":true,"variation":"what-is-imgur","readonly":true}]},
                experiments:   {"exp1868":{"active":false,"name":"hamburger-topics","inclusionProbability":1,"expirationDate":"2015-09-30T00:00:00.000Z","variations":[{"name":"hamburger-topics","inclusionProbability":0.5}]},"exp4053":{"active":false,"name":"recs-reddit-new","inclusionProbability":0.2,"expirationDate":"2015-12-30T00:00:00.000Z","variations":[{"name":"recs-new-users","inclusionProbability":0.5}]},"exp3025":{"active":true,"name":"virality-flavors","inclusionProbability":0.1,"expirationDate":"2015-12-17T00:00:00.000Z","variations":[{"name":"beta","inclusionProbability":0},{"name":"gamma","inclusionProbability":0},{"name":"delta","inclusionProbability":0}]},"exp4595":{"active":true,"name":"implicit-promo-optin","inclusionProbability":1,"startDate":1458410407000,"bucketFromDate":1456165800000,"bucketUntilDate":1456597800000,"expirationDate":1458757800000,"variations":[{"name":"control","inclusionProbability":1},{"name":"explicit","inclusionProbability":0},{"name":"implicit","inclusionProbability":0}]}},
                isGalleryAdmin:false,
                ppBlog: '//imgur.com/blog/?p=6101'
            };

            var imgur = Imgur.getInstance();
            imgur.init(_.merge(Imgur.Environment, {
                
                
                beta: {"enabled":false,"inBeta":false,"showInvite":false,"ctaInvite":[],"ctaWelcome":[]},
            }));

                            var uploader = Imgur.Upload.Global.getInstance(Imgur.getInstance()._);

                if(uploader._.el.$uploadGlobal.length > 0) {
                    uploader.bindPasteUpload();
                    uploader.generalInit();
                }
            

            imgur.generalInit();

            

            var _widgetFactory = new Imgur.Factory.Widget(Imgur.Environment);

            _widgetFactory.mergeConfig('global', Imgur.Environment);

            
        </script>

        <script type="text/javascript">
            var __nspid="1mvmyo";
            var __nsptags=[];

            (function() {
                var roll = Math.random();
                if(roll < 0.1) {
                    
                        // This is the NSONE Pulsar tag
                        (function(w, d) { var x = function() {
                        var j=d.createElement("script");j.type="text/javascript";j.async=true;
                        j.src="http"+("https:"===d.location.protocol?"s://cs":"://c")+".ns1p.net/p.js?a="+__nspid;
                        d.body.appendChild(j); }
                        if(w.addEventListener) { w.addEventListener("load", x, false); }
                        else if(w.attachEvent) { w.attachEvent("onload", x); }
                        else { w.onload = x; }
                        }(window, document));

                    
                }
            })();
        </script>

    

    

                    <script type="text/javascript">
            (function(widgetFactory) {
                widgetFactory.mergeConfig('analytics', {
                    isAdmin: false,
                    logPixel: '//p.imgur.com/lumbar.gif'
                });
                widgetFactory.mergeConfig('global', {
                    analyticsAccountId: 'UA-6671908-2'
                });

                widgetFactory.mergeConfig('search', {
                    type: 'global', 
                    q: ''
                });

                widgetFactory.produceJafo();

                widgetFactory.startExperiments();

                                    widgetFactory.produceCtaBanner('/gallery/HJKAF');
                
            })(_widgetFactory);
        </script>
    

    <script type="text/javascript">
    (function(widgetFactory) { 
        widgetFactory.mergeConfig('global', {
            sid: 'fb824b4aa76973a909178f80ed25cde3'
        });

        widgetFactory.mergeConfig('gallery', {
            sid         : 'fb824b4aa76973a909178f80ed25cde3',
            signed      : false,
            baseURL     : decodeURIComponent('%2Fgallery'),
            section     : 'hot',
            sort        : 'viral',
            window      : 'day',
            page        : 0,
            isPro       : false,
            topic       : null,
            tag         : null,
            subreddit   : null,
            maxPage     : 1904,
            showPast    : true,
            searchQuery : '',
            inSearch    : false,
            advSearch   : null,
            bestCaps    : true
        });

        widgetFactory.mergeConfig('groups', {
            groups: {
                
            }
        });

        widgetFactory.mergeConfig('search', {
            
        });
    })(_widgetFactory);
    </script>

    <script type="text/javascript">
        (function(widgetFactory) {
            var group_type = 'base';

            var group = {
                id: '[hot]',
                tags: [
                    
                ]
            }

            widgetFactory.mergeConfig('gallery', {
                section            : 'hot',
                showGalleryNewInfo : false,
                gallery_type       : null,
                topic              : null,
                tag                : null,
                subreddit          : null,
                sponsoredTag       : null,
                tagFollower        : false,
                group_type         : group_type,
                group              : group
            });
        })(_widgetFactory);
    </script>


            <script type="text/javascript">
            if (typeof _widgetFactory !== 'undefined') {
                _widgetFactory.initNotifications({"post":{"postUpvote":[10,50,100,500,1000,5000],"postComment":[10,50,100],"postView":[100000,500000,1000000,5000000],"postEmbed":[1],"postViral":[1]},"comment":{"commentUpvote":[10,100,500,1000,5000,10000],"commentReply":[1],"commentTop7":[1],"commentMention":[1]},"trophy":{"userTrophy":[1]},"account":{"userNotoriety":[399,999,1999,3999,7999,19999,9223372036854775807]},"message":{"message-msg":[1]}}, [{"name":"Forever Alone","reputation":-1},{"name":"Neutral","reputation":399},{"name":"Accepted","reputation":999},{"name":"Liked","reputation":1999},{"name":"Trusted","reputation":3999},{"name":"Idolized","reputation":7999},{"name":"Renowned","reputation":19999},{"name":"Glorious","reputation":9223372036854775807}]);
            }
        </script>
    

        <script type="text/javascript" src="//s.imgur.com/min/index.js?1458322880"></script>

    <script type="text/javascript" src="//s.imgur.com/min/gallery.js?1458322880"></script>

    <script type="text/javascript">
    (function(widgetFactory) {
        var emitter = new Imgur.Emitter();

        widgetFactory.produceLocationCommand(emitter);

        var galleryFactory = widgetFactory.produceOutsideGalleryFactory();
        var galleryGraph = galleryFactory.produceOutsideGallery(emitter);

        var gallery = galleryGraph.gallery,
            history = galleryGraph.history;

        var outsideTaggingWidget = galleryFactory.produceOutsideTaggingWidget(emitter, galleryGraph);

        gallery.outsideInit();

        

        Imgur.Upload.Index.getInstance().generalInit();
    })(_widgetFactory);
    </script>

    <script type="text/javascript">
    Imgur.Util.jafoLog({ 
        event: 'galleryDisplay', 
        meta: Imgur.Gallery.getInstance().getGalleryMetadata() 
    });
    </script>

    

            <script type="text/javascript">
        if (typeof _widgetFactory !== 'undefined') {
            _widgetFactory.produceSearch();
            _widgetFactory.initIdleMonitor();
        }
        </script>
    

        
    

            <script type="text/javascript">
            Imgur.Util.getGoogleTracker();
            __ga('send', 'pageview');
        </script>

        
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
        

        

        

        <!-- Begin comScore Tag -->
        <script type="text/javascript">
            var _comscore = _comscore || [];
            _comscore.push({ c1: "2", c2: "7770950" });
            (function() {
                var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
                s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
                el.parentNode.insertBefore(s, el);
            })();
        </script>
        <noscript>
            <img src="http://b.scorecardresearch.com/p?c1=2&c2=7770950&cv=2.0&cj=1" />
        </noscript>
        <!-- End comScore Tag -->

        <!-- Quantcast Tag -->
        <script type="text/javascript">
        var _qevents = _qevents || [];
        (function() {
            var elem = document.createElement('script');
            elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
            elem.async = true;
            elem.type = "text/javascript";
            var scpt = document.getElementsByTagName('script')[0];
            scpt.parentNode.insertBefore(elem, scpt);
        })();
        _qevents.push({
            qacct:"p-f8oruOqDFlMeI"
        });
        </script>

        <noscript>
            <div style="display:none;"><img src="//pixel.quantserve.com/pixel/p-f8oruOqDFlMeI.gif" border="0" height="1" width="1" alt="Quantcast"/></div>
        </noscript>
        <!-- End Quantcast tag -->

        <!-- Facebook Pixel Code -->
        <script type="text/javascript">
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','//connect.facebook.net/en_US/fbevents.js');

            fbq('init', '742377892535530');
            fbq('track', 'PageView');

            if (document.location.search === '?reg') {
                fbq('track', 'CompleteRegistration');
            }
        </script>
        <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1076474169030064&ev=PageView&noscript=1"/></noscript>
        <!-- End Facebook Pixel Code -->
    

        <script type="text/javascript">
        
        (function(){
            try{
                var msg = '\n      _\n     (_)\n      _ _ __ ___   __ _ _   _ _ __\n     | | \'_ ` _ \\ / _` | | | | \'__|\n     | | | | | | | (_| | |_| | |\n     |_|_| |_| |_|\\__, |\\__,_|_|\n                   __/ |\n                  |___/\n';
                msg += '========================================\nYou opened the console! Know some code,\ndo you? Want to work for one of the best\nstartups around? http://imgur.com/jobs\n========================================\n';
                imcon.log(msg);
            }catch(e){}
        })()
        
    </script>
    

        

</body>
</html>
