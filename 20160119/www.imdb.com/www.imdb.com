



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

    
    
    

    
    
    

    <meta name="apple-itunes-app" content="app-id=342792525, app-argument=imdb:///?src=mdot">
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "121-0853651-7164883";
                var ue_id = "04PX47AP1WQ2JESHWXEB";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        
        <meta property="pageType" content="home" />
        <meta property="subpageType" content="main" />


        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />

    <meta property='fb:app_id' content='115109575169727' />

    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="04PX47AP1WQ2JESHWXEB" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1a-c3-2xl-i-e9facc59.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-2467623394._CB300617431_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-3118624260._CB293333852_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-2454701167._CB293329573_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-2508181169._CB288096406_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-3988610918._CB289934424_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
(function(){var d=function(o){return Object.prototype.toString.call(o)==="[object Array]";},g=function(q,p){var o;for(o=0;o<q.length;o++){if(o in q){p.call(null,q[o],o);}}},h=[],k,b,l=false,n=false,f=function(){var o=[],p=[],q={};g(h,function(s){var r="";g(s.dartsite.split("/"),function(t){if(t!==""){if(t in q){}else{q[t]=o.length;o.push(t);}r+="/"+q[t];}});p.push(r);});return{iu_parts:o,enc_prev_ius:p};},c=function(){var o=[];g(h,function(q){var p=[];g(q.sizes,function(r){p.push(r.join("x"));});o.push(p.join("|"));});return o;},m=function(){var o=[];g(h,function(p){o.push(a(p.targeting));});return o.join("|");},a=function(r,o){var q,p,s=[];for(q in r){p=[];for(j=0;j<r[q].length;j++){p.push(encodeURIComponent(r[q][j]));}if(o){s.push(q+"="+encodeURIComponent(p.join(",")));}else{s.push(q+"="+p.join(","));}}return s.join("&");},e=function(){var o=+new Date();if(n){return;}if(!this.readyState||"loaded"===this.readyState){n=true;if(l){imdbads.cmd.push(function(){for(i=0;i<h.length;i++){generic.monitoring.record_metric(h[i].name+".fail",csm.duration(o));}});}}};window.tinygpt={define_slot:function(r,q,o,p){h.push({dartsite:r.replace(/\/$/,""),sizes:q,name:o,targeting:p});},set_targeting:function(o){k=o;},callback:function(q){var r,p={},t,o,s=+new Date();l=false;for(r=0;r<h.length;r++){t=h[r].dartsite;o=h[r].name;if(q[r][t]){p[o]=q[r][t];}else{window.console&&console.error&&console.error("Unable to correlate GPT response for "+o);}}imdbads.cmd.push(function(){for(r=0;r<h.length;r++){ad_utils.slot_events.trigger(h[r].name,"request",{timestamp:b});ad_utils.slot_events.trigger(h[r].name,"tagdeliver",{timestamp:s});}ad_utils.gpt.handle_response(p);});},send:function(){var r=[],q=function(s,t){if(d(t)){t=t.join(",");}if(t){r.push(s+"="+encodeURIComponent(""+t));}},o,p;if(h.length===0){tinygpt.callback({});return;}q("gdfp_req","1");q("correlator",Math.floor(4503599627370496*Math.random()));q("output","json_html");q("callback","tinygpt.callback");q("impl","fifs");q("json_a","1");result=f();q("iu_parts",result.iu_parts);q("enc_prev_ius",result.enc_prev_ius);q("prev_iu_szs",c());q("prev_scp",m());q("cust_params",a(k,true));o=document.createElement("script");p=document.getElementsByTagName("script")[0];o.async=true;o.type="text/javascript";o.src="http://pubads.g.doubleclick.net/gampad/ads?"+r.join("&");o.id="tinygpt";o.onload=o.onerror=o.onreadystatechange=e;l=true;p.parentNode.insertBefore(o,p);b=+new Date();}};})();</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[300,600],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['c'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['2'],
'u': ['178956282343'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-tarnhelm-3603579474._CB300876816_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"d121b8284177adb80e935987b619c04facf6aac5",
"2016-01-19T18%3A03%3A29GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 50191;
generic.days_to_midnight = 0.5809143781661987;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-3512629095._CB289935695_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'c']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm == null || window.generic == null || window.consoleLog == null)) {
                    if (window.console && console.log) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=178956282343;ord=178956282343?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;ord=178956282343?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;ord=178956282343?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/chart/toptv/?ref_=nv_tp_tv250_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                        <li><a href="/chart/top?ref_=nv_mv_250_6"
>Top Rated Movies</a></li>
                        <li><a href="/chart/moviemeter?ref_=nv_mv_mpm_7"
>Most Popular Movies</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/chart/?ref_=nv_ch_cht_1"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_2"
>Oscar Winners</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_3"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/imdbpicks/tv-releases/?ref_=nv_tvv_picks_1"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_2"
>On Tonight</a></li>
                        <li><a href="/chart/toptv/?ref_=nv_tvv_250_3"
>Top Rated TV Shows</a></li>
                        <li><a href="/chart/tvmeter?ref_=nv_tvv_mptv_4"
>Most Popular TV Shows</a></li>
                        <li><a href="/feature/watch-now-on-amazon/?ref_=nv_tvv_wn_5"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_6"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_2"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/imdbpicks/?ref_=nv_sf_picks_3"
>IMDb Picks</a></li>
                        <li><a href="/best-of/?ref_=nv_sf_bstof_4"
>Best of 2015</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=01-19&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_2"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_3"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_4"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_5"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_6"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_7"
>Emmy Awards</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_8"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_9"
>Toronto Film Festival</a></li>
                        <li><a href="/event/all/?ref_=nv_ev_all_10"
>All Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni59399393/?ref_=nv_nw_tn_1"
> Idris Elba Says Hollywood's Diversity Problem Extends Across the Pond, Too
</a><br />
                        <span class="time">4 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59397672/?ref_=nv_nw_tn_2"
> Glenn Frey, Founding Eagles Guitarist, Dead at 67
</a><br />
                        <span class="time">19 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59398506/?ref_=nv_nw_tn_3"
> Academy President âHeartbroken and Frustratedâ Over Lack of Diversity in Oscars
</a><br />
                        <span class="time">14 hours ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_3"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5>
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt1853728/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTk4NzQwODM5MF5BMl5BanBnXkFtZTcwNjgzNTI3Nw@@._V1._SY315_CR40,0,410,315_.jpg",
            titleYears : "2012",
            rank : 57,
                    headline : "Django Unchained"
    },
    nameAd : {
            clickThru : "/name/nm0000204/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMjI2NDQxNzUwM15BMl5BanBnXkFtZTcwNDg0MzcyNw@@._V1._SX250_CR0,0,250,315_.jpg",
            rank : 56,
            headline : "Natalie Portman"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYg4FICExaJxKIS1dYgE3wN8FrC2ZptY1Wy89qBHYo9r9XhdlhXmDR_mxZRsDJtf2XxN2ZFxYv_%0D%0AJg4rNq2KuFbwxY8ai46nygVkgEmNb5W2-lztnRb7sOZ7eG32zIrYJlHpPn4IdHHrIvJp7xqVSo-M%0D%0AfNGk0UuiDlQ0abUkoJk50zzJp6ZcbChOgRUomeMEToBCK6OC_nzq9MDNSbmiJ3mE_w%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYsGobGBjBkhFOLuaneSo3e9nkVkPIhikPnWr-fPu-rm0D_LldaQgO0OqJvxeWOKxh69rKbKsE_%0D%0AUWktRNSJrqNc35TkFIImq-S8QLLjYFJE5CUbtqRcfo-_zdHu8VGc59ZjSg8MrKGpc1hJNGWbMfII%0D%0AbgbX8MzGdeFOFzW-6LmQZSAqFnGZXqJk8tsXUhDxsIlj%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYqbuifI39cvqVzQKj5BeNqaIwS8PP44PGIPsPIKDGyny0R4-jqH-JnuvmYBN43afbyhoMv6LRh%0D%0Aly-KCpXOgRm-ZMLDGbrB3z-0rwrS3pcM9Q4WS0-ev5laKii2VeCEaXA8_5ByQiwemgLTq7Ra9LKo%0D%0Ak85JbPlserYODlivSep3333K_XUat7NONQKIbOlDWjC5%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=178956282343;ord=178956282343?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>

    <div id="pagecontent">
    <div class="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=178956282343;ord=178956282343?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	

    
    
    

    
    
    

    
    
    
    </div>
    <div id="top-slot-wrapper" class="pagecontent">

    
    
    

    
    
    
    </div>
    <div class="pagecontent">
        <div id="content-2-wide">
            <div id="main">

    
    
        <a name="slot_hero"></a>
        <div class="heroWidget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','HeroWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_hero">
<div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi664581401?ref_=hm_hp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2392744802&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi664581401" data-source="bylist" data-id="ls002322762" data-rid="04PX47AP1WQ2JESHWXEB" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="Returning stars Seth Rogen, Zac Efron, and Rose Byrne are joined by Chloï¿½ï¿½ï¿½ï¿½ Grace Moretz for the follow-up to the 2014 original in a film that follows what happens when the will of parenthood goes against the bonds of sisterhood." alt="Returning stars Seth Rogen, Zac Efron, and Rose Byrne are joined by Chloï¿½ï¿½ï¿½ï¿½ Grace Moretz for the follow-up to the 2014 original in a film that follows what happens when the will of parenthood goes against the bonds of sisterhood." src="http://ia.media-imdb.com/images/M/MV5BMTk0NTg1NjI3OF5BMl5BanBnXkFtZTgwNTE5MzI3NzE@._V1_SX201_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk0NTg1NjI3OF5BMl5BanBnXkFtZTgwNTE5MzI3NzE@._V1_SX201_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Returning stars Seth Rogen, Zac Efron, and Rose Byrne are joined by Chloï¿½ï¿½ï¿½ï¿½ Grace Moretz for the follow-up to the 2014 original in a film that follows what happens when the will of parenthood goes against the bonds of sisterhood." title="Returning stars Seth Rogen, Zac Efron, and Rose Byrne are joined by Chloï¿½ï¿½ï¿½ï¿½ Grace Moretz for the follow-up to the 2014 original in a film that follows what happens when the will of parenthood goes against the bonds of sisterhood." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Returning stars Seth Rogen, Zac Efron, and Rose Byrne are joined by Chloï¿½ï¿½ï¿½ï¿½ Grace Moretz for the follow-up to the 2014 original in a film that follows what happens when the will of parenthood goes against the bonds of sisterhood." title="Returning stars Seth Rogen, Zac Efron, and Rose Byrne are joined by Chloï¿½ï¿½ï¿½ï¿½ Grace Moretz for the follow-up to the 2014 original in a film that follows what happens when the will of parenthood goes against the bonds of sisterhood." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt4438848/?ref_=hm_hp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2392744802&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > Neighbors 2 </a> </div> </div> <div class="secondary ellipsis"> Official Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi815445273?ref_=hm_hp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2392744802&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi815445273" data-source="bylist" data-id="ls002322762" data-rid="04PX47AP1WQ2JESHWXEB" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="A successful investment banker (Jake Gyllenhaal) struggles after losing his wife in a tragic car crash." alt="A successful investment banker (Jake Gyllenhaal) struggles after losing his wife in a tragic car crash." src="http://ia.media-imdb.com/images/M/MV5BODQxMzMxMTM1N15BMl5BanBnXkFtZTgwNzA0MzU3NjE@._V1_SX201_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BODQxMzMxMTM1N15BMl5BanBnXkFtZTgwNzA0MzU3NjE@._V1_SX201_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="A successful investment banker (Jake Gyllenhaal) struggles after losing his wife in a tragic car crash." title="A successful investment banker (Jake Gyllenhaal) struggles after losing his wife in a tragic car crash." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="A successful investment banker (Jake Gyllenhaal) struggles after losing his wife in a tragic car crash." title="A successful investment banker (Jake Gyllenhaal) struggles after losing his wife in a tragic car crash." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt1172049/?ref_=hm_hp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2392744802&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > Demolition </a> </div> </div> <div class="secondary ellipsis"> Latest Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1452651801?ref_=hm_hp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2392744802&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi1452651801" data-source="bylist" data-id="ls056131825" data-rid="04PX47AP1WQ2JESHWXEB" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="All-new episodes of &quot;Star Wars Rebels&quot; return Jan. 20, 2016, on Disney XD." alt="All-new episodes of &quot;Star Wars Rebels&quot; return Jan. 20, 2016, on Disney XD." src="http://ia.media-imdb.com/images/M/MV5BMTg4NDg0NTY1OV5BMl5BanBnXkFtZTgwNzY3NDI3NzE@._V1_SX201_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg4NDg0NTY1OV5BMl5BanBnXkFtZTgwNzY3NDI3NzE@._V1_SX201_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="All-new episodes of &quot;Star Wars Rebels&quot; return Jan. 20, 2016, on Disney XD." title="All-new episodes of &quot;Star Wars Rebels&quot; return Jan. 20, 2016, on Disney XD." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="All-new episodes of &quot;Star Wars Rebels&quot; return Jan. 20, 2016, on Disney XD." title="All-new episodes of &quot;Star Wars Rebels&quot; return Jan. 20, 2016, on Disney XD." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2930604/?ref_=hm_hp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2392744802&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > "Star Wars Rebels" </a> </div> </div> <div class="secondary ellipsis"> Midseason Trailer </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/trailers?ref_=hm_hp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2392744802&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Browse more trailers</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','HeroWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-1"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/offsite/?page-action=offsite-amazon&token=BCYg2HwkhZ6csxSIDX77AIkYrJ0iD2duw47bcKpCiFsVLi8q21VzSutnUhizNHNY_Hn98G5YRhK1%0D%0Am80cV5fQCNWc06NwFjtDJxU2DCfeKl0SV-aXA0qloM-OxlF1umPKT55nzwpSkvimCBzxZfcWOq-l%0D%0AGFQ_BYT_pf2wzh6FAIBXHMuC7EGruVDkHERje1DMyo19_C2JtiS5ltV5vjWaUwhVN7mPRFN4dRJR%0D%0A0Rr_a7PwX_chckoNJNL4gh58CwVO6gOk%0D%0A&ref_=hm_pop_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391157122&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <h3>IMDb Asks With Host Jerry O'Connell: Marlon Wayans</h3> </a> </span> </span> <div class="widget_content inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?page-action=offsite-amazon&token=BCYrQ11tZ2JPDxepXCHfLYlPA7Mo7ioUSBDBXOYDzfkm-l_y1SCMkacFZsbvz3qgwfny0jUDDgHd%0D%0AlL4LVDNqN4xGU43TRTYCaX15Y7cqL5lXwJcD0rihVkBjOKVXRLEI37rAfGPcW4obaAxTHvTTrHXm%0D%0ArtA6HSwjgWPCHYjJTBp4iRxPyduU0Qs8hPFAx4FHIcQpRFlijyNXtwx2M7oA2er98mnqknUE7U4I%0D%0AleKQR_hDsAW28ZlFiLEWCOyLw26y5Hy0%0D%0A&ref_=hm_pop_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391157122&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTg3MzA1MTE3NV5BMl5BanBnXkFtZTgwMTEzNjI3NzE@._SX1200_CR350,0,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg3MzA1MTE3NV5BMl5BanBnXkFtZTgwMTEzNjI3NzE@._SX1200_CR350,0,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> <div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="widget_inline_blurb"><p class="blurb">On Tuesday, Jan. 19, at 9 p.m. ET/6 p.m. PT, IMDb Asks brings you a livestream Q&A and online chat with <a href="/name/nm0005541/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391157122&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pop_lk1">Marlon Wayans</a>, star of <i><a href="/title/tt4667094/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391157122&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pop_lk2">Fifty Shades of Black</a></i>. Tune in to <a href="/offsite/?page-action=offsite-amazon&token=BCYjEQBb3isi6YuMz_RStQQbKUS5C4iE-kakAxqTK3Cx3jM-U4E2mvAXFLHQBKKgnyUx5qKUlw_T%0D%0AFPsvwhLgAOaL2Cr5E1sMwR0-Lcg4QAK1SGN0LncgO6XK82Ocan_f1rI_AjyIwHXSApFEdFAhIkqx%0D%0AV7zvNS91XQ2elolXs4GJyix4lA6GLuoskFw_dayBJtHb0MjHzfLfaJW3-tSbLvdPrgNbZyw-ePru%0D%0AjylDjQopbfyNlSrY7eVZTi6xmuEiSXIC%0D%0A&ref_=hm_pop_lk3">Amazon.com/MarlonWayans</a> to participate in the live conversation and even ask a question yourself. The livestream is best viewed on laptops, desktops, and tablets.</p> <p class="seemore"><a href="/offsite/?page-action=offsite-amazon&token=BCYuXm1QMInHevBYH3fAbKI_Un_0Mlu3bHInadctREP_Zw5iylj-3rRiqSms0RCqEl8IWOcSU6UL%0D%0A7oRlf1Bp39filGuwMh0o2-UIh5_X0kGNVLokEwqT8cHfsRLg1QRsGLzcbOyyaZZ7NSXAcCZYdid3%0D%0Aaixnla7Ch4jUNH5K_kbZXSLvxppIzuqVN6-Zf0UbAXD-XD2LPE88lHjzj6Q9KlB4bYIS8e3o1niC%0D%0AGB66HnAz47QPCwV0-Yzrot4dGYIt1phF%0D%0A&ref_=hm_pop_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391157122&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" class="position_blurb supplemental" >Tune in here for the one-on-one interview</a></p> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-2"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/imdbpicks/caity-lotz-legends-of-tomorrow-interview/ls031965265?ref_=hm_tv_lot_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391257682&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Caity Lotz Steps Into the Light on "Legends of Tomorrow"</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt4532368/?ref_=hm_tv_lot_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391257682&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Victor Garber, Wentworth Miller, Dominic Purcell, Brandon Routh, Falk Hentschel, Caity Lotz, Franz Drameh, Arthur Darvill and Ciara RenÃ©e in Legends of Tomorrow (2016)" alt="Victor Garber, Wentworth Miller, Dominic Purcell, Brandon Routh, Falk Hentschel, Caity Lotz, Franz Drameh, Arthur Darvill and Ciara RenÃ©e in Legends of Tomorrow (2016)" src="http://ia.media-imdb.com/images/M/MV5BMjI0MzM5ODg3M15BMl5BanBnXkFtZTgwMTM1MzA0NzE@._V1_SX170_CR0,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI0MzM5ODg3M15BMl5BanBnXkFtZTgwMTM1MzA0NzE@._V1_SX170_CR0,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/caity-lotz-legends-of-tomorrow-interview/ls031965265?ref_=hm_tv_lot_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391257682&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTUwODQ4OTY3OV5BMl5BanBnXkFtZTgwMjE1MzU1NzE@._SX800_CR200,50,444,250_SY250_SX444_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUwODQ4OTY3OV5BMl5BanBnXkFtZTgwMjE1MzU1NzE@._SX800_CR200,50,444,250_SY250_SX444_AL_UY500_UX888_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb"><a href="/name/nm2362068/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391257682&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_lot_lk1">Veteran actress and star</a> of the CW's upcoming "<a href="/title/tt4532368/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391257682&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tv_lot_lk2">Legends of Tomorrow</a>" discusses White Canary and reveals how Sara Lance just might be on the road to a brighter future.</p> <p class="seemore"><a href="/imdbpicks/caity-lotz-legends-of-tomorrow-interview/ls031965265?ref_=hm_tv_lot_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391257682&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Read our interview</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-3"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NewsDeskWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59399393?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BNzEzMTI2NjEyNF5BMl5BanBnXkFtZTcwNTA0OTE4OA@@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59399393?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1"
class="headlines" >Idris Elba Says Hollywood's Diversity Problem Extends Across the Pond, Too</a>
    <div class="infobar">
            <span class="text-muted">4 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_src"
>PEOPLE.com</a></span>
    </div>
                                </div>
<p>In the wake of the Oscars snubbing non-white actors for the second year in a row, <a href="/name/nm0252961?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk1">Idris Elba</a> has weighed in on the diversity debate in the entertainment industry - and he says Hollywood isn't the only place where minority stars come up short. Elba, who earned acclaim in <a href="/title/tt1365050?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk2">Beasts of No Nation</a>, was ...                                        <span class="nobr"><a href="/news/ni59399393?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59397672?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2"
class="headlines" >Glenn Frey, Founding Eagles Guitarist, Dead at 67</a>
    <div class="infobar">
            <span class="text-muted">19 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59398506?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3"
class="headlines" >Academy President âHeartbroken and Frustratedâ Over Lack of Diversity in Oscars</a>
    <div class="infobar">
            <span class="text-muted">14 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59397670?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4"
class="headlines" >'Learn How to Read Cards!': Miss Colombia Jokes with Steve Harvey and He Reveals His Family Got Death Threats</a>
    <div class="infobar">
            <span class="text-muted">19 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59399411?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5"
class="headlines" >Tracy Morgan Says Watching Daughter Maven Take Her First Steps Inspired Him to Get out of Wheelchair After Deadly Crash</a>
    <div class="infobar">
            <span class="text-muted">3 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59399393?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BNzEzMTI2NjEyNF5BMl5BanBnXkFtZTcwNTA0OTE4OA@@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59399393?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1"
class="headlines" >Idris Elba Says Hollywood's Diversity Problem Extends Across the Pond, Too</a>
    <div class="infobar">
            <span class="text-muted">4 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_src"
>PEOPLE.com</a></span>
    </div>
                                </div>
<p>In the wake of the Oscars snubbing non-white actors for the second year in a row, <a href="/name/nm0252961?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk1">Idris Elba</a> has weighed in on the diversity debate in the entertainment industry - and he says Hollywood isn't the only place where minority stars come up short. Elba, who earned acclaim in <a href="/title/tt1365050?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk2">Beasts of No Nation</a>, was ...                                        <span class="nobr"><a href="/news/ni59399393?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59398506?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2"
class="headlines" >Academy President âHeartbroken and Frustratedâ Over Lack of Diversity in Oscars</a>
    <div class="infobar">
            <span class="text-muted">14 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59400275?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3"
class="headlines" >30 Great Actors Who've Never Been Oscar Nominated</a>
    <div class="infobar">
            <span class="text-muted">45 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0011867?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3_src"
>The Playlist</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59400222?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4"
class="headlines" >Remembering Eagles Guitarist Glenn Frey's First Film, 'Let's Get Harry'</a>
    <div class="infobar">
            <span class="text-muted">47 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000353?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4_src"
>Thompson on Hollywood</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59400209?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5"
class="headlines" >Philip K Dick film festival: still dreaming of electric sheep</a>
    <div class="infobar">
            <span class="text-muted">50 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000680?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5_src"
>The Guardian - Film News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59399167?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTM3NzU4ODA2M15BMl5BanBnXkFtZTcwOTUyMTE3Mw@@._V1_SY150_CR7,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59399167?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1"
class="headlines" >ABC Stations Renew âLive with Kelly and Michael,â âWho Wants to Be a Millionaireâ</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_src"
>Variety - TV News</a></span>
    </div>
                                </div>
<p> â<a href="/title/tt0096636?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk1">Live with Kelly and Michael</a>â has set a long-term renewal with the ABC-owned station group, assuring that <a href="/name/nm0727961?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk2">Kelly Ripa</a> and <a href="/name/nm1476939?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk3">Michael Strahan</a> will keep yakking in daytime through the 2019-20 season. â<a href="/title/tt0211178?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk4">Who Wants to Be a Millionaire</a>â also has been renewed by its ABC-owned incumbent stations through the ...                                        <span class="nobr"><a href="/news/ni59399167?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59399414?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2"
class="headlines" >Craig Fergusonâs âCelebrity Name Gameâ Renewed For Season 3</a>
    <div class="infobar">
            <span class="text-muted">3 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59400212?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3"
class="headlines" >Neighbors 2 Trailer: Seth Rogen and Zac Efron Team Up Against ChloÃ« Grace Moretzâs Sorority</a>
    <div class="infobar">
            <span class="text-muted">49 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000222?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3_src"
>TVovermind.com</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59400161?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4"
class="headlines" >Relativity Names Tamoor Shafi To Chief Digital Post</a>
    <div class="infobar">
            <span class="text-muted">57 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004913?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4_src"
>Deadline TV</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59400281?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5"
class="headlines" >What Paul Dano and the Cast of 'War & Peace' Discovered About Harvey Weinstein's Favorite Page-Turner</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0032055?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5_src"
>Indiewire Television</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59399411?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BNDMyNzcyMTA5MV5BMl5BanBnXkFtZTcwNDAyMDY4Mg@@._V1_SY150_CR2,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59399411?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1"
class="headlines" >Tracy Morgan Says Watching Daughter Maven Take Her First Steps Inspired Him to Get out of Wheelchair After Deadly Crash</a>
    <div class="infobar">
            <span class="text-muted">3 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_src"
>PEOPLE.com</a></span>
    </div>
                                </div>
<p><a href="/name/nm0605079?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk1">Tracy Morgan</a> says he survived and recovered from the deadly bus crash because of his "star players": his wife and daughter. The 47-year-old comedian stopped by the <a href="/title/tt3444938?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk2">The Tonight Show</a> on Monday, and told host <a href="/name/nm0266422?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk3">Jimmy Fallon</a> about how his 2-year-old daughter Maven gave him the strength he needed to ...                                        <span class="nobr"><a href="/news/ni59399411?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59397672?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2"
class="headlines" >Glenn Frey, Founding Eagles Guitarist, Dead at 67</a>
    <div class="infobar">
            <span class="text-muted">19 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59397670?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3"
class="headlines" >'Learn How to Read Cards!': Miss Colombia Jokes with Steve Harvey and He Reveals His Family Got Death Threats</a>
    <div class="infobar">
            <span class="text-muted">19 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59398091?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4"
class="headlines" >Steve Martin, Carole King and More Celebrities React to Death of Glenn Frey</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59400327?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5"
class="headlines" >Crazy Ex-Girlfriend's Rachel Bloom Calls Out the 'Contradictory Messages Women Are Given'</a>
    <div class="infobar">
            <span class="text-muted">28 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NewsDeskWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-4"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>IMDb Snapshot</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3345475328/rg4157577984?ref_=hm_snp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2392364642&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Suicide Squad (2016)" alt="Suicide Squad (2016)" src="http://ia.media-imdb.com/images/M/MV5BMjI0MzkwMDIzOV5BMl5BanBnXkFtZTgwMDQyNDQ3NzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI0MzkwMDIzOV5BMl5BanBnXkFtZTgwMDQyNDQ3NzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm3345475328/rg4157577984?ref_=hm_snp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2392364642&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <i>Suicide Squad</i> First Posters </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm831645440/rg1528338944?ref_=hm_snp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2392364642&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Americans (2013)" alt="The Americans (2013)" src="http://ia.media-imdb.com/images/M/MV5BMTg3Njk0MTAwMF5BMl5BanBnXkFtZTgwMTUyNDM3NzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg3Njk0MTAwMF5BMl5BanBnXkFtZTgwMTUyNDM3NzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm831645440/rg1528338944?ref_=hm_snp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2392364642&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > Latest Posters </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm483452672/rg784964352?ref_=hm_snp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2392364642&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Shannara Chronicles (2016)" alt="The Shannara Chronicles (2016)" src="http://ia.media-imdb.com/images/M/MV5BMTY3ODgxNjAyOV5BMl5BanBnXkFtZTgwODc0NDA3NzE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY3ODgxNjAyOV5BMl5BanBnXkFtZTgwODc0NDA3NzE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm483452672/rg784964352?ref_=hm_snp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2392364642&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > Latest Stills </a> </div> </div> </div> </div> </div> </div> </div> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-5"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BornTodayWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_borntoday">
<span class="widget_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=1-19&ref_=hm_brn_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Born Today</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0503567?ref_=hm_brn_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Logan Lerman" alt="Logan Lerman" src="http://ia.media-imdb.com/images/M/MV5BMTI5NzI3MjEzN15BMl5BanBnXkFtZTcwMzI5NzAwMw@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTI5NzI3MjEzN15BMl5BanBnXkFtZTcwMzI5NzAwMw@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0503567?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_1">Logan Lerman</a> (24) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0842332?ref_=hm_brn_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Jodie Sweetin" alt="Jodie Sweetin" src="http://ia.media-imdb.com/images/M/MV5BMjI4NTgyODU4Ml5BMl5BanBnXkFtZTgwNDIxMjU2NzE@._V1_SY172_CR71,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI4NTgyODU4Ml5BMl5BanBnXkFtZTgwNDIxMjU2NzE@._V1_SY172_CR71,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0842332?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_2">Jodie Sweetin</a> (34) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0005576?ref_=hm_brn_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Drea de Matteo" alt="Drea de Matteo" src="http://ia.media-imdb.com/images/M/MV5BMTU0MzE3ODMxMl5BMl5BanBnXkFtZTcwOTk2ODY3Mg@@._V1_SY172_CR2,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU0MzE3ODMxMl5BMl5BanBnXkFtZTcwOTk2ODY3Mg@@._V1_SY172_CR2,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0005576?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_3">Drea de Matteo</a> (44) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0005408?ref_=hm_brn_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Katey Sagal" alt="Katey Sagal" src="http://ia.media-imdb.com/images/M/MV5BNDU0ODA0NjMzMF5BMl5BanBnXkFtZTcwMjYyMTk3MQ@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNDU0ODA0NjMzMF5BMl5BanBnXkFtZTcwMjYyMTk3MQ@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0005408?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_4">Katey Sagal</a> (62) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0298807?ref_=hm_brn_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Antoine Fuqua" alt="Antoine Fuqua" src="http://ia.media-imdb.com/images/M/MV5BMTM2NDQ4OTkxNV5BMl5BanBnXkFtZTcwMzQ4NzIyMw@@._V1_SY172_CR2,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTM2NDQ4OTkxNV5BMl5BanBnXkFtZTcwMzQ4NzIyMw@@._V1_SY172_CR2,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0298807?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_5">Antoine Fuqua</a> (50) </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/search/name?refine=birth_monthday&birth_monthday=1-19&ref_=hm_brn_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See all birthdays</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','BornTodayWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-7"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/awards-central/tom-hardy-through-the-years/?ref_=hm_ph_tom_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391261242&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Tom Hardy Through the Years</h3> </a> </span> </span> <p class="blurb">Look back at <i><a href="/title/tt1392190/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391261242&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_tom_lk1">Mad Max: Fury Road</a></i> and <i><a href="/title/tt1663202/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391261242&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_tom_lk2">Revenant</a></i> star <a href="/name/nm0362766/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391261242&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_tom_lk3">Tom Hardy</a>'s screen career in photos.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/tom-hardy-through-the-years/?imageid=rm178712320&ref_=hm_ph_tom_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391261242&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjEyNDYwODkyM15BMl5BanBnXkFtZTgwMDE3NDQ3NjE@._SX700_CR200,50,232,344_SY172_SX116_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjEyNDYwODkyM15BMl5BanBnXkFtZTgwMDE3NDQ3NjE@._SX700_CR200,50,232,344_SY172_SX116_AL_UY344_UX232_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/tom-hardy-through-the-years/?imageid=rm1954084608&ref_=hm_ph_tom_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391261242&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BNzM5MDExMDI1OF5BMl5BanBnXkFtZTgwNjE3NTE2NTE@._SX750_CR270,25,232,344_SY172_SX116_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNzM5MDExMDI1OF5BMl5BanBnXkFtZTgwNjE3NTE2NTE@._SX750_CR270,25,232,344_SY172_SX116_AL_UY344_UX232_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/tom-hardy-through-the-years/?imageid=rm2529012480&ref_=hm_ph_tom_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391261242&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjI0NzQyMDc4N15BMl5BanBnXkFtZTcwNzQzNTMxOA@@._SX850_CR260,220,232,344_SY172_SX116_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI0NzQyMDc4N15BMl5BanBnXkFtZTcwNzQzNTMxOA@@._SX850_CR260,220,232,344_SY172_SX116_AL_UY344_UX232_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/tom-hardy-through-the-years/?imageid=rm2278606080&ref_=hm_ph_tom_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391261242&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTU0NTgxMDE0MV5BMl5BanBnXkFtZTgwNjIwODM4MjE@._SX330_CR70,0,232,344_BR15_SY172_SX116_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU0NTgxMDE0MV5BMl5BanBnXkFtZTgwNjIwODM4MjE@._SX330_CR70,0,232,344_BR15_SY172_SX116_AL_UY344_UX232_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/tom-hardy-through-the-years/?imageid=rm360493824&ref_=hm_ph_tom_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391261242&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTM3OTc0NjYxMF5BMl5BanBnXkFtZTcwNDk0MzExNw@@._SY360_CR160,0,232,344_SY172_SX116_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTM3OTc0NjYxMF5BMl5BanBnXkFtZTcwNDk0MzExNw@@._SY360_CR160,0,232,344_SY172_SX116_AL_UY344_UX232_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/awards-central/tom-hardy-through-the-years/?ref_=hm_ph_tom_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391261242&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See Tom through the years</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-8"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/awards-central/video/?ref_=hm_ac_acd_vid_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391260182&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Oscars&#174; by the Numbers</h3> </a> </span> </span> <p class="blurb">Who was the youngest-ever Best Director nominee? Learn this and more fun trivia about the <a href="/awards-central/oscars/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391260182&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_acd_vid_lk1">Academy Awards&#174;</a>.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:624px;height:auto;" > <div style="width:624px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1150334233?ref_=hm_ac_acd_vid_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391260182&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi1150334233" data-source="bylist" data-id="ls031574778" data-rid="04PX47AP1WQ2JESHWXEB" data-type="playlist" class="video-colorbox" data-refsuffix="hm_ac_acd_vid" data-ref="hm_ac_acd_vid_i_1"> <img itemprop="image" class="pri_image" title="IMDb Originals (2015-)" alt="IMDb Originals (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMTQ4MjQxMTk3MV5BMl5BanBnXkFtZTcwMzc2NTk1Nw@@._V1_SX624_CR0,0,624,351_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ4MjQxMTk3MV5BMl5BanBnXkFtZTcwMzc2NTk1Nw@@._V1_SX624_CR0,0,624,351_AL_UY702_UX1248_AL_.jpg" /> <img alt="IMDb Originals (2015-)" title="IMDb Originals (2015-)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="IMDb Originals (2015-)" title="IMDb Originals (2015-)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/awards-central/video/?ref_=hm_ac_acd_vid_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391260182&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Watch more IMDb Originals video</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-13"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','RecsWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','RecsWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-19"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TriviaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_trivia">
<span class="widget_header"> <span class="oneline"> <a href="/title/tt2293640/trivia?item=tr2605173&ref_=hm_trv_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Did You Know?</h3> </a> </span> </span> <div class="widget_content inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt2293640?ref_=hm_trv_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Minions (2015)" alt="Minions (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTg2MTMyMzU0M15BMl5BanBnXkFtZTgwOTU3ODk4NTE@._V1_SY132_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg2MTMyMzU0M15BMl5BanBnXkFtZTgwOTU3ODk4NTE@._V1_SY132_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> <div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="widget_inline_blurb"><strong class="text-large"><a href="/title/tt2293640?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Minions</a></strong> <p class="blurb">The first Non-Disney or Pixar animated film to take $1 billion worldwide at the box office.</p> <p class="seemore"><a href="/title/tt2293640/trivia?item=tr2605173&ref_=hm_trv_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" class="position_blurb supplemental" >See more trivia</a></p> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','TriviaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-26"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','PollWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;buEQAPelhm4&quot;}">
        
    

    <div class="ab_poll poll">
<span class="widget_header"> <span class="oneline"> <a href="/poll/buEQAPelhm4/?ref_=hm_poll_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390613642&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Poll: Oscars 2016: Best Adapted Screenplay</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390613642&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk">More Polls</a></h4> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/buEQAPelhm4/?ref_=hm_poll_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390613642&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Big Short (2015)" alt="The Big Short (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjM2MTQ2MzcxOF5BMl5BanBnXkFtZTgwNzE4NTUyNzE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjM2MTQ2MzcxOF5BMl5BanBnXkFtZTgwNzE4NTUyNzE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/buEQAPelhm4/?ref_=hm_poll_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390613642&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Brooklyn (2015)" alt="Brooklyn (2015)" src="http://ia.media-imdb.com/images/M/MV5BMzE4MDk5NzEyOV5BMl5BanBnXkFtZTgwNDM4NDA3NjE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzE4MDk5NzEyOV5BMl5BanBnXkFtZTgwNDM4NDA3NjE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/buEQAPelhm4/?ref_=hm_poll_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390613642&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Martian (2015)" alt="The Martian (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTc2MTQ3MDA1Nl5BMl5BanBnXkFtZTgwODA3OTI4NjE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc2MTQ3MDA1Nl5BMl5BanBnXkFtZTgwODA3OTI4NjE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/buEQAPelhm4/?ref_=hm_poll_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390613642&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Room (2015)" alt="Room (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjE4NzgzNzEwMl5BMl5BanBnXkFtZTgwMTMzMDE0NjE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE4NzgzNzEwMl5BMl5BanBnXkFtZTgwMTMzMDE0NjE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/buEQAPelhm4/?ref_=hm_poll_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390613642&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Carol (2015)" alt="Carol (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTcxNTkxMzA5OV5BMl5BanBnXkFtZTgwNTI0ODMzNzE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTcxNTkxMzA5OV5BMl5BanBnXkFtZTgwNTI0ODMzNzE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Which of the 2016 Academy Awards nominees for Best Writing, Screenplay Based on Material Previously Produced or Published do you think should win? Discuss <a href="http://www.imdb.com/board/bd0000088/nest/252617539?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390613642&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk1">here</a> after voting.</p> <p class="seemore"><a href="/poll/buEQAPelhm4/?ref_=hm_poll_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390613642&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Vote now</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','PollWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-28"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388717562&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Catch Up on IMDb Asks</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388717562&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMzAxMjY5MjY4OF5BMl5BanBnXkFtZTgwOTMxMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzAxMjY5MjY4OF5BMl5BanBnXkFtZTgwOTMxMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388717562&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BNjQ0OTEzMTk1OF5BMl5BanBnXkFtZTgwODQyMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjQ0OTEzMTk1OF5BMl5BanBnXkFtZTgwODQyMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388717562&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTY4MjEyMTE5Nl5BMl5BanBnXkFtZTgwNjI0Mzk2NzE@._SX700_CR70,100,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY4MjEyMTE5Nl5BMl5BanBnXkFtZTgwNjI0Mzk2NzE@._SX700_CR70,100,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388717562&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjQwOTY1NTE2M15BMl5BanBnXkFtZTgwNDk5MjM2NzE@._SX830_CR240,0,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQwOTY1NTE2M15BMl5BanBnXkFtZTgwNDk5MjM2NzE@._SX830_CR240,0,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Did you miss one of our one-on-one interviews? Check out our archived interviews with your favorite stars.</p> <p class="seemore"><a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2388717562&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Watch all our one-on-one interviews</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    
            </div>
            <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,300x600,11x1;p=tr;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=178956282343;ord=178956282343?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;ord=178956282343?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;ord=178956282343?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	

    
    
    

    
    
    

    
    
        <a name="slot_right-3"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-in-theaters/?ref_=hm_otw_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390105142&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Opening This Week</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1860213"></div> <div class="title"> <a href="/title/tt1860213?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390105142&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t0"> Dirty Grandpa </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2304933"></div> <div class="title"> <a href="/title/tt2304933?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390105142&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t1"> The 5th Wave </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3882082"></div> <div class="title"> <a href="/title/tt3882082?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390105142&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t2"> The Boy </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2888046"></div> <div class="title"> <a href="/title/tt2888046?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390105142&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t3"> Ip Man 3 </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2049543"></div> <div class="title"> <a href="/title/tt2049543?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390105142&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t4"> Synchronicity </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3781476"></div> <div class="title"> <a href="/title/tt3781476?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390105142&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t5"> Monster Hunt </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2322517"></div> <div class="title"> <a href="/title/tt2322517?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390105142&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t6"> Mojave </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4374460"></div> <div class="title"> <a href="/title/tt4374460?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390105142&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t7"> Aferim! </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4019560"></div> <div class="title"> <a href="/title/tt4019560?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390105142&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t8"> Exposed </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-in-theaters/?ref_=hm_otw_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390105142&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See more opening this week</a></p>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	

    
    
        <a name="slot_right-4"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="showtimesMessagingWidget"> <p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390105162&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Tickets & Showtimes &raquo;</a></p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> </div> </div> </span>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-6"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BoxOfficeListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/chart/?ref_=hm_cht_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Now Playing (Box Office)</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2869728"></div> <div class="title"> <a href="/title/tt2869728?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t0"> Ride Along 2 </a> <span class="secondary-text">$35.3M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1663202"></div> <div class="title"> <a href="/title/tt1663202?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t1"> The Revenant </a> <span class="secondary-text">$31.8M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2488496"></div> <div class="title"> <a href="/title/tt2488496?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t2"> Star Wars: The Force Awakens </a> <span class="secondary-text">$26.4M</span> </div> <div class="action"> <a href="/showtimes/title/tt2488496?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4172430"></div> <div class="title"> <a href="/title/tt4172430?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t3"> 13 Hours: The Secret Soldiers of Benghazi </a> <span class="secondary-text">$16.2M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1528854"></div> <div class="title"> <a href="/title/tt1528854?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t4"> Daddy's Home </a> <span class="secondary-text">$9.5M</span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/chart/?ref_=hm_cht_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See more box office results</a></p>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','BoxOfficeListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-8"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-coming-soon/?ref_=hm_cs_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Coming Soon</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2267968"></div> <div class="title"> <a href="/title/tt2267968?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t0"> Kung Fu Panda 3 </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2025690"></div> <div class="title"> <a href="/title/tt2025690?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t1"> The Finest Hours </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4667094"></div> <div class="title"> <a href="/title/tt4667094?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t2"> Fifty Shades of Black </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2140037"></div> <div class="title"> <a href="/title/tt2140037?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t3"> Jane Got a Gun </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-coming-soon/?ref_=hm_cs_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See more coming soon</a></p>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-12"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TwitterWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_twitter">
<span class="widget_header"> <span class="oneline"> <h3>Follow Us On Twitter</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/twitter-2032675409._CB285310264_.html#{"screen-name":"IMDb","height":"500px","width":"300px"}'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','TwitterWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-14"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','FacebookWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_facebook">
<span class="widget_header"> <span class="oneline"> <h3>Find Us On Facebook</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','FacebookWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-30"></a>
        <div class="aux-content-widget-2 sticky-widget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/awards-central/?ref_=hm_ac_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391257602&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <h3>It's Oscars&#174; Season on IMDb</h3> </a> </span> </span> <p class="blurb">Check out IMDb's full coverage of all the major awards events, including the <a href="/awards-central/oscars/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391257602&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_lk1">2016 Academy Awards</a>.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/?ref_=hm_ac_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391257602&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Michael Keaton, Liev Schreiber, Brian d'Arcy James, Mark Ruffalo, John Slattery and Rachel McAdams in Spotlight (2015)" alt="Still of Michael Keaton, Liev Schreiber, Brian d'Arcy James, Mark Ruffalo, John Slattery and Rachel McAdams in Spotlight (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTY1NTE1Njg0Ml5BMl5BanBnXkFtZTgwOTg1MzgzNjE@._V1_SY525_CR44,0,700,525_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY1NTE1Njg0Ml5BMl5BanBnXkFtZTgwOTg1MzgzNjE@._V1_SY525_CR44,0,700,525_AL_UY1050_UX1400_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/awards-central/?ref_=hm_ac_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2391257602&pf_rd_r=04PX47AP1WQ2JESHWXEB&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Visit our Awards Central section</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    
            </div>
            <br class="clear" />
        </div>
        

    
    
    

    
    
    
        
    
        <br class="clear" />
    </div>
</div>
                





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYjp9X3duY0w-imx62LKgLabd2mSJOP0X4nGo2-pQPokOegmY4uBX8To2O84mHrEQ1Xp9hLc-1S%0D%0Ap1lR98XVuK1KU9TJH8t14L5KJrqbujlyRY8FoSTLcBsvQJJKcucqTDnSVGv-8VteKxYLJ6tq2XfW%0D%0AzW8IgEEBNEbp8Pvt1enWl_akxV3GxC_dxNB3_4OxyJmBf0pa5CYOECE8aA_CzAjWzp5-6ybUlzHg%0D%0AJqPPrtKWw4w%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYjgb1c1KavctJXkZGH48Xk9coqYOkKengUQIp16C329F54fHMr5QYiVBsmAUmIH1wJK2-d98lM%0D%0AsaNBVcRgQDoeyYZQMPCBUzNMInXVL4OsnfrAi0au3mNWamZQGn8cj6RMrqnX12dMtpk6QzjNDVwr%0D%0A4omvZL-SJ4a17B-6PyXxufuwItoPWYuxHMMJOTQ7OkFURkiFJKTUyOo8gCgtxMn2CA%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYocx6r6cJ0_mmOUqFNPoqrwYjk5jU0REAOMTgzlXyhBRpdObNVF1OrK6QMLja4rN_k0bcwCFZ_%0D%0AEv1oBl6ZBA4eQM96mOTf05sl6_CwMxIOiCllw_pJJVwDG98FFK3946PJBUmBOCPmIfYBuqGkdmFK%0D%0AT_exCtRUgdE194aSM4mnEs53U9-ZGIplKbnrCunYGUb_txmpsrI8M26MZ8oHSWlW3624EoxmHT90%0D%0AnLi4TY55-FzWR5vs31te-7YJtuVMq_-P0SR9xeZayWt_Izz1XRgpQWwVo_NrEwwPYjj8GDy0150U%0D%0AG8mgJf_Brhnt8IEBasofIBm64ADrJXUqv_demigkV68r6cgzj521lf7hO9SmWw2fZbc0dZrFip3J%0D%0A4W1njuGjC0woGqckshWNwJjVvUjlJ6QcryyS_pc3QU-fWpJ90uU%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYgQw1H7xHTx4gOd3IznRVlthYh3WrGGqn_BAU9ZVZSRvcWG1A8k_tsFaVBRo6ImFcUgWsc6PSX%0D%0ApsiD-b0ji4OdrhOBPVQfap1httDs73SvG8MN-8qDn3bTy5Os1fMoUyocTf3Fc68WglXdDzQ627p9%0D%0A3miPiFudOTZ3jnElU15xy-c4WWFWy2aUUUixj3ljsKIzpP8TulfWeSJM5E_uoW-BRQ%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYmnEK2obL8j67pHrjRLE90c0TDv1V7XXJwdvaqTdtH0lwHdpCGTrEMjXSF2X0YBwEXLol0g04t%0D%0A-EfSLbubXy1mgNOH0hlwHM66MEsu6mm9JD-mbBpyzWHsos_Tj5FtUDdHOABWcJvKjgO40jPgYFTy%0D%0AJTmE35s4IWO9tcdcG9ZM-LOezSKAXPJnOnTjCRhu9fJd%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYqp657EckCuyUKMYbn2kL0EAYrAQLYyuhX9J0Hn7iyg71jgiJE24DpOyAU83QZTKYs2T0AaCir%0D%0ASG3WdMWMtLSOyPKGm8tlptgBxenrjihqyaUUWupErMgajsKXGC-O4KGl4mirLeH-s4aC-ds75Xtq%0D%0A4zQBrM9iGTPIzuQ519sEdJivUXZlJp0LAnJ3ABU_69vT%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top Rated Movies</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Box Office</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li><a href="/advertising/?ref_=ft_ad"
>Advertising</a></li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYgXXhUA7Mgjnp5ofAAAoe-y_QxSsUia2vgsGm7xcOtILF5kQPxSJCa1gaqorkunWmXOOeZEtxX%0D%0AjBcpDqTWcz47yJgHqH5eYAqUnRJ49bDq7E9eRcJfbLG2ggFvNs3ghu7itpTYdQXfjSEaOtnkJLz2%0D%0AaadlPvFCmGvuMD6_0YBacXJ9wYSA0-7JSEPmH_6jBhp25EXENyt9-cJK_B4m5gEP3Q%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYhVsbNmrdM3CUZu80JCoCAOW8o1EOXlCGNhKZR6auP69UVQax8jwzINObL_v1z2oayf3zZaybV%0D%0AX4RxgD1Rwa-fyjdhdNSn1uAOSFH-7MLKnaLvC7CeKOkhnaEIGtUdb18-rv5cLd0KMZi-WBhR8MdC%0D%0Af-glCLa1waxf2FlK8hS0wyRLUDI4NwDgDnVsvzlLQAnz%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYseSYh5nw9Efp4VI74OQB91XDRi5TQjW1TZX5uIh63k5rpusxtr6K7JLxGOVqxOkUvlG3KQe28%0D%0AwAcZsRPEsE2d43qh4ZXj7Ge9cGWxlkLD50JWQFFoJovVZ-Bnawn2m8p0eKxHdOJUewHa8tE-cKIL%0D%0Ag7PlstMgsiVBO4bPjqWwsF4q-rKxweRx2lEnGfxhgEDnM1JlHhhI2BWAQPOQpkaW06fuojX3VKOh%0D%0AmpT7t_ZpP_mt601JiBaJW63_QHS3BN8u%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2016
                <a
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYip_ueE7hFaQOFI_Ti_obF7QjDQwdYNsN-AiDLx5wu8Txkl2sj5ph_t3-n2m6CEoOTBRZZHH84%0D%0AU6st4Qydkf4f7wmIn8-8IWF_poFhZdRDweb6Sbh82-arc6PJ2P2p48z5YKqYNp6DHi-EGS2iJ3Ue%0D%0A_6RZxEOyPzDCIF2KqC83TSjzidXisASuMWtRdyy_A9u40kn8Lk5Kj7TbV1fOAZh8fLgaxLbW70K1%0D%0AL2wWqnuT0UU%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYuEauPkpFNpMQOEYxBiVXkrnpoqnst8FQFFXcbxyfNCPyS2-wY36-rKUH4lsa1_NPt-HzzCnyN%0D%0AkEZ72s5i6J5tOCf0KoWmyM5_8XwgoNjgiCIplVTHWX7Rhf3ja9j30dDxkpf62Y2lsgZ8_Pg82rKs%0D%0AbBGiL5RW9-Iwzq5RaceoUh9W3fdxV_hvhw0VvB-w9WeCvGZdURayj-SfPqG7cHRnbe3ZKi-asnHW%0D%0AsuHf0qUb958%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYq9sk8Hb7VWOzpi3BlUT-nak3p1WL7z8SYGjw_U1mMa2wq1U8vJzA3T9Qp4euxutv1SuLxOs2o%0D%0AXCUI13rSEESPKUjbPTTvd1CH_6PcbvY_GGEB9f1cOmxAkUn1KQJ9y2YfG-ytDnv0GFlgRykOKdsr%0D%0ASEAAL2otdotgXCE7BGgQLGUooh1cPNISuGnJkYYCUdTGgMLIIP1rEu71zSaNaX37wsOmtNPPs4a2%0D%0A-yGpYRHa0EU%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYv86bD0CEKYsrX1Lf812NJZMAyf7Sh4FCGdi9c1NvkBn2ltVCSUxw6SkcuprCpkWFlKIRnD1NS%0D%0A7Yrv8S6QU9boaSo1ZzyuKKnIW8W_HME7HPpgrqGM_ocFiCoJ7Ld4rEBG5N1uwqcpYG7Em8H8xFub%0D%0AXoJdNA8IaA1R0k0yW2U_rdfbLlUFTqTdCA2nSP9qkFYAta91OdhWVbTbR1N6vAa-ooBw20zHrTFb%0D%0A7TiMKAhldBU%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYkDMW23tvNUTBwcRlL8tfIRWWbFbGIuAqC8aYHi1EtZI6BtK-6d5Ag-eAjM02xMYG41FGLOgmK%0D%0AiXMAvGOEijlGc7T_VxIrhNzO_ckYvvchN0jvvaBCbNtH4L8uOKSKMM6pjHU7Op4AuR90CZmp4E5q%0D%0A1HiByy19Lb-PPflXBGDaOfqpgH6ssRyDtNGxjrCNhwkXp9jNiaWTfnbSEU1XX1dRzcochpGyyHqA%0D%0AEQJqInQRQo4%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYsDMnMQzpWHSuUJGJKbz4z_PSjSx1ygVg3wrmu-cbZT_iMw1jCksOkwmCEAAlAjqQnRenSi_JV%0D%0ADHbVRQ4qOQ3d2VSd_WrH1zY-3N4xMuWUoBJwUws2SQ_2Qq5O0CN_gjP6BEEvdEzTDikeQdkHryaL%0D%0AM14Gzrd4KPjTi0Vfd7XoP6Yg5Z5IxYk_mMNEaYxlRYS2dae0ITI6pMTMMBQDMLmTY_H-4Pw5FBrA%0D%0AOvbYg1IfH4VRbky0K47jz7KHSr4txt6w%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYvPVE_8TaVcUk-y3hSPNXSBpEP89788rQda-OShiiz9s-W5ffa8Qzqj2T6kytHcVFptPlZMG2Z%0D%0AeCZgtHYbQtGBrDTlDihf71w75lmB_no-sWwwFYhDyL10FgdNIqDxEkPq1lp4i7wpoqf55NvyOiIj%0D%0A3yyyXD0QBB1dpvAJj6XjLcI%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYuqLOWKy4t5CVymylUwMN854bT71W9qFgxUZa_IV6_z4lIuVi48Pqhi_jfiiLTzehMiaGCnLx-%0D%0A9O4gUgKgr38Ov49aAArq46iNYDKlDlVagUfIfoxOIIxTkpbIxG4r2dqaG83kEJ1eT8uD9wsZfqnJ%0D%0AjXvVoBVWa6PHfaO93htpmx4%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/common-3617413113._CB300284919_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-4073101944._CB300352925_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-335260898._CB289148898_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-194820129._CB286493173_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-2126106747._CB289323193_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=0101a8ed4ae02bd897a46a58ad6db9548f92294b7533d087ab89275accdf868a4151",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=178956282343"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-232398347._CB349580400_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=178956282343&ord=178956282343";
    },"unable to request AAN pixel");
</script>

<div id="flashContent" style="width: 0px; height: 0px; overflow:hidden;">
    <script type="text/javascript">
        if (generic && generic.monitoring.record_metric && flashAdUtils.canPlayFlash) {
            var flashLoaded = 0;
            function swfLoaded() {
                flashLoaded = 1;
                generic.monitoring.record_metric('ads_flash_did_play', 1, true);
            }
            window.onload = function() {
                // Flash was not loaded
                if (flashLoaded === 0) {
                    // Flash could be loaded but wasn't
                    if (flashAdUtils.canPlayFlash()) {
                        generic.monitoring.record_metric('ads_flash_can_but_did_not_play', 0, true);
                    } else {
                        generic.monitoring.record_metric('ads_flash_cannot_play', 0, true);
                    }
                }
            }
            /**
             * There is a rare possibility of window.onload getting called before swfLoaded.
             * Firing this metric to keep track of such calls.
             */
             generic.monitoring.record_metric('ads_flash_page_loaded', 1, true);
         }
    </script>
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="1x1" align="middle">
        <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
        <param name="quality" value="high" />
        <param name="play" value="true" />
        <param name="loop" value="true" />
        <param name="wmode" value="transparent" />
        <param name="scale" value="showall" />
        <param name="menu" value="true" />
        <param name="devicefont" value="false" />
        <param name="salign" value="" />
        <param name="allowScriptAccess" value="always" />

        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" width="1" height="1">
            <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
            <param name="quality" value="high" />
            <param name="play" value="true" />
            <param name="loop" value="true" />
            <param name="wmode" value="transparent" />
            <param name="scale" value="showall" />
            <param name="menu" value="true" />
            <param name="devicefont" value="false" />
            <param name="salign" value="" />
            <param name="allowScriptAccess" value="always" />
        </object>
        <!--<![endif]-->
    </object>
</div>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="604"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
