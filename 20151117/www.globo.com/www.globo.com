



<!DOCTYPE html>

<!--[if IE]><![endif]--><html><head><meta charset="utf-8" /><meta http-equiv="Content-Type" content="text/html;charset=UTF-8" /><title>globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento</title><meta name="google-site-verification" content="BKmmuVQac1JM6sKlj3IoXQvffyIRJvJfbicMouA2a88" /><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.5" /><meta property="og:title" content="globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento"/><meta property="og:type" content="website"/><meta property="og:url" content="http://www.globo.com/"/><meta property="og:image" content="http://s.glbimg.com/en/ho/static/globocom2012/img/fb_marca.png"/><meta property="og:site_name" content="globo.com"/><meta property="og:description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta property="fb:page_id" content="224969370851736" /><meta property='busca:title' content='Globo.com' /><meta property='busca:species' content='Home' /><meta property='busca:issued' content='17/11/2015 16:36:45' /><meta property='busca:modified' content='17/11/2015 16:36:45' /><meta property='busca:publisher' content='www.globo.com' /><meta name="description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta name="keywords" content="NotÃ­cias, Entretenimento, Esporte, Tecnologia, Portal, ConteÃºdo, Rede Globo, TV Globo, VÃ­deos, TelevisÃ£o"/><meta name="application-name" content="Globo.com"/><meta name="msapplication-TileColor" content="#0669DE"/><meta name="msapplication-TileImage" content="http://s.glbimg.com/en/ho/static/globocom2012/img/globo-win-tile.png"/><link rel="canonical" href="http://www.globo.com/" /><link rel="shortcut icon" href="http://s.glbimg.com/en/ho/static/globocom2012/img/favicon.png" type="image/ico" /><link rel="apple-touch-icon-precomposed" sizes="57x57" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone.png" /><link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-ipad.png" /><link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone-retina.png" /><link rel="stylesheet" href="http://s.glbimg.com/en/ho/static/CACHE/css/90da00b48320.css" type="text/css" media="screen" /><!--[if lt IE 10]><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/d783f8b1c48f.js"></script><![endif]--><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/843cd1cebddc.js"></script></head><!--[if lt IE 7 ]><body class="ie ie6"><![endif]--><!--[if IE 7 ]><body class="ie ie7"><![endif]--><!--[if IE 8 ]><body class="ie ie8"><![endif]--><!--[if IE 9 ]><body class="ie ie9gt ie9"><![endif]--><!--[if gt IE 9 ]><body class="ie ie9gt ie10gt"><![endif]--><!--[if !IE]><!--><body><!--<![endif]--><div id="home-push-menu"><div class="home-push-menu-container"><div class="home-push-menu-initial"><div class="home-push-menu-header">NAVEGUE</div><ul class="barra-itens" data-analytics-context="Link Produto"><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://g1.globo.com" accesskey="n" class="barra-item-g1 link-produto analytics-area analytics-id-T">g1</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://globoesporte.globo.com" accesskey="e" class="barra-item-globoesporte link-produto analytics-area analytics-id-T">globoesporte</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://gshow.globo.com" accesskey="t" class="barra-item-gshow link-produto analytics-area analytics-id-T">gshow</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://famosos.globo.com" accesskey="t" class="barra-item-famosos-etc link-produto analytics-area analytics-id-T">famosos &amp; etc</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://techtudo.com.br" accesskey="b" class="barra-item-tech link-produto analytics-area analytics-id-T">tecnologia</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://video.globo.com" accesskey="v" class="barra-item-videos link-produto analytics-area analytics-id-T">vÃ­deos</a></li><li><a href="#" id="home-push-menu-show-email">e-mail</a><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="2.423,0 0,2.399 5.153,7.5 0,12.602 2.423,15 10,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-right.png"></image></svg></span></li><li><a href="https://meuperfil.globo.com/">central globo.com</a></li></ul></div><div class="home-push-menu-email"><ul><li><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="7.577,0 10,2.399 4.847,7.5 10,12.602 7.577,15 0,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-left.png"></image></svg></span><a id="home-push-menu-hide-email" href="#">e-mail</a></li><li><a href="https://login.globo.com/login/1948">globomail free</a></li><li><a href="https://login.globo.com/login/1">globomail pro</a></li></ul></div></div></div><div id="home-pagecontent" class=" home-menu-rendered" style=""><script type="text/javascript">
function comScore(C){var A="comScore",j=document,y=j.location,B="",z="undefined",x=2048,D,v,i,w,c="characterSet",l="defaultCharset",u=(typeof encodeURIComponent!=z?encodeURIComponent:escape);if(j.cookie.indexOf(A+"=")!=-1){i=j.cookie.split(";");for(w=0,y=i.length;w<y;w++){var d=i[w].indexOf(A+"=");if(d!=-1){B="&"+unescape(i[w].substring(d+A.length+1))}}}C=C+"&ns__t="+(new Date().getTime());C=C+"&ns_c="+(j[c]?j[c]:(j[l]?j[l]:""))+"&c8="+u(j.title)+B+"&c7="+u(y&&y.href?y.href:j.URL)+"&c9="+u(j.referrer);if(C.length>x&&C.indexOf("&")>0){D=C.substr(0,x-8).lastIndexOf("&");C=(C.substring(0,D)+"&ns_cut="+u(C.substring(D+1))).substr(0,x)}if(j.images){v=new Image();if(typeof ns_p==z){ns_p=v}v.src=C}else{j.write('<p><img src="'+C+'" height="1" width="1" alt="*"/></p>')}}comScore("http"+(document.location.href.charAt(4)=="s"?"s://sb":"://b")+".scorecardresearch.com/p?c1=2&c2=6035227");</script><noscript><p><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6035227&amp;c4=http%3A%2F%2Fwww.globo.com%2Findex.html" height="1" width="1" alt="*"/></p></noscript><script>
libby.loadScript('http://b.scorecardresearch.com/c2/6035227/cs.js');</script><script type="text/javascript">
var utag_data = {"structure_tree": "[\"globocom\"]", "ad_site_page": "{\"adUnit\":\"tvg_Globo.com.Home\", \"adPositionsDesktop\": [\"banner_slim_topo\",\"banner_slb_meio\",\"banner_slb_fim\",\"banner_selo4\",\"banner_floating\"], \"adPositionsMobile\":[\"banner_mobile_topo\",\"banner_mobile_meio\",\"banner_mobile_fim\"]}", "page_name": "index"};</script><script type="text/javascript">
(function (a,b,c,d) {a = '//tags.globo.com/utag/globo/home/prod/utag.js';b=document;c = 'script';d=b.createElement(c);d.src=a;d.type = 'text/java' + c;d.async=true;a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);})();
</script><header><div id="base-container-width-element" class="container"><div class="header-mobile-top analytics-area analytics-id-H"><a id="open-menu" class="open-menu" href="#"><svg version="1.1" class="burger-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19px" height="13px" viewBox="0 0 19 13" enable-background="new 0 0 19 13" xml:space="preserve"><g><path fill="#1063E0" d="M17.493,5H1.485C0.665,5,0,5.665,0,6.485C0,7.305,0.665,7.97,1.485,7.97h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,5.665,18.313,5,17.493,5z M1.485,2.971h16.008c0.82,0,1.486-0.666,1.486-1.486C18.979,0.666,18.313,0,17.493,0H1.485C0.665,0,0,0.666,0,1.484C0,2.305,0.665,2.971,1.485,2.971z M17.493,10H1.485C0.665,10,0,10.666,0,11.485c0,0.82,0.665,1.485,1.485,1.485h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,10.666,18.313,10,17.493,10z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/menu-button.png"></image></svg></a><a class="logo-globo analytics-area analytics-id-T" href="http://globo.com" title="globo.com"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 98 22" enable-background="new 0 0 98 22" xml:space="preserve"><g><path fill="#1063E0" d="M92.982,5.033c-1.115,0-2.549,0.601-3.459,1.625c-0.865-1.069-2.026-1.625-3.392-1.625c-1.205,0-2.252,0.489-3.049,1.336c-0.114-0.757-0.661-1.203-1.502-1.203c-0.957,0-1.639,0.712-1.639,1.781v8.551c0,1.069,0.682,1.781,1.639,1.781s1.638-0.712,1.638-1.781V9.909c0-1.292,0.592-2.205,1.936-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.682,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.909c0-1.292,0.728-2.205,1.934-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.683,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.352C97.466,6.547,95.645,5.033,92.982,5.033z M10.036,5.166c-0.91,0-1.434,0.49-1.639,1.336c-0.614-0.98-1.889-1.469-3.004-1.469C2.093,5.033,0,8.039,0,11.268c0,3.184,1.616,6.146,5.052,6.146c1.502,0,2.617-0.535,3.299-1.515h0.045v0.757c0,1.625-1.183,2.538-2.776,2.538c-1.889,0-3.049-1.001-4.097-1.001c-0.637,0-1.319,0.8-1.319,1.403C0.205,21.354,4.028,22,5.371,22c3.732,0,6.304-1.938,6.304-5.791V6.947C11.674,5.878,10.991,5.166,10.036,5.166z M5.826,14.474c-1.707,0-2.549-1.559-2.549-3.273c0-1.492,0.842-3.229,2.549-3.229c1.797,0,2.571,1.781,2.571,3.339C8.397,12.871,7.578,14.474,5.826,14.474z M14.318,0c-0.956,0-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781s1.639-0.712,1.639-1.781V1.782C15.957,0.712,15.274,0,14.318,0z M22.479,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169c3.527,0,5.872-2.829,5.872-6.169C28.35,7.994,25.938,5.033,22.479,5.033z M22.479,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.797-3.273,2.594-3.273c1.798,0,2.594,1.737,2.594,3.273C25.073,12.782,24.254,14.474,22.479,14.474z M35.371,5.033c-1.138,0-2.253,0.378-3.118,1.113V1.782C32.253,0.712,31.57,0,30.614,0s-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781c0.911,0,1.434-0.49,1.639-1.336c0.614,0.979,1.889,1.471,3.004,1.471c3.3,0,5.393-3.008,5.393-6.036C40.65,8.261,38.897,5.033,35.371,5.033z M34.825,14.474c-1.798,0-2.572-1.781-2.572-3.34c0-1.559,0.819-3.162,2.572-3.162c1.707,0,2.548,1.759,2.548,3.273C37.373,12.737,36.531,14.474,34.825,14.474z M46.863,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169s5.872-2.829,5.872-6.169C52.735,7.994,50.322,5.033,46.863,5.033z M46.863,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273s2.594,1.737,2.594,3.273C49.458,12.782,48.638,14.474,46.863,14.474z M55.425,13.939c-0.978,0-1.774,0.779-1.774,1.736s0.796,1.738,1.774,1.738c0.979,0,1.775-0.781,1.775-1.738S56.404,13.939,55.425,13.939z M66.583,13.805c-0.386,0-1.616,0.669-2.436,0.669c-1.774,0-2.753-1.648-2.753-3.229c0-1.648,0.888-3.273,2.753-3.273c1.048,0,1.776,0.623,2.594,0.623c0.775,0,1.253-0.868,1.253-1.536c0-1.56-2.709-2.026-4.006-2.026c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.343,6.169,5.871,6.169c1.184,0,4.006-0.558,4.006-2.206C67.994,14.451,67.355,13.805,66.583,13.805z M73.446,5.033c-3.459,0-5.872,2.961-5.872,6.212c0,3.34,2.344,6.169,5.872,6.169c3.527,0,5.871-2.829,5.871-6.169C79.317,7.994,76.904,5.033,73.446,5.033z M73.446,14.474c-1.774,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273c1.797,0,2.594,1.737,2.594,3.273C76.04,12.782,75.221,14.474,73.446,14.474z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/logo.png"></image></svg></a><form id="search-form" action="http://www.globo.com/busca/" method="get"><div id="search-container" class="search-container"><span><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="12px" height="12px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><g><path fill="#999999" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/search-small.png"></image></svg></span><div class="search-internal-container"><input type="text" id="search-globo" name="q" placeholder="encontre na globo.com"></div><a id="close-search" class="close-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="11px" height="11px" viewBox="0 0 11 11" enable-background="new 0 0 11 11" xml:space="preserve"><g><path fill="#666766" d="M6.884,5.501c1.955-1.955,3.733-3.732,3.809-3.808c0.388-0.388,0.388-1.015,0-1.403 c-0.387-0.386-1.015-0.386-1.402,0C9.215,0.365,7.437,2.144,5.482,4.099c-1.947-1.947-3.714-3.714-3.79-3.79 c-0.387-0.388-1.015-0.388-1.402,0c-0.387,0.387-0.387,1.016,0,1.401c0.076,0.076,1.842,1.843,3.79,3.791 C2.135,7.446,0.372,9.209,0.297,9.285c-0.387,0.387-0.387,1.015,0,1.401c0.387,0.389,1.015,0.389,1.402,0 c0.075-0.075,1.838-1.838,3.784-3.783c1.953,1.952,3.727,3.727,3.802,3.802c0.387,0.387,1.015,0.387,1.402,0 c0.387-0.387,0.387-1.015,0-1.402C10.611,9.228,8.837,7.454,6.884,5.501z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/close.png"></image></svg></a></div><a id="open-search" class="open-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><path fill="#1063e0" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path><image src="http://s.glbimg.com/gl/ba/img/common/search.png"></image></svg></a></form></div><h1 class="analytics-area analytics-id-H"><a class="logo-topo analytics-area analytics-id-T" href="/" title="globo.com">
                globo.com
            </a></h1><div class="complementos analytics-area analytics-id-T"><div id="busca-padrao"><form action="http://www.globo.com/busca/" method="get" accept-charset="utf-8"><fieldset><legend>buscar</legend><label for="busca-campo" class="label-for-search">buscar</label><input id="busca-campo" type="text" name="q" autocomplete="off" accesskey="s" lang="pt-BR" x-webkit-speech speech /><button type="submit">buscar</button></fieldset></form></div><div id="libby-box-previsao-tempo" class="analytics-area analytics-id-L"></div></div><nav><div class="spacer"></div><header id="header-produto" class="header-navegacao header-home" data-analytics="T"><div id="glbbarrawidget"></div><div class="floating-bar"><div class="header-bar"><div class="grid-12 clearfix area-principal row"><div class="column"><div class="menu-area to-left" data-analytics="S"><div class="menu-button"><div class="burger"><b></b><b></b><b></b></div><span class="menu-label">MENU</span></div><span class="menu-produto"></span></div><a class="logo-area" href="http://www.globo.com/"><h1 class="logo icon-produto">Home</h1></a><div class="to-right"><div class="search-area"><form id="frmBuscaScroll" action="#"><input id="search-input-floating" type="text" name="q" placeholder="BUSCAR" class="search" /><label for="search-input-floating" class="glass-container"><div class="glass"><div class="iglass css3pie"></div></div></label></form></div></div></div></div></div></div><div class="cube-container"><div class="cube"><div class="face front"><div class="grid-12 clearfix area-principal row"><div class="column"><div class="menu-area to-left" data-analytics="S"><div class="menu-button"><div class="burger"><b></b><b></b><b></b></div><span class="menu-label">MENU</span></div><span class="menu-produto"></span></div><a class="logo-area" href="http://www.globo.com/"><h1 class="logo icon-produto">Home</h1></a><div class="to-right"><div class="search-area"><form id="frmBuscaScroll" action="#"><input id="search-input-top" type="text" name="q" placeholder="BUSCAR" class="search" /><label for="search-input-top" class="glass-container"><div class="glass"><div class="iglass css3pie"></div></div></label></form></div><div id="busca-padrao"></div></div></div></div></div></div></div></header><script>
(function(window,document) {try{new CustomEvent("test");} catch(e){var CustomEvent=function(event,params) {var evt;params=params || {bubbles: false,
cancelable: false,
detail: undefined};evt=document.createEvent("CustomEvent");evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);return evt;};
CustomEvent.prototype=window.Event.prototype;window.CustomEvent=CustomEvent;}
document.dispatchEvent(new CustomEvent('glb.headerDom.ready'));})(window,document);</script><style type="text/css">
    /*
       evita que o markup do menu seja exibido sem estilo enquanto
       ele nÃ£o foi carregado
    */
    #menu-container {
        display: none;
    }
    #menu-fonts {
        height: 1px;
        width: 1px;
        color: transparent;
        overflow: hidden;
        position: absolute;
    }
</style>

<nav id="menu-container" class="regua-navegacao-tab menu-no-animation">
    

<script id="menu-carousel-template" type="x-tmpl-mustache">

<div id="menu-carousel-header-sizes-infos" class="menu-carousel-header-sizes-infos">
</div>
<div id="menu-carousel-header" class="menu-carousel menu-carousel-header swiper-container">
    <ul class="menu-brands swiper-wrapper">
        {{#portals}}
        <li data-produto="{{name}}" class="menu-carousel-item menu-carousel-{{name}} {{#active}}menu-carousel-item-active{{/active}} swiper-slide">
            <a href="{{link}}" class="menu-carousel-link" data-index="{{index}}">
                <svg class="brand-icon">
                    <use class="menu-logo" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#eixo-icone-{{name}}"></use>
                </svg>
            </a>
            <div class="menu-carousel-link-shadow"></div>
        </li>
        {{/portals}}
    </ul>
</div>
<div id="menu-carousel-body" class="menu-carousel menu-carousel-body swiper-container">
    <ul class="menu-brands swiper-wrapper">
        {{#portals}}
        <li data-produto="{{name}}" class="menu-carousel-item menu-carousel-{{name}} {{#active}}menu-carousel-item-active{{/active}} swiper-slide">
            <div id="menu-rounder" class="menu-rounder">{{{menu}}}</div>
        </li>
        {{/portals}}
    </ul>
</div>

</script>

<script id="menu-rounder-template" type="x-tmpl-mustache">

  <div id="menu-cascade" class="menu-cascade" data-menu-index="{{index}}">
    <ul class="menu-root menu-level menu-level-0">{{#items}}{{>recursive_partial}}{{/items}}</ul>
  </div>
  <div id="menu-addon" class="menu-addon"></div>

</script>

<script id="menu-custom-template" type="x-tmpl-mustache">

    {{#items}}{{>recursive_partial}}{{/items}}

</script>


<script id="menu-dinamico-template" type="x-tmpl-mustache">

<li id="menu-{{deepness}}-{{tituloSlug}}" class="menu-item {{#separador}} menu-item-separator{{/separador}}{{#destacado}} menu-item-highlighted{{/destacado}}{{#agrupador}} menu-item-grouped{{/agrupador}}{{#hasChildren}} is-father{{/hasChildren}}{{#hasSubmenuBroken}} menu-item-submenu-broken{{/hasSubmenuBroken}}">

    {{#shouldBeLink}}<a href="{{link}}" class="menu-item-link">{{/shouldBeLink}}
    {{^shouldBeLink}}<span class="menu-item-link">{{/shouldBeLink}}
        <span class="menu-item-title">{{titulo}}<span class="menu-item-arrow"><svg class="regua-navegacao-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#regua-arrow"></use></svg></span></span>
    {{#shouldBeLink}}</a>{{/shouldBeLink}}
    {{^shouldBeLink}}</span>{{/shouldBeLink}}

    {{#hasChildren}}
        <div class="menu-level menu-submenu menu-submenu-level{{deepness}}{{#hasSubmenuBroken}} menu-item-submenu-broken{{/hasSubmenuBroken}} menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back"><span class="menu-item-arrow"><svg class="regua-navegacao-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#regua-arrow"></use></svg></span><span class="regua-navegacao-menu-item">{{titulo}}</span></a>
            <ul class="menu-submenu-vertical-scroll">
            {{#children}}
                {{>recursive_partial}}
            {{/children}}
            <ul>
        </div>
    {{/hasChildren}}
</li>

</script>


    <div id="menu-rounder" class="menu-rounder">
        <div id="menu-cascade" class="menu-cascade" data-menu-index="0">
            

<ul class="menu-root menu-level menu-level-0">
    
        




<li class="menu-item is-father " id="menu-1-famosos">

    <a class="menu-item-link">
        <span class="menu-item-title">Famosos</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Famosos</a>
            
                

                




<li class="menu-item " id="menu-2-ego">

    <a href="http://ego.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-paparazzo">

    <a href="http://ego.globo.com/paparazzo/index.html" class="menu-item-link">
        <span class="menu-item-title">Paparazzo</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-patricia-kogut">

    <a href="http://kogut.oglobo.globo.com/noticias-da-tv/index.html" class="menu-item-link">
        <span class="menu-item-title">Patricia Kogut</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-quem">

    <a href="http://revistaquem.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Quem</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-retratos-da-vida">

    <a href="http://extra.globo.com/famosos/" class="menu-item-link">
        <span class="menu-item-title">Retratos da Vida</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-moda-estilo">

    <a class="menu-item-link">
        <span class="menu-item-title">Moda &amp; Estilo</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Moda &amp; Estilo</a>
            
                

                




<li class="menu-item " id="menu-2-ela-no-o-globo">

    <a href="http://ela.oglobo.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Ela no O Globo</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-estilo-no-gshow">

    <a href="http://gshow.globo.com/Estilo/" class="menu-item-link">
        <span class="menu-item-title">Estilo no Gshow</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-moda-no-ego">

    <a href="http://ego.globo.com/moda/index.html" class="menu-item-link">
        <span class="menu-item-title">Moda no Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-moda-no-gnt">

    <a href="http://gnt.globo.com/moda/" class="menu-item-link">
        <span class="menu-item-title">Moda no GNT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-glamour">

    <a href="http://revistaglamour.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Glamour</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-gq-brasil">

    <a href="http://gq.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista GQ Brasil</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-marie-claire">

    <a href="http://revistamarieclaire.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Marie Claire</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-vogue">

    <a href="http://vogue.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Vogue</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-bem-estar-saude">

    <a class="menu-item-link">
        <span class="menu-item-title">Bem-Estar &amp; SaÃºde</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Bem-Estar &amp; SaÃºde</a>
            
                

                




<li class="menu-item " id="menu-2-beleza-no-ego">

    <a href="http://ego.globo.com/beleza/index.html" class="menu-item-link">
        <span class="menu-item-title">Beleza no Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-beleza-no-gnt">

    <a href="http://gnt.globo.com/beleza/" class="menu-item-link">
        <span class="menu-item-title">Beleza no GNT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-beleza-no-gshow">

    <a href="http://gshow.globo.com/Estilo/Beleza/" class="menu-item-link">
        <span class="menu-item-title">Beleza no Gshow</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-bem-estar-no-g1">

    <a href="http://g1.globo.com/bemestar/" class="menu-item-link">
        <span class="menu-item-title">Bem Estar no G1</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-bem-estar-no-gnt">

    <a href="http://gnt.com.br/bemestar" class="menu-item-link">
        <span class="menu-item-title">Bem Estar no GNT </span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-horoscopo-no-ego">

    <a href="http://horoscopo.ego.globo.com/" class="menu-item-link">
        <span class="menu-item-title">HorÃ³scopo no Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-maes-no-gnt">

    <a href="http://gnt.globo.com/maes-e-filhos/" class="menu-item-link">
        <span class="menu-item-title">MÃ£es no GNT </span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-meus-5-minutos">

    <a href="http://meus5minutos.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Meus 5 Minutos</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-crescer">

    <a href="http://revistacrescer.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Crescer</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-casa-decoracao">

    <a class="menu-item-link">
        <span class="menu-item-title">Casa &amp; DecoraÃ§Ã£o</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Casa &amp; DecoraÃ§Ã£o</a>
            
                

                




<li class="menu-item " id="menu-2-casa-no-gnt">

    <a href="http://gnt.globo.com/casa-e-decoracao/" class="menu-item-link">
        <span class="menu-item-title">Casa no GNT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-casa-vogue">

    <a href="http://casavogue.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Casa Vogue</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-casa-e-jardim">

    <a href="http://revistacasaejardim.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Casa e Jardim</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-do-zap">

    <a href="http://revista.zapimoveis.com.br/?utm_source=globo.com-etc&amp;utm_medium=botao-casa-decoracao&amp;utm_campaign=revista-imoveis" class="menu-item-link">
        <span class="menu-item-title">Revista do Zap</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-gastronomia">

    <a class="menu-item-link">
        <span class="menu-item-title">Gastronomia</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Gastronomia</a>
            
                

                




<li class="menu-item " id="menu-2-casa-e-comida">

    <a href="http://revistacasaejardim.globo.com/Casa-e-Comida/" class="menu-item-link">
        <span class="menu-item-title">Casa e Comida</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-colheradas">

    <a href="http://meus5minutos.globo.com/blogs/Colheradas/" class="menu-item-link">
        <span class="menu-item-title">Colheradas</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-receitas-da-ana-maria">

    <a href="http://gshow.globo.com/programas/mais-voce/Receitas-da-Ana-Maria/" class="menu-item-link">
        <span class="menu-item-title">Receitas da Ana Maria</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-receitascom">

    <a href="http://gshow.globo.com/receitas/" class="menu-item-link">
        <span class="menu-item-title">Receitas.com</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-receitas-no-gnt">

    <a href="http://gnt.globo.com/receitas/" class="menu-item-link">
        <span class="menu-item-title">Receitas no GNT</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-musica">

    <a class="menu-item-link">
        <span class="menu-item-title">MÃºsica</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">MÃºsica</a>
            
                

                




<li class="menu-item " id="menu-2-altas-horas">

    <a href=" http://gshow.globo.com/programas/altas-horas/" class="menu-item-link">
        <span class="menu-item-title">Altas Horas </span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-g1-musica">

    <a href="http://g1.globo.com/musica/" class="menu-item-link">
        <span class="menu-item-title">G1 MÃºsica</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-gshow-musica">

    <a href="http://gshow.globo.com/Musica/" class="menu-item-link">
        <span class="menu-item-title">Gshow MÃºsica</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-musica-no-multishow">

    <a href="http://multishow.globo.com/musica/" class="menu-item-link">
        <span class="menu-item-title">MÃºsica no Multishow</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-musicacombr">

    <a href="http://musica.com.br/" class="menu-item-link">
        <span class="menu-item-title">MÃºsica.com.br</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-radiobeat">

    <a href="http://radiobeat.com.br/" class="menu-item-link">
        <span class="menu-item-title">RADIOBEAT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-som-brasil">

    <a href="http://globotv.globo.com/rede-globo/som-brasil/" class="menu-item-link">
        <span class="menu-item-title">Som Brasil</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-cinema-teatro">

    <a class="menu-item-link">
        <span class="menu-item-title">Cinema &amp; Teatro</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Cinema &amp; Teatro</a>
            
                

                




<li class="menu-item " id="menu-2-canal-brasil">

    <a href="http://canalbrasil.globo.com/ " class="menu-item-link">
        <span class="menu-item-title">Canal Brasil</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-canal-universal">

    <a href="http://universal.globo.com" class="menu-item-link">
        <span class="menu-item-title">Canal Universal</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-filmes-na-globo">

    <a href="http://redeglobo.globo.com/filmes/index.html" class="menu-item-link">
        <span class="menu-item-title">Filmes na Globo</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-g1-cinema">

    <a href="http://g1.globo.com/pop-arte/cinema/" class="menu-item-link">
        <span class="menu-item-title">G1 Cinema</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-globo-filmes">

    <a href=" http://globofilmes.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Globo Filmes</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-megapix">

    <a href="http://megapix.globo.com/" class="menu-item-link">
        <span class="menu-item-title">MegaPix</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-monet">

    <a href="http://revistamonet.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Monet</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-teatro">

    <a href="http://www.agentesevenoteatro.com.br/" class="menu-item-link">
        <span class="menu-item-title">Teatro</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-telecine">

    <a href="http://telecine.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Telecine</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item menu-item-highlighted " id="menu-1-ego">

    <a href="http://ego.globo.com/" class="menu-item-link">
        <span class="menu-item-title">ego</span>
    </a>

    
</li>

    
        




<li class="menu-item menu-item-highlighted " id="menu-1-quem">

    <a href="http://revistaquem.globo.com/" class="menu-item-link">
        <span class="menu-item-title">quem</span>
    </a>

    
</li>

    
        




<li class="menu-item menu-item-highlighted " id="menu-1-patricia-kogut">

    <a href="http://kogut.oglobo.globo.com/" class="menu-item-link">
        <span class="menu-item-title">patricia kogut</span>
    </a>

    
</li>

    
</ul>

        </div>
        <div id="menu-addon" class="menu-addon"></div>
    </div>

    
</nav>

<div id="menu-fonts">
    
    
    <span class="open-sans">BESbswy</span>
    <span class="open-sans-bold">BESbswy</span>
    <span class="open-sans-light">BESbswy</span>
    <span class="roboto-slab">BESbswy</span>
    <span class="proximanova-semibold">BESbswy</span>
</div>

<div id="menu-content-overlay"></div>
<style>
.regua-svg-container{height:0;width:0;position:absolute;visibility:hidden}</style><script>
(function (window) {window.REGUA_SETTINGS=window.REGUA_SETTINGS || {};window.REGUA_SETTINGS.portalName = "home";window.REGUA_SETTINGS.portalHome = "http://www.globo.com/";window.REGUA_SETTINGS.portalsList = ["home","g1","ge","gshow","famosos","techtudo","globotv","globoplay","globosatplay"];window.REGUA_SETTINGS.portalsLinkList = ["http://www.globo.com/","http://g1.globo.com/index.html","http://globoesporte.globo.com/","http://gshow.globo.com/","http://famosos.globo.com/","http://www.techtudo.com.br/","http://globotv.globo.com/","http://globoplay.globo.com/","http://globosatplay.globo.com/"];window.REGUA_SETTINGS.staticUrl = "http://s.glbimg.com/en/ho/static/";window.REGUA_SETTINGS.svgSpriteName = "sprite-2f490120e0.svg";window.REGUA_SETTINGS.suggestUrl = "";})(window);
</script><script>(function(){var e,t,n,o;e=function(){function e(){this.name="Android",this.bridge=window.GloboBridge||{navigated:function(e){},menuAction:function(e){},reguaAction:function(e){}}}return e.prototype.navigateTo=function(e){this.bridge.navigated(JSON.stringify(e))},e.prototype.menuAction=function(e){this.bridge.menuAction(JSON.stringify(e))},e.prototype.reguaAction=function(e){this.bridge.reguaAction(JSON.stringify(e))},e}(),t=function(){function e(){this.name="iOS",this.connectWebViewJavascriptBridge(function(e){return e.init(function(e,t){return window.glb.masterApp.manageMessage(e)})})}return e.prototype.connectWebViewJavascriptBridge=function(e){window.WebViewJavascriptBridge?e(WebViewJavascriptBridge):document.addEventListener("WebViewJavascriptBridgeReady",function(){e(WebViewJavascriptBridge)},!1)},e.prototype.brigdeSendJson=function(e){return this.connectWebViewJavascriptBridge(function(t){t.send(JSON.stringify(e))})},e.prototype.navigateTo=function(e){this.brigdeSendJson(e)},e.prototype.menuAction=function(e){this.brigdeSendJson(e)},e.prototype.reguaAction=function(e){this.brigdeSendJson(e)},e}(),n=function(){function n(e){this.platformApp=e,this.initCalled=!1,null!=this.platformApp&&this.bind()}return n.prototype.init=function(){this.initCalled||(this.initCalled=!0,this.header&&(this.processHeaderInfo(),this.bindAfterInit()))},n.prototype.processHeaderInfo=function(){this.countHeaderTries=0,this.maxHeaderTries=100,this.headerCubeContainer=this.header.querySelector(".cube-container"),this.headerCubeContainer?this.processHeaderInfoAction():setTimeout(function(e){return function(){return e.processHeaderInfoAction()}}(this),1)},n.prototype.processHeaderInfoAction=function(){var e;if(this.countHeaderTries++,this.headerCubeContainer=this.header.querySelector(".cube-container"),this.headerCubeContainer)this.cacheVariables(),e=this.getHeaderData(),this.navigateTo(e);else{if(this.countHeaderTries>this.maxHeaderTries)return void console.log("Nao achou o markup interno do header");setTimeout(function(e){return function(){return e.processHeaderInfoAction()}}(this),1)}},n.prototype.bind=function(){var e;return this.bindAppEvents(),this.header=document.querySelector("#header-produto"),e=function(e){return function(){return e.header||(e.header=document.querySelector("#header-produto")),e.init()}}(this),this.header?this.init():(document.addEventListener("glb.headerDom.ready",e),document.addEventListener("DOMContentLoaded",e))},n.prototype.cacheVariables=function(){return this.elements={},this.elements.header=this.header,this.elements.headerCubeContainer=this.headerCubeContainer,this.elements.headerFront=this.elements.headerCubeContainer?this.elements.headerCubeContainer.querySelector(".front"):null,this.elements.headerLogoProduto=this.elements.headerCubeContainer?this.elements.headerCubeContainer.querySelector(".logo-produto-container"):null,this.elements.headerLogoProdutoLink=this.elements.headerLogoProduto?this.elements.headerLogoProduto.parentNode:null,this.elements.headerLogoArea=this.elements.headerCubeContainer?this.elements.headerCubeContainer.querySelector(".logo-area .logo"):null,this.elements.headerLogoAreaLink=this.elements.headerLogoArea?this.elements.headerLogoArea.parentNode:null,this.elements.headerSubeditoria=this.elements.headerFront?this.elements.headerFront.querySelector(".menu-subeditoria"):null,this.elements.headerSubeditoriaLink=this.elements.headerSubeditoria?this.elements.headerSubeditoria.querySelector("a"):null,!0},n.prototype.bindAppEvents=function(){document.addEventListener("glb.menu-carousel.ready",function(e){return function(){return e.platformApp.menuAction({action:"menuReady"})}}(this)),document.addEventListener("glb.menu-carousel.shown.before",function(e){return function(){return e.platformApp.menuAction({action:"menuShownBefore"})}}(this)),document.addEventListener("glb.menu-carousel.shown.after",function(e){return function(){return e.platformApp.menuAction({action:"menuShownAfter"})}}(this)),document.addEventListener("glb.menu-carousel.off.before",function(e){return function(){return e.platformApp.menuAction({action:"menuOffBefore"})}}(this)),document.addEventListener("glb.menu-carousel.off.after",function(e){return function(){return e.platformApp.menuAction({action:"menuOffAfter"})}}(this)),document.addEventListener("glb.regua.exists",function(e){return function(){return e.platformApp.reguaAction({action:"reguaExists"})}}(this))},n.prototype.bindAfterInit=function(){},n.prototype._getText=function(e){return e?(e.innerText||e.textContent).trim()||"":""},n.prototype._getHref=function(e){return e?e.getAttribute("href")||"":""},n.prototype._getCssProp=function(e,t){var n;return e?(n=window.getComputedStyle(e),n.getPropertyValue(t)||""):""},n.prototype._getBackgroundColor=function(e){return this._getCssProp(e,"background-color")},n.prototype.getHeaderData=function(){var e;return e={color:this._getBackgroundColor(this.elements.headerFront),editoria:"",editoriaHref:"",subeditoria:"",subeditoriaHref:"",logo:"",logoHref:""},this.elements.headerLogoProduto?(e.logo=this._getText(this.elements.headerLogoProduto),e.logoHref=this._getHref(this.elements.headerLogoProdutoLink),e.editoria=this._getText(this.elements.headerLogoArea),e.editoriaHref=this._getHref(this.elements.headerLogoArea),this.elements.headerSubeditoria&&(e.subeditoria=this._getText(this.elements.headerSubeditoria),e.subeditoriaHref=this._getHref(this.elements.headerSubeditoriaLink))):(e.logo=this._getText(this.elements.headerLogoArea),e.logoHref=this._getHref(this.elements.headerLogoAreaLink)),e},n.prototype.navigateTo=function(e){e.action="navigateTo",e.url=window.location.href,this.platformApp?this.platformApp.navigateTo(e):console.log("Sem Plataforma")},n.prototype.manageMessage=function(e){var t,n;return e&&(t=JSON.parse(e),t.action="regua-change")?(n=t.rulerItem||"home",null!=window.$?$(document).trigger("glb.regua.change",[n]):console.log("Sem jQuery")):void 0},n.prototype.getUserAgent=function(){return window.getUserAgent?window.getUserAgent():window.navigator.userAgent},n.prototype.getPlatformApp=function(){var n;return n=this.getUserAgent(),n.match(/(iPad|iPhone|iPod)/g)?new t:n.match(/(Android)/g)?new e:void 0},n}(),window.glb=window.glb||{},window.glb.MasterApp=n,window.glb.AndroidApp=e,window.glb.IosApp=t,window.initMasterApp=function(){var e;if(!window.glb.masterApp)return e=n.prototype.getPlatformApp(),window.glb.masterApp=new n(e)},o=null!=window.noAutoLoadReguaNavegacao&&window.noAutoLoadReguaNavegacao,o||window.initMasterApp()}).call(this);</script><script>(function(){var n,o,e,t;e=function(){return/MobApp_Android|MobApp_iOS|MobApp_WP/},o=function(){return window.getUserAgent?window.getUserAgent():window.navigator.userAgent},n=function(){var n,t,a,i,r;i=o(),r=i.split(" ");for(a in r)if(n=r[a],e().test(n))return t=n.split("/"),{name:t[0].replace("_"," "),version:t[1],frameworkVersion:t[2]};return null},window.addGlbOnAppClass=function(){var o;window.glb=window.glb||{},window.glb.extractAppFromUserAgent||(window.glb.extractAppFromUserAgent=n,window.glb.nativeAppInfo=n(),null!==window.glb.nativeAppInfo&&(window.glb.nativeAppInfo.frameworkVersion?(o=parseInt(window.glb.nativeAppInfo.frameworkVersion,10),o>=1&&(document.documentElement.className+=" glb-on-app")):document.documentElement.className+=" glb-on-app-comnio"))},t=null!=window.noAutoLoadReguaNavegacao&&window.noAutoLoadReguaNavegacao,t||window.addGlbOnAppClass()}).call(this);</script><script>(function(){var n,o,e,i,w,t,d,a,r,s,u,l;try{new CustomEvent("test")}catch(c){e=c,n=function(n,o){var e;return e=void 0,o=o||{bubbles:!1,cancelable:!1,detail:void 0},e=document.createEvent("CustomEvent"),e.initCustomEvent(n,o.bubbles,o.cancelable,o.detail),e},n.prototype=window.Event.prototype,window.CustomEvent=n}r=function(n,o){var e;return null==n||null==o?!1:(e=new RegExp("(?:^|\\s)"+o+"(?!\\S)"),!!n.className.match(e))},o=function(n,o){r(n,o)||(n.className+=" "+o)},u=function(n,o){var e;r(n,o)&&(e=new RegExp("(?:^|\\s)"+o+"(?!\\S)","g"),n.className=n.className.replace(e,""))},w=function(){return window.myInnerWidth||window.innerWidth},i=function(){return window.myInnerHeight||window.innerHeight},t=function(){var n;return null!=window.isAndroidBrowser?window.isAndroidBrowser:(n=navigator.userAgent,window.isAndroidBrowser=n.indexOf("Mozilla/5.0")>-1&&n.indexOf("Android ")>-1&&n.indexOf("AppleWebKit")>-1&&-1===n.indexOf("Chrome"),window.isAndroidBrowser)},d=function(){return null!=window.isPortrait?window.isPortrait:(window.isPortrait=w()<=i(),window.isPortrait)},a=function(){return null!=window.isTouchable?window.isTouchable:(window.isTouchable="ontouchstart"in window||navigator.msMaxTouchPoints||window.DocumentTouch&&document instanceof window.DocumentTouch,window.isTouchable)},s=function(){var n;return window.REGUAMAXWIDTHPORTRAIT=window.REGUAMAXWIDTHPORTRAIT||640,window.REGUAMAXWIDTHLANDSCAPE=window.REGUAMAXWIDTHLANDSCAPE||767,window.isPortrait=d(),window.isTouchable=a(),window.isAndroidBrowser=t(),n=w(),(window.isPortrait&&n<=window.REGUAMAXWIDTHPORTRAIT||!window.isPortrait&&n<=window.REGUAMAXWIDTHLANDSCAPE)&&!window.isAndroidBrowser&&document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image","1.1")&&(null==window.glb.nativeAppInfo||window.glb.nativeAppInfo.frameworkVersion)},window.glb=window.glb||{},window.glb.hasClass=r,window.glb.addClass=o,window.glb.removeClass=u,window.addHasReguaClass=function(){window.glb.reguaShouldStart||(window.glb.reguaShouldStart=s,window.glb.reguaShouldStart()?(window.glb.addClass(document.documentElement,"has-regua"),window.glb.addClass(document.documentElement,"svg-support"),window.glb.removeClass(document.documentElement,"has-not-regua"),document.dispatchEvent(new CustomEvent("glb.regua.exists"))):(window.glb.removeClass(document.documentElement,"has-regua"),window.glb.removeClass(document.documentElement,"svg-support"),window.glb.addClass(document.documentElement,"has-not-regua")))},l=null!=window.noAutoLoadReguaNavegacao&&window.noAutoLoadReguaNavegacao,l||window.addHasReguaClass()}).call(this);</script><nav id="regua-navegacao" class="regua-navegacao "><div id="regua-svg-container" class="regua-svg-container"><svg xmlns="http://www.w3.org/2000/svg"><symbol id="eixo-icone-busca" viewBox="0 0 24 24"><path d="M21.67 20.27l-5.72-5.73c.98-1.26 1.55-2.83 1.55-4.54 0-4.14-3.36-7.5-7.5-7.5-4.15 0-7.5 3.36-7.5 7.5 0 4.14 3.35 7.5 7.5 7.5 1.7 0 3.27-.57 4.53-1.54l5.73 5.73c.42.41 1.07.43 1.46.04.39-.39.37-1.04-.05-1.46zM10 15.5c-3.04 0-5.5-2.46-5.5-5.5S6.96 4.5 10 4.5c3.03 0 5.5 2.46 5.5 5.5s-2.47 5.5-5.5 5.5z"/></symbol><symbol id="eixo-icone-feed" viewBox="0 0 24 24"><path d="M17 14c0 .55-.45 1-1 1h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1zM19 10c0 .55-.45 1-1 1h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1z"/><path fill="#FFF" d="M17 14c0 .55-.45 1-1 1h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1zM19 10c0 .55-.45 1-1 1h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1z"/><path d="M19 10c0 .55-.45 1-1 1h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1zM17 14c0 .55-.45 1-1 1h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1z"/><path d="M4 3v4H0v11c0 1.66 1.34 3 3 3h19c1.1 0 2-.9 2-2V3H4zm0 15.09c0 .5-.45.91-1 .91s-1-.41-1-.91V9h2v9.09zM22 19H6V5h16v14z"/><path fill="#FFF" d="M4 9v9.09c0 .5-.45.91-1 .91s-1-.41-1-.91V9h2zM6 5v14h16V5H6zm10 10h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1s-.45 1-1 1zm2-4h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1s-.45 1-1 1z"/></symbol><symbol id="eixo-icone-menu" viewBox="0 0 24 24"><path d="M22 6c0 .552-.447 1-1 1H3c-.552 0-1-.448-1-1 0-.553.448-1 1-1h18c.553 0 1 .447 1 1zM22 12c0 .552-.447 1-1 1H3c-.552 0-1-.448-1-1 0-.553.448-1 1-1h18c.553 0 1 .447 1 1zM22 18c0 .552-.447 1-1 1H3c-.552 0-1-.448-1-1 0-.553.448-1 1-1h18c.553 0 1 .447 1 1z"/></symbol><symbol id="eixo-icone-usuario" viewBox="0 0 24 24"><path d="M12 2C8.4 2 5.5 4.9 5.5 8.5c0 3.61 2.9 6.5 6.5 6.5s6.5-2.89 6.5-6.5C18.5 4.9 15.6 2 12 2zm0 11.07c-2.49 0-4.5-2.02-4.5-4.5s2.01-4.5 4.5-4.5 4.5 2.02 4.5 4.5-2.01 4.5-4.5 4.5zM22.893 23c-.733 0-1.074-.455-1.074-.86C21.817 20.26 18.22 19 12 19s-9.818 1.136-9.818 3.14c0 .406-.41.86-1.092.86C.358 23 0 22.513 0 22.108 0 18.958 4.598 17 12 17s12 1.958 12 5.108c0 .405-.398.892-1.107.892z"/></symbol><symbol id="regua-arrow" viewBox="0 0 5 14"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.902 6.64L1.422.386C1.217.017.762-.11.402.104.05.317-.074.79.133 1.16l3.25 5.843L.1 12.847c-.208.368-.085.838.274 1.05.357.212.814.086 1.02-.28l3.412-6.074c.21-.243.26-.606.096-.904z"/></symbol></svg></div><div class="regua-navegacao-container"><ul class="regua-lista regua-lista-home"><li id="regua-navegacao-item-home" class="regua-navegacao-item active"><span data-href="http://www.globo.com/"
                                            data-target="regua-tab-home" class="home-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-feed"/></svg></span></li><li id="regua-navegacao-item-menu" class="regua-navegacao-item "><span data-target="regua-tab-menu" class="menu-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-menu"/></svg></span></li><li id="regua-navegacao-item-busca" class="regua-navegacao-item "><span data-target="regua-tab-busca" class="busca-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-busca"/></svg></span></li><li id="regua-navegacao-item-usuario" class="regua-navegacao-item "><span data-target="regua-tab-usuario" class="usuario-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-usuario"/></svg></span></li></ul></div></nav><nav id="regua-user-container" class="regua-user-container regua-user-home regua-navegacao-tab"><div id="regua-user-controls" class="regua-user-controls"></div><div id="regua-user-tabs" class="regua-tabs regua-user-tab-2"><div id="tab-labels" class="tab-labels"><span class="tab-label tab-label-1" data-index="1">notificaÃ§Ãµes</span><span class="tab-label tab-label-2" data-index="2">minha conta</span></div><div class="regua-tab regua-tab-1" data-index="1"><div class="regua-content"><ul id="regua-user-profile-notifications" class="regua-user-row regua-user-profile-notifications"></ul></div></div><div class="regua-tab regua-tab-2" data-index="2"><div class="regua-content"><ul id="regua-user-profile-list" class="regua-user-row regua-user-profile-list"></ul><div id="regua-user-logout" class="regua-user-logout"></div></div></div></div></nav><div id="regua-tab-busca" class="regua-navegacao-tab regua-tab-busca regua-busca-home"><div class="regua-search-header"><div class="regua-search-box"><form class="regua-search-form" action="http://globo.com/busca/" method="GET"><div class="regua-search-form-container regua-table"><div class="regua-search-input-box regua-table-cell"><input class="regua-search-input" name="q" type="search" placeholder="o que vocÃª procura?" autocorrect="off" autocapitalize="off" autocomplete="off"/></div><div class="regua-search-buttons-container regua-table-cell"><div class="regua-search-clear-container"><a href="#" class="regua-search-clear-button">clear</a></div><div class="search-button-icon"><input type="submit" value="go" class="regua-search-submit"/><div class="regua-icon-block"><svg class="regua-navegacao-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#eixo-icone-busca"></use></svg></div><a href="#" class="regua-icon-block search-button-go"><svg class="regua-navegacao-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#eixo-icone-busca"></use></svg></a></div></div></div></form></div></div><div class="regua-container-search-body"><ul class="regua-pre-suggest"></ul><div class="regua-container-suggest"><ul class="regua-suggest"></ul></div><div class="regua-container-results"></div></div></div>



<ul id="home-menu" class="nested widget-menu">
    
        <li class="g1 analytics-product analytics-multi-product">
            <a href="http://g1.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">g1</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://g1.globo.com/" data-menu-id="g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">g1</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="jornais-revistas-e-radio">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">jornais, revistas e rÃ¡dio</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="servicos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">serviÃ§os</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-g1">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-g1" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/carros/">
                                                <span class="titulo">Carros</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/economia/">
                                                <span class="titulo">Economia</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/">
                                                <span class="titulo">G1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/mundo/">
                                                <span class="titulo">Mundo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/">
                                                <span class="titulo">Jornal Extra</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/politica/">
                                                <span class="titulo">PolÃ­tica</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/concursos-e-emprego/">
                                                <span class="titulo">Concursos e Empregos</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/">
                                                <span class="titulo">Jornal O Globo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/jornal-hoje/">
                                                <span class="titulo">Jornal Hoje</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://fantastico.globo.com/">
                                                <span class="titulo">FantÃ¡stico</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="g1">
                                        <div class="submenu-title">g1</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Carros</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/concursos-e-emprego/">
                                                    <span class="titulo">Concursos e Empregos</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/">
                                                    <span class="titulo">Economia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/educacao/">
                                                    <span class="titulo">EducaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/imposto-de-renda/2015/index.html">
                                                    <span class="titulo">Imposto de Renda 2015</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/mundo/">
                                                    <span class="titulo">Mundo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/planeta-bizarro/">
                                                    <span class="titulo">Planeta Bizarro</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/politica/">
                                                    <span class="titulo">PolÃ­tica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/">
                                                    <span class="titulo">Pop &amp; Arte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/tecnologia/">
                                                    <span class="titulo">Tecnologia &amp; Games</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bom-dia-brasil/">
                                                    <span class="titulo">Bom dia Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://fantastico.globo.com/">
                                                    <span class="titulo">FantÃ¡stico</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/agronegocios/">
                                                    <span class="titulo">Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/globo-news/">
                                                    <span class="titulo">GloboNews</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/hora1">
                                                    <span class="titulo">Hora 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-da-globo/">
                                                    <span class="titulo">Jornal da Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-hoje/">
                                                    <span class="titulo">Jornal Hoje</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-nacional/">
                                                    <span class="titulo">Jornal Nacional</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="jornais-revistas-e-radio">
                                        <div class="submenu-title">jornais, revistas e rÃ¡dio</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/">
                                                    <span class="titulo">Jornal Extra</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/">
                                                    <span class="titulo">Jornal O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://cbn.globoradio.globo.com/home/HOME.htm">
                                                    <span class="titulo">RÃ¡dio CBN</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaautoesporte.globo.com/">
                                                    <span class="titulo">Revista Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaepoca.globo.com/">
                                                    <span class="titulo">Revista Ãpoca</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagalileu.globo.com/">
                                                    <span class="titulo">Revista Galileu</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagloborural.globo.com/">
                                                    <span class="titulo">Revista Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radioglobo.globoradio.globo.com/">
                                                    <span class="titulo">RÃ¡dio Globo</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="servicos">
                                        <div class="submenu-title">serviÃ§os</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/agenda.html">
                                                    <span class="titulo">Agenda de Shows</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://especiais.g1.globo.com/educacao/app-g1-enem/">
                                                    <span class="titulo">Aplicativo G1 Enem</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/">
                                                    <span class="titulo">Conversor de Moedas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">
                                                    <span class="titulo">CotaÃ§Ãµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/">
                                                    <span class="titulo">Enem e Vestibular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/loteria/">
                                                    <span class="titulo">Loterias</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/previsao-do-tempo.html">
                                                    <span class="titulo">PrevisÃ£o do tempo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/tabela-fipe/index.html">
                                                    <span class="titulo">Tabela FIPE</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/telecurso/">
                                                    <span class="titulo">Telecurso</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.techtudo.com.br/velocimetro.html">
                                                    <span class="titulo">VelocÃ­metro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-g1"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="globoesporte analytics-product analytics-multi-product">
            <a href="http://globoesporte.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">globoesporte</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-globoesporte">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/" data-menu-id="globoesportecom">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globoesporte.com</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://sportv.globo.com/" data-menu-id="sportv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sportv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/futebol/" data-menu-id="futebol">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">futebol</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="mais-esportes">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mais esportes</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-globoesporte">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-globoesporte" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/selecao-brasileira/">
                                                <span class="titulo">SeleÃ§Ã£o Brasileira</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie B</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/cartola-fc/">
                                                <span class="titulo">Cartola FC</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                <span class="titulo">Futebol Internacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                <span class="titulo">FÃ³rmula 1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://sportv.globo.com/site/combate/">
                                                <span class="titulo">MMA</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/">
                                                <span class="titulo">Globo Esporte</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/">
                                                <span class="titulo">Futebol Nacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/tenis/">
                                                <span class="titulo">TÃªnis</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="globoesportecom">
                                        <div class="submenu-title">globoesporte.com</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://futpedia.globo.com/">
                                                    <span class="titulo">FutpÃ©dia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/cartola-fc/">
                                                    <span class="titulo">Cartola FC</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="sportv">
                                        <div class="submenu-title">sportv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/nba/">
                                                    <span class="titulo">NBA</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/eventos/combate/">
                                                    <span class="titulo">Combate</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/corujao-do-esporte/">
                                                    <span class="titulo">CorujÃ£o do Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/esporte-espetacular/">
                                                    <span class="titulo">Esporte Espetacular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/">
                                                    <span class="titulo">Globo Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://app.globoesporte.globo.com/tv/planeta-extremo/">
                                                    <span class="titulo">Planeta Extremo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sociopremiere.globo.com/">
                                                    <span class="titulo">Premiere</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/">
                                                    <span class="titulo">SporTV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/lutas/tuf-brasil/">
                                                    <span class="titulo">The Ultimate Fighter Brasil</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="futebol">
                                        <div class="submenu-title">futebol</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie B</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-c/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie C</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-do-brasil/">
                                                    <span class="titulo">Copa do Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-sul-americana/">
                                                    <span class="titulo">Copa Sul-Americana</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                    <span class="titulo">Futebol Internacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol Nacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">
                                                    <span class="titulo">Liga dos CampeÃµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/selecao-brasileira/">
                                                    <span class="titulo">SeleÃ§Ã£o Brasileira</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="mais-esportes">
                                        <div class="submenu-title">mais esportes</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/atletismo/">
                                                    <span class="titulo">Atletismo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/basquete/">
                                                    <span class="titulo">Basquete</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eu-atleta/">
                                                    <span class="titulo">Eu Atleta</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                    <span class="titulo">FÃ³rmula 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eventos/futsal/">
                                                    <span class="titulo">Futsal</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/combate/">
                                                    <span class="titulo">MMA</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/natacao/">
                                                    <span class="titulo">NataÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/stock-car/">
                                                    <span class="titulo">StockCar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/tenis/">
                                                    <span class="titulo">TÃªnis</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/volei/">
                                                    <span class="titulo">VÃ´lei</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-globoesporte"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="gshow analytics-product analytics-multi-product">
            <a href="http://gshow.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">gshow</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/" data-menu-id="gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gshow</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="programas">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">programas</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="series">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sÃ©ries</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/programas/webseries/" data-menu-id="series-originais">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sÃ©ries originais</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com/" data-menu-id="rede-globo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">rede globo</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-gshow">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-gshow" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/mais-voce/">
                                                <span class="titulo">Mais VocÃª</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/realities/the-voice-brasil/">
                                                <span class="titulo">The Voice</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                <span class="titulo">Encontro com FÃ¡tima</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                <span class="titulo">VÃ­deo Show</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/">
                                                <span class="titulo">Gshow</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://redeglobo.globo.com/programacao.html">
                                                <span class="titulo">ProgramaÃ§Ã£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/programa-do-jo/">
                                                <span class="titulo">Programa do JÃ´</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                <span class="titulo">CaldeirÃ£o do Huck</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/webseries/">
                                                <span class="titulo">SÃ©ries originais</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="gshow">
                                        <div class="submenu-title">gshow</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Bastidores/">
                                                    <span class="titulo">Bastidores</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/como-fazer/">
                                                    <span class="titulo">Como Fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo">
                                                    <span class="titulo">Estilo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/especial-blog/gshow-troll/1.html">
                                                    <span class="titulo">Gshow Troll</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/participe/">
                                                    <span class="titulo">Participe</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas-da-Ana-Maria/">
                                                    <span class="titulo">Receitas da Ana Maria</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/tv/plantao/">
                                                    <span class="titulo">TV</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="programas">
                                        <div class="submenu-title">programas</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://gshow.globo.com/programas/altas-horas/">
                                                    <span class="titulo">Altas Horas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                    <span class="titulo">CaldeirÃ£o do Huck</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                    <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/e-de-casa/">
                                                    <span class="titulo">Ã de Casa</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                    <span class="titulo">Encontro com FÃ¡tima</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/estrelas/index.html">
                                                    <span class="titulo">Estrelas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/">
                                                    <span class="titulo">Mais VocÃª</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/programa-do-jo/">
                                                    <span class="titulo">Programa do JÃ´</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/realities/the-voice-brasil/">
                                                    <span class="titulo">The Voice</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                    <span class="titulo">VÃ­deo Show</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="series">
                                        <div class="submenu-title">sÃ©ries</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/amorteamo/">
                                                    <span class="titulo">Amorteamo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/chapa-quente/">
                                                    <span class="titulo">Chapa Quente</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/ligacoes-perigosas/">
                                                    <span class="titulo">LigaÃ§Ãµes Perigosas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/mister-brau/2015/">
                                                    <span class="titulo">Mister Brau</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/pe-na-cova/2015/">
                                                    <span class="titulo">PÃ© na Cova</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/especial-blog/ta-no-ar-a-tv-na-tv/1.html">
                                                    <span class="titulo">TÃ¡ no ar: a TV na TV </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/zorra/">
                                                    <span class="titulo">Zorra</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="series-originais">
                                        <div class="submenu-title">sÃ©ries originais</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/como-fazer/no-ar.html">
                                                    <span class="titulo">Como fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/dulce-delight/">
                                                    <span class="titulo">Dulce Delight</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/laboratorio-do-som/no-ar.html">
                                                    <span class="titulo">LaboratÃ³rio do som</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/lembrancas-do-iraja/no-ar.html">
                                                    <span class="titulo">LembranÃ§as do IrajÃ¡ - PÃ© na cova</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/maes-a-obra/no-ar.html">
                                                    <span class="titulo">MÃ£es Ã  obra</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/morri-na-tv/no-ar.html">
                                                    <span class="titulo">Morri na TV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/o-incrivel-superonix/no-ar.html">
                                                    <span class="titulo">O incrÃ­vel SuperÃ´nix</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/os-desatinados-malhacao-seu-lugar-no-mundo/no-ar.html">
                                                    <span class="titulo">Os desatinados</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/sebastiana-quebra-galho/no-ar.html">
                                                    <span class="titulo">Sebastiana Quebra-Galho</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/verdades-secretasdoc/no-ar.html">
                                                    <span class="titulo">Verdades Secretas.doc</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="rede-globo">
                                        <div class="submenu-title">rede globo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/programacao.html">
                                                    <span class="titulo">ProgramaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-gshow"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="famosos-etc analytics-product analytics-multi-product">
            <a href="http://famosos.globo.com" class="analytics-area analytics-id-T">
                <span class="titulo">famosos &amp; etc</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-famosos-etc">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://famosos.globo.com/" data-menu-id="famosos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">famosos</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="moda-estilo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">moda & estilo</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="bem-estar-saude">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">bem-estar & saÃºde</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="casa-decoracao">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">casa & decoraÃ§Ã£o</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="gastronomia">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gastronomia</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="musica-cinema-e-teatro">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mÃºsica, cinema e teatro</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="canais-de-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">canais de tv</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-famosos-etc">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-famosos-etc" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://ego.globo.com/">
                                                <span class="titulo">Ego</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://paparazzo.globo.com/">
                                                <span class="titulo">Paparazzo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://revistaquem.globo.com/">
                                                <span class="titulo">Quem</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/famosos/">
                                                <span class="titulo">Retratos da Vida</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/cultura/kogut/">
                                                <span class="titulo">Patricia Kogut</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://famosos.globo.com/">
                                                <span class="titulo">Famosos</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="famosos">
                                        <div class="submenu-title">famosos</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/">
                                                    <span class="titulo">Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://paparazzo.globo.com/">
                                                    <span class="titulo">Paparazzo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/cultura/kogut/">
                                                    <span class="titulo">Patricia Kogut</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaquem.globo.com/">
                                                    <span class="titulo">Quem</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/famosos/">
                                                    <span class="titulo">Retratos da Vida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="moda-estilo">
                                        <div class="submenu-title">moda &amp; estilo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/moda/index.html">
                                                    <span class="titulo">Moda no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ela.oglobo.globo.com/">
                                                    <span class="titulo">Ela no O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo/">
                                                    <span class="titulo">Estilo no Gshow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaglamour.globo.com/">
                                                    <span class="titulo">Revista Glamour</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gq.globo.com/">
                                                    <span class="titulo">Revista GQ Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistamarieclaire.globo.com/">
                                                    <span class="titulo">Revista Marie Claire </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/moda/">
                                                    <span class="titulo">Moda no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://vogue.globo.com/">
                                                    <span class="titulo">Revista Vogue</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="bem-estar-saude">
                                        <div class="submenu-title">bem-estar &amp; saÃºde</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/beleza/index.html">
                                                    <span class="titulo">Beleza no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/beleza/">
                                                    <span class="titulo">Beleza no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.com.br/bemestar">
                                                    <span class="titulo">Bem Estar no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar no G1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/maes-e-filhos/">
                                                    <span class="titulo">MÃ£es no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/">
                                                    <span class="titulo">Meus 5 Minutos </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://horoscopo.ego.globo.com/">
                                                    <span class="titulo">HorÃ³scopo no Ego</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="casa-decoracao">
                                        <div class="submenu-title">casa &amp; decoraÃ§Ã£o</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/">
                                                    <span class="titulo">Revista Casa e Jardim</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/casa-e-decoracao/">
                                                    <span class="titulo">Casa no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://casavogue.globo.com/">
                                                    <span class="titulo">Casa Vogue </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revista.zapimoveis.com.br/?utm_source=globo.com-etc&amp;utm_medium=botao-casa-decoracao&amp;utm_campaign=Revista-Home">
                                                    <span class="titulo">Revista do Zap</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="gastronomia">
                                        <div class="submenu-title">gastronomia</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/receitas/">
                                                    <span class="titulo">Receitas no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas/index.html">
                                                    <span class="titulo">Receitas no Mais VocÃª </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/blogs/Colheradas/">
                                                    <span class="titulo">Colheradas </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/Casa-e-Comida/">
                                                    <span class="titulo">Casa e Comida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="musica-cinema-e-teatro">
                                        <div class="submenu-title">mÃºsica, cinema e teatro</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/cinema/">
                                                    <span class="titulo">G1 Cinema</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/">
                                                    <span class="titulo">G1 MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">Gshow MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://musica.com.br/">
                                                    <span class="titulo">MÃºsica.com.br</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radiobeat.com.br/">
                                                    <span class="titulo">RADIOBEAT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.agentesevenoteatro.com.br/">
                                                    <span class="titulo">Teatro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="canais-de-tv">
                                        <div class="submenu-title">canais de tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/">
                                                    <span class="titulo">Rede Globo </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalbrasil.globo.com/">
                                                    <span class="titulo">Canal Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://mundogloob.globo.com/">
                                                    <span class="titulo">Gloob</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/">
                                                    <span class="titulo">GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://megapix.globo.com/">
                                                    <span class="titulo">Megapix</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://multishow.globo.com/">
                                                    <span class="titulo">Multishow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://telecine.globo.com/">
                                                    <span class="titulo">Telecine</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://uc.globo.com/">
                                                    <span class="titulo">Universal Channel</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalviva.globo.com/">
                                                    <span class="titulo">Viva</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://canaloff.globo.com">
                                                    <span class="titulo">Off</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-famosos-etc"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="tecnologia analytics-product analytics-multi-product">
            <a href="http://www.techtudo.com.br/" class="analytics-area analytics-id-T">
                <span class="titulo">tecnologia</span>

        
        
            </a>
        </li>
        
    
        <li class="videos analytics-product analytics-multi-product">
            <a href="http://globoplay.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">vÃ­deos</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                            
                                <li class="has-link ">
                                    <a href="http://globoplay.globo.com/" data-menu-id="globo-play">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globo play</span>
                                    </a>
                                </li>
                            
                                <li class="has-link ">
                                    <a href="http://globosatplay.globo.com/" data-menu-id="globosat-play">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globosat play</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-videos">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
</ul>
<!-- gerado em: 2015-11-1716:29:05Z --><div id="assinante-menu"></div></nav></div><div class="header-bottom"></div></header><script>
libby.loadScript('http://s.glbimg.com/en/ho/static/etc/busca/js/jquery.buscaPadrao.v2.compressed.js', function(){var searchArea=document.getElementById('busca-padrao'),
placeholderValue = 'encontre na globo.com';if(typeof searchWidget !== "undefined" && searchWidget){searchWidget(searchArea, {suggestionsEnabled: true,
baseSearchUrl: 'http://www.globo.com',
addClass:'home-search',
placeholder: placeholderValue,
buscaHome: true,
qtd_min_letras_pro_suggest: 2,
qtd_min_letras_pra_busca: 2,
addStyleSheet: ''});}
searchArea.style.display = "block";if(searchArea && searchArea.querySelectorAll){var inputElement=searchArea.querySelectorAll('input')[0];inputElement.onwebkitspeechchange=inputElement.onspeechchange=function (ev) {inputElement.value=inputElement.value.replace(placeholderValue, '');inputElement.form.submit();};}});</script><div id="x60" class="opec-area opec-mobile opec-x60 grid-12"><div id="banner_mobile_topo" class="tag-manager-publicidade-container"></div></div><div id="urgente" class="container urgente analytics-area analytics-id-U "><div class="grid-base super-wide"></div></div><div id="bloco-principal" class="container first-scroll-container clearfix"><div class="grid-base wide analytics-area analytics-id-A"><div class="grid-base narrow pull-left primeira-area hide-mobile"><div class="destaque-principal-vertical-foto-topo destaque destaque-primeiro-scroll principal glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/futebol-internacional/noticia/2015/11/estadio-e-evacuado-e-amistoso-entre-alemanha-e-holanda-e-cancelado.html" class=" " title="EstÃ¡dio Ã© esvaziado,
 e jogo Alemanha x Holanda Ã© cancelado"><div class="conteudo"><h2>EstÃ¡dio Ã© esvaziado,<br /> e jogo Alemanha x Holanda Ã© cancelado</h2></div></a></div></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/minas-gerais/noticia/2015/11/samarco-admite-risco-de-rompimento-nas-barragens-santarem-e-germano.html" class=" " title="Samarco admite que mais duas barragens podem se romper"><div class="conteudo"><h2>Samarco admite que mais duas barragens podem se romper</h2></div></a></div></div><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/noticia/2015/11/liderancas-defendem-que-pmdb-nao-se-atrele-programa-do-pt-e-tenha-voz-propria.html" class="foto " title="No local, Temer ouve gritos de &#39;presidente&#39; (JosÃ© Cruz/AgÃªncia Brasil)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/HRu0SI0VquZzUsc-_d5hmvw92pM=/filters:quality(10):strip_icc()/s2.glbimg.com/eUet2Pv9r3Hyi9toHkcRUEXFN30=/0x46:620x326/155x70/s.glbimg.com/jo/g1/f/original/2015/11/17/temer_-_pmdb.jpg" alt="No local, Temer ouve gritos de &#39;presidente&#39; (JosÃ© Cruz/AgÃªncia Brasil)" title="No local, Temer ouve gritos de &#39;presidente&#39; (JosÃ© Cruz/AgÃªncia Brasil)"
         data-original-image="s2.glbimg.com/eUet2Pv9r3Hyi9toHkcRUEXFN30=/0x46:620x326/155x70/s.glbimg.com/jo/g1/f/original/2015/11/17/temer_-_pmdb.jpg" data-url-smart_horizontal="xpvDNPTd7KFKq9id3GKF0Gcj_vw=/90x56/smart/filters:strip_icc()/" data-url-smart="xpvDNPTd7KFKq9id3GKF0Gcj_vw=/90x56/smart/filters:strip_icc()/" data-url-feature="xpvDNPTd7KFKq9id3GKF0Gcj_vw=/90x56/smart/filters:strip_icc()/" data-url-tablet="TeCmrgILbF0162Ayr6KoAQcKOvk=/160xorig/smart/filters:strip_icc()/" data-url-desktop="OSNJsdvXz4FuFkV1N48g9gHWr4I=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>No local, Temer ouve gritos de &#39;presidente&#39;</h2></div></a></div></div><div class="ultimo-destaque"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-entretenimento analytics-area  analytics-id-M
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/vem-por-ai/noticia/2015/11/dante-fica-um-passo-de-desmascarar-romero.html" class="foto " title="&#39;Regra&#39;: Dante fica perto de saber suspeitas (TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/vKjhyVYFdnmA-dvRfBOFbA1m_mc=/filters:quality(10):strip_icc()/s2.glbimg.com/B3NfX19lQtT4ev7mvoaDDhfRgh0=/26x0:620x287/155x75/s.glbimg.com/og/rg/f/original/2015/11/17/dante-620.jpg" alt="&#39;Regra&#39;: Dante fica perto de saber suspeitas (TV Globo)" title="&#39;Regra&#39;: Dante fica perto de saber suspeitas (TV Globo)"
         data-original-image="s2.glbimg.com/B3NfX19lQtT4ev7mvoaDDhfRgh0=/26x0:620x287/155x75/s.glbimg.com/og/rg/f/original/2015/11/17/dante-620.jpg" data-url-smart_horizontal="tVsXJCLScJgue_IgQ-25uY8Uoyc=/90x56/smart/filters:strip_icc()/" data-url-smart="tVsXJCLScJgue_IgQ-25uY8Uoyc=/90x56/smart/filters:strip_icc()/" data-url-feature="tVsXJCLScJgue_IgQ-25uY8Uoyc=/90x56/smart/filters:strip_icc()/" data-url-tablet="wWuIgH_VV7HnRVCloGLJqeHju90=/160xorig/smart/filters:strip_icc()/" data-url-desktop="HZqbAEdlJh-AiRHnAgWDbRjF67Q=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>&#39;Regra&#39;: Dante fica perto de saber suspeitas</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Guerra Ã© desmascarado" href="http://extra.globo.com/tv-e-lazer/telinha/a-regra-do-jogo-faustini-desmascara-guerra-18063474.html">Guerra Ã© desmascarado</a></div></li><li><div class="mobile-grid-partial"><a title="Domingas expulsa Juca" href="http://oglobo.globo.com/cultura/revista-da-tv/domingas-vai-expulsar-juca-de-casa-tera-novo-amor-em-regra-do-jogo-18072484">Domingas expulsa Juca</a></div></li></ul></div><div class="direita"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/totalmente-demais/vem-por-ai/noticia/2015/11/carolina-provoca-arthur-e-da-mole-para-rafael.html" class="foto " title="Carolina dÃ¡ em cima de Rafael em &#39;Totalmente&#39; (TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/gCw5z6KGfyA7hr2sl7QHysJFbcY=/filters:quality(10):strip_icc()/s2.glbimg.com/J5Yv7EFVnMYQQptN0Qg9l_oti3Q=/7x4:658x319/155x75/s.glbimg.com/et/gs/f/original/2015/11/16/gshow242_cortada.jpg" alt="Carolina dÃ¡ em cima de Rafael em &#39;Totalmente&#39; (TV Globo)" title="Carolina dÃ¡ em cima de Rafael em &#39;Totalmente&#39; (TV Globo)"
         data-original-image="s2.glbimg.com/J5Yv7EFVnMYQQptN0Qg9l_oti3Q=/7x4:658x319/155x75/s.glbimg.com/et/gs/f/original/2015/11/16/gshow242_cortada.jpg" data-url-smart_horizontal="SeFrFinRK5iZngC-NombF2Cg8M8=/90x56/smart/filters:strip_icc()/" data-url-smart="SeFrFinRK5iZngC-NombF2Cg8M8=/90x56/smart/filters:strip_icc()/" data-url-feature="SeFrFinRK5iZngC-NombF2Cg8M8=/90x56/smart/filters:strip_icc()/" data-url-tablet="wRrTKSYHwY4lXhMkbT7pUCs2M_Y=/160xorig/smart/filters:strip_icc()/" data-url-desktop="lIOrg23IGVnLDmhTvSnS7h5_S9I=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Carolina dÃ¡ em cima de Rafael em &#39;Totalmente&#39;</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Eliza sonha com Arthur" href="http://kogut.oglobo.globo.com/noticias-da-tv/novelas/noticia/2015/11/eliza-sonhara-com-arthur-mas-acabara-beijando-jonatas.html">Eliza sonha com Arthur</a></div></li><li><div class="mobile-grid-partial"><a title="Cassandra dÃ¡ vexame" href="http://gshow.globo.com/novelas/totalmente-demais/vem-por-ai/noticia/2015/11/cassandra-causa-na-agencia-e-leva-fora-de-arthur.html">Cassandra dÃ¡ vexame</a></div></li></ul></div></div></div></div><div class="grid-base narrow ultimo hide-mobile"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://extra.globo.com/noticias/mundo/marcas-de-tiros-em-escudo-de-policial-mostram-que-terroristas-estavam-fortemente-armados-18071909.html" class="foto " title="Escudo ficou crivado de balas em invasÃ£o (KENZO TRIBOUILLARD / AFP)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/ddtWEezP3uYJedbYCKNB8yN9BD8=/filters:quality(10):strip_icc()/s2.glbimg.com/U05voW_71MCTPDKZ93i2CutTo2k=/9x0:412x299/155x115/s.glbimg.com/en/ho/f/original/2015/11/17/escudo-3.jpg" alt="Escudo ficou crivado de balas em invasÃ£o (KENZO TRIBOUILLARD / AFP)" title="Escudo ficou crivado de balas em invasÃ£o (KENZO TRIBOUILLARD / AFP)"
         data-original-image="s2.glbimg.com/U05voW_71MCTPDKZ93i2CutTo2k=/9x0:412x299/155x115/s.glbimg.com/en/ho/f/original/2015/11/17/escudo-3.jpg" data-url-smart_horizontal="lXO3IbA2JB246uSsL7SIkh3qqPQ=/90x56/smart/filters:strip_icc()/" data-url-smart="lXO3IbA2JB246uSsL7SIkh3qqPQ=/90x56/smart/filters:strip_icc()/" data-url-feature="lXO3IbA2JB246uSsL7SIkh3qqPQ=/90x56/smart/filters:strip_icc()/" data-url-tablet="HTyMGCPDPpEdsqAvRTUnNFxvuPw=/160xorig/smart/filters:strip_icc()/" data-url-desktop="KrFh3vI6Zd5EbuALVA2dYzQ_qsk=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Escudo ficou crivado de balas em invasÃ£o</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Siga: Alemanha prende mulheres e homem" href="http://g1.globo.com/mundo/ao-vivo/2015/explosoes-e-tiroteio-em-paris.html">Siga: Alemanha prende mulheres e homem</a></div></li></ul></div><div class="direita"><div class="mobile-grid-partial"><a href="http://g1.globo.com/mundo/noticia/2015/11/irmao-de-salah-abdeslam-pede-que-ele-se-entregue-policia.html" class="foto " title="IrmÃ£o pede que suspeito chave se entregue (REUTERS/Benoit De Freine)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/EqGGTicc_vR6hRgyE5eakvJ0thQ=/filters:quality(10):strip_icc()/s2.glbimg.com/tngssqeSquHdZA5Q3EJGBcX6c-w=/500x234:1388x892/155x115/s.glbimg.com/jo/g1/f/original/2015/11/16/mohamed-abdeslam.jpg" alt="IrmÃ£o pede que suspeito chave se entregue (REUTERS/Benoit De Freine)" title="IrmÃ£o pede que suspeito chave se entregue (REUTERS/Benoit De Freine)"
         data-original-image="s2.glbimg.com/tngssqeSquHdZA5Q3EJGBcX6c-w=/500x234:1388x892/155x115/s.glbimg.com/jo/g1/f/original/2015/11/16/mohamed-abdeslam.jpg" data-url-smart_horizontal="155OlaOlDU-bsvUzpBIGnai-BDY=/90x56/smart/filters:strip_icc()/" data-url-smart="155OlaOlDU-bsvUzpBIGnai-BDY=/90x56/smart/filters:strip_icc()/" data-url-feature="155OlaOlDU-bsvUzpBIGnai-BDY=/90x56/smart/filters:strip_icc()/" data-url-tablet="rcsHZhsOCEKypG52RuJPfZZeCgU=/160xorig/smart/filters:strip_icc()/" data-url-desktop="_9E70giA7Dak7DzP6DrFOSHnOf8=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>IrmÃ£o pede que suspeito chave se entregue</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Jovem chama por amigo que morreu por ela" href="http://extra.globo.com/noticias/mundo/vitima-de-ataque-em-paris-nao-para-de-repetir-nome-do-homem-que-salvou-18068954.html">Jovem chama por amigo que morreu por ela</a></div></li></ul></div></div><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/mundo/noticia/2015/11/anonymous-diz-ter-derrubado-contas-do-estado-islamico-no-twitter.html" class="foto " title="Hackers dizem ter tirado 5,5 mil contas do ar (ReproduÃ§Ã£o/Youtube)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/Le5z1WNxE7MI6rHaxNArHecAxp8=/filters:quality(10):strip_icc()/s2.glbimg.com/BWOAMy4048U9kITBy5tddFG_WUU=/0x63:1700x831/155x70/s.glbimg.com/jo/g1/f/original/2015/11/16/hackers-anonymous-estado-islamico.jpg" alt="Hackers dizem ter tirado 5,5 mil contas do ar (ReproduÃ§Ã£o/Youtube)" title="Hackers dizem ter tirado 5,5 mil contas do ar (ReproduÃ§Ã£o/Youtube)"
         data-original-image="s2.glbimg.com/BWOAMy4048U9kITBy5tddFG_WUU=/0x63:1700x831/155x70/s.glbimg.com/jo/g1/f/original/2015/11/16/hackers-anonymous-estado-islamico.jpg" data-url-smart_horizontal="EQuLywuNR-cSUdUt8P8_gfCdMIU=/90x56/smart/filters:strip_icc()/" data-url-smart="EQuLywuNR-cSUdUt8P8_gfCdMIU=/90x56/smart/filters:strip_icc()/" data-url-feature="EQuLywuNR-cSUdUt8P8_gfCdMIU=/90x56/smart/filters:strip_icc()/" data-url-tablet="YXx5nRUFWyVOSEZnr0_Lk5Noewo=/160xorig/smart/filters:strip_icc()/" data-url-desktop="p0eyqy6GT2Kt_OTpGA-O4rxbcus=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Hackers dizem ter tirado 5,5 mil contas do ar</h2></div></a></div></div><div class="ultimo-destaque"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://extra.globo.com/esporte/jogadores-da-franca-caminham-perto-do-wembley-com-policiais-fortemente-armados-tiram-fotos-com-fas-18071271.html" class="foto " title="SeleÃ§Ã£o francesa Ã© escoltada em rua de Londres (Justin Tallis/AFP)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/vDIZpjUlMrql8S0eMw7XeswkLdI=/filters:quality(10):strip_icc()/s2.glbimg.com/GyQ1MGdLttVmFe9OKm6GG9uyZyg=/116x7:628x221/155x65/s.glbimg.com/en/ho/f/original/2015/11/17/france.jpg" alt="SeleÃ§Ã£o francesa Ã© escoltada em rua de Londres (Justin Tallis/AFP)" title="SeleÃ§Ã£o francesa Ã© escoltada em rua de Londres (Justin Tallis/AFP)"
         data-original-image="s2.glbimg.com/GyQ1MGdLttVmFe9OKm6GG9uyZyg=/116x7:628x221/155x65/s.glbimg.com/en/ho/f/original/2015/11/17/france.jpg" data-url-smart_horizontal="YPU3aXcirqG3kRFrRpwgt9v9Zlk=/90x56/smart/filters:strip_icc()/" data-url-smart="YPU3aXcirqG3kRFrRpwgt9v9Zlk=/90x56/smart/filters:strip_icc()/" data-url-feature="YPU3aXcirqG3kRFrRpwgt9v9Zlk=/90x56/smart/filters:strip_icc()/" data-url-tablet="kIBhe9U4jqJb0SjPNeAgR0oLfls=/160xorig/smart/filters:strip_icc()/" data-url-desktop="NBL12ddwoVRRllL3LIq2X93FqjM=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>SeleÃ§Ã£o francesa Ã© escoltada em rua de Londres</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/blogs/especial-blog/brasil-mundial-fc/post/messi-treina-bate-forte-na-bola-e-parece-encerrar-duvidas-sobre-escalacao.html" class="foto " title="Messi treina e estÃ¡ prÃ³ximo da volta; veja vÃ­deo (DivulgaÃ§Ã£o FC Barcelona)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/4IayW3SwB8i4t8h2hy9ZfjoeArs=/filters:quality(10):strip_icc()/s2.glbimg.com/TTAqvyv8prswFdivMs22YbiIsRE=/105x56:563x248/155x65/s.glbimg.com/es/ge/f/original/2015/11/17/messi2.jpg" alt="Messi treina e estÃ¡ prÃ³ximo da volta; veja vÃ­deo (DivulgaÃ§Ã£o FC Barcelona)" title="Messi treina e estÃ¡ prÃ³ximo da volta; veja vÃ­deo (DivulgaÃ§Ã£o FC Barcelona)"
         data-original-image="s2.glbimg.com/TTAqvyv8prswFdivMs22YbiIsRE=/105x56:563x248/155x65/s.glbimg.com/es/ge/f/original/2015/11/17/messi2.jpg" data-url-smart_horizontal="-Kubk4HdhZg9_GUWHzglNX8sDpY=/90x56/smart/filters:strip_icc()/" data-url-smart="-Kubk4HdhZg9_GUWHzglNX8sDpY=/90x56/smart/filters:strip_icc()/" data-url-feature="-Kubk4HdhZg9_GUWHzglNX8sDpY=/90x56/smart/filters:strip_icc()/" data-url-tablet="F1u7Zh9jHgayeQ2_9i_XvkDrT58=/160xorig/smart/filters:strip_icc()/" data-url-desktop="7RiAL5FbzNZX9To1ppXJ_wYvPhc=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Messi treina e estÃ¡ prÃ³ximo da volta; veja vÃ­deo</h2></div></a></div></div></div></div></div><div class="grid-base wide analytics-area analytics-id-A mobile-grid-base"><div class="destaque-principal-vertical-foto-topo destaque destaque-primeiro-scroll principal glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/futebol-internacional/noticia/2015/11/estadio-e-evacuado-e-amistoso-entre-alemanha-e-holanda-e-cancelado.html" class=" " title="EstÃ¡dio Ã© esvaziado,
 e jogo Alemanha x Holanda Ã© cancelado"><div class="conteudo"><h2>EstÃ¡dio Ã© esvaziado,<br /> e jogo Alemanha x Holanda Ã© cancelado</h2></div></a></div></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/minas-gerais/noticia/2015/11/samarco-admite-risco-de-rompimento-nas-barragens-santarem-e-germano.html" class=" " title="Samarco admite que mais duas barragens podem se romper"><div class="conteudo"><h2>Samarco admite que mais duas barragens podem se romper</h2></div></a></div></div><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/noticia/2015/11/liderancas-defendem-que-pmdb-nao-se-atrele-programa-do-pt-e-tenha-voz-propria.html" class="foto " title="No local, Temer ouve gritos de &#39;presidente&#39; (JosÃ© Cruz/AgÃªncia Brasil)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/HRu0SI0VquZzUsc-_d5hmvw92pM=/filters:quality(10):strip_icc()/s2.glbimg.com/eUet2Pv9r3Hyi9toHkcRUEXFN30=/0x46:620x326/155x70/s.glbimg.com/jo/g1/f/original/2015/11/17/temer_-_pmdb.jpg" alt="No local, Temer ouve gritos de &#39;presidente&#39; (JosÃ© Cruz/AgÃªncia Brasil)" title="No local, Temer ouve gritos de &#39;presidente&#39; (JosÃ© Cruz/AgÃªncia Brasil)"
         data-original-image="s2.glbimg.com/eUet2Pv9r3Hyi9toHkcRUEXFN30=/0x46:620x326/155x70/s.glbimg.com/jo/g1/f/original/2015/11/17/temer_-_pmdb.jpg" data-url-smart_horizontal="xpvDNPTd7KFKq9id3GKF0Gcj_vw=/90x56/smart/filters:strip_icc()/" data-url-smart="xpvDNPTd7KFKq9id3GKF0Gcj_vw=/90x56/smart/filters:strip_icc()/" data-url-feature="xpvDNPTd7KFKq9id3GKF0Gcj_vw=/90x56/smart/filters:strip_icc()/" data-url-tablet="TeCmrgILbF0162Ayr6KoAQcKOvk=/160xorig/smart/filters:strip_icc()/" data-url-desktop="OSNJsdvXz4FuFkV1N48g9gHWr4I=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>No local, Temer ouve gritos de &#39;presidente&#39;</h2></div></a></div></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://extra.globo.com/noticias/mundo/marcas-de-tiros-em-escudo-de-policial-mostram-que-terroristas-estavam-fortemente-armados-18071909.html" class="foto " title="Escudo ficou crivado de balas em invasÃ£o (KENZO TRIBOUILLARD / AFP)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/ddtWEezP3uYJedbYCKNB8yN9BD8=/filters:quality(10):strip_icc()/s2.glbimg.com/U05voW_71MCTPDKZ93i2CutTo2k=/9x0:412x299/155x115/s.glbimg.com/en/ho/f/original/2015/11/17/escudo-3.jpg" alt="Escudo ficou crivado de balas em invasÃ£o (KENZO TRIBOUILLARD / AFP)" title="Escudo ficou crivado de balas em invasÃ£o (KENZO TRIBOUILLARD / AFP)"
         data-original-image="s2.glbimg.com/U05voW_71MCTPDKZ93i2CutTo2k=/9x0:412x299/155x115/s.glbimg.com/en/ho/f/original/2015/11/17/escudo-3.jpg" data-url-smart_horizontal="lXO3IbA2JB246uSsL7SIkh3qqPQ=/90x56/smart/filters:strip_icc()/" data-url-smart="lXO3IbA2JB246uSsL7SIkh3qqPQ=/90x56/smart/filters:strip_icc()/" data-url-feature="lXO3IbA2JB246uSsL7SIkh3qqPQ=/90x56/smart/filters:strip_icc()/" data-url-tablet="HTyMGCPDPpEdsqAvRTUnNFxvuPw=/160xorig/smart/filters:strip_icc()/" data-url-desktop="KrFh3vI6Zd5EbuALVA2dYzQ_qsk=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Escudo ficou crivado de balas em invasÃ£o</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Siga: Alemanha prende mulheres e homem" href="http://g1.globo.com/mundo/ao-vivo/2015/explosoes-e-tiroteio-em-paris.html">Siga: Alemanha prende mulheres e homem</a></div></li></ul></div><div class="direita"><div class="mobile-grid-partial"><a href="http://g1.globo.com/mundo/noticia/2015/11/irmao-de-salah-abdeslam-pede-que-ele-se-entregue-policia.html" class="foto " title="IrmÃ£o pede que suspeito chave se entregue (REUTERS/Benoit De Freine)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/EqGGTicc_vR6hRgyE5eakvJ0thQ=/filters:quality(10):strip_icc()/s2.glbimg.com/tngssqeSquHdZA5Q3EJGBcX6c-w=/500x234:1388x892/155x115/s.glbimg.com/jo/g1/f/original/2015/11/16/mohamed-abdeslam.jpg" alt="IrmÃ£o pede que suspeito chave se entregue (REUTERS/Benoit De Freine)" title="IrmÃ£o pede que suspeito chave se entregue (REUTERS/Benoit De Freine)"
         data-original-image="s2.glbimg.com/tngssqeSquHdZA5Q3EJGBcX6c-w=/500x234:1388x892/155x115/s.glbimg.com/jo/g1/f/original/2015/11/16/mohamed-abdeslam.jpg" data-url-smart_horizontal="155OlaOlDU-bsvUzpBIGnai-BDY=/90x56/smart/filters:strip_icc()/" data-url-smart="155OlaOlDU-bsvUzpBIGnai-BDY=/90x56/smart/filters:strip_icc()/" data-url-feature="155OlaOlDU-bsvUzpBIGnai-BDY=/90x56/smart/filters:strip_icc()/" data-url-tablet="rcsHZhsOCEKypG52RuJPfZZeCgU=/160xorig/smart/filters:strip_icc()/" data-url-desktop="_9E70giA7Dak7DzP6DrFOSHnOf8=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>IrmÃ£o pede que suspeito chave se entregue</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Jovem chama por amigo que morreu por ela" href="http://extra.globo.com/noticias/mundo/vitima-de-ataque-em-paris-nao-para-de-repetir-nome-do-homem-que-salvou-18068954.html">Jovem chama por amigo que morreu por ela</a></div></li></ul></div></div><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/mundo/noticia/2015/11/anonymous-diz-ter-derrubado-contas-do-estado-islamico-no-twitter.html" class="foto " title="Hackers dizem ter tirado 5,5 mil contas do ar (ReproduÃ§Ã£o/Youtube)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/Le5z1WNxE7MI6rHaxNArHecAxp8=/filters:quality(10):strip_icc()/s2.glbimg.com/BWOAMy4048U9kITBy5tddFG_WUU=/0x63:1700x831/155x70/s.glbimg.com/jo/g1/f/original/2015/11/16/hackers-anonymous-estado-islamico.jpg" alt="Hackers dizem ter tirado 5,5 mil contas do ar (ReproduÃ§Ã£o/Youtube)" title="Hackers dizem ter tirado 5,5 mil contas do ar (ReproduÃ§Ã£o/Youtube)"
         data-original-image="s2.glbimg.com/BWOAMy4048U9kITBy5tddFG_WUU=/0x63:1700x831/155x70/s.glbimg.com/jo/g1/f/original/2015/11/16/hackers-anonymous-estado-islamico.jpg" data-url-smart_horizontal="EQuLywuNR-cSUdUt8P8_gfCdMIU=/90x56/smart/filters:strip_icc()/" data-url-smart="EQuLywuNR-cSUdUt8P8_gfCdMIU=/90x56/smart/filters:strip_icc()/" data-url-feature="EQuLywuNR-cSUdUt8P8_gfCdMIU=/90x56/smart/filters:strip_icc()/" data-url-tablet="YXx5nRUFWyVOSEZnr0_Lk5Noewo=/160xorig/smart/filters:strip_icc()/" data-url-desktop="p0eyqy6GT2Kt_OTpGA-O4rxbcus=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Hackers dizem ter tirado 5,5 mil contas do ar</h2></div></a></div></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://extra.globo.com/esporte/jogadores-da-franca-caminham-perto-do-wembley-com-policiais-fortemente-armados-tiram-fotos-com-fas-18071271.html" class="foto " title="SeleÃ§Ã£o francesa Ã© escoltada em rua de Londres (Justin Tallis/AFP)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/vDIZpjUlMrql8S0eMw7XeswkLdI=/filters:quality(10):strip_icc()/s2.glbimg.com/GyQ1MGdLttVmFe9OKm6GG9uyZyg=/116x7:628x221/155x65/s.glbimg.com/en/ho/f/original/2015/11/17/france.jpg" alt="SeleÃ§Ã£o francesa Ã© escoltada em rua de Londres (Justin Tallis/AFP)" title="SeleÃ§Ã£o francesa Ã© escoltada em rua de Londres (Justin Tallis/AFP)"
         data-original-image="s2.glbimg.com/GyQ1MGdLttVmFe9OKm6GG9uyZyg=/116x7:628x221/155x65/s.glbimg.com/en/ho/f/original/2015/11/17/france.jpg" data-url-smart_horizontal="YPU3aXcirqG3kRFrRpwgt9v9Zlk=/90x56/smart/filters:strip_icc()/" data-url-smart="YPU3aXcirqG3kRFrRpwgt9v9Zlk=/90x56/smart/filters:strip_icc()/" data-url-feature="YPU3aXcirqG3kRFrRpwgt9v9Zlk=/90x56/smart/filters:strip_icc()/" data-url-tablet="kIBhe9U4jqJb0SjPNeAgR0oLfls=/160xorig/smart/filters:strip_icc()/" data-url-desktop="NBL12ddwoVRRllL3LIq2X93FqjM=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>SeleÃ§Ã£o francesa Ã© escoltada em rua de Londres</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/blogs/especial-blog/brasil-mundial-fc/post/messi-treina-bate-forte-na-bola-e-parece-encerrar-duvidas-sobre-escalacao.html" class="foto " title="Messi treina e estÃ¡ prÃ³ximo da volta; veja vÃ­deo (DivulgaÃ§Ã£o FC Barcelona)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/4IayW3SwB8i4t8h2hy9ZfjoeArs=/filters:quality(10):strip_icc()/s2.glbimg.com/TTAqvyv8prswFdivMs22YbiIsRE=/105x56:563x248/155x65/s.glbimg.com/es/ge/f/original/2015/11/17/messi2.jpg" alt="Messi treina e estÃ¡ prÃ³ximo da volta; veja vÃ­deo (DivulgaÃ§Ã£o FC Barcelona)" title="Messi treina e estÃ¡ prÃ³ximo da volta; veja vÃ­deo (DivulgaÃ§Ã£o FC Barcelona)"
         data-original-image="s2.glbimg.com/TTAqvyv8prswFdivMs22YbiIsRE=/105x56:563x248/155x65/s.glbimg.com/es/ge/f/original/2015/11/17/messi2.jpg" data-url-smart_horizontal="-Kubk4HdhZg9_GUWHzglNX8sDpY=/90x56/smart/filters:strip_icc()/" data-url-smart="-Kubk4HdhZg9_GUWHzglNX8sDpY=/90x56/smart/filters:strip_icc()/" data-url-feature="-Kubk4HdhZg9_GUWHzglNX8sDpY=/90x56/smart/filters:strip_icc()/" data-url-tablet="F1u7Zh9jHgayeQ2_9i_XvkDrT58=/160xorig/smart/filters:strip_icc()/" data-url-desktop="7RiAL5FbzNZX9To1ppXJ_wYvPhc=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Messi treina e estÃ¡ prÃ³ximo da volta; veja vÃ­deo</h2></div></a></div></div></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-entretenimento analytics-area  analytics-id-M
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/vem-por-ai/noticia/2015/11/dante-fica-um-passo-de-desmascarar-romero.html" class="foto " title="&#39;Regra&#39;: Dante fica perto de saber suspeitas (TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/vKjhyVYFdnmA-dvRfBOFbA1m_mc=/filters:quality(10):strip_icc()/s2.glbimg.com/B3NfX19lQtT4ev7mvoaDDhfRgh0=/26x0:620x287/155x75/s.glbimg.com/og/rg/f/original/2015/11/17/dante-620.jpg" alt="&#39;Regra&#39;: Dante fica perto de saber suspeitas (TV Globo)" title="&#39;Regra&#39;: Dante fica perto de saber suspeitas (TV Globo)"
         data-original-image="s2.glbimg.com/B3NfX19lQtT4ev7mvoaDDhfRgh0=/26x0:620x287/155x75/s.glbimg.com/og/rg/f/original/2015/11/17/dante-620.jpg" data-url-smart_horizontal="tVsXJCLScJgue_IgQ-25uY8Uoyc=/90x56/smart/filters:strip_icc()/" data-url-smart="tVsXJCLScJgue_IgQ-25uY8Uoyc=/90x56/smart/filters:strip_icc()/" data-url-feature="tVsXJCLScJgue_IgQ-25uY8Uoyc=/90x56/smart/filters:strip_icc()/" data-url-tablet="wWuIgH_VV7HnRVCloGLJqeHju90=/160xorig/smart/filters:strip_icc()/" data-url-desktop="HZqbAEdlJh-AiRHnAgWDbRjF67Q=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>&#39;Regra&#39;: Dante fica perto de saber suspeitas</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Guerra Ã© desmascarado" href="http://extra.globo.com/tv-e-lazer/telinha/a-regra-do-jogo-faustini-desmascara-guerra-18063474.html">Guerra Ã© desmascarado</a></div></li><li><div class="mobile-grid-partial"><a title="Domingas expulsa Juca" href="http://oglobo.globo.com/cultura/revista-da-tv/domingas-vai-expulsar-juca-de-casa-tera-novo-amor-em-regra-do-jogo-18072484">Domingas expulsa Juca</a></div></li></ul></div><div class="direita"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/totalmente-demais/vem-por-ai/noticia/2015/11/carolina-provoca-arthur-e-da-mole-para-rafael.html" class="foto " title="Carolina dÃ¡ em cima de Rafael em &#39;Totalmente&#39; (TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/gCw5z6KGfyA7hr2sl7QHysJFbcY=/filters:quality(10):strip_icc()/s2.glbimg.com/J5Yv7EFVnMYQQptN0Qg9l_oti3Q=/7x4:658x319/155x75/s.glbimg.com/et/gs/f/original/2015/11/16/gshow242_cortada.jpg" alt="Carolina dÃ¡ em cima de Rafael em &#39;Totalmente&#39; (TV Globo)" title="Carolina dÃ¡ em cima de Rafael em &#39;Totalmente&#39; (TV Globo)"
         data-original-image="s2.glbimg.com/J5Yv7EFVnMYQQptN0Qg9l_oti3Q=/7x4:658x319/155x75/s.glbimg.com/et/gs/f/original/2015/11/16/gshow242_cortada.jpg" data-url-smart_horizontal="SeFrFinRK5iZngC-NombF2Cg8M8=/90x56/smart/filters:strip_icc()/" data-url-smart="SeFrFinRK5iZngC-NombF2Cg8M8=/90x56/smart/filters:strip_icc()/" data-url-feature="SeFrFinRK5iZngC-NombF2Cg8M8=/90x56/smart/filters:strip_icc()/" data-url-tablet="wRrTKSYHwY4lXhMkbT7pUCs2M_Y=/160xorig/smart/filters:strip_icc()/" data-url-desktop="lIOrg23IGVnLDmhTvSnS7h5_S9I=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Carolina dÃ¡ em cima de Rafael em &#39;Totalmente&#39;</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Eliza sonha com Arthur" href="http://kogut.oglobo.globo.com/noticias-da-tv/novelas/noticia/2015/11/eliza-sonhara-com-arthur-mas-acabara-beijando-jonatas.html">Eliza sonha com Arthur</a></div></li><li><div class="mobile-grid-partial"><a title="Cassandra dÃ¡ vexame" href="http://gshow.globo.com/novelas/totalmente-demais/vem-por-ai/noticia/2015/11/cassandra-causa-na-agencia-e-leva-fora-de-arthur.html">Cassandra dÃ¡ vexame</a></div></li></ul></div></div></div><div class="grid-base wide pull-left wide-inferior"></div></div><div id="area-widgets-direita-topo" class="grid-base narrow area-widgets-direta-topo analytics-area analytics-id-V"><div id="ad-position-top1" class="opec"><div id="banner_slim_topo" class="tag-manager-publicidade-container"></div></div><div class="libby-agrupador-destaque-globotv-com-canais length-5"><div class="main-area mobile-grid-full"><div class="header-area analytics-area analytics-id-header"><a class="logo-link" href="http://globotv.globo.com"><div class="logo"></div></a><h3><a class="title-link" href="http://globoplay.globo.com/video-show/p/990/">vÃ­deo show</a></h3></div><div class="hover-area"><div class="image-area"><ul><li class="active analytics-area analytics-id-1 analytics-id-I glb-hl-style-entretenimento" data-href="http://globoplay.globo.com/v/4614991/"><a href="http://globoplay.globo.com/v/4614991/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4614991.jpg" alt="Joelma cai na risada com Monica e Otaviano e imita &#39;motinho&#39;" /><div class="time">03:16</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">&#39;isso Ã© calypsooooo!&#39;</span><span class="title">Joelma cai na risada com Monica e Otaviano e imita &#39;motinho&#39;</span></div></a></li><li class=" analytics-area analytics-id-2 analytics-id-I glb-hl-style-entretenimento" data-href="http://globoplay.globo.com/v/4614799/"><a href="http://globoplay.globo.com/v/4614799/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4614799.jpg" alt="Laura Keller levanta pneu de trator em treino e diz que toma cerveja" /><div class="time">05:03</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">musa de &#39;pÃ© na cova&#39;</span><span class="title">Laura Keller levanta pneu de trator em treino e diz que toma cerveja</span></div></a></li><li class=" analytics-area analytics-id-3 analytics-id-I glb-hl-style-entretenimento" data-href="http://globoplay.globo.com/v/4614759/"><a href="http://globoplay.globo.com/v/4614759/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4614759.jpg" alt="Paolla Oliveira posta foto ao lado do pai; veja destaques dos famosos" /><div class="time">03:40</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">joaquim na bancada</span><span class="title">Paolla Oliveira posta foto ao lado do pai; veja destaques dos famosos</span></div></a></li></ul></div><div class="title-area"><ul><li class="active analytics-area analytics-id-1 analytics-id-T glb-hl-style-entretenimento"><a href="http://globoplay.globo.com/v/4614991/"><span class="subtitle">&#39;isso Ã© calypsooooo!&#39;</span><span class="title">Joelma cai na risada com Monica e Otaviano e imita &#39;motinho&#39;</span></a></li><li class=" analytics-area analytics-id-2 analytics-id-T glb-hl-style-entretenimento"><a href="http://globoplay.globo.com/v/4614799/"><span class="subtitle">musa de &#39;pÃ© na cova&#39;</span><span class="title">Laura Keller levanta pneu de trator em treino e diz que toma cerveja</span></a></li><li class=" analytics-area analytics-id-3 analytics-id-T glb-hl-style-entretenimento"><a href="http://globoplay.globo.com/v/4614759/"><span class="subtitle">joaquim na bancada</span><span class="title">Paolla Oliveira posta foto ao lado do pai; veja destaques dos famosos</span></a></li></ul></div></div><div class="controls-area"><ul class="step-marker glb-hl-style-entretenimento"><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-1"><a href="#" data-related="http://globoplay.globo.com/v/4614991/"><div class="ball active"></div></a></div></li><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-2"><a href="#" data-related="http://globoplay.globo.com/v/4614799/"><div class="ball "></div></a></div></li><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-3"><a href="#" data-related="http://globoplay.globo.com/v/4614759/"><div class="ball "></div></a></div></li></ul><ul class="navigation-buttons"><li><a href="#left"><div class="left"></div></a></li><li><a href="#right"><div class="right"></div></a></li></ul></div><div class="footer-area analytics-area analytics-id-footer"><a class="title-link" href="http://globoplay.globo.com/">mais vÃ­deos <span class="arrow"> âº</span></a></div></div></div><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded-agrupador-destaque-globotv-com-canais']);</script></div></div><div class="separator-first-scroll-container"><section><div><div id="opec-banner-middle-container" class="opec-banner-middle-container"><div id="ad-position-middle" class="opec"><div id="banner_slb_meio" class="tag-manager-publicidade-container"></div></div></div></div></section></div><div id="x62" class="opec-area opec-mobile opec-x62 grid-12"><div id="banner_mobile_meio" class="tag-manager-publicidade-container"></div></div><div id="container-columns" class="container columns clearfix glb-area-colunas"><section class="area news-column   last-has-photo
            
        
        "><div class="destaques-container closed analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/sc/santa-catarina/noticia/2015/11/homem-morre-ao-bater-em-caminhao-apos-esfaquear-irma-em-ascurra-sc.html" class="foto" title="ApÃ³s briga por heranÃ§a, homem tenta matar a irmÃ£ na rua e morre depois (PRF/ DivulgaÃ§Ã£o)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/-__KCSWpNyR9mGEtEgEs7sXOeD0=/filters:quality(10):strip_icc()/s2.glbimg.com/dKm6zzcU4tyXasU_6UlPhuq0aC4=/0x0:1600x859/335x180/s.glbimg.com/jo/g1/f/original/2015/11/17/20151117060319.jpg" alt="ApÃ³s briga por heranÃ§a, homem tenta matar a irmÃ£ na rua e morre depois (PRF/ DivulgaÃ§Ã£o)" title="ApÃ³s briga por heranÃ§a, homem tenta matar a irmÃ£ na rua e morre depois (PRF/ DivulgaÃ§Ã£o)"
                data-original-image="s2.glbimg.com/dKm6zzcU4tyXasU_6UlPhuq0aC4=/0x0:1600x859/335x180/s.glbimg.com/jo/g1/f/original/2015/11/17/20151117060319.jpg" data-url-smart_horizontal="jk3PRd1bZskYLW1znBdXOzZZ_zc=/90x0/smart/filters:strip_icc()/" data-url-smart="jk3PRd1bZskYLW1znBdXOzZZ_zc=/90x0/smart/filters:strip_icc()/" data-url-feature="jk3PRd1bZskYLW1znBdXOzZZ_zc=/90x0/smart/filters:strip_icc()/" data-url-tablet="Yw7mrlXhTwmpBuq24MGXjqKalB0=/220x125/smart/filters:strip_icc()/" data-url-desktop="n0hOUgsmz8mCaieJdI1G_74JUy8=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>ApÃ³s briga por heranÃ§a, homem tenta matar a irmÃ£ na rua e morre depois</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-17 15:42:23" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/sp/campinas-regiao/noticia/2015/11/boeing-747-e-apreendido-pela-justica-do-trabalho-ao-pousar-em-viracopos.html" class="foto" title="Empresa tem Boeing retido em aeroporto de SP por dÃ­vida com funcionÃ¡rios (Hamilton Ravani Neves/Arquivo pessoal)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/Okb_6PvhR9Qv1D4LVbiNfMVzbaU=/filters:quality(10):strip_icc()/s2.glbimg.com/hnonaUqW_UiOehVowYBuSeCq5_w=/27x0:1626x1065/120x80/s.glbimg.com/jo/g1/f/original/2015/11/17/aviaoarresto02.jpg" alt="Empresa tem Boeing retido em aeroporto de SP por dÃ­vida com funcionÃ¡rios (Hamilton Ravani Neves/Arquivo pessoal)" title="Empresa tem Boeing retido em aeroporto de SP por dÃ­vida com funcionÃ¡rios (Hamilton Ravani Neves/Arquivo pessoal)"
                data-original-image="s2.glbimg.com/hnonaUqW_UiOehVowYBuSeCq5_w=/27x0:1626x1065/120x80/s.glbimg.com/jo/g1/f/original/2015/11/17/aviaoarresto02.jpg" data-url-smart_horizontal="QJN0hHDNpxcDqbmJaqpywIyVzUg=/90x0/smart/filters:strip_icc()/" data-url-smart="QJN0hHDNpxcDqbmJaqpywIyVzUg=/90x0/smart/filters:strip_icc()/" data-url-feature="QJN0hHDNpxcDqbmJaqpywIyVzUg=/90x0/smart/filters:strip_icc()/" data-url-tablet="lZf0QkU8mC3b0lYlxTyfNmeHeK8=/70x50/smart/filters:strip_icc()/" data-url-desktop="ZRMXUsLF9hq-eQ1GCCUlIsjhBRE=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Empresa tem Boeing retido em aeroporto de SP por dÃ­vida com funcionÃ¡rios</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-17 15:15:12" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/bemestar/noticia/2015/11/ex-anorexica-hospitalizada-com-30-kg-aos-15-anos-expoe-curvas-e-faz-campanha-por-corpos-normais-no-instagram.html" class="foto" title="Internada com 30kg se recupera e posta com dobrinhas e &#39;corpo normal&#39; (Megan Crabbe/Arquivo pessoal)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/FPFe5DZHTsikN63ny-seuAorbxA=/filters:quality(10):strip_icc()/s2.glbimg.com/si239Ds1zKrnj6tc9qrcPB35jFY=/42x0:565x349/120x80/s.glbimg.com/jo/g1/f/original/2015/11/17/megancrabbebbc.jpg" alt="Internada com 30kg se recupera e posta com dobrinhas e &#39;corpo normal&#39; (Megan Crabbe/Arquivo pessoal)" title="Internada com 30kg se recupera e posta com dobrinhas e &#39;corpo normal&#39; (Megan Crabbe/Arquivo pessoal)"
                data-original-image="s2.glbimg.com/si239Ds1zKrnj6tc9qrcPB35jFY=/42x0:565x349/120x80/s.glbimg.com/jo/g1/f/original/2015/11/17/megancrabbebbc.jpg" data-url-smart_horizontal="ORzkEPqjIAAD-NAG7pP2SCqXhyg=/90x0/smart/filters:strip_icc()/" data-url-smart="ORzkEPqjIAAD-NAG7pP2SCqXhyg=/90x0/smart/filters:strip_icc()/" data-url-feature="ORzkEPqjIAAD-NAG7pP2SCqXhyg=/90x0/smart/filters:strip_icc()/" data-url-tablet="6kKm-oXecg_v_ljk2N6flzQXrcA=/70x50/smart/filters:strip_icc()/" data-url-desktop="TDBZwaJp0VmYZqKADg8JsO4kMa0=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Internada com 30kg se recupera e posta com dobrinhas e &#39;corpo normal&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/politica/noticia/2015/11/eike-diz-cpi-que-bndes-teve-zero-de-prejuizo-com-suas-empresas.html" class="" title="EmpresÃ¡rio Eike Batista diz Ã  CPI que BNDES teve &#39;zero de prejuÃ­zo&#39; com suas empresas (ReproduÃ§Ã£o/Tv CÃ¢mara)" rel="bookmark"><span class="conteudo"><p>EmpresÃ¡rio Eike Batista diz Ã  CPI que BNDES teve &#39;zero de prejuÃ­zo&#39; com suas empresas</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-17 16:02:34" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/rio-de-janeiro/noticia/2015/11/justica-decreta-prisao-de-3-suspeitos-de-espancamento-em-ipanema.html" class="foto" title="JustiÃ§a decreta prisÃ£o de 2 mulheres e 1 homem por linchamento em Ipanema (ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/nO3NhjI70tfVGRnipG5wkZR0RUs=/filters:quality(10):strip_icc()/s2.glbimg.com/eBITr0n-ByMlq-tTvoahkyeLP1o=/160x163:542x418/120x80/s.glbimg.com/jo/g1/f/original/2015/11/16/fabiano.jpg" alt="JustiÃ§a decreta prisÃ£o de 2 mulheres e 1 homem por linchamento em Ipanema (ReproduÃ§Ã£o)" title="JustiÃ§a decreta prisÃ£o de 2 mulheres e 1 homem por linchamento em Ipanema (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/eBITr0n-ByMlq-tTvoahkyeLP1o=/160x163:542x418/120x80/s.glbimg.com/jo/g1/f/original/2015/11/16/fabiano.jpg" data-url-smart_horizontal="hhD-Yoqk8CniVFdUNRysMBPDxhI=/90x0/smart/filters:strip_icc()/" data-url-smart="hhD-Yoqk8CniVFdUNRysMBPDxhI=/90x0/smart/filters:strip_icc()/" data-url-feature="hhD-Yoqk8CniVFdUNRysMBPDxhI=/90x0/smart/filters:strip_icc()/" data-url-tablet="TsjFi_H-NGS8DeCGa6Mp58_Xb0A=/70x50/smart/filters:strip_icc()/" data-url-desktop="Cf-6I1jE0z1hkk4OzUz2B_kva50=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>JustiÃ§a decreta prisÃ£o de 2 mulheres e 1 homem por linchamento em Ipanema</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-17 15:58:30" class="chamada chamada-principal mobile-grid-full"><a href="http://www.techtudo.com.br/listas/noticia/2015/11/sete-coisas-para-comprar-com-o-valor-de-um-iphone-6s-no-brasil.html" class="foto" title="iPhone no Brasil: veja tudo o que vocÃª pode comprar com o valor do novo smart (Luana Marfim/TechTudo)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/XC0UMqAf3PtMTwsdL68RwtfmzgM=/filters:quality(10):strip_icc()/s2.glbimg.com/J7ecsNRTIKDbBpupwQf8C3OKd_E=/76x45:2398x1594/120x80/s.glbimg.com/po/tt2/f/original/2015/11/13/iphone_6s_techtudo.jpg" alt="iPhone no Brasil: veja tudo o que vocÃª pode comprar com o valor do novo smart (Luana Marfim/TechTudo)" title="iPhone no Brasil: veja tudo o que vocÃª pode comprar com o valor do novo smart (Luana Marfim/TechTudo)"
                data-original-image="s2.glbimg.com/J7ecsNRTIKDbBpupwQf8C3OKd_E=/76x45:2398x1594/120x80/s.glbimg.com/po/tt2/f/original/2015/11/13/iphone_6s_techtudo.jpg" data-url-smart_horizontal="au56DPsgMpeWP7AxuyD7LI-af4E=/90x0/smart/filters:strip_icc()/" data-url-smart="au56DPsgMpeWP7AxuyD7LI-af4E=/90x0/smart/filters:strip_icc()/" data-url-feature="au56DPsgMpeWP7AxuyD7LI-af4E=/90x0/smart/filters:strip_icc()/" data-url-tablet="WV6odbzWdqdLyhLQcnenqQHeAbQ=/70x50/smart/filters:strip_icc()/" data-url-desktop="HMoEQ7yBhbz0WtJlkbjcJPHzXoE=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>iPhone no Brasil: veja tudo <br />o que vocÃª pode comprar com o valor do novo smart</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-17 13:49:53" class="chamada chamada-principal mobile-grid-full"><a href="http://revistacrescer.globo.com/Curiosidades/noticia/2015/11/o-primeiro-desenho-do-seu-filho-antes-mesmo-de-ele-sair-da-barriga.html" class="" title="MÃ£es ganham desenhos dos filhos antes deles saÃ­rem da barriga; vÃ­deo (ReproduÃ§Ã£o)" rel="bookmark"><span class="conteudo"><p>MÃ£es ganham desenhos dos filhos antes deles saÃ­rem da barriga; vÃ­deo</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/rs/rio-grande-do-sul/noticia/2015/11/casal-procurado-pela-policia-por-morte-de-fotografo-no-rs-e-indiciado.html" class="foto" title="PolÃ­cia diz ter &#39;informaÃ§Ã£o contundente&#39; sobre casal que teria matado jovem (ReproduÃ§Ã£o/Facebook)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/X5mtvWRSsJC40eY0YW4jKZRr_8Y=/filters:quality(10):strip_icc()/s2.glbimg.com/7Drkia_I-m_Tl1dsBrsvud04CgY=/107x29:294x153/120x80/s.glbimg.com/jo/g1/f/original/2015/07/28/gustavo304372_368333393246952_644655591_n.jpg" alt="PolÃ­cia diz ter &#39;informaÃ§Ã£o contundente&#39; sobre casal que teria matado jovem (ReproduÃ§Ã£o/Facebook)" title="PolÃ­cia diz ter &#39;informaÃ§Ã£o contundente&#39; sobre casal que teria matado jovem (ReproduÃ§Ã£o/Facebook)"
                data-original-image="s2.glbimg.com/7Drkia_I-m_Tl1dsBrsvud04CgY=/107x29:294x153/120x80/s.glbimg.com/jo/g1/f/original/2015/07/28/gustavo304372_368333393246952_644655591_n.jpg" data-url-smart_horizontal="ExlF47u5NOoSbOK6IC4M9b11iJo=/90x0/smart/filters:strip_icc()/" data-url-smart="ExlF47u5NOoSbOK6IC4M9b11iJo=/90x0/smart/filters:strip_icc()/" data-url-feature="ExlF47u5NOoSbOK6IC4M9b11iJo=/90x0/smart/filters:strip_icc()/" data-url-tablet="OYL3OkfD1AdDFtH_HYqDh4HXlmQ=/70x50/smart/filters:strip_icc()/" data-url-desktop="KLt1mPDVEVXo0OmqrCueZrR9Pso=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>PolÃ­cia diz ter &#39;informaÃ§Ã£o contundente&#39; sobre casal que teria matado jovem</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-17 13:07:01" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/11/charlie-sheen-revela-na-tv-que-e-portador-do-virus-hiv.html" class="foto" title="Charlie Sheen revela que Ã© HIV positivo e que jÃ¡ pagou pessoas por segredo; vÃ­deo (ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/30-gEg4iz4RD5ALq4A0u12jHG8I=/filters:quality(10):strip_icc()/s2.glbimg.com/pE7WOHg1_q4T7_Qd7Ea47UTyWvA=/169x66:485x277/120x80/e.glbimg.com/og/ed/f/original/2015/11/17/charlie.png" alt="Charlie Sheen revela que Ã© HIV positivo e que jÃ¡ pagou pessoas por segredo; vÃ­deo (ReproduÃ§Ã£o)" title="Charlie Sheen revela que Ã© HIV positivo e que jÃ¡ pagou pessoas por segredo; vÃ­deo (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/pE7WOHg1_q4T7_Qd7Ea47UTyWvA=/169x66:485x277/120x80/e.glbimg.com/og/ed/f/original/2015/11/17/charlie.png" data-url-smart_horizontal="ykIukX9BqX4j7JdwbkTz6mXuzUk=/90x0/smart/filters:strip_icc()/" data-url-smart="ykIukX9BqX4j7JdwbkTz6mXuzUk=/90x0/smart/filters:strip_icc()/" data-url-feature="ykIukX9BqX4j7JdwbkTz6mXuzUk=/90x0/smart/filters:strip_icc()/" data-url-tablet="_Tw6Zjlms7hetUN2GHJnCZ6Adb8=/70x50/smart/filters:strip_icc()/" data-url-desktop="Jyfja-gisOpdTcBBXIH4yMEPXOQ=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Charlie Sheen revela que Ã© HIV positivo e que jÃ¡ pagou pessoas por segredo; vÃ­deo</p></span></a></div></div><div class="show-more"><span class="setinha-show-more"></span></div></div><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-noticias
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}});}
</script><div id="libby-home-widget-economia" class="libby-home-widgetBase libby-home-widget-economia analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais notÃ­cias" href="http://g1.globo.com/"><span class="text">mais notÃ­cias</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area sports-column   last-has-photo
            
        
        "><div class="destaques-container closed analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://sportv.globo.com/site/combate/blogs/especial-blog/ultimmato/post/amparada-pelo-namorado-ronda-esconde-o-rosto-apos-ser-nocauteada-por-holm.html" class="foto" title="Ronda chega aos EUA com namorado e esconde o rosto a todo custo; assista (reproduÃ§Ã£o)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/Mlx2E41c1XEqPSNhw3_7eDim2tU=/filters:quality(10):strip_icc()/s2.glbimg.com/ksiVNHLx0J5fzWMZGs3p8D0AxoE=/0x0:335x180/335x180/s.glbimg.com/en/ho/f/original/2015/11/17/ronda.jpg" alt="Ronda chega aos EUA com namorado e esconde o rosto a todo custo; assista (reproduÃ§Ã£o)" title="Ronda chega aos EUA com namorado e esconde o rosto a todo custo; assista (reproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/ksiVNHLx0J5fzWMZGs3p8D0AxoE=/0x0:335x180/335x180/s.glbimg.com/en/ho/f/original/2015/11/17/ronda.jpg" data-url-smart_horizontal="Fo7X8SznCCljJLtkgcjJuzv1Zyg=/90x0/smart/filters:strip_icc()/" data-url-smart="Fo7X8SznCCljJLtkgcjJuzv1Zyg=/90x0/smart/filters:strip_icc()/" data-url-feature="Fo7X8SznCCljJLtkgcjJuzv1Zyg=/90x0/smart/filters:strip_icc()/" data-url-tablet="1mwMnL-wMqdpYGg3hxz5zyIgnWY=/220x125/smart/filters:strip_icc()/" data-url-desktop="EJqKS-zYv_N9myE_3swQW-g-9OQ=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Ronda chega aos EUA com namorado e esconde o rosto a todo custo; assista</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-17 13:11:51" class="chamada chamada-principal mobile-grid-full"><a href="http://sportv.globo.com/site/combate/noticia/2015/11/holly-holm-parem-de-falar-mal-da-ronda-ela-e-uma-lenda-do-esporte.html" class="foto" title="Holm diz se sentir mal e pede respeito a Ronda: &#39;Parem de falar mal dela&#39; (AP)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/CdsxPUBC0jb8WlipE_JAtwkf2mA=/filters:quality(10):strip_icc()/s2.glbimg.com/aakARcRou0Q1sAnH5xK6Mw8dce0=/710x822:2329x1901/120x80/s.glbimg.com/es/ge/f/original/2015/11/15/holm_x_ronda.jpg" alt="Holm diz se sentir mal e pede respeito a Ronda: &#39;Parem de falar mal dela&#39; (AP)" title="Holm diz se sentir mal e pede respeito a Ronda: &#39;Parem de falar mal dela&#39; (AP)"
                data-original-image="s2.glbimg.com/aakARcRou0Q1sAnH5xK6Mw8dce0=/710x822:2329x1901/120x80/s.glbimg.com/es/ge/f/original/2015/11/15/holm_x_ronda.jpg" data-url-smart_horizontal="HKcgPvA10XLl_QJ90Zk0Xz4hnN0=/90x0/smart/filters:strip_icc()/" data-url-smart="HKcgPvA10XLl_QJ90Zk0Xz4hnN0=/90x0/smart/filters:strip_icc()/" data-url-feature="HKcgPvA10XLl_QJ90Zk0Xz4hnN0=/90x0/smart/filters:strip_icc()/" data-url-tablet="eH7ebC5_-eLOTxuHTd5yH19NfXU=/70x50/smart/filters:strip_icc()/" data-url-desktop="j4MiXedOZuJNGs3JvGSqHlxOCuU=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Holm diz se sentir mal e pede respeito a Ronda: &#39;Parem de falar mal dela&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-17 14:43:53" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/esporte/lutas/brasileira-ja-disputou-cinturao-com-holly-holm-aguentou-20-minutos-antes-de-levar-chute-nao-precisei-de-plastica-so-gelo-na-boca-18070412.html" class="foto" title="Brasileira Ã© rival que aguentou mais contra Holm: &#39;E nÃ£o fiz plÃ¡stica&#39; (ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/j_7CkVpCdjyu-ZHXHBXINUg8tIQ=/filters:quality(10):strip_icc()/s2.glbimg.com/McraWHakaFIbULV3U7wtsf7m8PQ=/0x0:448x298/120x80/s.glbimg.com/en/ho/f/original/2015/11/17/holly-holm-brasileira-julie-mma_1.jpg" alt="Brasileira Ã© rival que aguentou mais contra Holm: &#39;E nÃ£o fiz plÃ¡stica&#39; (ReproduÃ§Ã£o)" title="Brasileira Ã© rival que aguentou mais contra Holm: &#39;E nÃ£o fiz plÃ¡stica&#39; (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/McraWHakaFIbULV3U7wtsf7m8PQ=/0x0:448x298/120x80/s.glbimg.com/en/ho/f/original/2015/11/17/holly-holm-brasileira-julie-mma_1.jpg" data-url-smart_horizontal="cXGlCUtYPC7tuwIqNATNZyFN5r8=/90x0/smart/filters:strip_icc()/" data-url-smart="cXGlCUtYPC7tuwIqNATNZyFN5r8=/90x0/smart/filters:strip_icc()/" data-url-feature="cXGlCUtYPC7tuwIqNATNZyFN5r8=/90x0/smart/filters:strip_icc()/" data-url-tablet="KoLYvfSsav7PJlTWoWkhmLOeYKQ=/70x50/smart/filters:strip_icc()/" data-url-desktop="t8lL1EHExNntak1DxpQy-BIv_E8=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Brasileira Ã© rival que aguentou mais contra Holm: &#39;E nÃ£o fiz plÃ¡stica&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-17 08:24:40" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/cruzeiro/noticia/2015/11/cruzeiro-vai-enxugar-grupo-e-ja-sabe-quem-fica-e-quem-deixa-toca-em-2016.html" class="" title="Cruzeiro vai enxugar grupo e jÃ¡ sabe quem fica e quem deixa Toca no ano que vem (Washington Alves/Light Press)" rel="bookmark"><span class="conteudo"><p>Cruzeiro vai enxugar grupo e jÃ¡ sabe quem fica e quem deixa Toca no ano que vem</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/sp/santos-e-regiao/blogs/especial-blog/blog-da-tri/post/garoto-marca-golaco-em-goleada-do-santos-diante-do-corinthians-veja-video.html" class="foto" title="Sub-13 do Santos faz 4 a 0 no TimÃ£o com direito a golaÃ§o; assista aqui (Pedro Ernesto Guerra Azevedo/Santos FC)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/fOL-goExPK5lEW7VL3uABIDAURc=/filters:quality(10):strip_icc()/s2.glbimg.com/v2pZXkf7TB3tvcPGoZ94H_GF7aI=/0x0:1198x799/120x80/s.glbimg.com/es/ge/f/original/2015/11/17/22626484517_9e17d516c4_o.jpg" alt="Sub-13 do Santos faz 4 a 0 no TimÃ£o com direito a golaÃ§o; assista aqui (Pedro Ernesto Guerra Azevedo/Santos FC)" title="Sub-13 do Santos faz 4 a 0 no TimÃ£o com direito a golaÃ§o; assista aqui (Pedro Ernesto Guerra Azevedo/Santos FC)"
                data-original-image="s2.glbimg.com/v2pZXkf7TB3tvcPGoZ94H_GF7aI=/0x0:1198x799/120x80/s.glbimg.com/es/ge/f/original/2015/11/17/22626484517_9e17d516c4_o.jpg" data-url-smart_horizontal="3z8SX4lk5RvBRGYLjOO6pw3tGp0=/90x0/smart/filters:strip_icc()/" data-url-smart="3z8SX4lk5RvBRGYLjOO6pw3tGp0=/90x0/smart/filters:strip_icc()/" data-url-feature="3z8SX4lk5RvBRGYLjOO6pw3tGp0=/90x0/smart/filters:strip_icc()/" data-url-tablet="DuIHRKIUue6qKsdOiMd2WNCv2Zk=/70x50/smart/filters:strip_icc()/" data-url-desktop="xotuzpbiSoI7EOj76XKW_H0jvA4=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Sub-13 do Santos faz 4 a 0 no TimÃ£o com direito a golaÃ§o; assista aqui</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-17 16:04:41" class="chamada chamada-principal mobile-grid-full"><a href="http://sportv.globo.com/site/nba/noticia/2015/11/de-novo-huertas-armador-sofre-outro-drible-desconcertante-em-reves-do-la.html" class="foto" title="Huertas volta a levar drible desconcertante e &#39;fica no vÃ¡cuo&#39;; assista aqui (Reuters)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/GbAQIMud0rkVw0Yxvu8U5lZCuCw=/filters:quality(10):strip_icc()/s2.glbimg.com/625DSWaRnXWxm19_dOXvbpPbj9E=/803x173:1887x896/120x80/s.glbimg.com/es/ge/f/original/2015/11/17/2015-11-17t033409z_3486288_nocid_rtrmadp_3_nba-los-angeles-lakers-at-phoenix-suns_1.jpg" alt="Huertas volta a levar drible desconcertante e &#39;fica no vÃ¡cuo&#39;; assista aqui (Reuters)" title="Huertas volta a levar drible desconcertante e &#39;fica no vÃ¡cuo&#39;; assista aqui (Reuters)"
                data-original-image="s2.glbimg.com/625DSWaRnXWxm19_dOXvbpPbj9E=/803x173:1887x896/120x80/s.glbimg.com/es/ge/f/original/2015/11/17/2015-11-17t033409z_3486288_nocid_rtrmadp_3_nba-los-angeles-lakers-at-phoenix-suns_1.jpg" data-url-smart_horizontal="8mcTauwmBPiqDdBqg-S0MsWnrnk=/90x0/smart/filters:strip_icc()/" data-url-smart="8mcTauwmBPiqDdBqg-S0MsWnrnk=/90x0/smart/filters:strip_icc()/" data-url-feature="8mcTauwmBPiqDdBqg-S0MsWnrnk=/90x0/smart/filters:strip_icc()/" data-url-tablet="gJk04rAjPHTtYbS54lduxPJFnbI=/70x50/smart/filters:strip_icc()/" data-url-desktop="zgsrgiyzwkTVEk84hKVPE5HSS9Y=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Huertas volta a levar drible desconcertante e &#39;fica no vÃ¡cuo&#39;; assista aqui</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-17 15:21:05" class="chamada chamada-principal mobile-grid-full"><a href="http://sportv.globo.com/site/programas/bem-amigos/noticia/2015/11/junior-diz-que-nao-se-sentiu-ofendido-por-daniel-alves-e-galvao-critica-lateral.html" class="" title="JÃºnior afirma que nÃ£o se sentiu ofendido por Daniel Alves, e GalvÃ£o critica lateral (ReproduÃ§Ã£o)" rel="bookmark"><span class="conteudo"><p>JÃºnior afirma que nÃ£o se sentiu ofendido <br />por Daniel Alves, e GalvÃ£o critica lateral</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/esporte/flamengo/flamengo-aumenta-lista-de-alvos-com-alex-muralha-bruno-mas-precos-assustam-18067209.html" class="foto" title="Fla pÃµe Muralha e Bruno na mira, e se aproxima de acerto com zagueiro Juan (Luiz Henrique/Figueirense FC)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/Ka_cFwW_QiJdZ7PR54Kl5tA63yc=/filters:quality(10):strip_icc()/s2.glbimg.com/hlFkVE93e4UNgY4Bff4FFFqfkS4=/585x142:1449x717/120x80/s.glbimg.com/es/ge/f/original/2015/11/13/22955894402_16a8144fbb_o.jpg" alt="Fla pÃµe Muralha e Bruno na mira, e se aproxima de acerto com zagueiro Juan (Luiz Henrique/Figueirense FC)" title="Fla pÃµe Muralha e Bruno na mira, e se aproxima de acerto com zagueiro Juan (Luiz Henrique/Figueirense FC)"
                data-original-image="s2.glbimg.com/hlFkVE93e4UNgY4Bff4FFFqfkS4=/585x142:1449x717/120x80/s.glbimg.com/es/ge/f/original/2015/11/13/22955894402_16a8144fbb_o.jpg" data-url-smart_horizontal="4NtdLZfoY-xNk77WuygXb-6AZqw=/90x0/smart/filters:strip_icc()/" data-url-smart="4NtdLZfoY-xNk77WuygXb-6AZqw=/90x0/smart/filters:strip_icc()/" data-url-feature="4NtdLZfoY-xNk77WuygXb-6AZqw=/90x0/smart/filters:strip_icc()/" data-url-tablet="1X-ax0wSq8U3c0TEcIuUrEGC9jo=/70x50/smart/filters:strip_icc()/" data-url-desktop="ALbr4pPeEVQqchEKqXpa2MITWZ8=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Fla pÃµe Muralha e Bruno na mira, e se aproxima de acerto com zagueiro Juan</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-17 11:05:21" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/esporte/hazard-matic-oscar-diego-costa-sao-as-macas-podres-do-chelsea-na-mira-de-mourinho-diz-jornal-18068355.html" class="foto" title="Oscar estaria entre o quarteto &#39;maÃ§Ã£ podre&#39; do Chelsea alvo de Mourinho (Reuters)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/8Nxg1mCcj0ztk5kBpUX5ROdQhcM=/filters:quality(10):strip_icc()/s2.glbimg.com/YFYApXFei85CUnmXYjjgbVflbXg=/1298x442:3069x1624/120x80/s.glbimg.com/es/ge/f/original/2015/08/08/2015-08-08t172743z_82056463_mt1aci13945308_rtrmadp_3_soc.jpg" alt="Oscar estaria entre o quarteto &#39;maÃ§Ã£ podre&#39; do Chelsea alvo de Mourinho (Reuters)" title="Oscar estaria entre o quarteto &#39;maÃ§Ã£ podre&#39; do Chelsea alvo de Mourinho (Reuters)"
                data-original-image="s2.glbimg.com/YFYApXFei85CUnmXYjjgbVflbXg=/1298x442:3069x1624/120x80/s.glbimg.com/es/ge/f/original/2015/08/08/2015-08-08t172743z_82056463_mt1aci13945308_rtrmadp_3_soc.jpg" data-url-smart_horizontal="VyrQdIIlftWYkBqHZQgmKjXBev0=/90x0/smart/filters:strip_icc()/" data-url-smart="VyrQdIIlftWYkBqHZQgmKjXBev0=/90x0/smart/filters:strip_icc()/" data-url-feature="VyrQdIIlftWYkBqHZQgmKjXBev0=/90x0/smart/filters:strip_icc()/" data-url-tablet="Kz-nimprqYlFQA_g4aq2oII2lMg=/70x50/smart/filters:strip_icc()/" data-url-desktop="GZobmOPHhIPk4KXMhIas_tqWIyk=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Oscar estaria entre o quarteto &#39;maÃ§Ã£ podre&#39; do Chelsea alvo de Mourinho</p></span></a></div></div><div class="show-more"><span class="setinha-show-more"></span></div></div><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-times soccer-teams
"></div><div id="libby-home-widget-brasileirao" class="libby-home-widgetBase libby-home-widget-brasileirao analytics-area analytics-id-W"><input type="hidden" name="rodada_selecionada" value="current"></div><a class="link-see-more analytics-area analytics-id-L" title="mais esportes" href="http://globoesporte.globo.com/"><span class="text">mais esportes</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area etc-column analytics-area   last-has-photo
            
        
        "><div class="destaques-container closed analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://revistaquem.globo.com/QUEM-News/noticia/2015/11/deborah-secco-posa-gravida-na-praia-e-diz-minha-filha-vem-para-me-tornar-melhor.html" class="foto" title="Secco posa de biquÃ­ni e fala da atual relaÃ§Ã£o: &#39;NÃ£o aguentava mais sofrer&#39; (Tristana Pssaglia e Karla Passaglia)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/Mkr8FYql50m1RxaTwcTlbUKTnKc=/filters:quality(10):strip_icc()/s2.glbimg.com/yDLuTLseM_2Vv43tTflcwG2fJBE=/102x5:437x185/335x180/e.glbimg.com/og/ed/f/original/2015/11/17/deborah.jpg" alt="Secco posa de biquÃ­ni e fala da atual relaÃ§Ã£o: &#39;NÃ£o aguentava mais sofrer&#39; (Tristana Pssaglia e Karla Passaglia)" title="Secco posa de biquÃ­ni e fala da atual relaÃ§Ã£o: &#39;NÃ£o aguentava mais sofrer&#39; (Tristana Pssaglia e Karla Passaglia)"
                data-original-image="s2.glbimg.com/yDLuTLseM_2Vv43tTflcwG2fJBE=/102x5:437x185/335x180/e.glbimg.com/og/ed/f/original/2015/11/17/deborah.jpg" data-url-smart_horizontal="i1m4phKYzVX6vN7HjDHYCGN5Mlc=/90x0/smart/filters:strip_icc()/" data-url-smart="i1m4phKYzVX6vN7HjDHYCGN5Mlc=/90x0/smart/filters:strip_icc()/" data-url-feature="i1m4phKYzVX6vN7HjDHYCGN5Mlc=/90x0/smart/filters:strip_icc()/" data-url-tablet="s26lTfRV-ONdCEETtbfqY94ccIc=/220x125/smart/filters:strip_icc()/" data-url-desktop="f8eAwUjn8eCx3dHsPGjzYKp12HE=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Secco posa de biquÃ­ni e fala da atual relaÃ§Ã£o: &#39;NÃ£o aguentava mais sofrer&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-17 15:07:09" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/11/fernanda-souza-usa-microshort-em-passeio-com-thiaguinho.html" class="foto" title="FÃª Souza pÃµe shortinho e bolsa de R$ 13 mil para passeio com Thiaguinho (Fabio Moreno / AgNews)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/J2cgW6NZrFD9S5hDJpMqvtJFDKk=/filters:quality(10):strip_icc()/s2.glbimg.com/FSHtGeZLolhvpOvWP680a2nlrKE=/401x405:721x618/120x80/s.glbimg.com/jo/eg/f/original/2015/11/17/img_6601.jpg" alt="FÃª Souza pÃµe shortinho e bolsa de R$ 13 mil para passeio com Thiaguinho (Fabio Moreno / AgNews)" title="FÃª Souza pÃµe shortinho e bolsa de R$ 13 mil para passeio com Thiaguinho (Fabio Moreno / AgNews)"
                data-original-image="s2.glbimg.com/FSHtGeZLolhvpOvWP680a2nlrKE=/401x405:721x618/120x80/s.glbimg.com/jo/eg/f/original/2015/11/17/img_6601.jpg" data-url-smart_horizontal="tX_lhpWGh6YOnG3M1V1WTSKtEs8=/90x0/smart/filters:strip_icc()/" data-url-smart="tX_lhpWGh6YOnG3M1V1WTSKtEs8=/90x0/smart/filters:strip_icc()/" data-url-feature="tX_lhpWGh6YOnG3M1V1WTSKtEs8=/90x0/smart/filters:strip_icc()/" data-url-tablet="JrwigoH9NT3aYpd05UXlkZwOgQk=/70x50/smart/filters:strip_icc()/" data-url-desktop="IRGGF8W9Uo1aqrvaXglRyv0jZdw=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>FÃª Souza pÃµe shortinho e bolsa de R$ 13 mil para passeio com Thiaguinho</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/moda/noticia/2015/11/luiza-possi-camila-pitanga-e-outras-dao-show-de-estilo-em-aeroporto-do-rio.html" class="foto" title="ColÃ­rio: Luiza Possi, Brunet, Pitanga e Ju Alves viajam de Ã³culos escuros; looks (William Oda/AGnews)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/eOq_2Lnx9n0oZEszuGOuLmCDt5g=/filters:quality(10):strip_icc()/s2.glbimg.com/QHdNbAELPQBUzM265exUqN0UfiA=/336x229:637x430/120x80/s.glbimg.com/jo/eg/f/original/2015/11/17/dsc_4665.jpg" alt="ColÃ­rio: Luiza Possi, Brunet, Pitanga e Ju Alves viajam de Ã³culos escuros; looks (William Oda/AGnews)" title="ColÃ­rio: Luiza Possi, Brunet, Pitanga e Ju Alves viajam de Ã³culos escuros; looks (William Oda/AGnews)"
                data-original-image="s2.glbimg.com/QHdNbAELPQBUzM265exUqN0UfiA=/336x229:637x430/120x80/s.glbimg.com/jo/eg/f/original/2015/11/17/dsc_4665.jpg" data-url-smart_horizontal="3LeQv7MVw8OEOqthCApuFBvCoGU=/90x0/smart/filters:strip_icc()/" data-url-smart="3LeQv7MVw8OEOqthCApuFBvCoGU=/90x0/smart/filters:strip_icc()/" data-url-feature="3LeQv7MVw8OEOqthCApuFBvCoGU=/90x0/smart/filters:strip_icc()/" data-url-tablet="v5UXLCAGKSjcugbTPfPGptw3UZE=/70x50/smart/filters:strip_icc()/" data-url-desktop="LoZL0rVLa6wQ-Vee9uEUWCaajB8=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>ColÃ­rio: Luiza Possi, Brunet, Pitanga e Ju Alves viajam de Ã³culos escuros; looks</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/11/felipe-franco-mostra-corpo-todo-musculoso-e-impressiona-monstro.html" class="" title="Sem calÃ§a, marido de Juju Salimeni admira o prÃ³prio corpo supermusculoso; foto (ReproduÃ§Ã£o/Instagram)" rel="bookmark"><span class="conteudo"><p>Sem calÃ§a, marido de Juju Salimeni admira <br />o prÃ³prio corpo supermusculoso; foto</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-17 13:39:35" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/famosos/mae-de-quatro-simony-posa-de-biquini-rebate-critica-muito-feliz-com-meu-corpo-18071114.html" class="foto" title="MÃ£e de 4, Simony posa de biquÃ­ni e rebate crÃ­ticas: &#39;Muito feliz com o corpo&#39; (globo.com)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/HIXriJDjrZOE5Or1Ns7tCuz74D0=/filters:quality(10):strip_icc()/s2.glbimg.com/CWlhHyXVhW9dNEbGsjHXZpYu1Eo=/0x161:448x460/120x80/s.glbimg.com/en/ho/f/original/2015/11/17/mony.jpg" alt="MÃ£e de 4, Simony posa de biquÃ­ni e rebate crÃ­ticas: &#39;Muito feliz com o corpo&#39; (globo.com)" title="MÃ£e de 4, Simony posa de biquÃ­ni e rebate crÃ­ticas: &#39;Muito feliz com o corpo&#39; (globo.com)"
                data-original-image="s2.glbimg.com/CWlhHyXVhW9dNEbGsjHXZpYu1Eo=/0x161:448x460/120x80/s.glbimg.com/en/ho/f/original/2015/11/17/mony.jpg" data-url-smart_horizontal="22Pnkik8og0eCwiv-ZgqClyBBvM=/90x0/smart/filters:strip_icc()/" data-url-smart="22Pnkik8og0eCwiv-ZgqClyBBvM=/90x0/smart/filters:strip_icc()/" data-url-feature="22Pnkik8og0eCwiv-ZgqClyBBvM=/90x0/smart/filters:strip_icc()/" data-url-tablet="CeXPrHa3oH6Ybl00qkLcWRFCJc4=/70x50/smart/filters:strip_icc()/" data-url-desktop="_cRea5uyzPKyEAcRhb2x4RVhl0Q=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>MÃ£e de 4, Simony posa de biquÃ­ni e rebate crÃ­ticas: &#39;Muito feliz com o corpo&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-17 15:07:09" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/famosos/a-cara-da-mae-sandy-o-filho-theo-viram-sensacao-em-embarque-no-rio-18067321.html" class="foto" title="Filho de Sandy Ã© sensaÃ§Ã£o no Rio; compare com a mÃ£e na mesma idade (Extra)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/ohTR2L1Mx8oiUwEtA9Qb5UTkwBY=/filters:quality(10):strip_icc()/s2.glbimg.com/j5DjwQRlral17Xc3AznZpJiHNHc=/138x59:304x170/120x80/s.glbimg.com/en/ho/f/original/2015/11/17/sandy.jpg" alt="Filho de Sandy Ã© sensaÃ§Ã£o no Rio; compare com a mÃ£e na mesma idade (Extra)" title="Filho de Sandy Ã© sensaÃ§Ã£o no Rio; compare com a mÃ£e na mesma idade (Extra)"
                data-original-image="s2.glbimg.com/j5DjwQRlral17Xc3AznZpJiHNHc=/138x59:304x170/120x80/s.glbimg.com/en/ho/f/original/2015/11/17/sandy.jpg" data-url-smart_horizontal="alcZ6LwJmZ26W3WqdvjPFCF921g=/90x0/smart/filters:strip_icc()/" data-url-smart="alcZ6LwJmZ26W3WqdvjPFCF921g=/90x0/smart/filters:strip_icc()/" data-url-feature="alcZ6LwJmZ26W3WqdvjPFCF921g=/90x0/smart/filters:strip_icc()/" data-url-tablet="98Dv6K7YbnwCPI-qncknj89biGc=/70x50/smart/filters:strip_icc()/" data-url-desktop="MoR8Sjq-Dh_kXoZ3Vu69Z_e6o8g=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Filho de Sandy Ã© sensaÃ§Ã£o no Rio; compare com a mÃ£e na mesma idade</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-17 09:57:26" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/famosos/miss-rio-de-janeiro-nathalia-pinheiro-afina-cintura-para-tentar-ganhar-concurso-nacional-18066857.html" class="" title="Miss Rio de Janeiro afina cintura para tentar ganhar concurso nacional e exibe o corpaÃ§o (Johnson Parraguez / Photo Rionews)" rel="bookmark"><span class="conteudo"><p>Miss Rio de Janeiro afina cintura para tentar ganhar concurso nacional e exibe o corpaÃ§o</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-11-17 12:56:44" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/paparazzo/noticia/2015/11/janaina-santucci-sobre-sexo-faco-todos-os-dias.html" class="foto" title="Sem nada! JanaÃ­na Santucci mostra o bronzeado ao posar nua para &#39;Paparazzo&#39; (Roberto Teixeira / Paparazzo)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/J9xAJksG5hCkSg3JKcfDg6H4cDA=/filters:quality(10):strip_icc()/s2.glbimg.com/2EGlnHsW0wmve0y7aCog4p1g9hA=/45x36:209x145/120x80/s.glbimg.com/jo/eg/f/original/2015/11/04/01ynb.jpg" alt="Sem nada! JanaÃ­na Santucci mostra o bronzeado ao posar nua para &#39;Paparazzo&#39; (Roberto Teixeira / Paparazzo)" title="Sem nada! JanaÃ­na Santucci mostra o bronzeado ao posar nua para &#39;Paparazzo&#39; (Roberto Teixeira / Paparazzo)"
                data-original-image="s2.glbimg.com/2EGlnHsW0wmve0y7aCog4p1g9hA=/45x36:209x145/120x80/s.glbimg.com/jo/eg/f/original/2015/11/04/01ynb.jpg" data-url-smart_horizontal="XUvkXrHrCemAJg-WilZl9iDqsw0=/90x0/smart/filters:strip_icc()/" data-url-smart="XUvkXrHrCemAJg-WilZl9iDqsw0=/90x0/smart/filters:strip_icc()/" data-url-feature="XUvkXrHrCemAJg-WilZl9iDqsw0=/90x0/smart/filters:strip_icc()/" data-url-tablet="h9QRcObVaDx4nbc-6HDtajyhA3M=/70x50/smart/filters:strip_icc()/" data-url-desktop="Ct-B74wlk0QEyUh3smmmK36T6CY=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Sem nada! JanaÃ­na Santucci mostra o bronzeado ao posar nua para &#39;Paparazzo&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry publieditorial"><div data-photo-subtitle="2015-11-16 23:45:35" class="chamada chamada-principal mobile-grid-full"><a href="http://gshow.globo.com/novelas/totalmente-demais/ep/australia/noticia/2015/11/um-sonho-cenas-de-totalmente-demais-ne-inspire-se-e-embarque-para-esse-lugar.html?utm_source=home-globocom&amp;utm_medium=fake-banner&amp;utm_term=15-11-17&amp;utm_content=australia&amp;utm_campaign=australia" class="foto" title="Inspire-se nas cenas de &#39;Totalmente Demais&#39; e embarque para a AustrÃ¡lia (reproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/pvmv4KoCGPMz5cg9RrSY9Srklh8=/filters:quality(10):strip_icc()/s2.glbimg.com/sSGNb09tJ7UB98-HSZ1enISd1oY=/0x0:120x80/120x80/s.glbimg.com/en/ho/f/original/2015/11/16/fake-australia.jpg" alt="Inspire-se nas cenas de &#39;Totalmente Demais&#39; e embarque para a AustrÃ¡lia (reproduÃ§Ã£o)" title="Inspire-se nas cenas de &#39;Totalmente Demais&#39; e embarque para a AustrÃ¡lia (reproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/sSGNb09tJ7UB98-HSZ1enISd1oY=/0x0:120x80/120x80/s.glbimg.com/en/ho/f/original/2015/11/16/fake-australia.jpg" data-url-smart_horizontal="ek_q6fBLKD37861VInPV9llfdHs=/90x0/smart/filters:strip_icc()/" data-url-smart="ek_q6fBLKD37861VInPV9llfdHs=/90x0/smart/filters:strip_icc()/" data-url-feature="ek_q6fBLKD37861VInPV9llfdHs=/90x0/smart/filters:strip_icc()/" data-url-tablet="80z__nF2l3HG4urAkEExt0EtiOA=/70x50/smart/filters:strip_icc()/" data-url-desktop="_oi2beCQqHuejKnSbkTtmxFRsUI=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><span>ESPECIAL PUBLICITÃRIO</span><p>Inspire-se nas cenas de &#39;Totalmente Demais&#39; e embarque para a AustrÃ¡lia</p></span></a></div></div><div class="show-more"><span class="setinha-show-more"></span></div></div><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-novelas
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2015/", "logo": "http://s2.glbimg.com/tjsQl1SMvddbD-BGiSoC2Nb6kqg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-36x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2015/rss/", "logo_tv": "http://s2.glbimg.com/y8thjGY4IQuSvc4VfunyXi0d8C4=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/FRxoFZaNszGUmyoDZALOiIC_SOY=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/10/22/alem_do_tempo_segunda_fase-36x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/3Z-Sw1ITHzu4-EEfXG-zRbCs_dY=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/10/22/alem_g_45x30_1.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "Totalmente Demais", "url": "http://gshow.globo.com/novelas/totalmente-demais/", "logo": "http://s2.glbimg.com/R8T_N-bSmq23FCuZwf27cTTmjzg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/11/06/logo_36x20.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/totalmente-demais/rss/", "logo_tv": "http://s2.glbimg.com/DOKGjWF1k80-ujyw_Q-OT1IX65M=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/11/06/logo_45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "A Regra do Jogo", "url": "http://gshow.globo.com/novelas/a-regra-do-jogo/", "logo": "http://s2.glbimg.com/K-Nvvvm7zp2F7zS8TcY6TkIcxoE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-36x20.jpg", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/a-regra-do-jogo/rss/", "logo_tv": "http://s2.glbimg.com/4B7Jk0R6K1HlAAhuZIrDOJp8hbs=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-45x30.jpg"}]);}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2015/", "logo": "http://s2.glbimg.com/tjsQl1SMvddbD-BGiSoC2Nb6kqg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-36x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2015/rss/", "logo_tv": "http://s2.glbimg.com/y8thjGY4IQuSvc4VfunyXi0d8C4=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/FRxoFZaNszGUmyoDZALOiIC_SOY=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/10/22/alem_do_tempo_segunda_fase-36x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/3Z-Sw1ITHzu4-EEfXG-zRbCs_dY=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/10/22/alem_g_45x30_1.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "Totalmente Demais", "url": "http://gshow.globo.com/novelas/totalmente-demais/", "logo": "http://s2.glbimg.com/R8T_N-bSmq23FCuZwf27cTTmjzg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/11/06/logo_36x20.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/totalmente-demais/rss/", "logo_tv": "http://s2.glbimg.com/DOKGjWF1k80-ujyw_Q-OT1IX65M=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/11/06/logo_45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "A Regra do Jogo", "url": "http://gshow.globo.com/novelas/a-regra-do-jogo/", "logo": "http://s2.glbimg.com/K-Nvvvm7zp2F7zS8TcY6TkIcxoE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-36x20.jpg", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/a-regra-do-jogo/rss/", "logo_tv": "http://s2.glbimg.com/4B7Jk0R6K1HlAAhuZIrDOJp8hbs=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-45x30.jpg"}]);}});}
</script><div id="libby-home-widget-horoscopo" class="libby-home-widgetBase libby-home-widget-horoscopo analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais entretenimento" href="http://variedades.globo.com/"><span class="text">mais variedades</span><span class="arrow">&rsaquo;</span></a></div></section></div><div id="ad-position-middle3" class="opec"><div id="banner_slb_fim" class="tag-manager-publicidade-container"></div></div><div class="container columns clearfix glb-area-colunas second-area"><section class="area news-column"></section><section class="area sports-column"></section><section class="area etc-column analytics-area"></section></div><section class="area central container analytics-area analytics-id-O"><div class="separator"></div><section class="agrupador-quadruplo-tecnologia analytics-area analytics-id-T clearfix franja-inferior tecnologia"><div class="cabecalho"><h2><a href="http://www.techtudo.com.br/" title="TECNOLOGIA &amp; GAMES"><span class="word word-0">TECNOLOGIA</span><span class="word word-1">&</span><span class="word word-2">GAMES</span></a></h2><div id="ad-position-x34" class="opec"><div id="banner_selo4" class="tag-manager-publicidade-container"></div></div><aside class="links analytics-area analytics-id-T"><ul><li><a href="http://www.techtudo.com.br/jogos/" title="jogos">jogos</a></li><li><a href="http://www.techtudo.com.br/downloads/" title="downloads">downloads</a></li><li><a href="http://forum.techtudo.com.br/" title="fÃ³rum">fÃ³rum</a></li><li><a href="http://www.techtudo.com.br/mobile/" title="celulares">celulares</a></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaque"><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/review/star-wars-battlefront.html" title="Star Wars Battlefront traz grÃ¡fico incrÃ­vel e Ã© fiel aos filmes; confira"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/er_izSUJjQscc6yEVooLBesbU10=/filters:quality(10):strip_icc()/s2.glbimg.com/9TuT29N0GjEv_mubyiQ2aNa7HZA=/1x3:694x371/245x130/s.glbimg.com/po/tt2/f/original/2015/10/09/battlefront-preview-006.jpg" alt="Star Wars Battlefront traz grÃ¡fico incrÃ­vel e Ã© fiel aos filmes; confira (Darth Vader tambÃ©m estÃ¡ presente em Battlefront (Foto: DivulgalÃ§Ã£o/EA))" title="Star Wars Battlefront traz grÃ¡fico incrÃ­vel e Ã© fiel aos filmes; confira (Darth Vader tambÃ©m estÃ¡ presente em Battlefront (Foto: DivulgalÃ§Ã£o/EA))"
             data-original-image="s2.glbimg.com/9TuT29N0GjEv_mubyiQ2aNa7HZA=/1x3:694x371/245x130/s.glbimg.com/po/tt2/f/original/2015/10/09/battlefront-preview-006.jpg" data-url-smart_horizontal="1WIMrnd62yL9vrFg7BTPiV5KXDs=/90x56/smart/filters:strip_icc()/" data-url-smart="1WIMrnd62yL9vrFg7BTPiV5KXDs=/90x56/smart/filters:strip_icc()/" data-url-feature="1WIMrnd62yL9vrFg7BTPiV5KXDs=/90x56/smart/filters:strip_icc()/" data-url-tablet="2Dtue2zbRsMYRgcIqCrcdXdfwsY=/160x96/smart/filters:strip_icc()/" data-url-desktop="td1Hcxi18HLlm6IUpbftT8HUOi0=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Star Wars Battlefront traz grÃ¡fico incrÃ­vel e Ã© fiel aos filmes; confira</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/noticias/noticia/2015/11/como-remover-o-preview-de-links-no-whatsapp-para-iphone.html" title="FunÃ§Ã£o no WhatsApp do iPhone pode &#39;incomodar&#39;; saiba remover"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/b8d8gqR5U-MBiKE8opgcpR4XvZA=/filters:quality(10):strip_icc()/s2.glbimg.com/qFbJjtOh7xsOz_pPJkz3r-klvdY=/0x227:2391x1495/245x130/s.glbimg.com/po/tt2/f/original/2015/11/16/whatsapp-no-iphone-6s.jpg" alt="FunÃ§Ã£o no WhatsApp do iPhone pode &#39;incomodar&#39;; saiba remover (Anna Kellen Bull/TechTudo)" title="FunÃ§Ã£o no WhatsApp do iPhone pode &#39;incomodar&#39;; saiba remover (Anna Kellen Bull/TechTudo)"
             data-original-image="s2.glbimg.com/qFbJjtOh7xsOz_pPJkz3r-klvdY=/0x227:2391x1495/245x130/s.glbimg.com/po/tt2/f/original/2015/11/16/whatsapp-no-iphone-6s.jpg" data-url-smart_horizontal="c9rHHye9wqwilK8iMU_9JJThdwA=/90x56/smart/filters:strip_icc()/" data-url-smart="c9rHHye9wqwilK8iMU_9JJThdwA=/90x56/smart/filters:strip_icc()/" data-url-feature="c9rHHye9wqwilK8iMU_9JJThdwA=/90x56/smart/filters:strip_icc()/" data-url-tablet="Tg7mKkXoEduxC43J2VFKCfyzTt8=/160x96/smart/filters:strip_icc()/" data-url-desktop="ex7bAH7fY6mFPoCaIdCQlLMXCNQ=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>FunÃ§Ã£o no WhatsApp do iPhone pode &#39;incomodar&#39;; saiba remover</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/listas/noticia/2015/11/black-friday-os-melhores-sites-para-monitorar-precos-e-comprar-barato.html" title="Black Friday vem aÃ­: veja os sites que &#39;guiam&#39; aos melhores preÃ§os"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/bENH_mfJnlF5n-qd_cI-vO8yb1w=/filters:quality(10):strip_icc()/s2.glbimg.com/Nc2zl9HCdl_1QVMoT75G1WNz3HU=/0x58:695x427/245x130/s.glbimg.com/po/tt2/f/original/2015/07/17/032478227-brazilian-currency-real.jpeg" alt="Black Friday vem aÃ­: veja os sites que &#39;guiam&#39; aos melhores preÃ§os (Pond5)" title="Black Friday vem aÃ­: veja os sites que &#39;guiam&#39; aos melhores preÃ§os (Pond5)"
             data-original-image="s2.glbimg.com/Nc2zl9HCdl_1QVMoT75G1WNz3HU=/0x58:695x427/245x130/s.glbimg.com/po/tt2/f/original/2015/07/17/032478227-brazilian-currency-real.jpeg" data-url-smart_horizontal="K4ygPgXbwKBAeeF4hK36a-5uOyo=/90x56/smart/filters:strip_icc()/" data-url-smart="K4ygPgXbwKBAeeF4hK36a-5uOyo=/90x56/smart/filters:strip_icc()/" data-url-feature="K4ygPgXbwKBAeeF4hK36a-5uOyo=/90x56/smart/filters:strip_icc()/" data-url-tablet="tNXSyXzye5lf5wpJF93uhvYXCEQ=/160x96/smart/filters:strip_icc()/" data-url-desktop="h-kRUQ3gfLDG6jtf-WItThFVgTM=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Black Friday vem aÃ­: veja os sites que &#39;guiam&#39; aos melhores preÃ§os</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://forum.techtudo.com.br/perguntas/205181/erro-no-iphone-6-como-resolver-o-unknown-error-53" title="CarÃ­ssimo, iPhone 6 tem erro que &#39;estraga&#39; smart; usuÃ¡rios contam"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/14aodYnve0Qzc2sgZ8snJkvR9SQ=/filters:quality(10):strip_icc()/s2.glbimg.com/HmvVek41fXJCBfuJUT4nCzFJC-4=/94x47:596x313/245x130/s.glbimg.com/po/tt2/f/original/2015/02/04/img_9265_1.jpg" alt="CarÃ­ssimo, iPhone 6 tem erro que &#39;estraga&#39; smart; usuÃ¡rios contam (Anna Kellen Bull/TechTudo)" title="CarÃ­ssimo, iPhone 6 tem erro que &#39;estraga&#39; smart; usuÃ¡rios contam (Anna Kellen Bull/TechTudo)"
             data-original-image="s2.glbimg.com/HmvVek41fXJCBfuJUT4nCzFJC-4=/94x47:596x313/245x130/s.glbimg.com/po/tt2/f/original/2015/02/04/img_9265_1.jpg" data-url-smart_horizontal="FI5KkhFJ7dJl9qOQho02n213uMM=/90x56/smart/filters:strip_icc()/" data-url-smart="FI5KkhFJ7dJl9qOQho02n213uMM=/90x56/smart/filters:strip_icc()/" data-url-feature="FI5KkhFJ7dJl9qOQho02n213uMM=/90x56/smart/filters:strip_icc()/" data-url-tablet="NMSw9oRRHVHTNRX-4KUXEtTgRes=/160x96/smart/filters:strip_icc()/" data-url-desktop="ZmH-MGyb_PrEWd7lOaVGP9PVeNU=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>CarÃ­ssimo, iPhone 6 tem erro que &#39;estraga&#39; smart; usuÃ¡rios contam</p></span></a></li></ul></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-tecnologia']);</script><div class="separator"></div><section class="agrupador-sextuplo analytics-area analytics-id-S"><section class="analytics-area analytics-id-E esquerda tvg"><div class="cabecalho"><h2><span class="word word-0">moda</span><span class="word word-1">&amp;</span><span class="word word-2">beleza</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://revistaglamour.globo.com/Beleza/Beauty-news/noticia/2015/11/maquiagem-pra-pele-negra-cinco-influencers-de-beleza-pra-seguir.html" alt="ConheÃ§a cinco referÃªncias de maquiagem para pele negra"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/GUdzhc-kS2mIobgytAGhEif7PkY=/filters:quality(10):strip_icc()/s2.glbimg.com/_YYX4sHiqyi6EoGCgWG4WUool6M=/0x13:607x404/155x100/e.glbimg.com/og/ed/f/original/2015/11/16/jackie-aina.jpg" alt="ConheÃ§a cinco referÃªncias de maquiagem para pele negra (ReproduÃ§Ã£o/ Instagram)" title="ConheÃ§a cinco referÃªncias de maquiagem para pele negra (ReproduÃ§Ã£o/ Instagram)"
            data-original-image="s2.glbimg.com/_YYX4sHiqyi6EoGCgWG4WUool6M=/0x13:607x404/155x100/e.glbimg.com/og/ed/f/original/2015/11/16/jackie-aina.jpg" data-url-smart_horizontal="s6BA1elvmN7f8DEzlkb5WtHDET8=/90x56/smart/filters:strip_icc()/" data-url-smart="s6BA1elvmN7f8DEzlkb5WtHDET8=/90x56/smart/filters:strip_icc()/" data-url-feature="s6BA1elvmN7f8DEzlkb5WtHDET8=/90x56/smart/filters:strip_icc()/" data-url-tablet="tmZeN7LgujcP67d_XfBynU5Vm7Y=/122x75/smart/filters:strip_icc()/" data-url-desktop="RmJyp2jokfuSAur-I5PQUaMMPFo=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>make neon, preto e metÃ¡lico</h3><p>ConheÃ§a cinco referÃªncias de maquiagem para pele negra</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://vogue.globo.com/moda/moda-tendencias/noticia/2015/11/7-dos-melhores-botinhas-que-roubam-atencao-no-look.html" alt="Botinhas prometem invadir os looks de verÃ£o; veja opÃ§Ãµes"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/cuDDn50sf3PqJkWqAljY2-bNi80=/filters:quality(10):strip_icc()/s2.glbimg.com/asmhw8_JdpwKNK_d7mkLJYuiaaY=/0x136:933x737/155x100/e.glbimg.com/og/ed/f/original/2015/11/16/van-noten-bks-z-rf15-2376.jpg" alt="Botinhas prometem invadir os looks de verÃ£o; veja opÃ§Ãµes (DivulgaÃ§Ã£o)" title="Botinhas prometem invadir os looks de verÃ£o; veja opÃ§Ãµes (DivulgaÃ§Ã£o)"
            data-original-image="s2.glbimg.com/asmhw8_JdpwKNK_d7mkLJYuiaaY=/0x136:933x737/155x100/e.glbimg.com/og/ed/f/original/2015/11/16/van-noten-bks-z-rf15-2376.jpg" data-url-smart_horizontal="X9dCaKUufWqia_hUcTQQEKzaGhM=/90x56/smart/filters:strip_icc()/" data-url-smart="X9dCaKUufWqia_hUcTQQEKzaGhM=/90x56/smart/filters:strip_icc()/" data-url-feature="X9dCaKUufWqia_hUcTQQEKzaGhM=/90x56/smart/filters:strip_icc()/" data-url-tablet="tg8kQ9OsgvqBK9JoN-lWrZE1ue0=/122x75/smart/filters:strip_icc()/" data-url-desktop="m6w1PaQmTq7jBH0ul0ScIrVfLQ0=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>cano curto, salto grosso e material inusitado</h3><p>Botinhas prometem invadir <br />os looks de verÃ£o; veja opÃ§Ãµes</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://gnt.globo.com/bem-estar/materias/os-beneficios-da-vitamina-d-especialista-esclarece-principais-duvidas-sobre-o-nutriente.htm" alt="Veja como suprir a necessidade de vitamina D sem excesso de sol"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/u1q_XwiiJ4Br1pQEqL57H7kQlfg=/filters:quality(10):strip_icc()/s2.glbimg.com/og2yXz767urg2p1wXMAGNSs5RGA=/128x47:587x343/155x100/g.glbimg.com/og/gs/gsat5/f/original/2015/11/09/vitamina_d_getty_images.jpg" alt="Veja como suprir a necessidade de vitamina D sem excesso de sol (Getty Images)" title="Veja como suprir a necessidade de vitamina D sem excesso de sol (Getty Images)"
            data-original-image="s2.glbimg.com/og2yXz767urg2p1wXMAGNSs5RGA=/128x47:587x343/155x100/g.glbimg.com/og/gs/gsat5/f/original/2015/11/09/vitamina_d_getty_images.jpg" data-url-smart_horizontal="cXfhoyC1ZTL5KmzH9lBe44gnk-Y=/90x56/smart/filters:strip_icc()/" data-url-smart="cXfhoyC1ZTL5KmzH9lBe44gnk-Y=/90x56/smart/filters:strip_icc()/" data-url-feature="cXfhoyC1ZTL5KmzH9lBe44gnk-Y=/90x56/smart/filters:strip_icc()/" data-url-tablet="yal8b0IpSAqm9NrlEkjFuHgx06w=/122x75/smart/filters:strip_icc()/" data-url-desktop="wos9wg40Ij8Ku2H8rfn8gpIr2Pg=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>riscos do verÃ£o</h3><p>Veja como suprir a necessidade <br />de vitamina D sem excesso de sol</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://gnt.globo.com/" title="GNT">GNT</a></h3></li><li><h3><a href="http://vogue.globo.com/" title="vogue">vogue</a></h3></li><li></li></ul></aside></section><div class="separator"></div><section class="analytics-area analytics-id-D direita tvg"><div class="cabecalho"><h2><span class="word word-0">CASA</span><span class="word word-1">&amp;</span><span class="word word-2">DECORAÃÃO</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://casavogue.globo.com/Arquitetura/Casas/noticia/2015/09/um-lar-com-adega-e-estudio-de-musica.html" alt="Lar de casal com filhas gÃªmeas tem atÃ© adega e estÃºdio de mÃºsica; fotos"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/T3cmE7bB68QJveJSoxHDCpi3onU=/filters:quality(10):strip_icc()/s2.glbimg.com/jAYmY6E2gChv5r0r1E63plaKvTU=/106x37:495x287/155x100/e.glbimg.com/og/ed/f/original/2015/09/22/casa-carla-kiss-12.jpg" alt="Lar de casal com filhas gÃªmeas tem atÃ© adega e estÃºdio de mÃºsica; fotos (Marcelo Stammer)" title="Lar de casal com filhas gÃªmeas tem atÃ© adega e estÃºdio de mÃºsica; fotos (Marcelo Stammer)"
            data-original-image="s2.glbimg.com/jAYmY6E2gChv5r0r1E63plaKvTU=/106x37:495x287/155x100/e.glbimg.com/og/ed/f/original/2015/09/22/casa-carla-kiss-12.jpg" data-url-smart_horizontal="v4jIJWKouwJuR-B2Kk-cEru317g=/90x56/smart/filters:strip_icc()/" data-url-smart="v4jIJWKouwJuR-B2Kk-cEru317g=/90x56/smart/filters:strip_icc()/" data-url-feature="v4jIJWKouwJuR-B2Kk-cEru317g=/90x56/smart/filters:strip_icc()/" data-url-tablet="xRen03APuRGwTbDHFmb1VhQc4Aw=/122x75/smart/filters:strip_icc()/" data-url-desktop="n9tJTblDdwfNjxPeeLMjOK37JLc=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>casa de  950 mÂ² no ParanÃ¡</h3><p>Lar de casal com filhas gÃªmeas tem atÃ© adega e estÃºdio de mÃºsica; fotos</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revista.zapimoveis.com.br/dicas-para-o-uso-de-persianas-2964467-sc/" alt="Persianas sÃ£o mais fÃ¡ceis de limpar e podem ser usadas atÃ© no banheiro"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/UF6jR4Cfn8w0Ytp6RwM_KSu8rt0=/filters:quality(10):strip_icc()/s2.glbimg.com/-W6bnWzhtb5ZOv1hEN-XNQGTKxA=/24x48:451x324/155x100/s.glbimg.com/en/ho/f/original/2015/11/17/persiana-jardim.jpg" alt="Persianas sÃ£o mais fÃ¡ceis de limpar e podem ser usadas atÃ© no banheiro (Shutterstock)" title="Persianas sÃ£o mais fÃ¡ceis de limpar e podem ser usadas atÃ© no banheiro (Shutterstock)"
            data-original-image="s2.glbimg.com/-W6bnWzhtb5ZOv1hEN-XNQGTKxA=/24x48:451x324/155x100/s.glbimg.com/en/ho/f/original/2015/11/17/persiana-jardim.jpg" data-url-smart_horizontal="rhMbra4hRcX8i5VYdpfoHWpIovU=/90x56/smart/filters:strip_icc()/" data-url-smart="rhMbra4hRcX8i5VYdpfoHWpIovU=/90x56/smart/filters:strip_icc()/" data-url-feature="rhMbra4hRcX8i5VYdpfoHWpIovU=/90x56/smart/filters:strip_icc()/" data-url-tablet="tkMHyztHc7DOIQLd7HKpFqBEhgI=/122x75/smart/filters:strip_icc()/" data-url-desktop="eEFm-iAKY1FEg4qFaDzJMfrr8cw=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>escolha &#39;clean&#39;</h3><p>Persianas sÃ£o mais fÃ¡ceis de limpar e podem ser usadas atÃ© no banheiro</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revistacasaejardim.globo.com/Casa-e-Jardim/Decoracao/Estilo/Colorido/noticia/2015/11/cores-na-decoracao-como-usar-escolher-e-combinar.html" alt="SeleÃ§Ã£o de dicas e fotos mostra como usar investir melhor nas cores"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/tNLDbHeW07RYzap4oN6uLV2mlTg=/filters:quality(10):strip_icc()/s2.glbimg.com/yVi4m6FFQ0CazadNLxfUqPicbMI=/72x133:707x542/155x100/e.glbimg.com/og/ed/f/original/2014/09/19/cj696_dubatuba_80.jpg" alt="SeleÃ§Ã£o de dicas e fotos mostra como usar investir melhor nas cores (Victor Affaro/Editora Globo)" title="SeleÃ§Ã£o de dicas e fotos mostra como usar investir melhor nas cores (Victor Affaro/Editora Globo)"
            data-original-image="s2.glbimg.com/yVi4m6FFQ0CazadNLxfUqPicbMI=/72x133:707x542/155x100/e.glbimg.com/og/ed/f/original/2014/09/19/cj696_dubatuba_80.jpg" data-url-smart_horizontal="f7UUXZLusuHU6kKzblO15SSSW7g=/90x56/smart/filters:strip_icc()/" data-url-smart="f7UUXZLusuHU6kKzblO15SSSW7g=/90x56/smart/filters:strip_icc()/" data-url-feature="f7UUXZLusuHU6kKzblO15SSSW7g=/90x56/smart/filters:strip_icc()/" data-url-tablet="KC_rqd1Mc4vF2bWJSsmvQNJMJ7w=/122x75/smart/filters:strip_icc()/" data-url-desktop="vLla9l7PxIdQplcbI3y-Qa1HIhU=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>pinte atÃ© o chÃ£o</h3><p>SeleÃ§Ã£o de dicas e fotos mostra como usar investir melhor nas cores</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://revistacasaejardim.globo.com/" title="CASA E JARDIM">CASA E JARDIM</a></h3></li><li><h3><a href="http://casavogue.globo.com/" title="CASA VOGUE">CASA VOGUE</a></h3></li><li></li></ul></aside></section></section><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior famosos-style"><div class="cabecalho"><h2><a title="famosos"><span class='logo'>famosos</span><span class='texto'>famosos</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://ego.globo.com/">ego</a></h3></li><li class=""><h3><a href="http://revistaquem.globo.com/">quem</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://ego.globo.com/natal/noticia/2015/11/geisy-arruda-vira-mamae-noel-sexy-e-posa-decotada-em-limousine-rosa.html" title="Geisy posa de gorro natalino e decotÃ£o em limousine rosa"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/dMGgGoa84hCGMxpO6FZR09Qa_78=/filters:quality(10):strip_icc()/s2.glbimg.com/CjzmTYEJJeUsdm1m3u7J2HreHI0=/0x387:1223x1036/245x130/s.glbimg.com/jo/eg/f/original/2015/11/17/image10.jpg" alt="Geisy posa de gorro natalino e decotÃ£o em limousine rosa (Thais Aline / AgÃªncia Fio Condutor)" title="Geisy posa de gorro natalino e decotÃ£o em limousine rosa (Thais Aline / AgÃªncia Fio Condutor)"
                    data-original-image="s2.glbimg.com/CjzmTYEJJeUsdm1m3u7J2HreHI0=/0x387:1223x1036/245x130/s.glbimg.com/jo/eg/f/original/2015/11/17/image10.jpg" data-url-smart_horizontal="t4x1Hu6uTAkyn3wStZW202Z0sOE=/90x56/smart/filters:strip_icc()/" data-url-smart="t4x1Hu6uTAkyn3wStZW202Z0sOE=/90x56/smart/filters:strip_icc()/" data-url-feature="t4x1Hu6uTAkyn3wStZW202Z0sOE=/90x56/smart/filters:strip_icc()/" data-url-tablet="0NENPEshVEwvwiijCccDQ2BeqDE=/160x95/smart/filters:strip_icc()/" data-url-desktop="iA7Uq4J6gLnb8URpA7x_E24J64E=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Geisy posa de gorro natalino e decotÃ£o em limousine rosa</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/Bastidores/noticia/2015/11/mae-aos-43-christiana-kalache-se-dedica-integralmente-ao-filho.html" title="Lembra dela? Atriz de &#39;AmÃ©rica&#39; tem filho aos 43: &#39;Ã puxadÃ­ssimo&#39;"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/T0WiH4UDdnT3Y5UI3k5Lx7jKesU=/filters:quality(10):strip_icc()/s2.glbimg.com/X1TNTbMiojLp7F7JGQgNbW9B_JM=/3x106:661x456/245x130/s.glbimg.com/et/gs/f/original/2015/11/16/christiana-e-filho-no-colo.jpg" alt="Lembra dela? Atriz de &#39;AmÃ©rica&#39; tem filho aos 43: &#39;Ã puxadÃ­ssimo&#39; (Arquivo Pessoal)" title="Lembra dela? Atriz de &#39;AmÃ©rica&#39; tem filho aos 43: &#39;Ã puxadÃ­ssimo&#39; (Arquivo Pessoal)"
                    data-original-image="s2.glbimg.com/X1TNTbMiojLp7F7JGQgNbW9B_JM=/3x106:661x456/245x130/s.glbimg.com/et/gs/f/original/2015/11/16/christiana-e-filho-no-colo.jpg" data-url-smart_horizontal="6PlJnf-ulfxz3oQeDNaZNYCJ6gQ=/90x56/smart/filters:strip_icc()/" data-url-smart="6PlJnf-ulfxz3oQeDNaZNYCJ6gQ=/90x56/smart/filters:strip_icc()/" data-url-feature="6PlJnf-ulfxz3oQeDNaZNYCJ6gQ=/90x56/smart/filters:strip_icc()/" data-url-tablet="oWLnhPfCVxiVd70LROlh9HUEOCk=/160x95/smart/filters:strip_icc()/" data-url-desktop="-p3PXRnE-A1CuCy8lW9SXHHcBTc=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Lembra dela? Atriz de &#39;AmÃ©rica&#39; tem filho aos 43: &#39;Ã puxadÃ­ssimo&#39;</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://revistaquem.globo.com/Popquem/noticia/2015/11/quero-espalhar-minha-musica-por-todo-mundo-diz-maria-luiza.html" title="Atriz de &#39;MalhaÃ§Ã£o&#39; posta ao redor do mundo e fala de clipe"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/yfYOqy0FIMHnNeu78IvMykLcLJ4=/filters:quality(10):strip_icc()/s2.glbimg.com/_JLeHEI7tQ7SICmg3kvkwas3_Ag=/55x202:300x332/245x130/e.glbimg.com/og/ed/f/original/2015/11/17/ma0.jpg" alt="Atriz de &#39;MalhaÃ§Ã£o&#39; posta ao redor do mundo e fala de clipe (ReproduÃ§Ã£o)" title="Atriz de &#39;MalhaÃ§Ã£o&#39; posta ao redor do mundo e fala de clipe (ReproduÃ§Ã£o)"
                    data-original-image="s2.glbimg.com/_JLeHEI7tQ7SICmg3kvkwas3_Ag=/55x202:300x332/245x130/e.glbimg.com/og/ed/f/original/2015/11/17/ma0.jpg" data-url-smart_horizontal="bEJnR02VvBC9zsgkJnYe9fPyaew=/90x56/smart/filters:strip_icc()/" data-url-smart="bEJnR02VvBC9zsgkJnYe9fPyaew=/90x56/smart/filters:strip_icc()/" data-url-feature="bEJnR02VvBC9zsgkJnYe9fPyaew=/90x56/smart/filters:strip_icc()/" data-url-tablet="n5Wopi_g4hk0h3WP8wfBJQlcWPk=/160x95/smart/filters:strip_icc()/" data-url-desktop="K27AHM6rLukBsgoB-UZ4RH9DKtk=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Atriz de &#39;MalhaÃ§Ã£o&#39; posta ao redor do mundo e fala de clipe</p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://g1.globo.com/pop-arte/cinema/noticia/2015/11/ate-que-sorte-nos-separe-3-com-leandro-hassum-ganha-1-trailer.html" title="Filme de Hassum tem piada com mandioca da Dilma; trailer"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/YTc6_yziOFz9KegaO6NY1XRWE0g=/filters:quality(10):strip_icc()/s2.glbimg.com/OUJpUppuM5qMtxUF60aSpn7urpw=/20x15:393x213/245x130/s.glbimg.com/en/ho/f/original/2015/11/17/dilma.jpg" alt="Filme de Hassum tem piada com mandioca da Dilma; trailer (Globo Filmes)" title="Filme de Hassum tem piada com mandioca da Dilma; trailer (Globo Filmes)"
                    data-original-image="s2.glbimg.com/OUJpUppuM5qMtxUF60aSpn7urpw=/20x15:393x213/245x130/s.glbimg.com/en/ho/f/original/2015/11/17/dilma.jpg" data-url-smart_horizontal="MB6CBLRpfwOH4dKj2PSNqfGFc-4=/90x56/smart/filters:strip_icc()/" data-url-smart="MB6CBLRpfwOH4dKj2PSNqfGFc-4=/90x56/smart/filters:strip_icc()/" data-url-feature="MB6CBLRpfwOH4dKj2PSNqfGFc-4=/90x56/smart/filters:strip_icc()/" data-url-tablet="MntqK-7GTrpyEawzpoOF8ncsW0U=/160x95/smart/filters:strip_icc()/" data-url-desktop="V1jK819t9xkMJxURg0ldbrksIHM=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Filme de Hassum tem piada com mandioca da Dilma; trailer</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_famosos-style']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior gshow"><div class="cabecalho"><h2><a href="http://gshow.globo.com/" title="novelas, sÃ©ries, programas e muito mais"><span class='logo'>novelas, sÃ©ries, programas e muito mais</span><span class='texto'>novelas, sÃ©ries, programas e muito mais</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://gshow.globo.com/estilo-tv/">ESTILO TV</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/">A REGRA DO JOGO</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/totalmente-demais/">TOTALMENTE DEMAIS</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/alem-do-tempo/">ALÃM DO TEMPO</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/malhacao/2015/">MALHAÃÃO</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/alem-do-tempo/vem-por-ai/noticia/2015/11/melissa-revela-ser-capaz-de-tudo-para-nao-perder-felipe.html" title="&#39;AlÃ©m do Tempo&#39;: Melissa revela ser capaz de tudo por Felipe"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/6uiCn0dXjTavekve5y8sqKgz0aA=/filters:quality(10):strip_icc()/s2.glbimg.com/aEwEHMO8RrckaUacfJ4ddQzdgUg=/150x72:601x312/245x130/s.glbimg.com/et/gs/f/original/2015/11/16/paolla_oliveira.jpg" alt="&#39;AlÃ©m do Tempo&#39;: Melissa revela ser capaz de tudo por Felipe (TV Globo)" title="&#39;AlÃ©m do Tempo&#39;: Melissa revela ser capaz de tudo por Felipe (TV Globo)"
                    data-original-image="s2.glbimg.com/aEwEHMO8RrckaUacfJ4ddQzdgUg=/150x72:601x312/245x130/s.glbimg.com/et/gs/f/original/2015/11/16/paolla_oliveira.jpg" data-url-smart_horizontal="I7k0n87cbJUel8q95LIlufnuvIY=/90x56/smart/filters:strip_icc()/" data-url-smart="I7k0n87cbJUel8q95LIlufnuvIY=/90x56/smart/filters:strip_icc()/" data-url-feature="I7k0n87cbJUel8q95LIlufnuvIY=/90x56/smart/filters:strip_icc()/" data-url-tablet="eEmHbj7QjsK6yvVcxUR9FhF2zQU=/160x95/smart/filters:strip_icc()/" data-url-desktop="2dfTCk9AlniMeX8ZL3RhYgdFgSY=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>&#39;AlÃ©m do Tempo&#39;: Melissa revela ser capaz de tudo por Felipe</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/malhacao/2015/Vem-por-ai/noticia/2015/11/livia-diz-que-cica-e-culpada-pelo-termino-de-rodrigo-com-luciana.html" title="&#39;MalhaÃ§Ã£o&#39;: Rodrigo ouve que Lu se afastou por causa de CiÃ§a"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/-d6ojlb7jdYlUo2PJcJkcOOUsMI=/filters:quality(10):strip_icc()/s2.glbimg.com/YS5glMYw2gZ-akPBSkQJFDPl7Mw=/0x17:690x384/245x130/s.glbimg.com/et/gs/f/original/2015/11/16/livia-rodrigo.jpg" alt="&#39;MalhaÃ§Ã£o&#39;: Rodrigo ouve que Lu se afastou por causa de CiÃ§a (TV Globo)" title="&#39;MalhaÃ§Ã£o&#39;: Rodrigo ouve que Lu se afastou por causa de CiÃ§a (TV Globo)"
                    data-original-image="s2.glbimg.com/YS5glMYw2gZ-akPBSkQJFDPl7Mw=/0x17:690x384/245x130/s.glbimg.com/et/gs/f/original/2015/11/16/livia-rodrigo.jpg" data-url-smart_horizontal="wX7ou6NbXMxqEqxcaoTaZFkv1N8=/90x56/smart/filters:strip_icc()/" data-url-smart="wX7ou6NbXMxqEqxcaoTaZFkv1N8=/90x56/smart/filters:strip_icc()/" data-url-feature="wX7ou6NbXMxqEqxcaoTaZFkv1N8=/90x56/smart/filters:strip_icc()/" data-url-tablet="TakXYyOEirmHlu5lsZ-GTIu0N0c=/160x95/smart/filters:strip_icc()/" data-url-desktop="7OR1xwgpKetiTV39Cxvd6qUvrtw=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>&#39;MalhaÃ§Ã£o&#39;: Rodrigo ouve que Lu se afastou por causa de CiÃ§a</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/tv/noticia/2015/11/fernando-ceylao-perde-42-kg-so-com-dieta-e-afirma-na-cabeca-ainda-sou-gordo.html" title="Fernando CeylÃ£o perde 42 kg: &#39;Na cabeÃ§a ainda sou gordo&#39;
"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/VHmJED0t0ISZrKVtiYHkCRCyFQI=/filters:quality(10):strip_icc()/s2.glbimg.com/7fN2DUxY7KJFtIX2aL4Dsas0L0c=/0x24:720x406/245x130/s.glbimg.com/et/gs/f/original/2015/11/16/fernando-ceylao-antes-e-depois.jpg" alt="Fernando CeylÃ£o perde 42 kg: &#39;Na cabeÃ§a ainda sou gordo&#39;
 (TV Globo)" title="Fernando CeylÃ£o perde 42 kg: &#39;Na cabeÃ§a ainda sou gordo&#39;
 (TV Globo)"
                    data-original-image="s2.glbimg.com/7fN2DUxY7KJFtIX2aL4Dsas0L0c=/0x24:720x406/245x130/s.glbimg.com/et/gs/f/original/2015/11/16/fernando-ceylao-antes-e-depois.jpg" data-url-smart_horizontal="lqIRTzUCrsWUZb4ABG-8e8T56qg=/90x56/smart/filters:strip_icc()/" data-url-smart="lqIRTzUCrsWUZb4ABG-8e8T56qg=/90x56/smart/filters:strip_icc()/" data-url-feature="lqIRTzUCrsWUZb4ABG-8e8T56qg=/90x56/smart/filters:strip_icc()/" data-url-tablet="ecDbJC4wIP2CqN8ZVuV23bMU56Q=/160x95/smart/filters:strip_icc()/" data-url-desktop="9RPkYpS167Z6TJAA4qf60CkMzoc=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Fernando CeylÃ£o perde 42 kg: &#39;Na cabeÃ§a ainda sou gordo&#39;<br /></p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://gshow.globo.com/Bastidores/noticia/2015/11/carolina-dieckmann-mostra-marmita-e-comenta-barriga-chapada-sempre-foi-musculosa.html" title="Com barriga seca, Dieckmann nÃ£o deixa de comer bolinho"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/2zn-IQ56ZCM7KTDFtC2yq7mOWJo=/filters:quality(10):strip_icc()/s2.glbimg.com/dkW9nsXoQ3kRn-CkyRb3W3EwnDE=/0x87:690x453/245x130/s.glbimg.com/et/gs/f/original/2015/11/13/dieckamann_1.jpg" alt="Com barriga seca, Dieckmann nÃ£o deixa de comer bolinho (Carolina Morgado/Gshow)" title="Com barriga seca, Dieckmann nÃ£o deixa de comer bolinho (Carolina Morgado/Gshow)"
                    data-original-image="s2.glbimg.com/dkW9nsXoQ3kRn-CkyRb3W3EwnDE=/0x87:690x453/245x130/s.glbimg.com/et/gs/f/original/2015/11/13/dieckamann_1.jpg" data-url-smart_horizontal="PBu54rdei-pMzqp_yr1LaOxbjSg=/90x56/smart/filters:strip_icc()/" data-url-smart="PBu54rdei-pMzqp_yr1LaOxbjSg=/90x56/smart/filters:strip_icc()/" data-url-feature="PBu54rdei-pMzqp_yr1LaOxbjSg=/90x56/smart/filters:strip_icc()/" data-url-tablet="Zi4spXGgWpALb5TI1GNxXKFPGKU=/160x95/smart/filters:strip_icc()/" data-url-desktop="sG4ueeFfK4Q8wdN70_xT1KKsVNI=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Com barriga seca, Dieckmann nÃ£o deixa de comer bolinho</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_gshow']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-musica-triplo analytics-area analytics-id-U franja-inferior"><div class="cabecalho"><h2><a href="http://musica.com.br/" title="MÃSICA">
            MÃSICA
        </a></h2><aside class="links analytics-area analytics-id-T"><ul><li class="mobile-grid-full "><h3><a href="http://musica.com.br/">MÃSICA.COM.BR</a></h3></li><li class="mobile-grid-full "><h3><a href="http://g1.globo.com/musica/">G1 MÃSICA</a></h3></li><li class="mobile-grid-full "><h3><a href="http://multishow.globo.com/musica/">MULTISHOW</a></h3></li><li class="mobile-grid-full "><h3><a href="http://radiobeat.com.br/">RADIOBEAT</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://g1.globo.com/distrito-federal/noticia/2015/11/pearl-jam-monitora-emissao-de-co2-em-show-em-brasilia.html" title="Pearl Jam monitora emissÃ£o de CO2 em show em BrasÃ­lia para &#39;taxar&#39; a prÃ³pria banda  (Fabio Tito / G1)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/Hr5g865T17x-cnsl4nhQleClzfg=/filters:quality(10):strip_icc()/s2.glbimg.com/fezOyk3bSEMOnx8cNpaxdpJPRyY=/0x311:1095x973/215x130/s.glbimg.com/jo/g1/f/original/2015/11/14/pearljam_fabio_tito-28.jpg"
                data-original-image="s2.glbimg.com/fezOyk3bSEMOnx8cNpaxdpJPRyY=/0x311:1095x973/215x130/s.glbimg.com/jo/g1/f/original/2015/11/14/pearljam_fabio_tito-28.jpg" data-url-smart_horizontal="s1DkaG-382aXn3S7vT1i4leQxtw=/90x56/smart/filters:strip_icc()/" data-url-smart="s1DkaG-382aXn3S7vT1i4leQxtw=/90x56/smart/filters:strip_icc()/" data-url-feature="s1DkaG-382aXn3S7vT1i4leQxtw=/90x56/smart/filters:strip_icc()/" data-url-tablet="RvMw7F9O65FiWNlnuIgOZg_-qi0=/120x80/smart/filters:strip_icc()/" data-url-desktop="_wZGPQTdQ5RGVDsvBQfxEoVd-Mk=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Pearl Jam monitora emissÃ£o de CO2 em show em BrasÃ­lia para &#39;taxar&#39; a prÃ³pria banda </div></div></a></li><li class="mobile-grid-full"><a class="foto" href="http://oglobo.globo.com/cultura/musica/adele-revela-when-we-were-young-segundo-single-de-25-18069454" title="OuÃ§a o 2Âª single da cantora Adele; em poucas horas, quase 500 mil visualizaÃ§Ãµes (DivulgaÃ§Ã£o)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/jUFHabJ78NGUJ6lFRv_QpQyxPAo=/filters:quality(10):strip_icc()/s2.glbimg.com/hg6Re6xXhaRS0zCCwJ3DnssQ_g4=/110x29:432x224/215x130/s.glbimg.com/jo/g1/f/original/2015/11/17/adele.jpg"
                data-original-image="s2.glbimg.com/hg6Re6xXhaRS0zCCwJ3DnssQ_g4=/110x29:432x224/215x130/s.glbimg.com/jo/g1/f/original/2015/11/17/adele.jpg" data-url-smart_horizontal="1FSiWb2zKddpKn2Hw-nFK_MYfTs=/90x56/smart/filters:strip_icc()/" data-url-smart="1FSiWb2zKddpKn2Hw-nFK_MYfTs=/90x56/smart/filters:strip_icc()/" data-url-feature="1FSiWb2zKddpKn2Hw-nFK_MYfTs=/90x56/smart/filters:strip_icc()/" data-url-tablet="wyrbUTi7QY48VnOmFwy4YrfgV4I=/120x80/smart/filters:strip_icc()/" data-url-desktop="7IZ8Dciy60D5GJNdpd0qoQMFTD8=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">OuÃ§a o 2Âª single da cantora Adele; em poucas horas, quase 500 mil visualizaÃ§Ãµes</div></div></a></li><li class="mobile-grid-full"><a class="foto" href="http://globoplay.globo.com/v/4574181/" title="Roberto Carlos fez showzÃ£o histÃ³rico em JerusalÃ©m em 2011; relembre na Ã­ntegra (TV Globo / ZÃ© Paulo Cardeal)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/dECFwgt5Vmzs4STK-08CKa1m1tI=/filters:quality(10):strip_icc()/s2.glbimg.com/lec0M1IRwa3YuvBG01x25BZRHCo=/46x64:496x336/215x130/s.glbimg.com/og/rg/f/original/2011/09/09/roberto-carlos-foto.jpg"
                data-original-image="s2.glbimg.com/lec0M1IRwa3YuvBG01x25BZRHCo=/46x64:496x336/215x130/s.glbimg.com/og/rg/f/original/2011/09/09/roberto-carlos-foto.jpg" data-url-smart_horizontal="t3hICAYPyZ7QK0HIxLXxnAy-x7A=/90x56/smart/filters:strip_icc()/" data-url-smart="t3hICAYPyZ7QK0HIxLXxnAy-x7A=/90x56/smart/filters:strip_icc()/" data-url-feature="t3hICAYPyZ7QK0HIxLXxnAy-x7A=/90x56/smart/filters:strip_icc()/" data-url-tablet="RJaJrC8e-DkkUP0LK4T8Mv5VbMI=/120x80/smart/filters:strip_icc()/" data-url-desktop="tUzHgV1NdSXiJA_CFCxRz7w-158=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Roberto Carlos fez showzÃ£o histÃ³rico em JerusalÃ©m em 2011; relembre na Ã­ntegra</div></div></a></li></ul><div class="top-container analytics-area analytics-id-L mobile-grid-full"><h3><a href="http://musica.com.br/">
            TOP HITS
        </a></h3><ul><li><a class="foto" href="http://musica.com.br/artistas/luan-santana/m/escreve-ai/letra.html"><div class="area-foto"><div class="number">1</div><img width="45" height="45" src="http://s2.glbimg.com/DejQr6wzwaLjEToKO0IBZYMpp9E=/filters:quality(10):strip_icc()/s2.glbimg.com/gffJ6jy3x9TdEFawxnsku-K_Igs=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/12/20/artista-65018-2013151926.jpg"
                data-original-image="s2.glbimg.com/gffJ6jy3x9TdEFawxnsku-K_Igs=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/12/20/artista-65018-2013151926.jpg" data-url-smart_horizontal="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-smart="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-feature="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-tablet="jBcm2gM8Cbg_Vkcg-0n1BAJmuKE=/60x60/smart/filters:strip_icc()/" data-url-desktop="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Luan Santana</div><div class="title">Escreve AÃ­</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li><li><a class="foto" href="http://musica.com.br/artistas/mc-anitta/m/no-meu-talento-part-mc-guime/letra.html"><div class="area-foto"><div class="number">2</div><img width="45" height="45" src="http://s2.glbimg.com/yHnJPHom5FPA3tk69JCcIphdNJE=/filters:quality(10):strip_icc()/s2.glbimg.com/i6tbAg-HNpqidGWWFJqVJ788g6o=/65x65/smart/s.glbimg.com/po/ms/f/original/2014/03/28/anitta-nova-cut.jpg"
                data-original-image="s2.glbimg.com/i6tbAg-HNpqidGWWFJqVJ788g6o=/65x65/smart/s.glbimg.com/po/ms/f/original/2014/03/28/anitta-nova-cut.jpg" data-url-smart_horizontal="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-smart="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-feature="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-tablet="AODuTqXeNCV61xTQZim-SUSZDBg=/60x60/smart/filters:strip_icc()/" data-url-desktop="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Anitta</div><div class="title">No Meu Talento (part. MC GuimÃª)</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li><li class="last-top"><a class="foto" href="http://musica.com.br/artistas/jorge-e-mateus/m/nocaute/letra.html"><div class="area-foto"><div class="number">3</div><img width="45" height="45" src="http://s2.glbimg.com/jdC6lPjKcSpWF8v5jzFy0uuol6c=/filters:quality(10):strip_icc()/s2.glbimg.com/JgDLLARwYvvuCkqHPgmPue-pxa8=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/06/26/jorge.jpg"
                data-original-image="s2.glbimg.com/JgDLLARwYvvuCkqHPgmPue-pxa8=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/06/26/jorge.jpg" data-url-smart_horizontal="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-smart="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-feature="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-tablet="EfTWuRDHHuk_LklhIijQiqahj3A=/60x60/smart/filters:strip_icc()/" data-url-desktop="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Jorge e Mateus</div><div class="title">Nocaute</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li></ul></div><div class="separator"></div><div class="widget analytics-area analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-musica-triplo']);</script></section><section class="area topglobocom"><div class="container area"><div class="separator"></div><div id="topglobocom3" class="clearfix destaque topglobocom3"><div class="topo"><h3 class="titulo">
            top <strong>globo</strong><span class="subtitulo">tÃ¡ todo mundo clicando...</span></h3></div><div id="mais-vistas" class="clearfix mais-vistas tab-content analytics-area analytics-id-R"><div class="slider"><ol class="noticias analytics-area analytics-id-J"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/mundo/noticia/2015/11/russia-diz-que-bomba-derrubou-aviao-no-egito-e-afirma-que-ato-foi-terrorista.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">RÃºssia diz que bomba derrubou aviÃ£o no Egito e afirma que ato foi terrorista</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/mundo/noticia/2015/11/alvo-de-terroristas-em-janeiro-charlie-hebdo-ironiza-atentados-em-paris.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Alvo de terroristas em janeiro, &#39;Charlie Hebdo&#39; ironiza atentados em Paris</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/mundo/noticia/2015/11/franca-faz-novo-ataque-aereo-sobre-reduto-do-estado-islamico-na-siria.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">FranÃ§a e RÃºssia fazem novos ataques contra o Estado IslÃ¢mico na SÃ­ria</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://revistagalileu.globo.com/Ciencia/noticia/2015/11/dna-sugere-que-menino-sacrificado-pelos-incas-era-de-uma-linhagem-perdida.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">DNA de menino de 7 anos mumificado apÃ³s a morte surpreende cientistas</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/planeta-bizarro/noticia/2015/11/modelo-fica-ferida-apos-dj-lancar-bola-de-golfe-colocada-em-seu-bumbum.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Modelo ferida por DJ com taco de golfe faz acordo em processo de R$ 1,9 milhÃµes</span></span></a></div></div></li></ol><ol class="esportes analytics-area analytics-id-E"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://sportv.globo.com/site/programas/bem-amigos/noticia/2015/11/junior-diz-que-nao-se-sentiu-ofendido-por-daniel-alves-e-galvao-critica-lateral.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">JÃºnior diz que nÃ£o se sentiu ofendido por Daniel Alves, e GalvÃ£o critica lateral</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/futebol/selecao-brasileira/noticia/2015/11/dani-alves-critica-energia-pesada-do-brasil-e-diz-que-so-historia-nao-basta.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Dani Alves critica &#39;energia pesada&#39; do Brasil e diz que sÃ³ histÃ³ria nÃ£o basta</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://sportv.globo.com/site/combate/noticia/2015/11/dana-diz-que-ufc-esta-trabalhando-em-revanche-entre-anderson-e-belfort.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Dana diz que UFC estÃ¡ trabalhando em revanche entre Anderson e Belfort</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://sportv.globo.com/site/eventos/liga-nacional-de-futsal/noticia/2015/11/corinthians-para-no-orlandia-de-novo-e-cai-na-semifinal-pelo-6-ano-seguido.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Em jogo marcado por briga de torcida, OrlÃ¢ndia passa por TimÃ£o e vai Ã  final da LNF</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/futebol/times/cruzeiro/noticia/2015/11/cruzeiro-vai-enxugar-grupo-e-ja-sabe-quem-fica-e-quem-deixa-toca-em-2016.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Cruzeiro vai enxugar grupo e jÃ¡ sabe quem fica e quem deixa Toca em 2016</span></span></a></div></div></li></ol><ol class="entretenimento analytics-area analytics-id-M"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://revistaquem.globo.com/QUEM-News/noticia/2015/11/uau-isabeli-fontana-posta-foto-completamente-nua-banho-de-lua.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Linda: Isabeli Fontana posta foto nua e deixa fÃ£s babando: &#39;Banho de lua&#39;</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://extra.globo.com/famosos/a-cara-da-mae-sandy-o-filho-theo-viram-sensacao-em-embarque-no-rio-18067321.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Filho de Sandy Ã© sensaÃ§Ã£o no Rio; compare com a mÃ£e na mesma idade</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/vem-por-ai/noticia/2015/11/domingas-se-humilha-leva-tapao-de-juca-e-comete-loucura.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">&#39;A Regra do Jogo&#39;: Domingas se humilha, leva tapÃ£o de Juca e comete loucura</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://epoca.globo.com/colunas-e-blogs/bruno-astuto/noticia/2015/11/daniela-mercury-vai-aparecer-nua-na-capa-do-novo-cd.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Daniela Mercury copia foto clÃ¡ssica e fica<br /> nua com esposa na capa de disco</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/vem-por-ai/noticia/2015/11/policia-prende-poderoso-da-faccao-e-ze-maria-nao-reage.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Mais de &#39;A Regra&#39;: polÃ­cia prende poderoso da facÃ§Ã£o, e ZÃ© Maria nÃ£o reage</span></span></a></div></div></li></ol></div></div><div class="controls-area"><ul class="step-marker"><li class=" glb-hl-style-noticias"><div class=""><a href="#" data-related=""><div class="ball active"></div></a></div></li><li class=" glb-hl-style-esporte"><div class=""><a href="#" data-related=""><div class="ball"></div></a></div></li><li class=" glb-hl-style-entretenimento"><div class=""><a href="#" data-related=""><div class="ball "></div></a></div></li></ul></div></div><section class="top-videos analytics-area analytics-id-R"><h1><a href="http://globotv.globo.com/">globo.tv</a></h1><div class="separator-line"></div><ul class="destaque analytics-area analytics-id-V"><li ><a class="foto" href="#"><span class="borda-foto has-video"><span></span><img width="190" height="110"></span><span class="number">1</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto has-video"><span></span><img width="190" height="110"></span><span class="number">2</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto has-video"><span></span><img width="190" height="110"></span><span class="number">3</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto has-video"><span></span><img width="190" height="110"></span><span class="number">4</span><span class="conteudo"><p>descr</p></span></a></li><li class="ult"><a class="foto" href="#"><span class="borda-foto has-video"><span></span><img width="190" height="110"></span><span class="number">5</span><span class="conteudo"><p>descr</p></span></a></li></ul></section></div></section><section class="area globo-servicos container analytics-area analytics-id-C"><h2 class="titulo">serviÃ§os</h2><div class="borda"><span class="rodape"></span></div><div class="content"><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-loterias
" 
    data-lotteries-url="http://api.globo.com/lotteries/megasena.jsonp"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-fipe
"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-zap
" 
    data-zap-url="http://api.globo.com/zap/search/"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"></div></div></section><section class="area widgets_inferior franja-inferior container analytics-area analytics-id-C"><div class="box-links-servicos"><div class="links"><p>MAIS SERVIÃOS</p><a href="http://revistapegn.globo.com/Franquias/compare/">Abra uma franquia</a><a href="http://globoesporte.globo.com/eu-atleta/calculo-imc.html">AvaliaÃ§Ã£o fÃ­sica</a><a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">Conversor de moedas</a><a href="http://www.techtudo.com.br/softwares/">Downloads</a><a href="http://g1.globo.com/previsao-do-tempo/">PrevisÃ£o do tempo</a><a href="http://g1.globo.com/radar-g1/platb/">TrÃ¢nsito</a></div></div><div id="glb-shopping-widget" class="area globoshopping container" data-url="http://vitrines.globo.com/vitrine/vitrine.min.js" data-sitepage="globocom/home" data-responsive="0" data-branding-color="0669DE"></div></section><section class="area diretorio"><div class="container analytics-area analytics-id-Z"><div class="diretorio-responsive"><a href="/todos-os-sites.html">veja todos os sites da <strong>globo.com</strong> âº</a></div><div id="glb-diretorio"><div class="glb-conteudo"><div class="glb-bloco"><div class="glb-grid-12"><ul class="diretorio-indice"><li class="diretorio-linha diretorio-first "><ul><li class="diretorio-first-level analytics-product glb-menu-g1 diretorio-quebra "><a title="g1" href="http://g1.globo.com/">g1</a><ul><li class="diretorio-second-level"><a title="Acre" href="http://g1.globo.com/ac/acre/">Acre</a></li><li class="diretorio-second-level"><a title="Alagoas" href="http://g1.globo.com/al/alagoas/">Alagoas</a></li><li class="diretorio-second-level"><a title="Amazonas" href="http://g1.globo.com/am/amazonas/">Amazonas</a></li><li class="diretorio-second-level"><a title="Bahia" href="http://g1.globo.com/bahia/?noAudience=true">Bahia</a></li><li class="diretorio-second-level"><a title="Bem Estar" href="http://g1.globo.com/bemestar/">Bem Estar</a></li><li class="diretorio-second-level"><a title="Bom Dia Brasil" href="http://g1.globo.com/bom-dia-brasil/">Bom Dia Brasil</a></li><li class="diretorio-second-level"><a title="Brasil" href="http://g1.globo.com/brasil/">Brasil</a></li><li class="diretorio-second-level"><a title="Carnaval" href="http://g1.globo.com/carnaval/2013/">Carnaval</a></li><li class="diretorio-second-level"><a title="Carros" href="http://g1.globo.com/carros/">Carros</a></li><li class="diretorio-second-level"><a title="CearÃ¡" href="http://g1.globo.com/ceara/">CearÃ¡</a></li><li class="diretorio-second-level"><a title="CiÃªncia e SaÃºde" href="http://g1.globo.com/ciencia-e-saude/">CiÃªncia e SaÃºde</a></li><li class="diretorio-second-level"><a title="Como SerÃ¡?" href="http://redeglobo.globo.com/como-sera/index.html">Como SerÃ¡?</a></li><li class="diretorio-second-level"><a title="Concursos e Emprego" href="http://g1.globo.com/concursos-e-emprego/">Concursos e Emprego</a></li><li class="diretorio-second-level"><a title="Distrito Federal" href="http://g1.globo.com/distrito-federal/">Distrito Federal</a></li><li class="diretorio-second-level"><a title="Economia" href="http://g1.globo.com/economia/">Economia</a></li><li class="diretorio-second-level"><a title="EducaÃ§Ã£o" href="http://www.educacao.globo.com/">EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="EspÃ­rito Santo" href="http://g1.globo.com/espirito-santo/">EspÃ­rito Santo</a></li><li class="diretorio-second-level"><a title="FantÃ¡stico" href="http://fantastico.globo.com/">FantÃ¡stico</a></li><li class="diretorio-second-level"><a title="G1 EducaÃ§Ã£o" href="http://g1.globo.com/educacao/">G1 EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Globo Natureza" href="http://g1.globo.com/natureza/">Globo Natureza</a></li><li class="diretorio-second-level"><a title="Globo News" href="http://globonews.globo.com/">Globo News</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://g1.globo.com/economia/agronegocios/">Globo Rural</a></li><li class="diretorio-second-level"><a title="Globo RepÃ³rter" href="http://g1.globo.com/globo-reporter/">Globo RepÃ³rter</a></li><li class="diretorio-second-level"><a title="GoiÃ¡s" href="http://g1.globo.com/goias/">GoiÃ¡s</a></li><li class="diretorio-second-level"><a title="Jornal da Globo" href="http://g1.globo.com/jornal-da-globo/">Jornal da Globo</a></li><li class="diretorio-second-level"><a title="Jornal Hoje" href="http://g1.globo.com/jornal-hoje/">Jornal Hoje</a></li><li class="diretorio-second-level"><a title="Jornal Nacional" href="http://g1.globo.com/jornal-nacional/">Jornal Nacional</a></li><li class="diretorio-second-level"><a title="MaranhÃ£o" href="http://g1.globo.com/ma/maranhao/">MaranhÃ£o</a></li><li class="diretorio-second-level"><a title="Mato Grosso" href="http://g1.globo.com/mato-grosso/">Mato Grosso</a></li><li class="diretorio-second-level"><a title="Mato Grosso do Sul" href="http://g1.globo.com/mato-grosso-do-sul/">Mato Grosso do Sul</a></li><li class="diretorio-second-level"><a title="Minas Gerais" href="http://g1.globo.com/minas-gerais/">Minas Gerais</a></li><li class="diretorio-second-level"><a title="Mundo" href="http://g1.globo.com/mundo/">Mundo</a></li><li class="diretorio-second-level"><a title="ParÃ¡" href="http://g1.globo.com/pa/para/">ParÃ¡</a></li><li class="diretorio-second-level"><a title="ParaÃ­ba" href="http://g1.globo.com/paraiba/">ParaÃ­ba</a></li><li class="diretorio-second-level"><a title="ParanÃ¡" href="http://g1.globo.com/parana/">ParanÃ¡</a></li><li class="diretorio-second-level"><a title="Pernambuco" href="http://g1.globo.com/pernambuco/">Pernambuco</a></li><li class="diretorio-second-level"><a title="PiauÃ­" href="http://g1.globo.com/pi/piaui/">PiauÃ­</a></li><li class="diretorio-second-level"><a title="Planeta Bizarro" href="http://g1.globo.com/planeta-bizarro/">Planeta Bizarro</a></li><li class="diretorio-second-level"><a title="PolÃ­tica" href="http://g1.globo.com/politica/">PolÃ­tica</a></li><li class="diretorio-second-level"><a title="Pop &amp; Arte" href="http://g1.globo.com/pop-arte/">Pop &amp; Arte</a></li><li class="diretorio-second-level"><a title="PrevisÃ£o do Tempo" href="http://g1.globo.com/previsao-do-tempo.html">PrevisÃ£o do Tempo</a></li><li class="diretorio-second-level"><a title="ProfissÃ£o RepÃ³rter" href="http://g1.globo.com/profissao-reporter/">ProfissÃ£o RepÃ³rter</a></li><li class="diretorio-second-level"><a title="Rio de Janeiro" href="http://g1.globo.com/rio-de-janeiro/">Rio de Janeiro</a></li><li class="diretorio-second-level"><a title="Rio Grande do Norte" href="http://g1.globo.com/rn/rio-grande-do-norte/">Rio Grande do Norte</a></li><li class="diretorio-second-level"><a title="Rio Grande do Sul" href="http://g1.globo.com/rs/rio-grande-do-sul/">Rio Grande do Sul</a></li><li class="diretorio-second-level"><a title="RondÃ´nia" href="http://g1.globo.com/ro/rondonia/">RondÃ´nia</a></li><li class="diretorio-second-level"><a title="Santa Catarina" href="http://g1.globo.com/sc/santa-catarina/">Santa Catarina</a></li><li class="diretorio-second-level"><a title="SÃ£o Paulo" href="http://g1.globo.com/sao-paulo/">SÃ£o Paulo</a></li><li class="diretorio-second-level"><a title="Sergipe" href="http://g1.globo.com/se/sergipe/">Sergipe</a></li><li class="diretorio-second-level"><a title="Serra, Lagos e Norte" href="http://g1.globo.com/rj/serra-lagos-norte/">Serra, Lagos e Norte</a></li><li class="diretorio-second-level"><a title="Tecnologia e Games" href="http://g1.globo.com/tecnologia/">Tecnologia e Games</a></li><li class="diretorio-second-level"><a title="Turismo e Viagem" href="http://g1.globo.com/turismo-e-viagem/">Turismo e Viagem</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-globoesporte diretorio-quebra "><a title="globoesporte" href="http://globoesporte.globo.com/">globoesporte</a><ul><li class="diretorio-second-level"><a title="Atletismo" href="http://globoesporte.globo.com/atletismo/">Atletismo</a></li><li class="diretorio-second-level"><a title="Basquete" href="http://globoesporte.globo.com/basquete/">Basquete</a></li><li class="diretorio-second-level"><a title="Basquete/NBB" href="http://globoesporte.globo.com/basquete/nbb/">Basquete/NBB</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie A" href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">BrasileirÃ£o SÃ©rie A</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie B" href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">BrasileirÃ£o SÃ©rie B</a></li><li class="diretorio-second-level"><a title="Campeonato Carioca" href="http://globoesporte.globo.com/rj/futebol/campeonato-carioca/index.html">Campeonato Carioca</a></li><li class="diretorio-second-level"><a title="Campeonato GaÃºcho" href="http://globoesporte.globo.com/rs/futebol/campeonato-gaucho/index.html">Campeonato GaÃºcho</a></li><li class="diretorio-second-level"><a title="Campeonato Mineiro" href="http://globoesporte.globo.com/mg/futebol/campeonato-mineiro/index.html">Campeonato Mineiro</a></li><li class="diretorio-second-level"><a title="Campeonato Paulista" href="http://globoesporte.globo.com/sp/futebol/campeonato-paulista/index.html">Campeonato Paulista</a></li><li class="diretorio-second-level"><a title="Cartola FC" href="http://globoesporte.globo.com/cartola-fc/">Cartola FC</a></li><li class="diretorio-second-level"><a title="Copa do Mundo da Fifa" href="http://globoesporte.globo.com/futebol/copa-do-mundo/index.html">Copa do Mundo da Fifa</a></li><li class="diretorio-second-level"><a title="Copa AmÃ©rica" href="http://globoesporte.globo.com/futebol/copa-america/">Copa AmÃ©rica</a></li><li class="diretorio-second-level"><a title="Copa Sul-Americana" href="http://globoesporte.globo.com/futebol/copa-sul-americana/index.html">Copa Sul-Americana</a></li><li class="diretorio-second-level"><a title="Desafio de Futsal" href="http://globoesporte.globo.com/eventos/desafio-de-futsal/">Desafio de Futsal</a></li><li class="diretorio-second-level"><a title="Esporte Espetacular" href="http://globoesporte.globo.com/programas/esporte-espetacular/">Esporte Espetacular</a></li><li class="diretorio-second-level"><a title="Esportes AquÃ¡ticos" href="http://globoesporte.globo.com/aquaticos/">Esportes AquÃ¡ticos</a></li><li class="diretorio-second-level"><a title="Esportes Radicais" href="http://globoesporte.globo.com/radicais/">Esportes Radicais</a></li><li class="diretorio-second-level"><a title="Eu Atleta" href="http://globoesporte.globo.com/eu-atleta/">Eu Atleta</a></li><li class="diretorio-second-level"><a title="FÃ³rmula 1" href="http://globoesporte.globo.com/motor/formula-1/">FÃ³rmula 1</a></li><li class="diretorio-second-level"><a title="Futebol" href="http://globoesporte.globo.com/futebol/">Futebol</a></li><li class="diretorio-second-level"><a title="Futebol Internacional" href="http://globoesporte.globo.com/futebol/futebol-internacional/">Futebol Internacional</a></li><li class="diretorio-second-level"><a title="FutpÃ©dia" href="http://futpedia.globo.com/">FutpÃ©dia</a></li><li class="diretorio-second-level"><a title="Game Futebol" href="http://gamefutebol.globoesporte.globo.com/">Game Futebol</a></li><li class="diretorio-second-level"><a title="Liga dos CampeÃµes" href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">Liga dos CampeÃµes</a></li><li class="diretorio-second-level"><a title="OlimpÃ­adas" href="http://globoesporte.globo.com/olimpiadas/">OlimpÃ­adas</a></li><li class="diretorio-second-level"><a title="SeleÃ§Ã£o Brasileira" href="http://globoesporte.globo.com/futebol/selecao-brasileira/">SeleÃ§Ã£o Brasileira</a></li><li class="diretorio-second-level"><a title="StockCar" href="http://globoesporte.globo.com/motor/stock-car/">StockCar</a></li><li class="diretorio-second-level"><a title="Surfe" href="http://globoesporte.globo.com/radicais/surfe/">Surfe</a></li><li class="diretorio-second-level"><a title="TÃªnis" href="http://globoesporte.globo.com/tenis/">TÃªnis</a></li><li class="diretorio-second-level"><a title="Vai-e-vem do Mercado" href="http://globoesporte.globo.com/futebol/vai-e-vem-do-mercado/">Vai-e-vem do Mercado</a></li><li class="diretorio-second-level"><a title="VerÃ£o Espetacular" href="http://globoesporte.globo.com/programas/verao-espetacular/">VerÃ£o Espetacular</a></li><li class="diretorio-second-level"><a title="VÃ´lei" href="http://globoesporte.globo.com/volei/">VÃ´lei</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-gshow diretorio-quebra "><a title="gshow" href="http://gshow.globo.com/">gshow</a><ul><li class="diretorio-second-level"><a title="A Regra do Jogo" href="http://gshow.globo.com/novelas/a-regra-do-jogo/">A Regra do Jogo</a></li><li class="diretorio-second-level"><a title="AlÃ©m do Tempo" href="http://gshow.globo.com/novelas/alem-do-tempo/">AlÃ©m do Tempo</a></li><li class="diretorio-second-level"><a title="Altas Horas" href="http://altashoras.globo.com/">Altas Horas</a></li><li class="diretorio-second-level"><a title="Amor &amp; Sexo" href="http://amoresexo.globo.com/">Amor &amp; Sexo</a></li><li class="diretorio-second-level"><a title="Bastidores" href="http://gshow.globo.com/Bastidores/">Bastidores</a></li><li class="diretorio-second-level"><a title="BBB" href="http://bbb.globo.com/">BBB</a></li><li class="diretorio-second-level"><a title="CaldeirÃ£o do Huck" href="http://caldeiraodohuck.globo.com/">CaldeirÃ£o do Huck</a></li><li class="diretorio-second-level"><a title="Caminho das Ãndias" href="http://gshow.globo.com/novelas/caminho-das-indias/">Caminho das Ãndias</a></li><li class="diretorio-second-level"><a title="Como Fazer" href="http://gshow.globo.com/como-fazer/">Como Fazer</a></li><li class="diretorio-second-level"><a title="DomingÃ£o do FaustÃ£o" href="http://domingaodofaustao.globo.com/">DomingÃ£o do FaustÃ£o</a></li><li class="diretorio-second-level"><a title="Ã de Casa" href="http://gshow.globo.com/programas/e-de-casa/">Ã de Casa</a></li><li class="diretorio-second-level"><a title="Encontro com FÃ¡tima Bernardes" href="http://tvg.globo.com/programas/encontro-com-fatima-bernardes/">Encontro com FÃ¡tima Bernardes</a></li><li class="diretorio-second-level"><a title="Escolinha do Professor Raimundo" href="http://gshow.globo.com/programas/escolinha-do-professor-raimundo/">Escolinha do Professor Raimundo</a></li><li class="diretorio-second-level"><a title="Esquenta!" href="http://tvg.globo.com/programas/esquenta/">Esquenta!</a></li><li class="diretorio-second-level"><a title="Estilo" href="http://gshow.globo.com/Estilo/">Estilo</a></li><li class="diretorio-second-level"><a title="Estrelas" href="http://estrelas.globo.com/">Estrelas</a></li><li class="diretorio-second-level"><a title="LigaÃ§Ãµes Perigosas" href="http://gshow.globo.com/series/ligacoes-perigosas/">LigaÃ§Ãµes Perigosas</a></li><li class="diretorio-second-level"><a title="Mais VocÃª" href="http://maisvoce.globo.com/">Mais VocÃª</a></li><li class="diretorio-second-level"><a title="MalhaÃ§Ã£o" href="http://malhacao.globo.com/">MalhaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Mister Brau" href="http://gshow.globo.com/series/mister-brau/2015/">Mister Brau</a></li><li class="diretorio-second-level"><a title="MÃºsica" href="http://gshow.globo.com/Musica/">MÃºsica</a></li><li class="diretorio-second-level"><a title="PÃ© na Cova" href="http://gshow.globo.com/series/pe-na-cova/2015/">PÃ© na Cova</a></li><li class="diretorio-second-level"><a title="Programa do JÃ´" href="http://programadojo.globo.com/">Programa do JÃ´</a></li><li class="diretorio-second-level"><a title="Receitas.com" href="http://www.receitas.com/">Receitas.com</a></li><li class="diretorio-second-level"><a title="Receitas da Ana Maria" href="http://gshow.globo.com/programas/mais-voce/Receitas-da-Ana-Maria/">Receitas da Ana Maria</a></li><li class="diretorio-second-level"><a title="SÃ©ries Originais" href="http://gshow.globo.com/webseries/">SÃ©ries Originais</a></li><li class="diretorio-second-level"><a title="SessÃ£o ComÃ©dia" href="http://gshow.globo.com/sessao-comedia/videos/">SessÃ£o ComÃ©dia</a></li><li class="diretorio-second-level"><a title="TÃ¡ no Ar" href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/index.html">TÃ¡ no Ar</a></li><li class="diretorio-second-level"><a title="The Voice Brasil" href="http://gshow.globo.com/realities/the-voice-brasil/">The Voice Brasil</a></li><li class="diretorio-second-level"><a title="The Voice Kids" href="http://gshow.globo.com/programas/the-voice-kids/">The Voice Kids</a></li><li class="diretorio-second-level"><a title="Totalmente Demais" href="http://gshow.globo.com/novelas/totalmente-demais/">Totalmente Demais</a></li><li class="diretorio-second-level"><a title="Tomara que Caia" href="http://gshow.globo.com/programas/tomara-que-caia/">Tomara que Caia</a></li><li class="diretorio-second-level"><a title="Video Show" href="http://videoshow.globo.com/">Video Show</a></li><li class="diretorio-second-level"><a title="Zorra" href="http://gshow.globo.com/programas/zorra/">Zorra</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-famosos-etc diretorio-quebra "><a title="famosos &amp; etc" href="http://famosos.globo.com/">famosos &amp; etc</a><ul><li class="diretorio-second-level"><a title="EGO" href="http://ego.globo.com/">EGO</a></li><li class="diretorio-second-level"><a title="G1 Cinema" href="http://g1.globo.com/pop-arte/cinema/">G1 Cinema</a></li><li class="diretorio-second-level"><a title="G1 MÃºsica" href="http://g1.globo.com/pop-arte/musica/">G1 MÃºsica</a></li><li class="diretorio-second-level"><a title="Globo Jogos" href="http://jogos.globo.com/">Globo Jogos</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="HorÃ³scopo" href="http://horoscopo.ego.globo.com/ne/home.html">HorÃ³scopo</a></li><li class="diretorio-second-level"><a title="Meus 5 minutos" href="http://www.meus5minutos.com.br/">Meus 5 minutos</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="Namoro na Web" href="http://tracking.parperfeito.com.br/ppbanner/bannerTracker?originId=13&amp;identifierId=41763&amp;actionId=1">Namoro na Web</a></li><li class="diretorio-second-level"><a title="Paparazzo" href="http://paparazzo.globo.com/">Paparazzo</a></li><li class="diretorio-second-level"><a title="Patricia Kogut" href="http://oglobo.globo.com/cultura/kogut/">Patricia Kogut</a></li><li class="diretorio-second-level"><a title="Retratos da Vida" href="http://extra.globo.com/lazer/retratosdavida/">Retratos da Vida</a></li><li class="diretorio-second-level"><a title="Senninha" href="http://senninha.globo.com/">Senninha</a></li><li class="diretorio-second-level"><a title="Teatro" href="http://www.agentesevenoteatro.com.br/">Teatro</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-globo-play diretorio-sem-quebra "><a title="globo play" href="http://globoplay.globo.com/">globo play</a></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-grupo-globo diretorio-quebra "><a title="Grupo Globo" href="http://grupoglobo.globo.com/">Grupo Globo</a><ul><li class="diretorio-second-level"><a title="princÃ­pios editoriais" href="http://g1.globo.com/principios-editoriais-do-grupo-globo.html">princÃ­pios editoriais</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-editora-globo diretorio-sem-quebra "><a title="editora globo" href="http://editoraglobo.globo.com/">editora globo</a><ul><li class="diretorio-second-level"><a title="Auto Esporte" href="http://revistaautoesporte.globo.com/">Auto Esporte</a></li><li class="diretorio-second-level"><a title="Casa e Comida" href="http://revistacasaejardim.globo.com/Casa-e-Comida/">Casa e Comida</a></li><li class="diretorio-second-level"><a title="Casa e Jardim" href="http://revistacasaejardim.globo.com/">Casa e Jardim</a></li><li class="diretorio-second-level"><a title="Casa Vogue" href="http://casavogue.globo.com/">Casa Vogue</a></li><li class="diretorio-second-level"><a title="Crescer" href="http://revistacrescer.globo.com/">Crescer</a></li><li class="diretorio-second-level"><a title="Ãpoca" href="http://revistaepoca.globo.com/">Ãpoca</a></li><li class="diretorio-second-level"><a title="Ãpoca NegÃ³cios" href="http://epocanegocios.globo.com/">Ãpoca NegÃ³cios</a></li><li class="diretorio-second-level"><a title="Galileu" href="http://revistagalileu.globo.com/">Galileu</a></li><li class="diretorio-second-level"><a title="Glamour" href="http://revistaglamour.globo.com/">Glamour</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://revistagloborural.globo.com/">Globo Rural</a></li><li class="diretorio-second-level"><a title="GQ Brasil" href="http://gq.globo.com/">GQ Brasil</a></li><li class="diretorio-second-level"><a title="Marie Claire" href="http://revistamarieclaire.globo.com/">Marie Claire</a></li><li class="diretorio-second-level"><a title="Meus 5 Minutos" href="http://meus5minutos.globo.com/">Meus 5 Minutos</a></li><li class="diretorio-second-level"><a title="Monet" href="http://revistamonet.globo.com/">Monet</a></li><li class="diretorio-second-level"><a title="Mundo do SÃ­tio" href="http://mundodositio.globo.com/">Mundo do SÃ­tio</a></li><li class="diretorio-second-level"><a title="PEGN" href="http://revistapegn.globo.com/">PEGN</a></li><li class="diretorio-second-level"><a title="Quem" href="http://revistaquem.globo.com/">Quem</a></li><li class="diretorio-second-level"><a title="Vogue" href="http://vogue.globo.com/">Vogue</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-infoglobo diretorio-sem-quebra "><a title="infoglobo" href="http://infoglobo.com.br/Anuncie/Home.aspx">infoglobo</a><ul><li class="diretorio-second-level"><a title="Ela Digital" href="http://ela.oglobo.globo.com/">Ela Digital</a></li><li class="diretorio-second-level"><a title="Extra" href="http://extra.globo.com/">Extra</a></li><li class="diretorio-second-level"><a title="O Globo" href="http://oglobo.globo.com/">O Globo</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-last "><ul><li class="diretorio-first-level analytics-product glb-menu-globosat diretorio-quebra "><a title="globosat" href="http://canaisglobosat.globo.com/">globosat</a><ul><li class="diretorio-second-level"><a title="Globosat Play" href="http://globosatplay.globo.com/">Globosat Play</a></li><li class="diretorio-second-level"><a title="+Globosat" href="http://maisglobosat.globo.com/">+Globosat</a></li><li class="diretorio-second-level"><a title="BIS" href="http://canalbis.globo.com/">BIS</a></li><li class="diretorio-second-level"><a title="Canal Brasil" href="http://canalbrasil.globo.com/">Canal Brasil</a></li><li class="diretorio-second-level"><a title="Canal Futura" href="http://www.futura.org.br/">Canal Futura</a></li><li class="diretorio-second-level"><a title="Combate" href="http://combate.globo.com/">Combate</a></li><li class="diretorio-second-level"><a title="GloboNews" href="http://globonews.globo.com/">GloboNews</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="Megapix" href="http://megapix.globo.com/">Megapix</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="Off" href="http://canaloff.globo.com/">Off</a></li><li class="diretorio-second-level"><a title="Premiere" href="http://sociopremiere.globo.com/">Premiere</a></li><li class="diretorio-second-level"><a title="SporTV" href="http://sportv.globo.com/">SporTV</a></li><li class="diretorio-second-level"><a title="Studio Universal" href="http://studiouniversal.globo.com/">Studio Universal</a></li><li class="diretorio-second-level"><a title="Syfy" href="http://syfy.globo.com/">Syfy</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Universal Channel" href="http://uc.globo.com/">Universal Channel</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-sistema-globo-de-radio diretorio-sem-quebra "><a title="sistema globo de rÃ¡dio" href="http://globoradio.globo.com/home/HOME.htm">sistema globo de rÃ¡dio</a><ul><li class="diretorio-second-level"><a title="BHFM" href="http://bhfm.globoradio.globo.com/home/HOME.htm">BHFM</a></li><li class="diretorio-second-level"><a title="CBN" href="http://cbn.globoradio.globo.com/home/HOME.htm">CBN</a></li><li class="diretorio-second-level"><a title="RADIOBEAT" href="http://radiobeat.com.br/">RADIOBEAT</a></li><li class="diretorio-second-level"><a title="RÃ¡dio Globo" href="http://radioglobo.globoradio.globo.com/home/HOME.htm">RÃ¡dio Globo</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-globo-filmes diretorio-sem-quebra "><a title="globo filmes" href="http://globofilmes.globo.com/">globo filmes</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-som-livre diretorio-sem-quebra "><a title="som livre" href="http://www.somlivre.com/">som livre</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-blog-globocom diretorio-sem-quebra "><a title="blog globo.com" href="http://blog.globo.com/1.html">blog globo.com</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-zap-imoveis diretorio-sem-quebra "><a title="zap imÃ³veis" href="http://www.zapimoveis.com.br/">zap imÃ³veis</a></li></ul></li></ul></div></div></div></div></div></section><div id="x61" class="opec-area opec-mobile opec-x61 grid-12"><div id="banner_mobile_fim" class="tag-manager-publicidade-container"></div></div><footer><div class="container analytics-area analytics-id-B"><a href="http://www.globo.com/" title="Globo.com">Globo.com</a><span class="copyright">&copy; Copyright 2000-2015 Globo ComunicaÃ§Ã£o e ParticipaÃ§Ãµes S.A.</span><span class="copyright-480 hidden"><p>&copy; 2000-2015.</p><p>Todos os direitos reservados.</p></span><ul class="footer-menu"><li><a href="/privacidade.html">polÃ­tica de privacidade e seguranÃ§a</a></li><li><a href="/todos-os-sites.html">todos os sites</a></li><li><a href="http://anuncie.globo.com/">anuncie conosco</a></li><li><a href="http://meuperfil.globo.com">central globo.com</a></li></ul></div></footer></div><!--[if lt IE 9]><script src="http://s.glbimg.com/en/ho/static/libby/css3pie/js/PIE.js" type="text/javascript"></script><![endif]--><script>
responsiveHub.updateImages();</script><script>
(function(){var scr=document.createElement('script');scr.type = 'text/javascript';scr.async=true;scr.src = 'http://s.glbimg.com/en/ho/static/CACHE/js/c9570221d3da.js';document.getElementsByTagName('head')[0].appendChild(scr);}());</script><div id="opec-frame1"><div id="banner_floating" class="tag-manager-publicidade-container"></div></div><script>if(document.body && document.body.offsetWidth <= 711){}else{setTimeout(function(){var bannerMiddleTop = $("#ad-position-middle"),
bannerMiddleBottom = $('#ad-position-middle3');if(bannerMiddleTop.height() < 15){bannerMiddleTop.addClass('without-opec');}if(bannerMiddleBottom.height() < 15){bannerMiddleBottom.addClass('without-opec');}}, 6000);}
</script><script>
window.glb=window.glb || {};window.glb.analyticsConfig=window.glb.analyticsConfig  || {};window.glb.analyticsConfig = {scrollEventChance: 0};</script><script>
document.cookie = 'globoapi.geo.geolocation=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.estado=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.novela=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.time=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';setTimeout(function(){window.glb.barra=window.glb.barra || {};window.glb.barra.component=window.glb.barra.component || {};window.glb.barra.component.barraDiv = {className: ''};}, 1000);</script><style>
#assinante-menu .barra-notifications-area .float-box.config-active .save-area{bottom:0;position:absolute;width:100%}#assinante-menu .barra-notifications-area .float-box.config-active .config-box{padding-bottom:44px}</style><style>
header .container{height:150px}.box-destaque-uber .destaques-primario .titulo{font-family:"ProximaNovaA-Bold"}@media (min-width:1600px){.conteudo-uber .box-destaque-uber{position:static;max-width:1600px;margin:0 auto}}</style><style>
.agrupador-quadruplo-tecnologia .destaque-inferior ul{display:none;// css de folder}</style></body></html>
<!-- PÃ¡gina gerada em 17/11/2015 16:36:52 -->
