<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="auto">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-c634124.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-c634124.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-c634124.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-c634124.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kickass.to/sc-c634124.js');

        sc('setHost', 'a.kickass.to');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: 'c634124',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-c634124.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<div id="_60318cd4e8d28f6fb76fe34e9bd9c498"></div>
<div id="_39ecb76dd457e5ac33776fdf11500d56"></div>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka ka-settings"></i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka ka-search"></i></button></div>
			</form>
		</div>
        <div id="_277923e5f9d753c5b0630c28e641790c"></div>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080p/" class="tag5">1080p</a>
	<a href="/search/2014/" class="tag2">2014</a>
	<a href="/search/2015/" class="tag10">2015</a>
	<a href="/search/3d/" class="tag3">3d</a>
	<a href="/search/agent%20x/" class="tag2">agent x</a>
	<a href="/search/android/" class="tag2">android</a>
	<a href="/search/ant%20man/" class="tag2">ant man</a>
	<a href="/search/christmas/" class="tag3">christmas</a>
	<a href="/search/czech%20streets/" class="tag3">czech streets</a>
	<a href="/search/discography/" class="tag3">discography</a>
	<a href="/search/doctor%20who/" class="tag2">doctor who</a>
	<a href="/search/dual%20audio%20hindi/" class="tag2">dual audio hindi</a>
	<a href="/search/fallout%204/" class="tag3">fallout 4</a>
	<a href="/search/family%20guy/" class="tag2">family guy</a>
	<a href="/search/french/" class="tag3">french</a>
	<a href="/search/hindi/" class="tag7">hindi</a>
	<a href="/search/hindi%202015/" class="tag5">hindi 2015</a>
	<a href="/search/homeland/" class="tag3">homeland</a>
	<a href="/search/homeland%20s05e07/" class="tag3">homeland s05e07</a>
	<a href="/search/into%20the%20badlands/" class="tag2">into the badlands</a>
	<a href="/search/ita/" class="tag2">ita</a>
	<a href="/search/malayalam/" class="tag3">malayalam</a>
	<a href="/search/movies/" class="tag2">movies</a>
	<a href="/search/nezu/" class="tag10">nezu</a>
	<a href="/search/nl/" class="tag2">nl</a>
	<a href="/search/once%20upon%20a%20time/" class="tag3">once upon a time</a>
	<a href="/search/once%20upon%20a%20time%20s05e08/" class="tag2">once upon a time s05e08</a>
	<a href="/search/one%20punch%20man/" class="tag2">one punch man</a>
	<a href="/search/prem%20ratan%20dhan%20payo/" class="tag2">prem ratan dhan payo</a>
	<a href="/search/quantico/" class="tag3">quantico</a>
	<a href="/search/quantico%20s01e08/" class="tag2">quantico s01e08</a>
	<a href="/search/ripsalot/" class="tag4">ripsalot</a>
	<a href="/search/spectre/" class="tag3">spectre</a>
	<a href="/search/star%20wars/" class="tag2">star wars</a>
	<a href="/search/tamil/" class="tag4">tamil</a>
	<a href="/search/telugu/" class="tag3">telugu</a>
	<a href="/search/telugu%202015/" class="tag2">telugu 2015</a>
	<a href="/search/the%20affair/" class="tag2">the affair</a>
	<a href="/search/the%20blacklist/" class="tag2">the blacklist</a>
	<a href="/search/the%20martian/" class="tag2">the martian</a>
	<a href="/search/the%20walking%20dead/" class="tag10">the walking dead</a>
	<a href="/search/the%20walking%20dead%20s06/" class="tag2">the walking dead s06</a>
	<a href="/search/the%20walking%20dead%20s06e06/" class="tag5">the walking dead s06e06</a>
	<a href="/search/walking%20dead/" class="tag5">walking dead</a>
	<a href="/search/walking%20dead%20s06e06/" class="tag2">walking dead s06e06</a>
	<a href="/search/yify/" class="tag10">yify</a>
	<a href="/search/yify%201080p/" class="tag4">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag5">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11601819,0" class="icommentjs kaButton smallButton rightButton" href="/maze-runner-the-scorch-trials-2015-dvdrip-xvid-ac3-etrg-t11601819.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/maze-runner-the-scorch-trials-2015-dvdrip-xvid-ac3-etrg-t11601819.html" class="cellMainLink">Maze Runner The Scorch Trials 2015.DVDRip.XViD.AC3-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="1509876763">1.41 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T22:29:16+00:00">16 Nov 2015, 22:29:16</span></td>
			<td class="green center">2610</td>
			<td class="red lasttd center">5650</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11590075,0" class="icommentjs kaButton smallButton rightButton" href="/love-2015-720p-webrip-aac2-0-h-264-gaspar-noe-t11590075.html#comment">30 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/love-2015-720p-webrip-aac2-0-h-264-gaspar-noe-t11590075.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/love-2015-720p-webrip-aac2-0-h-264-gaspar-noe-t11590075.html" class="cellMainLink">Love (2015) 720P WEBRip AAC2 0 H 264 (Gaspar Noe)</a></div>
			</td>
			<td class="nobr center" data-sort="1626731834">1.52 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T01:44:33+00:00">15 Nov 2015, 01:44:33</span></td>
			<td class="green center">2739</td>
			<td class="red lasttd center">4067</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11596526,0" class="icommentjs kaButton smallButton rightButton" href="/prem-ratan-dhan-payo-2015-720p-desi-dvdscr-rip-x264-aac-marhaba-t11596526.html#comment">22 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/prem-ratan-dhan-payo-2015-720p-desi-dvdscr-rip-x264-aac-marhaba-t11596526.html" class="cellMainLink">Prem Ratan Dhan Payo (2015) 720p Desi DVDSCR Rip x264 AAC - Marhaba</a></div>
			</td>
			<td class="nobr center" data-sort="2528411270">2.35 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T05:06:05+00:00">16 Nov 2015, 05:06:05</span></td>
			<td class="green center">1673</td>
			<td class="red lasttd center">5580</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11592908,0" class="icommentjs kaButton smallButton rightButton" href="/hitman-agent-47-2015-dvdrip-xvid-etrg-t11592908.html#comment">47 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/hitman-agent-47-2015-dvdrip-xvid-etrg-t11592908.html" class="cellMainLink">Hitman Agent 47 2015 DVDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="739794661">705.52 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T14:55:30+00:00">15 Nov 2015, 14:55:30</span></td>
			<td class="green center">3009</td>
			<td class="red lasttd center">2875</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11594344,0" class="icommentjs kaButton smallButton rightButton" href="/landmine-goes-click-2015-hdrip-xvid-etrg-t11594344.html#comment">20 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/landmine-goes-click-2015-hdrip-xvid-etrg-t11594344.html" class="cellMainLink">Landmine Goes Click 2015 HDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="747193401">712.58 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T20:28:13+00:00">15 Nov 2015, 20:28:13</span></td>
			<td class="green center">1825</td>
			<td class="red lasttd center">1790</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11587331,0" class="icommentjs kaButton smallButton rightButton" href="/the-hobbit-the-battle-of-the-five-armies-2014-extended-1080p-bluray-x264-dts-jyk-t11587331.html#comment">51 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-hobbit-the-battle-of-the-five-armies-2014-extended-1080p-bluray-x264-dts-jyk-t11587331.html" class="cellMainLink">The Hobbit The Battle of the Five Armies 2014 EXTENDED 1080p BluRay x264 DTS-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="4438346960">4.13 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T13:09:22+00:00">14 Nov 2015, 13:09:22</span></td>
			<td class="green center">1856</td>
			<td class="red lasttd center">1447</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11587454,0" class="icommentjs kaButton smallButton rightButton" href="/ant-man-2015-repack-hdrip-xvid-etrg-t11587454.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ant-man-2015-repack-hdrip-xvid-etrg-t11587454.html" class="cellMainLink">Ant Man 2015 REPACK HDRip XviD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="744137374">709.66 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T13:43:51+00:00">14 Nov 2015, 13:43:51</span></td>
			<td class="green center">1132</td>
			<td class="red lasttd center">1149</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11603794,0" class="icommentjs kaButton smallButton rightButton" href="/90-minutes-in-heaven-2015-hdrip-xvid-etrg-t11603794.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/90-minutes-in-heaven-2015-hdrip-xvid-etrg-t11603794.html" class="cellMainLink">90 Minutes in Heaven 2015 HDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="740809059">706.49 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T08:25:37+00:00">17 Nov 2015, 08:25:37</span></td>
			<td class="green center">855</td>
			<td class="red lasttd center">1589</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11587027,0" class="icommentjs kaButton smallButton rightButton" href="/lemon-tree-passage-2014-dvdrip-xvid-ac3-evo-t11587027.html#comment">19 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/lemon-tree-passage-2014-dvdrip-xvid-ac3-evo-t11587027.html" class="cellMainLink">Lemon Tree Passage 2014 DVDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="951628901">907.54 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T12:15:22+00:00">14 Nov 2015, 12:15:22</span></td>
			<td class="green center">1044</td>
			<td class="red lasttd center">781</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11597754,0" class="icommentjs kaButton smallButton rightButton" href="/if-there-be-thorns-2015-hdrip-xvid-ac3-evo-t11597754.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/if-there-be-thorns-2015-hdrip-xvid-ac3-evo-t11597754.html" class="cellMainLink">If There Be Thorns 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1484306695">1.38 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T10:53:37+00:00">16 Nov 2015, 10:53:37</span></td>
			<td class="green center">866</td>
			<td class="red lasttd center">1064</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11587419,0" class="icommentjs kaButton smallButton rightButton" href="/assassination-2015-720p-brrip-x264-korean-aac-etrg-t11587419.html#comment">13 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/assassination-2015-720p-brrip-x264-korean-aac-etrg-t11587419.html" class="cellMainLink">Assassination 2015 720p BRRip x264 Korean AAC-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="1102502462">1.03 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T13:37:06+00:00">14 Nov 2015, 13:37:06</span></td>
			<td class="green center">873</td>
			<td class="red lasttd center">824</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11600856,0" class="icommentjs kaButton smallButton rightButton" href="/rasputin-2015-malayalam-dvdrip-x264-1cd-700mb-esubs-t11600856.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/rasputin-2015-malayalam-dvdrip-x264-1cd-700mb-esubs-t11600856.html" class="cellMainLink">Rasputin [2015] Malayalam DVDRip x264 1CD 700MB ESubs</a></div>
			</td>
			<td class="nobr center" data-sort="732080084">698.17 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T18:00:41+00:00">16 Nov 2015, 18:00:41</span></td>
			<td class="green center">717</td>
			<td class="red lasttd center">896</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11593612,0" class="icommentjs kaButton smallButton rightButton" href="/pyaar-ka-punchnama-2-2015-dvdscr-with-clear-audio-by-men-t11593612.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/pyaar-ka-punchnama-2-2015-dvdscr-with-clear-audio-by-men-t11593612.html" class="cellMainLink">Pyaar Ka Punchnama 2 (2015) DVDSCR With Clear Audio By *MEN*</a></div>
			</td>
			<td class="nobr center" data-sort="647501069">617.51 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T18:01:54+00:00">15 Nov 2015, 18:01:54</span></td>
			<td class="green center">636</td>
			<td class="red lasttd center">982</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11598499,0" class="icommentjs kaButton smallButton rightButton" href="/three-kings-1999-720p-bluray-x264-aac-etrg-t11598499.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/three-kings-1999-720p-bluray-x264-aac-etrg-t11598499.html" class="cellMainLink">Three Kings 1999 720p BluRay x264 AAC-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="904101958">862.22 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T13:52:21+00:00">16 Nov 2015, 13:52:21</span></td>
			<td class="green center">588</td>
			<td class="red lasttd center">844</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11588979,0" class="icommentjs kaButton smallButton rightButton" href="/national-lampoons-van-wilder-unrated-2002-1080p-bluray-x264-aac-etrg-t11588979.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/national-lampoons-van-wilder-unrated-2002-1080p-bluray-x264-aac-etrg-t11588979.html" class="cellMainLink">National Lampoons Van Wilder Unrated 2002 1080p BluRay x264 AAC-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="1476815332">1.38 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T18:33:54+00:00">14 Nov 2015, 18:33:54</span></td>
			<td class="green center">429</td>
			<td class="red lasttd center">637</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11602387,0" class="icommentjs kaButton smallButton rightButton" href="/gotham-s02e09-hdtv-x264-lol-ettv-t11602387.html#comment">114 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/gotham-s02e09-hdtv-x264-lol-ettv-t11602387.html" class="cellMainLink">Gotham S02E09 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="243121921">231.86 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T01:59:21+00:00">17 Nov 2015, 01:59:21</span></td>
			<td class="green center">12858</td>
			<td class="red lasttd center">5733</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11602391,0" class="icommentjs kaButton smallButton rightButton" href="/supergirl-s01e05-hdtv-x264-lol-ettv-t11602391.html#comment">126 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/supergirl-s01e05-hdtv-x264-lol-ettv-t11602391.html" class="cellMainLink">Supergirl S01E05 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="306836370">292.62 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T02:00:19+00:00">17 Nov 2015, 02:00:19</span></td>
			<td class="green center">11410</td>
			<td class="red lasttd center">6226</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11602592,0" class="icommentjs kaButton smallButton rightButton" href="/blindspot-s01e09-hdtv-x264-lol-ettv-t11602592.html#comment">72 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/blindspot-s01e09-hdtv-x264-lol-ettv-t11602592.html" class="cellMainLink">Blindspot S01E09 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="296287258">282.56 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T03:00:15+00:00">17 Nov 2015, 03:00:15</span></td>
			<td class="green center">8601</td>
			<td class="red lasttd center">4591</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11596170,0" class="icommentjs kaButton smallButton rightButton" href="/quantico-s01e08-hdtv-x264-lol-ettv-t11596170.html#comment">116 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/quantico-s01e08-hdtv-x264-lol-ettv-t11596170.html" class="cellMainLink">Quantico S01E08 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="274357200">261.65 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T03:00:13+00:00">16 Nov 2015, 03:00:13</span></td>
			<td class="green center">7144</td>
			<td class="red lasttd center">1653</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11596057,0" class="icommentjs kaButton smallButton rightButton" href="/homeland-s05e07-web-dl-x264-fum-ettv-t11596057.html#comment">58 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/homeland-s05e07-web-dl-x264-fum-ettv-t11596057.html" class="cellMainLink">Homeland S05E07 WEB-DL x264-FUM[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="302535274">288.52 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T02:12:18+00:00">16 Nov 2015, 02:12:18</span></td>
			<td class="green center">7188</td>
			<td class="red lasttd center">965</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11602570,0" class="icommentjs kaButton smallButton rightButton" href="/scorpion-s02e09-hdtv-x264-lol-ettv-t11602570.html#comment">36 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/scorpion-s02e09-hdtv-x264-lol-ettv-t11602570.html" class="cellMainLink">Scorpion S02E09 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="296675058">282.93 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T02:58:16+00:00">17 Nov 2015, 02:58:16</span></td>
			<td class="green center">5729</td>
			<td class="red lasttd center">2973</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11597828,0" class="icommentjs kaButton smallButton rightButton" href="/the-walking-dead-s06e06-fastsub-vostfr-hdtv-xvid-ark01-avi-t11597828.html#comment">15 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-walking-dead-s06e06-fastsub-vostfr-hdtv-xvid-ark01-avi-t11597828.html" class="cellMainLink">The Walking Dead S06E06 FASTSUB VOSTFR HDTV XviD-ARK01 avi</a></div>
			</td>
			<td class="nobr center" data-sort="367278080">350.26 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T11:12:44+00:00">16 Nov 2015, 11:12:44</span></td>
			<td class="green center">6622</td>
			<td class="red lasttd center">1185</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11602405,0" class="icommentjs kaButton smallButton rightButton" href="/castle-2009-s08e07-hdtv-x264-lol-ettv-t11602405.html#comment">25 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/castle-2009-s08e07-hdtv-x264-lol-ettv-t11602405.html" class="cellMainLink">Castle 2009 S08E07 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="287924163">274.59 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T02:03:18+00:00">17 Nov 2015, 02:03:18</span></td>
			<td class="green center">4162</td>
			<td class="red lasttd center">1950</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11596035,0" class="icommentjs kaButton smallButton rightButton" href="/once-upon-a-time-s05e08-hdtv-x264-killers-ettv-t11596035.html#comment">52 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/once-upon-a-time-s05e08-hdtv-x264-killers-ettv-t11596035.html" class="cellMainLink">Once Upon a Time S05E08 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="325260071">310.19 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T02:04:15+00:00">16 Nov 2015, 02:04:15</span></td>
			<td class="green center">4657</td>
			<td class="red lasttd center">829</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11590966,0" class="icommentjs kaButton smallButton rightButton" href="/ufc-193-rousey-vs-holm-ppv-webrip-x264-jkkk-sparrow-t11590966.html#comment">112 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ufc-193-rousey-vs-holm-ppv-webrip-x264-jkkk-sparrow-t11590966.html" class="cellMainLink">UFC 193 Rousey vs Holm PPV WEBRip x264-jkkk -={SPARROW}=-</a></div>
			</td>
			<td class="nobr center" data-sort="1946674772">1.81 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T06:46:41+00:00">15 Nov 2015, 06:46:41</span></td>
			<td class="green center">3971</td>
			<td class="red lasttd center">858</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11596067,0" class="icommentjs kaButton smallButton rightButton" href="/brooklyn-nine-nine-s03e07-hdtv-x264-killers-ettv-t11596067.html#comment">37 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/brooklyn-nine-nine-s03e07-hdtv-x264-killers-ettv-t11596067.html" class="cellMainLink">Brooklyn Nine-Nine S03E07 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="181195056">172.8 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T02:18:16+00:00">16 Nov 2015, 02:18:16</span></td>
			<td class="green center">3434</td>
			<td class="red lasttd center">467</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11603049,0" class="icommentjs kaButton smallButton rightButton" href="/wwe-raw-2015-11-16-hdtv-x264-ebi-sparrow-t11603049.html#comment">43 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/wwe-raw-2015-11-16-hdtv-x264-ebi-sparrow-t11603049.html" class="cellMainLink">WWE RAW 2015 11 16 HDTV x264-Ebi -={SPARROW}=-</a></div>
			</td>
			<td class="nobr center" data-sort="1496009496">1.39 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T05:23:17+00:00">17 Nov 2015, 05:23:17</span></td>
			<td class="green center">2310</td>
			<td class="red lasttd center">2678</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11585891,0" class="icommentjs kaButton smallButton rightButton" href="/ash-vs-evil-dead-s01e03-webrip-x264-fum-ettv-t11585891.html#comment">144 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ash-vs-evil-dead-s01e03-webrip-x264-fum-ettv-t11585891.html" class="cellMainLink">Ash vs Evil Dead S01E03 WEBRip x264-FUM[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="264909491">252.64 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T09:46:20+00:00">14 Nov 2015, 09:46:20</span></td>
			<td class="green center">3243</td>
			<td class="red lasttd center">267</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11603033,0" class="icommentjs kaButton smallButton rightButton" href="/fargo-s02e06-internal-hdtv-x264-batv-ettv-t11603033.html#comment">33 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fargo-s02e06-internal-hdtv-x264-batv-ettv-t11603033.html" class="cellMainLink">Fargo S02E06 INTERNAL HDTV x264-BATV[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="265359924">253.07 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T05:19:33+00:00">17 Nov 2015, 05:19:33</span></td>
			<td class="green center">2388</td>
			<td class="red lasttd center">949</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11596178,0" class="icommentjs kaButton smallButton rightButton" href="/agent-x-us-s01e03-hdtv-x264-lol-ettv-t11596178.html#comment">34 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/agent-x-us-s01e03-hdtv-x264-lol-ettv-t11596178.html" class="cellMainLink">Agent X US S01E03 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="230215450">219.55 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T03:02:15+00:00">16 Nov 2015, 03:02:15</span></td>
			<td class="green center">2357</td>
			<td class="red lasttd center">449</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11592127,0" class="icommentjs kaButton smallButton rightButton" href="/various-artists-100-beautiful-songs-320kbps-mp3-t11592127.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/various-artists-100-beautiful-songs-320kbps-mp3-t11592127.html" class="cellMainLink">Various Artists - 100 Beautiful Songs - 320Kbps MP3</a></div>
			</td>
			<td class="nobr center" data-sort="1050514824">1001.85 <span>MB</span></td>
			<td class="center">101</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T11:39:44+00:00">15 Nov 2015, 11:39:44</span></td>
			<td class="green center">688</td>
			<td class="red lasttd center">348</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11587722,0" class="icommentjs kaButton smallButton rightButton" href="/one-direction-made-in-the-a-m-deluxe-edition-2015-itunes-plus-pirate-shovon-t11587722.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/one-direction-made-in-the-a-m-deluxe-edition-2015-itunes-plus-pirate-shovon-t11587722.html" class="cellMainLink">One Direction - Made In The A.M. [Deluxe Edition] [2015] [iTunes Plus] [Pirate Shovon]</a></div>
			</td>
			<td class="nobr center" data-sort="246574648">235.15 <span>MB</span></td>
			<td class="center">21</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T14:45:09+00:00">14 Nov 2015, 14:45:09</span></td>
			<td class="green center">657</td>
			<td class="red lasttd center">354</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11599026,0" class="icommentjs kaButton smallButton rightButton" href="/va-the-seventies-album-3cd-2015-mp3-320kbps-sn3h1t87-glodls-t11599026.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-the-seventies-album-3cd-2015-mp3-320kbps-sn3h1t87-glodls-t11599026.html" class="cellMainLink">VA - The Seventies Album (3CD) [2015] [MP3-320Kbps] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="547858677">522.48 <span>MB</span></td>
			<td class="center">68</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T14:13:50+00:00">16 Nov 2015, 14:13:50</span></td>
			<td class="green center">355</td>
			<td class="red lasttd center">135</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11594292,0" class="icommentjs kaButton smallButton rightButton" href="/va-mega-hits-best-of-2015-2015-mp3-vbr-sn3h1t87-glodls-t11594292.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-mega-hits-best-of-2015-2015-mp3-vbr-sn3h1t87-glodls-t11594292.html" class="cellMainLink">VA - Mega Hits - Best Of 2015 [2015] [MP3-VBR] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="263708109">251.49 <span>MB</span></td>
			<td class="center">41</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T20:15:07+00:00">15 Nov 2015, 20:15:07</span></td>
			<td class="green center">311</td>
			<td class="red lasttd center">108</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11582133,0" class="icommentjs kaButton smallButton rightButton" href="/jeezy-church-in-these-streets-album-itunes-aac-plus-m4a-kween-t11582133.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/jeezy-church-in-these-streets-album-itunes-aac-plus-m4a-kween-t11582133.html" class="cellMainLink">Jeezy - Church In These Streets (Album) [iTunes AAC Plus M4A] [Kween]</a></div>
			</td>
			<td class="nobr center" data-sort="133513971">127.33 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-13T17:36:56+00:00">13 Nov 2015, 17:36:56</span></td>
			<td class="green center">272</td>
			<td class="red lasttd center">64</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11588436,0" class="icommentjs kaButton smallButton rightButton" href="/kirk-franklin-losing-my-religion-2015-320kbps-pirate-shovon-t11588436.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/kirk-franklin-losing-my-religion-2015-320kbps-pirate-shovon-t11588436.html" class="cellMainLink">Kirk Franklin - Losing My Religion [2015] [320Kbps] [Pirate Shovon]</a></div>
			</td>
			<td class="nobr center" data-sort="148044344">141.19 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T16:52:54+00:00">14 Nov 2015, 16:52:54</span></td>
			<td class="green center">262</td>
			<td class="red lasttd center">58</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11588244,0" class="icommentjs kaButton smallButton rightButton" href="/rich-homie-quan-abta-still-going-in-320kbps-mixjoint-t11588244.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/rich-homie-quan-abta-still-going-in-320kbps-mixjoint-t11588244.html" class="cellMainLink">Rich Homie Quan - ABTA Still Going In [320Kbps] (MixJoint)</a></div>
			</td>
			<td class="nobr center" data-sort="90064660">85.89 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T16:19:45+00:00">14 Nov 2015, 16:19:45</span></td>
			<td class="green center">196</td>
			<td class="red lasttd center">40</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11597936,0" class="icommentjs kaButton smallButton rightButton" href="/va-need-for-speed-100-hits-320kbps-mp3-t11597936.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-need-for-speed-100-hits-320kbps-mp3-t11597936.html" class="cellMainLink">VA - Need For Speed (100 Hits) - 320Kbps MP3</a></div>
			</td>
			<td class="nobr center" data-sort="1154522656">1.08 <span>GB</span></td>
			<td class="center">101</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T11:42:19+00:00">16 Nov 2015, 11:42:19</span></td>
			<td class="green center">154</td>
			<td class="red lasttd center">117</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11589064,0" class="icommentjs kaButton smallButton rightButton" href="/neil-young-bluenote-cafe-2015-freak37-t11589064.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/neil-young-bluenote-cafe-2015-freak37-t11589064.html" class="cellMainLink">Neil Young â Bluenote Cafe (2015)...Freak37</a></div>
			</td>
			<td class="nobr center" data-sort="352423848">336.1 <span>MB</span></td>
			<td class="center">25</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T18:57:40+00:00">14 Nov 2015, 18:57:40</span></td>
			<td class="green center">193</td>
			<td class="red lasttd center">32</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11593400,0" class="icommentjs kaButton smallButton rightButton" href="/va-fetenhits-best-of-80-s-2015-mp3-320-kbps-t11593400.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-fetenhits-best-of-80-s-2015-mp3-320-kbps-t11593400.html" class="cellMainLink">VA - Fetenhits: Best Of 80âs (2015) MP3 [320 Kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="586197851">559.04 <span>MB</span></td>
			<td class="center">61</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T17:03:28+00:00">15 Nov 2015, 17:03:28</span></td>
			<td class="green center">168</td>
			<td class="red lasttd center">76</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11594349,0" class="icommentjs kaButton smallButton rightButton" href="/va-538-hitzone-christmas-2015-2cd-mp3-320kbps-sn3h1t87-glodls-t11594349.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-538-hitzone-christmas-2015-2cd-mp3-320kbps-sn3h1t87-glodls-t11594349.html" class="cellMainLink">VA - 538 Hitzone Christmas [2015] (2CD) [MP3-320Kbps] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="409201438">390.24 <span>MB</span></td>
			<td class="center">49</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T20:30:15+00:00">15 Nov 2015, 20:30:15</span></td>
			<td class="green center">163</td>
			<td class="red lasttd center">74</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11603843,0" class="icommentjs kaButton smallButton rightButton" href="/va-now-thats-what-i-call-country-christmas-2015-2cd-mp3-320kbps-sn3h1t87-glodls-t11603843.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-now-thats-what-i-call-country-christmas-2015-2cd-mp3-320kbps-sn3h1t87-glodls-t11603843.html" class="cellMainLink">VA - Now Thats What I Call Country Christmas [2015] [2CD] [MP3-320Kbps] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="260642540">248.57 <span>MB</span></td>
			<td class="center">32</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T08:33:18+00:00">17 Nov 2015, 08:33:18</span></td>
			<td class="green center">112</td>
			<td class="red lasttd center">40</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/nuit-a-paris-lounge-music-mp3-divxtotal-t11595262.html" class="cellMainLink">Nuit a Paris Lounge Music MP3-DiVXTOTAL</a></div>
			</td>
			<td class="nobr center" data-sort="136824082">130.49 <span>MB</span></td>
			<td class="center">13</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T23:52:30+00:00">15 Nov 2015, 23:52:30</span></td>
			<td class="green center">111</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11595274,0" class="icommentjs kaButton smallButton rightButton" href="/acoustic-blues-mp3-divxtotal-t11595274.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/acoustic-blues-mp3-divxtotal-t11595274.html" class="cellMainLink">Acoustic Blues MP3-DiVXTOTAL</a></div>
			</td>
			<td class="nobr center" data-sort="357304804">340.75 <span>MB</span></td>
			<td class="center">51</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T23:55:20+00:00">15 Nov 2015, 23:55:20</span></td>
			<td class="green center">95</td>
			<td class="red lasttd center">18</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11600009,0" class="icommentjs kaButton smallButton rightButton" href="/train-christmas-in-tahoe-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11600009.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/train-christmas-in-tahoe-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11600009.html" class="cellMainLink">Train - Christmas in Tahoe [2015] [MP3-320Kbps] [CBR] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="126861655">120.98 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T14:56:43+00:00">16 Nov 2015, 14:56:43</span></td>
			<td class="green center">86</td>
			<td class="red lasttd center">31</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11583680,0" class="icommentjs kaButton smallButton rightButton" href="/star-wars-battlefront-deluxe-edition-t11583680.html#comment">134 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/star-wars-battlefront-deluxe-edition-t11583680.html" class="cellMainLink">STAR WARS Battlefront Deluxe Edition</a></div>
			</td>
			<td class="nobr center" data-sort="24998581972">23.28 <span>GB</span></td>
			<td class="center">13</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-13T22:49:03+00:00">13 Nov 2015, 22:49:03</span></td>
			<td class="green center">297</td>
			<td class="red lasttd center">1241</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11589261,0" class="icommentjs kaButton smallButton rightButton" href="/call-of-duty-black-ops-iii-update-1-reloaded-t11589261.html#comment">74 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/call-of-duty-black-ops-iii-update-1-reloaded-t11589261.html" class="cellMainLink">Call of Duty Black Ops III Update 1-RELOADED</a></div>
			</td>
			<td class="nobr center" data-sort="546406593">521.09 <span>MB</span></td>
			<td class="center">39</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T19:57:48+00:00">14 Nov 2015, 19:57:48</span></td>
			<td class="green center">241</td>
			<td class="red lasttd center">59</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11586757,0" class="icommentjs kaButton smallButton rightButton" href="/construction-simulator-gold-edition-skidrow-t11586757.html#comment">16 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/construction-simulator-gold-edition-skidrow-t11586757.html" class="cellMainLink">Construction Simulator Gold Edition-SKIDROW</a></div>
			</td>
			<td class="nobr center" data-sort="1303299415">1.21 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T11:04:55+00:00">14 Nov 2015, 11:04:55</span></td>
			<td class="green center">173</td>
			<td class="red lasttd center">74</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11594004,0" class="icommentjs kaButton smallButton rightButton" href="/darksiders-2-deathinitive-edition-update-1-2015-pc-repack-by-r-g-mechanics-t11594004.html#comment">25 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/darksiders-2-deathinitive-edition-update-1-2015-pc-repack-by-r-g-mechanics-t11594004.html" class="cellMainLink">Darksiders 2: Deathinitive Edition [Update 1] (2015) PC | RePack by R.G. Mechanics</a></div>
			</td>
			<td class="nobr center" data-sort="8494382166">7.91 <span>GB</span></td>
			<td class="center">24</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T19:05:30+00:00">15 Nov 2015, 19:05:30</span></td>
			<td class="green center">117</td>
			<td class="red lasttd center">98</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11585537,0" class="icommentjs kaButton smallButton rightButton" href="/dark-tales-8-eap-the-tell-tale-heart-collector-s-edition-asg-t11585537.html#comment">13 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/dark-tales-8-eap-the-tell-tale-heart-collector-s-edition-asg-t11585537.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/dark-tales-8-eap-the-tell-tale-heart-collector-s-edition-asg-t11585537.html" class="cellMainLink">Dark Tales 8 - EAP The Tell-Tale Heart Collector&#039;s Edition [ASG]</a></div>
			</td>
			<td class="nobr center" data-sort="719030969">685.72 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T07:42:16+00:00">14 Nov 2015, 07:42:16</span></td>
			<td class="green center">118</td>
			<td class="red lasttd center">18</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11587664,0" class="icommentjs kaButton smallButton rightButton" href="/the-binding-of-isaac-afterbirth-update-6-t11587664.html#comment">19 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/the-binding-of-isaac-afterbirth-update-6-t11587664.html" class="cellMainLink">The Binding of Isaac: Afterbirth (Update 6)</a></div>
			</td>
			<td class="nobr center" data-sort="470219232">448.44 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T14:34:29+00:00">14 Nov 2015, 14:34:29</span></td>
			<td class="green center">97</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11587594,0" class="icommentjs kaButton smallButton rightButton" href="/space-engineers-v01-108-946-t11587594.html#comment">16 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/space-engineers-v01-108-946-t11587594.html" class="cellMainLink">Space Engineers v01.108.946</a></div>
			</td>
			<td class="nobr center" data-sort="4078580135">3.8 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T14:22:36+00:00">14 Nov 2015, 14:22:36</span></td>
			<td class="green center">73</td>
			<td class="red lasttd center">37</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11589389,0" class="icommentjs kaButton smallButton rightButton" href="/dark-tales-8-edgar-allan-poes-the-tell-tale-heart-ce-2015-pc-final-t11589389.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/dark-tales-8-edgar-allan-poes-the-tell-tale-heart-ce-2015-pc-final-t11589389.html" class="cellMainLink">Dark Tales 8: Edgar Allan Poes The Tell-Tale Heart CE (2015) PC [FINAL]</a></div>
			</td>
			<td class="nobr center" data-sort="723377711">689.87 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T20:39:18+00:00">14 Nov 2015, 20:39:18</span></td>
			<td class="green center">43</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11587605,0" class="icommentjs kaButton smallButton rightButton" href="/starbound-nightly-v13-11-2015-t11587605.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/starbound-nightly-v13-11-2015-t11587605.html" class="cellMainLink">Starbound (Nightly v13.11.2015)</a></div>
			</td>
			<td class="nobr center" data-sort="848022356">808.74 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T14:23:51+00:00">14 Nov 2015, 14:23:51</span></td>
			<td class="green center">38</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11582951,0" class="icommentjs kaButton smallButton rightButton" href="/football-manager-2016-v16-1-1-wait-for-crack-t11582951.html#comment">17 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/football-manager-2016-v16-1-1-wait-for-crack-t11582951.html" class="cellMainLink">Football Manager 2016 v16.1.1 (Wait for Crack)</a></div>
			</td>
			<td class="nobr center" data-sort="1674752453">1.56 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-13T18:44:25+00:00">13 Nov 2015, 18:44:25</span></td>
			<td class="green center">38</td>
			<td class="red lasttd center">18</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11585356,0" class="icommentjs kaButton smallButton rightButton" href="/starpoint-gemini-2-v-1-9-3-dlc-2014-pc-repack-t11585356.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/starpoint-gemini-2-v-1-9-3-dlc-2014-pc-repack-t11585356.html" class="cellMainLink">Starpoint Gemini 2 [v 1.9 + 3 DLC] (2014) PC | RePack</a></div>
			</td>
			<td class="nobr center" data-sort="4583290880">4.27 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T06:57:31+00:00">14 Nov 2015, 06:57:31</span></td>
			<td class="green center">23</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11604310,0" class="icommentjs kaButton smallButton rightButton" href="/spintires-build-9-11-15-t11604310.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/spintires-build-9-11-15-t11604310.html" class="cellMainLink">SPINTIRES Build 9.11.15</a></div>
			</td>
			<td class="nobr center" data-sort="574369261">547.76 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T10:35:55+00:00">17 Nov 2015, 10:35:55</span></td>
			<td class="green center">19</td>
			<td class="red lasttd center">9</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11600029,0" class="icommentjs kaButton smallButton rightButton" href="/tropico-5-gog-t11600029.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/tropico-5-gog-t11600029.html" class="cellMainLink">Tropico 5 (GOG)</a></div>
			</td>
			<td class="nobr center" data-sort="2945638913">2.74 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T15:01:28+00:00">16 Nov 2015, 15:01:28</span></td>
			<td class="green center">15</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11586822,0" class="icommentjs kaButton smallButton rightButton" href="/game-of-thrones-episode-1-5-ps3-duplex-t11586822.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/game-of-thrones-episode-1-5-ps3-duplex-t11586822.html" class="cellMainLink">Game of Thrones Episode 1-5 PS3-DUPLEX</a></div>
			</td>
			<td class="nobr center" data-sort="4822682184">4.49 <span>GB</span></td>
			<td class="center">52</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T11:22:34+00:00">14 Nov 2015, 11:22:34</span></td>
			<td class="green center">15</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11587573,0" class="icommentjs kaButton smallButton rightButton" href="/kerbal-space-program-v1-0-5-1028-t11587573.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/kerbal-space-program-v1-0-5-1028-t11587573.html" class="cellMainLink">Kerbal Space Program v1.0.5.1028</a></div>
			</td>
			<td class="nobr center" data-sort="609415162">581.18 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T14:17:25+00:00">14 Nov 2015, 14:17:25</span></td>
			<td class="green center">19</td>
			<td class="red lasttd center">3</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11583728,0" class="icommentjs kaButton smallButton rightButton" href="/microsoft-windows-10-5in1-full-x64-nov-2015-techtools-t11583728.html#comment">91 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/microsoft-windows-10-5in1-full-x64-nov-2015-techtools-t11583728.html" class="cellMainLink">Microsoft Windows 10 5in1 FULL (x64) Nov 2015 [TechTools]</a></div>
			</td>
			<td class="nobr center" data-sort="4451828841">4.15 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-13T23:09:26+00:00">13 Nov 2015, 23:09:26</span></td>
			<td class="green center">421</td>
			<td class="red lasttd center">490</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11595895,0" class="icommentjs kaButton smallButton rightButton" href="/malwarebytes-anti-malware-premium-latest-serials-14-11-15-4realtorrentz-t11595895.html#comment">31 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType txtType">
                    <a href="/malwarebytes-anti-malware-premium-latest-serials-14-11-15-4realtorrentz-t11595895.html" class="cellMainLink">Malwarebytes Anti-Malware Premium Latest Serials (14.11.15) [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="735">735 <span>bytes</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T01:08:52+00:00">16 Nov 2015, 01:08:52</span></td>
			<td class="green center">279</td>
			<td class="red lasttd center">9</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11587941,0" class="icommentjs kaButton smallButton rightButton" href="/daemon-tools-ultra-4-0-1-425-2015-pc-t11587941.html#comment">28 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/daemon-tools-ultra-4-0-1-425-2015-pc-t11587941.html" class="cellMainLink">Daemon Tools Ultra 4.0.1.425 (2015) PC</a></div>
			</td>
			<td class="nobr center" data-sort="34603039">33 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T15:25:21+00:00">14 Nov 2015, 15:25:21</span></td>
			<td class="green center">195</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11591799,0" class="icommentjs kaButton smallButton rightButton" href="/total-uninstall-6-16-0-320-professional-edition-mltilanguage-cracked-remek002-at-team-t11591799.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/total-uninstall-6-16-0-320-professional-edition-mltilanguage-cracked-remek002-at-team-t11591799.html" class="cellMainLink">Total Uninstall 6.16.0.320 Professional Edition [Mltilanguage] [Cracked remek002] [AT-TEAM]</a></div>
			</td>
			<td class="nobr center" data-sort="35616332">33.97 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T10:26:22+00:00">15 Nov 2015, 10:26:22</span></td>
			<td class="green center">156</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11584931,0" class="icommentjs kaButton smallButton rightButton" href="/ilike-android-data-recovery-pro-key-4realtorrentz-t11584931.html#comment">13 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/ilike-android-data-recovery-pro-key-4realtorrentz-t11584931.html" class="cellMainLink">iLike Android Data Recovery Pro + Key [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="3286197">3.13 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T05:16:03+00:00">14 Nov 2015, 05:16:03</span></td>
			<td class="green center">143</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11600605,0" class="icommentjs kaButton smallButton rightButton" href="/windows-10-manager-1-0-5-final-eng-patch-keygen-amped-at-team-t11600605.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-10-manager-1-0-5-final-eng-patch-keygen-amped-at-team-t11600605.html" class="cellMainLink">Windows 10 Manager 1.0.5 Final [ENG] [Patch &amp; Keygen AMPED] [AT-TEAM]</a></div>
			</td>
			<td class="nobr center" data-sort="12608937">12.02 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T17:12:45+00:00">16 Nov 2015, 17:12:45</span></td>
			<td class="green center">127</td>
			<td class="red lasttd center">30</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11596062,0" class="icommentjs kaButton smallButton rightButton" href="/iobit-driver-booster-pro-3-0-3-275-multilingual-key-4realtorrentz-t11596062.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/iobit-driver-booster-pro-3-0-3-275-multilingual-key-4realtorrentz-t11596062.html" class="cellMainLink">IObit Driver Booster Pro 3.0.3.275 Multilingual + Key [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="14233401">13.57 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T02:14:17+00:00">16 Nov 2015, 02:14:17</span></td>
			<td class="green center">124</td>
			<td class="red lasttd center">20</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11591059,0" class="icommentjs kaButton smallButton rightButton" href="/windows-10-pro-rtm-build-10586-x64-multi-6-nov2015-pre-activated-t11591059.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-10-pro-rtm-build-10586-x64-multi-6-nov2015-pre-activated-t11591059.html" class="cellMainLink">Windows 10 Pro RTM Build 10586 x64 MULTi-6 Nov2015-Pre-Activated~</a></div>
			</td>
			<td class="nobr center" data-sort="3654529610">3.4 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T07:14:30+00:00">15 Nov 2015, 07:14:30</span></td>
			<td class="green center">69</td>
			<td class="red lasttd center">113</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11596306,0" class="icommentjs kaButton smallButton rightButton" href="/destroy-windows-10-spying-1-5-0-build-693-4realtorrentz-t11596306.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/destroy-windows-10-spying-1-5-0-build-693-4realtorrentz-t11596306.html" class="cellMainLink">Destroy Windows 10 Spying 1.5.0 Build 693 [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="134183">131.04 <span>KB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T03:44:54+00:00">16 Nov 2015, 03:44:54</span></td>
			<td class="green center">107</td>
			<td class="red lasttd center">5</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/room-arranger-8-3-0-538-32bit-64bit-multilanguage-serial-at-team-t11603209.html" class="cellMainLink">Room Arranger 8.3.0.538 - 32bit &amp; 64bit [Multilanguage] [Serial] [AT-TEAM]</a></div>
			</td>
			<td class="nobr center" data-sort="48392807">46.15 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T06:09:17+00:00">17 Nov 2015, 06:09:17</span></td>
			<td class="green center">96</td>
			<td class="red lasttd center">25</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11590763,0" class="icommentjs kaButton smallButton rightButton" href="/media-player-classic-home-cinema-1-7-10-stable-2015-Ð Ð¡-portable-t11590763.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/media-player-classic-home-cinema-1-7-10-stable-2015-Ð Ð¡-portable-t11590763.html" class="cellMainLink">Media Player Classic Home Cinema 1.7.10 Stable (2015) Ð Ð¡ | + Portable</a></div>
			</td>
			<td class="nobr center" data-sort="110579180">105.46 <span>MB</span></td>
			<td class="center">176</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T05:57:08+00:00">15 Nov 2015, 05:57:08</span></td>
			<td class="green center">94</td>
			<td class="red lasttd center">25</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/acdsee-pro-9-1-build-453-lite-by-mkn-pre-activated-4realtorrentz-t11596135.html" class="cellMainLink">ACDSee Pro 9.1 Build 453 Lite by MKN Pre-Activated [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="48221725">45.99 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T02:50:47+00:00">16 Nov 2015, 02:50:47</span></td>
			<td class="green center">90</td>
			<td class="red lasttd center">9</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11588941,0" class="icommentjs kaButton smallButton rightButton" href="/4k-video-downloader-3-6-4-1795-2015-pc-portable-t11588941.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/4k-video-downloader-3-6-4-1795-2015-pc-portable-t11588941.html" class="cellMainLink">4K Video Downloader 3.6.4.1795 (2015) PC | + Portable</a></div>
			</td>
			<td class="nobr center" data-sort="129426202">123.43 <span>MB</span></td>
			<td class="center">52</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T18:27:35+00:00">14 Nov 2015, 18:27:35</span></td>
			<td class="green center">82</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11585797,0" class="icommentjs kaButton smallButton rightButton" href="/ashampoo-burning-studio-2016-v16-0-0-17-final-serial-b4tman-t11585797.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/ashampoo-burning-studio-2016-v16-0-0-17-final-serial-b4tman-t11585797.html" class="cellMainLink">Ashampoo Burning Studio 2016 v16.0.0.17 Final + Serial {B4tman}</a></div>
			</td>
			<td class="nobr center" data-sort="69751705">66.52 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T09:12:53+00:00">14 Nov 2015, 09:12:53</span></td>
			<td class="green center">46</td>
			<td class="red lasttd center">3</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11584194,0" class="icommentjs kaButton smallButton rightButton" href="/platinum-hide-ip-3-4-7-8-patch-4realtorrentz-t11584194.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/platinum-hide-ip-3-4-7-8-patch-4realtorrentz-t11584194.html" class="cellMainLink">Platinum Hide IP 3.4.7.8 + Patch [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="2397100">2.29 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T01:50:40+00:00">14 Nov 2015, 01:50:40</span></td>
			<td class="green center">34</td>
			<td class="red lasttd center">3</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11594279,0" class="icommentjs kaButton smallButton rightButton" href="/animerg-one-punch-man-07-720p-mkv-t11594279.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-one-punch-man-07-720p-mkv-t11594279.html" class="cellMainLink">[AnimeRG] One Punch Man - 07 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="286920430">273.63 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T20:11:42+00:00">15 Nov 2015, 20:11:42</span></td>
			<td class="green center">532</td>
			<td class="red lasttd center">155</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11589311,0" class="icommentjs kaButton smallButton rightButton" href="/horriblesubs-haikyuu-s2-07-720p-mkv-t11589311.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-haikyuu-s2-07-720p-mkv-t11589311.html" class="cellMainLink">[HorribleSubs] Haikyuu!! S2 - 07 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="342465219">326.6 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T20:10:03+00:00">14 Nov 2015, 20:10:03</span></td>
			<td class="green center">95</td>
			<td class="red lasttd center">679</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11594206,0" class="icommentjs kaButton smallButton rightButton" href="/one-punch-man-07-480p-engsub-iorchid-mkv-t11594206.html#comment">23 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/one-punch-man-07-480p-engsub-iorchid-mkv-t11594206.html" class="cellMainLink">One-Punch Man - 07 [480p][EngSub][iORcHiD].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="101255408">96.56 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T19:48:33+00:00">15 Nov 2015, 19:48:33</span></td>
			<td class="green center">195</td>
			<td class="red lasttd center">40</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11590547,0" class="icommentjs kaButton smallButton rightButton" href="/one-piece-718-480p-engsub-iorchid-t11590547.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/one-piece-718-480p-engsub-iorchid-t11590547.html" class="cellMainLink">One Piece - 718 [480p][EngSub][iORcHiD]</a></div>
			</td>
			<td class="nobr center" data-sort="100414003">95.76 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T05:02:33+00:00">15 Nov 2015, 05:02:33</span></td>
			<td class="green center">187</td>
			<td class="red lasttd center">18</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-jk-meshi-07-raw-mx-1280x720-x264-aac-mp4-t11603176.html" class="cellMainLink">[Leopard-Raws] JK Meshi! - 07 RAW (MX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="47389881">45.19 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T06:00:03+00:00">17 Nov 2015, 06:00:03</span></td>
			<td class="green center">123</td>
			<td class="red lasttd center">35</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11593553,0" class="icommentjs kaButton smallButton rightButton" href="/ae-one-punch-man-07-720p-mp4-t11593553.html#comment">13 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ae-one-punch-man-07-720p-mp4-t11593553.html" class="cellMainLink">[AE] One-Punch Man - 07 [720p].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="206517951">196.95 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T17:41:34+00:00">15 Nov 2015, 17:41:34</span></td>
			<td class="green center">133</td>
			<td class="red lasttd center">11</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11592296,0" class="icommentjs kaButton smallButton rightButton" href="/dragon-ball-super-episode-019-english-subbed-720p-arizone-t11592296.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dragon-ball-super-episode-019-english-subbed-720p-arizone-t11592296.html" class="cellMainLink">DRAGON BALL SUPER Episode - 019 [ENGLISH SUBBED] 720p ~ARIZONE</a></div>
			</td>
			<td class="nobr center" data-sort="160030107">152.62 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T12:26:55+00:00">15 Nov 2015, 12:26:55</span></td>
			<td class="green center">111</td>
			<td class="red lasttd center">40</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11592034,0" class="icommentjs kaButton smallButton rightButton" href="/arrg-dragon-ball-super-019-480p-eng-sub-lucifer22-t11592034.html#comment">13 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/arrg-dragon-ball-super-019-480p-eng-sub-lucifer22-t11592034.html" class="cellMainLink">[ARRG] Dragon Ball Super - 019 [480p][Eng Sub]_lucifer22</a></div>
			</td>
			<td class="nobr center" data-sort="64972686">61.96 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T11:23:52+00:00">15 Nov 2015, 11:23:52</span></td>
			<td class="green center">74</td>
			<td class="red lasttd center">11</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/commie-teekyuu-67-e2b88e60-mkv-t11601330.html" class="cellMainLink">[Commie] Teekyuu - 67 [E2B88E60].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="44540098">42.48 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T19:55:02+00:00">16 Nov 2015, 19:55:02</span></td>
			<td class="green center">38</td>
			<td class="red lasttd center">38</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11593525,0" class="icommentjs kaButton smallButton rightButton" href="/bakedfish-one-punch-man-07-720p-aac-mp4-t11593525.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/bakedfish-one-punch-man-07-720p-aac-mp4-t11593525.html" class="cellMainLink">[BakedFish] One Punch Man - 07 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="506927758">483.44 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T17:34:48+00:00">15 Nov 2015, 17:34:48</span></td>
			<td class="green center">46</td>
			<td class="red lasttd center">4</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11598328,0" class="icommentjs kaButton smallButton rightButton" href="/ipunisher-triage-x-vol-5-bd-1280x720-h264-10bit-aac-uncensored-t11598328.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ipunisher-triage-x-vol-5-bd-1280x720-h264-10bit-aac-uncensored-t11598328.html" class="cellMainLink">[iPUNISHER] Triage X - Vol.5 (BD 1280x720 h264 10bit AAC UNCENSORED)</a></div>
			</td>
			<td class="nobr center" data-sort="718595578">685.31 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T13:28:30+00:00">16 Nov 2015, 13:28:30</span></td>
			<td class="green center">35</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11590771,0" class="icommentjs kaButton smallButton rightButton" href="/ae-valkyrie-drive-mermaid-06-uncen-720p-mp4-t11590771.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ae-valkyrie-drive-mermaid-06-uncen-720p-mp4-t11590771.html" class="cellMainLink">[AE] Valkyrie Drive - Mermaid - 06 [UNCEN] [720p].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="178146536">169.89 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T05:59:28+00:00">15 Nov 2015, 05:59:28</span></td>
			<td class="green center">32</td>
			<td class="red lasttd center">5</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ipunisher-valkyrie-drive-mermaid-06-at-x-1280x720-h264-10bit-aac-uncensored-mkv-t11589044.html" class="cellMainLink">[iPUNISHER] Valkyrie Drive ~Mermaid~ - 06 (AT-X 1280x720 h264 10bit AAC UNCENSORED).mkv</a></div>
			</td>
			<td class="nobr center" data-sort="271590373">259.01 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T18:52:24+00:00">14 Nov 2015, 18:52:24</span></td>
			<td class="green center">31</td>
			<td class="red lasttd center">1</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/deadfish-valkyrie-drive-mermaid-06-720p-aac-mp4-t11603230.html" class="cellMainLink">[DeadFish] Valkyrie Drive: Mermaid - 06 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="348627285">332.48 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T06:13:59+00:00">17 Nov 2015, 06:13:59</span></td>
			<td class="green center">24</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11585766,0" class="icommentjs kaButton smallButton rightButton" href="/fairy-tail-s2-84-259-480p-engsub-iorchid-t11585766.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fairy-tail-s2-84-259-480p-engsub-iorchid-t11585766.html" class="cellMainLink">Fairy Tail S2 - 84 (259) [480p][EngSub][iORcHiD]</a></div>
			</td>
			<td class="nobr center" data-sort="102569258">97.82 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T09:01:34+00:00">14 Nov 2015, 09:01:34</span></td>
			<td class="green center">27</td>
			<td class="red lasttd center">2</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11583972,0" class="icommentjs kaButton smallButton rightButton" href="/assorted-magazines-bundle-november-14-2015-true-pdf-t11583972.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/assorted-magazines-bundle-november-14-2015-true-pdf-t11583972.html" class="cellMainLink">Assorted Magazines Bundle - November 14 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="426865924">407.09 <span>MB</span></td>
			<td class="center">34</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T00:38:25+00:00">14 Nov 2015, 00:38:25</span></td>
			<td class="green center">341</td>
			<td class="red lasttd center">231</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11589577,0" class="icommentjs kaButton smallButton rightButton" href="/ultimate-iq-tests-philip-carter-ken-russell-t11589577.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/ultimate-iq-tests-philip-carter-ken-russell-t11589577.html" class="cellMainLink">Ultimate IQ Tests - Philip Carter &amp; Ken Russell</a></div>
			</td>
			<td class="nobr center" data-sort="10088588">9.62 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T21:51:27+00:00">14 Nov 2015, 21:51:27</span></td>
			<td class="green center">366</td>
			<td class="red lasttd center">49</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11587420,0" class="icommentjs kaButton smallButton rightButton" href="/the-belly-fat-cure-fast-track-discover-the-ultimate-carb-swap-and-drop-up-to-14-lbs-the-first-14-days-2015-epub-gooner-t11587420.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-belly-fat-cure-fast-track-discover-the-ultimate-carb-swap-and-drop-up-to-14-lbs-the-first-14-days-2015-epub-gooner-t11587420.html" class="cellMainLink">The Belly Fat Cure Fast Track - Discover the Ultimate Carb Swap and Drop up to 14 Lbs The First 14 Days (2015).epub Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="16711457">15.94 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T13:37:27+00:00">14 Nov 2015, 13:37:27</span></td>
			<td class="green center">372</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11589878,0" class="icommentjs kaButton smallButton rightButton" href="/islam-and-terrorism-revised-and-updated-edition-the-truth-about-isis-the-middle-east-and-islamic-jihad-m-gabriel-t11589878.html#comment">19 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/islam-and-terrorism-revised-and-updated-edition-the-truth-about-isis-the-middle-east-and-islamic-jihad-m-gabriel-t11589878.html" class="cellMainLink">Islam and Terrorism (Revised and Updated Edition) : The Truth About ISIS, the Middle East and Islamic Jihad - M Gabriel</a></div>
			</td>
			<td class="nobr center" data-sort="6585711">6.28 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T23:56:29+00:00">14 Nov 2015, 23:56:29</span></td>
			<td class="green center">327</td>
			<td class="red lasttd center">51</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11590748,0" class="icommentjs kaButton smallButton rightButton" href="/fallout-4-vault-dweller-s-survival-guide-prima-official-game-guide-t11590748.html#comment">51 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/fallout-4-vault-dweller-s-survival-guide-prima-official-game-guide-t11590748.html" class="cellMainLink">Fallout 4 Vault Dweller&#039;s Survival Guide: Prima Official Game Guide</a></div>
			</td>
			<td class="nobr center" data-sort="75282330">71.79 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T05:53:38+00:00">15 Nov 2015, 05:53:38</span></td>
			<td class="green center">325</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11587239,0" class="icommentjs kaButton smallButton rightButton" href="/english-grammar-for-the-utterly-confused-by-laurie-e-rozakis-pdf-zeke23-t11587239.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/english-grammar-for-the-utterly-confused-by-laurie-e-rozakis-pdf-zeke23-t11587239.html" class="cellMainLink">English Grammar for the Utterly Confused by Laurie E. Rozakis - pdf - zeke23</a></div>
			</td>
			<td class="nobr center" data-sort="1495451">1.43 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T12:53:12+00:00">14 Nov 2015, 12:53:12</span></td>
			<td class="green center">256</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11591071,0" class="icommentjs kaButton smallButton rightButton" href="/womens-magazines-bundle-november-15-2015-true-pdf-t11591071.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/womens-magazines-bundle-november-15-2015-true-pdf-t11591071.html" class="cellMainLink">Womens Magazines Bundle - November 15 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="359233743">342.59 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T07:17:07+00:00">15 Nov 2015, 07:17:07</span></td>
			<td class="green center">158</td>
			<td class="red lasttd center">82</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11586238,0" class="icommentjs kaButton smallButton rightButton" href="/the-civil-war-a-visual-history-revised-edition-2015-dk-publishing-pdf-gooner-t11586238.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-civil-war-a-visual-history-revised-edition-2015-dk-publishing-pdf-gooner-t11586238.html" class="cellMainLink">The Civil War - A Visual History - Revised Edition (2015) (DK Publishing).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="129563887">123.56 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T10:23:06+00:00">14 Nov 2015, 10:23:06</span></td>
			<td class="green center">189</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11600045,0" class="icommentjs kaButton smallButton rightButton" href="/the-mission-chinese-food-cookbook-t11600045.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/the-mission-chinese-food-cookbook-t11600045.html" class="cellMainLink">The Mission Chinese Food Cookbook</a></div>
			</td>
			<td class="nobr center" data-sort="52889967">50.44 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T15:06:43+00:00">16 Nov 2015, 15:06:43</span></td>
			<td class="green center">175</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11587155,0" class="icommentjs kaButton smallButton rightButton" href="/the-china-study-quick-easy-cookbook-cook-once-eat-all-week-with-whole-food-plant-based-recipes-2015-epub-gooner-t11587155.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-china-study-quick-easy-cookbook-cook-once-eat-all-week-with-whole-food-plant-based-recipes-2015-epub-gooner-t11587155.html" class="cellMainLink">The China Study Quick &amp; Easy Cookbook - Cook Once, Eat All Week with Whole Food, Plant-Based Recipes (2015).epub Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="21184614">20.2 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T12:39:38+00:00">14 Nov 2015, 12:39:38</span></td>
			<td class="green center">145</td>
			<td class="red lasttd center">4</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11586707,0" class="icommentjs kaButton smallButton rightButton" href="/the-changing-world-religion-map-sacred-places-identities-practices-and-politics-2015-pdf-gooner-t11586707.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-changing-world-religion-map-sacred-places-identities-practices-and-politics-2015-pdf-gooner-t11586707.html" class="cellMainLink">The Changing World Religion Map - Sacred Places, Identities, Practices and Politics (2015).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="100447883">95.79 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T10:54:56+00:00">14 Nov 2015, 10:54:56</span></td>
			<td class="green center">140</td>
			<td class="red lasttd center">6</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11586801,0" class="icommentjs kaButton smallButton rightButton" href="/the-campside-guide-to-dutch-oven-cooking-66-easy-delicious-recipes-for-backpackers-day-hikers-and-campers-2015-epub-gooner-t11586801.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-campside-guide-to-dutch-oven-cooking-66-easy-delicious-recipes-for-backpackers-day-hikers-and-campers-2015-epub-gooner-t11586801.html" class="cellMainLink">The Campside Guide to Dutch Oven Cooking - 66 Easy, Delicious Recipes for Backpackers, Day Hikers and Campers (2015).epub Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="28573201">27.25 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T11:16:15+00:00">14 Nov 2015, 11:16:15</span></td>
			<td class="green center">138</td>
			<td class="red lasttd center">6</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11600053,0" class="icommentjs kaButton smallButton rightButton" href="/real-cajun-rustic-home-cooking-from-donald-link-s-louisiana-t11600053.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/real-cajun-rustic-home-cooking-from-donald-link-s-louisiana-t11600053.html" class="cellMainLink">Real Cajun Rustic Home Cooking from Donald Link&#039;s Louisiana</a></div>
			</td>
			<td class="nobr center" data-sort="39789901">37.95 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T15:06:49+00:00">16 Nov 2015, 15:06:49</span></td>
			<td class="green center">130</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11590114,0" class="icommentjs kaButton smallButton rightButton" href="/batman-v1-001-713-extras-1940-2011-digital-scans-novus-empire-minutemen-nem-t11590114.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/batman-v1-001-713-extras-1940-2011-digital-scans-novus-empire-minutemen-nem-t11590114.html" class="cellMainLink">Batman v1 (001-713+Extras) (1940-2011) (digital+scans) (Novus+Empire+Minutemen) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="24441374901">22.76 <span>GB</span></td>
			<td class="center">877</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T02:04:17+00:00">15 Nov 2015, 02:04:17</span></td>
			<td class="green center">47</td>
			<td class="red lasttd center">123</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11586994,0" class="icommentjs kaButton smallButton rightButton" href="/the-carefree-garden-letting-nature-play-her-part-2015-pdf-gooner-t11586994.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-carefree-garden-letting-nature-play-her-part-2015-pdf-gooner-t11586994.html" class="cellMainLink">The Carefree Garden - Letting Nature Play Her Part (2015).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="3597204">3.43 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T12:08:43+00:00">14 Nov 2015, 12:08:43</span></td>
			<td class="green center">95</td>
			<td class="red lasttd center">2</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11591176,0" class="icommentjs kaButton smallButton rightButton" href="/neil-young-bluenote-cafe-2015-flac-sn3h1t87-glodls-t11591176.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/neil-young-bluenote-cafe-2015-flac-sn3h1t87-glodls-t11591176.html" class="cellMainLink">Neil Young - Bluenote Cafe (2015) [FLAC] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="1002802681">956.35 <span>MB</span></td>
			<td class="center">29</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T07:48:07+00:00">15 Nov 2015, 07:48:07</span></td>
			<td class="green center">143</td>
			<td class="red lasttd center">28</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11600889,0" class="icommentjs kaButton smallButton rightButton" href="/va-favourite-classics-definitive-classical-collection-flac-t11600889.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-favourite-classics-definitive-classical-collection-flac-t11600889.html" class="cellMainLink">VA - Favourite Classics (Definitive Classical Collection) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="690652289">658.66 <span>MB</span></td>
			<td class="center">26</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T18:05:58+00:00">16 Nov 2015, 18:05:58</span></td>
			<td class="green center">87</td>
			<td class="red lasttd center">62</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11593040,0" class="icommentjs kaButton smallButton rightButton" href="/billie-holiday-solitude-2015-24-192-hd-flac-t11593040.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/billie-holiday-solitude-2015-24-192-hd-flac-t11593040.html" class="cellMainLink">Billie Holiday - Solitude (2015) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1690023298">1.57 <span>GB</span></td>
			<td class="center">30</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T15:33:24+00:00">15 Nov 2015, 15:33:24</span></td>
			<td class="green center">92</td>
			<td class="red lasttd center">33</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11601187,0" class="icommentjs kaButton smallButton rightButton" href="/big-bands-of-the-swingin-years-flac-tntvillage-t11601187.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/big-bands-of-the-swingin-years-flac-tntvillage-t11601187.html" class="cellMainLink">Big Bands Of The Swingin&#039; Years [Flac][TntVillage]</a></div>
			</td>
			<td class="nobr center" data-sort="265538273">253.24 <span>MB</span></td>
			<td class="center">23</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T19:11:52+00:00">16 Nov 2015, 19:11:52</span></td>
			<td class="green center">86</td>
			<td class="red lasttd center">36</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11589585,0" class="icommentjs kaButton smallButton rightButton" href="/brahms-piano-trios-julius-katchen-josef-suk-jÃ¡nos-starker-t11589585.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/brahms-piano-trios-julius-katchen-josef-suk-jÃ¡nos-starker-t11589585.html" class="cellMainLink">Brahms - Piano trios - Julius Katchen, Josef Suk, JÃ¡nos Starker</a></div>
			</td>
			<td class="nobr center" data-sort="506836900">483.36 <span>MB</span></td>
			<td class="center">9</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T21:57:13+00:00">14 Nov 2015, 21:57:13</span></td>
			<td class="green center">87</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11592282,0" class="icommentjs kaButton smallButton rightButton" href="/va-magical-melodies-that-will-live-forever-3-cd-box-set-flac-t11592282.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-magical-melodies-that-will-live-forever-3-cd-box-set-flac-t11592282.html" class="cellMainLink">VA - Magical Melodies That Will Live Forever 3 CD Box Set [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1107754632">1.03 <span>GB</span></td>
			<td class="center">65</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T12:20:49+00:00">15 Nov 2015, 12:20:49</span></td>
			<td class="green center">65</td>
			<td class="red lasttd center">50</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11597684,0" class="icommentjs kaButton smallButton rightButton" href="/aretha-franklin-spirit-in-the-dark-2012-24-96-hd-flac-t11597684.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/aretha-franklin-spirit-in-the-dark-2012-24-96-hd-flac-t11597684.html" class="cellMainLink">Aretha Franklin - Spirit in the Dark (2012) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="890093561">848.86 <span>MB</span></td>
			<td class="center">30</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T10:35:45+00:00">16 Nov 2015, 10:35:45</span></td>
			<td class="green center">74</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11588870,0" class="icommentjs kaButton smallButton rightButton" href="/brahms-clarinet-trio-op-114-clarinet-quintet-op-115-danubius-string-quartet-t11588870.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/brahms-clarinet-trio-op-114-clarinet-quintet-op-115-danubius-string-quartet-t11588870.html" class="cellMainLink">Brahms - Clarinet Trio op. 114, Clarinet Quintet op. 115 - Danubius String Quartet</a></div>
			</td>
			<td class="nobr center" data-sort="249201370">237.66 <span>MB</span></td>
			<td class="center">7</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T18:09:54+00:00">14 Nov 2015, 18:09:54</span></td>
			<td class="green center">65</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11594392,0" class="icommentjs kaButton smallButton rightButton" href="/prokofiev-violin-sonatas-shlomo-mintz-yefim-bronfman-t11594392.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/prokofiev-violin-sonatas-shlomo-mintz-yefim-bronfman-t11594392.html" class="cellMainLink">Prokofiev - Violin Sonatas - Shlomo Mintz, Yefim Bronfman</a></div>
			</td>
			<td class="nobr center" data-sort="224823840">214.41 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T20:40:19+00:00">15 Nov 2015, 20:40:19</span></td>
			<td class="green center">57</td>
			<td class="red lasttd center">11</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11582164,0" class="icommentjs kaButton smallButton rightButton" href="/r-e-m-chronic-town-2014-24-192-hd-flac-t11582164.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/r-e-m-chronic-town-2014-24-192-hd-flac-t11582164.html" class="cellMainLink">R.E.M. - Chronic Town (2014) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="907431808">865.39 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-13T17:44:58+00:00">13 Nov 2015, 17:44:58</span></td>
			<td class="green center">50</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11591100,0" class="icommentjs kaButton smallButton rightButton" href="/jimmy-smith-back-at-the-chicken-shack-2013-24-96-hd-flac-t11591100.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/jimmy-smith-back-at-the-chicken-shack-2013-24-96-hd-flac-t11591100.html" class="cellMainLink">Jimmy Smith - Back At The Chicken Shack (2013) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="920899655">878.24 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-15T07:25:36+00:00">15 Nov 2015, 07:25:36</span></td>
			<td class="green center">43</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/ellie-goulding-2015-delirium-target-deluxe-edition-flac-freak37-t11597101.html" class="cellMainLink">Ellie Goulding - 2015 - Delirium [Target Deluxe Edition] - FLAC ... [ Freak37 ]</a></div>
			</td>
			<td class="nobr center" data-sort="672447474">641.3 <span>MB</span></td>
			<td class="center">29</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T07:47:55+00:00">16 Nov 2015, 07:47:55</span></td>
			<td class="green center">39</td>
			<td class="red lasttd center">6</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11601177,0" class="icommentjs kaButton smallButton rightButton" href="/jeezy-church-in-these-streets-2015-flac-sn3h1t87-glodls-t11601177.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/jeezy-church-in-these-streets-2015-flac-sn3h1t87-glodls-t11601177.html" class="cellMainLink">Jeezy - Church in These Streets (2015) [FLAC] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="428824639">408.96 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-16T19:06:49+00:00">16 Nov 2015, 19:06:49</span></td>
			<td class="green center">34</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/one-direction-made-in-the-a-m-deluxe-edition-2015-flac-sn3h1t87-glodls-t11603083.html" class="cellMainLink">One Direction - Made In The A.M. [Deluxe Edition] (2015) [FLAC] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="430819514">410.86 <span>MB</span></td>
			<td class="center">9</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-17T05:33:56+00:00">17 Nov 2015, 05:33:56</span></td>
			<td class="green center">25</td>
			<td class="red lasttd center">25</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/v-a-muddy-waters-100-2015-flac-t11588079.html" class="cellMainLink">V.A. - Muddy Waters 100 (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="387950246">369.98 <span>MB</span></td>
			<td class="center">57</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-14T15:53:41+00:00">14 Nov 2015, 15:53:41</span></td>
			<td class="green center">30</td>
			<td class="red lasttd center">5</td>
        </tr>
			</table>


	<script type="text/javascript">
		if ($.cookie('kat_settings[fpw_column]') != null && $.cookie('kat_settings[fpw_order]')) {
			var colIndex = $.cookie('kat_settings[fpw_column]')
			var ascend = $.cookie('kat_settings[fpw_order]');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			sortTables(colIndex, ascend);
		}
		$('.frontPageWidget .firstr th .fpw-sort').click(function() {
			var colIndex = $(this).parent().index();
			var ascend = !$(this).hasClass('sortedAsc')?1:0;
			$('.fpw-sort').removeClass('sortedAsc sortedDesc');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			updateFPWsettings(colIndex, ascend);
			sortTables(colIndex, ascend);
		});
		function sortTables(colIndex, ascend) {	
			$('.frontPageWidget').each(function() {
				var data = [];
				$('tr:not(.firstr)', $(this)).each(function() {
					var name = $('td:first .cellMainLink', $(this)).text();
					var id = $('td:first .cellMainLink', $(this)).attr('href').match(/.*-t(\d+)\.html/)[1];
					var fileSize = $('td:eq(1)', $(this)).attr('data-sort');
					data.push({
						'col0':name, 'col1':padNum(fileSize), 'col2':padNum($('td:eq(2)',$(this)).text()), 'col3':padNum(id), 'col4':padNum($('td:eq(4)',$(this)).text()), 'col5':padNum($('td:eq(5)',$(this)).text()), 'html':$(this).html()
					});
				});
				data.sort(function(a, b) {
					var x = a['col'+colIndex].toLowerCase(); var y = b['col'+colIndex].toLowerCase();
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
				if (!ascend||ascend=='0') data.reverse();
				$(this).find('tr:not(.firstr)').remove();
				for (var i=0;i<data.length;i++) {
					$(this).append('<tr class="'+((i+1)%2==0?'even':'odd')+'">'+data[i].html+'</tr>');
				}
			});
		}
		function padNum(val) {
			val = '0000000000000000'+val;
			return val.substring(val.length-15);
		}
		function updateFPWsettings(t, o) {
			$.cookie('kat_settings[fpw_column]', t, { expires: 365, path: '/' });
			$.cookie('kat_settings[fpw_order]', o, { expires: 365, path: '/' });
		}
	</script>
		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
    
            
        <div id="_119b0a17fab5493361a252d04bf527db"></div>
    
                
    	    <div class="spareBlock">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <div id="_7063408f1c01d50e0dc2d833186ce962"></div>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/sinhala-translation-words-discussion-thread/?unread=17112298">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Sinhala Translation Words Discussion Thread
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_4"><a class="plain" href="/user/Nispa/">Nispa</a></span></span> <time class="timeago" datetime="2015-11-17T17:31:50+00:00">17 Nov 2015, 17:31</time></span>
	</li>
		<li>
		<a href="/community/show/kickass-anime-community-v-6/?unread=17112294">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Kickass Anime Community V.6!
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/LawrenceFive/">LawrenceFive</a></span></span> <time class="timeago" datetime="2015-11-17T17:30:13+00:00">17 Nov 2015, 17:30</time></span>
	</li>
		<li>
		<a href="/community/show/edm-rg-request-thread/?unread=17112288">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				EDM RG Request Thread
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/ScrublordBG/">ScrublordBG</a></span></span> <time class="timeago" datetime="2015-11-17T17:28:12+00:00">17 Nov 2015, 17:28</time></span>
	</li>
		<li>
		<a href="/community/show/my-first-torrent-please-post-here-and-help-me/?unread=17112287">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				My First Torrent - Please POST Here! and Help Me..
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_4"><a class="plain" href="/user/thLullaby/">thLullaby</a></span></span> <time class="timeago" datetime="2015-11-17T17:28:03+00:00">17 Nov 2015, 17:28</time></span>
	</li>
		<li>
		<a href="/community/show/i-am-delicate-little-flower/?unread=17112278">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				I am a delicate little flower
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/demeanour20/">demeanour20</a></span></span> <time class="timeago" datetime="2015-11-17T17:25:30+00:00">17 Nov 2015, 17:25</time></span>
	</li>
		<li>
		<a href="/community/show/please-request-ebooks-and-audio-books-here-v12/?unread=17112276">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Please request ebooks and audio books here. V12
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/RavensFan/">RavensFan</a></span></span> <time class="timeago" datetime="2015-11-17T17:24:50+00:00">17 Nov 2015, 17:24</time></span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/new-site-rules/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				New site Rules
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-15T14:18:43+00:00">15 Oct 2015, 14:18</time></span>
	</li>
	<li>
		<a href="/blog/post/look-mama-i-m-popular/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Look, mama, I&#039;m popular!
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-05T17:42:40+00:00">05 Oct 2015, 17:42</time></span>
	</li>
	<li>
		<a href="/blog/post/summer-updates-september-1/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Summer updates [September, 1]
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-09-01T16:13:36+00:00">01 Sep 2015, 16:13</time></span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/TheDels/post/why-youtube-s-copyright-system-is-broken/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Why Youtube&#039;s Copyright System is Broken</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/TheDels/">TheDels</a> <time class="timeago" datetime="2015-11-17T10:01:27+00:00">17 Nov 2015, 10:01</time></span></li>
	<li><a href="/blog/floofiness/post/global-opportunities/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Global Opportunities</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/floofiness/">floofiness</a> <time class="timeago" datetime="2015-11-16T23:15:16+00:00">16 Nov 2015, 23:15</time></span></li>
	<li><a href="/blog/Maraya21/post/this-blog-doesn-t-contain-the-words-pray-paris-seriously-read-without-fear/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> This blog doesn&#039;t contain the words &quot;Pray&quot; &amp; &quot;Paris&quot;. Seriously, read without fear..</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/Maraya21/">Maraya21</a> <time class="timeago" datetime="2015-11-16T22:15:20+00:00">16 Nov 2015, 22:15</time></span></li>
	<li><a href="/blog/Bartacus/post/lets-have-some-fun/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Lets have some fun....</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/Bartacus/">Bartacus</a> <time class="timeago" datetime="2015-11-16T19:04:00+00:00">16 Nov 2015, 19:04</time></span></li>
	<li><a href="/blog/thLullaby/post/silent-observer/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Silent Observer</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/thLullaby/">thLullaby</a> <time class="timeago" datetime="2015-11-16T15:03:33+00:00">16 Nov 2015, 15:03</time></span></li>
	<li><a href="/blog/luxferre/post/a-memory-2/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> A memory 2</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/luxferre/">luxferre</a> <time class="timeago" datetime="2015-11-16T14:31:05+00:00">16 Nov 2015, 14:31</time></span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/ideer/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Ideer
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/games%2Bjava/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				games+java
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/the%20returned%20s01e07/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				the returned s01e07
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/larry%20the%20cable%20guy/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				larry the cable guy
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/sword%20art%20online%20season%202/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				sword art online season 2
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/the%20godfather/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				The Godfather
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/the%20sexxxtons/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				the sexxxtons
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/in%20the%20moment%20black%20widow/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				in the moment black widow
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/naughty%20america%202015/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				naughty america 2015
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/jacquieetmichel%20nancy/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				jacquieetmichel nancy
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/son%20of%20saul/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				son of saul
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a href="#" onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a href="#" onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a href="#" onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a href="#" onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a href="#" onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a href="#" onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a href="#" onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a href="#" onclick="setLanguage('bsc', '.kat.cr');return false;" class="plain">Bosnian-Cyrillic</a></li>
                                <li><a href="#" onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a href="#" onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a href="#" onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a href="#" onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a href="#" onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a href="#" onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a href="#" onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a href="#" onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a href="#" onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a href="#" onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a href="#" onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a href="#" onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a href="#" onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a href="#" onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a href="#" onclick="setLanguage('he', '.kat.cr');return false;" class="plain">Hebrew</a></li>
                                <li><a href="#" onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a href="#" onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a href="#" onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a href="#" onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a href="#" onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a href="#" onclick="setLanguage('ko', '.kat.cr');return false;" class="plain">Korean</a></li>
                                <li><a href="#" onclick="setLanguage('lv', '.kat.cr');return false;" class="plain">Latvian</a></li>
                                <li><a href="#" onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a href="#" onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a href="#" onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a href="#" onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a href="#" onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a href="#" onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a href="#" onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a href="#" onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a href="#" onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a href="#" onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a href="#" onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a href="#" onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a href="#" onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a href="#" onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a href="#" onclick="setLanguage('sl', '.kat.cr');return false;" class="plain">Slovenian</a></li>
                                <li><a href="#" onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a href="#" onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a href="#" onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a href="#" onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a href="#" onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a href="#" onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a href="#" onclick="setLanguage('ur', '.kat.cr');return false;" class="plain">Urdu</a></li>
                                <li><a href="#" onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" data-nop href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
        		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
        <li><a href="https://kastatus.com">KAT status</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <div id="_673e31f53f8166159b8e996c4124765b"></div>
        <div id="_e7050fb15fd39b3e4e99a5be4a57b6ea"></div>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script>
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "Organization",
"name": "KickassTorrents",
"url": "https://kat.cr",
"logo": "https://kat.cr/content/images/kickasslogo.png",
"sameAs": [
    "https://www.facebook.com/Official.KAT.Fanclub",
    "https://twitter.com/kickasstorrents"
]
}
</script>
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "WebSite",
"url": "https://kat.cr",
"potentialAction": {
    "@type": "SearchAction",
    "target": "https://kat.cr/usearch/{q}/",
    "query-input": {
        "@type": "PropertyValueSpecification",
        "valueRequired": true,
        "valueName": "q"
    }
}
}
</script>
<script>
 sc('addGlobal', 'pagetype', 'front');
 sc('addSlot', '_60318cd4e8d28f6fb76fe34e9bd9c498');
sc('addSlot', '_39ecb76dd457e5ac33776fdf11500d56');
sc('addSlot', '_277923e5f9d753c5b0630c28e641790c');
sc('addSlot', '_119b0a17fab5493361a252d04bf527db');
sc('addSlot', '_7063408f1c01d50e0dc2d833186ce962', { 'searchQuery': '' });
sc('addSlot', '_673e31f53f8166159b8e996c4124765b');
sc('addSlot', '_e7050fb15fd39b3e4e99a5be4a57b6ea');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>

</body>
</html>
