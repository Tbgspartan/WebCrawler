<!DOCTYPE html>
<html id="atomic" lang="en-US" class="atomic my3columns  l-out Pos-r https fp fp-v2 rc1 fp-default mini-uh-on viewer-right ltr desktop Desktop bkt201">
<head>
    <title>Yahoo</title><meta http-equiv="x-dns-prefetch-control" content="on"><link rel="dns-prefetch" href="//s.yimg.com"><link rel="preconnect" href="//s.yimg.com"><link rel="dns-prefetch" href="//y.analytics.yahoo.com"><link rel="preconnect" href="//y.analytics.yahoo.com"><link rel="dns-prefetch" href="//geo.query.yahoo.com"><link rel="preconnect" href="//geo.query.yahoo.com"><link rel="dns-prefetch" href="//csc.beap.bc.yahoo.com"><link rel="preconnect" href="//csc.beap.bc.yahoo.com"><link rel="dns-prefetch" href="//geo.yahoo.com"><link rel="preconnect" href="//geo.yahoo.com"><link rel="dns-prefetch" href="//comet.yahoo.com"><link rel="preconnect" href="//comet.yahoo.com">    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta name="description" content="News, email and search are just the beginning. Discover more every day. Find your yodel.">
    <meta name="keywords" content="yahoo, yahoo home page, yahoo homepage, yahoo search, yahoo mail, yahoo messenger, yahoo games, news, finance, sport, entertainment">
    <meta property="og:title" content="Yahoo" />
    <meta property="og:type" content='website' />
    <meta property="og:url" content="http://www.yahoo.com" />
    <meta property="og:description" content="News, email and search are just the beginning. Discover more every day. Find your yodel."/>
    <meta property="og:image" content="https://s.yimg.com/dh/ap/default/130909/y_200_a.png"/>
    <meta property="og:site_name" content="Yahoo" />
    <meta property="fb:app_id" content="90376669494" />
    <meta name="format-detection" content="telephone=no" />
    <link rel="icon" sizes="any" mask href="/sy/os/mit/media/p/common/images/favicon_new-7483e38.svg">
<meta name="theme-color" content="#400090">
    <link rel="shortcut icon" href="/sy/rz/l/favicon.ico" />
    <link rel="canonical" href="https://www.yahoo.com/" />        <link href="/sy/os/fp/atomic-css.d1bbf0c4.css" rel="stylesheet" type="text/css">
            
    
    
    
    <!-- streaming unlocked -->

    <!-- MapleTop -->
    
    
<link rel="stylesheet" type="text/css" href="/sy/zz/combo?nn/lib/metro/g/myy/advance_base_rc4_0.0.31.css&nn/lib/metro/g/myy/font_rc4_spdy_0.0.31.css&nn/lib/metro/g/myy/yahoo20_grid_0.0.96.css&nn/lib/metro/g/myy/video_styles_0.0.23.css&nn/lib/metro/g/myy/advance_color_0.0.7.css&nn/lib/metro/g/theme/viewer_modal_0.0.29.css&nn/lib/metro/g/theme/yglyphs-legacy_0.0.5.css&nn/lib/metro/g/sda/fp_sda_0.0.4.css&nn/lib/metro/g/sda/sda_advance_0.0.8.css&nn/lib/metro/g/fpfooter/advance_0.0.4.css&/os/stencil/3.1.0/styles-ltr.css&/os/yc/css/bundle.c60a6d54.css" />
    <link rel="search" type="application/opensearchdescription+xml" href="https://search.yahoo.com/opensearch.xml" title="Yahoo Search" />
    
    <script>
    var myYahoostartTime = new Date(),
        afPerfHeadStart=new Date().getTime(),
        ie;

    
    document.documentElement.className += ' JsEnabled jsenabled';</script>
    

<style>.breakingnews.gradient-1 {
    background: #ff7617; /* Old browsers */
    background: -moz-linear-gradient(45deg,  #ff7617 0%, #ff9b00 65%); /* FF3.6+ */
    background: -webkit-gradient(linear, left bottom, right top, color-stop(0%,#ff7617), color-stop(65%,#ff9b00)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(45deg,  #ff7617 0%,#ff9b00 65%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(45deg,  #ff7617 0%,#ff9b00 65%); /* Opera 11.10+ */
    background: -ms-linear-gradient(45deg,  #ff7617 0%,#ff9b00 65%); /* IE10+ */
    background: linear-gradient(45deg,  #ff7617 0%,#ff9b00 65%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff7617', endColorstr='#ff9b00',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
}

.breakingnews.gradient-2 {
    background: #e91857; /* Old browsers */
    background: -moz-linear-gradient(45deg,  #e91857 0%, #ff353c 65%); /* FF3.6+ */
    background: -webkit-gradient(linear, left bottom, right top, color-stop(0%,#e91857), color-stop(65%,#ff353c)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(45deg,  #e91857 0%,#ff353c 65%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(45deg,  #e91857 0%,#ff353c 65%); /* Opera 11.10+ */
    background: -ms-linear-gradient(45deg,  #e91857 0%,#ff353c 65%); /* IE10+ */
    background: linear-gradient(45deg,  #e91857 0%,#ff353c 65%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e91857', endColorstr='#ff353c',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
}</style>
<style>.js-stream-adfdb-reason {
    display: none;
}
.js-stream-adfdb-options .js-stream-adfdb-other-selected + .js-stream-adfdb-reason {
    display: block;
}

.stream-collapse {
    max-height: 0;
    -webkit-transition: max-height 0.3s ease;
    -moz-transition: max-height 0.3s ease;
    -o-transition: max-height 0.3s ease;
    transition: max-height 0.3s ease;
}

.RevealNested-on .ActionDislike {
    display: none;
}

.streamv2 .stream-share-open .js-stream-share-panel {
    height: auto !important;
    opacity: 1 !important;
}

/**
* Tooltips
*/
.js-stream-actions a:hover .ActionTooltip.hide,
.js-stream-actions .ActionTooltip,
.js-stream-actions a:active .ActionTooltip,
.js-stream-actions a:focus .ActionTooltip {
    clip: rect(1px 1px 1px 1px);
    clip: rect(1px,1px,1px,1px);
    height: 1px;
    width: 1px;
    overflow: hidden;
    line-height: 1.4;
    white-space: nowrap
}

.js-stream-actions a:hover .ActionTooltip {
    clip: rect(auto auto auto auto);
    clip: auto;
    height: auto;
    width: auto;
    overflow: visible;
    -webkit-transform: translateX(-50%) !important;
    -ms-transform: translateX(-50%) !important;
    transform: translateX(-50%) !important;
    *min-width: 80px;
    *margin-right: -48px;
    width: 136px;
    text-align: center;
}

.streamv2 .js-stream-dense .strm-left {
    max-width: 190px;
    width: 29%;
}

.streamv2 .js-stream-sparse .strm-left {
    width: 33%;
    max-width: 230px;
}
.streamv2 .js-stream-sparse .strm-right {
    width: 57%;
}
.streamv2 .js-stream-sparse .strm-full {
    width: 90%;
}

.streamv2 .js-stream-dense .strm-gs-tile-roundup .strm-left {
    width: 62.825%;
    max-width: 441px;
}

.streamv2 .js-stream-dense .strm-right-menu-roundup .strm-left {
    width: 72%;
    max-width: 508px;
}

.streamv2 .js-stream-dense .strm-right-menu-roundup .strm-right,
.streamv2 .js-stream-dense .strm-gs-tile-roundup .strm-right {
    width: auto;
    max-width: none;
}


.streamv2 .strm-right-menu-roundup .strm-right .js-stream-content-link:before {
    content: '';
    vertical-align: middle;
    height: 100%;
    display: inline-block;
}

.streamv2 .strm-chevron {
    display: none;
}

.streamv2 .strm-right-menu-roundup .js-stream-content-link.selected .strm-chevron,
.streamv2 .strm-right-menu-roundup .js-stream-content-link:hover .strm-chevron {
    display: block;
}

.streamv2 .rounded-img {
    border-radius: 3px;
}
.streamv2 .js-stream-sparse .storyline-main-img .rounded-img {
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
}
.streamv2 .js-stream-sparse .storyline-img-0 {
    border-bottom-left-radius: 3px;
}
.streamv2 .js-stream-sparse .storyline-img-1 {
    border-bottom-right-radius: 3px;
}

.ua-ff#atomic .strm-headline-label {
    margin-bottom: 4px;
}
/* cluster image gradient transparent to dark overlay */
.streamv2 .strm-img-gradient {
    background: -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0) 32%, rgba(0,0,0,0.65) 97%, rgba(0,0,0,0.65) 98%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(32%,rgba(0,0,0,0)), color-stop(97%,rgba(0,0,0,0.65)), color-stop(98%,rgba(0,0,0,0.65))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 32%,rgba(0,0,0,0.65) 97%,rgba(0,0,0,0.65) 98%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 32%,rgba(0,0,0,0.65) 97%,rgba(0,0,0,0.65) 98%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 32%,rgba(0,0,0,0.65) 97%,rgba(0,0,0,0.65) 98%); /* IE10+ */
    background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 32%,rgba(0,0,0,0.65) 97%,rgba(0,0,0,0.65) 98%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#a6000000',GradientType=0 ); /* IE6-9 */
}

/* cluster image gradient transparent to dark overlay */
.streamv2 .js-stream-roundup .strm-img-gradient {
    background: -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0) 52%, rgba(0,0,0,0.85) 107%, rgba(0,0,0,0.85) 78%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(52%,rgba(0,0,0,0)), color-stop(107%,rgba(0,0,0,0.85)), color-stop(78%,rgba(0,0,0,0.85))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 52%,rgba(0,0,0,0.85) 107%,rgba(0,0,0,0.85) 78%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 52%,rgba(0,0,0,0.85) 107%,rgba(0,0,0,0.85) 78%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 52%,rgba(0,0,0,0.85) 107%,rgba(0,0,0,0.85) 78%); /* IE10+ */
    background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 52%,rgba(0,0,0,0.85) 107%,rgba(0,0,0,0.85) 78%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#a6000000',GradientType=0 ); /* IE6-9 */
}

.ua-ie10 .streamv2 .js-stream-related-content ul,
.ua-ie10 .streamv2 .js-stream-roundup-filmstrip > ul {
    margin-right: -7px !important;
}

.ua-ie10 .streamv2 .js-stream-related-content li,
.ua-ie10 .streamv2 .js-stream-roundup-filmstrip .W\(25\%\) {
    width: 24.5% !important;
}

.streamv2 .js-stream-side-buttons li:nth-child(1) .ActionTooltip {
    top: -1px;
}
.streamv2 .js-stream-side-buttons.has-comments li:nth-child(1) .ActionTooltip {
    top: 0px;
}

.streamv2 .js-stream-side-buttons li:nth-child(2) .ActionTooltip {
    top: 22px;
}
.streamv2 .js-stream-side-buttons.has-comments li:nth-child(2) .ActionTooltip {
    top: 40px;
}
.streamv2 .js-stream-side-buttons li:nth-child(3) .ActionTooltip,
.streamv2 .js-stream-side-buttons li:nth-child(3) .js-stream-share-panel {
    top: 45px;
}
.streamv2 .js-stream-side-buttons.has-comments li:nth-child(3) .ActionTooltip,
.streamv2 .js-stream-side-buttons.has-comments li:nth-child(3) .js-stream-share-panel {
    top: 64px;
}
.streamv2 .js-stream-side-buttons li:nth-child(4) .js-stream-share-panel {
    top: 86px;
}

.streamv2 .js-stream-side-buttons .ActionComments {
    margin-top: 6px;
}
.streamv2 .js-stream-side-buttons.has-comments .ActionComments {
    margin-top: 0px;
}
.streamv2 .js-stream-side-buttons .ActionComments:hover .C\(\$c_icon\),
.streamv2 .js-stream-side-buttons .ActionComments:hover .C\(\#96989f\),
.streamv2 .js-stream-drawer .js-stream-comments-button:hover .C\(\$c_icon\),
.streamv2 .js-stream-drawer .js-stream-comments-button:hover .C\(\#96989f\) {
    color: inherit !important;
}
.streamv2 .js-stream-side-buttons .ActionComments .ActionTooltip {
    margin-top: 15px;
}

.js-stream-comment-counter-update {
    position: absolute;
    top: 0;
    right: 0;
    width: 100%;
}

/* Only set opacity to 0 for animation when js is enabled and css3 supported */
.JsEnabled .streamv2 .js-stream-comment-hidden:nth-of-type(1n) {
    opacity: 0;
}

.JsEnabled .streamv2 .animated {
    -webkit-animation-duration: 1.5s;
    animation-duration: 1.5s;
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;
}

@-webkit-keyframes fadeOut {
    from {
        opacity: 1;
    }
    to {
        opacity: 0;
    }
}

@keyframes fadeOut {
    from {
        opacity: 1;
    }
    to {
        opacity: 0;
    }
}

.JsEnabled .streamv2 .fadeOut {
    -webkit-animation-name: fadeOut;
    animation-name: fadeOut;
}

@-webkit-keyframes fadeIn {
    from {
        opacity: 0;
        color: #188fff;
    }
    to {
        opacity: 1;
        color: #96989f;
    }
}

@keyframes fadeIn {
    from {
        opacity: 0;
        color: #188fff;
    }
    to {
        opacity: 1;
        color: #96989f;
    }
}

@-webkit-keyframes fadeInNtk {
    from {
        opacity: 0;
        color: #188fff;
    }
    to {
        opacity: 1;
        color: #fff;
    }
}

@keyframes fadeInNtk {
    from {
        opacity: 0;
        color: #188fff;
    }
    to {
        opacity: 1;
        color: #fff;
    }
}

.JsEnabled .streamv2 .fadeIn {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
}


.JsEnabled .streamv2 .js-stream-roundup-filmstrip .fadeIn {
    -webkit-animation-name: fadeInNtk;
    animation-name: fadeInNtk;
}

.streamv2 .js-stream-comment .js-stream-cmnt-up:hover,
.streamv2 .js-stream-comment .js-stream-cmnt-up.selected {
    color: #1AC567 !important;
}
.streamv2 .js-stream-comment .js-stream-cmnt-down:hover,
.streamv2 .js-stream-comment .js-stream-cmnt-down.selected,
.streamv2 .js-stream-comment .js-stream-cmnt-flag:hover,
.streamv2 .js-stream-comment .js-stream-cmnt-flag.selected {
    color: #F0162F !important;
}

/* to show the dislike ad tooltip */
#atomic .Collapse-opened .js-stream-related-item-ad {
    overflow: visible;
}
</style>
 

<style>.js-activitylist-item .orb {
    background-position: 0 0;
    width: 80px;
    height: 80px;
}

.js-activitylist-item .hexagon {
    background-position: -81px 0;
    width: 80px;
    height: 90px;
}

.js-activitylist-item .ribbon {
    background-position: -162px 0;
    width: 91px;
    height: 31px;
}
</style>
 

<style>#uh-search .yui3-aclist {
    width: inherit !important;
}
#uh-search .yui3-aclist-content {
    background-color: #fff;
    text-align: left;
    border: 1px solid #ccc;
    margin-top: 1px;
    border-top: 0;
}
#uh-search .yui3-aclist-list {
    margin: 0;
    line-height: 1.1;
}
#uh-search .yui3-aclist-item {
    padding: 5px 0 6px 0;
    font-size: 18px;
    font-weight: bold;
}
#uh-search .yui3-aclist-item:hover,
#uh-search .yui3-aclist-item-active {
    background-color: #c6d7ff;
}
#uh-search .yui3-aclist-item {
    padding-left: 10px;
    padding-right: 10px;
}
#uh-search .yui3-highlight {
    font-weight: 200;
}
#uh-search-box::-ms-clear {
    display: none;
}


/* Instant Search Styles */

#uh-search .yui3-aclist-item {
    line-height: 0.9;
}
#InstantSearchMask {
    visibility: hidden;
    opacity: 0;
    transition: visibility 0s linear 0.15s, opacity 0.15s linear;
}
.iSearch-display .Col1,
.iSearch-display .Col2,
.iSearch-display .Col3 {
    height: 0;
    overflow: hidden;
}
.iSearch-display #InstantSearch {
    visibility: visible;
}
.iSearch-mask #InstantSearchMask {
    visibility: visible;
    opacity: 1;
    transition: visibility 0s linear, opacity 0.15s linear;
}
.iSearch-loading.iSearch-mask #InstantSearchMask,
.iSearch-loading #InstantSearchMask {
    visibility: visible;
    opacity: .7;
}


/* Firefox 28 and below fix */
#uh-search-box,
#uh-ghost-box,
.instant-filters,
.instant-results {
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
/* ----------------------- */
</style>
 

<style>@charset "UTF-8";.Lb-Close-Icon,.icon-share-close,.icons-arrow,.icons-slideshow-icon{background-color:transparent;background-image:url(/sy/os/publish-images/news/2014-04-23/65fcff60-cb23-11e3-bead-55c0602d5659_icons-sb930b067ee.png);background-repeat:no-repeat}#Share .icon-share-close{width:30px;height:25px;background-size:cover;background-position:8px -72px}.Hl-Viewer{background:#fff}.Hl-Viewer .Card-Item .Lead-l~.Cover-Overlay{position:absolute;color:#fff;background:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodâ¦EiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);background:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4gPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGRlZnM+PGxpbmVhckdyYWRpZW50IGlkPSJncmFkIiBncmFkaWVudFVuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgeDE9IjAuNSIgeTE9IjAuMCIgeDI9IjAuNSIgeTI9IjEuMCI+PHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjAiLz48c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMwMDAwMDAiIHN0b3Atb3BhY2l0eT0iMC45NSIvPjwvbGluZWFyR3JhZGllbnQ+PC9kZWZzPjxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA==);background:-webkit-gradient(linear,50% 0,50% 100%,color-stop(0%,rgba(0,0,0,0)),color-stop(100%,rgba(0,0,0,.95)));background:-moz-linear-gradient(rgba(0,0,0,0),rgba(0,0,0,.95));background:-webkit-linear-gradient(rgba(0,0,0,0),rgba(0,0,0,.95));background:linear-gradient(rgba(0,0,0,0),rgba(0,0,0,.95))}.Hl-Viewer .Card-Item .Lead-l~.Cover-Overlay .Headline{text-shadow:0 1px 0 #000}.Hl-Viewer .Card-Item .Lead-l~.Cover-Overlay .Headline-Box{position:absolute}.Hl-Viewer .Content-Body{color:#000;font-weight:300;letter-spacing:.3px;word-wrap:break-word;word-break:break-word}.Hl-Viewer .Content-Body>p{margin-bottom:1.45em;margin-top:1.45em}.Hl-Viewer .Content-Body h1,.Hl-Viewer .Content-Body h2,.Hl-Viewer .Content-Body h3,.Hl-Viewer .Content-Body h4,.Hl-Viewer .Content-Body h5,.Hl-Viewer .Content-Body h6{font-weight:400}.Hl-Viewer .Content-Body h1{font-size:18px;font-size:1.8rem}.Hl-Viewer .Content-Body h2{font-size:17px;font-size:1.7rem}.Hl-Viewer .Content-Body h3{font-size:16px;font-size:1.6rem}.Hl-Viewer .Content-Body h4{font-size:15px;font-size:1.5rem}.Hl-Viewer .Content-Body h5{font-size:14px;font-size:1.4rem}.Hl-Viewer .Content-Body h6{font-size:13px;font-size:1.3rem}.Hl-Viewer .icons-slideshow-icon{left:5%;top:5%;height:46px;width:47px;background-size:cover}.Hl-Viewer blockquote{padding:5px 10px;quotes:"\201C" "\201C" "\201C" "\201C";line-height:18px}.Hl-Viewer blockquote:before{color:#ccc;content:open-quote;font-size:3em;line-height:.1em;vertical-align:-.4em}.Hl-Viewer blockquote p{display:inline;color:#747474;font-weight:400}.Hl-Viewer blockquote p:after{content:"\A";white-space:pre}.Hl-Viewer .Credit,.Hl-Viewer .Provider{letter-spacing:.5px}.JsEnabled .ImageLoader-Delayed,.JsEnabled .ImageLoader-Loaded{-moz-transition-duration:.2s;-o-transition-duration:.2s;-webkit-transition-duration:.2s;transition-duration:.2s;-moz-transition-property:opacity;-o-transition-property:opacity;-webkit-transition-property:opacity;transition-property:opacity;-moz-transition-timing-function:ease-out;-o-transition-timing-function:ease-out;-webkit-transition-timing-function:ease-out;transition-timing-function:ease-out}#Stencil .Bdrs-100{border-top-left-radius:100px;border-top-right-radius:100px;border-bottom-left-radius:100px;border-bottom-right-radius:100px}.Slideshow-Lightbox .lb-meta-content{padding-bottom:30px;background:#fff}.Slideshow-Lightbox .lb-meta-content .lb-meta-txt-container-full,.Slideshow-Lightbox .lb-meta-content.expand .lb-meta-txt-container-short{display:none}.Slideshow-Lightbox .lb-meta-content.expand .lb-meta-txt-container-full{display:block}.Slideshow-Lightbox .hide-meta .lb-meta-content{display:none}.Slideshow-Lightbox .Lb-Close-Icon{background-size:cover;width:47px;height:47px;z-index:202;background-position:0 -46px}.Reader-open .BrandBar,.Reader-open .rmp-TDYDBM{display:none}.Content-Body .Editorial-Left{float:left;margin-right:30px}.Content-Body .Editorial-Right{float:right;margin-left:30px}.Content-Body .Editorial-Left,.Content-Body .Editorial-Right{margin-top:0}#hl-viewer .Content-Col,.MagOn .Content-Col{margin-left:300px;margin-right:300px;min-height:100px}.HideCentralColumn .Content-Col{margin-left:150px;margin-right:150px}#hl-viewer{min-height:500px}.ShareBtns a:hover{opacity:.3;filter:alpha(opacity=33)}.modal-actions{bottom:3px}.modal-actions .ShareBtns a:hover{opacity:1;filter:alpha(opacity=100)}#hl-viewer:focus{outline:0}#hl-viewer .Timestamp{color:#abaeb7}#hl-viewer .Viewer-Close-Btn{position:fixed;top:10px;width:28px;height:28px;color:#fff;background-color:#2D1152;font-size:16px;margin-left:11px;z-index:5}#hl-viewer .ShareBtns .Share-Btn{border-radius:50px;width:17px;height:17px;line-height:1.3}#hl-viewer .ShareBtns .Share-Btn:hover{text-decoration:none}#hl-viewer .slideshow-carousel{background:#212124;padding-bottom:66%}#hl-viewer .lb-meta-content{border-bottom:1px solid #e8e8e8}#hl-viewer .lb-meta-caption-container{-webkit-font-smoothing:antialiased}#hl-viewer .lb-meta-txt-container.caption-scroll{max-height:4.3em;overflow-y:auto}#hl-viewer .viewer-wrapper{background:#fff;margin:0 0 30px 58px;min-width:980px}#hl-viewer .viewer-wrapper.mega-modal{min-height:750px}#hl-viewer .content-modal,#hl-viewer .js-viewer-slot-readMore{display:inline-block;width:640px;margin-right:29px;vertical-align:top}#hl-viewer .js-slider-item.first .content-modal{padding-top:0}#hl-viewer .Content-Body{padding-bottom:50px;border-bottom:1px solid #c8c8c8}#hl-viewer .Content-Body i{font-style:italic}#hl-viewer .js-slider-item.last .Content-Body{padding-bottom:0;border-bottom:none}#hl-viewer .Content-Body p:last-child{margin-bottom:0!important}#hl-viewer .js-slider-item.multirightrail .Content-Body.multirightrail{border-bottom:none}#hl-viewer .js-slider-item.multirightrail .content-modal{padding-top:0}#hl-viewer .js-slider-item.multirightrail{border-top:1px solid #B2B2B2;padding-top:50px}#hl-viewer .js-slider-item.multirightrail.first{padding-top:0;border-top:none}#hl-viewer .modal-aside.single{position:absolute;right:0;float:none;width:300px}#hl-viewer .modal-aside{display:inline-block;width:300px;position:relative;vertical-align:top;z-index:1}#hl-viewer .slideshow-carousel .main-col{padding-top:15px}#hl-viewer .index-count{color:#878c91;position:absolute;left:12px;bottom:7px;-webkit-font-smoothing:antialiased}#hl-viewer .js-lb-next,#hl-viewer .js-lb-prev{top:50%;z-index:3;width:28px;color:#878c91;font-size:30px;margin-top:-17px}#hl-viewer .js-lb-next:hover,#hl-viewer .js-lb-prev:hover{color:#fff}#hl-viewer .js-lb-next.Disabled,#hl-viewer .js-lb-prev.Disabled{display:none}#hl-viewer .js-lb-next:focus,#hl-viewer .js-lb-prev:focus{outline:0}#hl-viewer .slideshowv2 .slv2-carousel{padding-bottom:78%}#hl-viewer .slideshowv2 .slv2-carousel .main-col{padding-top:20px;padding-bottom:60px}#hl-viewer .slideshowv2 .slv2-carousel .js-thm-sl-icon{color:#fff;font-size:25px;top:12px;display:none}#hl-viewer .slideshowv2 .slv2-carousel .sl-thumb-wide .sl-slider-ct{background-color:rgba(33,33,35,.5);border-radius:10px}#hl-viewer .slideshowv2 .slv2-carousel .sl-thumb-wide .sl-car-mask{width:300px;opacity:1}#hl-viewer .slideshowv2 .slv2-carousel .sl-thumb-wide .js-thm-sl-icon{display:inline-block}#hl-viewer .slideshowv2 .slv2-carousel .sl-thumb-wide .sl-pic{width:52px;height:52px}#hl-viewer .slideshowv2 .js-lb-next,#hl-viewer .slideshowv2 .js-lb-prev{opacity:.8;background:#444;color:#fff;width:45px;height:45px;padding:0;border-radius:3px}#hl-viewer .slideshowv2 .js-lb-next:hover,#hl-viewer .slideshowv2 .js-lb-prev:hover{background:#333;opacity:1}#hl-viewer .slideshowv2 .index-count{bottom:inherit;left:inherit;color:#000;position:relative;font-size:19px;font-size:1.9rem;font-weight:500;margin-bottom:8px}#hl-viewer .slideshowv2 .slide-title{font-weight:700;margin-bottom:4px}#hl-viewer .slideshowv2 .lb-meta-txt-container a:hover,#hl-viewer .slideshowv2 .lb-meta-txt-container a:link,#hl-viewer .slideshowv2 .lb-meta-txt-container a:visited{color:#188fff}#hl-viewer .slideshowv2 .sl-thumbnails{bottom:10px}#hl-viewer .slideshowv2 .sl-slider-ct{width:350px}#hl-viewer .slideshowv2 .sl-car-mask{width:150px;opacity:.5;transition:height .2s}#hl-viewer .slideshowv2 .sl-carousel .Selected .sl-pic{border:2px solid #fff}#hl-viewer .slideshowv2 .sl-carousel .sl-pic{width:22px;height:22px;border:2px solid transparent}#hl-viewer .Headline{line-height:1.2;margin-bottom:12px;font-size:30px;font-size:3rem}#hl-viewer .Headline-Box{margin-bottom:10px}#hl-viewer .Off-Network{border:1px solid #188FFF;padding:8px 20px;color:#188FFF!important;font-weight:700;text-decoration:none}#hl-viewer .Off-Network:hover{color:#fff!important;background:#0078ff;border-color:#0078ff}#hl-viewer .Content-Body p,#hl-viewer .remaining-body p{margin-bottom:1.1em;margin-top:0}#hl-viewer .Content-Body p p,#hl-viewer .remaining-body p p{margin:0}#hl-viewer img.Mb-20+p{margin-top:0}#hl-viewer .Content-Body,#hl-viewer .remaining-body{font-weight:400;font-size:15px;font-size:1.55rem;line-height:1.5;font-family:"Helvetica Neue",Helvetica,Arial,sans-serif!important}#hl-viewer .Content-Body #photo_copyright,#hl-viewer .Content-Body .caption,#hl-viewer .Content-Body .credit,#hl-viewer .Content-Body .source,#hl-viewer .remaining-body #photo_copyright,#hl-viewer .remaining-body .caption,#hl-viewer .remaining-body .credit,#hl-viewer .remaining-body .source{font-size:11px;font-size:1.1rem;color:#b2b2b2}#hl-viewer h2,#hl-viewer h3{font-size:22px;font-size:2.2rem;margin-bottom:.6em}#hl-viewer h4,#hl-viewer h5,#hl-viewer h6{font-size:19px;font-size:1.9rem;margin-bottom:.6em}#hl-viewer .Embed-Img{margin-bottom:20px}#hl-viewer .image{margin-bottom:20px;display:block;line-height:1}#hl-viewer .image .Embed-Img{margin-bottom:0}#hl-viewer .sidekick.fp h4{font-size:13px;font-size:1.3rem;margin-bottom:0}#hl-viewer .twitter-tweet-rendered{margin:10px auto}#hl-viewer #photo_copyright{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif!important}#hl-viewer .alignright{float:right;margin:0 0 10px 10px}#hl-viewer .alignleft{float:left;margin:0 10px 10px 0}#hl-viewer .pmc-related-type{margin-right:10px}#hl-viewer blockquote{line-height:1.6}#hl-viewer p:empty{display:none}#hl-viewer .Ad-Viewer blockquote p,#hl-viewer .Hl-Viewer blockquote p{display:block}#hl-viewer .hl-ad-LREC{height:250px}#hl-viewer .source{font-size:14px;font-size:1.4rem}#hl-viewer .attribution{font-size:12px}#hl-viewer .first .Fixed-Header{top:-100px}#hl-viewer .Fixed-Header{transition:opacity .8s ease-in-out,top .6s ease-in-out;-webkit-transition:opacity .8s ease-in-out,top .6s ease-in-out;position:fixed;top:0;width:600px;padding:14px 0 0}#hl-viewer .Fixed-Header .source{max-width:540px}#hl-viewer .Fixed-Header .modal-actions{top:11px;left:540px;width:100px!important}#hl-viewer .Fixed-Header-Wrap{position:fixed;top:-100px;width:100%;z-index:4;height:48px;background-color:#fff;margin-left:-57px;border-bottom:1px solid #e5e5e5;transition:opacity .6s ease-in-out,top .6s ease-in-out;-webkit-transition:opacity .6s ease-in-out,top .6s ease-in-out;opacity:0;box-shadow:0 2px 4px -1px rgba(0,9,30,.1)}#hl-viewer .fadein{opacity:1}#hl-viewer .backfill-video-ads{width:300px;height:169px}#hl-viewer .backfill-video-ads h3{font-size:16px;font-size:1.6rem}#hl-viewer .backfill-video-ads h4{font-size:15px;font-size:1.5rem}#hl-viewer .backfill-video-ads .yvp-setting-btn{display:none}#hl-viewer .SidekickTV .list-view-item{display:inline-block;zoom:1;letter-spacing:normal;word-spacing:normal;text-rendering:auto;vertical-align:top}#hl-viewer .SidekickTV .SidekickTVArrow{border-bottom:40px solid transparent;border-left:40px solid}#hl-viewer .SidekickTV .ad-sponsored{color:#959595;font-size:11px;margin-right:0;padding-right:0}#hl-viewer .continue_reading .arraw,#hl-viewer .js-viewer-view-article .arraw{border-left:5px solid transparent;border-right:5px solid transparent;border-top:6px solid #000;vertical-align:middle}#hl-viewer .continue_reading:hover{color:#188FFF}#hl-viewer .continue_reading:hover .arraw{border-top-color:#188FFF}#hl-viewer .Mt-neg-40{margin-top:-40px}#hl-viewer .Mt-neg-30{margin-top:-30px}#hl-viewer .modal-sidekick-following .sidekick.fp{margin-top:-20px}#hl-viewer figure{margin:0}#hl-viewer iframe{border:none}#hl-viewer #viewer-end{height:1px;width:1px;display:block}#hl-viewer #viewer-end:focus{outline:0}.ShareBtns .Share-Btn{padding:5px;border:1px solid #abaeb7}.ShareBtns .Share-Btn:hover{opacity:1;filter:alpha(opacity=100)}.ShareBtns .Tumblr-Btn{color:#35506d}.ShareBtns .Tumblr-Btn:hover{color:#35506d;background-color:#3593d3}.ShareBtns .Facebook-Btn{color:#3b5998}.ShareBtns .Facebook-Btn:hover{color:#3b5998;background-color:#4e91f2}.ShareBtns .Twitter-Btn{color:#00aced}.ShareBtns .Twitter-Btn:hover{color:#00aced;background-color:#94e8ff}.ShareBtns .Mail-Btn{background-position:-1px 161px;color:#0a80e3}.ShareBtns .Mail-Btn:hover{color:#0a80e3;background-color:#94e8ff}.ShareBtns .StickyPholder{display:inline-block}.ShareBtns .Viewer-Close-Btn{background-position:0 185px;padding:5px;margin-left:40px}.ShareBtns .Viewer-Close-Btn.Sticky{margin-left:-19px}#ModalSticker{max-width:1180px}#ModalSticker.Sticky{display:block!important;opacity:1;filter:alpha(opacity=100)}#ModalSticker .content-modal{border-bottom-width:3px;border-color:#000}#ModalSticker .ShareBtns{position:relative;float:right;margin-top:5px}.Modal-Credit{max-width:200px}.video-stage{margin-right:240px}.video-side-stage{width:240px}.Nav-Start{display:none}.First-Item .Nav-Start{display:block}.First-Item .Nav-Next,.First-Item .Nav-Prev{display:none}.slide-nav-arrow{background-color:#009bfb}.slide-nav-arrow:hover{opacity:1;filter:alpha(opacity=100)}.modal-sidekick,.modal-sidekick-following{display:inline-block}.js-sidekick-container{border-top:1px solid #f1f1f5}.modal-tooltip:hover .modal-tooltip\:h_H\(a\){height:auto!important}.modal-tooltip:hover .modal-tooltip\:h_Op\(1\){opacity:1!important}.c-modal-red{color:#ff156f}.lrec-before-loading{width:300px;height:250px;border:1px solid #e0e4e9;-webkit-transition:height 1.5s;-moz-transition:height 1.5s;-ms-transition:height 1.5s;-o-transition:height 1.5s;transition:height 1.5s}.js-header-searchbox{display:none;position:absolute;height:30px;left:730px;top:9px;border-radius:2px}.js-header-searchbox .srch-input{width:268px;height:100%;line-height:inherit;vertical-align:top;outline:0;box-shadow:none;border:none;border:1px solid #b0b0b0;border-radius:2px 0 0 2px;padding:0 10px}.js-header-searchbox .srch-input:focus{border-color:#0179ff}.js-header-searchbox button{width:32px;height:100%;background-color:#0179ff;font-size:16px;font-weight:700;color:#fff;border-radius:0 2px 2px 0;margin-left:-4px}.Z-6{z-index:6}.Z-7{z-index:7}.Z-8{z-index:8}.Z-9{z-index:9}.fixZindex{z-index:5!important}.viewer-right .js-slider-item.multirightrail.first{margin-top:25px!important}.viewer-right .Viewer-Close-Btn{border-radius:20px!important;font-size:17px!important;height:37px!important;margin-left:-18px!important;top:87px!important;width:37px!important}.viewer-right .viewer-wrapper{border-radius:6px;margin:1px!important;padding:0 0 30px 40px!important}.viewer-right.stream-sparse .viewer-wrapper{top:-12px}@media screen and (min-width:1100px){.js-header-searchbox{display:inline-block}}.Pt-35{padding-top:35px}.Pt-7{padding-top:7px}.JsEnabled .ImageLoader-Delayed{background:#f5f5f5}</style>
 

<style>#applet_p_63794 .Bd-0{border:0}#applet_p_63794 .Bd-1{border-width:1px}#applet_p_63794 .Bdx-1{border-right-width:1px;border-left-width:1px}#applet_p_63794 .Bdy-1{border-top-width:1px;border-bottom-width:1px}#applet_p_63794 .Bd-t{border-top-width:1px}#applet_p_63794 .Bd-end{border-right-width:1px}#applet_p_63794 .Bd-b{border-bottom-width:1px}#applet_p_63794 .Bd-start{border-left-width:1px}#applet_p_63794 .Bdt-0{border-top:0}#applet_p_63794 .Bdend-0{border-right:0}#applet_p_63794 .Bdb-0{border-bottom:0}#applet_p_63794 .Bdstart-0{border-left:0}#applet_p_63794 .Bdrs-0{border-radius:0}#applet_p_63794 .Bdtrrs-0{border-top-right-radius:0}#applet_p_63794 .Bdtlrs-0{border-top-left-radius:0}#applet_p_63794 .Bdbrrs-0{border-bottom-right-radius:0}#applet_p_63794 .Bdblrs-0{border-bottom-left-radius:0}#applet_p_63794 .Bdrs-100{border-radius:100px}#applet_p_63794 .Bdtrrs-100{border-top-right-radius:100px}#applet_p_63794 .Bdtlrs-100{border-top-left-radius:100px}#applet_p_63794 .Bdbrrs-100{border-bottom-right-radius:100px}#applet_p_63794 .Bdblrs-100{border-bottom-left-radius:100px}#applet_p_63794 .Bdrs-300{border-radius:300px}#applet_p_63794 .Bdtrrs-300{border-top-right-radius:300px}#applet_p_63794 .Bdtlrs-300{border-top-left-radius:300px}#applet_p_63794 .Bdbrrs-300{border-bottom-right-radius:300px}#applet_p_63794 .Bdblrs-300{border-bottom-left-radius:300px}#applet_p_63794 .Bdrs{border-radius:3px}#applet_p_63794 .Bdtrrs{border-top-right-radius:3px}#applet_p_63794 .Bdtlrs{border-top-left-radius:3px}#applet_p_63794 .Bdbrrs{border-bottom-right-radius:3px}#applet_p_63794 .Bdblrs{border-bottom-left-radius:3px}#applet_p_63794 .Ff{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif}#applet_p_63794 .Ff-a{font-family:Georgia,"Times New Roman",serif}#applet_p_63794 .Ff-b{font-family:Helvetica,Arial,sans-serif}#applet_p_63794 .Ff-c{font-family:"Monotype Corsiva","Comic Sans MS",cursive}#applet_p_63794 .Ff-d{font-family:Capitals,Impact,fantasy}#applet_p_63794 .Ff-e{font-family:Monaco,"Courier New",monospace}#applet_p_63794 .Fz-0{font-size:0}#applet_p_63794 .Fz-3xs{font-size:7px}#applet_p_63794 .Fz-2xs{font-size:9px}#applet_p_63794 .Fz-xs{font-size:11px;}#applet_p_63794 .Fz-s{font-size:13px;}#applet_p_63794 .Fz-m{font-size:15px;}#applet_p_63794 .Bgc-t{background-color:transparent}#applet_p_63794 .Bgi-n{background-image:none}#applet_p_63794 .Bg-n{background:0 0}#applet_p_63794 .Bgcp-bb{background-clip:border-box}#applet_p_63794 .Bgcp-pb{background-clip:padding-box}#applet_p_63794 .Bgcp-cb{background-clip:content-box}#applet_p_63794 .Bgo-pb{background-origin:padding-box}#applet_p_63794 .Bgo-bb{background-origin:border-box}#applet_p_63794 .Bgo-cb{background-origin:content-box}#applet_p_63794 .Bgz-a{background-size:auto}#applet_p_63794 .Bgz-ct{background-size:contain}#applet_p_63794 .Bgz-cv{background-size:cover;background-position:50% 15%}#applet_p_63794 .Bdcl-c{border-collapse:collapse}#applet_p_63794 .Bdcl-s{border-collapse:separate}#applet_p_63794 .Bxz-cb{-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box}#applet_p_63794 .Bxz-bb{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}#applet_p_63794 .Bxsh-n{box-shadow:none}#applet_p_63794 .Cl-n{clear:none}#applet_p_63794 .Cl-b{clear:both}#applet_p_63794 .Cl-start{clear:left}#applet_p_63794 .Cl-end{clear:right}#applet_p_63794 .Cur-d{cursor:default}#applet_p_63794 .Cur-he{cursor:help}#applet_p_63794 .Cur-m{cursor:move}#applet_p_63794 .Cur-na{cursor:not-allowed}#applet_p_63794 .Cur-nsr{cursor:ns-resize}#applet_p_63794 .Cur-p{cursor:pointer}#applet_p_63794 .Cur-cr{cursor:col-resize}#applet_p_63794 .Cur-w{cursor:wait}#applet_p_63794 .Cur-a{cursor:auto!important}#applet_p_63794 .D-n{display:none}#applet_p_63794 .D-b{display:block}#applet_p_63794 .D-i{display:inline}#applet_p_63794 .D-ib{display:inline-block;*display:inline;zoom:1}#applet_p_63794 .D-tb{display:table}#applet_p_63794 .D-tbr{display:table-row}#applet_p_63794 .D-tbc{display:table-cell}#applet_p_63794 .D-li{display:list-item}#applet_p_63794 .D-ri{display:run-in}#applet_p_63794 .D-cp{display:compact}#applet_p_63794 .D-itb{display:inline-table}#applet_p_63794 .D-tbcl{display:table-column}#applet_p_63794 .D-tbclg{display:table-column-group}#applet_p_63794 .D-tbhg{display:table-header-group}#applet_p_63794 .D-tbfg{display:table-footer-group}#applet_p_63794 .D-tbrg{display:table-row-group}#applet_p_63794 .Fl-n{float:none}#applet_p_63794 .Fl-start{float:left;_display:inline}#applet_p_63794 .Fl-end{float:right;_display:inline}#applet_p_63794 .Fw-n{font-weight:400}#applet_p_63794 .Fw-b{font-weight:700;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}#applet_p_63794 .Fw-100{font-weight:100;*font-weight:normal}#applet_p_63794 .Fw-200{font-weight:200;*font-weight:normal}#applet_p_63794 .Fw-400{font-weight:400;*font-weight:normal}#applet_p_63794 .Fw-br{font-weight:bolder}#applet_p_63794 .Fw-lr{font-weight:lighter}#applet_p_63794 .Fs-n{font-style:normal}#applet_p_63794 .Fs-i{font-style:italic}#applet_p_63794 .Fv-sc{font-variant:small-caps}#applet_p_63794 .Fv-n{font-variant:normal}#applet_p_63794 .H-0{height:0}#applet_p_63794 .H-50{height:50%}#applet_p_63794 .H-100{height:100%}#applet_p_63794 .H-a{height:auto}#applet_p_63794 .H-n,#applet_p_63794 .h-n{-webkit-hyphens:none;-moz-hyphens:none;-ms-hyphens:none;hyphens:none}#applet_p_63794 .List-n{list-style-type:none}#applet_p_63794 .List-d{list-style-type:disc}#applet_p_63794 .List-c{list-style-type:circle}#applet_p_63794 .List-s{list-style-type:square}#applet_p_63794 .List-dc{list-style-type:decimal}#applet_p_63794 .List-dclz{list-style-type:decimal-leading-zero}#applet_p_63794 .List-lr{list-style-type:lower-roman}#applet_p_63794 .List-ur{list-style-type:upper-roman}#applet_p_63794 .Lisi-n{list-style-image:none}#applet_p_63794 .Lh-0{line-height:0}#applet_p_63794 .Lh-01{line-height:.1}#applet_p_63794 .Lh-02{line-height:.2}#applet_p_63794 .Lh-03{line-height:.3}#applet_p_63794 .Lh-04{line-height:.4}#applet_p_63794 .Lh-05{line-height:.5}#applet_p_63794 .Lh-06{line-height:.6}#applet_p_63794 .Lh-07{line-height:.7}#applet_p_63794 .Lh-08{line-height:.8}#applet_p_63794 .Lh-09{line-height:.9}#applet_p_63794 .Lh-1{line-height:1}#applet_p_63794 .Lh-11{line-height:1.1}#applet_p_63794 .Lh-12{line-height:1.2}#applet_p_63794 .Lh-125{line-height:1.25}#applet_p_63794 .Lh-13{line-height:1.3}#applet_p_63794 .Lh-14{line-height:1.4}#applet_p_63794 .Lh-15{line-height:1.5}#applet_p_63794 .Lh-16{line-height:1.6}#applet_p_63794 .Lh-17{line-height:1.7}#applet_p_63794 .Lh-18{line-height:1.8}#applet_p_63794 .Lh-19{line-height:1.9}#applet_p_63794 .Lh-2{line-height:2}#applet_p_63794 .Lh-21{line-height:2.1}#applet_p_63794 .Lh-22{line-height:2.2}#applet_p_63794 .Lh-23{line-height:2.3}#applet_p_63794 .Lh-24{line-height:2.4}#applet_p_63794 .Lh-25{line-height:2.5}#applet_p_63794 .Lh-3{line-height:3}#applet_p_63794 .Lh-reset{line-height:normal}#applet_p_63794 .M-0{margin:0}#applet_p_63794 .M-2{margin:2px}#applet_p_63794 .M-4{margin:4px}#applet_p_63794 .M-6{margin:6px}#applet_p_63794 .M-8{margin:8px}#applet_p_63794 .M-10{margin:10px}#applet_p_63794 .M-12{margin:12px}#applet_p_63794 .M-14{margin:14px}#applet_p_63794 .M-16{margin:16px}#applet_p_63794 .M-18{margin:18px}#applet_p_63794 .M-20{margin:20px}#applet_p_63794 .Mx-1{margin-right:1px;margin-left:1px}#applet_p_63794 .Mx-2{margin-right:2px;margin-left:2px}#applet_p_63794 .Mx-4{margin-right:4px;margin-left:4px}#applet_p_63794 .Mx-6{margin-right:6px;margin-left:6px}#applet_p_63794 .Mx-8{margin-right:8px;margin-left:8px}#applet_p_63794 .Mx-10{margin-right:10px;margin-left:10px}#applet_p_63794 .Mx-12{margin-right:12px;margin-left:12px}#applet_p_63794 .Mx-14{margin-right:14px;margin-left:14px}#applet_p_63794 .Mx-16{margin-right:16px;margin-left:16px}#applet_p_63794 .Mx-18{margin-right:18px;margin-left:18px}#applet_p_63794 .Mx-20{margin-right:20px;margin-left:20px}#applet_p_63794 .My-1{margin-top:1px;margin-bottom:1px}#applet_p_63794 .My-2{margin-top:2px;margin-bottom:2px}#applet_p_63794 .My-4{margin-top:4px;margin-bottom:4px}#applet_p_63794 .My-6{margin-top:6px;margin-bottom:6px}#applet_p_63794 .My-8{margin-top:8px;margin-bottom:8px}#applet_p_63794 .My-10{margin-top:10px;margin-bottom:10px}#applet_p_63794 .My-12{margin-top:12px;margin-bottom:12px}#applet_p_63794 .My-14{margin-top:14px;margin-bottom:14px}#applet_p_63794 .My-16{margin-top:16px;margin-bottom:16px}#applet_p_63794 .My-18{margin-top:18px;margin-bottom:18px}#applet_p_63794 .My-20{margin-top:20px;margin-bottom:20px}#applet_p_63794 .Mt-1{margin-top:1px}#applet_p_63794 .Mb-1{margin-bottom:1px}#applet_p_63794 .Mstart-1{margin-left:1px}#applet_p_63794 .Mend-1{margin-right:1px}#applet_p_63794 .Mt-2{margin-top:2px}#applet_p_63794 .Mb-2{margin-bottom:2px}#applet_p_63794 .Mstart-2{margin-left:2px}#applet_p_63794 .Mend-2{margin-right:2px}#applet_p_63794 .Mt-4{margin-top:4px}#applet_p_63794 .Mb-4{margin-bottom:4px}#applet_p_63794 .Mstart-4{margin-left:4px}#applet_p_63794 .Mend-4{margin-right:4px}#applet_p_63794 .Mt-6{margin-top:6px}#applet_p_63794 .Mb-6{margin-bottom:6px}#applet_p_63794 .Mstart-6{margin-left:6px}#applet_p_63794 .Mend-6{margin-right:6px}#applet_p_63794 .Mt-8{margin-top:8px}#applet_p_63794 .Mb-8{margin-bottom:8px}#applet_p_63794 .Mstart-8{margin-left:8px}#applet_p_63794 .Mend-8{margin-right:8px}#applet_p_63794 .Mt-10{margin-top:10px}#applet_p_63794 .Mb-10{margin-bottom:10px}#applet_p_63794 .Mstart-10{margin-left:10px}#applet_p_63794 .Mend-10{margin-right:10px}#applet_p_63794 .Mt-12{margin-top:12px}#applet_p_63794 .Mb-12{margin-bottom:12px}#applet_p_63794 .Mstart-12{margin-left:12px}#applet_p_63794 .Mend-12{margin-right:12px}#applet_p_63794 .Mt-14{margin-top:14px}#applet_p_63794 .Mb-14{margin-bottom:14px}#applet_p_63794 .Mstart-14{margin-left:14px}#applet_p_63794 .Mend-14{margin-right:14px}#applet_p_63794 .Mt-16{margin-top:16px}#applet_p_63794 .Mb-16{margin-bottom:16px}#applet_p_63794 .Mstart-16{margin-left:16px}#applet_p_63794 .Mend-16{margin-right:16px}#applet_p_63794 .Mt-18{margin-top:18px}#applet_p_63794 .Mb-18{margin-bottom:18px}#applet_p_63794 .Mstart-18{margin-left:18px}#applet_p_63794 .Mend-18{margin-right:18px}#applet_p_63794 .Mt-20{margin-top:20px}#applet_p_63794 .Mb-20{margin-bottom:20px}#applet_p_63794 .Mstart-20{margin-left:20px}#applet_p_63794 .Mend-20{margin-right:20px}#applet_p_63794 .Mt-30{margin-top:30px}#applet_p_63794 .Mb-30{margin-bottom:30px}#applet_p_63794 .Mstart-30{margin-left:30px}#applet_p_63794 .Mend-30{margin-right:30px}#applet_p_63794 .Mt-40{margin-top:40px}#applet_p_63794 .Mb-40{margin-bottom:40px}#applet_p_63794 .Mstart-40{margin-left:40px}#applet_p_63794 .Mend-40{margin-right:40px}#applet_p_63794 .Mt-50{margin-top:50px}#applet_p_63794 .Mb-50{margin-bottom:50px}#applet_p_63794 .Mstart-50{margin-left:50px}#applet_p_63794 .Mend-50{margin-right:50px}#applet_p_63794 .Mt-60{margin-top:60px}#applet_p_63794 .Mb-60{margin-bottom:60px}#applet_p_63794 .Mstart-60{margin-left:60px}#applet_p_63794 .Mend-60{margin-right:60px}#applet_p_63794 .Mt-70{margin-top:70px}#applet_p_63794 .Mb-70{margin-bottom:70px}#applet_p_63794 .Mstart-70{margin-left:70px}#applet_p_63794 .Mend-70{margin-right:70px}#applet_p_63794 .Mt-neg-1{margin-top:-1px}#applet_p_63794 .Mb-neg-1{margin-bottom:-1px}#applet_p_63794 .Mstart-neg-1{margin-left:-1px}#applet_p_63794 .Mend-neg-1{margin-right:-1px}#applet_p_63794 .Mt-neg-4{margin-top:-4px}#applet_p_63794 .Mb-neg-4{margin-bottom:-4px}#applet_p_63794 .Mstart-neg-4{margin-left:-4px}#applet_p_63794 .Mend-neg-4{margin-right:-4px}#applet_p_63794 .Mt-neg-6{margin-top:-6px}#applet_p_63794 .Mb-neg-6{margin-bottom:-6px}#applet_p_63794 .Mstart-neg-6{margin-left:-6px}#applet_p_63794 .Mend-neg-6{margin-right:-6px}#applet_p_63794 .Mt-neg-8{margin-top:-8px}#applet_p_63794 .Mb-neg-8{margin-bottom:-8px}#applet_p_63794 .Mstart-neg-8{margin-left:-8px}#applet_p_63794 .Mend-neg-8{margin-right:-8px}#applet_p_63794 .Mt-neg-10{margin-top:-10px}#applet_p_63794 .Mb-neg-10{margin-bottom:-10px}#applet_p_63794 .Mstart-neg-10{margin-left:-10px}#applet_p_63794 .Mend-neg-10{margin-right:-10px}#applet_p_63794 .Mt-neg-12{margin-top:-12px}#applet_p_63794 .Mb-neg-12{margin-bottom:-12px}#applet_p_63794 .Mstart-neg-12{margin-left:-12px}#applet_p_63794 .Mend-neg-12{margin-right:-12px}#applet_p_63794 .Mt-neg-14{margin-top:-14px}#applet_p_63794 .Mb-neg-14{margin-bottom:-14px}#applet_p_63794 .Mstart-neg-14{margin-left:-14px}#applet_p_63794 .Mend-neg-14{margin-right:-14px}#applet_p_63794 .Mt-neg-16{margin-top:-16px}#applet_p_63794 .Mb-neg-16{margin-bottom:-16px}#applet_p_63794 .Mstart-neg-16{margin-left:-16px}#applet_p_63794 .Mend-neg-16{margin-right:-16px}#applet_p_63794 .Mt-neg-18{margin-top:-18px}#applet_p_63794 .Mb-neg-18{margin-bottom:-18px}#applet_p_63794 .Mstart-neg-18{margin-left:-18px}#applet_p_63794 .Mend-neg-18{margin-right:-18px}#applet_p_63794 .Mt-neg-20{margin-top:-20px}#applet_p_63794 .Mb-neg-20{margin-bottom:-20px}#applet_p_63794 .Mstart-neg-20{margin-left:-20px}#applet_p_63794 .Mend-neg-20{margin-right:-20px}#applet_p_63794 .Mstart-50\%{margin-left:50%}#applet_p_63794 .M-a{margin:auto}#applet_p_63794 .Mx-a{margin-right:auto;margin-left:auto}#applet_p_63794 .\!Mx-a{margin-right:auto!important;margin-left:auto!important}#applet_p_63794 .Mstart-a{margin-left:auto}#applet_p_63794 .Mend-a{margin-right:auto}#applet_p_63794 .Mt-0,#applet_p_63794 .My-0{margin-top:0}#applet_p_63794 .Mb-0,#applet_p_63794 .My-0{margin-bottom:0}#applet_p_63794 .Mstart-0,#applet_p_63794 .Mx-0{margin-left:0}#applet_p_63794 .Mend-0,#applet_p_63794 .Mx-0{margin-right:0}#applet_p_63794 .Miw-0{min-width:0}#applet_p_63794 .Miw-10{min-width:10%}#applet_p_63794 .Miw-15{min-width:15%}#applet_p_63794 .Miw-20{min-width:20%}#applet_p_63794 .Miw-25{min-width:25%}#applet_p_63794 .Miw-30{min-width:30%}#applet_p_63794 .Miw-35{min-width:35%}#applet_p_63794 .Miw-40{min-width:40%}#applet_p_63794 .Miw-45{min-width:45%}#applet_p_63794 .Miw-50{min-width:50%}#applet_p_63794 .Miw-60{min-width:60%}#applet_p_63794 .Miw-70{min-width:70%}#applet_p_63794 .Miw-80{min-width:80%}#applet_p_63794 .Miw-90{min-width:90%}#applet_p_63794 .Miw-100{min-width:100%}#applet_p_63794 .Maw-n{max-width:none}#applet_p_63794 .Maw-10{max-width:10%}#applet_p_63794 .Maw-15{max-width:15%}#applet_p_63794 .Maw-20{max-width:20%}#applet_p_63794 .Maw-25{max-width:25%}#applet_p_63794 .Maw-30{max-width:30%}#applet_p_63794 .Maw-35{max-width:35%}#applet_p_63794 .Maw-40{max-width:40%}#applet_p_63794 .Maw-45{max-width:45%}#applet_p_63794 .Maw-50{max-width:50%}#applet_p_63794 .Maw-60{max-width:60%}#applet_p_63794 .Maw-70{max-width:70%}#applet_p_63794 .Maw-80{max-width:80%}#applet_p_63794 .Maw-90{max-width:90%}#applet_p_63794 .Maw-99{max-width:99%}#applet_p_63794 .Maw-100{max-width:100%}#applet_p_63794 .Mih-0{min-height:0}#applet_p_63794 .Mih-50{min-height:50%;_height:50%}#applet_p_63794 .Mih-60{min-height:60%;_height:60%}#applet_p_63794 .Mih-100{min-height:100%;_height:100%}#applet_p_63794 .Mah-n{max-height:none}#applet_p_63794 .Mah-0{max-height:0}#applet_p_63794 .Mah-50{max-height:50%}#applet_p_63794 .Mah-60{max-height:60%}#applet_p_63794 .Mah-100{max-height:100%}#applet_p_63794 .O-0{outline:0}#applet_p_63794 .T-0{top:0}#applet_p_63794 .B-0{bottom:0}#applet_p_63794 .Start-0{left:0}#applet_p_63794 .End-0{right:0}#applet_p_63794 .T-10{top:10%}#applet_p_63794 .B-10{bottom:10%}#applet_p_63794 .Start-10{left:10%}#applet_p_63794 .End-10{right:10%}#applet_p_63794 .T-25{top:25%}#applet_p_63794 .B-25{bottom:25%}#applet_p_63794 .Start-25{left:25%}#applet_p_63794 .End-25{right:25%}#applet_p_63794 .T-50{top:50%}#applet_p_63794 .B-50{bottom:50%}#applet_p_63794 .Start-50{left:50%}#applet_p_63794 .End-50{right:50%}#applet_p_63794 .T-75{top:75%}#applet_p_63794 .B-75{bottom:75%}#applet_p_63794 .Start-75{left:75%}#applet_p_63794 .End-75{right:75%}#applet_p_63794 .T-100{top:100%}#applet_p_63794 .B-100{bottom:100%}#applet_p_63794 .Start-100{left:100%}#applet_p_63794 .End-100{right:100%}#applet_p_63794 .T-a{top:auto!important}#applet_p_63794 .B-a{bottom:auto!important}#applet_p_63794 .Start-a{left:auto!important}#applet_p_63794 .End-a{right:auto!important}#applet_p_63794 .Op-0{opacity:0;filter:alpha(opacity=0)}#applet_p_63794 .Op-33{opacity:.33;filter:alpha(opacity=33)}#applet_p_63794 .Op-50{opacity:.5;filter:alpha(opacity=50)}#applet_p_63794 .Op-66{opacity:.66;filter:alpha(opacity=66)}#applet_p_63794 .Op-100{opacity:1;filter:alpha(opacity=100)}#applet_p_63794 .Ov-h{overflow:hidden;zoom:1}#applet_p_63794 .Ov-v{overflow:visible}#applet_p_63794 .Ov-s{overflow:scroll}#applet_p_63794 .Ov-a{overflow:auto}#applet_p_63794 .Ovs-t{-webkit-overflow-scrolling:touch}#applet_p_63794 .Ovx-v{overflow-x:visible}#applet_p_63794 .Ovx-h{overflow-x:hidden}#applet_p_63794 .Ovx-s{overflow-x:scroll}#applet_p_63794 .Ovx-a{overflow-x:auto}#applet_p_63794 .Ovy-v{overflow-y:visible}#applet_p_63794 .Ovy-h{overflow-y:hidden}#applet_p_63794 .Ovy-s{overflow-y:scroll}#applet_p_63794 .Ovy-a{overflow-y:auto}#applet_p_63794 .P-0{padding:0}#applet_p_63794 .P-1{padding:1px}#applet_p_63794 .P-2{padding:2px}#applet_p_63794 .P-4{padding:4px}#applet_p_63794 .P-6{padding:6px}#applet_p_63794 .P-8{padding:8px}#applet_p_63794 .P-10{padding:10px}#applet_p_63794 .P-12{padding:12px}#applet_p_63794 .P-14{padding:14px}#applet_p_63794 .P-16{padding:16px}#applet_p_63794 .P-18{padding:18px}#applet_p_63794 .P-20{padding:20px}#applet_p_63794 .P-30{padding:30px}#applet_p_63794 .Px-1{padding-right:1px;padding-left:1px}#applet_p_63794 .Px-2{padding-right:2px;padding-left:2px}#applet_p_63794 .Px-4{padding-right:4px;padding-left:4px}#applet_p_63794 .Px-6{padding-right:6px;padding-left:6px}#applet_p_63794 .Px-8{padding-right:8px;padding-left:8px}#applet_p_63794 .Px-10{padding-right:10px;padding-left:10px}#applet_p_63794 .Px-12{padding-right:12px;padding-left:12px}#applet_p_63794 .Px-14{padding-right:14px;padding-left:14px}#applet_p_63794 .Px-16{padding-right:16px;padding-left:16px}#applet_p_63794 .Px-18{padding-right:18px;padding-left:18px}#applet_p_63794 .Px-20{padding-right:20px;padding-left:20px}#applet_p_63794 .Px-30{padding-right:30px;padding-left:30px}#applet_p_63794 .Py-1{padding-top:1px;padding-bottom:1px}#applet_p_63794 .Py-2{padding-top:2px;padding-bottom:2px}#applet_p_63794 .Py-4{padding-top:4px;padding-bottom:4px}#applet_p_63794 .Py-6{padding-top:6px;padding-bottom:6px}#applet_p_63794 .Py-8{padding-top:8px;padding-bottom:8px}#applet_p_63794 .Py-10{padding-top:10px;padding-bottom:10px}#applet_p_63794 .Py-12{padding-top:12px;padding-bottom:12px}#applet_p_63794 .Py-14{padding-top:14px;padding-bottom:14px}#applet_p_63794 .Py-16{padding-top:16px;padding-bottom:16px}#applet_p_63794 .Py-18{padding-top:18px;padding-bottom:18px}#applet_p_63794 .Py-20{padding-top:20px;padding-bottom:20px}#applet_p_63794 .Py-30{padding-top:30px;padding-bottom:30px}#applet_p_63794 .Pt-1{padding-top:1px}#applet_p_63794 .Pb-1{padding-bottom:1px}#applet_p_63794 .Pstart-1{padding-left:1px}#applet_p_63794 .Pend-1{padding-right:1px}#applet_p_63794 .Pt-2{padding-top:2px}#applet_p_63794 .Pb-2{padding-bottom:2px}#applet_p_63794 .Pstart-2{padding-left:2px}#applet_p_63794 .Pend-2{padding-right:2px}#applet_p_63794 .Pt-4{padding-top:4px}#applet_p_63794 .Pb-4{padding-bottom:4px}#applet_p_63794 .Pstart-4{padding-left:4px}#applet_p_63794 .Pend-4{padding-right:4px}#applet_p_63794 .Pt-6{padding-top:6px}#applet_p_63794 .Pb-6{padding-bottom:6px}#applet_p_63794 .Pstart-6{padding-left:6px}#applet_p_63794 .Pend-6{padding-right:6px}#applet_p_63794 .Pt-8{padding-top:8px}#applet_p_63794 .Pb-8{padding-bottom:8px}#applet_p_63794 .Pstart-8{padding-left:8px}#applet_p_63794 .Pend-8{padding-right:8px}#applet_p_63794 .Pt-10{padding-top:10px}#applet_p_63794 .Pb-10{padding-bottom:10px}#applet_p_63794 .Pstart-10{padding-left:10px}#applet_p_63794 .Pend-10{padding-right:10px}#applet_p_63794 .Pt-12{padding-top:12px}#applet_p_63794 .Pb-12{padding-bottom:12px}#applet_p_63794 .Pstart-12{padding-left:12px}#applet_p_63794 .Pend-12{padding-right:12px}#applet_p_63794 .Pt-14{padding-top:14px}#applet_p_63794 .Pb-14{padding-bottom:14px}#applet_p_63794 .Pstart-14{padding-left:14px}#applet_p_63794 .Pend-14{padding-right:14px}#applet_p_63794 .Pt-16{padding-top:16px}#applet_p_63794 .Pb-16{padding-bottom:16px}#applet_p_63794 .Pstart-16{padding-left:16px}#applet_p_63794 .Pend-16{padding-right:16px}#applet_p_63794 .Pt-18{padding-top:18px}#applet_p_63794 .Pb-18{padding-bottom:18px}#applet_p_63794 .Pstart-18{padding-left:18px}#applet_p_63794 .Pend-18{padding-right:18px}#applet_p_63794 .Pt-20{padding-top:20px}#applet_p_63794 .Pend-20{padding-right:20px}#applet_p_63794 .Pb-20{padding-bottom:20px}#applet_p_63794 .Pstart-20{padding-left:20px}#applet_p_63794 .Pt-30{padding-top:30px}#applet_p_63794 .Pend-30{padding-right:30px}#applet_p_63794 .Pb-30{padding-bottom:30px}#applet_p_63794 .Pstart-30{padding-left:30px}#applet_p_63794 .Pt-0,#applet_p_63794 .Py-0{padding-top:0}#applet_p_63794 .Pb-0,#applet_p_63794 .Py-0{padding-bottom:0}#applet_p_63794 .Pstart-0,#applet_p_63794 .Px-0{padding-left:0}#applet_p_63794 .Pend-0,#applet_p_63794 .Px-0{padding-right:0}#applet_p_63794 .Pe-n{pointer-events:none}#applet_p_63794 .Pe-a{pointer-events:auto}#applet_p_63794 .Pos-s{position:static}#applet_p_63794 .Pos-a{position:absolute}#applet_p_63794 .Pos-r{position:relative}#applet_p_63794 .Pos-f{position:fixed}#applet_p_63794 .Ws-n::-webkit-scrollbar{-webkit-appearance:none}#applet_p_63794 .Tbl-f{table-layout:fixed}#applet_p_63794 .Tbl-a{table-layout:auto}#applet_p_63794 .Ta-c{text-align:center}#applet_p_63794 .Ta-j{text-align:justify}#applet_p_63794 .Ta-start{text-align:left}#applet_p_63794 .Ta-end{text-align:right}#applet_p_63794 .Td-n{text-decoration:none!important}#applet_p_63794 .Td-u,#applet_p_63794 .Td-u\:h:hover{text-decoration:underline}#applet_p_63794 .Tr-a{-webkit-text-rendering:auto;-moz-text-rendering:auto;-ms-text-rendering:auto;-o-text-rendering:auto;text-rendering:auto}#applet_p_63794 .Tr-os{-webkit-text-rendering:optimizeSpeed;-moz-text-rendering:optimizeSpeed;-ms-text-rendering:optimizeSpeed;-o-text-rendering:optimizeSpeed;text-rendering:optimizeSpeed}#applet_p_63794 .Tr-ol{-webkit-text-rendering:optimizeLegibility;-moz-text-rendering:optimizeLegibility;-ms-text-rendering:optimizeLegibility;-o-text-rendering:optimizeLegibility;text-rendering:optimizeLegibility}#applet_p_63794 .Tr-gp{-webkit-text-rendering:geometricPrecision;-moz-text-rendering:geometricPrecision;-ms-text-rendering:geometricPrecision;-o-text-rendering:geometricPrecision;text-rendering:geometricPrecision}#applet_p_63794 .Tr-i{-webkit-text-rendering:inherit;-moz-text-rendering:inherit;-ms-text-rendering:inherit;-o-text-rendering:inherit;text-rendering:inherit}#applet_p_63794 .Tt-n{text-transform:none}#applet_p_63794 .Tt-u{text-transform:uppercase}#applet_p_63794 .Tt-c{text-transform:capitalize}#applet_p_63794 .Tt-l{text-transform:lowercase}#applet_p_63794 .Tsh{text-shadow:0 1px 1px #000}#applet_p_63794 .Tsh-n{text-shadow:none}#applet_p_63794 .Us-n{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}#applet_p_63794 .Va-sup{vertical-align:super}#applet_p_63794 .Va-t{vertical-align:top}#applet_p_63794 .Va-tt{vertical-align:text-top}#applet_p_63794 .Va-m{vertical-align:middle}#applet_p_63794 .Va-bl{vertical-align:baseline}#applet_p_63794 .Va-b{vertical-align:bottom}#applet_p_63794 .Va-tb{vertical-align:text-bottom}#applet_p_63794 .Va-sub{vertical-align:sub}#applet_p_63794 .V-v{visibility:visible}#applet_p_63794 .V-h{visibility:hidden}#applet_p_63794 .V-c{visibility:collapse}#applet_p_63794 .Whs-nw{white-space:nowrap}#applet_p_63794 .Whs-n{white-space:normal}#applet_p_63794 .W-a{width:auto}#applet_p_63794 .W-0{width:0}#applet_p_63794 .W-1{width:1%}#applet_p_63794 .W-10{width:10%;*width:9.6%}#applet_p_63794 .W-15{width:15%;*width:14.8%}#applet_p_63794 .W-20{width:20%;*width:19.5%}#applet_p_63794 .W-25{width:25%;*width:24.5%}#applet_p_63794 .W-30{width:30%;*width:29.6%}#applet_p_63794 .W-33{width:33.33%;*width:33%}#applet_p_63794 .W-35{width:35%;*width:34.9%}#applet_p_63794 .W-40{width:40%;*width:39.5%}#applet_p_63794 .W-45{width:45%;*width:44.9%}#applet_p_63794 .W-50{width:50%;*width:49.5%}#applet_p_63794 .W-55{width:55%;*width:54.8%}#applet_p_63794 .W-60{width:60%}#applet_p_63794 .W-66{width:66.66%}#applet_p_63794 .W-70{width:70%}#applet_p_63794 .W-75{width:75%}#applet_p_63794 .W-80{width:80%}#applet_p_63794 .W-90{width:90%}#applet_p_63794 .W-100{width:100%}#applet_p_63794 .Wpx-1{width:1px}#applet_p_63794 .Wpx-2{width:2px}#applet_p_63794 .Wpx-4{width:4px}#applet_p_63794 .Wpx-6{width:6px}#applet_p_63794 .Wpx-8{width:8px}#applet_p_63794 .Wpx-10{width:10px}#applet_p_63794 .Wpx-12{width:12px}#applet_p_63794 .Wpx-14{width:14px}#applet_p_63794 .Wpx-16{width:16px}#applet_p_63794 .Wpx-18{width:18px}#applet_p_63794 .Wpx-20{width:20px}#applet_p_63794 .Wpx-24{width:24px}#applet_p_63794 .Wpx-26{width:26px}#applet_p_63794 .Wpx-28{width:28px}#applet_p_63794 .Wpx-30{width:30px}#applet_p_63794 .Wpx-32{width:32px}#applet_p_63794 .Wob-ba{word-break:break-all}#applet_p_63794 .Wob-n{word-break:normal!important}#applet_p_63794 .Wow-bw{word-wrap:break-word}#applet_p_63794 .Wow-n{word-wrap:normal!important}#applet_p_63794 .Z-0{z-index:0}#applet_p_63794 .Z-1{z-index:1}#applet_p_63794 .Z-3{z-index:3}#applet_p_63794 .Z-5{z-index:5}#applet_p_63794 .Z-7{z-index:7}#applet_p_63794 .Z-10{z-index:10}#applet_p_63794 .Z-a{z-index:auto!important}#applet_p_63794 .Zoom-1{zoom:1}

#applet_p_63794 .StencilRoot input {
    font-size: 13px;    
}

#applet_p_63794 .StencilRoot button {
    font-size: 14px;
    line-height: normal;
}
</style>

    
    </head>
    <body class="my3columns  l-out Pos-r https fp fp-v2 rc1 fp-default mini-uh-on viewer-right ltr    stream-dense" dir="ltr">
        
        
        
                    <div id="darla-assets-top">
            <script type='text/javascript' src='/sy/rq/darla/2-9-4/js/g-r-min.js'></script>
            <script>
    var resourceTimingAssets = null;
    if (window.performance && window.performance.mark && window.performance.getEntriesByName) {
        resourceTimingAssets = {'darlaJsLoaded' : 'https://www.yahoo.com/sy/rq/darla/2-9-4/js/g-r-min.js'};
        window.performance.mark('darlaJsLoaded');
    }
</script>
            </div>
                <script type="text/javascript">
        var rapidPageConfig = {
            rapidEarlyConfig : {},
            rapidConfig: {"compr_type":"deflate","tracked_mods":[],"spaceid":2023538075,"ywa":{"project_id":"10001806365479","host":"y.analytics.yahoo.com"},"click_timeout":300,"track_right_click":true,"apv":true,"apv_time":0,"yql_host":"","test_id":"201","client_only":0,"pageview_on_init":true,"perf_navigationtime":2,"addmodules_timeout":500,"keys":{"_rid":"dp4utbtbbmtc2","mrkt":"us","pt":1,"ver":"megastrm","uh_vw":0,"colo":"slw9.fp.bf1.yahoo.com","navtype":"server","nob":1},"viewability":true},
            rapidSingleInstance: 0,
            ywaCF: {"mrkt":12,"pt":13,"test":14,"sec":18,"slk":19,"cpos":21,"pkgt":22,"ct":23,"bpos":24,"cat":25,"dcl":26,"f":40,"prfm1":41,"site":42,"prfm2":43,"prfm3":44,"prfm4":45,"pct":48,"ver":49,"ft":51,"sca":53,"elm":56,"elmt":57,"ad":58,"tsrc":69,"err":72,"bkpt":73,"bw":98,"bh":99,"olncust":100,"noct":101,"uh_vw":102,"rspns":107,"grpt":109,"itc":111,"enr":112,"tar":113,"sp":115,"pos":117,"cposx":118,"cw":119,"ch":120,"t1":121,"t2":122,"t3":123,"t4":124,"t5":125,"refcnt":135,"tar_qa":145,"navtype":146},
            ywaActionMap: {"swp":103,"click":12,"secvw":18,"hvr":115,"error":99},
            ywaOutcomeMap: {"fetch":30,"end":31,"dclent":101,"dclitm":102,"op":105,"cl":106,"nav":108,"svct":109,"unsvct":110,"rmsvct":117,"slct":121,"imprt":123,"lgn":125,"lgo":126,"flagitm":129,"unflagitm":130,"flatcat":131,"unflagcat":132,"slctfltr":133} 
        };
        if (rapidPageConfig.rapidEarlyConfig.keys) {
            rapidPageConfig.rapidEarlyConfig.keys.bw = document.body.offsetWidth;
            rapidPageConfig.rapidEarlyConfig.keys.bh = document.body.offsetHeight;
        }
        rapidPageConfig.rapidConfig.keys.bw = document.body.offsetWidth;
        rapidPageConfig.rapidConfig.keys.bh = document.body.offsetHeight;
        </script>
        
                            
                    
                    
                    
                    
                    
                    

    <div id="UH">
        <div id="UH-ColWrap">
            <div id="applet_p_30345894" class=" M-0 js-applet header Zoom-1  Mb-0 " data-applet-guid="p_30345894" data-applet-type="header" data-applet-params="_suid:30345894" data-i13n="auto:true;sec:hd" data-i13n-sec="hd" data-ylk="rspns:nav;t1:a1;t2:hd;itc:0;"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  <div id="mega-uh-wrapper" class="Bds(n) Bdc($headerBdr) Scrolling_Bdc($headerBdrScroll) has-scrolled_Bdc($headerBdrScroll) Bdbw(1px) ua-ie7_Bdbs(s)! ua-ie8_Bdbs(s)! Scrolling_Bxsh($headerShadow) has-scrolled_Bxsh($headerShadow) Bgc(#fff) T(0) Start(0) End(0) Pos(f) Z(3) Zoom">
    
    <div id="mega-topbar" class="Pos(r) H(22px) mini-header_Mt(-19px) Reader-open_Mt(-19px) Bg($topbarBgc) Bxsh($topbarShadow) Z(7)"   data-ylk="rspns:nav;t1:a1;t2:hd;t3:tb;sec:hd;itc:0;elm:itm;elmt:pty;">
    <ul class="Pos(r) Miw(1000px) Pstart(9px) Lh(1.7) Reader-open_Op(0) mini-header_Op(0)" role="navigation">
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://www.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:home;t5:home;cpos:1;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) Icon-Fp2 IconHome"></i>Home</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://mail.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:mail;t5:mail;cpos:2;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Mail</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://www.flickr.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:flickr;t5:flickr;cpos:3;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Flickr</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://www.tumblr.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:tumblr;t5:tumblr;cpos:4;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Tumblr</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://answers.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:answers;t5:answers;cpos:5;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Answers</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://groups.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:groups;t5:groups;cpos:6;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Groups</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://mobile.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:mobile;t5:mobile;cpos:7;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Mobile</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(10px) navigation-menu">
            <a href="https://everything.yahoo.com/" class="navigation-menu-title Pos(r) C(#fff) Pstart(12px) Pend(24px) Py(4px) Fz(13px) menu-open_C($topbarMenu) menu-open_Bgc(#fff) menu-open_Z(8) rapidnofollow"   data-ylk="rspns:op;t5:more;slk:more;itc:1;elmt:mu;cpos:8;" tabindex="1">More<i class="Pos(a) Pt(2px) Pstart(6px) Fw(b) Icon-Fp2 IconDownCaret"></i></a>
            <div class="Pos(a) Bgc(#fff) Bxsh($topbarMenuShadow) Z(7) V(h) Op(0) menu-open_V(v) menu-open_Op(1)">
                <ul class="Px(12px) Py(5px)">
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/politics/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:politics;t5:politics;cpos:9;" tabindex="1">Politics</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/celebrity/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:celebrity;t5:celebrity;cpos:10;" tabindex="1">Celebrity</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/movies/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:movies;t5:movies;cpos:11;" tabindex="1">Movies</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/music/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:music;t5:music;cpos:12;" tabindex="1">Music</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/tv/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:tv;t5:tv;cpos:13;" tabindex="1">TV</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/health/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:health;t5:health;cpos:14;" tabindex="1">Health</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/style/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:style;t5:style;cpos:15;" tabindex="1">Style</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/beauty/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:beauty;t5:beauty;cpos:16;" tabindex="1">Beauty</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/food/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:food;t5:food;cpos:17;" tabindex="1">Food</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/parenting/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:parenting;t5:parenting;cpos:18;" tabindex="1">Parenting</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/makers/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:makers;t5:makers;cpos:19;" tabindex="1">Makers</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/tech/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:tech;t5:tech;cpos:20;" tabindex="1">Tech</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://shopping.yahoo.com/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:shopping;t5:shopping;cpos:21;" tabindex="1">Shopping</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/travel/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:travel;t5:travel;cpos:22;" tabindex="1">Travel</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/autos/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:autos;t5:autos;cpos:23;" tabindex="1">Autos</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/realestate/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:homes;t5:homes;cpos:24;" tabindex="1">Real Estate</a>
                    </li>
                
                </ul>
            </div>
        </li>
    
    
    
    </ul>
</div>

    
    <div id="mega-uh" class="M(a) Maw(1256px) Miw(1000px) Pos(r) Pt(22px) Pb(24px) Reader-open_Pt(13px) mini-header_Pt(13px) Reader-open_Pb(14px) mini-header_Pb(14px) Z(6)">
        <h1 class="Fz(0) Pos(a) Pstart(15px) Reader-open_Pt(0px) mini-header_Pt(0px) search-mini-header_Pstart(10px) ua-ie7_Mt(4px)">
    <a id="uh-logo" href="https://www.yahoo.com/" class="D(ib) H(47px) W($bigLogoWidth) Bgi($logoImage) Bgr(nr) Bgz($bigLogoWidth) Reader-open_Bgz($mediumLogoWidth) mini-header_Bgz($mediumLogoWidth) search-mini-header_Bgz($searchLogoWidth) Bgz($smallLogoWidth)!--sm1024 Bgp($bigLogoPos) Reader-open_Bgp($mediumLogoPos) mini-header_Bgp($mediumLogoPos) search-mini-header_Bgp($searchLogoPos) Bgp($searchLogoPos)!--sm1024 ua-ie7_Bgi($logoImageIe) ua-ie7_Mstart(-170px) ua-ie8_Bgi($logoImageIe) "   data-ylk="rspns:nav;t1:a1;t2:hd;sec:hd;itc:0;slk:logo;elm:img;elmt:logo;" tabindex="1">
        <b class="Hidden">Yahoo</b>
    </a>
</h1>

        <ul class="Pos(a) End(15px) List(n) Mt(3px) Reader-open_Mt(1px) mini-header_Mt(1px)" role="menubar">
            
    <li class="Pos(r) Fl(start) Mend(26px)">
        <a id="uh-signin" class="Bdc($signInBtn) Bdrs(5px) Bds(s) Bdw(2px) Bgc($signInBtn):h C($signInBtn) C(#fff):h D(ib) Ell Fz(14px) Fw(b) Py(2px) Mt(5px) Ta(c) Td(n):h Miw(78px) H(18px)" href="https://login.yahoo.com/config/login?.src=fpctx&amp;.intl=us&amp;.lang=en-US&amp;.done=https%3A%2F%2Fwww.yahoo.com" data-action-outcome="lgn"  data-ylk="t3:usr;elm:btn;elmt:lgn;" tabindex="4"><div class="Miw(78px) Ta(c) Pos(a) T(0) Lh($userNavTextLh)">Sign in</div></a>
    </li>


            
    <li id="uh-notifications" class="uh-menu uh-notifications Fl(start) D(ib) Mend(13px) Cur(p) ua-ie7_D(n) ua-ie8_Pb(10px) ua-ie9_Pb(10px)" role="menuitem">
        <button class="uh-menu-btn Pos(r) Bd(0) Px(8px) Pt(1px) Lh(1.3)" aria-label="Notifications" aria-haspopup="true" aria-expanded="true" tabindex="5" title="Notifications">
            <i class="Lh($userNotifIconLh) C($mailBtn) Fz(28px) Grid-U Icon-Fp2 IconBell uh-notifications-icon"></i>
            <span class="Bgc($mailBadge) Bdrs(11px) C(#fff) D(n) display-count_D(b) End(-4px) Fz(11px) Fw(b) Pos(a) Py(4px) Ta(c) T(-7px) uh-notification-count W(22px)"></span>
        </button>
        <div class="uh-notification-panel uh-menu-panel Pos(a) End(0) T(45px) W(280px) Bgc($menuBgc) Bd Bdc($menuBdr) Bdrs(4px) Bxsh($menuShadow) Ovy(a) Trs($menuTransition) Cur(d) V(h) Op(0) Mah(0) uh-notifications:h_V(v) uh-notifications:h_Op(1) uh-notifications:h_Mah($panelMah) panel-open_V(v) panel-open_Op(1) panel-open_Mah($panelMah)" aria-label="Notifications" role="menu" tabindex="5">
            <div class="uh-notifications-loading Py(16px) Px(24px) Ta(c)">
                <div class="W(30px) H(30px) Mx(a) My(12px) Bgz(30px) Bgi($animatedSpinner) ua-ie8_D(n)"></div>
                <span class="C($mailTstamp) D(n) ua-ie8_D(b)">Loading Updates</span>
            </div>
        </div>
    </li>


            
    <li id="uh-mail" class="uh-menu uh-mail D(ib) Mstart(14px) ua-ie8_Pb(10px) ua-ie9_Pb(10px)" role="menuitem">
        <a id="uh-mail-link" href="https://mail.yahoo.com/" class="Pos(r) D(ib) Ta(s) Td(n):h uh-menu-btn" aria-label="Mail" aria-haspopup="true" role="button" aria-expanded="true" tabindex="6"><i class="Lh($userNavIconLh) W(28px) Mend(8px) C($mailBtn) Fz(30px) Grid-U Icon-Fp2 IconMail uh-mail-icon"></i><span class="Lh($userNavTextLh) D(ib) C($mailBtn) Fz(14px) Fw(b) Va(t)">Mail</span><span class="Bgc($mailBadge) Bdrs(11px) C(#fff) D(n) display-count_D(b) Start(16px) Fz(11px) Fw(b) Pos(a) Py(4px) Ta(c) T(-7px) uh-mail-count W(22px)"></span>
        </a>
        
            <div class="uh-mail-preview uh-menu-panel Pos(a) End(0) T(45px) W(280px) Bgc($menuBgc) Bd Bdc($menuBdr) Bdrs(4px) Bxsh($menuShadow) Trs($menuTransition) V(h) Op(0) Mah(0) uh-mail:h_V(v) uh-mail:h_Op(1) uh-mail:h_Mah($panelMah) panel-open_V(v) panel-open_Op(1) panel-open_Mah($panelMah)" role="menu" aria-label="Mail" tabindex="6">
                
                    <div class="Px(24px) Py(20px) Ta(c)">
                        <a class="C($menuLink) Fw(b) Td(n)" href="https://login.yahoo.com/config/login?.src=fpctx&amp;.intl=us&amp;.lang=en-US&amp;.done=https%3A%2F%2Fwww.yahoo.com" data-action-outcome="lgn"  data-ylk="t3:usr;elm:btn;elmt:lgn;">Sign in</a>  to view your mail
                    </div>
                
            </div>
        
    </li>


        </ul>
        <div id="uh-search" class="Pos(r) Mend(375px) Mstart(206px) search-mini-header_Mstart(112px) Mstart(120px)--sm1024 Maw(595px) H(40px) Reader-open_H(40px) mini-header_H(40px) search-mini-header_Maw(685px) Maw(745px)--sm1024 Va(t)">
    <form name="input" class="glowing glow" action="https://search.yahoo.com/search;_ylc=X3oDMTFiN25laTRvBF9TAzIwMjM1MzgwNzUEaXRjAzEEc2VjA3NyY2hfcWEEc2xrA3NyY2h3ZWI-" method="get" data-assist-action="https://search.yahoo.com/search;_ylc=X3oDMTFiaHBhMnJmBF9TAzIwMjM1MzgwNzUEaXRjAzEEc2VjA3NyY2hfcWEEc2xrA3NyY2hhc3Q-">
        <table class="Bdcl(s)" role="presentation">
            <tbody>
                <tr>
                    <td class="Pos(r) P(0) W(100%) H(40px) Reader-open_H(40px) mini-header_H(40px) D(tb) ua-safari_D(tbc) Tbl(f)">
                        <input id="uh-search-box" type="text" name="p" class="Pos(a) T(0) Start(0) Bd Bdc($searchBdrFoc):f Bdc($searchBdr) glow_Bdc($searchBtnGlow) Bgc(t) Bxsh(n) Fz(18px) M(0) O(0) Px(10px) W(100%) H(inh) Z(2) Bxz(bb) Bdrs(2px) ua-ie7_H(36px) ua-ie7_Lh(36px) ua-ie8_Lh(36px) ua-ie7_W(92%)" autofocus autocomplete="off" autocapitalize="off" aria-label="Search" tabindex="2">
                        
                        
                            <input type="hidden" data-fr="yfp-t-201" name="fr" value="yfp-t-201" />
                        
                    </td>
                    <td class="Miw(5px)"></td>
                    <td>
                        <button id="uh-search-button" type="submit" class="rapid-noclick-resp C(#fff) Fz(16px) H(40px) W(140px) Reader-open_H(40px) mini-header_H(40px) Px(26px) Py(0) Whs(nw) Bdrs(3px) Bdw(0) Bgc($searchBtn) glow_Bgc($searchBtnGlow) Bxsh($searchBtnShadow) glow_Bxsh($searchBtnShadowGlow) Lh(1)"   data-ylk="rspns:nav;t1:a1;t2:srch;sec:srch;slk:srchweb;elm:btn;elmt:srch;tar:search.yahoo.com;tar_uri:/search;itc:0;" tabindex="3">
                        <span class="search-default-label Fw(500) ">Search Web</span>
                        <span class="search-short-label Fw(500) D(n) ">Search</span>
                        </button>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
    
    <ul id="Skip-links" class="Pos(a)">
        <li><a href="#Navigation" class="W(0) O(h) D(ib) Whs(nw) Pos(a) Bg(#0078ff) C(#fff) Op(0) W(a):f Op(1):f P(5px):f">Skip to Navigation</a></li>
        <li><a href="#Main" class="W(0) O(h) D(ib) Whs(nw) Pos(a) Bg(#0078ff) C(#fff) Op(0) W(a):f Op(1):f P(5px):f">Skip to Main content</a></li>
        <li><a href="#Aside" class="W(0) O(h) D(ib) Whs(nw) Pos(a) Bg(#0078ff) C(#fff) Op(0) W(a):f Op(1):f P(5px):f">Skip to Right rail</a></li>
    </ul>
    
</div>

    <script>
!function(){var e,t,a,n,c=window,o=document,r=o.getElementById("uh-search-box"),g=function(c){try{a=c.keyCode,n=c.target?c.target:c.srcElement,e=r.value.length,n===r&&0===e&&a>=32&&40>=a?r.blur():"EMBED"===n.tagName||"OBJECT"===n.tagName||"INPUT"===n.tagName||"TEXTAREA"===n.tagName||c.altKey||c.metaKey||c.ctrlKey||!(32>a||a>40)||9==a||13==a||16==a||27==a||(e&&(r.setSelectionRange?r.setSelectionRange(e,e):r.createTextRange&&(t=r.createTextRange(),t.moveStart("character",e),t.select())),r.focus())}catch(c){}};r&&(c.addEventListener?c.addEventListener("keydown",g,!1):c.attachEvent&&o.attachEvent("onkeydown",g),setTimeout(function(){r.focus()},500))}();
</script>



    </div>
</div>

 </div> </div> </div>            <!-- App close -->
            </div>
        </div>
    </div><!-- Header -->
    <div id="Stencil">
    <div id="Reader" class="js-viewer-viewerwrapper">
        <div class="ReaderWrap">
            <div class="InnerWrap">
                <div id="applet_p_50000101" class=" App_v2  M-0 js-applet viewer Zoom-1  App-Chromeless " data-applet-guid="p_50000101" data-applet-type="viewer" data-applet-params="_suid:50000101" data-i13n="auto:true;sec:app-view" data-i13n-sec="app-view" data-applet-init="now"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  

<div id="hl-viewer" class="js-slider Hl-Viewer Ov-h desktop" role="document" aria-labelledby="modal-header" aria-live="polite" aria-hidden="true" tabindex="-1">
    
    
        <button id="closebtn" class="Viewer-Close-Btn Bdr-0 D-b Fw(b) js-close-content-viewer rapid-noclick-resp Z-7">
            <i class="Icon-Fp2 IconActionCross"></i>
            <b class="Td(n) Hidden">Close this content, you can also use the Escape key at anytime</b>
        </button>
        <div class="viewer-wrapper Ov-h mega-modal">
            <div class="">
                
                    <div class="content-container"></div>
                
                
                
                
                <div class="footer-ads">
                    <div id="hl-ad-FSRVY-0" class="D-ib">
                        <div id="hl-ad-FSRVY-0-0-iframe" class="hl-display-ads"></div>
                    </div>
                    <div id="hl-ad-FOOT9-0" class="D-ib">
                        <div id="hl-ad-FOOT9-0-0-iframe" class="hl-display-ads"></div>
                    </div>
                    <div id="hl-ad-FOOT-0" class="D-ib">
                        <div id="hl-ad-FOOT-0-0-iframe" class="hl-display-ads"></div>
                    </div>
                </div>
                <span id="viewer-end" tabindex="0"></span>
            </div>
        </div>
    

    


</div>

 </div> </div> </div>            <!-- App close -->
            </div>
            </div>
        </div>
        <div id="Overlay"></div>
        <div class="Reader-bg"></div>
    </div>
</div>
    
    <div id="Masterwrap" class="slider js-viewer-pagewrapper">
        <div class="page">
            <div id="Billboard-ad">
                                                    <div id="my-adsMAST" class="D-n">
                    <div id="my-adsMAST-iframe">
                        
                        <!-- MAST has no ad contents. Other Ad position will take over this ad --><noscript>
<!-- SpaceID=2023538075 loc=MAST noad --><!-- fac-gd2-noad --><!-- gd2-status-2 --><!--QYZ CMS_NONE_SELECTED,,;;MAST;2023538075;2-->
<img width="1" height="1" alt="" src="https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&amp;bs=(1367hb1jv(gid%24xPsxnTcyLjOW3e8W0kbDGBqqMjAwMVa7dYIQrmos,st%241455125890888424,si%244452051,sp%242023538075,pv%241,v%242.0))&amp;t=J_3-D_3&amp;al=(as%24125dodo5t,aid%24p4zX_GKLDz0-,cr%24-1,ct%2425,at%24H,eob%24gd1_match_id=-1:ypos=MAST)"></noscript>
                    </div>
                </div>
                                <div id="ad-north-base" class=""><div id="ad-north" class="BillboardAd"></div></div>
            </div>
            
            <div class="Col1" role="navigation" id="Navigation" tabindex="-1">
                <div class="Col1-stack" data-plugin="sticker" data-sticker-top="75px">
                                <div id="applet_p_50000195" class=" M-0 js-applet navrailv2 Zoom-1  Mb-20 " data-applet-guid="p_50000195" data-applet-type="navrailv2" data-applet-params="_suid:50000195" data-i13n="auto:true;sec:nav" data-i13n-sec="nav" data-ylk="rspns:nav;itc:0;t1:a2;t2:nav;elm:itm;"> <!-- App open -->
        <!-- src=mdbm type=popular mod=td-applet-navlinks-atomic ins=p_50000195 ts=1455125663.2786 --><div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  

    

    

    <ul class="navlinks-list D(tbc) W(134px) Mstart(6px)">
    
        <li class="navlink  Py(5px)">
            <a href=https://mail.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconMail C($MailNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Mail</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://news.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconNews C($NewsNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">News</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://sports.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconSports C($SportsNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Sports</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://finance.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconFinance C($FinanceNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Finance</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/autos/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconAutos C($AutosNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Autos</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/celebrity/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconCelebrity C($CelebrityNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Celebrity</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://shopping.yahoo.com class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconShopping C($ShoppingNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Shopping</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/movies/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconMovies C($MoviesNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Movies</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/politics/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconPolitics C($PoliticsNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Politics</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/beauty/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconBeauty C($BeautyNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Beauty</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh700 Py(5px)">
            <a href=https://www.yahoo.com/style/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconStyle C($StyleNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Style</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh700 Py(5px)">
            <a href=https://www.yahoo.com/tech/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconTech C($TechNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Tech</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh700 Py(5px)">
            <a href=https://www.yahoo.com/travel/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconTravel C($TravelNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Travel</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh770 Py(5px)">
            <a href=https://www.yahoo.com/food/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconFood C($FoodNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Food</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh770 Py(5px)">
            <a href=https://www.yahoo.com/health/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconHealth C($HealthNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Health</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh860 Py(5px)">
            <a href=https://www.yahoo.com/makers/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconMakers C($MakersNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Makers</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh860 Py(5px)">
            <a href=https://www.yahoo.com/parenting/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconParenting C($ParentingNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Parenting</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh900 Py(5px)">
            <a href=https://www.yahoo.com/tv/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconTV C($TVNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">TV</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh900 Py(5px)">
            <a href=https://www.yahoo.com/realestate/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconRealEstate C($RealEstateNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Real Estate</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh900 Py(5px)">
            <a href=https://everything.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconMoreOnYahoo C($MoreOnYahooNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">More on Yahoo</span>
            </a>
        </li>
    
        <li class="D(b)--maxh900 Py(5px) js-navlinks-seemore Pos(r) D(n) O(0)"  role="button" tabindex="0" aria-haspopup="true" aria-label="More Navlinks">
            <i class="Icon-Fp2 IconStreamShare Va(m) Pend(6px) Fz(22px) D(tbc) Miw(29px) Ta(c) C($MoreNav)"></i>
            <span class="Va(m) D(tbc) Fz(14px) Fw(b) V(h)--sm1024 C($navlink) Col1-stack:h_V(v) C($navlinkHover):h Cur(p)">More</span>

            <div class="js-navlinks-seemore-menu D(n) js-navlinks-menu-open_D(ib) Va(b) Pos(a) B(-14px) Start(60px) Pstart(60px)">
                <ul class="Bxsh($menuShadow) Bd($submenuBdr) Bdrs(3px) Bgc(#fff) Cur(d) Py(13px) Px(20px) Miw(140px)">
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh700">
                        <a href=https://www.yahoo.com/style/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconStyle C($StyleNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Style</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh700">
                        <a href=https://www.yahoo.com/tech/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconTech C($TechNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Tech</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh700">
                        <a href=https://www.yahoo.com/travel/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconTravel C($TravelNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Travel</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh770">
                        <a href=https://www.yahoo.com/food/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconFood C($FoodNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Food</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh770">
                        <a href=https://www.yahoo.com/health/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconHealth C($HealthNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Health</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh860">
                        <a href=https://www.yahoo.com/makers/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconMakers C($MakersNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Makers</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh860">
                        <a href=https://www.yahoo.com/parenting/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconParenting C($ParentingNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Parenting</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh900">
                        <a href=https://www.yahoo.com/tv/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconTV C($TVNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">TV</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh900">
                        <a href=https://www.yahoo.com/realestate/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconRealEstate C($RealEstateNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Real Estate</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh900">
                        <a href=https://everything.yahoo.com/ class="C($navlink) Td(n):h D(b) js-navlinks-lastitem">
                            <i class="Icon-Fp2 IconMoreOnYahoo C($MoreOnYahooNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">More on Yahoo</span>
                        </a>
                    </li>
                
                </ul>
            </div>
        </li>
    </ul>


 </div> </div> </div> <!-- yrid: n3S7VgAAAACyVAAAQvLEBQ.. | edgepipe: 0 | authed: 0 | ynet: 1 | ssl: 1 | spdy: 0 | ytee: 0 | bucket: 201 | colo: bf1 | device: desktop | bot: 0 | environment: prod | lang: en-US | partner: default | site: fp | region: US | intl: us | tz: America/Los_Angeles | mode: fallback -->
<!-- tdserver-my.fp.production.manhattan.bf1.yahoo.com (pprd3-node5034-lh1.manhattan.bf1.yahoo.com) | Served by ynodejs | Wed Feb 10 2016 17:34:23 GMT+0000 (UTC) -->            <!-- App close -->
            </div>                           <div id="my-adsTXTL" class="Mt-10 Mb-20 Pt-20 Pos-r ad-txtl D-n">
                    <div class="Mx-a" id="my-adsTXTL-iframe">
                        <noscript>
<!-- APT Vendor: Right Media, Format: Standard Graphical -->
<style>
	#fc_align{
		position: static !important;
		width: 120px !important;
		margin: auto !important;
	}
</style>
<!--QYZ 2275188051,4451009051,;;TXTL;2023538075;1-->
<img width="1" height="1" alt="" src="https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&amp;bs=(1367hb1jv(gid%24xPsxnTcyLjOW3e8W0kbDGBqqMjAwMVa7dYIQrmos,st%241455125890888424,si%244452051,sp%242023538075,pv%241,v%242.0))&amp;t=J_3-D_3&amp;al=(as%2413a3k053f,aid%24lmvY_GKLDz0-,bi%242275188051,agp%243476502551,cr%244451009051,ct%2425,at%24H,eob%24gd1_match_id=-1:ypos=TXTL)"></noscript>
                    </div>
                </div>
                </div>
            </div>
            <div class="Col2">
                <div id="Main" class="Col2-stack" role="content" tabindex="-1">
                    <div id="Banner">
                        
                    </div><!-- Banner -->
                                <div id="applet_p_50000173" class=" M-0 js-applet streamv2 Zoom-1  Mb-20 " data-applet-guid="p_50000173" data-applet-type="streamv2" data-applet-params="_suid:50000173" data-i13n="auto:true;sec:strm;useViewability:true" data-i13n-sec="strm" data-ylk="t1:a3;t2:strm;t3:ct;cat:default;rspns:nav;itc:0;" data-applet-init="now"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  

<div data-uuid-list="p_50000173">
    

    
    <ul class=" js-stream-tmpl-items js-stream-dense  js-stream-hover-enable My(0) Mb(0) Wow(bw)" id="Stream">
    
        
    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:New_Hampshire|YCT:001000661"  data-uuid="f56de522-62a4-3394-89c9-3bb8bf1c6b18" data-cauuid="f56de522-62a4-3394-89c9-3bb8bf1c6b18" data-type="article" data-cluster="f56de522-62a4-3394-89c9-3bb8bf1c6b18"  data-hosted="HOSTED">
    <div class="js-stream-item-wrap Pos(r) Pt(12px) Pb(17px)"><div class="js-stream-roundup js-stream-roundup-filmstrip Pos(r) Wow(bw) Cf">
    <div class="strm-headline Pos(r)">
        <a href="https://gma.yahoo.com/chris-christie-expected-formally-end-presidential-bid-early-161038064--abc-news-topstories.html#" class="Pos(r) D(ib) Z(1) js-stream-content-link js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:1;cposy:1;bpos:1;pos:1;ss_cid:f56de522-62a4-3394-89c9-3bb8bf1c6b18;refcnt:4;subsec:1800;imgt:ss;g:f56de522-62a4-3394-89c9-3bb8bf1c6b18;aid:id-3594243;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;elm:img;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;" >
        
        <img src="/sy/uu/api/res/1.2/8Sq1KSZg3U0iNiBg2TsR_w--/Zmk9c3RyaW07aD0yNzQ7cHlvZmY9MDtxPTk1O3c9NzAyO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/021016/images/smush/christie2_ipad_1455124927.jpg.cf.jpg" class="W(100%) rounded-img" alt="">
        
        <div class="Pos(a) T(0) Start(0) W(100%) H(100%) Ov(h)">
            <div class="Pos(a) B(0) M(15px) Mb(11px) Mend(120px) C(#fff) Z(1)">
                <h2 class="js-stream-item-title Fz(21px) Fw(b) Mb(3px) Lh(24px) Td(u):h">Christie to suspend presidential campaign</h2>
                <span class="stream-summary Fz(13px) C(#d9d9db) Mend(5px)">After canceling an event in South Carolina, the New Jersey governor is expected to halt his White House bid.</span><span class="Fw(b) D(ib) Va(b) Lh(18px) Td(u):h">Finished sixth in N.H. Â»</span></div>
            <div class="strm-img-gradient W(100%) H(100%) rounded-img"></div>
        </div>
        </a>
        <div class="Pos(a) End(13px) T(10px) W(30px) Mend(2px) Ta(end) Z(2)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions has-comments js-stream-dislike-disabled">
    
    <li class="ActionComments D(ib) O(n) Mstart(-1px) Pos(r) C(white)" data-cmntnum="1800"><span class="js-stream-comment-counter D(b) Fz(11px) Ta(c) Fw(b) Tsh($comment_shadow) js-stream-comment-hidden">1800</span><span class="js-stream-comment-counter-update D(b) Fz(11px) Ta(c) Fw(b) Tsh($comment_shadow)"></span><a href="javascript:void(0)" class="Pos(r) js-stream-comments-button-drawer Td(n) O(n) C(white) Px(14px) Pb(5px) Pt(25px)" role="button" tabindex="0"   data-ylk="cpos:1;cposy:1;bpos:1;pos:1;ss_cid:f56de522-62a4-3394-89c9-3bb8bf1c6b18;refcnt:4;subsec:1800;imgt:ss;g:f56de522-62a4-3394-89c9-3bb8bf1c6b18;aid:id-3594243;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;slk:cmmt;itc:1;"><b aria-live="polite" class="js-stream-comment-label ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) Lh(14px) Py(7px) End(-25px) C($signin_blue) js-stream-comments-button>Start(50%) js-stream-comments-button:h>Start(a)">Comments</b><i class="Icon-Fp2 IconComments Fz(21px) Tsh($comment_shadow) ActionComments:h_C($signin_blue)"></i></a>
    </li>
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C(white) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) Tsh($comment_shadow)"></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C(white) P(14px)" role="button" tabindex="0"   data-ylk="cpos:1;cposy:1;bpos:1;pos:1;ss_cid:f56de522-62a4-3394-89c9-3bb8bf1c6b18;refcnt:4;subsec:1800;imgt:ss;g:f56de522-62a4-3394-89c9-3bb8bf1c6b18;aid:id-3594243;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) Tsh($comment_shadow)"></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C(white) Tsh($comment_shadow) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
    </div>
    <ul class="P(0) Mt(7px) Mstart(-2px) Mend(7px) Fz(12px) Whs(nw) Lts(-.31em)">
        
        
        <li class="Pos(r) Va(t) D(ib) Bxz(bb) Whs(n) W(25%) Ov(h)">
            <a href="https://www.yahoo.com/politics/hillarys-millennial-woman-problem-010617452.html" data-uuid="23006958-8539-38b2-9e4a-548943c96b0e" class="js-stream-content-link js-stream-item-title js-content-title js-content-viewer rapid-noclick-resp rapidnofollow O(n):f C($m_blue):f Td(n) Fz(13px) Fw(b) Lts(n) Whs(n) D(b) Px(2px)"    data-ylk="cpos:1;cposy:2;bpos:1;pos:2;ss_cid:f56de522-62a4-3394-89c9-3bb8bf1c6b18;refcnt:4;subsec:1800;imgt:ss;g:23006958-8539-38b2-9e4a-548943c96b0e;aid:id-3594205;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;elm:rhdln;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" >
                <img src="/sy/uu/api/res/1.2/fJHwp.m3Cc_sLjIc.y362g--/Zmk9c3RyaW07aD0xNjA7cHlvZmY9MDtxPTgwO3c9MzQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/021016/images/smush/clinton_ipad_1455115976.jpg.cf.jpg" class="W(100%) rounded-img" alt="">
                <h3 class="D(b) Mt(5px) Mend(10px) LineClamp(2,30px) Fz(12px) Fw(b) C($link) js-stream-content-link:h>C($m_blue) js-stream-content-link:f_C($m_blue)">Has feminism moved beyond Hillary Clinton?</h3>
            </a>
        </li>
        
        
        
        <li class="Pos(r) Va(t) D(ib) Bxz(bb) Whs(n) W(25%) Ov(h)">
            <a href="https://www.yahoo.com/politics/johnny-depp-trump-funny-or-die-movie-video-160120350.html" data-uuid="6de4eedd-3ee0-334f-85a5-0f36da1b3d60" class="js-stream-content-link js-stream-item-title js-content-title js-content-viewer rapid-noclick-resp rapidnofollow O(n):f C($m_blue):f Td(n) Fz(13px) Fw(b) Lts(n) Whs(n) D(b) Px(2px)"    data-ylk="cpos:1;cposy:3;bpos:1;pos:3;ss_cid:f56de522-62a4-3394-89c9-3bb8bf1c6b18;refcnt:4;subsec:1800;imgt:ss;g:6de4eedd-3ee0-334f-85a5-0f36da1b3d60;aid:id-3594255;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;elm:rhdln;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" >
                <img src="/sy/uu/api/res/1.2/P5FpRCVao4vpB9z4AUM9Aw--/Zmk9c3RyaW07aD0xNjA7cHlvZmY9MDtxPTgwO3c9MzQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/021016/images/smush/fod_ipad_1455122883.jpg.cf.jpg" class="W(100%) rounded-img" alt="">
                <h3 class="D(b) Mt(5px) Mend(10px) LineClamp(2,30px) Fz(12px) Fw(b) C($link) js-stream-content-link:h>C($m_blue) js-stream-content-link:f_C($m_blue)">Johnny Depp plays Trump in spoof biopic</h3>
            </a>
        </li>
        
        
        
        <li class="Pos(r) Va(t) D(ib) Bxz(bb) Whs(n) W(25%) Ov(h)">
            <a href="http://news.yahoo.com/valentine-s-day--tweethearts---america-s-longest-married-couple-to-share-insights-on-love-144214702.html" data-uuid="eab1bdbf-29db-3bfe-8ca1-1b7e1ac63382" class="js-stream-content-link js-stream-item-title js-content-title js-content-viewer rapid-noclick-resp rapidnofollow O(n):f C($m_blue):f Td(n) Fz(13px) Fw(b) Lts(n) Whs(n) D(b) Px(2px)"    data-ylk="cpos:1;cposy:4;bpos:1;pos:4;ss_cid:f56de522-62a4-3394-89c9-3bb8bf1c6b18;refcnt:4;subsec:1800;imgt:ss;g:eab1bdbf-29db-3bfe-8ca1-1b7e1ac63382;aid:id-3594246;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;elm:rhdln;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" >
                <img src="/sy/uu/api/res/1.2/Qq_mUGbCmu.y9jB9ZDr38w--/Zmk9c3RyaW07aD0xNjA7cHlvZmY9MDtxPTgwO3c9MzQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/021016/images/smush/couple_ipad_1455121721.jpg.cf.jpg" class="W(100%) rounded-img" alt="">
                <h3 class="D(b) Mt(5px) Mend(10px) LineClamp(2,30px) Fz(12px) Fw(b) C($link) js-stream-content-link:h>C($m_blue) js-stream-content-link:f_C($m_blue)">Longest-married couple tell their secrets</h3>
            </a>
        </li>
        
        
        
        <li class="Pos(r) Va(t) D(ib) Bxz(bb) Whs(n) W(25%) Ov(h)">
            <a href="http://sports.yahoo.com/blogs/ncaaf-dr-saturday/tennessee-faces-sexual-assault-lawsuit-involving-football-players-232613263.html" data-uuid="666d713c-4222-3c08-bb0d-7e4278ae7f5c" class="js-stream-content-link js-stream-item-title js-content-title js-content-viewer rapid-noclick-resp rapidnofollow O(n):f C($m_blue):f Td(n) Fz(13px) Fw(b) Lts(n) Whs(n) D(b) Px(2px)"    data-ylk="cpos:1;cposy:5;bpos:1;pos:5;ss_cid:f56de522-62a4-3394-89c9-3bb8bf1c6b18;refcnt:4;subsec:1800;imgt:ss;g:666d713c-4222-3c08-bb0d-7e4278ae7f5c;aid:id-3594187;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;elm:rhdln;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" >
                <img src="/sy/uu/api/res/1.2/M6drVHcAbsYhk6all.cWjQ--/Zmk9c3RyaW07aD0xNjA7cHlvZmY9MDtxPTgwO3c9MzQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/021016/images/smush/tenn_fp_ipad_1455108407.jpg.cf.jpg" class="W(100%) rounded-img" alt="">
                <h3 class="D(b) Mt(5px) Mend(10px) LineClamp(2,30px) Fz(12px) Fw(b) C($link) js-stream-content-link:h>C($m_blue) js-stream-content-link:f_C($m_blue)">Tennessee facing sexual assault lawsuit</h3>
            </a>
        </li>
        
        
    </ul>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:North_Korea|YCT:001000705|YCT:001000661" data-offnet="1" data-uuid="326eba9a-9987-3915-9e3f-c3fd2ec16a18" data-type="article" data-cluster="b2ce10f4-c68c-46f4-ba5b-6b22f06bbc5a"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="strm-default-clusters Pos(r) Wow(bw) Cf Pend(30px) ">

    
    <div class="strm-left Fl(start) Mend(18px) Pos(r)"><a href="http://www.businessinsider.com/r-north-korea-executes-army-chief-of-staff-south-korean-media-2016-2" class="streamImage Pos(r) D(ib) js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:2;cposy:6;bpos:1;pos:1;ss_cid:b2ce10f4-c68c-46f4-ba5b-6b22f06bbc5a;refcnt:2;imgt:ss;g:326eba9a-9987-3915-9e3f-c3fd2ec16a18;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:World;elm:img;elmt:ct;r:4000026310S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/ourxJPqPfnPxItv9khjMMw--/Zmk9c3RyaW07aD0zODA7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/41ae73b9a5478d46935610223c70c1f1" height="190" width="190" class="W(100%) BackgroundPic rounded-img" alt="">

</a></div>
    
    <div class="strm-right Pos(r) Mstart(29%)"><div class="strm-headline Pend(14px)">
            <div class="strm-headline-label js-content-label C(c_world) Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="world">World</div>
            <h3 class="Mb(4px) Mb(0px)--md1160 LineClamp(2,42px) Lh(21px)">
                <a href="http://www.businessinsider.com/r-north-korea-executes-army-chief-of-staff-south-korean-media-2016-2" class="O(n):f C($m_blue):f D(b) Pos(r) js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:2;cposy:6;bpos:1;pos:1;ss_cid:b2ce10f4-c68c-46f4-ba5b-6b22f06bbc5a;refcnt:2;imgt:ss;g:326eba9a-9987-3915-9e3f-c3fd2ec16a18;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:World;elm:hdln;elmt:ct;r:4000026310S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><span>North Korea has reportedly executed its army chief of staff</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,34px) Lh(17px) C($gray) Bfv(h) D(n)--md1160">SEOUL (Reuters) - North Korea has executed its army chief of staff, Ri Yong Gil, South Korea&#39;s Yonhap news agency reported on Wednesday, which, if true, would be the latest in a series of executions, purges and disappearances under its young leader. The news comes amid heightened tension surrounding isolated North Korea after its Sunday launch of a long-range rocket, which came about a month after it drew international condemnation for conducting its fourth nuclear test. A source familiar with North Korean affairs also told Reuters that Ri had been executed. The source declined to be identified, given the sensitivity of the matter. Ri, who was chief of the Korean People&#39;s Army (KPA) General Staff,</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Tt(c) Mt(2px)">Business Insider</span>
            </div>
        </div>
        
        <div class="Mt(8px) Mt(7px)--md1160 Ov(h)">
            
            
                <a href="http://news.yahoo.com/north-korea-turns-old-workhorse-rocket-repeat-past-090158388.html" data-uuid="2d5595d1-c688-355b-bf26-203e91d6ab20" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Mb(6px)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb) Pend(13px)"    data-ylk="cpos:2;cposy:7;bpos:1;pos:2;ss_cid:b2ce10f4-c68c-46f4-ba5b-6b22f06bbc5a;refcnt:2;imgt:ss;g:2d5595d1-c688-355b-bf26-203e91d6ab20;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:World;elm:rhdln;elmt:ct;r:4000026310S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/ki4RYBHb3ZcKBDVgrIrIzQ--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en_us/News/Reuters/2016-02-09T114112Z_2_LYNXNPEC180DG_RTROPTP_2_NORTHKOREA-SATELLITE.JPG.cf.jpg" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160" alt=""><div class="D(ib) W(100%)"><span class="Mend(7px) D(b) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">North Korea turns to &#39;old workhorse&#39; rocket to repeat past success</span><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Reuters</span></div></a>
            
            
            
                <a href="http://news.yahoo.com/south-korea-suspends-operations-at-joint-factory-144825916.html" data-uuid="728f060e-ccea-3f92-b514-85145240dd4f" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb)"    data-ylk="cpos:2;cposy:8;bpos:1;pos:3;ss_cid:b2ce10f4-c68c-46f4-ba5b-6b22f06bbc5a;refcnt:2;imgt:ss;g:728f060e-ccea-3f92-b514-85145240dd4f;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:World;elm:rhdln;elmt:ct;r:4000026310S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/sC0xGL1YUZKXv_7KystG9Q--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en/homerun/feed_manager_auto_publish_494/8da44292668b50199c9ed260e5f025af" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160" alt=""><div class="D(ib) W(100%)"><span class="Mend(7px) D(b) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">South Korea suspends operations at joint factory park with North Korea</span><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Yahoo News Digest</span></div></a>
            
            
        </div>
        
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:2;cposy:6;bpos:1;pos:1;ss_cid:b2ce10f4-c68c-46f4-ba5b-6b22f06bbc5a;refcnt:2;imgt:ss;g:326eba9a-9987-3915-9e3f-c3fd2ec16a18;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:World;r:4000026310S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    
<li class="js-stream-ad-noview  js-stream-ad  Wow(bw) Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)" data-beacon="https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&amp;es=51lgVlEGIS8wp_oXPaOmlXnzqZUI.wb94sB_AQ4hbZndA7by35Q5Vlz0E5zDbWhDiw6i7ZXYHe318UW49PYAbEZXtrq6OAgTzPSQ2GL0LMCRRAEUeOoWhf1.TBoZOumNdKXZFWyAF_DAAx7aLhueD2kEeLCB1QvN8LTV29OgXr79WoldWJnFopojvSsBUWRMn7PYMu3xSssHD6enRH6Ko7P2Q3.f3XLu_tJBz76_ZY6kGjK953rqmOAr4iuI0H5Z.sD_LSEvWEIvb1Kj1S6uOSuAIvY8yZNuSChKcKVA7OkYpnMj9L0pPBLMBBuxPwaVq6eByhBGta.KTJE2qpOmuiWiCp9Z6fI_t0EevTyoaIp8Q_kbs6oFvsQ78BeEt01Hwwe5IH.u0w--&amp;ap=3" data-ad-feedback-beacon="https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&amp;bs=(15ott6rjb(gid$0de98d92-d01d-11e5-9b07-008cfacc4628-7f1d99642700,st$1455125891097000,li$0,cr$31710498501,dmn$startupf5.net,srv$3,exp$1455133091097000,ct$27,v$1.0,adv$1167625,pbid$1,seid$4250754))&amp;r=1455125891097&amp;al=$(AD_FEEDBACK)" data-uuid="31710498501">
    <div class="js-stream-item-wrap Py(15px) Pos(r)">
        <div class="Pos(r) Wow(bw) Cf Pend(30px)">
            
            <div class="strm-left Fl(start) Pos(r) Mend(18px)">
                <a href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=M5mibOkGIS.hAkXC8jJZ7StRe9sAXRArdi5nNiu9XkV_pBRxmugojQQlac1AZh8U_smB2u4BAb0tpU30odnq7op3PzZmc1YDHY1r5fA6x4L.FaQ63mHVhx5tDIb.MWnTqkcuDP664GJthYYHbdwYx9ennAykz6nYJbsN6t57LdMOYBUS1ZW6CciYEf2aGX7J5SB816ZGZ_VYI1Sp.j22zl80l8gGQv4qhXxLiorl2P_O90MfbURsqKuUS.B7lrekJXWRFwootcEWP2k4hzvpjvxcsE3atP1V3h8uJbPeK83HRif4HM7ebCxA6wfmmCgYZaOfSoWK6TvZVZm1VMr8xD29vWF7Er37Lh49kCV.8h820POSEb2p13AJnEQNOqXaX2uWVBgT4iBQmJe4OaLxlaciVNF53fjHsJn3055d.8Bx2iDbqopYksQ7nKzkc8JCwm0RFJy0j6INP7aIC.HPRt6N8BY.b1ANfmKlR9ZFgalo10Erbmuh_cxGRy2Cf6Xxrwo.ig--%26lp=http%3A%2F%2Ftrack.fbytemedia.com%2F0f0868f1-4986-4ec1-be9a-d18abdda27ef%3Fad%3D1" class="streamImage Pos(r) D(ib) W(100%) ua-ie8_D(i)" target="_blank"   data-ylk="cpos:3;cposy:9;bpos:1;pos:1;ad:1;elmt:ct;g:31710498501;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:img;r:false;ccode:mega_global_ranking_hlv2_up_based;" >
                    <img src="/sy/uu/api/res/1.2/ZSVHW29l5RMiZGNnhIqfRQ--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1454992791721-5955.jpg.cf.jpg" height="107" width="190" class="fea-ad H(100%) W(100%) BackgroundPic rounded-img" alt="">
                </a>
            </div>
            
            <div class="strm-right Pos(r) Mstart(29%)">
                
                <div class="Fw(b) Lh(17px) Mb(4px)">
                    <a class="Mend(4px) Fz(13px) D(ib) Va(m) Td(n) C($sponsored) Tt(c)" href="http://help.yahoo.com/kb/index?page=content&amp;y=PROD_FRONT&amp;locale=en_US&amp;id=SLN14553" target="_blank"   data-ylk="cpos:3;cposy:9;bpos:1;pos:1;ad:1;elmt:sp;g:31710498501;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;">Sponsored</a>
                    <a class="D(ib) Va(m) Td(n) adlink C($sponsored)" href="https://info.yahoo.com/privacy/us/yahoo/relevantads.html" target="_blank"   data-ylk="cpos:3;cposy:9;bpos:1;pos:1;ad:1;elmt:info;g:31710498501;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;"><i class="Icon-Fp2 IconMoneyball Fz(13px) D(ib)"></i></a>
                </div>
                
                <h3 class="Mb(4px) Lh(21px)">
                    <a href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=M5mibOkGIS.hAkXC8jJZ7StRe9sAXRArdi5nNiu9XkV_pBRxmugojQQlac1AZh8U_smB2u4BAb0tpU30odnq7op3PzZmc1YDHY1r5fA6x4L.FaQ63mHVhx5tDIb.MWnTqkcuDP664GJthYYHbdwYx9ennAykz6nYJbsN6t57LdMOYBUS1ZW6CciYEf2aGX7J5SB816ZGZ_VYI1Sp.j22zl80l8gGQv4qhXxLiorl2P_O90MfbURsqKuUS.B7lrekJXWRFwootcEWP2k4hzvpjvxcsE3atP1V3h8uJbPeK83HRif4HM7ebCxA6wfmmCgYZaOfSoWK6TvZVZm1VMr8xD29vWF7Er37Lh49kCV.8h820POSEb2p13AJnEQNOqXaX2uWVBgT4iBQmJe4OaLxlaciVNF53fjHsJn3055d.8Bx2iDbqopYksQ7nKzkc8JCwm0RFJy0j6INP7aIC.HPRt6N8BY.b1ANfmKlR9ZFgalo10Erbmuh_cxGRy2Cf6Xxrwo.ig--%26lp=http%3A%2F%2Ftrack.fbytemedia.com%2F0f0868f1-4986-4ec1-be9a-d18abdda27ef%3Fad%3D1" target="_blank"   data-ylk="cpos:3;cposy:9;bpos:1;pos:1;ad:1;elmt:ad;g:31710498501;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:hdln;r:false;ccode:mega_global_ranking_hlv2_up_based;" class="Pos(r) js-stream-content-link js-stream-item-title js-content-title Td(n) Fz(17px) Fw(b) C($link) C($m_blue):h O(n):f C($m_blue):f" >
                        <span>N'achetez AUCUNE action avant d'avoir vu ceci !</span>
                        <u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u>
                    </a>
                </h3>
                <div>
                    <p class="Pos(r) stream-summary LineClamp(2,36px) Lh(17px) C($gray) Bfv(h)">Ce nouvel algorithme de trading vient d'Ãªtre rÃ©vÃ©lÃ© au public et est en train de secouer toute la France.</p>
                    <a class="Fz(11px) D(ib) Td(n) C(gray_dark) Tt(c) Mt(4px)" href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=M5mibOkGIS.hAkXC8jJZ7StRe9sAXRArdi5nNiu9XkV_pBRxmugojQQlac1AZh8U_smB2u4BAb0tpU30odnq7op3PzZmc1YDHY1r5fA6x4L.FaQ63mHVhx5tDIb.MWnTqkcuDP664GJthYYHbdwYx9ennAykz6nYJbsN6t57LdMOYBUS1ZW6CciYEf2aGX7J5SB816ZGZ_VYI1Sp.j22zl80l8gGQv4qhXxLiorl2P_O90MfbURsqKuUS.B7lrekJXWRFwootcEWP2k4hzvpjvxcsE3atP1V3h8uJbPeK83HRif4HM7ebCxA6wfmmCgYZaOfSoWK6TvZVZm1VMr8xD29vWF7Er37Lh49kCV.8h820POSEb2p13AJnEQNOqXaX2uWVBgT4iBQmJe4OaLxlaciVNF53fjHsJn3055d.8Bx2iDbqopYksQ7nKzkc8JCwm0RFJy0j6INP7aIC.HPRt6N8BY.b1ANfmKlR9ZFgalo10Erbmuh_cxGRy2Cf6Xxrwo.ig--%26lp=http%3A%2F%2Ftrack.fbytemedia.com%2F0f0868f1-4986-4ec1-be9a-d18abdda27ef%3Fad%3D1" target="_blank"   data-ylk="cpos:3;cposy:9;bpos:1;pos:1;ad:1;elmt:ad;g:31710498501;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;">Preditrend</a>
                    
                </div>
            </div>
            <div class="Pos(a) End(-7px) T(0) W(30px) Mend(2px) Ta(end)">
    <ul class="js-stream-side-buttons js-stream-actions">
         <li class="ActionDislike Lh(10px)">
            <a href="javascript:void(0)" class="js-stream-dislike-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(10px)" role="button"   data-ylk="cpos:3;cposy:9;bpos:1;pos:1;ad:1;elmt:op;g:31710498501;ct:1;pkgt:sponsored_img;grpt:singlestory;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;itc:1;rspns:op;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) T(-10px) End(-38px)! Py(5px) js-stream-dislike-button>Start(50%) js-stream-dislike-button:h>Start(a) C(#000)">Dislike</b><i class="Icon-Fp2 IconActionCross Fz(16px) C(#000):h js-stream-ad_D(n) js-stream-ad:h_D(i)"></i></a>
        </li>
    </ul>
</div>

        </div>
    </div>
</li>


    <li class="js-stream-content Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="YCT:001000780|YCT:001000667|YCT:001000267" data-offnet="1" data-uuid="dced9374-4418-3536-8b30-77abbb43ba0e" data-type="article"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="Pos(r) Wow(bw) Cf Pend(30px)">

    
    <div class="strm-left Fl(start) Pos(r) Pend(18px)"><div class="Pos(r)"><a href="https://www.washingtonpost.com/local/public-safety/he-always-had-six-or-seven-kids-around-its-a-little-strange/2016/02/09/546489da-cf4f-11e5-88cd-753e80cd29ad_story.html" class="Pos(r) D(ib) streamImage js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:4;cposy:10;bpos:1;pos:1;imgt:ss;g:dced9374-4418-3536-8b30-77abbb43ba0e;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:U.S.;elm:img;elmt:ct;r:4000024680S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/wUt3klA4L_ozB..2gv4vSA--/Zmk9c3RyaW07aD0xMDc7cHlvZmY9MDtxPTk1O3c9MTkwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/65953ebeb44bfd287197053f13c0f0b0" height="107" width="190" class="W(100%) BackgroundPic rounded-img" alt="">

</a>
        </div>
    </div>
    
    <div class="strm-right Fl(start) W(66%)"><div class="strm-headline">
            <div class="js-content-label C(c_us) Fw(b) Mb(6px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="us">U.S.</div>
            <h3 class="Mb(4px) LineClamp(3,57px) Lh(19px)">
                <a href="https://www.washingtonpost.com/local/public-safety/he-always-had-six-or-seven-kids-around-its-a-little-strange/2016/02/09/546489da-cf4f-11e5-88cd-753e80cd29ad_story.html" class="Pos(r) O(n):f C($m_blue):f js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:4;cposy:10;bpos:1;pos:1;imgt:ss;g:dced9374-4418-3536-8b30-77abbb43ba0e;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:U.S.;elm:hdln;elmt:ct;r:4000024680S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><span>âHe always had six or seven kids around. Itâs a little strange.â</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,33px) Lh(17px) C($gray) Bfv(h)">Deonte Carraway was considered trustworthy by some - a man who did a good job keeping children entertained at the local recreation center and taking them to church on Sundays. To others, he seemed unusual- sometimes showing up as a volunteer at the elementary school near his home in pajamas and slippers. Now, shock, anger and confusion have consumed much of the Prince Georgeâs County city of about 6,000 residents after community members learned that Carraway was arrested for allegedly producing 40 pornographic videos featuring at least 10 youngsters. Parents are demanding to know how Carraway apparently was left alone with students long enough at Judge Sylvania W. Woods Elementary School to produce the videos on school grounds and during the middle of the school day, as police have stated.</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Mt(5px) Tt(c)">Washington Post</span>
            </div>
        </div>
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mstart(12px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:4;cposy:10;bpos:1;pos:1;imgt:ss;g:dced9374-4418-3536-8b30-77abbb43ba0e;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:U.S.;r:4000024680S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:NASA|YCT:001000798" data-offnet="1" data-uuid="a59894ae-0a10-3dd0-afd7-61d80c081d77" data-type="article" data-cluster="c1c4e431-5853-4ad7-91eb-b1f619fe4d83"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="strm-default-clusters Pos(r) Wow(bw) Cf Pend(30px) ">

    
    <div class="strm-left Fl(start) Mend(18px) Pos(r)"><a href="http://www.nytimes.com/2016/02/10/world/asia/that-wasnt-a-meteorite-that-killed-a-man-in-india-nasa-says.html" class="streamImage Pos(r) D(ib) js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:5;cposy:11;bpos:1;pos:1;ss_cid:c1c4e431-5853-4ad7-91eb-b1f619fe4d83;refcnt:2;imgt:ss;g:a59894ae-0a10-3dd0-afd7-61d80c081d77;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:World;elm:img;elmt:ct;r:4000021900S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/.tBz1VYUMFwdR0l7W4wZjA--/Zmk9c3RyaW07aD0zODA7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/34c765a46f84497fc80a284db529cd2b" height="190" width="190" class="W(100%) BackgroundPic rounded-img" alt="">

</a></div>
    
    <div class="strm-right Pos(r) Mstart(29%)"><div class="strm-headline Pend(14px)">
            <div class="strm-headline-label js-content-label C(c_world) Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="world">World</div>
            <h3 class="Mb(4px) Mb(0px)--md1160 LineClamp(2,42px) Lh(21px)">
                <a href="http://www.nytimes.com/2016/02/10/world/asia/that-wasnt-a-meteorite-that-killed-a-man-in-india-nasa-says.html" class="O(n):f C($m_blue):f D(b) Pos(r) js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:5;cposy:11;bpos:1;pos:1;ss_cid:c1c4e431-5853-4ad7-91eb-b1f619fe4d83;refcnt:2;imgt:ss;g:a59894ae-0a10-3dd0-afd7-61d80c081d77;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:World;elm:hdln;elmt:ct;r:4000021900S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><span>That Wasnât a Meteorite That Killed a Man in India, NASA Says</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,34px) Lh(17px) C($gray) Bfv(h) D(n)--md1160">The news reported by Indian newspapers and picked up by many outlets around the world was startling: A bus driver was killed and three people were injured after a meteorite hit a college campus on Saturday. If true, it would have been the first scientifically confirmed report in history of someone being killed by a meteorite impact. By Tuesday, however, the story appeared to be fizzling as scientific experts weighed in. The early reports included images of a crater, five feet deep and two feet wide. Witnesses described hearing an explosion, and the police recovered a black, pockmarked stone from the site, in southeast India. The chief minister of the state, Jayalalithaa Jayaram, promised compensation</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Tt(c) Mt(2px)">New York Times</span>
            </div>
        </div>
        
        <div class="Mt(8px) Mt(7px)--md1160 Ov(h)">
            
            
                <a href="http://news.yahoo.com/man-may-have-been-killed-by-meteorite-in-southern-215721131.html" data-uuid="11b37ea1-9981-37f8-a7ba-008e20db8057" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Mb(6px)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb) Pend(13px)"    data-ylk="cpos:5;cposy:12;bpos:1;pos:2;ss_cid:c1c4e431-5853-4ad7-91eb-b1f619fe4d83;refcnt:2;imgt:ss;g:11b37ea1-9981-37f8-a7ba-008e20db8057;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:World;elm:rhdln;elmt:ct;r:4000021900S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/DWDyKwVM6uLJKQxIf0Q7Vg--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en/homerun/feed_manager_auto_publish_494/832d62797768a0f414c3943dca39fe23" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160" alt=""><div class="D(ib) W(100%)"><span class="Mend(7px) D(b) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Man may have been killed by meteorite in southern India</span><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Yahoo News Digest</span></div></a>
            
            
            
                <a href="https://gma.yahoo.com/indian-man-killed-explosion-caused-meteorite-government-official-205516769--abc-news-topstories.html" data-uuid="aa408ddb-8992-3a24-ac70-ca93ea8e6b71" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb)"    data-ylk="cpos:5;cposy:13;bpos:1;pos:3;ss_cid:c1c4e431-5853-4ad7-91eb-b1f619fe4d83;refcnt:2;imgt:ss;g:aa408ddb-8992-3a24-ac70-ca93ea8e6b71;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:World;elm:rhdln;elmt:ct;r:4000021900S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/3mNqpsBCXfUJUG.UW0KcNQ--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en_us/gma/us.abcnews.go.com/ht_india_meteorite_01_jc_160209_16x9_992.jpg.cf.jpg" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160" alt=""><div class="D(ib) W(100%)"><span class="Mend(7px) D(b) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Indian Man Killed in Explosion Caused by Meteorite, Government Official Says</span><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">ABC News</span></div></a>
            
            
        </div>
        
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:5;cposy:11;bpos:1;pos:1;ss_cid:c1c4e431-5853-4ad7-91eb-b1f619fe4d83;refcnt:2;imgt:ss;g:a59894ae-0a10-3dd0-afd7-61d80c081d77;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:World;r:4000021900S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:United_States_Department_of_State|WIKIID:Classified_information|WIKIID:Hillary_Clinton|YCT:001000661" data-offnet="1" data-uuid="8bdea805-26c2-39ec-9886-dc3c189fb190" data-type="article"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="Pos(r) Wow(bw) Cf Pend(30px)">

    
    <div class="strm-left Fl(start) Pos(r) Pend(18px)"><div class="Pos(r)"><a href="http://thefederalist.com/2016/02/10/how-hillary-could-wiggle-out-of-her-email-woes/" class="Pos(r) D(ib) streamImage js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:6;cposy:14;bpos:1;pos:1;imgt:ss;g:8bdea805-26c2-39ec-9886-dc3c189fb190;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Politics;elm:img;elmt:ct;r:4000023400S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/sd7iYWh3yiH098zID4PeDA--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/58508c4535ee7890e991a573bd6294a4" height="107" width="190" class="W(100%) BackgroundPic rounded-img" alt="">

</a>
        </div>
    </div>
    
    <div class="strm-right Fl(start) W(66%)"><div class="strm-headline">
            <div class="js-content-label C(c_politics) Fw(b) Mb(6px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="politics">Politics</div>
            <h3 class="Mb(4px) LineClamp(3,57px) Lh(19px)">
                <a href="http://thefederalist.com/2016/02/10/how-hillary-could-wiggle-out-of-her-email-woes/" class="Pos(r) O(n):f C($m_blue):f js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:6;cposy:14;bpos:1;pos:1;imgt:ss;g:8bdea805-26c2-39ec-9886-dc3c189fb190;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Politics;elm:hdln;elmt:ct;r:4000023400S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><span>How Hillary Could Wiggle Out Of Her Email Woes</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,33px) Lh(17px) C($gray) Bfv(h)">Emerson reminds us that âa foolish consistency is the hobgoblin of little minds, adored by little statesmen and philosophers and divines.â As much as it may pain conservatives to admit it, former Secretary of State Hillary Clinton is no âlittle statesman.â Indeed, she and her campaign have shown a very clever inconsistency in their statements on lâaffaire email since the story first broke in March 2015. Letâs not underestimate the considerable rhetorical acumen of Clinton and her defenders. Their apparent inconsistency displays an acute understanding of how to deploy legal terms of art before a public accustomed to the lesser form of discourse known as âplain English.â As is well known, during</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Mt(5px) Tt(c)">thefederalist.com</span>
            </div>
        </div>
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mstart(12px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:6;cposy:14;bpos:1;pos:1;imgt:ss;g:8bdea805-26c2-39ec-9886-dc3c189fb190;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Politics;r:4000023400S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Automotive_industry|YCT:001000123"  data-uuid="319a04b7-cd4c-3629-ada0-269b9c126943" data-type="slideshow"  data-hosted="HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="Pos(r) Wow(bw) Cf Pend(30px)">

    
    <div class="strm-left Fl(start) Pos(r) Pend(18px)"><div class="Pos(r)"><a href="https://www.yahoo.com/autos/24-things-wouldnt-expect-car-200023313.html" class="Pos(r) D(ib) streamImage js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:7;cposy:15;bpos:1;pos:1;subsec:24;imgt:ss;g:319a04b7-cd4c-3629-ada0-269b9c126943;ct:4;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:Business;elm:img;elmt:ct;r:4000000970S00004;ccode:mega_global_ranking_hlv2_up_based;" ><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="107" width="190" class="W(100%) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/7spgorjfHgQwuUbmPxeVkw--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en/homerun/feed_manager_auto_publish_494/0ca2e730b9f1aa630676ecc3c3084b56')" alt="">

    <span class="Pos(a) T(50%) Start(50%) Mstart(-27px) Mt(-27px) orb-sprite slideshow-icon Bgp(-228px,-32px) W(66px) H(66px) Ta(c) Va(m)"></span>

</a>
        </div>
    </div>
    
    <div class="strm-right Fl(start) W(66%)"><div class="strm-headline">
            <div class="js-content-label C(c_business) Fw(b) Mb(6px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="business">Business</div>
            <h3 class="Mb(4px) LineClamp(3,57px) Lh(19px)">
                <a href="https://www.yahoo.com/autos/24-things-wouldnt-expect-car-200023313.html" class="Pos(r) O(n):f C($m_blue):f js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:7;cposy:15;bpos:1;pos:1;subsec:24;imgt:ss;g:319a04b7-cd4c-3629-ada0-269b9c126943;ct:4;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:Business;elm:hdln;elmt:ct;r:4000000970S00004;ccode:mega_global_ranking_hlv2_up_based;" ><span>24 Things You Wouldn&#39;t Expect Car Companies to Make</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,33px) Lh(17px) C($gray) Bfv(h)">When carmakers get creative.â</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Mt(5px) Tt(c)">Popular Mechanics</span>
            </div>
        </div>
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mstart(12px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions has-comments js-stream-dislike-disabled">
    
    <li class="ActionComments D(ib) O(n) Mstart(-1px) Pos(r) " data-cmntnum="24"><span class="js-stream-comment-counter D(b) Fz(11px) Ta(c) Fw(b) C(#96989f) js-stream-comment-hidden">24</span><span class="js-stream-comment-counter-update D(b) Fz(11px) Ta(c) Fw(b) C(#96989f)"></span><a href="javascript:void(0)" class="Pos(r) js-stream-comments-button-drawer Td(n) O(n) C($c_icon) Px(14px) Pb(5px) Pt(25px)" role="button" tabindex="0"   data-ylk="cpos:7;cposy:15;bpos:1;pos:1;subsec:24;imgt:ss;g:319a04b7-cd4c-3629-ada0-269b9c126943;ct:4;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:Business;r:4000000970S00004;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;slk:cmmt;itc:1;"><b aria-live="polite" class="js-stream-comment-label ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) Lh(14px) Py(7px) End(-25px) C($signin_blue) js-stream-comments-button>Start(50%) js-stream-comments-button:h>Start(a)">Comments</b><i class="Icon-Fp2 IconComments Fz(21px) "></i></a>
    </li>
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:7;cposy:15;bpos:1;pos:1;subsec:24;imgt:ss;g:319a04b7-cd4c-3629-ada0-269b9c126943;ct:4;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:Business;r:4000000970S00004;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    
<li class="js-stream-ad-noview  js-stream-ad js-video-content Wow(bw) Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)" data-beacon="https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&amp;es=JR0APsEGIS.IJDwOCpme3Jmm2XXYJiIpgieO1hymA6dlqw9b38yS96ro5mrtprrRaoy_VDf7pV0z5wTxGOHpz9tEje149IwCoea3H7pZNu9SgRTvQwkx58NzAHG2OZ1qNVUI3q7aT_CKirNZViHnKhw_S3yPz4EPjOLrkv8DMeflzCXnZPrIfiHZfTJgAnw1_KHJEsuCkbdFJWirq4UmrVIeuMim6zO0KlfngVn5lU4285V.DxenqYXVtdFMGWvfneEjE0ZotRyreC.cNhniIerYtc2l0LHESIYA_pkJebGHFaD5_OnlZRzGgaVkXn8uTY8Cd0_ONHbCQRG0ZbSoFh0WV3VSl2NWkO_F8WHbCM1rPm7wiaP2OpTVVVsjX7Pfxhs9&amp;ap=8" data-ad-feedback-beacon="https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&amp;bs=(15h2moi2a(gid$0de98d92-d01d-11e5-9b07-008cfacc4628-7f1d99642700,st$1455125891097000,li$0,cr$31709922006,dmn$m72020,srv$3,exp$1455133091097000,ct$27,v$1.0,adv$1343196,pbid$1,seid$4250754))&amp;r=1455125891097&amp;al=$(AD_FEEDBACK)" data-uuid="31709922006">
    <div class="js-stream-item-wrap Py(15px) Pos(r)">
        <div class="Pos(r) Wow(bw) Cf Pend(30px)">
            
            <div class="strm-left Fl(start) Pos(r) Mend(18px)">
                <a href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=QrttbQAGIS_nrGwvamgOgYBfaIxOr826w80wFJp7r_cB2fkXmpGMwgYtO0wj07poIgOch3Hr3Mg2o38TGkmEWExUbeOWvsxILh22bCHBOw2wa0Ra3eiGchiHzlGtIveFJtBcvNH3ACKg90uouLu54TqsYoLaJiz8Cqy8X_NvJhXLfAMQttQnrBZnXU_MkVQo.__En9qs00XYQmn27CgzUmPlq6FVNx_0uy6IrVv1KdgoBf2lW2sGEAKNu.rfTxFJmuppwpWDQIXzKAMQ61iHIwHwJ60eLIM_osQPFtiLaC8nvHEaryLbcbkw3Q1wsvMQOIayACmPSHoBzWccoH1W8dhxtlWCxOOr1XgfDrJCJ5GVtP8XRB6f4pli79fIb2BS3Q86wkIpPhOXGXeE1_HlOx4J7rJEqEAymw1gEFtI9GJ_Uw--%26lp=http%3A%2F%2Fpo.st%2FAveCesarBAvf" class="streamImage Pos(r) D(ib) W(100%) ua-ie8_D(i) js-video-target js-video-image" target="_blank"   data-ylk="cpos:8;cposy:16;bpos:1;pos:1;ad:1;elmt:ct;g:31709922006;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:img;r:false;ccode:mega_global_ranking_hlv2_up_based;" >
                    <img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="107" width="190" class="fea-ad H(100%) W(100%) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/mSk4Cy1uWHcmHbjHJYACPA--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1454953225117-474.jpg.cf.jpg')" alt="">
                </a>
            </div>
            
            <div class="strm-right Pos(r) Mstart(29%)">
                
                <div class="Fw(b) Lh(17px) Mb(4px)">
                    <a class="Mend(4px) Fz(13px) D(ib) Va(m) Td(n) C($sponsored) Tt(c)" href="http://help.yahoo.com/kb/index?page=content&amp;y=PROD_FRONT&amp;locale=en_US&amp;id=SLN14553" target="_blank"   data-ylk="cpos:8;cposy:16;bpos:1;pos:1;ad:1;elmt:sp;g:31709922006;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;">Sponsored</a>
                    <a class="D(ib) Va(m) Td(n) adlink C($sponsored)" href="https://info.yahoo.com/privacy/us/yahoo/relevantads.html" target="_blank"   data-ylk="cpos:8;cposy:16;bpos:1;pos:1;ad:1;elmt:info;g:31709922006;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;"><i class="Icon-Fp2 IconMoneyball Fz(13px) D(ib)"></i></a>
                </div>
                
                <h3 class="Mb(4px) Lh(21px)">
                    <a href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=QrttbQAGIS_nrGwvamgOgYBfaIxOr826w80wFJp7r_cB2fkXmpGMwgYtO0wj07poIgOch3Hr3Mg2o38TGkmEWExUbeOWvsxILh22bCHBOw2wa0Ra3eiGchiHzlGtIveFJtBcvNH3ACKg90uouLu54TqsYoLaJiz8Cqy8X_NvJhXLfAMQttQnrBZnXU_MkVQo.__En9qs00XYQmn27CgzUmPlq6FVNx_0uy6IrVv1KdgoBf2lW2sGEAKNu.rfTxFJmuppwpWDQIXzKAMQ61iHIwHwJ60eLIM_osQPFtiLaC8nvHEaryLbcbkw3Q1wsvMQOIayACmPSHoBzWccoH1W8dhxtlWCxOOr1XgfDrJCJ5GVtP8XRB6f4pli79fIb2BS3Q86wkIpPhOXGXeE1_HlOx4J7rJEqEAymw1gEFtI9GJ_Uw--%26lp=http%3A%2F%2Fpo.st%2FAveCesarBAvf" target="_blank"   data-ylk="cpos:8;cposy:16;bpos:1;pos:1;ad:1;elmt:ad;g:31709922006;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:hdln;r:false;ccode:mega_global_ranking_hlv2_up_based;" class="Pos(r) js-stream-content-link js-stream-item-title js-content-title Td(n) Fz(17px) Fw(b) C($link) C($m_blue):h O(n):f C($m_blue):f" >
                        <span>AVE CESAR - Le 17 fÃ©vrier</span>
                        <u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u>
                    </a>
                </h3>
                <div>
                    <p class="Pos(r) stream-summary LineClamp(2,36px) Lh(17px) C($gray) Bfv(h)">DÃ©couvrez la nouvelle comÃ©die Ã©pique de Joel et Ethan Coen</p>
                    <a class="Fz(11px) D(ib) Td(n) C(gray_dark) Tt(c) Mt(4px)" href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=QrttbQAGIS_nrGwvamgOgYBfaIxOr826w80wFJp7r_cB2fkXmpGMwgYtO0wj07poIgOch3Hr3Mg2o38TGkmEWExUbeOWvsxILh22bCHBOw2wa0Ra3eiGchiHzlGtIveFJtBcvNH3ACKg90uouLu54TqsYoLaJiz8Cqy8X_NvJhXLfAMQttQnrBZnXU_MkVQo.__En9qs00XYQmn27CgzUmPlq6FVNx_0uy6IrVv1KdgoBf2lW2sGEAKNu.rfTxFJmuppwpWDQIXzKAMQ61iHIwHwJ60eLIM_osQPFtiLaC8nvHEaryLbcbkw3Q1wsvMQOIayACmPSHoBzWccoH1W8dhxtlWCxOOr1XgfDrJCJ5GVtP8XRB6f4pli79fIb2BS3Q86wkIpPhOXGXeE1_HlOx4J7rJEqEAymw1gEFtI9GJ_Uw--%26lp=http%3A%2F%2Fpo.st%2FAveCesarBAvf" target="_blank"   data-ylk="cpos:8;cposy:16;bpos:1;pos:1;ad:1;elmt:ad;g:31709922006;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;">Universal Pictures</a>
                    
                </div>
            </div>
            <div class="Pos(a) End(-7px) T(0) W(30px) Mend(2px) Ta(end)">
    <ul class="js-stream-side-buttons js-stream-actions">
         <li class="ActionDislike Lh(10px)">
            <a href="javascript:void(0)" class="js-stream-dislike-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(10px)" role="button"   data-ylk="cpos:8;cposy:16;bpos:1;pos:1;ad:1;elmt:op;g:31709922006;ct:1;pkgt:sponsored_img;grpt:singlestory;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;itc:1;rspns:op;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) T(-10px) End(-38px)! Py(5px) js-stream-dislike-button>Start(50%) js-stream-dislike-button:h>Start(a) C(#000)">Dislike</b><i class="Icon-Fp2 IconActionCross Fz(16px) C(#000):h js-stream-ad_D(n) js-stream-ad:h_D(i)"></i></a>
        </li>
    </ul>
</div>

        </div>
    </div>
</li>


    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Liam_Neeson|WIKIID:Natasha_Richardson|YCT:001000288|YCT:001000031|YCT:001000069"  data-uuid="0a86cae5-6ffb-3275-8115-2b1d34efce3b" data-type="article" data-cluster="27eafe81-f8be-4d8c-93f0-be52df31c9a9"  data-hosted="HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="strm-default-clusters Pos(r) Wow(bw) Cf Pend(30px) ">

    
    <div class="strm-left Fl(start) Mend(18px) Pos(r)"><a href="https://www.yahoo.com/style/liam-neeson-is-dating-an-incredibly-famous-woman-114926822.html" class="streamImage Pos(r) D(ib) js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:9;cposy:17;bpos:1;pos:1;ss_cid:27eafe81-f8be-4d8c-93f0-be52df31c9a9;refcnt:2;subsec:1982;imgt:ss;g:0a86cae5-6ffb-3275-8115-2b1d34efce3b;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Celebrity;elm:img;elmt:ct;r:4000017140S00001;ccode:mega_global_ranking_hlv2_up_based;" ><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="190" width="190" class="W(100%) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/caCph_tFqotr_PauDgSHPw--/Zmk9c3RyaW07aD0zODA7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/cd/resizer/2.0/FIT_TO_WIDTH-w809/6acb724c9eda13544bd496e88489b531a7c66b88.jpg.cf.jpg')" alt="">

</a></div>
    
    <div class="strm-right Pos(r) Mstart(29%)"><div class="strm-headline Pend(14px)">
            <div class="strm-headline-label js-content-label C(c_celebrity) Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="celebrity">Celebrity</div>
            <h3 class="Mb(4px) Mb(0px)--md1160 LineClamp(2,42px) Lh(21px)">
                <a href="https://www.yahoo.com/style/liam-neeson-is-dating-an-incredibly-famous-woman-114926822.html" class="O(n):f C($m_blue):f D(b) Pos(r) js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:9;cposy:17;bpos:1;pos:1;ss_cid:27eafe81-f8be-4d8c-93f0-be52df31c9a9;refcnt:2;subsec:1982;imgt:ss;g:0a86cae5-6ffb-3275-8115-2b1d34efce3b;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Celebrity;elm:hdln;elmt:ct;r:4000017140S00001;ccode:mega_global_ranking_hlv2_up_based;" ><span>Liam Neeson Is Dating An &#39;Incredibly Famous Woman&#39; Seven Years After His Wife&#39;s Death</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,34px) Lh(17px) C($gray) Bfv(h) D(n)--md1160">Liam Neeson has revealed that he has found love again, seven years after the death of his wife, Natasha Richardson. Speaking in a new interview, Liam revealed: âIâd embarrass her if I said her name, sheâs incredibly famous.</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Tt(c) Mt(2px)">Stephanie Soteriou</span>
            </div>
        </div>
        
        <div class="Mt(8px) Mt(7px)--md1160 Ov(h)">
            
            
                <a href="http://www.upi.com/Entertainment_News/2016/02/09/Liam-Neeson-romantically-involved-with-famous-woman/8621455029063/" data-uuid="5b59454b-82d2-3e21-8c0b-330594d3b6db" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Mb(6px)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb) Pend(13px)"    data-ylk="cpos:9;cposy:18;bpos:1;pos:2;ss_cid:27eafe81-f8be-4d8c-93f0-be52df31c9a9;refcnt:2;subsec:1982;imgt:ss;g:5b59454b-82d2-3e21-8c0b-330594d3b6db;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Celebrity;elm:rhdln;elmt:ct;r:4000017140S00001;ccode:mega_global_ranking_hlv2_up_based;t4:rel;"  data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/duHK12myX86.FDRXWGCeLA--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/0e274f225f3e1d0f5a0b45f72dd17edf')" alt=""><div class="D(ib) W(100%)"><span class="Mend(7px) D(b) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Liam Neeson romantically involved with 'famous' woman</span><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">UPI</span></div></a>
            
            
            
                <a href="http://news.yahoo.com/is-liam-neeson-dating-kristen-stewart-rumour-mill-113959299.html" data-uuid="b93a22c7-d417-38ec-9124-161e74f655fd" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb)"    data-ylk="cpos:9;cposy:19;bpos:1;pos:3;ss_cid:27eafe81-f8be-4d8c-93f0-be52df31c9a9;refcnt:2;subsec:1982;imgt:ss;g:b93a22c7-d417-38ec-9124-161e74f655fd;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Celebrity;elm:rhdln;elmt:ct;r:4000017140S00001;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" ><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/fLP.g6vgt3Cw4dL.TcfoWg--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en/homerun/feed_manager_auto_publish_494/a25cb59d5b92ebf726b24e22b39d7a0a')" alt=""><div class="D(ib) W(100%)"><span class="Mend(7px) D(b) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Is Liam Neeson Dating Kristen Stewart? Rumour Mill Continues To Go Into Overdrive About Taken ActorâsÂ âFamousâ Girlfriend</span><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Yahoo Celebrity UK</span></div></a>
            
            
        </div>
        
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions has-comments js-stream-dislike-disabled">
    
    <li class="ActionComments D(ib) O(n) Mstart(-1px) Pos(r) " data-cmntnum="1982"><span class="js-stream-comment-counter D(b) Fz(11px) Ta(c) Fw(b) C(#96989f) js-stream-comment-hidden">1982</span><span class="js-stream-comment-counter-update D(b) Fz(11px) Ta(c) Fw(b) C(#96989f)"></span><a href="javascript:void(0)" class="Pos(r) js-stream-comments-button-drawer Td(n) O(n) C($c_icon) Px(14px) Pb(5px) Pt(25px)" role="button" tabindex="0"   data-ylk="cpos:9;cposy:17;bpos:1;pos:1;ss_cid:27eafe81-f8be-4d8c-93f0-be52df31c9a9;refcnt:2;subsec:1982;imgt:ss;g:0a86cae5-6ffb-3275-8115-2b1d34efce3b;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Celebrity;r:4000017140S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;slk:cmmt;itc:1;"><b aria-live="polite" class="js-stream-comment-label ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) Lh(14px) Py(7px) End(-25px) C($signin_blue) js-stream-comments-button>Start(50%) js-stream-comments-button:h>Start(a)">Comments</b><i class="Icon-Fp2 IconComments Fz(21px) "></i></a>
    </li>
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:9;cposy:17;bpos:1;pos:1;ss_cid:27eafe81-f8be-4d8c-93f0-be52df31c9a9;refcnt:2;subsec:1982;imgt:ss;g:0a86cae5-6ffb-3275-8115-2b1d34efce3b;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Celebrity;r:4000017140S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="YCT:001001051|YCT:001001039|YCT:001000992" data-offnet="1" data-uuid="8b5477e3-80f5-3b94-9585-3ceaf6e15007" data-type="article"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="Pos(r) Wow(bw) Cf Pend(30px)">

    
    <div class="strm-left Fl(start) Pos(r) Pend(18px)"><div class="Pos(r)"><a href="http://www.elledecor.com/life-culture/travel/a8151/airline-employee-secrets/" class="Pos(r) D(ib) streamImage js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:10;cposy:20;bpos:1;pos:1;imgt:ss;g:8b5477e3-80f5-3b94-9585-3ceaf6e15007;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:Politics;elm:img;elmt:ct;r:4000014780S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="homes"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="107" width="190" class="W(100%) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/pc1MhNUtVRlYFIp0m6TT.w--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/dd8cea7ad361b3c547faf5622299f1d2')" alt="">

</a>
        </div>
    </div>
    
    <div class="strm-right Fl(start) W(66%)"><div class="strm-headline">
            <div class="js-content-label C(c_politics) Fw(b) Mb(6px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="politics">Politics</div>
            <h3 class="Mb(4px) LineClamp(3,57px) Lh(19px)">
                <a href="http://www.elledecor.com/life-culture/travel/a8151/airline-employee-secrets/" class="Pos(r) O(n):f C($m_blue):f js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:10;cposy:20;bpos:1;pos:1;imgt:ss;g:8b5477e3-80f5-3b94-9585-3ceaf6e15007;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:Politics;elm:hdln;elmt:ct;r:4000014780S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="homes"><span>Airline Employees Spill 12 Fascinating Secrets That Passengers Don&#39;t Know</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,33px) Lh(17px) C($gray) Bfv(h)">Over at Reddit, someone asked airline employees to share secrets that us muggles don&#39;t know about the magical world of air travel. Hoo boy! There are some doozies. As someone who has to fly the (sometimes not so) friendly skies next week, I might have to pack some extra valium and whiskey to deal with what I&#39;ve just learned. Oh well! It&#39;s worth it to speed through the air and 300mph in a metal prison. Hooray! Without further ado, I present you some of the most fascinating secrets of air travel: 1. Unsurprisingly, dogs aren&#39;t into flying. &quot;If you checked your dog there&#39;s about a 30% chance it&#39;s terrified before it even gets on the plane, who knows how scared it gets during the actual flight. Bag</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Mt(5px) Tt(c)">ELLE DECOR</span>
            </div>
        </div>
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mstart(12px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:10;cposy:20;bpos:1;pos:1;imgt:ss;g:8b5477e3-80f5-3b94-9585-3ceaf6e15007;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:Politics;r:4000014780S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Donald_Trump|WIKIID:Patient_Protection_and_Affordable_Care_Act|YCT:001000395|YCT:001000661|YCT:001000511" data-offnet="1" data-uuid="6791242b-1729-39a1-bc9c-23574973fcc9" data-type="article" data-cluster="0f2d5b0e-9026-463b-96f9-d54347e66434"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="strm-default-clusters Pos(r) Wow(bw) Cf Pend(30px) ">

    
    <div class="strm-left Fl(start) Mend(18px) Pos(r)"><a href="https://www.washingtonpost.com/blogs/plum-line/wp/2016/02/09/trump-disqualifies-himself-for-the-gop-nomination-again/" class="streamImage Pos(r) D(ib) js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:11;cposy:21;bpos:1;pos:1;ss_cid:0f2d5b0e-9026-463b-96f9-d54347e66434;refcnt:2;imgt:ss;g:6791242b-1729-39a1-bc9c-23574973fcc9;ct:1;pkgt:cluster_first_img;grpt:storyCluster;cnt_tpc:Politics;elm:img;elmt:ct;r:4000025610S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="190" width="190" class="W(100%) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/TGQ8ThXSpWrq6yBKlcUVqQ--/Zmk9c3RyaW07aD0zODA7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/8a82e4ea60b8d9abd83c56e396eeb808')" alt="">

</a></div>
    
    <div class="strm-right Pos(r) Mstart(29%)"><div class="strm-headline Pend(14px)">
            <div class="strm-headline-label js-content-label C(c_politics) Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="politics">Politics</div>
            <h3 class="Mb(4px) Mb(0px)--md1160 LineClamp(2,42px) Lh(21px)">
                <a href="https://www.washingtonpost.com/blogs/plum-line/wp/2016/02/09/trump-disqualifies-himself-for-the-gop-nomination-again/" class="O(n):f C($m_blue):f D(b) Pos(r) js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:11;cposy:21;bpos:1;pos:1;ss_cid:0f2d5b0e-9026-463b-96f9-d54347e66434;refcnt:2;imgt:ss;g:6791242b-1729-39a1-bc9c-23574973fcc9;ct:1;pkgt:cluster_first_img;grpt:storyCluster;cnt_tpc:Politics;elm:hdln;elmt:ct;r:4000025610S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><span>Trump disqualifies himself for the GOP nomination (again)</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,34px) Lh(17px) C($gray) Bfv(h) D(n)--md1160">The voting is underway in New Hampshire, and the polls show Donald Trump with a comfortable lead. If Trump does manage to win, he will have done so in spite of taking a heretical (for conservatives) position: He refused to state outright that he would do away with the Medicaid expansion, which is a big issue in the state. Politico has a fun story today about how Trump actually cost himself the support of several local Republican politicians, who wanted him to declare flatly that he opposed the Medicaid expansion, which the state undertook, thanks to Democratic governor Maggie Hassan. Governor Hassan has called on lawmakers to renew the Medicaid expansion, again making it an issue in the state.</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Tt(c) Mt(2px)">Washington Post</span>
            </div>
        </div>
        
        <div class="Mt(8px) Mt(7px)--md1160 Ov(h)">
            
            
                <a href="http://www.huffingtonpost.com/john-a-tures/could-kasich-win-by-follo_b_9199220.html?ncid=txtlnkusaolp00000592" data-uuid="2c487265-3e28-3206-a6cd-e7c3da913f43" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Mb(6px)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb) Pend(13px)"    data-ylk="cpos:11;cposy:22;bpos:1;pos:2;ss_cid:0f2d5b0e-9026-463b-96f9-d54347e66434;refcnt:2;imgt:ss;g:2c487265-3e28-3206-a6cd-e7c3da913f43;ct:1;pkgt:cluster_first_img;grpt:storyCluster;cnt_tpc:Politics;elm:rhdln;elmt:ct;r:4000025610S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/Jczt7d8pZ7sXjrdrb4MybA--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en-US/homerun/huffingtonpost.com.rss/df385e01a308a25bb70cf0edc5c0bf2f')" alt=""><div class="D(ib) W(100%)"><span class="Mend(7px) D(b) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Could Kasich Win By Following In Lincoln&#39;s Footsteps?</span><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Huffington Post</span></div></a>
            
            
            
                <a href="http://finance.yahoo.com/news/why-republicans-more-divided-ever-042821960.html" data-uuid="dc922822-7d37-35b5-9664-f623de7d046e" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb)"    data-ylk="cpos:11;cposy:23;bpos:1;pos:3;ss_cid:0f2d5b0e-9026-463b-96f9-d54347e66434;refcnt:2;imgt:ss;g:dc922822-7d37-35b5-9664-f623de7d046e;ct:1;pkgt:cluster_first_img;grpt:storyCluster;cnt_tpc:Politics;elm:rhdln;elmt:ct;r:4000025610S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><div class="D(ib)"><span class="Mend(7px) D(b) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Why Republicans Are More Divided than Ever after New Hampshire</span><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Fortune</span></div></a>
            
            
        </div>
        
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:11;cposy:21;bpos:1;pos:1;ss_cid:0f2d5b0e-9026-463b-96f9-d54347e66434;refcnt:2;imgt:ss;g:6791242b-1729-39a1-bc9c-23574973fcc9;ct:1;pkgt:cluster_first_img;grpt:storyCluster;cnt_tpc:Politics;r:4000025610S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Credit_card|WIKIID:Dave_Ramsey|WIKIID:Credit_card_debt|YCT:001000303|YCT:001000298|YCT:001000299|YCT:001000306" data-offnet="1" data-uuid="d883006f-ca5d-3404-a481-b760a152b761" data-type="article"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="Pos(r) Wow(bw) Cf Pend(30px)">

    <div class="Fl(start) Pos(r) strm-headline strm-full"><div class="C(c_business) js-content-label Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="business">Business</div>
        <h3 class="Mb(4px) LineClamp(3,65px) Lh(21px)">
            <a href="https://www.nerdwallet.com/blog/credit-cards/dave-ramsey-credit-cards/" class="Pos(r) O(n):f C($m_blue):f js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:12;cposy:24;bpos:1;pos:1;g:d883006f-ca5d-3404-a481-b760a152b761;ct:1;pkgt:orphan_no_img;grpt:storyCluster;cnt_tpc:Business;elm:hdln;elmt:ct;r:4000007960S00001;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="finance"><span>What Dave Ramsey Doesnât Acknowledge About Credit Cards</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
        </h3>
        <div>
            <p class="Pos(r) stream-summary LineClamp(3,54px) Lh(17px) C($gray) Bfv(h)">Taking thatÂ information in isolation, Ramseyâs stanceÂ thatÂ thereâs no such thing as responsible credit card use seems to make sense. Several studies do support the notionÂ that consumers paying with credit cardsÂ typically spend more than those using cash. For instance, in 2004, when many fast-food restaurants were just starting to take credit cards,Â then-Wendyâs CEOÂ John Schuessler reported that the average transaction for credit cardÂ purchases was $7, compared withÂ $5 for cash purchases, a 40% increase.</p>
            <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Mt(4px) Tt(c)">nerdwallet</span>
        </div>
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mstart(12px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:12;cposy:24;bpos:1;pos:1;g:d883006f-ca5d-3404-a481-b760a152b761;ct:1;pkgt:orphan_no_img;grpt:storyCluster;cnt_tpc:Business;r:4000007960S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    


    
    </ul>
</div>

<img src="/sy/os/mit/ape/m/81f43c2/t.gif" width="16" height="16" class="D(b) Mx(a) My(12px) js-stream-load-more ImageLoader" style="background-image:url('https://s.yimg.com/zz/nn/lib/metro/g/my/anim_loading_sm.gif')" alt="">


 </div> </div> </div>  <div class="App-Ft Row"> </div>            <!-- App close -->
            </div>
                </div>
            </div>
            <div class="Col3" id="Aside" role="complementary" tabindex="-1">
                <div class="Col3-stack" data-sticker-top="75px" >
                                <div id="applet_p_32209491" class=" M-0 js-applet trending Zoom-1  Mb(30px) " data-applet-guid="p_32209491" data-applet-type="trending" data-applet-params="_suid:32209491" data-i13n="auto:true;sec:tc-ts" data-i13n-sec="tc-ts"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  <div id="mega-trending" class="slingstone-selected"><h2 class="trending-title Cur(p) D(ib) W(50%) Fz(15px) Ell Ov(v) gifts-selected_C($disabledHeading) Trs($trendTrs)" data-category="slingstone">Trending Now</h2><h2 class="trending-title Cur(p) D(ib) W(50%) Fz(15px) Ell Ov(v) Pos(r) slingstone-selected_C($disabledHeading) Trs($trendTrs)" data-category="gifts">
                <i class="Icon-Fp2 IconTumblrHeart gifts-selected_C($giftsIcon) Fz(17px) Fw(100) Pos(a) Start(-19px) T(-2px)"></i>Valentine's Day</h2><ul class="Pos(r) Mt(10px)">
        <li class="trending-list" data-category="slingstone">
            <ul class="M(0) Mstart(-8px) ua-ie8_W(100%) ua-ie7_W(100%) Op(1) gifts-selected_Op(0) gifts-selected_V(h) Trs($trendTrs)"><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Irina+Shayk&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Irina Shayk"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:1;bpos:1;ccode:trending_search;g:c9791905-66e2-3800-83a1-a6091c39cbd8;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">1.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Irina Shayk</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Hannah+Simone&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Hannah Simone"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:2;bpos:1;ccode:trending_search;g:62a5c86a-f4f4-3252-add5-20db459f21ce;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">2.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Hannah Simone</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Hope+Solo&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Hope Solo"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:3;bpos:1;ccode:trending_search;g:c9253e99-c6bb-30df-8faa-74b151ff0922;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">3.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Hope Solo</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Karen+Kasich&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Karen Kasich"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:4;bpos:1;ccode:trending_search;g:65a1ca7c-f8bb-3bff-a77b-5553119e7995;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">4.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Karen Kasich</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Aubrey+O%27Day&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Aubrey O'Day"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:5;bpos:1;ccode:trending_search;g:cfe37a6f-93d1-3a66-8ebf-a3aa7d8e2c73;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">5.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Aubrey O'Day</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Laptop+computers&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Laptop computers"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:6;bpos:1;ccode:trending_search;g:e5eb924f-ebe7-38e2-8114-be8adcc83449;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">6.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Laptop computers</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Jessica+Chastain&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Jessica Chastain"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:7;bpos:1;ccode:trending_search;g:69329b6e-88fc-3f46-86c4-b6ed314f166a;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">7.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Jessica Chastain</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Flavored+coffee&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Flavored coffee"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:8;bpos:1;ccode:trending_search;g:cd95a1dc-eef1-377e-bce2-8c7df37c84a1;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">8.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Flavored coffee</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Ashley+Graham&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Ashley Graham"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:9;bpos:1;ccode:trending_search;g:d1a5c0b8-dad7-3731-baec-fadfa27c8a44;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">9.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Ashley Graham</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=2016+Kia+Sedona&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="2016 Kia Sedona"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:10;bpos:1;ccode:trending_search;g:3cb55c4f-9247-330b-a575-455e1ae94b56;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">10.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> 2016 Kia Sedona</span>
                    </a>
                </li></ul>
        </li><li class="trending-list Pos(a) T(0) W(100%)" data-category="gifts">
            <ul class="M(0) Mstart(-8px) ua-ie8_W(100%) ua-ie7_W(100%) Op(1) slingstone-selected_Op(0) slingstone-selected_V(h) slingstone-selected_H(0) Ov(h) Trs($trendTrs)"><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Open+Heart+necklace&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Open Heart necklace"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:1;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">1.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Open Heart necklace</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Chocolate-dipped+fruit&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Chocolate-dipped fruit"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:2;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">2.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Chocolate-dipped fruit</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Name+a+star&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Name a star"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:3;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">3.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Name a star</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Nightgowns&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Nightgowns"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:4;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">4.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Nightgowns</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Men%27s+rings&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Men's rings"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:5;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">5.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Men's rings</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Cruise+deals&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Cruise deals"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:6;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">6.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Cruise deals</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Luxury+hotels&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Luxury hotels"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:7;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">7.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Luxury hotels</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Beard+trimmers&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Beard trimmers"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:8;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">8.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Beard trimmers</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Balloon+bouquet&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Balloon bouquet"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:9;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">9.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Balloon bouquet</span>
                        </a>
                    </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                        <a href="https://search.yahoo.com/search?p=Corsets&amp;fr=fp-tts-thg-201&amp;fr2=p:fp,m:tn,ct:gift,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Corsets"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:holiday;cpos:10;bpos:2;ccode:trending_search;elm:itm;elmt:topic;itc:0;">
                            <span class="D(ib) W(1.3em) Ta(e) C(#000)">10.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Corsets</span>
                        </a>
                    </li></ul>
        </li></ul>
</div>
 </div> </div> </div>            <!-- App close -->
            </div>                            <div id="my-adsFPAD-base">
                    <div id="my-adsFPAD" class="D-n sda-DAPF">
                        <script>
                            rtAdStart = window.performance && window.performance.now && window.performance.now();
                        </script>
                        <div class="Mx-a" id="my-adsFPAD-iframe">
                            <!-- FPAD has no ad contents. Other Ad position will take over this ad --><noscript>
<!-- SpaceID=2023538075 loc=FPAD noad --><!-- fac-gd2-noad --><!-- gd2-status-2 --><!--QYZ CMS_NONE_SELECTED,,;;FPAD;2023538075;2-->
<img width="1" height="1" alt="" src="https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&amp;bs=(1367hb1jv(gid%24xPsxnTcyLjOW3e8W0kbDGBqqMjAwMVa7dYIQrmos,st%241455125890888424,si%244452051,sp%242023538075,pv%241,v%242.0))&amp;t=J_3-D_3&amp;al=(as%24125vi4u6i,aid%24yc7V_GKLDz0-,cr%24-1,ct%2425,at%24H,eob%24gd1_match_id=-1:ypos=FPAD)"></noscript>
                        </div>
                        <script>
                            rtAdDone = window.performance && window.performance.now && window.performance.now();
                        </script>
                    </div>
                </div>                           <div id="my-adsLREC-base" class="lrec-bgcolor">
                   <div id="my-adsLREC" class="Ta-c Pos-r Z-1 Ht-250 Mb-20">
                        <div id="my-adsLREC-iframe">
                            <noscript><img width="1" height="1" alt="" src="https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&amp;bs=(136cqb9ct(gid%24xPsxnTcyLjOW3e8W0kbDGBqqMjAwMVa7dYIQrmos,si%244452051,sp%242023538075,pv%241,v%242.0,st%241455125891110800))&amp;t=J_3-D_3&amp;al=(as%24125hkc28e,aid%24uK3W_GKLDz0-,cr%24-1,ct%2425,at%24H,eob%24fac2_r=1:fed_status=17:gd1_match_id=-1:pt=8:rbkid=2264184051:ud_risk=0.00:win_s=4:ypos=LREC)"><img width="1" height="1" alt="" src="https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&amp;bs=(136cqb9ct(gid%24xPsxnTcyLjOW3e8W0kbDGBqqMjAwMVa7dYIQrmos,si%244452051,sp%242023538075,pv%241,v%242.0,st%241455125891110800))&amp;t=J_3-D_3&amp;al=(as%24125hkc28e,aid%24uK3W_GKLDz0-,cr%24-1,ct%2425,at%24H,eob%24fac2_r=1:fed_status=17:gd1_match_id=-1:pt=8:rbkid=2264184051:ud_risk=0.00:win_s=4:ypos=LREC)"></noscript>
                        </div>
                    </div>
                </div>            <div id="applet_p_63794" class=" App_v2  M-0 js-applet weather Zoom-1  Mb(30px) " data-applet-guid="p_63794" data-applet-type="weather" data-applet-params="_suid:63794" data-i13n="auto:true;sec:app-wea" data-i13n-sec="app-wea"> <!-- App open -->
        <div class="App-Hd" data-region="header"> <div class="js-applet-view-container-header"> <div class="StencilRoot">
    
        <a href="https://weather.yahoo.com" class="C(#000) C($m_blue):h Td(n)">
            <h2 class="Fz(15px) Fw(b) Mb(18px) D(ib) Grid-U App-Title js-show-panel">
                <span class="js-city-name">
                    
                        
                            Yvrac, 33
                        
                    
                </span>
            </h2>
        </a>
    
    
    <div class="Grid-U">
        <div class="LocationPanel Pos(r) Fl(end) Z(3)"></div>
    </div>
    
</div>
 </div> </div>  <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  <div class="D(ib)">
    
    <ul class="P(0) Mt(0) Mend(0) Cl(b) Td(n) Mb(10px) Mstart(-2px) forecasts forecasts-12597132">
        
         <li class="Pos(r) Ta(c) D(tbc) Pend(30px)">
            <a href="https://weather.yahoo.com/fr/33/yvrac-12597132/" class="C(#000) Td(n)"   data-ylk="itc:0;elm:itm;elmt:ct;rspns:nav;">
                <h3 class="Fz(13px) Mb(7px)">Today</h3>
                <img class="D(b) M(a)" src="/sy/os/mit/ape/w/d8f6e02/dark/rain_day_night.png" width="43" height="43" alt="Fair" />
                <span class="Mstart(6px) D(tbc) Mt(2px)">
                    <b class="Fz(13px) Fw(b) D(tbc) Pend(3px)">46&#xb0;<b class="Hidden">f High</b></b>
                    <b class="Fz(13px) Fw(b) C($temp_low) D(tbc)">42&#xb0;<b class="Hidden">f Low</b></b>
                </span>
            </a>
        </li>
        
         <li class="Pos(r) Ta(c) D(tbc) Pend(30px)">
            <a href="https://weather.yahoo.com/fr/33/yvrac-12597132/" class="C(#000) Td(n)"   data-ylk="itc:0;elm:itm;elmt:ct;rspns:nav;">
                <h3 class="Fz(13px) Mb(7px)">Thu</h3>
                <img class="D(b) M(a)" src="/sy/os/mit/ape/w/d8f6e02/dark/rain_day_night.png" width="43" height="43" alt="Fair" />
                <span class="Mstart(6px) D(tbc) Mt(2px)">
                    <b class="Fz(13px) Fw(b) D(tbc) Pend(3px)">49&#xb0;<b class="Hidden">f High</b></b>
                    <b class="Fz(13px) Fw(b) C($temp_low) D(tbc)">43&#xb0;<b class="Hidden">f Low</b></b>
                </span>
            </a>
        </li>
        
         <li class="Pos(r) Ta(c) D(tbc) Pend(30px)">
            <a href="https://weather.yahoo.com/fr/33/yvrac-12597132/" class="C(#000) Td(n)"   data-ylk="itc:0;elm:itm;elmt:ct;rspns:nav;">
                <h3 class="Fz(13px) Mb(7px)">Fri</h3>
                <img class="D(b) M(a)" src="/sy/os/mit/ape/w/d8f6e02/dark/rain_day_night.png" width="43" height="43" alt="Fair" />
                <span class="Mstart(6px) D(tbc) Mt(2px)">
                    <b class="Fz(13px) Fw(b) D(tbc) Pend(3px)">55&#xb0;<b class="Hidden">f High</b></b>
                    <b class="Fz(13px) Fw(b) C($temp_low) D(tbc)">50&#xb0;<b class="Hidden">f Low</b></b>
                </span>
            </a>
        </li>
        
         <li class="Pos(r) Ta(c) D(tbc) ">
            <a href="https://weather.yahoo.com/fr/33/yvrac-12597132/" class="C(#000) Td(n)"   data-ylk="itc:0;elm:itm;elmt:ct;rspns:nav;">
                <h3 class="Fz(13px) Mb(7px)">Sat</h3>
                <img class="D(b) M(a)" src="/sy/os/mit/ape/w/d8f6e02/dark/rain_day_night.png" width="43" height="43" alt="Fair" />
                <span class="Mstart(6px) D(tbc) Mt(2px)">
                    <b class="Fz(13px) Fw(b) D(tbc) Pend(3px)">56&#xb0;<b class="Hidden">f High</b></b>
                    <b class="Fz(13px) Fw(b) C($temp_low) D(tbc)">47&#xb0;<b class="Hidden">f Low</b></b>
                </span>
            </a>
        </li>
        
    </ul>
    
</div>
 </div> </div> </div>            <!-- App close -->
            </div>            <div id="applet_p_50000171" class=" M-0 js-applet activitylist Zoom-1  Mb-20 " data-applet-guid="p_50000171" data-applet-type="activitylist" data-applet-params="_suid:50000171" data-i13n="auto:true;sec:ltst" data-i13n-sec="ltst"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  
<ul class="Mb(0) P(0) js-activitylist-list">

<li class="js-activitylist-item My(20px)" data-id="139054274079">
    
    <a href="https://www.yahoo.com/katiecouric/los-angeles-mayor-on-nh-primary-and-why-the-070857838.html" class="Td(n):h"   data-ylk="pkgt:2;ct:16;rspns:nav;t1:a4;t2:ltst;t3:post;elm:img;elmt:ct;itc:0;cat:false;g:-1;">
    <div class="Ta(c) Pos(r)">
        <img src="/sy/uu/api/res/1.2/LONSllYJkTcCHuhYcWeMNw--/Zmk9ZmlsbDtoPTQxNDtweW9mZj0wO3E9ODA7dz00MTQ7c209MTthcHBpZD15dGFjaHlvbg--/http://l.yimg.com/dh/ap/default/160210/garcetti.jpg" width="207px" height="207px" class="Bdrs(115px)" alt="">
        
        <div class="orb-sprite C($description) Fz(11px) Pos(a) B(-5px) End(24px) D(tb) orb Bdrs(34px)">
            
        </div>
    </div><span class="D(b) Ta(c) Td(n) M(0) Pt(16px) Pb(23px) Px(50px)">
        <div class="C(#400090) Tt(c) Fw(b) Fz(12px) Mb(1px)">Yahoo News</div>
        <span class="C($title) C($m_blue):h Fw(b) Fz(16px) Lh(18px)">L.A. mayor: Presidential race like âBizarrolandâ</span>
    </span>
    </a>
    
</li>

<li class="js-activitylist-item My(20px)" data-id="139053850189">
    
    <a href="https://www.yahoo.com/food/cake-of-the-day-easy-mini-king-cakes-190752026.html" class="Td(n):h"   data-ylk="pkgt:2;ct:16;rspns:nav;t1:a4;t2:ltst;t3:post;elm:img;elmt:ct;itc:0;cat:false;g:-1;">
    <div class="Ta(c) Pos(r)">
        <img src="/sy/uu/api/res/1.2/B2E9pUMEGggA9zRNFTc_Bw--/Zmk9ZmlsbDtoPTQxNDtweW9mZj0wO3E9ODA7dz00MTQ7c209MTthcHBpZD15dGFjaHlvbg--/http://l.yimg.com/dh/ap/default/160210/kingcake.jpg" width="207px" height="207px" class="Bdrs(115px)" alt="">
        
        
    </div><span class="D(b) Ta(c) Td(n) M(0) Pt(16px) Pb(23px) Px(50px)">
        <div class="C(#400090) Tt(c) Fw(b) Fz(12px) Mb(1px)">Yahoo Food</div>
        <span class="C($title) C($m_blue):h Fw(b) Fz(16px) Lh(18px)">Celebrate Mardi Gras with easy-to-make King Cakes</span>
    </span>
    </a>
    
</li>

<li class="js-activitylist-item Mt(20px)" data-id="139053263779">
    
    <a href="https://www.yahoo.com/parenting/in-praise-of-targets-new-gender-neutral-home-line-185510876.html" class="Td(n):h"   data-ylk="pkgt:2;ct:16;rspns:nav;t1:a4;t2:ltst;t3:post;elm:img;elmt:ct;itc:0;cat:false;g:-1;">
    <div class="Ta(c) Pos(r)">
        <img src="/sy/uu/api/res/1.2/QJEpQZK.iuITPpifLrki1Q--/Zmk9ZmlsbDtoPTQxNDtweW9mZj0wO3E9ODA7dz00MTQ7c209MTthcHBpZD15dGFjaHlvbg--/http://l.yimg.com/dh/ap/default/160210/target2.jpg" width="207px" height="207px" class="Bdrs(115px)" alt="">
        
        
    </div><span class="D(b) Ta(c) Td(n) M(0) Pt(16px) Pb(23px) Px(50px)">
        <div class="C(#400090) Tt(c) Fw(b) Fz(12px) Mb(1px)">Yahoo Parenting</div>
        <span class="C($title) C($m_blue):h Fw(b) Fz(16px) Lh(18px)">Target launching gender-neutral home line for kids</span>
    </span>
    </a>
    
</li>

</ul>

 </div> </div> </div>            <!-- App close -->
            </div>                           <div id="sticky-lrec2-footer" data-plugin="sticker" data-sticker-top="110px" data-sticker-forceposition="top">
               <div class="lrec-bgcolor">
               <div id="my-adsLREC2" class="Ta-c Mt-10 Mb-20 Pos-r Ht-250 darla" data-autorotate="1" data-autoeventrt="15000" data-config={"pos":"LREC2","id":"LREC2","clean":"my-adsLREC2","dest":"my-adsLREC2-iframe","metaSize":true,"w":300,"h":250,"fdb":true,"supports":{"exp-ovr":1}} data-lrec3replace="1" >
                   <div class='Mx-a Ta-c' id="my-adsLREC2-iframe">
                        
                   </div>
               </div>
                               <div id="my-adsLREC3" class="Ta-c Mt-10 Mb-20 Pos-r Ht-250 darla D-n" data-config={"pos":"LREC3","id":"LREC3","clean":"my-adsLREC3","dest":"my-adsLREC3-iframe","metaSize":true,"w":300,"h":250,"fdb":true,"supports":{"exp-ovr":1}}>
                    <div class='Mx-a Ta-c' id="my-adsLREC3-iframe">
                    </div>
               </div>
               <div id="my-adsLREC2-fallback" class="D-n">
                    <a href="https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=20&mc=click&pli=14816902&PluID=0&ord=${CACHEBUSTER}" style="background:url(https://www.yahoo.com/sy/dh/ap/default/160201/sports-dailyfantasy-v2-a966da5__1_.jpg) 0 0 no-repeat;height:250px;width:300px;display:block;"></a>
               </div>
               </div>
                    <div id="Footer" class="Row Pstart-20 Pend-10 Pos-r" role="contentinfo">
                         <ul class="Bdrs(3px) Px(20px) Py(14px) Ta(c)" role="contentinfo" id="fp-footer"><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://info.yahoo.com/legal/us/yahoo/utos/terms/" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Terms</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://info.yahoo.com/privacy/us/yahoo/" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Privacy</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="https://advertising.yahoo.com/" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Advertise</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://info.yahoo.com/privacy/us/yahoo/relevantads.html" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">About our Ads</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://careers.yahoo.com/us" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Careers</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://help.yahoo.com/l/us/yahoo/helpcentral/" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Help</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="https://yahoo.uservoice.com/forums/341361-yahoo-home" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Feedback</a></li></ul></div>
                         
                    </div>
                </div>
            </div>
        </div>
    </div>





        
                        <div id="darla-assets-bottom">
                    <script type="text/x-safeframe" id="fc" _ver="2-9-4">{ "positions": [ { "html": "<!-- SpaceID=2023538075 loc=FPAD noad --><!-- fac-gd2-noad --><!-- gd2-status-2 --><!--QYZ CMS_NONE_SELECTED,,;;FPAD;2023538075;2-->", "id": "FPAD", "meta": { "y": { "cscHTML": "<scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_d==null)window.xzq_d=new Object();\nwindow.xzq_d['yc7V_GKLDz0-']='(as$125vi4u6i,aid$yc7V_GKLDz0-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=FPAD)';\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(1367hb1jv(gid$xPsxnTcyLjOW3e8W0kbDGBqqMjAwMVa7dYIQrmos,st$1455125890888424,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$125vi4u6i,aid$yc7V_GKLDz0-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=FPAD)\"></noscr"+"ipt>", "cscURI": "https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(1367hb1jv(gid$xPsxnTcyLjOW3e8W0kbDGBqqMjAwMVa7dYIQrmos,st$1455125890888424,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$125vi4u6i,aid$yc7V_GKLDz0-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=FPAD)", "impID": "", "supp_ugc": "0", "placementID": "-1", "creativeID": "-1", "serveTime": "1.4551258908884E+15", "behavior": "non_exp", "adID": "#2", "matchID": "#2", "err": "invalid_space", "facStatus": {"fedStatusCode":"0","fedStatusMessage":"federation is not configured for ad slot"}, "hasExternal": 0, "size": "", "bookID": "-1", "serveType": "-1", "slotID": "0", "fdb": "{ \"fdb_url\": \"http:\\/\\/beap-bc.yahoo.com\\/af?bv=1.0.0&bs=(15ir45r6b(gid$jmTVQDk4LjHHbFsHU5jMkgKkMTAuNwAAAACljpkK,st$1402537233026922,srv$1,si$13303551,adv$25941429036,ct$25,li$3239250051,exp$1402544433026922,cr$4154984551,pbid$25372728133,v$1.0))&al=(type${type},cmnt${cmnt},subo${subo})&r=10\", \"fdb_on\": \"1\", \"fdb_exp\": \"1402544433026\", \"fdb_intl\": \"en-us\" , \"d\" : \"1\" }", "slotData": "{\"pt\":\"0\",\"bamt\":\"10000000000.000000\",\"namt\":\"0.000000\",\"isLiveAdPreview\":\"false\",\"is_ad_feedback\":\"false\",\"trusted_custom\":\"false\",\"isCompAds\":\"false\",\"pvid\":\"xPsxnTcyLjOW3e8W0kbDGBqqMjAwMVa7dYIQrmos\"}", "adc": "{\"label\":\"AdChoices\",\"url\":\"https:\\/\\/info.yahoo.com\\/privacy\\/us\\/yahoo\\/relevantads.html\",\"close\":\"Close\",\"closeAd\":\"Close Ad\",\"showAd\":\"Show ad\",\"collapse\":\"Collapse\",\"fdb\":\"I don't like this ad\",\"code\":\"en-us\"}", "is3rd": "0", "userProvidedData": {} } } },{ "html": "<scr"+"ipt type=\"text/javascr"+"ipt\">\ndocument.write('<img src=\"https://na.ads.yahoo.com/yax/csc?sn=a5ae132d507eb93db40bd4d21136238d268e4414&es=whx8C87RVIPfbHKYc_nm6zGVcr2e02bBRRrhXK_ORKjyikO7dFtA3umeM6eBc7j2rKIBRto2Y5BeMQkrne5OGDG9R_BWszaCAY4Xr3f7lsK0IJeKsNsI7HGqX9Q.Zi_SOZQEXVrvOJKcv4dZ10qmnzgPvbcqIZKLRjSY3JnhFiJm9Ec8cVfRX_baocFPMzbov7vNOv4PwKMY._2cd4JXczptJBFrnq96U10qgeQ3lXwJdxrK2RrWrDxbLVdND8UGvPFIRLFCNsfUPG4FHRWd5Tl62V7dvkbggIu28pYF7t6I7S20Z7ljFe7djBdLL3OJfk17dRo3cu7se0Kc67c1LrEcp8xdPahi91UFQ1qt3irCtIY-&ve=2&ty=0&url=\" style=\"display:none\" height=\"1\" width=\"1\" alt=\"\">');\ndocument.write('<scr' + 'ipt type=\\\"text\\/javascr"+"ipt\\\" src=\\\"https:\\/\\/pr.ybp.yahoo.com\\/ab\\/secure\\/true\\/imp\\/SjKgPA4_v-25xmlTNAd-znJtYuGPr-2gjSy-15pjZnqW3qqL0GLLJX_Ui85jnLJVEPKhv4Op7h7JXtVyCaYCKFBAFz5diebTcxMP2pz0awS3NKKOUDIuVQQauFR4wz2Ue5RJWa1OjaF-_K9hkPdrPQ0koxMMMtbsisSAy_P8qSpnfQO7hS-9um5_UFI5XTksY0SCD_6XDBRZtjegZWJ1hTaQbnb2owgisyW0ueqjolW9kYQ8oWJT4zWWcZXf3tH8hokY64jrnI0g6Bp-67_8YKCsOUF4xhkW_wYhBXPkDIRNj4fIHjHsCFq-zZB_wobLIJmSRnw-wE6e2UQlOnv2ihWzn1OcTsdE07LyU8_GAOF3s-cDR-W1h4Jx1vGnAmQzvaR-lEKOatdP_Qo8YZWNlppGqpdnWtC5-4Vrcs7rNua9LnmIqgIYheJF6JhtWvkyvh_8xR3eLbC0-z3SOehFRCXyIcV-6jnHBaN6sjgnP4jcrl_IyzhwOtZ54koxXZYWIS_KH0_-VKVu8YkbS_K0PEPE_Nevwt8dLVcq7QGJhKVtAT9ql1LA21dKz9HS2eXba3jbYOZwdKPoSv8DzxRVIlVKRjjwKPpGBcJ5g-Q7UESUaOqBcW5BP9eDdvAx2ZsJyLaNLUcUhMWDxZK6BwB5TInuhchTJnSQ0f3bLjzNXEwxYl14af_EkgGsK6aZRcDo_6Nfdb3sxFPFVpjVJ4UrKTD7caTjRvjKR2ivT14lymvPx8R0Ajotg1Y3V0Sd1WSIyqy1mOxx9lqa7nrOiIyjFgOirIuPpCxPCZ_0FjAJpUTZ-QS2k3toswbdQV33u-ksdnRVxwpracFWvQc2jTMRCOf7rtEfZsYlW4ZzH18jb_Y9ffab48_jD2nzFMg6oRF8kHDF3reWc67IiZEp6O7gAZUYXfz5T8xIGkowZmuoZOjP7F0dXOrZjjooLAOYlefL6Q_1in_M93afLNbbQ0YcXBfkThL_nUwKG16OQP5SsFgpWiTdnO222cDORexROWew2MTUO7SUMFNWuYGvVEsynmFFCv0uh9f2zNiwP-zTlclkhQE5TyjtzOwCF7Mw2qXVDwcuzwXO1Ssylg1McCS3L3pcPIBhgYRxje1XuTtXEVF2br0p5GlvPwqhdUZjloymxyWWNBf0xFj5QSFajeXMAwBTLYCzAEr0vU_zAk7VAt6xhlqo3sumcpa1BKvCF-Gsgxb0GSSuSPpHjiTbN7ZcIwNl7Fc6Nt6-FzLm0kzzfbALuBKG3N7jEKL8eIG3ybXXQttcKSFIAEJmnXKp1KUUm-3UqoSxI73nwBHnpz5RTBM\\/wp\\/0.05250000000000001\\/pclick\\/https://na.ads.yahoo.com/yax/clk?sn%3D7e49d8867826e45612b427908529d048216a3d92%26es%3DmxO1W2HRVINXceh_fmOhFEI9tKpkFPfb2voHHvBQZzwwQ381xmtvK2GBL8VVWe8CoF.g51NrQzzfCN3j0efXDYe6oSt_exqbxChjCo4fHFFe50k6KTrVuR8fV8vVxMtXoS_QSG5jwB6mdvbElN48B3ExJpq9cPni3Upz3vYouVCJiApjjLmtQfAHx61MuWf8zlDQQQ2btL_46LQ0NhoobFCYhZk8pGpAaymqk4AXFVyaFqXYkisWpMunaseoLoBkfIqPWubwjguBWRDh4b3gzDABLti2KCXVzNWxB7fCa79TltVFfvUQoklHNtoEyhB_4gMVYx2zCacQAfULjbi3anEQO2e76RFk6vOifq342rHgg8A-%26ve%3D2%26ty%3D0%26url%3D\\\"><\\/scr' + 'ipt>');\ndocument.write('<scr"+"ipt type=\"text/javascr"+"ipt\" src=\"https://ads.yahoo.com/get-user-id?ver=2&n=23351&ts=1455125890&sig=64bc4d0c14e62b2c\"><\\/scr"+"ipt>');document.write('<!--YAXR BannerAd CrId:2637744, RmxCrId:36403843, DspCrId:250625-->');\n</scr"+"ipt><scr"+"ipt language=javascr"+"ipt> if(window.xzq_d==null)window.xzq_d=new Object();window.xzq_d['uK3W_GKLDz0-']='(as$125hkc28e,aid$uK3W_GKLDz0-,cr$-1,ct$25,at$H,eob$fac2_r=1:fed_status=17:gd1_match_id=-1:pt=8:rbkid=2264184051:ud_risk=0.00:win_s=4:ypos=LREC)';</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136cqb9ct(gid$xPsxnTcyLjOW3e8W0kbDGBqqMjAwMVa7dYIQrmos,si$4452051,sp$2023538075,pv$1,v$2.0,st$1455125891110800))&t=J_3-D_3&al=(as$125hkc28e,aid$uK3W_GKLDz0-,cr$-1,ct$25,at$H,eob$fac2_r=1:fed_status=17:gd1_match_id=-1:pt=8:rbkid=2264184051:ud_risk=0.00:win_s=4:ypos=LREC)\"></noscr"+"ipt>", "id": "LREC", "meta": { "y": { "cscHTML": "<scr"+"ipt language=javascr"+"ipt> if(window.xzq_d==null)window.xzq_d=new Object();window.xzq_d['uK3W_GKLDz0-']='(as$125hkc28e,aid$uK3W_GKLDz0-,cr$-1,ct$25,at$H,eob$fac2_r=1:fed_status=17:gd1_match_id=-1:pt=8:rbkid=2264184051:ud_risk=0.00:win_s=4:ypos=LREC)';</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136cqb9ct(gid$xPsxnTcyLjOW3e8W0kbDGBqqMjAwMVa7dYIQrmos,si$4452051,sp$2023538075,pv$1,v$2.0,st$1455125891110800))&t=J_3-D_3&al=(as$125hkc28e,aid$uK3W_GKLDz0-,cr$-1,ct$25,at$H,eob$fac2_r=1:fed_status=17:gd1_match_id=-1:pt=8:rbkid=2264184051:ud_risk=0.00:win_s=4:ypos=LREC)\"></noscr"+"ipt>", "cscURI": "https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136cqb9ct(gid$xPsxnTcyLjOW3e8W0kbDGBqqMjAwMVa7dYIQrmos,si$4452051,sp$2023538075,pv$1,v$2.0,st$1455125891110800))&t=J_3-D_3&al=(as$125hkc28e,aid$uK3W_GKLDz0-,cr$-1,ct$25,at$H,eob$fac2_r=1:fed_status=17:gd1_match_id=-1:pt=8:rbkid=2264184051:ud_risk=0.00:win_s=4:ypos=LREC)", "impID": "uK3W_GKLDz0-", "supp_ugc": "0", "placementID": "3459460551", "creativeID": "4467049051", "serveTime": "1455125890888424", "behavior": "non_exp", "adID": "1234567", "matchID": "999999.999999.999999.999999", "err": "", "facStatus": {"fedStatusCode":"17","fedStatusMessage":"YAX/YAM/SAPY replaced GD2 ESOV line"}, "hasExternal": 0, "size": "300x250", "bookID": "2264184051", "serveType": "-1", "slotID": "1", "fdb": "{ \"fdb_url\": \"https:\\\/\\\/beap-bc.yahoo.com\\\/af\\\/us?bv=1.0.0&bs=(160ntlugf(gid$xPsxnTcyLjOW3e8W0kbDGBqqMjAwMVa7dYIQrmos,st$1455125890888424,srv$1,si$4452051,ct$25,exp$1455133090888424,adv$26513753608,li$3458215051,cr$4467049051,v$1.0,pbid$20459933223,seid$146750051))&al=(type${type},cmnt${cmnt},subo${subo})&r=10\", \"fdb_on\": \"1\", \"fdb_exp\": \"1455133090888\", \"fdb_intl\": \"en-US\" }", "slotData": "", "adc": "{\"label\":\"AdChoices\",\"url\":\"https:\\/\\/info.yahoo.com\\/privacy\\/us\\/yahoo\\/relevantads.html\",\"close\":\"Close\",\"closeAd\":\"Close Ad\",\"showAd\":\"Show ad\",\"collapse\":\"Collapse\",\"fdb\":\"I don't like this ad\",\"code\":\"en-us\"}", "is3rd": "1", "userProvidedData": {} } } },{ "html": "<!-- SpaceID=2023538075 loc=MAST noad --><!-- fac-gd2-noad --><!-- gd2-status-2 --><!--QYZ CMS_NONE_SELECTED,,;;MAST;2023538075;2-->", "id": "MAST", "meta": { "y": { "cscHTML": "<scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_d==null)window.xzq_d=new Object();\nwindow.xzq_d['p4zX_GKLDz0-']='(as$125dodo5t,aid$p4zX_GKLDz0-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=MAST)';\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(1367hb1jv(gid$xPsxnTcyLjOW3e8W0kbDGBqqMjAwMVa7dYIQrmos,st$1455125890888424,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$125dodo5t,aid$p4zX_GKLDz0-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=MAST)\"></noscr"+"ipt>", "cscURI": "https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(1367hb1jv(gid$xPsxnTcyLjOW3e8W0kbDGBqqMjAwMVa7dYIQrmos,st$1455125890888424,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$125dodo5t,aid$p4zX_GKLDz0-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=MAST)", "impID": "", "supp_ugc": "0", "placementID": "-1", "creativeID": "-1", "serveTime": "1455125890888424", "behavior": "non_exp", "adID": "#2", "matchID": "#2", "err": "invalid_space", "facStatus": {"fedStatusCode":"31","fedStatusMessage":"Yield optimization did not run"}, "hasExternal": 0, "size": "", "bookID": "-1", "serveType": "-1", "slotID": "2", "fdb": "{ \"fdb_url\": \"http:\\/\\/beap-bc.yahoo.com\\/af?bv=1.0.0&bs=(15ir45r6b(gid$jmTVQDk4LjHHbFsHU5jMkgKkMTAuNwAAAACljpkK,st$1402537233026922,srv$1,si$13303551,adv$25941429036,ct$25,li$3239250051,exp$1402544433026922,cr$4154984551,pbid$25372728133,v$1.0))&al=(type${type},cmnt${cmnt},subo${subo})&r=10\", \"fdb_on\": \"1\", \"fdb_exp\": \"1402544433026\", \"fdb_intl\": \"en-us\" , \"d\" : \"1\" }", "slotData": "{\"pt\":\"0\",\"bamt\":\"10000000000.000000\",\"namt\":\"0.000000\",\"isLiveAdPreview\":\"false\",\"is_ad_feedback\":\"false\",\"trusted_custom\":\"false\",\"isCompAds\":\"false\",\"pvid\":\"xPsxnTcyLjOW3e8W0kbDGBqqMjAwMVa7dYIQrmos\"}", "adc": "{\"label\":\"AdChoices\",\"url\":\"https:\\/\\/info.yahoo.com\\/privacy\\/us\\/yahoo\\/relevantads.html\",\"close\":\"Close\",\"closeAd\":\"Close Ad\",\"showAd\":\"Show ad\",\"collapse\":\"Collapse\",\"fdb\":\"I don't like this ad\",\"code\":\"en-us\"}", "is3rd": "0", "userProvidedData": {} } } },{ "html": "<!-- APT Vendor: Right Media, Format: Standard Graphical -->\n<style>\n	#fc_align{\n		position: static !important;\n		width: 120px !important;\n		margin: auto !important;\n	}\n</style>\n<SCR"+"IPT TYPE=\"text/javascr"+"ipt\" SRC=\"https://na.ads.yahoo.com/yax/banner?ve=1&tt=1&si=251157193&asz=120x60&u=https://www.yahoo.com&gdAdId=lmvY_GKLDz0-&gdUuid=xPsxnTcyLjOW3e8W0kbDGBqqMjAwMVa7dYIQrmos&gdSt=1455125890888424&publisher_blob=${RS}|xPsxnTcyLjOW3e8W0kbDGBqqMjAwMVa7dYIQrmos|2023538075|TXTL|1455125890.574447|2-9-4:ysd:1&pub_redirect=https://beap-bc.yahoo.com/yc/YnY9MS4wLjAmYnM9KDE3aWQxb3F1NShnaWQkeFBzeG5UY3lMak9XM2U4VzBrYkRHQnFxTWpBd01WYTdkWUlRcm1vcyxzdCQxNDU1MTI1ODkwODg4NDI0LHNpJDQ0NTIwNTEsc3AkMjAyMzUzODA3NSxjdCQyNSx5YngkOUY2UkhYQ3cwaEtobGFFLm5NUmp1ZyxsbmckZW4tdXMsY3IkNDQ1MTAwOTA1MSx2JDIuMCxhaWQkbG12WV9HS0xEejAtLGJpJDIyNzUxODgwNTEsbW1lJDk1OTMzMjA3NzYxNzQzNDk0MDgsciQwLHlvbyQxLGFncCQzNDc2NTAyNTUxLGFwJFRYVEwpKQ/2/*&K=1\"></SCR"+"IPT><scr"+"ipt>var url = \"\"; if(url && url.search(\"http\") != -1){document.write('<scr"+"ipt src=\"' + url + '\"><\\/scr"+"ipt>');}</scr"+"ipt><!--QYZ 2275188051,4451009051,;;TXTL;2023538075;1-->", "id": "TXTL", "meta": { "y": { "cscHTML": "<scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_d==null)window.xzq_d=new Object();\nwindow.xzq_d['lmvY_GKLDz0-']='(as$13a3k053f,aid$lmvY_GKLDz0-,bi$2275188051,agp$3476502551,cr$4451009051,ct$25,at$H,eob$gd1_match_id=-1:ypos=TXTL)';\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(1367hb1jv(gid$xPsxnTcyLjOW3e8W0kbDGBqqMjAwMVa7dYIQrmos,st$1455125890888424,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$13a3k053f,aid$lmvY_GKLDz0-,bi$2275188051,agp$3476502551,cr$4451009051,ct$25,at$H,eob$gd1_match_id=-1:ypos=TXTL)\"></noscr"+"ipt>", "cscURI": "https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(1367hb1jv(gid$xPsxnTcyLjOW3e8W0kbDGBqqMjAwMVa7dYIQrmos,st$1455125890888424,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$13a3k053f,aid$lmvY_GKLDz0-,bi$2275188051,agp$3476502551,cr$4451009051,ct$25,at$H,eob$gd1_match_id=-1:ypos=TXTL)", "impID": "lmvY_GKLDz0-", "supp_ugc": "0", "placementID": "3476502551", "creativeID": "4451009051", "serveTime": "1455125890888424", "behavior": "non_exp", "adID": "9593320776174349408", "matchID": "999999.999999.999999.999999", "err": "", "facStatus": {"fedStatusCode":"0","fedStatusMessage":"federation is not configured for ad slot"}, "hasExternal": 0, "size": "120x45", "bookID": "2275188051", "serveType": "-1", "slotID": "3", "fdb": "{ \"fdb_url\": \"https:\\\/\\\/beap-bc.yahoo.com\\\/af\\\/us?bv=1.0.0&bs=(160r8h731(gid$xPsxnTcyLjOW3e8W0kbDGBqqMjAwMVa7dYIQrmos,st$1455125890888424,srv$1,si$4452051,ct$25,exp$1455133090888424,adv$26679945979,li$3474892551,cr$4451009051,v$1.0,pbid$20459933223,seid$146750051))&al=(type${type},cmnt${cmnt},subo${subo})&r=10\", \"fdb_on\": \"1\", \"fdb_exp\": \"1455133090888\", \"fdb_intl\": \"en-US\" }", "slotData": "{\"pt\":\"3\",\"bamt\":\"10000000000.000000\",\"namt\":\"0.000000\",\"isLiveAdPreview\":\"false\",\"is_ad_feedback\":\"false\",\"trusted_custom\":\"false\",\"isCompAds\":\"false\",\"adjf\":\"1.000000\",\"alpha\":\"1.000000\",\"ffrac\":\"1.000000\",\"pcpm\":\"-1.000000\",\"fc\":\"false\",\"ecpm\":0.000000,\"sdate\":\"1446744408\",\"edate\":\"1514782799\",\"bimpr\":0.000000,\"pimpr\":-376954784.000000,\"spltp\":100.000000,\"frp\":\"false\",\"pvid\":\"xPsxnTcyLjOW3e8W0kbDGBqqMjAwMVa7dYIQrmos\"}", "adc": "{\"label\":\"AdChoices\",\"url\":\"https:\\/\\/info.yahoo.com\\/privacy\\/us\\/yahoo\\/relevantads.html\",\"close\":\"Close\",\"closeAd\":\"Close Ad\",\"showAd\":\"Show ad\",\"collapse\":\"Collapse\",\"fdb\":\"I don't like this ad\",\"code\":\"en-us\"}", "is3rd": "0", "userProvidedData": {} } } } ], "meta": { "y": { "pageEndHTML": "<scr"+"ipt language=javascr"+"ipt>\n(function(){window.xzq_p=function(R){M=R};window.xzq_svr=function(R){J=R};function F(S){var T=document;if(T.xzq_i==null){T.xzq_i=new Array();T.xzq_i.c=0}var R=T.xzq_i;R[++R.c]=new Image();R[R.c].src=S}window.xzq_sr=function(){var S=window;var Y=S.xzq_d;if(Y==null){return }if(J==null){return }var T=J+M;if(T.length>P){C();return }var X=\"\";var U=0;var W=Math.random();var V=(Y.hasOwnProperty!=null);var R;for(R in Y){if(typeof Y[R]==\"string\"){if(V&&!Y.hasOwnProperty(R)){continue}if(T.length+X.length+Y[R].length<=P){X+=Y[R]}else{if(T.length+Y[R].length>P){}else{U++;N(T,X,U,W);X=Y[R]}}}}if(U){U++}N(T,X,U,W);C()};function N(R,U,S,T){if(U.length>0){R+=\"&al=\"}F(R+U+\"&s=\"+S+\"&r=\"+T)}function C(){window.xzq_d=null;M=null;J=null}function K(R){xzq_sr()}function B(R){xzq_sr()}function L(U,V,W){if(W){var R=W.toString();var T=U;var Y=R.match(new RegExp(\"\\\\\\\\(([^\\\\\\\\)]*)\\\\\\\\)\"));Y=(Y[1].length>0?Y[1]:\"e\");T=T.replace(new RegExp(\"\\\\\\\\([^\\\\\\\\)]*\\\\\\\\)\",\"g\"),\"(\"+Y+\")\");if(R.indexOf(T)<0){var X=R.indexOf(\"{\");if(X>0){R=R.substring(X,R.length)}else{return W}R=R.replace(new RegExp(\"([^a-zA-Z0-9$_])this([^a-zA-Z0-9$_])\",\"g\"),\"$1xzq_this$2\");var Z=T+\";var rv = f( \"+Y+\",this);\";var S=\"{var a0 = '\"+Y+\"';var ofb = '\"+escape(R)+\"' ;var f = new Function( a0, 'xzq_this', unescape(ofb));\"+Z+\"return rv;}\";return new Function(Y,S)}else{return W}}return V}window.xzq_eh=function(){if(E||I){this.onload=L(\"xzq_onload(e)\",K,this.onload,0);if(E&&typeof (this.onbeforeunload)!=O){this.onbeforeunload=L(\"xzq_dobeforeunload(e)\",B,this.onbeforeunload,0)}}};window.xzq_s=function(){setTimeout(\"xzq_sr()\",1)};var J=null;var M=null;var Q=navigator.appName;var H=navigator.appVersion;var G=navigator.userAgent;var A=parseInt(H);var D=Q.indexOf(\"Microsoft\");var E=D!=-1&&A>=4;var I=(Q.indexOf(\"Netscape\")!=-1||Q.indexOf(\"Opera\")!=-1)&&A>=4;var O=\"undefined\";var P=2000})();\n</scr"+"ipt><scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_svr)xzq_svr('https://csc.beap.bc.yahoo.com/');\nif(window.xzq_p)xzq_p('yi?bv=1.0.0&bs=(1367hb1jv(gid$xPsxnTcyLjOW3e8W0kbDGBqqMjAwMVa7dYIQrmos,st$1455125890888424,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3');\nif(window.xzq_s)xzq_s();\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(1367hb1jv(gid$xPsxnTcyLjOW3e8W0kbDGBqqMjAwMVa7dYIQrmos,st$1455125890888424,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3\"></noscr"+"ipt>", "pos_list": [ "FPAD","LREC","MAST","TXTL" ], "filtered": [  ], "spaceID": "2023538075", "host": "www.yahoo.com", "lookupTime": "227", "k2_uri": "", "fac_rt": "-1", "serveTime":"1455125890888424", "pvid": "xPsxnTcyLjOW3e8W0kbDGBqqMjAwMVa7dYIQrmos", "tID": "darla_prefetch_1455125890887_2029884474_1", "npv": "0", "ep": "{\"site-attribute\":\"fdn=1 Y-BUCKET=\\\"201\\\"\",\"secure\":true,\"lang\":\"en-US\",\"ref\":\"https:\\/\\/www.yahoo.com\",\"filter\":\"no_expandable;\",\"darlaID\":\"darla_instance_1455125890886_574818959_0\"}", "pe": "CWZ1bmN0aW9uIGRwZWQoKSB7IAoJaWYod2luZG93Lnh6cV9kPT1udWxsKXdpbmRvdy54enFfZD1uZXcgT2JqZWN0KCk7CndpbmRvdy54enFfZFsneWM3Vl9HS0xEejAtJ109JyhhcyQxMjV2aTR1NmksYWlkJHljN1ZfR0tMRHowLSxjciQtMSxjdCQyNSxhdCRILGVvYiRnZDFfbWF0Y2hfaWQ9LTE6eXBvcz1GUEFEKSc7CglpZih3aW5kb3cueHpxX2Q9PW51bGwpd2luZG93Lnh6cV9kPW5ldyBPYmplY3QoKTt3aW5kb3cueHpxX2RbJ3VLM1dfR0tMRHowLSddPScoYXMkMTI1aGtjMjhlLGFpZCR1SzNXX0dLTER6MC0sY3IkLTEsY3QkMjUsYXQkSCxlb2IkZmFjMl9yPTE6ZmVkX3N0YXR1cz0xNzpnZDFfbWF0Y2hfaWQ9LTE6cHQ9ODpyYmtpZD0yMjY0MTg0MDUxOnVkX3Jpc2s9MC4wMDp3aW5fcz00Onlwb3M9TFJFQyknOwoJaWYod2luZG93Lnh6cV9kPT1udWxsKXdpbmRvdy54enFfZD1uZXcgT2JqZWN0KCk7CndpbmRvdy54enFfZFsncDR6WF9HS0xEejAtJ109JyhhcyQxMjVkb2RvNXQsYWlkJHA0elhfR0tMRHowLSxjciQtMSxjdCQyNSxhdCRILGVvYiRnZDFfbWF0Y2hfaWQ9LTE6eXBvcz1NQVNUKSc7aWYod2luZG93Lnh6cV9kPT1udWxsKXdpbmRvdy54enFfZD1uZXcgT2JqZWN0KCk7CndpbmRvdy54enFfZFsnbG12WV9HS0xEejAtJ109JyhhcyQxM2EzazA1M2YsYWlkJGxtdllfR0tMRHowLSxiaSQyMjc1MTg4MDUxLGFncCQzNDc2NTAyNTUxLGNyJDQ0NTEwMDkwNTEsY3QkMjUsYXQkSCxlb2IkZ2QxX21hdGNoX2lkPS0xOnlwb3M9VFhUTCknOwoJCSB9OwpkcGVkLnRyYW5zSUQgPSAiZGFybGFfcHJlZmV0Y2hfMTQ1NTEyNTg5MDg4N18yMDI5ODg0NDc0XzEiOwoKCWZ1bmN0aW9uIGRwZXIoKSB7IAoJCmlmKHdpbmRvdy54enFfc3ZyKXh6cV9zdnIoJ2h0dHBzOi8vY3NjLmJlYXAuYmMueWFob28uY29tLycpOwppZih3aW5kb3cueHpxX3ApeHpxX3AoJ3lpP2J2PTEuMC4wJmJzPSgxMzY3aGIxanYoZ2lkJHhQc3huVGN5TGpPVzNlOFcwa2JER0JxcU1qQXdNVmE3ZFlJUXJtb3Msc3QkMTQ1NTEyNTg5MDg4ODQyNCxzaSQ0NDUyMDUxLHNwJDIwMjM1MzgwNzUscHYkMSx2JDIuMCkpJnQ9Sl8zLURfMycpOwppZih3aW5kb3cueHpxX3MpeHpxX3MoKTsKCgoJCiB9OwpkcGVyLnRyYW5zSUQgPSJkYXJsYV9wcmVmZXRjaF8xNDU1MTI1ODkwODg3XzIwMjk4ODQ0NzRfMSI7Cgo=", "pym": "" } } } </script>    <script type="text/javascript">
        var pageloadValidAds = ["TXTL","LREC"];
        var mastRotationEnabled = 0;
        var bucketSAEnabled = true;
        var w = window, D = w.DARLA,
            C = {"useYAC":0,"usePE":0,"servicePath":"https:\/\/www.yahoo.com\/sdarla\/php\/fc.php","xservicePath":"","beaconPath":"https:\/\/www.yahoo.com\/sdarla\/php\/b.php","renderPath":"","allowFiF":false,"srenderPath":"https:\/\/s.yimg.com\/rq\/darla\/2-9-4\/html\/r-sf.html","renderFile":"https:\/\/s.yimg.com\/rq\/darla\/2-9-4\/html\/r-sf.html","sfbrenderPath":"https:\/\/s.yimg.com\/rq\/darla\/2-9-4\/html\/r-sf.html","msgPath":"https:\/\/www.yahoo.com\/sdarla\/2-9-4\/html\/msg.html","cscPath":"https:\/\/s.yimg.com\/rq\/darla\/2-9-4\/html\/r-csc.html","root":"sdarla","edgeRoot":"http:\/\/l.yimg.com\/rq\/darla\/2-9-4","sedgeRoot":"https:\/\/s.yimg.com\/rq\/darla\/2-9-4","version":"2-9-4","tpbURI":"","hostFile":"https:\/\/s.yimg.com\/rq\/darla\/2-9-4\/js\/g-r-min.js","beaconsDisabled":true,"rotationTimingDisabled":true,"fdb_locale":"What don't you like about this ad?|It's offensive|Something else|Thank you for helping us improve your Yahoo experience|It's not relevant|It's distracting|I don't like this ad|Send|Done|Why do I see ads?|Learn more about your feedback.","positions":{"DEFAULT":{"supports":false},"FPAD":[],"LREC":{"w":300,"h":250},"MAST":[],"TXTL":{"w":120,"h":45}},"lang":"en-US"},
            _adPerfBeaconData,
            _pendingAds = [],
            _adLT = [];
        var safeframeOptinPositions = {"FPAD":true};
                window._navStart = function _navStart () {
            if (window.performance) {
                if (window.performance.clearMeasures) {
                    try {
                        window.performance.clearMeasures();
                    } catch (ex) {
                    }
                }
                if (window.performance.clearMarks) {
                    try {
                        window.performance.clearMarks();
                    } catch (ex) {
                    }
                }
                _perfMark('MODAL_OPEN');
            }
            window._pendingAds = [];
            window._perfBackfillAdBeacon = false;
            window._adPerfBeaconData = {positions:{}};
        };
        window._adCallStart = function _adCallStart () {
            window._perfMark('MODAL_AD_EVENT_START');
        };
        window._isModalOpen = function _isModalOpen () {
            return (document.getElementsByTagName('html')[0].className.match(/Reader-open/) && !document.getElementsByTagName('html')[0].className.match(/Reader-closed/));
        };
        window._perfMark = function _perfMark (name) {
            if (window.performance && window.performance.mark) {
                window.performance.mark(name);
            }
        };
        window._perfBackfillAd = function _perfBackfillAd() {
                for (var i in window._adPerfBeaconData.positions) {
                    if (window._adPerfBeaconData.positions.hasOwnProperty(i) && i.match(/LREC-/)) {
                        window._perfMark('PSTMSG_' + i);
                        window._adPerfBeaconData.positions[i].backfill = true;
                    } else {
                        window._adPerfBeaconData.positions[i].backfill = false;
                    }
                }
        };
        window._backfillVideoStart = function _backfillVideoStart() {
            window._perfMark('PSTMSG_VIDSTART');
        };
        window._perfModalFetchStart = function _perfModalFetchStart() {
            window._perfMark('MODAL_FETCH_START');
        };
        window._perfModalFetchEnd = function _perfModalFetchEnd() {
            window._perfMark('MODAL_FETCH_END');
        };
        window._backfillVideoPlaybackStart = function _backfillVideoPlaybackStart(isAd) {
            var modalStart,
                videoInit,
                rapidPerfData,
                rapidInstance;
            if (!window._perfBackfillAdBeacon && window._isModalOpen() &&
                window.performance && window.performance.getEntriesByName) {

                window._perfBackfillAdBeacon = true;
                window._perfMark('PSTMSG_PBSTART');
                modalStart = window.performance.getEntriesByName('MODAL_OPEN')[0].startTime;
                videoInit = Math.round(window.performance.getEntriesByName('PSTMSG_VIDSTART')[0].startTime - modalStart);
                playbackStart = Math.round(window.performance.getEntriesByName('PSTMSG_PBSTART')[0].startTime - modalStart);

                rapidInstance = (YMedia.Af && YMedia.Af.rapid && YMedia.Af.rapid.tracker) ? YMedia.Af.rapid.tracker : null;
                rapidPerfData = {
                    perf_usertime: {
                        utm: [{
                            name: 'brightroll_backfill_info',
                            site : window._adPerfBeaconData.property || 'fp',
                            lang: window.Af.context.lang,
                            region: window.Af.context.region,
                            device: window.Af.context.device,
                            rid: window.Af.context.rid,
                            bucket: '201',
                            videoInit: videoInit,
                            videoAd: isAd,
                            playbackStart: playbackStart
                }]}};
                if (rapidInstance) {
                    rapidInstance.beaconPerformanceData(rapidPerfData);
                }
            }
        };
        window._adRenderComplete = function _adRenderComplete() {
            if (window.performance && window.performance.getEntriesByName) {
                var modalStart = window.performance.getEntriesByName('MODAL_OPEN')[0];
                var modalFetchStart = window.performance.getEntriesByName('MODAL_FETCH_START');
                var modalFetchEnd = window.performance.getEntriesByName('MODAL_FETCH_END');
                var rapidCustomData = [];
                var darlaFetchPerf= {};
                var rapidInstance = (YMedia.Af && YMedia.Af.rapid && YMedia.Af.rapid.tracker) ? YMedia.Af.rapid.tracker : null;
                var modalStartTime = modalStart.startTime;

                darlaFetchPerf['name'] = 'darla_info';
                darlaFetchPerf['spaceid'] = window._adPerfBeaconData.spaceId;
                darlaFetchPerf['pvid'] = window._adPerfBeaconData.pvid;
                darlaFetchPerf['site'] = window._adPerfBeaconData.property || 'fp';
                darlaFetchPerf['lang'] = window.Af.context.lang;
                darlaFetchPerf['region'] =  window.Af.context.region;
                darlaFetchPerf['device'] =  window.Af.context.device;
                darlaFetchPerf['rid'] =  window.Af.context.rid;
                darlaFetchPerf['bucket'] = '201';

                if (modalFetchStart && modalFetchStart.length > 0) {
                    darlaFetchPerf['modalFetchStart'] =  Math.round(modalFetchStart[0].startTime -modalStartTime);
                    darlaFetchPerf['modalFetchEnd'] =  Math.round(modalFetchEnd[0].startTime -modalStartTime);
                }
                darlaFetchPerf['eventStart'] =  Math.round(window.performance.getEntriesByName('MODAL_AD_EVENT_START')[0].startTime -modalStartTime);
                darlaFetchPerf['wsStart'] =  Math.round(window.performance.getEntriesByName('MODAL_AD_WS_START')[0].startTime - modalStartTime);
                darlaFetchPerf['wsEnd'] =  Math.round(window.performance.getEntriesByName('MODAL_AD_WS_END')[0].startTime - modalStartTime);
                rapidCustomData.push(darlaFetchPerf);

                for (var i in window._adPerfBeaconData.positions) {
                    var darlaPositionPerf = {};
                    var posInfo = window._adPerfBeaconData.positions.hasOwnProperty(i) && window._adPerfBeaconData.positions[i];
                    if(posInfo) {
                        darlaPositionPerf['name'] = i + '_info';
                        darlaPositionPerf['valid'] = posInfo.valid ? 1 : 0;
                        if (posInfo.valid) {
                            darlaPositionPerf['renderStart'] =  Math.round(window.performance.getEntriesByName('ADSTART_' + i)[0].startTime - modalStartTime);
                            darlaPositionPerf['renderEnd'] =  Math.round(window.performance.getEntriesByName('ADEND_' + i)[0].startTime - modalStartTime);
                            darlaPositionPerf['bookId'] = posInfo.bookId;
                            darlaPositionPerf['creativeId'] = posInfo.creativeId;
                            darlaPositionPerf['placementId'] = posInfo.placementId;
                            darlaPositionPerf['size'] = posInfo.size;
                            darlaPositionPerf['backfill'] = posInfo.backfill ? 1 : 0;
                            if (posInfo.backfill) {
                                darlaPositionPerf['pstmsg'] = Math.round(window.performance.getEntriesByName('PSTMSG_' + i)[0].startTime - modalStartTime);
                            }
                        }
                        rapidCustomData.push(darlaPositionPerf);
                    }
                }

                if (rapidInstance) {
                    var customPerfData = {'utm': rapidCustomData};
                    var rapidPerfData = {perf_usertime: customPerfData};
                    rapidInstance.beaconPerformanceData(rapidPerfData);
                }
            }
        };
        if (D && C) {
            C.positions = {"FPAD":{"clean":"my-adsFPAD","dest":"my-adsFPAD-iframe","metaSize":1,"fdb":"true,","supports":{"resize-to":1,"exp-ovr":1,"exp-push":1,"lyr":1}},"LREC":{"pos":"LREC","clean":"my-adsLREC","dest":"my-adsLREC-iframe","metaSize":true,"w":300,"h":250,"fdb":true,"supports":{"exp-ovr":0,"lyr":0}},"MAST":{"pos":"MAST","clean":"my-adsMAST","dest":"my-adsMAST-iframe","fr":"expIfr_exp","rmxp":0,"metaSize":true,"w":970,"h":250,"supports":{"exp-ovr":1,"exp-push":1,"resize-to":1},"closeBtn":{"mode":2,"useShow":1}},"TXTL":{"pos":"TXTL","id":"TXTL","clean":"my-adsTXTL","dest":"my-adsTXTL-iframe","w":120,"h":170,"fr":"expIfr_exp","rmxp":0,"css":".ad-tl2b {overflow:hidden; text-align:left;} p {margin:0px;} a {color:#020e65;} a:hover {color:#0078ff;} .y-fp-pg-controls {margin-top:5px; margin-bottom:5px;} #tl1_slug {font-family:'Helvetica Neue',Helvetica,Arial,sans-serif; font-size:11px; color:#999;} #fc_align a {font-family:'Helvetica Neue',Helvetica,Arial,sans-serif; font-size:11px;} a:link {text-decoration:none;}"}};
            C.k2={"res":{"rate":100,"pos":["LREC","MAST","FPAD","LREC2","LREC3","LREC-0","LREC2-0","LREC3-0","MAST-0","LDRB-0","SPL2-0","SPL-0","MON-0"]}};
            C.k2E2ERate=100;
C.k2Rate=100;
C.so=1;

            C.events = null;
            
                    C.onStartRequest = function(action_event_name) {
            if (window._isModalOpen && window._isModalOpen()) {
                window._perfMark('MODAL_AD_WS_START');
            }
        };
                    C.onFinishRequest = function(action_event_name) {
            if (window._isModalOpen && window._isModalOpen()) {
                window._perfMark('MODAL_AD_WS_END');
            }
        };
                        C.onFinishParse = function(eventName, result) {
                var ps = result.ps(),
                    modalOpen = false,
                    position, posItem, curAd, curEvt,
                    validPositions = {},
                    isMONFetch = false;
                            if (window._isModalOpen && window._isModalOpen()) {
                window._perfMark('AD_PARSE');
                modalOpen = true;
                window._adPerfBeaconData.spaceId = result.spaceID;
                window._adPerfBeaconData.pvid = result.pvid;
                curEvt = DARLA.evtSettings(eventName);
                if (curEvt.property) {
                    window._adPerfBeaconData.property = curEvt.property;
                }
            }
                        function fireBeacon(position, size, pageMode) {
            if (position && size && pageMode) {
                try {
                    var img = new Image();
                    img.src = '/p.gif?beaconType=darla&pos=' + position + '&size=' + size + '&mode=' +pageMode + '&bucket=' + 201 + '&rid=' + "dp4utbtbbmtc2";
                } catch (e) {
                    if (console) {
                        console.log('failed to send darla beacon :', e);
                    }
                }
            }
        }

                if (ps && ps.length) {
                            if (eventName === 'hlAdsAll' || eventName === 'hlAdsCustom') {
            for (i = 0, l = ps.length; i < l; i++) {
                position = ps[i];
                posItem = result.item(position);
                if (posItem.hasErr || posItem.size + '' === '1x1' || (posItem.meta && posItem.meta.y && posItem.meta.y.size && posItem.meta.y.size + ''=== '1x1')) {
                    validPositions[position] = false;
                    if (position.indexOf("MON") > -1) {
                        isMONFetch = false;
                    }
                } else {
                    if (position.indexOf("MON") > -1) {
                        isMONFetch = true;
                    }
                    validPositions[position] = true;
                }
            }
            if (YMedia && YMedia.Af && YMedia.Af.Event && YMedia.Af.Event.fire) {
                YMedia.Af.Event.fire('sidekickrefresh', validPositions);
            }
        }

                    for (i = 0, l = ps.length; i < l; i++) {
                        position = ps[i];
                        posItem = result.item(position);
                                    if (modalOpen) {
                if (posItem.err || posItem.hasErr) {
                    window._adPerfBeaconData.positions[position] = {error: true};
                } else if (posItem.size + '' === '1x1' || (posItem.meta && posItem.meta.y && posItem.meta.y.size && posItem.meta.y.size + ''=== '1x1')) {
                    window._adPerfBeaconData.positions[position] = {size: '1x1'};
                } else {
                    window._adPerfBeaconData.positions[position] = {valid: true};
                    if (posItem.meta && posItem.meta.y) {
                        window._adPerfBeaconData.positions[position].size = posItem.meta.y.size;
                        window._adPerfBeaconData.positions[position].bookId = posItem.meta.y.bookID;
                        window._adPerfBeaconData.positions[position].creativeId = posItem.meta.y.creativeID;
                        window._adPerfBeaconData.positions[position].placementId = posItem.meta.y.placementID;
                    }
                    window._pendingAds.push(position);
                }
            }
                        if (posItem && posItem.conf && posItem.conf.clean) {
                            curAd = document.getElementById(posItem.conf.clean);
                            if (curAd) {
                                            if (eventName === 'hlAdsAll' || eventName === 'hlAdsCustom') {
                var posName = position.split('-')[0];
                if ((posName === "LDRB" || posName === "MAST" || posName === "SPL" || posName === "SPL2") &&
                   (!posItem.hasErr && posItem.size + '' !== '1x1')) {
                    curAd.className = curAd.className.replace('D-n', 'D-ib');
                    if (posName !== "SPL" && posName !== "SPL2") {
                        curAd.className = curAd.className + " Bdb-Grey-1";
                    }
                }
            }
            if (eventName === 'hlAdsAll' || eventName === 'hlAdsCustom') {
                if (position.match(/^SPL/)) {
                    if (posItem.hasErr || posItem.size + '' === '1x1') {
                        if (YMedia && YMedia.one('#' + posItem.conf.clean + '-wrap')) {
                            YMedia.one(curAd).setStyle('padding-top', '0px');
                        }
                    } else if (YMedia && YMedia.one('#' + posItem.conf.clean + '-wrap')) {
                        var splashWrap = YMedia.one('#' + posItem.conf.clean + '-wrap');
                        splashWrap.setStyle('padding-top', (5/12*100)+'%');
                        splashWrap.setStyle('width', '100%');
                        splashWrap.setStyle('height', '0px');
                        splashWrap.addClass('Mt-20 Mb-50');
                    }
                }
            }
            if ((eventName === 'hlAdsAll' || eventName === 'hlAdsCustom') && position.indexOf("LREC") > -1) {
              if (posItem && posItem.conf && posItem.conf.clean) {
                var fallbackDiv = document.getElementById(posItem.conf.clean + "-fallback");
                }
                if ((posItem.hasErr || posItem.size + '' === '1x1' || (posItem.meta && posItem.meta.y && posItem.meta.y.size && posItem.meta.y.size+'' === '1x1'))  && (!isMONFetch || position !== 'LREC-0')) {
                    var lrecBackfillString = "<a href=\"https:\/\/bs.serving-sys.com\/BurstingPipe\/adServer.bs?cn=tf&c=20&mc=click&pli=14816902&PluID=0&ord=${CACHEBUSTER}\" style=\"background:url(https:\/\/www.yahoo.com\/sy\/dh\/ap\/default\/160201\/sports-dailyfantasy-v2-a966da5__1_.jpg) 0 0 no-repeat;height:height:250px;width:300px;display:block;\"><\/a>";
                    if (fallbackDiv) {
                       fallbackDiv.innerHTML = lrecBackfillString;
                       fallbackDiv.className = fallbackDiv.className.replace('D-n', '');
                    }
                    curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-before-loading/, '');
                    curAd.className = curAd.className.replace(/hl-ad-LREC/, '');
                } else {
                  if (fallbackDiv && fallbackDiv.className.indexOf("D-n") < 0) {
                      fallbackDiv.className += ' D-n';
                    }
                    if (isMONFetch && position === 'LREC-0') {
                        curAd.parentElement.style.height = '600px';
                        var curAd = document.getElementById(posItem.conf.clean);
                        if (curAd.className.indexOf("D-n") < 0  && curAd.className.indexOf("D-ib") >= 0) {
                          curAd.className = curAd.className.replace(/D-ib/, 'D-n');
                        }
                    }
                }
            }
            if (eventName === 'prefetch' && position.indexOf("LREC") > -1) {
                curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-bgcolor/, '');
            }
            if ((eventName === 'fetch_selective_ad_lrec2' && position === 'LREC2') || (eventName === 'fetch_selective_ad_lrec3' && position === 'LREC3')) {
                var fallbackDiv = document.getElementById("my-adsLREC2-fallback");
                if (posItem.hasErr || (posItem.meta && posItem.meta.y && posItem.meta.y.size && posItem.meta.y.size+'' === '1x1')) {
                    curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-bgcolor/, '');
                    curAd.className = curAd.className.replace('Ht-250', '');
                    // hide div if ad fetch failed or is 1x1
                    if (curAd.className.indexOf("D-n") < 0) {
                        curAd.className += ' D-n';
                    }
                    fallbackDiv.className = fallbackDiv.className.replace('D-n', '');
                } else {
                    if (fallbackDiv.className.indexOf("D-n") < 0) {
                        fallbackDiv.className += ' D-n';
                    }
                    curAd.className = curAd.className.replace('D-n', '');
                }

                if (position === 'LREC3') {
                    var lrec2Div = document.getElementById("my-adsLREC2");
                    if (lrec2Div && lrec2Div.className.indexOf("D-n") < 0) {
                        lrec2Div.className += ' D-n';
                    }
                }
            }


                            }
                        }
                    }
                }
                            if (modalOpen && window._pendingAds.length === 0) {
                window._adRenderComplete();
            }
            };

                        C.onStartPosRender = function(posItem) {
                if (window.performance  && window.performance.now) {
                    var ltime = window.performance.now(),
                        posId = posItem && posItem.pos;
                    _adLT.push(['ADSTART_'+posId, Math.round(ltime)]);
                }
                if (window._isModalOpen && window._isModalOpen()) {
                    window._perfMark('ADSTART_' + posId);
                }
            };

                        C.onBeforeStartPosRender = function(posItem) {
                if (posItem && safeframeOptinPositions && safeframeOptinPositions[posItem.pos]) {
                    if (posItem.html && posItem.html.match(/<!--[^>]*sfoptout[^>]*-->/)) {
                        return true;
                    }
                }
            };

                        C.onFinishPosRender = function(posId, reqList, posItem) {
                var curAd = document.getElementById("my-ads"+posId),
                    adIndex,
                    posSize = (posItem && posItem.meta && posItem.meta.value("size", "y"));

                // Get clean div for ad position in case defined
                if (posItem && posItem.conf && posItem.conf.clean) {
                    curAd = document.getElementById(posItem.conf.clean);
                }
                if (curAd) {
                    // Let ad take its original size, remove default height given to ad div
                    curAd.className = curAd.className.replace('Ht-250', '');
                    curAd.className = curAd.className.replace('Ht-MAST', '');

                    if(posSize && posSize != "1x1") {
                        curAd.className = curAd.className.replace('D-n', '');
                        curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-bgcolor/, '');
                        curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-before-loading/, '');
                    }
                }

                if (window.performance !== undefined && window.performance.now !== undefined) {
                    var whiteListedAds = {"LREC":"my-adsLREC-base","MAST":"my-adsMAST","TL1":"my-adsTL1","TXTL":"my-adsTXTL","LREC-0":"hl-ad-LREC-0","MON-0":"hl-ad-MON-0","MAST-0":"hl-ad-MAST-0","LDRB-0":"hl-ad-LDRB-0","SPL2-0":"hl-ad-SPL2-0","SPL-0":"hl-ad-SPL-0"},
                      ltime = window.performance.now();
                     _adLT.push(['ADEND_'+posId, Math.round(ltime)]);
                    setTimeout(function () {
                        if (window.YAFT !== undefined && window.YAFT.isInitialized() && whiteListedAds[posId]) {
                            // Trigger custom timing for LREC ad position
                            window.YAFT.triggerCustomTiming(whiteListedAds[posId], '', ltime);
                        }
                    },300);
                }
                if (window._isModalOpen && window._isModalOpen()) {
                    window._perfMark('ADEND_' + posId);
                    adIndex = window._pendingAds.indexOf(posId);
                    if (adIndex >= 0) {
                        window._pendingAds.splice(adIndex, 1);
                        if (window._pendingAds.length === 0) {
                            window._adRenderComplete();
                        }
                    }
                }
            };

            C.onBeforePosMsg = function(msg_name, position) {
        // Make these configurable for INTLS
        var maxWidth = 970,
            maxHeight = 600,
            newWidth,
            newHeight,
            origWidth,
            origHeight,
            pos;

        if('MAST' !== position) {
            return;
        }

        if (msg_name === 'resize-to') {
            newWidth = arguments[2];
            newHeight = arguments[3];
        }
        else if (msg_name === 'exp-push' || msg_name === 'exp-ovr') {
            pos = $sf.host.get('MAST'),
            origWidth = pos.conf.w;
            origHeight = pos.conf.h;
            //"exp-ovr" or "exp-push", position id, delta X, delta Y, push (true /false), top increase, left increase, right increase, bottom increase
            newWidth = origWidth + arguments[6] + arguments[7];
            newHeight = origHeight + arguments[5] + arguments[8];
        }
        if(newWidth > maxWidth || newHeight > maxHeight) {
            return true;
        }
    };
                        //call back when the ad is expanded or collapsed
            C.onPosMsg = function (msg_name, data, msg_data)  {
                var visible;
                if(msg_name == "collapse" && data == "MAST") {
                    var bodyTag = document.getElementsByTagName("body")[0];
                    bodyTag.className = bodyTag.className.replace('mastAdExpanded', '');
                    bodyTag.className += " " +  "mastAdCollapsed";
                }
                if(msg_name == "exp-push" && data == "MAST") {
                    var bodyTag = document.getElementsByTagName("body")[0];
                    bodyTag.className = bodyTag.className.replace('mastAdCollapsed', '');
                    bodyTag.className += " " +  "mastAdExpanded";
                }

                /* generic ad expansion logic */
                if(msg_name == "collapse") {
                    var bodyTag = document.getElementsByTagName("body")[0];
                    bodyTag.className = bodyTag.className.replace(data + "-ad-expanded", '');
                }

                if(msg_name == "exp-ovr") {
                    var bodyTag = document.getElementsByTagName("body")[0];
                    bodyTag.className += " " + data + "-ad-expanded";
                }

                if (msg_name === 'geom-update') {
                    visible = D.render.RenderMgr.get(data).viewedAt > 0;
                    // geom-update event will always be available when Y is available
                    if (YMedia && visible) {
                        YMedia.Global.fire('ads:beacon', {id: data});
                    }
                }
                                if (msg_name === 'cmsg') {
                    var splashConf = DARLA.posSettings(data)
                    if (YMedia && splashConf && splashConf.clean) {
                        var splashNode = document.getElementById(splashConf.clean + '-wrap');
                        if (splashNode) {
                            if (msg_data === 'splash-expand') {
                                YMedia.one(splashNode).setStyle('padding-top', (9/16*100)+'%');
                                if (typeof splashNode.scrollIntoView === 'function') {
                                    splashNode.scrollIntoView();
                                }
                            } else if (msg_data === 'splash-collapse') {
                                YMedia.one(splashNode).setStyle('padding-top', (5/12*100)+'%');
                            }
                        }
                    }
                }
            };

            

            


            if ("OK" == D.config(C)) {
                setTimeout(function() {
                    if (window.performance  && window.performance.now) {
                        var ltime = window.performance.now();
                        _adLT.push(['DARLA_RSTART', Math.round(ltime)]);
                    }
                    var w = window,
                        d = document,
                        e = d.documentElement,
                        g = d.getElementsByTagName('body')[0],
                        winWidth = w.innerWidth || e.clientWidth || g.clientWidth;
                    if (true && winWidth < 1024 && window.pageloadValidAds && (window.pageloadValidAds.indexOf('TXTL') >= 0)) {
                        var prefetched = D.prefetched();
                        var txtlindex = prefetched.indexOf('TXTL');
                        if (txtlindex >= 0) {
                            delete(prefetched[txtlindex]);
                            for (var i=0; i < prefetched.length; i++) {
                                if (prefetched.hasOwnProperty(i)) {
                                    D.render(prefetched[i]);
                                }
                            }
                        } else {
                            D.render();
                        }
                    } else {
                        D.render();
                    }
                    if (window.performance  && window.performance.now) {
                        var ltime = window.performance.now();
                        _adLT.push(['DARLA_REND', Math.round(ltime)]);
                    }
                }, 2);
            }
        }
    </script>
                </div>
        
        

        
        
        <input type="hidden" id="afhistorystate">
    <!-- bottom -->
    
                    <script type="text/javascript" src="/sy/zz/combo?yui:/3.18.0/yui/yui-min.js&/ss/rapid-3.29.1.js&/os/mit/td/aperollup-min-163d3e68_desktop_advance.js"></script>
                    <script type="text/javascript" src="/sy/zz/combo?&&/os/mit/td/td-applet-stream-atomic-2.0.440/r-min.js&/os/mit/td/td-applet-mega-header-1.0.193/r-min.js&/os/mit/td/td-applet-viewer-0.1.2104/r-min.js&/os/mit/td/td-applet-navlinks-atomic-0.0.54/r-min.js" async defer></script>                                <script type="text/javascript">                YUI().use('node-base', function(Y){
                    Y.on("domready", function(e) {
                        YUI.Env.core.push.apply(YUI.Env.core,["loader-angus","loader-ape-af","loader-ape-applet","loader-ape-location","loader-ape-pipe","loader-ape-social","loader-applet-server","loader-assembler","loader-dust-helpers","loader-finance-streamer","loader-highlander-client","loader-live-event-data","loader-mjata","loader-stencil","loader-td-api","loader-td-dev-info","loader-td-lib-entertainment","loader-td-lib-hovercards","loader-td-lib-livevideo","loader-td-lib-social","loader-td-applet-stream-atomic","loader-td-applet-mega-header","loader-td-applet-viewer","loader-td-applet-navlinks-atomic","loader-td-applet-trending-atomic","loader-td-applet-sidekick","loader-td-applet-related-content","loader-td-applet-mega-comments","loader-td-applet-weather-atomic"]);
YUI.add("loader-ape-af",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-beacon":{group:"ape-af",requires:["json-stringify","querystring-stringify-simple","io-base"]},"af-bootstrap":{group:"ape-af",requires:["af-utils"]},"af-cache":{group:"ape-af",requires:["af-beacon","json","gallery-storage-lite"]},"af-compositeview":{group:"ape-af",requires:["parallel","af-utils"]},"af-comscore":{group:"ape-af",requires:["af-beacon","af-config","querystring-stringify-simple"]},"af-config":{group:"ape-af",requires:["af-utils","cookie"]},"af-content":{group:"ape-af",requires:["af-config","af-dom","af-utils","json-parse","mjata-rest-http","node-pluginhost","querystring-stringify"]},"af-cookie":{group:"ape-af",requires:["cookie","json"]},"af-dom":{group:"ape-af",requires:["node-base","node-core"]},"af-dwelltime":{group:"ape-af",requires:["af-rapid","json-stringify"]},"af-eu-tracking":{group:"ape-af",requires:["af-beacon","media-agof-tracking"]},"af-event":{group:"ape-af",requires:["event-custom"]},"af-history":{group:"ape-af",requires:["json"]},"af-message":{group:"ape-af",langBundles:["strings"],requires:["ape-af-templates-message","af-utils","node-base","selector-css3"]},"af-pageviz":{group:"ape-af",requires:["event-custom"]},"af-poll":{group:"ape-af",requires:["af-pageviz"]},"af-rapid":{group:"ape-af",requires:["af-bootstrap","af-utils","media-rapid-tracking","node-core"]},"af-sync":{group:"ape-af",requires:["af-transport","af-utils"]},"af-trans":{group:"ape-af",requires:["node-base","transition","af-utils"]},"af-transport":{group:"ape-af",requires:["af-beacon","af-config","af-utils","json-stringify","tdapi-remotestore"]},"af-utils":{group:"ape-af",requires:["cookie","intl","oop","querystring-parse-simple"]},"af-viewport-loader":{group:"ape-af",requires:["node","event-base"]},"ape-af-lang-strings":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_ar":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_ar-jo":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_de-de":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_el-gr":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-ae":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-au":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-ca":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-gb":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-ie":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-in":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-jo":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-my":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-nz":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-ph":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-sg":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-us":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-za":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-ar":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-cl":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-co":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-es":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-mx":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-pe":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-us":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-ve":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_fr-be":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_fr-ca":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_fr-fr":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_id-id":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_it-it":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_nl-be":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_nl-nl":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_pt-br":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_ro-ro":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_sv-se":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_vi-vn":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_zh-hant-hk":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_zh-hant-tw":{group:"ape-af",requires:["intl"]},"ape-af-templates-message":{group:"ape-af",requires:["template-base","dust"]},"gallery-storage-lite":{group:"ape-af",requires:["event-base","event-custom","event-custom-complex","json","node-base"]},"media-agof-tracking":{group:"ape-af"},"media-rapid-tracking":{group:"ape-af",requires:["event-custom","base","node"]},"media-rmp":{group:"ape-af",requires:["node","io-base","async-queue","get"]},"tdapi-remotestore":{group:"ape-af",requires:["mjata-store","mjata-rest-http","json"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-ape-applet",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-applet":{group:"ape-applet",requires:["af-applet-dom","af-applet-model","af-applet-headerview","af-bootstrap","af-compositeview","af-config","af-utils","array-extras","event-custom","node","stencil-tooltip"]},"af-applet-action":{group:"ape-applet",requires:["af-applet-dom","af-utils","node-event-delegate","node-base"]},"af-applet-dom":{group:"ape-applet",requires:["node-core","af-trans"]},"af-applet-editview":{group:"ape-applet",requires:["af-applet-view","ape-applet-templates-moreinfo","ape-applet-templates-remove"]},"af-applet-headerview":{group:"ape-applet",requires:["af-applet-view","stencil-selectbox"]},"af-applet-init":{group:"ape-applet",requires:["af-applet","af-beacon","af-bootstrap","af-config","af-utils","event-touch","oop"]},"af-applet-load":{group:"ape-applet",requires:["af-applet-dom","af-message","af-transport","ape-applet-templates-reload"]},"af-applet-mgr":{group:"ape-applet",requires:["af-applet-dom","af-beacon","af-content","af-transport","af-utils"]},"af-applet-model":{group:"ape-applet",langBundles:["strings"],requires:["af-bootstrap","af-config","af-sync","base-build","mjata-json","model"]},"af-applet-savesettings":{group:"ape-applet",requires:["af-dom","af-message"]},"af-applet-settingsview":{group:"ape-applet",requires:["af-applet-view","ape-applet-templates-settingswrap"]},"af-applet-view":{group:"ape-applet",requires:["af-bootstrap","af-utils","base-build","dust","view"]},"af-applet-viewmgr":{group:"ape-applet",requires:["af-applet-dom"]},"af-applets":{group:"ape-applet",requires:["af-applet-action","af-applet-init","af-applet-load","af-applet-mgr","af-applet-savesettings","af-applet-viewmgr","af-dom","af-utils"]},"ape-applet-lang-strings":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_ar":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_ar-jo":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_de-de":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_el-gr":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-ae":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-au":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-ca":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-gb":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-ie":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-in":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-jo":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-my":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-nz":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-ph":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-sg":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-us":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-za":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-ar":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-cl":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-co":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-es":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-mx":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-pe":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-us":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-ve":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_fr-be":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_fr-ca":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_fr-fr":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_id-id":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_it-it":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_nl-be":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_nl-nl":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_pt-br":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_ro-ro":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_sv-se":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_vi-vn":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_zh-hant-hk":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_zh-hant-tw":{group:"ape-applet",requires:["intl"]},"ape-applet-templates-moreinfo":{group:"ape-applet",requires:["template-base","dust"]},"ape-applet-templates-reload":{group:"ape-applet",requires:["template-base","dust"]},"ape-applet-templates-remove":{group:"ape-applet",requires:["template-base","dust"]},"ape-applet-templates-settingswrap":{group:"ape-applet",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-ape-location",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-location-detection":{group:"ape-location",langBundles:["strings"],requires:["view","mjata-model-store","af-config","af-message","af-locations","af-utils","ape-location-templates-location-detection"]},"af-location-panel":{group:"ape-location",langBundles:["strings"],requires:["view","mjata-model-store","af-config","af-message","af-locations","af-utils","ape-location-templates-location-list","ape-location-templates-location-panel","stencil-toggle","stencil-tooltip"]},"af-locations":{group:"ape-location",requires:["af-sync","af-utils","mjata-model-base","mjata-model","mjata-lazy-modellist","mjata-model-store"]},"ape-location-lang-strings":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_ar":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_ar-jo":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_de-de":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_el-gr":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-ae":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-au":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-ca":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-gb":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-ie":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-in":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-jo":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-my":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-nz":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-ph":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-sg":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-us":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-za":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-ar":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-cl":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-co":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-es":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-mx":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-pe":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-us":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-ve":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_fr-be":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_fr-ca":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_fr-fr":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_id-id":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_it-it":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_nl-be":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_nl-nl":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_pt-br":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_ro-ro":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_sv-se":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_vi-vn":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_zh-hant-hk":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_zh-hant-tw":{group:"ape-location",requires:["intl"]},"ape-location-templates-location-detection":{group:"ape-location",requires:["template-base","dust"]},"ape-location-templates-location-list":{group:"ape-location",requires:["template-base","dust"]},"ape-location-templates-location-panel":{group:"ape-location",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-ape-pipe",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-comet":{group:"ape-pipe",requires:["af-beacon","af-config","comet","event-custom-base","json-parse"]},"af-pipe":{group:"ape-pipe",requires:["af-beacon","af-comet","af-config","af-utils","event-custom-base"]},comet:{group:"ape-pipe"}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-ape-social",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-social":{group:"ape-social",requires:[]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-dust-helpers",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{dust:{group:"dust-helpers",requires:["stencil-imageloader","moment","intl-helper"]},"dust-helper-intl":{es:!0,group:"dust-helpers",requires:["intl-messageformat"]},"intl-helper":{group:"dust-helpers",requires:["dust-helper-intl"]},"intl-messageformat":{es:!0,group:"dust-helpers"},moment:{group:"dust-helpers"}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-mjata",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"mjata-bind-model2dom":{group:"mjata",requires:["mjata-model-store","mjata-template","mjata-util","node-base","node-core"]},"mjata-binder":{group:"mjata",requires:["mjata-bind-model2dom"]},"mjata-json":{group:"mjata"},"mjata-lazy-modellist":{group:"mjata",requires:["lazy-model-list","mjata-model-store"]},"mjata-model":{group:"mjata",requires:["model","mjata-json","mjata-model-store"]},"mjata-model-base":{group:"mjata",requires:["base","mjata-model","mjata-modellist","mjata-model-store"]},"mjata-model-store":{group:"mjata",requires:["event-custom","promise"]},"mjata-modellist":{group:"mjata",requires:["model-list","mjata-model-store"]},"mjata-queue":{group:"mjata"},"mjata-rest-http":{group:"mjata",requires:["json-stringify","io-base","io-xdr"]},"mjata-store":{group:"mjata",requires:["mjata-queue","mjata-util"]},"mjata-template":{group:"mjata",requires:["dust"]},"mjata-util":{group:"mjata"}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-stencil",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{hammer:{group:"stencil"},stencil:{group:"stencil"},"stencil-base":{group:"stencil",requires:["stencil","yui-base","event-base","event-delegate","event-mouseenter","json-parse","dom-base","node-base","node-pluginhost","selector"]},"stencil-bquery":{group:"stencil",requires:["stencil-base","yui-base","dom-base","dom-style","node-base","node-pluginhost"]},"stencil-carousel":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","node-style","node-pluginhost","event-hover"]},"stencil-fx":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","pluginhost","event-custom","selector-css3"]},"stencil-fx-collapse":{group:"stencil",requires:["stencil-fx","node-style"]},"stencil-fx-fade":{group:"stencil",requires:["stencil-toggle","stencil-fx"]},"stencil-fx-flip":{group:"stencil",requires:["stencil-toggle","stencil-fx"]},"stencil-gallery":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","node-style","node-pluginhost","transition"]},"stencil-imageloader":{group:"stencil",requires:["stencil-base","yui-base","dom-base","dom-screen","node-base","node-screen","node-style","node-pluginhost","array-extras"]},"stencil-lightbox":{group:"stencil",requires:["stencil-base","yui-base","dom-base","dom-screen","node-base","node-pluginhost","shim-plugin"]},"stencil-scrollview":{group:"stencil",requires:["node-base","node-style","hammer"]},"stencil-selectbox":{group:"stencil",requires:["stencil-base"]},"stencil-slider":{group:"stencil",requires:["stencil"]},"stencil-source":{group:"stencil",requires:["node","oop","pluginhost","mjata-util"]},"stencil-source-af":{group:"stencil",requires:["node","stencil-source","mjata-model-store","mjata-bind-model2dom","af-applets"]},"stencil-source-dom":{group:"stencil",requires:["stencil-source"]},"stencil-source-rmp":{group:"stencil",requires:["stencil-source","stencil-source-url","media-rmp"]},"stencil-source-simple":{group:"stencil",requires:["stencil-source"]},"stencil-source-url":{group:"stencil",requires:["stencil-source","io-base","io-xdr"]},"stencil-sticker":{group:"stencil",requires:["node-base","node-style","node-screen"]},"stencil-tabpanel":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","node-pluginhost"]},"stencil-toggle":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","node-style","node-pluginhost","event-focus","event-mouseenter","json-parse"]},"stencil-tooltip":{group:"stencil",requires:["stencil-base","yui-base","dom-base","dom-screen","node-base","node-pluginhost","node-style","stencil-source"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-mega-comments",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-mega-comments-mainview":{group:"td-applet-mega-comments",requires:["af-applet-view","af-transport","af-utils","af-rapid","stencil-tooltip","td-applet-mega-comments-templates-styles"]},"td-applet-mega-comments-templates-avatar":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-comments":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-avatar","td-applet-mega-comments-templates-comments-body"]},"td-applet-mega-comments-templates-comments-body":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-comments-editor-tools"]},"td-applet-mega-comments-templates-comments-editor-tools":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-form-abuse":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-form-comment":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-form-delete":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-form-reply":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-main":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-form-comment","td-applet-mega-comments-templates-form-reply","td-applet-mega-comments-templates-form-abuse","td-applet-mega-comments-templates-form-delete"]},"td-applet-mega-comments-templates-replies":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-avatar","td-applet-mega-comments-templates-replies-body"]},"td-applet-mega-comments-templates-replies-body":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-styles":{group:"td-applet-mega-comments",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-mega-header",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"mega-header-plugin-account-switch":{group:"td-applet-mega-header",requires:["node","event-custom","event-move","event-mouseenter","td-applet-mega-header-constants","get","anim","json-parse","escape"]},"mega-header-plugin-autocomplete":{group:"td-applet-mega-header",requires:["node","autocomplete","autocomplete-highlighters","autocomplete-list","td-applet-mega-header-constants"]},"mega-header-plugin-avatar":{group:"td-applet-mega-header",requires:["jsonp","td-applet-mega-header-constants"]},"mega-header-plugin-instant":{group:"td-applet-mega-header",requires:["node","event","af-event","json","jsonp","querystring","td-applet-mega-header-constants"]},"mega-header-plugin-mailpreview":{group:"td-applet-mega-header",requires:["jsonp","af-event","td-applet-mega-header-constants"]},"mega-header-plugin-notifications":{group:"td-applet-mega-header",requires:["af-event","af-cache","td-applet-mega-header-constants"]},"mega-header-plugin-username":{group:"td-applet-mega-header",requires:["jsonp"]},"td-applet-mega-header-constants":{group:"td-applet-mega-header"},"td-applet-mega-header-mainview":{group:"td-applet-mega-header",requires:["af-applet-view","node","td-applet-mega-header-constants","mega-header-plugin-autocomplete","mega-header-plugin-avatar","mega-header-plugin-username","mega-header-plugin-mailpreview","mega-header-plugin-notifications","mega-header-plugin-instant","mega-header-plugin-account-switch"]},"td-applet-mega-header-templates-accountSwitchPanel":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-autofocus_script":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-firefoxPromo_script":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-instant_filters":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-logo":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-mail":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-mailpreview":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-main":{group:"td-applet-mega-header",requires:["template-base","dust","td-applet-mega-header-templates-topbar","td-applet-mega-header-templates-logo","td-applet-mega-header-templates-profile","td-applet-mega-header-templates-notifications","td-applet-mega-header-templates-mail","td-applet-mega-header-templates-search","td-applet-mega-header-templates-instant_filters"]},"td-applet-mega-header-templates-notificationpanel":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-notifications":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-profile":{group:"td-applet-mega-header",requires:["template-base","dust","td-applet-mega-header-templates-accountSwitchPanel","td-applet-mega-header-templates-profilePanel"]},"td-applet-mega-header-templates-profilePanel":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-search":{group:"td-applet-mega-header",requires:["template-base","dust","td-applet-mega-header-templates-autofocus_script"]},"td-applet-mega-header-templates-topbar":{group:"td-applet-mega-header",requires:["template-base","dust","td-applet-mega-header-templates-firefoxPromo_script"]},"td-mega-header-model":{group:"td-applet-mega-header",requires:["model","af-sync"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-navlinks-atomic",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-navlinks-atomic-templates-main":{group:"td-applet-navlinks-atomic",requires:["template-base","dust"]},"td-applet-navlinks-mainview":{group:"td-applet-navlinks-atomic",requires:["af-applet-view"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-related-content",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-related-content-model":{group:"td-applet-related-content",requires:["model","af-event"]},"td-applet-related-content-templates-main":{group:"td-applet-related-content",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-sidekick",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-sidekick-templates-item":{group:"td-applet-sidekick",requires:["template-base","dust"]},"td-applet-sidekick-templates-main":{group:"td-applet-sidekick",requires:["template-base","dust","td-applet-sidekick-templates-item"]},"td-sidekick-mainview":{group:"td-applet-sidekick",requires:["af-applet-view","af-config","af-event","stencil"]},"td-sidekick-model":{group:"td-applet-sidekick",requires:["af-sync","base-build","model"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-stream-atomic",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"stream-actiondrawer-v2":{group:"td-applet-stream-atomic",requires:["af-applet-view","af-cache","af-event","af-utils","event-outside","stencil-fx","stencil-fx-collapse"]},"stream-needtoknow-anim":{group:"td-applet-stream-atomic",requires:["af-applet-view","af-pageviz"]},"stream-onboard-v2":{group:"td-applet-stream-atomic",requires:["af-applet-view","stencil-fx","stencil-fx-collapse"]},"td-applet-comments-model-v2":{group:"td-applet-stream-atomic",requires:["mjata-model-base","af-sync"]},"td-applet-interest-model-v2":{group:"td-applet-stream-atomic",requires:["mjata-model-base","af-sync"]},"td-applet-stream-appletmodel-v2":{group:"td-applet-stream-atomic",requires:["mjata-model-base","af-cookie","af-event","af-utils","af-applet-model","af-beacon","af-poll","td-applet-stream-model-v2","td-applet-stream-items-model-v2"]},"td-applet-stream-atomic-templates-breaking_news":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-debug":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-drawer_action":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-drawer_desktop":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-drawer_flyout":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-drawer_share":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-errormsg":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-ad":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-ad_dislike"]},"td-applet-stream-atomic-templates-item-ad_dislike":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-ad_dislike_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-ad_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-ad_dislike_v2"]},"td-applet-stream-atomic-templates-item-default":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-followable"]},"td-applet-stream-atomic-templates-item-default_clusters":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-followable"]},"td-applet-stream-atomic-templates-item-default_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-followable","td-applet-stream-atomic-templates-item-storyline_images","td-applet-stream-atomic-templates-item-storyline"]},"td-applet-stream-atomic-templates-item-featured_ad":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-ad_dislike"]},"td-applet-stream-atomic-templates-item-featured_ad_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-ad_dislike_v2"]},"td-applet-stream-atomic-templates-item-featured_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-storyline"]},"td-applet-stream-atomic-templates-item-filmstrip":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-followable":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-gs_tile":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon"]},"td-applet-stream-atomic-templates-item-inline_video":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon"]},"td-applet-stream-atomic-templates-item-inline_video_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-storyline_images","td-applet-stream-atomic-templates-item-storyline"]},"td-applet-stream-atomic-templates-item-needtoknow_actions":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-play_icon":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-right_menu":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-right_menu_featured"]},"td-applet-stream-atomic-templates-item-right_menu_featured":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon"]},"td-applet-stream-atomic-templates-item-storyline":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-storyline_images":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-items":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-debug"]},"td-applet-stream-atomic-templates-main":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-breaking_news","td-applet-stream-atomic-templates-errormsg"]},"td-applet-stream-atomic-templates-related":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-removeditem":{group:"td-applet-stream-atomic",requires:["template-base"
,"dust"]},"td-applet-stream-baseview-v2":{group:"td-applet-stream-atomic",requires:["af-applet-view","node-scroll-info","af-beacon"]},"td-applet-stream-headerview-v2":{group:"td-applet-stream-atomic",requires:["af-applet-view"]},"td-applet-stream-items-model-v2":{group:"td-applet-stream-atomic",requires:["model","af-sync"]},"td-applet-stream-mainview-v2":{group:"td-applet-stream-atomic",requires:["af-beacon","af-cookie","af-cache","af-event","af-pipe","stencil-imageloader","td-applet-stream-baseview-v2"]},"td-applet-stream-model-v2":{group:"td-applet-stream-atomic",requires:["model","af-sync"]},"td-applet-stream-onboarding-model-v2":{group:"td-applet-stream-atomic",requires:["model","af-sync"]},"td-applet-stream-payoff-model-v2":{group:"td-applet-stream-atomic",requires:["model","af-sync"]},"td-applet-stream-related-model-atomic":{group:"td-applet-stream-atomic",requires:["model","af-sync"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-trending-atomic",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-trending-atomic-mainview":{group:"td-applet-trending-atomic",requires:["af-applet-view","node"]},"td-applet-trending-atomic-templates-main":{group:"td-applet-trending-atomic",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-viewer",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-ads-model":{group:"td-applet-viewer",requires:["af-sync","base-build","model","af-beacon"]},"td-applet-viewer-templates-ad_fdb_thank_you":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-ads_story":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-cards":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-debug","td-applet-viewer-templates-fallback","td-applet-viewer-templates-footer"]},"td-applet-viewer-templates-carousel":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-content_body":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-mag_slideshow","td-applet-viewer-templates-ads_story"]},"td-applet-viewer-templates-content_body_mega":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-mag_slideshow"]},"td-applet-viewer-templates-credit":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-debug":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-drawer_feedback":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-fallback":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-footer":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-header":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-share_btns_mega"]},"td-applet-viewer-templates-header_ads":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-lightbox":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-carousel"]},"td-applet-viewer-templates-lightbox_mega":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-carousel","td-applet-viewer-templates-thumbnail_items"]},"td-applet-viewer-templates-mag_slideshow":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-main":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-cards","td-applet-viewer-templates-lightbox"]},"td-applet-viewer-templates-modal_aside_mega":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-modal_lrecs_mega"]},"td-applet-viewer-templates-modal_lrecs_mega":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-reblog":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-share_btns":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-share_btns_mega":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-sidekicktv":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-slideshow":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-header","td-applet-viewer-templates-lightbox_mega","td-applet-viewer-templates-content_body_mega","td-applet-viewer-templates-modal_aside_mega","td-applet-viewer-templates-story_cover","td-applet-viewer-templates-content_body"]},"td-applet-viewer-templates-story":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-header","td-applet-viewer-templates-content_body_mega","td-applet-viewer-templates-modal_aside_mega","td-applet-viewer-templates-story_cover","td-applet-viewer-templates-content_body"]},"td-applet-viewer-templates-story_cover":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-header_ads","td-applet-viewer-templates-video","td-applet-viewer-templates-header"]},"td-applet-viewer-templates-thumbnail_items":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-video":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-header","td-applet-viewer-templates-content_body_mega","td-applet-viewer-templates-modal_aside_mega","td-applet-viewer-templates-header_ads","td-applet-viewer-templates-content_body"]},"td-viewer-ads":{group:"td-applet-viewer",requires:["af-beacon","af-config","af-event"]},"td-viewer-mainview":{group:"td-applet-viewer",requires:["af-applet-view","af-beacon","af-cache","af-config","af-event","stencil","angus-slider","event-tap","td-viewer-ads","td-viewer-slideshow"]},"td-viewer-model":{group:"td-applet-viewer",requires:["af-cache","af-sync","base-build","model","af-beacon"]},"td-viewer-slideshow":{group:"td-applet-viewer",requires:["stencil","angus-slider","event-tap"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-weather-atomic",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-weather-atomic-appletmodel":{group:"td-applet-weather-atomic",requires:["mjata-model-base","af-applet-model","mjata-model-store"]},"td-applet-weather-atomic-headerview":{group:"td-applet-weather-atomic",requires:["af-applet-view","mjata-model-store"]},"td-applet-weather-atomic-liteview":{group:"td-applet-weather-atomic",requires:["td-applet-weather-atomic-mainview"]},"td-applet-weather-atomic-mainview":{group:"td-applet-weather-atomic",requires:["af-applet-view","af-message","stencil-fx","stencil-fx-collapse"]},"td-applet-weather-atomic-model":{group:"td-applet-weather-atomic",requires:["model","af-sync"]},"td-applet-weather-atomic-templates-header":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-header.chiclet":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-header.error.chiclet":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-list-lite":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.chiclet":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.error.chiclet":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.error.lite":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.hovercard":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.lite":{group:"td-applet-weather-atomic",requires:["template-base","dust","td-applet-weather-atomic-templates-list-lite"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-dev-info",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-dev-info":{group:"td-dev-info",requires:["overlay","node-core","td-dev-info-templates-perf"]},"td-dev-info-templates-init":{group:"td-dev-info",requires:["template-base","dust"]},"td-dev-info-templates-perf":{group:"td-dev-info",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-lib-social",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"social-sharing-lib":{group:"td-lib-social",requires:["base","node-base","event-base","event-custom","stencil-lightbox","stencil-tooltip"]},"td-lib-social-templates-mtf":{group:"td-lib-social",requires:["template-base","dust","td-lib-social-templates-mtf_styles"]},"td-lib-social-templates-mtf_styles":{group:"td-lib-social",requires:["template-base","dust"]},"td-social-email-autocomplete":{group:"td-lib-social",requires:["autocomplete","autocomplete-highlighters","io","json","lang","node-base","node-core","yql"]},"td-social-mtf-popup":{group:"td-lib-social",requires:["base","event-base","io","autosuggest-standalone-loader","autosuggest-compose-utils","td-social-email-autocomplete","node-base","gallery-node-tokeninput"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.applyConfig({"groups":{"ape-af":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-af-0.0.326/","root":"os/mit/td/ape-af-0.0.326/"}}});
YUI.applyConfig({"groups":{"ape-applet":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-applet-0.0.207/","root":"os/mit/td/ape-applet-0.0.207/"}}});
YUI.applyConfig({"groups":{"ape-location":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-location-1.0.9/","root":"os/mit/td/ape-location-1.0.9/"}}});
YUI.applyConfig({"groups":{"ape-pipe":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-pipe-0.0.64/","root":"os/mit/td/ape-pipe-0.0.64/"}}});
YUI.applyConfig({"groups":{"ape-social":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-social-0.0.5/","root":"os/mit/td/ape-social-0.0.5/"}}});
YUI.applyConfig({"groups":{"dust-helpers":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/dust-helpers-0.0.144/","root":"os/mit/td/dust-helpers-0.0.144/"}}});
YUI.applyConfig({"groups":{"mjata":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/mjata-0.4.35/","root":"os/mit/td/mjata-0.4.35/"}}});
YUI.applyConfig({"groups":{"stencil":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/stencil-3.1.0/","root":"os/mit/td/stencil-3.1.0/"}}});
YUI.applyConfig({"groups":{"td-dev-info":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/td-dev-info-0.0.30/","root":"os/mit/td/td-dev-info-0.0.30/"}}});
YUI.applyConfig({"groups":{"td-lib-social":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/td-lib-social-0.1.207/","root":"os/mit/td/td-lib-social-0.1.207/"}}});
YUI.applyConfig({"modules":{"IntlPolyfill":{"fullpath":"https:\u002F\u002Fs.yimg.com\u002Fzz\u002Fcombo?yui:platform\u002Fintl\u002F0.1.4\u002FIntl.min.js&yui:platform\u002Fintl\u002F0.1.4\u002Flocale-data\u002Fjsonp\u002F{lang}.js","condition":{"name":"IntlPolyfill","trigger":"intl-messageformat","test":function (Y) {
                        return !Y.config.global.Intl;
                    },"when":"before"},"configFn":function (mod) {
                    var lang = 'en-US';
                    if (window.YUI_config && window.YUI_config.lang && window.IntlAvailableLangs && window.IntlAvailableLangs[window.YUI_config.lang]) {
                        lang = window.YUI_config.lang;
                    }
                    mod.fullpath = mod.fullpath.replace('{lang}', lang);
                    return true;
                }}}});
                    });
                });                </script>

<script type="text/javascript">
                YUI().use('node-base', function(Y){
                    Y.on("domready", function(e) {
                                        
                        (function(root) {
            root.YUI_config = root.YUI_config || {};
            root.YUI_config.lang = 'en-US';
        }(this));
            var YMedia = YUI({
                
                bootstrap: true,
                lang: 'en-US',
                comboBase: 'https://s.yimg.com/zz/combo?',
                comboSep: '&',
                root: 'yui:' + YUI.version + '/',
                filter: 'min',
                combine: true,
                maxURLLength: 2000,
                groups: {
                    arcade : {
                        base: 'https://s.yimg.com/nn/',
                        combine: true,
                        comboSep: '&',
                        comboBase: 'https://s.yimg.com/zz/combo?',
                        root: '',
                        modules: {
                                                'type_appscontainer_smartphone': {
                        'requires': ['node','node-event-delegate','anim','transition','event-move','stencil','stencil-scrollview'],
                        'path': '/nn/lib/metro/g/appscontainer/appscontainer_smartphone_0.0.18.js'
                    },
                    'type_customizedbutton': {
                        'requires': ['node','event'],
                        'path': '/nn/lib/metro/g/customizedbutton/customizedbutton_0.0.9.js'
                    },
                    'type_events_js': {
                        'requires': ['af-applet-contentmodel','af-applet-contentsettingsview'],
                        'path': '/nn/lib/metro/g/events/events_0.0.3.js'
                    },
                    'type_geminiads': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/geminiads/geminiads_0.0.4.js'
                    },
                    'type_myy': {
                        'requires': ['stencil-toggle','stencil','stencil-sticker','stencil-lightbox','stencil-bquery','af-applets','app','af-rapid','af-eu-tracking'],
                        'path': '/nn/lib/metro/g/myy/myy_0.0.41.js'
                    },
                    'type_myy_stub_rapid': {
                        'requires': ['stencil-toggle','stencil','stencil-sticker','stencil-lightbox','stencil-bquery','af-applets','app'],
                        'path': '/nn/lib/metro/g/myy/myy_stub_rapid_0.0.4.js'
                    },
                    'type_myy_mobile': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/myy_mobile_0.0.9.js'
                    },
                    'type_myy_viewer': {
                        'requires': ['highlander-client'],
                        'path': '/nn/lib/metro/g/myy/myy_viewer_0.0.10.js'
                    },
                    'type_advance': {
                        'requires': ['node'],
                        'path': '/nn/lib/metro/g/myy/advance_0.0.4.js'
                    },
                    'type_video_manager': {
                        'requires': ['af-content','af-event','base','event-synthetic','node-core','node-scroll-info'],
                        'path': '/nn/lib/metro/g/myy/video_manager_0.0.127.js'
                    },
                    'type_video_stage': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/video_stage_0.0.8.js'
                    },
                    'type_yahoodotcom_client': {
                        'requires': ['node'],
                        'path': '/nn/lib/metro/g/myy/yahoodotcom_client_0.0.15.js'
                    },
                    'type_myy_scroller': {
                        'requires': ['node'],
                        'path': '/nn/lib/metro/g/myy/myy_scroller_0.0.8.js'
                    },
                    'type_rapidworker_1_1': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/rapidworker_1_1_0.0.4.js'
                    },
                    'type_rapidworker_1_2': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/rapidworker_1_2_0.0.3.js'
                    },
                    'type_featurecue': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/featurecue_0.0.14.js'
                    },
                    'type_fp': {
                        'requires': ['stencil-toggle','stencil','stencil-sticker','stencil-lightbox','stencil-bquery','af-applets','app','af-rapid'],
                        'path': '/nn/lib/metro/g/myy/myy_0.0.41.js'
                    },
                    'type_addtomy': {
                        'requires': ['node','event','io','panel','io-base','transition','json-stringify'],
                        'path': '/nn/lib/metro/g/myy/addtomy_0.0.21.js'
                    },
                    'af-applet-basemodel': {
                        'requires': ['af-config','af-sync','af-utils','model'],
                        'path': '/nn/lib/metro/g/myy/af_applet_basemodel_0.0.3.js'
                    },
                    'af-applet-contentmodel': {
                        'requires': ['af-applet-basemodel'],
                        'path': '/nn/lib/metro/g/myy/af_applet_contentmodel_0.0.3.js'
                    },
                    'af-applet-baseview': {
                        'requires': ['af-dom','view'],
                        'path': '/nn/lib/metro/g/myy/af_applet_baseview_0.0.3.js'
                    },
                    'af-applet-contentview': {
                        'requires': ['af-applet-baseview','af-trans'],
                        'path': '/nn/lib/metro/g/myy/af_applet_contentview_0.0.3.js'
                    },
                    'af-applet-contentsettingsview': {
                        'requires': ['af-applet-contentview','af-utils','ape-applet-templates-settingswrap'],
                        'path': '/nn/lib/metro/g/myy/af_applet_contentsettingsview_0.0.3.js'
                    },
                    'af-applet-dd': {
                        'requires': ['af-applet-dom','af-message','event-custom-base'],
                        'path': '/nn/lib/metro/g/myy/af_applet_dd_0.0.4.js'
                    },
                    'type_abu': {
                        'requires': ['af-event'],
                        'path': '/nn/lib/metro/g/myy/abu_0.0.16.js'
                    },
                    'type_abu_event': {
                        'requires': ['event-synthetic','node-scroll-info'],
                        'path': '/nn/lib/metro/g/myy/abu_event_0.0.2.js'
                    },
                    'type_abu_video': {
                        'requires': ['af-content','af-event','base','event-custom','type_abu_event','node-core'],
                        'path': '/nn/lib/metro/g/myy/abu_video_0.0.6.js'
                    },
                    'type_abu_video_manager': {
                        'requires': ['af-content','af-event','base','event-custom','type_abu_event','type_abu_video','node-core'],
                        'path': '/nn/lib/metro/g/myy/abu_video_manager_0.0.20.js'
                    },
                    'type_advance_desktop': {
                        'requires': ['stencil','stencil-base','stencil-sticker','af-applets','base','af-rapid'],
                        'path': '/nn/lib/metro/g/myy/advance_desktop_0.0.8.js'
                    },
                    'type_advance_desktop_viewer': {
                        'requires': ['stencil','stencil-base','stencil-sticker','af-applets','base','af-rapid','highlander-client'],
                        'path': '/nn/lib/metro/g/myy/advance_desktop-viewer_0.0.3.js'
                    },
                    'type_app_declarations': {
                        'requires': ['af-cookie'],
                        'path': '/nn/lib/metro/g/myy/app_declarations_0.0.6.js'
                    },
                    'type_partner_att_enus_foreseealive': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/foresee/en-us/foresee-alive_0.0.3.js'
                    },
                    'type_partner_att_enus_foreseetrigger': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/foresee/en-us/foresee-trigger_0.0.3.js'
                    },
                    'pure_client_darla': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/pure_client_darla_0.0.2.js'
                    },
                    'type_idletimer': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/idletimer_0.0.1.js'
                    },
                    'type_myycontentdb': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myycontentdb/myycontentdb_0.0.54.js'
                    },
                    'type_uh_init': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myyheader/uh_init_0.0.13.js'
                    },
                    'type_myymail_mainview': {
                        'requires': ['af-applet-contentsettingsview','stencil-selectbox','stencil-toggle'],
                        'path': '/nn/lib/metro/g/myymail/myymail-mainview_0.0.23.js'
                    },
                    'type_myymail_appletmodel': {
                        'requires': ['mjata-model-base','af-applet-contentmodel'],
                        'path': '/nn/lib/metro/g/myymail/myymail-appletmodel_0.0.16.js'
                    },
                    'type_myyrss_js': {
                        'requires': ['af-applet-contentmodel','af-applet-contentsettingsview'],
                        'path': '/nn/lib/metro/g/myyrss/myyrss_0.0.13.js'
                    },
                    'type_nux': {
                        'requires': ['node-base','event-base','panel','io-base','json-stringify','af-transport'],
                        'path': '/nn/lib/metro/g/nux/nux_0.0.44.js'
                    },
                    'type_optin': {
                        'requires': ['node','event','io','panel','io-base'],
                        'path': '/nn/lib/metro/g/optin/optin_0.0.33.js'
                    },
                    'type_changelayout': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/pagenav/changelayout_0.0.40.js'
                    },
                    'type_changetheme': {
                        'requires': ['af-transport'],
                        'path': '/nn/lib/metro/g/pagenav/changetheme_0.0.50.js'
                    },
                    'type_addmodule': {
                        'requires': ['node','event','io','json-parse','json-stringify'],
                        'path': '/nn/lib/metro/g/pagenav/addmodule_0.0.16.js'
                    },
                    'type_addpage': {
                        'requires': ['af-transport'],
                        'path': '/nn/lib/metro/g/pagenav/addpage_0.0.27.js'
                    },
                    'type_pagenav': {
                        'requires': ['af-transport'],
                        'path': '/nn/lib/metro/g/pagenav/pagenav_0.0.35.js'
                    },
                    'type_reco': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/reco/reco_0.0.10.js'
                    },
                    'type_sda': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/sda/sda_0.0.27.js'
                    },
                    'type_sdarotate': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/sda/sdarotate_0.0.17.js'
                    },
                    'type_signoutcta': {
                        'requires': ['node','event'],
                        'path': '/nn/lib/metro/g/signoutcta/signoutcta_0.0.9.js'
                    },
                    'type_windowshade_js': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/windowshade/windowshade_0.0.4.js'
                    }
                        }
                    }
                }
            });

            if (!YMedia.config.patches || !YMedia.config.patches.length) {
                YMedia.config.patches = [
                    function patchLangBundlesRequires(Y, loader) {
                        var getRequires = loader.getRequires;
                        loader.getRequires = function (mod) {
                            var i, j, m, name, mods, loadDefaultBundle,
                                locales = Y.config.lang || [],
                                r = getRequires.apply(this, arguments);
                            // expanding requirements with optional requires
                            if (mod.langBundles && !mod.langBundlesExpanded) {
                                mod.langBundlesExpanded = [];
                                locales = typeof locales === 'string' ? [locales] : locales.concat();
                                for (i = 0; i < mod.langBundles.length; i += 1) {
                                    mods = [];
                                    loadDefaultBundle = false;
                                    name = mod.group + '-lang-' + mod.langBundles[i];
                                    for (j = 0; j < locales.length; j += 1) {
                                        m = this.getModule(name + '_' + locales[j].toLowerCase());
                                        if (m) {
                                            mods.push(m);
                                        } else {
                                            // if one of the requested locales is missing,
                                            // the default lang should be fetched
                                            loadDefaultBundle = true;
                                        }
                                    }
                                    if (!mods.length || loadDefaultBundle) {
                                        // falling back to the default lang bundle when needed
                                        m = this.getModule(name);
                                        if (m) {
                                            mods.push(m);
                                        }
                                    }
                                    // adding requirements for each lang bundle
                                    // (duplications are not a problem since they will be deduped)
                                    for (j = 0; j < mods.length; j += 1) {
                                        mod.langBundlesExpanded = mod.langBundlesExpanded.concat(this.getRequires(mods[j]), [mods[j].name]);
                                    }
                                }
                            }
                            return mod.langBundlesExpanded && mod.langBundlesExpanded.length ?
                                    [].concat(mod.langBundlesExpanded, r) : r;
                        };
                    }
            ];
        }
        for (var i = 0; i < YMedia.config.patches.length; i += 1) {YMedia.config.patches[i](YMedia, YMedia.Env._loader);}

        
                        YUI().use('node-base', function(Y) {
                    Y.Global.fire('ymediaReady', {e: YMedia});
                });

                    });
                });        YUI().use('node-base', function(Y) {
        Y.Global.on('ymediaReady', function(data) {
            YMedia = data.e;
    YMedia.applyConfig({"groups":{"td-applet-mega-header":{"base":"/sy/os/mit/td/td-applet-mega-header-1.0.193/","root":"os/mit/td/td-applet-mega-header-1.0.193/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_30345894"] = {"applet_type":"td-applet-mega-header","views":{"main":{"yui_module":"td-applet-mega-header-mainview","yui_class":"TD.Applet.MegaHeaderMainView","config":{"alphatar":{"enabled":true,"urlPath":"https://s.yimg.com/rz/uh/alphatars/","imgType":".png"},"avatar":{"serverCall":false,"api":{"protocol":"https","host":"ucs.query.yahoo.com","path":"/v1/console/yql","query":{"format":"json","_maxage":900}}},"useAvatar":true,"bucket":"201","followable":{"enabled":true,"uri":"/_td_api"},"hasMailPreview":true,"hasMail":true,"hasNotifications":true,"hasProfile":true,"iSearch":{"isEnabled":false,"instantTrending":false,"frcode":"yfp-t-201","frcodeTrending":"fp-tts-201","spaceid":97162737,"cacheMaxAge":30000,"pageViewDelay":1000,"comscoreDelay":3000,"timeout":2500,"highConfidence":true,"autocomplete":{"additionalParams":{"appid":"is","nresults":4},"max_results":4},"api":{"protocol":"https://","host":"search.yahoo.com","path":"/search?","tmpl":"ISRCHA:A0124"},"filterUrls":{"web":"https://search.yahoo.com/search?p=","images":"https://images.search.yahoo.com/search/images?p=","video":"https://video.search.yahoo.com/search/video?p=","news":"https://news.search.yahoo.com/search?p=","local":"https://search.yahoo.com/local/s?p=","answers":"https://answers.search.yahoo.com/search/search_result?p=","shopping":"https://shopping.search.yahoo.com/search?p="}},"isFallback":false,"loginUrl":"https://login.yahoo.com/config/login?.src=fpctx&.intl=us&.lang=en-US&.done=https%3A%2F%2Fwww.yahoo.com","logoutUrl":"https://login.yahoo.com/config/login?logout=1&.direct=2&.src=fpctx&.intl=us&.lang=en-US&.done=https%3A%2F%2Fwww.yahoo.com","mail":{"compose":{"url":"https://mrd.mail.yahoo.com/compose"},"count":{"api":{"protocol":"https","host":"mg.mail.yahoo.com","path":"/mailservices/v1/newmailcount","query":{"appid":"UnivHeader"}},"refreshInterval":120,"maxCountDisplay":99},"preview":{"prefetch":true,"api":{"protocol":"https","host":"ucs.query.yahoo.com","path":"/v1/console/yql","query":{"q":"select messageInfo.receivedDate, messageInfo.mid, messageInfo.flags.isRead, messageInfo.from.name, messageInfo.subject from ymail.messages where numMid=\"3\" limit 6","format":"json"}},"urls":{"message":"https://mrd.mail.yahoo.com/msg?fid=Inbox&src=hp&mid="}},"url":"https://mail.yahoo.com/"},"miniheader":true,"miniheaderYPos":0,"notifications":{"inlineReader":true,"maxDisplay":0,"maxUpsellsDisplay":10,"count":{"api":{"uri":"/_td_api","requestType":"clustersUpdatesCount"},"refreshInterval":120,"maxCountDisplay":99},"list":{"api":{"uri":"/_td_api","requestType":"clustersList","image_tag":"pc:size=square,pc:size=small_fixed"}}},"search":{"action":"https://search.yahoo.com/search","assistYlc":";_ylc=X3oDMTFiaHBhMnJmBF9TAzIwMjM1MzgwNzUEaXRjAzEEc2VjA3NyY2hfcWEEc2xrA3NyY2hhc3Q-","ylc":";_ylc=X3oDMTFiN25laTRvBF9TAzIwMjM1MzgwNzUEaXRjAzEEc2VjA3NyY2hfcWEEc2xrA3NyY2h3ZWI-","queries":[{"name":"fr","value":"yfp-t-201"}],"autocomplete":{"ghostEnabled":false,"host":"https://search.yahoo.com/sugg/gossip/gossip-us-ura/","crumbKey":"gossip","theme":{"highlight":{"color":"black","background":"#c6d7ff"}},"plugin":{"minQueryLength":3,"resultHighlighter":"phraseMatch"}}},"searchFormGlow":true,"searchMiniHeader":false,"accountSwitchData":{"enabled":""}}}},"templates":{"main":{"yui_module":"td-applet-mega-header-templates-main","template_name":"td-applet-mega-header-templates-main"},"mailpreview":{"yui_module":"td-applet-mega-header-templates-mailpreview","template_name":"td-applet-mega-header-templates-mailpreview"},"accountSwitchPanel":{"yui_module":"td-applet-mega-header-templates-accountSwitchPanel","template_name":"td-applet-mega-header-templates-accountSwitchPanel"},"notificationpanel":{"yui_module":"td-applet-mega-header-templates-notificationpanel","template_name":"td-applet-mega-header-templates-notificationpanel"},"topbar":{"yui_module":"td-applet-mega-header-templates-topbar","template_name":"td-applet-mega-header-templates-topbar"}},"i18n":{"ACCOUNT_INFO":"Account Info","ACCOUNT_SWITCH_WELCOME_1":"Easily switch between multiple Yahoo accounts using the new","ACCOUNT_SWITCH_WELCOME_2":"Click \"Add account\" below to get started!","ACCOUNT_SWITCH_COOKIE_ERR":"It looks like you switched accounts. Refresh the browser to view your personalized page.","ACCOUNT_SWITCH_CRUMB_ERR":"Your account data may be out of sync.<br>Refresh the page to see your accounts.","ACCOUNT_SWITCH_ACCOUNT_MANAGER":"Account Manager","ADD_ACCOUNT":"Add account","ADD_MANAGE_ACCOUNT":"Add or manage accounts","ANSWERS":"Answers","CLEAR_SEARCH":"Clear search query","COMPOSE":"Compose","COMPOSE_CAPS":"COMPOSE","CORPMAIL":"Corp Mail","DEVELOPING_NOW":"Developing Now","ENTER_KEY":"Enter","FIREFOX_PROMO_TEXT":"Install the new Firefox","FOLLOW":"Follow","FOLLOWING":"Following","FROM":"From","GO_TO_MAIL":"Go To Mail","GO_TO_MAIL_CAPS":"GO TO MAIL","HAVE_NO_NEW_MESSAGES":"You have no new messages.","HAVE_NO_UPDATES":"Check back later for updates on stories you are following.","HOME":"Home","IMAGES":"Images","LOADING_MAIL":"Loading Mail Preview","LOADING_UPDATES":"Loading Updates","LOCAL":"Local","MAIL":"Mail","MAIL_CAPS":"MAIL","MENU":"Menu","NEW":"New","NEWS":"News","NEW_MESSAGE":"new message.","NEW_MESSAGES":"new messages.","NOT_FOLLOWING":"Follow a story and get updates here.","NOTIFICATIONS":"Notifications","PRESS":"Press ","PROFILE":"Profile","REL_DAYS":"{0}d","REL_DAYS_AGO":"{0} days ago","REL_HOURS":"{0}h","REL_HOURS_AGO":"{0} hours ago","REL_MINS":"{0}m","REL_MINS_AGO":"{0} minutes ago","REL_MONTHS":"{0}mo","REL_MONTHS_AGO":"{0} months ago","REL_SECS":"{0}s","REL_SECS_AGO":"{0} seconds ago","REL_WEEKS":"{0}wk","REL_WEEKS_AGO":"{0} weeks ago","REL_YEARS":"{0}yr","REL_YEARS_AGO":"{0} years ago","SEARCH":"Search","SEARCH_WEB":"Search Web","SETTINGS":"Settings","SETTINGS_CAPS":"SETTINGS","SHOPPING":"Shopping","SIGNIN":"Sign in","SIGNIN_CAPS":"SIGN IN","SIGNOUT":"Sign Out","SIGNOUT_ALL":"Sign out all","SIGNOUT_CAPS":"SIGN OUT","START_TYPING":"Start typing...","SUBJECT":"Subject","TO_SAVE_FOLLOWS":" to save and get updates.","TO_SEARCH":" to search.","TO_VIEW_NOTIF":" to view your notifications","TO_VIEW_MAIL":" to view your mail","UNABLE_TO_PREVIEW_MAIL":"We are unable to preview your mail.","UNABLE_TO_FETCH_UPDATES":"Check back later for updates on stories you are following.","UNFOLLOW":"Unfollow","VIDEO":"Video","WEB":"Web","WELCOME_BACK":"Welcome back","YOU_HAVE":"You have","SKIP_ASIDE":"Skip to Right rail","SKIP_MAIN":"Skip to Main content","SKIP_NAV":"Skip to Navigation"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-viewer":{"base":"/sy/os/mit/td/td-applet-viewer-0.1.2104/","root":"os/mit/td/td-applet-viewer-0.1.2104/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_50000101"] = {"applet_type":"td-applet-viewer","models":{"viewer":{"yui_module":"td-viewer-model","yui_class":"TD.Viewer.Model","config":{"dataFetch":{"NUM_PREFETCH_ITEMS":20,"NUM_RENDER_ITEMS":1,"NUM_BATCH_PHOTOS":25,"STALE_DATA_FETCH":1800},"cache":{"max-age":{"SLIDESHOW":300,"STORY":600,"VIDEO":3600},"stale-while-revalidate":{"SLIDESHOW":43200,"STORY":43200,"VIDEO":43200}},"enableCategories":true,"enableEntities":true,"useUuidPrefix":true,"optimisticPrefetch":false}},"ads":{"yui_module":"td-ads-model","yui_class":"TD.Ads.Model","config":{"sectionId":4250754,"positionThreshold":200,"enabled":false,"count":5,"lowerBound":2,"fromContentAPI":0,"fromCurveballAPI":1,"enableBeaconListenerOnly":true,"enableVisibilityRect":true,"adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html"}},"applet_model":{"models":["viewer","ads"]}},"views":{"main":{"yui_module":"td-viewer-mainview","yui_class":"TD.Viewer.MainView","config":{"ads":{"enableAdsFeedback":0,"enableVideoSpaceid":1,"curveball":{"sectionId":4250754,"positionThreshold":200,"enabled":false,"count":5,"lowerBound":2,"fromContentAPI":0,"fromCurveballAPI":1,"enableBeaconListenerOnly":true,"enableVisibilityRect":true,"adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html"},"displayAds":{"config":{"LDRP-9":{"w":"768","h":"1"},"LDRB-9":{"w":"728","h":"90","supports":{"exp-ovr":1,"exp-push":1},"fdb":true,"z":11},"LREC-9":{"w":"300","h":"250","fdb":true,"supports":{"exp-ovr":1,"exp-push":1},"z":11},"WPS-9":{"w":"320","h":"50"},"WP-9":{"w":"320","h":"50"},"sa":"LREC='300x250;1x1' LREC2='300x250;1x1' LREC3='300x250;1x1' MON='300x600;1x1' megamodal=true","LREC2-9":{"w":"300","h":"250","fdb":true,"supports":{"exp-ovr":1,"exp-push":1},"z":11},"LREC3-9":{"w":"300","h":"250","fdb":true,"supports":{"exp-ovr":1,"exp-push":1},"z":11},"MAST-9":{"supports":{"exp-ovr":1,"exp-push":1,"resize-to":1},"fdb":false,"closeBtn":{"mode":2,"useShow":1},"metaSize":true,"z":11},"MON-9":{"w":"300","h":"600","fdb":true,"supports":{"exp-ovr":1,"exp-push":1},"z":11},"SPL-9":{"flex":"both","supports":{"cmsg":1},"fdb":false,"meta":{"type":"stream"},"z":11,"css":".Mags-FontA {font-family: 'Freight Big Pro', Georgia, Times, serif;font-weight: 300;} .Mags-FontA.Size1 {font-size: 13px;}.Mags-FontA.Size2 {font-size: 16px;} .Mags-FontA.Size3 {font-size: 20px;}.Mags-FontA.Size4 {font-size: 22px;} .Mags-FontA.Size5 {font-size: 33px;}.Mags-FontA.Size6 {font-size: 35px;} .Mags-FontA.Size7 {font-size: 58px;}.Mags-FontA.Size8 {font-size: 70px;} .Mags-FontA.Size9 {font-size: 100px;} .Mags-FontB {font-family: Georgia, Times, serif;font-weight: 400;} .Mags-FontB.Size1 {font-size: 18px;} .Mags-FontC {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 400;}.Mags-FontC.Size1 {font-size: 11px;}.Mags-FontC.Size2 {font-size: 14px;} .Mags-FontC.Size3 {font-size: 16px;}.Mags-FontC.Size4 {font-size: 20px;} .Mags-FontC.Size5 {font-size: 30px;}.Mags-FontC.Size6 {font-size: 32px;} .Mags-FontC.Size7 {font-size: 52px;}"},"SPL2-9":{"flex":"both","supports":{"cmsg":1},"fdb":false,"meta":{"type":"stream"},"z":11,"css":".Mags-FontA {font-family: 'Freight Big Pro', Georgia, Times, serif;font-weight: 300;} .Mags-FontA.Size1 {font-size: 13px;}.Mags-FontA.Size2 {font-size: 16px;} .Mags-FontA.Size3 {font-size: 20px;}.Mags-FontA.Size4 {font-size: 22px;} .Mags-FontA.Size5 {font-size: 33px;}.Mags-FontA.Size6 {font-size: 35px;} .Mags-FontA.Size7 {font-size: 58px;}.Mags-FontA.Size8 {font-size: 70px;} .Mags-FontA.Size9 {font-size: 100px;} .Mags-FontB {font-family: Georgia, Times, serif;font-weight: 400;} .Mags-FontB.Size1 {font-size: 18px;} .Mags-FontC {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 400;}.Mags-FontC.Size1 {font-size: 11px;}.Mags-FontC.Size2 {font-size: 14px;} .Mags-FontC.Size3 {font-size: 16px;}.Mags-FontC.Size4 {font-size: 20px;} .Mags-FontC.Size5 {font-size: 30px;}.Mags-FontC.Size6 {font-size: 32px;} .Mags-FontC.Size7 {font-size: 52px;}"},"FSRVY-9":{"w":"1","h":"1","supports":{"exp-ovr":1,"exp-push":1,"bg":1,"lyr":1},"z":11},"FOOT9-9":{"w":"1","h":"1","supports":{"exp-ovr":1,"exp-push":1},"z":11},"FOOT-9":{"w":"1","h":"1","supports":{"exp-ovr":1,"exp-push":1},"z":11}},"enabled":1,"enabledAuto":false,"enabledDeferRenderAds":false,"forced_modalspaceid":"","enabledMultiAds":true,"positions":{"aboveFold":["LREC-9"],"belowFold":[],"custom":["MAST-9","LDRB-9","SPL-9","SPL2-9"]},"modalposition":"LREC-9,MAST-9,LDRB-9,SPL-9,FSRVY-9,FOOT9-9,MON-9","lrecTimeout":"","enableK2BeaconConf":true,"enableBucketSiteAttribute":true,"offnetSpaceid":"1197762606","offnetposition":"LREC-9","magazineposition":"LREC-9,MAST-9,LDRB-9,SPL-9,FOOT-9,MON-9"},"YVAP":{"accountId":"904","playContext":"default","spaceId":null},"displayAdsFP":{"enabled":0,"pos":"FPAD,LREC","config":[{"id":"FPAD","dest":"my-adsFPAD","pos":"FPAD","clean":"my-adsFPAD-base","h":"250","w":"300"},{"id":"LREC","dest":"my-adsLREC","pos":"LREC","clean":"my-adsLREC-base","h":"250","w":"300"}]},"modalYVAP":{"news":"145","finance":"193","sports":"82","gma":"145","default":"904"},"modalBackfillYVAP":{"news":"1115","finance":"1116","sports":"1118","default":"1115"},"modalPlayContext":{"news":"default","finance":"default","sports":"default","gma":"gma","autos":"bmprpreover","beauty":"bmprpreover","celebrity":"bmprpreover","food":"bmprpreover","health":"bmprpreover","makers":"bmprpreover","movies":"bmprpreover","music":"bmprpreover","parenting":"bmprpreover","politics":"bmprpreover","style":"bmprpreover","tech":"bmprpreover","travel":"bmprpreover","tv":"bmprpreover"},"modalBackfillExpName":"sidekickTVlrecbackfillHTML5","modalBackfillExpType":"right-rail-autoplay","modalBackfillAdTitlePrefix":"UP NEXT: ","modalBackfillYVAPHTML5":{"news":"1115","finance":"1116","sports":"1118","default":"1115"},"modalBackfillExpNameHTML5":"sidekickTVlrecbackfillHTML5"},"category":"","hlView":true,"js":{"twitter":"https://platform.twitter.com/widgets.js","videoplayer":"https://yep.video.yahoo.com/js/3/videoplayer-min.js?r=&ypv=","embed":["click-capture-min","utils"]},"redirectNoContent":true,"rendered":false,"search":{"action":"https://search.yahoo.com/search","frcode":"yfp-t-201-m"},"spaceid":0,"ui":{"adsMeta":true,"alphatar":{"enabled":true,"defaultProfile":"https://s.yimg.com/dh/ap/social/profile/profile_b48.png"},"branding":{"gma":{"src":"https://s.yimg.com/dh/ap/default/151203/GMA-banner.jpg","alt":"Good Morning America"}},"comments":{"enabled":true,"proxy":1,"applet":"td-applet-mega-comments","offnet":{"enabled":false}},"comments_allow_supression":true,"comments_preview":true,"comments_writes_enabled":true,"disableSlideshowMON":true,"enableCategories":true,"enableCaptionScroll":false,"enableEntities":true,"enableContinueReading":"","enableModalBackfillViewportDetection":false,"enableModalBackfillVideoAds":true,"enableModalBackfillVideoAdsHtml5":true,"enableModalBackfillVideoAdsIframe":false,"enableModalContentPV":true,"enableModalCustomEvent":true,"enableModalLargeAds":true,"enableModalLargeAdsOffnetwork":true,"enableModalLRECBackfill":true,"enableModalSingleAutoplay":true,"enableModalSponsoredAds":true,"enableSlideshowv2":true,"enableSlideshowv2Thumbnails":false,"enableSidekickAdditionalRendering":true,"enableSidekickCommentsRefresh":true,"enableSidekickDynamicHeight":true,"enableSidekickFederationCall":true,"enableLike":true,"enableLongCaption":true,"enableReblog":true,"enableReadMore":true,"enableShare":true,"fixedHeader":false,"forceRedisRead":true,"imageResize":true,"footer":true,"inlineView":true,"lazyLoad":true,"lead":{"minWidth":600,"resizedWidth":300},"licensedFullBody":false,"linkYlk":"itc:0,elm:context_link;","loadAppletDelay":"","loginUrl":"https://login.yahoo.com/config/login?.intl=us&.lang=en-US","nydcContent":true,"optimisticPrefetch":false,"readMoreMinScore":"0.7","renderAdsOnly":false,"resizeSlideshowEnabled":true,"searchBox":false,"share":{"tumblr":{"url":"//www.tumblr.com/widgets/share/tool/preview","post_url":"https://www.tumblr.com/widgets/share/tool?shareSource=legacy&canonicalUrl=&posttype=link&"}},"singleRightRail":false,"slots":{"sidekickdesktop":{"name":"td-applet-sidekick","condition":"all","config":{"disableAppClass":1,"site":"fp","ui.template":"megastream","estHeight":230,"countMargin":5,"ads.display.start":2,"ads.display.frequency":4,"enableAdsFeedback":1,"enableStreamViewCall":1,"mode":"","viewer":"1","condition":"all","maxBatchCount":"35","smartCrop":"1","maxTotalCount":"90","wideImageDesign":"1","uiTemplate":"megacards"}},"readMore":{"name":"td-applet-related-content","condition":"all","config":{"disableAppClass":1,"site":"fp","viewer_enabled":1,"smart_crop":1,"count":"4","image_size":"img:159x159|2|80","min_count":"3","request_count":"20"}},"sidekick":{"config":{"enableAdsFeedback":1},"condition":"none"}},"slotsOrder":[],"sticker":true,"stickerTarget":"#SearchBar-Wrapper-Mini","stream":{"enabled":false,"config":{"header":0,"ui.dislike":0,"ui.filters":0,"ui.hq_ad_template":"featured_ad","ui.like":0,"ui.save":0,"ui.templates.all.gap":1,"ui.templates.all.start":5,"ui.templates.featured.batch_max":3,"ui.templates.featured.gap":2,"ui.viewer_off_network":1,"ui.viewer":1}},"swipe":false,"pcsExclusions":false,"useCapSummary":true,"useUuidPrefix":true,"useSsYcts":true,"lcpBodyEnabled":false,"videoEventsWaitTime":300,"videoPlayer":{"version":"","env":""},"yqlResizeEnabled":true,"estSideAdsHeight":610,"estSideNoAdsHeight":60,"mode":"mega-modal","sidekickBatchDelay":500,"sidekickFollowingDelay":3000,"sidekickMaxBatchCount":35,"sidekickMaxTotalCount":90,"slotsSide":["sidekickdesktop"],"experience":"","useContentSite":1,"enable":{"cluster":{"items":1}},"lrecBackfill":{"img":"https://s.yimg.com/os/mit/media/m/ads/images/backfill/sports-dailyfantasy-v2-a966da5.jpg","link":"https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=20&mc=click&pli=14816902&PluID=0&ord=${CACHEBUSTER}"},"sidekickFixCount":"5"}}}},"templates":{"main":{"yui_module":"td-applet-viewer-templates-main","template_name":"td-applet-viewer-templates-main"},"cards":{"yui_module":"td-applet-viewer-templates-cards","template_name":"td-applet-viewer-templates-cards"},"carousel":{"yui_module":"td-applet-viewer-templates-carousel","template_name":"td-applet-viewer-templates-carousel"},"story":{"yui_module":"td-applet-viewer-templates-story","template_name":"td-applet-viewer-templates-story"},"reblog":{"yui_module":"td-applet-viewer-templates-reblog","template_name":"td-applet-viewer-templates-reblog"},"video":{"yui_module":"td-applet-viewer-templates-video","template_name":"td-applet-viewer-templates-video"},"slideshow":{"yui_module":"td-applet-viewer-templates-slideshow","template_name":"td-applet-viewer-templates-slideshow"},"header":{"yui_module":"td-applet-viewer-templates-header","template_name":"td-applet-viewer-templates-header"},"content_body":{"yui_module":"td-applet-viewer-templates-content_body","template_name":"td-applet-viewer-templates-content_body"},"content_body_mega":{"yui_module":"td-applet-viewer-templates-content_body_mega","template_name":"td-applet-viewer-templates-content_body_mega"},"story_cover":{"yui_module":"td-applet-viewer-templates-story_cover","template_name":"td-applet-viewer-templates-story_cover"},"lightbox":{"yui_module":"td-applet-viewer-templates-lightbox","template_name":"td-applet-viewer-templates-lightbox"},"lightbox_mega":{"yui_module":"td-applet-viewer-templates-lightbox_mega","template_name":"td-applet-viewer-templates-lightbox_mega"},"fallback":{"yui_module":"td-applet-viewer-templates-fallback","template_name":"td-applet-viewer-templates-fallback"},"ads_story":{"yui_module":"td-applet-viewer-templates-ads_story","template_name":"td-applet-viewer-templates-ads_story"},"header_ads":{"yui_module":"td-applet-viewer-templates-header_ads","template_name":"td-applet-viewer-templates-header_ads"},"footer":{"yui_module":"td-applet-viewer-templates-footer","template_name":"td-applet-viewer-templates-footer"},"share_btns":{"yui_module":"td-applet-viewer-templates-share_btns","template_name":"td-applet-viewer-templates-share_btns"},"share_btns_mega":{"yui_module":"td-applet-viewer-templates-share_btns_mega","template_name":"td-applet-viewer-templates-share_btns_mega"},"mag_slideshow":{"yui_module":"td-applet-viewer-templates-mag_slideshow","template_name":"td-applet-viewer-templates-mag_slideshow"},"drawer":{"yui_module":"td-applet-viewer-templates-drawer_feedback","template_name":"td-applet-viewer-templates-drawer_feedback"},"thank_you":{"yui_module":"td-applet-viewer-templates-ad_fdb_thank_you","template_name":"td-applet-viewer-templates-ad_fdb_thank_you"},"sidekicktv":{"yui_module":"td-applet-viewer-templates-sidekicktv","template_name":"td-applet-viewer-templates-sidekicktv"}},"i18n":{"PREVIOUS":"Previous","Next":"Next","MORE":"...","LESS":"less","NOCONTENT_FUNNY":"Feeding the engineers.  Will be back soon...","NOCONTENT_READMORE":"Read more on \"{0}\"","OFFNETWORK":"Read more on {0}","ELLIPSIS":"{0} ...","VIDEO_DURATION":"Duration {0}","SPONSORED":"Sponsored","SWIPE_FOR_NEXT":"Swipe for next article","INSTALL_NOW":"Install now","FULL_ARTICLE":"Read Full Article","ARTICLE_SUMMARY":"Read Summary","READ_MORE":"Read More","AD":"Advertisement","SHARE_EMAIL":"Share to Mail","CLOSE":"Close","RELATED_NEWS":"Related news","START_SLIDESHOW":"Start Slideshow","CONTINUE_READING":"Continue Reading","CLICK_FOR_VIDEOS":"Click here to watch the video","CLICK_FOR_PHOTOS":"Click here to view photos","AD_FDB1":"It's offensive to me","AD_FDB2":"I keep seeing this","AD_FDB3":"It's not relevant to me","AD_FDB4":"Something else","AD_FDB_HEADING":"Why don't you like this ad ?","AD_REVIEW":"We'll review and make changes needed.","AD_THANKYOU":"Thank you for your feedback","AD_SUBMIT":"Submit","AD_FDB_ERORR":"Please select option. To help us making your experience better.","AD_FDB_SOMETHING_ELSE":"Tell us, why you don't like this ad?","DONE":"Done","READ_ARTICLE":"Read more","LIKE":"Like","LIKE_THIS_TOPIC":"Like this topic","REBLOG":"Reblog","REBLOG_TUMBLR":"Reblog on Tumblr","UNDO":"Undo","SIGN_IN_TO_LIKE":"Sign in to like","LIKABLE_ARTICLE_SIGN_IN":"Sign in to see more stories you like.","SIGN_IN_2":"Sign in","READ_FULL_ARTICLE":"Read full article","COMMENTS":"Comments","FACEBOOK":"Share","TWITTER":"Tweet","EMAIL":"Email","CLOSE_VIEWER":"Close this content, you can also use the Escape key at anytime"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};window.ViewerClickCapture||(window.ViewerClickCapture=function(){function a(b,c,d){return b?b.tagName===c&&b.className&&b.className.indexOf(d)>=0?b:a(b.parentNode,c,d):!1}function b(b){var c=a(b.target,"A","js-content-viewer");c&&(d=b,b.preventDefault&&b.preventDefault())}function c(){!e&&f.removeEventListener&&f.removeEventListener("click",b),e=!0}var d,e,f=document.documentElement;return f.addEventListener&&(f.addEventListener("click",b),window.setTimeout(function(){c()},4e3)),{clear:function(){d=null},last:function(){return d},disable:c}}());window.ViewerUtils||!function(){function a(){function a(a){return a&&"object"==typeof a&&"number"==typeof a.length&&g.call(a)===h||!1}function b(a){return"string"==typeof a||a&&"object"==typeof a&&g.call(a)===i||!1}function c(a){return"undefined"==typeof a}function d(a){return null===a}function e(a){return c(a)||d(a)}var f=Object.prototype,g=f.toString,h="[object Array]",i="[object String]";return{isArray:Array.isArray||a,isNull:d,isString:b,isUndefined:c,isVoid:e}}function b(b,c,d){var e=a();if(!b)return d;if(!c)return b;!e.isArray(c)&&e.isString(c)&&(c=c.split("."));for(var f=0,g=c.length;b&&g>f;f++)b=b[c[f]];return e.isVoid(b)?d:b}window.ViewerUtils={getObjValue:b}}();
YMedia.applyConfig({"groups":{"td-applet-navlinks-atomic":{"base":"https://s.yimg.com/os/mit/td/td-applet-navlinks-atomic-0.0.54/","root":"os/mit/td/td-applet-navlinks-atomic-0.0.54/","combine":true,"filter":"min","comboBase":"https://s.yimg.com/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_50000195"] = {"applet_type":"td-applet-navlinks-atomic","views":{"main":{"yui_module":"td-applet-navlinks-mainview","yui_class":"TD.Applet.NavlinksMainView"}},"i18n":{"TITLE":"navlinks-atomic","TOPICS":"Topics"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-stream-atomic":{"base":"/sy/os/mit/td/td-applet-stream-atomic-2.0.440/","root":"os/mit/td/td-applet-stream-atomic-2.0.440/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_50000173"] = {"applet_type":"td-applet-stream-atomic","models":{"stream":{"yui_module":"td-applet-stream-model-v2","yui_class":"TD.Applet.StreamModel2","data":{"ccode":"mega_global_ranking_hlv2_up_based","more_items":[{"type":"ad","id":"31690843011","title":"CrÃ©dit sans votre banque de 3000 Ã  40 000â¬ ?","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=2ihgeZMGIS9rL1LcJ0_28yqGyxF640dGp91rKDkdTAzgXK5Hfwq0aLhwx6EdUqF1JV4sCbXd39reag.LXV9pnA1ONBFtpDDq6GLBwGGvKbl4o1IBSpka7nYqpa6SSy.ZIa7su.tc1U7TOTPRJaiC7KknwMK_HSZcVZvgHtpUMlpiR_TelaiFECaldc6tQbWALEfUITuVFKMTPJErR.nS2N5nFc4o.X3Zolj5D4z2yvjubCoAxacTL6KM1lj6E4FlLtNKr02rYtHjLW9CJBTohQd7IOe7jydhjWm.Jt5FEDATsN0xYk4ntQKgML6Fi6itFYoUHAoxfs_1CQAnBqjkBJAhmTEIoP78FPGQp3SU80ZGZPJEec0yImNldIYkpKiQkaYDrMgVcpXRVbrqrIeNBMPZIfcPLR_zy0tWSk32Lk7fm8TXe5iBSBNC8NyUHk4R2Y.mLhLsxepra6dTtvPvwzYqmTOaJr7Q0qLsAuiY9kxD1JUHzI0MuYrIdhzSE.szJr75Vbj0vVWtvxzxVxanNSNCm_bhFPs-%26lp=http%3A%2F%2Feulerian.pret-dunion.fr%2Fclick%2Fpret-dunion%2F8lL.QlYVeQ7BL6AqQORY_PvmxNoZxgyVzZobd9R9rA--%2F","publisher":"PrÃªt d'Union","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=er1I6ToGIS.31IUO2dZibr.f_BCcHg8Tm1hz.i_olAsLQts26_K_AYd_iybcileLEhO5qiL0U_t9RLwu8CfNIGs0c_EeblkP295Q3nFwUmv4YNOrwjXEzr7hmWz00cGZrE0AwYOyIWoijpuqfDV4NAADXBb8zgzgfCuHM5HGTgVznE9U9QMl3ZiEZAlUJ7A.SuDSdIf3EefU7VK0LJR1lODmNFntxvNFL5PAUa9e4s5XU6R6x.YBpah2f1aRKLGWzLWXJaRQqSgbgnmB0ED2TWe3Auq90KVKdOqUsOZlu1dM41c3nTZShfMOGJh3_4UVW0zxnCRtqLDe.vNgtzJu5B6L46FVwGOBxncuRdp75aQ4JMfxKC5XVWvRZk.9Je0UHnGwECc-&ap=13","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15o4kljqd(gid$0de98d92-d01d-11e5-9b07-008cfacc4628-7f1d99642700,st$1455125891097000,li$0,cr$31690843011,dmn$pret-dunion.fr,srv$3,exp$1455133091097000,ct$27,v$1.0,adv$936665,pbid$1,seid$4250754))&r=1455125891097&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/4YL6ZXnMadN1zynlD8Fbnw--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1454074889483-7824.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"DÃ¨s 48,29â¬/mois, rÃ©alisez vos projets moins cher sans les organismes de crÃ©dit traditionnels et plus rapidement qu'en passant par votre banque!","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":1,"cposy":1,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31690843011","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":0},{"type":"unknown","id":"8b39fda4-b5a3-3492-811f-c513afd5e781","instrument":"4000023540S00001","is_eligible":"true"},{"type":"unknown","id":"6ca13080-50c5-3d1a-83ce-c2a3c8e16d0e","instrument":"4000019070S00008","is_eligible":"false"},{"type":"unknown","id":"da53fc5d-2444-36a7-994b-9efb44e6b888","instrument":"4000021490S00008","is_eligible":"false"},{"type":"unknown","id":"7157e5c9-8e53-30b8-9054-dc8610d373d5","instrument":"4000008260S00002","is_eligible":"true"},{"type":"ad","id":"31709710404","title":"Vous voulez faire de l'argentâ¦mais comment?","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=I6ZMeI0GIS_1BJ_0cz1v2PtPgbADmxt3UCgKbIlW6A71bwr5_7.PhDPr8N8OcqwyHI2rEqTUP5WDjqaURciMJTRnybhHG1urqnLIMOXXeFluv3YhDLEs7sXYRYvxC_u7iJMLaAm87_Hd.0I25S04HHBu18yVT1wX6Vgfndk2q2UBHoRyMuJWVWz9hszqLIa_XQf9lR6zY7vWaaAftyaa1mO00L.OgVzrFrxM18grL4Yw.7.Km5T2Vh5F9UEBkuOy2GpoXyPGijj0WRxXN1d5v1A9uFccNn2o9x7.POeYnwl5Hhf5jLbXGDAQEdwc2pBLE0eJwLwikAzUsImkNT4Ew9GhXWjBnfhReBCsxQbLicW6x7J0.9QtR6bsFh9hUWouQPjqAB8ilNzpJSMb4Eehkd7aLmZhoh5ktXT4.2.YFHhdOwy5yegK2T374sYtZze0vP.64KaWdDaJbh2fgMM41hagf46.xvQ9D_Il2bm45sZnW1ttfooYnaC1EYh2c6kk.Y2U5kFc_NxSk9725chqj9YJWwpvOBkJjD6UANGrWaceDsbJEfK95ur_LkwJ0aKJTzPJbkMO0V.nYAS2ZppWUizajCSlTKMPUNnqIb9PsU2TIbqy2HydvokXiAdhN_pA22_Ogf9o1HvUFO307o3aMeksYW22hxNi1XalklP5MCFzmMGUN.V3oDo-%26lp=https%3A%2F%2Fserving.plexop.net%2Fpserving%2Fbridge_002.htm%3Fa%3D4%26t%3Dhttp%253a%252f%252fpreg.TradeLG.org%252faserving%252f4%252f1%252f1020%252f2_fr_14498.htm%253fSerialId%253d1120679%2526FormId%253d1746%26adv%3D1%26f%3D82269","publisher":"TradeLG","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=8OJcCtwGIS9Flom6xTBgLFRyDIrzJ3MwvFbCgYuEn8IUSHrrRAYobOnUrCCd87iIqSCY.ClPc2RFjMZ0PBqb_BM1H6vzBiPw.pBKwuKvnhxnSiHoEWg2YHaB78SStUVzDBG6SyI3fXrs1uDWBeAQrNd3R_gR8gXniN5PgUZ5XsGAvY3Pkc8kJy5OibhevWB.PRd.KfkGVmLKradqnnVs3Vof8TOoFu8amfbllBXa57HAplCFseOC93y4yGAhs9ILu9Oe9R8wxxkwOFPaxwsSGKOVJaho.pNDgt8ERFpCaxERCaT8YLYEwmH.cpm3xW1zy8_CUhdh1pIE3Z0EUS6pcEzOanFG3ly4IT784EoCn7kPWyaYxPTu9ocTAe0ICto-&ap=18","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15t5oh3ro(gid$0de98d92-d01d-11e5-9b07-008cfacc4628-7f1d99642700,st$1455125891097000,li$0,cr$31709710404,dmn$serving.plexop.net,srv$3,exp$1455133091097000,ct$27,v$1.0,adv$1135676,pbid$1,seid$4250754))&r=1455125891097&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/43AUeMnPU44SSqYU3FF_Sg--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1454933657651-9679.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Vous avez entendu parler de la chute du pÃ©trole ou du krash asiatique mais vous ne savez pas comment investir?Cliquez et nos courtiers vous rÃ©pondront","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":2,"cposy":2,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31709710404","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":1},{"type":"unknown","id":"c-75030e88-1bf9-4d6f-980b-f34b0f5553e4","instrument":"4000005860S00001","is_eligible":"false"},{"type":"unknown","id":"2d9ce451-9885-31ef-a91a-c53f569b412c","instrument":"4000025300S00008","is_eligible":"false"},{"type":"unknown","id":"c0eb4ee8-76ec-39b8-b161-e9902b7a7537","instrument":"4000019090S00008","is_eligible":"false"},{"type":"unknown","id":"12c2ef3d-ab15-30db-956c-5e8fe6bfff8f","instrument":"4000016750S00008","is_eligible":"false"},{"type":"ad","id":"31700964765","title":"Add Classic Looks, Not Extra Costs","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=ChrljRUGIS8PcIo1Y7Z6y7VoevNo0OLvmRfFnyI22LCyOu5hf88RgHL99xf1yVbk2O7mMy38BTBeUsh7s5kAFYRO61sQjcjrwOrUrde4Sj5gwq71JREZSiG935uL030kxXx3.O23TmBy8kTBZZvCZT8j5elqRRuMdVr4254rktp8h_hwLttvKIrBbJ1A4Lv5OjH0rHgaofm4xaHggLfV8luB2ILDVjpnHw2dM1EjNLuY4oZ8wXsYZhEHvF6RTAeM9h_nWsYIrG5qvCgFmSdRois54aLgSpuDqxjzqD_tHxqo.sSWpJ.9DhoU5ozSDVf_RMuaMTvPFrOJ6w4HyMqRxdUK5iNCmXaGmxfOTQIRWF._ad_lJ_lGsdGWNb.aadvmdvAtdV.AdZEwul5D4WIveOZ15A5wcjt9QF9c44pceE.GmD.DUmoUozpPjvOq4auoItxnq7UZXyxQBDnP9ViYb0ZNjiAZQQQZ9ZXFl5abVa3IIZ5AInC_x6k7GNNSqxSm4FUUeRY5dYFLo3VYcqKLSB9d_5dpIeXjW1tovcdm804K2S9OKdLC2ps6jYIOuZiJhI5_SE8sd5iKvDz5lK4OcZguEpI3aQaIzPpuTs2wRfM5faSbUCmZT2dY1o86qGKtgGBkchmK.2O0V9oanJDPeaEkFkAGbDmA8KHzDGf1sEw-%26lp=http%3A%2F%2Fwww.birchlane.com%2Fgateway.php%3Frefid%3DBLYSTM1.31700964765%26image%3D27003935%26url%3Dhttp%253A%252F%252Fwww.birchlane.com%252FShop-The-Look%252FLiving%252BRoom%252Fspace%253Fphoto_id%253D209443%26gateway%3D1","publisher":"Birch Lane","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=1tCEapYGIS8fqT5Nuo4kyz5fa0UXVMD31y6TdyxgMeKKJIjPjujtE96g1qAJCmz1g6nRclWcaNxDYtWYMAvd.Su3EMflY8R25KRI2SIXHKjdUKfxiEc3WXDuEDLGLge9KIhPFgcf8eoaHnAjqbEnzJLeNSp3awwagV.5vCpX2JajsPWmZ_eyQtvdMPlaBwHOTEkB_KIWxkUz63c0myXwY7ir_bMql4ej_XgCJAdrSHxf4pXLIzxP97zjbbyiJRf_OcqOILVIhotuLhm9EMeYuWMWkdZRn6ClNsMDOhF9M6xLDk51IhWaOnXs2uGkSfvU__BKNwxZUpmZ8RztCpN7fvjVcOrHTENBw.7J1k.V7o0zsgegRQVQSOjg7u8-&ap=23","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15njd274b(gid$0de98d92-d01d-11e5-9b07-008cfacc4628-7f1d99642700,st$1455125891097000,li$0,cr$31700964765,dmn$birchlane.com,srv$3,exp$1455133091097000,ct$27,v$1.0,adv$950319,pbid$1,seid$4250754))&r=1455125891097&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/6bRKN7wp5yFer.9oMJaGlA--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1454617940519-5883.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Shop this look exclusively on Birch Lane. Classic styles and Free Shipping on all orders over $49 help make decorating easy and affordable.","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":3,"cposy":3,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31700964765","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":2},{"type":"unknown","id":"52af2a5c-bf4e-3d72-b649-95fe437cab8d","instrument":"4000024250S00008","is_eligible":"false"},{"type":"unknown","id":"0147380c-1fee-3f9d-b5d1-a10e965a470f","instrument":"4000021120S00008","is_eligible":"false"},{"type":"unknown","id":"010d4c73-7899-3d81-8335-0c26c16f6626","instrument":"4000014700S00008","is_eligible":"false"},{"type":"unknown","id":"496475c5-7d7c-3805-8593-d0eff4002bcb","instrument":"4000004870S00002","is_eligible":"true"},{"type":"ad","id":"31590019210","title":"Plonge dans la plus grosse bataille de ta vie !","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=TjipX_UGIS.BJ.twQvT.HFFpwF5AiWU2NhE.yy6Xw083BSLDuYkzUX.1Vz6Vgfxs72Up8wZ2DaG2BxMxfZX_vryTRnq0wD75408mnIUiMgAGTZDEZSljWUuE0vA2qGHnCqfMK1x7Cm8AjJ7Ak8vSFX7dWq2Lnj6yeyL.4uH7JHhhFzdBqn2M50q9VHubBSH2E9oE3qqP2IaIVkpaNHj1HPK7AxXDIpgMvTlGTihA5q0iKoV3poNTuN1QtiJM3vyc3lbVaYPzoLDWwIqHKDUD3EJzIrY5KDctgqwpnYX0ask_727wqc9hmIaHALkgEpFVAgKL11JBXm0QikTFCGBNV5I3pQXPOSG6XSyjCGfMVu._qaCVfc2kZtSEyF3Esr8iFAYSFDj9zhbu1gMmhFWydONbLV9vOXdujGmJSKR9azTPYbAI2beEKnF6w5pUZ6VViR7GXHKrEb2pwA_8fhmxTNk91t70sgy_AfHm8UnIK2LhxluVY7PnRKRbyPnti0fVZo36JKjNO2c_UK_G4jKFJUOYC9Ql8sNeg.PSpcvgPPsNY9SlwoM_rM5k6Bc9fB9bwtKO2Q--%26lp=http%3A%2F%2Fplarium.com%2Ffr%2Fjeux-de-strategie%2Fsparta-war-of-empires%2F%3Fplid%3D82731%26pxl%3Dyahoo%21%26publisherID%3D%23sid%23_%23creativeID%23","publisher":"Sparta : War of Empire","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=DhjaYzMGIS.UFuqBMbB8OXZXVDmg0kTDbzaByJIB4X_sb7gptvksWY_CCYGj6pMade400s523Z6x_B0cXuyglYIANf1ExH15nU50UBlvAKiJxff0mc1pDEsXa0yMWi3Kx6hqTBAuGtVSbbEgpL7XMBxzjG5mad6vYmZOBpL6rMwpB7WlH_NpKzQ1Cop3b3741slcs41s_4KkCnNE3JoamJuhKb6z8w112nbu2DTflbbgGgw..mIcIVH66RZ4K7s0p87tgOt1WiJGFtz6dO._1yVt2ZK055rhIy.We92IQe9Uv.3hnl8qSleGapUHoS5eTPue1q65NKs1mEXK1vk2ONUeJLIPEVFBg04A8QPOW0EUmeAVurzmWbR.sgn9miH3oD5N&ap=28","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15n60n78h(gid$0de98d92-d01d-11e5-9b07-008cfacc4628-7f1d99642700,st$1455125891097000,li$0,cr$31590019210,dmn$c31590019210,srv$3,exp$1455133091097000,ct$27,v$1.0,adv$1280969,pbid$1,seid$4250754))&r=1455125891097&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/pnD7UE9ORpKoXSP2Ef3TKQ--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1450170379340-315.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Le succÃ¨s qui fait fureur dans le monde des MMO. Sois de la partie !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":4,"cposy":4,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31590019210","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":3},{"type":"unknown","id":"02048a6f-12d0-3047-978b-9d5cad7a8369","instrument":"4000015640S00008","is_eligible":"false"},{"type":"unknown","id":"2cb515f4-155d-3329-b01f-87604c16ce29","instrument":"4000007110S00001","is_eligible":"true"},{"type":"unknown","id":"c-3345f3ce-f28b-4fd9-b1b7-efecb30f906b","instrument":"4000008310S00003","is_eligible":"false"},{"type":"unknown","id":"dfdf2121-b1c8-38cc-a0e9-6a4b7ef368c3","instrument":"4000019160S00008","is_eligible":"false"},{"type":"ad","id":"31710582160","title":"MADE.COM : Vente flash 72h seulement","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=xPNcVBIGIS_HyrRfsx6DLL4wbgEl0UpUCcOrPCsO6X98Mz6d0XT8dd40cwizDgkbI.4clzYEUuQZAqUtaFl8WQwac62h2rJMNbsQ5WnRcMIXdfpEcvV74NHkng86B9AFDx3ZPPsVuZUiWEK1Vuz.6zKSYrbwhQjaCJ93LRc4_UjWmwVZH8WkfuPW4z5dVEVBYpsM3RB_GE3Wx3N_gaqPEJ5ypOhmHtj4wMILqpKMvPLrFCHpKfc.PpRHtg8cITLTWm6YULvl_m9bGcSHuDhc94o_A2THZkD2l7uOygfUP2WFXCvIjdbKE8Ol8lkx.HGZXJVba.864tLemz6LPrlsJIIP_JzQOzxmlloucCVgoBS_VZciVr6EByddpfAttyVvQ5EdQkD7NjuoXS.5zciYlifzOLEo8qjKWOZj5Gq3FoOmYQ7N8KWsWLVH0VD4Q55LVknrrwt2QYNDW80hR7l0.ImmjRs7qkGU570Y11Pj07ka5var8G1LEa7oVmCLJ2V_CEbiLbokn.aTNyAtl9U9cAoUINbJ9P3k9YXpey5J_mMXXbQOAdpFi_Mnd0lou07sE2SPVM0emCrqb9n5.ts-%26lp=http%3A%2F%2Fwww.made.com%2Ffr%2Fsoldes-meubles%2Fventes-flash-living%3Fc%3DYAHOFRDSP%26utm_source%3DYahoo%26utm_medium%3Dcpm%26utm_campaign%3DYahoo_Native_flashsale_living","publisher":"MADE.COM","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=R7kVlvkGIS.IYYvY5hREKOhMjhAw0GIISz0UTVrVbSItW9iLjONLks.OYyyjTRABuNpBK83B33E.Pk_HUV6UDuBwuronL5ZF4IsejKjhVWXzaIhPI0ib4U7kUE2AFKyc9Cxp_BrM7nMRGF_VU2oMN_Qhk93nJGZ6TnXi4euEvNHBxRCStlJyR49Q6IUX6MRpElhZQ4z12PsJQs8iZ0.5u8QALakdtAmRajL5dCU7POo4nd8yUKP1nK5Kzcs87muJJSOb0SeONE3LnVvuER_NFPSSfbMd0Y1AwMA3BWUrActLREJA3Xw2_fCpsmuGEF5gcr36qLeBOdOPlz9hfL2W3q9QYDm5YfXhNskNNNADKj7gTzdZQnPIU.HAn74r.hxqraVRamipM.d6WNw2dv8cQ5E2rwDRuMYq&ap=33","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15io2ug5f(gid$0de98d92-d01d-11e5-9b07-008cfacc4628-7f1d99642700,st$1455125891097000,li$0,cr$31710582160,dmn$made.com,srv$3,exp$1455133091097000,ct$27,v$1.0,adv$933182,pbid$1,seid$4250754))&r=1455125891097&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/KFnkVmpPJb0gC9H0waU8RA--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1455008932706-4350.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"72h top chrono - Profitez vite de nos ventes flash* pour vous amÃ©nager un salon design : CanapÃ©s, Fauteuils, Tables basses...Tout y est sur MADE.COM !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":5,"cposy":5,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31710582160","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":4},{"type":"unknown","id":"1a391554-47eb-38e7-8a73-3051d28699e1","instrument":"4000022580S00008","is_eligible":"false"},{"type":"unknown","id":"7c7bff87-6141-3029-9edf-8d29c25675e0","instrument":"4000006960S00001","is_eligible":"true"},{"type":"unknown","id":"3c2ec264-7dc5-3e88-9353-0924fbb71e1d","instrument":"4000015990S00008","is_eligible":"false"},{"type":"unknown","id":"bd2d560d-3b6c-32d5-822f-495b1d955285","instrument":"4000007750S00001","is_eligible":"true"},{"type":"ad","id":"31156552467","title":"PropriÃ©taire Ã  Bordeaux avec Pichet Immobilier","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=_yi9UPYGIS.cQ_XkeEjOvKDwyDe9WIo93UP5XKbmHyf9DL7fQKNFSR4lmaJU0plVsuP8UtQsOMP3C1QSZTbEuExwmQfxmLU39GJehXCLkd99RkHwIzXIysoePmEfis5q95ZQMY0vdae_ZXaK_8gKPAMURBV2DvSo6tL28W0HZFfgyMoLchHM1gVuX7mmM32dYfbPd3GHEkPHQPrXzxYTYpk3W0lI6osp4SSX4bCfSBNPnVOyJmT5OuJXj655edMugdritRxs15NbnLGSwpTgbr09rPnke7BLAATo3kLHd7wXLmE13WGAt0HTLVY9QDTnTdpPLJFx.zn0XZ2LabLfvHJMrdk.V1pFAjyxzn4UiVH7_HZRPVtca1QTqfD4O937I0vidHgmRbvYk0QyMrfC.mQNGVDcqpbbPhok_bD.yrALqAnDPTCfI2YjV87PyXpEfsWu2JrRITXGbYYlS4sYhPOBknIGOxvhxe8gkpXyKkarT7DB.dGMJb0MDQ--%26lp=http%3A%2F%2Fwww.pichet-immobilier.fr%2Fglp%2Fcub%2Fbordeaux%2F%3Foid%3DPI15REC0711%26xtor%3DAD-3782","publisher":"Pichet Immobilier","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=mg672_0GIS8cDZd97o4RMZqm1SojlFngxee_HO76DICPXnsHDaZAyLPLDxceTdiV3Za0JI2QXb3zrff3vLaL4TshPaonti6OGI37RrnqHOTncAhNjk_b2UB6QXnBXMQ0pvsdOK8e2YNxyAfsDdrUJWiXHQy_lZ060gKHQ40D46myCN4vmlXHNAA6iBfAHU4Ew9Oxb8AGe5uiepirEPRghHxXR6pCr0waKXcW2SqG52vxVPtKLh_Jm1wyiRK6oVEejm.Utp5aF3p3cGY3mCeT6dnDOc471Ga29iNZa8l4ddWRUPuJV5jt6en_mTQxec1.RWIys08HfVKDc.Ul2rpSA.zHPCmZXMxXlVQzrIkDIj_LfyDRHmP67iYeh6XlQMFl9196okIu52c1Hk2RbQzd__gv8lZvQREyE5QZqac-&ap=38","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15vptc7uf(gid$0de98d92-d01d-11e5-9b07-008cfacc4628-7f1d99642700,st$1455125891097000,li$0,cr$31156552467,dmn$pichet-immobilier.fr,srv$3,exp$1455133091097000,ct$27,v$1.0,adv$1022549,pbid$1,seid$4250754))&r=1455125891097&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/1U8XA8JS_OmZuLQ.sWvtkQ--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1445594677778-3518.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Le spÃ©cialiste de l'immobilier neuf vous prÃ©sente ses offres exclusives Ã  Bordeaux. Emplacements & prestations d'exceptionâ¦ TÃ©lÃ©chargez la brochure !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":6,"cposy":6,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31156552467","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":5},{"type":"unknown","id":"c-7b5c5ceb-f6ce-48d1-84d0-de8d11f903c1","instrument":"4000007350S00001","is_eligible":"false"},{"type":"unknown","id":"54f519bd-175b-3a8c-83e8-1f0652bd0559","instrument":"4000014990S00008","is_eligible":"false"},{"type":"unknown","id":"2d98a5d5-b1c9-320d-80a0-45d2158084e8","instrument":"4000002040S00004","is_eligible":"true"},{"type":"unknown","id":"6d19602a-2a30-327b-aa89-18a9b0e16b39","instrument":"4000014240S00001","is_eligible":"true"},{"type":"ad","id":"31675877762","title":"Soldes Habitat : DerniÃ¨re DÃ©marque !","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=sHk3DT4GIS_w4u1xZ3G94PNQqCHSIIjYllx.33_l9CWNH_UgAIqHIVvSwlbWHs40q4YhFICAed94gq4F.gIy_clq1atbgi72QlYF9OhsEp2ZB4Xf31D8UvUJS3Do.rj2IBQjhkUdoyv8vguar1FK0Nehl_q8jvoN2.iy062dGuOxeP69nG4s0b0sqvIRE5zIslIcifuOQ8aiQnLaoUwbGsBnEo1TK_yEqgIG.._Su0bI_QOuUQa02x8CMHTYAMvvF5uqggwJ8bO4f0be_5GeFwMsED8F5hNTxvfOyIZPNeEO.vHMk92tZ2TgRrdwAZp5B4lkJ5mdzHCOOQWuzBFxgHWIXT64gSITAgNyOV4JhcRIcXYOMQy_swtr3GPPDkKhmYuQK3lpfDraA3CTevAMs3JGm3Ck25KDELbglZ3g_mQvVuZj0LjILPvVcqJCTTXciN82zojljeRvMZbKUcAMOtp.w6GnFh5dcgEXloNVLtahsprCeSz87kGhZXT5gkf9Xl27wECcLG7RdsW5BotLBcxvnfOfKtx9_f7z6DeVLmIa8AAadb0sni_FdT4GLnHfLt.POdzTZlPYB9dG5_rF9PghLDD9QqyKqfPgJu8lFqF6eUUNNo.i.bpvGp4I7kSmraq4IP5Wg2cnhpvBnwItiOnxegwdrIeQfo8g6fXLaUzN_CQJWVRXqa5yBDBT0IlAG4BuUwNu0xhmNrTygCSCRcihET.k%26lp=http%3A%2F%2Fea.habitat.fr%2Fdynclick%2Fhabitat-fr%2F%3Fead-publisher%3DYahoo-Stream-Ads%26ead-name%3DSOLDES_Mobilier_Maison_Femmes%2B35%26ead-location%3DMaison%26ead-creative%3DSoldes_nouvelle_dem%25%26ead-creativetype%3D1x1%26eurl%3Dwww.habitat.fr%2Fac%2Fsoldes-et-offres-speciales","publisher":"Habitat.fr","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=wsKuzAYGIS8JisYw1GNvv.Lzw_WKv5xvwgSK3XpVi6pI3QT_Mk.n93k39vgGPrUQQionHDHz71BFUtr8RUiiahTXI3Et8IIj0GM5bkcNRNnSw8eH0r610EyyGC7TKzNb2ugDfS1uhIn.uyAvQGYlO49ST3iLPijer1T35Irj7kD02DS3rhtlboQtpHN8bOvjUnQFK4oEuv3ZhM.KifS1cxoAyGuHukrewkEaM1c4A0qoexCmJjmAKi1vXHbWBLSWH2RHabq.lltFu528uS9XD3pD6yG4PzNNxc52IbEqO6RviZjLXzJgx8ynQMPG3L77lxKoETbRY04EO2loGDW85ElMa.hGlmCbm2vGwdrz4OffwgblcCBMWbCReVhtnuZazPw7viP8i81RaLbl4Y_wtYzzFr6GnF23&ap=43","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15mhr2rr2(gid$0de98d92-d01d-11e5-9b07-008cfacc4628-7f1d99642700,st$1455125891097000,li$0,cr$31675877762,dmn$c31675877762,srv$3,exp$1455133091097000,ct$27,v$1.0,adv$946852,pbid$1,seid$4250754))&r=1455125891097&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/cXlLin7r76tdHjQTdQgpKQ--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1450890916924-4359.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Jusqu'Ã  -70% sur des centaines de canapÃ©s, meubles, literie, accessoires, luminaires et textiles.","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":7,"cposy":7,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31675877762","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":6},{"type":"unknown","id":"e16425b2-1e66-3c52-ae73-b05dbf23d3f9","instrument":"4000023260S00008","is_eligible":"false"},{"type":"unknown","id":"ee35a247-fc27-3aa3-aa62-507b74d75e30","instrument":"4000017730S00008","is_eligible":"false"},{"type":"unknown","id":"d3b7188a-390d-3d49-a879-30ac0a0d5912","instrument":"4000008149S00002","is_eligible":"true"},{"type":"unknown","id":"f58fae5d-14f7-365d-96d4-3ee29ee980d1","instrument":"4000020300S00008","is_eligible":"false"},{"type":"ad","id":"31352200037","title":"Mutuelle MMA : Ã  partir de 17â¬/mois !","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=PCWeyVwGIS8cgR4j4FvknprN7PyoNc9hjl6rnLhpWpELj8KAwsMEei4vahsGmgOnMHgK_hCONpNi7m9LV4kbVfpM_rZceeXoAxtqOh0Ck6xLT1UQg3M9YnIEW2yntEGnW1Gl30wPrjek7eXT45O20zu4zXAU67ObZ4Xmm9SV8L66KT3au30VzQPXq0WVLLcaq6KyOatK3guLHwrHbf7B.2kuB8hXoQe70C3ImS2MsIMAMkWAhviwgSWGtB3gPEPHpMf6emA4MPepIqQXGI30UW3R.bT0uKy_MIGX.dLoafx2bE_F4P.hlfn9_TxIG3uRc02AWBW3PGaoEfE8A5DBctRMgDyn682hzAp2IBmgGFJEWbBZAO6ri7F2dIbF2k5eD3FgOXEFNwcTH92Kh0jz07jh8eDHH6q6EpxWIQ_rsx1IB9MobvPPQ68gkyLyOjW82ClKO7hafYlbxSP1RTxDJizX84Puk_EFv4OMN5zg.EuKIE0fhMaP.gcwoR8DKkzYvPdYA1.5jCAKb2PkZ3jFRDHxMakOd7oJkzGTDV6kF7WgL5EhlEgyu7VImQHKcAQoKf6bD2Ois2zDqsVvR1.vDgXPxND_uc8fT.qO8t4XTHqWCVF2ZWdkArcbK.AzsxqHtihFwvqzKnl5P7fQhO2H55jO7bpUNhpAQcqoLWtsbB4sGsTalmoE0uOoBVkHFyuIeUlWoex716kmxe4ZxogabzlWU9lP%26lp=https%3A%2F%2Fwww.mma.fr%2Fmutuelle-complementaire-sante%2Ftarif-devis%2Fsante.html%3Futm_source%3Dyahoo%26utm_medium%3Dstreamads%26utm_campaign%3Dcampagne_sante%26utm_term%3Dfrvitale1%26xts%3D154037%26xtor%3DCS6-32-%5Bstreamads%5D-%5Byahoo%5D-%5Bauto%5D-%5Bfrvitale1%5D","publisher":"MMA","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=eVNMDpcGIS9V7d0PTj7hSjtO8eIVSw4r3HyKQevMDffty96QlgKn6jg2S0jgxa7V.SrGYbxbH5ietbDmrnr67tDpL59i_PGUUm2n2qrtnvrbzgrvl.I1I3VjEd_HQBCIqqty6CnfnrbK3.IAWj3oPELP9pX9jS.GeAlCf.NEoU3dGMDJHap79EeRLn2uGnGJeC.BtHOIY_gobl.c8NETOBWPkDm.Iz2wsKYXHw169nUMjd9ZcYylJLdz17SlPJEr1kHBOat_Wptr_TqOKCJjwN_ubEmAlwQBAav28mL4UDowFSuZmnDGKVcJ3aaJ3qxnDApTLlttfsY3kEURPZkL9BXViqQbhpILoGXfzMVDxXGk_gCAJQ1koKf2e8yc7DNUVQ8WF5_jstrP&ap=48","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15hv9act3(gid$0de98d92-d01d-11e5-9b07-008cfacc4628-7f1d99642700,st$1455125891097000,li$0,cr$31352200037,dmn$mma.fr,srv$3,exp$1455133091097000,ct$27,v$1.0,adv$1013623,pbid$1,seid$4250754))&r=1455125891097&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/A4rKVoXENaDRHche38Vu1Q--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1442592306095-1761.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"DÃ©couvrez la nouvelle offre SantÃ© Vitale MMA. C'est Ã  partir de 17â¬/mois !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":8,"cposy":8,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31352200037","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":7},{"type":"unknown","id":"530bf9a4-60d8-30e7-a148-49f18c38414b","instrument":"4000010330S00001","is_eligible":"true"},{"type":"unknown","id":"7afe7ab7-ea5e-30b6-b56a-ad83b07fabac","instrument":"4000007060S00001","is_eligible":"true"},{"type":"unknown","id":"fc6ed823-f685-3a23-9b3b-1b7a67170dca","instrument":"4000016180S00008","is_eligible":"false"},{"type":"unknown","id":"50158001-2cff-3d29-98e6-0e2d8dae6ec6","instrument":"4000012080S00001","is_eligible":"true"},{"type":"ad","id":"31710689005","title":"Are you ready to learn a language?","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=5CFSBT8GIS8Sg4mhF1.4qyZn6z0oU_pIs_WXwBJMN5Ar531tkgWFPr3Czg6CqmUArSqCw6_XLpcI1lST9JwSU9gxY3WaZG_2k1GIGfaYF6jlYz3YmZjZTPKbL7GhEE0jXwqSBRasxkAkBKs_ia0nYYbRRZFICAoVzzK0H2.7VVbKPRqcS28ldBiB64cUvAbZphUamjSNqmE7Sx6FvS_OwU2OVBHOav8zHHuD_1D9alu_6knG7ehPIDM66M1TV1IsVyhFAbicpUz1YUBF_egUG2x024q.Tk_pL9zS4qI5IgTNdQt9n5yQB_PsuFmtBL.C07n1dW1lpvQbpM4Q9y3Kttg6_ZKJZTXFjucouO_rq.eKI8F08ENCpl.lB.JNBouwuc83PeO3bpHEgdI6y22bE9.rod2.uez6JPA7Lw6NowbGRBSEKsSN6RVGwwTBWpJ1frBT%26lp=https%3A%2F%2Fgo.babbel.com%2Fc207_engall_cpm%2Fdefault","publisher":"Babbel","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=RHXKwAYGIS.Yf7v6ek3C4zgLKID_IHlAfJur7THHsICxvGoluv9s_gCfWFUy7sGPRQLxeOTHyukhdfWjFQAyCPueSH82wRFEQQQN2w1xnYWBNwPkg0hGbB6cixntRsF8yXrmRQKDTZS5SvUJ31P_vWZTMuJVuHCSEjdisMdOpneQcBmlA0qeTsG0VaAq_5gtU0sHJOhJY6OZ9csaJw7k7Vk8VB6pKALz39np3hbRYEeyGJ3Ffl5kp5ydCqpFjysj3RetfmbveIye4ELFQS1sHVG_kUMqiLWpqXYQtQOE.XX7jBopzKKEx78soPmqyBdYGhOvilbE0tzyUVJ4d6GKpzoHQKCN8_jnXW0JCa6urPossY3d5r_xZrDkCE6aur5TGoaeUgilTUsEhb9l9aZ0Rjm417RgZBM_&ap=53","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15n6ovqh3(gid$0de98d92-d01d-11e5-9b07-008cfacc4628-7f1d99642700,st$1455125891097000,li$0,cr$31710689005,dmn$lp.babbel.com,srv$3,exp$1455133091097000,ct$27,v$1.0,adv$959887,pbid$1,seid$4250754))&r=1455125891097&al=$(AD_FEEDBACK)","full_template":"ad","images":{},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Babbel gives you everything you need to speak, write, and understand a new language. Set your own pace - learn what you want, when you want.","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":9,"cposy":9,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31710689005","ct":1,"pkgt":"sponsored_noimg","grpt":"singlestory"},"actionsEnabled":true,"index":8},{"type":"unknown","id":"c-0da9e9e5-3d44-4e9e-9019-30959d2c959a","instrument":"4000003130S00001","is_eligible":"false"},{"type":"unknown","id":"282e4ecc-81ce-30be-bf73-643bcb245005","instrument":"4000017070S00008","is_eligible":"false"},{"type":"unknown","id":"9e885bdb-a621-3a14-8fbf-69482bda1205","instrument":"4000015580S00008","is_eligible":"false"},{"type":"unknown","id":"b46f33ea-5365-31b6-a6d1-3f3ac2cf0549","instrument":"4000010030S00001","is_eligible":"true"},{"type":"ad","id":"31666944427","title":"Rachat de CrÃ©dit JusquâÃ  -60% sur vos mensualitÃ©s","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=nQH003AGIS.wGdnLUz7x6PErk8uOiliRZGvkscCRy4W9pZ.bO0RfZCm1cQPzBB0DvSXkGnXOj5C4.j.tJpqfROluoIuSM8JCx_4LE35C4efT39s3sH_ipzFcIql1Jz7oS5viQNxYaCs9bC.xXMZ6wxcaZaU7n4IKUDfjyS0NmXzAomY0UiBD5OqmlyLq13I_pd3Ghrarcxpf5gvSMgbBaDrhVwMpE8boesW6fUfax3TeB.nbjEi4m1WXA3PvG8QxzfUCJTbHIq49l.l7Oqqe74DLAi8.mTLdF5H2zKYTm3eedPSZE7kAicTcogKcsM7EqjfN_MPh_dDBMatUlMbr.oGWks3OuUEynpGN6laoVLNbJ15ykcFFD4MOlx8_fkQgqrU17vL0fMtiu3CLk9YVKULUNtnCYsII3lnvS8OJ7p5wnTkosTibj3LTB4wYoi3tvrzzQ2eHAl.XrwKbAyPC4sRwEtVj39n4JLgxOFbCPepvrycK2UZU6UV9gsH.1y3ZeV80rog-%26lp=http%3A%2F%2Fwww.partners-finances.com%2Fformulaires%2F%3Fprov%3Dlienpromo%3A%3Apartners-yahoo-ads-pf-oct15","publisher":"Partners Finances","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=KsfFfWkGIS.zKKeLeL_CxvVu7Q9cbNEIzkkWGTyvr_7PM81xQwt62HRAs7dXF8v2RoONsouQUhbnntLrfvBzxbogOgGDICw3HP8S6KQ6nlhDbc6rpS65bWum1XD2WoirLKBTIpy7WpGOuvel.DbpaOAKLcMmfbjoUgkEWfnilN0yAzF3GBvm_0sguJKnEY8vIxUZJdTZHZOd3ws6UkshzD_xMlGtmVS0pSdFNxfW1zLqDCNvTQIX1bH9AfgeOA13ZHcARh6_RZ9WUIOzSCHgXMD.hYYDkzXqUz18tWnjy_wdwy0vd2ZeC6RipeDf7ZBqc9REu3Q0jRzJa.FgudqMCazumBeoddaUbvG2GorgA7OhV7eXoFTmsVHg0r52lqdrU8Ut4MkoshtXEX59bOcV4JTNTiStcijy&ap=58","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15m6004mv(gid$0de98d92-d01d-11e5-9b07-008cfacc4628-7f1d99642700,st$1455125891097000,li$0,cr$31666944427,dmn$c31666944427,srv$3,exp$1455133091097000,ct$27,v$1.0,adv$933642,pbid$1,seid$4250754))&r=1455125891097&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/WHEJuTBse4ege8pzB0twyQ--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1452851626498-6281.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Simulation Gratuite en 3 mn ! Tous vos crÃ©dits en 1 seul sans changer de banque !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":10,"cposy":10,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31666944427","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":9},{"type":"unknown","id":"c5f11e8e-4301-3e3d-9958-738e51f32e0e","instrument":"4000014470S00001","is_eligible":"true"},{"type":"unknown","id":"9b4e4bc7-e385-30f8-b458-b9973c4549a2","instrument":"4000008600S00001","is_eligible":"true"},{"type":"unknown","id":"6199dce5-b9c3-370a-b73b-4b58293d78c5","instrument":"4000007080S00001","is_eligible":"true"},{"type":"unknown","id":"4bd923f4-ca99-3817-8645-df6a9a76768d","instrument":"4000001150S00004","is_eligible":"true"},{"type":"ad","id":"31687619321","title":"Votre complÃ©mentaire santÃ© dÃ¨s 8,10 â¬ par mois","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=97Vki2sGIS.83b27Qrq7EwSh5JnNnk5pfus_rUC8x_p35pWJSM8mQBklX_rRfyCAXSfhaFi3BRfpWeGVtpuvTBfvBmJV2f0n_UIDK1yuhKicnVxfyFJNOM45SeDDsR63IQOXNOOBR.agE4DLmtA0ODu4zgvztjCaOB85soF4OMSdGMYxPNO7TEduMcizlZtEPBydYDbYr54CntdfexiqFyVlv_53XQ2STVMRCCkDRDhHgPAzjjZIq2CM1fjvGQet5Q_i1EVZPGo1TQqPy1EF17MzHjNGqHa6IBK3c1GA_JxQJvIn1Orln3Z2_jZh0gZvM47cRDWF9ja1EFpO95Z8NgwexlDie7HymRrozunlmZGKLTXAUP1y7tjVa2qEn6WaYpuAvE4.KTGTLMz_9unK4_clzSdlDqynriQTlIOyUMpjwvPUNhnm5qTdaBgu77DWrgrdfJncxVAU2TZdVjIzgkGg3Bo-%26lp=https%3A%2F%2Fad.doubleclick.net%2Fddm%2Fclk%2F299229867%3B126242518%3Bu","publisher":"Axa.fr/SantÃ©","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=dqjMTIkGIS.utxapezIY8HMR_9KeIwrHpdkciW6kvaAb2d7RqZbw5FItWNvDJxi1DCU.Qo4OON8hSsL1k60VSCkrGnw2qNcdPJjc4s3p5SIJirtpyMZVN5R.rgAJY6Pw3iocSMhOgxAW0aNT5OflbqeMUX9RP3uaVLJ697vC.NeL2WdCH_va1U1wrVv8q3btrusDYwcRDVY0iZt9YFPzBxNH0AFBclrsd8VXC6kEK_ZeD2dOspIOMhxR1_.EnSYwgkKx0w3LaW9dD70dvEKG.yOhpKmbTEyDSIX94Fn230PYnSPZB6mPL4Co1EnOawLX7iqJlRo_oHGwqDb10RC_AdgbD7Az7VjEcxaQlb03vmqCGDFf8MqmppnE2MCbIgvBgcgqyTq6v5E4Thi4_ZMJyR..Xas3YlVN09dYVQ--&ap=63","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15gfhmmpe(gid$0de98d92-d01d-11e5-9b07-008cfacc4628-7f1d99642700,st$1455125891097000,li$0,cr$31687619321,dmn$axa.fr,srv$3,exp$1455133091097000,ct$27,v$1.0,adv$984869,pbid$1,seid$4250754))&r=1455125891097&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/8PrC4mj9LQPIciIZ5p8JLA--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1452592646249-5474.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Obtenez votre devis sur mesure en 5 min et choisissiez une couverture santÃ© qui s'adapte Ã  vos besoins avec AXA, votre partenaire santÃ©.","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":11,"cposy":11,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31687619321","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":10},{"type":"unknown","id":"bffcb972-1685-34a5-a9b5-eac9fd90f8de","instrument":"4000007510S00001","is_eligible":"true"},{"type":"unknown","id":"08e08363-9f4f-3599-af4a-347999593479","instrument":"4000008500S00001","is_eligible":"true"},{"type":"unknown","id":"77390e4e-7871-3563-88a2-98a143040f76","instrument":"4000011160S00001","is_eligible":"true"},{"type":"unknown","id":"eb02014b-6d20-3a3c-90dc-a1dc4e231f1b","instrument":"4000011430S00002","is_eligible":"true"},{"type":"ad","id":"31687009400","title":"DÃ©mÃ©nagement ÃlectricitÃ© & Gaz Naturel avec ENGIE","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=EH6wOEoGIS_1WeY7W.c_ltdXto6gjkk2k7Bh1udJs9PF0td7a4XtUKG3SgjrEBnR5GIdxXT60dwXIhBs6aCdmqGO7UDx3znVpFYa632qo1jckQog1cg0iZPKZtQFVtAAWZwIcTnpDB1etmZQT.6VQuM0NbTP02rXya9_Cjxg6PdVXukU98XC0.wdNZu2oQtPITls632vaUMn9P1dNRnUlka3Jz8iTfqOKSe.IVcAX_PgfCA0tua3jS2gyg4.EF9FWa4yDTaCiXhVqacx0FM8DwluBSGAPVs2hrpSfZbK_a83SHfIxjF8E2YmVTtEo.ERiGHGBiGLVo6XmfGIkQr9ub4vTO69fudtPFOlP_DJxVW0BJiViDm4LEvG8p9oLVCqrkv9VssbnrHzKIIKU0JXXmPkFmTyFT.WyzWDVDJ.LSINaYQbe7slVg9D7beV12yqb6BrW1Q_HtrRS0iBNa2B1Bb62mN2YhKHMxINn6PWbm.R9IvjlQ7eaDKkKaeFVz80NR1zTjoM7L.Tshv7r8DCJPFZDTst9OEUfjcrYZeQllN.GOQrPUnI3_7BZ6p3Y.GzHZURoOa1W.XX0vfP4tCYwNxQUVtv57e_Y.HKrQl3.MxnT6oBRjuCfAwn1N0m.AdDrqR6SwKX4ZA2wQ--%26lp=http%3A%2F%2Fsatellites.particuliers.engie.fr%2Fdemenagement-gaz-electricite%2F%3Fns_campaign%3Ddemenagement%26ns_mchannel%3Ddisplay_native_prospecting%26ns_source%3Dyahoo%26ns_linkname%3Dgemini","publisher":"Engie","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=9WjyljgGIS8N.V7PqAqbdNI8L3m3lG3RYJ9BS_HkPNAtHJLcbzhJ6FxKZiQpQN7OKXxYWoQPjzSHpKPpkxmVmjb01IPgvm3sr5DwkKmoBg0P9Da_IkUa5P9PfMNR.WdnZPz_SmvxSWircGQv0pgjnClk4mLokezQV45BFyxxiFbC4NHa1poOBlJc_ayFLSmuiX.sUQMmoQ9quKkyR52rTgEx8RpSxg43OXD4hMzHG.jVNyvkR1X9bYZk3d7gyYiLbHFpPSKlS4FsJ4UrJcjXz2H4kXheOkd1I_1RoqbyWB0sqDcOnscQ_W7sJcxg8hqMEyXQmGe.JCJxAdUdkBycfGfDd2NY8E3wAwb1YpYXrfmC258OjF.69pN6EjM2P9UqJX99kzaodOfI&ap=68","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(16bd4aj78(gid$0de98d92-d01d-11e5-9b07-008cfacc4628-7f1d99642700,st$1455125891097000,li$0,cr$31687009400,dmn$satellites.particuliers.engie.fr,srv$3,exp$1455133091097000,ct$27,v$1.0,adv$1135769,pbid$1,seid$4250754))&r=1455125891097&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/O48Zr0BaGQlj2a44lC1VbQ--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1452843802937-1264.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"DÃ©marches SimplifiÃ©es & Rapides. Demandez votre nouveau contrat en quelques clics, rÃ©ponse en 24H ouvrÃ©es !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":12,"cposy":12,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31687009400","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":11},{"type":"unknown","id":"9c8e5101-42ff-3d8f-bf32-b5c08c80e2c6","instrument":"4000015850S00001","is_eligible":"true"},{"type":"unknown","id":"61cb3cf0-863a-3564-82ef-09025ba29b2a","instrument":"4000017480S00008","is_eligible":"false"},{"type":"unknown","id":"25fb5543-b33f-3415-81f5-2671bc19bc92","instrument":"4000014160S00008","is_eligible":"false"},{"type":"unknown","id":"5d04b680-2e6f-3606-9ab7-3685b3bfb571","instrument":"4000007870S00001","is_eligible":"true"},{"type":"ad","id":"31224817868","title":"Brico, jardin, maison â¦ JusquâÃ  -80% !","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=y3WxzmcGIS_L2Cq7oUxjfTSYJeY_HhZ2J1IENUkFsApl_KOb3..oT4Kb88201eEQnsJFnl7zDMscjQw3XXLbB4fazNcOe8dLGfdMKFlTlN.sQJu2L6STljzqZm34ym6TlvMjlMjdLywfX_l6rnDy0DZRyA0MxYeoAlQgSVBNa6LRfNJAOa.BplHCUSS0rxj0RDEeqYKjCJvN26imhr3JfyK6O.uDb_aGkQgYbnNbDUK62BklFX7.Z33IhuElpbkIPxVhtohey90xC8FYd0GPMxmiTroCicnw79S3LknIqBPGRcJKo6vwZE1L1LV.Rwq4.jA29tyRKHWi5vbsBktCDb7SDNcE9VXPCDuuyLaibYIhxJMBqplbueAm_39eLTo7CM6NmRja9A_HKavpIJarSk.uG66Zr2g4hQL8BbycIXQFr5siV.LSYlLfOQgSL6QmYqqN73H2tnyATcTsq8G4QsZEW5MRLQbypgMZydLQSdY8uRPBfLeR..SZxciHC8YDpjDsl5GO5OnnZsrSfc7XOtkKcUTFVa2dAAKM4RxsgJX07QRg1juV3h6bvaA21ndPC27b4Vh39yKSqVCqZ_hFt7l2tZll4txDddl74kziFHAypwTV8ZPGieyodyAYf.2AZ2SdJ8O2aY8YTbAjmHltiwq0TUlPeqwv5QwAidCRlksxNAINJ2nS7tPZABpUHH0-%26lp=http%3A%2F%2Fwww.bricoprive.com%2Finvite%2FMjAxNS0xMi0x%3D%3DQbvNmLlZXayB3bjlmciB0bjlmcitSaulWbld2bvhWY5tybm5Wa%3Futm_source%3Demaildedie%26utm_medium%3Dcpc%26utm_campaign%3Dyahoogemini-brico%26display_hp%3D1%26register%26create_account","publisher":"Bricoprive","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=WEdMjLsGIS9F8MPOoJSNfP2G2usQV9f3eLdKkeY.Yon3ZtGEwvAkMWtmxRh7YIdUn9Y5qqVTjoT5n6fLcH0lyqmPIhRAoTaAgScwI1vLrm9kXhE89NRFc7ul9lEAZo1Rbs.vjRfWXBHZrcTQPZ8x7UySbVmmHBdC7uEp_IX1TzamRepCDvEePggAQQ1XVUq9p7wPckB8iD49ZRp.OoLZHoSfipSYDkm.RLOOqCPnhL6.SVgBQIH_EJOr9ga5fHqM1Vs3j92EbUweSoZH6iYQwsiwwWcd2Zs1cUkOxMCKrrn9WGC2VY29evDsnkMrQlcNeNKk0_ZOz2ZmiS4JdkdYmTOufrwdg7iRfGVdNa8buuQ7yTqZaHa7c4NlS_1exlNVZc_riLDYc_s-&ap=73","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15nj91nm0(gid$0de98d92-d01d-11e5-9b07-008cfacc4628-7f1d99642700,st$1455125891097000,li$0,cr$31224817868,dmn$c31224817868,srv$3,exp$1455133091097000,ct$27,v$1.0,adv$1166620,pbid$1,seid$4250754))&r=1455125891097&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/GElxgZ7S6uEQdKNHw7fVKg--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1446119284479-574.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"40 nouvelles ventes privÃ©es chaque semaine, pour rÃ©ussir vos petits et grands projets comme un pro !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":13,"cposy":13,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31224817868","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":12},{"type":"unknown","id":"87bd4790-cf45-11e5-ae73-509001aa8f57","instrument":"4000002820S00004","is_eligible":"true"},{"type":"unknown","id":"29685e6d-586c-3362-8d46-d51ac8f77860","instrument":"4000018710S00008","is_eligible":"false"},{"type":"unknown","id":"cfc23e45-d47b-3d18-b2d7-dfd596884751","instrument":"4000009430S00001","is_eligible":"true"},{"type":"unknown","id":"c-7b23dc75-96ed-4595-af95-62dfb2ef0de8","instrument":"4000001490S00001","is_eligible":"false"},{"type":"ad","id":"31672352908","title":"Faites des Ã©conomies grÃ¢ce Ã  votre assurance auto","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=oibu3jkGIS8czLjNHds54C_6W68JZrVLZ92qL4lnG5aqMfhi3mfRw0yw3.OFzIWyv8DiaVhQZDEebjvBfX_P5Lv452Wq.WL4VMr4taXhDYrlJWOb3.pZ_xS1JKDR6sz4Mfo7Lft_Gl1wmwyAqc5.trhlQSfpdAVtrWiGgexBbqUmCmkxc4uTaDADXsvOqRet5YvlqdqogBA2Al2_Cv6dYnyWx0c03j6wo5gJX6eogh8A3NE89g.EkCl0qAcc4pef_Lb2xAusCrJ58qfiVd2NGpwnHl35_Ig.zqBcRNbd_3FDbeypfgUtzl.CgpMKJB6atIK9RX0h2RqxgRumJB54H0UCzJzhNHIzzwL8MDm2Fve4uec3GD8pxI3XW_dWzpSZzaVsenKXppqES.agSDtHgF_j.6Cs8s4O7vDsuxtwntsTiE0fwp2KJFkAco4ESTRhGgR4ImtW6Qy_gJVTdkz.3fTvTrRJPscoFP71O2mu2r7_Mc9gGEkHQ97V5ac2Fh_d_gby0vgOwuI86CjkB6S5DnKH07RChcQacwALhKezjIKA7ACMU_d98Y0PFhf4oVfzuNMrdh72n0gUO_4bCfyXVOPpqmHBDVtBJWDbvX.8st8gmoPsHaMcJeHYvf3X5XT_7RKjxBUyZzarxfTvyMz1nd6j%26lp=https%3A%2F%2Fad.doubleclick.net%2Fddm%2Ftrackclk%2FN3021.140905.YAHOO.FR%2FB9375649.127505902%3Bdc_trk_aid%3D300530271%3Bdc_trk_cid%3D68290868%3Bdc_lat%3D%3Bdc_rdid%3D%3Btag_for_child_directed_treatment%3D","publisher":"Groupama","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=lmE06XgGIS9A2MsYD5XHqEHCSPdbxICQJM5lLx0UIR5ckOLw7bqZRBrkGzWtF047957hzHK0e1I8e8reb.cpYPLMrrwB7Ei6hat2xc_B0G54cpcuDiG4kpjW8Itij8mbC4gvzMYP8qM0_PyerSw2IMSK4kqHs6Cgnd2jmx4Yb_KY5ujywTBdj6U9DRFd7kTk5a2I.np7Hy85eQH4yxzGshHQt3oXTDFAholgzdmWd9dIPEHBXtCfElD9nqBDKxLvTrmGkYi8CRWw5YLXge6lMqa0E9uXo5zMvx5Dv05x0z.xVNIYJnexNO3mx42Kdd54vaO9HrzHpEyNRgcemEvKOGPcLC.eBLMBEa4V7KSwZ4q_w22sn8oVwdtdtk1ylv9spP2.0LM810vevQA-&ap=78","imprTrackingUrl":"https://ad.doubleclick.net/ddm/trackimp/N3021.140905.YAHOO.FR/B9375649.127505902;dc_trk_aid=300530271;dc_trk_cid=68290868;ord=1455125891;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=?","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15nruk2a8(gid$0de98d92-d01d-11e5-9b07-008cfacc4628-7f1d99642700,st$1455125891097000,li$0,cr$31672352908,dmn$c31672352908,srv$3,exp$1455133091097000,ct$27,v$1.0,adv$1127964,pbid$1,seid$4250754))&r=1455125891097&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/.RsV5wbRqXDCKETeVFUhHw--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1453137678600-2231.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"En passant chez Groupama, les nouveaux clients Ã©conomisent en moyenne 149â¬ sur leur assurance auto. Faites votre devis en 3 minutes.","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":14,"cposy":14,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31672352908","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":13},{"type":"unknown","id":"b2923a27-5c99-3cef-8f2e-aea29e29acc0","instrument":"4000007510S00001","is_eligible":"true"},{"type":"unknown","id":"c-2e30d6d4-e071-4f47-bca2-0dd44fb9ec40","instrument":"4000003430S00001","is_eligible":"false"},{"type":"unknown","id":"c5596e72-17d8-3fb2-bede-ecf179e42287","instrument":"4000012320S00001","is_eligible":"true"},{"type":"unknown","id":"276a0ab4-d88c-3f53-9f7c-ceb8ddf24ac9","instrument":"4000007780S00001","is_eligible":"true"},{"type":"ad","id":"31671510345","title":"Films : Regarder Maintenant","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=ti2Mv1oGIS9p_PAYT.gIiNazrxJ_2vixIeUfGQN0Gnu8kEd_7c9l44PKkM8BocOLuiRLvDrl_FMu0fEl9LZyd9CdvTYtjdlmV2iv3xEN8Tzf3yqjJGVG1SlK.LUSoAffC1.V9gNB0HTkY9fbhZywXHVGP70l3pRc7dfc.MPI.X5ExfVf3G51R_0hfyqSxutm7PjR.yiC6QokJVpJWY3LBYhmBXcLKspTn15CM58nR_Zv2nwBrHaISsVTZeTx14bnIPC0gVvgWMaS.O9wpE5t99_4hxU3.5GaRDmHL05mqDWFfinWaYRTk8HzzFwCZlbWMHqc.qjvXujb0FA2McB.Yi.oWacosaWncjiPgTd.Gfz_NUApX92ZYr3ynYdhRv16g0th7Vp9Tsay9jMXPfWyeBtwoIKfs_o7FFHzd94w91VlrXJGfQrWtDF3U8w8htmMBqkcXHgvyN9V2p3UoxfCQ.zJJR_gDGfEkuXlP_PqveWSdpVKYtp2FHNucAAgegJziQ--%26lp=http%3A%2F%2Fwww.track2cash.com%2Fclick.php%3Ft2c%3Da0af8b810de83f542fe5f1f890e2af76a354","publisher":"PLAYVOD","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=tAflOkIGIS9g8sX3Cks2IWXShvCBEuN0CT.MWvwoBcRBWQ3nkz2j0lr_EPBCOdtBqOZyGYyOqjN6jnlJ979BIZj9854GO8XDGWTPHIWWflSZz0ARF1K3zQ4mz1YE8nE3V3brloJIkwAAuRUo5PFNXkbg9sOxIH3JYczueVpbzAOQzMxKPOMc3Ju24enl94icbntu42YYaQhkUISvMQt7XknNYrsCJF23ypHkF1ttsLh0je8CHDhphdP54GRbCTbA2mUR2i8gMDMx2tmBSjIQuSGzxrYomRi8pEE1mHFdpqBngTXrTq2rUv0b0PocDZAels41xKmS3mlVlwLU96IgYAeNi6Q7q08hQnCx9lj84JHJWCrPrejRu3tKMb0.fTCBbJ1W&ap=83","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15np34fvl(gid$0de98d92-d01d-11e5-9b07-008cfacc4628-7f1d99642700,st$1455125891097000,li$0,cr$31671510345,dmn$c31671510345,srv$3,exp$1455133091097000,ct$27,v$1.0,adv$1335464,pbid$1,seid$4250754))&r=1455125891097&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/bIwPNYD.1fgcMRRaziU_iQ--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1453108771010-9190.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Films et sÃ©ries en VOD, streaming ou Ã  tÃ©lÃ©charger !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":15,"cposy":15,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31671510345","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":14},{"type":"unknown","id":"a269c60f-7f57-3916-b783-16a28200f0dc","instrument":"4000009530S00001","is_eligible":"true"},{"type":"unknown","id":"c-3147a0f8-693a-4dfe-8fb9-5fd1843babf8","instrument":"4000000000S00001","is_eligible":"false"},{"type":"unknown","id":"f41a6cec-f555-3210-a90d-6a9d7ea5e643","instrument":"4000021420S00008","is_eligible":"false"},{"type":"unknown","id":"41c56e70-115e-3607-9f63-cd1f1805eba3","instrument":"4000009260S00001","is_eligible":"true"},{"type":"unknown","id":"c-e6acf665-78ff-4a77-bd40-8363aabd72da","instrument":"4000008990S00001","is_eligible":"false"},{"type":"unknown","id":"17784c03-f88c-3f5d-bbab-b56dad05effe","instrument":"4000015580S00008","is_eligible":"false"},{"type":"unknown","id":"0c5dfe24-18da-3897-8407-9205a9c37f6c","instrument":"4000012910S00008","is_eligible":"false"},{"type":"unknown","id":"6fa95903-915d-3bbd-aa46-99b36d6dbba9","instrument":"4000015650S00008","is_eligible":"false"},{"type":"unknown","id":"6b623da3-b85d-3142-b68b-05ab15d649a5","instrument":"4000017180S00008","is_eligible":"false"},{"type":"unknown","id":"9ff36a88-c3cc-36a8-96b9-78e7123f3151","instrument":"4000000570A100S00004","is_eligible":"true"},{"type":"unknown","id":"c-e5130f75-afeb-43bb-b9e0-ce47f1461dd5","instrument":"4000005720S00001","is_eligible":"false"},{"type":"unknown","id":"229298a2-dbad-398d-9440-3aae55cf3d1f","instrument":"4000011000S00001","is_eligible":"true"},{"type":"unknown","id":"e363c543-aebd-396b-9c36-b6c9092c7691","instrument":"4000006690S00002","is_eligible":"true"},{"type":"unknown","id":"34e2159c-9c77-3242-ae68-08deedced07c","instrument":"4000014130S00001","is_eligible":"true"},{"type":"unknown","id":"ae97cbf0-f72c-30ec-b535-7944bdfe3c4c","instrument":"4000016250S00008","is_eligible":"false"},{"type":"unknown","id":"90c35849-7fe0-315f-aeba-95b749b4325c","instrument":"4000015760S00008","is_eligible":"false"},{"type":"unknown","id":"adc197ec-89f9-3dd3-9cc5-ad219c945042","instrument":"4000016860S00008","is_eligible":"false"},{"type":"unknown","id":"c-4ebf72b2-79d5-4f83-879b-81b305141dd8","instrument":"4000002790S00001","is_eligible":"false"},{"type":"unknown","id":"c-99f256d5-1d78-4712-96f4-26dbc78df301","instrument":"4000000380S00001","is_eligible":"false"},{"type":"unknown","id":"7fed67a4-4252-3970-aa3e-61d550713fc6","instrument":"4000000510A100S00004","is_eligible":"true"},{"type":"unknown","id":"065d7ab0-36df-37d0-b55f-a390a884505c","instrument":"4000013530S00008","is_eligible":"false"},{"type":"unknown","id":"c-d38c0e22-3c43-49cb-9f0f-700a3d1087df","instrument":"4000002000S00001","is_eligible":"false"},{"type":"unknown","id":"469def62-fd6e-3e03-ac31-51e1316cb4d7","instrument":"4000016289S00001","is_eligible":"true"},{"type":"unknown","id":"302881bc-27fa-3e52-8dfe-35f790db28cc","instrument":"4000016049S00008","is_eligible":"false"},{"type":"unknown","id":"3ee4f3d5-c086-3ca8-b8cf-cc206f4c0491","instrument":"4000017660S00008","is_eligible":"false"},{"type":"unknown","id":"09585027-4acf-375e-a865-3816142bb2a4","instrument":"4000017460S00008","is_eligible":"false"},{"type":"unknown","id":"23f595e9-1af9-35b6-b951-ada9ebca2a11","instrument":"4000019410S00008","is_eligible":"false"},{"type":"unknown","id":"8b424246-0778-3674-bc0b-182d8118d8ed","instrument":"4000016010S00008","is_eligible":"false"},{"type":"unknown","id":"b40773bf-d811-315c-ac79-96e020c3670e","instrument":"4000010460S00001","is_eligible":"true"},{"type":"unknown","id":"d11bf002-d056-3411-93d7-870fd677d4a2","instrument":"4000009660S00001","is_eligible":"true"},{"type":"unknown","id":"c-4fdd6d0a-091b-4d7e-8904-b68a77cf2eaf","instrument":"4000003830S00001","is_eligible":"false"},{"type":"unknown","id":"7ade8f86-862a-3c4b-b7fe-242a49668b1e","instrument":"4000007150S00002","is_eligible":"true"},{"type":"unknown","id":"9a0dd57c-5e62-35d3-8714-56aed20bceea","instrument":"4000008580S00001","is_eligible":"true"},{"type":"unknown","id":"3b8145fc-8437-3abb-8e0e-f026e246e758","instrument":"4000019570S00008","is_eligible":"false"},{"type":"unknown","id":"c8675add-a701-32cb-90b0-19ebd4f9e82b","instrument":"4000008220S00001","is_eligible":"true"},{"type":"unknown","id":"c-d3e1d910-01c3-42f4-919b-dabb6567bdd5","instrument":"4000002640S00003","is_eligible":"false"},{"type":"unknown","id":"2ce6bc9d-bf60-378e-8b19-9dd591520c21","instrument":"4000020190S00008","is_eligible":"false"},{"type":"unknown","id":"c-8e00d8b8-05fa-463a-8a89-920123cc8230","instrument":"4000014970S00001","is_eligible":"false"},{"type":"unknown","id":"6126bcdc-f218-3e70-9a76-7d4e49ca5386","instrument":"4000012100S00001","is_eligible":"true"},{"type":"unknown","id":"e39b086b-5e95-3221-a970-3da2a89518ac","instrument":"4000009790S00001","is_eligible":"true"},{"type":"unknown","id":"12fbd670-58a6-392d-8a91-b599f55254a4","instrument":"4000011290S00001","is_eligible":"true"},{"type":"unknown","id":"c-d631e91a-1a66-4cac-9dc0-7086405af166","instrument":"4000004600S00001","is_eligible":"false"},{"type":"unknown","id":"48daeaba-25be-335c-a64a-1f1db63ca636","instrument":"4000015540S00008","is_eligible":"false"},{"type":"unknown","id":"d77c9cdd-fae8-37be-b676-1f320830b485","instrument":"4000010230S00001","is_eligible":"true"},{"type":"unknown","id":"eb32f55a-e762-3226-a25c-6bf33fe292eb","instrument":"4000002930S00002","is_eligible":"true"},{"type":"unknown","id":"e560755d-d4e0-31f5-9043-4f38bbaa02fb","instrument":"4000010540S00001","is_eligible":"true"},{"type":"unknown","id":"f1a45f41-2ced-381f-b7c1-4db82f799737","instrument":"4000008430S00001","is_eligible":"true"},{"type":"unknown","id":"2060b04d-6518-302c-a708-cd1d4c0fe6ff","instrument":"4000001120S00004","is_eligible":"true"},{"type":"unknown","id":"c-d0080bbd-d891-4ca1-8df7-a9fe3d6ce6e5","instrument":"4000007020S00001","is_eligible":"false"},{"type":"unknown","id":"c-6e163266-344b-4e41-aec1-025d23a8e54b","instrument":"4000005260S00001","is_eligible":"false"},{"type":"unknown","id":"c-09d4e9c4-56b4-4c08-b9ad-9ec9ff81fa45","instrument":"4000001880S00001","is_eligible":"false"},{"type":"unknown","id":"704b6a3f-642e-3cfc-b8de-5720c8b8280b","instrument":"4000009940S00001","is_eligible":"true"},{"type":"unknown","id":"02f91294-4666-3af4-b339-8b98c0dc1380","instrument":"4000016280S00001","is_eligible":"true"},{"type":"unknown","id":"c-35ceba28-05b3-4f25-ac8a-7df34cc2b773","instrument":"4000011560S00001","is_eligible":"false"},{"type":"unknown","id":"f6d3fdd9-77bf-38fb-8494-de86cab2ee45","instrument":"4000022400S00008","is_eligible":"false"},{"type":"unknown","id":"7c0819ad-06f4-36bb-91a4-abc729fbb9e1","instrument":"4000014630S00001","is_eligible":"true"},{"type":"unknown","id":"aac34387-3e62-3cf4-998e-ab8ccbf93f53","instrument":"4000008019S00001","is_eligible":"true"},{"type":"unknown","id":"f73d0bf7-839e-348a-89ad-5d3125bf0fce","instrument":"4000008750S00001","is_eligible":"true"},{"type":"unknown","id":"06c575d5-b47b-3f8d-8ed4-bf9c6ee1f906","instrument":"4000012800S00008","is_eligible":"false"},{"type":"unknown","id":"46884b86-5a62-32aa-adc5-855c3fd84dbc","instrument":"4000006630S00002","is_eligible":"true"},{"type":"unknown","id":"dbbf3083-1f24-3d21-b0ae-d06fc423c889","instrument":"4000009180S00001","is_eligible":"true"},{"type":"unknown","id":"925ef154-ef61-3cc6-a7f8-c38c4f3d9739","instrument":"4000014480S00008","is_eligible":"false"},{"type":"unknown","id":"6e6869b9-adf6-3b74-be05-c3da61d8cf2f","instrument":"4000001030S00004","is_eligible":"true"},{"type":"unknown","id":"c-330b18ae-6807-46ed-ac5c-4f3b8797b66d","instrument":"4000004330S00001","is_eligible":"false"},{"type":"unknown","id":"6283f2bf-4de7-3f74-b04f-ad4ff6ebd2cb","instrument":"4000012040S00001","is_eligible":"true"},{"type":"unknown","id":"5b8f1587-afbb-3d20-a0e3-d08c6a6c54ca","instrument":"4000007590S00001","is_eligible":"true"},{"type":"unknown","id":"87376437-1c12-3e26-8081-1832e0e09f71","instrument":"4000008900S00001","is_eligible":"true"},{"type":"unknown","id":"baae4847-d95b-3682-a1a7-55a6587eff36","instrument":"4000001080S00004","is_eligible":"true"},{"type":"unknown","id":"d352dc8a-1529-3e6f-a8ab-92bae8b588a8","instrument":"4000008780S00001","is_eligible":"true"},{"type":"unknown","id":"b35120d0-8a8e-3c96-a875-f3ee37e3e551","instrument":"4000012010S00001","is_eligible":"true"},{"type":"unknown","id":"c0b253d1-34d1-30b4-9af7-27db10f15865","instrument":"4000015470S00008","is_eligible":"false"},{"type":"unknown","id":"762b3793-f6c6-3cc3-8d79-7594d45ddb3f","instrument":"4000014390S00008","is_eligible":"false"},{"type":"unknown","id":"c-6bfdbb6c-a3cd-4828-abbb-501e65de5bfa","instrument":"4000002810S00001","is_eligible":"false"},{"type":"unknown","id":"a981a533-277a-3af6-8520-4d2a0e1221b0","instrument":"4000014080S00001","is_eligible":"true"},{"type":"unknown","id":"e1beee98-8a61-3d36-b07c-474345a4010e","instrument":"4000012270S00001","is_eligible":"true"},{"type":"unknown","id":"fc9fef0a-5157-3dbe-8c32-2aae177e3bd3","instrument":"4000003170S00002","is_eligible":"true"},{"type":"unknown","id":"1e920fac-1f7f-3301-a2b6-0840a3550c8f","instrument":"4000016420S00008","is_eligible":"false"},{"type":"unknown","id":"3af7bcfd-7f1f-3585-82ac-e04adeec681c","instrument":"4000009030S00001","is_eligible":"true"},{"type":"unknown","id":"403d9436-f340-39c9-813d-cd52c0374171","instrument":"4000013350S00008","is_eligible":"false"},{"type":"unknown","id":"c-ce507a83-1b77-42f1-bced-5ab249ca09c7","instrument":"4000009430S00001","is_eligible":"false"},{"type":"unknown","id":"c-455649a0-862d-4c3f-b05d-0b798d850875","instrument":"4000006160S00001","is_eligible":"false"},{"type":"unknown","id":"c-5155cc12-92d3-49cd-9cde-5a677a8df523","instrument":"4000001730S00001","is_eligible":"false"},{"type":"unknown","id":"cab28446-66ec-35dd-bea7-3bedabd6946c","instrument":"4000014320S00001","is_eligible":"true"},{"type":"unknown","id":"e4bb7905-bb0c-35f2-9963-ce4dcd036384","instrument":"4000004680S00002","is_eligible":"true"},{"type":"unknown","id":"bf5cb6b7-ff28-34c0-8885-97b0142dc3d3","instrument":"4000006710S00001","is_eligible":"true"},{"type":"unknown","id":"63d3960f-b9d3-381d-81a8-a59c2395e14e","instrument":"4000007660S00001","is_eligible":"true"},{"type":"unknown","id":"2e85e413-aeda-372b-99ae-72c30aa331ec","instrument":"4000009440S00001","is_eligible":"true"},{"type":"unknown","id":"98b5e0cc-b8fc-3b29-9a99-5057fd00c813","instrument":"4000014510S00008","is_eligible":"false"},{"type":"unknown","id":"9b1218b8-a302-3440-b83d-6f81f8372715","instrument":"4000010360S00001","is_eligible":"true"},{"type":"unknown","id":"a536e4c3-7b64-356d-94d8-22971c803391","instrument":"4000016720S00008","is_eligible":"false"},{"type":"unknown","id":"327d3777-761b-3ffb-80b4-e23787903e56","instrument":"4000001770S00004","is_eligible":"true"},{"type":"unknown","id":"7e1e0246-6c9b-3ec0-8126-c38dbda3a846","instrument":"4000009970S00001","is_eligible":"true"},{"type":"unknown","id":"e94a394e-1064-3b96-82bc-9c7ded46c896","instrument":"4000016299S00001","is_eligible":"true"},{"type":"unknown","id":"c-b51ec9ff-bf03-4b36-8698-21ed2429dc84","instrument":"4000002730S00001","is_eligible":"false"},{"type":"unknown","id":"a6a12ff0-2b80-3b77-8dbf-c48217e463ef","instrument":"4000017460S00001","is_eligible":"true"},{"type":"unknown","id":"c-c574179d-3d46-43a4-b751-19175d73569f","instrument":"4000004090S00001","is_eligible":"false"},{"type":"unknown","id":"c-78f90efa-09a8-4643-93d6-49e42463909e","instrument":"4000003520S00001","is_eligible":"false"},{"type":"unknown","id":"992532e6-7cf0-37f9-8239-9e646f912e38","instrument":"4000008090S00001","is_eligible":"true"},{"type":"unknown","id":"5ebc54b3-71cc-304d-b821-973955e1c723","instrument":"4000016170S00008","is_eligible":"false"},{"type":"unknown","id":"25b7c5af-1c91-3ceb-bbfe-ae0e47cfd550","instrument":"4000016310S00001","is_eligible":"true"},{"type":"unknown","id":"ebb60d4b-28e1-367f-b886-f828e08a4d7b","instrument":"4000001780S00004","is_eligible":"true"},{"type":"unknown","id":"ef4f3f24-368f-305e-9605-581981cf2719","instrument":"4000019580S00001","is_eligible":"true"},{"type":"unknown","id":"c-1cf382ee-738c-4c2a-a621-38b5eb91a097","instrument":"4000004430S00001","is_eligible":"false"},{"type":"unknown","id":"0ab159a4-c305-3973-9ef1-1dfa70cf96ae","instrument":"4000009720S00002","is_eligible":"true"},{"type":"unknown","id":"637fb89a-f03e-35cf-b0e0-b488b956f84d","instrument":"4000013880S00008","is_eligible":"false"}],"continuation":"","more":176,"enrichment_ids":[],"cposy":24,"stream_items":[{"id":"id-3594243","cauuid":"f56de522-62a4-3394-89c9-3bb8bf1c6b18","i13n":{"cpos":1,"cposy":1,"bpos":1,"pos":1,"ss_cid":"f56de522-62a4-3394-89c9-3bb8bf1c6b18","refcnt":4,"subsec":1800,"imgt":"ss","g":"f56de522-62a4-3394-89c9-3bb8bf1c6b18","aid":"id-3594243","ct":1,"pkgt":"need_to_know","grpt":"roundup","cnt_tpc":"Need To Know"},"link":"https://gma.yahoo.com/chris-christie-expected-formally-end-presidential-bid-early-161038064--abc-news-topstories.html#","type":"article","viewer_eligible":true,"viewer_fetch":true,"storyline":[{"id":"id-3594205","cauuid":"23006958-8539-38b2-9e4a-548943c96b0e","link":"https://www.yahoo.com/politics/hillarys-millennial-woman-problem-010617452.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"id-3594255","cauuid":"6de4eedd-3ee0-334f-85a5-0f36da1b3d60","link":"https://www.yahoo.com/politics/johnny-depp-trump-funny-or-die-movie-video-160120350.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"id-3594246","cauuid":"eab1bdbf-29db-3bfe-8ca1-1b7e1ac63382","link":"http://news.yahoo.com/valentine-s-day--tweethearts---america-s-longest-married-couple-to-share-insights-on-love-144214702.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"id-3594187","cauuid":"666d713c-4222-3c08-bb0d-7e4278ae7f5c","link":"http://sports.yahoo.com/blogs/ncaaf-dr-saturday/tennessee-faces-sexual-assault-lawsuit-involving-football-players-232613263.html","type":"article","viewer_eligible":true,"viewer_fetch":true}]},{"id":"326eba9a-9987-3915-9e3f-c3fd2ec16a18","i13n":{"cpos":2,"cposy":6,"bpos":1,"pos":1,"ss_cid":"b2ce10f4-c68c-46f4-ba5b-6b22f06bbc5a","refcnt":2,"imgt":"ss","g":"326eba9a-9987-3915-9e3f-c3fd2ec16a18","ct":1,"pkgt":"cluster_all_img","grpt":"storyCluster","cnt_tpc":"World"},"link":"http://www.businessinsider.com/r-north-korea-executes-army-chief-of-staff-south-korean-media-2016-2","type":"article","viewer_eligible":true,"viewer_fetch":false,"storyline":[{"id":"2d5595d1-c688-355b-bf26-203e91d6ab20","link":"http://news.yahoo.com/north-korea-turns-old-workhorse-rocket-repeat-past-090158388.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"728f060e-ccea-3f92-b514-85145240dd4f","link":"http://news.yahoo.com/south-korea-suspends-operations-at-joint-factory-144825916.html","type":"article","viewer_eligible":true,"viewer_fetch":true}]},{"id":"31710498501","i13n":{"cpos":3,"cposy":9,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31710498501","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=M5mibOkGIS.hAkXC8jJZ7StRe9sAXRArdi5nNiu9XkV_pBRxmugojQQlac1AZh8U_smB2u4BAb0tpU30odnq7op3PzZmc1YDHY1r5fA6x4L.FaQ63mHVhx5tDIb.MWnTqkcuDP664GJthYYHbdwYx9ennAykz6nYJbsN6t57LdMOYBUS1ZW6CciYEf2aGX7J5SB816ZGZ_VYI1Sp.j22zl80l8gGQv4qhXxLiorl2P_O90MfbURsqKuUS.B7lrekJXWRFwootcEWP2k4hzvpjvxcsE3atP1V3h8uJbPeK83HRif4HM7ebCxA6wfmmCgYZaOfSoWK6TvZVZm1VMr8xD29vWF7Er37Lh49kCV.8h820POSEb2p13AJnEQNOqXaX2uWVBgT4iBQmJe4OaLxlaciVNF53fjHsJn3055d.8Bx2iDbqopYksQ7nKzkc8JCwm0RFJy0j6INP7aIC.HPRt6N8BY.b1ANfmKlR9ZFgalo10Erbmuh_cxGRy2Cf6Xxrwo.ig--%26lp=http%3A%2F%2Ftrack.fbytemedia.com%2F0f0868f1-4986-4ec1-be9a-d18abdda27ef%3Fad%3D1","type":"ad"},{"id":"dced9374-4418-3536-8b30-77abbb43ba0e","i13n":{"cpos":4,"cposy":10,"bpos":1,"pos":1,"imgt":"ss","g":"dced9374-4418-3536-8b30-77abbb43ba0e","ct":1,"pkgt":"orphan_img","grpt":"singlestory","cnt_tpc":"U.S."},"link":"https://www.washingtonpost.com/local/public-safety/he-always-had-six-or-seven-kids-around-its-a-little-strange/2016/02/09/546489da-cf4f-11e5-88cd-753e80cd29ad_story.html","type":"article","viewer_eligible":true,"viewer_fetch":false},{"id":"a59894ae-0a10-3dd0-afd7-61d80c081d77","i13n":{"cpos":5,"cposy":11,"bpos":1,"pos":1,"ss_cid":"c1c4e431-5853-4ad7-91eb-b1f619fe4d83","refcnt":2,"imgt":"ss","g":"a59894ae-0a10-3dd0-afd7-61d80c081d77","ct":1,"pkgt":"cluster_all_img","grpt":"storyCluster","cnt_tpc":"World"},"link":"http://www.nytimes.com/2016/02/10/world/asia/that-wasnt-a-meteorite-that-killed-a-man-in-india-nasa-says.html","type":"article","viewer_eligible":true,"viewer_fetch":false,"storyline":[{"id":"11b37ea1-9981-37f8-a7ba-008e20db8057","link":"http://news.yahoo.com/man-may-have-been-killed-by-meteorite-in-southern-215721131.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"aa408ddb-8992-3a24-ac70-ca93ea8e6b71","link":"https://gma.yahoo.com/indian-man-killed-explosion-caused-meteorite-government-official-205516769--abc-news-topstories.html","type":"article","viewer_eligible":true,"viewer_fetch":true}]},{"id":"8bdea805-26c2-39ec-9886-dc3c189fb190","i13n":{"cpos":6,"cposy":14,"bpos":1,"pos":1,"imgt":"ss","g":"8bdea805-26c2-39ec-9886-dc3c189fb190","ct":1,"pkgt":"orphan_img","grpt":"singlestory","cnt_tpc":"Politics"},"link":"http://thefederalist.com/2016/02/10/how-hillary-could-wiggle-out-of-her-email-woes/","type":"article","viewer_eligible":true,"viewer_fetch":false},{"id":"319a04b7-cd4c-3629-ada0-269b9c126943","i13n":{"cpos":7,"cposy":15,"bpos":1,"pos":1,"subsec":24,"imgt":"ss","g":"319a04b7-cd4c-3629-ada0-269b9c126943","ct":4,"pkgt":"orphan_img","grpt":"storyCluster","cnt_tpc":"Business"},"link":"https://www.yahoo.com/autos/24-things-wouldnt-expect-car-200023313.html","type":"slideshow","viewer_eligible":true,"viewer_fetch":true},{"id":"31709922006","i13n":{"cpos":8,"cposy":16,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31709922006","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=QrttbQAGIS_nrGwvamgOgYBfaIxOr826w80wFJp7r_cB2fkXmpGMwgYtO0wj07poIgOch3Hr3Mg2o38TGkmEWExUbeOWvsxILh22bCHBOw2wa0Ra3eiGchiHzlGtIveFJtBcvNH3ACKg90uouLu54TqsYoLaJiz8Cqy8X_NvJhXLfAMQttQnrBZnXU_MkVQo.__En9qs00XYQmn27CgzUmPlq6FVNx_0uy6IrVv1KdgoBf2lW2sGEAKNu.rfTxFJmuppwpWDQIXzKAMQ61iHIwHwJ60eLIM_osQPFtiLaC8nvHEaryLbcbkw3Q1wsvMQOIayACmPSHoBzWccoH1W8dhxtlWCxOOr1XgfDrJCJ5GVtP8XRB6f4pli79fIb2BS3Q86wkIpPhOXGXeE1_HlOx4J7rJEqEAymw1gEFtI9GJ_Uw--%26lp=http%3A%2F%2Fpo.st%2FAveCesarBAvf","type":"ad","video_ad_beacons":{"VIDEO_30_SEC":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=bPKRYrAGIS_yR790BpNQ7vcCv6icYp.Zv6QFQSzVEjqENS5.hjlXToKqp0LBjqiAPPnzqK5fW5zaa6mEBoujHMlkXAB4XA62CfpHTNvHSJ1W5HwsO0z70ZsDTjYuGTgqKtLlBVd1VDKzz3DUI5qtY1DVtMY.qf6cXbmfahlgzh1drhcHYOif.y0AvPTOwT459qLRGhJ.EottWxaxuCaM2ei9_q7RUHvQUKZvQ1MXW6KQZkA.yjAuHlvn4_1N1jUm2Dkik8Y9L49ROnYc1.laZdiVGZklMd21C6OxDCwnYvwtrQ--","VIDEO_CLOSE":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=hVfg3DgGIS91ntmNeA_P_91A3MM834xm0RJ.ZKYv7YURaGWV6jvQjIDSdOv8VaxEgUkpNeeXxObU2WDJq86KTUM4fmYSgTGPFgBkny3XakinNBq3ZBMabo2rbL1Yf1bF8ghQZPt32ByDoRCH2OxPPdUbAshNtvdQ.CDmEEp24LtzYaMNT1cFEcLLOO7E.oDfKdfh8QKP1GiJcZiFxMxskzmETtfts0KT9jc.y8m9qDJhbgbrwZbyitDbApzc0mOtWI0LWnWK4sFDxh0_.PB_Gd6FYbLyaitgL2FtWGrFd3yA_g--","VIDEO_QUARTILE_100":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=_xODisYGIS.xLe.gCs4eYZgVUorxERl5L2az8AL9dYh8Ee7x1KId4JGxoktP5oBBnWcPv58x8bzAaJYThoySNduhwlMjJlJzzH0hCZbtTEi1dGNSDbL0sQnKqDymAhVDXZxtNTSvXHCaRik22KWklq3INwjFsfBcS6aU0ii.RCTJ4RV60hJiI_JKYOUybBJ.rZAT0a_hAk5uz3l0G7WdJlFoXKI0j352KSip4K5tbxZJklh0p6aSRI71pike0a4N2r_HcjTeRbNFc_QYwknXjiwShmPHcWnko0TJMbZIOxqV5lgeqnwd9OILb7LgS4Fe2Q--","VIDEO_QUARTILE_25":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=DqLe1s8GIS8LWjfNcizIs_x.KcV4HM5nUt9QR9xQiCG9PbCYS1.WJ3MvFOjCb9zio3YCAgLMP3EA_WNehBAB0J.nTc_rbipW4O51aHQ2N4S_8KQ84kdNZyy0wh0.VHj_CHNtIFmesiJ3Ao4f23IEbv1zBjSjr6D4eu4P_h0Mh4TAN7hdRLaHs3cHEQrtxvrr1LV.grQaPQ6etoznndVqWpWqmgi4.8L2hYG1gOmw1iC.j7tBaA2pvlUIx4AKqoqMqiT2yKnl6gCwQVEfR9qobqAmI.X6ra2CYCessVJVxaAm","VIDEO_QUARTILE_50":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=iYKLle8GIS_xEilZC2EyT2p97Gt8AtgxbibyQAVAHkkiHiVZqO_eIhqR1acTSknkNh.Adl5X3QkfO.ELj0nCqHyLUu5Zrkk7IACrYuS5aDjDvXqlM_KqNCUjClkfQowck6iMlNZpCbh3ToG5oFbkdAxOinwECWsbFbkuA0DtJ3mb.RpUtv_fC3_p_12a6u_Vqc7TREeyXVhrnx7xkY4m4asa_n5r6w.HAKsLW_FTHusI0GUzug0F.B8sG8qPmrUF.sm1n65Oeg2uWogZr4EZ2ySB8ep_iZ_CdzwqfSgf6fKa","VIDEO_QUARTILE_75":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=l5w3KcwGIS8FWT8aJXjEoqfcKjd_xTK0bG6bOQkonzAfTGSAB1QmpcsbtRzh9rY4c_u4DOLRoop1GupGvzWmaUfJ_ABOrCwhvQfMOG35AdqESXevodlxfjlRiUujH8cYUU7HvMuUkOOLlXV2HqH.3WHSvTkfCOtD7JIxg8eicn9J05zynsu9ZQVcEy7J0ZXPp9SBose1h6F.DGDNDS8Aed8DWKddSVSeKfccNjniRyc9rgSD05rm22rOB5VyOZCFBumvGbF9zUQ1uqhU9SOIEGFVZKYWZJOkf49ghjzOciql","VIDEO_SKIP":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=rTF9CO8GIS.WPoOHOa7bEJJpOuOwjdVSUV.oZFJ1RI7pT5nYHnaEE59wGZrCk86_VCygLfjItQ6eak_qLn46e.9xoGcqS3JSvtf0VEqdNpDJ4EHyojDwIB52WYwrGvwpiWZghHVwKjxf9eOifKhZl_WBP7T8UkgyF.pxy41F_NuEYgts1Dn8Te565pDqTvlqm5eUrJdNNMwJOO1UBLgfzdl79ZW8PV2VEjmFfODoQ3eQsKnszSI73lw03pvSbXMwslE.bv6mYCei_2uNr9mbrNbB8pw59_qUtqi6Ufy4AR2Wgg--","VIDEO_START":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=ero.FiwGIS8P4avWkXGHatHDbMTBNEXkBDp9BMzg291JmF1C16KDo7cbgWp8FEkz9LbeJ8La1Wb6a6UfgNBSA0Nb_hTMTM2l7Z1D.Si86LFIfL31HqmVNZ6gK11sT6lDEKHmdDSEUvJDQCnjqNwSdDaSCECAbS1D_o2V.zAmWupFpvfVHbtP3QMFnQJi_KBjqP8iyrMgC.TL.SzaogU4lSGgxrG7Gj2Puz_3MDLviYAjgR6_QTt6AXNrO_bqpEg4FJxd7R6Z.b8QLwbRwam_48dUjkpFcShc8P5KKYba_0da&vsa=$(V_SKIP_AVAIL)&va=$(V_AUTOPLAYED)&vph=$(V_PLAYER_HEIGHT)&vpw=$(V_PLAYER_WIDTH)&vm=$(V_MUTED)","VIDEO_VIEW":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=SsJhNJQGIS8EmoUHieg6RQFRL1qb.nFBGA9g8zO2bxUTcTbOgDskymTRXsA5V2UNpP2g59jxaQEk1L0ofchWCEKBcH5kquOBRpWr4W14WFHraPnlc2b4iG9ctRcQ77AICL8Zq4dukCgBOTp0RXZdwztg4jALTnY9smrjeqnQBipwTFXr1jGkpwLHvlvVb.kj.kntCZirsJc7rsylqkC54wq6xfL7Q8GRsxk57OXcZ2e6ERrziipb1yLX.VvusP27NAeMGKWMuKVM2PZBY6WKdltS.CvLe07dx3DNpNSsPMGQfphK0iH8JX1lhHgHJI8OgCFQTmxbPyCN2A--"},"video_ad_assets":{"usageType":"VIDEO_PRIMARY","mediaInfo":{"url":"https://c-592319c911a73128458a395e0763fa3e.http.atlas.cdn.yimg.com/gemini/pr/video_-2dkRNZyBZKt9CMx4J6h1hyQkwurRhjBro96eBlVOAii2a4yElb7xLD3-nwOzK03auDE8mgBo0I-_4.mp4?a=gemini&mr=0&s=0e4e865082de8538aec8a22c319856b9","contentType":"video/mp4","height":"270","width":"480","bitrate":"500","length":"30"}}},{"id":"0a86cae5-6ffb-3275-8115-2b1d34efce3b","i13n":{"cpos":9,"cposy":17,"bpos":1,"pos":1,"ss_cid":"27eafe81-f8be-4d8c-93f0-be52df31c9a9","refcnt":2,"subsec":1982,"imgt":"ss","g":"0a86cae5-6ffb-3275-8115-2b1d34efce3b","ct":1,"pkgt":"cluster_all_img","grpt":"storyCluster","cnt_tpc":"Celebrity"},"link":"https://www.yahoo.com/style/liam-neeson-is-dating-an-incredibly-famous-woman-114926822.html","type":"article","viewer_eligible":true,"viewer_fetch":true,"storyline":[{"id":"5b59454b-82d2-3e21-8c0b-330594d3b6db","link":"http://www.upi.com/Entertainment_News/2016/02/09/Liam-Neeson-romantically-involved-with-famous-woman/8621455029063/","type":"unknown","viewer_eligible":true,"viewer_fetch":false},{"id":"b93a22c7-d417-38ec-9124-161e74f655fd","link":"http://news.yahoo.com/is-liam-neeson-dating-kristen-stewart-rumour-mill-113959299.html","type":"article","viewer_eligible":true,"viewer_fetch":true}]},{"id":"8b5477e3-80f5-3b94-9585-3ceaf6e15007","i13n":{"cpos":10,"cposy":20,"bpos":1,"pos":1,"imgt":"ss","g":"8b5477e3-80f5-3b94-9585-3ceaf6e15007","ct":1,"pkgt":"orphan_img","grpt":"storyCluster","cnt_tpc":"Politics"},"link":"http://www.elledecor.com/life-culture/travel/a8151/airline-employee-secrets/","type":"article","viewer_eligible":true,"viewer_fetch":false},{"id":"6791242b-1729-39a1-bc9c-23574973fcc9","i13n":{"cpos":11,"cposy":21,"bpos":1,"pos":1,"ss_cid":"0f2d5b0e-9026-463b-96f9-d54347e66434","refcnt":2,"imgt":"ss","g":"6791242b-1729-39a1-bc9c-23574973fcc9","ct":1,"pkgt":"cluster_first_img","grpt":"storyCluster","cnt_tpc":"Politics"},"link":"https://www.washingtonpost.com/blogs/plum-line/wp/2016/02/09/trump-disqualifies-himself-for-the-gop-nomination-again/","type":"article","viewer_eligible":true,"viewer_fetch":false,"storyline":[{"id":"2c487265-3e28-3206-a6cd-e7c3da913f43","link":"http://www.huffingtonpost.com/john-a-tures/could-kasich-win-by-follo_b_9199220.html?ncid=txtlnkusaolp00000592","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"dc922822-7d37-35b5-9664-f623de7d046e","link":"http://finance.yahoo.com/news/why-republicans-more-divided-ever-042821960.html","type":"article","viewer_eligible":true,"viewer_fetch":true}]},{"id":"d883006f-ca5d-3404-a481-b760a152b761","i13n":{"cpos":12,"cposy":24,"bpos":1,"pos":1,"g":"d883006f-ca5d-3404-a481-b760a152b761","ct":1,"pkgt":"orphan_no_img","grpt":"storyCluster","cnt_tpc":"Business"},"link":"https://www.nerdwallet.com/blog/credit-cards/dave-ramsey-credit-cards/","type":"article","viewer_eligible":true,"viewer_fetch":true}],"fetched":12,"interest_data":{"WIKIID:New_Hampshire":{"name":"New Hampshire"},"YCT:001000661":{"name":"Politics & Government"},"WIKIID:North_Korea":{"name":"North Korea"},"YCT:001000705":{"name":"Military"},"YCT:001000780":{"name":"Society"},"YCT:001000667":{"name":"Crime & Justice"},"YCT:001000267":{"name":"Education"},"WIKIID:NASA":{"name":"NASA"},"YCT:001000798":{"name":"Disasters & Accidents"},"WIKIID:United_States_Department_of_State":{"name":"United States Department of State"},"WIKIID:Classified_information":{"name":"Classified information"},"WIKIID:Hillary_Clinton":{"name":"Hillary Clinton"},"WIKIID:Automotive_industry":{"name":"Automotive industry"},"YCT:001000123":{"name":"Business"},"WIKIID:Liam_Neeson":{"name":"Liam Neeson"},"WIKIID:Natasha_Richardson":{"name":"Natasha Richardson"},"YCT:001000288":{"name":"Relationships"},"YCT:001000031":{"name":"Arts & Entertainment"},"YCT:001000069":{"name":"Celebrities"},"YCT:001001051":{"name":"Travel Transportation"},"YCT:001001039":{"name":"Travel"},"YCT:001000992":{"name":"Transportation"},"WIKIID:Donald_Trump":{"name":"Donald Trump"},"WIKIID:Patient_Protection_and_Affordable_Care_Act":{"name":"Patient Protection and Affordable Care Act"},"YCT:001000395":{"name":"Health"},"YCT:001000511":{"name":"Health Care Policy"},"WIKIID:Credit_card":{"name":"Credit card"},"WIKIID:Dave_Ramsey":{"name":"Dave Ramsey"},"WIKIID:Credit_card_debt":{"name":"Credit card debt"},"YCT:001000303":{"name":"Credit"},"YCT:001000298":{"name":"Finance"},"YCT:001000299":{"name":"Banking & Budgeting"},"YCT:001000306":{"name":"Debt"}}}},"items":{"yui_module":"td-applet-stream-items-model-v2","yui_class":"TD.Applet.StreamItemsModel2"},"applet_model":{"yui_module":"td-applet-stream-appletmodel-v2","yui_class":"TD.Applet.StreamAppletModel2","config":{"ads":{"adchoices_text":true,"adchoices_url":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","ad_polices":true,"count":25,"contentType":"","fallback":0,"feedback":true,"frequency":4,"inline_video":true,"pu":"www.yahoo.com","se":4250754,"related_ct_se":"5454650","related_ct_single_ad":true,"related_dedupe_ads":true,"spaceid":"2023538075","sponsored_url":"http://help.yahoo.com/kb/index?page=content&y=PROD_FRONT&locale=en_US&id=SLN14553","start_index":1,"timeout":0,"type":"STRM,STRM_CONTENT,STRM_VIDEO","version":2,"related_start_index":3},"category":"","js":{"attribution_pos":"bottom","click_context":false,"content_events":true,"filters":false,"full_content_event":false,"pluck_item_fields":"id,cauuid,payoffId,events,i13n,link,type,videoUuid,viewer_eligible,viewer_fetch,video_ad_beacons,video_ad_assets","pluck_today_fields":"image,title,summary,publisher,more_link_text","poll_interval":60000,"related_collapse":true,"related_content":true,"related_count":4,"restore_filter":false,"restore_state":false,"restore_state_storage":"history","show_read":true,"sticker":false,"sticker_toptarget":"","summary_pos":"left","xhr_ss_timeout":500,"stories_like_this":0,"login_url":"https://login.yahoo.com/config/login?.intl=us&.lang=en-US&.src=fpctx&.done="},"linkSupplement":null,"onboarding":{"count":3,"debug":false,"pos":-1,"result":3,"topic":false},"pagination":true,"remoteConfig":{"endpoint":"/_td_remote","params":{"disableAppClass":true,"footer":false,"i13n":{"auto":false},"noAbbreviation":true,"rendermode":"hovercard"},"passLocdropCrumb":true,"type":"td-applet-weather"},"request_xhtrace":0,"signed_in":false,"ss_endpoint":"ga-hr.slingstone.yahoo.com@score/v9/homerun/en-US/unified/mega","ss_timeout":300,"template":{"main":"td-applet-stream-atomic:main","drawer":"td-applet-stream-atomic:drawer_desktop","drawer_action":"td-applet-stream-atomic:drawer_action","drawer_share":"td-applet-stream-atomic:drawer_share"},"timeout":800,"total":170,"ui":{"backfill_property_region":false,"big_click_target":true,"bleed":false,"bnb_enabled":true,"cat_label_enabled":true,"comments":true,"comments_allow_supression":true,"comments_inline":true,"comments_inline_ranking":"highestRated","comments_offnet":false,"comments_editorial_suppression":true,"comments_request_count":5,"comments_update":false,"comments_update_maxidle":300000,"comments_update_monitoring":true,"comments_update_throttle":1750,"comments_writes_enabled":true,"followable":true,"followable_signedout":true,"dislike":false,"dollar_sign":true,"editorial_edition":{"label":"EDITION_DEFAULT","top":true,"shouldUpdateLVtimeInCookie":true},"enable_featured_ad_video_gap":true,"enable_hovercolor":true,"enable_stream_notify":false,"enrich_tweet":0,"enrichment":{"types":"","unique":false},"entity_max":9,"exclude_types":"","exclusive_type":"","fauxdal":true,"featured_align":"left","featured_delayed_defer":false,"featured_percent_width":45,"featured_width":460,"featured_height":258,"featured_width_portrait":200,"featured_height_portrait":200,"first_featured_treatment":true,"fixed_layout":false,"fptoday_thumbs_sparse":false,"headline_wrapper":true,"hq_ad_template":"featured_ad","i13n_key_tar_qa":false,"image_quality_override":true,"incremental_count":0,"incremental_delay":300,"incremental_history":100,"inline_filters":false,"inline_filters_article_min":10,"inline_filters_max":6,"inline_video":true,"item_template":"items","like":true,"limit_summary_height":true,"link_to_finance":false,"sticker_top":"94px","mms":false,"needtoknow_actions":true,"needtoknow_template":"filmstrip","needtoknow_storyline_min":4,"off_network_tab":false,"open_in_tab":false,"defaultDrawerAction":{"title":"ACTION_DEFAULT","description":"Content preferences","url":"https://settings.yahoo.com/interests"},"payoffDrawerActions":{"local":{"title":"ACTION_LOCAL","description":"More local news","urlPrefix":"https://news.yahoo.com/local/"},"finance":{"title":"ACTION_FINANCE","description":"Manage portfolios","url":"https://finance.yahoo.com/portfolios"},"sports":{"actionsEnabled":false,"attributionEnabled":false,"title":"ACTION_SPORTS","description":"Edit my teams","url":"https://sports.yahoo.com/myteams"}},"payoffs":"","payoffExpTime":24,"payoffFinanceArticleCount":2,"payoffInterval":6,"payoffInvalidTime":0.5,"payoffSportsLocalTeams":false,"payoffLocalArticleCount":3,"payoffLocalHeadliner":true,"payoffStartIndex":1,"pcsExclusions":false,"preview_enabled":false,"preview_lcp":true,"pubtime_format":"ONE_UNIT_ABBREVIATED","pubtime_maxage":3600,"ribbon_headers":false,"sfl":false,"sfl_get_started_string":"SFL_LINK","scrollbuffer":900,"scroll_node_selector":"body","show_tooltips":true,"smart_crop":true,"stateful_subtle_hint":true,"storyline_cap_image":2,"storyline_cap_noimage":3,"storyline_count":2,"storyline_elapsed_time":true,"storyline_elapsed_time_threshold":720,"storyline_enabled":true,"storyline_items_to_show":2,"storyline_multi_image":true,"storyline_multi_image_roundup":true,"storyline_newsy_disabled":false,"stream_actions":true,"stream_actions_minimal":true,"stream_actions_reblog":true,"stream_actions_share":false,"stream_actions_share_panel":true,"stream_actions_likable":true,"stream_actions_tumblr":true,"stream_debug":false,"stream_payoff_actions":false,"stream_item_maxwidth":"none","stream_item_image_fallback":"https://s.yimg.com/dh/ap/default/151015/stream-gradient-230x130.png","stream_item_large_image_fallback":"https://s.yimg.com/dh/ap/default/150902/stream-gradient.png","stream_single_item_image_fallback":"https://s.yimg.com/dh/ap/default/150903/stream-gradient-single-3.png","summary":true,"summary_length":160,"template_label":"js-stream-dense","template_suffix":"","today_count":5,"today_snippet_count":5,"fc_related_minCount":2,"today_featured_image_tag":"pc:size=medium","tap_for_summary":false,"templates":{"all":{"batch_max":20,"gap":0,"max":170,"start":1,"batch":2,"total":2,"last":7},"featured":{"batch_max":20,"gap":0,"max":170,"portrait":false,"start":0,"batch":0,"total":0},"featured_ad":{"batch_max":20,"gap":0,"max":170,"start":0,"batch":2,"total":2,"last":7},"inline_video":{"batch_max":20,"gap":0,"max":170,"start":0,"batch":0,"total":0}},"thumbnail":true,"thumbnail_align":"right","thumbnail_hover":true,"thumbnail_size":160,"thumbnail_specs":{"featured":"img:190x107|2|80","featured_square":"img:190x190|2|80","inline_video":"img:190x107|2|80","items":"img:190x107|2|80","items_large":"img:190x190|2|80","storyline_square":"img:70x70|2|90","related":"img:165x120|2|80","featured_roundup":"img:702x274|1|95","roundup_wide":"img:170x80|2|80"},"toJpg":true,"truncate_summary":false,"tweet_action":true,"uis_inferred_finance":false,"ult_count":10,"ult_host":"hsrd.yahoo.com","ult_links":false,"use_ss_roundup_slot":true,"view_article_button":false,"viewer":true,"viewer_include_all":true,"viewer_off_network":false,"viewer_reblog":false,"viewer_action":true,"visit_state_threshold":1800000,"preview":0,"related_host":"","stream_payoff_v2":1,"stream_payoff_vizify":0,"stream_rc4":0,"show_jump_to_historical_items":0,"item_templates":["items"]},"weather":false,"woeid":12597132,"tz":"Europe/Paris","xcc":"fr"},"settings":{"size":9,"woeid":12597132},"models":["stream","items","related"]},"interest":{"yui_module":"td-applet-interest-model-v2","yui_class":"TD.Applet.InterestModel2"},"comments":{"yui_module":"td-applet-comments-model-v2","yui_class":"TD.Applet.CommentsModel2"},"related":{"yui_module":"td-applet-stream-related-model-atomic","yui_class":"TD.Applet.StreamRelatedModelAtomic"}},"views":{"main":{"yui_module":"td-applet-stream-mainview-v2","yui_class":"TD.Applet.StreamMainView2"},"drawer":{"yui_module":"stream-actiondrawer-v2"}},"templates":{"main":{"yui_module":"td-applet-stream-atomic-templates-main","template_name":"td-applet-stream-atomic-templates-main"},"related":{"yui_module":"td-applet-stream-atomic-templates-related","template_name":"td-applet-stream-atomic-templates-related"},"drawer":{"yui_module":"td-applet-stream-atomic-templates-drawer_desktop","template_name":"td-applet-stream-atomic-templates-drawer_desktop"},"removedItem":{"yui_module":"td-applet-stream-atomic-templates-removeditem","template_name":"td-applet-stream-atomic-templates-removeditem"},"drawerAction":{"yui_module":"td-applet-stream-atomic-templates-drawer_action","template_name":"td-applet-stream-atomic-templates-drawer_action"},"drawerSharePanel":{"yui_module":"td-applet-stream-atomic-templates-drawer_share","template_name":"td-applet-stream-atomic-templates-drawer_share"},"breakingNews":{"yui_module":"td-applet-stream-atomic-templates-breaking_news","template_name":"td-applet-stream-atomic-templates-breaking_news"},"flyoutDrawer":{"yui_module":"td-applet-stream-atomic-templates-drawer_flyout","template_name":"td-applet-stream-atomic-templates-drawer_flyout"},"items":{"yui_module":"td-applet-stream-atomic-templates-items","template_name":"td-applet-stream-atomic-templates-items"},"item_default":{"yui_module":"td-applet-stream-atomic-templates-item-default","template_name":"td-applet-stream-atomic-templates-item-default"},"item_default_clusters":{"yui_module":"td-applet-stream-atomic-templates-item-default_clusters","template_name":"td-applet-stream-atomic-templates-item-default_clusters"},"item_ad":{"yui_module":"td-applet-stream-atomic-templates-item-ad","template_name":"td-applet-stream-atomic-templates-item-ad"},"item_ad_dislike":{"yui_module":"td-applet-stream-atomic-templates-item-ad_dislike","template_name":"td-applet-stream-atomic-templates-item-ad_dislike"},"item_featured_ad":{"yui_module":"td-applet-stream-atomic-templates-item-featured_ad","template_name":"td-applet-stream-atomic-templates-item-featured_ad"},"item_featured":{"yui_module":"td-applet-stream-atomic-templates-item-featured","template_name":"td-applet-stream-atomic-templates-item-featured"},"item_inline_video":{"yui_module":"td-applet-stream-atomic-templates-item-inline_video","template_name":"td-applet-stream-atomic-templates-item-inline_video"}},"i18n":{"AD_FDB_HEADING":"Why don't you like this ad?","AD_FDB_TELL_US":"Tell us why","AD_FDB_THANKYOU":"Thank you for your feedback. We will remove this and make the changes needed.","AD_FDB1":"It is offensive to me","AD_FDB2":"It is not relevant to me","AD_FDB3":"I keep seeing this","AD_FDB4":"Something else","ABCLOGO":"ABC","ACTION_MORE_LIKE_THIS":"Got it! Tell us more about what you like:","ACTION_FEWER_LIKE_THIS":"Got it! Tell us more about what you dislike:","ACTION_DEFAULT":"Content preferences Â»","ACTION_SPORTS":"Edit my teams Â»","ACTION_FINANCE":"Manage portfolios Â»","ACTION_LOCAL":"More local news Â»","ACTION_STORY_REMOVED":"Story removed","ACTION_SEE_LESS":"Okay. You'll see fewer like this.","ACTION_SEE_MORE":"Great! You'll see more like this.","ACTION_SEE_NO_MORE":"This item has been removed from your stream.","ACTION_SIGN_IN_TO_DISLIKE":"{linkstart}Sign-in{linkend} and we'll show you less like this in the future.","ACTION_SIGN_IN_TO_DISLIKE_TUMBLR":"{linkstart}Sign-in{linkend} to dislike","ACTION_SIGN_IN_TO_LIKE":"{linkstart}Sign-in{linkend} and we'll show you more like this in the future.","ACTION_SIGN_IN_TO_LIKE_SIMPLE":"Sign in to like","ACTION_SIGN_IN_TO_LIKE_TUMBLR":"{linkstart}Sign-in{linkend} to like","ACTION_SIGN_IN_TO_PERSONALIZE":"{linkstart}Sign-in{linkend} to personalize!","ACTION_SIGN_IN_TO_SAVE":"{linkstart}Sign-in{linkend} to save this story to read later.","ACTION_SIGN_IN_TO_SAVE_SHORT":"Sign in to save","ACTION_TELL_LIKE":"Tell us more about what you like:","ACTION_TELL_DISLIKE":"Tell us more about what you dislike:","ACTION_MORE_LESS":"Show more or less of","ACTION_OPTIONS":"Options","ACTION_REMOVE":"Remove from my stream","ACTION_STORIES_LIKE_THIS":"Stories like this","ACTION_OPEN":"Open","ACTION_CLOSE":"Close","ADCHOICES":"AdChoices","ALL_ARTICLES":"All Articles","ALL_CELEBRITY":"All Celebrity News","ALL_FINANCE":"All Finance","ALL_MOVIES":"All Movies News","ALL_MUSIC":"All Music News","ALL_NEWS":"All News","ALL_SPORTS":"All Sports","ALL_STORIES":"All Stories","ALL_TRAVEL":"All Travel Ideas","ALL_TV":"All TV News","AROUND_THE_WEB":"Around The Web","ALSO_LIKE":"You may also like","ASTRO_GO_TO":"Go to Horoscopes Â»","ASTRO_NAME_AQUARIUS":"Aquarius","ASTRO_NAME_ARIES":"Aries","ASTRO_NAME_CANCER":"Cancer","ASTRO_NAME_CAPRICORN":"Capricorn","ASTRO_NAME_GEMINI":"Gemini","ASTRO_NAME_LEO":"Leo","ASTRO_NAME_LIBRA":"Libra","ASTRO_NAME_PISCES":"Pisces","ASTRO_NAME_SAGITTARIUS":"Sagittarius","ASTRO_NAME_SCORPIO":"Scorpio","ASTRO_NAME_TAURUS":"Taurus","ASTRO_NAME_VIRGO":"Virgo","ASTRO_TITLE":"Today's overview","AUCTION":"Action","AUTHOR_AT_PUBLISHER":"{author} at {publisher}","BREAKING_NEWS":"BREAKING NEWS","CNBCLOGO":"","COMMENT_ARTICLE_SIGN_IN":"Sign in to perform this action.","COMMENTS":"Comments","COMMENTS_ZERO":"Start the conversation","CONTENT_PREF":"Content preferences","DISLIKE":"Dislike","DONE":"Done","DYNAMIC_FILTERS":"You Might Like","EDITION_DEFAULT":"Need To Know","FEATURED_MOVIE":"Featured Movie","FLAGGED_COMMENT":"Flagged as inappropriate. Thanks for your feedback.","FOLLOW":"Follow","FOLLOW_GAME":"Follow Game","FOLLOWING":"Following {name}","FOLLOWING_STORY":"Following","FOLLOW_UPDATES":"Follow to receive updates on","FULL_HOROSCOPE":"Full Horoscope","GAME_PERIOD:baseball":"Innings","GAME_PERIOD:football":"Quarters","GAME_PERIOD_1":"1st","GAME_PERIOD_2":"2nd","GAME_PERIOD_3":"3rd","GAME_PERIOD_4":"4th","GAME_COVERAGE":"See Full Coverage","GAME_FINAL":"Final","GAME_RECAP":"Game Recap","GAME_TITLE":"{away_team} vs. {home_team}","HALFTIME":"Halftime","HOROSCOPE":"Horoscope","IN_THEATERS_NOW":"In Theaters Now","IN_THEATERS_TODAY":"In Theaters Today","INSTALL_APP":"Install now","IN_WATCHLIST":"In watchlist","IS_IN_FAVORITES":"is in your favorites","JUMP_TO_HISTORICAL_ITEMS":"Go to where you left off","LATEST_NEWS":"Latest News","LESS":"Less","LIKABLE_ARTICLE_SIGN_IN":"Sign in to save your preferences.","LIKE":"Like","LIKE_THIS_TOPIC":"Like this topic","LIVE":"Live","LIVE_GAME":"Live","LOAD_MORE":"Load more stories","LOCAL":"Local","LOCAL_BLOWING_SNOW":"Blowing Snow","LOCAL_BLUSTERY":"Blustery","LOCAL_CELSIUS":"Â°C","LOCAL_CLEAR":"Clear","LOCAL_CLOUDY":"Cloudy","LOCAL_COLD":"Cold","LOCAL_DRIZZLE":"Drizzle","LOCAL_DUST":"Dust","LOCAL_FAIR":"Fair","LOCAL_FOGGY":"Foggy","LOCAL_FAHRENHEIT":"Â°F","LOCAL_FREEZING_DRIZZLE":"Freezing Drizzle","LOCAL_FREEZING_RAIN":"Freezing Rain","LOCAL_GO_TO":"Go to Weather Â»","LOCAL_HAIL":"Hail","LOCAL_HAZE":"Haze","LOCAL_HEAVY_SNOW":"Heavy Snow","LOCAL_HOT":"Hot","LOCAL_HURRICANE":"Hurricane","LOCAL_ISOLATED_THUNDERSHOWERS":"Isolated Thundershowers","LOCAL_ISOLATED_THUNDERSTORMS":"Isolated Thunderstorms","LOCAL_LIGHT_SNOW_SHOWERS":"Light Snow Showers","LOCAL_MIXED_RAIN_AND_HAIL":"Mixed Rain and Hail","LOCAL_MIXED_RAIN_AND_SNOW":"Mixed Rain and Snow","LOCAL_MIXED_SNOW_AND_SLEET":"Mixed Snow and Sleet","LOCAL_MOSTLY_CLOUDY":"Mostly Cloudy","LOCAL_NEWS":"Local news","LOCAL_NO_STORIES":"Sorry, there are no news articles related to this location. Try setting your location to the nearest large city for more stories.","LOCAL_PARTLY_CLOUDY":"Partly Cloudy","LOCAL_SCATTERED_SHOWERS":"Scattered Snow Showers","LOCAL_SCATTERED_SNOW_SHOWERS":"Scattered Snow Showers","LOCAL_SCATTERED_THUNDERSTORMS":"Scattered Thunderstorms","LOCAL_SEVERE_THUNDERSTORMS":"Severe Thunderstorms","LOCAL_SHOWERS":"Showers","LOCAL_SLEET":"Sleet","LOCAL_SMOKY":"Smoky","LOCAL_SNOW":"Snow","LOCAL_SNOW_FLURRIES":"Snow Flurries","LOCAL_SNOW_SHOWERS":"Snow Showers","LOCAL_SUNNY":"Sunny","LOCAL_THUNDERSHOWERS":"Thundershowers","LOCAL_THUNDERSTORMS":"Thunderstorms","LOCAL_TITLE":"{city} News","LOCAL_TORNADO":"Tornado","LOCAL_TROPICAL_STORM":"Tropical Storm","LOCAL_UNKNOWN":"Unknown Conditions","LOCAL_WINDY":"Windy","MIDFIELD":"midfield","MLB_BALLS":"Balls","MLB_ERRORS":"Errors","MLB_HITS":"Hits","MLB_OUTS":"Outs","MLB_RUNS":"Runs","MLB_STRIKES":"Strikes","MORE":"More","MORE_FROM_ROUNDUP":"More from this Roundup","MORE_FROM_TOPIC":"More from {topic} news","MYQUOTES_ADD_MORE":"Add more investments to your {0}Portfolio{1} to stay up to date whenever you visit Yahoo Finance.","MYQUOTES_LOGIN":"{0}Sign-in{1} to see the latest news from the investments in your portfolio.","MYTEAMS_ADD_TEAMS":"{0}Add your favorite teams{1} to start getting news about them today.","MYTEAMS_LOGIN":"{0}Sign-in{1} to get news for your favorite teams.","MYTEAMS_NO_CONTENT":"We can't find recent news for your teams. {0}Edit your teams{1} or visit the {2}All Sports news{3}","MYSAVES":"My Saves","NEW_SINCE_YOUR_LAST_VISIT":"New since your last visit","NEW_STORIES":"New for you","NEW_STORIES_COUNT":"View {new_item_count} new {new_item_count, plural, one {story} other {stories}}","NEXT":"Next","NFL_PROGRESS":"on {field_side} {yard_line}","NO_STORIES_HEADER":"We couldn't find any new stories for you.","NO_STORIES_BODY":"Please check back later or {0}try again{1}","OFF":"OFF","OFFNETWORK":"Read this on {0}","ONBOARDING_CONFIRMATION_INITIAL":"Got it! Almost there","ONBOARDING_CONFIRMATION_ONE":"Got it! One more time","ONBOARDING_CONFIRMATION_ZERO":"Got it! Youâre done","ONBOARDING_TITLE1_FINAL":"Here are some stories based on your preferences","ONBOARDING_TITLE1_INITIAL":"Which would you rather read?","ONBOARDING_TITLE1_ONE":"Now which do you prefer?","ONBOARDING_TITLE1_ZERO":"Awesome! What is your final pick?","OPENS_TODAY":"Opens Today","PORTFOLIO":"Go to your portfolio","PORTFOLIO_GAINERS":"Gainers","PORTFOLIO_LOSERS":"Losers","PORTFOLIO_MARKET_CLOSED":"Market close","PLAY":"Play","PLAY_VIDEO":"Play Video","PLAYING":"playing","PLAYING_NEAR_YOU":"Playing near you","PREVIEW_GAME":"Preview Game","PREVIOUS":"Previous","PREVIOUSLY_VIEWED":"From your last visit","PROGRESS":"{possession_team} {down} & {to_go} at {field_side} {yard_line}","READ_MORE":"Read More","REBLOG":"Reblog on Tumblr","REMOVE":"Remove","SAVE":"Save","SCORE":"Score!","SCORE_ATTRIBUTION":"Click here to track the {display_name} &rarr;","SEE_ALL_STORIES":"See all stories Â»","SEE_DETAILS":"See details Â»","SEE_MORE_STORIES":"See more stories Â»","SEE_FULL_BOXSCORE":"See full boxscore Â»","SFL_HEADER":"Hi {0}, you have no saves. Here's how to get started:","SFL_LINK":"Get started now on the {0}Yahoo Homepage{1}.","SFL_LINK_ATT":"Get started now on the {0}att.net Homepage{1}.","SFL_LINK_FRONTIER":"Get started now on the {0}Frontier Yahoo Homepage{1}.","SFL_LINK_ROGERS":"Get started now on the {0}Rogers Yahoo Homepage{1}.","SFL_LINK_VERIZON":"Get started now on the {0}Verizon Yahoo Homepage{1}.","SFL_STEP_ONE":"Step 1","SFL_STEP_TWO":"Step 2","SFL_TITLE_ONE":"Click on {0} in the stream and on articles across Yahoo to save stories for later.","SFL_TITLE_TWO":"Access your saves from the profile menu {0} on desktop & tablet or menu {1} on your smartphone.","SHARE_THIS":"SHARE THIS","SHOPPING":"Shopping","SIGN_IN":"Sign in!","SIGN_IN_2":"Sign in","SIGN_IN_FOR_MORE":"Looking for past stories?","SIGN_IN_TO_SEE_MORE_TEAMS":" to save your preferences. You'll see more of this team next time you visit.","SLIDESHOW_COUNT":"({0} photos)","SLIDESHOW_PREVIEW_COUNT":"1 of {0}","SPONSORED":"Sponsored","STATUS":"{time} {period}","STORE":"Store","STORIES":"Stories:","STORIES_ABOUT":"Stories about:","SYNOPSIS":"Synopsis","TIME_AGO":"ago","TIME_DAY":"day","TIME_H":"h","TIME_M":"m","TITLE":"Recommended for You","TITLE_LOCAL":"{city} News","TO":"to","TWITTER_REPLY":"Reply","TWITTER_RETWEET":"Retweet","TWITTER_FAVORITE":"Favorite","UNFOLLOW":"Unfollow","UNDO":"Undo","UPCOMING_GAME":"Today at {game_time}","UP_OVER":"Up over","DOWN_OVER":"Down over","VIEW":"View","VIEW_ALL":"View All","VIEW_SLIDESHOW":"View Slideshow","VIEW_FULL_ARTICLE":"VIEW FULL ARTICLE","VIEW_FULL_QUOTE":"View full quote Â»","YAHOO_MOVIES":"Yahoo Movies","YAHOO_ORIGINALS":"Yahoo Originals","YCT:001000931":"Technology","YCUSTOM:COMMERCE":"Commerce","YCUSTOM:MYQUOTES":"My Quotes","YCUSTOM:MYTEAMS":"My Teams","YLABEL:autos":"Autos","YLABEL:business":"Business","YLABEL:beauty":"Beauty","YLABEL:celebrity":"Celebrity","YLABEL:diy":"DIY","YLABEL:entertainment":"entertainment","YLABEL:finance":"Finance","YLABEL:food":"Food","YLABEL:games":"Games","YLABEL:gma":"News","YLABEL:health":"Health","YLABEL:homes":"Homes","YLABEL:ivy":"Screen","YLABEL:lifestyle":"Lifestyle","YLABEL:makers":"Projects","YLABEL:movies":"Movies","YLABEL:music":"Music","YLABEL:news":"News","YLABEL:omg":"Celebrity","YLABEL:politics":"Politics","YLABEL:science":"science","YLABEL:shopping":"Shopping","YLABEL:sports":"Sports","YLABEL:style":"Style","YLABEL:tech":"Tech","YLABEL:technology":"Technology","YLABEL:travel":"Travel","YLABEL:trending":"Trending","YLABEL:tv":"TV","YLABEL:us":"US","YLABEL:world":"World","YPROP:FINANCE":"Business","YPROP:TOPSTORIES":"All Stories","YPROP:LOCAL":"Local","YPROP:NEWS":"News","YPROP:OMG":"Entertainment","YPROP:SCIENCE":"Science","YPROP:SPORTS":"Sports","YPROV:ABCNEWS":"News","YPROV:ap.org":"AP","YPROV:CNBC":"CNBC","YPROV:NBCSPORT":"NBC Sports","YPROV:reuters.com":"Reuters","YPROV:ROLLINGSTONES":"Rolling Stone","YPROV:us.sports.yahoo.com":"Experts","YTYPE:VIDEO":"Video","YPROP:STYLE":"Style","YMAG:food":"Food","YMAG:tech":"Tech","YMAG:travel":"Travel"},"i13n":{"pv":2,"pp":{"ccode_st":"mega_global_ranking_hlv2_up_based"}},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-trending-atomic":{"base":"/sy/os/mit/td/td-applet-trending-atomic-0.0.45/","root":"os/mit/td/td-applet-trending-atomic-0.0.45/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_32209491"] = {"applet_type":"td-applet-trending-atomic","views":{"main":{"yui_module":"td-applet-trending-atomic-mainview","yui_class":"TD.Applet.TrendingAtomicMainView","config":{"clickableTitles":true,"rotationEnabled":true,"rotationInterval":12000,"giftsEnabled":true}}},"templates":{"main":{"yui_module":"td-applet-trending-atomic-templates-main","template_name":"td-applet-trending-atomic-templates-main"}},"i18n":{"GIFTS_TITLE":"Valentine's Day","TITLE":"Trending","TRENDING_NEWS":"Trending News","TRENDING_NOW":"Trending Now"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-weather-atomic":{"base":"/sy/os/mit/td/td-applet-weather-atomic-0.0.23/","root":"os/mit/td/td-applet-weather-atomic-0.0.23/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_63794"] = {"applet_type":"td-applet-weather-atomic","models":{"applet_model":{"yui_module":"td-applet-weather-atomic-appletmodel","yui_class":"TD.Applet.WeatherAppletModel","models":["weather"],"data":{"i13n":{"sec":"app-wea","bucket":"201"},"current":12597132,"favorite":null,"useplus":false},"user_settings":{"unit":"f","curloc":"12597132"},"config":{"moreinfo":{"link":"http://www.weather.com/?par=yahoonews","img":"http://l.yimg.com/dh/ap/default/130915/wcl1.gif"},"enableFav":false,"enableWeatherYQLP":true,"baseLink":"https://weather.yahoo.com","showWidgetLink":true,"urlGeneratorRules":{"enabled":false,"url_format_local":"","url_format_international":"","url_format_local_ajax":"","local_country_code":[]},"daysLimit":4,"fixed_layout":false}},"weather":{"yui_module":"td-applet-weather-atomic-model","yui_class":"TD.Applet.WeatherModel","data":{"weatherDataList":[{"location":{"woeid":"12597132","city":"Yvrac","state":"33","country":"FR"},"link":{"href":"https://weather.yahoo.com/fr/33/yvrac-12597132/"},"timezone":"CET","flickr":{"attribution":null,"image_assets":[{"width":"2048","height":"2048","type":"main","url":"https://s1.yimg.com/nn/lib/metro/clear_n.jpg"}]},"current":{"sunrise":"08:10:59","sunset":"18:22:10","condition":{"description":"Fair","code":"34","images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/fair_day.png"}}},"temp":{"unit":"f","now":"46"},"atmosphere":{"humidity":"87","wind_speed":"7","wind_chill":"43"}},"forecast":{"day":[{"label":"Today","day_of_week":"3","condition":{"description":"Showers","code":"12","images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/rain_day_night.png"}}},"temp":{"high":"46","low":"42","unit":"f"}},{"label":"Thu","day_of_week":"4","condition":{"description":"Showers","code":"11","images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/rain_day_night.png"}}},"temp":{"high":"49","low":"43","unit":"f"}},{"label":"Fri","day_of_week":"5","condition":{"description":"Showers","code":"11","images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/rain_day_night.png"}}},"temp":{"high":"55","low":"50","unit":"f"}},{"label":"Sat","day_of_week":"6","condition":{"description":"Showers","code":"11","images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/rain_day_night.png"}}},"temp":{"high":"56","low":"47","unit":"f"}}]},"woeid":"12597132","accordianState":"e"}],"currentLoc":{"woeid":"12597132","city":"Yvrac","state":"33","country":"FR"}}}},"views":{"main":{"yui_module":"td-applet-weather-atomic-mainview","yui_class":"TD.Applet.WeatherMainView"},"header":{"yui_module":"td-applet-weather-atomic-headerview","yui_class":"TD.Applet.WeatherHeaderView","config":{}}},"templates":{"main":{"yui_module":"td-applet-weather-atomic-templates-main","template_name":"td-applet-weather-atomic-templates-main"}},"i18n":{"CURRENT_LOCATION":"Current Location","WEATHER":"Weather","HIGH":"High","LOW":"Low","TODAY":"Today","TOMORROW":"Tomorrow","SUNDAY":"Sunday","MONDAY":"Monday","TUESDAY":"Tuesday","WEDNESDAY":"Wednesday","THURSDAY":"Thursday","FRIDAY":"Friday","SATURDAY":"Saturday","ABBR_SUNDAY":"Sun","ABBR_MONDAY":"Mon","ABBR_TUESDAY":"Tue","ABBR_WEDNESDAY":"Wed","ABBR_THURSDAY":"Thu","ABBR_FRIDAY":"Fri","ABBR_SATURDAY":"Sat","BLOWING_SNOW":"Blowing Snow","BLUSTERY":"Blustery","CLEAR":"Clear","CLOUDY":"Cloudy","COLD":"Cold","DRIZZLE":"Drizzle","DUST":"Dust","FAIR":"Fair","FREEZING_DRIZZLE":"Freezing Drizzle","FREEZING_RAIN":"Freezing Rain","FOGGY":"Foggy","FORECAST":"Forecast","FULL_FORECAST":"Full forecast","HAIL":"Hail","HAZE":"Haze","HEAVY_SNOW":"Heavy Snow","HOT":"Hot","HURRICANE":"Hurricane","ISOLATED_THUNDERSHOWERS":"Isolated Thundershowers","ISOLATED_THUNDERSTORMS":"Isolated Thunderstorms","LIGHT_SNOW_SHOWERS":"Light Snow Showers","MIXED_RAIN_AND_HAIL":"Mixed Rain and Hail","MIXED_RAIN_AND_SLEET":"Mixed Rain and Sleet","MIXED_RAIN_AND_SNOW":"Mixed Rain and Snow","MIXED_SNOW_AND_SLEET":"Mixed Snow and Sleet","MOSTLY_CLOUDY":"Mostly Cloudy","PARTLY_CLOUDY":"Partly Cloudy","SCATTERED_SHOWERS":"Scattered Showers","SCATTERED_SNOW_SHOWERS":"Scattered Snow Showers","SCATTERED_THUNDERSTORMS":" Scattered Thunderstorms","SEARCH_CITY":"Kindly search your city","SEVERE_THUNDERSTORMS":"Severe Thunderstorms","SHOWERS":"Showers","SLEET":"Sleet","SMOKY":"Smoky","SNOW":"Snow","SNOW_FLURRIES":"Snow Flurries","SNOW_SHOWERS":"Snow Showers","SUNNY":"Sunny","THUNDERSHOWERS":"Thundershowers","THUNDERSTORMS":"Thunderstorms","TORNADO":"Tornado","TROPICAL_STORM":"Tropical Storm","WINDY":"Windy","WEATHER_CHICLET_ERROR":"Visit {linkstart}Yahoo Weather{linkend} for a detailed forecast.","GOTO_WEATHER":"See more Â»","NO_WEATHER_MESSAGE":"Visit {linkstart}Yahoo Weather{linkend} for the latest forecast or {detectstart}click here{detectend} to select your precise location.","TEN_DAY":"10 day"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-activitylist-atomic":{"base":"/sy/os/mit/td/td-applet-activitylist-atomic-0.0.60/","root":"os/mit/td/td-applet-activitylist-atomic-0.0.60/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_50000171"] = {"applet_type":"td-applet-activitylist-atomic","templates":{"list":{"yui_module":"td-applet-activitylist-atomic-templates-list.tumblr","template_name":"td-applet-activitylist-atomic-templates-list.tumblr"}},"i18n":{"ACTION_POST":"Posted","FAVORITE":"favorite","FIND_OUT_WHY":"Find out why Â»","LIVE":"Live","REASON:BREAKING":"Breaking","REASON:COMMENTS":"{count, number, integer} Comments","REASON:EVERGREEN":"","REASON:EXCLUSIVE":"Exclusive","REASON:NEW":"New","REASON:NEW_ON":"New on {site}","REASON:ONLYONYAHOO":"Only on Yahoo","REASON:POPULARONSEARCH":"Popular on Search","REASON:READINGNOW":"{count, number, integer} Reading Now","REASON:SEARCHVISITS":"{count, number, integer} Search Visits","REASON:SHARES":"{count, number, integer} Shares","REASON:SOCIALVISITS":"{count, number, integer} Social Visits","REASON:VIDEOPLAYS":"{count, number, integer} Video Plays","REASON:VIEWS":"{count, number, integer} Views","REASON:VISITS":"{count, number, integer} Visits","REASON:WATCHINGNOW":"{count, number, integer} Watching Now","REASON:WATCHLIVE":"Watch Live","REPLY":"reply","RETWEET":"retweet","TIME_FORMAT":"h:mm A","TIME_FORMAT_SAME_DAY":"[Today]","TIME_FORMAT_NEXT_DAY":"[Tomorrow]","TIME_FORMAT_NEXT_WEEK":"dddd","TIME_FORMAT_ELSE":"MMM D","TITLE":"Only from Yahoo","TWEET_NAVIGATE":"navigate to tweet","VIA_TWITTER":"via Twitter","YLABEL:autos":"Autos","YLABEL:beauty":"Beauty","YLABEL:diy":"DIY","YLABEL:finance":"Finance","YLABEL:food":"Food","YLABEL:games":"Games","YLABEL:gma":"News","YLABEL:health":"Health","YLABEL:homes":"Homes","YLABEL:ivy":"Screen","YLABEL:movies":"Movies","YLABEL:music":"Music","YLABEL:news":"News","YLABEL:omg":"Celebrity","YLABEL:parenting":"Parenting","YLABEL:shopping":"Shopping","YLABEL:sports":"Sports","YLABEL:style":"Style","YLABEL:tech":"Tech","YLABEL:travel":"Travel","YLABEL:tv":"TV","YPROP:autos":"Yahoo Autos","YPROP:beauty":"Yahoo Beauty","YPROP:diy":"Yahoo DIY","YPROP:finance":"Yahoo Finance","YPROP:food":"Yahoo Food","YPROP:games":"Yahoo Games","YPROP:gma":"Yahoo News","YPROP:health":"Yahoo Health","YPROP:homes":"Yahoo Homes","YPROP:ivy":"Yahoo Screen","YPROP:makers":"Yahoo Makers","YPROP:movies":"Yahoo Movies","YPROP:music":"Yahoo Music","YPROP:news":"Yahoo News","YPROP:omg":"Yahoo Celebrity","YPROP:parenting":"Yahoo Parenting","YPROP:politics":"Yahoo Politics","YPROP:shopping":"Yahoo Shopping","YPROP:sports":"Yahoo Sports","YPROP:style":"Yahoo Style","YPROP:tech":"Yahoo Tech","YPROP:travel":"Yahoo Travel","YPROP:tv":"Yahoo TV","YPROP:yahoo":"Yahoo"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
    window.Af = window.Af || {};
    window.Af.config = window.Af.config || {};
    window.Af.config.spaceid = "2023538075";
    window.Af.context= {
        crumb : '80cxxO/imKP',
        guid : '',
        mcCrumb: 'fk1pbFJyPDz',
        ucsCrumb: 'OHDTF5CF/5/',
        device: 'desktop',
        rid : 'dp4utbtbbmtc2',
        default_page : 'p1',
        _p : 'p1',
        site : 'fp',
        lang : 'en-US',
        region : 'US',
        authed: 0,
        enable_dd : '',
        default_appletinit : 'viewport',
        locdrop_crumb: '', woeid: '12597132',
        ssl: 1,
        
        bucket: '201'
        
        
    };

    window.Af.config.transport = {
        crumbForGET: true,
        xhr: '/fpjs',
        consolidate: true,
        timeout: 6000
    };

    window.Af.config.onepush = {
        subscribeTimeout : 5000,
        subscribeMaxTries : 1,
        trackInitComet : true,
        trackLatency : true,
        latencySampleSize : 50,
        publicCometHost: 'https://comet.yahoo.com/comet',
        shutdown: false,
        trackShutdown: false
    };

    window.Af.config.beacon = {
        beaconUncaughtErr: true,
        sampleSizeUncaughtErr: 1,
        maxUncaughtErrCount: 5,
        beaconPageLoadPerf: false,
        sampleSize : 1,
        pathPrefix : '/_td_api/beacon',
        batch: false,
        batchInterval: 3
    };

    window.Af.config.pipe = {
        msg_throttle: 2000,
        err_maxstreak: 2
    };

    window.Af.config.td = {
        remote: '/_td_remote',
        xhr: '/_td_api'
    };

    
    YUI.namespace('Env.My.settings').context = {
        uh_js_file : '',
                videoAsyncEnabled: 0,
        videoplayerScriptElementId: 'videoplayerJs',
        videoplayerUrl: 'https://www.yahoo.com/sy/rx/builds/6.155.0.1453355318/en-us/videoplayer-nextgen-flash-min.js',
        videoAutoplay: 1,
        videoLooping: 0,
        videoForcedError: 0,
        videoFullscreen: 1,
        videoHtml5: 0,
        videoMinControls: 0,
        videoCmsEnv: 'prod',
        videoMustWatch: 1,
        videoMWSticky: 0,
        videoMute: 1,
        videoQosRate: 1,
        videoBuffering: 0,
        videoRelated: 0,
        videoYwaRate: 0,
        videoMaxLoops: 3,
        videoModeEnabled: 0,
        videoTiltbackModeEnabled: 0,
        videoSSButton: 0,
        videoPausescreen: 0,
        videoSingleVideo: 0,
        videoMaxInstances: 12,
        videoInitEvent: 'domready',
        videoExpName: 'advance',
        videoComscoreC4: 'US fp',
        videoplayerScrollThrottle: '200',
        
        themeCssEnabled : false,
        themeBgImageEnabled: false,
        transparentPixelImage: 'https://s.yimg.com/os/mit/media/m/base/images/transparent-95031.png',
        pageMessage: {
            type: '',
            msg: ''
        },
        servingBeaconUrl: 'https://bs.serving-sys.com/BurstingPipe/ActivityServer.bs',
        enablePerfBeacon: '0',
        test_id: '201',
        customizedEnabled: false,
        trendingNowOffScreen: 0
    };

    YUI.namespace('Env.Af.Perf').secondYUIUseStart = (new Date()).getTime();

    var yuiPreloadModules = ["type_advance_desktop_viewer","type_video_manager","type_sdarotate","type_sda"];

    yuiPreloadModules = yuiPreloadModules.concat((function (appletTypes) {
        if (!appletTypes || appletTypes.length === 0) {
            return [];
        }
        var yui_modules = [],
            types = YMedia.Array.hash(appletTypes);
        YMedia.Object.each(window.Af.bootstrap, function (bootstrap, guid) {
            if (!types[bootstrap.applet_type]) {
                 return;
            }
            YMedia.Array.each(['models', 'views', 'templates'], function (type) {
                YMedia.Object.each(bootstrap[type], function (config, name) {
                    if (config.yui_module) {
                         yui_modules.push(config.yui_module);
                    }
                });
            });
        });
        return yui_modules;
    })([]));

    YMedia.use(yuiPreloadModules, function (Y, NAME) {
        YUI.namespace('Env.Af.Perf').secondYUIUseStop = (new Date()).getTime();
        var pageMessage = YUI.Env.My.settings.context.pageMessage,
        enablePerfBeacon = YUI.Env.My.settings.context.enablePerfBeacon,
        test_id = YUI.Env.My.settings.context.test_id,
        pausePerfBeacon = false,
        perfBeacon = {}
                ,
        rapidConfig = rapidPageConfig.rapidConfig,
        YWA_CF_MAP = rapidPageConfig.ywaCF,
        YWA_ACTION_MAP = rapidPageConfig.ywaActionMap,
        YWA_OUTCOME_MAP = rapidPageConfig.ywaOutcomeMap;
        if(YAHOO.i13n) {
            YAHOO.i13n.WEBWORKER_FILE = '/lib/metro/g/myy/rapidworker_1_2_0.0.3.js';
            YAHOO.i13n.SPACEID = '2023538075';
            YAHOO.i13n.TEST_ID = '201';
        }
        if (pageMessage.msg != '') {
            Y.Af.Message.show('body', {level: pageMessage.type, content: pageMessage.msg});
        }

        if (1 === 1) {
            Y.Lang.later(45000, null, function() {
                var failedModules  = Y.all("#myColumns .js-applet .App-loading");
                failedModules.each(function (module) {
                    module.setContent('Sorry! We are temporarily unable to load the content. Please refresh or try again later.');
                    module.removeClass('App-loading');
                    module.addClass('App-failed');
                });
            });
        }
        
        
        
        

        YUI.namespace('Env.Af.Perf').YMyAppCreateStart = (new Date()).getTime();
        YMedia.My.App = new Y.My.App(
            {                i13nConfig: {
                    rapid: rapidConfig,
                    rapidInstance: rapidPageConfig.rapidSingleInstance && YAHOO && YAHOO.i13n && YAHOO.i13n.rapidInstance || null,
                    ywaMaps: {
                        YWA_OUTCOME_MAP: YWA_OUTCOME_MAP,
                        YWA_CF_MAP: YWA_CF_MAP,
                        YWA_ACTION_MAP: YWA_ACTION_MAP
                    }
                },
                inlineViewer: 0,
                summaryView: 0,
                stickerTarget: "#SearchBar-Wrapper-Mini",
                magazineViewerEnabled: 1,
                viewerBlacklistDisable: 1,
                viewer: {
                    viewerAnimation: "slide",
                    pageAnimation: "zoom",
                    scrollTopAmount: 0,
                    highlanderMode: "mega-modal"
                }}
        );
        

        var firePerfBeacon = function(e) {
            var key, value, beaconNode, queryString = [];

            if (Y.Object.size(perfBeacon) < 1 || pausePerfBeacon) {
                return;
            }
            pausePerfBeacon = true;
            for (key in perfBeacon) {
                if (perfBeacon.hasOwnProperty(key)) {
                    queryString.push(key+'='+perfBeacon[key]);
                }
            }
            queryString.push('test='+test_id);
            beaconNode = Y.Node.create('<img src="myperf.php?'+queryString.join('&')+'" style="width:1px;height:1px;position:absolute;left:-900px;">');
            Y.one('body').append(beaconNode);
        };

        if (enablePerfBeacon == '1') {
            Y.one('window').once('scroll', firePerfBeacon);
            Y.Lang.later(10000, null, firePerfBeacon);

            YMedia.My.App.after('appletsinit', function (e) {
                if (pausePerfBeacon == true) {
                    return;
                }
                perfBeacon[e.applet.type] = (new Date()) - myYahoostartTime;
            });
        }
        YMedia.on('appletsinit', function (e) {
            
        });
        
    });
            });
     });
</script>
    <!-- bottom -->

    <!-- Comscore -->
            <!-- Begin comScore Tag -->
		<script>
		  var _comscore = _comscore || [];
		  _comscore.push({
		    c1: "2",
		    c2: "7241469",
		    c5: "2023538075",
		    c7: "https://www.yahoo.com/"
		  });
		  (function() {
		    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
		    s.src = "/sy/lq/lib/3pm/cs_0.2.js";
		    el.parentNode.insertBefore(s, el);
		  })();
		</script>
		<noscript>
		  <img src="https://sb.scorecardresearch.com/p?c1=2&c2=7241469&c7=https%3A%2F%2Fwww.yahoo.com%2F&c5=2023538075&cv=2.0&cj=1" />
		</noscript>
		<!-- End comScore Tag -->

    <!-- Nielsen -->
    

    <!-- Lighthouse  -->
    

    <!-- yaft -->
            <script type="text/javascript" src="/sy/zz/combo?/os/mit/media/p/content/content-aft-min-a202e73.js&/os/mit/media/p/content/yaft2-aftnoad-min-e5cc02b.js&os/mit/media/p/content/yaft2-harbeacon-min-dabe12d.js"> </script>        
        <script type="text/javascript">
            if (window.YAFT !== undefined) {
                var __yaftConfig = {
                    modules: ["UH","mega-uh","mega-topbar","applet_p_","ad-north-base","fea-","my-adsFPAD-base","my-adsLREC-base","my-adsMAST","my-adsTXTL","content-modal-","hl-ad-LREC-","modal-sidekick-","hl-ad-LREC-0","hl-ad-MON-0","hl-ad-MAST-0","hl-ad-LDRB-0","hl-ad-SPL2-0","hl-ad-SPL-0"], 
                    modulesExclude: ["UH-Search","UH-ColWrap","mega-uh-wrapper"],
                    canShowVisualReport: false,
                    useNormalizeCoverage: true,
                    generateHAR: false,
                    includeOnlyAft2: true,
                    useNativeStartRender : true,
                    modulesAft2Container: ["hl-viewer"],
                    maxWaitTime: 6000
                };
                __yaftConfig.plugins = [];                __yaftConfig.plugins.push({
                     name: 'aftnoad',
                     isPre: true,
                     config: {
                         useNormalizeCoverage: true,
                         adModules:["ad-north-base","my-adsFPAD-base","my-adsLREC-base","my-adsTL1","my-adsMAST","hl-ad-LREC-0","hl-ad-MAST-0","hl-ad-LDRB-0","hl-ad-SPL2-0","my-adsTXTL","hl-ad-SPL-0"]
                     }
                });
                            __yaftConfig.plugins.push({
                name: 'har',
                config: {
                    sec: 'har',
                    rapid: function() { if(YMedia && YMedia.My && YMedia.My.App && YMedia.My.App.getRapidTracker) {return YMedia.My.App.getRapidTracker(); } else {return null;}},
                    aftThreshold: 10000,
                    onlySlowest: false
                }
            });
            __yaftConfig.generateHAR = true;
                        if (window.LH && window.LH.isInitialized) {
            window.LH.tag('b',{val: '201'});               
            window.LH.tag("l", {val: false});
        }
                        window.aft2CB = function(data, error) {
        if (!error) {
            var aft2 = Math.round(data.aft);
            var vic2 = data.visuallyComplete;
            var srt2 = Math.round(data.startRender);

            if (window.LH && window.LH.isInitialized) {
                var lhRecord = function lhRecord(name, type, startTime, duration) {
                    window.LH.record(name, {
                        name: name, type: type, startTime: startTime, duration: duration
                    });
                };

                lhRecord('AFT', 'mark', aft2, 0);
                lhRecord('AFT2', 'mark', aft2, 0);
                lhRecord('VIC2', 'mark', vic2, 0);
                lhRecord('STR2', 'mark', srt2, 0);
                window.LH.beacon({
                    clearMarks:false,
                    clearMeasures: false,
                    clearCustomEntries: true,
                    clearTags: false
                });
            }

            var rapidInstance = (YMedia.My && YMedia.My.App && YMedia.My.App.getRapidTracker) ? YMedia.My.App.getRapidTracker() : null;
            if (rapidInstance) {
                var afterPageLoad = {
                    AFT: aft2,
                    AFT2: aft2,
                    STR: srt2,
                    VIC: vic2
                };
                var perfData = {
                    perf_commontime: {afterPageLoad: afterPageLoad}
                };
                rapidInstance.beaconPerformanceData(perfData);
            }
        }
    };
                window.YAFT.init(__yaftConfig, function(data, error) {
                    if (!error) {
                        try {
                                    if (window.LH && window.LH.isInitialized) {
            var results = [0], module = '', index = '', lhRecord = '';
            for (module in data.modulesReport) {
                for (index in data.modulesReport[module].resources) {
                    results.push(Math.round(data.modulesReport[module].resources[index].durationFromNStart));
                }
            }
            lhRecord = function (ma, va, custom) {
                var res = '';                                
                if (undefined === custom) {
                    if (va && !isNaN(va)) {
                        res = Math.round(va);
                    } else {
                        res = 0;
                    }
                } else {
                    res = va;
                }
                    window.LH.record(ma, { name: ma, type: 'mark', startTime: res, duration: 0 });
            };
                            
            lhRecord('AFT', data.aft);
            lhRecord('AFT1', data.aft);
            if (data.aftNoAd) {
                lhRecord('AFTNOAD', data.aftNoAd);
            }
            lhRecord('X_FB1', 37);
            lhRecord('X_FBN', 61);
            lhRecord('PLT_CACHED_REQs', data.httpRequests.onloadCached);
            lhRecord('COSTLY_RESOURCE', Math.max.apply(null, results));
            lhRecord('StartRender', data.startRender);
            if (screen) {
                lhRecord('SCR_H', screen.height);
                lhRecord('SCR_W', screen.width);
            }
            if (window.FPAD_rendered) {
                lhRecord('xAFT', data.aft);
                lhRecord('xPLT', data.pageLoadTime);
                if (window.rtAdStart && window.rtAdDone) {
                    lhRecord('ADSTART_FPAD', Math.round(rtAdStart));
                    lhRecord('ADEND_FPAD', Math.round(rtAdDone));
                }
            }

            // Track ad metrics
            if ( window._adLT) {
                for (var i = 0; i< _adLT.length; i++) {
                    lhRecord(_adLT[i][0], _adLT[i][1]);
                }
            }
            window.LH.beacon({ clearMarks: false, clearMeasures: false, clearCustomEntries: true, clearTags: false });   
        }
                                    var rapidInstance = (YMedia.My && YMedia.My.App && YMedia.My.App.getRapidTracker) ? YMedia.My.App.getRapidTracker() : null;
        if(rapidInstance) {
            var initialPageLoad = {
                AFT: Math.round(data.aft),
                AFT1: Math.round(data.aft),
                STR: data.startRender,
                VIC: data.visuallyComplete,
                PLT: data.pageLoadTime,
                DOMC: data.domElementsCount,
                HTTPC: data.httpRequests.count,
                CP: data.totalCoveragePercentage,
                NCP: data.normTotalCoveragePercentage
            };

            if(data.aftNoAd) {
                initialPageLoad.AFTNOAD = Math.round(data.aftNoAd);
            }              

            var customPerfData = {},
                pagePerfData = {},
                results = [];
            
            var yaftResults = [0], yaftModule = '', yaftIndex = '';
            // Find costly resource time
            for (yaftModule in data.modulesReport) {
                for (yaftIndex in data.modulesReport[yaftModule].resources) {
                    yaftResults.push(Math.round(data.modulesReport[yaftModule].resources[yaftIndex].durationFromNStart));
                }
            }
            pagePerfData['COSTLY_RESOURCE'] = Math.max.apply(null, yaftResults);
            pagePerfData['X_FB1'] = 37;
            pagePerfData['X_FBN'] = 61; 
   
            // Log ad perf data to rapid perf metric
            if (window.FPAD_rendered) {
                pagePerfData['xAFT'] = data.aft;
                pagePerfData['xPLT'] = data.pageLoadTime;
                if (window.rtAdStart && window.rtAdDone) {
                    pagePerfData['ADSTART_FPAD'] = Math.round(rtAdStart);
                    pagePerfData['ADEND_FPAD'] = Math.round(rtAdDone);
                }
            }
            
            // Track ad metrics
            if ( window._adLT) {
                for (var i = 0; i< _adLT.length; i++) {
                    pagePerfData[_adLT[i][0]]  = _adLT[i][1];
                }
            }
                        if(resourceTimingAssets) {
                for(i in resourceTimingAssets) {
                    if (resourceTimingAssets.hasOwnProperty(i)) {
                        resourceName = window.performance.getEntriesByName(resourceTimingAssets[i]);
                        if(resourceName && resourceName.length) {
                            var resourceFinish = resourceName[0].responseEnd;
                            pagePerfData[i] = Math.round(resourceFinish);
                        }
                    }
                }
            }

            customPerfData['utm'] = pagePerfData;
            var perfData = {
                perf_commontime: {initialPageLoad: initialPageLoad},
                perf_usertime: customPerfData
            };
            rapidInstance.beaconPerformanceData(perfData);
        }
                        } catch (e) {}                                                        
                    }
               });
            }
      </script>

    <!-- Via https/1.1 ir15.fp.ir2.yahoo.com[D992B341] (YahooTrafficServer), http/1.1 usproxy1.fp.bf1.yahoo.com[448EE3A0] (YahooTrafficServer) -->
    <!-- sid=2023538075 -->
    

    </body>
</html>
<!-- myproperty:myservice-us:0:Success -->
<!-- slw9.fp.bf1.yahoo.com compressed/chunked Wed Feb 10 17:38:10 UTC 2016 -->
