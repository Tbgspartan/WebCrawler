



<!DOCTYPE html>

<!--[if IE]><![endif]--><html><head><meta charset="utf-8" /><meta http-equiv="Content-Type" content="text/html;charset=UTF-8" /><title>globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento</title><meta name="google-site-verification" content="BKmmuVQac1JM6sKlj3IoXQvffyIRJvJfbicMouA2a88" /><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.5" /><meta property="og:title" content="globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento"/><meta property="og:type" content="website"/><meta property="og:url" content="http://www.globo.com/"/><meta property="og:image" content="http://s.glbimg.com/en/ho/static/globocom2012/img/fb_marca.png"/><meta property="og:site_name" content="globo.com"/><meta property="og:description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta property="fb:page_id" content="224969370851736" /><meta property='busca:title' content='Globo.com' /><meta property='busca:species' content='Home' /><meta property='busca:issued' content='22/10/2015 21:15:10' /><meta property='busca:modified' content='22/10/2015 21:15:10' /><meta property='busca:publisher' content='www.globo.com' /><meta name="description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta name="keywords" content="NotÃ­cias, Entretenimento, Esporte, Tecnologia, Portal, ConteÃºdo, Rede Globo, TV Globo, VÃ­deos, TelevisÃ£o"/><meta name="application-name" content="Globo.com"/><meta name="msapplication-TileColor" content="#0669DE"/><meta name="msapplication-TileImage" content="http://s.glbimg.com/en/ho/static/globocom2012/img/globo-win-tile.png"/><link rel="canonical" href="http://www.globo.com/" /><link rel="shortcut icon" href="http://s.glbimg.com/en/ho/static/globocom2012/img/favicon.png" type="image/ico" /><link rel="apple-touch-icon-precomposed" sizes="57x57" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone.png" /><link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-ipad.png" /><link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone-retina.png" /><link rel="stylesheet" href="http://s.glbimg.com/en/ho/static/CACHE/css/050f23516bb7.css" type="text/css" media="screen" /><!--[if lt IE 10]><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/d783f8b1c48f.js"></script><![endif]--><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/34511e603636.js"></script></head><!--[if lt IE 7 ]><body class="ie ie6"><![endif]--><!--[if IE 7 ]><body class="ie ie7"><![endif]--><!--[if IE 8 ]><body class="ie ie8"><![endif]--><!--[if IE 9 ]><body class="ie ie9gt ie9"><![endif]--><!--[if gt IE 9 ]><body class="ie ie9gt ie10gt"><![endif]--><!--[if !IE]><!--><body><!--<![endif]--><div id="home-push-menu"><div class="home-push-menu-container"><div class="home-push-menu-initial"><div class="home-push-menu-header">NAVEGUE</div><ul class="barra-itens" data-analytics-context="Link Produto"><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://g1.globo.com" accesskey="n" class="barra-item-g1 link-produto analytics-area analytics-id-T">g1</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://globoesporte.globo.com" accesskey="e" class="barra-item-globoesporte link-produto analytics-area analytics-id-T">globoesporte</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://gshow.globo.com" accesskey="t" class="barra-item-gshow link-produto analytics-area analytics-id-T">gshow</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://famosos.globo.com" accesskey="t" class="barra-item-famosos-etc link-produto analytics-area analytics-id-T">famosos &amp; etc</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://techtudo.com.br" accesskey="b" class="barra-item-tech link-produto analytics-area analytics-id-T">tecnologia</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://video.globo.com" accesskey="v" class="barra-item-videos link-produto analytics-area analytics-id-T">vÃ­deos</a></li><li><a href="#" id="home-push-menu-show-email">e-mail</a><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="2.423,0 0,2.399 5.153,7.5 0,12.602 2.423,15 10,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-right.png"></image></svg></span></li><li><a href="https://meuperfil.globo.com/">central globo.com</a></li></ul></div><div class="home-push-menu-email"><ul><li><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="7.577,0 10,2.399 4.847,7.5 10,12.602 7.577,15 0,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-left.png"></image></svg></span><a id="home-push-menu-hide-email" href="#">e-mail</a></li><li><a href="https://login.globo.com/login/1948">globomail free</a></li><li><a href="https://login.globo.com/login/1">globomail pro</a></li></ul></div></div></div><div id="home-pagecontent" class=" home-menu-rendered" style=""><script type="text/javascript">
function comScore(C){var A="comScore",j=document,y=j.location,B="",z="undefined",x=2048,D,v,i,w,c="characterSet",l="defaultCharset",u=(typeof encodeURIComponent!=z?encodeURIComponent:escape);if(j.cookie.indexOf(A+"=")!=-1){i=j.cookie.split(";");for(w=0,y=i.length;w<y;w++){var d=i[w].indexOf(A+"=");if(d!=-1){B="&"+unescape(i[w].substring(d+A.length+1))}}}C=C+"&ns__t="+(new Date().getTime());C=C+"&ns_c="+(j[c]?j[c]:(j[l]?j[l]:""))+"&c8="+u(j.title)+B+"&c7="+u(y&&y.href?y.href:j.URL)+"&c9="+u(j.referrer);if(C.length>x&&C.indexOf("&")>0){D=C.substr(0,x-8).lastIndexOf("&");C=(C.substring(0,D)+"&ns_cut="+u(C.substring(D+1))).substr(0,x)}if(j.images){v=new Image();if(typeof ns_p==z){ns_p=v}v.src=C}else{j.write('<p><img src="'+C+'" height="1" width="1" alt="*"/></p>')}}comScore("http"+(document.location.href.charAt(4)=="s"?"s://sb":"://b")+".scorecardresearch.com/p?c1=2&c2=6035227");</script><noscript><p><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6035227&amp;c4=http%3A%2F%2Fwww.globo.com%2Findex.html" height="1" width="1" alt="*"/></p></noscript><script>
libby.loadScript('http://b.scorecardresearch.com/c2/6035227/cs.js');</script><script type="text/javascript">
var utag_data = {"structure_tree": "[\"globocom\"]", "ad_site_page": "{\"adUnit\":\"tvg_Globo.com.Home\", \"adPositions\":[\"banner_mobile_topo\",\"banner_mobile_meio\",\"banner_mobile_fim\",\"banner_slim_topo\",\"banner_slb_meio\",\"banner_slb_fim\",\"banner_selo4\",\"banner_floating\"]}", "page_name": "index"};</script><script type="text/javascript">
(function (a,b,c,d) {a = '//tags.globo.com/utag/globo/home/prod/utag.js';b=document;c = 'script';d=b.createElement(c);d.src=a;d.type = 'text/java' + c;d.async=true;a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);})();
</script><header><div id="base-container-width-element" class="container"><div class="header-mobile-top analytics-area analytics-id-H"><a id="open-menu" class="open-menu" href="#"><svg version="1.1" class="burger-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19px" height="13px" viewBox="0 0 19 13" enable-background="new 0 0 19 13" xml:space="preserve"><g><path fill="#1063E0" d="M17.493,5H1.485C0.665,5,0,5.665,0,6.485C0,7.305,0.665,7.97,1.485,7.97h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,5.665,18.313,5,17.493,5z M1.485,2.971h16.008c0.82,0,1.486-0.666,1.486-1.486C18.979,0.666,18.313,0,17.493,0H1.485C0.665,0,0,0.666,0,1.484C0,2.305,0.665,2.971,1.485,2.971z M17.493,10H1.485C0.665,10,0,10.666,0,11.485c0,0.82,0.665,1.485,1.485,1.485h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,10.666,18.313,10,17.493,10z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/menu-button.png"></image></svg></a><a class="logo-globo analytics-area analytics-id-T" href="http://globo.com" title="globo.com"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 98 22" enable-background="new 0 0 98 22" xml:space="preserve"><g><path fill="#1063E0" d="M92.982,5.033c-1.115,0-2.549,0.601-3.459,1.625c-0.865-1.069-2.026-1.625-3.392-1.625c-1.205,0-2.252,0.489-3.049,1.336c-0.114-0.757-0.661-1.203-1.502-1.203c-0.957,0-1.639,0.712-1.639,1.781v8.551c0,1.069,0.682,1.781,1.639,1.781s1.638-0.712,1.638-1.781V9.909c0-1.292,0.592-2.205,1.936-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.682,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.909c0-1.292,0.728-2.205,1.934-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.683,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.352C97.466,6.547,95.645,5.033,92.982,5.033z M10.036,5.166c-0.91,0-1.434,0.49-1.639,1.336c-0.614-0.98-1.889-1.469-3.004-1.469C2.093,5.033,0,8.039,0,11.268c0,3.184,1.616,6.146,5.052,6.146c1.502,0,2.617-0.535,3.299-1.515h0.045v0.757c0,1.625-1.183,2.538-2.776,2.538c-1.889,0-3.049-1.001-4.097-1.001c-0.637,0-1.319,0.8-1.319,1.403C0.205,21.354,4.028,22,5.371,22c3.732,0,6.304-1.938,6.304-5.791V6.947C11.674,5.878,10.991,5.166,10.036,5.166z M5.826,14.474c-1.707,0-2.549-1.559-2.549-3.273c0-1.492,0.842-3.229,2.549-3.229c1.797,0,2.571,1.781,2.571,3.339C8.397,12.871,7.578,14.474,5.826,14.474z M14.318,0c-0.956,0-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781s1.639-0.712,1.639-1.781V1.782C15.957,0.712,15.274,0,14.318,0z M22.479,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169c3.527,0,5.872-2.829,5.872-6.169C28.35,7.994,25.938,5.033,22.479,5.033z M22.479,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.797-3.273,2.594-3.273c1.798,0,2.594,1.737,2.594,3.273C25.073,12.782,24.254,14.474,22.479,14.474z M35.371,5.033c-1.138,0-2.253,0.378-3.118,1.113V1.782C32.253,0.712,31.57,0,30.614,0s-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781c0.911,0,1.434-0.49,1.639-1.336c0.614,0.979,1.889,1.471,3.004,1.471c3.3,0,5.393-3.008,5.393-6.036C40.65,8.261,38.897,5.033,35.371,5.033z M34.825,14.474c-1.798,0-2.572-1.781-2.572-3.34c0-1.559,0.819-3.162,2.572-3.162c1.707,0,2.548,1.759,2.548,3.273C37.373,12.737,36.531,14.474,34.825,14.474z M46.863,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169s5.872-2.829,5.872-6.169C52.735,7.994,50.322,5.033,46.863,5.033z M46.863,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273s2.594,1.737,2.594,3.273C49.458,12.782,48.638,14.474,46.863,14.474z M55.425,13.939c-0.978,0-1.774,0.779-1.774,1.736s0.796,1.738,1.774,1.738c0.979,0,1.775-0.781,1.775-1.738S56.404,13.939,55.425,13.939z M66.583,13.805c-0.386,0-1.616,0.669-2.436,0.669c-1.774,0-2.753-1.648-2.753-3.229c0-1.648,0.888-3.273,2.753-3.273c1.048,0,1.776,0.623,2.594,0.623c0.775,0,1.253-0.868,1.253-1.536c0-1.56-2.709-2.026-4.006-2.026c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.343,6.169,5.871,6.169c1.184,0,4.006-0.558,4.006-2.206C67.994,14.451,67.355,13.805,66.583,13.805z M73.446,5.033c-3.459,0-5.872,2.961-5.872,6.212c0,3.34,2.344,6.169,5.872,6.169c3.527,0,5.871-2.829,5.871-6.169C79.317,7.994,76.904,5.033,73.446,5.033z M73.446,14.474c-1.774,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273c1.797,0,2.594,1.737,2.594,3.273C76.04,12.782,75.221,14.474,73.446,14.474z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/logo.png"></image></svg></a><form id="search-form" action="http://www.globo.com/busca/" method="get"><div id="search-container" class="search-container"><span><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="12px" height="12px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><g><path fill="#999999" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/search-small.png"></image></svg></span><div class="search-internal-container"><input type="text" id="search-globo" name="q" placeholder="encontre na globo.com"></div><a id="close-search" class="close-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="11px" height="11px" viewBox="0 0 11 11" enable-background="new 0 0 11 11" xml:space="preserve"><g><path fill="#666766" d="M6.884,5.501c1.955-1.955,3.733-3.732,3.809-3.808c0.388-0.388,0.388-1.015,0-1.403 c-0.387-0.386-1.015-0.386-1.402,0C9.215,0.365,7.437,2.144,5.482,4.099c-1.947-1.947-3.714-3.714-3.79-3.79 c-0.387-0.388-1.015-0.388-1.402,0c-0.387,0.387-0.387,1.016,0,1.401c0.076,0.076,1.842,1.843,3.79,3.791 C2.135,7.446,0.372,9.209,0.297,9.285c-0.387,0.387-0.387,1.015,0,1.401c0.387,0.389,1.015,0.389,1.402,0 c0.075-0.075,1.838-1.838,3.784-3.783c1.953,1.952,3.727,3.727,3.802,3.802c0.387,0.387,1.015,0.387,1.402,0 c0.387-0.387,0.387-1.015,0-1.402C10.611,9.228,8.837,7.454,6.884,5.501z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/close.png"></image></svg></a></div><a id="open-search" class="open-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><path fill="#1063e0" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path><image src="http://s.glbimg.com/gl/ba/img/common/search.png"></image></svg></a></form></div><h1 class="analytics-area analytics-id-H"><a class="logo-topo analytics-area analytics-id-T" href="/" title="globo.com">
                globo.com
            </a></h1><div class="complementos analytics-area analytics-id-T"><div id="busca-padrao"><form action="http://www.globo.com/busca/" method="get" accept-charset="utf-8"><fieldset><legend>buscar</legend><label for="busca-campo" class="label-for-search">buscar</label><input id="busca-campo" type="text" name="q" autocomplete="off" accesskey="s" lang="pt-BR" x-webkit-speech speech /><button type="submit">buscar</button></fieldset></form></div><div id="libby-box-previsao-tempo" class="analytics-area analytics-id-L"></div></div><nav><div class="spacer"></div><header id="header-produto" class="header-navegacao header-home" data-analytics="T"><div id="glbbarrawidget"></div><div class="floating-bar"><div class="header-bar"><div class="grid-12 clearfix area-principal row"><div class="column"><div class="menu-area to-left" data-analytics="S"><div class="menu-button"><div class="burger"><b></b><b></b><b></b></div><span class="menu-label">MENU</span></div><span class="menu-produto"></span></div><a class="logo-area" href="http://www.globo.com/"><h1 class="logo icon-produto">Home</h1></a><div class="to-right"><div class="search-area"><form id="frmBuscaScroll" action="#"><input id="search-input-floating" type="text" name="q" placeholder="BUSCAR" class="search" /><label for="search-input-floating" class="glass-container"><div class="glass"><div class="iglass css3pie"></div></div></label></form></div></div></div></div></div></div><div class="cube-container"><div class="cube"><div class="face front"><div class="grid-12 clearfix area-principal row"><div class="column"><div class="menu-area to-left" data-analytics="S"><div class="menu-button"><div class="burger"><b></b><b></b><b></b></div><span class="menu-label">MENU</span></div><span class="menu-produto"></span></div><a class="logo-area" href="http://www.globo.com/"><h1 class="logo icon-produto">Home</h1></a><div class="to-right"><div class="search-area"><form id="frmBuscaScroll" action="#"><input id="search-input-top" type="text" name="q" placeholder="BUSCAR" class="search" /><label for="search-input-top" class="glass-container"><div class="glass"><div class="iglass css3pie"></div></div></label></form></div><div id="busca-padrao"></div></div></div></div></div></div></div></header><script>
(function(window,document) {try{new CustomEvent("test");} catch(e){var CustomEvent=function(event,params) {var evt;params=params || {bubbles: false,
cancelable: false,
detail: undefined};evt=document.createEvent("CustomEvent");evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);return evt;};
CustomEvent.prototype=window.Event.prototype;window.CustomEvent=CustomEvent;}
document.dispatchEvent(new CustomEvent('glb.headerDom.ready'));})(window,document);</script><style type="text/css">
    /*
       evita que o markup do menu seja exibido sem estilo enquanto
       ele nÃ£o foi carregado
    */
    #menu-container {
        display: none;
    }
    #menu-fonts {
        height: 1px;
        width: 1px;
        color: transparent;
        overflow: hidden;
        position: absolute;
    }
</style>

<nav id="menu-container" class="regua-navegacao-tab menu-no-animation">
    

<script id="menu-carousel-template" type="x-tmpl-mustache">

<div id="menu-carousel-header-sizes-infos" class="menu-carousel-header-sizes-infos">
</div>
<div id="menu-carousel-header" class="menu-carousel menu-carousel-header swiper-container">
    <ul class="menu-brands swiper-wrapper">
        {{#portals}}
        <li data-produto="{{name}}" class="menu-carousel-item menu-carousel-{{name}} {{#active}}menu-carousel-item-active{{/active}} swiper-slide">
            <a href="{{link}}" class="menu-carousel-link" data-index="{{index}}">
                <svg class="brand-icon">
                    <use class="menu-logo" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#eixo-icone-{{name}}"></use>
                </svg>
            </a>
            <div class="menu-carousel-link-shadow"></div>
        </li>
        {{/portals}}
    </ul>
</div>
<div id="menu-carousel-body" class="menu-carousel menu-carousel-body swiper-container">
    <ul class="menu-brands swiper-wrapper">
        {{#portals}}
        <li data-produto="{{name}}" class="menu-carousel-item menu-carousel-{{name}} {{#active}}menu-carousel-item-active{{/active}} swiper-slide">
            <div id="menu-rounder" class="menu-rounder">{{{menu}}}</div>
        </li>
        {{/portals}}
    </ul>
</div>

</script>

<script id="menu-rounder-template" type="x-tmpl-mustache">

  <div id="menu-cascade" class="menu-cascade" data-menu-index="{{index}}">
    <ul class="menu-root menu-level menu-level-0">{{#items}}{{>recursive_partial}}{{/items}}</ul>
  </div>
  <div id="menu-addon" class="menu-addon"></div>

</script>

<script id="menu-custom-template" type="x-tmpl-mustache">

    {{#items}}{{>recursive_partial}}{{/items}}

</script>


<script id="menu-dinamico-template" type="x-tmpl-mustache">

<li id="menu-{{deepness}}-{{tituloSlug}}" class="menu-item {{#separador}} menu-item-separator{{/separador}}{{#destacado}} menu-item-highlighted{{/destacado}}{{#agrupador}} menu-item-grouped{{/agrupador}}{{#hasChildren}} is-father{{/hasChildren}}{{#hasSubmenuBroken}} menu-item-submenu-broken{{/hasSubmenuBroken}}">

    {{#shouldBeLink}}<a href="{{link}}" class="menu-item-link">{{/shouldBeLink}}
    {{^shouldBeLink}}<span class="menu-item-link">{{/shouldBeLink}}
        <span class="menu-item-title">{{titulo}}<span class="menu-item-arrow"><svg class="regua-navegacao-icon"><use xlink:href="#regua-arrow" /></svg></span></span>
    {{#shouldBeLink}}</a>{{/shouldBeLink}}
    {{^shouldBeLink}}</span>{{/shouldBeLink}}

    {{#hasChildren}}
        <div class="menu-level menu-submenu menu-submenu-level{{deepness}}{{#hasSubmenuBroken}} menu-item-submenu-broken{{/hasSubmenuBroken}} menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back"><span class="menu-item-arrow"><svg class="regua-navegacao-icon"><use xlink:href="#regua-arrow" /></svg></span><span class="regua-navegacao-menu-item">{{titulo}}</span></a>
            <ul class="menu-submenu-vertical-scroll">
            {{#children}}
                {{>recursive_partial}}
            {{/children}}
            <ul>
        </div>
    {{/hasChildren}}
</li>

</script>


    <div id="menu-rounder" class="menu-rounder">
        <div id="menu-cascade" class="menu-cascade" data-menu-index="0">
            

<ul class="menu-root menu-level menu-level-0">
    
        




<li class="menu-item is-father " id="menu-1-famosos">

    <a class="menu-item-link">
        <span class="menu-item-title">Famosos</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Famosos</a>
            
                

                




<li class="menu-item " id="menu-2-ego">

    <a href="http://ego.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-paparazzo">

    <a href="http://ego.globo.com/paparazzo/index.html" class="menu-item-link">
        <span class="menu-item-title">Paparazzo</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-patricia-kogut">

    <a href="http://kogut.oglobo.globo.com/noticias-da-tv/index.html" class="menu-item-link">
        <span class="menu-item-title">Patricia Kogut</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-quem">

    <a href="http://revistaquem.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Quem</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-retratos-da-vida">

    <a href="http://extra.globo.com/famosos/" class="menu-item-link">
        <span class="menu-item-title">Retratos da Vida</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-moda-estilo">

    <a class="menu-item-link">
        <span class="menu-item-title">Moda &amp; Estilo</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Moda &amp; Estilo</a>
            
                

                




<li class="menu-item " id="menu-2-ela-no-o-globo">

    <a href="http://ela.oglobo.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Ela no O Globo</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-estilo-no-gshow">

    <a href="http://gshow.globo.com/Estilo/" class="menu-item-link">
        <span class="menu-item-title">Estilo no Gshow</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-moda-no-ego">

    <a href="http://ego.globo.com/moda/index.html" class="menu-item-link">
        <span class="menu-item-title">Moda no Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-moda-no-gnt">

    <a href="http://gnt.globo.com/moda/" class="menu-item-link">
        <span class="menu-item-title">Moda no GNT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-glamour">

    <a href="http://revistaglamour.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Glamour</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-gq-brasil">

    <a href="http://gq.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista GQ Brasil</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-marie-claire">

    <a href="http://revistamarieclaire.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Marie Claire</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-vogue">

    <a href="http://vogue.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Vogue</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-bem-estar-saude">

    <a class="menu-item-link">
        <span class="menu-item-title">Bem-Estar &amp; SaÃºde</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Bem-Estar &amp; SaÃºde</a>
            
                

                




<li class="menu-item " id="menu-2-beleza-no-ego">

    <a href="http://ego.globo.com/beleza/index.html" class="menu-item-link">
        <span class="menu-item-title">Beleza no Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-beleza-no-gnt">

    <a href="http://gnt.globo.com/beleza/" class="menu-item-link">
        <span class="menu-item-title">Beleza no GNT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-beleza-no-gshow">

    <a href="http://gshow.globo.com/Estilo/Beleza/" class="menu-item-link">
        <span class="menu-item-title">Beleza no Gshow</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-bem-estar-no-g1">

    <a href="http://g1.globo.com/bemestar/" class="menu-item-link">
        <span class="menu-item-title">Bem Estar no G1</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-bem-estar-no-gnt">

    <a href="http://gnt.com.br/bemestar" class="menu-item-link">
        <span class="menu-item-title">Bem Estar no GNT </span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-horoscopo-no-ego">

    <a href="http://horoscopo.ego.globo.com/" class="menu-item-link">
        <span class="menu-item-title">HorÃ³scopo no Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-maes-no-gnt">

    <a href="http://gnt.globo.com/maes-e-filhos/" class="menu-item-link">
        <span class="menu-item-title">MÃ£es no GNT </span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-meus-5-minutos">

    <a href="http://meus5minutos.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Meus 5 Minutos</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-crescer">

    <a href="http://revistacrescer.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Crescer</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-casa-decoracao">

    <a class="menu-item-link">
        <span class="menu-item-title">Casa &amp; DecoraÃ§Ã£o</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Casa &amp; DecoraÃ§Ã£o</a>
            
                

                




<li class="menu-item " id="menu-2-casa-no-gnt">

    <a href="http://gnt.globo.com/casa-e-decoracao/" class="menu-item-link">
        <span class="menu-item-title">Casa no GNT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-casa-vogue">

    <a href="http://casavogue.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Casa Vogue</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-casa-e-jardim">

    <a href="http://revistacasaejardim.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Casa e Jardim</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-do-zap">

    <a href="http://revista.zapimoveis.com.br/?utm_source=globo.com-etc&amp;utm_medium=botao-casa-decoracao&amp;utm_campaign=revista-imoveis" class="menu-item-link">
        <span class="menu-item-title">Revista do Zap</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-gastronomia">

    <a class="menu-item-link">
        <span class="menu-item-title">Gastronomia</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Gastronomia</a>
            
                

                




<li class="menu-item " id="menu-2-casa-e-comida">

    <a href="http://revistacasaejardim.globo.com/Casa-e-Comida/" class="menu-item-link">
        <span class="menu-item-title">Casa e Comida</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-colheradas">

    <a href="http://meus5minutos.globo.com/blogs/Colheradas/" class="menu-item-link">
        <span class="menu-item-title">Colheradas</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-receitas-da-ana-maria">

    <a href="http://gshow.globo.com/programas/mais-voce/Receitas-da-Ana-Maria/" class="menu-item-link">
        <span class="menu-item-title">Receitas da Ana Maria</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-receitascom">

    <a href="http://gshow.globo.com/receitas/" class="menu-item-link">
        <span class="menu-item-title">Receitas.com</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-receitas-no-gnt">

    <a href="http://gnt.globo.com/receitas/" class="menu-item-link">
        <span class="menu-item-title">Receitas no GNT</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-musica">

    <a class="menu-item-link">
        <span class="menu-item-title">MÃºsica</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">MÃºsica</a>
            
                

                




<li class="menu-item " id="menu-2-altas-horas">

    <a href=" http://gshow.globo.com/programas/altas-horas/" class="menu-item-link">
        <span class="menu-item-title">Altas Horas </span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-g1-musica">

    <a href="http://g1.globo.com/musica/" class="menu-item-link">
        <span class="menu-item-title">G1 MÃºsica</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-gshow-musica">

    <a href="http://gshow.globo.com/Musica/" class="menu-item-link">
        <span class="menu-item-title">Gshow MÃºsica</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-musica-no-multishow">

    <a href="http://multishow.globo.com/musica/" class="menu-item-link">
        <span class="menu-item-title">MÃºsica no Multishow</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-musicacombr">

    <a href="http://musica.com.br/" class="menu-item-link">
        <span class="menu-item-title">MÃºsica.com.br</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-radiobeat">

    <a href="http://radiobeat.com.br/" class="menu-item-link">
        <span class="menu-item-title">RADIOBEAT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-som-brasil">

    <a href="http://globotv.globo.com/rede-globo/som-brasil/" class="menu-item-link">
        <span class="menu-item-title">Som Brasil</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-cinema-teatro">

    <a class="menu-item-link">
        <span class="menu-item-title">Cinema &amp; Teatro</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Cinema &amp; Teatro</a>
            
                

                




<li class="menu-item " id="menu-2-canal-brasil">

    <a href="http://canalbrasil.globo.com/ " class="menu-item-link">
        <span class="menu-item-title">Canal Brasil</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-canal-universal">

    <a href="http://universal.globo.com" class="menu-item-link">
        <span class="menu-item-title">Canal Universal</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-filmes-na-globo">

    <a href="http://redeglobo.globo.com/filmes/index.html" class="menu-item-link">
        <span class="menu-item-title">Filmes na Globo</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-g1-cinema">

    <a href="http://g1.globo.com/pop-arte/cinema/" class="menu-item-link">
        <span class="menu-item-title">G1 Cinema</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-globo-filmes">

    <a href=" http://globofilmes.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Globo Filmes</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-megapix">

    <a href="http://megapix.globo.com/" class="menu-item-link">
        <span class="menu-item-title">MegaPix</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-monet">

    <a href="http://revistamonet.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Monet</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-teatro">

    <a href="http://www.agentesevenoteatro.com.br/" class="menu-item-link">
        <span class="menu-item-title">Teatro</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-telecine">

    <a href="http://telecine.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Telecine</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item menu-item-highlighted " id="menu-1-ego">

    <a href="http://ego.globo.com/" class="menu-item-link">
        <span class="menu-item-title">ego</span>
    </a>

    
</li>

    
        




<li class="menu-item menu-item-highlighted " id="menu-1-quem">

    <a href="http://revistaquem.globo.com/" class="menu-item-link">
        <span class="menu-item-title">quem</span>
    </a>

    
</li>

    
        




<li class="menu-item menu-item-highlighted " id="menu-1-patricia-kogut">

    <a href="http://kogut.oglobo.globo.com/" class="menu-item-link">
        <span class="menu-item-title">patricia kogut</span>
    </a>

    
</li>

    
</ul>

        </div>
        <div id="menu-addon" class="menu-addon"></div>
    </div>

    
</nav>

<div id="menu-fonts">
    
    
    <span class="open-sans">BESbswy</span>
    <span class="open-sans-bold">BESbswy</span>
    <span class="open-sans-light">BESbswy</span>
    <span class="roboto-slab">BESbswy</span>
    <span class="proximanova-semibold">BESbswy</span>
</div>

<div id="menu-content-overlay"></div>
<script>
(function (window) {window.REGUA_SETTINGS=window.REGUA_SETTINGS || {};window.REGUA_SETTINGS.portalName = "home";window.REGUA_SETTINGS.portalHome = "http://www.globo.com/";window.REGUA_SETTINGS.portalsList = ["home","g1","ge","gshow","famosos","techtudo","globotv","globosatplay"];window.REGUA_SETTINGS.portalsLinkList = ["http://www.globo.com/","http://g1.globo.com/index.html","http://globoesporte.globo.com/","http://gshow.globo.com/","http://famosos.globo.com/","http://www.techtudo.com.br/","http://globotv.globo.com/","http://globosatplay.globo.com/"];window.REGUA_SETTINGS.staticUrl = "http://s.glbimg.com/en/ho/static/";window.REGUA_SETTINGS.svgSpriteName = "sprite-43e3e93f50.svg";window.REGUA_SETTINGS.suggestUrl = "";})(window);
</script><script>(function(){var e,t,n,o;e=function(){function e(){this.name="Android",this.bridge=window.GloboBridge||{navigated:function(e){},menuAction:function(e){},reguaAction:function(e){}}}return e.prototype.navigateTo=function(e){this.bridge.navigated(JSON.stringify(e))},e.prototype.menuAction=function(e){this.bridge.menuAction(JSON.stringify(e))},e.prototype.reguaAction=function(e){this.bridge.reguaAction(JSON.stringify(e))},e}(),t=function(){function e(){this.name="iOS",this.connectWebViewJavascriptBridge(function(e){return e.init(function(e,t){return window.glb.masterApp.manageMessage(e)})})}return e.prototype.connectWebViewJavascriptBridge=function(e){window.WebViewJavascriptBridge?e(WebViewJavascriptBridge):document.addEventListener("WebViewJavascriptBridgeReady",function(){e(WebViewJavascriptBridge)},!1)},e.prototype.brigdeSendJson=function(e){return this.connectWebViewJavascriptBridge(function(t){t.send(JSON.stringify(e))})},e.prototype.navigateTo=function(e){this.brigdeSendJson(e)},e.prototype.menuAction=function(e){this.brigdeSendJson(e)},e.prototype.reguaAction=function(e){this.brigdeSendJson(e)},e}(),n=function(){function n(e){this.platformApp=e,this.initCalled=!1,null!=this.platformApp&&this.bind()}return n.prototype.init=function(){this.initCalled||(this.initCalled=!0,this.header&&(this.processHeaderInfo(),this.bindAfterInit()))},n.prototype.processHeaderInfo=function(){this.countHeaderTries=0,this.maxHeaderTries=100,this.headerCubeContainer=this.header.querySelector(".cube-container"),this.headerCubeContainer?this.processHeaderInfoAction():setTimeout(function(e){return function(){return e.processHeaderInfoAction()}}(this),1)},n.prototype.processHeaderInfoAction=function(){var e;if(this.countHeaderTries++,this.headerCubeContainer=this.header.querySelector(".cube-container"),this.headerCubeContainer)this.cacheVariables(),e=this.getHeaderData(),this.navigateTo(e);else{if(this.countHeaderTries>this.maxHeaderTries)return void console.log("Nao achou o markup interno do header");setTimeout(function(e){return function(){return e.processHeaderInfoAction()}}(this),1)}},n.prototype.bind=function(){var e;return this.bindAppEvents(),this.header=document.querySelector("#header-produto"),e=function(e){return function(){return e.header||(e.header=document.querySelector("#header-produto")),e.init()}}(this),this.header?this.init():(document.addEventListener("glb.headerDom.ready",e),document.addEventListener("DOMContentLoaded",e))},n.prototype.cacheVariables=function(){return this.elements={},this.elements.header=this.header,this.elements.headerCubeContainer=this.headerCubeContainer,this.elements.headerFront=this.elements.headerCubeContainer?this.elements.headerCubeContainer.querySelector(".front"):null,this.elements.headerLogoProduto=this.elements.headerCubeContainer?this.elements.headerCubeContainer.querySelector(".logo-produto-container"):null,this.elements.headerLogoProdutoLink=this.elements.headerLogoProduto?this.elements.headerLogoProduto.parentNode:null,this.elements.headerLogoArea=this.elements.headerCubeContainer?this.elements.headerCubeContainer.querySelector(".logo-area .logo"):null,this.elements.headerLogoAreaLink=this.elements.headerLogoProduto?this.elements.headerLogoArea.parentNode:null,this.elements.headerSubeditoria=this.elements.headerFront?this.elements.headerFront.querySelector(".menu-subeditoria"):null,this.elements.headerSubeditoriaLink=this.elements.headerSubeditoria?this.elements.headerSubeditoria.querySelector("a"):null,!0},n.prototype.bindAppEvents=function(){document.addEventListener("glb.menu-carousel.ready",function(e){return function(){return e.platformApp.menuAction({action:"menuReady"})}}(this)),document.addEventListener("glb.menu-carousel.shown.before",function(e){return function(){return e.platformApp.menuAction({action:"menuShownBefore"})}}(this)),document.addEventListener("glb.menu-carousel.shown.after",function(e){return function(){return e.platformApp.menuAction({action:"menuShownAfter"})}}(this)),document.addEventListener("glb.menu-carousel.off.before",function(e){return function(){return e.platformApp.menuAction({action:"menuOffBefore"})}}(this)),document.addEventListener("glb.menu-carousel.off.after",function(e){return function(){return e.platformApp.menuAction({action:"menuOffAfter"})}}(this)),document.addEventListener("glb.regua.exists",function(e){return function(){return e.platformApp.reguaAction({action:"reguaExists"})}}(this))},n.prototype.bindAfterInit=function(){},n.prototype._getText=function(e){return e?(e.innerText||e.textContent).trim()||"":""},n.prototype._getHref=function(e){return e?e.getAttribute("href")||"":""},n.prototype._getCssProp=function(e,t){var n;return e?(n=window.getComputedStyle(e),n.getPropertyValue(t)||""):""},n.prototype._getBackgroundColor=function(e){return this._getCssProp(e,"background-color")},n.prototype.getHeaderData=function(){var e;return e={color:this._getBackgroundColor(this.elements.headerFront),editoria:"",editoriaHref:"",subeditoria:"",subeditoriaHref:"",logo:"",logoHref:""},this.elements.headerLogoProduto?(e.logo=this._getText(this.elements.headerLogoProduto),e.logoHref=this._getHref(this.elements.headerLogoProdutoLink),e.editoria=this._getText(this.elements.headerLogoArea),e.editoriaHref=this._getHref(this.elements.headerLogoArea),this.elements.headerSubeditoria&&(e.subeditoria=this._getText(this.elements.headerSubeditoria),e.subeditoriaHref=this._getHref(this.elements.headerSubeditoriaLink))):(e.logo=this._getText(this.elements.headerLogoArea),e.logoHref=this._getHref(this.elements.headerLogoAreaLink)),e},n.prototype.navigateTo=function(e){e.action="navigateTo",e.url=window.location.href,this.platformApp?this.platformApp.navigateTo(e):console.log("Sem Plataforma")},n.prototype.manageMessage=function(e){var t,n;return e&&(t=JSON.parse(e),t.action="regua-change")?(n=t.rulerItem||"home",null!=window.$?$(document).trigger("glb.regua.change",[n]):console.log("Sem jQuery")):void 0},n.prototype.getUserAgent=function(){return window.getUserAgent?window.getUserAgent():window.navigator.userAgent},n.prototype.getPlatformApp=function(){var n;return n=this.getUserAgent(),n.match(/(iPad|iPhone|iPod)/g)?new t:n.match(/(Android)/g)?new e:void 0},n}(),window.glb=window.glb||{},window.glb.MasterApp=n,window.glb.AndroidApp=e,window.glb.IosApp=t,window.initMasterApp=function(){var e;if(!window.glb.masterApp)return e=n.prototype.getPlatformApp(),window.glb.masterApp=new n(e)},o=null!=window.noAutoLoadReguaNavegacao&&window.noAutoLoadReguaNavegacao,o||window.initMasterApp()}).call(this);</script><script>(function(){var n,o,e,t;e=function(){return/MobApp_Android|MobApp_iOS|MobApp_WP/},o=function(){return window.getUserAgent?window.getUserAgent():window.navigator.userAgent},n=function(){var n,t,a,i,r;i=o(),r=i.split(" ");for(a in r)if(n=r[a],e().test(n))return t=n.split("/"),{name:t[0].replace("_"," "),version:t[1],frameworkVersion:t[2]};return null},window.addGlbOnAppClass=function(){var o;window.glb=window.glb||{},window.glb.extractAppFromUserAgent||(window.glb.extractAppFromUserAgent=n,window.glb.nativeAppInfo=n(),null!==window.glb.nativeAppInfo&&(window.glb.nativeAppInfo.frameworkVersion?(o=parseInt(window.glb.nativeAppInfo.frameworkVersion,10),o>=1&&(document.documentElement.className+=" glb-on-app")):document.documentElement.className+=" glb-on-app-comnio"))},t=null!=window.noAutoLoadReguaNavegacao&&window.noAutoLoadReguaNavegacao,t||window.addGlbOnAppClass()}).call(this);</script><script>(function(){var n,o,e,i,w,t,d,a,r,s,u,l;try{new CustomEvent("test")}catch(c){e=c,n=function(n,o){var e;return e=void 0,o=o||{bubbles:!1,cancelable:!1,detail:void 0},e=document.createEvent("CustomEvent"),e.initCustomEvent(n,o.bubbles,o.cancelable,o.detail),e},n.prototype=window.Event.prototype,window.CustomEvent=n}r=function(n,o){var e;return null==n||null==o?!1:(e=new RegExp("(?:^|\\s)"+o+"(?!\\S)"),!!n.className.match(e))},o=function(n,o){r(n,o)||(n.className+=" "+o)},u=function(n,o){var e;r(n,o)&&(e=new RegExp("(?:^|\\s)"+o+"(?!\\S)","g"),n.className=n.className.replace(e,""))},w=function(){return window.myInnerWidth||window.innerWidth},i=function(){return window.myInnerHeight||window.innerHeight},t=function(){var n;return null!=window.isAndroidBrowser?window.isAndroidBrowser:(n=navigator.userAgent,window.isAndroidBrowser=n.indexOf("Mozilla/5.0")>-1&&n.indexOf("Android ")>-1&&n.indexOf("AppleWebKit")>-1&&-1===n.indexOf("Chrome"),window.isAndroidBrowser)},d=function(){return null!=window.isPortrait?window.isPortrait:(window.isPortrait=w()<=i(),window.isPortrait)},a=function(){return null!=window.isTouchable?window.isTouchable:(window.isTouchable="ontouchstart"in window||navigator.msMaxTouchPoints||window.DocumentTouch&&document instanceof window.DocumentTouch,window.isTouchable)},s=function(){var n,o;return window.REGUAMAXWIDTHPORTRAIT=window.REGUAMAXWIDTHPORTRAIT||640,window.REGUAMAXWIDTHLANDSCAPE=window.REGUAMAXWIDTHLANDSCAPE||767,window.isPortrait=d(),window.isTouchable=a(),window.isAndroidBrowser=t(),n=w(),o=-1!==window.location.host.indexOf("g1.globo.com"),(window.isPortrait&&n<=window.REGUAMAXWIDTHPORTRAIT||!window.isPortrait&&n<=window.REGUAMAXWIDTHLANDSCAPE)&&!window.isAndroidBrowser&&document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image","1.1")&&(null==window.glb.nativeAppInfo||window.glb.nativeAppInfo.frameworkVersion)&&!o},window.glb=window.glb||{},window.glb.hasClass=r,window.glb.addClass=o,window.glb.removeClass=u,window.addHasReguaClass=function(){window.glb.reguaShouldStart||(window.glb.reguaShouldStart=s,window.glb.reguaShouldStart()?(window.glb.addClass(document.documentElement,"has-regua"),window.glb.addClass(document.documentElement,"svg-support"),window.glb.removeClass(document.documentElement,"has-not-regua"),document.dispatchEvent(new CustomEvent("glb.regua.exists"))):(window.glb.removeClass(document.documentElement,"has-regua"),window.glb.removeClass(document.documentElement,"svg-support"),window.glb.addClass(document.documentElement,"has-not-regua")))},l=null!=window.noAutoLoadReguaNavegacao&&window.noAutoLoadReguaNavegacao,l||window.addHasReguaClass()}).call(this);</script><nav id="regua-navegacao" class="regua-navegacao "><div id="regua-svg-container" class="regua-svg-container"><svg xmlns="http://www.w3.org/2000/svg"><symbol id="eixo-icone-busca" viewBox="0 0 24 24"><path d="M21.67 20.27l-5.72-5.73c.98-1.26 1.55-2.83 1.55-4.54 0-4.14-3.36-7.5-7.5-7.5-4.15 0-7.5 3.36-7.5 7.5 0 4.14 3.35 7.5 7.5 7.5 1.7 0 3.27-.57 4.53-1.54l5.73 5.73c.42.41 1.07.43 1.46.04.39-.39.37-1.04-.05-1.46zM10 15.5c-3.04 0-5.5-2.46-5.5-5.5S6.96 4.5 10 4.5c3.03 0 5.5 2.46 5.5 5.5s-2.47 5.5-5.5 5.5z"/></symbol><symbol id="eixo-icone-feed" viewBox="0 0 24 24"><path d="M17 14c0 .55-.45 1-1 1h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1zM19 10c0 .55-.45 1-1 1h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1z"/><path fill="#FFF" d="M17 14c0 .55-.45 1-1 1h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1zM19 10c0 .55-.45 1-1 1h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1z"/><path d="M19 10c0 .55-.45 1-1 1h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1zM17 14c0 .55-.45 1-1 1h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1z"/><path d="M4 3v4H0v11c0 1.66 1.34 3 3 3h19c1.1 0 2-.9 2-2V3H4zm0 15.09c0 .5-.45.91-1 .91s-1-.41-1-.91V9h2v9.09zM22 19H6V5h16v14z"/><path fill="#FFF" d="M4 9v9.09c0 .5-.45.91-1 .91s-1-.41-1-.91V9h2zM6 5v14h16V5H6zm10 10h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1s-.45 1-1 1zm2-4h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1s-.45 1-1 1z"/></symbol><symbol id="eixo-icone-menu" viewBox="0 0 24 24"><path d="M22 6c0 .552-.447 1-1 1H3c-.552 0-1-.448-1-1 0-.553.448-1 1-1h18c.553 0 1 .447 1 1zM22 12c0 .552-.447 1-1 1H3c-.552 0-1-.448-1-1 0-.553.448-1 1-1h18c.553 0 1 .447 1 1zM22 18c0 .552-.447 1-1 1H3c-.552 0-1-.448-1-1 0-.553.448-1 1-1h18c.553 0 1 .447 1 1z"/></symbol><symbol id="eixo-icone-usuario" viewBox="0 0 24 24"><path d="M12 2C8.4 2 5.5 4.9 5.5 8.5c0 3.61 2.9 6.5 6.5 6.5s6.5-2.89 6.5-6.5C18.5 4.9 15.6 2 12 2zm0 11.07c-2.49 0-4.5-2.02-4.5-4.5s2.01-4.5 4.5-4.5 4.5 2.02 4.5 4.5-2.01 4.5-4.5 4.5zM22.893 23c-.733 0-1.074-.455-1.074-.86C21.817 20.26 18.22 19 12 19s-9.818 1.136-9.818 3.14c0 .406-.41.86-1.092.86C.358 23 0 22.513 0 22.108 0 18.958 4.598 17 12 17s12 1.958 12 5.108c0 .405-.398.892-1.107.892z"/></symbol><symbol id="regua-arrow" viewBox="0 0 5 14"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.902 6.64L1.422.386C1.217.017.762-.11.402.104.05.317-.074.79.133 1.16l3.25 5.843L.1 12.847c-.208.368-.085.838.274 1.05.357.212.814.086 1.02-.28l3.412-6.074c.21-.243.26-.606.096-.904z"/></symbol></svg></div><div class="regua-navegacao-container"><ul class="regua-lista regua-lista-home"><li id="regua-navegacao-item-home" class="regua-navegacao-item active"><span data-href="http://www.globo.com/"
                                            data-target="regua-tab-home" class="home-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-feed"/></svg></span></li><li id="regua-navegacao-item-menu" class="regua-navegacao-item "><span data-target="regua-tab-menu" class="menu-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-menu"/></svg></span></li><li id="regua-navegacao-item-busca" class="regua-navegacao-item "><span data-target="regua-tab-busca" class="busca-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-busca"/></svg></span></li><li id="regua-navegacao-item-usuario" class="regua-navegacao-item "><span data-target="regua-tab-usuario" class="usuario-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-usuario"/></svg></span></li></ul></div></nav><nav id="regua-user-container" class="regua-user-container regua-user-home regua-navegacao-tab"><div id="regua-user-controls" class="regua-user-controls"></div><div id="regua-user-tabs" class="regua-tabs regua-user-tab-2"><div id="tab-labels" class="tab-labels"><span class="tab-label tab-label-1" data-index="1">notificaÃ§Ãµes</span><span class="tab-label tab-label-2" data-index="2">minha conta</span></div><div class="regua-tab regua-tab-1" data-index="1"><div class="regua-content"><ul id="regua-user-profile-notifications" class="regua-user-row regua-user-profile-notifications"></ul></div></div><div class="regua-tab regua-tab-2" data-index="2"><div class="regua-content"><ul id="regua-user-profile-list" class="regua-user-row regua-user-profile-list"></ul><div id="regua-user-logout" class="regua-user-logout"></div></div></div></div></nav><div id="regua-tab-busca" class="regua-navegacao-tab regua-tab-busca regua-busca-home"><div class="regua-search-header"><div class="regua-search-box"><form class="regua-search-form" action="http://globo.com/busca/" method="GET"><div class="regua-search-form-container regua-table"><div class="regua-search-input-box regua-table-cell"><input class="regua-search-input" name="q" type="search" placeholder="o que vocÃª procura?" autocorrect="off" autocapitalize="off" autocomplete="off"/></div><div class="regua-search-buttons-container regua-table-cell"><div class="regua-search-clear-container"><a href="#" class="regua-search-clear-button">clear</a></div><div class="search-button-icon"><input type="submit" value="go" class="regua-search-submit"/><div class="regua-icon-block"><svg class="regua-navegacao-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#eixo-icone-busca"></use></svg></div><a href="#" class="regua-icon-block search-button-go"><svg class="regua-navegacao-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#eixo-icone-busca"></use></svg></a></div></div></div></form></div></div><div class="regua-container-search-body"><ul class="regua-pre-suggest"></ul><div class="regua-container-suggest"><ul class="regua-suggest"></ul></div><div class="regua-container-results"></div></div></div>



<ul id="home-menu" class="nested widget-menu">
    
        <li class="g1 analytics-product analytics-multi-product">
            <a href="http://g1.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">g1</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://g1.globo.com/" data-menu-id="g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">g1</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="jornais-revistas-e-radio">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">jornais, revistas e rÃ¡dio</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="servicos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">serviÃ§os</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-g1">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-g1" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/economia/">
                                                <span class="titulo">Economia</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/carros/">
                                                <span class="titulo">Carros</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/">
                                                <span class="titulo">G1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/politica/">
                                                <span class="titulo">PolÃ­tica</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/mundo/">
                                                <span class="titulo">Mundo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/">
                                                <span class="titulo">Jornal Extra</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/concursos-e-emprego/">
                                                <span class="titulo">Concursos e Empregos</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/">
                                                <span class="titulo">Jornal O Globo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/jornal-hoje/">
                                                <span class="titulo">Jornal Hoje</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/bemestar/">
                                                <span class="titulo">Bem Estar no G1</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="g1">
                                        <div class="submenu-title">g1</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Carros</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/concursos-e-emprego/">
                                                    <span class="titulo">Concursos e Empregos</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/">
                                                    <span class="titulo">Economia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/educacao/">
                                                    <span class="titulo">EducaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/imposto-de-renda/2015/index.html">
                                                    <span class="titulo">Imposto de Renda 2015</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/mundo/">
                                                    <span class="titulo">Mundo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/planeta-bizarro/">
                                                    <span class="titulo">Planeta Bizarro</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/politica/">
                                                    <span class="titulo">PolÃ­tica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/">
                                                    <span class="titulo">Pop &amp; Arte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/tecnologia/">
                                                    <span class="titulo">Tecnologia &amp; Games</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bom-dia-brasil/">
                                                    <span class="titulo">Bom dia Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://fantastico.globo.com/">
                                                    <span class="titulo">FantÃ¡stico</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/agronegocios/">
                                                    <span class="titulo">Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/globo-news/">
                                                    <span class="titulo">GloboNews</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/hora1">
                                                    <span class="titulo">Hora 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-da-globo/">
                                                    <span class="titulo">Jornal da Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-hoje/">
                                                    <span class="titulo">Jornal Hoje</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-nacional/">
                                                    <span class="titulo">Jornal Nacional</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="jornais-revistas-e-radio">
                                        <div class="submenu-title">jornais, revistas e rÃ¡dio</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/">
                                                    <span class="titulo">Jornal Extra</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/">
                                                    <span class="titulo">Jornal O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://cbn.globoradio.globo.com/home/HOME.htm">
                                                    <span class="titulo">RÃ¡dio CBN</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaautoesporte.globo.com/">
                                                    <span class="titulo">Revista Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaepoca.globo.com/">
                                                    <span class="titulo">Revista Ãpoca</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagalileu.globo.com/">
                                                    <span class="titulo">Revista Galileu</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagloborural.globo.com/">
                                                    <span class="titulo">Revista Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radioglobo.globoradio.globo.com/">
                                                    <span class="titulo">RÃ¡dio Globo</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="servicos">
                                        <div class="submenu-title">serviÃ§os</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/agenda.html">
                                                    <span class="titulo">Agenda de Shows</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://especiais.g1.globo.com/educacao/app-g1-enem/">
                                                    <span class="titulo">Aplicativo G1 Enem</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/">
                                                    <span class="titulo">Conversor de Moedas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">
                                                    <span class="titulo">CotaÃ§Ãµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/">
                                                    <span class="titulo">Enem e Vestibular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/loteria/">
                                                    <span class="titulo">Loterias</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/previsao-do-tempo.html">
                                                    <span class="titulo">PrevisÃ£o do tempo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/tabela-fipe/index.html">
                                                    <span class="titulo">Tabela FIPE</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/telecurso/">
                                                    <span class="titulo">Telecurso</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.techtudo.com.br/velocimetro.html">
                                                    <span class="titulo">VelocÃ­metro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-g1"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="globoesporte analytics-product analytics-multi-product">
            <a href="http://globoesporte.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">globoesporte</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-globoesporte">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/" data-menu-id="globoesportecom">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globoesporte.com</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://sportv.globo.com/" data-menu-id="sportv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sportv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/futebol/" data-menu-id="futebol">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">futebol</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="mais-esportes">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mais esportes</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-globoesporte">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-globoesporte" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie B</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                <span class="titulo">Futebol Internacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                <span class="titulo">FÃ³rmula 1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/cartola-fc/">
                                                <span class="titulo">Cartola FC</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/copa-do-brasil/">
                                                <span class="titulo">Copa do Brasil</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/">
                                                <span class="titulo">Globo Esporte</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://sportv.globo.com/site/combate/">
                                                <span class="titulo">MMA</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/">
                                                <span class="titulo">Futebol Nacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">
                                                <span class="titulo">Liga dos CampeÃµes</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="globoesportecom">
                                        <div class="submenu-title">globoesporte.com</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://futpedia.globo.com/">
                                                    <span class="titulo">FutpÃ©dia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/cartola-fc/">
                                                    <span class="titulo">Cartola FC</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="sportv">
                                        <div class="submenu-title">sportv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/bolao/">
                                                    <span class="titulo">BolÃ£o</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/eventos/combate/">
                                                    <span class="titulo">Combate</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/corujao-do-esporte/">
                                                    <span class="titulo">CorujÃ£o do Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/esporte-espetacular/">
                                                    <span class="titulo">Esporte Espetacular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/">
                                                    <span class="titulo">Globo Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://app.globoesporte.globo.com/tv/planeta-extremo/">
                                                    <span class="titulo">Planeta Extremo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sociopremiere.globo.com/">
                                                    <span class="titulo">Premiere</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/">
                                                    <span class="titulo">SporTV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/lutas/tuf-brasil/">
                                                    <span class="titulo">The Ultimate Fighter Brasil</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="futebol">
                                        <div class="submenu-title">futebol</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie B</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-c/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie C</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-do-brasil/">
                                                    <span class="titulo">Copa do Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-sul-americana/">
                                                    <span class="titulo">Copa Sul-Americana</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                    <span class="titulo">Futebol Internacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol Nacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">
                                                    <span class="titulo">Liga dos CampeÃµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/selecao-brasileira/">
                                                    <span class="titulo">SeleÃ§Ã£o Brasileira</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="mais-esportes">
                                        <div class="submenu-title">mais esportes</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/atletismo/">
                                                    <span class="titulo">Atletismo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/basquete/">
                                                    <span class="titulo">Basquete</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eu-atleta/">
                                                    <span class="titulo">Eu Atleta</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                    <span class="titulo">FÃ³rmula 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eventos/futsal/">
                                                    <span class="titulo">Futsal</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/combate/">
                                                    <span class="titulo">MMA</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/natacao/">
                                                    <span class="titulo">NataÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/stock-car/">
                                                    <span class="titulo">StockCar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/tenis/">
                                                    <span class="titulo">TÃªnis</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/volei/">
                                                    <span class="titulo">VÃ´lei</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-globoesporte"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="gshow analytics-product analytics-multi-product">
            <a href="http://gshow.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">gshow</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/" data-menu-id="gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gshow</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="programas">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">programas</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="series">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sÃ©ries</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/programas/webseries/" data-menu-id="webseries">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">websÃ©ries</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com/" data-menu-id="rede-globo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">rede globo</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-gshow">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-gshow" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/realities/the-voice-brasil/">
                                                <span class="titulo">The Voice</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/mais-voce/">
                                                <span class="titulo">Mais VocÃª</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                <span class="titulo">Encontro com FÃ¡tima</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                <span class="titulo">VÃ­deo Show</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/">
                                                <span class="titulo">Gshow</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://redeglobo.globo.com/programacao.html">
                                                <span class="titulo">ProgramaÃ§Ã£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                <span class="titulo">CaldeirÃ£o do Huck</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/programa-do-jo/">
                                                <span class="titulo">Programa do JÃ´</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://redeglobo.globo.com/">
                                                <span class="titulo">Rede globo</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="gshow">
                                        <div class="submenu-title">gshow</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Bastidores/">
                                                    <span class="titulo">Bastidores</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/como-fazer/">
                                                    <span class="titulo">Como Fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo">
                                                    <span class="titulo">Estilo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/especial-blog/gshow-troll/1.html">
                                                    <span class="titulo">Gshow Troll</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/participe/">
                                                    <span class="titulo">Participe</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas-da-Ana-Maria/">
                                                    <span class="titulo">Receitas da Ana Maria</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/tv/plantao/">
                                                    <span class="titulo">TV</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="programas">
                                        <div class="submenu-title">programas</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://gshow.globo.com/programas/altas-horas/">
                                                    <span class="titulo">Altas Horas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                    <span class="titulo">CaldeirÃ£o do Huck</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                    <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/e-de-casa/">
                                                    <span class="titulo">Ã de Casa</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                    <span class="titulo">Encontro com FÃ¡tima</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/estrelas/index.html">
                                                    <span class="titulo">Estrelas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/">
                                                    <span class="titulo">Mais VocÃª</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/programa-do-jo/">
                                                    <span class="titulo">Programa do JÃ´</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/realities/the-voice-brasil/">
                                                    <span class="titulo">The Voice</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                    <span class="titulo">VÃ­deo Show</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="series">
                                        <div class="submenu-title">sÃ©ries</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/amorteamo/">
                                                    <span class="titulo">Amorteamo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/chapa-quente/">
                                                    <span class="titulo">Chapa Quente</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/ligacoes-perigosas/">
                                                    <span class="titulo">LigaÃ§Ãµes Perigosas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/mister-brau/2015/">
                                                    <span class="titulo">Mister Brau</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/pe-na-cova/2015/">
                                                    <span class="titulo">PÃ© na Cova</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/especial-blog/ta-no-ar-a-tv-na-tv/1.html">
                                                    <span class="titulo">TÃ¡ no ar: a TV na TV </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/zorra/">
                                                    <span class="titulo">Zorra</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="webseries">
                                        <div class="submenu-title">websÃ©ries</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/como-fazer/no-ar.html">
                                                    <span class="titulo">Como fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/dulce-delight/">
                                                    <span class="titulo">Dulce Delight</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/look-do-dia-com-tia-suelly/no-ar.html">
                                                    <span class="titulo">Look do Dia com Tia Suelly</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/marrom-combina-com-tudo/no-ar.html">
                                                    <span class="titulo">Marrom Combina com Tudo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/morri-na-tv/no-ar.html">
                                                    <span class="titulo">Morri na TV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/sebastiana-quebra-galho/no-ar.html">
                                                    <span class="titulo">Sebastiana Quebra-Galho</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/sete-vidas-casos-reais/">
                                                    <span class="titulo">Sete Vidas: casos reais</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/o-incrivel-superonix/no-ar.html">
                                                    <span class="titulo">SuperÃnix</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/tome-prumo/">
                                                    <span class="titulo">Tome Prumo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/vlog-da-valeska/no-ar.html">
                                                    <span class="titulo">Vlog da Valeska</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="rede-globo">
                                        <div class="submenu-title">rede globo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/programacao.html">
                                                    <span class="titulo">ProgramaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-gshow"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="famosos-etc analytics-product analytics-multi-product">
            <a href="http://famosos.globo.com" class="analytics-area analytics-id-T">
                <span class="titulo">famosos &amp; etc</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-famosos-etc">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://famosos.globo.com/" data-menu-id="famosos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">famosos</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="moda-estilo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">moda & estilo</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="bem-estar-saude">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">bem-estar & saÃºde</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="casa-decoracao">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">casa & decoraÃ§Ã£o</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="gastronomia">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gastronomia</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="musica-cinema-e-teatro">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mÃºsica, cinema e teatro</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="canais-de-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">canais de tv</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-famosos-etc">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-famosos-etc" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://ego.globo.com/">
                                                <span class="titulo">Ego</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://paparazzo.globo.com/">
                                                <span class="titulo">Paparazzo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://revistaquem.globo.com/">
                                                <span class="titulo">Quem</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/famosos/">
                                                <span class="titulo">Retratos da Vida</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/cultura/kogut/">
                                                <span class="titulo">Patricia Kogut</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://famosos.globo.com/">
                                                <span class="titulo">Famosos</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="famosos">
                                        <div class="submenu-title">famosos</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/">
                                                    <span class="titulo">Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://paparazzo.globo.com/">
                                                    <span class="titulo">Paparazzo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/cultura/kogut/">
                                                    <span class="titulo">Patricia Kogut</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaquem.globo.com/">
                                                    <span class="titulo">Quem</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/famosos/">
                                                    <span class="titulo">Retratos da Vida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="moda-estilo">
                                        <div class="submenu-title">moda &amp; estilo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/moda/index.html">
                                                    <span class="titulo">Moda no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ela.oglobo.globo.com/">
                                                    <span class="titulo">Ela no O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo/">
                                                    <span class="titulo">Estilo no Gshow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaglamour.globo.com/">
                                                    <span class="titulo">Revista Glamour</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gq.globo.com/">
                                                    <span class="titulo">Revista GQ Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistamarieclaire.globo.com/">
                                                    <span class="titulo">Revista Marie Claire </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/moda/">
                                                    <span class="titulo">Moda no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://vogue.globo.com/">
                                                    <span class="titulo">Revista Vogue</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="bem-estar-saude">
                                        <div class="submenu-title">bem-estar &amp; saÃºde</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/beleza/index.html">
                                                    <span class="titulo">Beleza no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/beleza/">
                                                    <span class="titulo">Beleza no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.com.br/bemestar">
                                                    <span class="titulo">Bem Estar no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar no G1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/maes-e-filhos/">
                                                    <span class="titulo">MÃ£es no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/">
                                                    <span class="titulo">Meus 5 Minutos </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://horoscopo.ego.globo.com/">
                                                    <span class="titulo">HorÃ³scopo no Ego</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="casa-decoracao">
                                        <div class="submenu-title">casa &amp; decoraÃ§Ã£o</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/">
                                                    <span class="titulo">Revista Casa e Jardim</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/casa-e-decoracao/">
                                                    <span class="titulo">Casa no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://casavogue.globo.com/">
                                                    <span class="titulo">Casa Vogue </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revista.zapimoveis.com.br/?utm_source=globo.com-etc&amp;utm_medium=botao-casa-decoracao&amp;utm_campaign=Revista-Home">
                                                    <span class="titulo">Revista do Zap</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="gastronomia">
                                        <div class="submenu-title">gastronomia</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/receitas/">
                                                    <span class="titulo">Receitas no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas/index.html">
                                                    <span class="titulo">Receitas no Mais VocÃª </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/blogs/Colheradas/">
                                                    <span class="titulo">Colheradas </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/Casa-e-Comida/">
                                                    <span class="titulo">Casa e Comida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="musica-cinema-e-teatro">
                                        <div class="submenu-title">mÃºsica, cinema e teatro</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/cinema/">
                                                    <span class="titulo">G1 Cinema</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/">
                                                    <span class="titulo">G1 MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">Gshow MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://musica.com.br/">
                                                    <span class="titulo">MÃºsica.com.br</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radiobeat.com.br/">
                                                    <span class="titulo">RADIOBEAT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.agentesevenoteatro.com.br/">
                                                    <span class="titulo">Teatro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="canais-de-tv">
                                        <div class="submenu-title">canais de tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/">
                                                    <span class="titulo">Rede Globo </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalbrasil.globo.com/">
                                                    <span class="titulo">Canal Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://mundogloob.globo.com/">
                                                    <span class="titulo">Gloob</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/">
                                                    <span class="titulo">GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://megapix.globo.com/">
                                                    <span class="titulo">Megapix</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://multishow.globo.com/">
                                                    <span class="titulo">Multishow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://telecine.globo.com/">
                                                    <span class="titulo">Telecine</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://uc.globo.com/">
                                                    <span class="titulo">Universal Channel</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalviva.globo.com/">
                                                    <span class="titulo">Viva</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://canaloff.globo.com">
                                                    <span class="titulo">Off</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-famosos-etc"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="tecnologia analytics-product analytics-multi-product">
            <a href="http://www.techtudo.com.br/" class="analytics-area analytics-id-T">
                <span class="titulo">tecnologia</span>

        
        
            </a>
        </li>
        
    
        <li class="videos analytics-product analytics-multi-product">
            <a href="http://globotv.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">vÃ­deos</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                            
                                <li class="has-link ">
                                    <a href="http://globotv.globo.com/" data-menu-id="globotv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globo.tv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link ">
                                    <a href="http://globosatplay.globo.com/" data-menu-id="globosat-play">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globosat play</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-videos">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
</ul>
<!-- gerado em: 2015-10-2221:07:13Z --><div id="assinante-menu"></div></nav></div><div class="header-bottom"></div></header><script>
libby.loadScript('http://s.glbimg.com/en/ho/static/etc/busca/js/jquery.buscaPadrao.v2.compressed.js', function(){var searchArea=document.getElementById('busca-padrao'),
placeholderValue = 'encontre na globo.com';if(typeof searchWidget !== "undefined" && searchWidget){searchWidget(searchArea, {suggestionsEnabled: true,
baseSearchUrl: 'http://www.globo.com',
addClass:'home-search',
placeholder: placeholderValue,
buscaHome: true,
qtd_min_letras_pro_suggest: 2,
qtd_min_letras_pra_busca: 2,
addStyleSheet: ''});}
searchArea.style.display = "block";if(searchArea && searchArea.querySelectorAll){var inputElement=searchArea.querySelectorAll('input')[0];inputElement.onwebkitspeechchange=inputElement.onspeechchange=function (ev) {inputElement.value=inputElement.value.replace(placeholderValue, '');inputElement.form.submit();};}});</script><div id="x60" class="opec-area opec-mobile opec-x60 grid-12"><div id="banner_mobile_topo" class="tag-manager-publicidade-container"></div></div><div id="urgente" class="container urgente analytics-area analytics-id-U "><div class="grid-base super-wide"></div></div><div id="bloco-principal" class="container first-scroll-container clearfix"><div class="grid-base wide analytics-area analytics-id-A"><div class="grid-base narrow pull-left primeira-area hide-mobile"><div class="destaque-principal-vertical-foto-topo destaque destaque-primeiro-scroll principal glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/operacao-lava-jato/noticia/2015/10/ministro-autoriza-sequestro-de-r-9-milhoes-atribuidos-cunha-na-suica.html" class=" " title="STF autoriza a &#39;volta&#39; de R$ 9,6 milhÃµes atribuÃ­dos a Cunha"><div class="conteudo"><h2>STF autoriza a &#39;volta&#39; de R$ 9,6 milhÃµes<br /> atribuÃ­dos a Cunha</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="AÃ§Ã£o de Cunha e PT evita convocaÃ§Ã£o de Baiano a CPI" href="http://oglobo.globo.com/brasil/acao-de-cunha-pt-evita-convocacao-de-fernando-baiano-17852110">AÃ§Ã£o de Cunha e PT evita convocaÃ§Ã£o de Baiano a CPI</a></div></li></ul></div><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/blog/matheus-leitao/post/passaporte-de-mulher-e-filha-de-cunha-foram-usados-para-abrir-conta-veja-fotos.html" class="foto " title="Conta suÃ­Ã§a tem passaporte de mulher e filha (ReproduÃ§Ã£o)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/bykfDUL8uULwSAKxDpC9sbtzqQU=/filters:quality(10):strip_icc()/s2.glbimg.com/wpG8mkIwG0yvOZFC_aYgajiMA8o=/11x560:331x705/155x70/s.glbimg.com/jo/g1/f/original/2015/10/22/passaportevers.jpg" alt="Conta suÃ­Ã§a tem passaporte de mulher e filha (ReproduÃ§Ã£o)" title="Conta suÃ­Ã§a tem passaporte de mulher e filha (ReproduÃ§Ã£o)"
         data-original-image="s2.glbimg.com/wpG8mkIwG0yvOZFC_aYgajiMA8o=/11x560:331x705/155x70/s.glbimg.com/jo/g1/f/original/2015/10/22/passaportevers.jpg" data-url-smart_horizontal="sG6UUU5nhTv6yKZQxYcZ9Ms3hxo=/90x56/smart/filters:strip_icc()/" data-url-smart="sG6UUU5nhTv6yKZQxYcZ9Ms3hxo=/90x56/smart/filters:strip_icc()/" data-url-feature="sG6UUU5nhTv6yKZQxYcZ9Ms3hxo=/90x56/smart/filters:strip_icc()/" data-url-tablet="FIh2QEZ0eVRatWYv1A_RW9VakjM=/160xorig/smart/filters:strip_icc()/" data-url-desktop="rUIqp3miQRjfRsGU6uGKK5ITtSU=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Conta suÃ­Ã§a tem passaporte de mulher e filha</h2></div></a></div></div><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-entretenimento analytics-area  analytics-id-M
        "><div class="mobile-grid-partial"><a href="http://gshow.globo.com/tv/noticia/2015/10/tecnicos-brigam-por-candidatos-na-penultima-audicao-do-voice.html" class="foto " title="&#39;TVB&#39;: candidato de hoje &#39;arrepia&#39; pele de Brown (Gshow)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/yBWW6kcKM1ApHqwiqRhMdHFCSvM=/filters:quality(10):strip_icc()/s2.glbimg.com/W1kAVM_9huvlJq9DlolUIWuQFpA=/92x23:300x117/155x70/s.glbimg.com/en/ho/f/original/2015/10/22/voice.jpg" alt="&#39;TVB&#39;: candidato de hoje &#39;arrepia&#39; pele de Brown (Gshow)" title="&#39;TVB&#39;: candidato de hoje &#39;arrepia&#39; pele de Brown (Gshow)"
         data-original-image="s2.glbimg.com/W1kAVM_9huvlJq9DlolUIWuQFpA=/92x23:300x117/155x70/s.glbimg.com/en/ho/f/original/2015/10/22/voice.jpg" data-url-smart_horizontal="VgkgqTiaTfvlW0U_3g1cRmwuDiI=/90x56/smart/filters:strip_icc()/" data-url-smart="VgkgqTiaTfvlW0U_3g1cRmwuDiI=/90x56/smart/filters:strip_icc()/" data-url-feature="VgkgqTiaTfvlW0U_3g1cRmwuDiI=/90x56/smart/filters:strip_icc()/" data-url-tablet="FD33cD3hie_tBdgOPOCHNR9pPA0=/160xorig/smart/filters:strip_icc()/" data-url-desktop="ikxd5qpv1hYCuiwAldZ9cF867nE=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>&#39;TVB&#39;: candidato de hoje &#39;arrepia&#39; pele de Brown</h2></div></a></div></div><div class="ultimo-destaque"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-entretenimento analytics-area  analytics-id-M
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/alem-do-tempo/vem-por-ai/noticia/2015/10/nova-fase-livia-e-melissa-sentem-antipatia-uma-pela-outra.html" class="foto " title="Em &#39;AlÃ©m&#39;, LÃ­via sente antipatia (TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/ZWaLWqwhdmYGnWbZtiZ4GXrgERE=/filters:quality(10):strip_icc()/s2.glbimg.com/KcmjrM5bh6IGkq81QFR8U-3r0F0=/0x93:690x361/155x60/s.glbimg.com/et/gs/f/original/2015/10/22/livia_1.jpg" alt="Em &#39;AlÃ©m&#39;, LÃ­via sente antipatia (TV Globo)" title="Em &#39;AlÃ©m&#39;, LÃ­via sente antipatia (TV Globo)"
         data-original-image="s2.glbimg.com/KcmjrM5bh6IGkq81QFR8U-3r0F0=/0x93:690x361/155x60/s.glbimg.com/et/gs/f/original/2015/10/22/livia_1.jpg" data-url-smart_horizontal="2643fZ3gAfEEuSFUmK_IhITcFzk=/90x56/smart/filters:strip_icc()/" data-url-smart="2643fZ3gAfEEuSFUmK_IhITcFzk=/90x56/smart/filters:strip_icc()/" data-url-feature="2643fZ3gAfEEuSFUmK_IhITcFzk=/90x56/smart/filters:strip_icc()/" data-url-tablet="tFBcS2f2N-jtUNRudCB_4jqihog=/160xorig/smart/filters:strip_icc()/" data-url-desktop="KgRbgEEJ9Xae9RowoKPB7aDTrCY=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Em &#39;AlÃ©m&#39;, LÃ­via sente antipatia</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Reveja final da 1Âª fase" href="http://gshow.globo.com/tv/noticia/2015/10/quer-rever-alem-do-tempo-confira-o-capitulo-aberto-e-completo-que-encerrou-1-fase.html">Reveja final da 1Âª fase</a></div></li><li><div class="mobile-grid-partial"><a title="Anjo atrapalharÃ¡ casal" href="http://gshow.globo.com/novelas/alem-do-tempo/vem-por-ai/noticia/2015/10/nova-fase-ariel-perde-fe-no-amor-e-atrapalha-aproximacao-de-livia-e-felipe.html">Anjo atrapalharÃ¡ casal</a></div></li></ul></div><div class="direita"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/vem-por-ai/noticia/2015/10/toia-faz-revelacao-dante-e-policial-pressiona-romero.html" class="foto " title="Toia revela fato e intriga Dante (Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/qAZRRpYSya2eCFtgHMib3j7y_OM=/filters:quality(10):strip_icc()/s2.glbimg.com/3XDZZ6zpfj1rpbaEdj9UG6deVcM=/0x70:640x317/155x60/s.glbimg.com/og/rg/f/original/2015/10/22/regra640.jpg" alt="Toia revela fato e intriga Dante (Globo)" title="Toia revela fato e intriga Dante (Globo)"
         data-original-image="s2.glbimg.com/3XDZZ6zpfj1rpbaEdj9UG6deVcM=/0x70:640x317/155x60/s.glbimg.com/og/rg/f/original/2015/10/22/regra640.jpg" data-url-smart_horizontal="ZJFpgvCOLZ4m2OWkrI9x2C9Sw6U=/90x56/smart/filters:strip_icc()/" data-url-smart="ZJFpgvCOLZ4m2OWkrI9x2C9Sw6U=/90x56/smart/filters:strip_icc()/" data-url-feature="ZJFpgvCOLZ4m2OWkrI9x2C9Sw6U=/90x56/smart/filters:strip_icc()/" data-url-tablet="UgaBMW5qcoY3oxrGaOfc7Mm7rEw=/160xorig/smart/filters:strip_icc()/" data-url-desktop="xXJfqnyFwwOv65PJsJwGuJoF1cU=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Toia revela fato e intriga Dante</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Preso, Juliano vÃª TÃ³ia " href="http://kogut.oglobo.globo.com/noticias-da-tv/novelas/noticia/2015/10/ze-maria-armou-para-juliano-ser-preso-vai-ser-bom-para-ele.html">Preso, Juliano vÃª TÃ³ia </a></div></li><li><div class="mobile-grid-partial"><a title="ZÃ© e Adisabeba transam" href="http://extra.globo.com/tv-e-lazer/telinha/a-regra-do-jogo-ze-maria-seduz-adisabeba-eles-transam-17838670.html">ZÃ© e Adisabeba transam</a></div></li></ul></div></div></div></div><div class="grid-base narrow ultimo hide-mobile"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/mensalao/noticia/2015/10/policia-italiana-entrega-pizzolato-pf.html" class="foto " title="ItÃ¡lia entrega,
 e Pizzolato jÃ¡ segue ao Brasil (Pedro VÃªdova/TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/Fj1UdR_nHlvzvhuFxPFy0GKJkwM=/filters:quality(10):strip_icc()/s2.glbimg.com/2YJRCOvcU9AJfSpGNS8YooUx1NI=/180x69:1163x830/155x120/s.glbimg.com/jo/g1/f/original/2015/10/22/pizzolato-aviao.jpg" alt="ItÃ¡lia entrega,
 e Pizzolato jÃ¡ segue ao Brasil (Pedro VÃªdova/TV Globo)" title="ItÃ¡lia entrega,
 e Pizzolato jÃ¡ segue ao Brasil (Pedro VÃªdova/TV Globo)"
         data-original-image="s2.glbimg.com/2YJRCOvcU9AJfSpGNS8YooUx1NI=/180x69:1163x830/155x120/s.glbimg.com/jo/g1/f/original/2015/10/22/pizzolato-aviao.jpg" data-url-smart_horizontal="b_Iok0rfnJVtiHYyXTejiT0OA1s=/90x56/smart/filters:strip_icc()/" data-url-smart="b_Iok0rfnJVtiHYyXTejiT0OA1s=/90x56/smart/filters:strip_icc()/" data-url-feature="b_Iok0rfnJVtiHYyXTejiT0OA1s=/90x56/smart/filters:strip_icc()/" data-url-tablet="qRdNB6P5bdaXTtoKL4BGVnNcD7I=/160xorig/smart/filters:strip_icc()/" data-url-desktop="lN0BOKubI9niCO49z6eY8ErRrC4=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>ItÃ¡lia entrega,<br /> e Pizzolato jÃ¡ segue ao Brasil</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://blogs.oglobo.globo.com/blog-do-moreno/post/o-estresse-de-levy.html" class="foto " title="Levy se irrita com flagra e xinga fotÃ³grafos (Ailton de Freitas/O Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/MHUdDDcx_Phxg9IGWDz8ml-atig=/filters:quality(10):strip_icc()/s2.glbimg.com/tyADO_XnUuvU4XnVsfn8yiTUZbU=/47x30:222x166/155x120/s.glbimg.com/en/ho/f/original/2015/10/22/levy.jpg" alt="Levy se irrita com flagra e xinga fotÃ³grafos (Ailton de Freitas/O Globo)" title="Levy se irrita com flagra e xinga fotÃ³grafos (Ailton de Freitas/O Globo)"
         data-original-image="s2.glbimg.com/tyADO_XnUuvU4XnVsfn8yiTUZbU=/47x30:222x166/155x120/s.glbimg.com/en/ho/f/original/2015/10/22/levy.jpg" data-url-smart_horizontal="LO1EnBM2i4ishQciLw4faQoID0Q=/90x56/smart/filters:strip_icc()/" data-url-smart="LO1EnBM2i4ishQciLw4faQoID0Q=/90x56/smart/filters:strip_icc()/" data-url-feature="LO1EnBM2i4ishQciLw4faQoID0Q=/90x56/smart/filters:strip_icc()/" data-url-tablet="ozwo955yF2QwYolqdE0E2woZv8g=/160xorig/smart/filters:strip_icc()/" data-url-desktop="_nALcYavrf0dHaZfhedxvhVtSRg=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Levy se irrita com flagra e xinga fotÃ³grafos</h2></div></a></div></div></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/noticia/2015/10/dilma-sanciona-lei-que-mantem-lotericas-que-nao-foram-licitadas.html" class=" " title="Dilma sanciona lei sobre lotÃ©ricas"><div class="conteudo"><h2>Dilma sanciona lei sobre lotÃ©ricas</h2></div></a></div></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/economia/noticia/2015/10/ccr-assume-projeto-para-construir-novo-aeroporto-em-sp.html" class=" " title="CCR assume projeto do 3Âº aeroporto de SP; veja onde serÃ¡ e os detalhes"><div class="conteudo"><h2>CCR assume projeto do 3Âº aeroporto de SP; veja onde serÃ¡ e os detalhes</h2></div></a></div></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/blogs/especial-blog/bastidores-fc/post/liga-sul-minas-rio-comemora-apoio-do-governo-federal.html" class=" " title="Governo sinaliza apoio para Liga Sul-Minas-Rio"><div class="conteudo"><h2>Governo sinaliza apoio para Liga Sul-Minas-Rio</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/times/vasco/noticia/2015/10/volta-para-casa-eurico-confirma-jogo-contra-o-corinthians-em-sao-januario.html" class=" " title="Vasco vai pegar o Corinthians em SÃ£o JanuÃ¡rio"><div class="conteudo"><h2>Vasco vai pegar o Corinthians em SÃ£o JanuÃ¡rio</h2></div></a></div></div></div><div class="ultimo-destaque"><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/selecao-brasileira/noticia/2015/10/dunga-convoca-setimo-goleiro-e-deixa-aberta-briga-pela-camisa-1-do-brasil.html" class=" " title="Dunga &#39;acirra&#39; disputa pela camisa 1"><div class="conteudo"><h2>Dunga &#39;acirra&#39; disputa pela camisa 1</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Careca vai prestar auxÃ­lio a tÃ©cnico em dois dos jogos" href="http://globoesporte.globo.com/futebol/selecao-brasileira/noticia/2015/10/ex-atacante-careca-vai-auxiliar-dunga-na-selecao-contra-argentina-e-peru.html">Careca vai prestar auxÃ­lio a tÃ©cnico em dois dos jogos</a></div></li><li><div class="mobile-grid-partial"><a title="Dunga prevÃª &#39;guerra&#39; contra a Argentina com Neymar" href="http://globoesporte.globo.com/futebol/selecao-brasileira/noticia/2015/10/dunga-preve-guerra-contra-argentina-e-brasil-vitaminado-com-neymar.html">Dunga prevÃª &#39;guerra&#39; contra a Argentina com Neymar</a></div></li></ul></div></div></div><div class="grid-base wide analytics-area analytics-id-A mobile-grid-base"><div class="destaque-principal-vertical-foto-topo destaque destaque-primeiro-scroll principal glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/operacao-lava-jato/noticia/2015/10/ministro-autoriza-sequestro-de-r-9-milhoes-atribuidos-cunha-na-suica.html" class=" " title="STF autoriza a &#39;volta&#39; de R$ 9,6 milhÃµes atribuÃ­dos a Cunha"><div class="conteudo"><h2>STF autoriza a &#39;volta&#39; de R$ 9,6 milhÃµes<br /> atribuÃ­dos a Cunha</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="AÃ§Ã£o de Cunha e PT evita convocaÃ§Ã£o de Baiano a CPI" href="http://oglobo.globo.com/brasil/acao-de-cunha-pt-evita-convocacao-de-fernando-baiano-17852110">AÃ§Ã£o de Cunha e PT evita convocaÃ§Ã£o de Baiano a CPI</a></div></li></ul></div><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/blog/matheus-leitao/post/passaporte-de-mulher-e-filha-de-cunha-foram-usados-para-abrir-conta-veja-fotos.html" class="foto " title="Conta suÃ­Ã§a tem passaporte de mulher e filha (ReproduÃ§Ã£o)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/bykfDUL8uULwSAKxDpC9sbtzqQU=/filters:quality(10):strip_icc()/s2.glbimg.com/wpG8mkIwG0yvOZFC_aYgajiMA8o=/11x560:331x705/155x70/s.glbimg.com/jo/g1/f/original/2015/10/22/passaportevers.jpg" alt="Conta suÃ­Ã§a tem passaporte de mulher e filha (ReproduÃ§Ã£o)" title="Conta suÃ­Ã§a tem passaporte de mulher e filha (ReproduÃ§Ã£o)"
         data-original-image="s2.glbimg.com/wpG8mkIwG0yvOZFC_aYgajiMA8o=/11x560:331x705/155x70/s.glbimg.com/jo/g1/f/original/2015/10/22/passaportevers.jpg" data-url-smart_horizontal="sG6UUU5nhTv6yKZQxYcZ9Ms3hxo=/90x56/smart/filters:strip_icc()/" data-url-smart="sG6UUU5nhTv6yKZQxYcZ9Ms3hxo=/90x56/smart/filters:strip_icc()/" data-url-feature="sG6UUU5nhTv6yKZQxYcZ9Ms3hxo=/90x56/smart/filters:strip_icc()/" data-url-tablet="FIh2QEZ0eVRatWYv1A_RW9VakjM=/160xorig/smart/filters:strip_icc()/" data-url-desktop="rUIqp3miQRjfRsGU6uGKK5ITtSU=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Conta suÃ­Ã§a tem passaporte de mulher e filha</h2></div></a></div></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/mensalao/noticia/2015/10/policia-italiana-entrega-pizzolato-pf.html" class="foto " title="ItÃ¡lia entrega,
 e Pizzolato jÃ¡ segue ao Brasil (Pedro VÃªdova/TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/Fj1UdR_nHlvzvhuFxPFy0GKJkwM=/filters:quality(10):strip_icc()/s2.glbimg.com/2YJRCOvcU9AJfSpGNS8YooUx1NI=/180x69:1163x830/155x120/s.glbimg.com/jo/g1/f/original/2015/10/22/pizzolato-aviao.jpg" alt="ItÃ¡lia entrega,
 e Pizzolato jÃ¡ segue ao Brasil (Pedro VÃªdova/TV Globo)" title="ItÃ¡lia entrega,
 e Pizzolato jÃ¡ segue ao Brasil (Pedro VÃªdova/TV Globo)"
         data-original-image="s2.glbimg.com/2YJRCOvcU9AJfSpGNS8YooUx1NI=/180x69:1163x830/155x120/s.glbimg.com/jo/g1/f/original/2015/10/22/pizzolato-aviao.jpg" data-url-smart_horizontal="b_Iok0rfnJVtiHYyXTejiT0OA1s=/90x56/smart/filters:strip_icc()/" data-url-smart="b_Iok0rfnJVtiHYyXTejiT0OA1s=/90x56/smart/filters:strip_icc()/" data-url-feature="b_Iok0rfnJVtiHYyXTejiT0OA1s=/90x56/smart/filters:strip_icc()/" data-url-tablet="qRdNB6P5bdaXTtoKL4BGVnNcD7I=/160xorig/smart/filters:strip_icc()/" data-url-desktop="lN0BOKubI9niCO49z6eY8ErRrC4=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>ItÃ¡lia entrega,<br /> e Pizzolato jÃ¡ segue ao Brasil</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://blogs.oglobo.globo.com/blog-do-moreno/post/o-estresse-de-levy.html" class="foto " title="Levy se irrita com flagra e xinga fotÃ³grafos (Ailton de Freitas/O Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/MHUdDDcx_Phxg9IGWDz8ml-atig=/filters:quality(10):strip_icc()/s2.glbimg.com/tyADO_XnUuvU4XnVsfn8yiTUZbU=/47x30:222x166/155x120/s.glbimg.com/en/ho/f/original/2015/10/22/levy.jpg" alt="Levy se irrita com flagra e xinga fotÃ³grafos (Ailton de Freitas/O Globo)" title="Levy se irrita com flagra e xinga fotÃ³grafos (Ailton de Freitas/O Globo)"
         data-original-image="s2.glbimg.com/tyADO_XnUuvU4XnVsfn8yiTUZbU=/47x30:222x166/155x120/s.glbimg.com/en/ho/f/original/2015/10/22/levy.jpg" data-url-smart_horizontal="LO1EnBM2i4ishQciLw4faQoID0Q=/90x56/smart/filters:strip_icc()/" data-url-smart="LO1EnBM2i4ishQciLw4faQoID0Q=/90x56/smart/filters:strip_icc()/" data-url-feature="LO1EnBM2i4ishQciLw4faQoID0Q=/90x56/smart/filters:strip_icc()/" data-url-tablet="ozwo955yF2QwYolqdE0E2woZv8g=/160xorig/smart/filters:strip_icc()/" data-url-desktop="_nALcYavrf0dHaZfhedxvhVtSRg=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Levy se irrita com flagra e xinga fotÃ³grafos</h2></div></a></div></div></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/politica/noticia/2015/10/dilma-sanciona-lei-que-mantem-lotericas-que-nao-foram-licitadas.html" class=" " title="Dilma sanciona lei sobre lotÃ©ricas"><div class="conteudo"><h2>Dilma sanciona lei sobre lotÃ©ricas</h2></div></a></div></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/economia/noticia/2015/10/ccr-assume-projeto-para-construir-novo-aeroporto-em-sp.html" class=" " title="CCR assume projeto do 3Âº aeroporto de SP; veja onde serÃ¡ e os detalhes"><div class="conteudo"><h2>CCR assume projeto do 3Âº aeroporto de SP; veja onde serÃ¡ e os detalhes</h2></div></a></div></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/blogs/especial-blog/bastidores-fc/post/liga-sul-minas-rio-comemora-apoio-do-governo-federal.html" class=" " title="Governo sinaliza apoio para Liga Sul-Minas-Rio"><div class="conteudo"><h2>Governo sinaliza apoio para Liga Sul-Minas-Rio</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/times/vasco/noticia/2015/10/volta-para-casa-eurico-confirma-jogo-contra-o-corinthians-em-sao-januario.html" class=" " title="Vasco vai pegar o Corinthians em SÃ£o JanuÃ¡rio"><div class="conteudo"><h2>Vasco vai pegar o Corinthians em SÃ£o JanuÃ¡rio</h2></div></a></div></div></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/selecao-brasileira/noticia/2015/10/dunga-convoca-setimo-goleiro-e-deixa-aberta-briga-pela-camisa-1-do-brasil.html" class=" " title="Dunga &#39;acirra&#39; disputa pela camisa 1"><div class="conteudo"><h2>Dunga &#39;acirra&#39; disputa pela camisa 1</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Careca vai prestar auxÃ­lio a tÃ©cnico em dois dos jogos" href="http://globoesporte.globo.com/futebol/selecao-brasileira/noticia/2015/10/ex-atacante-careca-vai-auxiliar-dunga-na-selecao-contra-argentina-e-peru.html">Careca vai prestar auxÃ­lio a tÃ©cnico em dois dos jogos</a></div></li><li><div class="mobile-grid-partial"><a title="Dunga prevÃª &#39;guerra&#39; contra a Argentina com Neymar" href="http://globoesporte.globo.com/futebol/selecao-brasileira/noticia/2015/10/dunga-preve-guerra-contra-argentina-e-brasil-vitaminado-com-neymar.html">Dunga prevÃª &#39;guerra&#39; contra a Argentina com Neymar</a></div></li></ul></div><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-entretenimento analytics-area  analytics-id-M
        "><div class="mobile-grid-partial"><a href="http://gshow.globo.com/tv/noticia/2015/10/tecnicos-brigam-por-candidatos-na-penultima-audicao-do-voice.html" class="foto " title="&#39;TVB&#39;: candidato de hoje &#39;arrepia&#39; pele de Brown (Gshow)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/yBWW6kcKM1ApHqwiqRhMdHFCSvM=/filters:quality(10):strip_icc()/s2.glbimg.com/W1kAVM_9huvlJq9DlolUIWuQFpA=/92x23:300x117/155x70/s.glbimg.com/en/ho/f/original/2015/10/22/voice.jpg" alt="&#39;TVB&#39;: candidato de hoje &#39;arrepia&#39; pele de Brown (Gshow)" title="&#39;TVB&#39;: candidato de hoje &#39;arrepia&#39; pele de Brown (Gshow)"
         data-original-image="s2.glbimg.com/W1kAVM_9huvlJq9DlolUIWuQFpA=/92x23:300x117/155x70/s.glbimg.com/en/ho/f/original/2015/10/22/voice.jpg" data-url-smart_horizontal="VgkgqTiaTfvlW0U_3g1cRmwuDiI=/90x56/smart/filters:strip_icc()/" data-url-smart="VgkgqTiaTfvlW0U_3g1cRmwuDiI=/90x56/smart/filters:strip_icc()/" data-url-feature="VgkgqTiaTfvlW0U_3g1cRmwuDiI=/90x56/smart/filters:strip_icc()/" data-url-tablet="FD33cD3hie_tBdgOPOCHNR9pPA0=/160xorig/smart/filters:strip_icc()/" data-url-desktop="ikxd5qpv1hYCuiwAldZ9cF867nE=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>&#39;TVB&#39;: candidato de hoje &#39;arrepia&#39; pele de Brown</h2></div></a></div></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-entretenimento analytics-area  analytics-id-M
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/alem-do-tempo/vem-por-ai/noticia/2015/10/nova-fase-livia-e-melissa-sentem-antipatia-uma-pela-outra.html" class="foto " title="Em &#39;AlÃ©m&#39;, LÃ­via sente antipatia (TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/ZWaLWqwhdmYGnWbZtiZ4GXrgERE=/filters:quality(10):strip_icc()/s2.glbimg.com/KcmjrM5bh6IGkq81QFR8U-3r0F0=/0x93:690x361/155x60/s.glbimg.com/et/gs/f/original/2015/10/22/livia_1.jpg" alt="Em &#39;AlÃ©m&#39;, LÃ­via sente antipatia (TV Globo)" title="Em &#39;AlÃ©m&#39;, LÃ­via sente antipatia (TV Globo)"
         data-original-image="s2.glbimg.com/KcmjrM5bh6IGkq81QFR8U-3r0F0=/0x93:690x361/155x60/s.glbimg.com/et/gs/f/original/2015/10/22/livia_1.jpg" data-url-smart_horizontal="2643fZ3gAfEEuSFUmK_IhITcFzk=/90x56/smart/filters:strip_icc()/" data-url-smart="2643fZ3gAfEEuSFUmK_IhITcFzk=/90x56/smart/filters:strip_icc()/" data-url-feature="2643fZ3gAfEEuSFUmK_IhITcFzk=/90x56/smart/filters:strip_icc()/" data-url-tablet="tFBcS2f2N-jtUNRudCB_4jqihog=/160xorig/smart/filters:strip_icc()/" data-url-desktop="KgRbgEEJ9Xae9RowoKPB7aDTrCY=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Em &#39;AlÃ©m&#39;, LÃ­via sente antipatia</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Reveja final da 1Âª fase" href="http://gshow.globo.com/tv/noticia/2015/10/quer-rever-alem-do-tempo-confira-o-capitulo-aberto-e-completo-que-encerrou-1-fase.html">Reveja final da 1Âª fase</a></div></li><li><div class="mobile-grid-partial"><a title="Anjo atrapalharÃ¡ casal" href="http://gshow.globo.com/novelas/alem-do-tempo/vem-por-ai/noticia/2015/10/nova-fase-ariel-perde-fe-no-amor-e-atrapalha-aproximacao-de-livia-e-felipe.html">Anjo atrapalharÃ¡ casal</a></div></li></ul></div><div class="direita"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/vem-por-ai/noticia/2015/10/toia-faz-revelacao-dante-e-policial-pressiona-romero.html" class="foto " title="Toia revela fato e intriga Dante (Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/qAZRRpYSya2eCFtgHMib3j7y_OM=/filters:quality(10):strip_icc()/s2.glbimg.com/3XDZZ6zpfj1rpbaEdj9UG6deVcM=/0x70:640x317/155x60/s.glbimg.com/og/rg/f/original/2015/10/22/regra640.jpg" alt="Toia revela fato e intriga Dante (Globo)" title="Toia revela fato e intriga Dante (Globo)"
         data-original-image="s2.glbimg.com/3XDZZ6zpfj1rpbaEdj9UG6deVcM=/0x70:640x317/155x60/s.glbimg.com/og/rg/f/original/2015/10/22/regra640.jpg" data-url-smart_horizontal="ZJFpgvCOLZ4m2OWkrI9x2C9Sw6U=/90x56/smart/filters:strip_icc()/" data-url-smart="ZJFpgvCOLZ4m2OWkrI9x2C9Sw6U=/90x56/smart/filters:strip_icc()/" data-url-feature="ZJFpgvCOLZ4m2OWkrI9x2C9Sw6U=/90x56/smart/filters:strip_icc()/" data-url-tablet="UgaBMW5qcoY3oxrGaOfc7Mm7rEw=/160xorig/smart/filters:strip_icc()/" data-url-desktop="xXJfqnyFwwOv65PJsJwGuJoF1cU=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Toia revela fato e intriga Dante</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Preso, Juliano vÃª TÃ³ia " href="http://kogut.oglobo.globo.com/noticias-da-tv/novelas/noticia/2015/10/ze-maria-armou-para-juliano-ser-preso-vai-ser-bom-para-ele.html">Preso, Juliano vÃª TÃ³ia </a></div></li><li><div class="mobile-grid-partial"><a title="ZÃ© e Adisabeba transam" href="http://extra.globo.com/tv-e-lazer/telinha/a-regra-do-jogo-ze-maria-seduz-adisabeba-eles-transam-17838670.html">ZÃ© e Adisabeba transam</a></div></li></ul></div></div></div><div class="grid-base wide pull-left wide-inferior"></div></div><div id="area-widgets-direita-topo" class="grid-base narrow area-widgets-direta-topo analytics-area analytics-id-V"><div id="ad-position-top1" class="opec"><div id="banner_slim_topo" class="tag-manager-publicidade-container"></div></div><div class="libby-agrupador-destaque-globotv-com-canais length-5"><div class="main-area mobile-grid-full"><div class="header-area analytics-area analytics-id-header"><a class="logo-link" href="http://globotv.globo.com"><div class="logo"></div></a><h3><a class="title-link" href="http://globotv.globo.com/rede-globo/video-show/">VÃ­deo show</a></h3></div><div class="hover-area"><div class="image-area"><ul><li class="active analytics-area analytics-id-1 analytics-id-I glb-hl-style-entretenimento" data-href="http://globotv.globo.com/rede-globo/video-show/v/diretor-explica-como-foi-gravada-ultima-sequencia-de-alem-do-tempo/4556523/"><a href="http://globotv.globo.com/rede-globo/video-show/v/diretor-explica-como-foi-gravada-ultima-sequencia-de-alem-do-tempo/4556523/"><div class="image-container"><div class="image-wrapper"><img src="http://s2.glbimg.com/NmL--OWlYzk8swFxfDqLQB1l8FM=/0x19:690x380/335x175/s.glbimg.com/et/gs/f/original/2015/10/20/pedro-felipe-livia.jpg" alt="Diretor explica como foi gravada Ãºltima sequÃªncia da 1Âª fase" /><div class="time">03:54</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">cenas marcantes de &#39;alÃ©m&#39;</span><span class="title">Diretor explica como foi gravada Ãºltima sequÃªncia da 1Âª fase</span></div></a></li><li class=" analytics-area analytics-id-2 analytics-id-I glb-hl-style-entretenimento" data-href="http://globotv.globo.com/rede-globo/video-show/v/irene-ravache-comenta-as-emocoes-da-virada-de-alem-do-tempo/4556553/"><a href="http://globotv.globo.com/rede-globo/video-show/v/irene-ravache-comenta-as-emocoes-da-virada-de-alem-do-tempo/4556553/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4556553.jpg" alt="Ravache conta que foi atrÃ¡s de Paolla apÃ³s cena: &#39;Deslumbrante&#39;" /><div class="time">03:44</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">de tirar o chapÃ©u</span><span class="title">Ravache conta que foi atrÃ¡s de Paolla apÃ³s cena: &#39;Deslumbrante&#39;</span></div></a></li><li class=" analytics-area analytics-id-3 analytics-id-I glb-hl-style-entretenimento" data-href="http://globotv.globo.com/rede-globo/video-show/v/boninho-invade-o-estudio-e-entrega-camisa-a-otaviano-costa/4556684/"><a href="http://globotv.globo.com/rede-globo/video-show/v/boninho-invade-o-estudio-e-entrega-camisa-a-otaviano-costa/4556684/"><div class="image-container"><div class="image-wrapper"><img src="http://s2.glbimg.com/zZTMy5zjk-7uGMtenf6NiBRn-3s=/0x0:335x175/335x175/s.glbimg.com/en/ho/f/original/2015/10/22/otaviano_leva_susto.jpg" alt="Otaviano leva susto com Boninho e Monica Ã© criticada por Dennis" /><div class="time">04:01</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">papo com diretor</span><span class="title">Otaviano leva susto com Boninho e Monica Ã© criticada por Dennis</span></div></a></li></ul></div><div class="title-area"><ul><li class="active analytics-area analytics-id-1 analytics-id-T glb-hl-style-entretenimento"><a href="http://globotv.globo.com/rede-globo/video-show/v/diretor-explica-como-foi-gravada-ultima-sequencia-de-alem-do-tempo/4556523/"><span class="subtitle">cenas marcantes de &#39;alÃ©m&#39;</span><span class="title">Diretor explica como foi gravada Ãºltima sequÃªncia da 1Âª fase</span></a></li><li class=" analytics-area analytics-id-2 analytics-id-T glb-hl-style-entretenimento"><a href="http://globotv.globo.com/rede-globo/video-show/v/irene-ravache-comenta-as-emocoes-da-virada-de-alem-do-tempo/4556553/"><span class="subtitle">de tirar o chapÃ©u</span><span class="title">Ravache conta que foi atrÃ¡s de Paolla apÃ³s cena: &#39;Deslumbrante&#39;</span></a></li><li class=" analytics-area analytics-id-3 analytics-id-T glb-hl-style-entretenimento"><a href="http://globotv.globo.com/rede-globo/video-show/v/boninho-invade-o-estudio-e-entrega-camisa-a-otaviano-costa/4556684/"><span class="subtitle">papo com diretor</span><span class="title">Otaviano leva susto com Boninho e Monica Ã© criticada por Dennis</span></a></li></ul></div></div><div class="controls-area"><ul class="step-marker glb-hl-style-entretenimento"><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-1"><a href="#" data-related="http://globotv.globo.com/rede-globo/video-show/v/diretor-explica-como-foi-gravada-ultima-sequencia-de-alem-do-tempo/4556523/"><div class="ball active"></div></a></div></li><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-2"><a href="#" data-related="http://globotv.globo.com/rede-globo/video-show/v/irene-ravache-comenta-as-emocoes-da-virada-de-alem-do-tempo/4556553/"><div class="ball "></div></a></div></li><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-3"><a href="#" data-related="http://globotv.globo.com/rede-globo/video-show/v/boninho-invade-o-estudio-e-entrega-camisa-a-otaviano-costa/4556684/"><div class="ball "></div></a></div></li></ul><ul class="navigation-buttons"><li><a href="#left"><div class="left"></div></a></li><li><a href="#right"><div class="right"></div></a></li></ul></div><div class="menu-area"><ul><li><a href="http://globotv.globo.com/rede-globo/"><div class="channels rede-globo"></div></a></li><li><a href="http://globotv.globo.com/sportv/"><div class="channels sportv"></div></a></li><li><a href="http://globotv.globo.com/globo-news/"><div class="channels globo-news"></div></a></li><li><a href="http://globotv.globo.com/multishow/"><div class="channels multishow"></div></a></li><li><a href="http://globotv.globo.com/gnt/"><div class="channels gnt"></div></a></li></ul></div><div class="footer-area analytics-area analytics-id-footer"><a class="title-link" href="http://globotv.globo.com/">mais vÃ­deos <span class="arrow"> âº</span></a></div></div></div><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded-agrupador-destaque-globotv-com-canais']);</script></div></div><div class="separator-first-scroll-container"><section><div><div id="opec-banner-middle-container" class="opec-banner-middle-container"><div id="ad-position-middle" class="opec"><div id="banner_slb_meio" class="tag-manager-publicidade-container"></div></div></div></div></section></div><div id="x62" class="opec-area opec-mobile opec-x62 grid-12"><div id="banner_mobile_meio" class="tag-manager-publicidade-container"></div></div><div id="container-columns" class="container columns clearfix glb-area-colunas"><section class="area news-column   last-has-photo
            
        
        "><div class="destaques-container closed analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/jornal-nacional/noticia/2015/10/policiais-do-rio-encontram-homem-prestes-ser-torturado-por-milicianos.html" class="foto" title="PolÃ­cia Civil salva homem prestes a ser torturado por milicianos no RJ; vÃ­deo (JN)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/izCDVuogE0oWNkAobf2q6hBOS8k=/filters:quality(10):strip_icc()/s2.glbimg.com/bxcXstwAugIZtWzjXSzoLA71IQ0=/0x11:426x240/335x180/s.glbimg.com/en/ho/f/original/2015/10/22/milicia.jpg" alt="PolÃ­cia Civil salva homem prestes a ser torturado por milicianos no RJ; vÃ­deo (JN)" title="PolÃ­cia Civil salva homem prestes a ser torturado por milicianos no RJ; vÃ­deo (JN)"
                data-original-image="s2.glbimg.com/bxcXstwAugIZtWzjXSzoLA71IQ0=/0x11:426x240/335x180/s.glbimg.com/en/ho/f/original/2015/10/22/milicia.jpg" data-url-smart_horizontal="04XxZ5PD6qAB5BKHW0RRubKE3Ns=/90x0/smart/filters:strip_icc()/" data-url-smart="04XxZ5PD6qAB5BKHW0RRubKE3Ns=/90x0/smart/filters:strip_icc()/" data-url-feature="04XxZ5PD6qAB5BKHW0RRubKE3Ns=/90x0/smart/filters:strip_icc()/" data-url-tablet="lGMwcbt5RAgZHRd4oTwHPrIc0Iw=/220x125/smart/filters:strip_icc()/" data-url-desktop="WSMvrQPrj9xccWBkoS6So7b9rtY=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>PolÃ­cia Civil salva homem prestes a ser torturado por milicianos no RJ; vÃ­deo</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-22 17:33:06" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/sp/santos-regiao/noticia/2015/10/acusados-de-matar-jovem-por-causa-de-r-7-sao-condenados-em-guaruja.html" class="foto" title="Pai e filho acusados de matar jovem por causa de R$ 7 sÃ£o condenados (Solange Freitas / G1)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/KIwruguE2_sLoLYcLqgnQ3crMYs=/filters:quality(10):strip_icc()/s2.glbimg.com/2yjxYdE8KeC6fxR-f0PzN0SCSgw=/111x48:572x356/120x80/s.glbimg.com/jo/g1/f/original/2015/10/22/foto_7.jpg" alt="Pai e filho acusados de matar jovem por causa de R$ 7 sÃ£o condenados (Solange Freitas / G1)" title="Pai e filho acusados de matar jovem por causa de R$ 7 sÃ£o condenados (Solange Freitas / G1)"
                data-original-image="s2.glbimg.com/2yjxYdE8KeC6fxR-f0PzN0SCSgw=/111x48:572x356/120x80/s.glbimg.com/jo/g1/f/original/2015/10/22/foto_7.jpg" data-url-smart_horizontal="N45-dfYjNW-ZnNZROBfHmpn5h-8=/90x0/smart/filters:strip_icc()/" data-url-smart="N45-dfYjNW-ZnNZROBfHmpn5h-8=/90x0/smart/filters:strip_icc()/" data-url-feature="N45-dfYjNW-ZnNZROBfHmpn5h-8=/90x0/smart/filters:strip_icc()/" data-url-tablet="ssftDn6lyixhEcUazyf8dGVhpnk=/70x50/smart/filters:strip_icc()/" data-url-desktop="sBCpkFjn3qbzhcqxJX7QIwpjrX8=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Pai e filho acusados de matar jovem por causa<br /> de R$ 7 sÃ£o condenados</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/sao-paulo/noticia/2015/10/justica-aceita-pedido-e-suzane-richthofen-ira-regime-semiaberto.html" class="foto" title="JustiÃ§a autoriza, e Suzane von Richthofen vai para o regime semiaberto (ReproduÃ§Ã£o/TV Globo)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/eg2gidwvFWFaMXQH7zbXHo4erSI=/filters:quality(10):strip_icc()/s2.glbimg.com/cQo0-6WI159zOI-ssPVpcKct558=/61x0:241x120/120x80/s.glbimg.com/jo/g1/f/original/2015/02/26/suzane.jpg" alt="JustiÃ§a autoriza, e Suzane von Richthofen vai para o regime semiaberto (ReproduÃ§Ã£o/TV Globo)" title="JustiÃ§a autoriza, e Suzane von Richthofen vai para o regime semiaberto (ReproduÃ§Ã£o/TV Globo)"
                data-original-image="s2.glbimg.com/cQo0-6WI159zOI-ssPVpcKct558=/61x0:241x120/120x80/s.glbimg.com/jo/g1/f/original/2015/02/26/suzane.jpg" data-url-smart_horizontal="Bm0yb4RtDROIGjQeiMVYoZvMcVM=/90x0/smart/filters:strip_icc()/" data-url-smart="Bm0yb4RtDROIGjQeiMVYoZvMcVM=/90x0/smart/filters:strip_icc()/" data-url-feature="Bm0yb4RtDROIGjQeiMVYoZvMcVM=/90x0/smart/filters:strip_icc()/" data-url-tablet="OsHqqLYDmNOQ3OmrfzqN22-RF5U=/70x50/smart/filters:strip_icc()/" data-url-desktop="K4G1fJtjS3dXg2kX5zWczFFwYvI=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>JustiÃ§a autoriza, e Suzane von Richthofen vai para<br /> o regime semiaberto</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-22 20:58:04" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/sao-paulo/sorocaba-jundiai/noticia/2015/10/policia-civil-indicia-grupo-pela-contratacao-de-falsos-medicos.html" class="" title="PolÃ­cia Civil indicia grupo pela contrataÃ§Ã£o de falsos mÃ©dicos no interior de SP (ReproduÃ§Ã£o TV TEM)" rel="bookmark"><span class="conteudo"><p>PolÃ­cia Civil indicia grupo pela contrataÃ§Ã£o de falsos mÃ©dicos no interior de SP</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-22 21:06:54" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/sp/bauru-marilia/noticia/2015/10/policia-investiga-maus-tratos-apos-menino-levar-correcao-do-pai.html" class="foto" title="Professora vÃª marcas de agressÃ£o em garoto de 7 anos e pai Ã© suspeito (DivulgaÃ§Ã£o / Guarda Municipal de Botucatu)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/_OYfY9zTMeDWA8T8HdFCfp_6gIs=/filters:quality(10):strip_icc()/s2.glbimg.com/azpmRLDjYnxhrLWYzLdWXYMHDLo=/0x179:300x379/120x80/s.glbimg.com/jo/g1/f/original/2015/10/22/gcm_e_conselho_tutelar_registram_caso_de_maus_tratos_a_crianca_de_7_anos_4.jpg" alt="Professora vÃª marcas de agressÃ£o em garoto de 7 anos e pai Ã© suspeito (DivulgaÃ§Ã£o / Guarda Municipal de Botucatu)" title="Professora vÃª marcas de agressÃ£o em garoto de 7 anos e pai Ã© suspeito (DivulgaÃ§Ã£o / Guarda Municipal de Botucatu)"
                data-original-image="s2.glbimg.com/azpmRLDjYnxhrLWYzLdWXYMHDLo=/0x179:300x379/120x80/s.glbimg.com/jo/g1/f/original/2015/10/22/gcm_e_conselho_tutelar_registram_caso_de_maus_tratos_a_crianca_de_7_anos_4.jpg" data-url-smart_horizontal="q5JQ94QAjgwf2XZ2Y6B3mOv0isM=/90x0/smart/filters:strip_icc()/" data-url-smart="q5JQ94QAjgwf2XZ2Y6B3mOv0isM=/90x0/smart/filters:strip_icc()/" data-url-feature="q5JQ94QAjgwf2XZ2Y6B3mOv0isM=/90x0/smart/filters:strip_icc()/" data-url-tablet="5leK9muAIro3kKZGm0r7Y2jPiqk=/70x50/smart/filters:strip_icc()/" data-url-desktop="hbd6nj_7UfxiNYdfyAq0wEf_siY=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Professora vÃª marcas de agressÃ£o em garoto de 7 anos e pai Ã© suspeito</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-22 18:29:33" class="chamada chamada-principal mobile-grid-full"><a href="http://www.techtudo.com.br/noticias/noticia/2015/10/iphone-6s-tera-lancamento-no-brasil-com-preco-de-r-3999-diz-site.html" class="foto" title="Novo iPhone deve chegar ao paÃ­s com preÃ§o &#39;surreal&#39;;
conheÃ§a os valores (TechTudo)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/2ACrwP4smlZ1u5Fy8aQ3v-O9LtQ=/filters:quality(10):strip_icc()/s2.glbimg.com/qa70TqLi_P_j5l6auhlF0epBhZ0=/60x0:554x330/120x80/s.glbimg.com/po/tt2/f/original/2015/10/16/agvypp-_gx0bl6yxwhp1qnn3caq9siv037hnmtstctyj_copy.jpg" alt="Novo iPhone deve chegar ao paÃ­s com preÃ§o &#39;surreal&#39;;
conheÃ§a os valores (TechTudo)" title="Novo iPhone deve chegar ao paÃ­s com preÃ§o &#39;surreal&#39;;
conheÃ§a os valores (TechTudo)"
                data-original-image="s2.glbimg.com/qa70TqLi_P_j5l6auhlF0epBhZ0=/60x0:554x330/120x80/s.glbimg.com/po/tt2/f/original/2015/10/16/agvypp-_gx0bl6yxwhp1qnn3caq9siv037hnmtstctyj_copy.jpg" data-url-smart_horizontal="tBj_purozXfYgfO6qULfDhmGIW8=/90x0/smart/filters:strip_icc()/" data-url-smart="tBj_purozXfYgfO6qULfDhmGIW8=/90x0/smart/filters:strip_icc()/" data-url-feature="tBj_purozXfYgfO6qULfDhmGIW8=/90x0/smart/filters:strip_icc()/" data-url-tablet="Qng4CcywcMQG3ztwdW1gFcLt7qw=/70x50/smart/filters:strip_icc()/" data-url-desktop="ibcYGq-sQKfzbfMiHwdMuO3TPdg=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Novo iPhone deve chegar <br />ao paÃ­s com preÃ§o &#39;surreal&#39;;<br />conheÃ§a os valores</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-22 16:17:52" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/noticia/2015/10/mae-e-suspeita-de-enviar-para-rede-de-pedofilia-fotos-sensuais-da-filha-e-neta.html" class="" title="MÃ£e Ã© suspeita de enviar fotos sensuais da filha e da neta para rede de pedofilia em SP" rel="bookmark"><span class="conteudo"><p>MÃ£e Ã© suspeita de enviar fotos sensuais da filha e da neta para rede de pedofilia em SP</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/pr/parana/noticia/2015/10/video-que-mostra-parede-caindo-em-cima-de-pedreiro-viraliza-na-internet.html" class="foto" title="Pedreiro fica em estado grave apÃ³s parede cair sobre ele no PR; veja vÃ­deo (ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/tfVFLPGsPOMin4aBeuOFiEWLErg=/filters:quality(10):strip_icc()/s2.glbimg.com/pZXS5RnnRY_Jtr7mnsMI6WFR8HY=/33x0:303x180/120x80/s.glbimg.com/en/ho/f/original/2015/10/22/muro.jpg" alt="Pedreiro fica em estado grave apÃ³s parede cair sobre ele no PR; veja vÃ­deo (ReproduÃ§Ã£o)" title="Pedreiro fica em estado grave apÃ³s parede cair sobre ele no PR; veja vÃ­deo (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/pZXS5RnnRY_Jtr7mnsMI6WFR8HY=/33x0:303x180/120x80/s.glbimg.com/en/ho/f/original/2015/10/22/muro.jpg" data-url-smart_horizontal="9-F5QC8fZNQk_vH-P1rx27n1thI=/90x0/smart/filters:strip_icc()/" data-url-smart="9-F5QC8fZNQk_vH-P1rx27n1thI=/90x0/smart/filters:strip_icc()/" data-url-feature="9-F5QC8fZNQk_vH-P1rx27n1thI=/90x0/smart/filters:strip_icc()/" data-url-tablet="JGMrPH0bpDW2_AjWR41d_7APpp0=/70x50/smart/filters:strip_icc()/" data-url-desktop="V-u8XbBNpUrrWBW8Nq43tw5jKX4=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Pedreiro fica em estado grave apÃ³s parede cair sobre ele no PR; veja vÃ­deo</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-22 20:05:44" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/rj/regiao-dos-lagos/noticia/2015/10/incendio-mata-tres-jacares-em-parque-estadual-so-tinha-um-unico-refugio.html" class="foto" title="IncÃªndio em Arraial do Cabo, RJ, mata jacarÃ©s, corujas e cÃ¡gados; fotos (DivulgaÃ§Ã£o/ Anderson Santos)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/52ADe26uMqr_3K02BQ_FXtW5XBU=/filters:quality(10):strip_icc()/s2.glbimg.com/piOhYu1golXvtrWlmO3a97Ayibs=/0x108:742x604/120x80/s.glbimg.com/jo/g1/f/original/2015/10/22/captura_de_tela_2015-10-22_as_18.52.08.png" alt="IncÃªndio em Arraial do Cabo, RJ, mata jacarÃ©s, corujas e cÃ¡gados; fotos (DivulgaÃ§Ã£o/ Anderson Santos)" title="IncÃªndio em Arraial do Cabo, RJ, mata jacarÃ©s, corujas e cÃ¡gados; fotos (DivulgaÃ§Ã£o/ Anderson Santos)"
                data-original-image="s2.glbimg.com/piOhYu1golXvtrWlmO3a97Ayibs=/0x108:742x604/120x80/s.glbimg.com/jo/g1/f/original/2015/10/22/captura_de_tela_2015-10-22_as_18.52.08.png" data-url-smart_horizontal="gqQudzrS2AkV3ipQskF_rkRZ2cA=/90x0/smart/filters:strip_icc()/" data-url-smart="gqQudzrS2AkV3ipQskF_rkRZ2cA=/90x0/smart/filters:strip_icc()/" data-url-feature="gqQudzrS2AkV3ipQskF_rkRZ2cA=/90x0/smart/filters:strip_icc()/" data-url-tablet="b1KhE8ugNq8IT7toIov4d-jE5lg=/70x50/smart/filters:strip_icc()/" data-url-desktop="qBYy6Jsns18ucGNdIK0V5sspkwI=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>IncÃªndio em Arraial do Cabo, RJ, mata jacarÃ©s, corujas e cÃ¡gados; fotos</p></span></a></div></div><div class="show-more"><span class="setinha-show-more"></span></div></div><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-noticias
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}});}
</script><div id="libby-home-widget-economia" class="libby-home-widgetBase libby-home-widget-economia analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais notÃ­cias" href="http://g1.globo.com/"><span class="text">mais notÃ­cias</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area sports-column   last-has-photo
            
        
        "><div class="destaques-container closed analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/blogs/especial-blog/brasil-mundial-fc/post/torcedores-negros-sao-impedidos-de-sair-de-estadio-em-jogo-da-champions-em-kiev.html" class="foto" title="Torcedores negros sÃ£o impedidos de sair de estÃ¡dio em jogo da Champions (Reuters)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/1BbdTtlJdfAIdBFSvOfwKBESgU8=/filters:quality(10):strip_icc()/s2.glbimg.com/5okcaykFAdFsSJXKvYRLq5vqWBc=/174x121:546x321/335x180/s.glbimg.com/es/ge/f/original/2015/10/22/2015-10-22t132356z_1006920001_lynxnpeb9l0tx_rtroptp_3_esportes-fut-ucrania-kiev-racismo.jpg" alt="Torcedores negros sÃ£o impedidos de sair de estÃ¡dio em jogo da Champions (Reuters)" title="Torcedores negros sÃ£o impedidos de sair de estÃ¡dio em jogo da Champions (Reuters)"
                data-original-image="s2.glbimg.com/5okcaykFAdFsSJXKvYRLq5vqWBc=/174x121:546x321/335x180/s.glbimg.com/es/ge/f/original/2015/10/22/2015-10-22t132356z_1006920001_lynxnpeb9l0tx_rtroptp_3_esportes-fut-ucrania-kiev-racismo.jpg" data-url-smart_horizontal="ZjpmIVl25JREY3gBwLsYv3ywZYA=/90x0/smart/filters:strip_icc()/" data-url-smart="ZjpmIVl25JREY3gBwLsYv3ywZYA=/90x0/smart/filters:strip_icc()/" data-url-feature="ZjpmIVl25JREY3gBwLsYv3ywZYA=/90x0/smart/filters:strip_icc()/" data-url-tablet="dpTd7eCfKKwOskkCIH6ly7MnDKA=/220x125/smart/filters:strip_icc()/" data-url-desktop="WVSv3pMc5nSRVqa9iJKIc3UGckE=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Torcedores negros sÃ£o impedidos de sair de estÃ¡dio em jogo da Champions</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-22 19:47:00" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/corinthians/noticia/2015/10/presidente-do-corinthians-prega-paz-com-pato-e-cogita-volta-dele-em-2016.html" class="foto" title="Presidente do Corinthians prega paz com Pato e cogita volta dele em 2016 (reproduÃ§Ã£o Globoesporte.com)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/6SC6QlaET2b2anV_85JUvotXsnA=/filters:quality(10):strip_icc()/s2.glbimg.com/V3tCPvzolsem1cPzb-hMbjrQOzI=/94x17:223x103/120x80/s.glbimg.com/og/rg/f/original/2015/10/20/sp_x_stos_606.jpg" alt="Presidente do Corinthians prega paz com Pato e cogita volta dele em 2016 (reproduÃ§Ã£o Globoesporte.com)" title="Presidente do Corinthians prega paz com Pato e cogita volta dele em 2016 (reproduÃ§Ã£o Globoesporte.com)"
                data-original-image="s2.glbimg.com/V3tCPvzolsem1cPzb-hMbjrQOzI=/94x17:223x103/120x80/s.glbimg.com/og/rg/f/original/2015/10/20/sp_x_stos_606.jpg" data-url-smart_horizontal="fttVyjPwtnyeO3lUwPxzEoaZdxw=/90x0/smart/filters:strip_icc()/" data-url-smart="fttVyjPwtnyeO3lUwPxzEoaZdxw=/90x0/smart/filters:strip_icc()/" data-url-feature="fttVyjPwtnyeO3lUwPxzEoaZdxw=/90x0/smart/filters:strip_icc()/" data-url-tablet="vN8hxUtYa05uuCE0d_sD_UWaj1s=/70x50/smart/filters:strip_icc()/" data-url-desktop="sKWiKw5sjCltBLAF-gb7Jurls3Y=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Presidente do Corinthians prega paz com Pato e cogita volta dele em 2016</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/flamengo/noticia/2015/10/stjd-mantem-advertencia-e-wallace-nao-vai-desfalcar-o-flamengo.html" class="foto" title="Fla no Tribunal: STJD suspende Emerson Sheik e o diretor Rodrigo Caetano (Jana Aguiar)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/cu7jvG8keAzJVwx8mRr21CMN1gA=/filters:quality(10):strip_icc()/s2.glbimg.com/kU7zJtftK5zXako2-llK_gVbm7c=/666x171:1301x595/120x80/s.glbimg.com/es/ge/f/original/2015/10/11/12091202_10153047215537273_2669312453306235495_o.jpg" alt="Fla no Tribunal: STJD suspende Emerson Sheik e o diretor Rodrigo Caetano (Jana Aguiar)" title="Fla no Tribunal: STJD suspende Emerson Sheik e o diretor Rodrigo Caetano (Jana Aguiar)"
                data-original-image="s2.glbimg.com/kU7zJtftK5zXako2-llK_gVbm7c=/666x171:1301x595/120x80/s.glbimg.com/es/ge/f/original/2015/10/11/12091202_10153047215537273_2669312453306235495_o.jpg" data-url-smart_horizontal="YJIee_2Ln1NsMHYW5uQI38u8ufU=/90x0/smart/filters:strip_icc()/" data-url-smart="YJIee_2Ln1NsMHYW5uQI38u8ufU=/90x0/smart/filters:strip_icc()/" data-url-feature="YJIee_2Ln1NsMHYW5uQI38u8ufU=/90x0/smart/filters:strip_icc()/" data-url-tablet="vHyZbUj3Ww-o2vwsk3wqlZ3Zr0o=/70x50/smart/filters:strip_icc()/" data-url-desktop="JxjZHrbvui5588YjjG8_kbN_VWk=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Fla no Tribunal: STJD suspende Emerson Sheik e o diretor Rodrigo Caetano</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-22 20:47:44" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/noticia/2015/10/filho-da-p-m-ladrao-do-c-veja-10-xingamentos-relatados-em-sumulas.html" class="" title="Veja uma lista dos xingamentos jÃ¡ feitos por jogadores e dirigentes a Ã¡rbitros" rel="bookmark"><span class="conteudo"><p>Veja uma lista dos xingamentos jÃ¡ feitos por jogadores e dirigentes a Ã¡rbitros</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-22 20:51:43" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/blogs/especial-blog/brasil-mundial-fc/post/neymar-jantarzinho-com-os-brod.html" class="foto" title="Neymar sai para jantar com Gil Cebola e os &#39;parÃ§as&#39; em Barcelona; amplie (ReproduÃ§Ã£o / Instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/qSeoiqkTJ8tIokZdocWz9HIOr_8=/filters:quality(10):strip_icc()/s2.glbimg.com/bTtIzKqQ3C9kM8AFCMMz82F2XBc=/0x209:633x632/120x80/s.glbimg.com/es/ge/f/original/2015/10/22/neymar-jantar.png" alt="Neymar sai para jantar com Gil Cebola e os &#39;parÃ§as&#39; em Barcelona; amplie (ReproduÃ§Ã£o / Instagram)" title="Neymar sai para jantar com Gil Cebola e os &#39;parÃ§as&#39; em Barcelona; amplie (ReproduÃ§Ã£o / Instagram)"
                data-original-image="s2.glbimg.com/bTtIzKqQ3C9kM8AFCMMz82F2XBc=/0x209:633x632/120x80/s.glbimg.com/es/ge/f/original/2015/10/22/neymar-jantar.png" data-url-smart_horizontal="sl0KHsxXMnTpSBBTl7w2mE7-eOE=/90x0/smart/filters:strip_icc()/" data-url-smart="sl0KHsxXMnTpSBBTl7w2mE7-eOE=/90x0/smart/filters:strip_icc()/" data-url-feature="sl0KHsxXMnTpSBBTl7w2mE7-eOE=/90x0/smart/filters:strip_icc()/" data-url-tablet="Svf-9pjcKVS6Lx_VIyF-AqKCeAo=/70x50/smart/filters:strip_icc()/" data-url-desktop="tfcmeHM_Crv5Gf9v168Vlel1NQs=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Neymar sai para jantar com Gil Cebola e os &#39;parÃ§as&#39; em Barcelona; amplie</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/blogs/especial-blog/brasil-mundial-fc/post/boneco-nazista-de-schweinsteiger-gera-polemica-e-pode-parar-na-justica-alema.html" class="foto" title="Schweinsteiger se revolta com &#39;Bastian&#39; nazi, mas China alega coincidÃªncia (ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/-Uz5VicmaF3pDRHzQFQiwM-9CtI=/filters:quality(10):strip_icc()/s2.glbimg.com/7yupu79TaIyutBeO3TOjWjf-eHg=/0x18:336x242/120x80/s.glbimg.com/es/ge/f/original/2015/10/22/as_2.jpg" alt="Schweinsteiger se revolta com &#39;Bastian&#39; nazi, mas China alega coincidÃªncia (ReproduÃ§Ã£o)" title="Schweinsteiger se revolta com &#39;Bastian&#39; nazi, mas China alega coincidÃªncia (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/7yupu79TaIyutBeO3TOjWjf-eHg=/0x18:336x242/120x80/s.glbimg.com/es/ge/f/original/2015/10/22/as_2.jpg" data-url-smart_horizontal="HjfrM8GxXiMwRsaudGZZ_RWSDFs=/90x0/smart/filters:strip_icc()/" data-url-smart="HjfrM8GxXiMwRsaudGZZ_RWSDFs=/90x0/smart/filters:strip_icc()/" data-url-feature="HjfrM8GxXiMwRsaudGZZ_RWSDFs=/90x0/smart/filters:strip_icc()/" data-url-tablet="aYnB3Juk0oLT0-DRW9xo7Bm6BPA=/70x50/smart/filters:strip_icc()/" data-url-desktop="zhkO_ec-pENcpLEYZKlrjvdXVbw=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Schweinsteiger se revolta com &#39;Bastian&#39; nazi, mas China alega coincidÃªncia</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-22 20:58:04" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/santos/noticia/2015/10/exames-podem-fazer-dorival-poupar-titulares-do-santos-pela-primeira-vez.html" class="" title="Exames podem fazer Dorival poupar titulares do Santos pela 1Âª vez desde sua volta (ReproduÃ§Ã£o)" rel="bookmark"><span class="conteudo"><p>Exames podem fazer Dorival poupar titulares do Santos pela 1Âª vez desde sua volta</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-22 20:15:27" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/motor/formula-1/noticia/2015/10/jenson-button-espero-que-alonso-me-aniquile-durante-todo-fim-de-semana.html" class="foto" title="Jenson Button: &#39;Espero que Alonso me aniquile em todo o fim de semana&#39; (AFP)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/q28jASPlE5igqeWSHfz6Jmq_SgQ=/filters:quality(10):strip_icc()/s2.glbimg.com/vtXuLRvX98i9-aAoS4XGUblo8lI=/797x355:1913x1099/120x80/s.glbimg.com/es/ge/f/original/2015/06/20/020_477574652_1.jpg" alt="Jenson Button: &#39;Espero que Alonso me aniquile em todo o fim de semana&#39; (AFP)" title="Jenson Button: &#39;Espero que Alonso me aniquile em todo o fim de semana&#39; (AFP)"
                data-original-image="s2.glbimg.com/vtXuLRvX98i9-aAoS4XGUblo8lI=/797x355:1913x1099/120x80/s.glbimg.com/es/ge/f/original/2015/06/20/020_477574652_1.jpg" data-url-smart_horizontal="lmkadnLSxZmbvzX5kvd8a7Ph2sU=/90x0/smart/filters:strip_icc()/" data-url-smart="lmkadnLSxZmbvzX5kvd8a7Ph2sU=/90x0/smart/filters:strip_icc()/" data-url-feature="lmkadnLSxZmbvzX5kvd8a7Ph2sU=/90x0/smart/filters:strip_icc()/" data-url-tablet="5B2-Mh-xvo_7E_FDB4KhIKu6Ws4=/70x50/smart/filters:strip_icc()/" data-url-desktop="kco2guzPyBS0dw27-3CUUsyy8eE=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Jenson Button: &#39;Espero que Alonso me aniquile em todo o fim de semana&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-22 11:39:21" class="chamada chamada-principal mobile-grid-full"><a href="http://canalbrasil.globo.com/programas/ayrton-retratos-e-memorias/materias/assessora-de-ayrton-fala-sobre-relacionamento-com-adriane-galisteu-e-xuxa.html" class="foto" title="Assessora fala da relaÃ§Ã£o
de Senna com Galisteu e Xuxa em sÃ©rie; assista (ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/sVsytVX6YWftynX1z0m-Tu3VWhs=/filters:quality(10):strip_icc()/s2.glbimg.com/3f0NjGG-RMzwlBXOIWg3BOyPWz0=/0x0:300x200/120x80/s.glbimg.com/en/ho/f/original/2015/10/22/senna_galisteu300x200_1.jpg" alt="Assessora fala da relaÃ§Ã£o
de Senna com Galisteu e Xuxa em sÃ©rie; assista (ReproduÃ§Ã£o)" title="Assessora fala da relaÃ§Ã£o
de Senna com Galisteu e Xuxa em sÃ©rie; assista (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/3f0NjGG-RMzwlBXOIWg3BOyPWz0=/0x0:300x200/120x80/s.glbimg.com/en/ho/f/original/2015/10/22/senna_galisteu300x200_1.jpg" data-url-smart_horizontal="J3FgvcW83pw6n26iOauU9MyEnyc=/90x0/smart/filters:strip_icc()/" data-url-smart="J3FgvcW83pw6n26iOauU9MyEnyc=/90x0/smart/filters:strip_icc()/" data-url-feature="J3FgvcW83pw6n26iOauU9MyEnyc=/90x0/smart/filters:strip_icc()/" data-url-tablet="SwEbtNIoNpItQOsOZjSJq3FYgmU=/70x50/smart/filters:strip_icc()/" data-url-desktop="I6JmItZDj7u2B5bJpCz_S_9Yneo=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Assessora fala da relaÃ§Ã£o<br />de Senna com Galisteu e Xuxa em sÃ©rie; assista</p></span></a></div></div><div class="show-more"><span class="setinha-show-more"></span></div></div><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-times soccer-teams
"></div><div id="libby-home-widget-brasileirao" class="libby-home-widgetBase libby-home-widget-brasileirao analytics-area analytics-id-W"><input type="hidden" name="rodada_selecionada" value="current"></div><a class="link-see-more analytics-area analytics-id-L" title="mais esportes" href="http://globoesporte.globo.com/"><span class="text">mais esportes</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area etc-column analytics-area   last-has-photo
            
        
        "><div class="destaques-container closed analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://revistacrescer.globo.com/Pais-famosos/noticia/2015/10/carolinie-figueiredo-aqui-em-casa-educamos-sem-sexismo.html" class="foto" title="Carolinie Figueiredo posta foto do filho brincando de bonecas e rebate crÃ­ticas (ReproduÃ§Ã£o - Instagram)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/67b9MA3aNZy6_2oQB4SN03rhnUM=/filters:quality(10):strip_icc()/s2.glbimg.com/qwXhtJJKBMWlrRczxZWiogp7-lA=/0x52:594x371/335x180/e.glbimg.com/og/ed/f/original/2015/10/22/theo-boneca.jpg" alt="Carolinie Figueiredo posta foto do filho brincando de bonecas e rebate crÃ­ticas (ReproduÃ§Ã£o - Instagram)" title="Carolinie Figueiredo posta foto do filho brincando de bonecas e rebate crÃ­ticas (ReproduÃ§Ã£o - Instagram)"
                data-original-image="s2.glbimg.com/qwXhtJJKBMWlrRczxZWiogp7-lA=/0x52:594x371/335x180/e.glbimg.com/og/ed/f/original/2015/10/22/theo-boneca.jpg" data-url-smart_horizontal="Ruga0IPLrUS6MRFugU0KlNO--Us=/90x0/smart/filters:strip_icc()/" data-url-smart="Ruga0IPLrUS6MRFugU0KlNO--Us=/90x0/smart/filters:strip_icc()/" data-url-feature="Ruga0IPLrUS6MRFugU0KlNO--Us=/90x0/smart/filters:strip_icc()/" data-url-tablet="h6rF6P7xczzi5gwB7sZHWTqlHxc=/220x125/smart/filters:strip_icc()/" data-url-desktop="EDbAWJBUAO9NKlZ7g4H5cipGoLs=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Carolinie Figueiredo posta foto do filho brincando de bonecas e rebate crÃ­ticas</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-22 19:19:16" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/famosos/antonia-fontenelle-aparece-ao-lado-do-filho-samuel-em-registro-raro-sexy-de-tao-timido-que-ele-e-17850389.html" class="foto" title="Em registro raro, Antonia Fontenelle mostra o filho de 18 anos com ator morto (Instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/ZDszkO3ve9zOpd4SWP8ExzX8hmk=/filters:quality(10):strip_icc()/s2.glbimg.com/Ru8gXHI5HGkwo-PzhzQsXFqdtWM=/0x51:448x350/120x80/s.glbimg.com/en/ho/f/original/2015/10/22/fontenelle.jpg" alt="Em registro raro, Antonia Fontenelle mostra o filho de 18 anos com ator morto (Instagram)" title="Em registro raro, Antonia Fontenelle mostra o filho de 18 anos com ator morto (Instagram)"
                data-original-image="s2.glbimg.com/Ru8gXHI5HGkwo-PzhzQsXFqdtWM=/0x51:448x350/120x80/s.glbimg.com/en/ho/f/original/2015/10/22/fontenelle.jpg" data-url-smart_horizontal="WNi6sXDe5s7hZXLSWvJNcffhDFM=/90x0/smart/filters:strip_icc()/" data-url-smart="WNi6sXDe5s7hZXLSWvJNcffhDFM=/90x0/smart/filters:strip_icc()/" data-url-feature="WNi6sXDe5s7hZXLSWvJNcffhDFM=/90x0/smart/filters:strip_icc()/" data-url-tablet="Ac1m_TGzEBwfVX4w6ytT5DBX1bw=/70x50/smart/filters:strip_icc()/" data-url-desktop="ayLTbF_WIKWYavj_fQrs7K__ZzQ=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Em registro raro, Antonia Fontenelle mostra o filho de 18 anos com ator morto</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-22 14:59:46" class="chamada chamada-principal mobile-grid-full"><a href="http://gshow.globo.com/Bastidores/noticia/2015/10/sheila-mello-afirma-que-fernando-scherer-nao-e-ciumento-quando-saio-pergunto-se-estou-gostosa.html" class="foto" title="Sheila Mello posta fotos em Noronha e diz que se arrepende de plÃ¡stica (Neuronha)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/Q5cSRDycMZkaFr5O6DN5r4TjFvM=/filters:quality(10):strip_icc()/s2.glbimg.com/VYKHP3-A24JFrcoixaOxojMQBDs=/106x114:528x395/120x80/s.glbimg.com/et/gs/f/original/2015/10/20/sheila4.jpg" alt="Sheila Mello posta fotos em Noronha e diz que se arrepende de plÃ¡stica (Neuronha)" title="Sheila Mello posta fotos em Noronha e diz que se arrepende de plÃ¡stica (Neuronha)"
                data-original-image="s2.glbimg.com/VYKHP3-A24JFrcoixaOxojMQBDs=/106x114:528x395/120x80/s.glbimg.com/et/gs/f/original/2015/10/20/sheila4.jpg" data-url-smart_horizontal="rDMt5byF330Hw3XiV87bxlp0JTE=/90x0/smart/filters:strip_icc()/" data-url-smart="rDMt5byF330Hw3XiV87bxlp0JTE=/90x0/smart/filters:strip_icc()/" data-url-feature="rDMt5byF330Hw3XiV87bxlp0JTE=/90x0/smart/filters:strip_icc()/" data-url-tablet="t4pXNOw_78R8YZ7_faxl9BnR914=/70x50/smart/filters:strip_icc()/" data-url-desktop="6ofvxE8KtueJmZkqU00hnN6f3nw=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Sheila Mello posta fotos em Noronha e diz que se arrepende de plÃ¡stica</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-22 16:45:12" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/famosos/luiza-brunet-recebe-critica-por-mostrar-foto-de-charutos-rebate-compartilho-minha-intimidade-nao-precisa-voltar-17848880.html" class="" title="ApÃ³s post com charutos e crÃ­ticas, Luiza Brunet fala a seguidora: &#39;NÃ£o precisa voltar&#39;" rel="bookmark"><span class="conteudo"><p>ApÃ³s post com charutos e crÃ­ticas, Luiza Brunet fala a seguidora: &#39;NÃ£o precisa voltar&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-22 19:34:45" class="chamada chamada-principal mobile-grid-full"><a href="http://revistaquem.globo.com/QUEM-News/noticia/2015/10/sabrina-sato-conta-sobre-queda-de-escada-nos-bastidores-da-spfw.html" class="foto" title="Sabrina Sato mostra a Yasmin Brunet marca que ganhou ao cair de escada (QUEM)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/-sJgw7Ejcme3Fir6IXVuq8g6azE=/filters:quality(10):strip_icc()/s2.glbimg.com/UjaaqI5VN6MRfEbpHwHpliXSPrg=/94x0:596x335/120x80/e.glbimg.com/og/ed/f/original/2015/10/22/sabrina-machucada.jpg" alt="Sabrina Sato mostra a Yasmin Brunet marca que ganhou ao cair de escada (QUEM)" title="Sabrina Sato mostra a Yasmin Brunet marca que ganhou ao cair de escada (QUEM)"
                data-original-image="s2.glbimg.com/UjaaqI5VN6MRfEbpHwHpliXSPrg=/94x0:596x335/120x80/e.glbimg.com/og/ed/f/original/2015/10/22/sabrina-machucada.jpg" data-url-smart_horizontal="09tQF3r5GSrpkTKqGCho0tPvmzQ=/90x0/smart/filters:strip_icc()/" data-url-smart="09tQF3r5GSrpkTKqGCho0tPvmzQ=/90x0/smart/filters:strip_icc()/" data-url-feature="09tQF3r5GSrpkTKqGCho0tPvmzQ=/90x0/smart/filters:strip_icc()/" data-url-tablet="nWK0iEeRZt-F878Kes87yPtqaxo=/70x50/smart/filters:strip_icc()/" data-url-desktop="bGhr5o-pi1Hwzdi1NT8sp-oiYVA=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Sabrina Sato mostra a Yasmin Brunet marca que ganhou ao cair de escada</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="small" class="chamada chamada-principal mobile-grid-full"><a href="http://gshow.globo.com/Bastidores/noticia/2015/10/sereias-bordo-bailarinas-do-domingao-exibem-formas-perfeitas-em-ensaio-de-biquini.html" class="foto" title="Bailarinas do &#39;FaustÃ£o&#39; abalam em ensaio de biquÃ­ni em barco; fotos (Levi Cruz/DivulgaÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/bWLnW8f9PMy6YyMln2fS0uB1MNU=/filters:quality(10):strip_icc()/s2.glbimg.com/NscunFHHeYzQj0zSaKiXecu8NfE=/0x5:719x484/120x80/s.glbimg.com/et/gs/f/original/2015/10/15/b2.jpg" alt="Bailarinas do &#39;FaustÃ£o&#39; abalam em ensaio de biquÃ­ni em barco; fotos (Levi Cruz/DivulgaÃ§Ã£o)" title="Bailarinas do &#39;FaustÃ£o&#39; abalam em ensaio de biquÃ­ni em barco; fotos (Levi Cruz/DivulgaÃ§Ã£o)"
                data-original-image="s2.glbimg.com/NscunFHHeYzQj0zSaKiXecu8NfE=/0x5:719x484/120x80/s.glbimg.com/et/gs/f/original/2015/10/15/b2.jpg" data-url-smart_horizontal="03xomrDV6Zx5VeGkDqv4SmLSqLc=/90x0/smart/filters:strip_icc()/" data-url-smart="03xomrDV6Zx5VeGkDqv4SmLSqLc=/90x0/smart/filters:strip_icc()/" data-url-feature="03xomrDV6Zx5VeGkDqv4SmLSqLc=/90x0/smart/filters:strip_icc()/" data-url-tablet="DC-SZbI34iNS0S6w9ldAVpjJTDo=/70x50/smart/filters:strip_icc()/" data-url-desktop="216sZlwK6pAax8dfxeBbObZJSog=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Bailarinas do &#39;FaustÃ£o&#39; abalam em ensaio de biquÃ­ni em barco; fotos</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="small" class="chamada chamada-principal mobile-grid-full"><a href="http://epoca.globo.com/colunas-e-blogs/bruno-astuto/noticia/2015/10/modelo-se-desequilibra-e-cai-na-sao-paulo-fashion-week.html" class="" title="Modelo se desequilibra e cai na SPFW: 
&#39;A plateia aplaudiu&#39;, afirma estilista (Ag News)" rel="bookmark"><span class="conteudo"><p>Modelo se desequilibra e cai na SPFW: <br />&#39;A plateia aplaudiu&#39;, afirma estilista</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-22 13:32:26" class="chamada chamada-principal mobile-grid-full"><a href="http://revistamonet.globo.com/Series/noticia/2015/10/lady-gaga-se-pega-com-atriz-em-cena-de-american-horror-story-e-quebra-internet.html" class="foto" title="Lady Gaga &#39;recepciona&#39; atriz estreante em sÃ©rie com cena picante; assista (ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/ZVM-ha9dLLvGOJSWVnMFC3CMXrA=/filters:quality(10):strip_icc()/s2.glbimg.com/0Vle6jfj8m-koPmfIFl69usKuBM=/139x131:428x324/120x80/s.glbimg.com/en/ho/f/original/2015/10/22/lady_gaga_american_horror.jpg" alt="Lady Gaga &#39;recepciona&#39; atriz estreante em sÃ©rie com cena picante; assista (ReproduÃ§Ã£o)" title="Lady Gaga &#39;recepciona&#39; atriz estreante em sÃ©rie com cena picante; assista (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/0Vle6jfj8m-koPmfIFl69usKuBM=/139x131:428x324/120x80/s.glbimg.com/en/ho/f/original/2015/10/22/lady_gaga_american_horror.jpg" data-url-smart_horizontal="_lw3lcRv32Yo7PkkHHYuqltlQVI=/90x0/smart/filters:strip_icc()/" data-url-smart="_lw3lcRv32Yo7PkkHHYuqltlQVI=/90x0/smart/filters:strip_icc()/" data-url-feature="_lw3lcRv32Yo7PkkHHYuqltlQVI=/90x0/smart/filters:strip_icc()/" data-url-tablet="oZhN57PpUUVl69aFvnvu168NYx8=/70x50/smart/filters:strip_icc()/" data-url-desktop="wAzSueOaRrtjSZ2EbYHFIOTmjQU=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Lady Gaga &#39;recepciona&#39; atriz estreante em sÃ©rie com cena picante; assista</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-10-22 14:44:50" class="chamada chamada-principal mobile-grid-full"><a href="http://gnt.globo.com/receitas/receitas/espaguete-carbonara-do-olivier.htm" class="foto" title="Anquier mostra como fazer o verdadeiro espaguete Ã  carbonara; vÃ­deo e receita (reproduÃ§Ã£o GNT)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/M1hdE5sITP7k3BfaxOZJrtVubAQ=/filters:quality(10):strip_icc()/s2.glbimg.com/ukTuWu96CBsAXisy5WvMvurfL9E=/13x148:356x377/120x80/s.glbimg.com/en/ho/f/original/2015/10/22/espaguete_a_carbonara_do_olivier.jpg" alt="Anquier mostra como fazer o verdadeiro espaguete Ã  carbonara; vÃ­deo e receita (reproduÃ§Ã£o GNT)" title="Anquier mostra como fazer o verdadeiro espaguete Ã  carbonara; vÃ­deo e receita (reproduÃ§Ã£o GNT)"
                data-original-image="s2.glbimg.com/ukTuWu96CBsAXisy5WvMvurfL9E=/13x148:356x377/120x80/s.glbimg.com/en/ho/f/original/2015/10/22/espaguete_a_carbonara_do_olivier.jpg" data-url-smart_horizontal="_z_xNyaVk5cXDlkMCs782ew6mNM=/90x0/smart/filters:strip_icc()/" data-url-smart="_z_xNyaVk5cXDlkMCs782ew6mNM=/90x0/smart/filters:strip_icc()/" data-url-feature="_z_xNyaVk5cXDlkMCs782ew6mNM=/90x0/smart/filters:strip_icc()/" data-url-tablet="v4zzw540a08cE5nXonnkG1dogxI=/70x50/smart/filters:strip_icc()/" data-url-desktop="vM1K6c8ht1uBCJcVDJBMC5sdMTQ=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Anquier mostra como fazer o verdadeiro espaguete Ã  carbonara; vÃ­deo e receita</p></span></a></div></div><div class="show-more"><span class="setinha-show-more"></span></div></div><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-novelas
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2015/", "logo": "http://s2.glbimg.com/tjsQl1SMvddbD-BGiSoC2Nb6kqg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-36x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2015/rss/", "logo_tv": "http://s2.glbimg.com/y8thjGY4IQuSvc4VfunyXi0d8C4=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/FRxoFZaNszGUmyoDZALOiIC_SOY=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/10/22/alem_do_tempo_segunda_fase-36x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/3Z-Sw1ITHzu4-EEfXG-zRbCs_dY=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/10/22/alem_g_45x30_1.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "I Love Parais\u00f3polis", "url": "http://gshow.globo.com/novelas/i-love-paraisopolis/index.html", "logo": "http://s2.glbimg.com/ChecsIkfHlI4H_yHoe_c_6tXTA4=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis36x20.jpg", "slug": "alto-astral", "url_feed": "http://gshow.globo.com/novelas/i-love-paraisopolis/rss/", "logo_tv": "http://s2.glbimg.com/6Kj2EtayTr5_t0ya4Iy8HHIn-vg=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "A Regra do Jogo", "url": "http://gshow.globo.com/novelas/a-regra-do-jogo/", "logo": "http://s2.glbimg.com/K-Nvvvm7zp2F7zS8TcY6TkIcxoE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-36x20.jpg", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/a-regra-do-jogo/rss/", "logo_tv": "http://s2.glbimg.com/4B7Jk0R6K1HlAAhuZIrDOJp8hbs=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-45x30.jpg"}, {"ordering": 70, "default": false, "cor": "#FF7F00", "name": "Verdades Secretas", "url": "http://gshow.globo.com/novelas/verdades-secretas/index.html", "logo": "http://s2.glbimg.com/ouhqJ9mLuAXh-gcEUW8wk165DUQ=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_1.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/verdades-secretas/rss", "logo_tv": "http://s2.glbimg.com/Huyh0y1AcqgjDpRaBLWCCkQY-Oo=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_2.jpg"}]);}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2015/", "logo": "http://s2.glbimg.com/tjsQl1SMvddbD-BGiSoC2Nb6kqg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-36x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2015/rss/", "logo_tv": "http://s2.glbimg.com/y8thjGY4IQuSvc4VfunyXi0d8C4=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/FRxoFZaNszGUmyoDZALOiIC_SOY=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/10/22/alem_do_tempo_segunda_fase-36x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/3Z-Sw1ITHzu4-EEfXG-zRbCs_dY=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/10/22/alem_g_45x30_1.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "I Love Parais\u00f3polis", "url": "http://gshow.globo.com/novelas/i-love-paraisopolis/index.html", "logo": "http://s2.glbimg.com/ChecsIkfHlI4H_yHoe_c_6tXTA4=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis36x20.jpg", "slug": "alto-astral", "url_feed": "http://gshow.globo.com/novelas/i-love-paraisopolis/rss/", "logo_tv": "http://s2.glbimg.com/6Kj2EtayTr5_t0ya4Iy8HHIn-vg=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "A Regra do Jogo", "url": "http://gshow.globo.com/novelas/a-regra-do-jogo/", "logo": "http://s2.glbimg.com/K-Nvvvm7zp2F7zS8TcY6TkIcxoE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-36x20.jpg", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/a-regra-do-jogo/rss/", "logo_tv": "http://s2.glbimg.com/4B7Jk0R6K1HlAAhuZIrDOJp8hbs=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-45x30.jpg"}, {"ordering": 70, "default": false, "cor": "#FF7F00", "name": "Verdades Secretas", "url": "http://gshow.globo.com/novelas/verdades-secretas/index.html", "logo": "http://s2.glbimg.com/ouhqJ9mLuAXh-gcEUW8wk165DUQ=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_1.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/verdades-secretas/rss", "logo_tv": "http://s2.glbimg.com/Huyh0y1AcqgjDpRaBLWCCkQY-Oo=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_2.jpg"}]);}});}
</script><div id="libby-home-widget-horoscopo" class="libby-home-widgetBase libby-home-widget-horoscopo analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais entretenimento" href="http://variedades.globo.com/"><span class="text">mais variedades</span><span class="arrow">&rsaquo;</span></a></div></section></div><div id="ad-position-middle3" class="opec"><div id="banner_slb_fim" class="tag-manager-publicidade-container"></div></div><div class="container columns clearfix glb-area-colunas second-area"><section class="area news-column"></section><section class="area sports-column"></section><section class="area etc-column analytics-area"></section></div><section class="area central container analytics-area analytics-id-O"><div class="separator"></div><section class="agrupador-quadruplo-tecnologia analytics-area analytics-id-T clearfix franja-inferior tecnologia"><div class="cabecalho"><h2><a href="http://www.techtudo.com.br/" title="TECNOLOGIA &amp; GAMES"><span class="word word-0">TECNOLOGIA</span><span class="word word-1">&</span><span class="word word-2">GAMES</span></a></h2><div id="ad-position-x34" class="opec"><div id="banner_selo4" class="tag-manager-publicidade-container"></div></div><aside class="links analytics-area analytics-id-T"><ul><li><a href="http://www.techtudo.com.br/jogos/" title="jogos">jogos</a></li><li><a href="http://www.techtudo.com.br/downloads/" title="downloads">downloads</a></li><li><a href="http://forum.techtudo.com.br/" title="fÃ³rum">fÃ³rum</a></li><li><a href="http://www.techtudo.com.br/mobile/" title="celulares">celulares</a></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaque"><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/noticias/noticia/2015/10/novos-emojis-chegam-ao-iphone-com-atualizacao-ios-91-veja-como-baixar.html" title="iPhone ganha mais emojis com a 
chegada do novo iOS 9.1; confira"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/qmZV9ARy7Pc7CICUBTOojY1e1Nk=/filters:quality(10):strip_icc()/s2.glbimg.com/AiQobE7flA27_xD03BKcanpFXls=/0x125:4000x2247/245x130/s.glbimg.com/po/tt2/f/original/2015/10/22/2.jpg" alt="iPhone ganha mais emojis com a 
chegada do novo iOS 9.1; confira (Lucas Mendes/TechTudo)" title="iPhone ganha mais emojis com a 
chegada do novo iOS 9.1; confira (Lucas Mendes/TechTudo)"
             data-original-image="s2.glbimg.com/AiQobE7flA27_xD03BKcanpFXls=/0x125:4000x2247/245x130/s.glbimg.com/po/tt2/f/original/2015/10/22/2.jpg" data-url-smart_horizontal="piI2PXCkke_4h0xIS-Ef-5VT_NE=/90x56/smart/filters:strip_icc()/" data-url-smart="piI2PXCkke_4h0xIS-Ef-5VT_NE=/90x56/smart/filters:strip_icc()/" data-url-feature="piI2PXCkke_4h0xIS-Ef-5VT_NE=/90x56/smart/filters:strip_icc()/" data-url-tablet="Ac6Wn1XNBTD2m-vCOZT4SY3sYQc=/160x96/smart/filters:strip_icc()/" data-url-desktop="NC3syCUXdafb0ndTeyJPKQ9QA6Q=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>iPhone ganha mais emojis com a <br />chegada do novo iOS 9.1; confira</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/noticias/noticia/2015/10/snapchat-e-o-aplicativo-que-mais-atrapalha-desempenho-do-celular-diz-avg.html" title="Aplicativos &#39;famosos&#39; travam e 
deixam seu smart lento; lista"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/IxXbycFqJx4Azb8V-euTH0HhPSg=/filters:quality(10):strip_icc()/s2.glbimg.com/WrD_u9VNKG9KEuwIeq-THrT_uYU=/0x168:5471x3072/245x130/s.glbimg.com/po/tt2/f/original/2015/09/15/img_5485_2.jpg" alt="Aplicativos &#39;famosos&#39; travam e 
deixam seu smart lento; lista (Luana Marfim/TechTudo)" title="Aplicativos &#39;famosos&#39; travam e 
deixam seu smart lento; lista (Luana Marfim/TechTudo)"
             data-original-image="s2.glbimg.com/WrD_u9VNKG9KEuwIeq-THrT_uYU=/0x168:5471x3072/245x130/s.glbimg.com/po/tt2/f/original/2015/09/15/img_5485_2.jpg" data-url-smart_horizontal="sk8QJ-Wh2VNRVqNtKJFe9S0xrnw=/90x56/smart/filters:strip_icc()/" data-url-smart="sk8QJ-Wh2VNRVqNtKJFe9S0xrnw=/90x56/smart/filters:strip_icc()/" data-url-feature="sk8QJ-Wh2VNRVqNtKJFe9S0xrnw=/90x56/smart/filters:strip_icc()/" data-url-tablet="I6rvEsQhovmqXF4MC1d-cOzKJi4=/160x96/smart/filters:strip_icc()/" data-url-desktop="X-t76Nus3v1zDb0kbWHZUzEC9aU=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Aplicativos &#39;famosos&#39; travam e <br />deixam seu smart lento; lista</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/dicas-e-tutoriais/noticia/2015/10/google-grava-tudo-que-voce-fala-com-o-now-no-android-como-ouvir-e-apagar.html" title="Google pode gravar tudo o que vocÃª diz no Android; saiba ouvir"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/hSE6WICgbOGfKnoBoFgEmPzdOnI=/filters:quality(10):strip_icc()/s2.glbimg.com/cY4-GPy15TLQY2cphNheYOSZ6pk=/0x80:3204x1781/245x130/s.glbimg.com/po/tt2/f/original/2015/02/06/luana_android.jpg" alt="Google pode gravar tudo o que vocÃª diz no Android; saiba ouvir (Luciana Maline/TechTudo)" title="Google pode gravar tudo o que vocÃª diz no Android; saiba ouvir (Luciana Maline/TechTudo)"
             data-original-image="s2.glbimg.com/cY4-GPy15TLQY2cphNheYOSZ6pk=/0x80:3204x1781/245x130/s.glbimg.com/po/tt2/f/original/2015/02/06/luana_android.jpg" data-url-smart_horizontal="lLtx1O68nxZlyjAh6elNiwOBOfc=/90x56/smart/filters:strip_icc()/" data-url-smart="lLtx1O68nxZlyjAh6elNiwOBOfc=/90x56/smart/filters:strip_icc()/" data-url-feature="lLtx1O68nxZlyjAh6elNiwOBOfc=/90x56/smart/filters:strip_icc()/" data-url-tablet="Z9exRKTL1q3amETBPNCPYCcBB2s=/160x96/smart/filters:strip_icc()/" data-url-desktop="2s8CLIGEmb1wVsA1WFeKu5S772g=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Google pode gravar tudo o que <br />vocÃª diz no Android; saiba ouvir</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/listas/noticia/2015/10/lista-reune-jogos-exclusivos-do-playstation-4-que-ainda-serao-lancados.html" title="Veja lista dos games exclusivos para PS4 que ainda estÃ£o por vir"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/efOOW2fIns1b7zPl5x9dt0AKz08=/filters:quality(10):strip_icc()/s2.glbimg.com/Xxx3VFYFQWLVrCyt8ry1EUvUK3E=/0x0:1920x1018/245x130/s.glbimg.com/po/tt2/f/original/2015/10/20/the-nathan-drake-collection.jpg" alt="Veja lista dos games exclusivos para PS4 que ainda estÃ£o por vir (DivulgaÃ§Ã£o)" title="Veja lista dos games exclusivos para PS4 que ainda estÃ£o por vir (DivulgaÃ§Ã£o)"
             data-original-image="s2.glbimg.com/Xxx3VFYFQWLVrCyt8ry1EUvUK3E=/0x0:1920x1018/245x130/s.glbimg.com/po/tt2/f/original/2015/10/20/the-nathan-drake-collection.jpg" data-url-smart_horizontal="Jp8BwieolCxAHr2swLzpdaWCKzg=/90x56/smart/filters:strip_icc()/" data-url-smart="Jp8BwieolCxAHr2swLzpdaWCKzg=/90x56/smart/filters:strip_icc()/" data-url-feature="Jp8BwieolCxAHr2swLzpdaWCKzg=/90x56/smart/filters:strip_icc()/" data-url-tablet="spriaztZCXxN3Rc4x3oWe5IygVA=/160x96/smart/filters:strip_icc()/" data-url-desktop="fRVzCAinsZ_A6CZsei0aqWC8fPA=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Veja lista dos games exclusivos <br />para PS4 que ainda estÃ£o por vir</p></span></a></li></ul></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-tecnologia']);</script><div class="separator"></div><section class="agrupador-sextuplo analytics-area analytics-id-S"><section class="analytics-area analytics-id-E esquerda tvg"><div class="cabecalho"><h2><span class="word word-0">moda</span><span class="word word-1">&amp;</span><span class="word word-2">beleza</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://gnt.globo.com/moda/votacoes/vote-nos-looks-favoritos-de-caio-braz-pelos-corredores-do-spfw.htm" alt="Vote nos looks bem extravagantes de quem circula pelo SPFW"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/1KmrnUTvMuza1WSJCaoD26PPJDg=/filters:quality(10):strip_icc()/s2.glbimg.com/dzwq0SGa2ukL3crtk0y6_S0ZepI=/90x22:284x147/155x100/s.glbimg.com/en/ho/f/original/2015/10/22/vote_no_look_gnt_maga_moura-02.jpg" alt="Vote nos looks bem extravagantes de quem circula pelo SPFW (GNT)" title="Vote nos looks bem extravagantes de quem circula pelo SPFW (GNT)"
            data-original-image="s2.glbimg.com/dzwq0SGa2ukL3crtk0y6_S0ZepI=/90x22:284x147/155x100/s.glbimg.com/en/ho/f/original/2015/10/22/vote_no_look_gnt_maga_moura-02.jpg" data-url-smart_horizontal="QRYr6n3M4ydMfRMBhpX1lFX2XxU=/90x56/smart/filters:strip_icc()/" data-url-smart="QRYr6n3M4ydMfRMBhpX1lFX2XxU=/90x56/smart/filters:strip_icc()/" data-url-feature="QRYr6n3M4ydMfRMBhpX1lFX2XxU=/90x56/smart/filters:strip_icc()/" data-url-tablet="xrrcA1lCWuLMdNPAUUxhzDkp62c=/122x75/smart/filters:strip_icc()/" data-url-desktop="TNpyV5bXLRkZRh-_jKvBFZM_kWY=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>Quem usaria?</h3><p>Vote nos looks bem extravagantes de quem circula pelo SPFW</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revistamarieclaire.globo.com/Moda/noticia/2015/10/azulejos-portugueses-novo-trico-noivas-veja-os-destaques-do-quarto-dia-de-spfw-inverno-2016.html" alt="Noivas transparentes, azulejos e novo tricÃ´ sÃ£o destaques; fotos"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/Xha1Il1JTlLsOlwJ8ObQxUDQtFA=/filters:quality(10):strip_icc()/s2.glbimg.com/Cjhs4YPPPIDFwo9dT-I1YgRm78I=/225x8:613x258/155x100/e.glbimg.com/og/ed/f/original/2015/10/21/helo.jpg" alt="Noivas transparentes, azulejos e novo tricÃ´ sÃ£o destaques; fotos (DivulgaÃ§Ã£o/AgÃªncia Fotosite)" title="Noivas transparentes, azulejos e novo tricÃ´ sÃ£o destaques; fotos (DivulgaÃ§Ã£o/AgÃªncia Fotosite)"
            data-original-image="s2.glbimg.com/Cjhs4YPPPIDFwo9dT-I1YgRm78I=/225x8:613x258/155x100/e.glbimg.com/og/ed/f/original/2015/10/21/helo.jpg" data-url-smart_horizontal="iQtxA1Vxs5y8o8zQ_U4V63cv4Lg=/90x56/smart/filters:strip_icc()/" data-url-smart="iQtxA1Vxs5y8o8zQ_U4V63cv4Lg=/90x56/smart/filters:strip_icc()/" data-url-feature="iQtxA1Vxs5y8o8zQ_U4V63cv4Lg=/90x56/smart/filters:strip_icc()/" data-url-tablet="otTlvOIMdzjzdI6-v9bsGJ1h2MA=/122x75/smart/filters:strip_icc()/" data-url-desktop="8ibPDfu8e0MF9c5ldWujLe6DMys=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>quarto dia de SPFW</h3><p>Noivas transparentes, azulejos e novo tricÃ´ sÃ£o destaques; fotos</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://epoca.globo.com/colunas-e-blogs/bruno-astuto/noticia/2015/10/de-quem-sao-esses-pes.html" alt="FotÃ³grafo fala de &#39;batalha&#39; para clicar os pÃ©s de Paolla Oliveira"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/35mvhplcNj3XTdD0yWWg7iA8URs=/filters:quality(10):strip_icc()/s2.glbimg.com/AstA0YgVfFX_j8vwSDgQoaTCVdc=/109x1950:4221x4602/155x100/e.glbimg.com/og/ed/f/original/2015/10/21/paola.jpg" alt="FotÃ³grafo fala de &#39;batalha&#39; para clicar os pÃ©s de Paolla Oliveira (Rodrigo Lopes)" title="FotÃ³grafo fala de &#39;batalha&#39; para clicar os pÃ©s de Paolla Oliveira (Rodrigo Lopes)"
            data-original-image="s2.glbimg.com/AstA0YgVfFX_j8vwSDgQoaTCVdc=/109x1950:4221x4602/155x100/e.glbimg.com/og/ed/f/original/2015/10/21/paola.jpg" data-url-smart_horizontal="8Q_8mIh2MGJMpEsOqbvv2a0oGe4=/90x56/smart/filters:strip_icc()/" data-url-smart="8Q_8mIh2MGJMpEsOqbvv2a0oGe4=/90x56/smart/filters:strip_icc()/" data-url-feature="8Q_8mIh2MGJMpEsOqbvv2a0oGe4=/90x56/smart/filters:strip_icc()/" data-url-tablet="779IPeqduDvYOLtxg8p6guMk3K8=/122x75/smart/filters:strip_icc()/" data-url-desktop="yvOvrzihfAkgC1BoiW9_orGl53E=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>fetiche</h3><p>FotÃ³grafo fala de &#39;batalha&#39; para clicar os pÃ©s de Paolla Oliveira</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://gnt.globo.com/" title="GNT">GNT</a></h3></li><li><h3><a href="http://vogue.globo.com/" title="vogue">vogue</a></h3></li><li></li></ul></aside></section><div class="separator"></div><section class="analytics-area analytics-id-D direita tvg"><div class="cabecalho"><h2><span class="word word-0">CASA</span><span class="word word-1">&amp;</span><span class="word word-2">DECORAÃÃO</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://gnt.globo.com/programas/decora/videos/4548498.htm" alt="Aprenda a fazer materiais de trabalho para renovar o ambiente"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/KWhXc1giT3ZMxjfLgML-ZPjuN8A=/filters:quality(10):strip_icc()/s2.glbimg.com/xx3-7K2mF2aYvSvfNiH-puGJquY=/23x0:620x385/155x100/g.glbimg.com/og/gs/gsat5/f/original/2015/10/20/cestinhos.jpg" alt="Aprenda a fazer materiais de trabalho para renovar o ambiente (ReproduÃ§Ã£o/ GNT)" title="Aprenda a fazer materiais de trabalho para renovar o ambiente (ReproduÃ§Ã£o/ GNT)"
            data-original-image="s2.glbimg.com/xx3-7K2mF2aYvSvfNiH-puGJquY=/23x0:620x385/155x100/g.glbimg.com/og/gs/gsat5/f/original/2015/10/20/cestinhos.jpg" data-url-smart_horizontal="7LB2JolrAyHg5CAQTpETQN-Q-DQ=/90x56/smart/filters:strip_icc()/" data-url-smart="7LB2JolrAyHg5CAQTpETQN-Q-DQ=/90x56/smart/filters:strip_icc()/" data-url-feature="7LB2JolrAyHg5CAQTpETQN-Q-DQ=/90x56/smart/filters:strip_icc()/" data-url-tablet="TXkVxXD6NJ2x9MdTE3fb6K4gUnA=/122x75/smart/filters:strip_icc()/" data-url-desktop="S7pyH3Se_xeRASi6QnBUcAQp9eQ=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>mÃ£o na massa!</h3><p>Aprenda a fazer materiais de trabalho para renovar o ambiente</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revista.zapimoveis.com.br/saiba-como-usar-baus-para-decorar-a-casa-e-ganhar-espaco-extra-para-os-objetos-3371935-sc/" alt="DecoraÃ§Ã£o com baÃº deixa espaÃ§o mais charmoso e funcional"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/Oo52BJHXSFFJJ1LfT2_i_iqSxhU=/filters:quality(10):strip_icc()/s2.glbimg.com/pLWJAufFZKeRBynm4fwG902fp-Y=/0x2:580x376/155x100/s.glbimg.com/en/ho/f/original/2015/10/22/bau_1.jpg" alt="DecoraÃ§Ã£o com baÃº deixa espaÃ§o mais charmoso e funcional (Shutterstock)" title="DecoraÃ§Ã£o com baÃº deixa espaÃ§o mais charmoso e funcional (Shutterstock)"
            data-original-image="s2.glbimg.com/pLWJAufFZKeRBynm4fwG902fp-Y=/0x2:580x376/155x100/s.glbimg.com/en/ho/f/original/2015/10/22/bau_1.jpg" data-url-smart_horizontal="I4lSr--X0B_mYdCuQ3uu6kNhO4k=/90x56/smart/filters:strip_icc()/" data-url-smart="I4lSr--X0B_mYdCuQ3uu6kNhO4k=/90x56/smart/filters:strip_icc()/" data-url-feature="I4lSr--X0B_mYdCuQ3uu6kNhO4k=/90x56/smart/filters:strip_icc()/" data-url-tablet="tXZJ6YYqIfxko3Mqxyl9KMVEKhg=/122x75/smart/filters:strip_icc()/" data-url-desktop="U0wyeraNz5q7k7w27-eRFeHMnoQ=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>ar vintage!</h3><p>DecoraÃ§Ã£o com baÃº deixa espaÃ§o mais charmoso e funcional</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://casavogue.globo.com/Interiores/noticia/2015/10/17-ideias-de-decor-para-quarto-de-bebe.html" alt="Confira sugestÃµes diversas para decorar o quarto do bebÃª"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/H0Tqyw8auYIR5q1Hd42VPzJ7hck=/filters:quality(10):strip_icc()/s2.glbimg.com/lUH9X1l5WjKMafZBb5vdBY9j6i0=/0x13:618x412/155x100/s.glbimg.com/en/ho/f/original/2015/10/22/top-18-quartos-de-bebe_14.jpg" alt="Confira sugestÃµes diversas para decorar o quarto do bebÃª (DivulgaÃ§Ã£o)" title="Confira sugestÃµes diversas para decorar o quarto do bebÃª (DivulgaÃ§Ã£o)"
            data-original-image="s2.glbimg.com/lUH9X1l5WjKMafZBb5vdBY9j6i0=/0x13:618x412/155x100/s.glbimg.com/en/ho/f/original/2015/10/22/top-18-quartos-de-bebe_14.jpg" data-url-smart_horizontal="SEZcbK-dZam5A_Tf30IYGD4AZ8Q=/90x56/smart/filters:strip_icc()/" data-url-smart="SEZcbK-dZam5A_Tf30IYGD4AZ8Q=/90x56/smart/filters:strip_icc()/" data-url-feature="SEZcbK-dZam5A_Tf30IYGD4AZ8Q=/90x56/smart/filters:strip_icc()/" data-url-tablet="EHpiJeyaYiKnKMRo8dO7hzt2KJ0=/122x75/smart/filters:strip_icc()/" data-url-desktop="LHG6x5cDE1BN7NwsoS9m6aNTfhU=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>se inspire!</h3><p>Confira sugestÃµes diversas para decorar o quarto do bebÃª</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://revistacasaejardim.globo.com/" title="CASA E JARDIM">CASA E JARDIM</a></h3></li><li><h3><a href="http://casavogue.globo.com/" title="CASA VOGUE">CASA VOGUE</a></h3></li><li></li></ul></aside></section></section><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior famosos-style"><div class="cabecalho"><h2><a title="famosos"><span class='logo'>famosos</span><span class='texto'>famosos</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://ego.globo.com/">ego</a></h3></li><li class=""><h3><a href="http://revistaquem.globo.com/">quem</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://extra.globo.com/famosos/roberta-close-esconde-rosto-ao-ser-fotografada-deixando-supermercado-em-bairro-da-zona-sul-do-rio-17849311.html" title="Roberta Close esconde o rosto em dia de ir ao mercado; fotos"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/8Wl9GLPXhbRLWzNwoUn0PwWLk3U=/filters:quality(10):strip_icc()/s2.glbimg.com/Cb8LTW11snTNMdxL1CdYj8p-vVc=/0x0:448x237/245x130/s.glbimg.com/en/ho/f/original/2015/10/22/close.jpg" alt="Roberta Close esconde o rosto em dia de ir ao mercado; fotos (Marcus PavÃ£o)" title="Roberta Close esconde o rosto em dia de ir ao mercado; fotos (Marcus PavÃ£o)"
                    data-original-image="s2.glbimg.com/Cb8LTW11snTNMdxL1CdYj8p-vVc=/0x0:448x237/245x130/s.glbimg.com/en/ho/f/original/2015/10/22/close.jpg" data-url-smart_horizontal="bbzzwnlwP3CLtoe8-Gtv6BVj-P8=/90x56/smart/filters:strip_icc()/" data-url-smart="bbzzwnlwP3CLtoe8-Gtv6BVj-P8=/90x56/smart/filters:strip_icc()/" data-url-feature="bbzzwnlwP3CLtoe8-Gtv6BVj-P8=/90x56/smart/filters:strip_icc()/" data-url-tablet="Sp0cxHX6QS5vG-V9v_Fs1jTqol8=/160x95/smart/filters:strip_icc()/" data-url-desktop="qqtIKnILZNxD7stWXd0LMSWSyMk=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Roberta Close esconde o rosto em dia de ir ao mercado; fotos</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://extra.globo.com/famosos/sasha-meneghel-muda-visual-adota-franja-17853511.html" title="Sasha muda o visual e passa a usar franja; amplie a foto aqui"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/yBRVuG7Ryd4MbDDkR815aJMY4Hk=/filters:quality(10):strip_icc()/s2.glbimg.com/AaOCbWdRrnCEmPGloHJcpl3Q4tU=/0x130:448x367/245x130/s.glbimg.com/en/ho/f/original/2015/10/22/sasha.jpg" alt="Sasha muda o visual e passa a usar franja; amplie a foto aqui (reproduÃ§Ã£o)" title="Sasha muda o visual e passa a usar franja; amplie a foto aqui (reproduÃ§Ã£o)"
                    data-original-image="s2.glbimg.com/AaOCbWdRrnCEmPGloHJcpl3Q4tU=/0x130:448x367/245x130/s.glbimg.com/en/ho/f/original/2015/10/22/sasha.jpg" data-url-smart_horizontal="qmd9m9Oif1BdeKvkKFB7lT9y1hE=/90x56/smart/filters:strip_icc()/" data-url-smart="qmd9m9Oif1BdeKvkKFB7lT9y1hE=/90x56/smart/filters:strip_icc()/" data-url-feature="qmd9m9Oif1BdeKvkKFB7lT9y1hE=/90x56/smart/filters:strip_icc()/" data-url-tablet="bOfCNzeOSSylAK3aRc7EBrMa5Mc=/160x95/smart/filters:strip_icc()/" data-url-desktop="yZcIo7cmXoClVwJ5dh8RH1cH3qM=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Sasha muda o visual e passa a usar franja; amplie a foto aqui</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://vogue.globo.com/moda/gente/noticia/2015/10/atriz-e-comediante-sarah-silverman-abusa-mesmo-do-decote.html" title="Comediante investe em decote vertiginoso para prÃ©-estreia"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/iVpJXm23QW14qNPlZUc8dUBWGS4=/filters:quality(10):strip_icc()/s2.glbimg.com/UDA-QnMS5pbC9tsTO2jLrGGdMbc=/105x86:862x487/245x130/e.glbimg.com/og/ed/f/original/2015/10/22/sarahsilv.jpg" alt="Comediante investe em decote vertiginoso para prÃ©-estreia (Getty)" title="Comediante investe em decote vertiginoso para prÃ©-estreia (Getty)"
                    data-original-image="s2.glbimg.com/UDA-QnMS5pbC9tsTO2jLrGGdMbc=/105x86:862x487/245x130/e.glbimg.com/og/ed/f/original/2015/10/22/sarahsilv.jpg" data-url-smart_horizontal="OEPQdnRq_YBVgSFWJa1nWMDC_BI=/90x56/smart/filters:strip_icc()/" data-url-smart="OEPQdnRq_YBVgSFWJa1nWMDC_BI=/90x56/smart/filters:strip_icc()/" data-url-feature="OEPQdnRq_YBVgSFWJa1nWMDC_BI=/90x56/smart/filters:strip_icc()/" data-url-tablet="eMRCUnm6DUMi_x_AWpomWjKI5ng=/160x95/smart/filters:strip_icc()/" data-url-desktop="6gAnld5FdfWJT11rIi9RxoknE9Y=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Comediante investe em decote vertiginoso para prÃ©-estreia</p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://revistaglamour.globo.com/Celebridades/noticia/2015/10/candice-swanepoel-em-clique-ousadissimo.html" title="Top da barriga negativa mostra que tambÃ©m Ã© musa de costas"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/JErUdeUnno1cdlWE45ZF4TrIzuc=/filters:quality(10):strip_icc()/s2.glbimg.com/Clcj_ZCfxhRIJuV4P9dZZO6-AUs=/101x118:578x371/245x130/e.glbimg.com/og/ed/f/original/2015/10/22/candice.jpg" alt="Top da barriga negativa mostra que tambÃ©m Ã© musa de costas (ReproduÃ§Ã£o/Instagram)" title="Top da barriga negativa mostra que tambÃ©m Ã© musa de costas (ReproduÃ§Ã£o/Instagram)"
                    data-original-image="s2.glbimg.com/Clcj_ZCfxhRIJuV4P9dZZO6-AUs=/101x118:578x371/245x130/e.glbimg.com/og/ed/f/original/2015/10/22/candice.jpg" data-url-smart_horizontal="6PKK7hyIy2VC6y7T6DypDVLmMi0=/90x56/smart/filters:strip_icc()/" data-url-smart="6PKK7hyIy2VC6y7T6DypDVLmMi0=/90x56/smart/filters:strip_icc()/" data-url-feature="6PKK7hyIy2VC6y7T6DypDVLmMi0=/90x56/smart/filters:strip_icc()/" data-url-tablet="jhtrFQ3v768xEuO5TZXxUlR3qRo=/160x95/smart/filters:strip_icc()/" data-url-desktop="XEO7d7MAav6QlA4RTmMTqLXXhR0=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Top da barriga negativa mostra que tambÃ©m Ã© musa de costas</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_famosos-style']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior gshow"><div class="cabecalho"><h2><a href="http://gshow.globo.com/" title="novelas, sÃ©ries, programas e muito mais"><span class='logo'>novelas, sÃ©ries, programas e muito mais</span><span class='texto'>novelas, sÃ©ries, programas e muito mais</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://gshow.globo.com/estilo-tv/">ESTILO TV</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/">A REGRA DO JOGO</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/i-love-paraisopolis/index.html">I LOVE PARAISÃPOLIS</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/alem-do-tempo/">ALÃM DO TEMPO</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/malhacao/2015/">MALHAÃÃO</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/i-love-paraisopolis/Vem-por-ai/noticia/2015/10/juneca-purpurina-ressurge-e-se-apresenta-ao-som-da-banda-uo.html" title="Em &#39;I Love&#39;, Juneca Purpurina danÃ§a ao som da Banda UÃ³"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/AbsIWGfqrCqpKodG2GPnWYBSy10=/filters:quality(10):strip_icc()/s2.glbimg.com/vzt-ODr3v47oiGosswHQHai_zv4=/0x0:640x339/245x130/s.glbimg.com/og/rg/f/original/2015/10/22/para640.jpg" alt="Em &#39;I Love&#39;, Juneca Purpurina danÃ§a ao som da Banda UÃ³ (Globo)" title="Em &#39;I Love&#39;, Juneca Purpurina danÃ§a ao som da Banda UÃ³ (Globo)"
                    data-original-image="s2.glbimg.com/vzt-ODr3v47oiGosswHQHai_zv4=/0x0:640x339/245x130/s.glbimg.com/og/rg/f/original/2015/10/22/para640.jpg" data-url-smart_horizontal="haffqHmev1iZNbonfM50vmWExPc=/90x56/smart/filters:strip_icc()/" data-url-smart="haffqHmev1iZNbonfM50vmWExPc=/90x56/smart/filters:strip_icc()/" data-url-feature="haffqHmev1iZNbonfM50vmWExPc=/90x56/smart/filters:strip_icc()/" data-url-tablet="7jwhPb1ai6iljtwz2hvMwWDaseI=/160x95/smart/filters:strip_icc()/" data-url-desktop="ysDRuhTVhTP02_PSF9r59LcW4Tg=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Em &#39;I Love&#39;, Juneca Purpurina danÃ§a ao som da Banda UÃ³</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/malhacao/2015/Vem-por-ai/noticia/2015/10/luciana-fica-desesperada-ao-ouvir-revelacao-de-jorge-e-sueli.html" title="&#39;MalhaÃ§Ã£o&#39;: Lu fica desesperada ao ouvir revelaÃ§Ã£o dos pais"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/58xrTujM4cHK8W6nEG1cJcVvUU0=/filters:quality(10):strip_icc()/s2.glbimg.com/zDZThh48yKAl_IB5l4v4SohdWUQ=/0x34:692x401/245x130/s.glbimg.com/et/gs/f/original/2015/10/22/luciana-malhacao-2015.jpg" alt="&#39;MalhaÃ§Ã£o&#39;: Lu fica desesperada ao ouvir revelaÃ§Ã£o dos pais (TV Globo)" title="&#39;MalhaÃ§Ã£o&#39;: Lu fica desesperada ao ouvir revelaÃ§Ã£o dos pais (TV Globo)"
                    data-original-image="s2.glbimg.com/zDZThh48yKAl_IB5l4v4SohdWUQ=/0x34:692x401/245x130/s.glbimg.com/et/gs/f/original/2015/10/22/luciana-malhacao-2015.jpg" data-url-smart_horizontal="YmDkMpxG-m_VIV7G4xQ__orbmhY=/90x56/smart/filters:strip_icc()/" data-url-smart="YmDkMpxG-m_VIV7G4xQ__orbmhY=/90x56/smart/filters:strip_icc()/" data-url-feature="YmDkMpxG-m_VIV7G4xQ__orbmhY=/90x56/smart/filters:strip_icc()/" data-url-tablet="_8W4Cr-Yy4ReE8K4F0cO4THZNAg=/160x95/smart/filters:strip_icc()/" data-url-desktop="O_a1f7e1bnoxJ-0fwhHQjsz37A8=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>&#39;MalhaÃ§Ã£o&#39;: Lu fica desesperada ao ouvir revelaÃ§Ã£o dos pais</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/Bastidores/noticia/2015/10/livian-aragao-tieta-demi-lovato-nos-bastidores-do-caldeirao-muito-fa.html" title="LÃ­vian AragÃ£o nÃ£o perde sua chance e tieta Demi Lovato"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/wMe6wrQcjq_AkYljPCYAjIAbi98=/filters:quality(10):strip_icc()/s2.glbimg.com/eMA-8tPu4oa_EXJilbqL1BEhHkQ=/0x80:690x446/245x130/s.glbimg.com/et/gs/f/original/2015/10/22/demi-e-livian.jpg" alt="LÃ­vian AragÃ£o nÃ£o perde sua chance e tieta Demi Lovato (Arquivo Pessoal)" title="LÃ­vian AragÃ£o nÃ£o perde sua chance e tieta Demi Lovato (Arquivo Pessoal)"
                    data-original-image="s2.glbimg.com/eMA-8tPu4oa_EXJilbqL1BEhHkQ=/0x80:690x446/245x130/s.glbimg.com/et/gs/f/original/2015/10/22/demi-e-livian.jpg" data-url-smart_horizontal="c8fkiH-I9TUqwhuYJMNWBbaw0Bs=/90x56/smart/filters:strip_icc()/" data-url-smart="c8fkiH-I9TUqwhuYJMNWBbaw0Bs=/90x56/smart/filters:strip_icc()/" data-url-feature="c8fkiH-I9TUqwhuYJMNWBbaw0Bs=/90x56/smart/filters:strip_icc()/" data-url-tablet="BMH5JHj4ZuWrX8JXf4xxAkiMiNQ=/160x95/smart/filters:strip_icc()/" data-url-desktop="w20gozere9l_Ja_nKpVS0_D04TU=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>LÃ­vian AragÃ£o nÃ£o perde sua chance e tieta Demi Lovato</p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://gshow.globo.com/Bastidores/noticia/2015/10/cleo-pires-elogia-tom-cavalcante-imitando-fabio-jr-e-incrivel.html" title="&#39;Altas Horas&#39;: Cleo Pires fica surpresa com imitaÃ§Ã£o do pai"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/FQzZCk7wsDrI91ecgcd6QYwv2nc=/filters:quality(10):strip_icc()/s2.glbimg.com/7yaP7dzcMIJLM1-vxzCY8Uvsp6c=/320x200:5455x2927/245x130/s.glbimg.com/et/gs/f/original/2015/10/22/car_4717.jpg" alt="&#39;Altas Horas&#39;: Cleo Pires fica surpresa com imitaÃ§Ã£o do pai (Carol Caminha/Gshow)" title="&#39;Altas Horas&#39;: Cleo Pires fica surpresa com imitaÃ§Ã£o do pai (Carol Caminha/Gshow)"
                    data-original-image="s2.glbimg.com/7yaP7dzcMIJLM1-vxzCY8Uvsp6c=/320x200:5455x2927/245x130/s.glbimg.com/et/gs/f/original/2015/10/22/car_4717.jpg" data-url-smart_horizontal="jAT4dVF5lOWAK_7JAZZQ2sTlWcY=/90x56/smart/filters:strip_icc()/" data-url-smart="jAT4dVF5lOWAK_7JAZZQ2sTlWcY=/90x56/smart/filters:strip_icc()/" data-url-feature="jAT4dVF5lOWAK_7JAZZQ2sTlWcY=/90x56/smart/filters:strip_icc()/" data-url-tablet="bex46eqmalc9SXreEYaoKA8Qvuc=/160x95/smart/filters:strip_icc()/" data-url-desktop="LT2MBoIVxgIC0-kev3vZf_fWBXY=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>&#39;Altas Horas&#39;: Cleo Pires fica surpresa com imitaÃ§Ã£o do pai</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_gshow']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-musica-triplo analytics-area analytics-id-U franja-inferior"><div class="cabecalho"><h2><a href="http://musica.com.br/" title="MÃSICA">
            MÃSICA
        </a></h2><aside class="links analytics-area analytics-id-T"><ul><li class="mobile-grid-full "><h3><a href="http://musica.com.br/">MÃSICA.COM.BR</a></h3></li><li class="mobile-grid-full "><h3><a href="http://g1.globo.com/musica/">G1 MÃSICA</a></h3></li><li class="mobile-grid-full "><h3><a href="http://multishow.globo.com/musica/">MULTISHOW</a></h3></li><li class="mobile-grid-full "><h3><a href="http://radiobeat.com.br/">RADIOBEAT</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://oglobo.globo.com/cultura/musica/apos-video-viral-foo-fighters-confirma-show-em-cesena-17848315" title="Foo Fighters anuncia show em cidade da ItÃ¡lia que fez vÃ­deo especial; reveja aqui (reproduÃ§Ã£o)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/mV2qR3D5m4D52hXZqJnnNaDLFH4=/filters:quality(10):strip_icc()/s2.glbimg.com/6w1KJxZXqgx-wBvO0IqHC0AvH4M=/0x0:695x420/215x130/s.glbimg.com/en/ho/f/original/2015/07/30/foo.jpg"
                data-original-image="s2.glbimg.com/6w1KJxZXqgx-wBvO0IqHC0AvH4M=/0x0:695x420/215x130/s.glbimg.com/en/ho/f/original/2015/07/30/foo.jpg" data-url-smart_horizontal="1OYDRIf6BdU94NDOYkEJsHiT6js=/90x56/smart/filters:strip_icc()/" data-url-smart="1OYDRIf6BdU94NDOYkEJsHiT6js=/90x56/smart/filters:strip_icc()/" data-url-feature="1OYDRIf6BdU94NDOYkEJsHiT6js=/90x56/smart/filters:strip_icc()/" data-url-tablet="Nhgvltm43YzAp4UKeFePtSpkZ-0=/120x80/smart/filters:strip_icc()/" data-url-desktop="cZ_QDlJdX7G7DxOOss61zgGV5ko=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Foo Fighters anuncia show em cidade da ItÃ¡lia que fez vÃ­deo especial; reveja aqui</div></div></a></li><li class="mobile-grid-full"><a class="foto" href="http://g1.globo.com/musica/noticia/2015/10/justin-bieber-lanca-musica-sorry-e-divulga-clipe-com-dancarinas.html" title="Justin Bieber lanÃ§a mÃºsica &#39;Sorry&#39; e divulga clipe com danÃ§arinas; veja aqui (DivulgaÃ§Ã£o)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/z9hfffrTHbqVNHGaitTl_poEDW8=/filters:quality(10):strip_icc()/s2.glbimg.com/UapAiIOgpb_9XT0XsnoMz-WnZ3M=/52x0:572x315/215x130/s.glbimg.com/jo/g1/f/original/2015/10/22/bieber.jpg"
                data-original-image="s2.glbimg.com/UapAiIOgpb_9XT0XsnoMz-WnZ3M=/52x0:572x315/215x130/s.glbimg.com/jo/g1/f/original/2015/10/22/bieber.jpg" data-url-smart_horizontal="1Bwlr6Y8PmIgznM1WLxaoHf7hqM=/90x56/smart/filters:strip_icc()/" data-url-smart="1Bwlr6Y8PmIgznM1WLxaoHf7hqM=/90x56/smart/filters:strip_icc()/" data-url-feature="1Bwlr6Y8PmIgznM1WLxaoHf7hqM=/90x56/smart/filters:strip_icc()/" data-url-tablet="b9qliWIvTxCtdS0VsfrYwnAJup8=/120x80/smart/filters:strip_icc()/" data-url-desktop="sVnz5DfMkIUDTKkUHrIXnYqZdcI=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Justin Bieber lanÃ§a mÃºsica &#39;Sorry&#39; e divulga clipe com danÃ§arinas; veja aqui</div></div></a></li><li class="mobile-grid-full"><a class="foto" href="http://ego.globo.com/ego-teen/noticia/2015/10/demi-lovato-se-derrete-apos-show-adoro-estar-no-brasil.html" title="Demi Lovato elogia fÃ£s, diz que adora o paÃ­s, mas avisa:  agora sÃ³ deve voltar em 2017 (ReproduÃ§Ã£o/Vevo)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/U6mUGke_Y4etUN_84xHoRNUj3PU=/filters:quality(10):strip_icc()/s2.glbimg.com/FbituKA2bjGO3XefsQ1oCnsHM8E=/135x55:401x216/215x130/s.glbimg.com/jo/g1/f/original/2015/10/20/demi-lovato-cantando.jpg"
                data-original-image="s2.glbimg.com/FbituKA2bjGO3XefsQ1oCnsHM8E=/135x55:401x216/215x130/s.glbimg.com/jo/g1/f/original/2015/10/20/demi-lovato-cantando.jpg" data-url-smart_horizontal="dCDbzC1vCeRcoehVnWZxFYMtoV0=/90x56/smart/filters:strip_icc()/" data-url-smart="dCDbzC1vCeRcoehVnWZxFYMtoV0=/90x56/smart/filters:strip_icc()/" data-url-feature="dCDbzC1vCeRcoehVnWZxFYMtoV0=/90x56/smart/filters:strip_icc()/" data-url-tablet="zY9dqsxu_A3czYTkhhR7DQr1YLE=/120x80/smart/filters:strip_icc()/" data-url-desktop="ozGzE3J9tA2jLyAGxJcXWNXzGks=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Demi Lovato elogia fÃ£s, diz que adora o paÃ­s, mas avisa:  agora sÃ³ deve voltar em 2017</div></div></a></li></ul><div class="top-container analytics-area analytics-id-L mobile-grid-full"><h3><a href="http://musica.com.br/">
            TOP HITS
        </a></h3><ul><li><a class="foto" href="http://musica.com.br/artistas/luan-santana/m/escreve-ai/letra.html"><div class="area-foto"><div class="number">1</div><img width="45" height="45" src="http://s2.glbimg.com/DejQr6wzwaLjEToKO0IBZYMpp9E=/filters:quality(10):strip_icc()/s2.glbimg.com/gffJ6jy3x9TdEFawxnsku-K_Igs=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/12/20/artista-65018-2013151926.jpg"
                data-original-image="s2.glbimg.com/gffJ6jy3x9TdEFawxnsku-K_Igs=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/12/20/artista-65018-2013151926.jpg" data-url-smart_horizontal="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-smart="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-feature="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-tablet="jBcm2gM8Cbg_Vkcg-0n1BAJmuKE=/60x60/smart/filters:strip_icc()/" data-url-desktop="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Luan Santana</div><div class="title">Escreve AÃ­</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li><li><a class="foto" href="http://musica.com.br/artistas/mc-anitta/m/no-meu-talento-part-mc-guime/letra.html"><div class="area-foto"><div class="number">2</div><img width="45" height="45" src="http://s2.glbimg.com/yHnJPHom5FPA3tk69JCcIphdNJE=/filters:quality(10):strip_icc()/s2.glbimg.com/i6tbAg-HNpqidGWWFJqVJ788g6o=/65x65/smart/s.glbimg.com/po/ms/f/original/2014/03/28/anitta-nova-cut.jpg"
                data-original-image="s2.glbimg.com/i6tbAg-HNpqidGWWFJqVJ788g6o=/65x65/smart/s.glbimg.com/po/ms/f/original/2014/03/28/anitta-nova-cut.jpg" data-url-smart_horizontal="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-smart="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-feature="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-tablet="AODuTqXeNCV61xTQZim-SUSZDBg=/60x60/smart/filters:strip_icc()/" data-url-desktop="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Anitta</div><div class="title">No Meu Talento (part. MC GuimÃª)</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li><li class="last-top"><a class="foto" href="http://musica.com.br/artistas/jorge-e-mateus/m/nocaute/letra.html"><div class="area-foto"><div class="number">3</div><img width="45" height="45" src="http://s2.glbimg.com/jdC6lPjKcSpWF8v5jzFy0uuol6c=/filters:quality(10):strip_icc()/s2.glbimg.com/JgDLLARwYvvuCkqHPgmPue-pxa8=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/06/26/jorge.jpg"
                data-original-image="s2.glbimg.com/JgDLLARwYvvuCkqHPgmPue-pxa8=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/06/26/jorge.jpg" data-url-smart_horizontal="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-smart="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-feature="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-tablet="EfTWuRDHHuk_LklhIijQiqahj3A=/60x60/smart/filters:strip_icc()/" data-url-desktop="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Jorge e Mateus</div><div class="title">Nocaute</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li></ul></div><div class="separator"></div><div class="widget analytics-area analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-musica-triplo']);</script></section><section class="area topglobocom"><div class="container area"><div class="separator"></div><div id="topglobocom3" class="clearfix destaque topglobocom3"><div class="topo"><h3 class="titulo">
            top <strong>globo</strong><span class="subtitulo">tÃ¡ todo mundo clicando...</span></h3></div><div id="mais-vistas" class="clearfix mais-vistas tab-content analytics-area analytics-id-R"><div class="slider"><ol class="noticias analytics-area analytics-id-J"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/sao-paulo/noticia/2015/10/apos-explosao-de-caixas-eletronicos-pessoas-pegam-dinheiro-do-chao.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">ApÃ³s explosÃ£o, pedestres levam dinheiro de caixa explodido<br /></span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/pernambuco/noticia/2015/10/pf-apreende-r-85-milhoes-e-prende-gerente-de-empresa-no-recife.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">OperaÃ§Ã£o da PF apreende R$ 85 milhÃµes em empresa de Pernambuco</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://blogs.oglobo.globo.com/pagenotfound/post/botas-masculinas-em-foto-no-snapchat-denunciam-traicao-de-casada.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Mulher envia fotos sexy ao marido e botas masculinas denunciam traiÃ§Ã£o; amplie</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/espirito-santo/noticia/2015/10/suspeitos-confessam-morte-no-es-e-pedem-perdao-mae-de-vitima.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">MÃ£e de morto no ES ouve pedido de perdÃ£o de suspeitos e os abraÃ§a; vÃ­deo</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/distrito-federal/noticia/2015/10/tj-manda-plano-pagar-r-5-mil-por-negar-correcao-craniana-bebe-do-df.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Plano indenizarÃ¡ mÃ£e apÃ³s nÃ£o tratar assimetria que causa &#39;cabeÃ§a torta&#39;</span></span></a></div></div></li></ol><ol class="esportes analytics-area analytics-id-E"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/futebol/selecao-brasileira/noticia/2015/10/neymar-volta-e-cassio-e-novidade-da-selecao-para-pegar-argentina-e-peru.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">CÃ¡ssio Ã© novidade contra Argentina e Peru, e Neymar reaparece; confira lista</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/rj/futebol/copa-do-brasil/jogo/21-10-2015/fluminense-palmeiras/" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Flu vence o Palmeiras no MaracanÃ£ e vai com vantagem para SÃ£o Paulo</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/sp/santos-e-regiao/futebol/copa-do-brasil/jogo/21-10-2015/sao-paulo-santos/" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Peixe vence o SÃ£o Paulo por 3x1 e fica perto da final; confira</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/futebol/times/flamengo/noticia/2015/10/radar-do-fla-tem-diego-souza-dede-arao-anderson-martins-e-pikachu.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Fla mira Diego Souza, DedÃ©, Anderson Martins, ArÃ£o e Pikachu</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/futebol/times/sao-paulo/noticia/2015/10/pato-muda-discurso-confirma-papo-com-andres-e-diz-quero-ser-feliz.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Pato muda discurso, confirma papo com AndrÃ©s e diz: &#39;Quero ser feliz&#39;</span></span></a></div></div></li></ol><ol class="entretenimento analytics-area analytics-id-M"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/famosos/noticia/2015/10/michelly-crisfepe-faz-brincadeira-e-mostra-bracos-musculosos.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Ex-BBB Michelly assusta os fÃ£s ao posar com braÃ§os musculosos do marido</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://kogut.oglobo.globo.com/noticias-da-tv/noticia/2015/10/louise-dtuani-fala-de-desafio-no-caldeirao-e-de-casamento-com-humorista-queremos-filhos.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Louise D&#39;Tuani conta que passou sufoco ao ficar longe do marido apÃ³s casamento</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://vogue.globo.com/moda/gente/noticia/2015/10/kelly-rohrbach-namorada-de-leonardo-di-caprio-posa-sexy.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Namorada de Di Caprio, a nova Cameron Diaz, exibe pernÃ£o com saia curtinha</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://gshow.globo.com/novelas/alem-do-tempo/vem-por-ai/noticia/2015/10/nova-fase-pedro-e-noivo-de-livia-e-planeja-casamento-so-minha.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Em nova fase de &#39;AlÃ©m do Tempo&#39;, Pedro jÃ¡ planeja uniÃ£o com LÃ­via</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://gshow.globo.com/tv/noticia/2015/10/quer-rever-alem-do-tempo-confira-o-capitulo-aberto-e-completo-que-encerrou-1-fase.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Confira o capÃ­tulo, aberto e completo, que encerrou a 1Âª fase de &#39;AlÃ©m do Tempo&#39;</span></span></a></div></div></li></ol></div></div><div class="controls-area"><ul class="step-marker"><li class=" glb-hl-style-noticias"><div class=""><a href="#" data-related=""><div class="ball active"></div></a></div></li><li class=" glb-hl-style-esporte"><div class=""><a href="#" data-related=""><div class="ball"></div></a></div></li><li class=" glb-hl-style-entretenimento"><div class=""><a href="#" data-related=""><div class="ball "></div></a></div></li></ul></div></div><section class="top-videos analytics-area analytics-id-R"><h1><a href="http://globotv.globo.com/">globo.tv</a></h1><div class="separator-line"></div><ul class="destaque analytics-area analytics-id-V"><li ><a class="foto" href="#"><span class="borda-foto has-video"><span></span><img width="190" height="110"></span><span class="number">1</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto has-video"><span></span><img width="190" height="110"></span><span class="number">2</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto has-video"><span></span><img width="190" height="110"></span><span class="number">3</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto has-video"><span></span><img width="190" height="110"></span><span class="number">4</span><span class="conteudo"><p>descr</p></span></a></li><li class="ult"><a class="foto" href="#"><span class="borda-foto has-video"><span></span><img width="190" height="110"></span><span class="number">5</span><span class="conteudo"><p>descr</p></span></a></li></ul></section></div></section><section class="area globo-servicos container analytics-area analytics-id-C"><h2 class="titulo">serviÃ§os</h2><div class="borda"><span class="rodape"></span></div><div class="content"><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-loterias
" 
    data-lotteries-url="http://api.globo.com/lotteries/megasena.jsonp"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-fipe
"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-zap
" 
    data-zap-url="http://api.globo.com/zap/search/"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"></div></div></section><section class="area widgets_inferior franja-inferior container analytics-area analytics-id-C"><div class="box-links-servicos"><div class="links"><p>MAIS SERVIÃOS</p><a href="http://revistapegn.globo.com/Franquias/compare/">Abra uma franquia</a><a href="http://globoesporte.globo.com/eu-atleta/calculo-imc.html">AvaliaÃ§Ã£o fÃ­sica</a><a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">Conversor de moedas</a><a href="http://www.techtudo.com.br/softwares/">Downloads</a><a href="http://g1.globo.com/previsao-do-tempo/">PrevisÃ£o do tempo</a><a href="http://g1.globo.com/radar-g1/platb/">TrÃ¢nsito</a></div></div><div id="glb-shopping-widget" class="area globoshopping container" data-url="http://vitrines.globo.com/vitrine/vitrine.min.js" data-sitepage="globocom/home" data-responsive="0" data-branding-color="0669DE"></div></section><section class="area diretorio"><div class="container analytics-area analytics-id-Z"><div class="diretorio-responsive"><a href="/todos-os-sites.html">veja todos os sites da <strong>globo.com</strong> âº</a></div><div id="glb-diretorio"><div class="glb-conteudo"><div class="glb-bloco"><div class="glb-grid-12"><ul class="diretorio-indice"><li class="diretorio-linha diretorio-first "><ul><li class="diretorio-first-level analytics-product glb-menu-g1 diretorio-quebra "><a title="g1" href="http://g1.globo.com/">g1</a><ul><li class="diretorio-second-level"><a title="Acre" href="http://g1.globo.com/ac/acre/">Acre</a></li><li class="diretorio-second-level"><a title="Alagoas" href="http://g1.globo.com/al/alagoas/">Alagoas</a></li><li class="diretorio-second-level"><a title="Amazonas" href="http://g1.globo.com/am/amazonas/">Amazonas</a></li><li class="diretorio-second-level"><a title="Bahia" href="http://g1.globo.com/bahia/?noAudience=true">Bahia</a></li><li class="diretorio-second-level"><a title="Bem Estar" href="http://g1.globo.com/bemestar/">Bem Estar</a></li><li class="diretorio-second-level"><a title="Bom Dia Brasil" href="http://g1.globo.com/bom-dia-brasil/">Bom Dia Brasil</a></li><li class="diretorio-second-level"><a title="Brasil" href="http://g1.globo.com/brasil/">Brasil</a></li><li class="diretorio-second-level"><a title="Carnaval" href="http://g1.globo.com/carnaval/2013/">Carnaval</a></li><li class="diretorio-second-level"><a title="Carros" href="http://g1.globo.com/carros/">Carros</a></li><li class="diretorio-second-level"><a title="CearÃ¡" href="http://g1.globo.com/ceara/">CearÃ¡</a></li><li class="diretorio-second-level"><a title="CiÃªncia e SaÃºde" href="http://g1.globo.com/ciencia-e-saude/">CiÃªncia e SaÃºde</a></li><li class="diretorio-second-level"><a title="Como SerÃ¡?" href="http://redeglobo.globo.com/como-sera/index.html">Como SerÃ¡?</a></li><li class="diretorio-second-level"><a title="Concursos e Emprego" href="http://g1.globo.com/concursos-e-emprego/">Concursos e Emprego</a></li><li class="diretorio-second-level"><a title="Distrito Federal" href="http://g1.globo.com/distrito-federal/">Distrito Federal</a></li><li class="diretorio-second-level"><a title="Economia" href="http://g1.globo.com/economia/">Economia</a></li><li class="diretorio-second-level"><a title="EducaÃ§Ã£o" href="http://www.educacao.globo.com/">EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="EspÃ­rito Santo" href="http://g1.globo.com/espirito-santo/">EspÃ­rito Santo</a></li><li class="diretorio-second-level"><a title="FantÃ¡stico" href="http://fantastico.globo.com/">FantÃ¡stico</a></li><li class="diretorio-second-level"><a title="G1 EducaÃ§Ã£o" href="http://g1.globo.com/educacao/">G1 EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Globo Natureza" href="http://g1.globo.com/natureza/">Globo Natureza</a></li><li class="diretorio-second-level"><a title="Globo News" href="http://globonews.globo.com/">Globo News</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://g1.globo.com/economia/agronegocios/">Globo Rural</a></li><li class="diretorio-second-level"><a title="Globo RepÃ³rter" href="http://g1.globo.com/globo-reporter/">Globo RepÃ³rter</a></li><li class="diretorio-second-level"><a title="GoiÃ¡s" href="http://g1.globo.com/goias/">GoiÃ¡s</a></li><li class="diretorio-second-level"><a title="Jornal da Globo" href="http://g1.globo.com/jornal-da-globo/">Jornal da Globo</a></li><li class="diretorio-second-level"><a title="Jornal Hoje" href="http://g1.globo.com/jornal-hoje/">Jornal Hoje</a></li><li class="diretorio-second-level"><a title="Jornal Nacional" href="http://g1.globo.com/jornal-nacional/">Jornal Nacional</a></li><li class="diretorio-second-level"><a title="MaranhÃ£o" href="http://g1.globo.com/ma/maranhao/">MaranhÃ£o</a></li><li class="diretorio-second-level"><a title="Mato Grosso" href="http://g1.globo.com/mato-grosso/">Mato Grosso</a></li><li class="diretorio-second-level"><a title="Mato Grosso do Sul" href="http://g1.globo.com/mato-grosso-do-sul/">Mato Grosso do Sul</a></li><li class="diretorio-second-level"><a title="Minas Gerais" href="http://g1.globo.com/minas-gerais/">Minas Gerais</a></li><li class="diretorio-second-level"><a title="Mundo" href="http://g1.globo.com/mundo/">Mundo</a></li><li class="diretorio-second-level"><a title="ParÃ¡" href="http://g1.globo.com/pa/para/">ParÃ¡</a></li><li class="diretorio-second-level"><a title="ParaÃ­ba" href="http://g1.globo.com/paraiba/">ParaÃ­ba</a></li><li class="diretorio-second-level"><a title="ParanÃ¡" href="http://g1.globo.com/parana/">ParanÃ¡</a></li><li class="diretorio-second-level"><a title="Pernambuco" href="http://g1.globo.com/pernambuco/">Pernambuco</a></li><li class="diretorio-second-level"><a title="PiauÃ­" href="http://g1.globo.com/pi/piaui/">PiauÃ­</a></li><li class="diretorio-second-level"><a title="Planeta Bizarro" href="http://g1.globo.com/planeta-bizarro/">Planeta Bizarro</a></li><li class="diretorio-second-level"><a title="PolÃ­tica" href="http://g1.globo.com/politica/">PolÃ­tica</a></li><li class="diretorio-second-level"><a title="Pop &amp; Arte" href="http://g1.globo.com/pop-arte/">Pop &amp; Arte</a></li><li class="diretorio-second-level"><a title="PrevisÃ£o do Tempo" href="http://g1.globo.com/previsao-do-tempo.html">PrevisÃ£o do Tempo</a></li><li class="diretorio-second-level"><a title="ProfissÃ£o RepÃ³rter" href="http://g1.globo.com/profissao-reporter/">ProfissÃ£o RepÃ³rter</a></li><li class="diretorio-second-level"><a title="Rio de Janeiro" href="http://g1.globo.com/rio-de-janeiro/">Rio de Janeiro</a></li><li class="diretorio-second-level"><a title="Rio Grande do Norte" href="http://g1.globo.com/rn/rio-grande-do-norte/">Rio Grande do Norte</a></li><li class="diretorio-second-level"><a title="Rio Grande do Sul" href="http://g1.globo.com/rs/rio-grande-do-sul/">Rio Grande do Sul</a></li><li class="diretorio-second-level"><a title="RondÃ´nia" href="http://g1.globo.com/ro/rondonia/">RondÃ´nia</a></li><li class="diretorio-second-level"><a title="Santa Catarina" href="http://g1.globo.com/sc/santa-catarina/">Santa Catarina</a></li><li class="diretorio-second-level"><a title="SÃ£o Paulo" href="http://g1.globo.com/sao-paulo/">SÃ£o Paulo</a></li><li class="diretorio-second-level"><a title="Sergipe" href="http://g1.globo.com/se/sergipe/">Sergipe</a></li><li class="diretorio-second-level"><a title="Serra, Lagos e Norte" href="http://g1.globo.com/rj/serra-lagos-norte/">Serra, Lagos e Norte</a></li><li class="diretorio-second-level"><a title="Tecnologia e Games" href="http://g1.globo.com/tecnologia/">Tecnologia e Games</a></li><li class="diretorio-second-level"><a title="Turismo e Viagem" href="http://g1.globo.com/turismo-e-viagem/">Turismo e Viagem</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-globoesporte diretorio-quebra "><a title="globoesporte" href="http://globoesporte.globo.com/">globoesporte</a><ul><li class="diretorio-second-level"><a title="Atletismo" href="http://globoesporte.globo.com/atletismo/">Atletismo</a></li><li class="diretorio-second-level"><a title="Basquete" href="http://globoesporte.globo.com/basquete/">Basquete</a></li><li class="diretorio-second-level"><a title="Basquete/NBB" href="http://globoesporte.globo.com/basquete/nbb/">Basquete/NBB</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie A" href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">BrasileirÃ£o SÃ©rie A</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie B" href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">BrasileirÃ£o SÃ©rie B</a></li><li class="diretorio-second-level"><a title="Campeonato Carioca" href="http://globoesporte.globo.com/rj/futebol/campeonato-carioca/index.html">Campeonato Carioca</a></li><li class="diretorio-second-level"><a title="Campeonato GaÃºcho" href="http://globoesporte.globo.com/rs/futebol/campeonato-gaucho/index.html">Campeonato GaÃºcho</a></li><li class="diretorio-second-level"><a title="Campeonato Mineiro" href="http://globoesporte.globo.com/mg/futebol/campeonato-mineiro/index.html">Campeonato Mineiro</a></li><li class="diretorio-second-level"><a title="Campeonato Paulista" href="http://globoesporte.globo.com/sp/futebol/campeonato-paulista/index.html">Campeonato Paulista</a></li><li class="diretorio-second-level"><a title="Cartola FC" href="http://globoesporte.globo.com/cartola-fc/">Cartola FC</a></li><li class="diretorio-second-level"><a title="Copa do Mundo da Fifa" href="http://globoesporte.globo.com/futebol/copa-do-mundo/index.html">Copa do Mundo da Fifa</a></li><li class="diretorio-second-level"><a title="Copa AmÃ©rica" href="http://globoesporte.globo.com/futebol/copa-america/">Copa AmÃ©rica</a></li><li class="diretorio-second-level"><a title="Copa Sul-Americana" href="http://globoesporte.globo.com/futebol/copa-sul-americana/index.html">Copa Sul-Americana</a></li><li class="diretorio-second-level"><a title="Desafio de Futsal" href="http://globoesporte.globo.com/eventos/desafio-de-futsal/">Desafio de Futsal</a></li><li class="diretorio-second-level"><a title="Esporte Espetacular" href="http://globoesporte.globo.com/programas/esporte-espetacular/">Esporte Espetacular</a></li><li class="diretorio-second-level"><a title="Esportes AquÃ¡ticos" href="http://globoesporte.globo.com/aquaticos/">Esportes AquÃ¡ticos</a></li><li class="diretorio-second-level"><a title="Esportes Radicais" href="http://globoesporte.globo.com/radicais/">Esportes Radicais</a></li><li class="diretorio-second-level"><a title="Eu Atleta" href="http://globoesporte.globo.com/eu-atleta/">Eu Atleta</a></li><li class="diretorio-second-level"><a title="FÃ³rmula 1" href="http://globoesporte.globo.com/motor/formula-1/">FÃ³rmula 1</a></li><li class="diretorio-second-level"><a title="Futebol" href="http://globoesporte.globo.com/futebol/">Futebol</a></li><li class="diretorio-second-level"><a title="Futebol Internacional" href="http://globoesporte.globo.com/futebol/futebol-internacional/">Futebol Internacional</a></li><li class="diretorio-second-level"><a title="FutpÃ©dia" href="http://futpedia.globo.com/">FutpÃ©dia</a></li><li class="diretorio-second-level"><a title="Game Futebol" href="http://gamefutebol.globoesporte.globo.com/">Game Futebol</a></li><li class="diretorio-second-level"><a title="Liga dos CampeÃµes" href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">Liga dos CampeÃµes</a></li><li class="diretorio-second-level"><a title="OlimpÃ­adas" href="http://globoesporte.globo.com/olimpiadas/">OlimpÃ­adas</a></li><li class="diretorio-second-level"><a title="SeleÃ§Ã£o Brasileira" href="http://globoesporte.globo.com/futebol/selecao-brasileira/">SeleÃ§Ã£o Brasileira</a></li><li class="diretorio-second-level"><a title="StockCar" href="http://globoesporte.globo.com/motor/stock-car/">StockCar</a></li><li class="diretorio-second-level"><a title="Surfe" href="http://globoesporte.globo.com/radicais/surfe/">Surfe</a></li><li class="diretorio-second-level"><a title="TÃªnis" href="http://globoesporte.globo.com/tenis/">TÃªnis</a></li><li class="diretorio-second-level"><a title="Vai-e-vem do Mercado" href="http://globoesporte.globo.com/futebol/vai-e-vem-do-mercado/">Vai-e-vem do Mercado</a></li><li class="diretorio-second-level"><a title="VerÃ£o Espetacular" href="http://globoesporte.globo.com/programas/verao-espetacular/">VerÃ£o Espetacular</a></li><li class="diretorio-second-level"><a title="VÃ´lei" href="http://globoesporte.globo.com/volei/">VÃ´lei</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-gshow diretorio-quebra "><a title="gshow" href="http://gshow.globo.com/">gshow</a><ul><li class="diretorio-second-level"><a title="Amorteamo" href="http://gshow.globo.com/programas/amorteamo/">Amorteamo</a></li><li class="diretorio-second-level"><a title="A Teia" href="http://gshow.globo.com/programas/a-teia/">A Teia</a></li><li class="diretorio-second-level"><a title="Altas Horas" href="http://altashoras.globo.com/">Altas Horas</a></li><li class="diretorio-second-level"><a title="Alto Astral" href="http://gshow.globo.com/novelas/alto-astral/index.html">Alto Astral</a></li><li class="diretorio-second-level"><a title="Amor &amp; Sexo" href="http://amoresexo.globo.com/">Amor &amp; Sexo</a></li><li class="diretorio-second-level"><a title="BabilÃ´nia" href="http://gshow.globo.com/novelas/babilonia/index.html">BabilÃ´nia</a></li><li class="diretorio-second-level"><a title="BBB" href="http://bbb.globo.com/">BBB</a></li><li class="diretorio-second-level"><a title="Boogie Oogie" href="http://gshow.globo.com/novelas/boogie-oogie/index.html">Boogie Oogie</a></li><li class="diretorio-second-level"><a title="CaldeirÃ£o do Huck" href="http://caldeiraodohuck.globo.com/">CaldeirÃ£o do Huck</a></li><li class="diretorio-second-level"><a title="Cobras &amp; Lagartos" href="http://gshow.globo.com/novelas/cobras-e-lagartos/videos">Cobras &amp; Lagartos</a></li><li class="diretorio-second-level"><a title="DomingÃ£o do FaustÃ£o" href="http://domingaodofaustao.globo.com/">DomingÃ£o do FaustÃ£o</a></li><li class="diretorio-second-level"><a title="Encontro com FÃ¡tima Bernardes" href="http://tvg.globo.com/programas/encontro-com-fatima-bernardes/">Encontro com FÃ¡tima Bernardes</a></li><li class="diretorio-second-level"><a title="Esquenta!" href="http://tvg.globo.com/programas/esquenta/">Esquenta!</a></li><li class="diretorio-second-level"><a title="Estrelas" href="http://estrelas.globo.com/">Estrelas</a></li><li class="diretorio-second-level"><a title="ImpÃ©rio" href="http://gshow.globo.com/novelas/imperio/index.html">ImpÃ©rio</a></li><li class="diretorio-second-level"><a title="LigaÃ§Ãµes Perigosas" href="http://gshow.globo.com/series/ligacoes-perigosas/">LigaÃ§Ãµes Perigosas</a></li><li class="diretorio-second-level"><a title="Mais VocÃª" href="http://maisvoce.globo.com/">Mais VocÃª</a></li><li class="diretorio-second-level"><a title="MalhaÃ§Ã£o" href="http://malhacao.globo.com/">MalhaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Na Moral" href="http://gshow.globo.com/programas/na-moral/">Na Moral</a></li><li class="diretorio-second-level"><a title="O Rei do Gado" href="http://gshow.globo.com/novelas/o-rei-do-gado/videos/">O Rei do Gado</a></li><li class="diretorio-second-level"><a title="PÃ© na Cova" href="http://gshow.globo.com/series/pe-na-cova/2015/">PÃ© na Cova</a></li><li class="diretorio-second-level"><a title="Programa do JÃ´" href="http://programadojo.globo.com/">Programa do JÃ´</a></li><li class="diretorio-second-level"><a title="Receitas.com" href="http://www.receitas.com/">Receitas.com</a></li><li class="diretorio-second-level"><a title="Receitas Mais VocÃª" href="http://www.receitas.com/maisvoce/">Receitas Mais VocÃª</a></li><li class="diretorio-second-level"><a title="Sete Vidas" href="http://gshow.globo.com/novelas/sete-vidas/index.html">Sete Vidas</a></li><li class="diretorio-second-level"><a title="SuperStar" href="http://gshow.globo.com/programas/superstar/index.html">SuperStar</a></li><li class="diretorio-second-level"><a title="TÃ¡ no Ar" href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/index.html">TÃ¡ no Ar</a></li><li class="diretorio-second-level"><a title="Tapas &amp; Beijos" href="http://tapasebeijos.globo.com/">Tapas &amp; Beijos</a></li><li class="diretorio-second-level"><a title="The Voice Brasil" href="http://gshow.globo.com/realities/the-voice-brasil/">The Voice Brasil</a></li><li class="diretorio-second-level"><a title="Sexo e as Negas" href="http://gshow.globo.com/programas/sexo-e-as-negas/index.html">Sexo e as Negas</a></li><li class="diretorio-second-level"><a title="The Voice Kids" href="http://gshow.globo.com/programas/the-voice-kids/">The Voice Kids</a></li><li class="diretorio-second-level"><a title="Totalmente Demais" href="http://gshow.globo.com/novelas/totalmente-demais/">Totalmente Demais</a></li><li class="diretorio-second-level"><a title="Verdades Secretas" href="http://gshow.globo.com/novelas/verdades-secretas/index.html">Verdades Secretas</a></li><li class="diretorio-second-level"><a title="Video Show" href="http://videoshow.globo.com/">Video Show</a></li><li class="diretorio-second-level"><a title="WebsÃ©ries " href="http://gshow.globo.com/programas/webseries/videos/">WebsÃ©ries </a></li><li class="diretorio-second-level"><a title="ZORRA" href="http://gshow.globo.com/programas/zorra/">ZORRA</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-famosos-etc diretorio-quebra "><a title="famosos &amp; etc" href="http://famosos.globo.com/">famosos &amp; etc</a><ul><li class="diretorio-second-level"><a title="EGO" href="http://ego.globo.com/">EGO</a></li><li class="diretorio-second-level"><a title="G1 Cinema" href="http://g1.globo.com/pop-arte/cinema/">G1 Cinema</a></li><li class="diretorio-second-level"><a title="G1 MÃºsica" href="http://g1.globo.com/pop-arte/musica/">G1 MÃºsica</a></li><li class="diretorio-second-level"><a title="Globo Jogos" href="http://jogos.globo.com/">Globo Jogos</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="HorÃ³scopo" href="http://horoscopo.ego.globo.com/ne/home.html">HorÃ³scopo</a></li><li class="diretorio-second-level"><a title="Meus 5 minutos" href="http://www.meus5minutos.com.br/">Meus 5 minutos</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="MÃºsica" href="http://musica.com.br/">MÃºsica</a></li><li class="diretorio-second-level"><a title="Namoro na Web" href="http://tracking.parperfeito.com.br/ppbanner/bannerTracker?originId=13&amp;identifierId=41763&amp;actionId=1">Namoro na Web</a></li><li class="diretorio-second-level"><a title="Paparazzo" href="http://paparazzo.globo.com/">Paparazzo</a></li><li class="diretorio-second-level"><a title="Patricia Kogut" href="http://oglobo.globo.com/cultura/kogut/">Patricia Kogut</a></li><li class="diretorio-second-level"><a title="Retratos da Vida" href="http://extra.globo.com/lazer/retratosdavida/">Retratos da Vida</a></li><li class="diretorio-second-level"><a title="Senninha" href="http://senninha.globo.com/">Senninha</a></li><li class="diretorio-second-level"><a title="Teatro" href="http://www.agentesevenoteatro.com.br/">Teatro</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-grupo-globo diretorio-quebra "><a title="Grupo Globo" href="http://grupoglobo.globo.com/">Grupo Globo</a><ul><li class="diretorio-second-level"><a title="princÃ­pios editoriais" href="http://g1.globo.com/principios-editoriais-do-grupo-globo.html">princÃ­pios editoriais</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-editora-globo diretorio-sem-quebra "><a title="editora globo" href="http://editoraglobo.globo.com/">editora globo</a><ul><li class="diretorio-second-level"><a title="Auto Esporte" href="http://revistaautoesporte.globo.com/">Auto Esporte</a></li><li class="diretorio-second-level"><a title="Casa e Comida" href="http://revistacasaejardim.globo.com/Casa-e-Comida/">Casa e Comida</a></li><li class="diretorio-second-level"><a title="Casa e Jardim" href="http://revistacasaejardim.globo.com/">Casa e Jardim</a></li><li class="diretorio-second-level"><a title="Casa Vogue" href="http://casavogue.globo.com/">Casa Vogue</a></li><li class="diretorio-second-level"><a title="Crescer" href="http://revistacrescer.globo.com/">Crescer</a></li><li class="diretorio-second-level"><a title="Ãpoca" href="http://revistaepoca.globo.com/">Ãpoca</a></li><li class="diretorio-second-level"><a title="Ãpoca NegÃ³cios" href="http://epocanegocios.globo.com/">Ãpoca NegÃ³cios</a></li><li class="diretorio-second-level"><a title="Galileu" href="http://revistagalileu.globo.com/">Galileu</a></li><li class="diretorio-second-level"><a title="Glamour" href="http://revistaglamour.globo.com/">Glamour</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://revistagloborural.globo.com/">Globo Rural</a></li><li class="diretorio-second-level"><a title="GQ Brasil" href="http://gq.globo.com/">GQ Brasil</a></li><li class="diretorio-second-level"><a title="Marie Claire" href="http://revistamarieclaire.globo.com/">Marie Claire</a></li><li class="diretorio-second-level"><a title="Meus 5 Minutos" href="http://meus5minutos.globo.com/">Meus 5 Minutos</a></li><li class="diretorio-second-level"><a title="Monet" href="http://revistamonet.globo.com/">Monet</a></li><li class="diretorio-second-level"><a title="Mundo do SÃ­tio" href="http://mundodositio.globo.com/">Mundo do SÃ­tio</a></li><li class="diretorio-second-level"><a title="PEGN" href="http://revistapegn.globo.com/">PEGN</a></li><li class="diretorio-second-level"><a title="Quem" href="http://revistaquem.globo.com/">Quem</a></li><li class="diretorio-second-level"><a title="Vogue" href="http://vogue.globo.com/">Vogue</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-infoglobo diretorio-sem-quebra "><a title="infoglobo" href="http://infoglobo.com.br/Anuncie/Home.aspx">infoglobo</a><ul><li class="diretorio-second-level"><a title="Ela Digital" href="http://ela.oglobo.globo.com/">Ela Digital</a></li><li class="diretorio-second-level"><a title="Extra" href="http://extra.globo.com/">Extra</a></li><li class="diretorio-second-level"><a title="O Globo" href="http://oglobo.globo.com/">O Globo</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-last "><ul><li class="diretorio-first-level analytics-product glb-menu-globosat diretorio-quebra "><a title="globosat" href="http://canaisglobosat.globo.com/">globosat</a><ul><li class="diretorio-second-level"><a title="Globosat Play" href="http://globosatplay.globo.com/">Globosat Play</a></li><li class="diretorio-second-level"><a title="+Globosat" href="http://maisglobosat.globo.com/">+Globosat</a></li><li class="diretorio-second-level"><a title="BIS" href="http://canalbis.globo.com/">BIS</a></li><li class="diretorio-second-level"><a title="Canal Brasil" href="http://canalbrasil.globo.com/">Canal Brasil</a></li><li class="diretorio-second-level"><a title="Canal Futura" href="http://www.futura.org.br/">Canal Futura</a></li><li class="diretorio-second-level"><a title="Combate" href="http://combate.globo.com/">Combate</a></li><li class="diretorio-second-level"><a title="GloboNews" href="http://globonews.globo.com/">GloboNews</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="Megapix" href="http://megapix.globo.com/">Megapix</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="Off" href="http://canaloff.globo.com/">Off</a></li><li class="diretorio-second-level"><a title="Premiere" href="http://sociopremiere.globo.com/">Premiere</a></li><li class="diretorio-second-level"><a title="SporTV" href="http://sportv.globo.com/">SporTV</a></li><li class="diretorio-second-level"><a title="Studio Universal" href="http://studiouniversal.globo.com/">Studio Universal</a></li><li class="diretorio-second-level"><a title="Syfy" href="http://syfy.globo.com/">Syfy</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Universal Channel" href="http://uc.globo.com/">Universal Channel</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-sistema-globo-de-radio diretorio-sem-quebra "><a title="sistema globo de rÃ¡dio" href="http://globoradio.globo.com/home/HOME.htm">sistema globo de rÃ¡dio</a><ul><li class="diretorio-second-level"><a title="BHFM" href="http://bhfm.globoradio.globo.com/home/HOME.htm">BHFM</a></li><li class="diretorio-second-level"><a title="CBN" href="http://cbn.globoradio.globo.com/home/HOME.htm">CBN</a></li><li class="diretorio-second-level"><a title="RADIOBEAT" href="http://radiobeat.com.br/">RADIOBEAT</a></li><li class="diretorio-second-level"><a title="RÃ¡dio Globo" href="http://radioglobo.globoradio.globo.com/home/HOME.htm">RÃ¡dio Globo</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-globo-filmes diretorio-sem-quebra "><a title="globo filmes" href="http://globofilmes.globo.com/">globo filmes</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-som-livre diretorio-sem-quebra "><a title="som livre" href="http://www.somlivre.com/">som livre</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-blog-globocom diretorio-sem-quebra "><a title="blog globo.com" href="http://blog.globo.com/1.html">blog globo.com</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-zap-imoveis diretorio-sem-quebra "><a title="zap imÃ³veis" href="http://www.zapimoveis.com.br/">zap imÃ³veis</a></li></ul></li></ul></div></div></div></div></div></section><div id="x61" class="opec-area opec-mobile opec-x61 grid-12"><div id="banner_mobile_fim" class="tag-manager-publicidade-container"></div></div><footer><div class="container analytics-area analytics-id-B"><a href="http://www.globo.com/" title="Globo.com">Globo.com</a><span class="copyright">&copy; Copyright 2000-2015 Globo ComunicaÃ§Ã£o e ParticipaÃ§Ãµes S.A.</span><span class="copyright-480 hidden"><p>&copy; 2000-2015.</p><p>Todos os direitos reservados.</p></span><ul class="footer-menu"><li><a href="/privacidade.html">polÃ­tica de privacidade</a></li><li><a href="/todos-os-sites.html">todos os sites</a></li><li><a href="http://anuncie.globo.com/">anuncie conosco</a></li><li><a href="http://meuperfil.globo.com">central globo.com</a></li></ul></div></footer></div><!--[if lt IE 9]><script src="http://s.glbimg.com/en/ho/static/libby/css3pie/js/PIE.js" type="text/javascript"></script><![endif]--><script>
responsiveHub.updateImages();</script><script>
(function(){var scr=document.createElement('script');scr.type = 'text/javascript';scr.async=true;scr.src = 'http://s.glbimg.com/en/ho/static/CACHE/js/956a6c2ed351.js';document.getElementsByTagName('head')[0].appendChild(scr);}());</script><div id="opec-frame1"><div id="banner_floating" class="tag-manager-publicidade-container"></div></div><script>if(document.body && document.body.offsetWidth <= 711){}else{setTimeout(function(){var bannerMiddleTop = $("#ad-position-middle"),
bannerMiddleBottom = $('#ad-position-middle3');if(bannerMiddleTop.height() < 15){bannerMiddleTop.addClass('without-opec');}if(bannerMiddleBottom.height() < 15){bannerMiddleBottom.addClass('without-opec');}}, 6000);}
</script><script>
window.glb=window.glb || {};window.glb.analyticsConfig=window.glb.analyticsConfig  || {};window.glb.analyticsConfig = {scrollEventChance: 0};</script><script>
document.cookie = 'globoapi.geo.geolocation=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.estado=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.novela=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.time=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';setTimeout(function(){window.glb.barra=window.glb.barra || {};window.glb.barra.component=window.glb.barra.component || {};window.glb.barra.component.barraDiv = {className: ''};}, 1000);</script><style>
#assinante-menu .barra-notifications-area .float-box.config-active .save-area{bottom:0;position:absolute;width:100%}#assinante-menu .barra-notifications-area .float-box.config-active .config-box{padding-bottom:44px}</style><style>
header .container{height:150px}.box-destaque-uber .destaques-primario .titulo{font-family:"ProximaNovaA-Bold"}@media (min-width:1600px){.conteudo-uber .box-destaque-uber{position:static;max-width:1600px;margin:0 auto}}</style><style>
.agrupador-quadruplo-tecnologia .destaque-inferior ul{display:none;// css de folder}</style></body></html>
<!-- PÃ¡gina gerada em 22/10/2015 21:15:12 -->
