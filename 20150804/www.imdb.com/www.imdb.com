



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>

                    
                    
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "715-1970621-4306044";
                var ue_id = "10YAFVMV5H1GHT8BT830";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        
    
        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />
    
    <meta property='fb:app_id' content='115109575169727' />
    
    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="10YAFVMV5H1GHT8BT830" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1d-c3-2xl-i-72cba68f.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-1506377481._CB315509730_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-485555676._CB318510215_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-247421025._CB318510254_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-3978168775._CB318510288_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-1541130848._CB318510255_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
(function(){var d=function(o){return Object.prototype.toString.call(o)==="[object Array]";},g=function(q,p){var o;for(o=0;o<q.length;o++){if(o in q){p.call(null,q[o],o);}}},h=[],k,b,l=false,n=false,f=function(){var o=[],p=[],q={};g(h,function(s){var r="";g(s.dartsite.split("/"),function(t){if(t!==""){if(t in q){}else{q[t]=o.length;o.push(t);}r+="/"+q[t];}});p.push(r);});return{iu_parts:o,enc_prev_ius:p};},c=function(){var o=[];g(h,function(q){var p=[];g(q.sizes,function(r){p.push(r.join("x"));});o.push(p.join("|"));});return o;},m=function(){var o=[];g(h,function(p){o.push(a(p.targeting));});return o.join("|");},a=function(r,o){var q,p,s=[];for(q in r){p=[];for(j=0;j<r[q].length;j++){p.push(encodeURIComponent(r[q][j]));}if(o){s.push(q+"="+encodeURIComponent(p.join(",")));}else{s.push(q+"="+p.join(","));}}return s.join("&");},e=function(){var o=+new Date();if(n){return;}if(!this.readyState||"loaded"===this.readyState){n=true;if(l){imdbads.cmd.push(function(){for(i=0;i<h.length;i++){generic.monitoring.record_metric(h[i].name+".fail",csm.duration(o));}});}}};window.tinygpt={define_slot:function(r,q,o,p){h.push({dartsite:r.replace(/\/$/,""),sizes:q,name:o,targeting:p});},set_targeting:function(o){k=o;},callback:function(q){var r,p={},t,o,s=+new Date();l=false;for(r=0;r<h.length;r++){t=h[r].dartsite;o=h[r].name;if(q[r][t]){p[o]=q[r][t];}else{window.console&&console.error&&console.error("Unable to correlate GPT response for "+o);}}imdbads.cmd.push(function(){for(r=0;r<h.length;r++){ad_utils.slot_events.trigger(h[r].name,"request",{timestamp:b});ad_utils.slot_events.trigger(h[r].name,"tagdeliver",{timestamp:s});}ad_utils.gpt.handle_response(p);});},send:function(){var r=[],q=function(s,t){if(d(t)){t=t.join(",");}if(t){r.push(s+"="+encodeURIComponent(""+t));}},o,p;if(h.length===0){tinygpt.callback({});return;}q("gdfp_req","1");q("correlator",Math.floor(4503599627370496*Math.random()));q("output","json_html");q("callback","tinygpt.callback");q("impl","fifs");q("json_a","1");result=f();q("iu_parts",result.iu_parts);q("enc_prev_ius",result.enc_prev_ius);q("prev_iu_szs",c());q("prev_scp",m());q("cust_params",a(k,true));o=document.createElement("script");p=document.getElementsByTagName("script")[0];o.async=true;o.type="text/javascript";o.src="http://pubads.g.doubleclick.net/gampad/ads?"+r.join("&");o.id="tinygpt";o.onload=o.onerror=o.onreadystatechange=e;l=true;p.parentNode.insertBefore(o,p);b=+new Date();}};})();</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['b'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['1'],
'u': ['697077898729'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-3196534337._CB302695131_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"d52477823d80b134d07ed767f18f4e817ceadb8c",
"2015-08-03T23%3A58%3A32GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 25288;
generic.days_to_midnight = 0.2926851809024811;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-1582251138._CB317085808_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'b']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm === undefined || window.generic === undefined || window.consoleLog === undefined)) {
                    if (console !== undefined && console.log !== undefined) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=697077898729;ord=697077898729?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=697077898729?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=697077898729?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
onsubmit="(new Image()).src='/rg/SEARCH-BOX/HEADER/images/b.gif?link=/find';"
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/imdbpicks/monthly-tv-picks/?ref_=nv_tp_tvpicks_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/search/title?count=100&title_type=feature,tv_series,tv_movie&explore=title_type,genres,year,countries&ref_=nv_ch_mm_1"
>Popular Movies & TV</a></li>
                        <li><a href="/chart/?ref_=nv_ch_cht_2"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_3"
>Oscar Winners</a></li>
                        <li><a href="/chart/top?ref_=nv_ch_250_4"
>Top 250</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_5"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/imdbpicks/monthly-tv-picks/?ref_=nv_tvv_picks_1"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_2"
>On Tonight</a></li>
                        <li><a href="/list/ls074093523/?ref_=nv_tvv_wn_3"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_4"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/academymuseum/?ref_=nv_sf_am_1"
>Academy Museum</a></li>
                        <li><a href="/imdbpicks/?ref_=nv_sf_pks_2"
>IMDb Picks</a></li>
                        <li><a href="/best-of/?ref_=nv_sf_bo_3"
>Best of 2014</a></li>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_4"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/whattowatch/?ref_=nv_sf_wtw_5"
>What to Watch</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=08-03&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_2"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_3"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_4"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_5"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_6"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_7"
>Road to the Emmys</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_8"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_9"
>Toronto Film Festival</a></li>
                        <li><a href="/awards-central/?ref_=nv_ev_all_10"
>More Popular Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni58859052/?ref_=nv_nw_tn_1"
> Venice Days Adds Julie Delpy-Directed âLoloâ Starring Herself And Dany Boon To Lineup
</a><br />
                        <span class="time">12 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni58859414/?ref_=nv_nw_tn_2"
> Pez Candy Animated Movie in the Works
</a><br />
                        <span class="time">8 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni58860423/?ref_=nv_nw_tn_3"
> Will & Jada Not Splitting
</a><br />
                        <span class="time">5 hours ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/games/guess?ref_=nv_cm_qz_3"
>Quiz Game</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_4"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5> 
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0095765/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMjE2OTc3NzA3MF5BMl5BanBnXkFtZTcwMDg2NzIwNw@@._V1._SY315_CR6,0,410,315_.jpg",
            titleYears : "1988",
            rank : 57,
                    headline : "Cinema Paradiso"
    },
    nameAd : {
            clickThru : "/name/nm0350453/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMjAwMDQ0MjQzMF5BMl5BanBnXkFtZTcwNDczNjQ4NA@@._V1._SX250_CR0,0,250,315_.jpg",
            rank : 10,
            headline : "Jake Gyllenhaal"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYjoO3O9NGOE9F0QxY6OGocEnqfGKYGuZLJSpp6rPdCXZ32gviVo52U7w1Jf37CkLJugxJPPTpm%0D%0A7NQzb4k1vEHFhErkMXcqz2_Wu5dxDt7xGBkG0iME5WwDApcmY0jXoEvmFjKkdVQJg1GrLtmRV6-6%0D%0Ae4fUsvbyHgJ3ookmvB9yF8NhemLsQoy26sAkueTyxazSpMZ7Ov9OIGeTqGtVdjpvWg%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYgLmGEC00HUcMVpJW7SIOkKUrFHJ4fYhA98Y5GTm2I9LGr7KKV-L-CPZcJDBQbG8jLPoGePrJW%0D%0Au0wB80OW1SMS8JDdyoE82_I8blUzNZyafzw9UouRPAQpwsUfjBYZJ3XNg2SR0L6PXvCTaGOI_UBX%0D%0A9s6ZsM6-RPjJAz0hI4cD3SDfpAZ7PppdjMSpyk0wN7Sg%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYmonQfjY8SqX-UYeep6LV02uF5vab6_QxEfOrp2-iAKEpzm65ExNtDlQ9IVhceBOv_I6ZMx6Fr%0D%0AIG52d7GY81-CvUBKpSqN7IRVBFn3Sqz6MsN7NRTq9j2t35BsjiskVLeL6hWa2Gt8_QagzggqoEqu%0D%0Ab4rHtkRzqS8SR-wXH2QnDJ7zxsWDmekUiRWsLIMaknu-oIfAKQJT7m06jA-rL1cnTw%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=697077898729;ord=697077898729?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>
                    <div id="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=697077898729;ord=697077898729?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	

                    
                    

<div id="content-2-wide">
    <div id="main">
                    
        <div class="heroWidget">
        <span class="ab_widget"
        >
            <div class="ab_hero">
<div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3908481817?ref_=hm_hp_i_1" data-video="vi3908481817" data-source="bylist" data-id="ls002309697" data-rid="10YAFVMV5H1GHT8BT830" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="Ben Whittaker is a 70-year-old widower who has discovered that retirement isn't all it's cracked up to be. Seizing an opportunity to get back in the game, he becomes a senior intern at an online fashion site, founded and run by Jules Ostin." alt="Ben Whittaker is a 70-year-old widower who has discovered that retirement isn't all it's cracked up to be. Seizing an opportunity to get back in the game, he becomes a senior intern at an online fashion site, founded and run by Jules Ostin." src="http://ia.media-imdb.com/images/M/MV5BNTk5OTc5NzQ2NV5BMl5BanBnXkFtZTgwMTcyNzQ0NjE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNTk5OTc5NzQ2NV5BMl5BanBnXkFtZTgwMTcyNzQ0NjE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Ben Whittaker is a 70-year-old widower who has discovered that retirement isn't all it's cracked up to be. Seizing an opportunity to get back in the game, he becomes a senior intern at an online fashion site, founded and run by Jules Ostin." title="Ben Whittaker is a 70-year-old widower who has discovered that retirement isn't all it's cracked up to be. Seizing an opportunity to get back in the game, he becomes a senior intern at an online fashion site, founded and run by Jules Ostin." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Ben Whittaker is a 70-year-old widower who has discovered that retirement isn't all it's cracked up to be. Seizing an opportunity to get back in the game, he becomes a senior intern at an online fashion site, founded and run by Jules Ostin." title="Ben Whittaker is a 70-year-old widower who has discovered that retirement isn't all it's cracked up to be. Seizing an opportunity to get back in the game, he becomes a senior intern at an online fashion site, founded and run by Jules Ostin." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2361509/?ref_=hm_hp_cap_pri_1" > The Intern </a> </div> </div> <div class="secondary ellipsis"> Trailer #2 </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1670361881?ref_=hm_hp_i_2" data-video="vi1670361881" data-source="bylist" data-id="ls002922459" data-rid="10YAFVMV5H1GHT8BT830" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="NWA shuts down the media in an exclusive clip from Straight Outta Compton." alt="NWA shuts down the media in an exclusive clip from Straight Outta Compton." src="http://ia.media-imdb.com/images/M/MV5BMTA5MzkyMzIxNjJeQTJeQWpwZ15BbWU4MDU0MDk0OTUx._V1_SX201_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTA5MzkyMzIxNjJeQTJeQWpwZ15BbWU4MDU0MDk0OTUx._V1_SX201_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="NWA shuts down the media in an exclusive clip from Straight Outta Compton." title="NWA shuts down the media in an exclusive clip from Straight Outta Compton." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="NWA shuts down the media in an exclusive clip from Straight Outta Compton." title="NWA shuts down the media in an exclusive clip from Straight Outta Compton." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt1398426/?ref_=hm_hp_cap_pri_2" > Straight Outta Compton </a> </div> </div> <div class="secondary ellipsis"> Exclusive Clip </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1217377049?ref_=hm_hp_i_3" data-video="vi1217377049" data-source="bylist" data-id="ls002322762" data-rid="10YAFVMV5H1GHT8BT830" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="En route to meet his estranged daughter and attempting to revive his dwindling career, a broken, aging comedian plays a string of dead-end shows in the Mojave desert." alt="En route to meet his estranged daughter and attempting to revive his dwindling career, a broken, aging comedian plays a string of dead-end shows in the Mojave desert." src="http://ia.media-imdb.com/images/M/MV5BMTA5OTEyMTQwODFeQTJeQWpwZ15BbWU4MDA0MzMyNDYx._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTA5OTEyMTQwODFeQTJeQWpwZ15BbWU4MDA0MzMyNDYx._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="En route to meet his estranged daughter and attempting to revive his dwindling career, a broken, aging comedian plays a string of dead-end shows in the Mojave desert." title="En route to meet his estranged daughter and attempting to revive his dwindling career, a broken, aging comedian plays a string of dead-end shows in the Mojave desert." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="En route to meet his estranged daughter and attempting to revive his dwindling career, a broken, aging comedian plays a string of dead-end shows in the Mojave desert." title="En route to meet his estranged daughter and attempting to revive his dwindling career, a broken, aging comedian plays a string of dead-end shows in the Mojave desert." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt3343784/?ref_=hm_hp_cap_pri_3" > Entertainment </a> </div> </div> <div class="secondary ellipsis"> Official Trailer </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/trailers?ref_=hm_hp_sm" class="position_bottom supplemental" > Browse more trailers </a> </p>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/imdbpicks/?ref_=hm_pks_hd" > <h3>IMDb Picks: August</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/?ref_=hm_pks_i_1" > <img itemprop="image" class="pri_image" title="Fantastic Four" alt="Fantastic Four" src="http://ia.media-imdb.com/images/M/MV5BMTk0OTMyMDA0OF5BMl5BanBnXkFtZTgwMzY5NTkzNTE@._V1_SX148_CR0,0,148,219_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk0OTMyMDA0OF5BMl5BanBnXkFtZTgwMzY5NTkzNTE@._V1_SX148_CR0,0,148,219_AL_UY438_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/?ref_=hm_pks_i_2" > <img itemprop="image" class="pri_image" title="Fear the Walking Dead" alt="Fear the Walking Dead" src="http://ia.media-imdb.com/images/M/MV5BMjMxMDA0NDM5NV5BMl5BanBnXkFtZTgwNDMwNTIxNjE@._V1_SY219_CR8,0,148,219_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjMxMDA0NDM5NV5BMl5BanBnXkFtZTgwNDMwNTIxNjE@._V1_SY219_CR8,0,148,219_AL_UY438_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/?ref_=hm_pks_i_3" > <img itemprop="image" class="pri_image" title="The Man from U.N.C.L.E." alt="The Man from U.N.C.L.E." src="http://ia.media-imdb.com/images/M/MV5BMTc2NjQ4ODYyNF5BMl5BanBnXkFtZTgwODA3OTU5NTE@._V1_SX148_CR0,0,148,219_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc2NjQ4ODYyNF5BMl5BanBnXkFtZTgwODA3OTU5NTE@._V1_SX148_CR0,0,148,219_AL_UY438_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/?ref_=hm_pks_i_4" > <img itemprop="image" class="pri_image" title="Shaun the Sheep Movie" alt="Shaun the Sheep Movie" src="http://ia.media-imdb.com/images/M/MV5BMjMyMDQ1MDA4OV5BMl5BanBnXkFtZTgwOTg1MzE0NjE@._V1_SX148_CR0,0,148,219_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjMyMDQ1MDA4OV5BMl5BanBnXkFtZTgwOTg1MzE0NjE@._V1_SX148_CR0,0,148,219_AL_UY438_UX296_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">You've got one goal this month: to pack in as much fun and relaxation as possible before school goes back into session and work ramps up. In order to help you accomplish such a lofty mission, use our handpicked list of August movies and TV shows that we're excited about as your guide. We hope you'll discover some instant hits and hidden gems!</p> <p class="seemore"> <a href="/imdbpicks/?ref_=hm_pks_sm" class="position_bottom supplemental" > Visit the IMDb Picks section </a> </p>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/gallery/rg1103796992?ref_=hm_if_ps_hd" > <h3>Exclusive Photos: <i>Pawn Sacrifice</i></h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm136701952/rg1103796992?ref_=hm_if_ps_i_1" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTYzOTAwMDU3MF5BMl5BanBnXkFtZTgwNTA3MzE0NjE@._V1._CR342,0,1365,1365_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTYzOTAwMDU3MF5BMl5BanBnXkFtZTgwNTA3MzE0NjE@._V1._CR342,0,1365,1365_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm254142464/rg1103796992?ref_=hm_if_ps_i_2" > <img itemprop="image" class="pri_image" title="Pawn Sacrifice (2014)" alt="Pawn Sacrifice (2014)" src="http://ia.media-imdb.com/images/M/MV5BMzYzNjAzMzYzNV5BMl5BanBnXkFtZTgwNTk2MzE0NjE@._V1_SY148_CR57,0,148,148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzYzNjAzMzYzNV5BMl5BanBnXkFtZTgwNTk2MzE0NjE@._V1_SY148_CR57,0,148,148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm170256384/rg1103796992?ref_=hm_if_ps_i_3" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjI4Mzg5Mjc0NF5BMl5BanBnXkFtZTgwOTk2MzE0NjE@._V1._CR342,0,1365,1365_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI4Mzg5Mjc0NF5BMl5BanBnXkFtZTgwOTk2MzE0NjE@._V1._CR342,0,1365,1365_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm220588032/rg1103796992?ref_=hm_if_ps_i_4" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjE3NDAyOTQ4N15BMl5BanBnXkFtZTgwNzk2MzE0NjE@._V1._CR320,0,1408,1408_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE3NDAyOTQ4N15BMl5BanBnXkFtZTgwNzk2MzE0NjE@._V1._CR320,0,1408,1408_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">Directed by <a href="/name/nm0001880/?ref_=hm_if_ps_lk1">Edward Zwick</a>, <a href="/title/tt1596345/?ref_=hm_if_ps_lk2"><i>Pawn Sacrifice</i></a> follows American chess champion Bobby Fischer (<a href="/name/nm0001497/?ref_=hm_if_ps_lk3">Tobey Maguire</a>) as he prepares for a legendary match-up against Russian Boris Spassky (<a href="/name/nm0000630/?ref_=hm_if_ps_lk4">Liev Schreiber</a>).</p> <p class="seemore"> <a href="/gallery/rg1103796992?ref_=hm_if_ps_sm" class="position_bottom supplemental" > See full photo gallery </a> </p>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58859052?ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTg4ODM0MjI5NV5BMl5BanBnXkFtZTYwNDQ5NjM1._V1_SY150_CR6,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58859052?ref_=hm_nw_tp1"
class="headlines" >Venice Days Adds Julie Delpy-Directed âLoloâ Starring Herself And Dany Boon To Lineup</a>
    <div class="infobar">
            <span class="text-muted">12 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?ref_=hm_nw_tp1_src"
>Variety - Film News</a></span>
    </div>
                                </div>
<p>Rome â <a href="/name/nm0000365?ref_=hm_nw_tp1_lk1">Julie Delpy</a>âs return to French filmmaking, â<a href="/title/tt4085944?ref_=hm_nw_tp1_lk2">Lolo</a>,â a high-profile comedy starring herself and Gallic superstar <a href="/name/nm0200702?ref_=hm_nw_tp1_lk3">Dany Boon</a> (âWelcome to the Sticksâ) has been added as the eleventh competition title at the Venice Film Festivalâs independently run Venice Days section, which is modeled on the ...                                        <span class="nobr"><a href="/news/ni58859052?ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58859414?ref_=hm_nw_tp2"
class="headlines" >Pez Candy Animated Movie in the Works</a>
    <div class="infobar">
            <span class="text-muted">8 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?ref_=hm_nw_tp2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58860423?ref_=hm_nw_tp3"
class="headlines" >Will & Jada Not Splitting</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000936?ref_=hm_nw_tp3_src"
>Access Hollywood</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58858222?ref_=hm_nw_tp4"
class="headlines" >Amy Schumer Determined to Reduce Gun Violence, Says She Thinks of Lafayette Shooting Victims "Everyday"</a>
    <div class="infobar">
            <span class="text-muted">22 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?ref_=hm_nw_tp4_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58861056?ref_=hm_nw_tp5"
class="headlines" >âThe Walkâ Star Charlotte Le Bon Joins Christian Bale, Oscar Isaac in âThe Promiseâ (Exclusive)</a>
    <div class="infobar">
            <span class="text-muted">33 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?ref_=hm_nw_tp5_src"
>The Wrap</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58859540?ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BNjM3OTAxMzc0NF5BMl5BanBnXkFtZTgwMjA5MTQ2NTE@._V1_SY150_CR2,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58859540?ref_=hm_nw_mv1"
class="headlines" >Weinstein Co.âs âMacbethâ Gets Exclusive Amazon Deal</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?ref_=hm_nw_mv1_src"
>Variety - Film News</a></span>
    </div>
                                </div>
<p>The <a href="/name/nm1055413?ref_=hm_nw_mv1_lk1">Michael Fassbender</a>-<a href="/name/nm0182839?ref_=hm_nw_mv1_lk2">Marion Cotillard</a> starrer â<a href="/title/tt2884018?ref_=hm_nw_mv1_lk3">Macbeth</a>â will stream exclusively on Amazon soonÂ after its December theatrical release.The Weinstein Co. has not set the exact release date on Amazon for â<a href="/title/tt2884018?ref_=hm_nw_mv1_lk4">Macbeth</a>.â TWC bought North American rights in 2013 from Studiocanal.Fassbender stars as the ...                                        <span class="nobr"><a href="/news/ni58859540?ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58859204?ref_=hm_nw_mv2"
class="headlines" >Eran Creevyâs Thriller âCollideâ Reverses Out of U.S. Release with Relativity: Report</a>
    <div class="infobar">
            <span class="text-muted">11 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?ref_=hm_nw_mv2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58859599?ref_=hm_nw_mv3"
class="headlines" >Ronda Rousey to Star in Film Based on Her Autobiography (Exclusive)</a>
    <div class="infobar">
            <span class="text-muted">6 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?ref_=hm_nw_mv3_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58861126?ref_=hm_nw_mv4"
class="headlines" >Ali Larter Confirmed to Return to the âResident Evilâ Series</a>
    <div class="infobar">
            <span class="text-muted">27 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000151?ref_=hm_nw_mv4_src"
>Slash Film</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58861127?ref_=hm_nw_mv5"
class="headlines" >Cool Prints: Dave Perilloâs âFeastâ and Scott Câs âMad Max: Fury Roadâ</a>
    <div class="infobar">
            <span class="text-muted">58 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000151?ref_=hm_nw_mv5_src"
>Slash Film</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58859292?ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTQ4NTMzMDM5OF5BMl5BanBnXkFtZTgwMDczOTA5MjE@._V1_SY150_CR6,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58859292?ref_=hm_nw_tv1"
class="headlines" >Castle Season 8: [Spoiler] Will Return in a 'Surprising' Way</a>
    <div class="infobar">
            <span class="text-muted">10 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?ref_=hm_nw_tv1_src"
>TVLine.com</a></span>
    </div>
                                </div>
<p> TVLine can now reveal another intriguing piece of the puzzle that is <a href="/title/tt1219024?ref_=hm_nw_tv1_lk1">Castle</a>âs Season 8 premiere. As previously reported, the ABC procedural will return Sept. 21 with a two-parter, the first installment of which is titled âXy.â The double-episode salvo will introduce two new characters: Scotland ...                                        <span class="nobr"><a href="/news/ni58859292?ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58858438?ref_=hm_nw_tv2"
class="headlines" >Things get bad for the 'True Detective' heroes right as the season gets good?</a>
    <div class="infobar">
            <span class="text-muted">20 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000173?ref_=hm_nw_tv2_src"
>Hitfix</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58859407?ref_=hm_nw_tv3"
class="headlines" >Vice Launches Female-Focused Channel Called Broadly</a>
    <div class="infobar">
            <span class="text-muted">9 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?ref_=hm_nw_tv3_src"
>The Wrap</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58859342?ref_=hm_nw_tv4"
class="headlines" >Why Jon Stewart Might Be Irreplaceable</a>
    <div class="infobar">
            <span class="text-muted">9 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?ref_=hm_nw_tv4_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58858404?ref_=hm_nw_tv5"
class="headlines" >âHalt and Catch Fireâ Finale Brings Closure to AMC Drama (Spoilers)</a>
    <div class="infobar">
            <span class="text-muted">20 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?ref_=hm_nw_tv5_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58860423?ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTY4MzI4NTQzOF5BMl5BanBnXkFtZTgwMDkyOTA2NDE@._V1_SY150_CR7,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58860423?ref_=hm_nw_cel1"
class="headlines" >Will & Jada Not Splitting</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000936?ref_=hm_nw_cel1_src"
>Access Hollywood</a></span>
    </div>
                                </div>
<p><a href="/name/nm0000226?ref_=hm_nw_cel1_lk1">Will Smith</a> and <a href="/name/nm0000586?ref_=hm_nw_cel1_lk2">Jada Pinkett Smith</a> are at the center of yet another split rumor, but <a href="/title/tt0167515?ref_=hm_nw_cel1_lk3">Access Hollywood</a> has learned the couple is, in fact, not set to divorce. Following a tabloid report claiming the Hollywood power couple was planning to divorce at the end of the summer, a source close to Will and ...                                        <span class="nobr"><a href="/news/ni58860423?ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58859297?ref_=hm_nw_cel2"
class="headlines" >Is Drew Barrymore Done With Hollywood? Actress Admits She's Turning Down "A Lot" of Jobs</a>
    <div class="infobar">
            <span class="text-muted">10 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?ref_=hm_nw_cel2_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58859409?ref_=hm_nw_cel3"
class="headlines" >Jessica Albaâs Honest Company Sunscreen Under Fire by Burnt Customers</a>
    <div class="infobar">
            <span class="text-muted">9 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?ref_=hm_nw_cel3_src"
>The Wrap</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58858377?ref_=hm_nw_cel4"
class="headlines" >Jamie Chung Celebrates Upcoming Wedding With Enchanting Bridal Shower and Surprise Tribute to The Bachelor</a>
    <div class="infobar">
            <span class="text-muted">21 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?ref_=hm_nw_cel4_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58861226?ref_=hm_nw_cel5"
class="headlines" >Gwen Stefani & Gavin Rossdale Split</a>
    <div class="infobar">
            <span class="text-muted">20 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000936?ref_=hm_nw_cel5_src"
>Access Hollywood</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/gallery/rg1942657792?ref_=hm_snp_sunbst_hd" > <h3>IMDb Snapshot - Photos We Love</h3> </a> </span> </span> <p class="blurb">Here's a look back at some of our favorite event photos, still images, and posters released between July 26 - August 1.</p> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3125143552/rg1942657792?ref_=hm_snp_sunbst_i_1" > <img itemprop="image" class="pri_image" title="The Man from U.N.C.L.E. (2015)" alt="The Man from U.N.C.L.E. (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTExMTUwMzAwMzZeQTJeQWpwZ15BbWU4MDg3NjY5MzYx._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTExMTUwMzAwMzZeQTJeQWpwZ15BbWU4MDg3NjY5MzYx._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm461039616/rg1942657792?ref_=hm_snp_sunbst_i_2" > <img itemprop="image" class="pri_image" title="Queen of Earth (2015)" alt="Queen of Earth (2015)" src="http://ia.media-imdb.com/images/M/MV5BMzMzOTc2MzYyNl5BMl5BanBnXkFtZTgwODA4MDgzNjE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzMzOTc2MzYyNl5BMl5BanBnXkFtZTgwODA4MDgzNjE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1648814080/rg1942657792?ref_=hm_snp_sunbst_i_3" > <img itemprop="image" class="pri_image" title="Vacation (2015)" alt="Vacation (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTQ5MDgzMTk2N15BMl5BanBnXkFtZTgwODc5NjkzNjE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ5MDgzMTk2N15BMl5BanBnXkFtZTgwODc5NjkzNjE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/gallery/rg1942657792?ref_=hm_snp_sunbst_sm" class="position_bottom supplemental" > See full gallery </a> </p>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
                <div class="ab_borntoday">
<span class="ninja_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=8-3&ref_=hm_brn_hd" > <h3>Born Today</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1431940?ref_=hm_brn_i_1" > <img itemprop="image" class="pri_image" title="Evangeline Lilly" alt="Evangeline Lilly" src="http://ia.media-imdb.com/images/M/MV5BMjEwOTA1MTcxOF5BMl5BanBnXkFtZTcwMDQyMjU5MQ@@._V1_SY172_CR13,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjEwOTA1MTcxOF5BMl5BanBnXkFtZTcwMDQyMjU5MQ@@._V1_SY172_CR13,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1431940?ref_=hm_brn_cap_pri_lk1_1">Evangeline Lilly</a> (36) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0336701?ref_=hm_brn_i_2" > <img itemprop="image" class="pri_image" title="Mamie Gummer" alt="Mamie Gummer" src="http://ia.media-imdb.com/images/M/MV5BMTc2MDk3MTI1N15BMl5BanBnXkFtZTcwMDM0NjM1Mg@@._V1_SY172_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc2MDk3MTI1N15BMl5BanBnXkFtZTcwMDM0NjM1Mg@@._V1_SY172_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0336701?ref_=hm_brn_cap_pri_lk1_2">Mamie Gummer</a> (32) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1013003?ref_=hm_brn_i_3" > <img itemprop="image" class="pri_image" title="Michael Ealy" alt="Michael Ealy" src="http://ia.media-imdb.com/images/M/MV5BMTQwODUwOTg1OV5BMl5BanBnXkFtZTcwMjg2NjM5MQ@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQwODUwOTg1OV5BMl5BanBnXkFtZTcwMjg2NjM5MQ@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1013003?ref_=hm_brn_cap_pri_lk1_3">Michael Ealy</a> (42) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0000640?ref_=hm_brn_i_4" > <img itemprop="image" class="pri_image" title="Martin Sheen" alt="Martin Sheen" src="http://ia.media-imdb.com/images/M/MV5BOTM1MTA5MTY0MV5BMl5BanBnXkFtZTcwMTQ4OTUzMg@@._V1_SY172_CR4,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BOTM1MTA5MTY0MV5BMl5BanBnXkFtZTcwMTQ4OTUzMg@@._V1_SY172_CR4,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0000640?ref_=hm_brn_cap_pri_lk1_4">Martin Sheen</a> (75) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0001525?ref_=hm_brn_i_5" > <img itemprop="image" class="pri_image" title="John C. McGinley" alt="John C. McGinley" src="http://ia.media-imdb.com/images/M/MV5BMTM5NjQ2OTIwMV5BMl5BanBnXkFtZTcwMjcxOTYyMQ@@._V1_SY172_CR3,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTM5NjQ2OTIwMV5BMl5BanBnXkFtZTcwMjcxOTYyMQ@@._V1_SY172_CR3,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0001525?ref_=hm_brn_cap_pri_lk1_5">John C. McGinley</a> (56) </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/search/name?refine=birth_monthday&birth_monthday=8-3&ref_=hm_brn_sm" class="position_bottom supplemental" > See all birthdays </a> </p>        </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/title/tt2750182/?ref_=hm_tv_ph_hd" > <h3>TV Spotlight: "Playing House"</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm657975296/tt2750182?ref_=hm_tv_ph_i_1" > <img itemprop="image" class="pri_image" title="Playing House (2014-)" alt="Playing House (2014-)" src="http://ia.media-imdb.com/images/M/MV5BMjA0OTkyMzEwMF5BMl5BanBnXkFtZTgwNzIyNDI0NjE@._V1_SY250_CR9,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA0OTkyMzEwMF5BMl5BanBnXkFtZTgwNzIyNDI0NjE@._V1_SY250_CR9,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi395293465?ref_=hm_tv_ph_i_2" data-video="vi395293465" data-rid="10YAFVMV5H1GHT8BT830" data-type="single" class="video-colorbox" data-refsuffix="hm_tv_ph" data-ref="hm_tv_ph_i_2"> <img itemprop="image" class="pri_image" title="Playing House (2014-)" alt="Playing House (2014-)" src="http://ia.media-imdb.com/images/M/MV5BMjEyMzU3MjkxNF5BMl5BanBnXkFtZTgwMDMyNDI0NjE@._V1_SX444_CR0,0,444,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjEyMzU3MjkxNF5BMl5BanBnXkFtZTgwMDMyNDI0NjE@._V1_SX444_CR0,0,444,250_AL_UY500_UX888_AL_.jpg" /> <img alt="Playing House (2014-)" title="Playing House (2014-)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Playing House (2014-)" title="Playing House (2014-)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">"Playing House" returns for a second season on Tuesday, August 4 on USA, picking up a few months after the birth of baby Charlotte, with Maggie and Emma getting the hang of mommyhood and ready to start dating.</p> <p class="seemore"> <a href="/title/tt2750182/?ref_=hm_tv_ph_sm" class="position_bottom supplemental" > Learn more about "Playing House" </a> </p>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_trivia">
<span class="ninja_header"> <span class="oneline"> <a href="/title/tt1951265/trivia?item=tr2321166&ref_=hm_trv_hd" > <h3>Did You Know?</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt1951265?ref_=hm_trv_i_1" > <img itemprop="image" class="pri_image" title="The Hunger Games: Mockingjay - Part 1 (2014)" alt="The Hunger Games: Mockingjay - Part 1 (2014)" src="http://ia.media-imdb.com/images/M/MV5BMTcxNDI2NDAzNl5BMl5BanBnXkFtZTgwODM3MTc2MjE@._V1_SX89_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTcxNDI2NDAzNl5BMl5BanBnXkFtZTgwODM3MTc2MjE@._V1_SX89_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" > <strong class="text-large"><a href="/title/tt1951265?ref_=hm_trv_lk1">The Hunger Games: Mockingjay - Part 1</a></strong> <p class="blurb">Jennifer Lawrence doesn't like singing, and hated filming the part where she sang "The Hanging Tree."</p></div> </div> </div> <p class="seemore"> <a href="/title/tt1951265/trivia?item=tr2321166&ref_=hm_trv_sm" class="position_bottom supplemental" > See more trivia </a> </p>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;NhGqcV20HQo&quot;}"
        >
            <div class="ab_poll poll">
<span class="ninja_header"> <span class="oneline"> <a href="/poll/NhGqcV20HQo/?ref_=hm_poll_hd" > <h3>Poll: Favorite Film of 1985</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?ref_=hm_poll_mp">More Polls</a></h4> </span> </span> <p class="blurb">Which film celebrating its 30th anniversary is your favorite? <a href="http://www.imdb.com/board/bd0000088/thread/245914352/?ref_=hm_poll_lk1">Discuss here</a> after voting! Mark which ones you've seen <a href="http://www.imdb.com/seen/ls074186642/?ref_=hm_poll_lk2">here</a>.</p> <p class="seemore"> <a href="/poll/NhGqcV20HQo/?ref_=hm_poll_sm" class="position_blurb supplemental" > Vote now </a> </p> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:31.952%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/NhGqcV20HQo/?ref_=hm_poll_i_1" > <img itemprop="image" class="pri_image" title="Back to the Future (1985)" alt="Back to the Future (1985)" src="http://ia.media-imdb.com/images/M/MV5BMjA5NTYzMDMyM15BMl5BanBnXkFtZTgwNjU3NDU2MTE@._V1_SX233_CR0,0,233,345_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA5NTYzMDMyM15BMl5BanBnXkFtZTgwNjU3NDU2MTE@._V1_SX233_CR0,0,233,345_AL_UY690_UX466_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:31.952%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/NhGqcV20HQo/?ref_=hm_poll_i_2" > <img itemprop="image" class="pri_image" title="Mad Max Beyond Thunderdome (1985)" alt="Mad Max Beyond Thunderdome (1985)" src="http://ia.media-imdb.com/images/M/MV5BMTk0MDQ5NTYxNV5BMl5BanBnXkFtZTcwNTA0ODYyMQ@@._V1_SY345_CR12,0,233,345_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk0MDQ5NTYxNV5BMl5BanBnXkFtZTcwNTA0ODYyMQ@@._V1_SY345_CR12,0,233,345_AL_UY690_UX466_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:31.952%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/NhGqcV20HQo/?ref_=hm_poll_i_3" > <img itemprop="image" class="pri_image" title="I Goonies (1985)" alt="I Goonies (1985)" src="http://ia.media-imdb.com/images/M/MV5BMTEyMzM3NDQyMjJeQTJeQWpwZ15BbWU4MDE4ODY0NjEx._V1_SX233_CR0,0,233,345_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTEyMzM3NDQyMjJeQTJeQWpwZ15BbWU4MDE4ODY0NjEx._V1_SX233_CR0,0,233,345_AL_UY690_UX466_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
    </div>
    <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=697077898729;ord=697077898729?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=697077898729?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=697077898729?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	
                    
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/movies-in-theaters/?ref_=hm_otw_hd" > <h3>Opening This Week</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1502712"></div> <div class="title"> <a href="/title/tt1502712?ref_=hm_otw_t0"> Fantastic Four </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2872750"></div> <div class="title"> <a href="/title/tt2872750?ref_=hm_otw_t1"> Shaun the Sheep Movie </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3623726"></div> <div class="title"> <a href="/title/tt3623726?ref_=hm_otw_t2"> Ricki and the Flash </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4178092"></div> <div class="title"> <a href="/title/tt4178092?ref_=hm_otw_t3"> The Gift </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3813310"></div> <div class="title"> <a href="/title/tt3813310?ref_=hm_otw_t4"> Cop Car </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3172532"></div> <div class="title"> <a href="/title/tt3172532?ref_=hm_otw_t5"> The Diary of a Teenage Girl </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1640718"></div> <div class="title"> <a href="/title/tt1640718?ref_=hm_otw_t6"> Kahlil Gibran's The Prophet </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3819668"></div> <div class="title"> <a href="/title/tt3819668?ref_=hm_otw_t7"> Dragon Ball Z: Resurrection 'F' </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2402101"></div> <div class="title"> <a href="/title/tt2402101?ref_=hm_otw_t8"> Dark Places </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> </div> </div> </div> <div><a href="/movies-in-theaters/?ref_=hm_otw_sm" ><p class="seemore position_bottom">See more opening this week</p></a></div>
        </span>
        </div>
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
            <div class="ab_ninja showtimesMessagingWidget">
<p class="blurb"><a href="/showtimes?ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p>    </div>

        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/chart/?ref_=hm_cht_hd" > <h3>Now Playing (Box Office)</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2381249"></div> <div class="title"> <a href="/title/tt2381249?ref_=hm_cht_t0"> Mission: Impossible - Rogue Nation </a> <span class="secondary-text">$55.5M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1524930"></div> <div class="title"> <a href="/title/tt1524930?ref_=hm_cht_t1"> Vacation </a> <span class="secondary-text">$14.7M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt0478970"></div> <div class="title"> <a href="/title/tt0478970?ref_=hm_cht_t2"> Ant-Man </a> <span class="secondary-text">$12.8M</span> </div> <div class="action"> <a href="/showtimes/title/tt0478970?ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2293640"></div> <div class="title"> <a href="/title/tt2293640?ref_=hm_cht_t3"> Minions </a> <span class="secondary-text">$12.4M</span> </div> <div class="action"> <a href="/showtimes/title/tt2293640?ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2120120"></div> <div class="title"> <a href="/title/tt2120120?ref_=hm_cht_t4"> Pixels </a> <span class="secondary-text">$10.5M</span> </div> <div class="action"> <a href="/showtimes/title/tt2120120?ref_=hm_cht_gs"> Showtimes </a> </div> </div> </div> </div> </div> <div><a href="/chart/?ref_=hm_cht_sm" ><p class="seemore position_bottom">See more box office results</p></a></div>
        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/movies-coming-soon/?ref_=hm_cs_hd" > <h3>Coming Soon</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1638355"></div> <div class="title"> <a href="/title/tt1638355?ref_=hm_cs_t0"> The Man from U.N.C.L.E. </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1398426"></div> <div class="title"> <a href="/title/tt1398426?ref_=hm_cs_t1"> Straight Outta Compton </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2872462"></div> <div class="title"> <a href="/title/tt2872462?ref_=hm_cs_t2"> Mistress America </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1634003"></div> <div class="title"> <a href="/title/tt1634003?ref_=hm_cs_t3"> Underdogs </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <div><a href="/movies-coming-soon/?ref_=hm_cs_sm" ><p class="seemore position_bottom">See more coming soon</p></a></div>
        </span>
        </div>
                    
                    
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
                <h3>Follow Us On Twitter</h3>
    <iframe class="twitter-frame" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3776329187._CB305760180_.html#{"screen-name":"IMDb","height":"500px","width":"300px"}'></iframe>

 

        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
            <div id="fb-root"></div>

    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    
    <h3>Find Us On Facebook</h3>
    <div class="fb-like-box" data-href="http://www.facebook.com/imdb" data-width="285" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
        <div class="aux-content-widget-2 sticky-widget">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/imdbpicks/?ref_=hm_pks_hd" > <h3>IMDb Picks: August</h3> </a> </span> </span> <p class="blurb">Visit our IMDb Picks section to see our recommendations of movies and TV shows coming out in August.</p> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/?ref_=hm_pks_i_1" > <img itemprop="image" class="pri_image" title="Fear the Walking Dead (2015-)" alt="Fear the Walking Dead (2015-)" src="http://ia.media-imdb.com/images/M/MV5BNzkxNTY4MzUyM15BMl5BanBnXkFtZTgwODQwNzUzNjE@._V1_SY525_CR0,0,700,525_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNzkxNTY4MzUyM15BMl5BanBnXkFtZTgwODQwNzUzNjE@._V1_SY525_CR0,0,700,525_AL_UY1050_UX1400_AL_.jpg" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/imdbpicks/?ref_=hm_pks_cap_pri_1" > Fear the Walking Dead </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/imdbpicks/?ref_=hm_pks_sm" class="position_bottom supplemental" > Visit the IMDb Picks section </a> </p>    </div>

        </span>
        </div>
    </div>
</div>

                    
                    


                   <br class="clear" />
                </div>





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYoF9gLl9a97FQKmBd3bIzqlqcWmomDSTWfR-_V87CyMgvY-EVwe-c-3wOdYqWhBBKtNHdAImbV%0D%0AhqqgZu2BZEtgCQAUzs6iuH8LTf7CFbQK1Rqvc8eY3HFc8Hpb97fi7j9BshnzBYnyMg2AJVtQ1lez%0D%0AkJwOCMoVOw7tajzwpu-pI7mV7b3oJ7kEpqCQtMZdLesAB_9U0ZduoxyNsaYuborrJnBaezAALZyb%0D%0AoaAtDzbiPGQ%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYsNLltzmIH8_VBhyKMqBDBtARthQRoghrceU0SFljif4N2bttMaw6KrbgX7GtLi9cI1VueCClo%0D%0AmBxKJwZsgcL7YoI8niEBYDaDtBcqqC3G1kd2nEW7AB1smLidjU9hbCWI-cuOHU7lbw0IxcltvOB4%0D%0AVU32lwkIL1VBj2lJ06D5D_y1O_mmrT_gDHG422tYOnZcFAjQUBw4Dpg3xe0Nhdo6qg%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYuIZHedh7eSgShfFa0_0bLKtXfd_twXRRoGBfYQv9R7oIf2xwoDU6llkwgVEfLXOTrCPeQw5xy%0D%0AFkwdcFC_kq-on8FnsBoDeU7nXr9EqivZFuI3p5wN6_xUt_d2bGVEC3POlUA7czlXysQPMpl3lRyc%0D%0AMxEX5RgOJ77vl9xYEthe0mKpD0nDNZvIq0K9Isu8chHEpAJEM_fEpd1dmG1P1-XAMfmq2JpP4c5o%0D%0AF8SftzPrx2EFUCnArwXpoRC_j0eVvQjRhhuAHhuezZWXKzEm2BuyiyyUFIXfTQs8DF_p67MwOxQD%0D%0AQ85_EsCEK5WOafNfB1mB_FHn6Ab2JykQLq7QOU2g2aP5qNUXQVG0B8OjqGd5RxqItMO_RESQhzKD%0D%0AVEuFefL_jMXMStBMiFAkFbkNilBOKQ%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYqyEzh1rfFTd4zD3XW4w8xJTcFq7sATrWsVP6gkRbFZgsVl5G2dtoRxqW_A-QakgnI-XB1VHpu%0D%0AQ_WJsP5qbvarIxOQ2Vh_MyyCt9RlqRTnrComIhl5o60m0ZM-Il4Ej1_Q56-nxORqTJezIrvtvKto%0D%0AWbdbjDm-YH2JdRH8IQAyn5p-hsJDp7TVZgf5aZjDo7u0PV14-6poxahJ9QrdAjljqg%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYgZi3Qpzzj7Ib3ieim0hYhuUyzL6AIGt3g4GGGyIzBSy4xPuTWt8GJkU8LewFhxUbDhV1K-5gv%0D%0Ah2Yl-hjWIPOqHpHVnxbq41OYztqzwNBtRmMAPiAohXCx7rj6n2TeFn_-q5O61ruwJjVF8fGmNYeG%0D%0AMFaQpSQSs75w3oh_BPFKUvTMBPqVPRnUy4idC3rgQd_e%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYrE5XPY5hdDjaavy7zPM90-FaXPdtlIGuT107LYUe7euVrINRALSnDV1A1BbgpXitbfz51dfsR%0D%0AMR01Q5GqxsGc9vt0TjMfee4jrWAOKqwZQhpWY3QjZyLV4VB4LHrMao5xx2Spu88mnaT-5HoLnxwk%0D%0AORy0IC5bC7hnM0zddPWSMw8Ah0q5ugupddqqppqWcGsJ985e7hCBzIQYlANESNNnsA%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top 250</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Top Movies</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li>    <a href="/offsite/?page-action=ft_ad&token=BCYpAUm_DMQP1xBTe8oNLXd3Y8ezEfZAofR1LT7zjP8dgTtloVwT6LzpxoNDlSoi5cJKevBzmMpq%0D%0AmJHm1JK3ej7QH13YHMJM5JbzjbUN3XJKkPnjQ55PwpucJN12Hxhjy7SgHSIVKPV7vO_WpnehLPZ9%0D%0A8xS2Qin3-g6H0aqYdJcWM_jvEe8jioPVcmmx98tiQXUos2vTU9WIWofcD9uD5hEYxFIoP2dDQLX-%0D%0AIUoZnuqNq5XJ8JXmr5q_p0h_5eUM77rUm1zQV7zLWpWRrlzDWYrUDA%0D%0A&ref_=ft_ad"
itemprop='url'>Advertising</a>
</li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYshiCi_6jTYzmSwwB9D6x-kzzTRC5pHM1Oq0IkyfJ9MRP4UISvE8bdMLVPAg-lWXtGv-27cqwE%0D%0A9t9YYRBaF70G3_kLuw1mkY3d8mhy84Naz9EG9cRcaRkxpQ0lawL4NHVgacfzmqeWZErUcOeMAqGo%0D%0ArISuD7UWoY3lK1qhS95qzy83irEzoSrlUQv4Wd_TlGBo7Xym8dBsKCJSybQdkV-YJw%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYuhfYb4kuD43nfy7fxPt18FrAMeL3wywos7XHblnjuAecGnKWC9nZ4obNJj_p_PKRjnKqpmxf4%0D%0A8EBTXnUPfbiJbpHEpjCrYGbvU3e6gMiZ9vRT5M45GcB_KUe5BOIjsPUdWjmVHP_H18xzVj2twBKj%0D%0AABi4K_sjMk2aYknfU4AnrzNI-l2jH3r7WX-NJcQc8dHk%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYg8YY0qJ1ZC557i3bsmqzoO1q3x0Od2QbzYC6_PcYm2b5meVPiXmQSfCkXveieo-_YucoEeMd5%0D%0AbtEQKj6nr1UKBVwp75ypcMydOp2ULhkJRw5-j1is-QOkZz5qWlcCjftIUUmmQmgMkaq4rJgCmosp%0D%0AgWldgWmQgNu6vxqLeQSQG30IRwU-ltpsJbOP_JKBDthR53C_Ny06AHbhb91lcuN-qoZ60R5TquZ4%0D%0AEIPEcZX2otNVpE325TULQqXERio_NYsf%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/show_article?conditions';"
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2015
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/';"
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYqc0gabiVonQPZU99aPmT6lf8rPtMzmKHkRjOaV9SI34ZK5GGhuyALdg-7PR68BV4OXz8lTiKI%0D%0A2-ifCLi_wNPdhzqAkn2fmLXTFrXx2BxNyKAMUHvIKkc9PiVfbjzNZeIoJgpSInsb_OPJMWaunj0t%0D%0A8SY5J4c6oLYVghxB3LEZW-N9gTcncfdMXn1b1euL6eTCxYSdjIMvaU9w_7swLhg2UgMlWuDBGSsJ%0D%0AUlYwud0cMCw%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Instant Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYgWAI382GXS6dijZc2wbfe1oBO3nA8IKbpuLsiZzCnFwFYTRMQYpk-VyWeVMqK0q4-umFho2AY%0D%0Ae7wlazBXnEON9PYpgKCWMDtBzEI7puZllRIJvzxflH0crd9Iw7kObYsKhXk06gLvOOE6GqD8-_HL%0D%0A4gbBRhM5yA6ISVL5aPEpNcLML2Bdu6mfGxWbLyH7sqGk3AAJiHmwQkLsR9idEWT2H4nB3QAX7lPn%0D%0AT7ZQAkmMdaI%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Instant Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYkvP0TmFvSKIEQ7VdAQS6wyoqUKPAXw4rJ7Vz5MuvNm4zIDAFV9ipzdYRC5AiHzROPBDVNy8ZG%0D%0As44W4-KWug9F3CA3zxMoogPPL7OUjfMDhHW0WnRbM6XNAL1CQg6MPlir4Y7XJv3LSDtmibKT_gXu%0D%0Af0IcZ_GV0pGHRd1jp9C6P_mCuE79WVnu8rqswf8xBodZL4sAHjB77mAj03NARIrt_Rjo_RQsTstp%0D%0AEwGR0kwyfrs%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYjYpO0yXlFSFXXg5RWGQemNUDuU6hXl35n3Li5aSRPFkORujuCsxYcNjizns3FeM_f3GPxhCZ0%0D%0Ak-LcCJrYbymm00TYmnXDDIXmsqdr2U0ajYgah6l4-hhmiS_SvYEmS86l6M3posB9YFSeo7gt6ZsU%0D%0ABbEbpjcJsAt6iaGjlOW96UdcODJehCuqCOydDE8YR4B4zOt-2_bV5Z9cyE9F-6e-aIss9y6whRt7%0D%0AQfBzQ_DHyX4%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYukJJedOPEQDY5Y4TzZBtTsKsI-61xpEyxJtU7Mn1j_XyeAXVQJluHXuwHBOttSL15H-jCXrmN%0D%0AC9f8czZQyZXHx2ZOS8VZS1m1bTs2kviqKD94RaC3F33fOteDp0otjO79H0z3jfhhEdeeIfhjkVV4%0D%0ATUnGuikmnkgX0MI3PPkt7EG2Rz5lfA8KJxJE1mNLYgqwCrzDJKfYaDpbHMHy2GMBasjmsK5eeNLS%0D%0Ad3EbntRoS3Y%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYlE0BW4cj4ARjaa_aHt22RBuxo4ojEivM-sSvqpqZ1R2RWW2Z6ZdtCCQTXPXMFyc0ImJR-CO9q%0D%0ARXQ7Ur3OVbeebkIff4R_reUcBzbGiAszSomd3LxorHYXSTQ5oQq5Et6EN-CSVlU00yJoIJHz4Z-U%0D%0Al3Zdot9gDNuKDOu0tW8EFJaBHSMeRic56shXQFiZBerc495AwNqUlfBFObzFVdTwK4AXDONlipzV%0D%0ATYbo0ZkRshHKysaKVpBXpyVtRqy9Booj%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYkqOSmf9vZn-p4noO_TcJUqKINdoTHbWFo4W00pCIl0XQE6TkTFMvOxGwnYI8xGYmihM3Iuk3v%0D%0Az-ESZkUWCAeiArGqsjRWizgxPecreWGyCbFio7WnXsbJZqD-3G9DaJxxJkx2lRTHbJ6lqXvi-wf7%0D%0ANfL7cSW_KBJkzA8jTuo6edY%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYtJ0NtWtXrr7bJ9C4vtoTRgla14ko1xXRdqlx5unQ4Rgo4y39UNNn2T377KHXWzLPybKF0RxS1%0D%0ALqisvMicvweNKfnST2cUYEIEh48mffGi8z5BOJOgOcSSlzITRqSL2FEty6ETNWfS2Cy6XN-obieg%0D%0Ai6H1VDKntVgcAQgOZHunUZw%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-4053134957._CB315300329_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-1753959650._CB315300322_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-3627981178._CB318528484_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-2430112694._CB318528507_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=0101f2ad60738780664fed9e024f586dabb9afc8a6a8a3f23f8b747292d913e53da1",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=697077898729"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-232398347._CB349580400_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=697077898729&ord=697077898729";
    },"unable to request AAN pixel");
</script>

<div id="flashContent" style="width: 0px; height: 0px; overflow:hidden;">
    <script type="text/javascript">
        if (generic && generic.monitoring.record_metric && flashAdUtils.canPlayFlash) {
            var flashLoaded = 0;
            function swfLoaded() {
                flashLoaded = 1;
                generic.monitoring.record_metric('ads_flash_did_play', 1, true);
            }
            window.onload = function() {
                // Flash was not loaded
                if (flashLoaded === 0) {
                    // Flash could be loaded but wasn't
                    if (flashAdUtils.canPlayFlash()) {
                        generic.monitoring.record_metric('ads_flash_can_but_did_not_play', 0, true);
                    } else {
                        generic.monitoring.record_metric('ads_flash_cannot_play', 0, true);
                    }
                }
            }
            /**
             * There is a rare possibility of window.onload getting called before swfLoaded.
             * Firing this metric to keep track of such calls.
             */
             generic.monitoring.record_metric('ads_flash_page_loaded', 1, true);
         }
    </script>
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="1x1" align="middle">
        <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
        <param name="quality" value="high" />
        <param name="play" value="true" />
        <param name="loop" value="true" />
        <param name="wmode" value="transparent" />
        <param name="scale" value="showall" />
        <param name="menu" value="true" />
        <param name="devicefont" value="false" />
        <param name="salign" value="" />
        <param name="allowScriptAccess" value="always" />

        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" width="1" height="1">
            <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
            <param name="quality" value="high" />
            <param name="play" value="true" />
            <param name="loop" value="true" />
            <param name="wmode" value="transparent" />
            <param name="scale" value="showall" />
            <param name="menu" value="true" />
            <param name="devicefont" value="false" />
            <param name="salign" value="" />
            <param name="allowScriptAccess" value="always" />
        </object>
        <!--<![endif]-->
    </object>
</div>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="655"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
