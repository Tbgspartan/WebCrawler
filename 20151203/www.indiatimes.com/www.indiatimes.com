<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trending  stories on Indian Lifestyle, Culture, Relationships, Food, Travel, Entertainment, News & New Technology News - Indiatimes.com</title>

<meta name="keywords" content="" />
<meta name="description" content="Indiatimes.com brings you the news, articles, stories and videos on entertainment, latest lifestyle, culture & new technologies emerging worldwide." />



		

<link rel="image_src" href="http://media.indiatimes.in/resources/images/fbimage.png" />
<meta property="og:title" content="" />    
<meta property="og:url" content="http://www.indiatimes.com" />
<meta property="og:image" content="http://media.indiatimes.in/resources/images/fbimage.png" />

<meta property="og:site_name" content="indiatimes.com" />
<meta property="fb:app_id" content="117787264903013" />
<meta property="og:description" content="" />
<meta name="google-site-verification" content="ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw" />
<meta name="msvalidate.01" content="BAB71AAED7CCC01DB106A58E99625EB4" />
<link href="https://plus.google.com/+indiatimes/posts" rel="publisher" />

<meta property="og:type" content="website" />
    
<!--<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />-->

<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">    <link rel="shortcut icon" href="http://media.indiatimes.in/resources/images/favicon.ico?v=25" type="image/x-icon"><link rel="stylesheet" href="http://www.indiatimes.com/fonts/fonts.min.css?v=100.65" media="screen"><link rel="stylesheet" href="http://media.indiatimes.in/resources/css/site.css?v=100.65" media="screen"><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery-1.11.1.min.js?v=100.65"></script><script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/handlebars-v1.3.0.js?v=100.65"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.inview.min.js?v=100.65"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.preload.min.js?v=100.65"></script><script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script><!-- Facebook Pixel Code --><script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');fbq('init', '853339751421540');fbq('track', 'PageView');</script><noscript><img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=853339751421540&ev=PageView&noscript=1"/></noscript><!-- End Facebook Pixel Code -->           <!--  Analytics Code Begin -->    <!-- Begin comScore Tag -->    <script>        var _comscore = _comscore || [];        _comscore.push({ c1: "2", c2: "6036484" });        (function() {            var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";            el.parentNode.insertBefore(s, el);        })();    </script>    <noscript>    <img src="http://b.scorecardresearch.com/p?c1=2&c2=6036484&cv=2.0&cj=1" />    </noscript>    <!-- End comScore Tag -->    <!-- Begin Google Analytics Tag -->    <script>        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');        ga('create', 'UA-198011-6', 'indiatimes.com');            ga('require', 'displayfeatures');        ga('send', 'pageview');    </script>    <!-- End Google Analytics Tag -->    <!--  Analytics Code Begin --><!--adcode common script--><script type='text/javascript' src="http://ad.crwdcntrl.net/5/c=2800/pe=y/var=_ccaud"></script><script type="text/javascript">    var googletag = googletag || {};    googletag.cmd = googletag.cmd || [];    (function() {        var gads = document.createElement("script");        gads.async = true;        gads.type = "text/javascript";        var useSSL = "https:" == document.location.protocol;        gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";        var node =document.getElementsByTagName("script")[0];        node.parentNode.insertBefore(gads, node);    })();</script> <script type="text/javascript">        var nid='';        var cid='';        var sid='';        var brand_name='';                                                             </script>    <script>
    var contType = "";
    </script>
        <script type="text/javascript" src="http://media.indiatimes.in/resources/js/banner/home.js?v=100.65"></script>
            
<!--[if lt IE 9]>
  <script type="text/javascript" src="http://media.indiatimes.in/js/html5.js?v1.1"></script>
  <![endif]-->
<script>
var isDetailPage = 0</script>
</head>
<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];   
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=117787264903013&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script> 	
	
<div id="wrap" class="cf"><!--wrap start-->
    <div class='darkDisabledBg' id='disabledBg'></div>
<header>
    <div class="header-inner">
        <div class="comm-header ">
            <nav>
                <div class='forTop'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>

                    <div class="lnks">
                                                        <a href="http://www.indiatimes.com/news/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','News');">News</a> 
                                                               <a href="http://www.indiatimes.com/lifestyle/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Lifestyle');">Lifestyle</a> 
                                                               <a href="http://www.indiatimes.com/entertainment/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Entertainment');">Entertainment</a> 
                                                               <a href="http://www.indiatimes.com/health/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Health');">Health</a> 
                                                               <a href="http://www.indiatimes.com/videocafe/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Videos');">Videos</a> 
                                                                                          <a href='http://www.indiatimes.com/trending'  target="_blank" class="#fffff" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Trending');">Trending</a> 
                                                </div>
                </div>
                <div class='onScrolled'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>
                    <a href="javascript:;" class="menu rc sprite" id="headerMenu">Menu</a>
                    <div class="has-tag">
                                           </div>
                </div>

                                
            </nav>
			                 
	    <div id="sticker" style="display:none;"><span class="readTitle">
				</span></div>
                    <div class="socls fr share">
            </div>
                        
            
			 <div class="social fr">
                            <a title="facebook" target="_blank"  class="sprite fb" href="https://www.facebook.com/indiatimes">facebook</a>
                            <a title="twitter" target="_blank"  class="sprite twt" href="https://twitter.com/indiatimes">twitter</a>
                            <a title="search"  class="sprite serach" onclick="$('body').showSearch();" href="javascript:void(0);">search</a>
                        </div>
        </div>
    </div>
</header>
<div class="clr"></div>
<div id="overlay" class="animated bounceOut"><!--overlay start-->
    <div class="search_conts"><!--search-cont start-->
        <form onsubmit="return false;" class="serach-form">
            <input type="text" onkeyup="search_data();" class="input" onfocus="if(this.value=='Search'){this.value=''}" onblur="if(this.value==''){this.value='Search'};" value="Search" name="q" id="q" autocomplete="off">
            <input type="button" class="sprite src-btn" onclick="search_data();">
            <a class="sprite sclose" id="close" href="javascript:void(0);">X</a>
        </form>
        <div class="scroll-panes" id="scroll-panes">
            <div class="clr"></div>
            <div class="res-msg" id="res-msg"></div>
            <div class="search-list cf" id="results"></div>
            <div class="clr"></div>
            <div class="loadmore" id="progressBar" style="display: none;">Loading......</div>
            <a class="loadmore" style="display:none;" onclick="load_search_data(); " id="load_more_button" href="javascript:void(0);">Load more</a>
        </div>
    </div>
</div>
<div class="blk">
    <div  id="leftMenu" class="leftMenu accordion inner">
        <dl >
            <dt data-color="red-bg" class=""><a href="http://www.indiatimes.com">Home</a></dt>
            <dd> </dd>
                            <dt  data-color="blue-bg" ><a href="http://www.indiatimes.com/news/">News</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/news/india/'>India</a>
                                     
                            <a href='http://www.indiatimes.com/news/world/'>World</a>
                                     
                            <a href='http://www.indiatimes.com/news/sports/'>Sports</a>
                                     
                            <a href='http://www.indiatimes.com/news/weird/'>Weird</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="purple-bg" ><a href="http://www.indiatimes.com/lifestyle/">Lifestyle</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/lifestyle/self/'>Self</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/style/'>Style</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                     
                            <a href='http://www.indiatimes.com/culture/who-we-are/'>Who we are</a>
                                     
                            <a href='http://www.indiatimes.com/culture/travel/'>Travel</a>
                                     
                            <a href='http://www.indiatimes.com/culture/food/'>Food</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="red-bg" ><a href="http://www.indiatimes.com/entertainment/">Entertainment</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/entertainment/bollywood/'>Bollywood</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/celebs/'>Celebscoop</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/hollywood/'>Hollywood</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="green-bg" ><a href="http://www.indiatimes.com/health/">Health</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                     
                            <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                     
                            <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                     
                            <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                     
                            <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="orange-bg" ><a href="http://www.indiatimes.com/videocafe/">Videos</a>
                                </dt>
                				<dt data-color="" class=""><a href="http://www.indiatimes.com/trending/">Trending</a>
                           </dt>
            <dt class="pink-bg">
            <div class="follow">Follow indiatimes </div>
            <div class="follow_cont"> 
                <a href="https://www.facebook.com/indiatimes" class="fbleft sprite"> facebook</a> 
                <a href="https://twitter.com/indiatimes" class="twtleft sprite"> twitter</a> 
            </div>
            </dt>

            <dd>&nbsp;</dd>
            <div class="clr"></div>
        </dl>
    </div>
</div>


<div class="clr"></div>
<div class="dummy-cont">&nbsp;</div>
<div class="clr"></div>


<div id="pushdown">
    <div class="pull-ad"><!--pull-ad start-->   
	
                <script type="text/javascript"> 
                    showHeaderCode();
                </script>
	    
</div><!--pull-ad end-->
</div>
    <!--testing 15-12-03 23:50:26-->  

    <section id="hp_block_1" class="container cf" style="border: 0px solid red;"><!--container start-->
                

        <div class="news-panel cf"><!--news-panel start-->
            <h2>Big news</h2>

            <div class="feature-list cf"><!--feature-list start-->	
                                    <figure>
                        <div class="hash-tag">
                             1 day ago                         </div>
						 						          
                        <a href="http://www.indiatimes.com/news/india/vijay-pingale-was-the-ias-officer-who-was-transferred-away-from-chennai-corporation-cause-he-warned-about-chennaifloods-247991.html" class=" tint" title="IAS Officer Vijay Pingale Was Transferred Away From Chennai Corporation Because He Warned About #ChennaiFloods">
                            <img  class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Dec/chenni_1449060324_1449060351_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Dec/chenni_1449060324_1449060351_236x111.jpg"  border="0" alt="IAS Officer Vijay Pingale Was Transferred Away From Chennai Corporation Because He Warned About #ChennaiFloods"/></a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/vijay-pingale-was-the-ias-officer-who-was-transferred-away-from-chennai-corporation-cause-he-warned-about-chennaifloods-247991.html" title="IAS Officer Vijay Pingale Was Transferred Away From Chennai Corporation Because He Warned About #ChennaiFloods">
                            IAS Officer Vijay Pingale Was Transferred Away From Chennai Corporation Because He Warned About #ChennaiFloods                        </a>
                    </figcaption>
                            </div><!--feature-list end-->

                            <div class="news-panel-list cf" id="column1_0"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            2 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/india/pib-photoshops-an-image-of-pm-modi-doing-an-aerial-survey-of-flood-hit-chennai-fail-248031.html" title="PIB Photoshops An Image Of PM Modi Doing An Aerial Survey Of Flood-Hit Chennai! #Fail" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Dec/modi-chennai502_1449158274_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Dec/modi-chennai502_1449158274_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/pib-photoshops-an-image-of-pm-modi-doing-an-aerial-survey-of-flood-hit-chennai-fail-248031.html" title="PIB Photoshops An Image Of PM Modi Doing An Aerial Survey Of Flood-Hit Chennai! #Fail">
                            PIB Photoshops An Image Of PM Modi Doing An Aerial Survey Of Flood-Hit Chennai! #Fail                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_1"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            5 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/world/new-survey-reveals-top-10-ignorant-countries-of-the-world-guess-where-india-is-on-the-list-248022.html" title="New Survey Reveals Top 10 Ignorant Countries Of The World. Guess Where India Is On The List!" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Dec/india5_1449144764_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Dec/india5_1449144764_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/new-survey-reveals-top-10-ignorant-countries-of-the-world-guess-where-india-is-on-the-list-248022.html" title="New Survey Reveals Top 10 Ignorant Countries Of The World. Guess Where India Is On The List!">
                            New Survey Reveals Top 10 Ignorant Countries Of The World. Guess Where India Is On The List!                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_2"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            5 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/sports/harbhajan-singh-and-yuvraj-singh-are-taking-a-road-trip-together-and-it-looks-like-a-lot-of-fun-248030.html" title="Harbhajan Singh And Yuvraj Singh Are Taking A Road Trip Together And It Looks Like A Lot Of Fun" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Dec/bhajjiyuvimap_1449144215_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Dec/bhajjiyuvimap_1449144215_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/harbhajan-singh-and-yuvraj-singh-are-taking-a-road-trip-together-and-it-looks-like-a-lot-of-fun-248030.html" title="Harbhajan Singh And Yuvraj Singh Are Taking A Road Trip Together And It Looks Like A Lot Of Fun">
                            Harbhajan Singh And Yuvraj Singh Are Taking A Road Trip Together And It Looks Like A Lot Of Fun                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_3"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            5 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/india/this-bengaluru-man-shows-india-how-its-done-he-s-driving-from-bengaluru-to-chennai-with-relief-material-248027.html" title="This Bengaluru Man Shows India How Its Done - He's Driving From Bengaluru To Chennai With Relief Material!" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Dec/chennai-card_1449145472_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Dec/chennai-card_1449145472_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/this-bengaluru-man-shows-india-how-its-done-he-s-driving-from-bengaluru-to-chennai-with-relief-material-248027.html" title="This Bengaluru Man Shows India How Its Done - He's Driving From Bengaluru To Chennai With Relief Material!">
                            This Bengaluru Man Shows India How Its Done - He's Driving From Bengaluru To Chennai With Relief Material!                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                    </div><!--news-panel end-->

        <div class="life-panel cf" id="middleBlock1"><!--life-panel start-->
            <h2>lifestyle</h2>

                                <div class="life-panel-list cf"><!--life-panel-list start-->
                        <figure><span class="label-tag spon">Partner</span>
                            
                               
                            <a href="http://www.indiatimes.com/culture/travel/10-posters-only-an-explorer-at-heart-will-relate-to-247914.html" class=" tint" title="" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/travel/10-posters-only-an-explorer-at-heart-will-relate-to-247914.html');">
                                <img  class="greyBg23 lazy23"  src="http://media.indiatimes.in/media/content/2015/Dec/ranbir-kapoor_1448957408_1448957425_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Dec/ranbir-kapoor_1448957408_1448957425_502x234.jpg"  border="0" alt="10 Posters Only An Explorer At Heart Will Relate To" class="img-responsive"/>
                            </a>
                        </figure>
                        <figcaption>
                            <a href="http://www.indiatimes.com/culture/travel/10-posters-only-an-explorer-at-heart-will-relate-to-247914.html" title="10 Posters Only An Explorer At Heart Will Relate To" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/travel/10-posters-only-an-explorer-at-heart-will-relate-to-247914.html');">10 Posters Only An Explorer At Heart Will Relate To</a>
                        </figcaption>
                    </div><!--life-panel-list end-->
                                    <div class="life-panel-list cf"><!--life-panel-list start-->
                        <figure><span class="label-tag spon">Partner</span>
                            
                               
                            <a href="http://www.indiatimes.com/culture/travel/5-things-to-do-before-you-speak-to-your-travel-agent-247892.html" class=" tint" title="" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/travel/5-things-to-do-before-you-speak-to-your-travel-agent-247892.html');">
                                <img  class="greyBg23 lazy23"  src="http://media.indiatimes.in/media/content/2015/Dec/card_1449052778_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Dec/card_1449052778_502x234.jpg"  border="0" alt="5 Things To Do Before You Speak To Your Travel Agent" class="img-responsive"/>
                            </a>
                        </figure>
                        <figcaption>
                            <a href="http://www.indiatimes.com/culture/travel/5-things-to-do-before-you-speak-to-your-travel-agent-247892.html" title="5 Things To Do Before You Speak To Your Travel Agent" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/travel/5-things-to-do-before-you-speak-to-your-travel-agent-247892.html');">5 Things To Do Before You Speak To Your Travel Agent</a>
                        </figcaption>
                    </div><!--life-panel-list end-->
                

                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                   
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/celebs/11-revelations-rimi-sen-made-after-her-eviction-from-bigg-boss-9_-248016.html" class="tint" title="11 Revelations Rimi Sen Made After Her Eviction From Bigg Boss 9" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/entertainment/celebs/11-revelations-rimi-sen-made-after-her-eviction-from-bigg-boss-9_-248016.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Dec/rimi-priya-c_1449143871_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Dec/rimi-priya-c_1449143871_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/11-revelations-rimi-sen-made-after-her-eviction-from-bigg-boss-9_-248016.html" title="11 Revelations Rimi Sen Made After Her Eviction From Bigg Boss 9" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/entertainment/celebs/11-revelations-rimi-sen-made-after-her-eviction-from-bigg-boss-9_-248016.html');">
                            11 Revelations Rimi Sen Made After Her Eviction From Bigg Boss 9                        </a>
                    </figcaption>
                </div>
                        
        </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_0">
                    <span class="strip skyblue-bg"></span>                    <figure>

                                                <a href="http://www.indiatimes.com/entertainment/the-entire-system-has-collapsed-says-kamal-haasan-in-a-distressed-message-from-chennai-248013.html" class="tint" title="The Entire System Has Collapsed, Says Kamal Haasan In A Distressed Message From Chennai!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Dec/kamal_1449134835_1449134841_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Dec/kamal_1449134835_1449134841_218x102.jpg" border="0" alt="The Entire System Has Collapsed, Says Kamal Haasan In A Distressed Message From Chennai!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/the-entire-system-has-collapsed-says-kamal-haasan-in-a-distressed-message-from-chennai-248013.html" title="The Entire System Has Collapsed, Says Kamal Haasan In A Distressed Message From Chennai!">
                            The Entire System Has Collapsed, Says Kamal Haasan In A Distressed Message From Chennai!                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_1">
                                        <figure>

                                                <a href="http://www.indiatimes.com/entertainment/bollywood/farhan-akhtar-speaks-out-on-the-national-anthem-row-says-he-will-always-stand-when-it-plays-248004.html" class="tint" title="Farhan Akhtar Speaks Out On The National Anthem Row, Says He Will Always Stand When It Plays">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Dec/card_1449129470_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Dec/card_1449129470_218x102.jpg" border="0" alt="Farhan Akhtar Speaks Out On The National Anthem Row, Says He Will Always Stand When It Plays"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/farhan-akhtar-speaks-out-on-the-national-anthem-row-says-he-will-always-stand-when-it-plays-248004.html" title="Farhan Akhtar Speaks Out On The National Anthem Row, Says He Will Always Stand When It Plays">
                            Farhan Akhtar Speaks Out On The National Anthem Row, Says He Will Always Stand When It Plays                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_2">
                                        <figure>

                                                <a href="http://www.indiatimes.com/entertainment/celebs/farmers-in-uttarakhand-are-using-honey-singh-s-music-to-scare-away-wild-boars-and-it-s-actually-working-247974.html" class="tint" title="Farmers In Uttarakhand Are Using Honey Singh's Music To Scare Away Wild Boars And It's Actually Working!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Dec/yo-yo-honey-singh-style_1449051181_1449051192_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Dec/yo-yo-honey-singh-style_1449051181_1449051192_218x102.jpg" border="0" alt="Farmers In Uttarakhand Are Using Honey Singh's Music To Scare Away Wild Boars And It's Actually Working!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/farmers-in-uttarakhand-are-using-honey-singh-s-music-to-scare-away-wild-boars-and-it-s-actually-working-247974.html" title="Farmers In Uttarakhand Are Using Honey Singh's Music To Scare Away Wild Boars And It's Actually Working!">
                            Farmers In Uttarakhand Are Using Honey Singh's Music To Scare Away Wild Boars And It's Actually Working!                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_3">
                                        <figure>

                                                <a href="http://www.indiatimes.com/lifestyle/self/16-moments-that-prove-love-is-about-all-the-little-things-247986.html" class="tint" title="16 Moments That Prove Love Is About All The Little Things">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Dec/card1_1449059296_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Dec/card1_1449059296_218x102.jpg" border="0" alt="16 Moments That Prove Love Is About All The Little Things"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/16-moments-that-prove-love-is-about-all-the-little-things-247986.html" title="16 Moments That Prove Love Is About All The Little Things">
                            16 Moments That Prove Love Is About All The Little Things                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_4">
                                        <figure>

                                                <a href="http://www.indiatimes.com/entertainment/hollywood/these-pictures-of-celebrities-depicted-as-victims-of-domestic-violence-will-make-you-sit-up-straight-248025.html" class="tint" title="These Pictures Of Celebrities Depicted As Victims Of Domestic Violence Will Make You Sit Up Straight In Shock">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Dec/picmonkey-collage_1449143782_1449143799_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Dec/picmonkey-collage_1449143782_1449143799_218x102.jpg" border="0" alt="These Pictures Of Celebrities Depicted As Victims Of Domestic Violence Will Make You Sit Up Straight In Shock"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/hollywood/these-pictures-of-celebrities-depicted-as-victims-of-domestic-violence-will-make-you-sit-up-straight-248025.html" title="These Pictures Of Celebrities Depicted As Victims Of Domestic Violence Will Make You Sit Up Straight In Shock">
                            These Pictures Of Celebrities Depicted As Victims Of Domestic Violence Will Make You Sit Up Straight In Shock                        </a>
                    </figcaption>
                </div>
                        </div><!--trending-panel end-->
    </section><!--container end-->

<!-- -------------------    end Block 1  ------------------------------------- -->
<section class="big-ads" id="ad1"> 
<div id="bigAd1_slot"></div>
    <script>        
        showBigAD1('bigAd1_slot');
    </script> 
</section>
<!-- -------------------    start Block 2  ------------------------------------- -->

    <section id="hp_block_2" class="container cf" style="border: 0px solid red;"><!--container start-->
        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
                            <div class="news-panel-list cf" id="column1_4"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            5 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/while-the-nation-stands-with-chennai-this-news-channel-decided-to-trivialise-the-situation-fail-248026.html" title="While The Nation Stands With Chennai, This News Channel Decided To Trivialise The Situation #Fail" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/flood3_1449144597_1449144606_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/while-the-nation-stands-with-chennai-this-news-channel-decided-to-trivialise-the-situation-fail-248026.html" title="While The Nation Stands With Chennai, This News Channel Decided To Trivialise The Situation #Fail">
                            While The Nation Stands With Chennai, This News Channel Decided To Trivialise The Situation #Fail                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_5"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            6 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/madras-high-court-just-prescribed-a-dress-code-for-those-wanting-to-visit-temples-in-tamil-nadu-248021.html" title="Madras High Court Just Prescribed A Dress Code For Those Wanting To Visit Temples In Tamil Nadu" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/temple-card_1449142772_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/madras-high-court-just-prescribed-a-dress-code-for-those-wanting-to-visit-temples-in-tamil-nadu-248021.html" title="Madras High Court Just Prescribed A Dress Code For Those Wanting To Visit Temples In Tamil Nadu">
                            Madras High Court Just Prescribed A Dress Code For Those Wanting To Visit Temples In Tamil Nadu                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_6"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            7 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/world/these-16-tech-tycoons-have-pledged-the-majority-of-their-wealth-to-charity-respect-248017.html" title="These 16 Tech Tycoons Have Pledged The Majority Of Their Wealth To Charity #Respect" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1449140621_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/world/these-16-tech-tycoons-have-pledged-the-majority-of-their-wealth-to-charity-respect-248017.html" title="These 16 Tech Tycoons Have Pledged The Majority Of Their Wealth To Charity #Respect">
                            These 16 Tech Tycoons Have Pledged The Majority Of Their Wealth To Charity #Respect                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_7"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            8 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/sports/sania-mirza-justifies-demanding-private-jet-for-madhya-pradesh-function-blames-tight-schedule-248018.html" title="Sania Mirza Justifies Demanding Private Jet For Madhya Pradesh Function, Blames Tight Schedule" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/saniatalksbccl_1449138913_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/sports/sania-mirza-justifies-demanding-private-jet-for-madhya-pradesh-function-blames-tight-schedule-248018.html" title="Sania Mirza Justifies Demanding Private Jet For Madhya Pradesh Function, Blames Tight Schedule">
                            Sania Mirza Justifies Demanding Private Jet For Madhya Pradesh Function, Blames Tight Schedule                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_8"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            8 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/11-photos-that-show-how-delhi-s-air-pollution-has-gone-from-bad-to-life-threatening-248011.html" title="11 Photos That Show How Delhi's Air Pollution Has Gone From Bad To Life-Threatening" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/502_1449133448_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/11-photos-that-show-how-delhi-s-air-pollution-has-gone-from-bad-to-life-threatening-248011.html" title="11 Photos That Show How Delhi's Air Pollution Has Gone From Bad To Life-Threatening">
                            11 Photos That Show How Delhi's Air Pollution Has Gone From Bad To Life-Threatening                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/health/recipes/check-out-these-6-easy-quick-and-healthy-snack-and-dessert-recipes-247972.html'>video</a>                          
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/recipes/check-out-these-6-easy-quick-and-healthy-snack-and-dessert-recipes-247972.html" class="tint" title="Check Out These 6 Easy, Quick And Healthy Snack And Dessert Recipes!">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/cover_1449048865_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/recipes/check-out-these-6-easy-quick-and-healthy-snack-and-dessert-recipes-247972.html" title="Check Out These 6 Easy, Quick And Healthy Snack And Dessert Recipes!">
                            Check Out These 6 Easy, Quick And Healthy Snack And Dessert Recipes!                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/bollywood/20-short-films-that-are-way-too-beautiful-to-miss-yes-you-can-watch-them-right-here-247970.html" class="tint" title="20 Short Films That Are Way Too Beautiful To Miss. Yes, You Can Watch Them Right Here!">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/films-card_1449057789_1449057797_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/20-short-films-that-are-way-too-beautiful-to-miss-yes-you-can-watch-them-right-here-247970.html" title="20 Short Films That Are Way Too Beautiful To Miss. Yes, You Can Watch Them Right Here!">
                            20 Short Films That Are Way Too Beautiful To Miss. Yes, You Can Watch Them Right Here!                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/culture/travel/18-things-you-miss-the-most-after-a-roadtrip-248014.html" class="tint" title="18 Reasons Why You Need To Drop Everything And Take A Roadtrip Right Now!">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card-11_1449159196_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/travel/18-things-you-miss-the-most-after-a-roadtrip-248014.html" title="18 Reasons Why You Need To Drop Everything And Take A Roadtrip Right Now!">
                            18 Reasons Why You Need To Drop Everything And Take A Roadtrip Right Now!                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_5"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/lifestyle/self/11-things-you-ll-relate-to-if-your-relationship-just-became-a-long-distance-one-247870.html" class="tint" title="11 Things Youâll Relate To If Your Relationship Just Became A Long-Distance One">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card-image-1_1448867831_218x102.jpg" border="0" alt="11 Things Youâll Relate To If Your Relationship Just Became A Long-Distance One"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/11-things-you-ll-relate-to-if-your-relationship-just-became-a-long-distance-one-247870.html" title="11 Things Youâll Relate To If Your Relationship Just Became A Long-Distance One">
                            11 Things Youâll Relate To If Your Relationship Just Became A Long-Distance One                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_6"><!--trending-panel-list start-->
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/shahrukh-and-kajol-bring-back-old-school-romance-in-this-new-track-from-dilwale-248019.html'>video</a>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/shahrukh-and-kajol-bring-back-old-school-romance-in-this-new-track-from-dilwale-248019.html" class="tint" title="Shahrukh And Kajol Bring Back Old School Romance In This New Track From 'Dilwale'!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/screenshot_3_1449141912_1449141916_218x102.jpg" border="0" alt="Shahrukh And Kajol Bring Back Old School Romance In This New Track From 'Dilwale'!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/shahrukh-and-kajol-bring-back-old-school-romance-in-this-new-track-from-dilwale-248019.html" title="Shahrukh And Kajol Bring Back Old School Romance In This New Track From 'Dilwale'!">
                            Shahrukh And Kajol Bring Back Old School Romance In This New Track From 'Dilwale'!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_7"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/lifestyle/self/17-quotes-by-the-greats-that-will-change-the-way-you-look-at-money-247888.html" class="tint" title="17 Quotes By The Greats That Will Change The Way You Look At Money">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1448952588_218x102.jpg" border="0" alt="17 Quotes By The Greats That Will Change The Way You Look At Money"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/17-quotes-by-the-greats-that-will-change-the-way-you-look-at-money-247888.html" title="17 Quotes By The Greats That Will Change The Way You Look At Money">
                            17 Quotes By The Greats That Will Change The Way You Look At Money                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_8"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/celebs/meet-sasha-chettri-the-airtel-4g-girl-yes-the-one-internet-loves-to-mock-247987.html" class="tint" title="Meet Sasha Chettri - The Airtel 4G Girl. Yes, The One Internet Loves To Mock!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/airtel-ca_1449121395_1449121404_218x102.jpg" border="0" alt="Meet Sasha Chettri - The Airtel 4G Girl. Yes, The One Internet Loves To Mock!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/meet-sasha-chettri-the-airtel-4g-girl-yes-the-one-internet-loves-to-mock-247987.html" title="Meet Sasha Chettri - The Airtel 4G Girl. Yes, The One Internet Loves To Mock!">
                            Meet Sasha Chettri - The Airtel 4G Girl. Yes, The One Internet Loves To Mock!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_9"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/health/buzz/eating-veggies-like-potatoes-onions-and-cauliflower-could-reduce-your-risk-of-stomach-cancer-247918.html" class="tint" title="Eating Veggies Like Potatoes, Onions And Cauliflower Could Reduce Your Risk Of Stomach Cancer!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/cover_1448959961_218x102.jpg" border="0" alt="Eating Veggies Like Potatoes, Onions And Cauliflower Could Reduce Your Risk Of Stomach Cancer!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/buzz/eating-veggies-like-potatoes-onions-and-cauliflower-could-reduce-your-risk-of-stomach-cancer-247918.html" title="Eating Veggies Like Potatoes, Onions And Cauliflower Could Reduce Your Risk Of Stomach Cancer!">
                            Eating Veggies Like Potatoes, Onions And Cauliflower Could Reduce Your Risk Of Stomach Cancer!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div>

    </section><!--container end-->

<!--------------------------- end Block2 ------------------------------------------>
<section class="big-ads" id="ad2"> 
<div id="bigAd2_slot"></div>
</section>
<!--------------------------- start Block3 ------------------------------------------>

    <section id="hp_block_3" class="container cf"><!--container start-->

        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
          
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_9">
                    <figure>
                        <div class="hash-tag">
                            9 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/now-practising-yoga-can-get-you-out-of-jail-quicker-248008.html" title="Now Practising Yoga Can Get You Out Of Jail Quicker!" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/jail-yoga-card_1449130408_236x111.jpg" border="0" alt="Now Practising Yoga Can Get You Out Of Jail Quicker!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/now-practising-yoga-can-get-you-out-of-jail-quicker-248008.html" title="Now Practising Yoga Can Get You Out Of Jail Quicker!">
                            Now Practising Yoga Can Get You Out Of Jail Quicker!                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_10">
                    <figure>
                        <div class="hash-tag">
                            10 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/11-facts-about-dr-rajendra-prasad-india-s-first-president-on-his-131st-birth-anniversary-248000.html" title="11 Facts About Dr Rajendra Prasad, India's First President, On His 131st Birth Anniversary" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/raju5_1449126254_236x111.jpg" border="0" alt="11 Facts About Dr Rajendra Prasad, India's First President, On His 131st Birth Anniversary"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/11-facts-about-dr-rajendra-prasad-india-s-first-president-on-his-131st-birth-anniversary-248000.html" title="11 Facts About Dr Rajendra Prasad, India's First President, On His 131st Birth Anniversary">
                            11 Facts About Dr Rajendra Prasad, India's First President, On His 131st Birth Anniversary                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_11">
                    <figure>
                        <div class="hash-tag">
                            10 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/isis-rakes-up-dadri-lynching-vows-to-expand-war-against-india-248005.html" title="ISIS Sets Sight On Modi, Says He Is Preparing His People For War Against Muslims" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/modi-card_1449132565_236x111.jpg" border="0" alt="ISIS Sets Sight On Modi, Says He Is Preparing His People For War Against Muslims"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/isis-rakes-up-dadri-lynching-vows-to-expand-war-against-india-248005.html" title="ISIS Sets Sight On Modi, Says He Is Preparing His People For War Against Muslims">
                            ISIS Sets Sight On Modi, Says He Is Preparing His People For War Against Muslims                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_12">
                    <figure>
                        <div class="hash-tag">
                            10 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/sports/rohit-sharma-invites-prime-minister-narendra-modi-for-his-wedding-gets-out-for-1_-248006.html" title="Rohit Sharma Invites Prime Minister Narendra Modi For His Wedding, Gets Out For 1" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/rohitmodiout_1449131884_236x111.jpg" border="0" alt="Rohit Sharma Invites Prime Minister Narendra Modi For His Wedding, Gets Out For 1"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/rohit-sharma-invites-prime-minister-narendra-modi-for-his-wedding-gets-out-for-1_-248006.html" title="Rohit Sharma Invites Prime Minister Narendra Modi For His Wedding, Gets Out For 1">
                            Rohit Sharma Invites Prime Minister Narendra Modi For His Wedding, Gets Out For 1                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_13">
                    <figure>
                        <div class="hash-tag">
                            11 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/after-delhi-and-pune-now-iit-patna-student-from-lucknow-gets-rs-1-8-crore-job-in-google-247999.html" title="After Delhi And Pune, Now IIT Patna Student From Lucknow Gets Rs 1.8 Crore Job In Google" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/google-card_1449125742_236x111.jpg" border="0" alt="After Delhi And Pune, Now IIT Patna Student From Lucknow Gets Rs 1.8 Crore Job In Google"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/after-delhi-and-pune-now-iit-patna-student-from-lucknow-gets-rs-1-8-crore-job-in-google-247999.html" title="After Delhi And Pune, Now IIT Patna Student From Lucknow Gets Rs 1.8 Crore Job In Google">
                            After Delhi And Pune, Now IIT Patna Student From Lucknow Gets Rs 1.8 Crore Job In Google                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/lifestyle/self/11-skills-you-can-pick-up-in-under-a-week-and-use-throughout-your-life-247905.html" class="tint" title="11 Skills You Can Pick Up In Under A Week And Use Throughout Your Life">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/cp_1448951976_1448951980_502x234.jpg" border="0" alt="http://www.indiatimes.com/lifestyle/self/11-skills-you-can-pick-up-in-under-a-week-and-use-throughout-your-life-247905.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/11-skills-you-can-pick-up-in-under-a-week-and-use-throughout-your-life-247905.html" title="11 Skills You Can Pick Up In Under A Week And Use Throughout Your Life">
                            11 Skills You Can Pick Up In Under A Week And Use Throughout Your Life                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/hollywood/these-pictures-of-celebrities-depicted-as-victims-of-domestic-violence-will-make-you-sit-up-straight-248025.html" class="tint" title="These Pictures Of Celebrities Depicted As Victims Of Domestic Violence Will Make You Sit Up Straight In Shock">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/picmonkey-collage_1449143782_1449143799_502x234.jpg" border="0" alt="http://www.indiatimes.com/entertainment/hollywood/these-pictures-of-celebrities-depicted-as-victims-of-domestic-violence-will-make-you-sit-up-straight-248025.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/hollywood/these-pictures-of-celebrities-depicted-as-victims-of-domestic-violence-will-make-you-sit-up-straight-248025.html" title="These Pictures Of Celebrities Depicted As Victims Of Domestic Violence Will Make You Sit Up Straight In Shock">
                            These Pictures Of Celebrities Depicted As Victims Of Domestic Violence Will Make You Sit Up Straight In Shock                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/shahrukh-and-kajol-bring-back-old-school-romance-in-this-new-track-from-dilwale-248019.html'>video</a>                          
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/bollywood/shahrukh-and-kajol-bring-back-old-school-romance-in-this-new-track-from-dilwale-248019.html" class="tint" title="Shahrukh And Kajol Bring Back Old School Romance In This New Track From 'Dilwale'!">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/screenshot_3_1449141912_1449141916_502x234.jpg" border="0" alt="http://www.indiatimes.com/entertainment/bollywood/shahrukh-and-kajol-bring-back-old-school-romance-in-this-new-track-from-dilwale-248019.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/shahrukh-and-kajol-bring-back-old-school-romance-in-this-new-track-from-dilwale-248019.html" title="Shahrukh And Kajol Bring Back Old School Romance In This New Track From 'Dilwale'!">
                            Shahrukh And Kajol Bring Back Old School Romance In This New Track From 'Dilwale'!                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_10"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/celebs/11-revelations-rimi-sen-made-after-her-eviction-from-bigg-boss-9_-248016.html" class="tint" title="11 Revelations Rimi Sen Made After Her Eviction From Bigg Boss 9">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/rimi-priya-c_1449143871_218x102.jpg" border="0" alt="11 Revelations Rimi Sen Made After Her Eviction From Bigg Boss 9"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/11-revelations-rimi-sen-made-after-her-eviction-from-bigg-boss-9_-248016.html" title="11 Revelations Rimi Sen Made After Her Eviction From Bigg Boss 9">
                            11 Revelations Rimi Sen Made After Her Eviction From Bigg Boss 9                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_11"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/bollywood/20-short-films-that-are-way-too-beautiful-to-miss-yes-you-can-watch-them-right-here-247970.html" class="tint" title="20 Short Films That Are Way Too Beautiful To Miss. Yes, You Can Watch Them Right Here!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/films-card_1449057789_1449057797_218x102.jpg" border="0" alt="20 Short Films That Are Way Too Beautiful To Miss. Yes, You Can Watch Them Right Here!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/20-short-films-that-are-way-too-beautiful-to-miss-yes-you-can-watch-them-right-here-247970.html" title="20 Short Films That Are Way Too Beautiful To Miss. Yes, You Can Watch Them Right Here!">
                            20 Short Films That Are Way Too Beautiful To Miss. Yes, You Can Watch Them Right Here!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_12"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/culture/travel/11-reasons-a-train-journey-beats-a-car-ride-hands-down-247971.html" class="tint" title="11 Reasons A Train Journey Beats A Car Ride Hands Down">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/jabwemet_1449046956_1449046977_218x102.jpg" border="0" alt="11 Reasons A Train Journey Beats A Car Ride Hands Down"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/travel/11-reasons-a-train-journey-beats-a-car-ride-hands-down-247971.html" title="11 Reasons A Train Journey Beats A Car Ride Hands Down">
                            11 Reasons A Train Journey Beats A Car Ride Hands Down                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_13"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/culture/travel/these-men-trekked-to-chandrataal-lake-to-clean-up-trash-left-behind-by-other-trekkers-inspiring-247933.html" class="tint" title="These Men Trekked To Chandrataal Lake To Clean Up Trash Left Behind By Other Trekkers! #Inspiring">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/chandrataal_card_new_1449043085_218x102.jpg" border="0" alt="These Men Trekked To Chandrataal Lake To Clean Up Trash Left Behind By Other Trekkers! #Inspiring"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/travel/these-men-trekked-to-chandrataal-lake-to-clean-up-trash-left-behind-by-other-trekkers-inspiring-247933.html" title="These Men Trekked To Chandrataal Lake To Clean Up Trash Left Behind By Other Trekkers! #Inspiring">
                            These Men Trekked To Chandrataal Lake To Clean Up Trash Left Behind By Other Trekkers! #Inspiring                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_14"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/health/healthyliving/5-winter-proof-exercises-because-hibernation-is-for-losers-247926.html" class="tint" title="5 Winter Proof Exercises, Because Hibernation Is For Losers">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1449048863_218x102.jpg" border="0" alt="5 Winter Proof Exercises, Because Hibernation Is For Losers"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/5-winter-proof-exercises-because-hibernation-is-for-losers-247926.html" title="5 Winter Proof Exercises, Because Hibernation Is For Losers">
                            5 Winter Proof Exercises, Because Hibernation Is For Losers                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div><!--trending-panel end-->

    </section><!--container end-->

<!------------------------------- end Block 3 ---------------------------------------->
<section class="big-ads">
<div id="bigAd3_slot"></div>
</section>

<!------------------------------- start Block4  -------------------------------------->

    <section class="container cf" id="container4"><!--container start-->
        <div class="news-panel cf ">
            <h2>news</h2>

                            <div class="news-panel-list cf" id="column1_14">
                    <figure>
                        <div class="hash-tag">
                            11 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/flipkart-s-discount-sales-cost-the-company-rs-2000-crore-this-year-247998.html" title="Flipkart's Discount Sales Cost The Company Rs 2,000 Crore This Year!" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1449123983_236x111.jpg" border="0" alt="Flipkart's Discount Sales Cost The Company Rs 2,000 Crore This Year!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/flipkart-s-discount-sales-cost-the-company-rs-2000-crore-this-year-247998.html" title="Flipkart's Discount Sales Cost The Company Rs 2,000 Crore This Year!">
                            Flipkart's Discount Sales Cost The Company Rs 2,000 Crore This Year!                        </a>
                    </figcaption> 
                </div>
                <div class='container1'>                <div class="news-panel-list cf" id="column1_15">
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/after-russia-france-and-the-usa-britain-launches-airstrikes-against-isis-in-syria-247996.html" title="After Russia, France And The USA, Britain Launches Airstrikes Against ISIS In Syria" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/raf502_1449121270_236x111.jpg" border="0" alt="After Russia, France And The USA, Britain Launches Airstrikes Against ISIS In Syria"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/after-russia-france-and-the-usa-britain-launches-airstrikes-against-isis-in-syria-247996.html" title="After Russia, France And The USA, Britain Launches Airstrikes Against ISIS In Syria">
                            After Russia, France And The USA, Britain Launches Airstrikes Against ISIS In Syria                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_16">
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/sports/virender-sehwag-thanks-sachin-tendulkar-rahul-dravid-anil-kumble-at-kotla-felicitation-skips-ms-dhoni-s-name-in-speech-247997.html" title="Virender Sehwag Thanks Sachin, Dravid, Ganguly And Kumble At Kotla Felicitation, Skips Dhoni's Name In Speech" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/sehwagbccikotla_1449120995_236x111.jpg" border="0" alt="Virender Sehwag Thanks Sachin, Dravid, Ganguly And Kumble At Kotla Felicitation, Skips Dhoni's Name In Speech"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/virender-sehwag-thanks-sachin-tendulkar-rahul-dravid-anil-kumble-at-kotla-felicitation-skips-ms-dhoni-s-name-in-speech-247997.html" title="Virender Sehwag Thanks Sachin, Dravid, Ganguly And Kumble At Kotla Felicitation, Skips Dhoni's Name In Speech">
                            Virender Sehwag Thanks Sachin, Dravid, Ganguly And Kumble At Kotla Felicitation, Skips Dhoni's Name In Speech                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_17">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/these-images-of-a-gulfstream-jet-other-planes-getting-swept-away-will-make-you-realise-the-extent-of-the-chennai-floods-247995.html" title="These Images Of A Gulfstream & Other Planes Getting Swept Away Will Make You Realise The Extent Of The Chennai Floods!" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/chennai6_1449073911_1449073935_236x111.jpg" border="0" alt="These Images Of A Gulfstream & Other Planes Getting Swept Away Will Make You Realise The Extent Of The Chennai Floods!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/these-images-of-a-gulfstream-jet-other-planes-getting-swept-away-will-make-you-realise-the-extent-of-the-chennai-floods-247995.html" title="These Images Of A Gulfstream & Other Planes Getting Swept Away Will Make You Realise The Extent Of The Chennai Floods!">
                            These Images Of A Gulfstream & Other Planes Getting Swept Away Will Make You Realise The Extent Of The Chennai Floods!                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_18">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/it-firms-look-for-alternatives-and-backups-as-it-hub-is-drowns-in-chennaifloods-247994.html" title="IT Firms Look For Alternatives And Backups As IT Hub Drowns In #ChennaiFloods" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/colla_1449065587_1449065597_236x111.jpg" border="0" alt="IT Firms Look For Alternatives And Backups As IT Hub Drowns In #ChennaiFloods"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/it-firms-look-for-alternatives-and-backups-as-it-hub-is-drowns-in-chennaifloods-247994.html" title="IT Firms Look For Alternatives And Backups As IT Hub Drowns In #ChennaiFloods">
                            IT Firms Look For Alternatives And Backups As IT Hub Drowns In #ChennaiFloods                        </a>
                    </figcaption> 
                </div>
                 
        </div>
    </div><!--news-panel end-->

    <div class="life-panel cf container2" id="b4c2"><!--life-panel start-->
        <h2>lifestyle</h2>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/videos/10-stress-management-tips-you-can-learn-from-the-bhagavad-gita-247956.html" class="tint" title="10 Stress Management Tips You Can Learn From The Bhagavad Gita">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1449052499_502x234.jpg" border="0" alt="10 Stress Management Tips You Can Learn From The Bhagavad Gita" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/videos/10-stress-management-tips-you-can-learn-from-the-bhagavad-gita-247956.html" title="10 Stress Management Tips You Can Learn From The Bhagavad Gita">
                        10 Stress Management Tips You Can Learn From The Bhagavad Gita                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/the-entire-system-has-collapsed-says-kamal-haasan-in-a-distressed-message-from-chennai-248013.html" class="tint" title="The Entire System Has Collapsed, Says Kamal Haasan In A Distressed Message From Chennai!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/kamal_1449134835_1449134841_502x234.jpg" border="0" alt="The Entire System Has Collapsed, Says Kamal Haasan In A Distressed Message From Chennai!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/the-entire-system-has-collapsed-says-kamal-haasan-in-a-distressed-message-from-chennai-248013.html" title="The Entire System Has Collapsed, Says Kamal Haasan In A Distressed Message From Chennai!">
                        The Entire System Has Collapsed, Says Kamal Haasan In A Distressed Message From Chennai!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/16-moments-that-prove-love-is-about-all-the-little-things-247986.html" class="tint" title="16 Moments That Prove Love Is About All The Little Things">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card1_1449059296_502x234.jpg" border="0" alt="16 Moments That Prove Love Is About All The Little Things" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/16-moments-that-prove-love-is-about-all-the-little-things-247986.html" title="16 Moments That Prove Love Is About All The Little Things">
                        16 Moments That Prove Love Is About All The Little Things                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/dilwale-s-next-song-janam-janam-has-srk-kajol-romancing-in-raj-kapoor-nargis-style-248009.html" class="tint" title="Dilwale's Next Song 'Janam Janam' Has SRK-Kajol Romancing In Raj Kapoor-Nargis Style!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card1_1449131412_1449131422_502x234.jpg" border="0" alt="Dilwale's Next Song 'Janam Janam' Has SRK-Kajol Romancing In Raj Kapoor-Nargis Style!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/dilwale-s-next-song-janam-janam-has-srk-kajol-romancing-in-raj-kapoor-nargis-style-248009.html" title="Dilwale's Next Song 'Janam Janam' Has SRK-Kajol Romancing In Raj Kapoor-Nargis Style!">
                        Dilwale's Next Song 'Janam Janam' Has SRK-Kajol Romancing In Raj Kapoor-Nargis Style!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/10-weird-things-music-can-be-used-for-248007.html" class="tint" title="Honey Singh's Songs Are Used To Scare Wild Boars + 9 Other Uses Of Music You Didn't Know About!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/yo-yo-honey-singh-12a_1449129060_1449129071_502x234.jpg" border="0" alt="Honey Singh's Songs Are Used To Scare Wild Boars + 9 Other Uses Of Music You Didn't Know About!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/10-weird-things-music-can-be-used-for-248007.html" title="Honey Singh's Songs Are Used To Scare Wild Boars + 9 Other Uses Of Music You Didn't Know About!">
                        Honey Singh's Songs Are Used To Scare Wild Boars + 9 Other Uses Of Music You Didn't Know About!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/delhi-s-rising-pollution-levels-putting-people-with-respiratory-conditions-at-risk-warn-doctors-247975.html" class="tint" title="Delhi's Rising Pollution Is Putting People With Respiratory Conditions At Risk, Warn Doctors">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/cover_1449052996_502x234.jpg" border="0" alt="Delhi's Rising Pollution Is Putting People With Respiratory Conditions At Risk, Warn Doctors" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/delhi-s-rising-pollution-levels-putting-people-with-respiratory-conditions-at-risk-warn-doctors-247975.html" title="Delhi's Rising Pollution Is Putting People With Respiratory Conditions At Risk, Warn Doctors">
                        Delhi's Rising Pollution Is Putting People With Respiratory Conditions At Risk, Warn Doctors                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/farhan-akhtar-speaks-out-on-the-national-anthem-row-says-he-will-always-stand-when-it-plays-248004.html" class="tint" title="Farhan Akhtar Speaks Out On The National Anthem Row, Says He Will Always Stand When It Plays">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1449129470_502x234.jpg" border="0" alt="Farhan Akhtar Speaks Out On The National Anthem Row, Says He Will Always Stand When It Plays" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/farhan-akhtar-speaks-out-on-the-national-anthem-row-says-he-will-always-stand-when-it-plays-248004.html" title="Farhan Akhtar Speaks Out On The National Anthem Row, Says He Will Always Stand When It Plays">
                        Farhan Akhtar Speaks Out On The National Anthem Row, Says He Will Always Stand When It Plays                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/rgv-writes-about-sridevi-s-thunder-thighs-boney-kapoor-calls-him-a-pervert-248002.html" class="tint" title="RGV Writes About Sridevi's Thunder-thighs, Boney Kapoor Calls Him A Pervert!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1449126513_1449126525_502x234.jpg" border="0" alt="RGV Writes About Sridevi's Thunder-thighs, Boney Kapoor Calls Him A Pervert!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/rgv-writes-about-sridevi-s-thunder-thighs-boney-kapoor-calls-him-a-pervert-248002.html" title="RGV Writes About Sridevi's Thunder-thighs, Boney Kapoor Calls Him A Pervert!">
                        RGV Writes About Sridevi's Thunder-thighs, Boney Kapoor Calls Him A Pervert!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/the-2nd-trailer-of-batman-vs-superman-dawn-of-justice-is-out-we-can-t-stop-overdosing-on-it-248003.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/the-2nd-trailer-of-batman-vs-superman-dawn-of-justice-is-out-we-can-t-stop-overdosing-on-it-248003.html" class="tint" title="The 2nd Trailer Of Batman VS Superman: Dawn Of Justice Is Out & We Can't Stop Overdosing On It!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/card_1449126363_502x234.jpg" border="0" alt="The 2nd Trailer Of Batman VS Superman: Dawn Of Justice Is Out & We Can't Stop Overdosing On It!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/the-2nd-trailer-of-batman-vs-superman-dawn-of-justice-is-out-we-can-t-stop-overdosing-on-it-248003.html" title="The 2nd Trailer Of Batman VS Superman: Dawn Of Justice Is Out & We Can't Stop Overdosing On It!">
                        The 2nd Trailer Of Batman VS Superman: Dawn Of Justice Is Out & We Can't Stop Overdosing On It!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/5-winter-proof-exercises-because-hibernation-is-for-losers-247926.html" class="tint" title="5 Winter Proof Exercises, Because Hibernation Is For Losers">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1449048863_502x234.jpg" border="0" alt="5 Winter Proof Exercises, Because Hibernation Is For Losers" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/5-winter-proof-exercises-because-hibernation-is-for-losers-247926.html" title="5 Winter Proof Exercises, Because Hibernation Is For Losers">
                        5 Winter Proof Exercises, Because Hibernation Is For Losers                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/11-things-you-ll-relate-to-if-your-relationship-just-became-a-long-distance-one-247870.html" class="tint" title="11 Things Youâll Relate To If Your Relationship Just Became A Long-Distance One">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card-image-1_1448867831_502x234.jpg" border="0" alt="11 Things Youâll Relate To If Your Relationship Just Became A Long-Distance One" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/11-things-you-ll-relate-to-if-your-relationship-just-became-a-long-distance-one-247870.html" title="11 Things Youâll Relate To If Your Relationship Just Became A Long-Distance One">
                        11 Things Youâll Relate To If Your Relationship Just Became A Long-Distance One                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/travel/11-reasons-a-train-journey-beats-a-car-ride-hands-down-247971.html" class="tint" title="11 Reasons A Train Journey Beats A Car Ride Hands Down">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/jabwemet_1449046956_1449046977_502x234.jpg" border="0" alt="11 Reasons A Train Journey Beats A Car Ride Hands Down" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/travel/11-reasons-a-train-journey-beats-a-car-ride-hands-down-247971.html" title="11 Reasons A Train Journey Beats A Car Ride Hands Down">
                        11 Reasons A Train Journey Beats A Car Ride Hands Down                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/eating-veggies-like-potatoes-onions-and-cauliflower-could-reduce-your-risk-of-stomach-cancer-247918.html" class="tint" title="Eating Veggies Like Potatoes, Onions And Cauliflower Could Reduce Your Risk Of Stomach Cancer!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/cover_1448959961_502x234.jpg" border="0" alt="Eating Veggies Like Potatoes, Onions And Cauliflower Could Reduce Your Risk Of Stomach Cancer!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/eating-veggies-like-potatoes-onions-and-cauliflower-could-reduce-your-risk-of-stomach-cancer-247918.html" title="Eating Veggies Like Potatoes, Onions And Cauliflower Could Reduce Your Risk Of Stomach Cancer!">
                        Eating Veggies Like Potatoes, Onions And Cauliflower Could Reduce Your Risk Of Stomach Cancer!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/meet-sasha-chettri-the-airtel-4g-girl-yes-the-one-internet-loves-to-mock-247987.html" class="tint" title="Meet Sasha Chettri - The Airtel 4G Girl. Yes, The One Internet Loves To Mock!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/airtel-ca_1449121395_1449121404_502x234.jpg" border="0" alt="Meet Sasha Chettri - The Airtel 4G Girl. Yes, The One Internet Loves To Mock!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/meet-sasha-chettri-the-airtel-4g-girl-yes-the-one-internet-loves-to-mock-247987.html" title="Meet Sasha Chettri - The Airtel 4G Girl. Yes, The One Internet Loves To Mock!">
                        Meet Sasha Chettri - The Airtel 4G Girl. Yes, The One Internet Loves To Mock!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/the-angry-indian-goddesses-talk-about-why-they-are-angry-bollywood-cliches-and-the-need-for-female-buddy-films-247891.html" class="tint" title="The Angry Indian Goddesses Talk About Why They Are Angry, Bollywood Cliches And The Need For Female Buddy Films">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/aa_1448891199_1448891211_502x234.jpg" border="0" alt="The Angry Indian Goddesses Talk About Why They Are Angry, Bollywood Cliches And The Need For Female Buddy Films" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/the-angry-indian-goddesses-talk-about-why-they-are-angry-bollywood-cliches-and-the-need-for-female-buddy-films-247891.html" title="The Angry Indian Goddesses Talk About Why They Are Angry, Bollywood Cliches And The Need For Female Buddy Films">
                        The Angry Indian Goddesses Talk About Why They Are Angry, Bollywood Cliches And The Need For Female Buddy Films                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/this-is-how-rdb-actor-siddharth-is-turning-into-a-real-life-superhero-for-people-stranded-in-chennai-floods-247980.html" class="tint" title="This Is How RDB Actor Siddharth Is Turning Into A Real Life Superhero For People Stranded In Chennai Floods!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/chennai-floods-card_1449056418_1449056440_502x234.jpg" border="0" alt="This Is How RDB Actor Siddharth Is Turning Into A Real Life Superhero For People Stranded In Chennai Floods!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/this-is-how-rdb-actor-siddharth-is-turning-into-a-real-life-superhero-for-people-stranded-in-chennai-floods-247980.html" title="This Is How RDB Actor Siddharth Is Turning Into A Real Life Superhero For People Stranded In Chennai Floods!">
                        This Is How RDB Actor Siddharth Is Turning Into A Real Life Superhero For People Stranded In Chennai Floods!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/shaheen-bhatt-s-post-describing-sister-alia-bhatt-will-make-you-want-to-hug-yours-right-now-247976.html" class="tint" title="Shaheen Bhatt's Post Describing Sister Alia Bhatt Will Make You Want To Hug Yours Right Now!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/aliabhatt-shaheen_1449053914_1449053923_502x234.jpg" border="0" alt="Shaheen Bhatt's Post Describing Sister Alia Bhatt Will Make You Want To Hug Yours Right Now!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/shaheen-bhatt-s-post-describing-sister-alia-bhatt-will-make-you-want-to-hug-yours-right-now-247976.html" title="Shaheen Bhatt's Post Describing Sister Alia Bhatt Will Make You Want To Hug Yours Right Now!">
                        Shaheen Bhatt's Post Describing Sister Alia Bhatt Will Make You Want To Hug Yours Right Now!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-mother-elephant-tirelessly-rescued-her-baby-who-got-stuck-in-a-waterhole-hearttouching-247981.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-mother-elephant-tirelessly-rescued-her-baby-who-got-stuck-in-a-waterhole-hearttouching-247981.html" class="tint" title="This Mother Elephant Tirelessly Rescued Her Baby Who Got Stuck In A Waterhole! #HeartTouching">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/elephant_card_1449055131_502x234.jpg" border="0" alt="This Mother Elephant Tirelessly Rescued Her Baby Who Got Stuck In A Waterhole! #HeartTouching" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-mother-elephant-tirelessly-rescued-her-baby-who-got-stuck-in-a-waterhole-hearttouching-247981.html" title="This Mother Elephant Tirelessly Rescued Her Baby Who Got Stuck In A Waterhole! #HeartTouching">
                        This Mother Elephant Tirelessly Rescued Her Baby Who Got Stuck In A Waterhole! #HeartTouching                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/11-times-when-censor-board-proved-that-it-should-be-renamed-to-beep-beep-board-247959.html" class="tint" title="11 Times When Censor Board Proved That It Should Be Renamed To 'Beep Beep' Board!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1449050840_502x234.jpg" border="0" alt="11 Times When Censor Board Proved That It Should Be Renamed To 'Beep Beep' Board!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/11-times-when-censor-board-proved-that-it-should-be-renamed-to-beep-beep-board-247959.html" title="11 Times When Censor Board Proved That It Should Be Renamed To 'Beep Beep' Board!">
                        11 Times When Censor Board Proved That It Should Be Renamed To 'Beep Beep' Board!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/farmers-in-uttarakhand-are-using-honey-singh-s-music-to-scare-away-wild-boars-and-it-s-actually-working-247974.html" class="tint" title="Farmers In Uttarakhand Are Using Honey Singh's Music To Scare Away Wild Boars And It's Actually Working!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/yo-yo-honey-singh-style_1449051181_1449051192_502x234.jpg" border="0" alt="Farmers In Uttarakhand Are Using Honey Singh's Music To Scare Away Wild Boars And It's Actually Working!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/farmers-in-uttarakhand-are-using-honey-singh-s-music-to-scare-away-wild-boars-and-it-s-actually-working-247974.html" title="Farmers In Uttarakhand Are Using Honey Singh's Music To Scare Away Wild Boars And It's Actually Working!">
                        Farmers In Uttarakhand Are Using Honey Singh's Music To Scare Away Wild Boars And It's Actually Working!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-video-beautifully-explains-why-india-has-been-one-of-the-most-tolerant-countries-in-the-world-247969.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-video-beautifully-explains-why-india-has-been-one-of-the-most-tolerant-countries-in-the-world-247969.html" class="tint" title="This Video Beautifully Explains Why India Has Been One Of The Most Tolerant Countries In The World!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/india_card_1449046546_502x234.jpg" border="0" alt="This Video Beautifully Explains Why India Has Been One Of The Most Tolerant Countries In The World!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-video-beautifully-explains-why-india-has-been-one-of-the-most-tolerant-countries-in-the-world-247969.html" title="This Video Beautifully Explains Why India Has Been One Of The Most Tolerant Countries In The World!">
                        This Video Beautifully Explains Why India Has Been One Of The Most Tolerant Countries In The World!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/17-quotes-by-the-greats-that-will-change-the-way-you-look-at-money-247888.html" class="tint" title="17 Quotes By The Greats That Will Change The Way You Look At Money">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1448952588_502x234.jpg" border="0" alt="17 Quotes By The Greats That Will Change The Way You Look At Money" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/17-quotes-by-the-greats-that-will-change-the-way-you-look-at-money-247888.html" title="17 Quotes By The Greats That Will Change The Way You Look At Money">
                        17 Quotes By The Greats That Will Change The Way You Look At Money                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/watch-chennai-citizens-form-human-chain-to-rescue-biker-caught-in-the-deluge-welldone-247964.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/watch-chennai-citizens-form-human-chain-to-rescue-biker-caught-in-the-deluge-welldone-247964.html" class="tint" title="Watch: Chennai Citizens Form Human Chain To Rescue A Biker Caught In The Floods #WellDone">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/chennaipeople_card1_1449044489_502x234.jpg" border="0" alt="Watch: Chennai Citizens Form Human Chain To Rescue A Biker Caught In The Floods #WellDone" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/watch-chennai-citizens-form-human-chain-to-rescue-biker-caught-in-the-deluge-welldone-247964.html" title="Watch: Chennai Citizens Form Human Chain To Rescue A Biker Caught In The Floods #WellDone">
                        Watch: Chennai Citizens Form Human Chain To Rescue A Biker Caught In The Floods #WellDone                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/sugar-free-sweets-and-drinks-are-far-worse-for-your-teeth-than-you-think-247910.html" class="tint" title="Sugar-Free Sweets And Drinks Are Far Worse For Your Teeth Than You Think!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1448958342_502x234.jpg" border="0" alt="Sugar-Free Sweets And Drinks Are Far Worse For Your Teeth Than You Think!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/sugar-free-sweets-and-drinks-are-far-worse-for-your-teeth-than-you-think-247910.html" title="Sugar-Free Sweets And Drinks Are Far Worse For Your Teeth Than You Think!">
                        Sugar-Free Sweets And Drinks Are Far Worse For Your Teeth Than You Think!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/13-worst-dressed-bigg-boss-contestants-who-need-a-fashion-makeover-asap-247408.html" class="tint" title="13 Worst Dressed Bigg Boss Contestants Who Need A Fashion Makeover ASAP!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card-new_1449042395_1449042402_502x234.jpg" border="0" alt="13 Worst Dressed Bigg Boss Contestants Who Need A Fashion Makeover ASAP!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/13-worst-dressed-bigg-boss-contestants-who-need-a-fashion-makeover-asap-247408.html" title="13 Worst Dressed Bigg Boss Contestants Who Need A Fashion Makeover ASAP!">
                        13 Worst Dressed Bigg Boss Contestants Who Need A Fashion Makeover ASAP!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/11-times-ranveer-deepika-broke-all-cuteness-records-as-a-couple-245320.html" class="tint" title="11 Times Ranveer & Deepika's PDA Broke All Cuteness Records!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/ra1_1449041240_1449041249_502x234.jpg" border="0" alt="11 Times Ranveer & Deepika's PDA Broke All Cuteness Records!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/11-times-ranveer-deepika-broke-all-cuteness-records-as-a-couple-245320.html" title="11 Times Ranveer & Deepika's PDA Broke All Cuteness Records!">
                        11 Times Ranveer & Deepika's PDA Broke All Cuteness Records!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/arjun-kapoor-blames-mastani-deepika-for-keeping-him-and-ranveer-singh-apart-from-each-other-247955.html" class="tint" title="Arjun Kapoor Blames 'Mastani' Deepika For Keeping Him And Ranveer Singh Apart From Each Other!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card1_1449039541_1449039547_502x234.jpg" border="0" alt="Arjun Kapoor Blames 'Mastani' Deepika For Keeping Him And Ranveer Singh Apart From Each Other!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/arjun-kapoor-blames-mastani-deepika-for-keeping-him-and-ranveer-singh-apart-from-each-other-247955.html" title="Arjun Kapoor Blames 'Mastani' Deepika For Keeping Him And Ranveer Singh Apart From Each Other!">
                        Arjun Kapoor Blames 'Mastani' Deepika For Keeping Him And Ranveer Singh Apart From Each Other!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/salman-khan-sends-a-video-message-to-the-specially-abled-people-urges-cinema-halls-to-have-special-facilities-for-them-247949.html" class="tint" title="Salman Khan Sends A Video Message To The Specially Abled People, Urges Cinema Halls To Have Special Facilities For Them">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/picmonkey-collage_1449036499_1449036504_502x234.jpg" border="0" alt="Salman Khan Sends A Video Message To The Specially Abled People, Urges Cinema Halls To Have Special Facilities For Them" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/salman-khan-sends-a-video-message-to-the-specially-abled-people-urges-cinema-halls-to-have-special-facilities-for-them-247949.html" title="Salman Khan Sends A Video Message To The Specially Abled People, Urges Cinema Halls To Have Special Facilities For Them">
                        Salman Khan Sends A Video Message To The Specially Abled People, Urges Cinema Halls To Have Special Facilities For Them                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/in-a-first-two-cinema-halls-get-notices-for-playing-national-anthem-before-film-shows-247952.html" class="tint" title="In A First, Two Cinema Halls Get Notices For Playing National Anthem Before Film Shows">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/cinema_hall_1449038055_1449038063_502x234.jpg" border="0" alt="In A First, Two Cinema Halls Get Notices For Playing National Anthem Before Film Shows" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/in-a-first-two-cinema-halls-get-notices-for-playing-national-anthem-before-film-shows-247952.html" title="In A First, Two Cinema Halls Get Notices For Playing National Anthem Before Film Shows">
                        In A First, Two Cinema Halls Get Notices For Playing National Anthem Before Film Shows                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/srk-rekha-had-the-most-adorably-dirty-conversation-at-a-recent-award-ceremony-247948.html" class="tint" title="SRK & Rekha Had The Most Adorably Dirty Conversation At A Recent Award Ceremony!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1449036714_502x234.jpg" border="0" alt="SRK & Rekha Had The Most Adorably Dirty Conversation At A Recent Award Ceremony!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/srk-rekha-had-the-most-adorably-dirty-conversation-at-a-recent-award-ceremony-247948.html" title="SRK & Rekha Had The Most Adorably Dirty Conversation At A Recent Award Ceremony!">
                        SRK & Rekha Had The Most Adorably Dirty Conversation At A Recent Award Ceremony!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/oops-an-isis-terrorist-accidentally-blows-himself-up-while-launching-a-rocket-at-his-enemies-247950.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/oops-an-isis-terrorist-accidentally-blows-himself-up-while-launching-a-rocket-at-his-enemies-247950.html" class="tint" title="Oops! An ISIS Terrorist Accidentally Blows Himself Up While Launching A Rocket At His Enemies!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/isisterrorist_card_1449036858_502x234.jpg" border="0" alt="Oops! An ISIS Terrorist Accidentally Blows Himself Up While Launching A Rocket At His Enemies!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/oops-an-isis-terrorist-accidentally-blows-himself-up-while-launching-a-rocket-at-his-enemies-247950.html" title="Oops! An ISIS Terrorist Accidentally Blows Himself Up While Launching A Rocket At His Enemies!">
                        Oops! An ISIS Terrorist Accidentally Blows Himself Up While Launching A Rocket At His Enemies!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/travel/these-men-trekked-to-chandrataal-lake-to-clean-up-trash-left-behind-by-other-trekkers-inspiring-247933.html" class="tint" title="These Men Trekked To Chandrataal Lake To Clean Up Trash Left Behind By Other Trekkers! #Inspiring">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/chandrataal_card_new_1449043085_502x234.jpg" border="0" alt="These Men Trekked To Chandrataal Lake To Clean Up Trash Left Behind By Other Trekkers! #Inspiring" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/travel/these-men-trekked-to-chandrataal-lake-to-clean-up-trash-left-behind-by-other-trekkers-inspiring-247933.html" title="These Men Trekked To Chandrataal Lake To Clean Up Trash Left Behind By Other Trekkers! #Inspiring">
                        These Men Trekked To Chandrataal Lake To Clean Up Trash Left Behind By Other Trekkers! #Inspiring                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-flora-fauna-inspired-version-of-our-national-anthem-is-the-best-thing-you-ll-watch-today-247943.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-flora-fauna-inspired-version-of-our-national-anthem-is-the-best-thing-you-ll-watch-today-247943.html" class="tint" title="This Flora & Fauna Inspired Version Of Our National Anthem Is The Best Thing You'll Watch Today">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/na_card_1448989898_502x234.jpg" border="0" alt="This Flora & Fauna Inspired Version Of Our National Anthem Is The Best Thing You'll Watch Today" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-flora-fauna-inspired-version-of-our-national-anthem-is-the-best-thing-you-ll-watch-today-247943.html" title="This Flora & Fauna Inspired Version Of Our National Anthem Is The Best Thing You'll Watch Today">
                        This Flora & Fauna Inspired Version Of Our National Anthem Is The Best Thing You'll Watch Today                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/10-health-benefits-of-running-that-you-didn-t-know-about-247907.html" class="tint" title="10 Health Benefits Of Running That You Didn't Know About">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1448957546_502x234.jpg" border="0" alt="10 Health Benefits Of Running That You Didn't Know About" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/10-health-benefits-of-running-that-you-didn-t-know-about-247907.html" title="10 Health Benefits Of Running That You Didn't Know About">
                        10 Health Benefits Of Running That You Didn't Know About                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/5-foods-you-should-eat-for-healthy-kidneys-247859.html" class="tint" title="5 Foods You Should Eat For Healthy Kidneys">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card2_1448860900_502x234.jpg" border="0" alt="5 Foods You Should Eat For Healthy Kidneys" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/5-foods-you-should-eat-for-healthy-kidneys-247859.html" title="5 Foods You Should Eat For Healthy Kidneys">
                        5 Foods You Should Eat For Healthy Kidneys                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/lifestyle/technology/pepsi-just-released-a-film-so-big-part-of-it-had-to-be-shot-from-outer-space-247544.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/pepsi-just-released-a-film-so-big-part-of-it-had-to-be-shot-from-outer-space-247544.html" class="tint" title="Pepsi Just Released A Film So Big, Part Of It Had To Be Shot From Outer Space">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/card_1448021669_502x234.jpg" border="0" alt="Pepsi Just Released A Film So Big, Part Of It Had To Be Shot From Outer Space" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/pepsi-just-released-a-film-so-big-part-of-it-had-to-be-shot-from-outer-space-247544.html" title="Pepsi Just Released A Film So Big, Part Of It Had To Be Shot From Outer Space">
                        Pepsi Just Released A Film So Big, Part Of It Had To Be Shot From Outer Space                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/hollywood/vin-diesel-paid-a-heartfelt-tribute-to-brother-paul-walker-on-his-second-death-anniversary-247936.html" class="tint" title="Vin Diesel Paid A Heartfelt Tribute To 'Brother' Paul Walker & It'll Make You Really Emotional!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/paulwalkervindiesel_1448972546_1448972551_502x234.jpg" border="0" alt="Vin Diesel Paid A Heartfelt Tribute To 'Brother' Paul Walker & It'll Make You Really Emotional!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/hollywood/vin-diesel-paid-a-heartfelt-tribute-to-brother-paul-walker-on-his-second-death-anniversary-247936.html" title="Vin Diesel Paid A Heartfelt Tribute To 'Brother' Paul Walker & It'll Make You Really Emotional!">
                        Vin Diesel Paid A Heartfelt Tribute To 'Brother' Paul Walker & It'll Make You Really Emotional!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/here-are-84-silly-mistakes-in-perfectionist-aamir-khan-s-3-idiots-in-under-8-minutes-247938.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/here-are-84-silly-mistakes-in-perfectionist-aamir-khan-s-3-idiots-in-under-8-minutes-247938.html" class="tint" title="Here Are 84 Silly Mistakes In Perfectionist Aamir Khan's 3 Idiots In Under 8 Minutes!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/3idotsmistakes_card_1448973129_502x234.jpg" border="0" alt="Here Are 84 Silly Mistakes In Perfectionist Aamir Khan's 3 Idiots In Under 8 Minutes!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/here-are-84-silly-mistakes-in-perfectionist-aamir-khan-s-3-idiots-in-under-8-minutes-247938.html" title="Here Are 84 Silly Mistakes In Perfectionist Aamir Khan's 3 Idiots In Under 8 Minutes!">
                        Here Are 84 Silly Mistakes In Perfectionist Aamir Khan's 3 Idiots In Under 8 Minutes!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-video-will-give-you-all-the-wrong-reasons-why-we-are-failing-to-tackle-climate-change-247915.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-video-will-give-you-all-the-wrong-reasons-why-we-are-failing-to-tackle-climate-change-247915.html" class="tint" title="This Video Will Give You All The Wrong Reasons Why We Are Failing To Tackle Climate Change!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/climate_card_1448957421_502x234.jpg" border="0" alt="This Video Will Give You All The Wrong Reasons Why We Are Failing To Tackle Climate Change!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-video-will-give-you-all-the-wrong-reasons-why-we-are-failing-to-tackle-climate-change-247915.html" title="This Video Will Give You All The Wrong Reasons Why We Are Failing To Tackle Climate Change!">
                        This Video Will Give You All The Wrong Reasons Why We Are Failing To Tackle Climate Change!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/9-people-who-contribute-to-the-boredom-at-a-bengali-wedding-247768.html" class="tint" title="9 People Who Contribute To The Boredom At A Bengali Wedding">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1448617226_502x234.jpg" border="0" alt="9 People Who Contribute To The Boredom At A Bengali Wedding" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/9-people-who-contribute-to-the-boredom-at-a-bengali-wedding-247768.html" title="9 People Who Contribute To The Boredom At A Bengali Wedding">
                        9 People Who Contribute To The Boredom At A Bengali Wedding                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/meet-tarun-an-hiv-positive-teenager-in-mumbai-who-is-chasing-his-dream-of-becoming-a-dj-247922.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/meet-tarun-an-hiv-positive-teenager-in-mumbai-who-is-chasing-his-dream-of-becoming-a-dj-247922.html" class="tint" title="Meet Tarun, An HIV Positive Teenager In Mumbai Who Is Chasing His Dream Of Becoming A DJ">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/tarun-cp_1448967138_1448967145_502x234.jpg" border="0" alt="Meet Tarun, An HIV Positive Teenager In Mumbai Who Is Chasing His Dream Of Becoming A DJ" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/meet-tarun-an-hiv-positive-teenager-in-mumbai-who-is-chasing-his-dream-of-becoming-a-dj-247922.html" title="Meet Tarun, An HIV Positive Teenager In Mumbai Who Is Chasing His Dream Of Becoming A DJ">
                        Meet Tarun, An HIV Positive Teenager In Mumbai Who Is Chasing His Dream Of Becoming A DJ                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/hrithik-roshan-fulfills-last-wish-of-a-15-yo-cancer-patient-spends-time-with-her-during-her-last-days-247916.html" class="tint" title="Hrithik Roshan Fulfills Last Wish Of A 15 YO Cancer Patient, Spends Time With Her During Her Last Days!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/hritik_1448958359_1448958365_502x234.jpg" border="0" alt="Hrithik Roshan Fulfills Last Wish Of A 15 YO Cancer Patient, Spends Time With Her During Her Last Days!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/hrithik-roshan-fulfills-last-wish-of-a-15-yo-cancer-patient-spends-time-with-her-during-her-last-days-247916.html" title="Hrithik Roshan Fulfills Last Wish Of A 15 YO Cancer Patient, Spends Time With Her During Her Last Days!">
                        Hrithik Roshan Fulfills Last Wish Of A 15 YO Cancer Patient, Spends Time With Her During Her Last Days!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/11-terrifying-reasons-why-india-should-be-worried-about-climate-change-247920.html" class="tint" title="11 Terrifying Reasons Why India Should Be Worried About Climate Change">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1448962473_502x234.jpg" border="0" alt="11 Terrifying Reasons Why India Should Be Worried About Climate Change" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/11-terrifying-reasons-why-india-should-be-worried-about-climate-change-247920.html" title="11 Terrifying Reasons Why India Should Be Worried About Climate Change">
                        11 Terrifying Reasons Why India Should Be Worried About Climate Change                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/here-s-how-painkillers-work-in-your-body-to-relieve-discomfort-247877.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/here-s-how-painkillers-work-in-your-body-to-relieve-discomfort-247877.html" class="tint" title="Hereâs How Painkillers Work In Your Body To Relieve Discomfort">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/cover_1448873592_502x234.jpg" border="0" alt="Hereâs How Painkillers Work In Your Body To Relieve Discomfort" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/here-s-how-painkillers-work-in-your-body-to-relieve-discomfort-247877.html" title="Hereâs How Painkillers Work In Your Body To Relieve Discomfort">
                        Hereâs How Painkillers Work In Your Body To Relieve Discomfort                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/the-latest-internet-craze-is-the-condom-challenge-and-it-s-absolutely-ridiculous-247923.html" class="tint" title="The Latest Internet Craze Is The 'Condom Challenge' And It's Absolutely Ridiculous!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/cover_1448964802_502x234.jpg" border="0" alt="The Latest Internet Craze Is The 'Condom Challenge' And It's Absolutely Ridiculous!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/the-latest-internet-craze-is-the-condom-challenge-and-it-s-absolutely-ridiculous-247923.html" title="The Latest Internet Craze Is The 'Condom Challenge' And It's Absolutely Ridiculous!">
                        The Latest Internet Craze Is The 'Condom Challenge' And It's Absolutely Ridiculous!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/anushka-sharma-speaks-her-mind-on-sexism-in-bollywood-like-only-she-can-respect-247913.html" class="tint" title="Anushka Sharma Speaks Her Mind On Sexism In Bollywood Like Only She Can! #Respect">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1448956572_502x234.jpg" border="0" alt="Anushka Sharma Speaks Her Mind On Sexism In Bollywood Like Only She Can! #Respect" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/anushka-sharma-speaks-her-mind-on-sexism-in-bollywood-like-only-she-can-respect-247913.html" title="Anushka Sharma Speaks Her Mind On Sexism In Bollywood Like Only She Can! #Respect">
                        Anushka Sharma Speaks Her Mind On Sexism In Bollywood Like Only She Can! #Respect                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/14-tiny-stories-so-beautiful-they-ll-leave-you-wanting-for-more-247785.html" class="tint" title="14 Tiny Stories So Beautiful, Theyâll Leave You Wanting For More">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/airplane-card_1448617612_502x234.jpg" border="0" alt="14 Tiny Stories So Beautiful, Theyâll Leave You Wanting For More" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/14-tiny-stories-so-beautiful-they-ll-leave-you-wanting-for-more-247785.html" title="14 Tiny Stories So Beautiful, Theyâll Leave You Wanting For More">
                        14 Tiny Stories So Beautiful, Theyâll Leave You Wanting For More                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/french-filmmaker-gaspar-noe-s-love-screened-uncut-with-intense-adult-scenes-at-iffi-our-censor-board-must-be-reeling-247912.html" class="tint" title="French Filmmaker Gaspar Noe's 'Love' Screened Uncut With Intense 'Adult' Scenes At IFFI. Censor Board Must Be Reeling!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/screenshot_1_1448955698_1448955711_502x234.jpg" border="0" alt="French Filmmaker Gaspar Noe's 'Love' Screened Uncut With Intense 'Adult' Scenes At IFFI. Censor Board Must Be Reeling!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/french-filmmaker-gaspar-noe-s-love-screened-uncut-with-intense-adult-scenes-at-iffi-our-censor-board-must-be-reeling-247912.html" title="French Filmmaker Gaspar Noe's 'Love' Screened Uncut With Intense 'Adult' Scenes At IFFI. Censor Board Must Be Reeling!">
                        French Filmmaker Gaspar Noe's 'Love' Screened Uncut With Intense 'Adult' Scenes At IFFI. Censor Board Must Be Reeling!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/12-gifs-that-prove-making-gerua-was-all-kinds-of-madness-mixed-with-real-exotic-locations-247903.html" class="tint" title="12 Gifs That Prove Making 'Gerua' Was All Kinds Of Madness Mixed With Real Exotic Locations!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/srk-and-kajol-card_1448951252_1448951257_502x234.jpg" border="0" alt="12 Gifs That Prove Making 'Gerua' Was All Kinds Of Madness Mixed With Real Exotic Locations!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/12-gifs-that-prove-making-gerua-was-all-kinds-of-madness-mixed-with-real-exotic-locations-247903.html" title="12 Gifs That Prove Making 'Gerua' Was All Kinds Of Madness Mixed With Real Exotic Locations!">
                        12 Gifs That Prove Making 'Gerua' Was All Kinds Of Madness Mixed With Real Exotic Locations!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/here-s-how-exercise-can-keep-your-mind-sharp-it-actually-generates-new-cells-in-your-brain-247873.html" class="tint" title="Hereâs How Exercise Keeps Your Mind Sharp â It Actually Generates New Cells In Your Brain!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cover_1448869964_502x234.jpg" border="0" alt="Hereâs How Exercise Keeps Your Mind Sharp â It Actually Generates New Cells In Your Brain!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/here-s-how-exercise-can-keep-your-mind-sharp-it-actually-generates-new-cells-in-your-brain-247873.html" title="Hereâs How Exercise Keeps Your Mind Sharp â It Actually Generates New Cells In Your Brain!">
                        Hereâs How Exercise Keeps Your Mind Sharp â It Actually Generates New Cells In Your Brain!                    </a>
                </figcaption>
            </div>
        
        <script id="HpMiddleBlock_tpl" type="text/x-handlebars-template">
            {{#if MoreData}}
            {{#each MoreData}} 
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>


                    <a  class='video-btn sprite' href="{{guid}}" style="display:{{card_id}}"> </a>

    				{{#if label_name.length}}
                    <a href="{{guid}}" class="{{label_name}} sticker">&nbsp;</a>
    						 {{/if}} 
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="{{guid}}" title="{{carousal_headline}}" class="tint">
                        <img class="greyBg lazy" data-original="{{thumbnail}}" border="0" alt="{{carousal_headline}}" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="{{guid}}" title="{{carousal_headline}}">
                        {{carousal_headline}}
                    </a>
                </figcaption>
            </div>  
            {{/each}} 
            {{/if}}

            </script>

        </div><!--life-panel end-->

        <div class="trending-panel cf "><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_15"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/health/healthyliving/10-health-benefits-of-running-that-you-didn-t-know-about-247907.html" class="tint" title="10 Health Benefits Of Running That You Didn't Know About">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1448957546_218x102.jpg" border="0" alt="10 Health Benefits Of Running That You Didn't Know About"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/10-health-benefits-of-running-that-you-didn-t-know-about-247907.html" title="10 Health Benefits Of Running That You Didn't Know About">
                            10 Health Benefits Of Running That You Didn't Know About                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                <div class='container3'>                <div class="trending-panel-list cf" id="column3_16"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/culture/travel/10-posters-only-an-explorer-at-heart-will-relate-to-247914.html" class="tint" title="10 Posters Only An Explorer At Heart Will Relate To">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/ranbir-kapoor_1448957408_1448957425_218x102.jpg" border="0" alt="10 Posters Only An Explorer At Heart Will Relate To"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/travel/10-posters-only-an-explorer-at-heart-will-relate-to-247914.html" title="10 Posters Only An Explorer At Heart Will Relate To">
                            10 Posters Only An Explorer At Heart Will Relate To                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_17"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/bollywood/salman-khan-sends-a-video-message-to-the-specially-abled-people-urges-cinema-halls-to-have-special-facilities-for-them-247949.html" class="tint" title="Salman Khan Sends A Video Message To The Specially Abled People, Urges Cinema Halls To Have Special Facilities For Them">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/picmonkey-collage_1449036499_1449036504_218x102.jpg" border="0" alt="Salman Khan Sends A Video Message To The Specially Abled People, Urges Cinema Halls To Have Special Facilities For Them"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/salman-khan-sends-a-video-message-to-the-specially-abled-people-urges-cinema-halls-to-have-special-facilities-for-them-247949.html" title="Salman Khan Sends A Video Message To The Specially Abled People, Urges Cinema Halls To Have Special Facilities For Them">
                            Salman Khan Sends A Video Message To The Specially Abled People, Urges Cinema Halls To Have Special Facilities For Them                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_18"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/celebs/shaheen-bhatt-s-post-describing-sister-alia-bhatt-will-make-you-want-to-hug-yours-right-now-247976.html" class="tint" title="Shaheen Bhatt's Post Describing Sister Alia Bhatt Will Make You Want To Hug Yours Right Now!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/aliabhatt-shaheen_1449053914_1449053923_218x102.jpg" border="0" alt="Shaheen Bhatt's Post Describing Sister Alia Bhatt Will Make You Want To Hug Yours Right Now!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/shaheen-bhatt-s-post-describing-sister-alia-bhatt-will-make-you-want-to-hug-yours-right-now-247976.html" title="Shaheen Bhatt's Post Describing Sister Alia Bhatt Will Make You Want To Hug Yours Right Now!">
                            Shaheen Bhatt's Post Describing Sister Alia Bhatt Will Make You Want To Hug Yours Right Now!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_19"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/bollywood/srk-rekha-had-the-most-adorably-dirty-conversation-at-a-recent-award-ceremony-247948.html" class="tint" title="SRK & Rekha Had The Most Adorably Dirty Conversation At A Recent Award Ceremony!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1449036714_218x102.jpg" border="0" alt="SRK & Rekha Had The Most Adorably Dirty Conversation At A Recent Award Ceremony!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/srk-rekha-had-the-most-adorably-dirty-conversation-at-a-recent-award-ceremony-247948.html" title="SRK & Rekha Had The Most Adorably Dirty Conversation At A Recent Award Ceremony!">
                            SRK & Rekha Had The Most Adorably Dirty Conversation At A Recent Award Ceremony!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                
        </div>

    </div><!--trending-panel end-->

    </section>
    <section class="container cf" id="loader" style="display: none;border: 0px solid red;">
    <div class="news-panel cf ">&nbsp;</div>
    <div class="life-panel cf" style="text-align: center;"><img src="http://media.indiatimes.in/resources/images/loading.gif" border="0" /></div>
    <div class="trending-panel cf ">&nbsp;</div>
</section>
<section class="big-ads remove-fixed-home" id="adfooter">
<div id="badRos_slot"></div> 
</section>   

<!------------------------ end Block 4------------------------------------------------>
<!--container end-->

<script type="text/javascript">
   
    $('#bigAd1_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    showBigAD2('bigAd2_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible     
       
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#bigAd2_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    showBigAD3('bigAd3_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible

    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible  
      
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#bigAd3_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    BADros('badRos_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});

$(document).ready(function() {
	/* spotlight onload tracking homepage */
	//console.log("homepage");
			ga('send', 'event', 'OnLoad Partner Stories', '247914', 'homepage', {'nonInteraction': 1});
			ga('send', 'event', 'OnLoad Partner Stories', '247892', 'homepage', {'nonInteraction': 1});
	           
});

$(document).ready(function(){
        var trigger_depth={b2:99, b3:499, b4:1199};
        var is_trigger_active={b2:true, b3:true, b4:true};
        var call_on_scroll=true;
        
            $(window).on("scroll",function(){
            if((!(is_trigger_active.b2)&&!(is_trigger_active.b3)&&!(is_trigger_active.b4))==false)
            {
                callPreload(is_trigger_active,trigger_depth);
                
            }
    });
});

function callPreload(active,depth)
{
    var scroll_top = $(window).scrollTop();
    if((active.b2)&&(scroll_top > depth.b2))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Dec/flood3_1449144597_1449144606_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/temple-card_1449142772_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1449140621_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/saniatalksbccl_1449138913_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/502_1449133448_236x111.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/cover_1449048865_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/films-card_1449057789_1449057797_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card-11_1449159196_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card-image-1_1448867831_218x102.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/screenshot_3_1449141912_1449141916_218x102.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1448952588_218x102.jpg","http://media.indiatimes.in/media/content/2015/Dec/airtel-ca_1449121395_1449121404_218x102.jpg","http://media.indiatimes.in/media/content/2015/Dec/cover_1448959961_218x102.jpg");
        active.b2=false;
    }
    if((active.b3)&&(scroll_top > depth.b3))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Dec/jail-yoga-card_1449130408_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/raju5_1449126254_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/modi-card_1449132565_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/rohitmodiout_1449131884_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/google-card_1449125742_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/cp_1448951976_1448951980_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/picmonkey-collage_1449143782_1449143799_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/screenshot_3_1449141912_1449141916_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/rimi-priya-c_1449143871_218x102.jpg","http://media.indiatimes.in/media/content/2015/Dec/films-card_1449057789_1449057797_218x102.jpg","http://media.indiatimes.in/media/content/2015/Dec/jabwemet_1449046956_1449046977_218x102.jpg","http://media.indiatimes.in/media/content/2015/Dec/chandrataal_card_new_1449043085_218x102.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1449048863_218x102.jpg");
        active.b3=false;
    }
    
    if((active.b4)&&(scroll_top > depth.b4))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Dec/card_1449123983_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/raf502_1449121270_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/sehwagbccikotla_1449120995_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/chennai6_1449073911_1449073935_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/colla_1449065587_1449065597_236x111.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1449052499_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/kamal_1449134835_1449134841_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card1_1449059296_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card1_1449131412_1449131422_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/yo-yo-honey-singh-12a_1449129060_1449129071_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/cover_1449052996_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1449129470_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1449126513_1449126525_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/card_1449126363_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1449048863_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card-image-1_1448867831_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/jabwemet_1449046956_1449046977_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/cover_1448959961_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/airtel-ca_1449121395_1449121404_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/aa_1448891199_1448891211_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/chennai-floods-card_1449056418_1449056440_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/aliabhatt-shaheen_1449053914_1449053923_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/elephant_card_1449055131_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1449050840_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/yo-yo-honey-singh-style_1449051181_1449051192_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/india_card_1449046546_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1448952588_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/chennaipeople_card1_1449044489_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1448958342_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card-new_1449042395_1449042402_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/ra1_1449041240_1449041249_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card1_1449039541_1449039547_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/picmonkey-collage_1449036499_1449036504_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/cinema_hall_1449038055_1449038063_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1449036714_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/isisterrorist_card_1449036858_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/chandrataal_card_new_1449043085_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/na_card_1448989898_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1448957546_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card2_1448860900_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/card_1448021669_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/paulwalkervindiesel_1448972546_1448972551_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/3idotsmistakes_card_1448973129_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/climate_card_1448957421_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1448617226_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/tarun-cp_1448967138_1448967145_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/hritik_1448958359_1448958365_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1448962473_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/cover_1448873592_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/cover_1448964802_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1448956572_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/airplane-card_1448617612_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/screenshot_1_1448955698_1448955711_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/srk-and-kajol-card_1448951252_1448951257_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/cover_1448869964_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1448957546_218x102.jpg","http://media.indiatimes.in/media/content/2015/Dec/ranbir-kapoor_1448957408_1448957425_218x102.jpg","http://media.indiatimes.in/media/content/2015/Dec/picmonkey-collage_1449036499_1449036504_218x102.jpg","http://media.indiatimes.in/media/content/2015/Dec/aliabhatt-shaheen_1449053914_1449053923_218x102.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1449036714_218x102.jpg");
        active.b4=false;
    }
}
</script>    <div class="clr"></div>
      
	
                 <script> 
				                    showFooterCode();
					            </script>
	           <style>
.bottom-strip{background:#ebebeb;display:block;padding:15px 0;overflow:hidden;}.bottom-strip-box{margin:0 auto;display:table;max-width:410px;width:100%;}.square-logo{width:66px;height:64px;float:left;margin-right:10px;}.bottom-strip-content{float:left;margin-top:14px;}.bottom-strip-content span{display:block;color:#737272;font:12px/1 "RobotoRegular", Arial, Helvetica, sans-serif;margin-bottom:2px;}.bottom-strip-content a{color:#1b85dd;font:24px/1 "RobotoRegular", Arial, Helvetica, sans-serif;text-decoration:none;}
</style>
<div class="last-container">
    <!--<br>--> 
    <!--social-panel start-->
    <div class="social-panel red-bg">
        <div class="soc-inner"><!--soc-inner start-->
            <p class="soc-title">Be a Part of The New & Next</p>
            <div class="soc-count"><!--soc-count start-->
                <a href="https://www.facebook.com/indiatimes" target="_blank" title="facebook" class="sprite soc-fb"></a>
                <p>
                    3,995,421<span class="black"> FRIENDS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://twitter.com/indiatimes" target="_blank" title="twitter" class="sprite soc-twt"></a>
                <p>10343  <span class="black">FOLLOWERS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://plus.google.com/+indiatimes" target="_blank" title="gplus" class="sprite soc-gplus"></a>
                <p>104,163 <span class="black">MEMBERS</span></p>
            </div>
            <div class="soc-count"><!--soc-count start-->
                <a href="javascript:void(0);" title="E-mail" class="sprite soc-mail" id="subscribers_id"></a>
                <p>50,925 <span class="black">SUBSCRIBERS</span></p>
            </div>
            <!--soc-count end-->
        </div>
        <!--soc-inner end-->
    </div>
    <!--social-panel end-->
    <div class="clr"></div>

    <footer>
        <!-- social container start-->
        <div class="socical">
            <div class="containers">
                <div class="fl left_cont"> Get your weekly dose of virality sent to your inbox! </div>
                <div class="fr right_cont">

                    <input type="text" value="Please Enter Email Address" class="Email" name="UserEmail" id="UserEmail" defaultValue="Please Enter Email Address"/>
                    <input type="submit" id="btn-submit" value="SIGN UP" name="btn-submit" class="Subscribe">
                    <span id="subsEmal" style='color: red; padding-left: 66px; text-align: left;'></span>
                </div>

            </div>
        </div><!-- social containers end-->
        <div class="footer"><!-- footer grey start-->
            <div class="sub_container">
                <!-- container start-->
                <div class="cont_new">
                    <h2>Our Channels</h2>
                                                <a href='http://www.indiatimes.com/news/' class="blue size">News</a> 
                                                    <a href='http://www.indiatimes.com/lifestyle/' class="purple size">Lifestyle</a> 
                                                    <a href='http://www.indiatimes.com/entertainment/' class="red size">Entertainment</a> 
                                                    <a href='http://www.indiatimes.com/health/' class="green size">Health</a> 
                                                    <a href='http://www.indiatimes.com/videocafe/' class="orange size">Videos</a> 
                        					<a href='http://www.indiatimes.com/trending' class="size" style="color:white">Trending</a> 
                </div><!-- container closed-->

                <div class="cont"> <!-- container start-->
                    <h2>Sections</h2>
                                            <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/news/india/'>india</a>
                                             
                                    <a href='http://www.indiatimes.com/news/world/'>world</a>
                                             
                                    <a href='http://www.indiatimes.com/news/sports/'>sports</a>
                                             
                                    <a href='http://www.indiatimes.com/news/weird/'>weird</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/lifestyle/self/'>self</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/style/'>style</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/who-we-are/'>who we are</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/travel/'>travel</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/food/'>food</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/entertainment/bollywood/'>bollywood</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/celebs/'>celebscoop</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/hollywood/'>hollywood</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                             
                                    <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                             
                                    <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                             
                                    <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                             
                                    <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                
                             </div>                        <div class="sub_link">
                             </div>                    <div class="sub_link"><a href="http://www.indiatimes.com/photogallery">Photogallery</a></div>
                </div><!-- container closed-->
                <div class="cont_new"> <!-- container start-->
                    <h2>Indiatimes Lifestyle Network</h2>

                    <div class="sub_link">                             <a href="http://www.mensxp.com"   target="_blank" >MENS XP</a>
                                                    <a href="http://www.idiva.com"   target="_blank" >iDiva</a>
                                                    <a href="http://luxpresso.com"   target="_blank" >Luxury</a>
                                                    <a href="http://in.askmen.com/"   target="_blank" >Ask Men</a>
                                                    <a href="http://www.pursuitist.in/"   target="_blank" >Pursuitist</a>
                        </div>
                </div><!-- container closed-->
                <div class="cont"> <!-- container start-->
                    <h2>Other Sites</h2>
                    <div class="sub_link"> 
                        
                            <a href="http://www.cricbuzz.com"  target="_blank">CricBuzz</a> 
                        
                            <a href="http://www.happytrips.com/"  target="_blank">Happy Trips</a> 
                        
                            <a href="http://www.businessinsider.in/"  target="_blank">Business Insider</a> 
                        
                            <a href="http://www.zoomtv.com"  target="_blank">ZoomTv</a> 
                        
                            <a href="http://www.gizmodo.in/"  target="_blank">Gizmodo</a> 
                        
                            <a href="http://www.lifehacker.co.in/"  target="_blank">Lifehacker</a> 
                        
                            <a href="http://in.ign.com/"  target="_blank">IGN</a> 
                        
                            <a href="http://boxtv.com"  target="_blank">BoxTV</a> 
                        
                            <a href="http://www.gaana.com"  target="_blank">Gaana</a> 
                        
                            <a href="http://shopping.indiatimes.com"  target="_blank">Online Shopping</a> 
                        
                            <a href="http://www.in.techradar.com/"  target="_blank">Techradar</a> 
                        
                            <a href="http://timesdeal.com"  target="_blank">Daily Deals</a> 
                        
                            <a href="http://www.gitanjalishop.com/"  target="_blank">Gitanjali Shop</a> 
                        
                            <a href="http://www.satvikshop.com/"  target="_blank">Satvik Shop</a> 
                        
                            <a href="http://whatshot.in"  target="_blank">WHAT'S HOT</a> 
                        
                            <a href="http://zigwheels.com"  target="_blank">ZigWheels</a> 
                        
                            <a href="http://filmipop.com"  target="_blank">FILMIPOP</a> 
                        
                            <a href="http://www.filmfare.com/"  target="_blank">Filmfare</a> 
                        
                            <a href="http://www.femina.in"  target="_blank">Femina</a> 
                        
                            <a href="http://mobile.indiatimes.com"  target="_blank">Mobile</a> 
                        
                            <a href="http://www.greetzap.com"  target="_blank">Greetings</a> 
                        
                            <a href="http://www.follo.co.in"  target="_blank">Follo</a> 
                        
                            <a href="http://timesjobs.com"  target="_blank">Jobs</a> 
                        
                            <a href="http://magicbricks.com"  target="_blank">Property</a> 
                        
                            <a href="http://www.astrospeak.com"  target="_blank">Astrology</a> 
                        
                            <a href="http://speakingtree.in"  target="_blank">Speaking Tree</a> 
                        
                            <a href="http://simplymarry.com"  target="_blank">Matrimonial</a> 
                        
                            <a href="http://itimes.com"  target="_blank">iTimes</a> 
                        
                            <a href="http://www.ads2book.com/"  target="_blank">Ads2book</a> 
                        
                            <a href="http://www.tcnext.com/"  target="_blank">TcNext</a> 
                        
                            <a href="http://www.romedynow.com"  target="_blank">Romedy Now</a> 
                                            </div>
                </div><!-- container closed-->
                <div class="cont border_none"><!-- container start-->
                    <h2>About us</h2>
                    <div class="sub_link">
                                                    <a href='http://www.indiatimes.com/privacypolicy' >Privacy Policy</a> 
                                                    <a href='http://www.indiatimes.com/sitemap.html' >sitemap</a> 
                                                    <a href='http://www.indiatimes.com/termsandcondition' >Terms & CONDITIONS</a> 
                                                    <a href='http://www.indiatimes.com/seoarchive' >ARCHIVES</a> 
                                                    <a href='http://www.indiatimes.com/contactus' >Contact us</a> 
                                                    <a href='http://www.indiatimes.com/aboutus' >ABOUT INDIATIMES</a> 
                                            </div>
                </div><!-- container closed-->
            </div><!-- footer grey closed-->
        </div><!-- footer black closed-->

        <div class="black_footer"><!-- footer black start-->
            <div class="containers">
                <p><a href="javascript:void(0)" class="copyrt">Copyright Â© 2015 Times Internet Limited. Powered by Indiatimes Lifestyle Network. All rights reserved</a></p>
                <span><a href="http://www.indiatimes.com/seoarchive/">Archive</a> | <a href="http://www.indiatimes.com/sitemap.html">Sitemap</a> | <a href="http://www.indiatimes.com/aboutus/">About Us</a> | <a href="http://www.indiatimes.com/contactus/">Contact Us</a> | <a href="http://www.indiatimes.com/privacypolicy/">Privacy Policy</a> | <a target="_blank" href="http://m.indiatimes.com">Indiatimes Mobile</a> | <a href="http://www.indiatimes.com/termsandcondition/">Terms of Use and Grievance Redressal Policy </a> </span> 
            </div>
        </div><!-- footer black end-->
    </footer>
</div>
<script>
    $( document ).ready(function() {
        $("#subscribers_id").click(function(){
            $("#UserEmail").focus();
        });
    });
    
</script>

    <script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.lazyload.min.js?v=100.65" charset="utf-8"></script>
<script defer type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.easing.1.3.min.js?v=100.65"></script>

<input type="hidden" name="currentId" id="currentId" value="">
    <div class="animate_cont dawn floating delay5">
        <span class="animate_arrow sprite_image"></span>
    </div>




<script type="text/javascript" src="http://media.indiatimes.in/resources/js/main.js?v=100.65"></script>    
<script type="text/javascript" src="http://media.indiatimes.in/resources/js/common.js?v=100.65"></script>
<script  defer type="text/javascript" src="http://media.indiatimes.in/resources/js/jquery-scrolltofixed.js?v=100.65"></script>

<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://cdn.mouseflow.com/projects/1e238441-c30e-4d1b-8edd-c0f14d8fa6b7.js' type='text/javascript'%3E%3C/script%3E"));</script>
<!-- mouse flow code ends-->
<script  src="http://tags.crwdcntrl.net/c/2818/cc.js?ns=_cc2818" id="LOTCC_2818"></script> 

<script> 
    _cc2818.bcp(); 
</script>

	 <p id="back-top" style="display:none;"><a href="#top"><span></span></a> </p>
</div><!--wrap end-->


</body>
</html>