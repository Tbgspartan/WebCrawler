



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>

                    
                    
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "740-9896904-1951595";
                var ue_id = "1B87KXHARYYE4HP49REA";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        
    
        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />
    
    <meta property='fb:app_id' content='115109575169727' />
    
    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="1B87KXHARYYE4HP49REA" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1e-c3-2xl-i-7410f7dd.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-3859843826._CB310498986_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-485555676._CB318510215_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-247421025._CB318510254_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-3978168775._CB318510288_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-1541130848._CB318510255_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
(function(){var d=function(o){return Object.prototype.toString.call(o)==="[object Array]";},g=function(q,p){var o;for(o=0;o<q.length;o++){if(o in q){p.call(null,q[o],o);}}},h=[],k,b,l=false,n=false,f=function(){var o=[],p=[],q={};g(h,function(s){var r="";g(s.dartsite.split("/"),function(t){if(t!==""){if(t in q){}else{q[t]=o.length;o.push(t);}r+="/"+q[t];}});p.push(r);});return{iu_parts:o,enc_prev_ius:p};},c=function(){var o=[];g(h,function(q){var p=[];g(q.sizes,function(r){p.push(r.join("x"));});o.push(p.join("|"));});return o;},m=function(){var o=[];g(h,function(p){o.push(a(p.targeting));});return o.join("|");},a=function(r,o){var q,p,s=[];for(q in r){p=[];for(j=0;j<r[q].length;j++){p.push(encodeURIComponent(r[q][j]));}if(o){s.push(q+"="+encodeURIComponent(p.join(",")));}else{s.push(q+"="+p.join(","));}}return s.join("&");},e=function(){var o=+new Date();if(n){return;}if(!this.readyState||"loaded"===this.readyState){n=true;if(l){imdbads.cmd.push(function(){for(i=0;i<h.length;i++){generic.monitoring.record_metric(h[i].name+".fail",csm.duration(o));}});}}};window.tinygpt={define_slot:function(r,q,o,p){h.push({dartsite:r.replace(/\/$/,""),sizes:q,name:o,targeting:p});},set_targeting:function(o){k=o;},callback:function(q){var r,p={},t,o,s=+new Date();l=false;for(r=0;r<h.length;r++){t=h[r].dartsite;o=h[r].name;if(q[r][t]){p[o]=q[r][t];}else{window.console&&console.error&&console.error("Unable to correlate GPT response for "+o);}}imdbads.cmd.push(function(){for(r=0;r<h.length;r++){ad_utils.slot_events.trigger(h[r].name,"request",{timestamp:b});ad_utils.slot_events.trigger(h[r].name,"tagdeliver",{timestamp:s});}ad_utils.gpt.handle_response(p);});},send:function(){var r=[],q=function(s,t){if(d(t)){t=t.join(",");}if(t){r.push(s+"="+encodeURIComponent(""+t));}},o,p;if(h.length===0){tinygpt.callback({});return;}q("gdfp_req","1");q("correlator",Math.floor(4503599627370496*Math.random()));q("output","json_html");q("callback","tinygpt.callback");q("impl","fifs");q("json_a","1");result=f();q("iu_parts",result.iu_parts);q("enc_prev_ius",result.enc_prev_ius);q("prev_iu_szs",c());q("prev_scp",m());q("cust_params",a(k,true));o=document.createElement("script");p=document.getElementsByTagName("script")[0];o.async=true;o.type="text/javascript";o.src="http://pubads.g.doubleclick.net/gampad/ads?"+r.join("&");o.id="tinygpt";o.onload=o.onerror=o.onreadystatechange=e;l=true;p.parentNode.insertBefore(o,p);b=+new Date();}};})();</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['c'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['2'],
'u': ['472914126105'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-tarnhelm-3246591341._CB310457691_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"c5e3e721be9d014317a9eb0c37ebfb2f5b862b30",
"2015-09-14T23%3A56%3A27GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 25413;
generic.days_to_midnight = 0.2941319346427917;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-1582251138._CB317085808_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'c']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm === undefined || window.generic === undefined || window.consoleLog === undefined)) {
                    if (console !== undefined && console.log !== undefined) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=472914126105;ord=472914126105?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;ord=472914126105?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;ord=472914126105?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
onsubmit="(new Image()).src='/rg/SEARCH-BOX/HEADER/images/b.gif?link=/find';"
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/chart/toptv/?ref_=nv_tp_tv250_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                            <li><a href="/search/title?count=100&title_type=feature,tv_series,tv_movie&explore=title_type,genres,year,countries&ref_=nv_ch_mm_1"
> Popular Movies & TV
</a>                            </li>
                        <li><a href="/chart/?ref_=nv_ch_cht_2"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_3"
>Oscar Winners</a></li>
                        <li><a href="/chart/top?ref_=nv_ch_250_4"
>Top 250 Movies</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_5"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/list/ls074418362/?ref_=nv_tvv_picks_1"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_2"
>On Tonight</a></li>
                        <li><a href="/chart/toptv/?ref_=nv_tvv_250_3"
>Top 250 TV</a></li>
                        <li><a href="/list/ls074093523/?ref_=nv_tvv_wn_4"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_5"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/academymuseum/?ref_=nv_sf_am_1"
>Academy Museum</a></li>
                        <li><a href="/best-of/?ref_=nv_sf_bo_2"
>Best of 2014</a></li>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_3"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/whattowatch/?ref_=nv_sf_wtw_4"
>What to Watch</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=09-14&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_2"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_3"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_4"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_5"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_6"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_7"
>Road to the Emmys</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_8"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_9"
>Toronto Film Festival</a></li>
                        <li><a href="/awards-central/?ref_=nv_ev_all_10"
>More Popular Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni59000916/?ref_=nv_nw_tn_1"
> Frances McDormand Leads âIn Brugesâ Director Martin McDonaghâs New Movie
</a><br />
                        <span class="time">7 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59001016/?ref_=nv_nw_tn_2"
> Arnold Schwarzenegger Named New 'Celebrity Apprentice' Host
</a><br />
                        <span class="time">7 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59001295/?ref_=nv_nw_tn_3"
> Could Red Sparrow Spell A Hunger Games Reunion For Jennifer Lawrence?
</a><br />
                        <span class="time">6 hours ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/games/guess?ref_=nv_cm_qz_3"
>Quiz Game</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_4"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5> 
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0034583/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTk3MTMyMjc3Nl5BMl5BanBnXkFtZTcwODg4NDYwOA@@._V1._SY400_CR30,30,410,315_.jpg",
            titleYears : "1942",
            rank : 33,
                    headline : "Casablanca"
    },
    nameAd : {
            clickThru : "/name/nm0447695/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTEwNjMwNDU0ODleQTJeQWpwZ15BbWU3MDYzNDY1NDc@._V1._SX550_CR130,15,250,315_CT10_.jpg",
            rank : 101,
            headline : "Anna Kendrick"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYtwLpNISjZGovoh1xuwVY0DCy6D2myP0pU57Z4Vjpp6YYjLaERkMK7zYBBeG5xa3N92ZTAZ9Qe%0D%0AjaPKsHzwk_9GMGg2LHYNWCtHXJF8hb52Vw6hxHYiJM0y1xWny4kis4flUjfItWzenvWW_Cg-b4lU%0D%0AeSl1L3Y9VVv6cWlF1MrbNw0T0ajQY_mYusUmfKuJZEWofSpZSp24OVeM7JM-NZ26kw%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYu3M9mY56e6Ox3CPq3iN_eyp4uq0En4VbXxHlGkLHL_wv3LP1AqOHG3xTZOCa8RM8xUw97KyOv%0D%0ArEMOeSX8UnWT85f_02HTLLuzsjMmgfeAw_RjiUvOFj3HOUrcWlO2POE7wCyjSJA1WhT5BZpVKj6i%0D%0ACWXQhqRRmBcFf0D0-JjSwF7QW0uPyFxVIfrbXSt2n-H2%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYjApFWha5DQs7WLWRMKS0i2DWUY0HjtGeb3xjm5EfNMA5xRGqBBC6C_xkTaZavyIOsJrWv5DQy%0D%0AG3fQqou6ulb0VvW4Oa3713mEB96QOgV-ZrpoegzrQf1iGIIw1by00bChznPu4u4hiBi0VNfYi5rh%0D%0AKN2o8yDpymjdsXqy8J-x_YmrXSTc1pPLDQOOkTJCQqd4oou_vJA_lb0snG2wnTFIDg%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=472914126105;ord=472914126105?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>

    <div id="pagecontent">
    <div class="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=472914126105;ord=472914126105?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	
    </div>
    <div id="top-slot-wrapper" class="pagecontent">
                    
                    
    </div>
    <div class="pagecontent">
        <div id="content-2-wide">
            <div id="main">
                    
        <div class="heroWidget">
        <span class="ab_widget"
        >
            <div class="ab_hero">
<div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1092006681?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207779742&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207779742&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi1092006681" data-source="bylist" data-id="ls056131825" data-rid="1B87KXHARYYE4HP49REA" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="In the summer of 1985, 20-year-old David Meyers takes a job as a tennis pro at Red Oaks Country Club in suburban New Jersey, as his parents, girlfriend, and co-workers pull him toward a future he's increasingly unsure he wants." alt="In the summer of 1985, 20-year-old David Meyers takes a job as a tennis pro at Red Oaks Country Club in suburban New Jersey, as his parents, girlfriend, and co-workers pull him toward a future he's increasingly unsure he wants." src="http://ia.media-imdb.com/images/M/MV5BMTQ3MjY4NDQyM15BMl5BanBnXkFtZTgwMTkzNjk3NjE@._V1_SY298_CR11,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ3MjY4NDQyM15BMl5BanBnXkFtZTgwMTkzNjk3NjE@._V1_SY298_CR11,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="In the summer of 1985, 20-year-old David Meyers takes a job as a tennis pro at Red Oaks Country Club in suburban New Jersey, as his parents, girlfriend, and co-workers pull him toward a future he's increasingly unsure he wants." title="In the summer of 1985, 20-year-old David Meyers takes a job as a tennis pro at Red Oaks Country Club in suburban New Jersey, as his parents, girlfriend, and co-workers pull him toward a future he's increasingly unsure he wants." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="In the summer of 1985, 20-year-old David Meyers takes a job as a tennis pro at Red Oaks Country Club in suburban New Jersey, as his parents, girlfriend, and co-workers pull him toward a future he's increasingly unsure he wants." title="In the summer of 1985, 20-year-old David Meyers takes a job as a tennis pro at Red Oaks Country Club in suburban New Jersey, as his parents, girlfriend, and co-workers pull him toward a future he's increasingly unsure he wants." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt3973820/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207779742&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207779742&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > "Red Oaks" </a> </div> </div> <div class="secondary ellipsis"> Stream All Episodes: Oct. 9 </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1024897817?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207779742&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207779742&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi1024897817" data-source="bylist" data-id="ls002309697" data-rid="1B87KXHARYYE4HP49REA" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="Follows comedian/author/activist Russell Brand as he dives headlong into drugs, sex, and fame in an attempt to find happiness, only to realize that our culture feeds us bad ideas and empty idols. Through his stand up, Brand explores his own true icons - Gandhi, Che Guevara, Malcolm X, and Jesus Christ - and evolves from addict and Hollywood star to an unexpected political disruptor and newfound hero to the underserved. Will Brand hold fast against the roar of criticism to break out of the very system that built him?" alt="Follows comedian/author/activist Russell Brand as he dives headlong into drugs, sex, and fame in an attempt to find happiness, only to realize that our culture feeds us bad ideas and empty idols. Through his stand up, Brand explores his own true icons - Gandhi, Che Guevara, Malcolm X, and Jesus Christ - and evolves from addict and Hollywood star to an unexpected political disruptor and newfound hero to the underserved. Will Brand hold fast against the roar of criticism to break out of the very system that built him?" src="http://ia.media-imdb.com/images/M/MV5BODkxMDQ1NjMwOF5BMl5BanBnXkFtZTgwODg4MDY2NDE@._V1_SY298_CR123,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BODkxMDQ1NjMwOF5BMl5BanBnXkFtZTgwODg4MDY2NDE@._V1_SY298_CR123,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Follows comedian/author/activist Russell Brand as he dives headlong into drugs, sex, and fame in an attempt to find happiness, only to realize that our culture feeds us bad ideas and empty idols. Through his stand up, Brand explores his own true icons - Gandhi, Che Guevara, Malcolm X, and Jesus Christ - and evolves from addict and Hollywood star to an unexpected political disruptor and newfound hero to the underserved. Will Brand hold fast against the roar of criticism to break out of the very system that built him?" title="Follows comedian/author/activist Russell Brand as he dives headlong into drugs, sex, and fame in an attempt to find happiness, only to realize that our culture feeds us bad ideas and empty idols. Through his stand up, Brand explores his own true icons - Gandhi, Che Guevara, Malcolm X, and Jesus Christ - and evolves from addict and Hollywood star to an unexpected political disruptor and newfound hero to the underserved. Will Brand hold fast against the roar of criticism to break out of the very system that built him?" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Follows comedian/author/activist Russell Brand as he dives headlong into drugs, sex, and fame in an attempt to find happiness, only to realize that our culture feeds us bad ideas and empty idols. Through his stand up, Brand explores his own true icons - Gandhi, Che Guevara, Malcolm X, and Jesus Christ - and evolves from addict and Hollywood star to an unexpected political disruptor and newfound hero to the underserved. Will Brand hold fast against the roar of criticism to break out of the very system that built him?" title="Follows comedian/author/activist Russell Brand as he dives headlong into drugs, sex, and fame in an attempt to find happiness, only to realize that our culture feeds us bad ideas and empty idols. Through his stand up, Brand explores his own true icons - Gandhi, Che Guevara, Malcolm X, and Jesus Christ - and evolves from addict and Hollywood star to an unexpected political disruptor and newfound hero to the underserved. Will Brand hold fast against the roar of criticism to break out of the very system that built him?" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt3798628/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207779742&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207779742&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > Brand: A Second Coming </a> </div> </div> <div class="secondary ellipsis"> Official Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1764602649?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207779742&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207779742&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi1764602649" data-source="bylist" data-id="ls002397163" data-rid="1B87KXHARYYE4HP49REA" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="When his dysfunctional family clashes over the holidays, young Max is disillusioned and turns his back on Christmas. Little does he know, this lack of festive spirit has unleashed the wrath of Krampus: a demonic force of ancient evil intent on punishing non-believers. All hell breaks loose as beloved holiday icons take on a monstrous life of their own, laying siege to the fractured family's home and forcing them to fight for each other if they hope to survive." alt="When his dysfunctional family clashes over the holidays, young Max is disillusioned and turns his back on Christmas. Little does he know, this lack of festive spirit has unleashed the wrath of Krampus: a demonic force of ancient evil intent on punishing non-believers. All hell breaks loose as beloved holiday icons take on a monstrous life of their own, laying siege to the fractured family's home and forcing them to fight for each other if they hope to survive." src="http://ia.media-imdb.com/images/M/MV5BNTc4ODM2MTU2NF5BMl5BanBnXkFtZTgwNTEzNTMyNjE@._V1_SX201_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNTc4ODM2MTU2NF5BMl5BanBnXkFtZTgwNTEzNTMyNjE@._V1_SX201_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="When his dysfunctional family clashes over the holidays, young Max is disillusioned and turns his back on Christmas. Little does he know, this lack of festive spirit has unleashed the wrath of Krampus: a demonic force of ancient evil intent on punishing non-believers. All hell breaks loose as beloved holiday icons take on a monstrous life of their own, laying siege to the fractured family's home and forcing them to fight for each other if they hope to survive." title="When his dysfunctional family clashes over the holidays, young Max is disillusioned and turns his back on Christmas. Little does he know, this lack of festive spirit has unleashed the wrath of Krampus: a demonic force of ancient evil intent on punishing non-believers. All hell breaks loose as beloved holiday icons take on a monstrous life of their own, laying siege to the fractured family's home and forcing them to fight for each other if they hope to survive." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="When his dysfunctional family clashes over the holidays, young Max is disillusioned and turns his back on Christmas. Little does he know, this lack of festive spirit has unleashed the wrath of Krampus: a demonic force of ancient evil intent on punishing non-believers. All hell breaks loose as beloved holiday icons take on a monstrous life of their own, laying siege to the fractured family's home and forcing them to fight for each other if they hope to survive." title="When his dysfunctional family clashes over the holidays, young Max is disillusioned and turns his back on Christmas. Little does he know, this lack of festive spirit has unleashed the wrath of Krampus: a demonic force of ancient evil intent on punishing non-believers. All hell breaks loose as beloved holiday icons take on a monstrous life of their own, laying siege to the fractured family's home and forcing them to fight for each other if they hope to survive." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt3850590/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207779742&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207779742&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > Krampus </a> </div> </div> <div class="secondary ellipsis"> Trailer #1 </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/trailers?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207779742&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207779742&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Browse more trailers </a> </p>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="http://www.imdb.com/falltv/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2206024442&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2206024442&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Fall TV Preview</h3> </a> </span> </span> <p class="blurb">Are you ready for Fall TV? Our Fall TV Preview offers everything you need to dive in to one of the most jam-packed seasons ever: trailers, photo galleries, premiere schedules, exclusive interviews, and more!</p> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/falltv/the-walking-dead-10-exciting-things-ahead-for-rick-grimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2206024442&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2206024442&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Walking Dead (2010-)" alt="The Walking Dead (2010-)" src="http://ia.media-imdb.com/images/M/MV5BNzM3NjI5MDgxOF5BMl5BanBnXkFtZTgwNzYwODA1NjE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNzM3NjI5MDgxOF5BMl5BanBnXkFtZTgwNzYwODA1NjE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="http://www.imdb.com/falltv/the-walking-dead-10-exciting-things-ahead-for-rick-grimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2206024442&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2206024442&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > 10 Exciting Things Ahead on "The Walking Dead" </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/falltv/galleries/doctor-who-rm2451172864?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2206024442&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2206024442&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Doctor Who (2005-)" alt="Doctor Who (2005-)" src="http://ia.media-imdb.com/images/M/MV5BNjgyMjQzODA0OF5BMl5BanBnXkFtZTgwMTE4MjA3NjE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjgyMjQzODA0OF5BMl5BanBnXkFtZTgwMTE4MjA3NjE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="http://www.imdb.com/falltv/galleries/doctor-who-rm2451172864?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2206024442&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2206024442&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > "Doctor Who": Featured Photos </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/falltv/fall-tv-premiere-schedule-sept-8-to-sept-19?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2206024442&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2206024442&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Doll & Em (2013-)" alt="Doll & Em (2013-)" src="http://ia.media-imdb.com/images/M/MV5BMTU3OTk1NDgyOV5BMl5BanBnXkFtZTgwMjE5NTc2NjE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU3OTk1NDgyOV5BMl5BanBnXkFtZTgwMjE5NTc2NjE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="http://www.imdb.com/falltv/fall-tv-premiere-schedule-sept-8-to-sept-19?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2206024442&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2206024442&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > Premiere Schedule </a> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="http://www.imdb.com/falltv/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2206024442&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ftv_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2206024442&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Browse our Fall TV section </a> </p>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="http://www.imdb.com/toronto/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207171822&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tor_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207171822&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <h3>2015 Toronto International Film Festival</h3> </a> </span> </span> <p class="blurb">The 2015 Toronto International Film Festival runs from September 10-20. Check back for daily photo updates, browse our handy Mini-Guide, <a href="http://www.imdb.com/toronto/trailers/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207171822&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tor_lk1">watch trailers for select Toronto films</a>, and read lists of the films we're excited to see at the festival.</p> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/toronto/galleries/toronto-2015-day-4-rm442494720?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207171822&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tor_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207171822&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Freeheld (2015)" alt="Freeheld (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjIyNTMyNTIwOV5BMl5BanBnXkFtZTgwNjQ0MTk3NjE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjIyNTMyNTIwOV5BMl5BanBnXkFtZTgwNjQ0MTk3NjE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="http://www.imdb.com/toronto/galleries/toronto-2015-day-4-rm442494720?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207171822&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tor_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207171822&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > Photos: Day 4 </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/toronto/programming/2015-world-cinema?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207171822&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tor_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207171822&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Fear (2015)" alt="The Fear (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjIyMTAyMTMwN15BMl5BanBnXkFtZTgwNTc5NTU3NjE@._V1_SY201_CR95,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjIyMTAyMTMwN15BMl5BanBnXkFtZTgwNTc5NTU3NjE@._V1_SY201_CR95,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="http://www.imdb.com/toronto/programming/2015-world-cinema?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207171822&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tor_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207171822&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > Contemporary World Cinema </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/toronto/galleries/toronto-2015-day-3-rm4031770368?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207171822&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tor_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207171822&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Danish Girl (2015)" alt="The Danish Girl (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTQ1NDE0NzgzM15BMl5BanBnXkFtZTgwNTI3NDg3NjE@._V1_SY201_CR39,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ1NDE0NzgzM15BMl5BanBnXkFtZTgwNTI3NDg3NjE@._V1_SY201_CR39,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="http://www.imdb.com/toronto/galleries/toronto-2015-day-3-rm4031770368?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207171822&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tor_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207171822&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > Photos: Day 3 </a> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="http://www.imdb.com/toronto/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207171822&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_tor_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2207171822&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Browse our Toronto section </a> </p>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59000916?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjE0MTU3MjAyMl5BMl5BanBnXkFtZTcwNDM0MTEzMQ@@._V1_SY150_CR9,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59000916?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1"
class="headlines" >Frances McDormand Leads âIn Brugesâ Director Martin McDonaghâs New Movie</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000151?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_src"
>Slash Film</a></span>
    </div>
                                </div>
<p>Playwright <a href="/name/nm1732981?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk1">Martin McDonagh</a>Â (<a href="/title/tt4918444?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk2">The Pillowman</a>, A Behanding in Spokane) actively moved into filmmaking with the short <a href="/title/tt0425458?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk3">Six Shooter</a> in 2005, and then gave <a href="/name/nm0268199?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk4">Colin Farrell</a> one of his best roles in the bleak comedy <a href="/title/tt0780536?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk5">In Bruges</a>. Heâs also had the film <a href="/title/tt1931533?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk6">Seven Psychopaths</a>, and is promoting a new play called Hangman...                                        <span class="nobr"><a href="/news/ni59000916?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59001016?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2"
class="headlines" >Arnold Schwarzenegger Named New 'Celebrity Apprentice' Host</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000001?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2_src"
>The Hollywood Reporter</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59001295?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3"
class="headlines" >Could Red Sparrow Spell A Hunger Games Reunion For Jennifer Lawrence?</a>
    <div class="infobar">
            <span class="text-muted">6 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0011772?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3_src"
>We Got This Covered</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59001260?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4"
class="headlines" >Report: Warner planning Akira trilogy, Christopher Nolan involved?</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0002471?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4_src"
>Den of Geek</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59001020?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5"
class="headlines" >Oliver Stone's 'Snowden' Pushed to 2016 (Exclusive)</a>
    <div class="infobar">
            <span class="text-muted">12 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000001?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5_src"
>The Hollywood Reporter</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59002189?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTc3MTA5MTIwOF5BMl5BanBnXkFtZTgwMzUyODgxMzE@._V1_SY150_CR60,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59002189?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1"
class="headlines" >Another spoonful of sugar: Mary Poppins to be remade by Disney</a>
    <div class="infobar">
            <span class="text-muted">3 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000680?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_src"
>The Guardian - Film News</a></span>
    </div>
                                </div>
<p>The new version of the 1964 film about a magical nanny will take place 20 years after the original and will be made by <a href="/title/tt2180411?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk1">Into the Woods</a> director <a href="/name/nm0551128?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk2">Rob Marshall</a> Even though the sound of it is something quite atrocious â or so naysayers may suggest â Disney is developing a new version of <a href="/title/tt0058331?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk3">Mary Poppins</a>, ...                                        <span class="nobr"><a href="/news/ni59002189?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59001295?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2"
class="headlines" >Could Red Sparrow Spell A Hunger Games Reunion For Jennifer Lawrence?</a>
    <div class="infobar">
            <span class="text-muted">6 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0011772?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2_src"
>We Got This Covered</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59001260?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3"
class="headlines" >Report: Warner planning Akira trilogy, Christopher Nolan involved?</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0002471?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3_src"
>Den of Geek</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59001532?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4"
class="headlines" >Doctor Strange Has Officially Found Its Leading Lady</a>
    <div class="infobar">
            <span class="text-muted">6 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000098?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4_src"
>cinemablend.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59000909?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5"
class="headlines" >How Star Wars Forced Ridley Scott To Make Alien</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000098?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5_src"
>cinemablend.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59001414?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjI0MTg3MzI0M15BMl5BanBnXkFtZTcwMzQyODU2Mw@@._V1_SY150_CR5,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59001414?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1"
class="headlines" >âThe Searcherâ Optioned By eOne, Leonardo DiCaprioâs Appian Way</a>
    <div class="infobar">
            <span class="text-muted">6 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004913?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_src"
>Deadline TV</a></span>
    </div>
                                </div>
<p>Exclusive: A new book from author <a href="/name/nm1100682?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk1">Simon Toyne</a>, The Searcher, has just been optioned for television by eOne and <a href="/name/nm0000138?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk2">Leonardo DiCaprio</a>âs <a href="/company/co0088354?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk3">Appian Way</a> Productions. The deal marks the first project between the two companies. The book, which debuts October 6 from Harper Collins, is a high-concept thriller ...                                        <span class="nobr"><a href="/news/ni59001414?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59000928?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2"
class="headlines" >Emmys: Jaime Alexander, Will Forte, Rob Lowe, More Join Presenters List</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0075659?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2_src"
></a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59001081?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3"
class="headlines" >Angela Bassett Books Role in Starz London-Set Post-WWII Miniseries, 'Close to the Enemy'</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0032055?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3_src"
>Indiewire Television</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59001016?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4"
class="headlines" >Arnold Schwarzenegger Named New 'Celebrity Apprentice' Host</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000001?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4_src"
>The Hollywood Reporter</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59001221?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5"
class="headlines" >Grey's Anatomy Season 12 Photos: New Docs, Shocks Hit Grey Sloan Memorial</a>
    <div class="infobar">
            <span class="text-muted">6 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5_src"
>TVLine.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59001049?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTQwOTQ5MTI4Nl5BMl5BanBnXkFtZTgwODkwODA3MTE@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59001049?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1"
class="headlines" >Adrian Grenier Shares Then Deletes Controversial 9/11 Post on InstagramâSee the Photo that Caused Uproar</a>
    <div class="infobar">
            <span class="text-muted">6 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_src"
>E! Online</a></span>
    </div>
                                </div>
<p><a href="/name/nm0004978?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk1">Adrian Grenier</a> caused quite the uproar with his social media post about the anniversary of 9/11 on Friday. The Entourage actor received some major backlash after he took to Instagram to share a controversial graphic about the Sept. 11, 2001, attacks, criticizing the War on Iraq. "R.I.P the 2,996 ...                                        <span class="nobr"><a href="/news/ni59001049?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59000901?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2"
class="headlines" >Jessica Biel Stuns in a Rare Postbaby Appearance</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000088?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2_src"
>Popsugar.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59001757?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3"
class="headlines" >'Twilight' Turns 10 Years Old!</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000936?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3_src"
>Access Hollywood</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59002186?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4"
class="headlines" >Sara Gilbert and Wife Linda Perry Attend Family Fun Day With 6-Month-Old Son RhodesâSee the Pic!</a>
    <div class="infobar">
            <span class="text-muted">3 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59001441?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5"
class="headlines" >Teresa Giudice "Still Controls Everything" From Prison According To Joe in New "Rhonj" Special</a>
    <div class="infobar">
            <span class="text-muted">7 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004699?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5_src"
>TooFab</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/gallery/rg2465176320?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2206014722&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_pwl_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2206014722&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <h3>IMDb Snapshot - Photos We Love</h3> </a> </span> </span> <p class="blurb">Here's a look back at some of our favorite event photos, still images, and posters featured between September 6 - September 12.</p> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm2564747008/rg2465176320?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2206014722&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_pwl_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2206014722&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BNTEwMDA5NjY3N15BMl5BanBnXkFtZTgwNjgyNTM3NjE@._V1._CR0,0,1365,1365_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNTEwMDA5NjY3N15BMl5BanBnXkFtZTgwNjgyNTM3NjE@._V1._CR0,0,1365,1365_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm4003327744/rg2465176320?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2206014722&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_pwl_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2206014722&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Leftovers (2014-)" alt="The Leftovers (2014-)" src="http://ia.media-imdb.com/images/M/MV5BMTQ4MzEzNTAxOF5BMl5BanBnXkFtZTgwMjQ1MTY3NjE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ4MzEzNTAxOF5BMl5BanBnXkFtZTgwMjQ1MTY3NjE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3632066304/rg2465176320?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2206014722&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_pwl_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2206014722&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="I Saw the Light (2015)" alt="I Saw the Light (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTY2Njc5Njk2OV5BMl5BanBnXkFtZTgwOTMzNjc3NjE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY2Njc5Njk2OV5BMl5BanBnXkFtZTgwOTMzNjc3NjE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/gallery/rg2465176320?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2206014722&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_pwl_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2206014722&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See full gallery </a> </p>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
                <div class="ab_borntoday">
<span class="ninja_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=9-14&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Born Today</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0511088?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Andrew Lincoln" alt="Andrew Lincoln" src="http://ia.media-imdb.com/images/M/MV5BMjI2NDYyNjg4NF5BMl5BanBnXkFtZTcwMjI5OTMwNA@@._V1_SY172_CR5,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI2NDYyNjg4NF5BMl5BanBnXkFtZTcwMjI5OTMwNA@@._V1_SY172_CR5,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0511088?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_1">Andrew Lincoln</a> (42) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm3726887?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Jessica Brown Findlay" alt="Jessica Brown Findlay" src="http://ia.media-imdb.com/images/M/MV5BMTM2NDQ2MjUzOV5BMl5BanBnXkFtZTcwNzI5ODYxNA@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTM2NDQ2MjUzOV5BMl5BanBnXkFtZTcwNzI5ODYxNA@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm3726887?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_2">Jessica Brown Findlay</a> (26) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0931090?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Kimberly Williams-Paisley" alt="Kimberly Williams-Paisley" src="http://ia.media-imdb.com/images/M/MV5BMjMxMzQxOTQzM15BMl5BanBnXkFtZTcwMjk5OTUxNw@@._V1_SY172_CR11,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjMxMzQxOTQzM15BMl5BanBnXkFtZTcwMjk5OTUxNw@@._V1_SY172_CR11,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0931090?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_3">Kimberly Williams-Paisley</a> (44) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0000554?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Sam Neill" alt="Sam Neill" src="http://ia.media-imdb.com/images/M/MV5BMTQxNjY1OTA1NF5BMl5BanBnXkFtZTcwNTcxMTAzNQ@@._V1_SY172_CR13,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQxNjY1OTA1NF5BMl5BanBnXkFtZTcwNTcxMTAzNQ@@._V1_SY172_CR13,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0000554?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_4">Sam Neill</a> (68) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0502425?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Melissa Leo" alt="Melissa Leo" src="http://ia.media-imdb.com/images/M/MV5BMTc3MDAxOTkyOF5BMl5BanBnXkFtZTcwOTg1MDIzNA@@._V1_SY172_CR4,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc3MDAxOTkyOF5BMl5BanBnXkFtZTcwOTg1MDIzNA@@._V1_SY172_CR4,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0502425?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_5">Melissa Leo</a> (55) </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/search/name?refine=birth_monthday&birth_monthday=9-14&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See all birthdays </a> </p>        </div>

        </span>
        </div>
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/title/tt2910904/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205381922&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205381922&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Indie Focus: 'The Dressmaker' - New Trailer</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm2115366400/tt2910904?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205381922&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205381922&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Dressmaker (2015)" alt="The Dressmaker (2015)" src="http://ia.media-imdb.com/images/M/MV5BNjQ3Nzk0MDM4M15BMl5BanBnXkFtZTgwMzI5ODA3NjE@._V1_SX170_CR0,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjQ3Nzk0MDM4M15BMl5BanBnXkFtZTgwMzI5ODA3NjE@._V1_SX170_CR0,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2284565273?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205381922&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205381922&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi2284565273" data-rid="1B87KXHARYYE4HP49REA" data-type="single" class="video-colorbox" data-refsuffix="hm_if" data-ref="hm_if_i_2"> <img itemprop="image" class="pri_image" title="The Dressmaker (2015)" alt="The Dressmaker (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTUzMjUzMDk3NF5BMl5BanBnXkFtZTgwMjMzNjU3NjE@._V1_SX444_CR0,0,444,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUzMjUzMDk3NF5BMl5BanBnXkFtZTgwMjMzNjU3NjE@._V1_SX444_CR0,0,444,250_AL_UY500_UX888_AL_.jpg" /> <img alt="The Dressmaker (2015)" title="The Dressmaker (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="The Dressmaker (2015)" title="The Dressmaker (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb"><a href="/name/nm0000701/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205381922&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_lk1">Kate Winslet</a> stars as a glamorous woman who returns to her small town in rural Australia. With her sewing machine and haute couture style, she transforms the women of the town and exacts sweet revenge on those who did her wrong.</p> <p class="seemore"> <a href="/title/tt2910904/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205381922&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205381922&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Learn more about <i>The Dressmaker</i> </a> </p>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_trivia">
<span class="ninja_header"> <span class="oneline"> <a href="/title/tt3832914/trivia?item=tr2601970&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Did You Know?</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt3832914?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="War Room (2015)" alt="War Room (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTYyNTUxMjQwNF5BMl5BanBnXkFtZTgwNDY5MDIwNTE@._V1_SY132_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTYyNTUxMjQwNF5BMl5BanBnXkFtZTgwNDY5MDIwNTE@._V1_SY132_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" > <strong class="text-large"><a href="/title/tt3832914?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">War Room</a></strong> <p class="blurb">When Tony wakes up from his nightmare, the clock reads 7:14, which alludes to the movie's theme verse from the Bible, 2 Chronicles 7:14: "If my people who are called by my name will humble themselves and pray and seek my face and turn from their wicked ways, I will hear from heaven and will forgive their sins and restore their land." (NLT)</p></div> </div> </div> <p class="seemore"> <a href="/title/tt3832914/trivia?item=tr2601970&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > See more trivia </a> </p>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;o9Pwtj-Zejk&quot;}"
        >
            <div class="ab_poll poll">
<span class="ninja_header"> <span class="oneline"> <a href="/poll/o9Pwtj-Zejk/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205818602&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205818602&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Poll: Dream Interview</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205818602&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk">More Polls</a></h4> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:23.5%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/o9Pwtj-Zejk/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205818602&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205818602&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="J. Michael Straczynski" alt="J. Michael Straczynski" src="http://ia.media-imdb.com/images/M/MV5BMjIxNjA0NzQ1OF5BMl5BanBnXkFtZTcwODIyNzY2Mw@@._V1_SY259_CR10,0,175,259_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjIxNjA0NzQ1OF5BMl5BanBnXkFtZTcwODIyNzY2Mw@@._V1_SY259_CR10,0,175,259_AL_UY518_UX350_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:23.5%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/o9Pwtj-Zejk/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205818602&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205818602&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Steven Spielberg" alt="Steven Spielberg" src="http://ia.media-imdb.com/images/M/MV5BMTY1NjAzNzE1MV5BMl5BanBnXkFtZTYwNTk0ODc0._V1_SX175_CR0,0,175,259_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY1NjAzNzE1MV5BMl5BanBnXkFtZTYwNTk0ODc0._V1_SX175_CR0,0,175,259_AL_UY518_UX350_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:23.5%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/o9Pwtj-Zejk/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205818602&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205818602&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Robin Williams" alt="Robin Williams" src="http://ia.media-imdb.com/images/M/MV5BNTYzMjc2Mjg4MF5BMl5BanBnXkFtZTcwODc1OTQwNw@@._V1_SX175_CR0,0,175,259_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNTYzMjc2Mjg4MF5BMl5BanBnXkFtZTcwODc1OTQwNw@@._V1_SX175_CR0,0,175,259_AL_UY518_UX350_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:23.5%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/o9Pwtj-Zejk/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205818602&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205818602&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Leonardo DiCaprio" alt="Leonardo DiCaprio" src="http://ia.media-imdb.com/images/M/MV5BMjI0MTg3MzI0M15BMl5BanBnXkFtZTcwMzQyODU2Mw@@._V1_SY259_CR8,0,175,259_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI0MTg3MzI0M15BMl5BanBnXkFtZTcwMzQyODU2Mw@@._V1_SY259_CR8,0,175,259_AL_UY518_UX350_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> <p class="blurb">Who would you most like to interview <a href="http://www.imdb.com/poll/o9Pwtj-Zejk/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205818602&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk1">from this list of celebrated icons</a>? <a href="http://www.imdb.com/board/bd0000088/nest/246126722?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205818602&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk2">Suggest others and discuss here</a> after voting.</p> <p class="seemore"> <a href="/poll/o9Pwtj-Zejk/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205818602&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205818602&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Vote now </a> </p>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
            </div>
            <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,11x1;p=tr;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=472914126105;ord=472914126105?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,11x1;p=tr;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;ord=472914126105?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,11x1;p=tr;fv=1;ab=c;bpx=2;c=0;s=3075;s=32;ord=472914126105?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	
                    
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2074322302&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2074322302&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Opening This Week</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2719848"></div> <div class="title"> <a href="/title/tt2719848?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2074322302&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t0"> Everest </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4046784"></div> <div class="title"> <a href="/title/tt4046784?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2074322302&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t1"> Maze Runner: The Scorch Trials </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1355683"></div> <div class="title"> <a href="/title/tt1355683?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2074322302&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t2"> Black Mass </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4467262"></div> <div class="title"> <a href="/title/tt4467262?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2074322302&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t3"> Katti Batti </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3397884"></div> <div class="title"> <a href="/title/tt3397884?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2074322302&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t4"> Sicario </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3268668"></div> <div class="title"> <a href="/title/tt3268668?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2074322302&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t5"> Captive </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1596345"></div> <div class="title"> <a href="/title/tt1596345?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2074322302&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t6"> Pawn Sacrifice </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4158624"></div> <div class="title"> <a href="/title/tt4158624?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2074322302&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t7"> About Ray </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <div><a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2074322302&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2074322302&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more opening this week</p></a></div>
        </span>
        </div>
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
            <div class="ab_ninja showtimesMessagingWidget">
<p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2006955922&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p>    </div>

        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/chart/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Now Playing (Box Office)</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3862750"></div> <div class="title"> <a href="/title/tt3862750?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t0"> The Perfect Guy </a> <span class="secondary-text">$25.9M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3567288"></div> <div class="title"> <a href="/title/tt3567288?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t1"> The Visit </a> <span class="secondary-text">$25.4M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3832914"></div> <div class="title"> <a href="/title/tt3832914?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t2"> War Room </a> <span class="secondary-text">$7.8M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1178665"></div> <div class="title"> <a href="/title/tt1178665?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t3"> A Walk in the Woods </a> <span class="secondary-text">$4.7M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2381249"></div> <div class="title"> <a href="/title/tt2381249?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t4"> Mission: Impossible - Rogue Nation </a> <span class="secondary-text">$4.1M</span> </div> <div class="action"> <a href="/showtimes/title/tt2381249?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> </div> </div> </div> <div><a href="/chart/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more box office results</p></a></div>
        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Coming Soon</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2510894"></div> <div class="title"> <a href="/title/tt2510894?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t0"> Hotel Transylvania 2 </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2361509"></div> <div class="title"> <a href="/title/tt2361509?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t1"> The Intern </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2403021"></div> <div class="title"> <a href="/title/tt2403021?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t2"> The Green Inferno </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3018070"></div> <div class="title"> <a href="/title/tt3018070?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t3"> Stonewall </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3825638"></div> <div class="title"> <a href="/title/tt3825638?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t4"> Le labyrinthe du silence </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <div><a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" ><p class="seemore position_bottom">See more coming soon</p></a></div>
        </span>
        </div>
                    
                    
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
            <div class="ab_twitter">
<span class="widget_header"> <h3>Follow Us On Twitter</h3> </span> <div class="widget_content"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3776329187._CB305760180_.html#{"screen-name":"IMDb","height":"500px","width":"300px"}'></iframe> </div> </div>    </div>

        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
            <div class="ab_facebook">
<span class="widget_header"> <h3>Find Us On Facebook</h3> </span> <div class="widget_content"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
        <div class="aux-content-widget-2 sticky-widget">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="http://www.imdb.com/family-entertainment-guide/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205032962&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_feg_rhs_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205032962&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <h3>10 Best Kids Shows Recommended by Parents</h3> </a> </span> </span> <p class="blurb">Check out IMDb's top-rated shows for kids, TV gems old and new that are funny, educational, and entertaining enough for parents.</p> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/family-entertainment-guide/best-kids-shows-recommended-by-parents/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205032962&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_feg_rhs_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205032962&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Pinky and the Brain (1995-1998)" alt="Pinky and the Brain (1995-1998)" src="http://ia.media-imdb.com/images/M/MV5BMTU2MjE2MTk0MV5BMl5BanBnXkFtZTgwMzQ0OTc5MjE@._V1_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU2MjE2MTk0MV5BMl5BanBnXkFtZTgwMzQ0OTc5MjE@._V1_UY750_UX1000_AL_.jpg" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="http://www.imdb.com/family-entertainment-guide/best-kids-shows-recommended-by-parents/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205032962&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_feg_rhs_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205032962&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > 10 Best Kids Shows Recommended by Parents </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="http://www.imdb.com/family-entertainment-guide/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205032962&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_feg_rhs_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2205032962&pf_rd_r=1B87KXHARYYE4HP49REA&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" > Visit our Family Entertainment Guide </a> </p>    </div>

        </span>
        </div>
            </div>
        </div>
        
                    
                    
        
    
        <br class="clear" />
    </div>
</div>
                





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYsfdi6U2_kfWNAK7MV-Cmu0qhTd6McigJl7jFj1aeSVUkAFIgyNYlo4UsJXQwfwPD93_R5XgFh%0D%0AChujTiPmBfRSnjFC_eHLRx9Oac8mF9vkvZawxN9F0XblurMADUwiXCwl9N8V3qYRdgF70511lrCU%0D%0ApZmh8dxrq1O-_e7bRQsLcrKwPjAWMvy6rcErTe6BmOWiawoUU0p_0clUC2uV6oG5oZ7fGhx4BU42%0D%0Ay5wF2RGNtJ0%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYura34bRA82QPkIqfQUGSMxzQK8oUtxYtJwShdz20w8pg42JLpvEZLdJu-AQeQKIC1bvSbX6Qf%0D%0AnmspjNcK9GJJM_eGz8oC1YCFif5MbKE-c80dTaZ9jAMSMFNzuMyCMOkHanVFLJboUFl-L28dkHsJ%0D%0AzuBn9hoNqj9IJYlOLhau6rkyl91hjk1UzOYzUn2HgKSFYyV-OlL7mfI8rp7JwjSKIg%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYqBJFBQ7325_0t2YHmMyo6ccFOEjAzaBMP6zDKMSNSHznVUeqEP2ReKPrIrNIZoArtmVKUJHp_%0D%0A9x6mkwP-T8WSi_PvPsVKBcwo7zfKWTWpMV6S0U9GYRPugPDOgd3EpyAdRpSpAtindVUFfg_sG-Ql%0D%0AvTywjimP2nVyyG2tw_qv8GMjlHrjyiheVUuMsmMuYJqVcHq-7Ruv871KaAtQMbQ8DOJNiCejprdL%0D%0AeoyRantD1PdAt8ke6ouHM2OL5GWI-nKapGZ_KOunDqpp-55q7sQwy12QcNtnCn0d1NKuP-yjyw9K%0D%0Auchb-LU6XaxE_8vXG9U4ne85eP7QZGuijlV8-DHWwMMdj0cdk3zuVFDb1HXoOEljID_jMZ9WPbYh%0D%0AXznNF1tNUqVhIOdjz1Gv-d4VYBBD3g%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYtyP6NtsmOvptGOJMoiJJe0es-vfeBiVuq9rPwpCkSRveVwFl64ohU7SWvCKBKU9h98y4_MlUG%0D%0AFmMTLOEepeZSAgQPw8msimsQyorLImVbGkrNEBTAKTzq61D1zBIXOv1vzMAQ7BuID9blb0BraICE%0D%0ABKFC6c2cv1476Ljy3ba_mg1XAI7fXwfLwQ6ScQKDO9X1KGGPm40Cimjg0puW6oIcqg%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYiRkbA1zmigZvjOejU46L6Q0EGa3O1MuoMTrDyfFuXgZpaWzJciqcq4L4b4ERvwBeLBaCBQK9G%0D%0AeJIPu40__ZkCQ2asOvZ3RWGLoXAcD9cTHH8B-I-bUKE9_c8hbnyiEwZHBURVDeFZSWttrzjTxvU0%0D%0AE0mMIwqxVXNcGZrXnY1wPlThfyGO-C0l_MiuVeSXFBpR%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYgvvms0hgfsSkxkB29rZLSxOWuvW7j1xyQ8IclCoQablhCPjtw-gF-4iDkgHzgDzuvtcIT4xLm%0D%0ALYdQi4QCSm0lqHDjdWO-7pDv19RQgbKQ7gp3PnA6o3ZhJ5SMJk6G89z-_svojma930VqMf5C-_GS%0D%0AxzGSYuW1Ff6D80-30ILRhMe3WuxOwhRc5thcWdjjsSYpyrnR__3do36p8V1frUcNRQ%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top 250</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Top Movies</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li>    <a href="/offsite/?page-action=ft_ad&token=BCYrhjKQ4L41ac2Evn-YiiONRS2BopvYbC6NyzBtN7zIqjvg2x8Y6rhmLSiZXsqhhsguN0SG0yhW%0D%0AGlSGFWsoB3SZhnMa1IIjtwWsr155if-bKFY27jiKsyJD-BYr6j2Gc-qGuUvRAJlOXn7nIIpsUs7-%0D%0AFnK9SFmYKM0f_qHf4GrMqs3fbHisW29LXqCUtI9okBRAml97IUnxzx2BJBgBH0BjbA5jozmwhP9I%0D%0Acqyo4QGC23HM0pL6u0Q531f9BGd6xvBZaCjNPSziQYBJFrMwfO59mw%0D%0A&ref_=ft_ad"
itemprop='url'>Advertising</a>
</li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYq7tZ078-UVtiSd6XhqGZRmHDlkAUTsechEE_klJ554hc2xgl5_BSuUqeKoPJfApoFelm4P5lH%0D%0AqCTj0R5VxzPuhE4WypWFG2TGYSk3ltZLML3Ihz8rOZl-Dohs7v7JCPCSv42Ej-Hv9uga92oFQKjy%0D%0AnhEsRRUI_zfUwuUPWnk4pi5SAMQqaNL-c03kwpt-eYf7yK4GgdKvmiA7jh_SOXRODA%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYrWgbNsmV_4ptKGdeJyxx4Ms0T1RCRx5uFqUxYHFZn5d7M9aMaHQEoNONYfRXJI-d98HPAJRC6%0D%0ADcTuU7FM5uyYZunUE9hhQQYiTy0wjjAUlk153eT6DdWbVW2h7a6rtTZlCWw-NJPiMluKdNWifqXn%0D%0AHod87eICUwUsMIQfT6mD5AOyFMlsMcd5o80sPY7IMLBF%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYutV1nJh2yl6he4BqneYlTRxgRB3vziKWbkMUyKaFpAgQpUdqUs8CoGCDfrCzN-EsUr-fYFOYn%0D%0Ajj033imPDP1_gxXf5j_NE8vVqilcWkCmycaOlwDWlSa-xEAuTlL3C1814l1Qy87vRH9GEoTdeN4p%0D%0Ac-6ggHENjy4qaNtk59j8L2TDcN5vGkndD74bxxMH-dE0-PE-i4kDmRArdAaamxuTb3gFZdoA2gEz%0D%0AThNERM_RuwPC_TZI8clnl76vTTkZmPc4%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/show_article?conditions';"
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2015
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/';"
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYm56RbgcNcOb1QPGddkpXIIZKiVi3rI9LFTUwf7NQUC6RK1_eu8NBa9L8RBGloNLzvLGaOUYhg%0D%0Am4Jw7NQIHlUDecsioVOQZm4O3cZAn-U4YlnveoirZxGoRSXekLLmRJlvp_RuswiP_S6_Gztn53N1%0D%0At0hgSSN3VTVgcEhogt2MKQ-3wOSoGu6HGyH5jZd-Jm5uLo2YMbHKaVzlkxMpcOI0CWW-ykVx0NtN%0D%0A3SysmAgTkQs%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Instant Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYvw9zRil9Yc78gUOQn2xCfHm6iSEoBM17OD8UDjJem92lmIZSwsOVXdqv_QLu-CewlKQlz47FS%0D%0A2hQKEwj6jYP_t6F_zNkBjsu90NjMhwuCEyUB9Ptkm_WYxjEoam_I9E_FuWk0W8EzNRQJG_gOZEVB%0D%0A-SyQXcKTtWdumP1K1A5Y-_6qqJ6s_i8zRIC7TtnzeherbaF4-Ll0SkuWIbj2qb6PfjoSGHwFHBaI%0D%0AJcQtyQ_vbDw%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Instant Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYrrznfyhfP-3QOxNUomrdfTRiYErrtUwdV9jmbi4u34Z1cQWeo5Bw6ZNyxkLh9-37__sF6KVKu%0D%0AMf8cDk9UMAYJrBxXCi3dDA39LGfo0VOWBgr8v1IEmKEYqkMOl6Ml2ocpAcSwNl_TDmOO5s53kNKH%0D%0AVICcB27gR6hydja1zrNqPgMuRzsJnRv3bbfryLl2WbCuJXFWTbctYX3MUmIZLkz7ToXMmRu_09Vr%0D%0A7vUg5GRw708%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYmnimSSb3Vj3lsa1gOPeDVGm7Avto-E-rxm7JPksEN48LuRqKBlPs9r-yHk_rcwquhj8ske_5k%0D%0A75VS87crDcKLQ6lSjYxu2oA_Fy0PH7sklK5ARqZQaWcED-ht5Dr2-gLv5XGPg58yNR_GOsVY17cW%0D%0Aazp-xEKGWBZY_-tb8bzvaelZ9Q7SvtwAYIL9zuZk4vhbMppnm_1FJrljBfU1gKYN38rh5anmsRF1%0D%0Aqk54nKMC4iY%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYuyU2bi120t3vne_DGwTII1sSCXfw3QcgVHUBuUQLw6gQH39tX2ypec3StrQga44gTtam_rprf%0D%0APBEDmUswXH-NwY1XKZuaFzhsg4gUnB91GCVBYeKm_kiFqGC8GRy-NlzIdWMgSXZSAgDk9g0-Qq3T%0D%0AcHSi48f8cZAoAcD62rOeaGPyMYaFfZ5KoTyHx3qrEOqJ41HGUWupQyMyjya4-DwhINwQHdUm0vBk%0D%0AxZG0IXH-KKQ%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYurkDVT296euxD0NSFo2XcRulLaTCrHU3k-oJq8PiXUGf5WhLycXZeC6O5A3Myl-sk_wsqI3QG%0D%0AKHlCwgdiavrdJzgvO1dy3fOpXaHJPUeaF8M_u-SJMWIt1f7EbDZ5ue4C6frYEi2SQxDfvJZVQUdN%0D%0AfwWuHLkpG5oqf8v-u_MvTBhO4Q3Lqx3YUpUXcudwWrTKNwLb38gZRWnJl1USGF5-GdsrnBnxF-Th%0D%0AQgusXFew2yqakIMuFUm9TNkzkJcpRM3R%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYrpdyxUBxzQ2ZpE_50rTRIIyyGWXx5icaIPu1Hp5OeDJXq-SCJ19JqX-3DArVaDaDnck9wmIbN%0D%0A1ld1QZNtLsTSYkZMxqZW0DbmnxR4Zfm8tTcA2BzVbLNiOKUUzz0HWdHdU5HtZb8RStJJZzXN4Gx3%0D%0AF-p54_uldQlnRXoMTh_Wcao%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYimfITisW5heH-8m8tHV2Acl4lumebOYj-w5kX73Pd7EHuPvpIB3zDChq1Rqm30-bMqCG9XBdA%0D%0Amq7upzHoKzDkRuFVFqPj_z3Hm9iJZqzoxw5m7Fp9Zg6cBzMP50O2YuxPKu01EQ8GB23ExEmuUGjI%0D%0AlQ-HNp5VU3Ac_rMzffcsGL8%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-2788217861._CB310738457_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-1773905456._CB310738452_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-605951042._CB311976234_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-3991510647._CB311976197_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=0101cb9d28ffe1d7a1a4f83c5448762d73453641e727585873d55b08d8285970b600",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=472914126105"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-232398347._CB349580400_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=472914126105&ord=472914126105";
    },"unable to request AAN pixel");
</script>

<div id="flashContent" style="width: 0px; height: 0px; overflow:hidden;">
    <script type="text/javascript">
        if (generic && generic.monitoring.record_metric && flashAdUtils.canPlayFlash) {
            var flashLoaded = 0;
            function swfLoaded() {
                flashLoaded = 1;
                generic.monitoring.record_metric('ads_flash_did_play', 1, true);
            }
            window.onload = function() {
                // Flash was not loaded
                if (flashLoaded === 0) {
                    // Flash could be loaded but wasn't
                    if (flashAdUtils.canPlayFlash()) {
                        generic.monitoring.record_metric('ads_flash_can_but_did_not_play', 0, true);
                    } else {
                        generic.monitoring.record_metric('ads_flash_cannot_play', 0, true);
                    }
                }
            }
            /**
             * There is a rare possibility of window.onload getting called before swfLoaded.
             * Firing this metric to keep track of such calls.
             */
             generic.monitoring.record_metric('ads_flash_page_loaded', 1, true);
         }
    </script>
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="1x1" align="middle">
        <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
        <param name="quality" value="high" />
        <param name="play" value="true" />
        <param name="loop" value="true" />
        <param name="wmode" value="transparent" />
        <param name="scale" value="showall" />
        <param name="menu" value="true" />
        <param name="devicefont" value="false" />
        <param name="salign" value="" />
        <param name="allowScriptAccess" value="always" />

        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" width="1" height="1">
            <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
            <param name="quality" value="high" />
            <param name="play" value="true" />
            <param name="loop" value="true" />
            <param name="wmode" value="transparent" />
            <param name="scale" value="showall" />
            <param name="menu" value="true" />
            <param name="devicefont" value="false" />
            <param name="salign" value="" />
            <param name="allowScriptAccess" value="always" />
        </object>
        <!--<![endif]-->
    </object>
</div>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="843"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
