



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

    
    
    

    
    
    
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "470-8563106-2419441";
                var ue_id = "10JTSYJ3ZT5XN7RRY3GF";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        

        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />

    <meta property='fb:app_id' content='115109575169727' />

    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="10JTSYJ3ZT5XN7RRY3GF" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1d-c3-2xl-i-cf5cd779.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-3133038884._CB286861937_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-3118624260._CB293333852_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-2454701167._CB293329573_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-2508181169._CB288096406_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-3988610918._CB289934424_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
(function(){var d=function(o){return Object.prototype.toString.call(o)==="[object Array]";},g=function(q,p){var o;for(o=0;o<q.length;o++){if(o in q){p.call(null,q[o],o);}}},h=[],k,b,l=false,n=false,f=function(){var o=[],p=[],q={};g(h,function(s){var r="";g(s.dartsite.split("/"),function(t){if(t!==""){if(t in q){}else{q[t]=o.length;o.push(t);}r+="/"+q[t];}});p.push(r);});return{iu_parts:o,enc_prev_ius:p};},c=function(){var o=[];g(h,function(q){var p=[];g(q.sizes,function(r){p.push(r.join("x"));});o.push(p.join("|"));});return o;},m=function(){var o=[];g(h,function(p){o.push(a(p.targeting));});return o.join("|");},a=function(r,o){var q,p,s=[];for(q in r){p=[];for(j=0;j<r[q].length;j++){p.push(encodeURIComponent(r[q][j]));}if(o){s.push(q+"="+encodeURIComponent(p.join(",")));}else{s.push(q+"="+p.join(","));}}return s.join("&");},e=function(){var o=+new Date();if(n){return;}if(!this.readyState||"loaded"===this.readyState){n=true;if(l){imdbads.cmd.push(function(){for(i=0;i<h.length;i++){generic.monitoring.record_metric(h[i].name+".fail",csm.duration(o));}});}}};window.tinygpt={define_slot:function(r,q,o,p){h.push({dartsite:r.replace(/\/$/,""),sizes:q,name:o,targeting:p});},set_targeting:function(o){k=o;},callback:function(q){var r,p={},t,o,s=+new Date();l=false;for(r=0;r<h.length;r++){t=h[r].dartsite;o=h[r].name;if(q[r][t]){p[o]=q[r][t];}else{window.console&&console.error&&console.error("Unable to correlate GPT response for "+o);}}imdbads.cmd.push(function(){for(r=0;r<h.length;r++){ad_utils.slot_events.trigger(h[r].name,"request",{timestamp:b});ad_utils.slot_events.trigger(h[r].name,"tagdeliver",{timestamp:s});}ad_utils.gpt.handle_response(p);});},send:function(){var r=[],q=function(s,t){if(d(t)){t=t.join(",");}if(t){r.push(s+"="+encodeURIComponent(""+t));}},o,p;if(h.length===0){tinygpt.callback({});return;}q("gdfp_req","1");q("correlator",Math.floor(4503599627370496*Math.random()));q("output","json_html");q("callback","tinygpt.callback");q("impl","fifs");q("json_a","1");result=f();q("iu_parts",result.iu_parts);q("enc_prev_ius",result.enc_prev_ius);q("prev_iu_szs",c());q("prev_scp",m());q("cust_params",a(k,true));o=document.createElement("script");p=document.getElementsByTagName("script")[0];o.async=true;o.type="text/javascript";o.src="http://pubads.g.doubleclick.net/gampad/ads?"+r.join("&");o.id="tinygpt";o.onload=o.onerror=o.onreadystatechange=e;l=true;p.parentNode.insertBefore(o,p);b=+new Date();}};})();</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[300,600],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['a'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['2'],
'u': ['995197607842'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-tarnhelm-1896858743._CB301456883_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"52cf401b102401ab231cee9440cece737088127f",
"2016-01-05T18%3A04%3A24GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 50136;
generic.days_to_midnight = 0.5802778005599976;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-3512629095._CB289935695_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'a']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm == null || window.generic == null || window.consoleLog == null)) {
                    if (window.console && console.log) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=a;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=995197607842;ord=995197607842?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=a;bpx=2;c=0;s=3075;s=32;ord=995197607842?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=a;bpx=2;c=0;s=3075;s=32;ord=995197607842?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/chart/toptv/?ref_=nv_tp_tv250_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                        <li><a href="/chart/top?ref_=nv_mv_250_6"
>Top Rated Movies</a></li>
                        <li><a href="/chart/moviemeter?ref_=nv_mv_mpm_7"
>Most Popular Movies</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/chart/?ref_=nv_ch_cht_1"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_2"
>Oscar Winners</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_3"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/imdbpicks/tv-releases/?ref_=nv_tvv_picks_1"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_2"
>On Tonight</a></li>
                        <li><a href="/chart/toptv/?ref_=nv_tvv_250_3"
>Top Rated TV Shows</a></li>
                        <li><a href="/chart/tvmeter?ref_=nv_tvv_mptv_4"
>Most Popular TV Shows</a></li>
                        <li><a href="/feature/watch-now-on-amazon/?ref_=nv_tvv_wn_5"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_6"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_2"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/imdbpicks/?ref_=nv_sf_picks_3"
>IMDb Picks</a></li>
                        <li><a href="/best-of/?ref_=nv_sf_bstof_4"
>Best of 2015</a></li>
                        <li><a href="/holiday-streaming/?ref_=nv_tvv_hldy_5"
>Holiday Streaming</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=01-05&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_2"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_3"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_4"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_5"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_6"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_7"
>Emmy Awards</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_8"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_9"
>Toronto Film Festival</a></li>
                        <li><a href="/event/all/?ref_=nv_ev_all_10"
>All Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni59346200/?ref_=nv_nw_tn_1"
> John Boyega, Brie Larson And Taron Egerton Among BAFTA Rising Star Nominees
</a><br />
                        <span class="time">5 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59340662/?ref_=nv_nw_tn_2"
> Michael Bay Confirms Heâll Direct âTransformers 5â But âThis Is the Last Oneâ
</a><br />
                        <span class="time">18 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59338041/?ref_=nv_nw_tn_3"
> âStar Warsâ Possibly a Day Away From Stealing âAvatarâsâ Box Office Crown
</a><br />
                        <span class="time">20 hours ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_3"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5>
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0050083/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTk0MDEzMjI3NV5BMl5BanBnXkFtZTcwODg4NDc3Mw@@._V1._SX430_CR10,10,410,315_.jpg",
            titleYears : "1957",
            rank : 5,
                    headline : "12 Angry Men"
    },
    nameAd : {
            clickThru : "/name/nm0000136/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMjQ0NzA5NTQzOF5BMl5BanBnXkFtZTcwOTkwNjE5Ng@@._V1._SX250_CR0,0,250,315_.jpg",
            rank : 53,
            headline : "Johnny Depp"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYk0BnNAJl70ViJ5Cz5ifjGJjqlWB-qeL9TxrQAf1_RxFQ9HU2dy2tW6ixeT4FEG2BWG2BfT0IS%0D%0AlO-rqQyB4Znp0QQlX26PeMAZu0M0rpRqT9nSvoWGoXcki5BjpUTLKOFnwY6G6XNs1Vl9xcGCpqXt%0D%0A9XV4VcGl6_PQK7vTEhpjC8CNgPSn6pgBcpmbQENJQZss9gf8SX_i4Gys_XcMdw878w%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYpsZzIRsL5vHrhi7NxpU3BYUASOWBtaBBTkFvHpVxXxOI02R_6t85icGmHOaX13qrZejP80lls%0D%0AJMbgbHwovonlJqzB8wXTEGwcPjxzVd7vSUvIDzlPxGcBM4I4XZ9n1oBQ7cCc9I-vahE9Rk-Xsbq7%0D%0ApRv-w9N2WzfBeLRRv30sqi5nlXP69spQJ5fjfb-4Uuir%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYhTQXlS7onz_jrnN1sHxLnx1C9SBikfyhjuvwtFGGFda3Xuw4FwMztGGEtz4JbGl6cGkmjyyk4%0D%0AntqsN6G07A9JJK_zsIZW7jhtBtVuXUGXColxcrI29uWTwBE_XFCLw6mprxtDss7DnmbRkwElIONL%0D%0A4hWzZaelve6GmKG2i-5rH5UIPXS3KZcvxpKxezDEgSra%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=995197607842;ord=995197607842?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>

    <div id="pagecontent">
    <div class="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=995197607842;ord=995197607842?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	

    
    
    

    
    
    

    
    
    
    </div>
    <div id="top-slot-wrapper" class="pagecontent">

    
    
    

    
    
    
    </div>
    <div class="pagecontent">
        <div id="content-2-wide">
            <div id="main">

    
    
        <a name="slot_hero"></a>
        <div class="heroWidget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','HeroWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_hero">
<div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2511975705?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377628202&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377628202&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi2511975705" data-source="bylist" data-id="ls002309697" data-rid="10JTSYJ3ZT5XN7RRY3GF" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="Watch the mid-season premiere trailer for &quot;Heroes Reborn;&quot; season 1 returns Thursday, Jan. 7." alt="Watch the mid-season premiere trailer for &quot;Heroes Reborn;&quot; season 1 returns Thursday, Jan. 7." src="http://ia.media-imdb.com/images/M/MV5BNDg2NTI5OTYzOV5BMl5BanBnXkFtZTgwMzI0MzA1NjE@._V1_SY298_CR11,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNDg2NTI5OTYzOV5BMl5BanBnXkFtZTgwMzI0MzA1NjE@._V1_SY298_CR11,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Watch the mid-season premiere trailer for &quot;Heroes Reborn;&quot; season 1 returns Thursday, Jan. 7." title="Watch the mid-season premiere trailer for &quot;Heroes Reborn;&quot; season 1 returns Thursday, Jan. 7." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Watch the mid-season premiere trailer for &quot;Heroes Reborn;&quot; season 1 returns Thursday, Jan. 7." title="Watch the mid-season premiere trailer for &quot;Heroes Reborn;&quot; season 1 returns Thursday, Jan. 7." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt3556944/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377628202&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377628202&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > "Heroes Reborn" </a> </div> </div> <div class="secondary ellipsis"> Season 1 Returns </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3099178265?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377628202&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377628202&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi3099178265" data-source="bylist" data-id="ls056131825" data-rid="10JTSYJ3ZT5XN7RRY3GF" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="Check out MTV's latest fantasy series about adventures, war, and evil that occur throughout the history of the Four Lands." alt="Check out MTV's latest fantasy series about adventures, war, and evil that occur throughout the history of the Four Lands." src="http://ia.media-imdb.com/images/M/MV5BMTkxNjEwOTY4M15BMl5BanBnXkFtZTgwNTA2ODk0NzE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTkxNjEwOTY4M15BMl5BanBnXkFtZTgwNTA2ODk0NzE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Check out MTV's latest fantasy series about adventures, war, and evil that occur throughout the history of the Four Lands." title="Check out MTV's latest fantasy series about adventures, war, and evil that occur throughout the history of the Four Lands." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Check out MTV's latest fantasy series about adventures, war, and evil that occur throughout the history of the Four Lands." title="Check out MTV's latest fantasy series about adventures, war, and evil that occur throughout the history of the Four Lands." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt1051220?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377628202&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377628202&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > "The Shannara Chronicles" </a> </div> </div> <div class="secondary ellipsis"> Series Premiere: Jan. 5 on MTV </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi565884185?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377628202&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377628202&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi565884185" data-source="bylist" data-id="ls053181649" data-rid="10JTSYJ3ZT5XN7RRY3GF" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="In the animal city of Zootopia, a fast-talking fox who's trying to make it big goes on the run when he's framed for a crime he didn't commit. Zootopia's top cop, a self-righteous rabbit, is hot on his tail, but when both become targets of a conspiracy, they're forced to team up and discover even natural enemies can become best friends." alt="In the animal city of Zootopia, a fast-talking fox who's trying to make it big goes on the run when he's framed for a crime he didn't commit. Zootopia's top cop, a self-righteous rabbit, is hot on his tail, but when both become targets of a conspiracy, they're forced to team up and discover even natural enemies can become best friends." src="http://ia.media-imdb.com/images/M/MV5BMjQwOTM4NTE0OF5BMl5BanBnXkFtZTgwMDkyMjM5MzE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQwOTM4NTE0OF5BMl5BanBnXkFtZTgwMDkyMjM5MzE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="In the animal city of Zootopia, a fast-talking fox who's trying to make it big goes on the run when he's framed for a crime he didn't commit. Zootopia's top cop, a self-righteous rabbit, is hot on his tail, but when both become targets of a conspiracy, they're forced to team up and discover even natural enemies can become best friends." title="In the animal city of Zootopia, a fast-talking fox who's trying to make it big goes on the run when he's framed for a crime he didn't commit. Zootopia's top cop, a self-righteous rabbit, is hot on his tail, but when both become targets of a conspiracy, they're forced to team up and discover even natural enemies can become best friends." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="In the animal city of Zootopia, a fast-talking fox who's trying to make it big goes on the run when he's framed for a crime he didn't commit. Zootopia's top cop, a self-righteous rabbit, is hot on his tail, but when both become targets of a conspiracy, they're forced to team up and discover even natural enemies can become best friends." title="In the animal city of Zootopia, a fast-talking fox who's trying to make it big goes on the run when he's framed for a crime he didn't commit. Zootopia's top cop, a self-righteous rabbit, is hot on his tail, but when both become targets of a conspiracy, they're forced to team up and discover even natural enemies can become best friends." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt3475734?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377628202&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377628202&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > "Agent Carter" </a> </div> </div> <div class="secondary ellipsis"> Season 2 First Look </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/trailers?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377628202&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377628202&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Browse more trailers</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','HeroWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-1"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/awards-central/video/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376499602&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_jl_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376499602&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Jennifer Lawrence's 'Embarrassing' First IMDb Credit</h3> </a> </span> </span> <p class="blurb"><a href="/name/nm2225369/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376499602&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_jl_lk1">Jennifer Lawrence</a>, the <a href="/awards-central/golden-globes/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376499602&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_jl_lk2">Golden Globe-nominated</a> star of <i><a href="/title/tt2446980/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376499602&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_jl_lk3">Joy</a></i>, shares the funny story behind her first on-screen role.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:624px;height:auto;" > <div style="width:624px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1062122009?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376499602&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_jl_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376499602&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi1062122009" data-source="bylist" data-id="ls031574778" data-rid="10JTSYJ3ZT5XN7RRY3GF" data-type="playlist" class="video-colorbox" data-refsuffix="hm_ac_jl" data-ref="hm_ac_jl_i_1"> <img itemprop="image" class="pri_image" title="IMDb on the Scene (2015-)" alt="IMDb on the Scene (2015-)" src="http://ia.media-imdb.com/images/M/MV5BNzYwNzg1MTk4NF5BMl5BcG5nXkFtZTgwMTc2NjI2NzE@._UX1000_CR190,44,624,351_SY351_SX624_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNzYwNzg1MTk4NF5BMl5BcG5nXkFtZTgwMTc2NjI2NzE@._UX1000_CR190,44,624,351_SY351_SX624_AL_UY702_UX1248_AL_.jpg" /> <img alt="IMDb on the Scene (2015-)" title="IMDb on the Scene (2015-)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="IMDb on the Scene (2015-)" title="IMDb on the Scene (2015-)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/awards-central/video/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376499602&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_jl_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376499602&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Watch Jennifer Lawrence discuss her first IMDb credit</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-2"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/awards-central/golden-globes-all-stars?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376623742&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_gg_ph_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376623742&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Golden Globe All-Stars</h3> </a> </span> </span> <p class="blurb">Check out photos of actors and actresses who have won at least three Golden Globe awards.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/golden-globes-all-stars?imageid=rm3162541056&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376623742&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_gg_ph_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376623742&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTY4NzAzMzgxOV5BMl5BanBnXkFtZTcwMTgxNzgyMw@@._SX1400_CR490,30,296,438_SY219_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY4NzAzMzgxOV5BMl5BanBnXkFtZTcwMTgxNzgyMw@@._SX1400_CR490,30,296,438_SY219_SX148_AL_UY438_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/golden-globes-all-stars?imageid=rm464248064&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376623742&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_gg_ph_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376623742&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BODE0MDIwNTI2OV5BMl5BanBnXkFtZTgwMDk2Njc0MDE@._SX1000_CR450,0,296,438_CT-25_SY219_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BODE0MDIwNTI2OV5BMl5BanBnXkFtZTgwMDk2Njc0MDE@._SX1000_CR450,0,296,438_CT-25_SY219_SX148_AL_UY438_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/golden-globes-all-stars?imageid=rm2345960704&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376623742&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_gg_ph_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376623742&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjEyMTY0NTQxNV5BMl5BanBnXkFtZTcwMjQwNzEzNA@@._CR230,0,296,438_SY219_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjEyMTY0NTQxNV5BMl5BanBnXkFtZTcwMjQwNzEzNA@@._CR230,0,296,438_SY219_SX148_AL_UY438_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/golden-globes-all-stars?imageid=rm2822360064&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376623742&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_gg_ph_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376623742&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Al Pacino in Serpico (1973)" alt="Still of Al Pacino in Serpico (1973)" src="http://ia.media-imdb.com/images/M/MV5BMjMxMDk5OTgxMl5BMl5BanBnXkFtZTgwNTY2NjIwMjE@._V1_SY219_CR0,0,148,219_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjMxMDk5OTgxMl5BMl5BanBnXkFtZTgwNTY2NjIwMjE@._V1_SY219_CR0,0,148,219_AL_UY438_UX296_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/awards-central/golden-globes-all-stars?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376623742&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_gg_ph_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376623742&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See the full gallery</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-3"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NewsDeskWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59346200?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjEzNTgzNTI1MF5BMl5BanBnXkFtZTgwMjc2OTM5MDE@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59346200?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1"
class="headlines" >John Boyega, Brie Larson And Taron Egerton Among BAFTA Rising Star Nominees</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_src"
>Deadline</a></span>
    </div>
                                </div>
<p><a href="/name/nm3915784?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk1">John Boyega</a>, <a href="/title/tt3170832?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk2">Room</a>âs <a href="/name/nm0488953?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk3">Brie Larson</a> and <a href="/title/tt2802144?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk4">Kingsman: The Secret Service</a>âs <a href="/name/nm5473782?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk5">Taron Egerton</a> are among the nominees for this yearâs BAFTA Rising Star Award. Also in the mix are <a href="/name/nm0424848?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk6">Dakota Johnson</a> (<a href="/title/tt2322441?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk7">Fifty Shades Of Grey</a>) and <a href="/name/nm2525790?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk8">Bel Powley</a> (<a href="/title/tt3172532?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk9">The Diary Of A Teenage Girl</a>). The award, which has previously been won by the ...                                        <span class="nobr"><a href="/news/ni59346200?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59340662?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2"
class="headlines" >Michael Bay Confirms Heâll Direct âTransformers 5â But âThis Is the Last Oneâ</a>
    <div class="infobar">
            <span class="text-muted">18 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2_src"
>The Wrap</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59338041?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3"
class="headlines" >âStar Warsâ Possibly a Day Away From Stealing âAvatarâsâ Box Office Crown</a>
    <div class="infobar">
            <span class="text-muted">20 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59337697?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4"
class="headlines" >âBaywatchâ Movie Casts Model Kelly Rohrbach in Pamela Andersonâs Role</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59346407?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5"
class="headlines" >âCarol,â âBig Shortâ Lead Australian Academy Nominations</a>
    <div class="infobar">
            <span class="text-muted">4 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59346200?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjEzNTgzNTI1MF5BMl5BanBnXkFtZTgwMjc2OTM5MDE@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59346200?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1"
class="headlines" >John Boyega, Brie Larson And Taron Egerton Among BAFTA Rising Star Nominees</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_src"
>Deadline</a></span>
    </div>
                                </div>
<p><a href="/name/nm3915784?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk1">John Boyega</a>, <a href="/title/tt3170832?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk2">Room</a>âs <a href="/name/nm0488953?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk3">Brie Larson</a> and <a href="/title/tt2802144?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk4">Kingsman: The Secret Service</a>âs <a href="/name/nm5473782?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk5">Taron Egerton</a> are among the nominees for this yearâs BAFTA Rising Star Award. Also in the mix are <a href="/name/nm0424848?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk6">Dakota Johnson</a> (<a href="/title/tt2322441?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk7">Fifty Shades Of Grey</a>) and <a href="/name/nm2525790?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk8">Bel Powley</a> (<a href="/title/tt3172532?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk9">The Diary Of A Teenage Girl</a>). The award, which has previously been won by the ...                                        <span class="nobr"><a href="/news/ni59346200?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59346407?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2"
class="headlines" >âCarol,â âBig Shortâ Lead Australian Academy Nominations</a>
    <div class="infobar">
            <span class="text-muted">4 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59337697?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3"
class="headlines" >âBaywatchâ Movie Casts Model Kelly Rohrbach in Pamela Andersonâs Role</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59346998?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4"
class="headlines" >âStar Warsâ Speeds Past âAvengers,â âFurious 7â at Worldwide Box Office</a>
    <div class="infobar">
            <span class="text-muted">27 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59347003?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5"
class="headlines" >7 Unforgettable Creations From the Mind of Hayao Miyazaki</a>
    <div class="infobar">
            <span class="text-muted">33 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000139?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5_src"
>Indiewire</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59337381?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTM4OTkwNzg3NF5BMl5BanBnXkFtZTcwMjc5NjcyNw@@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59337381?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1"
class="headlines" >Louis C.K., Albert Brooks Animated Pilot in the Works at FX</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_src"
>The Wrap</a></span>
    </div>
                                </div>
<p><a href="/name/nm0000983?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk1">Albert Brooks</a> and <a href="/name/nm0127373?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk2">Louis C.K.</a> are teaming up for an untitled animated pilot at FX, TheWrap has learned. C.K.âs production company <a href="/company/co0363979?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk3">Pig Newton</a> is producing for <a href="/company/co0216537?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk4">FX Productions</a>, and both he and Brooks will serve as EPs. The two comedians, who will co-star in the upcoming animated feature â...                                        <span class="nobr"><a href="/news/ni59337381?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59337310?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2"
class="headlines" >Judd Apatowâs Netflix Comedy âLoveâ Gets Premiere Date, Teaser</a>
    <div class="infobar">
            <span class="text-muted">4 January 2016 6:20 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59337289?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3"
class="headlines" >TV Review: âThe Shannara Chroniclesâ</a>
    <div class="infobar">
            <span class="text-muted">4 January 2016 6:00 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59337271?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4"
class="headlines" >Ratings: âGalavantâ Returns With Soft Season Premiere Versus Sunday Night Football</a>
    <div class="infobar">
            <span class="text-muted">4 January 2016 5:44 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4_src"
>The Wrap</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59337290?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5"
class="headlines" >âEllen DeGeneres Showâ Renewed Through 2020</a>
    <div class="infobar">
            <span class="text-muted">4 January 2016 6:04 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5_src"
>The Wrap</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59337312?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTQ4MTcwODc0MV5BMl5BanBnXkFtZTgwMzkxMTMwNzE@._V1_SY150_CR62,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59337312?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1"
class="headlines" >Jaden Smith Is New Face of Louis Vuittonâs Womenswear Campaign (Photos)</a>
    <div class="infobar">
            <span class="text-muted">4 January 2016 6:18 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_src"
>The Wrap</a></span>
    </div>
                                </div>
<p><a href="/name/nm1535523?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk1">Jaden Smith</a>Â has joined the illustrious ranks of models chosen to represent Louis Vuitton â for their new womenâs line. Jaden, the son of Will and <a href="/name/nm0000586?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk2">Jada Pinkett Smith</a>, appeared in two photos (below) posted to the Instagram account ofÂ Nicolas GhesquiÃ¨re, the creative director for Louis Vuitton. In ...                                        <span class="nobr"><a href="/news/ni59337312?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59337324?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2"
class="headlines" >Steve Carell Says Buttery New Orleans Fare Helped Him Gain About 25 Lbs. for His Role in The Big Short</a>
    <div class="infobar">
            <span class="text-muted">4 January 2016 6:25 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59345558?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3"
class="headlines" >KhloÃ© Kardashian Promises Drinking Games and Crazy Stories on Kocktails with KhloÃ©</a>
    <div class="infobar">
            <span class="text-muted">just now</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59345452?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4"
class="headlines" >KhloÃ© Kardashian Promises Drinking Games and Crazy Stories on Kocktails with KhloÃ©</a>
    <div class="infobar">
            <span class="text-muted">just now</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59347217?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5"
class="headlines" >'I Escaped From a Polygamist Colony': One Woman's Shocking Story</a>
    <div class="infobar">
            <span class="text-muted">24 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NewsDeskWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-4"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>IMDb Snapshot</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm2506614272/rg784964352?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377627342&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377627342&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Chris Evans, Elizabeth Olsen, Jeremy Renner and Sebastian Stan in Captain America: Civil War (2016)" alt="Still of Chris Evans, Elizabeth Olsen, Jeremy Renner and Sebastian Stan in Captain America: Civil War (2016)" src="http://ia.media-imdb.com/images/M/MV5BMTYxNDE0Mzc5M15BMl5BanBnXkFtZTgwMDg3MDM2NzE@._V1_SY201_CR102,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTYxNDE0Mzc5M15BMl5BanBnXkFtZTgwMDg3MDM2NzE@._V1_SY201_CR102,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm2506614272/rg784964352?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377627342&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377627342&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > Latest Stills - First Look at <i>Captain America: Civil War</i> </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1785193984/rg80714496?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377627342&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377627342&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Johnny Depp and Amber Heard" alt="Johnny Depp and Amber Heard" src="http://ia.media-imdb.com/images/M/MV5BNjE1MDU4MTI5OF5BMl5BanBnXkFtZTgwNjU1MDM2NzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjE1MDU4MTI5OF5BMl5BanBnXkFtZTgwNjU1MDM2NzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm1785193984/rg80714496?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377627342&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377627342&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > Warm Moments From the 2016 Palm Springs International Film Festival </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3093816832/rg231709440?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377627342&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377627342&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Benedict Cumberbatch in Doctor Strange (2016)" alt="Still of Benedict Cumberbatch in Doctor Strange (2016)" src="http://ia.media-imdb.com/images/M/MV5BMjE5NDc5NzUwNV5BMl5BanBnXkFtZTgwMDM3MDM2NzE@._V1_SY201_CR54,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE5NDc5NzUwNV5BMl5BanBnXkFtZTgwMDM3MDM2NzE@._V1_SY201_CR54,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm3093816832/rg231709440?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377627342&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377627342&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <i>Doctor Strange</i> Photos and Concept Art </a> </div> </div> </div> </div> </div> </div> </div> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-5"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BornTodayWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_borntoday">
<span class="widget_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=1-5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Born Today</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0177896?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Bradley Cooper" alt="Bradley Cooper" src="http://ia.media-imdb.com/images/M/MV5BMjM0OTIyMzY1M15BMl5BanBnXkFtZTgwMTg0OTE0NzE@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjM0OTIyMzY1M15BMl5BanBnXkFtZTgwMTg0OTE0NzE@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0177896?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_1">Bradley Cooper</a> (41) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0005064?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="January Jones" alt="January Jones" src="http://ia.media-imdb.com/images/M/MV5BMTgyODE1Mjg4NF5BMl5BanBnXkFtZTcwMTE5MjQ1Nw@@._V1_SY172_CR11,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTgyODE1Mjg4NF5BMl5BanBnXkFtZTcwMTE5MjQ1Nw@@._V1_SY172_CR11,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0005064?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_2">January Jones</a> (38) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0000473?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Diane Keaton" alt="Diane Keaton" src="http://ia.media-imdb.com/images/M/MV5BNjU5NjMwOTk2NV5BMl5BanBnXkFtZTYwODg1NzY0._V1_SY172_CR2,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjU5NjMwOTk2NV5BMl5BanBnXkFtZTYwODg1NzY0._V1_SY172_CR2,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0000473?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_3">Diane Keaton</a> (70) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0000380?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Robert Duvall" alt="Robert Duvall" src="http://ia.media-imdb.com/images/M/MV5BMjk1MjA2Mjc2MF5BMl5BanBnXkFtZTcwOTE4MTUwMg@@._V1_SY172_CR3,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjk1MjA2Mjc2MF5BMl5BanBnXkFtZTcwOTE4MTUwMg@@._V1_SY172_CR3,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0000380?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_4">Robert Duvall</a> (85) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0005068?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Vinnie Jones" alt="Vinnie Jones" src="http://ia.media-imdb.com/images/M/MV5BMjI4MjY0NjQ3OV5BMl5BanBnXkFtZTgwMTYzNTQ1NzE@._V1_SY172_CR56,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI4MjY0NjQ3OV5BMl5BanBnXkFtZTgwMTYzNTQ1NzE@._V1_SY172_CR56,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0005068?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_5">Vinnie Jones</a> (51) </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/search/name?refine=birth_monthday&birth_monthday=1-5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See all birthdays</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','BornTodayWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-6"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/offsite/?page-action=offsite-amazon&token=BCYku4E8b_ZY-QGRf6cQtQ5RF0eFuIT-RhniNGR7UX7YaYzn1HstingT9fROIsEBgjgyk0jThrCz%0D%0A1QBjlthc4KJxxmEG9tUSkkKQgcRvVATkcuIajC6jbHqD92JlszJNcHED-4LgAUIH8MkgvqL9EO1u%0D%0AQmT9mHkP591TXQTKXAOdT2aF0TcWhFXX54YEI9uLxITjAtyb4cFJQgUeTKztxHZPlAFCLLviuKNN%0D%0A53xGwBUc5v7aE0_S3rHXAxDKpXtV09aA%0D%0A&ref_=hm_pop_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2378307002&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage" > <h3>IMDb Asks: Cynthia Nixon, Natalie Dormer, and Brian D'Arcy James</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?page-action=offsite-amazon&token=BCYpTbP1aUJYRkZooP-bS9EDSPmMeNfJyee3ot2Ncb59YiGvM1t0MEPKKqgqieo4j2BJbFqAzHQf%0D%0AttPOxEvPBmr8V96s7-8ZkE-CXwLs5D4j6eeWpzwAfnj0t8YW1SJ-QAtBocE1WVga6KvtAxVwZ9U8%0D%0A6DQLxcr77Pkn2pGEfUxUBLAfLnoLIm56KyzIH0lV1nXOLU1AYRVsExGIH81eNffNJS_Gr-lE4Mbi%0D%0AWSelJMM_1rL871TzoZjmufZc0FN47tmT%0D%0A&ref_=hm_pop_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2378307002&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjQwOTY1NTE2M15BMl5BanBnXkFtZTgwNDk5MjM2NzE@._SX830_CR240,0,402,402_.jpgg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQwOTY1NTE2M15BMl5BanBnXkFtZTgwNDk5MjM2NzE@._SX830_CR240,0,402,402_.jpgg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?page-action=offsite-amazon&token=BCYk-TWnsi-mO4dNUCAaLMz4rdDtZutkeMihdBwlHoLmfpx0JuI7SXuKX2ICeVUp7P27sLjjCrrQ%0D%0AlSfyYQ3e6tnpmBrWWGpBwnqAuospv48I4XkLQ_NF_QzN8rpZqAEGjhBDZy7ATYCPqkgz-oBV--t6%0D%0ATZC-Gfy0qaE56LryAPIj19cQRe8ZVvJHEpMWcVzTtMpTrPBFgh5ww8ledecg4OH2Z0qRan8RFI8u%0D%0A9w2rnFH5VaNQI9jq--__jJqHQePKDhp3%0D%0A&ref_=hm_pop_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2378307002&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTY3NDM2NDkyOV5BMl5BanBnXkFtZTgwMzc0MDQ1NzE@._SX490_CR54,0,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY3NDM2NDkyOV5BMl5BanBnXkFtZTgwMzc0MDQ1NzE@._SX490_CR54,0,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?page-action=offsite-amazon&token=BCYskiRpi2ToRXxj0_rZqPaDSWeZM_hkqy5GFEYNjHB1V0kxoBxxqys95-uvCIDgjww8ewRY5w-p%0D%0AdHAnn7D1nVykkVQipgfQV-dSP7GKHLZDsJ_OpN-P3OsGa13wIuOhEQeD_TvLCwH-1Cx5N4MlpyLl%0D%0AIPxEMllWPCAMJzzj8747Ht6yPoXBO3DX6zgq05vXLVCfkb44Ej3xMdyxmodJJA9LZe9eqK1MOvjq%0D%0A1A6qUm7Hz8B-6cYxhoMvqPszxBLisVlD%0D%0A&ref_=hm_pop_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2378307002&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTg0NjcyMjc1NV5BMl5BanBnXkFtZTgwNDk0MDQ1NzE@._UX430_CR20,20,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg0NjcyMjc1NV5BMl5BanBnXkFtZTgwNDk0MDQ1NzE@._UX430_CR20,20,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">On Tuesday, Jan. 5, at 9 p.m. ET/6 p.m. PT, IMDb Asks brings you a livestream Q&A and online chat with <a href="/name/nm0633223/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2378307002&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pop_lk1">Cynthia Nixon</a>, star of the off-Broadway play "MotherStruck!" Tune in to <a href="/offsite/?page-action=offsite-amazon&token=BCYmF3YBzEnlSOjw0QkN8PoFDV6QkhhlOEfpxH2aN47JzTXAInw6Mbhzc6EIcrXsZCLfxOpjlycV%0D%0AudEXPkS9DRa8C90txFYHNZxtm8SJV7-OpFekjmOvU_HCGeIsKq22IfLNto5G6umTXHqaAXNZ_pG9%0D%0AnRE49BgznUze5CKNADy8cjpUil3txQp7FKkMYXkSKSlodZrem_rknxCswAroG53vJJ-J97fYB1Xi%0D%0AGd4CCSSjuPSwjj93PaOZtYrzSyeza4CC%0D%0A&ref_=hm_pop_lk2">Amazon.com/CynthiaNixon</a> to participate in the live conversation and even ask a question yourself. Plus, catch up with <a href="/name/nm1754059/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2378307002&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pop_lk3">Natalie Dormer</a>, star of <i><a href="/title/tt3387542/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2378307002&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pop_lk4">The Forest</a></i>, and <a href="/name/nm0195421/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2378307002&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pop_lk5">Brian D'Arcy James</a>, star of Golden Globe-nominated film <i><a href="/title/tt1895587/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2378307002&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pop_lk6">Spotlight</a></i>. The livestream is best viewed on laptops, desktops, and tablets.</p> <p class="seemore"><a href="/offsite/?page-action=offsite-amazon&token=BCYlB2GjqbTYvrgzl51u-BZNWzS45esvLS8eTS9LTIeehQyF9lUjbswJDNkCyxeova1CV7fI4THs%0D%0AkKd1XjJVYE2jyYQv8tYPdpPj_tZIrtYmsboFmqzLkRhq2O7-uSgkyMkRiJ9MkyrEDD1BbyX-XvKq%0D%0AI8F6By2ML9ufu3RBfGYbpZ148mLk2bLliYhMTNYR81ChDwQ03hoElAVuyuaWNKi4PWApEaj8egiP%0D%0AIdD58Of1vwpxwheXjhtDzmI9jiOV5BF1%0D%0A&ref_=hm_pop_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2378307002&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Tune in for our one-on-one interviews</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-7"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/awards-central/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376489222&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_gg_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376489222&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Ranking the Golden Globe TV Nominees</h3> </a> </span> </span> <p class="blurb">We ranked the Golden Globe nominees by their current STARmeter rankings and took a look at their top STARmeter rankings in 2015. Here's a look at the TV drama contenders.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/golden-globes-actor-tv-drama-starmeter/ls031649115?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376489222&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_gg_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376489222&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjI0NjY2MzQ3OF5BMl5BanBnXkFtZTgwMzcxNzMzNjE@._SX860_CR107,50,614,460_SY230_SX307_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI0NjY2MzQ3OF5BMl5BanBnXkFtZTgwMzcxNzMzNjE@._SX860_CR107,50,614,460_SY230_SX307_AL_UY460_UX614_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/awards-central/golden-globes-actor-tv-drama-starmeter/ls031649115?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376489222&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_gg_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376489222&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > Best Actor - TV Drama Nominees Ranked by STARMeter </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/golden-globes-actress-tv-drama-starmeter/ls031649346?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376489222&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_gg_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376489222&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Eva Green in Penny Dreadful (2014)" alt="Still of Eva Green in Penny Dreadful (2014)" src="http://ia.media-imdb.com/images/M/MV5BMjI5MzU3NzUwNV5BMl5BanBnXkFtZTgwODgzMjgxNjE@._V1_SY230_CR19,0,307,230_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI5MzU3NzUwNV5BMl5BanBnXkFtZTgwODgzMjgxNjE@._V1_SY230_CR19,0,307,230_AL_UY460_UX614_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/awards-central/golden-globes-actress-tv-drama-starmeter/ls031649346?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376489222&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_gg_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376489222&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > Best Actress TV - Drama Nominees Ranked by STARmeter </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/awards-central/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376489222&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_gg_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376489222&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Visit Awards Central on IMDb</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-8"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/imdbpicks/january-indie-releases/ls031190014?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376002742&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_jan_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376002742&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <h3>January's Indie, Foreign, and Documentary Releases</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/january-indie-releases/ls031190014?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376002742&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_jan_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376002742&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Band of Robbers (2015)" alt="Band of Robbers (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTczNTE3ODc0NF5BMl5BanBnXkFtZTgwODU1MDQyNzE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTczNTE3ODc0NF5BMl5BanBnXkFtZTgwODU1MDQyNzE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/january-indie-releases/ls031190014?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376002742&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_jan_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376002742&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Ron Perlman and Rupert Grint in Moonwalkers (2015)" alt="Ron Perlman and Rupert Grint in Moonwalkers (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjQyNzU4MjMyNF5BMl5BanBnXkFtZTgwMDQ0NzQ0NzE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQyNzU4MjMyNF5BMl5BanBnXkFtZTgwMDQ0NzQ0NzE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/january-indie-releases/ls031190014?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376002742&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_jan_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376002742&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Ip Man 3 (2015)" alt="Ip Man 3 (2015)" src="http://ia.media-imdb.com/images/M/MV5BNTA2MTk3NzI5Ml5BMl5BanBnXkFtZTgwNzU2MzYyNzE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNTA2MTk3NzI5Ml5BMl5BanBnXkFtZTgwNzU2MzYyNzE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Watch trailers, check release dates, and learn more about the slate of indie, foreign, and documentary movies that are out this month.</p> <p class="seemore"><a href="/imdbpicks/january-indie-releases/ls031190014?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376002742&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_jan_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2376002742&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See our list</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-13"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','RecsWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','RecsWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-19"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TriviaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_trivia">
<span class="widget_header"> <span class="oneline"> <a href="/title/tt2888046/trivia?item=tr2344522&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Did You Know?</h3> </a> </span> </span> <div class="widget_content inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt2888046?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Ip Man 3 (2015)" alt="Ip Man 3 (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTkxNzc0MjgwN15BMl5BanBnXkFtZTgwMzY3Nzk1NzE@._V1_SY132_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTkxNzc0MjgwN15BMl5BanBnXkFtZTgwMzY3Nzk1NzE@._V1_SY132_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> <div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="widget_inline_blurb"><strong class="text-large"><a href="/title/tt2888046?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Ip Man 3</a></strong> <p class="blurb">This will be the last film in which <a href="/name/nm0947447?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Donnie Yen</a> portrays the character of Ip Man.</p> <p class="seemore"><a href="/title/tt2888046/trivia?item=tr2344522&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" class="position_blurb supplemental" >See more trivia</a></p> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','TriviaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-25"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','PollWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;ugCKIgDZKd4&quot;}">
        
    

    <div class="ab_poll poll">
<span class="widget_header"> <span class="oneline"> <a href="/poll/ugCKIgDZKd4/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2370308362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2370308362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Poll: Golden Globes 2016: Best Performance by an Actor in a Television Series - Drama</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2370308362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk">More Polls</a></h4> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/ugCKIgDZKd4/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2370308362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2370308362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Jon Hamm" alt="Jon Hamm" src="http://ia.media-imdb.com/images/M/MV5BNzg0MzA4MTY5M15BMl5BanBnXkFtZTcwODg2MTIwOQ@@._V1_SY207_CR1,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNzg0MzA4MTY5M15BMl5BanBnXkFtZTcwODg2MTIwOQ@@._V1_SY207_CR1,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/ugCKIgDZKd4/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2370308362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2370308362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Rami Malek" alt="Rami Malek" src="http://ia.media-imdb.com/images/M/MV5BODEwMjkzNDIzOV5BMl5BanBnXkFtZTgwOTQ4NTU0NzE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BODEwMjkzNDIzOV5BMl5BanBnXkFtZTgwOTQ4NTU0NzE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/ugCKIgDZKd4/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2370308362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2370308362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Wagner Moura" alt="Wagner Moura" src="http://ia.media-imdb.com/images/M/MV5BMTk2NjI5NzE1NF5BMl5BanBnXkFtZTgwOTAzMDMzNzE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk2NjI5NzE1NF5BMl5BanBnXkFtZTgwOTAzMDMzNzE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/ugCKIgDZKd4/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2370308362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2370308362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Bob Odenkirk" alt="Bob Odenkirk" src="http://ia.media-imdb.com/images/M/MV5BMjA1Njg1NzI1Ml5BMl5BanBnXkFtZTcwNTE5ODk1OA@@._V1_SY207_CR44,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA1Njg1NzI1Ml5BMl5BanBnXkFtZTcwNTE5ODk1OA@@._V1_SY207_CR44,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/ugCKIgDZKd4/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2370308362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2370308362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Liev Schreiber" alt="Liev Schreiber" src="http://ia.media-imdb.com/images/M/MV5BMjExMjkwODU0NF5BMl5BanBnXkFtZTcwMDAzMjcyNw@@._V1_SY207_CR11,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjExMjkwODU0NF5BMl5BanBnXkFtZTcwMDAzMjcyNw@@._V1_SY207_CR11,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Which Actor should win the Golden Globe award for Best Performance by an Actor in a Television Series - Drama at the 73rd Annual Golden Globe Awards 2016? Discuss <a href="http://www.imdb.com/board/bd0000088/thread/251386588?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2370308362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk1">here</a> after voting.</p> <p class="seemore"><a href="/poll/ugCKIgDZKd4/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2370308362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2370308362&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-25&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Vote now</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','PollWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
        <a name="slot_center-28"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2367584382&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2367584382&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Catch Up on IMDb Asks</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2367584382&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2367584382&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTUzNDUzMTY3NV5BMl5BanBnXkFtZTgwMDUxOTE1NzE@._SX540_CR120,20,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUzNDUzMTY3NV5BMl5BanBnXkFtZTgwMDUxOTE1NzE@._SX540_CR120,20,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2367584382&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2367584382&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTgxNTAxNTI5M15BMl5BanBnXkFtZTgwODQxOTE1NzE@._SX440_CR0,20,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTgxNTAxNTI5M15BMl5BanBnXkFtZTgwODQxOTE1NzE@._SX440_CR0,20,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2367584382&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2367584382&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTg5OTA4Njg1NF5BMl5BanBnXkFtZTgwNzY0NDY0NzE@._UX307_CR0,20,307,307_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg5OTA4Njg1NF5BMl5BanBnXkFtZTgwNzY0NDY0NzE@._UX307_CR0,20,307,307_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2367584382&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2367584382&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTcyNjkwNjkwNF5BMl5BanBnXkFtZTgwMjgxNzU0NzE@._UX480_CR120,40,307,307_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTcyNjkwNjkwNF5BMl5BanBnXkFtZTgwMjgxNzU0NzE@._UX480_CR120,40,307,307_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Did you miss one of our one-on-one interviews? Check out our archived interviews with your favorite stars.</p> <p class="seemore"><a href="/feature/imdb-asks?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2367584382&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ia_lp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2367584382&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Watch all our one-on-one interviews</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    
            </div>
            <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,300x600,11x1;p=tr;fv=1;ab=a;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=995197607842;ord=995197607842?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=a;bpx=2;c=0;s=3075;s=32;ord=995197607842?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=a;bpx=2;c=0;s=3075;s=32;ord=995197607842?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	

    
    
    

    
    
    

    
    
        <a name="slot_right-3"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2369559202&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2369559202&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Opening This Week</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3387542"></div> <div class="title"> <a href="/title/tt3387542?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2369559202&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t0"> The Forest </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1663202"></div> <div class="title"> <a href="/title/tt1663202?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2369559202&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t1"> The Revenant </a> <span class="secondary-text"></span> </div> <div class="action"> Nationwide Expansion </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2401878"></div> <div class="title"> <a href="/title/tt2401878?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2369559202&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t2"> Anomalisa </a> <span class="secondary-text"></span> </div> <div class="action"> Expansion </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3103166"></div> <div class="title"> <a href="/title/tt3103166?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2369559202&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t3"> The Masked Saint </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt0315642"></div> <div class="title"> <a href="/title/tt0315642?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2369559202&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t4"> Wazir </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3317208"></div> <div class="title"> <a href="/title/tt3317208?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2369559202&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t5"> Anesthesia </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4515684"></div> <div class="title"> <a href="/title/tt4515684?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2369559202&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t6"> Le trÃ©sor </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2992000"></div> <div class="title"> <a href="/title/tt2992000?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2369559202&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t7"> Yosemite </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2369559202&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2369559202&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See more opening this week</a></p>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	

    
    
        <a name="slot_right-4"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2369559342&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Tickets & Showtimes &raquo;</a></p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-6"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BoxOfficeListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/chart/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Now Playing (Box Office)</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2488496"></div> <div class="title"> <a href="/title/tt2488496?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t0"> Star Wars: The Force Awakens </a> <span class="secondary-text">$90.2M</span> </div> <div class="action"> <a href="/showtimes/title/tt2488496?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1528854"></div> <div class="title"> <a href="/title/tt1528854?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t1"> Daddy's Home </a> <span class="secondary-text">$29.2M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3460252"></div> <div class="title"> <a href="/title/tt3460252?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t2"> The Hateful Eight </a> <span class="secondary-text">$15.7M</span> </div> <div class="action"> <a href="/showtimes/title/tt3460252?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1850457"></div> <div class="title"> <a href="/title/tt1850457?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t3"> Sisters </a> <span class="secondary-text">$12.8M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2974918"></div> <div class="title"> <a href="/title/tt2974918?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t4"> Alvin and the Chipmunks: The Road Chip </a> <span class="secondary-text">$12.1M</span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/chart/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See more box office results</a></p>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','BoxOfficeListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-8"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Coming Soon</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2869728"></div> <div class="title"> <a href="/title/tt2869728?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t0"> Ride Along 2 </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4172430"></div> <div class="title"> <a href="/title/tt4172430?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t1"> 13 Hours: The Secret Soldiers of Benghazi </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1594972"></div> <div class="title"> <a href="/title/tt1594972?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t2"> Norm of the North </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4048668"></div> <div class="title"> <a href="/title/tt4048668?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t3"> Band of Robbers </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3165630"></div> <div class="title"> <a href="/title/tt3165630?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t4"> The Benefactor </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See more coming soon</a></p>
                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-12"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TwitterWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_twitter">
<span class="widget_header"> <span class="oneline"> <h3>Follow Us On Twitter</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/twitter-2032675409._CB285310264_.html#{"screen-name":"IMDb","height":"500px","width":"300px"}'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','TwitterWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-14"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','FacebookWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_facebook">
<span class="widget_header"> <span class="oneline"> <h3>Find Us On Facebook</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','FacebookWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-30"></a>
        <div class="aux-content-widget-2 sticky-widget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="http://www.imdb.com/video/imdb/vi4038898969/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377610602&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ded_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377610602&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <h3>New <i>Deadpool</i> TV Spot</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi4038898969?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377610602&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ded_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377610602&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi4038898969" data-rid="10JTSYJ3ZT5XN7RRY3GF" data-type="single" class="video-colorbox" data-refsuffix="hm_ded" data-ref="hm_ded_i_1"> <img itemprop="image" class="pri_image" title="Deadpool (2016)" alt="Deadpool (2016)" src="http://ia.media-imdb.com/images/M/MV5BMjIxNjUzMjE2MF5BMl5BanBnXkFtZTgwNzM0NTY0NjE@._V1_SY525_CR116,0,700,525_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjIxNjUzMjE2MF5BMl5BanBnXkFtZTgwNzM0NTY0NjE@._V1_SY525_CR116,0,700,525_AL_UY1050_UX1400_AL_.jpg" /> <img alt="Deadpool (2016)" title="Deadpool (2016)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Deadpool (2016)" title="Deadpool (2016)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="http://www.imdb.com/video/imdb/vi4038898969/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377610602&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ded_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377610602&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <i>Deadpool</i> </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Watch a TV spot for <i>Deadpool</i> that aired during "The Bachelor."</p> <p class="seemore"><a href="http://www.imdb.com/video/imdb/vi4038898969/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377610602&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ded_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2377610602&pf_rd_r=10JTSYJ3ZT5XN7RRY3GF&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Watch the TV Spot</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uet)==='function'){uet('be','NinjaWidget',{wb:1});}</script>
        




        </div>
    
            </div>
            <br class="clear" />
        </div>
        

    
    
    

    
    
    
        
    
        <br class="clear" />
    </div>
</div>
                





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYiDoplZOGdRtn8cNqgKwN0copEvGk1-6-tl8xgjBe0ptSVNDwoTveFvpO-NAxJucFPMV141EWm%0D%0AwNTPPYH5NwqWv5CRGOiXWj0DMBiA1VIeK-xzweE0Hakzl9rhoThXFMuJHVlZLBA1CABTBw3Ymqer%0D%0AcP5WevW9WWaGrQvL5Ssg4fyDzqyXNFjz6R8lUHfEtEVV0SiPlLQYxTnwnF0L-yRqp_-BUqNx7yuj%0D%0ADXHvhhYSxMY%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYs6NspHERurKT-lqjhRds_CSAapIckGd6lD4Ui1ANxe4Z0xgNvzlcaEvBEcTE9y1eZrm3NpffX%0D%0AlvlOUmYIAC3h5Wgec2BdghsUXNKUgGz25qr_rcZECC_nfVHlJdZVkrr3utwATriJDBnP4rIy2Hc5%0D%0AeoaLjon6VwlC0WBNrPN5h4aKdZn_ojZBhfLZos05CIPuQymvV0tJryOB7J7klqc61w%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYjd6lx9vCw1wfnXM4H5iaFX28lYBbx0OnZCrmms-udDnJv4QZwn8kshmf_pP1IfxVNToWPRpQd%0D%0AuSWRS2hwtWPGlP4l92-N_LAirb5nbGbJ2kYeIGdCCyC5_mzR_pDLndjvIVT3o50SuINQKRy0_1J9%0D%0AjHPI6QH_ebVXhu64jc_NiN2riiK3rn06kX2oXBCx7N4ZG37kHY28PVyXNjGmglemZztYbBnMuMte%0D%0ASaaOXSz2b4SXA2BTBpK-nNffbG8eikoFENNdFMm7pID1v-WajNI-tiFRWfTupVyEyVq5KAkAwKLK%0D%0APCSYn-EghqHRUMdUmntAg6Kyt9kJcpXUArWNXY8TAe-hXlMMs0j6f28FX5PqXnRUdfmr9dA_q5d8%0D%0ApJcA0Qz2oDSppZ_yHh5ykgqE0ge24w%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYtJYi56_YVzFIdbuBSloVHCHQ6FnyBhr-3ZBYT-2u3WTCmES2IuliGrt8Z8RPA_usc-IzbZNKo%0D%0Ad4Nh3lQtEXOqY3nyaiEJuSta8Ch1Z1YqW8uZSm-G8XeaTcOe2I7pQOAUAad5rGDeBQYuFRdwiyEE%0D%0AYZuf7Eflf-RCEEdm4_670cRwzMwJfX3Be5dhkQidl1kw7_SjiobOQOHldwOlz3MC4w%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYg-3cCTYgSttZWMaMTLHclyoB1zZluvo43uUind1jqqbnBnhOOs1mpOEYENculBpzMOtzZAW8w%0D%0AMdevYSfrI2-29IGIU9vS7ezYr_-en71vEqXFnB_ITSNQFhJnYsD2A4TcqbgU1WL3rIX_AYAZ5m4X%0D%0A-ht5yMzfj4SNGOkq8Ln0R3EKKHrUgX4GmCo-pv4nO4GA%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYgws_vTGEjTOHVPGD8wE55PFbn9oYjA11QfSAdX_HRdZR6hcEOjhSzPU3WlWaVE3rLMlXc8A0x%0D%0AmK6IP_MCKf38tMul-HlpPw1CkPLcHHEqSGtFDHJsbpQlBVE-8RW_rtxYi1J03vqho8-UDnLGuZBi%0D%0AKK1C9F_SC-jtHDOP2kYtM98BZoes2FbwEqCeP1RrRmk5%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top Rated Movies</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Box Office</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li><a href="/advertising/?ref_=ft_ad"
>Advertising</a></li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYlvySpCWV8WyXUTT4_14huq6q5vRMba_viKWa8PkFNCBdb8HZloSeafnFFEvh9cApg-G3Go_B7%0D%0ACkyLECyOotJJVaC1aU_zsGZMNJFHvookX2fQH7OBDhhcwlJLOMTHV62ZFkaJUf-_7B6HWDawXYkK%0D%0AFlC0_2_HuhGeslnuJ9_VgPBBuYBLIignhElouelsKEPy1I1D3BHu8JU07eGqhq5uKw%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYkWauRijQ7nXTwzkvSBP3yvocTn4nj_s-5IkK-dVpjIasxrlVq8tIlnzkYAtdbu0IY5_dpHCbh%0D%0A1yRUPM4YXIHdM6XY4hY1wvnlXRUpFKbqbP_lIdwZ4qeF2emh3c8Vi0GaWrMZlTJ1KAHVObcstASR%0D%0A3utqdmeOUVHdEFvzAfLMXN1POH_BmTYrgEzF4SukyD_n%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYp5-6xGe25r6WYgydVpnWyfEHx2OmeZe7s2wF0tRCdA9JkWH6xldCXX7acE4bg9yDGItzsAM3I%0D%0AB8gW2FLTz3wawj_-5sp_XDE-wFal01NXMsc6YJvFpcEfbCBfKKis3AKW4Y8qC3mn1Of_VeXs3Is7%0D%0Ase1-1_IFJFBKxF69tYMuww_F0qkoT7d5vb0xGoavpuP3GyZW6LzUwpVhP_lgah2OsjRCx33MDWKl%0D%0AqLD8nnSnsMx-lHh062DmFC5X1O40rq2E%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2016
                <a
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYssIouF5NFcv--o70zwpaqx_ni8mhqIDVsjce12DKxx4aTXjpnkJszrgGaPq8foiEic78b5Qyp%0D%0Ab0SYRzi4rzT7GWfN3WQTAZElczw-SjZakCtcA2p_uPpLV9lEA2mFmCPMPB9TEcCxwmPIdSxQMwzF%0D%0AAt61Lx01-3SbmkoU8EhHz1tIK6oD4VNdakX6KvYYl2i8m7B5WbmpDXGIN1dn13V-tZUoSXtUiiwy%0D%0AQZHjq6Fcy2M%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYtBg3pIu3Ume0c8JccwR_2XlgM-Fi5oO575L4jBvTXFWp6taf6OhD7kwFAaq4ZeZFh49G3DHDK%0D%0A3ABDphpwHmscLehKKexGi7tNNPEJRXBFIb7T6vz9BUes7YiTXdfCbFV9qfnFSlTYCX5-Dr2k_CLe%0D%0Ae7kkeMCdJggFcYLvUmdWehd6r7Ijg_aGLyuC5miHUnGWLQQRWAhc7XP72Y8U5_c2-DUpIc6loGPv%0D%0APNjxVFJCmDQ%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYtgB9jCG5RuUjF9dmk-uE3RF1e0tuioucwz86OZGfwJwdM5ePVHC6vVeQPrvMtEWph4FB_GnV-%0D%0AqaW49epyhiaq-9KNWpULu54OJEho9nT-Y7d0WwtUfjyVN47yez8WcWbPfThRPzXzwxQJLmN4pgez%0D%0AGZh2VGBJzY-RONLNhHIk9p7Wc3NBrIJ883JHKE0IIagMmIDKBCSVIjxMXh5Vsft5k4SKlSt2CmuU%0D%0AbcWhmUo37c0%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYhqEr9Eu1RlGXzBqBJKLFt_Io0RxsTiRw-IFE79Vkonv9TqAl4ZPeEuDspoHloA4wh_p1gbgja%0D%0AO_-PipiRbUzAt-4dN3UuoZ4l-RVKwMnj2tC-wYZO3tUyTiri1iQbBlWZs2s_shbuH3IwIqUnGqBI%0D%0AlX7zHbUcCjBBHYgAvTlX76yoPE5pLYX2sH2eWzoc7u8ShYp29khC67I-pGASYemttuEHUecouxh0%0D%0A_joKx6LpxDc%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYnFr_K3tX0kE4RUv7qUey5AMFZFlExVewxjiMqQee-Cxzf_8XejBC8XN_j0YwuOMe9KXDwCWXr%0D%0AZQKJ9qGBn0CvfcyhQeXp4Oo1C_M17AykzknSFhqa_tcVp9m6OZbqeZlc7_oBq-HwHsC2epcxdLko%0D%0AN8GG203H_fRsxsSBn-xdmD7HqfxhSar-2vrzt5nTjUJhm4-sPtF_J-xSclAtQF4ig9V44W-0oKi1%0D%0ATmELK6NaKtE%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYqGSfDfKYEeiCotdtY7eDLRNOb7sNPYwv-ub65MVUGo5nguFnDJX15NrGr8E6w12g3yVVd4t8I%0D%0AdLuG9_Mt3ziSNqvfRWRdl8DQJZisROnMTbk357Br7odM_B8VIbJCLKZGZY8BSrbY3pjwyDWoTYwB%0D%0AB1SNEoF5llFHOiohhJYUyxMr0XUsphGx-fx9wRcSpCTETS5K-duOivCYWx_qKaPUtzwEW4svWens%0D%0AyHXduujtijFkt2LbEWqARKh8fK6dOgbn%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYjxefkg09S0Sm-ZXUPwygxo1AHWBZcd-XgRlSTfT8LysLEE4rkNxVqN637VvIuxaO7nMW-dbsJ%0D%0ABzjU0byiIK7S_jvv4Otvq5MTOGCMUGsa88RhIRduaXkAn2lF6Zkdni-sdgHL-ARA5NX2eiD8taYF%0D%0AOI5QO6q5fIlgrc9KkNBg0nw%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYjuCY9k41DCYmnz1Rq7zb1FnvBQ7-xufWyr7jTtfHIreY7eqKRxl91CH77TzhosQlAii_dqe1N%0D%0AlNW9jtQEE1-88qqgWonbY_geLhD1jL8H_beiznu1aUcakCNC3WdLsCtImtEv8cpyu-a7akxCKOlC%0D%0AYBY8i46aoUC9agXZFqoFYdo%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/common-136239425._CB285914029_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-1173232238._CB285296592_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-335260898._CB289148898_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-194820129._CB286493173_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-2126106747._CB289323193_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=0101d06d51d12701260868dc412ba5c69a10bdf964d4dbff40cce1fd859b9928e536",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=995197607842"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-232398347._CB349580400_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=995197607842&ord=995197607842";
    },"unable to request AAN pixel");
</script>

<div id="flashContent" style="width: 0px; height: 0px; overflow:hidden;">
    <script type="text/javascript">
        if (generic && generic.monitoring.record_metric && flashAdUtils.canPlayFlash) {
            var flashLoaded = 0;
            function swfLoaded() {
                flashLoaded = 1;
                generic.monitoring.record_metric('ads_flash_did_play', 1, true);
            }
            window.onload = function() {
                // Flash was not loaded
                if (flashLoaded === 0) {
                    // Flash could be loaded but wasn't
                    if (flashAdUtils.canPlayFlash()) {
                        generic.monitoring.record_metric('ads_flash_can_but_did_not_play', 0, true);
                    } else {
                        generic.monitoring.record_metric('ads_flash_cannot_play', 0, true);
                    }
                }
            }
            /**
             * There is a rare possibility of window.onload getting called before swfLoaded.
             * Firing this metric to keep track of such calls.
             */
             generic.monitoring.record_metric('ads_flash_page_loaded', 1, true);
         }
    </script>
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="1x1" align="middle">
        <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
        <param name="quality" value="high" />
        <param name="play" value="true" />
        <param name="loop" value="true" />
        <param name="wmode" value="transparent" />
        <param name="scale" value="showall" />
        <param name="menu" value="true" />
        <param name="devicefont" value="false" />
        <param name="salign" value="" />
        <param name="allowScriptAccess" value="always" />

        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" width="1" height="1">
            <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
            <param name="quality" value="high" />
            <param name="play" value="true" />
            <param name="loop" value="true" />
            <param name="wmode" value="transparent" />
            <param name="scale" value="showall" />
            <param name="menu" value="true" />
            <param name="devicefont" value="false" />
            <param name="salign" value="" />
            <param name="allowScriptAccess" value="always" />
        </object>
        <!--<![endif]-->
    </object>
</div>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="340"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
