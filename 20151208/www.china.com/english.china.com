ï»¿<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>China.com - Your guide on traveling and living in China</title>
<meta name="keywords" content="China,travel,lifestyle,learn Chinese,news,videos,business,films,sports,reports" />
<meta name="description" content="English.china.com is a one-stop shop for everything about China â news, events, culture, people, lifestyle, language. It also provides information about traveling and living in China." />
<meta name="auther" content="F7 13489" />
<!-- /etc/htmlhead.shtml Start -->
<link href="/css/style.css?20141205.1" rel="stylesheet" />
<!--[if lte IE 6]> 
<script src="/js/DD_belatedPNG_0.0.8a-min.js" type="text/javascript"></script>
<script type="text/javascript">
DD_belatedPNG.fix('.page-select-language-list em, .page-nav .page-openSearch em, .page-focus-prevnext a, .page-video-right .item-text, .page-watched .play, .page-video-right .item-icon, .page-video-right .item-watch'); 
</script>
<![endif]-->
<!-- /etc/htmlhead.shtml End -->
<!-- /etc/goto3g.shtml Start -->

<!-- /etc/goto3g.shtml End -->
</head>

<body>
<!-- /etc/channelhomehead.shtml Start -->
<div class="page-head">
  <div class="page-top-bg" id="page-top">
    <div class="maxWidth page-top">
      <div class="page-logo"><a href="/index.html"><img src="/img/logo.png" alt="china.com" width="208" height="46" /></a></div>
      <div class="page-language">
        <em>language:English</em>
      </div>
      <div class="page-top-right">
          <div class="page-top-time">Thursday, October 24, 2013</div>
          <div class="page-select-language" id="page-select-language">
              <div class="page-show-language" style="display: block;">Language</div>
              <div class="page-select-language-tit" style="display: none;"><i>Language</i></div>
              <div class="page-select-language-list" style="display: none;">
                <a href="http://www.china.com/index.html" class="langCn"><em>www</em></a>
                <a href="http://english.china.com/index.html" class="langEn"><em>english</em></a>
                <a href="http://german.china.com/index.html" class="langDe"><em>german</em></a>
                <a href="http://italy.china.com/index.html" class="langIt"><em>italy</em></a>
                <a href="http://portuguese.china.com/index.html" class="langPt"><em>portuguese</em></a>
                <a href="http://french.china.com/index.html" class="langFr"><em>french</em></a>
                <a href="http://russian.china.com/index.html" class="langRu"><em>russian</em></a>
                <a href="http://espanol.china.com/index.html" class="langEs"><em>espanol</em></a>
                <a href="http://malay.china.com/index.html" class="langMy"><em>malay</em></a>
                <a href="http://vietnamese.china.com/index.html" class="langVn"><em>vietnamese</em></a>
                <a href="http://laos.china.com/index.html" class="langLa"><em>laos</em></a>
                <a href="http://cambodian.china.com/index.html" class="langKh"><em>cambodian</em></a>
                <a href="http://thai.china.com/index.html" class="langTh"><em>thai</em></a>
                <a href="http://indonesian.china.com/index.html" class="langId"><em>indonesian</em></a>
                <a href="http://filipino.china.com/index.html" class="langPh"><em>filipino</em></a>
                <a href="http://myanmar.china.com/index.html" class="langMm"><em>myanmar</em></a>
                <a href="http://japanese.china.com/index.html" class="langJp"><em>japanese</em></a>
                <a href="http://korean.china.com/index.html" class="langKr"><em>korean</em></a>
                <a href="http://mongol.china.com/index.html" class="langMn"><em>mongol</em></a>
                <a href="http://nepal.china.com/index.html" class="langNp"><em>nepal</em></a>
                <a href="http://hindi.china.com/index.html" class="langIn"><em>hindi</em></a>
                <a href="http://bengali.china.com/index.html" class="langMd"><em>bengali</em></a>
                <a href="http://turkish.china.com/index.html" class="langTr"><em>turkish</em></a>
                <a href="http://persian.china.com/index.html" class="langIr"><em>persian</em></a>
                <a href="http://arabic.china.com/index.html" class="langAe"><em>arabic</em></a>
              </div>
            </div>
            <div class="page-search">
              <form id="web-search" name="web-search" method="get" action="http://www.google.com/search">
                <input type="hidden" name="sitesearch" value="english.china.com" />
                <input type="text" name="q" class="web-search-keyword" placeholder="Search here..." />
                <input type="submit" name="button" class="web-search-but" value="Search Site" />
              </form>
            </div>
      </div>
    </div>
  </div>
  <div class="page-nav-bg" id="page-nav">
    <ul class="page-nav maxWidth">
      <li class="page-openSearch"><a href="#"><em>Open search</em></a></li>
      <li class="small-logo"><a href="/index.html"><img src="/img/small-logo.png" /></a></li>
      <li><a href="/news/index.html">News</a></li>
      <li><a href="/video/index.html">Video</a></li>
      <!--<li><a href="/audio/index.html">Audio</a></li>-->
      <!--<li><a href="/photos/index.html">Photos</a></li>-->
      <li><a href="/travel/index.html">Travel</a></li>
      <!--<li><a href="/lifestyle/index.html">Lifestyle</a></li>-->
      <li><a href="/chinese/index.html">Learn Chinese</a></li>
	  <li><a href="http://english.china.com/cityguide/">City Guide</a></li>
      <li><a href="http://mail.china.com/en/" class="ext" target="_blank">Free Mail</a></li>
    </ul>
  </div>
</div><!-- page-head End -->
<!-- /etc/channelhomehead.shtml End -->

<div class="maxWidth">
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µè¶éå¹¿å1 #17286  Begin -->
<script type="text/javascript" src="http://dvser.china.com/s?z=china&c=206" charset="gbk" ></script>
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µè¶éå¹¿å1 End -->
</div>

<div class="page-main maxWidth">
  <div class="page-left">
    <div class="page-focus" id="page-focus">
      <div class="page-focus-body" id="page-focus-body">
        <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://news.xinhuanet.com/english/photo/2015-12/08/c_134895706.htm" title="Gorgeous Rime Scenery in China's Jilin"><img src="http://img04.mini.abroad.imgcdc.com/english/home/topphoto/1295/20151208/522973_136891.jpg.680x330.jpg" width="680" height="330" alt="Gorgeous Rime Scenery in China's Jilin" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://news.xinhuanet.com/english/photo/2015-12/08/c_134895706.htm" title="Gorgeous Rime Scenery in China's Jilin" class="title_default">Gorgeous Rime Scenery in China's Jilin</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12394/2015/12/08/3561s907405.htm" title="Breathtaking Sea of Clouds at Three Gorges Dam in Hubei"><img src="http://img04.mini.abroad.imgcdc.com/english/home/topphoto/1295/20151208/522965_136887.jpg.680x330.jpg" width="680" height="330" alt="Breathtaking Sea of Clouds at Three Gorges Dam in Hubei" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12394/2015/12/08/3561s907405.htm" title="Breathtaking Sea of Clouds at Three Gorges Dam in Hubei" class="title_default">Breathtaking Sea of Clouds at Three Gorges Dam in Hubei</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://news.xinhuanet.com/english/photo/2015-12/07/c_134891618.htm" title="Snow Scenery of West Lake in Hangzhou"><img src="http://img03.mini.abroad.imgcdc.com/english/home/topphoto/1295/20151207/522279_136678.jpg.680x330.jpg" width="680" height="330" alt="Snow Scenery of West Lake in Hangzhou" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://news.xinhuanet.com/english/photo/2015-12/07/c_134891618.htm" title="Snow Scenery of West Lake in Hangzhou" class="title_default">Snow Scenery of West Lake in Hangzhou</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12394/2015/12/07/4061s907308.htm" title="Heavy Pollution Returns to Beijing"><img src="http://img01.mini.abroad.imgcdc.com/english/home/topphoto/1295/20151207/522276_136676.jpg.680x330.jpg" width="680" height="330" alt="Heavy Pollution Returns to Beijing" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12394/2015/12/07/4061s907308.htm" title="Heavy Pollution Returns to Beijing" class="title_default">Heavy Pollution Returns to Beijing</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div><div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://news.xinhuanet.com/english/photo/2015-12/06/c_134888832.htm" title="Feeling festive: Christmas Pudding Race in London"><img src="http://img02.mini.abroad.imgcdc.com/english/news/topphotos/world/1208/20151206/521427_136441.jpg.680x330.jpg" width="565" height="250" alt="Feeling festive: Christmas Pudding Race in London" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://news.xinhuanet.com/english/photo/2015-12/06/c_134888832.htm" title="Feeling festive: Christmas Pudding Race in London" class="title_default">Feeling festive: Christmas Pudding Race in London</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div>
      </div>
      <div id="page-focus-console"></div>
      <div class="page-focus-prevnext">
        <a href="#" id="page-focus-prev">Previous focus</a>
        <a href="#" id="page-focus-next">Next focus</a>
      </div>
    </div><!-- page-focus End -->
    
    <div class="page-latest">
      <h2 class="modTit"><strong><a href="/news/index.html">LATEST NEWS</a></strong></h2>
      <div class="page-latest-body" id="page-latest">
        <div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/12/08/3941s907447.htm" title="76ers Turn in Worst Peformance of Year"><img src="http://img03.mini.abroad.imgcdc.com/english/news/sports/57/20151208/523079_136934.jpg.200x120.jpg" width="200" height="120" alt="76ers Turn in Worst Peformance of Year" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2015/12/08 19:55:45</em><em class="hide">December 09 2015 02:42:40</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/12/08/3941s907447.htm" title="76ers Turn in Worst Peformance of Year" class="title_default">76ers Turn in Worst Peformance of Year</a></h3>
            <p class="item-infor" title="The Philadelphia 76ers should consider themselves lucky the Golden State Warriors are all the talk of the NBA this season. If not, odds are things might well be different surrounding that team at this point.
">The Philadelphia 76ers should consider themselves lucky the Golden State Warriors are all the talk of the NBA this season. If not, odds are things might well be different surrounding that team at this point.
</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/12/08/3941s907444.htm" title="Super Dan Withdraws from World Super Series Finals in Dubai"><img src="http://img02.mini.abroad.imgcdc.com/english/news/sports/57/20151208/523070_136929.jpg.200x120.jpg" width="200" height="120" alt="Super Dan Withdraws from World Super Series Finals in Dubai" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2015/12/08 19:44:04</em><em class="hide">December 09 2015 02:42:40</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/12/08/3941s907444.htm" title="Super Dan Withdraws from World Super Series Finals in Dubai" class="title_default">Super Dan Withdraws from World Super Series Finals in Dubai</a></h3>
            <p class="item-infor" title="A shock announcement ahead of the World Super Series Finals in Dubai set to get underway Wendesday.">A shock announcement ahead of the World Super Series Finals in Dubai set to get underway Wendesday.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/12/08/3941s907450.htm" title="Field Hockey Player Named Youth Olympics Ambassador"><img src="http://img04.mini.abroad.imgcdc.com/english/news/sports/57/20151208/523068_136927.jpg.200x120.jpg" width="200" height="120" alt="Field Hockey Player Named Youth Olympics Ambassador" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2015/12/08 19:43:17</em><em class="hide">December 09 2015 02:42:40</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/12/08/3941s907450.htm" title="Field Hockey Player Named Youth Olympics Ambassador" class="title_default">Field Hockey Player Named Youth Olympics Ambassador</a></h3>
            <p class="item-infor" title="Argentinian legend Luciana Aymar has been named as an Ambassador for the Youth Olympic Games in Buenos Aires 2018.">Argentinian legend Luciana Aymar has been named as an Ambassador for the Youth Olympic Games in Buenos Aires 2018.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/travel/2015-12/08/content_22658729.htm" title="Chinese Tourists in Australia Outspend Brits, Americans and Canadians Combined"><img src="http://img01.mini.abroad.imgcdc.com/english/news/business/56/20151208/522983_136896.jpg.200x120.jpg" width="200" height="120" alt="Chinese Tourists in Australia Outspend Brits, Americans and Canadians Combined" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2015/12/08 18:18:15</em><em class="hide">December 09 2015 02:42:40</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/travel/2015-12/08/content_22658729.htm" title="Chinese Tourists in Australia Outspend Brits, Americans and Canadians Combined" class="title_default">Chinese Tourists in Australia Outspend Brits, Americans and Canadians Combined</a></h3>
            <p class="item-infor" title="Chinese tourists in the past year reportedly spent A$7.7 billion ($5.6 billion) in Australia, an increase of more than three times the rate of overall spending by visitors. ">Chinese tourists in the past year reportedly spent A$7.7 billion ($5.6 billion) in Australia, an increase of more than three times the rate of overall spending by visitors. </p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/china/2015-12/08/content_22661080.htm" title="Things You May Want to Know during Beijing's Red Alert"><img src="http://img04.mini.abroad.imgcdc.com/english/news/china/54/20151208/522982_136895.jpg.200x120.jpg" width="200" height="120" alt="Things You May Want to Know during Beijing's Red Alert" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/12/08 18:15:21</em><em class="hide">December 09 2015 02:42:40</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/china/2015-12/08/content_22661080.htm" title="Things You May Want to Know during Beijing's Red Alert" class="title_default">Things You May Want to Know during Beijing's Red Alert</a></h3>
            <p class="item-infor" title="Beijing's environmental watchdog issued its highest-level air pollution alert for the first time at 6pm on Monday with the capital facing a second wave of smog this winter.">Beijing's environmental watchdog issued its highest-level air pollution alert for the first time at 6pm on Monday with the capital facing a second wave of smog this winter.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/12/08/3821s907424.htm" title="Mt. Qomolangma Glaciers Shrink 28 Pct in 40 Years: Report"><img src="http://img02.mini.abroad.imgcdc.com/english/news/china/54/20151208/522979_136893.jpg.200x120.jpg" width="200" height="120" alt="Mt. Qomolangma Glaciers Shrink 28 Pct in 40 Years: Report" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/12/08 18:13:45</em><em class="hide">December 09 2015 02:42:40</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/12/08/3821s907424.htm" title="Mt. Qomolangma Glaciers Shrink 28 Pct in 40 Years: Report" class="title_default">Mt. Qomolangma Glaciers Shrink 28 Pct in 40 Years: Report</a></h3>
            <p class="item-infor" title="Glaciers on Mount Qomolangma have shrunk by 28 percent over the past 40 years due to climate change, according to a report released over the weekend.">Glaciers on Mount Qomolangma have shrunk by 28 percent over the past 40 years due to climate change, according to a report released over the weekend.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/12/08/3781s907400.htm" title="Michel Platini to Attend CAS Hearing"><img src="http://img02.mini.abroad.imgcdc.com/english/news/sports/57/20151208/522549_136761.jpg.200x120.jpg" width="200" height="120" alt="Michel Platini to Attend CAS Hearing" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2015/12/08 10:47:21</em><em class="hide">December 09 2015 02:42:40</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/12/08/3781s907400.htm" title="Michel Platini to Attend CAS Hearing" class="title_default">Michel Platini to Attend CAS Hearing</a></h3>
            <p class="item-infor" title="UEFA president Michel Platini and FIFA have been invited by the Court of Arbitration for Sports to attend a hearing on Tuesday.">UEFA president Michel Platini and FIFA have been invited by the Court of Arbitration for Sports to attend a hearing on Tuesday.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/12/08/3781s907372.htm" title="Shakespeare's Historical Play to Stage in China in February"><img src="http://img01.mini.abroad.imgcdc.com/english/news/showbiz/58/20151208/522478_136736.jpg.200x120.jpg" width="200" height="120" alt="Shakespeare's Historical Play to Stage in China in February" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/showbiz">Showbiz</a></strong><em>2015/12/08 10:40:06</em><em class="hide">December 09 2015 02:42:40</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/12/08/3781s907372.htm" title="Shakespeare's Historical Play to Stage in China in February" class="title_default">Shakespeare's Historical Play to Stage in China in February</a></h3>
            <p class="item-infor" title="It has been announced that the Royal Shakespeare Company is planning to bring Henry V to Beijing, Shanghai and Hong Kong in February next year.">It has been announced that the Royal Shakespeare Company is planning to bring Henry V to Beijing, Shanghai and Hong Kong in February next year.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.shanghaidaily.com/business/finance/China-sees-forex-reserves-decline-by-US872b/shdaily.shtml" title="China sees forex reserves decline by US$87.2b"><img src="http://img04.mini.abroad.imgcdc.com/english/news/business/56/20151208/522474_136735.jpg.200x120.jpg" width="200" height="120" alt="China sees forex reserves decline by US$87.2b" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2015/12/08 09:20:09</em><em class="hide">December 09 2015 02:42:40</em></div>
            <h3 class="item-tit"><a href="http://www.shanghaidaily.com/business/finance/China-sees-forex-reserves-decline-by-US872b/shdaily.shtml" title="China sees forex reserves decline by US$87.2b" class="title_default">China sees forex reserves decline by US$87.2b</a></h3>
            <p class="item-infor" title="ChinaâS foreign exchange reserves, the worldâs largest, fell by US$87.2 billion in November to US$3.44 trillion, central bank data showed yesterday, the lowest level since February 2013 and the third largest monthly drop on record.">ChinaâS foreign exchange reserves, the worldâs largest, fell by US$87.2 billion in November to US$3.44 trillion, central bank data showed yesterday, the lowest level since February 2013 and the third largest monthly drop on record.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2015-12/08/c_134894302.htm" title="Southern California shooters radicalized "for some time": FBI"><img src="http://img04.mini.abroad.imgcdc.com/english/news/world/55/20151208/522439_136731.jpg.200x120.jpg" width="200" height="120" alt="Southern California shooters radicalized "for some time": FBI" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2015/12/08 09:02:12</em><em class="hide">December 09 2015 02:42:40</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2015-12/08/c_134894302.htm" title="Southern California shooters radicalized "for some time": FBI" class="title_default">Southern California shooters radicalized "for some time": FBI</a></h3>
            <p class="item-infor" title="Investigators believed that the suspect couple who killed 14 people and injured 21 others in a deadly shooting in San Bernardino of Southern California had been radicalized "for some time," the FBI said on Monday.">Investigators believed that the suspect couple who killed 14 people and injured 21 others in a deadly shooting in San Bernardino of Southern California had been radicalized "for some time," the FBI said on Monday.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/china/2015-12/07/content_22652539.htm" title="Beijing issues first red alert for heavy air pollution"><img src="http://img03.mini.abroad.imgcdc.com/english/news/china/54/20151208/522438_136730.jpg.200x120.jpg" width="200" height="120" alt="Beijing issues first red alert for heavy air pollution" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/12/08 08:59:33</em><em class="hide">December 09 2015 02:42:40</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/china/2015-12/07/content_22652539.htm" title="Beijing issues first red alert for heavy air pollution" class="title_default">Beijing issues first red alert for heavy air pollution</a></h3>
            <p class="item-infor" title="Beijing has upgraded its alert for air pollution from orange to red, the most serious level, on Monday afternoon.">Beijing has upgraded its alert for air pollution from orange to red, the most serious level, on Monday afternoon.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/business/2015-12/07/content_22648953.htm" title="Property Giant Vanke Reports Change in Top Shareholder"><img src="http://img02.mini.abroad.imgcdc.com/english/news/business/56/20151207/522285_136681.jpg.200x120.jpg" width="200" height="120" alt="Property Giant Vanke Reports Change in Top Shareholder" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2015/12/07 19:51:01</em><em class="hide">December 09 2015 02:42:40</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/business/2015-12/07/content_22648953.htm" title="Property Giant Vanke Reports Change in Top Shareholder" class="title_default">Property Giant Vanke Reports Change in Top Shareholder</a></h3>
            <p class="item-infor" title="China's property giant Vanke Co saw its biggest shareholder change hands on Sunday, as Shenzhen Jushenghua Co and its related parties replaced China Resources with a combined stake of 20 percent.">China's property giant Vanke Co saw its biggest shareholder change hands on Sunday, as Shenzhen Jushenghua Co and its related parties replaced China Resources with a combined stake of 20 percent.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/12/07/3821s907294.htm" title="DPRK-invested Museum Opens in Cambodia's Siem Reap City"><img src="http://img01.mini.abroad.imgcdc.com/english/news/world/55/20151207/522284_136680.jpg.200x120.jpg" width="200" height="120" alt="DPRK-invested Museum Opens in Cambodia's Siem Reap City" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2015/12/07 19:48:50</em><em class="hide">December 09 2015 02:42:40</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/12/07/3821s907294.htm" title="DPRK-invested Museum Opens in Cambodia's Siem Reap City" class="title_default">DPRK-invested Museum Opens in Cambodia's Siem Reap City</a></h3>
            <p class="item-infor" title="After five years of building, the 24-million-U.S.-dollar Angkor Panorama Museum, invested by the Democratic People's Republic of Korea (DPRK), opened in northwest Cambodia's Siem Reap city, a local English newspaper reported on Monday.">After five years of building, the 24-million-U.S.-dollar Angkor Panorama Museum, invested by the Democratic People's Republic of Korea (DPRK), opened in northwest Cambodia's Siem Reap city, a local English newspaper reported on Monday.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/12/07/3941s907333.htm" title="Robertson Beats China's Liang Wenbo to Win UK Championship"><img src="http://img04.mini.abroad.imgcdc.com/english/news/sports/57/20151207/522275_136675.jpg.200x120.jpg" width="200" height="120" alt="Robertson Beats China's Liang Wenbo to Win UK Championship" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2015/12/07 19:43:03</em><em class="hide">December 09 2015 02:42:40</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/12/07/3941s907333.htm" title="Robertson Beats China's Liang Wenbo to Win UK Championship" class="title_default">Robertson Beats China's Liang Wenbo to Win UK Championship</a></h3>
            <p class="item-infor" title="Australia's Neil Robertson downed China's Liang Wenbo 10-5 to win the UK Championship for the second time, making the first ever 147 in a major tournament final. ">Australia's Neil Robertson downed China's Liang Wenbo 10-5 to win the UK Championship for the second time, making the first ever 147 in a major tournament final. </p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/12/07/3941s907327.htm" title="Golden State Warriors Beat Brooklyn Nets for 22th Win"><img src="http://img03.mini.abroad.imgcdc.com/english/news/sports/57/20151207/522272_136674.jpg.200x120.jpg" width="200" height="120" alt="Golden State Warriors Beat Brooklyn Nets for 22th Win" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2015/12/07 19:42:15</em><em class="hide">December 09 2015 02:42:40</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/12/07/3941s907327.htm" title="Golden State Warriors Beat Brooklyn Nets for 22th Win" class="title_default">Golden State Warriors Beat Brooklyn Nets for 22th Win</a></h3>
            <p class="item-infor" title="The Golden State Warriors continue to roll over the competition in their record-breaking start to this year's season.">The Golden State Warriors continue to roll over the competition in their record-breaking start to this year's season.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/china/2015-12/07/content_22643222.htm" title="Bye-bye blue: More smog settling in"><img src="http://img02.mini.abroad.imgcdc.com/english/news/china/54/20151207/521735_136509.jpg.200x120.jpg" width="200" height="120" alt="Bye-bye blue: More smog settling in" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/china">China</a></strong><em>2015/12/07 10:41:21</em><em class="hide">December 09 2015 02:42:40</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/china/2015-12/07/content_22643222.htm" title="Bye-bye blue: More smog settling in" class="title_default">Bye-bye blue: More smog settling in</a></h3>
            <p class="item-infor" title="With a new round of smog likely to engulf Beijing and surrounding areas over the next three days, Beijing issued an orange alertï¼the second-highest warning levelï¼on Saturday afternoon, 31 hours ahead of the expected gloom.">With a new round of smog likely to engulf Beijing and surrounding areas over the next three days, Beijing issued an orange alertï¼the second-highest warning levelï¼on Saturday afternoon, 31 hours ahead of the expected gloom.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://news.xinhuanet.com/english/2015-12/07/c_134891670.htm#" title="Obama calls Southern California carnage act of terrorism"><img src="http://img02.mini.abroad.imgcdc.com/english/news/world/55/20151207/521729_136505.jpg.200x120.jpg" width="200" height="120" alt="Obama calls Southern California carnage act of terrorism" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/world">World</a></strong><em>2015/12/07 10:38:02</em><em class="hide">December 09 2015 02:42:40</em></div>
            <h3 class="item-tit"><a href="http://news.xinhuanet.com/english/2015-12/07/c_134891670.htm#" title="Obama calls Southern California carnage act of terrorism" class="title_default">Obama calls Southern California carnage act of terrorism</a></h3>
            <p class="item-infor" title="U.S. President Barack Obama said Sunday the recent shooting rampage in southern California was an act of terrorism.">U.S. President Barack Obama said Sunday the recent shooting rampage in southern California was an act of terrorism.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.chinadaily.com.cn/business/motoring/2015-12/07/content_22643336.htm" title="China to become world's No 1 electric car market"><img src="http://img03.mini.abroad.imgcdc.com/english/news/business/56/20151207/521698_136498.jpg.200x120.jpg" width="200" height="120" alt="China to become world's No 1 electric car market" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/business">Business</a></strong><em>2015/12/07 10:09:58</em><em class="hide">December 09 2015 02:42:40</em></div>
            <h3 class="item-tit"><a href="http://www.chinadaily.com.cn/business/motoring/2015-12/07/content_22643336.htm" title="China to become world's No 1 electric car market" class="title_default">China to become world's No 1 electric car market</a></h3>
            <p class="item-infor" title="Electric car sales in China are expected to reach 220,000 to 250,000 this year, surpassing the US to rank first worldwide, China Association of Automobile Manufactures (CAAM) forecast.">Electric car sales in China are expected to reach 220,000 to 250,000 this year, surpassing the US to rank first worldwide, China Association of Automobile Manufactures (CAAM) forecast.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://www.shanghaidaily.com/sports/golf/Holman-wins-3way-playoff/shdaily.shtml" title="Holman wins 3-way playoff"><img src="http://img01.mini.abroad.imgcdc.com/english/news/sports/57/20151207/521685_136496.jpg.200x120.jpg" width="200" height="120" alt="Holman wins 3-way playoff" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/sports">Sports</a></strong><em>2015/12/07 10:08:40</em><em class="hide">December 09 2015 02:42:40</em></div>
            <h3 class="item-tit"><a href="http://www.shanghaidaily.com/sports/golf/Holman-wins-3way-playoff/shdaily.shtml" title="Holman wins 3-way playoff" class="title_default">Holman wins 3-way playoff</a></h3>
            <p class="item-infor" title="Nathan Holman rebounded after making bogeys on the last two holes in regulation to win the Australian PGA Championship after a three-way playoff.">Nathan Holman rebounded after making bogeys on the last two holes in regulation to win the Australian PGA Championship after a three-way playoff.</p>
          </div>
        </div><div class="item-phototext ">
          <div class="item-photo"><a href="http://english.cri.cn/12394/2015/12/07/3781s907257.htm" title="Harry Potter Magic World Tour Arrives in China for the First Time"><img src="http://img01.mini.abroad.imgcdc.com/english/news/showbiz/58/20151207/521672_136488.jpg.200x120.jpg" width="200" height="120" alt="Harry Potter Magic World Tour Arrives in China for the First Time" /></a></div>
          <div class="item-text">
            <div class="item-type"><strong><a href="http://english.china.com/news/showbiz">Showbiz</a></strong><em>2015/12/07 09:50:07</em><em class="hide">December 09 2015 02:42:40</em></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/12394/2015/12/07/3781s907257.htm" title="Harry Potter Magic World Tour Arrives in China for the First Time" class="title_default">Harry Potter Magic World Tour Arrives in China for the First Time</a></h3>
            <p class="item-infor" title="The Harry Potter Magic World Tour has arrived in China for the first time. The Harry Potter experience opened in Shanghai's Super Brand Mall over the weekend, with the Weasley brothers' actors James and Oliver Phelps given the honour of waving the wand to officially open the show.">The Harry Potter Magic World Tour has arrived in China for the first time. The Harry Potter experience opened in Shanghai's Super Brand Mall over the weekend, with the Weasley brothers' actors James and Oliver Phelps given the honour of waving the wand to officially open the show.</p>
          </div>
        </div>
      </div>
      <div class="page-latest-more">
        <a href="#" id="page-latest-show-more"><em class="page-latest-more-icon">&nbsp;</em></a>
        <a href="/news/index.html" id="page-latest-click-more"><em class="page-latest-more-icon">Show More</em></a>
      </div>
    </div><!-- page-latest End -->
    <div class="page-left-ad">
	<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µéæ å¹¿å1 #17286 Begin -->
<script type="text/javascript" src="http://dvser.china.com/s?z=china&c=207" charset="gbk" ></script>
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µéæ å¹¿å1 End -->
	</div> 
    <div class="page-video">
      <h2 class="modTit"><strong><a href="/video/index.html">VIDEO</a></strong></h2>
      <div class="page-video-body">
        <div class="page-video-left">
          <script type="text/javascript" src="http://c.wrating.com/v2_pre.js"></script>
<!--noscript-->
          <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="420" height="234">
            <param name="movie" value="http://english.china.com/videoPlayer/video.swf"/>
            <param name="quality" value="high"/>
            <param name="bgcolor" value="#ffffff"/>
            <param name="allowScriptAccess" value="sameDomain"/>
            <param name="allowFullScreen" value="true"/>
            <param name="wmode" value="Opaque">
            <param name="flashvars" value="sw=420&sh=234&url=http://mod.cri.cn/eng/video/chinarevealed/2015/11/1130todayinhistory.mp4&loop=1&autoplay=0"/>
            <!--[if!IE]>
            -->
            <object type="application/x-shockwave-flash" data="http://english.china.com/videoPlayer/video.swf" width="420" height="234">
              <param name="quality" value="high"/>
              <param name="bgcolor" value="#ffffff"/>
              <param name="allowScriptAccess" value="sameDomain"/>
              <param name="allowFullScreen" value="true"/>
              <param name="wmode" value="Opaque">
              <param name="flashvars" value="sw=420&sh=234&url=http://mod.cri.cn/eng/video/chinarevealed/2015/11/1130todayinhistory.mp4&loop=1&autoplay=0"/>
              <!--<![endif]-->
              <!--[if gte IE 6]>
              -->
              <p>
                Either scripts and active content are not permitted to run or Adobe Flash Player version 11.4.0 or greater is not installed.
              </p>
              <!--<![endif]-->
              <a href="http://www.adobe.com/go/getflashplayer">
                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash Player"/>
              </a>
              <!--[if!IE]>--></object>
            <!--<![endif]-->
          </object>
          <!--/noscript-->
          <a href="/home/videobig/1299/20151130/516953.html" class="video-tit">Today in History: The War against Japanese Aggression, 1941.11.30</a>
        </div>
        <div class="page-video-right" id="page-video-right">
          <div class="item-video">
            <div class="item-photo">
              <a href="http://english.china.com/video/life/2649/20151202/518653.html"><img src="http://img04.abroad.imgcdc.com/english/home/videosmall/1301/20151202/518679_135675.jpg" width="245" height="125" alt="My Chinese Life: Ben Giaimo--A Foreign Disciple of Confucius" /></a>
            </div>
            <a href="http://english.china.com/video/life/2649/20151202/518653.html" class="item-text">
              <h3 class="item-tit"><strong class="title_default">My Chinese Life: Ben Giaimo--A Foreign Disciple of Confucius</strong></h3>
              <p class="item-infor">American student, Ben, is trying to sell Confucian wisdom to the west by using American slang.</p>
              <div class="item-type">
                <span class="item-icon"></span>
                <span class="item-watch">watch</span>
              </div>
            </a>
          </div><div class="item-video">
            <div class="item-photo">
              <a href="http://english.china.com/video/life/2649/20151028/491957.html"><img src="http://img04.abroad.imgcdc.com/english/home/videosmall/1301/20151028/491968_127479.jpg" width="245" height="125" alt="Along the Silk Road:Dev Raturi--Indian Ingenuity" /></a>
            </div>
            <a href="http://english.china.com/video/life/2649/20151028/491957.html" class="item-text">
              <h3 class="item-tit"><strong class="title_default">Along the Silk Road:Dev Raturi--Indian Ingenuity</strong></h3>
              <p class="item-infor">Dev brings India's unique culture to China, showcasing its food, and rediscovering Silk Road ties.</p>
              <div class="item-type">
                <span class="item-icon"></span>
                <span class="item-watch">watch</span>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div><!-- page-video End -->
    <div class="page-mods">
      <div class="page-mod-item">
        <h2 class="modTit"><strong><a href="/travel/index.html">TRAVEL</a></strong></h2>
        <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://www.timeoutbeijing.com/features/Food__Drink-Food_awards_2015/38233/Food-Awards-2015-the-winners.html" title="Food Awards 2015: the winners"><img src="http://img03.mini.abroad.imgcdc.com/english/travel/listright/mostpopular/1534/20150506/366181_88898.jpg.330x190.jpg" width="330" height="190" alt="Food Awards 2015: the winners" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://www.timeoutbeijing.com/features/Food__Drink-Food_awards_2015/38233/Food-Awards-2015-the-winners.html" title="Food Awards 2015: the winners" class="title_default">Food Awards 2015: the winners</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div>
        <div class="newslist">
          <ul>
            <li><a href="http://english.cri.cn/12394/2015/08/11/2743s891245.htm" title="Japanese War Orphan Recounts Past" class="title_default">Japanese War Orphan Recounts Past</a></li><li><a href="http://english.cri.cn/6566/2014/12/25/44s858261.htm" title="Winter Nadam Kicks Off in North China" class="title_default">Winter Nadam Kicks Off in North China</a></li><li><a href="http://english.cri.cn/6566/2014/09/29/44s845986.htm" title="To Experience Authentic Taiwan Folk Art in Beijing" class="title_default">To Experience Authentic Taiwan Folk Art in Beijing</a></li>
          </ul>
        </div>

      </div>
      <div class="page-mod-item">
        <h2 class="modTit"><strong><a href="/chinese/index.html">LEARN CHINESE</a></strong></h2>
        <div class="focusTopic">
          <div class="focusTopic_pic"><a href="http://english.cri.cn/12514/2014/11/24/2001s853655.htm" title="China's First Love"><img src="http://img03.mini.abroad.imgcdc.com/english/home/learnpic/1315/20141124/211729_51886.jpg.330x190.jpg" width="330" height="190" alt="China's First Love" /></a></div>
          <div class="focusTopic_cont">
            <div class="focusTopic_tit"><a href="http://english.cri.cn/12514/2014/11/24/2001s853655.htm" title="China's First Love" class="title_default">China's First Love</a></div>
            <div class="focusTopic_txt"><p></p></div>
          </div>
        </div>
        <div class="newslist">
          <ul>
            <li><a href="http://english.cri.cn/12514/2014/10/16/2001s848110.htm" title="Top 10 Popular Chinese TV Dramas Overseas" class="title_default">Top 10 Popular Chinese TV Dramas Overseas</a></li><li><a href="http://english.cri.cn/12514/2014/10/17/2001s848240.htm" title="çµç¶ Chinese Pipa" class="title_default">çµç¶ Chinese Pipa</a></li><li><a href="http://english.cri.cn/12514/2014/09/25/2001s845407.htm" title="Useful Shopping Sentences in Chinese" class="title_default">Useful Shopping Sentences in Chinese</a></li>
          </ul>
        </div>

      </div>
      
      
    </div><!-- page-mods End -->
    <!--<div class="page-left-ad"><a href="#"><img src="/file/left-ad-2.jpg" /></a></div> page-left-ad End -->
    <!-- page-photos End -->
  </div><!-- page-left End -->
  <div class="page-right">
    
    <div class="page-right-ad noMarginTop">
      <!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µç»ä¸­ç»1 #17286 Begin -->
<script type="text/javascript" src="http://dvser.china.com/s?z=china&c=208" charset="gbk" ></script>
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µç»ä¸­ç»1 End -->
    </div><!-- page-right-ad End -->
    <!--include virtual="/etc/right_top_ad_index.shtml" -->
      
    ï»¿<!-- /home/imgtj/index.html CMSID:5828 Start -->
<div class="page-right-ad">
	<a href="http://english.cri.cn/12394/2015/09/21/Zt2821s896890.htm"><img src="http://img04.abroad.imgcdc.com/english/home/imgtj/5829/20151014/482283_124639.jpg" width="293" height="88" alt="20151014" /></a><a href="http://english.cri.cn/12394/2015/09/02/Zt2821s894283.htm"><img src="http://img01.abroad.imgcdc.com/english/home/imgtj/5829/20150908/482287_124640.jpg" width="293" height="88" alt="20150908" /></a>
</div>
<!-- /home/imgtj/index.html CMSID:5828 End --><!-- #15734 -->

    <!-- page-right-ad End -->
    
    <div class="page-watched">
      <h2 class="modTit"><strong>Most Watched</strong></h2>
      <div class="page-watched-body" id="rank-video">
      </div>
    </div><!-- page-watched End -->
    <div class="page-popular">
      <h2 class="modTit"><strong>Most Popular</strong></h2>
      <ul class="page-popular-body" id="rank-list"></ul>
    </div><!-- page-popular End -->
    <div class="page-right-ad">
	<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µæé®å¹¿å1 #17286 Begin -->
<script type="text/javascript" src="http://dvser.china.com/s?z=china&c=209" charset="gbk" ></script>
<!-- AdSame ShowCode: è±è¯­ç½ç« / é¦é¡µ / è±æç«é¦é¡µæé®å¹¿å1 End -->
	</div> 
    <div class="page-tochina">
      <h2 class="modTit"><strong>Tune in to China</strong></h2>
      <div class="page-tochina-body">
        <div class="item radio-news">
          <div class="item-title"></div>
          <div class="item-body">
            <div class="item-infor">
              <h3 id="title">-</h3>
              <h4 id="playtime">-</h4>
              <a href="http://english.cri.cn/7146/2012/12/03/301s736372.htm" class="full"></a>
            </div>
            <a href="mms://livexwb.cri.com.cn/am846.wsx" class="item-play"></a>
          </div>
        </div>
        <div class="item radio-ez">
          <div class="item-title"></div>
          <div class="item-body">
            <div class="item-infor">
              <h3 id="nexttitle">-</h3>
              <h4 id="nexttime">-</h4>
              <a href="http://english.cri.cn/easyfm/ezplaytime.html" class="full"></a>
            </div>
            <a href="mms://livexwb.cri.com.cn/fm915.wsx" class="item-play"></a>
          </div>
        </div>
      </div>
    </div><!-- page-tochina End -->

    
    <div class="page-hotListening">
      <h2 class="page-hotListening-tit"><strong>Hot Listening</strong></h2>
      <div class="page-hotListening-body">
        <div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/cribb/plus/pik.htm"><img src="http://img03.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4933_1370.jpg" width="120" height="90" alt="People in the Know" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/cribb/plus/pik.htm" class="title_default">People in the Know</a><a href="http://english.cri.cn/cribb/plus/pik.htm" class="icon-horn"></a></h3>
            <p class="item-infor">PIK gives you insights to the world through interviews.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/cribb/plus/today.htm"><img src="http://img02.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4931_1369.jpg" width="120" height="90" alt="Today" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/cribb/plus/today.htm" class="title_default">Today</a><a href="http://english.cri.cn/cribb/plus/today.htm" class="icon-horn"></a></h3>
            <p class="item-infor">A news magazine show with in-depth panel discussions.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/easyfm/easymorning.html"><img src="http://img01.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4928_1368.jpg" width="120" height="90" alt="EZ Morning" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/easyfm/easymorning.html" class="title_default">EZ Morning</a><a href="http://english.cri.cn/easyfm/easymorning.html" class="icon-horn"></a></h3>
            <p class="item-infor">It lights up your mornings with interesting chit-chats.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/easyfm/ezwheel.html"><img src="http://img04.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4927_1367.jpg" width="120" height="90" alt="More to Learn" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/easyfm/ezwheel.html" class="title_default">More to Learn</a><a href="http://english.cri.cn/easyfm/ezwheel.html" class="icon-horn"></a></h3>
            <p class="item-infor">More to Learn is filled up with English stories and anecdotes.</p>
          </div>
        </div><div class="item">
          <div class="item-photo">
            <a href="http://english.cri.cn/easyfm/hour.html"><img src="http://img03.abroad.imgcdc.com/english/audio/right/hot/1691/20131224/4923_1366.jpg" width="120" height="90" alt="The Beijing Hour" /></a>
          </div>
          <div class="item-text">
            <div class="item-type"><a href="http://english.china.com/audio/right/hot">Hot Listening</a></div>
            <h3 class="item-tit"><a href="http://english.cri.cn/easyfm/hour.html" class="title_default">The Beijing Hour</a><a href="http://english.cri.cn/easyfm/hour.html" class="icon-horn"></a></h3>
            <p class="item-infor">It opens up the world to you with latest news updates.</p>
          </div>
        </div>
      </div>
    </div><!-- page-hotListening End -->

    <div class="page-mobile">
      <h2 class="modTit"><strong>Mobile</strong></h2>
      <div class="page-mobile-body">
        <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=539404062&mt=8&s=143441" class="iphone" title="Mobile Iphone">Iphone</a>
      </div>
    </div><!-- page-mobile End -->

    <div class="page-cooperation">
      <h2 class="modTit"><strong>Cooperation</strong></h2>
      <div class="page-cooperation-body">
        <a href="http://gc.wrating.com/click.php?a=&c=860099-1000099998&cs=341_285_2559_860010_400000000&ds=354_355_356_357_358&url=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps%2Fdetails%3Fid%3Dmobi.mgeek.TunnyBrowser%26referrer%3Dchannel_id%253Dchinacom%2526utm_source%253Dchinacom"><img src="/file/logo-dolphin-140-90.png" height="90" width="140"></a>
      </div>
    </div><!-- page-cooperation End -->

  </div><!-- page-right End -->
</div><!-- page-main End -->

<script>
window.collectMethod_rank = window.collectMethod_rank || [];
collectMethod_rank.push(function () {
  setRank("rank-video", 3, "http://english.china.com/js/english_43_day.js", "video", function(){
    setRank("rank-list", 5, "http://english.china.com/js/english_22_day.js", "list");
  });
});
</script>

<!-- /etc/channelsitemap.shtml Start -->
<div class="page-map">
  <div class="page-map-body maxWidth">
  <dl class="item">
      <dt><a href="http://english.china.com/news/index.html">News:</a></dt>
      <dd>
        <a href="http://english.china.com/news/china/index.html">China</a>
        <a href="http://english.china.com/news/world/index.html">World</a>
        <a href="http://english.china.com/news/business/index.html">Business</a>
        <a href="http://english.china.com/news/sports/index.html">Sports</a>
        <a href="http://english.china.com/news/showbiz/index.html">Showbiz</a>
		<a href="http://english.china.com/audio/index.html">Audio</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/video/index.html">Video:</a></dt>
      <dd>
        <a href="http://english.china.com/video/c4/index.html">C4</a>
        <a href="http://english.china.com/video/life/index.html">My Chinese Life</a>
        <a href="http://english.china.com/video/thesoundstage/index.html">The Sound Stage</a>
        <a href="http://english.china.com/video/chinarevealed/index.html">China Revealed</a>
        <a href="http://english.china.com/video/showbiz/index.html">Showbiz Video</a>
        <a href="http://english.china.com/video/tour/index.html">Travel Video</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/photos/index.html">Photos:</a></dt>
      <dd>
        <a href="http://english.china.com/photos/china/index.html">China</a>
        <a href="http://english.china.com/photos/world/index.html">World</a>
        <a href="http://english.china.com/photos/fun/index.html">Fun</a>
        <a href="http://english.china.com/photos/travel/index.html">Travel</a>
        <a href="http://english.china.com/photos/entertainment/index.html">Entertainment</a>
        <a href="http://english.china.com/photos/sports/index.html">Sports</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/travel/index.html">Travel:</a></dt>
      <dd>
        <a href="http://english.china.com/travel/beijing/index.html">Beijing</a>
        <a href="http://english.china.com/travel/shanghai/index.html">Shanghai</a>
        <a href="http://english.china.com/travel/guangzhou/index.html">Guangzhou</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/lifestyle/index.html">Lifestyle:</a></dt>
      <dd>        
        <a href="http://english.china.com/lifestyle/livemusic/index.html">Live Music</a>
        <a href="http://english.china.com/lifestyle/opera/index.html">Opera & Classical</a>
        <a href="http://english.china.com/lifestyle/movies/index.html">Movies</a>
        <a href="http://english.china.com/lifestyle/traditionalshows/index.html">Traditional Shows</a>
        <a href="http://english.china.com/lifestyle/exhibitions/index.html">Exhibitions</a>
      </dd>
    </dl>
    <dl class="item">
      <dt><a href="http://english.china.com/chinese/index.html">Learn Chinese:</a></dt>
      <dd>
        <a href="http://english.china.com/chinese/studio/index.html">Chinese Studio</a>
        <a href="http://english.china.com/chinese/living/index.html">Living Chinese</a>
        <a href="http://english.china.com/chinese/everyday/index.html">Everyday Chinese</a>
        <a href="http://english.china.com/chinese/justforfun/index.html">Just For Fun</a>
        <a href="http://english.china.com/chinese/culture/index.html">Chinese Culture</a>
        <a href="http://english.china.com/chinese/buzzwords/index.html">Buzzwords</a>        
      </dd>
    </dl>      
  </div>
</div><!-- page-map End -->
<!-- /etc/channelsitemap.shtml End -->

<div class="page-link">
  <div class="page-link-body maxWidth">
    <a href="http://english.cri.cn/">CRIENGLISH.com</a>|<a href="http://www.chinadaily.com.cn/">China Daily</a>|<a href="http://www.xinhuanet.com/english/">Xinhua</a>|<a href="http://www.china.org.cn/index.htm">China.org.cn</a>|<a href="http://english.cntv.cn/">CNTV</a>
  </div>
</div><!-- page-link End -->

<!-- /etc/channelcopyright.shtml Start -->
<div class="page-footer">
  <div class="page-foot-link">
    <a href="/about/">About China.com</a>|<a href="/about/gmg.html">About GMG</a>|<a href="/ad/">Ad Services</a>|<a href="/contact/">Contact Information</a>|<a href="/copyright/">Copyright Notice</a>
  </div>
  <p><!--E-mail to:<a href="mailto:english@bj.china.com">english@bj.china.com</a><br />-->Copyright &copy; China.com All Rights Reserved</p>
</div><!-- page-footer End -->

<script src="/js/require.min.js" data-main="/js/main"></script>

<!-- START WRating v1.0 -->
<script type="text/javascript" src="http://c.wrating.com/a1.js">
</script>
<script type="text/javascript">
var vjAcc="860010-0446010000";
var wrUrl="http://c.wrating.com/";
vjTrack("");
</script>
<noscript><img src="http://c.wrating.com/a.gif?a=&c=860010-0446010000" width="1" 
height="1"/></noscript>
<!-- END WRating v1.0 -->

<!-- Start Alexa Certify Javascript #13481-->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"S6Upi1awA+00a/", domain:"china.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=S6Upi1awA+00a/" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript #13481-->


<!-- Start Google Analytics #16010-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60581848-2', 'auto');
  ga('send', 'pageview');

</script>
<!-- End Google Analytics #16010-->

<!-- /etc/channelcopyright.shtml End -->
</body>
</html>