



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

    
    
    

    
    
    

    <meta name="apple-itunes-app" content="app-id=342792525, app-argument=imdb:///?src=mdot">
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "985-2107998-2260488";
                var ue_id = "0H7CDFE1X45ZBVNDDCNP";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        
        <meta property="pageType" content="home" />
        <meta property="subpageType" content="main" />


        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />

    <meta property='fb:app_id' content='115109575169727' />

    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="0H7CDFE1X45ZBVNDDCNP" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1e-c3-2xl-i-a897e816.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-2467623394._CB300617431_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-3693178366._CB298945358_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-2454701167._CB293329573_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-2508181169._CB288096406_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-3988610918._CB289934424_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
(function(){var d=function(o){return Object.prototype.toString.call(o)==="[object Array]";},g=function(q,p){var o;for(o=0;o<q.length;o++){if(o in q){p.call(null,q[o],o);}}},h=[],k,b,l=false,n=false,f=function(){var o=[],p=[],q={};g(h,function(s){var r="";g(s.dartsite.split("/"),function(t){if(t!==""){if(t in q){}else{q[t]=o.length;o.push(t);}r+="/"+q[t];}});p.push(r);});return{iu_parts:o,enc_prev_ius:p};},c=function(){var o=[];g(h,function(q){var p=[];g(q.sizes,function(r){p.push(r.join("x"));});o.push(p.join("|"));});return o;},m=function(){var o=[];g(h,function(p){o.push(a(p.targeting));});return o.join("|");},a=function(r,o){var q,p,s=[];for(q in r){p=[];for(j=0;j<r[q].length;j++){p.push(encodeURIComponent(r[q][j]));}if(o){s.push(q+"="+encodeURIComponent(p.join(",")));}else{s.push(q+"="+p.join(","));}}return s.join("&");},e=function(){var o=+new Date();if(n){return;}if(!this.readyState||"loaded"===this.readyState){n=true;if(l){imdbads.cmd.push(function(){for(i=0;i<h.length;i++){generic.monitoring.record_metric(h[i].name+".fail",csm.duration(o));}});}}};window.tinygpt={define_slot:function(r,q,o,p){h.push({dartsite:r.replace(/\/$/,""),sizes:q,name:o,targeting:p});},set_targeting:function(o){k=o;},callback:function(q){var r,p={},t,o,s=+new Date();l=false;for(r=0;r<h.length;r++){t=h[r].dartsite;o=h[r].name;if(q[r][t]){p[o]=q[r][t];}else{window.console&&console.error&&console.error("Unable to correlate GPT response for "+o);}}imdbads.cmd.push(function(){for(r=0;r<h.length;r++){ad_utils.slot_events.trigger(h[r].name,"request",{timestamp:b});ad_utils.slot_events.trigger(h[r].name,"tagdeliver",{timestamp:s});}ad_utils.gpt.handle_response(p);});},send:function(){var r=[],q=function(s,t){if(d(t)){t=t.join(",");}if(t){r.push(s+"="+encodeURIComponent(""+t));}},o,p;if(h.length===0){tinygpt.callback({});return;}q("gdfp_req","1");q("correlator",Math.floor(4503599627370496*Math.random()));q("output","json_html");q("callback","tinygpt.callback");q("impl","fifs");q("json_a","1");result=f();q("iu_parts",result.iu_parts);q("enc_prev_ius",result.enc_prev_ius);q("prev_iu_szs",c());q("prev_scp",m());q("cust_params",a(k,true));o=document.createElement("script");p=document.getElementsByTagName("script")[0];o.async=true;o.type="text/javascript";o.src="http://pubads.g.doubleclick.net/gampad/ads?"+r.join("&");o.id="tinygpt";o.onload=o.onerror=o.onreadystatechange=e;l=true;p.parentNode.insertBefore(o,p);b=+new Date();}};})();</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[300,600],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['f'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['1'],
'u': ['051678840399'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-tarnhelm-3773997273._CB299167037_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"a0fd3414fc0376bb7c789a5fcd45e02f95de3cb9",
"2016-01-26T18%3A02%3A34GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 50246;
generic.days_to_midnight = 0.5815508961677551;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-3512629095._CB289935695_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'f']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm == null || window.generic == null || window.consoleLog == null)) {
                    if (window.console && console.log) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=051678840399;ord=051678840399?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;ord=051678840399?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;ord=051678840399?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/chart/toptv/?ref_=nv_tp_tv250_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                        <li><a href="/chart/top?ref_=nv_mv_250_6"
>Top Rated Movies</a></li>
                        <li><a href="/chart/moviemeter?ref_=nv_mv_mpm_7"
>Most Popular Movies</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/chart/?ref_=nv_ch_cht_1"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_2"
>Oscar Winners</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_3"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/imdbpicks/tv-releases/?ref_=nv_tvv_picks_1"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_2"
>On Tonight</a></li>
                        <li><a href="/chart/toptv/?ref_=nv_tvv_250_3"
>Top Rated TV Shows</a></li>
                        <li><a href="/chart/tvmeter?ref_=nv_tvv_mptv_4"
>Most Popular TV Shows</a></li>
                        <li><a href="/feature/watch-now-on-amazon/?ref_=nv_tvv_wn_5"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_6"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_2"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/imdbpicks/?ref_=nv_sf_picks_3"
>IMDb Picks</a></li>
                        <li><a href="/best-of/?ref_=nv_sf_bstof_4"
>Best of 2015</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=01-26&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_2"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_3"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_4"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_5"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_6"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_7"
>Emmy Awards</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_8"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_9"
>Toronto Film Festival</a></li>
                        <li><a href="/event/all/?ref_=nv_ev_all_10"
>All Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni59426498/?ref_=nv_nw_tn_1"
> Dakota Johnson Finally Explains That Wardrobe Malfunction at the 2016 People's Choice Awards
</a><br />
                        <span class="time">5 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59424494/?ref_=nv_nw_tn_2"
> Woody Allen to Star in His Amazon Series Opposite...Miley Cyrus?
</a><br />
                        <span class="time">19 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59424840/?ref_=nv_nw_tn_3"
> Matthew Perry Doesnât Remember Filming 3 Seasons of Friends
</a><br />
                        <span class="time">18 hours ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_3"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5>
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0062622/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTI0OTkyODYzNV5BMl5BanBnXkFtZTYwNjUxOTI2._V1._SX410_CR0,52,410,315_.jpg",
            titleYears : "1968",
            rank : 92,
                    headline : "2001: A Space Odyssey"
    },
    nameAd : {
            clickThru : "/name/nm1212722/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BODE3Mzk4NDkyM15BMl5BanBnXkFtZTcwNjczMTg1Ng@@._V1._SX350_CR45,10,250,315_.jpg",
            rank : 97,
            headline : "Benedict Cumberbatch"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYs5QWGwud6vv_5IVLBPP4SzS4TRE5rIWAUzXei8aGH8yfA6PEo07DRapsFtoWxYdSYRi03ulQ8%0D%0AjaZ1X9EezyBtqsyfOIwpbO0y5sr6HynOPvA3q6j-2o-st-fIDuQFIwoSxcDEfWvwL-lenLUepX9d%0D%0A-JMhXXD6bFu1RCAF73VshiWInb3g49ClRWNE7hFZ6M03HU4xJ7lT8tYpF4GxdxtePg%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYniIE7NdhgCThAAqtsN7n-FEeYfCrgNiiRfPLCQemfwGnXTbxWsB3QUX9-A5eAkI37QGVV-yeY%0D%0AI5Uwkj_U1minKNOdH9T2S9KDkclCuH6n84E-bQUWea9cxwFpKq9n3B1puCIhEFzj_Nvzu_-hcCLN%0D%0AYcQai1HzNIhb9GF2UKxUT2fTQeQYUmPpb7-vWLZQ64YN%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYjARJtzOP6L8utf3fCW3lfPZwTM6VcVD2-XFLZYTU7qXEodHWb3QSR0TNEvoAzwUK0X3BYT9xc%0D%0AY5HL9IRlx0cDLgdCw_5X4xfgy8C42P0cdo_JIndmgLtOWFOsAqy3vmbUIic0T5uNUphNDaLqYQGr%0D%0Ak1B74VV9YTrlCdV973PrLDCVfEHw0PycB2emc8xrmRgl%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=051678840399;ord=051678840399?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>

    <div id="pagecontent">
    <div class="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=051678840399;ord=051678840399?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	

    
    
    

    
    
    

    
    
    
    </div>
    <div id="top-slot-wrapper" class="pagecontent">

    
    
    

    
    
    
    </div>
    <div class="pagecontent">
        <div id="content-2-wide">
            <div id="main">

    
    
        <a name="slot_hero"></a>
        <div class="heroWidget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','HeroWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_hero">
<div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1770894617?ref_=hm_hp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396619962&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi1770894617" data-source="bylist" data-id="ls002322762" data-rid="0H7CDFE1X45ZBVNDDCNP" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="The friendly-but-forgetful blue tang fish reunites with her loved ones, and everyone learns a few things about the real meaning of family along the way." alt="The friendly-but-forgetful blue tang fish reunites with her loved ones, and everyone learns a few things about the real meaning of family along the way." src="http://ia.media-imdb.com/images/M/MV5BMTUwMjczMTY3NV5BMl5BanBnXkFtZTgwNjQwMjMyNzE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUwMjczMTY3NV5BMl5BanBnXkFtZTgwNjQwMjMyNzE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="The friendly-but-forgetful blue tang fish reunites with her loved ones, and everyone learns a few things about the real meaning of family along the way." title="The friendly-but-forgetful blue tang fish reunites with her loved ones, and everyone learns a few things about the real meaning of family along the way." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="The friendly-but-forgetful blue tang fish reunites with her loved ones, and everyone learns a few things about the real meaning of family along the way." title="The friendly-but-forgetful blue tang fish reunites with her loved ones, and everyone learns a few things about the real meaning of family along the way." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2277860/?ref_=hm_hp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396619962&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > Finding Dory </a> </div> </div> <div class="secondary ellipsis"> Something Looks Familiar </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2995369241?ref_=hm_hp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396619962&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi2995369241" data-source="bylist" data-id="ls002653141" data-rid="0H7CDFE1X45ZBVNDDCNP" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="Fearing the actions of Superman are left unchecked, Batman takes on Superman, while the world wrestles with what kind of a hero it really needs." alt="Fearing the actions of Superman are left unchecked, Batman takes on Superman, while the world wrestles with what kind of a hero it really needs." src="http://ia.media-imdb.com/images/M/MV5BNDA5NDAzMzg1MF5BMl5BanBnXkFtZTgwOTY2MjU2NzE@._V1_SY298_CR3,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNDA5NDAzMzg1MF5BMl5BanBnXkFtZTgwOTY2MjU2NzE@._V1_SY298_CR3,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Fearing the actions of Superman are left unchecked, Batman takes on Superman, while the world wrestles with what kind of a hero it really needs." title="Fearing the actions of Superman are left unchecked, Batman takes on Superman, while the world wrestles with what kind of a hero it really needs." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Fearing the actions of Superman are left unchecked, Batman takes on Superman, while the world wrestles with what kind of a hero it really needs." title="Fearing the actions of Superman are left unchecked, Batman takes on Superman, while the world wrestles with what kind of a hero it really needs." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2975590/?ref_=hm_hp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396619962&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > Batman v Superman </a> </div> </div> <div class="secondary ellipsis"> TV Promo #3 </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi4287149337?ref_=hm_hp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396619962&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi4287149337" data-source="bylist" data-id="ls002653141" data-rid="0H7CDFE1X45ZBVNDDCNP" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="An epic fantasy/adventure based on the popular video game series." alt="An epic fantasy/adventure based on the popular video game series." src="http://ia.media-imdb.com/images/M/MV5BMTgxMDAzNzMyMV5BMl5BanBnXkFtZTgwNjIwMTgxNzE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTgxMDAzNzMyMV5BMl5BanBnXkFtZTgwNjIwMTgxNzE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="An epic fantasy/adventure based on the popular video game series." title="An epic fantasy/adventure based on the popular video game series." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="An epic fantasy/adventure based on the popular video game series." title="An epic fantasy/adventure based on the popular video game series." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt0803096/?ref_=hm_hp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396619962&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > Warcraft </a> </div> </div> <div class="secondary ellipsis"> Latest TV Promo </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/trailers?ref_=hm_hp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396619962&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Browse more trailers</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','HeroWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-1"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/sundance/starmeter-award-party/?ref_=hm_hm_ph_bdh_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396614342&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Bryce Dallas Howard Receives IMDb's STARmeter Award</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/sundance/starmeter-award-party/?imageid=rm497280000&ref_=hm_hm_ph_bdh_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396614342&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjE0NDIyNjQ2OV5BMl5BanBnXkFtZTgwNzMxNDI4NzE@._UX370_CR38,10,296,438_SY219_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE0NDIyNjQ2OV5BMl5BanBnXkFtZTgwNzMxNDI4NzE@._UX370_CR38,10,296,438_SY219_SX148_AL_UY438_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/sundance/starmeter-award-party/?imageid=rm1722016768&ref_=hm_hm_ph_bdh_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396614342&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTk2MTM0MjQ0MV5BMl5BanBnXkFtZTgwNjQxNDI4NzE@._UX400_CR55,0,296,438_SY219_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk2MTM0MjQ0MV5BMl5BanBnXkFtZTgwNjQxNDI4NzE@._UX400_CR55,0,296,438_SY219_SX148_AL_UY438_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/sundance/starmeter-award-party/?imageid=rm228844544&ref_=hm_hm_ph_bdh_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396614342&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTgxMTM2OTA5MF5BMl5BanBnXkFtZTgwMTIxNDI4NzE@._UX600_CR150,20,296,438_SY219_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTgxMTM2OTA5MF5BMl5BanBnXkFtZTgwMTIxNDI4NzE@._UX600_CR150,20,296,438_SY219_SX148_AL_UY438_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/sundance/starmeter-award-party/?imageid=rm816047104&ref_=hm_hm_ph_bdh_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396614342&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BNzcyMDc3OTgyOV5BMl5BanBnXkFtZTgwMTAxNDI4NzE@._UY438_CR350,0,296,438_SY219_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNzcyMDc3OTgyOV5BMl5BanBnXkFtZTgwMTAxNDI4NzE@._UY438_CR350,0,296,438_SY219_SX148_AL_UY438_UX296_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Congratulations to <a href="/name/nm0397171/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396614342&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ph_bdh_lk1">Bryce Dallas Howard</a>! The star of <i><a href="/title/tt0369610/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396614342&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ph_bdh_lk2">Jurassic World</a></i> and director of <i><a href="/title/tt4442390/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396614342&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ph_bdh_lk3">Solemates</a></i> was presented with the IMDb STARmeter Award by IMDb Founder and CEO Col Needham Monday night at the <a href="/sundance/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396614342&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hm_ph_bdh_lk4">2016 Sundance Film Festival</a>. This award recognizes stars who are breakout fan favorites on IMDbPro's STARmeter chart, which is determined by the actual search behavior of IMDb's more than 250 million unique monthly visitors worldwide.</p> <p class="seemore"><a href="/sundance/starmeter-award-party/?ref_=hm_hm_ph_bdh_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396614342&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Browse photos from the event</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-2"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/sundance/video/?ref_=hm_sun_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396612802&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <h3>In the Picture: From 'Mr. Pig' to 'The Free World'</h3> </a> </span> </span> <p class="blurb"><i><a href="/title/tt2722504/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396612802&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sun_lk1">Mr. Pig</a></i> stars <a href="/name/nm0000418/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396612802&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sun_lk2">Danny Glover</a> and <a href="/name/nm0748973/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396612802&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sun_lk3">Maya Rudolph </a> visit the IMDb Studio at Sundance and explain the challenges of toiling with pigs. Plus, <a href="/name/nm0818055/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396612802&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sun_lk4">Octavia Spencer</a>, <a href="/name/nm0005253/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396612802&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sun_lk5">Elisabeth Moss</a>, and more discuss their powerful film <i><a href="/title/tt3517044/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396612802&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sun_lk6">The Free World</a></i>. Visit our <a href="/sundance/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396612802&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sun_lk7">special section</a> for photos, videos, and more.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1301001497?ref_=hm_sun_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396612802&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi1301001497" data-source="bylist" data-id="ls073917212" data-rid="0H7CDFE1X45ZBVNDDCNP" data-type="playlist" class="video-colorbox" data-refsuffix="hm_sun" data-ref="hm_sun_i_1"> <img itemprop="image" class="pri_image" title="The IMDb Studio (2015-)" alt="The IMDb Studio (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMTc5ODI2NDg0MF5BMl5BanBnXkFtZTgwNzUzOTE4NzE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc5ODI2NDg0MF5BMl5BanBnXkFtZTgwNzUzOTE4NzE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> <img alt="The IMDb Studio (2015-)" title="The IMDb Studio (2015-)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="The IMDb Studio (2015-)" title="The IMDb Studio (2015-)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/sundance/video/?ref_=hm_sun_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396612802&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > Danny Glover, Maya Rudolph Toil With <i>Mr. Pig</i> </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/sundance/studio-day-4?imageid=rm735437824&ref_=hm_sun_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396612802&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Gabrielle Union and Armie Hammer at event of The IMDb Studio (2015)" alt="Gabrielle Union and Armie Hammer at event of The IMDb Studio (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjM5MTM5NjAxN15BMl5BanBnXkFtZTgwNDAzNzE4NzE@._V1_SY201_CR7,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjM5MTM5NjAxN15BMl5BanBnXkFtZTgwNDAzNzE4NzE@._V1_SY201_CR7,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/sundance/studio-day-4/?ref_=hm_sun_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396612802&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > The IMDb Studio - Day 4 Photos </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi663467289?ref_=hm_sun_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396612802&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi663467289" data-source="bylist" data-id="ls073917212" data-rid="0H7CDFE1X45ZBVNDDCNP" data-type="playlist" class="video-colorbox" data-refsuffix="hm_sun" data-ref="hm_sun_i_3"> <img itemprop="image" class="pri_image" title="The IMDb Studio (2015-)" alt="The IMDb Studio (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMTEyNTk5MjM2MTFeQTJeQWpwZ15BbWU4MDM0OTcxODcx._V1_SY201_CR37,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTEyNTk5MjM2MTFeQTJeQWpwZ15BbWU4MDM0OTcxODcx._V1_SY201_CR37,0,201,201_AL_UY402_UX402_AL_.jpg" /> <img alt="The IMDb Studio (2015-)" title="The IMDb Studio (2015-)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="The IMDb Studio (2015-)" title="The IMDb Studio (2015-)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/sundance/video/?ref_=hm_sun_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396612802&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > Octavia Spencer, Elisabeth Moss Rock <i>The Free World</i> </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/sundance/video/?ref_=hm_sun_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396612802&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Watch more video from the IMDb Studio at Sundance</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-3"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NewsDeskWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59426498?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjAxOTcxMjg0OV5BMl5BanBnXkFtZTgwMjg1Mjg1NDE@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59426498?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1"
class="headlines" >Dakota Johnson Finally Explains That Wardrobe Malfunction at the 2016 People's Choice Awards</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_src"
>E! Online</a></span>
    </div>
                                </div>
<p><a href="/name/nm0424848?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk1">Dakota Johnson</a> almost lost her top on live television and now she's here to talk about it.Â  The 26-year-oldÂ 50 Shades of GreyÂ starlet had an unexpectedly awkward run-in with her co-star <a href="/name/nm0005182?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk2">Leslie Mann</a>Â earlier this month when she got up on stage to accept the 2016 People's Choice Award for Favorite ...                                        <span class="nobr"><a href="/news/ni59426498?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59424494?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2"
class="headlines" >Woody Allen to Star in His Amazon Series Opposite...Miley Cyrus?</a>
    <div class="infobar">
            <span class="text-muted">19 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0032055?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2_src"
>Indiewire Television</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59424840?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3"
class="headlines" >Matthew Perry Doesnât Remember Filming 3 Seasons of Friends</a>
    <div class="infobar">
            <span class="text-muted">18 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3_src"
>Vulture</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59424497?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4"
class="headlines" >Directors Guild of America President Wants More Than Just Academy Changes to Address Diversity Issues</a>
    <div class="infobar">
            <span class="text-muted">19 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4_src"
>Vulture</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59426562?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5"
class="headlines" >Jacob Tremblay, Julianne Moore and Eddie Redmayne Headline Star-Studded Presenters List at SAG Awards</a>
    <div class="infobar">
            <span class="text-muted">4 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59425622?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTM3OTUwMDYwNl5BMl5BanBnXkFtZTcwNTUyNzc3Nw@@._V1_SY150_CR11,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59425622?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1"
class="headlines" >âGhost in the Shellâ Moves to Paramount From Disney</a>
    <div class="infobar">
            <span class="text-muted">14 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_src"
>Variety - Film News</a></span>
    </div>
                                </div>
<p> DreamWorksâ â<a href="/title/tt1219827?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk1">Ghost in the Shell</a>,â starring <a href="/name/nm0424060?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk2">Scarlett Johansson</a>, has shifted from Disney to Paramount, keeping its March 31, 2017, wide release date. Paramount is also co-financing the film. Disney had previously announced the March 31 release date. <a href="/name/nm1561982?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk3">Pilou AsbÃ¦k</a> stars in the adaptation of the popular...                                        <span class="nobr"><a href="/news/ni59425622?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59424497?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2"
class="headlines" >Directors Guild of America President Wants More Than Just Academy Changes to Address Diversity Issues</a>
    <div class="infobar">
            <span class="text-muted">19 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2_src"
>Vulture</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59423802?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3"
class="headlines" >Ian McKellen, BFI Bring Back the Bard with 'Shakespeare on Film'</a>
    <div class="infobar">
            <span class="text-muted">21 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000353?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3_src"
>Thompson on Hollywood</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59426562?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4"
class="headlines" >Jacob Tremblay, Julianne Moore and Eddie Redmayne Headline Star-Studded Presenters List at SAG Awards</a>
    <div class="infobar">
            <span class="text-muted">4 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59425901?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5"
class="headlines" >Johnny Depp And Tom Hanks Are Circling A South American Crime Thriller, Which Sounds Awesome</a>
    <div class="infobar">
            <span class="text-muted">12 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000098?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5_src"
>cinemablend.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59424494?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTI1MjU3MTI2MF5BMl5BanBnXkFtZTcwMDgxNTE4MQ@@._V1_SY150_CR1,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59424494?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1"
class="headlines" >Woody Allen to Star in His Amazon Series Opposite...Miley Cyrus?</a>
    <div class="infobar">
            <span class="text-muted">19 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0032055?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_src"
>Indiewire Television</a></span>
    </div>
                                </div>
<p><a href="/name/nm0000095?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk1">Woody Allen</a> has found his new muse, and it's not who you'd expect. According to Deadline, Allen has cast <a href="/name/nm1415323?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk2">Miley Cyrus</a> (who recently appeared in <a href="/name/nm0001068?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk3">Sofia Coppola</a>'s Netflix special " <a href="/title/tt4537842?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk4">A Very Murray Christmas</a>") and <a href="/name/nm0561938?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk5">Elaine May</a> in his upcoming Amazon series. Allen worked with May in "<a href="/title/tt0196216?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk6">Small Time Crooks</a>," but ...                                        <span class="nobr"><a href="/news/ni59424494?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59424840?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2"
class="headlines" >Matthew Perry Doesnât Remember Filming 3 Seasons of Friends</a>
    <div class="infobar">
            <span class="text-muted">18 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2_src"
>Vulture</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59423764?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3"
class="headlines" >âStraight Outta Comptonâ Star Corey Hawkins to Topline Foxâs â24â Spinoff</a>
    <div class="infobar">
            <span class="text-muted">21 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59424495?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4"
class="headlines" >3.6 Million Viewers Watched 'Toni Braxton: Unbreak My Heart' - Lifetimeâs Most Watched Movie in a Year</a>
    <div class="infobar">
            <span class="text-muted">19 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0032055?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4_src"
>Indiewire Television</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59424634?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5"
class="headlines" >David Harewood on Playing Supergirlâs Martian Manhunter and Why Black British Actors Leave the U.K.</a>
    <div class="infobar">
            <span class="text-muted">19 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5_src"
>Vulture</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59426498?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjAxOTcxMjg0OV5BMl5BanBnXkFtZTgwMjg1Mjg1NDE@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59426498?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1"
class="headlines" >Dakota Johnson Finally Explains That Wardrobe Malfunction at the 2016 People's Choice Awards</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_src"
>E! Online</a></span>
    </div>
                                </div>
<p><a href="/name/nm0424848?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk1">Dakota Johnson</a> almost lost her top on live television and now she's here to talk about it.Â  The 26-year-oldÂ 50 Shades of GreyÂ starlet had an unexpectedly awkward run-in with her co-star <a href="/name/nm0005182?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk2">Leslie Mann</a>Â earlier this month when she got up on stage to accept the 2016 People's Choice Award for Favorite ...                                        <span class="nobr"><a href="/news/ni59426498?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59425611?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2"
class="headlines" >Sofia Vergara Files $15 Million Lawsuit Against Venus Concept for Fraudulent Commercial Exploitation</a>
    <div class="infobar">
            <span class="text-muted">15 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59424832?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3"
class="headlines" >Bobbi Kristina Brownâs Aunt Says She Was âDoing Fineâ Before She Died</a>
    <div class="infobar">
            <span class="text-muted">18 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3_src"
>The Wrap</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59424839?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4"
class="headlines" >Charlie Puth and Selena Gomez Made the Chillest Breakup Duet</a>
    <div class="infobar">
            <span class="text-muted">18 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4_src"
>Vulture</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59424045?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5"
class="headlines" >Don't Believe the Rumors - Ronda Rousey Is Not Engaged, Says Ufc Rep</a>
    <div class="infobar">
            <span class="text-muted">21 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NewsDeskWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-4"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>IMDb Snapshot</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1121052416/rg213949184?ref_=hm_snp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395572102&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Kaitlyn Leeb and Alberto Rosende in Shadowhunters: The Mortal Instruments (2016)" alt="Still of Kaitlyn Leeb and Alberto Rosende in Shadowhunters: The Mortal Instruments (2016)" src="http://ia.media-imdb.com/images/M/MV5BMjEyMDE1ODIwNF5BMl5BanBnXkFtZTgwMjExNTA3NzE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjEyMDE1ODIwNF5BMl5BanBnXkFtZTgwMjExNTA3NzE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm1121052416/rg213949184?ref_=hm_snp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395572102&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > "Shadowhunters: The Mortal Instruments" </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3151422464/rg230726400?ref_=hm_snp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395572102&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Falk Hentschel in Legends of Tomorrow (2016)" alt="Still of Falk Hentschel in Legends of Tomorrow (2016)" src="http://ia.media-imdb.com/images/M/MV5BMTkxOTc5MDc2Ml5BMl5BanBnXkFtZTgwNzUwODE4NzE@._V1_SY201_CR43,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTkxOTc5MDc2Ml5BMl5BanBnXkFtZTgwNzUwODE4NzE@._V1_SY201_CR43,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm3151422464/rg230726400?ref_=hm_snp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395572102&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > "Legends of Tomorrow" </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/sundance/2016-sundance-photos-documentary-films?imageid=rm3698909440&ref_=hm_snp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395572102&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Jessie Kahnweiler in The Skinny (2016)" alt="Still of Jessie Kahnweiler in The Skinny (2016)" src="http://ia.media-imdb.com/images/M/MV5BMzE2NTMxMzM5Ml5BMl5BanBnXkFtZTgwOTgyNzA1NzE@._V1_SY201_CR78,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzE2NTMxMzM5Ml5BMl5BanBnXkFtZTgwOTgyNzA1NzE@._V1_SY201_CR78,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/sundance/2016-sundance-photos-documentary-films?imageid=rm3698909440&ref_=hm_snp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395572102&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > Sundance Documentary Stills </a> </div> </div> </div> </div> </div> </div> </div> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-5"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BornTodayWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_borntoday">
<span class="widget_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=1-26&ref_=hm_brn_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Born Today</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1524440?ref_=hm_brn_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Colin O'Donoghue" alt="Colin O'Donoghue" src="http://ia.media-imdb.com/images/M/MV5BMjI1OTA1OTg5MV5BMl5BanBnXkFtZTgwNTUyNDMyMDE@._V1_SY172_CR3,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI1OTA1OTg5MV5BMl5BanBnXkFtZTgwNTUyNDMyMDE@._V1_SY172_CR3,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1524440?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_1">Colin O'Donoghue</a> (35) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0749081?ref_=hm_brn_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Sara Rue" alt="Sara Rue" src="http://ia.media-imdb.com/images/M/MV5BMTQ3Mjc3NTAxMl5BMl5BanBnXkFtZTgwNTY4ODIwMDE@._V1_SY172_CR18,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ3Mjc3NTAxMl5BMl5BanBnXkFtZTgwNTY4ODIwMDE@._V1_SY172_CR18,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0749081?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_2">Sara Rue</a> (37) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0000056?ref_=hm_brn_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Paul Newman" alt="Paul Newman" src="http://ia.media-imdb.com/images/M/MV5BODUwMDYwNDg3N15BMl5BanBnXkFtZTcwODEzNTgxMw@@._V1_SY172_CR12,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BODUwMDYwNDg3N15BMl5BanBnXkFtZTcwODEzNTgxMw@@._V1_SY172_CR12,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0000056?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_3">Paul Newman</a> (1925-2008) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0001122?ref_=hm_brn_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Ellen DeGeneres" alt="Ellen DeGeneres" src="http://ia.media-imdb.com/images/M/MV5BNDAwMzAyNDEzMV5BMl5BanBnXkFtZTcwMDU3MTAxMw@@._V1_SY172_CR6,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNDAwMzAyNDEzMV5BMl5BanBnXkFtZTcwMDU3MTAxMw@@._V1_SY172_CR6,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0001122?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_4">Ellen DeGeneres</a> (58) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0001277?ref_=hm_brn_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Scott Glenn" alt="Scott Glenn" src="http://ia.media-imdb.com/images/M/MV5BMTU3NzAwMzE1OF5BMl5BanBnXkFtZTYwMjkzOTY0._V1_SY172_CR7,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU3NzAwMzE1OF5BMl5BanBnXkFtZTYwMjkzOTY0._V1_SY172_CR7,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0001277?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_5">Scott Glenn</a> (75) </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/search/name?refine=birth_monthday&birth_monthday=1-26&ref_=hm_brn_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See all birthdays</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','BornTodayWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-6"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/offsite/?page-action=offsite-amazon&token=BCYh0ZXbCndmFpiY_wbkbu1xs_rM47U5ZCPPLtmBIFoDYzPdu6Y3TmXIfc0RLs6a3u3ke8_QZaWj%0D%0As7b-T_rBioojCezTh8g73oV1LRM25qQ-SAApHgGBoHkZHDR4ngYrqnjR3toBFtzHV9m-fTtrXPPN%0D%0AAJzxG_XvD2FpKMi0FcmP416biqMN9ThP0K1V9PxpXZRSgABpZiSjyExDpkTipRpQoHCu36tAtBvS%0D%0AZcYqlxZ5HZf65Sc-iqG8LKbIQxF_P_qx%0D%0A&ref_=hm_pop_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396055542&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage" > <h3>IMDb Asks With Host Jerry O'Connell: Dominic Monaghan</h3> </a> </span> </span> <div class="widget_content inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?page-action=offsite-amazon&token=BCYj2_hYsOuxnrBaCF2EfJEA_7os6H2NSbcr0MddRVBgWW9lqe2f4p-pFnd7CH6f5fZUW6SQpI38%0D%0AnM8RXk85TfGGpybsfDwEjoDp2n_Yud3kzTqjHBfbRoS2ZnUU89-ik2M5Hrk87tV8-3j-RyvD3wim%0D%0AtEV4vytlxdgoLxpBaCfnKBoKddnRf_U1FCtIe2QvjrCEoVBM2sdhHOoJIrbiPGnH4vwLpbMTOBM1%0D%0Ap_JxYj69omxjaaWk0EFkYNqWPmRWT0BM%0D%0A&ref_=hm_pop_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396055542&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjA5NDA0ODYwMV5BMl5BanBnXkFtZTgwMTc0ODk3NzE@._SX600_CR120,10,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA5NDA0ODYwMV5BMl5BanBnXkFtZTgwMTc0ODk3NzE@._SX600_CR120,10,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> <div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="widget_inline_blurb"><p class="blurb">Join our live conversation on Tuesday, Jan. 26, at 9 p.m. ET/6 p.m. PT with <a href="/name/nm0597480/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396055542&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pop_lk1">Dominic Monaghan</a>, star of Travel Channel's "<a href="/title/tt2523506/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396055542&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pop_lk2">Wild Things With Dominic Monaghan</a>," which kicks off Season 3 on Wednesday, Jan. 27. Tune into <a href="/offsite/?page-action=offsite-amazon&token=BCYjA6BGTzeAGSPh8cAcfzI96uwsU6sF4VClk9tXnNVmYTEya6qggcNxjqLD_pcO-J3RlHeslUlZ%0D%0A08Jul35QD4b7m5zHlL62QY-BEBofrV6u-oD6wbjFK-XWl8d6y_XI-5anzr-zMR3ovKwmmhOx5V9C%0D%0AupBYzpqK0-fCjq3VTWwq7-2trwfMUXF0sQpGO5UUw-YtNMg5CpdW28tsTa54yKAbDKo_APbVh527%0D%0AAqHRnAuqdV110WfdPzMSxAYi1hqbtpG1%0D%0A&ref_=hm_pop_lk3">Amazon.com/DominicMonaghan</a> to watch, live chat, and ask Dominic a question yourself! This livestream is best viewed on laptops, desktops, and tablets.</p> <p class="seemore"><a href="/offsite/?page-action=offsite-amazon&token=BCYsg1ywUCvlNTI_sh7aAfZJ-osFZQAJjd6c7GrmAbuNiagfphqiGf0FJBRSlFq2ftDWzZB_tDob%0D%0AiTbat_e0dDXaQ2xfd1ExZ_3lbLwG05hcr9UXnmoSNjDcBeIU6nBXmyVnm5Idta5eNgaSFw1uLrvR%0D%0Ap7Wyf8OeHzx4t5UgbYFzh0micNqKRpWiOfExrjv5ErPyYGXY4JNhrWfXyOtwIhPofZ79JhTtF--p%0D%0AH25LSp9xKnooDsEb9lQdjIX2u_2cCh7L%0D%0A&ref_=hm_pop_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396055542&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-6&pf_rd_t=15061&pf_rd_i=homepage" class="position_blurb supplemental" >Tune in here for the one-on-one interview</a></p> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-7"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>Photos From the 2016 Sundance Film Festival</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/sundance/portraits-and-people/?imageid=rm3282101248&ref_=hm_sun_ph_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396620002&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Tyler Posey" alt="Tyler Posey" src="http://ia.media-imdb.com/images/M/MV5BMjE3Njc1NjQ1Ml5BMl5BanBnXkFtZTgwMDExMzI4NzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE3Njc1NjQ1Ml5BMl5BanBnXkFtZTgwMDExMzI4NzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/sundance/portraits-and-people/?ref_=hm_sun_ph_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396620002&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > Portraits and People </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/sundance/studio-day-4?imageid=rm3098141696&ref_=hm_sun_ph_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396620002&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Maya Rudolph in The IMDb Studio (2015)" alt="Maya Rudolph in The IMDb Studio (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTUzMTczOTAzNl5BMl5BanBnXkFtZTgwNzYzOTE4NzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUzMTczOTAzNl5BMl5BanBnXkFtZTgwNzYzOTE4NzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/sundance/studio-day-4/?ref_=hm_sun_ph_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396620002&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > The IMDb Studio at Sundance -- Day 4 </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/sundance/premieres-and-parties/?imageid=rm3248546816&ref_=hm_sun_ph_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396620002&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjM4OTQ1MDEwMV5BMl5BanBnXkFtZTgwMjExMzI4NzE@._UY402_CR40,0,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjM4OTQ1MDEwMV5BMl5BanBnXkFtZTgwMjExMzI4NzE@._UY402_CR40,0,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/sundance/premieres-and-parties/?ref_=hm_sun_ph_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396620002&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > Premieres and Parties </a> </div> </div> </div> </div> </div> </div> </div> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-8"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>Indie Focus: Exclusive Clip From 'Badge of Honor'</h3> </span> </span> <p class="blurb"><a href="/name/nm0000640?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395410602&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_lk1">Martin Sheen</a> and <a href="name/nm0002546?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395410602&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_if_lk2">Mena Suvari</a> star in this drama about an Internal Affairs Detective investigating the shooting of a child during a drug bust.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt3355510/?ref_=hm_if_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395410602&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Badge of Honor (2015)" alt="Badge of Honor (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjM3NDE1NjI2NV5BMl5BanBnXkFtZTgwMzA2MzQ2NzE@._V1_SY250_CR3,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjM3NDE1NjI2NV5BMl5BanBnXkFtZTgwMzA2MzQ2NzE@._V1_SY250_CR3,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi4018713881?ref_=hm_if_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395410602&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi4018713881" data-rid="0H7CDFE1X45ZBVNDDCNP" data-type="single" class="video-colorbox" data-refsuffix="hm_if" data-ref="hm_if_i_2"> <img itemprop="image" class="pri_image" title="Clip from the film 'Badge of Honor'" alt="Clip from the film 'Badge of Honor'" src="http://ia.media-imdb.com/images/M/MV5BODQwNDcwMDk0Nl5BMl5BanBnXkFtZTgwNzYwNTk2NzE@._V1_SX444_CR0,0,444,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BODQwNDcwMDk0Nl5BMl5BanBnXkFtZTgwNzYwNTk2NzE@._V1_SX444_CR0,0,444,250_AL_UY500_UX888_AL_.jpg" /> <img alt="Clip from the film 'Badge of Honor'" title="Clip from the film 'Badge of Honor'" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Clip from the film 'Badge of Honor'" title="Clip from the film 'Badge of Honor'" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-13"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','RecsWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','RecsWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-19"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TriviaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_trivia">
<span class="widget_header"> <span class="oneline"> <a href="/title/tt0974015/trivia?item=tr2729124&ref_=hm_trv_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Did You Know?</h3> </a> </span> </span> <div class="widget_content inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt0974015?ref_=hm_trv_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="The Justice League Part One (2017)" alt="The Justice League Part One (2017)" src="http://ia.media-imdb.com/images/M/MV5BYmIzNjE1ZDUtMDI1NC00NjJlLTkyNzctODg5MGUzMDJlMzlhXkEyXkFqcGdeQXVyNjQ2NDcxNTA@._V1_SY132_CR81,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BYmIzNjE1ZDUtMDI1NC00NjJlLTkyNzctODg5MGUzMDJlMzlhXkEyXkFqcGdeQXVyNjQ2NDcxNTA@._V1_SY132_CR81,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> <div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="widget_inline_blurb"><strong class="text-large"><a href="/title/tt0974015?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">The Justice League Part One</a></strong> <p class="blurb">Oliver Queen / Green Arrow and Dinah Lance / Black Canary might cameo along with Hal Jordan / Green Lantern teaming up with the original six team members.</p> <p class="seemore"><a href="/title/tt0974015/trivia?item=tr2729124&ref_=hm_trv_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" class="position_blurb supplemental" >See more trivia</a></p> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','TriviaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-26"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','PollWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;XkU9LZbiE74&quot;}">
        
    

    <div class="ab_poll poll">
<span class="widget_header"> <span class="oneline"> <a href="/poll/XkU9LZbiE74/?ref_=hm_poll_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390695842&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Poll: Oscars 2016: Original Score</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390695842&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk">More Polls</a></h4> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/XkU9LZbiE74/?ref_=hm_poll_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390695842&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Thomas Newman" alt="Thomas Newman" src="http://ia.media-imdb.com/images/M/MV5BMTI4NzkzMjUxOF5BMl5BanBnXkFtZTYwOTM0ODY2._V1_SY207_CR21,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTI4NzkzMjUxOF5BMl5BanBnXkFtZTYwOTM0ODY2._V1_SY207_CR21,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/XkU9LZbiE74/?ref_=hm_poll_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390695842&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Carter Burwell" alt="Carter Burwell" src="http://ia.media-imdb.com/images/M/MV5BMTQ4NjI4NDU4MV5BMl5BanBnXkFtZTgwNTcyOTk3NjE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ4NjI4NDU4MV5BMl5BanBnXkFtZTgwNTcyOTk3NjE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/XkU9LZbiE74/?ref_=hm_poll_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390695842&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Ennio Morricone" alt="Ennio Morricone" src="http://ia.media-imdb.com/images/M/MV5BMTk4MDgxMjI2OF5BMl5BanBnXkFtZTYwNjkzODc0._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk4MDgxMjI2OF5BMl5BanBnXkFtZTYwNjkzODc0._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/XkU9LZbiE74/?ref_=hm_poll_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390695842&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="JÃ³hann JÃ³hannsson" alt="JÃ³hann JÃ³hannsson" src="http://ia.media-imdb.com/images/M/MV5BOTg2NzM1NTk4N15BMl5BanBnXkFtZTcwMDE5MDk4Ng@@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BOTg2NzM1NTk4N15BMl5BanBnXkFtZTcwMDE5MDk4Ng@@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/XkU9LZbiE74/?ref_=hm_poll_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390695842&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="John Williams" alt="John Williams" src="http://ia.media-imdb.com/images/M/MV5BMjY5MTgzMTQ1NF5BMl5BanBnXkFtZTYwNDg3OTcz._V1_SY207_CR2,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjY5MTgzMTQ1NF5BMl5BanBnXkFtZTYwNDg3OTcz._V1_SY207_CR2,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Who should win the Academy Award for Original Score at the 2016 Oscars Awards? Discuss <a href="http://www.imdb.com/board/bd0000088/nest/252617111?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390695842&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk1">here</a> after voting.</p> <p class="seemore"><a href="/poll/XkU9LZbiE74/?ref_=hm_poll_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390695842&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Vote now</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','PollWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-28"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396620602&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Catch Up on IMDb Asks</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396620602&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BOTUyMTQ2MTI2OF5BMl5BanBnXkFtZTgwNjU4OTc3NzE@._UX500_CR20,25,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BOTUyMTQ2MTI2OF5BMl5BanBnXkFtZTgwNjU4OTc3NzE@._UX500_CR20,25,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396620602&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjI4OTM0OTU0MF5BMl5BanBnXkFtZTgwNDg4MDE4NzE@._UX750_CR140,130,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI4OTM0OTU0MF5BMl5BanBnXkFtZTgwNDg4MDE4NzE@._UX750_CR140,130,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396620602&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMzAxMjY5MjY4OF5BMl5BanBnXkFtZTgwOTMxMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzAxMjY5MjY4OF5BMl5BanBnXkFtZTgwOTMxMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396620602&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BNjQ0OTEzMTk1OF5BMl5BanBnXkFtZTgwODQyMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjQ0OTEzMTk1OF5BMl5BanBnXkFtZTgwODQyMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Did you miss one of our one-on-one interviews? Check out our archived interviews with your favorite stars.</p> <p class="seemore"><a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2396620602&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Watch all our one-on-one interviews</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    
            </div>
            <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,300x600,11x1;p=tr;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=051678840399;ord=051678840399?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;ord=051678840399?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;ord=051678840399?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	

    
    
        <a name="slot_right-1"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<p class="blurb"><a href="https://imdb.co1.qualtrics.com/SE/?SID=SV_1AKLG3wDXgw8B7v&ref_=hm_sh_lk1"><b>Feedback:</b> Please take 5 minutes to participate in our short IMDb survey</a></p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-3"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-in-theaters/?ref_=hm_otw_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390111522&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Opening This Week</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2267968"></div> <div class="title"> <a href="/title/tt2267968?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390111522&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t0"> Kung Fu Panda 3 </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2025690"></div> <div class="title"> <a href="/title/tt2025690?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390111522&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t1"> The Finest Hours </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4667094"></div> <div class="title"> <a href="/title/tt4667094?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390111522&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t2"> Fifty Shades of Black </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2140037"></div> <div class="title"> <a href="/title/tt2140037?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390111522&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t3"> Jane Got a Gun </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-in-theaters/?ref_=hm_otw_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390111522&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See more opening this week</a></p>
                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	

    
    
        <a name="slot_right-4"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="showtimesMessagingWidget"> <p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2006955922&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=right-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> </div> </div> </span>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-6"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BoxOfficeListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/chart/?ref_=hm_cht_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Now Playing (Box Office)</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1663202"></div> <div class="title"> <a href="/title/tt1663202?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t0"> The Revenant </a> <span class="secondary-text">$16.0M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2488496"></div> <div class="title"> <a href="/title/tt2488496?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t1"> Star Wars: Episode VII - The Force Awakens </a> <span class="secondary-text">$14.1M</span> </div> <div class="action"> <a href="/showtimes/title/tt2488496?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2869728"></div> <div class="title"> <a href="/title/tt2869728?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t2"> Ride Along 2 </a> <span class="secondary-text">$12.5M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1860213"></div> <div class="title"> <a href="/title/tt1860213?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t3"> Dirty Grandpa </a> <span class="secondary-text">$11.1M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3882082"></div> <div class="title"> <a href="/title/tt3882082?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t4"> The Boy </a> <span class="secondary-text">$10.8M</span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/chart/?ref_=hm_cht_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See more box office results</a></p>
                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','BoxOfficeListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-8"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-coming-soon/?ref_=hm_cs_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Coming Soon</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt0475290"></div> <div class="title"> <a href="/title/tt0475290?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t0"> Hail, Caesar! </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1374989"></div> <div class="title"> <a href="/title/tt1374989?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t1"> Pride and Prejudice and Zombies </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-coming-soon/?ref_=hm_cs_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See more coming soon</a></p>
                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-12"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TwitterWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_twitter">
<span class="widget_header"> <span class="oneline"> <h3>Follow Us On Twitter</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3396827429._CB298884168_.html#%7B%22width%22%3A%22300px%22%2C%22height%22%3A%22500px%22%2C%22href%22%3A%22https%3A%2F%2Ftwitter.com%2FIMDb%22%2C%22heading%22%3A%22Follow%20Us%20On%20Twitter%22%2C%22screen-name%22%3A%22IMDb%22%2C%22list-name%22%3Anull%2C%22widget-id%22%3A%22354387022028357633%22%7D'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','TwitterWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-14"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','FacebookWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_facebook">
<span class="widget_header"> <span class="oneline"> <h3>Find Us On Facebook</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','FacebookWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-30"></a>
        <div class="aux-content-widget-2 sticky-widget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/awards-central/?ref_=hm_ac_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395406682&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <h3>It's Oscars&#174; Season on IMDb</h3> </a> </span> </span> <p class="blurb">Check out IMDb's full coverage of all the major awards events, including the <a href="/awards-central/oscars/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395406682&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_lk1">2016 Academy Awards</a>.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/?ref_=hm_ac_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395406682&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Chris Rock" alt="Chris Rock" src="http://ia.media-imdb.com/images/M/MV5BMTUwMjA0NzE4NV5BMl5BanBnXkFtZTgwOTM3OTI5MzE@._V1_SX700_CR0,0,700,525_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUwMjA0NzE4NV5BMl5BanBnXkFtZTgwOTM3OTI5MzE@._V1_SX700_CR0,0,700,525_AL_UY1050_UX1400_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/awards-central/?ref_=hm_ac_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395406682&pf_rd_r=0H7CDFE1X45ZBVNDDCNP&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Visit our Awards Central section</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    
            </div>
            <br class="clear" />
        </div>
        

    
    
    

    
    
    
        
    
        <br class="clear" />
    </div>
</div>
                





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYlKpuOVqwZZwH9nLg53RT_H4DpJp8bUgIuGdLVcmhzmpXIQBno6wzSViCieha_UHZ6-PM7jglc%0D%0Athsy3yGcn6VbLT4USW62eH0BqauqLHB1AEx4XG3gRg3gRIghg_ElAm97bTLLq15QBEuCSecVz7fa%0D%0Awl58AiZYRZyY29EXn5Ji9XSx_dCuOAfwuBAA_mj_dhRTbFDF6CMOLPG73Tmax-DMFMKMukYhsrX4%0D%0AKvzhk4jtmUM%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYjPkI9S5OiRMsYh1b2T4plLtg9lZw9H1kXfTyN6W2pzPd798ma4lLu85OuHR8wiVnCbAJny4wp%0D%0AnGnIr2l8shBB4FgSnpQfiNXDLbjjySRf2q73cIXYLHHJmJ0ASr7NavvZrnr95XOWKlJR356wVtDl%0D%0A1xyMMtn7QZHMpd4IEi2YJ_6oDstruDSCnJV-0EJzTihiX-4QFVW7B8tpNTT8YU8LLg%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYjpzygptktoSs2tqJICrX9RLaJk67pEjt108IkwWngdo224RjUiRLk-E1i1HWc7im2n6w7sVmZ%0D%0AIaUF6U7WmIctPZD0gog7LsOUTa8eBfwS1X9uip7ixfojwE5WDtVoLbzH6QRMjSPkhmq-aARpELzQ%0D%0AgrdIJb8ZPZi23NyprSlhJZubVZ59cFXMH2wG-tLxuvqK2AJc9j7Rq1CaFDUkoA2lNon-gHLJUx-e%0D%0AxaDwaMae2sx8m0wMqtgHGXUKn-W5iKKYkNd3GqxhAzIejrJEak8w88gq_Qa6ph1jR5s7MkrB7ZBE%0D%0A_6VkfJ-QrTe1j3U_ZjIg35pQx4MmCvG3VLUhY6kvduP43_R5VyqVGiG8Gt0C46Y4wywc4EXG8nYy%0D%0AU-Gh2bpspag9n9BwzXGJA7aRQuz7kMMizAoH9WcKn4_SwvAsr7I%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYujRHceXuRIejLYsx6DehNIbB3Dhhsu1THg_Ls_hax9xNlr_kTLZ80GLt8csN9jjRjqqSt48SO%0D%0AEnaA4SMi3pVd9HZLLDVLKFWVcN7BR5vw9Wytluml0P6E_KJq9W77SnDMbZ86Xs0V2Iwa-kXFNpBF%0D%0AsTTEEcjgafJ9jQl1buVziyfZ4PjTR4gL2DH0fyX7C_CCzbWDK74mPfRrEl4bCQ4f6g%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYr9bYyDnEJkmdgAurerIz_ZcAnxUAYBYYsvHw9sgcGIbFZk7coTELHqSzZkXufQotBhRSdcdrI%0D%0AlL2Gosw1Xj0COu5S0cr8cUreEiNZwVJST8vPoJNicdEqORllPxYTG5CNQzPnsqKBY84FLRaTgDxo%0D%0AmwQ6O6nVG0oi6DDuNTqbe-NBsuc0OYwZcaxfMPhz4goO%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYhSjq-AMfNQVIw1LzMoximnaYdXVG3eZeHTzjvWzqS3svyqHSVW099k_oXm1K7TYXbJGk7Fj8-%0D%0AL875Or5ZirHY4S9b08YWyr8WZFbwmDgIt59JhedFrw6b3iV7CUXSrDx8a_pKLz47Jvv_U2K97Vpq%0D%0AE6XGOkvA-uMYqxllfz9jnsp9wM9-GWoz8QpSJxON-DkW%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top Rated Movies</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Box Office</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li><a href="/advertising/?ref_=ft_ad"
>Advertising</a></li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYmfqnUXIDf_f2in4KioIDlnhtlrrmjPYtzAMv1UAcwcGZ1_FEy-IXc1Qw2rZzOgLNSMqZZckMq%0D%0A1zdyx2YjmutqiSZ143UvCD_vZdvlWx8fNiwpyIo2WtF0KSRRogtVIKllmnEpZt0NiGk8BkxAI90e%0D%0A7P2qgVGp9TzkVPEf-nW_gaSLfTVRlPZCS9kmEiGR57Lymh9KOCfpeOpbtu1fC2AHtw%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYvOJG4yMHND0AbOSLuZULmcwvV_ndqLplouY-XbJUzuuT1mfzzf0MHNli8Ykjv3HmBDsBVMD88%0D%0A9Pg_ZHGh89xebJD6DGCZGM0mhTaH1rHnvbO3hc9hyH3NalY1nJm3s9IgIXGirT5tjZNp3BngVf1z%0D%0AabhP0Q3Iz6lSZurGxyVDQPw3iB3XSUhychJh-oIbvtdw%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYvZoT2On190D8HyYXaVOCI-J3kiq_nTrfBXttPtp9xGt5pyHpN4xO9oeYH1u4QXF7L2ycmIJlR%0D%0Ar3XIg3ZU2DuCGi17Ru9neRSW9B5ZTFcs0lYE-mssTQDuLCR-QKAkuWBfvAy_pOAlBXIKyFkmhV50%0D%0AObTyR4KlX3Gz3kFwhbij1JFbQ6TDfbnl8aSInAswMRQJQxbSjkNP44qBGrm54vfsQqXOMmINyx-C%0D%0AVRyJcz6GOxOGND3LXlnhd8wwajGkWsD3%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2016
                <a
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYk0bIqUvPlqMlMme6H7y18km643uv3Ymv-49EswtAmcw1BbxRbQjRfAjj7T4AZiW4H3O9BtB4z%0D%0AZJlw_07tefFDXC_oJ9KaAYUFspkUpi1H58eKDyis0fPOnS4cIvKLL_DnJ71_hE22e2DIXd9bLXXt%0D%0AQKPpXNipJ-dCeUrtFXea9Gs_afKRF66vPdjTdRyivpvTUjOMqG-YP1xPrjfZylJqBeLN_I5ZOs3U%0D%0ArQ4fxRpZ6ls%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYoDQHAnHyfScfTtyWTCYVU4-qqqBNEMOob64hqrr0LsFIJka8wEwzCScRQVjG-ITKjC-IgSfSy%0D%0ANPVSsNUgkVW_cuRJ3Q4LmJafl81HYeo66J3fWZQNthKPak23zqyseGxKdjG0C5bi-k-W9ydrVvDO%0D%0AoRIOJIsqY_zCapYRYKSYn_Pj79v-bGFG_PL6DCSxB0oqcufClZpvL0PLy_OF_cuAE7X6Sk6IcP-H%0D%0ACq7Rmnfk0SY%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYppVR_3Wn4I4U1pr_R0ZfT-4MC66dKRW0m_Kmft-spiJnDTrjKz_CC3zGnH7OH1ZS2zQ-cuMVB%0D%0ALjW-U_0j6UcE-vHQrhVMqdGHanHmThA1W0FbKTLlRadtF-N8CRPVU1KvgDRW4SRT5NCOCHJkjJj9%0D%0AYk-rttsqwOTUOdXaiqJAh2K0G1xYibnKbMYNHOVYtHkKTp5rOfOOQAwdIRP5ZgXlfUY7fQFthQqY%0D%0AkoTDCAieX7s%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYnuyjp0xCOC-uZIfKhCLj5uAtUrD9RypIYSfhfb2BTPKdtERoJ27FC8swx1t8cf6EvgRuWo4C_%0D%0AX-6x-ftYKhwfExZp2bTawjQGEmXoy72yoQTy1SSoiYN6C5W4-SwajaFgVThMy2s9Si2bNtl0QhvD%0D%0AN32AONQMQnbD4wBynP9hDvzJDTxhXodj1IH33SN0taoeILdCz37DiybbSh2qziJ7eHiGdBvoyS5E%0D%0AvznDHK65lz0%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYjQjYC9o08Gu5aZJ6hTkhvwsrToE3Djbql17C64r8-8ZRut0Lrw08FIspqqpee7XdCcD942QxO%0D%0A5m_6hRajhSYl8Q3-UTYdyG-EsZ7MjWHb9586IUAAr36eQVfn9No1McxLXXp8cE93kKzvwnFoAozm%0D%0A8el5ENwhw5b1FGSJVd9cCSy7Kc3_o3fJSnfw0OnnMNZU4IcAr2bwcJX9q_HifoLGIyq7fGqh8kkT%0D%0A6gvi19Gnbuk%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYoXFvKOCNAcyvMNWNMVw_k2GVEo4k5Ft2lVDJ9A1k2x4gd9E8W09wcTZ72D_76W74zhZGUcgae%0D%0APwaqVYBGvUirddQVjvwqSsNvUaPIHrCAdrRj6gk3rWG85qWrB44huNvHscsNRrv0TeHKo58clkLT%0D%0A-M98_etMKo1h8_Jgf-ckascHEKOfqUqBI8nznmOjSZyxrb6t9F3rk-o1wISudMFjvaavOrnagp1B%0D%0A1N17AH9D3RtnAaiy9xxYLsbmCqfNP_-p%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYrV6JnwlK5NRsNp4Zt0XsXYfqsZIa82gPJLJ1LbxxdgN-HSaPSu0P10YvQE9H64bqhO_qaGCMh%0D%0AvWvgDSB-4F3oy9TTeohde2Kr_xYv6WWX92IkgazO0zA5uHDdco_7Y9nBvCbFKNKPN9dGTY0M0Fj2%0D%0ALQ6QH-gfRSTrZcUeffv8a3c%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYvLKax2M-tlUVtO27N9sjBVXk9I8chz4bNglG87zVoPGmvV6sXiPpXjx90EZU3Bk3PloVhMv2Q%0D%0AOBo6Z0ncJDQhUIhCzb5ocjZBjW9-6HIdhvUbzvNt80cJ3FZZrz8UVOcQnnSNXCZYzhTdm68f8Vhf%0D%0ApES3vvkpOvAJIBnlNCFwBGw%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/common-3617413113._CB300284919_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-2635043045._CB299003638_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-335260898._CB289148898_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-194820129._CB286493173_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-2126106747._CB289323193_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=01014611d7206027c939a9522857fe85d0fc9c336adfde99ec1d42842f126dd20a37",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=051678840399"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-232398347._CB349580400_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=051678840399&ord=051678840399";
    },"unable to request AAN pixel");
</script>

<div id="flashContent" style="width: 0px; height: 0px; overflow:hidden;">
    <script type="text/javascript">
        if (generic && generic.monitoring.record_metric && flashAdUtils.canPlayFlash) {
            var flashLoaded = 0;
            function swfLoaded() {
                flashLoaded = 1;
                generic.monitoring.record_metric('ads_flash_did_play', 1, true);
            }
            window.onload = function() {
                // Flash was not loaded
                if (flashLoaded === 0) {
                    // Flash could be loaded but wasn't
                    if (flashAdUtils.canPlayFlash()) {
                        generic.monitoring.record_metric('ads_flash_can_but_did_not_play', 0, true);
                    } else {
                        generic.monitoring.record_metric('ads_flash_cannot_play', 0, true);
                    }
                }
            }
            /**
             * There is a rare possibility of window.onload getting called before swfLoaded.
             * Firing this metric to keep track of such calls.
             */
             generic.monitoring.record_metric('ads_flash_page_loaded', 1, true);
         }
    </script>
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="1x1" align="middle">
        <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
        <param name="quality" value="high" />
        <param name="play" value="true" />
        <param name="loop" value="true" />
        <param name="wmode" value="transparent" />
        <param name="scale" value="showall" />
        <param name="menu" value="true" />
        <param name="devicefont" value="false" />
        <param name="salign" value="" />
        <param name="allowScriptAccess" value="always" />

        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" width="1" height="1">
            <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
            <param name="quality" value="high" />
            <param name="play" value="true" />
            <param name="loop" value="true" />
            <param name="wmode" value="transparent" />
            <param name="scale" value="showall" />
            <param name="menu" value="true" />
            <param name="devicefont" value="false" />
            <param name="salign" value="" />
            <param name="allowScriptAccess" value="always" />
        </object>
        <!--<![endif]-->
    </object>
</div>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="525"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
