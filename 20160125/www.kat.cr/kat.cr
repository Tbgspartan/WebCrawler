<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="auto">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-b76c8ca.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-b76c8ca.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-b76c8ca.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-b76c8ca.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kickass.to/sc-b76c8ca.js');

        sc('setHost', 'a.kickass.to');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: 'b76c8ca',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-b76c8ca.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<div  data-sc-slot="_60318cd4e8d28f6fb76fe34e9bd9c498"></div>
<div  data-sc-slot="_39ecb76dd457e5ac33776fdf11500d56"></div>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<a href="#" id="showHideSearch"><i class="ka ka-zoom"></i></a>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka ka-settings"></i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka ka-search"></i></button></div>
			</form>
		</div>
        <div  data-sc-slot="_277923e5f9d753c5b0630c28e641790c"></div>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080p/" class="tag7">1080p</a>
	<a href="/search/2015/" class="tag10">2015</a>
	<a href="/search/3d/" class="tag4">3d</a>
	<a href="/search/720p/" class="tag3">720p</a>
	<a href="/search/android/" class="tag2">android</a>
	<a href="/search/apk/" class="tag2">apk</a>
	<a href="/search/bajirao%20mastani/" class="tag2">bajirao mastani</a>
	<a href="/search/boruto%20naruto%20the%20movie/" class="tag2">boruto naruto the movie</a>
	<a href="/search/bridge%20of%20spies/" class="tag2">bridge of spies</a>
	<a href="/search/creed/" class="tag1">creed</a>
	<a href="/search/daddy%20s%20home/" class="tag2">daddy s home</a>
	<a href="/search/dilwale/" class="tag3">dilwale</a>
	<a href="/search/discography/" class="tag3">discography</a>
	<a href="/search/doctor%20who/" class="tag1">doctor who</a>
	<a href="/search/dual%20audio%20hindi/" class="tag4">dual audio hindi</a>
	<a href="/search/dvdscr/" class="tag5">dvdscr</a>
	<a href="/search/etrg/" class="tag2">etrg</a>
	<a href="/search/french/" class="tag3">french</a>
	<a href="/search/goosebumps/" class="tag2">goosebumps</a>
	<a href="/search/hateful%20eight/" class="tag1">hateful eight</a>
	<a href="/search/hindi/" class="tag9">hindi</a>
	<a href="/search/hindi%202015/" class="tag6">hindi 2015</a>
	<a href="/search/hunger%20games/" class="tag2">hunger games</a>
	<a href="/search/is%20safe%201/" class="tag2">is safe 1</a>
	<a href="/search/ita/" class="tag3">ita</a>
	<a href="/search/legend/" class="tag2">legend</a>
	<a href="/search/malayalam/" class="tag3">malayalam</a>
	<a href="/search/motorhead/" class="tag2">motorhead</a>
	<a href="/search/nezu/" class="tag10">nezu</a>
	<a href="/search/nl/" class="tag2">nl</a>
	<a href="/search/one%20punch%20man/" class="tag2">one punch man</a>
	<a href="/search/sicario/" class="tag2">sicario</a>
	<a href="/search/spectre/" class="tag4">spectre</a>
	<a href="/search/star%20wars/" class="tag6">star wars</a>
	<a href="/search/star%20wars%20the%20force%20awakens/" class="tag3">star wars the force awakens</a>
	<a href="/search/straight%20outta%20compton/" class="tag2">straight outta compton</a>
	<a href="/search/tamil/" class="tag5">tamil</a>
	<a href="/search/telugu/" class="tag4">telugu</a>
	<a href="/search/telugu%202015/" class="tag3">telugu 2015</a>
	<a href="/search/the%20big%20short/" class="tag2">the big short</a>
	<a href="/search/the%20hateful%20eight/" class="tag2">the hateful eight</a>
	<a href="/search/the%20martian/" class="tag3">the martian</a>
	<a href="/search/the%20revenant/" class="tag2">the revenant</a>
	<a href="/search/the%20walking%20dead/" class="tag3">the walking dead</a>
	<a href="/search/wwe/" class="tag2">wwe</a>
	<a href="/search/yify/" class="tag10">yify</a>
	<a href="/search/yify%201080p/" class="tag4">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag4">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11958315,0" class="icommentjs kaButton smallButton rightButton" href="/burnt-2015-french-bdrip-xvid-ac3-funkky-avi-t11958315.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/burnt-2015-french-bdrip-xvid-ac3-funkky-avi-t11958315.html" class="cellMainLink">Burnt 2015 FRENCH BDRip XViD AC3-FUNKKY avi</a></div>
			</td>
			<td class="nobr center" data-sort="1472028232">1.37 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T16:32:06+00:00">24 Jan 2016, 16:32:06</span></td>
			<td class="green center">10243</td>
			<td class="red lasttd center">4503</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11954463,0" class="icommentjs kaButton smallButton rightButton" href="/the-runner-2015-french-brrip-xvid-am84-avi-t11954463.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-runner-2015-french-brrip-xvid-am84-avi-t11954463.html" class="cellMainLink">The Runner 2015 FRENCH BRRip XviD-AM84 avi</a></div>
			</td>
			<td class="nobr center" data-sort="1298509232">1.21 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T22:32:50+00:00">23 Jan 2016, 22:32:50</span></td>
			<td class="green center">10145</td>
			<td class="red lasttd center">4112</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11945776,0" class="icommentjs kaButton smallButton rightButton" href="/terminus-2015-hdrip-xvid-ac3-evo-t11945776.html#comment">59 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/terminus-2015-hdrip-xvid-ac3-evo-t11945776.html" class="cellMainLink">Terminus 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1510126765">1.41 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T10:07:13+00:00">22 Jan 2016, 10:07:13</span></td>
			<td class="green center">6036</td>
			<td class="red lasttd center">4160</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11958685,0" class="icommentjs kaButton smallButton rightButton" href="/queen-of-the-desert-2016-hdrip-xvid-ac3-evo-t11958685.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/queen-of-the-desert-2016-hdrip-xvid-ac3-evo-t11958685.html" class="cellMainLink">Queen of the Desert 2016 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1509743899">1.41 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T18:09:19+00:00">24 Jan 2016, 18:09:19</span></td>
			<td class="green center">4167</td>
			<td class="red lasttd center">6806</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11952186,0" class="icommentjs kaButton smallButton rightButton" href="/afterdeath-2015-hdrip-xvid-ac3-evo-t11952186.html#comment">37 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/afterdeath-2015-hdrip-xvid-ac3-evo-t11952186.html" class="cellMainLink">AfterDeath 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1471012683">1.37 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T12:14:15+00:00">23 Jan 2016, 12:14:15</span></td>
			<td class="green center">5141</td>
			<td class="red lasttd center">4735</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11958826,0" class="icommentjs kaButton smallButton rightButton" href="/jeruzalem-2015-hdrip-xvid-etrg-t11958826.html#comment">32 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/jeruzalem-2015-hdrip-xvid-etrg-t11958826.html" class="cellMainLink">Jeruzalem 2015 HDRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="740717323">706.4 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T18:54:28+00:00">24 Jan 2016, 18:54:28</span></td>
			<td class="green center">3396</td>
			<td class="red lasttd center">4523</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/larry-gaye-2015-french-bdrip-xvid-mzisys-avi-t11959579.html" class="cellMainLink">Larry Gaye 2015 FRENCH BDRip XviD-MZISYS avi</a></div>
			</td>
			<td class="nobr center" data-sort="731224064">697.35 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T22:44:56+00:00">24 Jan 2016, 22:44:56</span></td>
			<td class="green center">4183</td>
			<td class="red lasttd center">1896</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11957026,0" class="icommentjs kaButton smallButton rightButton" href="/my-wifes-lover-2015-720p-uncut-hdrip-h264-avi-rehd-movietam-t11957026.html#comment">13 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/my-wifes-lover-2015-720p-uncut-hdrip-h264-avi-rehd-movietam-t11957026.html" class="cellMainLink">My Wifes Lover 2015 720p Uncut HDRip H264 avi-ReHD [MovietaM]</a></div>
			</td>
			<td class="nobr center" data-sort="3592818788">3.35 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T10:51:07+00:00">24 Jan 2016, 10:51:07</span></td>
			<td class="green center">3241</td>
			<td class="red lasttd center">2859</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11957922,0" class="icommentjs kaButton smallButton rightButton" href="/airlift-2016-720p-desiscr-rip-xvid-ac3-5-1-upmix-dus-t11957922.html#comment">19 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/airlift-2016-720p-desiscr-rip-xvid-ac3-5-1-upmix-dus-t11957922.html" class="cellMainLink">AIRLIFT (2016) 720p DesiSCR Rip - XviD AC3 5.1(UpMix) - DUS</a></div>
			</td>
			<td class="nobr center" data-sort="2667971403">2.48 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T14:48:06+00:00">24 Jan 2016, 14:48:06</span></td>
			<td class="green center">1334</td>
			<td class="red lasttd center">4965</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11959365,0" class="icommentjs kaButton smallButton rightButton" href="/ride-along-2-2016-hdcam-x264-zi-t-mp4-t11959365.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ride-along-2-2016-hdcam-x264-zi-t-mp4-t11959365.html" class="cellMainLink">RIDE ALONG 2 2016 HDCAM X264-ZI_T mp4</a></div>
			</td>
			<td class="nobr center" data-sort="665956267">635.11 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T21:24:08+00:00">24 Jan 2016, 21:24:08</span></td>
			<td class="green center">2746</td>
			<td class="red lasttd center">1499</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11956677,0" class="icommentjs kaButton smallButton rightButton" href="/su-su-sudhi-vathmeekam-2015-dvdscr-hq-x264-audio-cleaned-1cd-rip-mp3-700mb-malayalam-1st-on-net-t11956677.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/su-su-sudhi-vathmeekam-2015-dvdscr-hq-x264-audio-cleaned-1cd-rip-mp3-700mb-malayalam-1st-on-net-t11956677.html" class="cellMainLink">Su Su Sudhi Vathmeekam (2015) [DVDScr HQ - x264 - (Audio Cleaned) - 1CD-Rip - Mp3 - 700MB - Malayalam] [1st On Net]</a></div>
			</td>
			<td class="nobr center" data-sort="713031648">680 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T09:29:12+00:00">24 Jan 2016, 09:29:12</span></td>
			<td class="green center">2228</td>
			<td class="red lasttd center">1738</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11952461,0" class="icommentjs kaButton smallButton rightButton" href="/true-story-2015-1080p-bluray-x264-dts-jyk-t11952461.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/true-story-2015-1080p-bluray-x264-dts-jyk-t11952461.html" class="cellMainLink">True Story 2015 1080p BluRay x264 DTS-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="2689453938">2.5 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T13:38:45+00:00">23 Jan 2016, 13:38:45</span></td>
			<td class="green center">1918</td>
			<td class="red lasttd center">1560</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11942429,0" class="icommentjs kaButton smallButton rightButton" href="/straight-outta-compton-2015-theatrical-cut-1080p-bluray-x264-dts-jyk-t11942429.html#comment">21 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/straight-outta-compton-2015-theatrical-cut-1080p-bluray-x264-dts-jyk-t11942429.html" class="cellMainLink">Straight Outta Compton 2015 Theatrical Cut 1080p BluRay x264 DTS-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="3967701978">3.7 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-21T20:54:52+00:00">21 Jan 2016, 20:54:52</span></td>
			<td class="green center">1997</td>
			<td class="red lasttd center">1005</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11954210,0" class="icommentjs kaButton smallButton rightButton" href="/learning-to-drive-2014-french-brrip-xvid-ac3-am84-t11954210.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/learning-to-drive-2014-french-brrip-xvid-ac3-am84-t11954210.html" class="cellMainLink">Learning To Drive 2014 FRENCH BRRip XviD AC3-AM84</a></div>
			</td>
			<td class="nobr center" data-sort="1370331147">1.28 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T21:15:11+00:00">23 Jan 2016, 21:15:11</span></td>
			<td class="green center">2013</td>
			<td class="red lasttd center">778</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11956782,0" class="icommentjs kaButton smallButton rightButton" href="/at-grannys-house-2015-hdrip-xvid-ac3-evo-t11956782.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/at-grannys-house-2015-hdrip-xvid-ac3-evo-t11956782.html" class="cellMainLink">At Grannys House 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1481313217">1.38 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T09:51:33+00:00">24 Jan 2016, 09:51:33</span></td>
			<td class="green center">1352</td>
			<td class="red lasttd center">1647</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11960706,0" class="icommentjs kaButton smallButton rightButton" href="/the-x-files-s10e01-hdtv-x264-killers-ettv-t11960706.html#comment">126 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-x-files-s10e01-hdtv-x264-killers-ettv-t11960706.html" class="cellMainLink">The X-Files S10E01 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="315215482">300.61 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-25T04:28:24+00:00">25 Jan 2016, 04:28:24</span></td>
			<td class="green center">19971</td>
			<td class="red lasttd center">6724</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11943601,0" class="icommentjs kaButton smallButton rightButton" href="/dcs-legends-of-tomorrow-s01e01-hdtv-x264-lol-ettv-t11943601.html#comment">265 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dcs-legends-of-tomorrow-s01e01-hdtv-x264-lol-ettv-t11943601.html" class="cellMainLink">DCs Legends of Tomorrow S01E01 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="354955785">338.51 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T01:59:23+00:00">22 Jan 2016, 01:59:23</span></td>
			<td class="green center">15994</td>
			<td class="red lasttd center">3165</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11960685,0" class="icommentjs kaButton smallButton rightButton" href="/wwe-royal-rumble-2016-ppv-webrip-h264-wd-sparrow-t11960685.html#comment">66 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/wwe-royal-rumble-2016-ppv-webrip-h264-wd-sparrow-t11960685.html" class="cellMainLink">WWE Royal Rumble 2016 PPV WEBRip h264-WD -={SPARROW}=-</a></div>
			</td>
			<td class="nobr center" data-sort="2447462392">2.28 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-25T04:20:34+00:00">25 Jan 2016, 04:20:34</span></td>
			<td class="green center">7949</td>
			<td class="red lasttd center">11976</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11943897,0" class="icommentjs kaButton smallButton rightButton" href="/the-100-s03e01-hdtv-x264-killers-ettv-t11943897.html#comment">110 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-100-s03e01-hdtv-x264-killers-ettv-t11943897.html" class="cellMainLink">The 100 S03E01 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="388456617">370.46 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T03:07:23+00:00">22 Jan 2016, 03:07:23</span></td>
			<td class="green center">8036</td>
			<td class="red lasttd center">1144</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11949075,0" class="icommentjs kaButton smallButton rightButton" href="/stan-lees-lucky-man-s01e01-hdtv-x264-tla-ettv-t11949075.html#comment">86 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/stan-lees-lucky-man-s01e01-hdtv-x264-tla-ettv-t11949075.html" class="cellMainLink">Stan Lees Lucky Man S01E01 HDTV x264-TLA[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="338330090">322.66 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T22:15:18+00:00">22 Jan 2016, 22:15:18</span></td>
			<td class="green center">7238</td>
			<td class="red lasttd center">1373</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11960387,0" class="icommentjs kaButton smallButton rightButton" href="/shameless-us-s06e03-hdtv-x264-lol-ettv-t11960387.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/shameless-us-s06e03-hdtv-x264-lol-ettv-t11960387.html" class="cellMainLink">Shameless US S06E03 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="390907705">372.8 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-25T02:55:23+00:00">25 Jan 2016, 02:55:23</span></td>
			<td class="green center">5984</td>
			<td class="red lasttd center">2577</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11943620,0" class="icommentjs kaButton smallButton rightButton" href="/heroes-reborn-s01e13-hdtv-x264-killers-ettv-t11943620.html#comment">78 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/heroes-reborn-s01e13-hdtv-x264-killers-ettv-t11943620.html" class="cellMainLink">Heroes Reborn S01E13 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="422420484">402.85 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T02:05:24+00:00">22 Jan 2016, 02:05:24</span></td>
			<td class="green center">6166</td>
			<td class="red lasttd center">1092</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11951093,0" class="icommentjs kaButton smallButton rightButton" href="/black-sails-s03e01-hdtv-x264-fleet-rartv-t11951093.html#comment">75 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/black-sails-s03e01-hdtv-x264-fleet-rartv-t11951093.html" class="cellMainLink">Black Sails S03E01 HDTV x264-FLEET[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="406641664">387.8 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T07:28:39+00:00">23 Jan 2016, 07:28:39</span></td>
			<td class="green center">5118</td>
			<td class="red lasttd center">800</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11944165,0" class="icommentjs kaButton smallButton rightButton" href="/colony-s01e02-internal-hdtv-x264-killers-ettv-t11944165.html#comment">70 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/colony-s01e02-internal-hdtv-x264-killers-ettv-t11944165.html" class="cellMainLink">Colony S01E02 INTERNAL HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="307818688">293.56 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T04:16:25+00:00">22 Jan 2016, 04:16:25</span></td>
			<td class="green center">4908</td>
			<td class="red lasttd center">613</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11959100,0" class="icommentjs kaButton smallButton rightButton" href="/beowulf-return-to-the-shieldlands-s01e04-hdtv-x264-organic-ettv-t11959100.html#comment">20 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/beowulf-return-to-the-shieldlands-s01e04-hdtv-x264-organic-ettv-t11959100.html" class="cellMainLink">Beowulf Return to the Shieldlands S01E04 HDTV x264-ORGANiC[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="463677558">442.2 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T20:04:24+00:00">24 Jan 2016, 20:04:24</span></td>
			<td class="green center">3869</td>
			<td class="red lasttd center">1762</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11960638,0" class="icommentjs kaButton smallButton rightButton" href="/keeping-up-with-the-kardashians-s11e10-miscommunication-hdtv-megatv-t11960638.html#comment">24 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/keeping-up-with-the-kardashians-s11e10-miscommunication-hdtv-megatv-t11960638.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/keeping-up-with-the-kardashians-s11e10-miscommunication-hdtv-megatv-t11960638.html" class="cellMainLink">Keeping Up With The Kardashians S11E10 Miscommunication HDTV-MegaTV</a></div>
			</td>
			<td class="nobr center" data-sort="489130065">466.47 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-25T04:04:52+00:00">25 Jan 2016, 04:04:52</span></td>
			<td class="green center">3915</td>
			<td class="red lasttd center">912</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11956263,0" class="icommentjs kaButton smallButton rightButton" href="/saturday-night-live-s41e11-ronda-rousey-selena-gomez-hdtv-x264-crooks-ettv-t11956263.html#comment">23 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/saturday-night-live-s41e11-ronda-rousey-selena-gomez-hdtv-x264-crooks-ettv-t11956263.html" class="cellMainLink">Saturday Night Live S41E11 Ronda Rousey-Selena Gomez HDTV x264-CROOKS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="480439302">458.18 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T07:50:21+00:00">24 Jan 2016, 07:50:21</span></td>
			<td class="green center">2939</td>
			<td class="red lasttd center">556</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11959536,0" class="icommentjs kaButton smallButton rightButton" href="/war-and-peace-2016-s01e04-hdtv-x264-tla-ettv-t11959536.html#comment">17 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/war-and-peace-2016-s01e04-hdtv-x264-tla-ettv-t11959536.html" class="cellMainLink">War And Peace 2016 S01E04 HDTV x264-TLA[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="380401416">362.78 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T22:27:33+00:00">24 Jan 2016, 22:27:33</span></td>
			<td class="green center">2454</td>
			<td class="red lasttd center">1080</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11944268,0" class="icommentjs kaButton smallButton rightButton" href="/shades-of-blue-s01e03-hdtv-x264-fum-ettv-t11944268.html#comment">23 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/shades-of-blue-s01e03-hdtv-x264-fum-ettv-t11944268.html" class="cellMainLink">Shades of Blue S01E03 HDTV x264-FUM[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="245748781">234.36 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T04:44:32+00:00">22 Jan 2016, 04:44:32</span></td>
			<td class="green center">2535</td>
			<td class="red lasttd center">403</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11950066,0" class="icommentjs kaButton smallButton rightButton" href="/hawaii-five-0-2010-s06e13-hdtv-x264-lol-ettv-t11950066.html#comment">22 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/hawaii-five-0-2010-s06e13-hdtv-x264-lol-ettv-t11950066.html" class="cellMainLink">Hawaii Five-0 2010 S06E13 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="418577420">399.19 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T03:02:23+00:00">23 Jan 2016, 03:02:23</span></td>
			<td class="green center">1887</td>
			<td class="red lasttd center">272</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11957347,0" class="icommentjs kaButton smallButton rightButton" href="/black-sabbath-the-end-2016-ak-t11957347.html#comment">17 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/black-sabbath-the-end-2016-ak-t11957347.html" class="cellMainLink">Black Sabbath â The End (2016) ak</a></div>
			</td>
			<td class="nobr center" data-sort="133501542">127.32 <span>MB</span></td>
			<td class="center">9</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T12:30:20+00:00">24 Jan 2016, 12:30:20</span></td>
			<td class="green center">644</td>
			<td class="red lasttd center">88</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11955851,0" class="icommentjs kaButton smallButton rightButton" href="/the-eagles-edgy-and-heavy-deluxe-3-cd-edition-2016-320ak-t11955851.html#comment">13 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-eagles-edgy-and-heavy-deluxe-3-cd-edition-2016-320ak-t11955851.html" class="cellMainLink">The Eagles - Edgy And Heavy (Deluxe 3-CD Edition) 2016 320ak</a></div>
			</td>
			<td class="nobr center" data-sort="426246774">406.5 <span>MB</span></td>
			<td class="center">48</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T06:09:15+00:00">24 Jan 2016, 06:09:15</span></td>
			<td class="green center">563</td>
			<td class="red lasttd center">189</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11946584,0" class="icommentjs kaButton smallButton rightButton" href="/megadeth-dystopia-deluxe-edition-2016-320ak-t11946584.html#comment">29 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/megadeth-dystopia-deluxe-edition-2016-320ak-t11946584.html" class="cellMainLink">Megadeth - Dystopia (Deluxe Edition) (2016)320ak</a></div>
			</td>
			<td class="nobr center" data-sort="132913161">126.76 <span>MB</span></td>
			<td class="center">14</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T13:12:27+00:00">22 Jan 2016, 13:12:27</span></td>
			<td class="green center">577</td>
			<td class="red lasttd center">66</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11947804,0" class="icommentjs kaButton smallButton rightButton" href="/va-beatport-deep-house-great-mix-2016-mp3-320-kbps-t11947804.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-beatport-deep-house-great-mix-2016-mp3-320-kbps-t11947804.html" class="cellMainLink">VA - Beatport Deep House Great Mix (2016) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="1669239031">1.55 <span>GB</span></td>
			<td class="center">101</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T17:28:30+00:00">22 Jan 2016, 17:28:30</span></td>
			<td class="green center">272</td>
			<td class="red lasttd center">230</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11957084,0" class="icommentjs kaButton smallButton rightButton" href="/various-artists-original-hits-80s-12-72-original-hits-6cd-box-set-2016-collection-mp3-edm-rg-t11957084.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/various-artists-original-hits-80s-12-72-original-hits-6cd-box-set-2016-collection-mp3-edm-rg-t11957084.html" class="cellMainLink">Various Artists - Original Hits - 80s 12&quot; / 72 Original Hits [6CD Box Set] 2016 Collection @ MP3 [EDM RG]</a></div>
			</td>
			<td class="nobr center" data-sort="1025314509">977.82 <span>MB</span></td>
			<td class="center">86</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T11:07:50+00:00">24 Jan 2016, 11:07:50</span></td>
			<td class="green center">220</td>
			<td class="red lasttd center">120</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11957703,0" class="icommentjs kaButton smallButton rightButton" href="/paul-gilbert-i-can-destroy-2016-320ak-t11957703.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/paul-gilbert-i-can-destroy-2016-320ak-t11957703.html" class="cellMainLink">Paul Gilbert - I Can Destroy 2016 320ak</a></div>
			</td>
			<td class="nobr center" data-sort="140020314">133.53 <span>MB</span></td>
			<td class="center">14</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T13:56:30+00:00">24 Jan 2016, 13:56:30</span></td>
			<td class="green center">229</td>
			<td class="red lasttd center">42</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11947858,0" class="icommentjs kaButton smallButton rightButton" href="/va-reflective-deep-house-mix-2016-mp3-320-kbps-t11947858.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-reflective-deep-house-mix-2016-mp3-320-kbps-t11947858.html" class="cellMainLink">VA - Reflective Deep House Mix (2016) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="1655976119">1.54 <span>GB</span></td>
			<td class="center">131</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T17:37:13+00:00">22 Jan 2016, 17:37:13</span></td>
			<td class="green center">160</td>
			<td class="red lasttd center">140</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11958635,0" class="icommentjs kaButton smallButton rightButton" href="/va-urban-dance-vol-15-3cd-2016-mp3-262-265-kbps-t11958635.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-urban-dance-vol-15-3cd-2016-mp3-262-265-kbps-t11958635.html" class="cellMainLink">VA - Urban Dance Vol.15 [3CD] (2016) MP3 [262 ~ 265 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="402521189">383.87 <span>MB</span></td>
			<td class="center">72</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T17:50:35+00:00">24 Jan 2016, 17:50:35</span></td>
			<td class="green center">144</td>
			<td class="red lasttd center">106</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11956178,0" class="icommentjs kaButton smallButton rightButton" href="/charlie-puth-nine-track-mind-pop-2016-mtj-320-t11956178.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/charlie-puth-nine-track-mind-pop-2016-mtj-320-t11956178.html" class="cellMainLink">Charlie Puth - Nine Track Mind [Pop 2016 MTJ 320]</a></div>
			</td>
			<td class="nobr center" data-sort="108415948">103.39 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T07:19:55+00:00">24 Jan 2016, 07:19:55</span></td>
			<td class="green center">135</td>
			<td class="red lasttd center">116</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11953533,0" class="icommentjs kaButton smallButton rightButton" href="/carol-carter-burwell-ost-2015-t11953533.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/carol-carter-burwell-ost-2015-t11953533.html" class="cellMainLink">Carol - Carter Burwell (OST) (2015)</a></div>
			</td>
			<td class="nobr center" data-sort="121176908">115.56 <span>MB</span></td>
			<td class="center">25</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T18:03:22+00:00">23 Jan 2016, 18:03:22</span></td>
			<td class="green center">157</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/ken-y-the-king-of-romance-2016-320-kbps-t11957000.html" class="cellMainLink">Ken-Y â The King of Romance (2016) 320 KBPS</a></div>
			</td>
			<td class="nobr center" data-sort="126034195">120.2 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T10:45:50+00:00">24 Jan 2016, 10:45:50</span></td>
			<td class="green center">124</td>
			<td class="red lasttd center">27</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11953600,0" class="icommentjs kaButton smallButton rightButton" href="/va-house-clubhits-megamix-vol-6-3cd-2016-mp3-254-261-kbps-t11953600.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-house-clubhits-megamix-vol-6-3cd-2016-mp3-254-261-kbps-t11953600.html" class="cellMainLink">VA - House Clubhits Megamix Vol.6 [3CD] (2016) MP3 [254 ~ 261 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="453343328">432.34 <span>MB</span></td>
			<td class="center">12</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T18:21:23+00:00">23 Jan 2016, 18:21:23</span></td>
			<td class="green center">105</td>
			<td class="red lasttd center">64</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11946854,0" class="icommentjs kaButton smallButton rightButton" href="/borknagar-winter-thrice-2016-320ak-t11946854.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/borknagar-winter-thrice-2016-320ak-t11946854.html" class="cellMainLink">Borknagar - Winter Thrice (2016)320ak</a></div>
			</td>
			<td class="nobr center" data-sort="120211438">114.64 <span>MB</span></td>
			<td class="center">9</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T14:08:54+00:00">22 Jan 2016, 14:08:54</span></td>
			<td class="green center">115</td>
			<td class="red lasttd center">9</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11960100,0" class="icommentjs kaButton smallButton rightButton" href="/massive-attack-2016-ritual-spirit-ep-t11960100.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/massive-attack-2016-ritual-spirit-ep-t11960100.html" class="cellMainLink">Massive Attack - 2016 - Ritual Spirit EP</a></div>
			</td>
			<td class="nobr center" data-sort="44296098">42.24 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-25T01:07:25+00:00">25 Jan 2016, 01:07:25</span></td>
			<td class="green center">81</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11962996,0" class="icommentjs kaButton smallButton rightButton" href="/afrojack-hardwell-hollywood-extended-mix-mp3-edm-rg-t11962996.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/afrojack-hardwell-hollywood-extended-mix-mp3-edm-rg-t11962996.html" class="cellMainLink">Afrojack &amp; Hardwell - Hollywood (Extended Mix).mp3 [EDM RG]</a></div>
			</td>
			<td class="nobr center" data-sort="8608085">8.21 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-25T13:57:55+00:00">25 Jan 2016, 13:57:55</span></td>
			<td class="green center">68</td>
			<td class="red lasttd center">30</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11949416,0" class="icommentjs kaButton smallButton rightButton" href="/scrap-mechanic-v0-1-13-t11949416.html#comment">15 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/scrap-mechanic-v0-1-13-t11949416.html" class="cellMainLink">Scrap Mechanic v0.1.13</a></div>
			</td>
			<td class="nobr center" data-sort="326702483">311.57 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T00:05:49+00:00">23 Jan 2016, 00:05:49</span></td>
			<td class="green center">1326</td>
			<td class="red lasttd center">85</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11948401,0" class="icommentjs kaButton smallButton rightButton" href="/lego-marvels-avengers-xbox360-protocol-t11948401.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/lego-marvels-avengers-xbox360-protocol-t11948401.html" class="cellMainLink">Lego Marvels Avengers XBOX360-PROTOCOL</a></div>
			</td>
			<td class="nobr center" data-sort="8738849736">8.14 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T19:24:29+00:00">22 Jan 2016, 19:24:29</span></td>
			<td class="green center">305</td>
			<td class="red lasttd center">596</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11958876,0" class="icommentjs kaButton smallButton rightButton" href="/pro-evolution-soccer-2016-r-g-mechanics-t11958876.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/pro-evolution-soccer-2016-r-g-mechanics-t11958876.html" class="cellMainLink">Pro Evolution Soccer 2016 [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center" data-sort="3561650105">3.32 <span>GB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T19:08:37+00:00">24 Jan 2016, 19:08:37</span></td>
			<td class="green center">242</td>
			<td class="red lasttd center">220</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11948565,0" class="icommentjs kaButton smallButton rightButton" href="/fnaf-world-five-nights-at-freddy-s-world-v1-023-t11948565.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/fnaf-world-five-nights-at-freddy-s-world-v1-023-t11948565.html" class="cellMainLink">FNaF World ( Five Nights at Freddy&#039;s World ) v1.023</a></div>
			</td>
			<td class="nobr center" data-sort="241878827">230.67 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T20:01:08+00:00">22 Jan 2016, 20:01:08</span></td>
			<td class="green center">343</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11945331,0" class="icommentjs kaButton smallButton rightButton" href="/tom-clancys-the-division-beta-3dm-t11945331.html#comment">25 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/tom-clancys-the-division-beta-3dm-t11945331.html" class="cellMainLink">Tom Clancys The Division Beta-3DM</a></div>
			</td>
			<td class="nobr center" data-sort="29848234263">27.8 <span>GB</span></td>
			<td class="center">32</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T08:25:20+00:00">22 Jan 2016, 08:25:20</span></td>
			<td class="green center">87</td>
			<td class="red lasttd center">412</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11951624,0" class="icommentjs kaButton smallButton rightButton" href="/slime-rancher-v0-2-4b-t11951624.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/slime-rancher-v0-2-4b-t11951624.html" class="cellMainLink">Slime Rancher v0.2.4b</a></div>
			</td>
			<td class="nobr center" data-sort="142296418">135.7 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T09:44:50+00:00">23 Jan 2016, 09:44:50</span></td>
			<td class="green center">238</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11948575,0" class="icommentjs kaButton smallButton rightButton" href="/darkest-dungeon-patch-2-1-1-5-gog-t11948575.html#comment">22 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/darkest-dungeon-patch-2-1-1-5-gog-t11948575.html" class="cellMainLink">Darkest Dungeon (Patch 2.1.1.5) (GOG)</a></div>
			</td>
			<td class="nobr center" data-sort="4125890">3.93 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T20:04:24+00:00">22 Jan 2016, 20:04:24</span></td>
			<td class="green center">201</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11958897,0" class="icommentjs kaButton smallButton rightButton" href="/payday-the-heist-complete-edition-2011-pc-repack-by-mizantrop1337-t11958897.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/payday-the-heist-complete-edition-2011-pc-repack-by-mizantrop1337-t11958897.html" class="cellMainLink">PayDay: The Heist - Complete Edition (2011) PC | RePack by Mizantrop1337</a></div>
			</td>
			<td class="nobr center" data-sort="1719695455">1.6 <span>GB</span></td>
			<td class="center">24</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T19:12:16+00:00">24 Jan 2016, 19:12:16</span></td>
			<td class="green center">137</td>
			<td class="red lasttd center">81</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11954277,0" class="icommentjs kaButton smallButton rightButton" href="/subterrain-2016-1-0-0-4-steamrip-t11954277.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/subterrain-2016-1-0-0-4-steamrip-t11954277.html" class="cellMainLink">Subterrain 2016 1.0.0.4 SteamRip</a></div>
			</td>
			<td class="nobr center" data-sort="352734976">336.39 <span>MB</span></td>
			<td class="center">7</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T21:32:53+00:00">23 Jan 2016, 21:32:53</span></td>
			<td class="green center">172</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11951423,0" class="icommentjs kaButton smallButton rightButton" href="/fifa-16-ultimate-team-v3-2-113645-mod-patched-apk-obb-xpoz-t11951423.html#comment">36 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/fifa-16-ultimate-team-v3-2-113645-mod-patched-apk-obb-xpoz-t11951423.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/fifa-16-ultimate-team-v3-2-113645-mod-patched-apk-obb-xpoz-t11951423.html" class="cellMainLink">FIFA 16 Ultimate Team v3.2.113645 Mod (Patched) [Apk+Obb]-XpoZ</a></div>
			</td>
			<td class="nobr center" data-sort="1437239852">1.34 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T09:00:18+00:00">23 Jan 2016, 09:00:18</span></td>
			<td class="green center">93</td>
			<td class="red lasttd center">143</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/hard-west-r-g-mechanics-t11958889.html" class="cellMainLink">Hard West [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center" data-sort="2539402405">2.37 <span>GB</span></td>
			<td class="center">16</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T19:11:21+00:00">24 Jan 2016, 19:11:21</span></td>
			<td class="green center">117</td>
			<td class="red lasttd center">70</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11953295,0" class="icommentjs kaButton smallButton rightButton" href="/subnautica-build-3423-t11953295.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/subnautica-build-3423-t11953295.html" class="cellMainLink">Subnautica Build 3423</a></div>
			</td>
			<td class="nobr center" data-sort="2532322472">2.36 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T17:04:15+00:00">23 Jan 2016, 17:04:15</span></td>
			<td class="green center">106</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11958872,0" class="icommentjs kaButton smallButton rightButton" href="/hand-of-fate-r-g-mechanics-t11958872.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/hand-of-fate-r-g-mechanics-t11958872.html" class="cellMainLink">Hand of Fate [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center" data-sort="1220561876">1.14 <span>GB</span></td>
			<td class="center">12</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T19:08:13+00:00">24 Jan 2016, 19:08:13</span></td>
			<td class="green center">87</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/dragon-s-dogma-dark-arisen-black-box-t11962883.html" class="cellMainLink">Dragonâs Dogma Dark Arisen - Black Box</a></div>
			</td>
			<td class="nobr center" data-sort="6357492661">5.92 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-25T13:32:20+00:00">25 Jan 2016, 13:32:20</span></td>
			<td class="green center">31</td>
			<td class="red lasttd center">100</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/universe-sandbox-Â²-alpha-18-2-t11951425.html" class="cellMainLink">Universe Sandbox Â² Alpha 18.2</a></div>
			</td>
			<td class="nobr center" data-sort="594664373">567.12 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T09:00:26+00:00">23 Jan 2016, 09:00:26</span></td>
			<td class="green center">77</td>
			<td class="red lasttd center">3</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11945842,0" class="icommentjs kaButton smallButton rightButton" href="/adobe-flash-player-20-0-0-286-full-offline-installer-s0ft4pc-t11945842.html#comment">16 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/adobe-flash-player-20-0-0-286-full-offline-installer-s0ft4pc-t11945842.html" class="cellMainLink">Adobe Flash Player 20.0.0.286 Full Offline Installer [S0ft4PC]</a></div>
			</td>
			<td class="nobr center" data-sort="55849691">53.26 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T10:22:56+00:00">22 Jan 2016, 10:22:56</span></td>
			<td class="green center">254</td>
			<td class="red lasttd center">32</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11952782,0" class="icommentjs kaButton smallButton rightButton" href="/any-video-converter-ultimate-5-8-8-multilingual-serial-key-dtw-t11952782.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/any-video-converter-ultimate-5-8-8-multilingual-serial-key-dtw-t11952782.html" class="cellMainLink">Any Video Converter Ultimate 5.8.8 Multilingual +Serial Key [DTW]</a></div>
			</td>
			<td class="nobr center" data-sort="38556964">36.77 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T15:03:17+00:00">23 Jan 2016, 15:03:17</span></td>
			<td class="green center">237</td>
			<td class="red lasttd center">19</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11950307,0" class="icommentjs kaButton smallButton rightButton" href="/pc-cleaner-pro-2016-14-0-16-1-11-keys-4realtorrentz-t11950307.html#comment">22 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/pc-cleaner-pro-2016-14-0-16-1-11-keys-4realtorrentz-t11950307.html" class="cellMainLink">PC Cleaner Pro 2016 14.0.16.1.11 + Keys [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="5172929">4.93 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T04:10:49+00:00">23 Jan 2016, 04:10:49</span></td>
			<td class="green center">227</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11946278,0" class="icommentjs kaButton smallButton rightButton" href="/bs-player-pro-2-70-build-1080-final-multilingual-incl-serials-t11946278.html#comment">19 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/bs-player-pro-2-70-build-1080-final-multilingual-incl-serials-t11946278.html" class="cellMainLink">BS.Player Pro 2.70 Build 1080 Final Multilingual - Incl.Serials</a></div>
			</td>
			<td class="nobr center" data-sort="10973843">10.47 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T11:58:58+00:00">22 Jan 2016, 11:58:58</span></td>
			<td class="green center">156</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11957888,0" class="icommentjs kaButton smallButton rightButton" href="/vso-convertxtodvd-6-0-0-20-final-patch-s0ft4pc-t11957888.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/vso-convertxtodvd-6-0-0-20-final-patch-s0ft4pc-t11957888.html" class="cellMainLink">VSO ConvertXtoDVD 6.0.0.20 Final + Patch [S0ft4PC]</a></div>
			</td>
			<td class="nobr center" data-sort="52160854">49.74 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T14:41:27+00:00">24 Jan 2016, 14:41:27</span></td>
			<td class="green center">151</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11943778,0" class="icommentjs kaButton smallButton rightButton" href="/intuit-turbo-tax-2015-usa-home-and-business-all-states-t11943778.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/intuit-turbo-tax-2015-usa-home-and-business-all-states-t11943778.html" class="cellMainLink">Intuit Turbo Tax 2015 USA Home and Business + ALL States</a></div>
			</td>
			<td class="nobr center" data-sort="473855210">451.9 <span>MB</span></td>
			<td class="center">48</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T02:43:29+00:00">22 Jan 2016, 02:43:29</span></td>
			<td class="green center">138</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11955003,0" class="icommentjs kaButton smallButton rightButton" href="/tenorshare-windows-boot-genius-3-0-0-1-syed-t11955003.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/tenorshare-windows-boot-genius-3-0-0-1-syed-t11955003.html" class="cellMainLink">Tenorshare Windows Boot Genius 3.0.0.1 - [SyED]</a></div>
			</td>
			<td class="nobr center" data-sort="266127572">253.8 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T01:19:29+00:00">24 Jan 2016, 01:19:29</span></td>
			<td class="green center">128</td>
			<td class="red lasttd center">34</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11958236,0" class="icommentjs kaButton smallButton rightButton" href="/windows-10-multiple-editions-x64-multi-6-10586-oem-esd-jan-2016-pre-activated-t11958236.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-10-multiple-editions-x64-multi-6-10586-oem-esd-jan-2016-pre-activated-t11958236.html" class="cellMainLink">Windows 10 Multiple Editions (x64) MULTi 6 10586 OEM ESD Jan 2016-Pre Activated~</a></div>
			</td>
			<td class="nobr center" data-sort="4120396796">3.84 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T16:12:22+00:00">24 Jan 2016, 16:12:22</span></td>
			<td class="green center">54</td>
			<td class="red lasttd center">149</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11954801,0" class="icommentjs kaButton smallButton rightButton" href="/microsoft-office-2016-15-18-selectable-download-mac-os-x-mac599-t11954801.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/microsoft-office-2016-15-18-selectable-download-mac-os-x-mac599-t11954801.html" class="cellMainLink">Microsoft Office 2016 15.18 (Selectable Download) [Mac Os X] [MAC599]</a></div>
			</td>
			<td class="nobr center" data-sort="4771914633">4.44 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T00:03:04+00:00">24 Jan 2016, 00:03:04</span></td>
			<td class="green center">67</td>
			<td class="red lasttd center">101</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11957763,0" class="icommentjs kaButton smallButton rightButton" href="/serato-dj-1-8-1-multilingual-king-jena-t11957763.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/serato-dj-1-8-1-multilingual-king-jena-t11957763.html" class="cellMainLink">Serato DJ 1.8.1 Multilingual [King Jena]</a></div>
			</td>
			<td class="nobr center" data-sort="239499842">228.4 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T14:12:33+00:00">24 Jan 2016, 14:12:33</span></td>
			<td class="green center">86</td>
			<td class="red lasttd center">31</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/cpu-x-system-hardware-info-v1-40-ctrc-t11956423.html" class="cellMainLink">CPU X System &amp; Hardware Info v1.40 (CTRC)</a></div>
			</td>
			<td class="nobr center" data-sort="5014587">4.78 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T08:29:36+00:00">24 Jan 2016, 08:29:36</span></td>
			<td class="green center">60</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11961908,0" class="icommentjs kaButton smallButton rightButton" href="/daz3d-lynsey-for-genesis-3-female-teeth-fix-t11961908.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/daz3d-lynsey-for-genesis-3-female-teeth-fix-t11961908.html" class="cellMainLink">DAZ3D Lynsey for Genesis 3 Female (Teeth Fix)</a></div>
			</td>
			<td class="nobr center" data-sort="175961701">167.81 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-25T09:22:44+00:00">25 Jan 2016, 09:22:44</span></td>
			<td class="green center">59</td>
			<td class="red lasttd center">9</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11942274,0" class="icommentjs kaButton smallButton rightButton" href="/adobe-illustrator-cc-2015-v19-2-1-147-1-x86x64-incl-crack-update-sadeempc-t11942274.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/adobe-illustrator-cc-2015-v19-2-1-147-1-x86x64-incl-crack-update-sadeempc-t11942274.html" class="cellMainLink">Adobe Illustrator CC 2015 v19.2.1.147.1 (x86x64) Incl Crack &amp; Update [SadeemPC]</a></div>
			</td>
			<td class="nobr center" data-sort="5147096812">4.79 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-21T20:05:27+00:00">21 Jan 2016, 20:05:27</span></td>
			<td class="green center">27</td>
			<td class="red lasttd center">43</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11945933,0" class="icommentjs kaButton smallButton rightButton" href="/windows-7-sp1-aio-x32-x64-en-us-update-jan2016-seven7i-t11945933.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-7-sp1-aio-x32-x64-en-us-update-jan2016-seven7i-t11945933.html" class="cellMainLink">Windows 7 SP1 AIO x32 x64 en-US Update Jan2016 Seven7i</a></div>
			</td>
			<td class="nobr center" data-sort="8357093948">7.78 <span>GB</span></td>
			<td class="center">9</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T10:40:29+00:00">22 Jan 2016, 10:40:29</span></td>
			<td class="green center">17</td>
			<td class="red lasttd center">46</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11953977,0" class="icommentjs kaButton smallButton rightButton" href="/fl-studio-12-1-3-mac-os-x-t11953977.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/fl-studio-12-1-3-mac-os-x-t11953977.html" class="cellMainLink">FL Studio 12.1.3 - Mac OS X</a></div>
			</td>
			<td class="nobr center" data-sort="1120336013">1.04 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T19:54:40+00:00">23 Jan 2016, 19:54:40</span></td>
			<td class="green center">33</td>
			<td class="red lasttd center">8</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11958365,0" class="icommentjs kaButton smallButton rightButton" href="/horriblesubs-hai-to-gensou-no-grimgar-03-720p-mkv-t11958365.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-hai-to-gensou-no-grimgar-03-720p-mkv-t11958365.html" class="cellMainLink">[HorribleSubs] Hai to Gensou no Grimgar - 03 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="456474343">435.33 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T16:40:02+00:00">24 Jan 2016, 16:40:02</span></td>
			<td class="green center">2052</td>
			<td class="red lasttd center">256</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dragon-ball-super-028-french-subbed-mp4-t11958164.html" class="cellMainLink">Dragon Ball Super.028.[FRENCH SUBBED].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="278089076">265.21 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T15:51:58+00:00">24 Jan 2016, 15:51:58</span></td>
			<td class="green center">1802</td>
			<td class="red lasttd center">202</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-dimension-w-03-raw-kbs-1280x720-x264-aac-mp4-t11958451.html" class="cellMainLink">[Leopard-Raws] Dimension W - 03 RAW (KBS 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="406418659">387.59 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T17:05:02+00:00">24 Jan 2016, 17:05:02</span></td>
			<td class="green center">1477</td>
			<td class="red lasttd center">363</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/kaerizaki-fansub-one-piece-727-french-subbed-hd-1280x720-mp4-t11958163.html" class="cellMainLink">[Kaerizaki-Fansub] One Piece 727 [FRENCH SUBBED][HD_1280x720].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="294172839">280.55 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T15:51:48+00:00">24 Jan 2016, 15:51:48</span></td>
			<td class="green center">1280</td>
			<td class="red lasttd center">183</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11945296,0" class="icommentjs kaButton smallButton rightButton" href="/fansub-resistance-naruto-shippuuden-445-french-subbed-1280x720-mp4-t11945296.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fansub-resistance-naruto-shippuuden-445-french-subbed-1280x720-mp4-t11945296.html" class="cellMainLink">[Fansub-Resistance]Naruto Shippuuden 445 [FRENCH SUBBED] (1280x720).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="207798696">198.17 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T08:17:30+00:00">22 Jan 2016, 08:17:30</span></td>
			<td class="green center">941</td>
			<td class="red lasttd center">78</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11957126,0" class="icommentjs kaButton smallButton rightButton" href="/arrg-dragon-ball-super-28-english-subbed-720p-sehjada-t11957126.html#comment">25 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/arrg-dragon-ball-super-28-english-subbed-720p-sehjada-t11957126.html" class="cellMainLink">[ARRG] Dragon Ball Super - 28 English Subbed [720P] (Sehjada)</a></div>
			</td>
			<td class="nobr center" data-sort="114159610">108.87 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T11:22:01+00:00">24 Jan 2016, 11:22:01</span></td>
			<td class="green center">754</td>
			<td class="red lasttd center">109</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11958689,0" class="icommentjs kaButton smallButton rightButton" href="/commie-koyomimonogatari-01-53648bdd-mkv-t11958689.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/commie-koyomimonogatari-01-53648bdd-mkv-t11958689.html" class="cellMainLink">[Commie] Koyomimonogatari - 01 [53648BDD].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="74054257">70.62 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T18:10:02+00:00">24 Jan 2016, 18:10:02</span></td>
			<td class="green center">255</td>
			<td class="red lasttd center">9</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11953882,0" class="icommentjs kaButton smallButton rightButton" href="/deadfish-ajin-2016-02-720p-aac-mp4-t11953882.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/deadfish-ajin-2016-02-720p-aac-mp4-t11953882.html" class="cellMainLink">[DeadFish] Ajin (2016) - 02 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="476096585">454.04 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T19:29:06+00:00">23 Jan 2016, 19:29:06</span></td>
			<td class="green center">223</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fff-saijaku-muhai-no-bahamut-02-e0661c31-mkv-t11943970.html" class="cellMainLink">[FFF] Saijaku Muhai no Bahamut - 02 [E0661C31].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="402588841">383.94 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T03:25:02+00:00">22 Jan 2016, 03:25:02</span></td>
			<td class="green center">106</td>
			<td class="red lasttd center">4</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11955277,0" class="icommentjs kaButton smallButton rightButton" href="/animerg-one-piece-727-480p-kami-mkv-t11955277.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-one-piece-727-480p-kami-mkv-t11955277.html" class="cellMainLink">[AnimeRG] One Piece - 727 [480p] [KaMi].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="88649960">84.54 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T03:19:41+00:00">24 Jan 2016, 03:19:41</span></td>
			<td class="green center">97</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/bakedfish-hai-to-gensou-no-grimgar-03-720p-aac-mp4-t11958574.html" class="cellMainLink">[BakedFish] Hai to Gensou no Grimgar - 03 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="381605664">363.93 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T17:36:56+00:00">24 Jan 2016, 17:36:56</span></td>
			<td class="green center">76</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11948282,0" class="icommentjs kaButton smallButton rightButton" href="/bakedfish-gate-jieitai-kanochi-nite-kaku-tatakaeri-enryuu-hen-03-720p-aac-mp4-t11948282.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/bakedfish-gate-jieitai-kanochi-nite-kaku-tatakaeri-enryuu-hen-03-720p-aac-mp4-t11948282.html" class="cellMainLink">[BakedFish] Gate: Jieitai Kanochi nite, Kaku Tatakaeri - Enryuu-hen - 03 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="309235183">294.91 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T19:05:55+00:00">22 Jan 2016, 19:05:55</span></td>
			<td class="green center">76</td>
			<td class="red lasttd center">3</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11955486,0" class="icommentjs kaButton smallButton rightButton" href="/commie-ojisan-to-marshmallow-03-1e391681-mkv-t11955486.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/commie-ojisan-to-marshmallow-03-1e391681-mkv-t11955486.html" class="cellMainLink">[Commie] Ojisan to Marshmallow - 03 [1E391681].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="36146351">34.47 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T04:25:01+00:00">24 Jan 2016, 04:25:01</span></td>
			<td class="green center">76</td>
			<td class="red lasttd center">1</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/bakedfish-mobile-suit-gundam-iron-blooded-orphans-16-720p-aac-mp4-t11957612.html" class="cellMainLink">[BakedFish] Mobile Suit Gundam: Iron-Blooded Orphans - 16 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="527183510">502.76 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T13:35:12+00:00">24 Jan 2016, 13:35:12</span></td>
			<td class="green center">59</td>
			<td class="red lasttd center">20</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11949405,0" class="icommentjs kaButton smallButton rightButton" href="/assorted-magazines-bundle-january-23-2016-true-pdf-t11949405.html#comment">21 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/assorted-magazines-bundle-january-23-2016-true-pdf-t11949405.html" class="cellMainLink">Assorted Magazines Bundle - January 23 2016 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="1189176723">1.11 <span>GB</span></td>
			<td class="center">74</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T00:03:01+00:00">23 Jan 2016, 00:03:01</span></td>
			<td class="green center">429</td>
			<td class="red lasttd center">512</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11949375,0" class="icommentjs kaButton smallButton rightButton" href="/the-ultimate-pc-building-handbook-volume-2-t11949375.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-ultimate-pc-building-handbook-volume-2-t11949375.html" class="cellMainLink">The Ultimate PC Building Handbook Volume 2</a></div>
			</td>
			<td class="nobr center" data-sort="53400518">50.93 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T23:55:46+00:00">22 Jan 2016, 23:55:46</span></td>
			<td class="green center">506</td>
			<td class="red lasttd center">56</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11953194,0" class="icommentjs kaButton smallButton rightButton" href="/i-didn-t-know-my-slow-cooker-could-do-that-150-delicious-surprising-recipes-epub-t11953194.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/i-didn-t-know-my-slow-cooker-could-do-that-150-delicious-surprising-recipes-epub-t11953194.html" class="cellMainLink">I Didn&#039;t Know My Slow Cooker Could Do That: 150 Delicious, Surprising Recipes [epub]</a></div>
			</td>
			<td class="nobr center" data-sort="67938863">64.79 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T16:44:02+00:00">23 Jan 2016, 16:44:02</span></td>
			<td class="green center">446</td>
			<td class="red lasttd center">38</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11945161,0" class="icommentjs kaButton smallButton rightButton" href="/homebuilding-basics-carpentry-by-larry-haun-t11945161.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/homebuilding-basics-carpentry-by-larry-haun-t11945161.html" class="cellMainLink">Homebuilding Basics Carpentry by Larry Haun</a></div>
			</td>
			<td class="nobr center" data-sort="108036170">103.03 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T07:45:14+00:00">22 Jan 2016, 07:45:14</span></td>
			<td class="green center">435</td>
			<td class="red lasttd center">40</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11943026,0" class="icommentjs kaButton smallButton rightButton" href="/windows-10-tips-tricks-apps-volume-1-t11943026.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/windows-10-tips-tricks-apps-volume-1-t11943026.html" class="cellMainLink">Windows 10 Tips, Tricks &amp; Apps Volume 1</a></div>
			</td>
			<td class="nobr center" data-sort="58121891">55.43 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-21T22:59:01+00:00">21 Jan 2016, 22:59:01</span></td>
			<td class="green center">403</td>
			<td class="red lasttd center">45</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11947667,0" class="icommentjs kaButton smallButton rightButton" href="/raspberry-pi-tips-tricks-hacks-volume-1-second-revised-edition-t11947667.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/raspberry-pi-tips-tricks-hacks-volume-1-second-revised-edition-t11947667.html" class="cellMainLink">Raspberry Pi Tips, Tricks &amp; Hacks Volume 1 Second Revised Edition</a></div>
			</td>
			<td class="nobr center" data-sort="41391276">39.47 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T17:02:32+00:00">22 Jan 2016, 17:02:32</span></td>
			<td class="green center">393</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11945163,0" class="icommentjs kaButton smallButton rightButton" href="/how-to-start-a-business-in-2016-t11945163.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/how-to-start-a-business-in-2016-t11945163.html" class="cellMainLink">How To Start A Business in 2016</a></div>
			</td>
			<td class="nobr center" data-sort="2915989">2.78 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T07:45:23+00:00">22 Jan 2016, 07:45:23</span></td>
			<td class="green center">390</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11945191,0" class="icommentjs kaButton smallButton rightButton" href="/simplify-25-simple-habits-of-highly-successful-people-t11945191.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/simplify-25-simple-habits-of-highly-successful-people-t11945191.html" class="cellMainLink">SIMPLIFY 25 Simple Habits of Highly Successful People</a></div>
			</td>
			<td class="nobr center" data-sort="716500">699.71 <span>KB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T07:50:35+00:00">22 Jan 2016, 07:50:35</span></td>
			<td class="green center">375</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11945099,0" class="icommentjs kaButton smallButton rightButton" href="/building-small-projects-new-best-of-fine-woodworking-t11945099.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/building-small-projects-new-best-of-fine-woodworking-t11945099.html" class="cellMainLink">Building Small Projects (New Best of Fine Woodworking)</a></div>
			</td>
			<td class="nobr center" data-sort="25961525">24.76 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T07:34:04+00:00">22 Jan 2016, 07:34:04</span></td>
			<td class="green center">358</td>
			<td class="red lasttd center">13</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11951942,0" class="icommentjs kaButton smallButton rightButton" href="/assholes-a-theory-2012-pdf-epub-mobi-gooner-t11951942.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/assholes-a-theory-2012-pdf-epub-mobi-gooner-t11951942.html" class="cellMainLink">Assholes - A Theory (2012) (Pdf, Epub &amp; Mobi) Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="3313066">3.16 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T11:17:05+00:00">23 Jan 2016, 11:17:05</span></td>
			<td class="green center">307</td>
			<td class="red lasttd center">57</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11955125,0" class="icommentjs kaButton smallButton rightButton" href="/womens-magazines-bundle-january-24-2016-true-pdf-t11955125.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/womens-magazines-bundle-january-24-2016-true-pdf-t11955125.html" class="cellMainLink">Womens Magazines Bundle - January 24 2016 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="620614615">591.86 <span>MB</span></td>
			<td class="center">32</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T02:15:40+00:00">24 Jan 2016, 02:15:40</span></td>
			<td class="green center">227</td>
			<td class="red lasttd center">157</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11955544,0" class="icommentjs kaButton smallButton rightButton" href="/automobile-magazines-january-24-2016-true-pdf-t11955544.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/automobile-magazines-january-24-2016-true-pdf-t11955544.html" class="cellMainLink">Automobile Magazines - January 24 2016 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="550810128">525.29 <span>MB</span></td>
			<td class="center">30</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T04:49:47+00:00">24 Jan 2016, 04:49:47</span></td>
			<td class="green center">232</td>
			<td class="red lasttd center">129</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11949048,0" class="icommentjs kaButton smallButton rightButton" href="/the-economist-23th-january-29th-january-2016-true-pdf-w-cover-no-watermarks-t11949048.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-economist-23th-january-29th-january-2016-true-pdf-w-cover-no-watermarks-t11949048.html" class="cellMainLink">The Economist - 23TH January - 29TH January 2016 (True PDF w/ cover), NO Watermarks)</a></div>
			</td>
			<td class="nobr center" data-sort="16478385">15.72 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T22:07:35+00:00">22 Jan 2016, 22:07:35</span></td>
			<td class="green center">233</td>
			<td class="red lasttd center">52</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11950468,0" class="icommentjs kaButton smallButton rightButton" href="/home-garden-travel-mags-january-23-2016-true-pdf-t11950468.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/home-garden-travel-mags-january-23-2016-true-pdf-t11950468.html" class="cellMainLink">Home Garden &amp; Travel Mags - January 23 2016 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="332920073">317.5 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T04:54:32+00:00">23 Jan 2016, 04:54:32</span></td>
			<td class="green center">192</td>
			<td class="red lasttd center">80</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11950673,0" class="icommentjs kaButton smallButton rightButton" href="/tabloid-magazines-bundle-january-23-2016-true-pdf-t11950673.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/tabloid-magazines-bundle-january-23-2016-true-pdf-t11950673.html" class="cellMainLink">Tabloid Magazines Bundle - January 23 2016 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="266643830">254.29 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T05:46:58+00:00">23 Jan 2016, 05:46:58</span></td>
			<td class="green center">173</td>
			<td class="red lasttd center">68</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11957596,0" class="icommentjs kaButton smallButton rightButton" href="/janis-joplin-i-got-dem-ol-kozmic-blues-again-mama-2016-24-192-hd-flac-t11957596.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/janis-joplin-i-got-dem-ol-kozmic-blues-again-mama-2016-24-192-hd-flac-t11957596.html" class="cellMainLink">Janis Joplin - I Got Dem Ol&#039; Kozmic Blues Again Mama! (2016) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1495023235">1.39 <span>GB</span></td>
			<td class="center">24</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T13:30:48+00:00">24 Jan 2016, 13:30:48</span></td>
			<td class="green center">178</td>
			<td class="red lasttd center">74</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11953688,0" class="icommentjs kaButton smallButton rightButton" href="/various-artists-memories-are-made-of-this-10-cd-1999-boxset-flac-tfm-t11953688.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/various-artists-memories-are-made-of-this-10-cd-1999-boxset-flac-tfm-t11953688.html" class="cellMainLink">Various Artists - Memories Are Made Of This - 10 CD - (1999) - [BoxSet] - [FLAC] - [TFM]</a></div>
			</td>
			<td class="nobr center" data-sort="3038141876">2.83 <span>GB</span></td>
			<td class="center">256</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T18:39:32+00:00">23 Jan 2016, 18:39:32</span></td>
			<td class="green center">156</td>
			<td class="red lasttd center">99</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11952838,0" class="icommentjs kaButton smallButton rightButton" href="/david-bowie-the-rise-and-fall-of-ziggy-stardust-and-the-spiders-from-mars-2015-24-192-hd-flac-t11952838.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/david-bowie-the-rise-and-fall-of-ziggy-stardust-and-the-spiders-from-mars-2015-24-192-hd-flac-t11952838.html" class="cellMainLink">David Bowie - The Rise and Fall of Ziggy Stardust and the Spiders from Mars (2015) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1552263843">1.45 <span>GB</span></td>
			<td class="center">30</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T15:16:02+00:00">23 Jan 2016, 15:16:02</span></td>
			<td class="green center">151</td>
			<td class="red lasttd center">51</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11948838,0" class="icommentjs kaButton smallButton rightButton" href="/megadeth-dystopia-limited-edition-2016-flac-cd-t11948838.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/megadeth-dystopia-limited-edition-2016-flac-cd-t11948838.html" class="cellMainLink">Megadeth - Dystopia LIMITED EDITION [2016] FLAC CD</a></div>
			</td>
			<td class="nobr center" data-sort="419413688">399.98 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T21:07:31+00:00">22 Jan 2016, 21:07:31</span></td>
			<td class="green center">118</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/dire-straits-the-best-by-yeraycito-vinyl-yeraycito-master-series-t11952928.html" class="cellMainLink">Dire Straits - The Best by YERAYCITO (VINYL) YERAYCITO MASTER SERIES</a></div>
			</td>
			<td class="nobr center" data-sort="3017505066">2.81 <span>GB</span></td>
			<td class="center">27</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T15:40:23+00:00">23 Jan 2016, 15:40:23</span></td>
			<td class="green center">95</td>
			<td class="red lasttd center">65</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11958917,0" class="icommentjs kaButton smallButton rightButton" href="/bruce-springsteen-electric-ballroom-atlanta-ga-1975-08-23-sbd-flac-t11958917.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bruce-springsteen-electric-ballroom-atlanta-ga-1975-08-23-sbd-flac-t11958917.html" class="cellMainLink">Bruce Springsteen - Electric Ballroom, Atlanta, GA 1975-08-23 (SBD) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="675346005">644.06 <span>MB</span></td>
			<td class="center">28</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-24T19:16:23+00:00">24 Jan 2016, 19:16:23</span></td>
			<td class="green center">91</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11942816,0" class="icommentjs kaButton smallButton rightButton" href="/jefferson-airplane-bark-expanded-edition-2015-flac-beolab1700-t11942816.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/jefferson-airplane-bark-expanded-edition-2015-flac-beolab1700-t11942816.html" class="cellMainLink">Jefferson Airplane - Bark [Expanded Edition] (2015) FLAC Beolab1700</a></div>
			</td>
			<td class="nobr center" data-sort="384508536">366.7 <span>MB</span></td>
			<td class="center">25</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-21T22:24:40+00:00">21 Jan 2016, 22:24:40</span></td>
			<td class="green center">94</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11945761,0" class="icommentjs kaButton smallButton rightButton" href="/bach-flute-sonatas-viola-da-gamba-sonatas-larrieu-puyana-kuijken-cervera-t11945761.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bach-flute-sonatas-viola-da-gamba-sonatas-larrieu-puyana-kuijken-cervera-t11945761.html" class="cellMainLink">Bach - Flute sonatas, viola da gamba sonatas - Larrieu, Puyana, Kuijken, Cervera</a></div>
			</td>
			<td class="nobr center" data-sort="799678737">762.63 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T10:04:17+00:00">22 Jan 2016, 10:04:17</span></td>
			<td class="green center">91</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11949563,0" class="icommentjs kaButton smallButton rightButton" href="/the-oscar-peterson-trio-like-someone-in-love-flac-tntvillage-t11949563.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-oscar-peterson-trio-like-someone-in-love-flac-tntvillage-t11949563.html" class="cellMainLink">The Oscar Peterson Trio - Like Someone In Love [Flac][TntVillage]</a></div>
			</td>
			<td class="nobr center" data-sort="237886364">226.87 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T00:55:55+00:00">23 Jan 2016, 00:55:55</span></td>
			<td class="green center">88</td>
			<td class="red lasttd center">11</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11947294,0" class="icommentjs kaButton smallButton rightButton" href="/gov-t-mule-2015-stoned-side-of-the-mule-vol-1-2-flac-t11947294.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/gov-t-mule-2015-stoned-side-of-the-mule-vol-1-2-flac-t11947294.html" class="cellMainLink">Govât Mule â 2015 - Stoned Side of the Mule Vol. 1 &amp; 2 [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="540201110">515.18 <span>MB</span></td>
			<td class="center">21</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T15:39:09+00:00">22 Jan 2016, 15:39:09</span></td>
			<td class="green center">69</td>
			<td class="red lasttd center">6</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11952918,0" class="icommentjs kaButton smallButton rightButton" href="/eric-clapton-friends-the-breeze-vinyl-yeraycito-master-series-t11952918.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/eric-clapton-friends-the-breeze-vinyl-yeraycito-master-series-t11952918.html" class="cellMainLink">Eric Clapton &amp; Friends - The Breeze (VINYL) YERAYCITO MASTER SERIES</a></div>
			</td>
			<td class="nobr center" data-sort="1221287267">1.14 <span>GB</span></td>
			<td class="center">19</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-23T15:39:03+00:00">23 Jan 2016, 15:39:03</span></td>
			<td class="green center">55</td>
			<td class="red lasttd center">28</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11948137,0" class="icommentjs kaButton smallButton rightButton" href="/john-martyn-sapphire-2015-remastered-flac-beolab1700-t11948137.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/john-martyn-sapphire-2015-remastered-flac-beolab1700-t11948137.html" class="cellMainLink">John Martyn - Sapphire (2015) Remastered FLAC Beolab1700</a></div>
			</td>
			<td class="nobr center" data-sort="686201045">654.41 <span>MB</span></td>
			<td class="center">28</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T18:29:23+00:00">22 Jan 2016, 18:29:23</span></td>
			<td class="green center">54</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11943404,0" class="icommentjs kaButton smallButton rightButton" href="/tricky-skilled-mechanics-electronic-alternative-triphop-electronica-loungechill-conscious-vox-downtempo-experimental-2016-flac-codetempest-t11943404.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/tricky-skilled-mechanics-electronic-alternative-triphop-electronica-loungechill-conscious-vox-downtempo-experimental-2016-flac-codetempest-t11943404.html" class="cellMainLink">Tricky [Skilled Mechanics] [electronic/alternative/triphop/electronica/loungechill/conscious/vox/downtempo/experimental] [2016] [FLAC] [CodeTempest]</a></div>
			</td>
			<td class="nobr center" data-sort="209416740">199.72 <span>MB</span></td>
			<td class="center">34</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-22T01:06:45+00:00">22 Jan 2016, 01:06:45</span></td>
			<td class="green center">58</td>
			<td class="red lasttd center">3</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11961957,0" class="icommentjs kaButton smallButton rightButton" href="/oscar-peterson-an-oscar-peterson-christmas-flac-tntvillage-t11961957.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/oscar-peterson-an-oscar-peterson-christmas-flac-tntvillage-t11961957.html" class="cellMainLink">Oscar Peterson - An Oscar Peterson Christmas [Flac][TntVillage]</a></div>
			</td>
			<td class="nobr center" data-sort="253157758">241.43 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-25T09:33:17+00:00">25 Jan 2016, 09:33:17</span></td>
			<td class="green center">44</td>
			<td class="red lasttd center">19</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11962872,0" class="icommentjs kaButton smallButton rightButton" href="/charles-mingus-blues-and-roots-2014-24-96-hd-flac-t11962872.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/charles-mingus-blues-and-roots-2014-24-96-hd-flac-t11962872.html" class="cellMainLink">Charles Mingus - Blues and Roots (2014) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="464099611">442.6 <span>MB</span></td>
			<td class="center">19</td>
			<td class="center"><span class="timeago" data-age=1 title="2016-01-25T13:29:44+00:00">25 Jan 2016, 13:29:44</span></td>
			<td class="green center">31</td>
			<td class="red lasttd center">28</td>
        </tr>
			</table>


	<script type="text/javascript">
		if ($.cookie('kat_settings[fpw_column]') != null && $.cookie('kat_settings[fpw_order]')) {
			var colIndex = $.cookie('kat_settings[fpw_column]')
			var ascend = $.cookie('kat_settings[fpw_order]');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			sortTables(colIndex, ascend);
		}
		$('.frontPageWidget .firstr th .fpw-sort').click(function() {
			var colIndex = $(this).parent().index();
			var ascend = !$(this).hasClass('sortedAsc')?1:0;
			$('.fpw-sort').removeClass('sortedAsc sortedDesc');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			updateFPWsettings(colIndex, ascend);
			sortTables(colIndex, ascend);
		});
		function sortTables(colIndex, ascend) {	
			$('.frontPageWidget').each(function() {
				var data = [];
				$('tr:not(.firstr)', $(this)).each(function() {
					var name = $('td:first .cellMainLink', $(this)).text();
					var id = $('td:first .cellMainLink', $(this)).attr('href').match(/.*-t(\d+)\.html/)[1];
					var fileSize = $('td:eq(1)', $(this)).attr('data-sort');
					data.push({
						'col0':name, 'col1':padNum(fileSize), 'col2':padNum($('td:eq(2)',$(this)).text()), 'col3':padNum(id), 'col4':padNum($('td:eq(4)',$(this)).text()), 'col5':padNum($('td:eq(5)',$(this)).text()), 'html':$(this).html()
					});
				});
				data.sort(function(a, b) {
					var x = a['col'+colIndex].toLowerCase(); var y = b['col'+colIndex].toLowerCase();
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
				if (!ascend||ascend=='0') data.reverse();
				$(this).find('tr:not(.firstr)').remove();
				for (var i=0;i<data.length;i++) {
					$(this).append('<tr class="'+((i+1)%2==0?'even':'odd')+'">'+data[i].html+'</tr>');
				}
			});
		}
		function padNum(val) {
			val = '0000000000000000'+val;
			return val.substring(val.length-15);
		}
		function updateFPWsettings(t, o) {
			$.cookie('kat_settings[fpw_column]', t, { expires: 365, path: '/' });
			$.cookie('kat_settings[fpw_order]', o, { expires: 365, path: '/' });
		}
	</script>
		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
    
            
        <div  data-sc-slot="_119b0a17fab5493361a252d04bf527db"></div>
    
                
    	    <div class="spareBlock">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <div  data-sc-slot="_7063408f1c01d50e0dc2d833186ce962" data-sc-params="{ 'searchQuery': '' }"></div>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/kickass-anime-community-v-7/?unread=17351464">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Kickass Anime Community V.7!
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/TumblingCat/">TumblingCat</a></span></span> <time class="timeago" datetime="2016-01-25T18:04:15+00:00">25 Jan 2016, 18:04</time></span>
	</li>
		<li>
		<a href="/community/show/hevc-x265-club-uniteam-releases-thread-96931/?unread=17351461">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				HEVC/x265 Club UniTeamâ¢ Releases
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_verified"><a class="plain" href="/user/ByteShareRedo/">ByteShareRedo</a></span></span> <time class="timeago" datetime="2016-01-25T18:02:52+00:00">25 Jan 2016, 18:02</time></span>
	</li>
		<li>
		<a href="/community/show/torrents-need-updating-kat-changing-mod-work-thread-only-v6/?unread=17351459">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Torrents that need updating / KAT changing/ Mod Work Thread Only..V6
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/Ex-Gamer/">Ex-Gamer</a></span></span> <time class="timeago" datetime="2016-01-25T18:02:48+00:00">25 Jan 2016, 18:02</time></span>
	</li>
		<li>
		<a href="/community/show/please-request-ebooks-and-audio-books-here-v13/?unread=17351458">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Please request ebooks and audio books here. V13
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/sush2623/">sush2623</a></span></span> <time class="timeago" datetime="2016-01-25T18:02:34+00:00">25 Jan 2016, 18:02</time></span>
	</li>
		<li>
		<a href="/community/show/dc-extended-universe/?unread=17351456">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				DC Extended Universe
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_4"><a class="plain" href="/user/Globeleza/">Globeleza</a></span></span> <time class="timeago" datetime="2016-01-25T18:00:37+00:00">25 Jan 2016, 18:00</time></span>
	</li>
		<li>
		<a href="/community/show/karaoke-korner/?unread=17351455">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Karaoke Korner
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/KSK45/">KSK45</a></span></span> <time class="timeago" datetime="2016-01-25T18:00:32+00:00">25 Jan 2016, 18:00</time></span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/merry-xmas/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Merry Xmas
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-12-23T16:54:28+00:00">23 Dec 2015, 16:54</time></span>
	</li>
	<li>
		<a href="/blog/post/new-site-rules/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				New site Rules
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-15T14:18:43+00:00">15 Oct 2015, 14:18</time></span>
	</li>
	<li>
		<a href="/blog/post/look-mama-i-m-popular/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Look, mama, I&#039;m popular!
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-05T17:42:40+00:00">05 Oct 2015, 17:42</time></span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/TheDels/post/my-brother-s-school-play/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> My Brother&#039;s School Play</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/TheDels/">TheDels</a> <time class="timeago" datetime="2016-01-25T12:17:59+00:00">25 Jan 2016, 12:17</time></span></li>
	<li><a href="/blog/XGrimReaperX/post/sinners-i-hate-you/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Sinners (I hate you)</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/XGrimReaperX/">XGrimReaperX</a> <time class="timeago" datetime="2016-01-25T09:39:30+00:00">25 Jan 2016, 09:39</time></span></li>
	<li><a href="/blog/dfblast/post/what-tickles-my-funny-bone/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> What Tickles My Funny Bone</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/dfblast/">dfblast</a> <time class="timeago" datetime="2016-01-24T23:35:49+00:00">24 Jan 2016, 23:35</time></span></li>
	<li><a href="/blog/SushiKushi/post/january-2016-update/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> January 2016 Update</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/SushiKushi/">SushiKushi</a> <time class="timeago" datetime="2016-01-24T08:58:44+00:00">24 Jan 2016, 08:58</time></span></li>
	<li><a href="/blog/SFTDR/post/from-peer-to-peer-about-these-blogs/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> From peer to peer: &quot;About these blogs&quot;.</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/SFTDR/">SFTDR</a> <time class="timeago" datetime="2016-01-23T17:13:56+00:00">23 Jan 2016, 17:13</time></span></li>
	<li><a href="/blog/olderthangod/post/like-a-boss/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Like a Boss</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/olderthangod/">olderthangod</a> <time class="timeago" datetime="2016-01-23T12:17:05+00:00">23 Jan 2016, 12:17</time></span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/dp%20erotic/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				dp erotic
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/thirupachi/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				thirupachi
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/sonia%20style/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				sonia style
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/cabal%202/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				cabal 2
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/uncut/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				uncut
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/diamond%20foxxx%20big%20tits%20at%20school/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				diamond foxxx big tits at school
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/one%20wet%20cheerleader/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				one wet cheerleader
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/pompeii/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				pompeii
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/assetto%20corsa%201.4/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				assetto corsa 1.4
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/awards%202015/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				awards 2015
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/bella%20giann/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				bella giann
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="line-height:140%;-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a href="#" onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a href="#" onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a href="#" onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a href="#" onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a href="#" onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a href="#" onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a href="#" onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a href="#" onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a href="#" onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a href="#" onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a href="#" onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a href="#" onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a href="#" onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a href="#" onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a href="#" onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a href="#" onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a href="#" onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a href="#" onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a href="#" onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a href="#" onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a href="#" onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a href="#" onclick="setLanguage('he', '.kat.cr');return false;" class="plain">Hebrew</a></li>
                                <li><a href="#" onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a href="#" onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a href="#" onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a href="#" onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a href="#" onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a href="#" onclick="setLanguage('ko', '.kat.cr');return false;" class="plain">Korean</a></li>
                                <li><a href="#" onclick="setLanguage('lv', '.kat.cr');return false;" class="plain">Latvian</a></li>
                                <li><a href="#" onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a href="#" onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a href="#" onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a href="#" onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a href="#" onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a href="#" onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a href="#" onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a href="#" onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a href="#" onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a href="#" onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a href="#" onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a href="#" onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a href="#" onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a href="#" onclick="setLanguage('bsc', '.kat.cr');return false;" class="plain">Serbian-Cyrillic (ijekavica)</a></li>
                                <li><a href="#" onclick="setLanguage('si', '.kat.cr');return false;" class="plain">Sinhala</a></li>
                                <li><a href="#" onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a href="#" onclick="setLanguage('sl', '.kat.cr');return false;" class="plain">Slovenian</a></li>
                                <li><a href="#" onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a href="#" onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a href="#" onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a href="#" onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a href="#" onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a href="#" onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a href="#" onclick="setLanguage('ur', '.kat.cr');return false;" class="plain">Urdu</a></li>
                                <li><a href="#" onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" data-nop href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
        		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
        <li><a href="https://kastatus.com">KAT status</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <div  data-sc-slot="_673e31f53f8166159b8e996c4124765b"></div>
        <div  data-sc-slot="_e7050fb15fd39b3e4e99a5be4a57b6ea"></div>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script>
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "Organization",
"name": "KickassTorrents",
"url": "https://kat.cr",
"logo": "https://kat.cr/content/images/kickasslogo.png",
"sameAs": [
    "https://www.facebook.com/Official.KAT.Fanclub",
    "https://twitter.com/kickasstorrents"
]
}
</script>
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "WebSite",
"url": "https://kat.cr",
"potentialAction": {
    "@type": "SearchAction",
    "target": "https://kat.cr/usearch/{q}/",
    "query-input": {
        "@type": "PropertyValueSpecification",
        "valueRequired": true,
        "valueName": "q"
    }
}
}
</script>
<script>
 sc('addGlobal', 'pagetype', 'front');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>

</body>
</html>
