



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

    
    
    

    
    
    

    <meta name="apple-itunes-app" content="app-id=342792525, app-argument=imdb:///?src=mdot">
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "605-9284130-3721292";
                var ue_id = "19S0X6TMJEYG2871MMPM";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        
        <meta property="pageType" content="home" />
        <meta property="subpageType" content="main" />


        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />

    <meta property='fb:app_id' content='115109575169727' />

    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="19S0X6TMJEYG2871MMPM" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1e-c3-2xl-i-b2f08f0c.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-2467623394._CB300617431_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-3693178366._CB298945358_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-2454701167._CB293329573_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-2508181169._CB288096406_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-3988610918._CB289934424_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
(function(){var d=function(o){return Object.prototype.toString.call(o)==="[object Array]";},g=function(q,p){var o;for(o=0;o<q.length;o++){if(o in q){p.call(null,q[o],o);}}},h=[],k,b,l=false,n=false,f=function(){var o=[],p=[],q={};g(h,function(s){var r="";g(s.dartsite.split("/"),function(t){if(t!==""){if(t in q){}else{q[t]=o.length;o.push(t);}r+="/"+q[t];}});p.push(r);});return{iu_parts:o,enc_prev_ius:p};},c=function(){var o=[];g(h,function(q){var p=[];g(q.sizes,function(r){p.push(r.join("x"));});o.push(p.join("|"));});return o;},m=function(){var o=[];g(h,function(p){o.push(a(p.targeting));});return o.join("|");},a=function(r,o){var q,p,s=[];for(q in r){p=[];for(j=0;j<r[q].length;j++){p.push(encodeURIComponent(r[q][j]));}if(o){s.push(q+"="+encodeURIComponent(p.join(",")));}else{s.push(q+"="+p.join(","));}}return s.join("&");},e=function(){var o=+new Date();if(n){return;}if(!this.readyState||"loaded"===this.readyState){n=true;if(l){imdbads.cmd.push(function(){for(i=0;i<h.length;i++){generic.monitoring.record_metric(h[i].name+".fail",csm.duration(o));}});}}};window.tinygpt={define_slot:function(r,q,o,p){h.push({dartsite:r.replace(/\/$/,""),sizes:q,name:o,targeting:p});},set_targeting:function(o){k=o;},callback:function(q){var r,p={},t,o,s=+new Date();l=false;for(r=0;r<h.length;r++){t=h[r].dartsite;o=h[r].name;if(q[r][t]){p[o]=q[r][t];}else{window.console&&console.error&&console.error("Unable to correlate GPT response for "+o);}}imdbads.cmd.push(function(){for(r=0;r<h.length;r++){ad_utils.slot_events.trigger(h[r].name,"request",{timestamp:b});ad_utils.slot_events.trigger(h[r].name,"tagdeliver",{timestamp:s});}ad_utils.gpt.handle_response(p);});},send:function(){var r=[],q=function(s,t){if(d(t)){t=t.join(",");}if(t){r.push(s+"="+encodeURIComponent(""+t));}},o,p;if(h.length===0){tinygpt.callback({});return;}q("gdfp_req","1");q("correlator",Math.floor(4503599627370496*Math.random()));q("output","json_html");q("callback","tinygpt.callback");q("impl","fifs");q("json_a","1");result=f();q("iu_parts",result.iu_parts);q("enc_prev_ius",result.enc_prev_ius);q("prev_iu_szs",c());q("prev_scp",m());q("cust_params",a(k,true));o=document.createElement("script");p=document.getElementsByTagName("script")[0];o.async=true;o.type="text/javascript";o.src="http://pubads.g.doubleclick.net/gampad/ads?"+r.join("&");o.id="tinygpt";o.onload=o.onerror=o.onreadystatechange=e;l=true;p.parentNode.insertBefore(o,p);b=+new Date();}};})();</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[300,600],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['f'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['1'],
'u': ['905984553842'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-tarnhelm-4044276354._CB299069504_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"6f2fdec015d88eea04676d02bc17ce98941448ee",
"2016-01-25T18%3A03%3A28GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 50192;
generic.days_to_midnight = 0.5809259414672852;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-3512629095._CB289935695_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'f']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm == null || window.generic == null || window.consoleLog == null)) {
                    if (window.console && console.log) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=905984553842;ord=905984553842?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;ord=905984553842?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;ord=905984553842?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/chart/toptv/?ref_=nv_tp_tv250_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                        <li><a href="/chart/top?ref_=nv_mv_250_6"
>Top Rated Movies</a></li>
                        <li><a href="/chart/moviemeter?ref_=nv_mv_mpm_7"
>Most Popular Movies</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/chart/?ref_=nv_ch_cht_1"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_2"
>Oscar Winners</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_3"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/imdbpicks/tv-releases/?ref_=nv_tvv_picks_1"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_2"
>On Tonight</a></li>
                        <li><a href="/chart/toptv/?ref_=nv_tvv_250_3"
>Top Rated TV Shows</a></li>
                        <li><a href="/chart/tvmeter?ref_=nv_tvv_mptv_4"
>Most Popular TV Shows</a></li>
                        <li><a href="/feature/watch-now-on-amazon/?ref_=nv_tvv_wn_5"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_6"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_2"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/imdbpicks/?ref_=nv_sf_picks_3"
>IMDb Picks</a></li>
                        <li><a href="/best-of/?ref_=nv_sf_bstof_4"
>Best of 2015</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=01-25&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_2"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_3"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_4"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_5"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_6"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_7"
>Emmy Awards</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_8"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_9"
>Toronto Film Festival</a></li>
                        <li><a href="/event/all/?ref_=nv_ev_all_10"
>All Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni59419925/?ref_=nv_nw_tn_1"
> 'The Revenant' Weathers Snow Storm While Weekend's Newcomers Perform as Expected
</a><br />
                        <span class="time">24 January 2016 5:53 PM, UTC</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59419990/?ref_=nv_nw_tn_2"
> Amazon Lands Huge Deal for Sundance Sensation 'Manchester By the Sea'
</a><br />
                        <span class="time">22 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59419974/?ref_=nv_nw_tn_3"
> Shonda Rhimesâs Powerful Message on Receiving Her PGA Award: âI Deserve Thisâ
</a><br />
                        <span class="time">23 hours ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_3"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5>
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0056172/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTc5MzUxODg5OV5BMl5BanBnXkFtZTYwMzc3Nzc2._V1._UY315_CR70,0,410,315_CT10_.jpg",
            titleYears : "1962",
            rank : 85,
                    headline : "Lawrence of Arabia"
    },
    nameAd : {
            clickThru : "/name/nm0004266/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMjA1NzkyNjAzNV5BMl5BanBnXkFtZTcwNzYyOTUxOQ@@._V1._SX250_CR0,0,250,315_.jpg",
            rank : 141,
            headline : "Anne Hathaway"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYq_AQJ1RybWjkC78Gyx6P5LK9BllbDu8YTO3msTrVrUJvFxOGqbWQRkqKygtlq81Cap3xCDgbb%0D%0AMIc1mklS8xrYjrbjc_xCeRDIjbVfXg4KXP0x35G93T2Wwr8ang1JbyE6QF-LpBrqu67DL38TAoeK%0D%0AEclAHheGohyto5jCoT1GlaODmd6xGnALX869n4hF2e3raymkrqz8Tk27jqxPqEU6nw%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYqxDyJHwCiUrJfpdkqfZdVg0c0tfKkWEFDrXZ9h3pLE-GJNAJg5YBSi-dBD5qJVrWXU0o_TN5L%0D%0AlGE8kneibI1qviS3n1qwaT9WlB8CyXpx9x-DdCOdtz2gKY54kl-ayvUL-5W2c4qewxgl481e0s4L%0D%0AZeaw9yb3XIGTY-mxWp92KD8dhrOlujqPFKvEAiA7jw2-%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYqA8UZ4l-95h3S40H90CkPJvzHd2MbzLTYygiG7AVVN47X7ABzSWQyHrP0mwMsYlAByMqOlERy%0D%0AV-NFESli4023S0RowxrL7bz5NdZsNOvOlHFclPDDYL2FtTsYDPT5eG59c1V9MKwXiPuXFXxRR3mw%0D%0AlUy34oz-lcn_aHNHpHioAleuQ6zQmYcc4xF6weq6cfv_%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=905984553842;ord=905984553842?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>

    <div id="pagecontent">
    <div class="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=905984553842;ord=905984553842?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	

    
    
    

    
    
    

    
    
    
    </div>
    <div id="top-slot-wrapper" class="pagecontent">

    
    
    

    
    
    
    </div>
    <div class="pagecontent">
        <div id="content-2-wide">
            <div id="main">

    
    
        <a name="slot_hero"></a>
        <div class="heroWidget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','HeroWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_hero">
<div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2995369241?ref_=hm_hp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395419482&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi2995369241" data-source="bylist" data-id="ls002653141" data-rid="19S0X6TMJEYG2871MMPM" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="Fearing the actions of Superman are left unchecked, Batman takes on Superman, while the world wrestles with what kind of a hero it really needs." alt="Fearing the actions of Superman are left unchecked, Batman takes on Superman, while the world wrestles with what kind of a hero it really needs." src="http://ia.media-imdb.com/images/M/MV5BNDA5NDAzMzg1MF5BMl5BanBnXkFtZTgwOTY2MjU2NzE@._V1_SY298_CR3,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNDA5NDAzMzg1MF5BMl5BanBnXkFtZTgwOTY2MjU2NzE@._V1_SY298_CR3,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Fearing the actions of Superman are left unchecked, Batman takes on Superman, while the world wrestles with what kind of a hero it really needs." title="Fearing the actions of Superman are left unchecked, Batman takes on Superman, while the world wrestles with what kind of a hero it really needs." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Fearing the actions of Superman are left unchecked, Batman takes on Superman, while the world wrestles with what kind of a hero it really needs." title="Fearing the actions of Superman are left unchecked, Batman takes on Superman, while the world wrestles with what kind of a hero it really needs." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt2975590/?ref_=hm_hp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395419482&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > Batman v Superman </a> </div> </div> <div class="secondary ellipsis"> TV Promo #3 </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi4287149337?ref_=hm_hp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395419482&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi4287149337" data-source="bylist" data-id="ls002653141" data-rid="19S0X6TMJEYG2871MMPM" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="An epic fantasy/adventure based on the popular video game series." alt="An epic fantasy/adventure based on the popular video game series." src="http://ia.media-imdb.com/images/M/MV5BMTgxMDAzNzMyMV5BMl5BanBnXkFtZTgwNjIwMTgxNzE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTgxMDAzNzMyMV5BMl5BanBnXkFtZTgwNjIwMTgxNzE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="An epic fantasy/adventure based on the popular video game series." title="An epic fantasy/adventure based on the popular video game series." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="An epic fantasy/adventure based on the popular video game series." title="An epic fantasy/adventure based on the popular video game series." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt0803096/?ref_=hm_hp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395419482&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > Warcraft </a> </div> </div> <div class="secondary ellipsis"> Latest TV Promo </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2407904537?ref_=hm_hp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395419482&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi2407904537" data-source="bylist" data-id="ls056131825" data-rid="19S0X6TMJEYG2871MMPM" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="Season 6 Stark Battle Banner Teaser for &quot;Game of Thrones&quot; on HBO." alt="Season 6 Stark Battle Banner Teaser for &quot;Game of Thrones&quot; on HBO." src="http://ia.media-imdb.com/images/M/MV5BMTYwOTEzMDMzMl5BMl5BanBnXkFtZTgwNzExODIzNzE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTYwOTEzMDMzMl5BMl5BanBnXkFtZTgwNzExODIzNzE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Season 6 Stark Battle Banner Teaser for &quot;Game of Thrones&quot; on HBO." title="Season 6 Stark Battle Banner Teaser for &quot;Game of Thrones&quot; on HBO." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Season 6 Stark Battle Banner Teaser for &quot;Game of Thrones&quot; on HBO." title="Season 6 Stark Battle Banner Teaser for &quot;Game of Thrones&quot; on HBO." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt0944947/?ref_=hm_hp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395419482&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" > "Game of Thrones" </a> </div> </div> <div class="secondary ellipsis"> Lannister Battle Banner Teaser </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/trailers?ref_=hm_hp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395419482&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Browse more trailers</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','HeroWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-1"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/sundance/video/?ref_=hm_sun_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395462502&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Get to Know 'The Hollars' and 'Captain Fantastic'</h3> </a> </span> </span> <p class="blurb">Director <a href="/name/nm1024677/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395462502&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sun_lk1">John Krasinski</a> and <a href="/name/nm0553269/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395462502&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sun_lk2">Margo Martindale</a> visit the IMDb Studio at Sundance to discuss their comedy <i><a href="/title/tt3714720/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395462502&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sun_lk3">The Hollars</a></i>. Plus <a href="/name/nm0001557/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395462502&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sun_lk4">Viggo Mortensen</a> and director <a href="/name/nm0743671/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395462502&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sun_lk5">Matt Ross</a> explain the origins of <i><a href="/title/tt3553976/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395462502&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sun_lk6">Captain Fantastic</a></i>. Visit our <a href="/sundance/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395462502&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sun_lk7">special section</a> for photos, videos, and more.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi562672921?ref_=hm_sun_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395462502&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi562672921" data-source="bylist" data-id="ls073917212" data-rid="19S0X6TMJEYG2871MMPM" data-type="playlist" class="video-colorbox" data-refsuffix="hm_sun" data-ref="hm_sun_i_1"> <img itemprop="image" class="pri_image" title="The IMDb Studio (2015-)" alt="The IMDb Studio (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMjk4MDkwODA5Nl5BMl5BanBnXkFtZTgwMTIzMDE4NzE@._V1_SY201_CR42,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjk4MDkwODA5Nl5BMl5BanBnXkFtZTgwMTIzMDE4NzE@._V1_SY201_CR42,0,201,201_AL_UY402_UX402_AL_.jpg" /> <img alt="The IMDb Studio (2015-)" title="The IMDb Studio (2015-)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="The IMDb Studio (2015-)" title="The IMDb Studio (2015-)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/sundance/video/?ref_=hm_sun_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395462502&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > Martindale and Krasinski on Amazing <i>Hollars</i> Cast </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/sundance/studio-day-3/?imageid=rm1422844928&ref_=hm_sun_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395462502&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Allison Janney and Ellen Page at event of The IMDb Studio (2015)" alt="Allison Janney and Ellen Page at event of The IMDb Studio (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjQzMzAwMDE2MF5BMl5BanBnXkFtZTgwNjM5NzA4NzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQzMzAwMDE2MF5BMl5BanBnXkFtZTgwNjM5NzA4NzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/sundance/studio-day-3/?ref_=hm_sun_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395462502&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > The IMDb Studio - Day 3 Photos </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi613070105?ref_=hm_sun_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395462502&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi613070105" data-source="bylist" data-id="ls073917212" data-rid="19S0X6TMJEYG2871MMPM" data-type="playlist" class="video-colorbox" data-refsuffix="hm_sun" data-ref="hm_sun_i_3"> <img itemprop="image" class="pri_image" title="The IMDb Studio (2015-)" alt="The IMDb Studio (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMTUxNDM1ODM5M15BMl5BanBnXkFtZTgwMjQzMDE4NzE@._V1_SY201_CR43,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTUxNDM1ODM5M15BMl5BanBnXkFtZTgwMjQzMDE4NzE@._V1_SY201_CR43,0,201,201_AL_UY402_UX402_AL_.jpg" /> <img alt="The IMDb Studio (2015-)" title="The IMDb Studio (2015-)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="The IMDb Studio (2015-)" title="The IMDb Studio (2015-)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/sundance/video/?ref_=hm_sun_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395462502&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" > Ross and Mortensen Explain <i>Captain Fantastic</i> </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/sundance/video/?ref_=hm_sun_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395462502&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Watch more video from The IMDb Studio at Sundance</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-2"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/sundance/women-at-sundance-fellows/ls031910270?ref_=hm_sun_fel_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395411922&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Meet the 2015-2016 Women at Sundance Fellows</h3> </a> </span> </span> <p class="blurb">The Sundance Institute has chosen its 2015-2016 Women at Sundance Fellows, a group of directors and producers who will receive support from the Institute for their careers and projects. Read on to learn more about the Fellows.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/sundance/women-at-sundance-fellows/ls031910270?ref_=hm_sun_fel_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395411922&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage#image1" > <img itemprop="image" class="pri_image" title="Lyric R. Cabral at event of (T)ERROR (2015)" alt="Lyric R. Cabral at event of (T)ERROR (2015)" src="http://ia.media-imdb.com/images/M/MV5BNDYzMjI4ODczMF5BMl5BanBnXkFtZTgwNTQ2OTYxNDE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNDYzMjI4ODczMF5BMl5BanBnXkFtZTgwNTQ2OTYxNDE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/sundance/women-at-sundance-fellows/ls031910270?ref_=hm_sun_fel_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395411922&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage#image4" > <img itemprop="image" class="pri_image" title="Still of Jennifer Phang in Advantageous (2015)" alt="Still of Jennifer Phang in Advantageous (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTcwMDgxNTA1MF5BMl5BanBnXkFtZTgwNDEwMzg4MzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTcwMDgxNTA1MF5BMl5BanBnXkFtZTgwNDEwMzg4MzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/sundance/women-at-sundance-fellows/ls031910270?ref_=hm_sun_fel_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395411922&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage#image5" > <img itemprop="image" class="pri_image" title="Pamela Romanowsky" alt="Pamela Romanowsky" src="http://ia.media-imdb.com/images/M/MV5BNjYzMjg2NjU2NV5BMl5BanBnXkFtZTgwNDM1NDM4MTE@._V1_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjYzMjg2NjU2NV5BMl5BanBnXkFtZTgwNDM1NDM4MTE@._V1_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/sundance/women-at-sundance-fellows/ls031910270?ref_=hm_sun_fel_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395411922&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Meet the 2015-2016 Fellows</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-3"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NewsDeskWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59419925?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjU4NDExNDM1NF5BMl5BanBnXkFtZTgwMDIyMTgxNzE@._V1_SY150_CR0,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59419925?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1"
class="headlines" >'The Revenant' Weathers Snow Storm While Weekend's Newcomers Perform as Expected</a>
    <div class="infobar">
            <span class="text-muted">24 January 2016 6:53 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000159?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_src"
>Box Office Mojo</a></span>
    </div>
                                </div>
<p>With a blizzard blanketing the East Coast, the weekend's box office was down 27% compared to last year. How much of that percentage should be attributed to the snowstorm and how much is due to the fact <a href="/title/tt2179136?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk1">American Sniper</a> was tearing up the box office last year is difficult to decipher. In all, the ...                                        <span class="nobr"><a href="/news/ni59419925?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59419990?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2"
class="headlines" >Amazon Lands Huge Deal for Sundance Sensation 'Manchester By the Sea'</a>
    <div class="infobar">
            <span class="text-muted">22 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000139?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2_src"
>Indiewire</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59419974?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3"
class="headlines" >Shonda Rhimesâs Powerful Message on Receiving Her PGA Award: âI Deserve Thisâ</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3_src"
>Vulture</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59422927?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4"
class="headlines" >Supposed 'Labyrinth' reboot writer says it's not actually happening</a>
    <div class="infobar">
            <span class="text-muted">27 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000173?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4_src"
>Hitfix</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59422922?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5"
class="headlines" >Sundance: Logan Lermanâs âIndignationâ Bought by Lionsgate</a>
    <div class="infobar">
            <span class="text-muted">27 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59420483?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjE0OTAyMTI1M15BMl5BanBnXkFtZTgwOTY4NjkzNzE@._V1_SY150_CR88,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59420483?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1"
class="headlines" >A24 Acquires âMorris From Americaâ â Sundance</a>
    <div class="infobar">
            <span class="text-muted">19 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_src"
>Deadline</a></span>
    </div>
                                </div>
<p>Exclusive: A24 has gotten on the board here at Sundance, acquiring U.S. rights to <a href="/title/tt3652862?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk1">Morris From America</a>, the <a href="/name/nm1029961?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk2">Chad Hartigan</a>-directed film about a 13 year old hip hop loving American boy who moves to Germany with his father. A24 is putting this through its arrangement with DirecTV, in a deal that is ...                                        <span class="nobr"><a href="/news/ni59420483?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59419990?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2"
class="headlines" >Amazon Lands Huge Deal for Sundance Sensation 'Manchester By the Sea'</a>
    <div class="infobar">
            <span class="text-muted">22 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000139?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2_src"
>Indiewire</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59419925?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3"
class="headlines" >'The Revenant' Weathers Snow Storm While Weekend's Newcomers Perform as Expected</a>
    <div class="infobar">
            <span class="text-muted">24 January 2016 6:53 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000159?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3_src"
>Box Office Mojo</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59422922?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4"
class="headlines" >Sundance: Logan Lermanâs âIndignationâ Bought by Lionsgate</a>
    <div class="infobar">
            <span class="text-muted">27 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59422989?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5"
class="headlines" >âLabyrinthâ Honest Trailer: David Bowie Steals Pretty Much Every Scene Heâs In</a>
    <div class="infobar">
            <span class="text-muted">32 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000151?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5_src"
>Slash Film</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59422683?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTczNTQ5MzE1MV5BMl5BanBnXkFtZTgwOTk4MDU1NTE@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59422683?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1"
class="headlines" >Ratings: The X-Files Gets Off to a Spooky-Good Start</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_src"
>TVLine.com</a></span>
    </div>
                                </div>
<p> A whole lot of people still believe. The opening installment of Foxâs six-hour X-Files revival drew an impressive 13.5Â million total viewers on Sunday night during its first half-hour (starting around 10:25 pm), leading out of a well-watched (if lop-sided) Nfc Championship game (which delivered ...                                        <span class="nobr"><a href="/news/ni59422683?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59419974?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2"
class="headlines" >Shonda Rhimesâs Powerful Message on Receiving Her PGA Award: âI Deserve Thisâ</a>
    <div class="infobar">
            <span class="text-muted">23 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0031804?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2_src"
>Vulture</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59422788?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3"
class="headlines" >How Jane the Virgin Revived the Love Triangle</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000222?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3_src"
>TVovermind.com</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59422772?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4"
class="headlines" >David Duchovny Earns His Star on the Walk of Fame</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59422756?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5"
class="headlines" >Watch: 'Animals.' Trailer Lends Voices (Like Jessica Chastain's) to the Urban Animal Rebellion</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0032055?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5_src"
>Indiewire Television</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59420964?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTk1MDIwOTg3OV5BMl5BanBnXkFtZTcwMjE3MjM3Mw@@._V1_SY150_CR3,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59420964?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1"
class="headlines" >Jimmy Fallon Donates $10,000 to Flint Charity During City's Ongoing Water Crisis</a>
    <div class="infobar">
            <span class="text-muted">17 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_src"
>PEOPLE.com</a></span>
    </div>
                                </div>
<p><a href="/name/nm0266422?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk1">Jimmy Fallon</a> is doing good, and he's asking for his friends to join him. <a href="/title/tt3444938?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk2">The Tonight Show Starring Jimmy Fallon</a> host revealed on Twitter on Sunday that he was donating $10,000 to Community Foundation Greater Flint, a charity supporting the community in Flint, Michigan. "Happy Sunday!! I'm donating ...                                        <span class="nobr"><a href="/news/ni59420964?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59420898?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2"
class="headlines" >Mark Ruffalo Proves Human ''Decency'' Is Alive and Well After Strangers Return His Missing Phone and Wallet</a>
    <div class="infobar">
            <span class="text-muted">17 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59421292?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3"
class="headlines" >Kourtney Kardashian Can't Stop Smiling When Asked If She's "Beyond Friends" With Justin Bieber</a>
    <div class="infobar">
            <span class="text-muted">13 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3_src"
>E! Online</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59423031?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4"
class="headlines" >Kourtney Kardashian Insists She's Just 'Friends' with Justin Bieber - But Ellen DeGeneres Jokingly Digs Deeper: 'Why Are You Smiling That Way?'</a>
    <div class="infobar">
            <span class="text-muted">33 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59423032?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5"
class="headlines" >Jamie-Lynn Sigler Kisses New Husband Cutter Dykstra on Romantic Beach Honeymoon</a>
    <div class="infobar">
            <span class="text-muted">43 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1920909362&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NewsDeskWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-4"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>IMDb Snapshot</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm605546240/rg1528338944?ref_=hm_snp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395456682&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjEwMjEyMzM2Ml5BMl5BanBnXkFtZTgwNTk2Mjk3NzE@._UX402_CR0,190,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjEwMjEyMzM2Ml5BMl5BanBnXkFtZTgwNTk2Mjk3NzE@._UX402_CR0,190,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm605546240/rg1528338944?ref_=hm_snp_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395456682&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > Latest Posters </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/the-x-files-through-the-years/?imageid=rm2622211072&ref_=hm_snp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395456682&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of David Duchovny in The X-Files (1993)" alt="Still of David Duchovny in The X-Files (1993)" src="http://ia.media-imdb.com/images/M/MV5BMTg3MzA2MDg0OV5BMl5BanBnXkFtZTgwNzgzNzc3MjE@._V1_SY201_CR33,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg3MzA2MDg0OV5BMl5BanBnXkFtZTgwNzgzNzc3MjE@._V1_SY201_CR33,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/imdbpicks/the-x-files-through-the-years/?imageid=rm2622211072&ref_=hm_snp_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395456682&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > "The X-Files" Through the Years </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/sundance/2016-sundance-photos-documentary-films?imageid=rm3698909440&ref_=hm_snp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395456682&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Still of Jessie Kahnweiler in The Skinny (2016)" alt="Still of Jessie Kahnweiler in The Skinny (2016)" src="http://ia.media-imdb.com/images/M/MV5BMzE2NTMxMzM5Ml5BMl5BanBnXkFtZTgwOTgyNzA1NzE@._V1_SY201_CR78,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzE2NTMxMzM5Ml5BMl5BanBnXkFtZTgwOTgyNzA1NzE@._V1_SY201_CR78,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/sundance/2016-sundance-photos-documentary-films?imageid=rm3698909440&ref_=hm_snp_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395456682&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage" > Sundance Documentary Stills </a> </div> </div> </div> </div> </div> </div> </div> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-5"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BornTodayWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_borntoday">
<span class="widget_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=1-25&ref_=hm_brn_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Born Today</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0000477?ref_=hm_brn_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Mia Kirshner" alt="Mia Kirshner" src="http://ia.media-imdb.com/images/M/MV5BMTQ1NTQzNzQ1Nl5BMl5BanBnXkFtZTcwNzA1MTM5OA@@._V1_SY172_CR3,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ1NTQzNzQ1Nl5BMl5BanBnXkFtZTcwNzA1MTM5OA@@._V1_SY172_CR3,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0000477?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_1">Mia Kirshner</a> (41) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm2584392?ref_=hm_brn_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Tom Hopper" alt="Tom Hopper" src="http://ia.media-imdb.com/images/M/MV5BMTQ4ODk3Njc4NV5BMl5BanBnXkFtZTgwNjIxNzEzMDE@._V1_SY172_CR95,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ4ODk3Njc4NV5BMl5BanBnXkFtZTgwNjIxNzEzMDE@._V1_SY172_CR95,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm2584392?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_2">Tom Hopper</a> (31) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0003115?ref_=hm_brn_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Christine Lakin" alt="Christine Lakin" src="http://ia.media-imdb.com/images/M/MV5BMTkyMzU1MTkyM15BMl5BanBnXkFtZTgwOTEwNzE2NzE@._V1_SY172_CR71,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTkyMzU1MTkyM15BMl5BanBnXkFtZTgwOTEwNzE2NzE@._V1_SY172_CR71,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0003115?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_3">Christine Lakin</a> (37) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm2263791?ref_=hm_brn_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Claudia Kim" alt="Claudia Kim" src="http://ia.media-imdb.com/images/M/MV5BMTcyMzgxNTM4N15BMl5BanBnXkFtZTgwMjY1ODk4MzE@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTcyMzgxNTM4N15BMl5BanBnXkFtZTgwMjY1ODk4MzE@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm2263791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_4">Claudia Kim</a> (31) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0001503?ref_=hm_brn_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Dinah Manoff" alt="Dinah Manoff" src="http://ia.media-imdb.com/images/M/MV5BMTM1OTg2OTA4NF5BMl5BanBnXkFtZTcwNjQzNDg5Nw@@._V1_SY172_CR7,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTM1OTg2OTA4NF5BMl5BanBnXkFtZTcwNjQzNDg5Nw@@._V1_SY172_CR7,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0001503?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_5">Dinah Manoff</a> (58) </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/search/name?refine=birth_monthday&birth_monthday=1-25&ref_=hm_brn_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2150546862&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See all birthdays</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','BornTodayWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-7"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>Photos From the 2016 Sundance Film Festival</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/sundance/portraits-and-people/?imageid=rm232317952&ref_=hm_sun_ph_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395429662&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Zoe Kazan and Jenny Slate" alt="Zoe Kazan and Jenny Slate" src="http://ia.media-imdb.com/images/M/MV5BMTAwNjg3OTk1NTFeQTJeQWpwZ15BbWU4MDk5MTIxODcx._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTAwNjg3OTk1NTFeQTJeQWpwZ15BbWU4MDk5MTIxODcx._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/sundance/portraits-and-people/?ref_=hm_sun_ph_cap_pri_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395429662&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > Portraits and People </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/sundance/studio-day-2/?imageid=rm3170100224&ref_=hm_sun_ph_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395429662&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Tika Sumpter at event of The IMDb Studio (2015)" alt="Tika Sumpter at event of The IMDb Studio (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjA4MTc0NjI1Ml5BMl5BanBnXkFtZTgwMDM5MzA4NzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA4MTc0NjI1Ml5BMl5BanBnXkFtZTgwMDM5MzA4NzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/sundance/studio-day-2/?ref_=hm_sun_ph_cap_pri_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395429662&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > The IMDb Studio at Sundance -- Day 2 </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/sundance/premieres-and-parties/?imageid=rm2646860800&ref_=hm_sun_ph_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395429662&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Kristen Stewart and George Pimentel" alt="Kristen Stewart and George Pimentel" src="http://ia.media-imdb.com/images/M/MV5BMjQxMjc5NjQ3MF5BMl5BanBnXkFtZTgwNzUxMDE4NzE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQxMjc5NjQ3MF5BMl5BanBnXkFtZTgwNzUxMDE4NzE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/sundance/premieres-and-parties/?ref_=hm_sun_ph_cap_pri_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395429662&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage" > Premieres and Parties </a> </div> </div> </div> </div> </div> </div> </div> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
        <a name="slot_center-8"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>Indie Focus: Exclusive Clip From 'Lazer Team'</h3> </span> </span> <p class="blurb">The first feature film from Rooster Teeth Productions is a comedy about four small-town misfits who must use an extraterrestrial battle suit to save mankind.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt3864024/?ref_=hm_if_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395321882&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Lazer Team (2015)" alt="Lazer Team (2015)" src="http://ia.media-imdb.com/images/M/MV5BODU0MDM0ODMyN15BMl5BanBnXkFtZTgwMzk1MTk3NzE@._V1_SX170_CR0,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BODU0MDM0ODMyN15BMl5BanBnXkFtZTgwMzk1MTk3NzE@._V1_SX170_CR0,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2860889369?ref_=hm_if_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2395321882&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi2860889369" data-rid="19S0X6TMJEYG2871MMPM" data-type="single" class="video-colorbox" data-refsuffix="hm_if" data-ref="hm_if_i_2"> <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BNjY1OTYyODY3Ml5BMl5BanBnXkFtZTgwNDkyOTA5NDE@._V1_SX444_CR0,0,444,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjY1OTYyODY3Ml5BMl5BanBnXkFtZTgwNDkyOTA5NDE@._V1_SX444_CR0,0,444,250_AL_UY500_UX888_AL_.jpg" /> <img class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-13"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','RecsWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','RecsWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-19"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TriviaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_trivia">
<span class="widget_header"> <span class="oneline"> <a href="/title/tt0105236/trivia?item=tr0657734&ref_=hm_trv_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Did You Know?</h3> </a> </span> </span> <div class="widget_content inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt0105236?ref_=hm_trv_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Reservoir Dogs (1992)" alt="Reservoir Dogs (1992)" src="http://ia.media-imdb.com/images/M/MV5BMTQxMTAwMDQ3Nl5BMl5BanBnXkFtZTcwODMwNTgzMQ@@._V1_SY132_CR2,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQxMTAwMDQ3Nl5BMl5BanBnXkFtZTcwODMwNTgzMQ@@._V1_SY132_CR2,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> <div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="widget_inline_blurb"><strong class="text-large"><a href="/title/tt0105236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Reservoir Dogs</a></strong> <p class="blurb">The warehouse where the majority of the movie takes place was once a mortuary, and thus is full of coffins. Mr. Blonde doesn't sit down on a crate, it's actually an old hearse he perches on.</p> <p class="seemore"><a href="/title/tt0105236/trivia?item=tr0657734&ref_=hm_trv_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage" class="position_blurb supplemental" >See more trivia</a></p> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','TriviaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-26"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','PollWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;0yH3rX2R0JQ&quot;}">
        
    

    <div class="ab_poll poll">
<span class="widget_header"> <span class="oneline"> <a href="/poll/0yH3rX2R0JQ/?ref_=hm_poll_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390693242&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Poll: Oscars 2016: Best Actor in a Leading Role</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390693242&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk">More Polls</a></h4> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/0yH3rX2R0JQ/?ref_=hm_poll_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390693242&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Bryan Cranston" alt="Bryan Cranston" src="http://ia.media-imdb.com/images/M/MV5BMTA2NjEyMTY4MTVeQTJeQWpwZ15BbWU3MDQ5NDAzNDc@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTA2NjEyMTY4MTVeQTJeQWpwZ15BbWU3MDQ5NDAzNDc@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/0yH3rX2R0JQ/?ref_=hm_poll_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390693242&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Matt Damon" alt="Matt Damon" src="http://ia.media-imdb.com/images/M/MV5BMTM0NzYzNDgxMl5BMl5BanBnXkFtZTcwMDg2MTMyMw@@._V1_SY207_CR7,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTM0NzYzNDgxMl5BMl5BanBnXkFtZTcwMDg2MTMyMw@@._V1_SY207_CR7,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/0yH3rX2R0JQ/?ref_=hm_poll_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390693242&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Leonardo DiCaprio" alt="Leonardo DiCaprio" src="http://ia.media-imdb.com/images/M/MV5BMjI0MTg3MzI0M15BMl5BanBnXkFtZTcwMzQyODU2Mw@@._V1_SY207_CR7,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI0MTg3MzI0M15BMl5BanBnXkFtZTcwMzQyODU2Mw@@._V1_SY207_CR7,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/0yH3rX2R0JQ/?ref_=hm_poll_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390693242&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Michael Fassbender" alt="Michael Fassbender" src="http://ia.media-imdb.com/images/M/MV5BMTk0NjM2MTE5M15BMl5BanBnXkFtZTcwODIxMzcyNw@@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk0NjM2MTE5M15BMl5BanBnXkFtZTcwODIxMzcyNw@@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/0yH3rX2R0JQ/?ref_=hm_poll_i_5&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390693242&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" title="Eddie Redmayne" alt="Eddie Redmayne" src="http://ia.media-imdb.com/images/M/MV5BMTU0MjEyNzQyM15BMl5BanBnXkFtZTcwMTc4ODUxOQ@@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU0MjEyNzQyM15BMl5BanBnXkFtZTcwMTc4ODUxOQ@@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Who should win the Academy Award for Best Actor in a Leading Role at the Oscars Awards 2016? Discuss <a href="http://www.imdb.com/board/bd0000088/nest/252617236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390693242&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk1">here</a> after voting.</p> <p class="seemore"><a href="/poll/0yH3rX2R0JQ/?ref_=hm_poll_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390693242&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Vote now</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','PollWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-28"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2393877562&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Catch Up on IMDb Asks</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2393877562&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMzAxMjY5MjY4OF5BMl5BanBnXkFtZTgwOTMxMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzAxMjY5MjY4OF5BMl5BanBnXkFtZTgwOTMxMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_i_2&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2393877562&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BNjQ0OTEzMTk1OF5BMl5BanBnXkFtZTgwODQyMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjQ0OTEzMTk1OF5BMl5BanBnXkFtZTgwODQyMzY2NzE@._UX402_CR0,0,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_i_3&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2393877562&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTY4MjEyMTE5Nl5BMl5BanBnXkFtZTgwNjI0Mzk2NzE@._SX700_CR70,100,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY4MjEyMTE5Nl5BMl5BanBnXkFtZTgwNjI0Mzk2NzE@._SX700_CR70,100,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:148px;height:auto;" > <div style="width:148px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_i_4&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2393877562&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjQwOTY1NTE2M15BMl5BanBnXkFtZTgwNDk5MjM2NzE@._SX830_CR240,0,402,402_SY148_SX148_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQwOTY1NTE2M15BMl5BanBnXkFtZTgwNDk5MjM2NzE@._SX830_CR240,0,402,402_SY148_SX148_AL_UY296_UX296_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Did you miss one of our one-on-one interviews? Check out our archived interviews with your favorite stars.</p> <p class="seemore"><a href="/feature/imdb-asks?ref_=hm_hm_ia_lp_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2393877562&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=center-28&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Watch all our one-on-one interviews</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    
            </div>
            <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,300x600,11x1;p=tr;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=905984553842;ord=905984553842?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;ord=905984553842?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,300x600,11x1;p=tr;fv=1;ab=f;bpx=1;c=0;s=3075;s=32;ord=905984553842?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	

    
    
        <a name="slot_right-1"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<p class="blurb"><a href="https://imdb.co1.qualtrics.com/SE/?SID=SV_1AKLG3wDXgw8B7v&ref_=hm_sh_lk1"><b>Feedback:</b> Please take 5 minutes to participate in our short IMDb survey</a></p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-3"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-in-theaters/?ref_=hm_otw_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390111522&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Opening This Week</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2267968"></div> <div class="title"> <a href="/title/tt2267968?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390111522&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t0"> Kung Fu Panda 3 </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2025690"></div> <div class="title"> <a href="/title/tt2025690?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390111522&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t1"> The Finest Hours </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4667094"></div> <div class="title"> <a href="/title/tt4667094?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390111522&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t2"> Fifty Shades of Black </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2140037"></div> <div class="title"> <a href="/title/tt2140037?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390111522&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t3"> Jane Got a Gun </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-in-theaters/?ref_=hm_otw_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2390111522&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See more opening this week</a></p>
                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	

    
    
        <a name="slot_right-4"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="showtimesMessagingWidget"> <p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2006955922&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=right-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> </div> </div> </span>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-6"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','BoxOfficeListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/chart/?ref_=hm_cht_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Now Playing (Box Office)</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1663202"></div> <div class="title"> <a href="/title/tt1663202?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t0"> The Revenant </a> <span class="secondary-text">$16.0M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2488496"></div> <div class="title"> <a href="/title/tt2488496?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t1"> Star Wars: Episode VII - The Force Awakens </a> <span class="secondary-text">$14.3M</span> </div> <div class="action"> <a href="/showtimes/title/tt2488496?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2869728"></div> <div class="title"> <a href="/title/tt2869728?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t2"> Ride Along 2 </a> <span class="secondary-text">$13.0M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1860213"></div> <div class="title"> <a href="/title/tt1860213?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t3"> Dirty Grandpa </a> <span class="secondary-text">$11.5M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3882082"></div> <div class="title"> <a href="/title/tt3882082?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t4"> The Boy </a> <span class="secondary-text">$11.3M</span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/chart/?ref_=hm_cht_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971070862&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See more box office results</a></p>
                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','BoxOfficeListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-8"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-coming-soon/?ref_=hm_cs_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Coming Soon</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt0475290"></div> <div class="title"> <a href="/title/tt0475290?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t0"> Hail, Caesar! </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1374989"></div> <div class="title"> <a href="/title/tt1374989?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t1"> Pride and Prejudice and Zombies </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-coming-soon/?ref_=hm_cs_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >See more coming soon</a></p>
                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','WatchableTitlesListWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-12"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','TwitterWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_twitter">
<span class="widget_header"> <span class="oneline"> <h3>Follow Us On Twitter</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3396827429._CB298884168_.html#%7B%22width%22%3A%22300px%22%2C%22height%22%3A%22500px%22%2C%22href%22%3A%22https%3A%2F%2Ftwitter.com%2FIMDb%22%2C%22heading%22%3A%22Follow%20Us%20On%20Twitter%22%2C%22screen-name%22%3A%22IMDb%22%2C%22list-name%22%3Anull%2C%22widget-id%22%3A%22354387022028357633%22%7D'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','TwitterWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-14"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','FacebookWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_facebook">
<span class="widget_header"> <span class="oneline"> <h3>Find Us On Facebook</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','FacebookWidget',{wb:1});}</script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-30"></a>
        <div class="aux-content-widget-2 sticky-widget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof(uet)==='function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/awards-central/video/?ref_=hm_ac_obn_hd&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2393684762&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" > <h3>Oscars by the Numbers</h3> </a> </span> </span> <p class="blurb">As the Oscars approach, we took a look all the way back to the first ceremony in 1929. Find out fun facts such as how much it cost to attend the prestigious event back then. Plus, do you know who gave the shortest Oscar speech ever and what she said?</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1150334233?ref_=hm_ac_obn_i_1&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2393684762&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" data-video="vi1150334233" data-source="bylist" data-id="ls031574778" data-rid="19S0X6TMJEYG2871MMPM" data-type="playlist" class="video-colorbox" data-refsuffix="hm_ac_obn" data-ref="hm_ac_obn_i_1"> <img itemprop="image" class="pri_image" title="IMDb Originals (2015-)" alt="IMDb Originals (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMjMzMjQ4NzU1NV5BMl5BanBnXkFtZTgwMDkwODA3NzE@._V1_SY230_CR51,0,307,230_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjMzMjQ4NzU1NV5BMl5BanBnXkFtZTgwMDkwODA3NzE@._V1_SY230_CR51,0,307,230_AL_UY460_UX614_AL_.jpg" /> <img alt="IMDb Originals (2015-)" title="IMDb Originals (2015-)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="IMDb Originals (2015-)" title="IMDb Originals (2015-)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/awards-central/video/?ref_=hm_ac_obn_sm&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2393684762&pf_rd_r=19S0X6TMJEYG2871MMPM&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage" class="position_bottom supplemental" >Watch more IMDb Originals video</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">if(typeof(uex)==='function'){uex('ld','NinjaWidget',{wb:1});}</script>
        




        </div>
    
            </div>
            <br class="clear" />
        </div>
        

    
    
    

    
    
    
        
    
        <br class="clear" />
    </div>
</div>
                





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYtbrHdOTWuOyBj5WR2dTqvNMgm2qOxHKDcP9RX7SsAZV24k5UFuovcaOSsoZO_nu32cStip0MF%0D%0AkeZhjeBf0dq1oeAJ8YmSriXV1uD-jG2doSwYolS71rRPnt2AUZkFVR-MwxezUMe-qDj0EfLsl-PJ%0D%0AK6gLNMKjNJn5AcgywdCBC4Dw-yEg4Cfb0xmahkoPyUJM075S2ogga6biQu4g0OcMnCseXl-bUF9w%0D%0AlXA1fnD5yRM%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYgATmpTSEAVLIEfSxQUnnbGbdHI7Oz-t1GS7HMwxEVxyzySN5Z5O6K0aHj_3n89v61Gkz5pRpA%0D%0A4GQ6hdauMom6-_4yhHoFGjcA0HKmN5GjNfdF7VlvNwhZZNUUdBNbGh0N3JNsiILzhDnvnu7m6a8R%0D%0AxSwvIOqKPDZVj5I0xHRFsdb1tXGOVe_y-ssH6x9JJByxzdMvPlCYyjDkUVfxbmrWYg%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYiiftu6EX3IoVXk23mRqW0qcn45QRmx9uJeL9MjGvk5aatz4LBAwCzcHf7KmOIQcFdHhL9FH-o%0D%0AzPBcaWCDrmDbE_Fy4zI5tuK5s_51fF6luZ6GBbWDS7KK3nm6IoX4FbMuF9lpft_at9NMlkziL99H%0D%0ASWo-UMimq2_y7M7fD98oaOFSf_jumZVsMIJ4K0tVsP91tZmitHXygO_XFbNEkfj8syaptTMJX0OR%0D%0A_qHzZ9OYYKQHNMfbGdIOSaV0gifGdPSc-e3iTLgRo3nxecsn8_xhVFJrz7KkgKjCeI5iogyjxF_D%0D%0AIu_hgFkQL0XfVmvD_VIImsD6XnAjfzd_W-Bse1gDrlPTbVi0-EIWIvDojc4n0MDlzQLC_T23W4UK%0D%0A3T8nGzRXm3pxAHoEZxDl_RGDuGbAF1kKt8T_5hr3KSDy6Ybb3oc%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYnfB763quVIXrCnIYKv54V-NrhMRJeqofbnKSUoDf67AQOPA3ndvnH7rHfgHYbUwMm0xd-m3Of%0D%0AWSF0RGng0wO4hIg4fG8rT95pvIvF5o9Ef8OiykrSOuLveC8yHcq3dpf_XRuEkHc-XC1RZ7Jm06DF%0D%0A3rFBdqfVUTtkS5DW3GSo9zuqZqBzODtBt9nHJ8GE0r3nkao-xDmMCxHn_EYp26SC2g%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYjSfcLv77KCYtio0CzMNkr1HIPbplSM_G4zIHbXYlRYRIcl4JmkYSjVrIfZhNetCkKGzzq93it%0D%0Am8Epp9vBwh3YrZ2uAjKcqIG23fjR3qgdAnK4ds3p7xsLnOo-AAigAiqU1mPqgOqBaki9YnNJeUCI%0D%0AwuOOqywaUvDU7e4_H0pn4Hbb1gMb1G4bXsB1AcT0seS3%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYhpNbNFEzUOT8riFaJZw5VHpT0XZHCsDVdR_fOg_gh8nUy0E6calsO0PLPfJUO78x8F6mD_c1Z%0D%0ABz2PR-HMNtrug2SjOHOxim7l4P_xAMdhUHk1p2Oyt-pLIJMpmo5N5LpKbqXCjG7izm-XQm2gGaBO%0D%0AP-Xq_HCuCD9beV951o5u-bdbWq4izll4lxMD3e_1ecld%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top Rated Movies</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Box Office</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li><a href="/advertising/?ref_=ft_ad"
>Advertising</a></li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYgzyFvxP-QUwh8uvVICHnBlFDTcja_IrmYPoYqbk8MFPsRQByhlG6fYZvJU8pdv0gGrnTGA44x%0D%0AHSf9SMn-lPTQKt17kU1fS5vkUd7NRtHwQcPl4x6W1d2dfDIon8cJImzNQx07R8u6keEMo3TJNDtf%0D%0A73D_-YNx6hStFA-H2WxW3f9iYLSQw-qPHOwZPEEnJmLI_xtVNcKi-wBrj-Yxb4TdbQ%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYne6UQBU31iD03IsfYFzOFSGUOTAugXC1tm1DNZdZfDNTsqYF0hIqMIHFSoX4HKEzexWZK9xpP%0D%0AVEoEXSpFTuK5tUmtZqEWrbSkKve5dly5NPvi8q-tRbvDaMBDY-bVxLF5mGmPgdT9sYIp2olbZyMs%0D%0Ack9-2CyqMjEqrrc9fNVt9vFdohxEWYT9DkPdN-ZsSYd3%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYtRFVlozjH1ZAjjIau_vwNguezjXCePcEpLO-BrpF82JtI1N9e9Lw_sDh37ss23slLh0rmNRMv%0D%0ABOBHyu6MO9naNtuzCP-vHsXKtCNJ5rVTd7KiWL1Nr5OxSsasbmItD0dyQ4X0mNlLpbfsA7GTucy3%0D%0A6gjmiiMJ4pX6jVjEOiXh5O2fuFKSp5pyAZAPi4vBat6Jt42DqEulvso8YbZdeNCWX0tPx_qEtAI5%0D%0ARPTkOoibyVdLmi-xxH8yEZp8xoMLdHID%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2016
                <a
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYsroENreZ3pZU_wt5K_Z0pVxuzoL5M76gmUZJPfv4fhnmFhTc4eRc_Bm5Op8MJJmw2YQnGhoYG%0D%0AkJUm-FUbEp-FR1C0itMk1vpmwBK56Ew4xTUCkvbEjpuMa9OjbphqFjk6gddPLxSJ49QyAvCQFxCZ%0D%0Aj20_9mTT0oqyfOfvIhtjLAyjisI4vDFjhHYRYpLiLApQ94waaEfilv0hSl0zpqsrijo8buT1yMXS%0D%0Ah9p0khOWO18%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYru8vpY1jh-CkRLhohACrE0j2IBG3mmHHXBCRl-t0bjBtioXSgnW1WKnrS3x9cBXWmY0Cej53m%0D%0AA9vzi4xT-6w0_tvWM5FKufwuiRaE0M-VyqfhuetXQ3078kjOBDJM_w0pgJNEZjMBhLu3TD3irK6W%0D%0AEocj6T3U_iEsgBR4m5Tu4FqMHwirIQVYt-mXI8rGlrjxj4szElfVROIBUv0Aw0wfeoLCNdxmTm9C%0D%0AcKeGh_y7dD4%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYhgS3fXW8Toe1YOP7SBLeEE4OTLwcN0CKKY2VvdhxOKaUdc2otNEXmq7I37dOjrBHv-5uaADxG%0D%0A38BVzv5MyrDr8jEL3nbNLF_HvUpJWrq7UEAsLyu-JJjhlnjJi97m3fIBAdEupl19WDeRcHcLSKhH%0D%0A3t5mygv4Pl9ylJDGjqRvTlQR-RHbWGETNOjIt04l_ZkLrSQpDA6l0_P2P55yY_JvrfWkW-jEaSU0%0D%0AQvUAPVmKLso%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYh1gXq87xOEqxyMQDJZDljYkvO7OMDIoBCEQBTmBcTuj8dsRMXqr49JHqMXy95NsRAHUu6qjEE%0D%0APkwzymw5rPDdA1L4X5isBh0NmrQCXdyz1HzxkV-wMTgSLkjCi4h2G53w7iTmT_D0ljYfwxj_df3d%0D%0Ayz79ID-Vgl279_EE_KUB2a-Snlj2kZcRtk9rYFP5Lo0b4UzoeZ8CzlvVRHcnbX1tMZDm7x7ikgdE%0D%0AGL9ztZCPEO4%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYk3nKjVUuc7Jld1gWblGqEEQOhbcHJPYKPjOBe51jnwLt9cDPT29a_Hc68-duDflIF0-2fowon%0D%0AKYJLtVua97a_B4fX6X_gmWt0ikeo0nx_O0p-TbO5ZZtW7Ay65h1U_IW5eLJT2yCqr-hZkjDC1Cxi%0D%0AkmjNDeOXS6nm0zC89b-1QoDVphAUPxX_4dTglETlA3QI_BcAAAJe75Lja-C54_g129FTHPcObxUl%0D%0AFWMEo_erlsM%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYrtR9p0DRO0z84tf_uB8vEwdHErIDmDnm-9gZPE-yPnppvk3iY8gP_meKMshRvBLhm_QJy1vdo%0D%0Al-yFKMH8CF1AwMdFNzQmApDP5SS5eXJdlxgqUaMbET8smEPbKimb5MzIMDSsM3BaE4NfjADeR2g8%0D%0A2ZoQ5tzCDOHjcxELo0CipvxGY4nUtfRaD_TsFycpnODUJk8IBtpDxtfVNKJ_Pq30swscHu5d90yI%0D%0AoJIvTh1Kq_7IPAACUhYl9MvxpcY6koTV%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYlwDO41FeVreVsEpIbHul57GQTw7TerIjBi3t8fNQNk_AsdhDpeAGKgcvcSN8q9to7qDN4RGrI%0D%0AdHqj96Vfm6R1YzAQZM-LTxhYgup03vzUVx5R5mpEwSc7LR_t-Bt8_A_Do2Ob1cd_h9BO0HqYKi15%0D%0A37vQcu18S9_rCEyr8ntKOPs%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYhRHnmtRWJOrSvrhBnc8XehLvYqvwCfyu20hHZjpdqzQ5oHpvD9xZt_4FByWuBZxIwNBkIiEXM%0D%0Ah1o1NIxqfxEWtWF_KoY9efU9S-JDn8ZNl8DfiB1Av0au_riPS3zSAbxpshUrG1fsRcyZggx5fCss%0D%0ASiALysGppeWz-oB1X-Akb4Y%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/common-3617413113._CB300284919_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-2635043045._CB299003638_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-335260898._CB289148898_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-194820129._CB286493173_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-2126106747._CB289323193_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=010143fd484681152149b9bdc71ea4ee60eb34b236684d963ab5597dc2428c4b34d7",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=905984553842"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-232398347._CB349580400_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=905984553842&ord=905984553842";
    },"unable to request AAN pixel");
</script>

<div id="flashContent" style="width: 0px; height: 0px; overflow:hidden;">
    <script type="text/javascript">
        if (generic && generic.monitoring.record_metric && flashAdUtils.canPlayFlash) {
            var flashLoaded = 0;
            function swfLoaded() {
                flashLoaded = 1;
                generic.monitoring.record_metric('ads_flash_did_play', 1, true);
            }
            window.onload = function() {
                // Flash was not loaded
                if (flashLoaded === 0) {
                    // Flash could be loaded but wasn't
                    if (flashAdUtils.canPlayFlash()) {
                        generic.monitoring.record_metric('ads_flash_can_but_did_not_play', 0, true);
                    } else {
                        generic.monitoring.record_metric('ads_flash_cannot_play', 0, true);
                    }
                }
            }
            /**
             * There is a rare possibility of window.onload getting called before swfLoaded.
             * Firing this metric to keep track of such calls.
             */
             generic.monitoring.record_metric('ads_flash_page_loaded', 1, true);
         }
    </script>
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="1x1" align="middle">
        <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
        <param name="quality" value="high" />
        <param name="play" value="true" />
        <param name="loop" value="true" />
        <param name="wmode" value="transparent" />
        <param name="scale" value="showall" />
        <param name="menu" value="true" />
        <param name="devicefont" value="false" />
        <param name="salign" value="" />
        <param name="allowScriptAccess" value="always" />

        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" width="1" height="1">
            <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
            <param name="quality" value="high" />
            <param name="play" value="true" />
            <param name="loop" value="true" />
            <param name="wmode" value="transparent" />
            <param name="scale" value="showall" />
            <param name="menu" value="true" />
            <param name="devicefont" value="false" />
            <param name="salign" value="" />
            <param name="allowScriptAccess" value="always" />
        </object>
        <!--<![endif]-->
    </object>
</div>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="252"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
