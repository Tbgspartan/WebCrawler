<!DOCTYPE html>
<html id="atomic" lang="en-US" class="atomic my3columns  l-out Pos-r https fp fp-v2 rc1 fp-default mini-uh-on viewer-right ltr desktop Desktop bkt201">
<head>
    <title>Yahoo</title><meta http-equiv="x-dns-prefetch-control" content="on"><link rel="dns-prefetch" href="//s.yimg.com"><link rel="preconnect" href="//s.yimg.com"><link rel="dns-prefetch" href="//y.analytics.yahoo.com"><link rel="preconnect" href="//y.analytics.yahoo.com"><link rel="dns-prefetch" href="//geo.query.yahoo.com"><link rel="preconnect" href="//geo.query.yahoo.com"><link rel="dns-prefetch" href="//csc.beap.bc.yahoo.com"><link rel="preconnect" href="//csc.beap.bc.yahoo.com"><link rel="dns-prefetch" href="//geo.yahoo.com"><link rel="preconnect" href="//geo.yahoo.com"><link rel="dns-prefetch" href="//comet.yahoo.com"><link rel="preconnect" href="//comet.yahoo.com">    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta name="description" content="News, email and search are just the beginning. Discover more every day. Find your yodel.">
    <meta name="keywords" content="yahoo, yahoo home page, yahoo homepage, yahoo search, yahoo mail, yahoo messenger, yahoo games, news, finance, sport, entertainment">
    <meta property="og:title" content="Yahoo" />
    <meta property="og:type" content='website' />
    <meta property="og:url" content="http://www.yahoo.com" />
    <meta property="og:description" content="News, email and search are just the beginning. Discover more every day. Find your yodel."/>
    <meta property="og:image" content="https://s.yimg.com/dh/ap/default/130909/y_200_a.png"/>
    <meta property="og:site_name" content="Yahoo" />
    <meta property="fb:app_id" content="90376669494" />
    <meta name="format-detection" content="telephone=no" />
    <link rel="icon" sizes="any" mask href="/sy/os/mit/media/p/common/images/favicon_new-7483e38.svg">
<meta name="theme-color" content="#400090">
    <link rel="shortcut icon" href="/sy/rz/l/favicon.ico" />
    <link rel="canonical" href="https://www.yahoo.com/" />        <link href="/sy/os/fp/atomic-css.249a47d3.css" rel="stylesheet" type="text/css">
            
    
    
    
    <!-- streaming unlocked -->

    <!-- MapleTop -->
    
    
<link rel="stylesheet" type="text/css" href="/sy/zz/combo?nn/lib/metro/g/myy/advance_base_rc4_0.0.35.css&nn/lib/metro/g/myy/font_rc4_spdy_0.0.33.css&nn/lib/metro/g/myy/yahoo20_grid_0.0.99.css&nn/lib/metro/g/myy/video_styles_0.0.23.css&nn/lib/metro/g/myy/advance_color_0.0.7.css&nn/lib/metro/g/theme/viewer_modal_center_0.0.8.css&nn/lib/metro/g/theme/yglyphs-legacy_0.0.5.css&nn/lib/metro/g/sda/fp_sda_0.0.4.css&nn/lib/metro/g/sda/sda_advance_0.0.8.css&nn/lib/metro/g/fpfooter/advance_0.0.4.css&/os/stencil/3.1.0/styles-ltr.css&/os/yc/css/bundle.c60a6d54.css" />
    <link rel="search" type="application/opensearchdescription+xml" href="https://search.yahoo.com/opensearch.xml" title="Yahoo Search" />
    
    <script>
    var myYahoostartTime = new Date(),
        afPerfHeadStart=new Date().getTime(),
        ie;

    
    document.documentElement.className += ' JsEnabled jsenabled';</script>
    

<style>.breakingnews.gradient-1 {
    background: #ff7617; /* Old browsers */
    background: -moz-linear-gradient(45deg,  #ff7617 0%, #ff9b00 65%); /* FF3.6+ */
    background: -webkit-gradient(linear, left bottom, right top, color-stop(0%,#ff7617), color-stop(65%,#ff9b00)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(45deg,  #ff7617 0%,#ff9b00 65%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(45deg,  #ff7617 0%,#ff9b00 65%); /* Opera 11.10+ */
    background: -ms-linear-gradient(45deg,  #ff7617 0%,#ff9b00 65%); /* IE10+ */
    background: linear-gradient(45deg,  #ff7617 0%,#ff9b00 65%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff7617', endColorstr='#ff9b00',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
}

.breakingnews.gradient-2 {
    background: #e91857; /* Old browsers */
    background: -moz-linear-gradient(45deg,  #e91857 0%, #ff353c 65%); /* FF3.6+ */
    background: -webkit-gradient(linear, left bottom, right top, color-stop(0%,#e91857), color-stop(65%,#ff353c)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(45deg,  #e91857 0%,#ff353c 65%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(45deg,  #e91857 0%,#ff353c 65%); /* Opera 11.10+ */
    background: -ms-linear-gradient(45deg,  #e91857 0%,#ff353c 65%); /* IE10+ */
    background: linear-gradient(45deg,  #e91857 0%,#ff353c 65%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e91857', endColorstr='#ff353c',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
}</style>
<style>.js-stream-adfdb-reason {
    display: none;
}
.js-stream-adfdb-options .js-stream-adfdb-other-selected + .js-stream-adfdb-reason {
    display: block;
}

.stream-collapse {
    max-height: 0;
    -webkit-transition: max-height 0.3s ease;
    -moz-transition: max-height 0.3s ease;
    -o-transition: max-height 0.3s ease;
    transition: max-height 0.3s ease;
}

.RevealNested-on .ActionDislike {
    display: none;
}

.streamv2 .stream-share-open .js-stream-share-panel {
    height: auto !important;
    opacity: 1 !important;
}

/**
* Tooltips
*/
.js-stream-actions a:hover .ActionTooltip.hide,
.js-stream-actions .ActionTooltip,
.js-stream-actions a:active .ActionTooltip,
.js-stream-actions a:focus .ActionTooltip {
    clip: rect(1px 1px 1px 1px);
    clip: rect(1px,1px,1px,1px);
    height: 1px;
    width: 1px;
    overflow: hidden;
    line-height: 1.4;
    white-space: nowrap
}

.js-stream-actions a:hover .ActionTooltip {
    clip: rect(auto auto auto auto);
    clip: auto;
    height: auto;
    width: auto;
    overflow: visible;
    -webkit-transform: translateX(-50%) !important;
    -ms-transform: translateX(-50%) !important;
    transform: translateX(-50%) !important;
    *min-width: 80px;
    *margin-right: -48px;
    width: 136px;
    text-align: center;
}

.streamv2 .js-stream-dense .strm-left {
    max-width: 190px;
    width: 29%;
}

.streamv2 .js-stream-sparse .strm-left {
    width: 33%;
    max-width: 230px;
}
.streamv2 .js-stream-sparse .strm-right {
    width: 57%;
}
.streamv2 .js-stream-sparse .strm-full {
    width: 90%;
}

.streamv2 .js-stream-dense .strm-gs-tile-roundup .strm-left {
    width: 62.825%;
    max-width: 441px;
}

.streamv2 .js-stream-dense .strm-right-menu-roundup .strm-left {
    width: 72%;
    max-width: 508px;
}

.streamv2 .js-stream-dense .strm-right-menu-roundup .strm-right,
.streamv2 .js-stream-dense .strm-gs-tile-roundup .strm-right {
    width: auto;
    max-width: none;
}


.streamv2 .strm-right-menu-roundup .strm-right .js-stream-content-link:before {
    content: '';
    vertical-align: middle;
    height: 100%;
    display: inline-block;
}

.streamv2 .strm-chevron {
    display: none;
}

.streamv2 .strm-right-menu-roundup .js-stream-content-link.selected .strm-chevron,
.streamv2 .strm-right-menu-roundup .js-stream-content-link:hover .strm-chevron {
    display: block;
}

.streamv2 .rounded-img {
    border-radius: 3px;
}
.streamv2 .js-stream-sparse .storyline-main-img .rounded-img {
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
}
.streamv2 .js-stream-sparse .storyline-img-0 {
    border-bottom-left-radius: 3px;
}
.streamv2 .js-stream-sparse .storyline-img-1 {
    border-bottom-right-radius: 3px;
}

.ua-ff#atomic .strm-headline-label {
    margin-bottom: 4px;
}
/* cluster image gradient transparent to dark overlay */
.streamv2 .strm-img-gradient {
    background: -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0) 32%, rgba(0,0,0,0.65) 97%, rgba(0,0,0,0.65) 98%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(32%,rgba(0,0,0,0)), color-stop(97%,rgba(0,0,0,0.65)), color-stop(98%,rgba(0,0,0,0.65))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 32%,rgba(0,0,0,0.65) 97%,rgba(0,0,0,0.65) 98%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 32%,rgba(0,0,0,0.65) 97%,rgba(0,0,0,0.65) 98%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 32%,rgba(0,0,0,0.65) 97%,rgba(0,0,0,0.65) 98%); /* IE10+ */
    background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 32%,rgba(0,0,0,0.65) 97%,rgba(0,0,0,0.65) 98%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#a6000000',GradientType=0 ); /* IE6-9 */
}

/* cluster image gradient transparent to dark overlay */
.streamv2 .js-stream-roundup .strm-img-gradient {
    background: -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0) 52%, rgba(0,0,0,0.85) 107%, rgba(0,0,0,0.85) 78%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(52%,rgba(0,0,0,0)), color-stop(107%,rgba(0,0,0,0.85)), color-stop(78%,rgba(0,0,0,0.85))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 52%,rgba(0,0,0,0.85) 107%,rgba(0,0,0,0.85) 78%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 52%,rgba(0,0,0,0.85) 107%,rgba(0,0,0,0.85) 78%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 52%,rgba(0,0,0,0.85) 107%,rgba(0,0,0,0.85) 78%); /* IE10+ */
    background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 52%,rgba(0,0,0,0.85) 107%,rgba(0,0,0,0.85) 78%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#a6000000',GradientType=0 ); /* IE6-9 */
}

.streamv2 .strm-upsell-gradient-right {
    background: -moz-linear-gradient(right,  rgba(255,255,255,0) 0%, rgba(255,255,255,0.75) 85%, rgba(255,255,255,0.85) 98%); /* FF3.6+ */
    background: -webkit-linear-gradient(right,  rgba(255,255,255,0) 0%,rgba(255,255,255,0.75) 85%,rgba(255,255,255,0.85) 98%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(right,  rgba(255,255,255,0) 0%,rgba(255,255,255,0.75) 85%,rgba(255,255,255,0.85) 98%); /* Opera 11.10+ */
    background: -ms-linear-gradient(right,  rgba(255,255,255,0) 0%,rgba(255,255,255,0.75) 85%,rgba(255,255,255,0.85) 98%); /* IE10+ */
    background: linear-gradient(to right, rgba(255,255,255,0) 0%,rgba(255,255,255,0.75) 85%,rgba(255,255,255,0.85) 98%); /* W3C */
}
.streamv2 .strm-upsell-gradient-left {
    background: -moz-linear-gradient(left,  rgba(255,255,255,0) 0%, rgba(255,255,255,0.75) 85%, rgba(255,255,255,0.85) 98%); /* FF3.6+ */
    background: -webkit-linear-gradient(left,  rgba(255,255,255,0) 0%,rgba(255,255,255,0.75) 85%,rgba(255,255,255,0.85) 98%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(left,  rgba(255,255,255,0) 0%,rgba(255,255,255,0.75) 85%,rgba(255,255,255,0.85) 98%); /* Opera 11.10+ */
    background: -ms-linear-gradient(left,  rgba(255,255,255,0) 0%,rgba(255,255,255,0.75) 85%,rgba(255,255,255,0.85) 98%); /* IE10+ */
    background: linear-gradient(to left, rgba(255,255,255,0) 0%,rgba(255,255,255,0.75) 85%,rgba(255,255,255,0.85) 98%); /* W3C */
}

.ua-ie10 .streamv2 .js-stream-related-content ul,
.ua-ie10 .streamv2 .js-stream-roundup-filmstrip > ul {
    margin-right: -7px !important;
}

.ua-ie10 .streamv2 .js-stream-related-content li,
.ua-ie10 .streamv2 .js-stream-roundup-filmstrip .W\(25\%\) {
    width: 24.5% !important;
}

.streamv2 .js-stream-side-buttons li:nth-child(1) .ActionTooltip {
    top: -1px;
}
.streamv2 .js-stream-side-buttons.has-comments li:nth-child(1) .ActionTooltip {
    top: 0px;
}

.streamv2 .js-stream-side-buttons li:nth-child(2) .ActionTooltip {
    top: 22px;
}
.streamv2 .js-stream-side-buttons.has-comments li:nth-child(2) .ActionTooltip {
    top: 40px;
}
.streamv2 .js-stream-side-buttons li:nth-child(3) .ActionTooltip,
.streamv2 .js-stream-side-buttons li:nth-child(3) .js-stream-share-panel {
    top: 45px;
}
.streamv2 .js-stream-side-buttons.has-comments li:nth-child(3) .ActionTooltip,
.streamv2 .js-stream-side-buttons.has-comments li:nth-child(3) .js-stream-share-panel {
    top: 64px;
}
.streamv2 .js-stream-side-buttons li:nth-child(4) .js-stream-share-panel {
    top: 86px;
}

.streamv2 .js-stream-side-buttons .ActionComments {
    margin-top: 6px;
}
.streamv2 .js-stream-side-buttons.has-comments .ActionComments {
    margin-top: 0px;
}
.streamv2 .js-stream-side-buttons .ActionComments:hover .C\(\$c_icon\),
.streamv2 .js-stream-side-buttons .ActionComments:hover .C\(\#96989f\),
.streamv2 .js-stream-drawer .js-stream-comments-button:hover .C\(\$c_icon\),
.streamv2 .js-stream-drawer .js-stream-comments-button:hover .C\(\#96989f\) {
    color: inherit !important;
}
.streamv2 .js-stream-side-buttons .ActionComments .ActionTooltip {
    margin-top: 15px;
}

.js-stream-comment-counter-update {
    position: absolute;
    top: 0;
    right: 0;
    width: 100%;
}

/* Only set opacity to 0 for animation when js is enabled and css3 supported */
.JsEnabled .streamv2 .js-stream-comment-hidden:nth-of-type(1n) {
    opacity: 0;
}

.JsEnabled .streamv2 .animated {
    -webkit-animation-duration: 1.5s;
    animation-duration: 1.5s;
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;
}

@-webkit-keyframes fadeOut {
    from {
        opacity: 1;
    }
    to {
        opacity: 0;
    }
}

@keyframes fadeOut {
    from {
        opacity: 1;
    }
    to {
        opacity: 0;
    }
}

.JsEnabled .streamv2 .fadeOut {
    -webkit-animation-name: fadeOut;
    animation-name: fadeOut;
}

@-webkit-keyframes fadeIn {
    from {
        opacity: 0;
        color: #188fff;
    }
    to {
        opacity: 1;
        color: #96989f;
    }
}

@keyframes fadeIn {
    from {
        opacity: 0;
        color: #188fff;
    }
    to {
        opacity: 1;
        color: #96989f;
    }
}

@-webkit-keyframes fadeInNtk {
    from {
        opacity: 0;
        color: #188fff;
    }
    to {
        opacity: 1;
        color: #fff;
    }
}

@keyframes fadeInNtk {
    from {
        opacity: 0;
        color: #188fff;
    }
    to {
        opacity: 1;
        color: #fff;
    }
}

.JsEnabled .streamv2 .fadeIn {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
}


.JsEnabled .streamv2 .js-stream-roundup-filmstrip .fadeIn {
    -webkit-animation-name: fadeInNtk;
    animation-name: fadeInNtk;
}

.streamv2 .js-stream-comment .js-stream-cmnt-up:hover,
.streamv2 .js-stream-comment .js-stream-cmnt-up.selected {
    color: #1AC567 !important;
}
.streamv2 .js-stream-comment .js-stream-cmnt-down:hover,
.streamv2 .js-stream-comment .js-stream-cmnt-down.selected,
.streamv2 .js-stream-comment .js-stream-cmnt-flag:hover,
.streamv2 .js-stream-comment .js-stream-cmnt-flag.selected {
    color: #F0162F !important;
}

/* to show the dislike ad tooltip */
#atomic .Collapse-opened .js-stream-related-item-ad {
    overflow: visible;
}

.js-stream-upsell-carousel .js-stream-upsell-left.Disabled,
.js-stream-upsell-carousel .js-stream-upsell-right.Disabled,
.js-stream-upsell-carousel .strm-upsell-gradient-left.Disabled,
.js-stream-upsell-carousel .strm-upsell-gradient-right.Disabled {
    display: none;
}
.js-stream-upsell-carousel .js-stream-upsell-left,
.js-stream-upsell-carousel .js-stream-upsell-right {
    opacity: 0;
}
.js-stream-upsell-carousel:hover .js-stream-upsell-left,
.js-stream-upsell-carousel:hover .js-stream-upsell-right {
    opacity: 0.7;
}
.js-stream-upsell-carousel .js-stream-upsell-left:hover,
.js-stream-upsell-carousel .js-stream-upsell-right:hover {
    opacity: 1;
}
</style>
 

<style>.js-activitylist-item .orb {
    background-position: 0 0;
    width: 80px;
    height: 80px;
}

.js-activitylist-item .hexagon {
    background-position: -81px 0;
    width: 80px;
    height: 90px;
}

.js-activitylist-item .ribbon {
    background-position: -162px 0;
    width: 91px;
    height: 31px;
}
</style>
 

<style>#uh-search .yui3-aclist {
    width: inherit !important;
}
#uh-search .yui3-aclist-content {
    background-color: #fff;
    text-align: left;
    border: 1px solid #ccc;
    margin-top: 1px;
    border-top: 0;
}
#uh-search .yui3-aclist-list {
    margin: 0;
    line-height: 1.1;
}
#uh-search .yui3-aclist-item {
    padding: 5px 0 6px 0;
    font-size: 18px;
    font-weight: bold;
}
#uh-search .yui3-aclist-item:hover,
#uh-search .yui3-aclist-item-active {
    background-color: #c6d7ff;
}
#uh-search .yui3-aclist-item {
    padding-left: 10px;
    padding-right: 10px;
}
#uh-search .yui3-highlight {
    font-weight: 200;
}
#uh-search-box::-ms-clear {
    display: none;
}


/* Instant Search Styles */

#uh-search .yui3-aclist-item {
    line-height: 0.9;
}
#InstantSearchMask {
    visibility: hidden;
    opacity: 0;
    transition: visibility 0s linear 0.15s, opacity 0.15s linear;
}
.iSearch-display .Col1,
.iSearch-display .Col2,
.iSearch-display .Col3 {
    height: 0;
    overflow: hidden;
}
.iSearch-display #InstantSearch {
    visibility: visible;
}
.iSearch-mask #InstantSearchMask {
    visibility: visible;
    opacity: 1;
    transition: visibility 0s linear, opacity 0.15s linear;
}
.iSearch-loading.iSearch-mask #InstantSearchMask,
.iSearch-loading #InstantSearchMask {
    visibility: visible;
    opacity: .7;
}


/* Firefox 28 and below fix */
#uh-search-box,
#uh-ghost-box,
.instant-filters,
.instant-results {
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
/* ----------------------- */
</style>
 

<style>@charset "UTF-8";.Lb-Close-Icon,.icon-share-close,.icons-arrow,.icons-slideshow-icon{background-color:transparent;background-image:url(/sy/os/publish-images/news/2014-04-23/65fcff60-cb23-11e3-bead-55c0602d5659_icons-sb930b067ee.png);background-repeat:no-repeat}#Share .icon-share-close{width:30px;height:25px;background-size:cover;background-position:8px -72px}.Hl-Viewer{background:#fff}.Hl-Viewer .Card-Item .Lead-l~.Cover-Overlay{position:absolute;color:#fff;background:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodâ¦EiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);background:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4gPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGRlZnM+PGxpbmVhckdyYWRpZW50IGlkPSJncmFkIiBncmFkaWVudFVuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgeDE9IjAuNSIgeTE9IjAuMCIgeDI9IjAuNSIgeTI9IjEuMCI+PHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjAiLz48c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMwMDAwMDAiIHN0b3Atb3BhY2l0eT0iMC45NSIvPjwvbGluZWFyR3JhZGllbnQ+PC9kZWZzPjxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA==);background:-webkit-gradient(linear,50% 0,50% 100%,color-stop(0%,rgba(0,0,0,0)),color-stop(100%,rgba(0,0,0,.95)));background:-moz-linear-gradient(rgba(0,0,0,0),rgba(0,0,0,.95));background:-webkit-linear-gradient(rgba(0,0,0,0),rgba(0,0,0,.95));background:linear-gradient(rgba(0,0,0,0),rgba(0,0,0,.95))}.Hl-Viewer .Card-Item .Lead-l~.Cover-Overlay .Headline{text-shadow:0 1px 0 #000}.Hl-Viewer .Card-Item .Lead-l~.Cover-Overlay .Headline-Box{position:absolute}.Hl-Viewer .Content-Body{color:#000;font-weight:300;letter-spacing:.3px;word-wrap:break-word;word-break:break-word}.Hl-Viewer .Content-Body>p{margin-bottom:1.45em;margin-top:1.45em}.Hl-Viewer .Content-Body h1,.Hl-Viewer .Content-Body h2,.Hl-Viewer .Content-Body h3,.Hl-Viewer .Content-Body h4,.Hl-Viewer .Content-Body h5,.Hl-Viewer .Content-Body h6{font-weight:400}.Hl-Viewer .Content-Body h1{font-size:18px;font-size:1.8rem}.Hl-Viewer .Content-Body h2{font-size:17px;font-size:1.7rem}.Hl-Viewer .Content-Body h3{font-size:16px;font-size:1.6rem}.Hl-Viewer .Content-Body h4{font-size:15px;font-size:1.5rem}.Hl-Viewer .Content-Body h5{font-size:14px;font-size:1.4rem}.Hl-Viewer .Content-Body h6{font-size:13px;font-size:1.3rem}.Hl-Viewer .icons-slideshow-icon{left:5%;top:5%;height:46px;width:47px;background-size:cover}.Hl-Viewer blockquote{padding:5px 10px;quotes:"\201C" "\201C" "\201C" "\201C";line-height:18px}.Hl-Viewer blockquote:before{color:#ccc;content:open-quote;font-size:3em;line-height:.1em;vertical-align:-.4em}.Hl-Viewer blockquote p{display:inline;color:#747474;font-weight:400}.Hl-Viewer blockquote p:after{content:"\A";white-space:pre}.Hl-Viewer .Credit,.Hl-Viewer .Provider{letter-spacing:.5px}.JsEnabled .ImageLoader-Delayed,.JsEnabled .ImageLoader-Loaded{-moz-transition-duration:.2s;-o-transition-duration:.2s;-webkit-transition-duration:.2s;transition-duration:.2s;-moz-transition-property:opacity;-o-transition-property:opacity;-webkit-transition-property:opacity;transition-property:opacity;-moz-transition-timing-function:ease-out;-o-transition-timing-function:ease-out;-webkit-transition-timing-function:ease-out;transition-timing-function:ease-out}#Stencil .Bdrs-100{border-top-left-radius:100px;border-top-right-radius:100px;border-bottom-left-radius:100px;border-bottom-right-radius:100px}.Slideshow-Lightbox .lb-meta-content{padding-bottom:30px;background:#fff}.Slideshow-Lightbox .lb-meta-content .lb-meta-txt-container-full,.Slideshow-Lightbox .lb-meta-content.expand .lb-meta-txt-container-short{display:none}.Slideshow-Lightbox .lb-meta-content.expand .lb-meta-txt-container-full{display:block}.Slideshow-Lightbox .hide-meta .lb-meta-content{display:none}.Slideshow-Lightbox .Lb-Close-Icon{background-size:cover;width:47px;height:47px;z-index:202;background-position:0 -46px}.Reader-open .BrandBar,.Reader-open .rmp-TDYDBM{display:none}.Content-Body .Editorial-Left{float:left;margin-right:30px}.Content-Body .Editorial-Right{float:right;margin-left:30px}.Content-Body .Editorial-Left,.Content-Body .Editorial-Right{margin-top:0}#hl-viewer .Content-Col,.MagOn .Content-Col{margin-left:300px;margin-right:300px;min-height:100px}.HideCentralColumn .Content-Col{margin-left:150px;margin-right:150px}#hl-viewer{min-height:500px}.ShareBtns a:hover{opacity:.3;filter:alpha(opacity=33)}.modal-actions{bottom:3px}.modal-actions .ShareBtns a:hover{opacity:1;filter:alpha(opacity=100)}#hl-viewer:focus{outline:0}#hl-viewer .Timestamp{color:#abaeb7}#hl-viewer .Viewer-Close-Btn{position:fixed;top:10px;width:28px;height:28px;color:#fff;background-color:#2D1152;font-size:16px;margin-left:11px;z-index:5}#hl-viewer .ShareBtns .Share-Btn{border-radius:50px;width:17px;height:17px;line-height:1.3}#hl-viewer .ShareBtns .Share-Btn:hover{text-decoration:none}#hl-viewer .slideshow-carousel{background:#212124;padding-bottom:66%}#hl-viewer .lb-meta-content{border-bottom:1px solid #e8e8e8}#hl-viewer .lb-meta-caption-container{-webkit-font-smoothing:antialiased}#hl-viewer .lb-meta-txt-container.caption-scroll{max-height:4.3em;overflow-y:auto}#hl-viewer .viewer-wrapper{background:#fff;margin:0 0 30px 58px;min-width:980px}#hl-viewer .viewer-wrapper.mega-modal{min-height:750px}#hl-viewer .content-modal,#hl-viewer .js-viewer-slot-readMore{display:inline-block;width:640px;margin-right:29px;vertical-align:top}#hl-viewer .js-slider-item.first .content-modal{padding-top:0}#hl-viewer .Content-Body{padding-bottom:50px;border-bottom:1px solid #c8c8c8}#hl-viewer .Content-Body i{font-style:italic}#hl-viewer .js-slider-item.last .Content-Body{padding-bottom:0;border-bottom:none}#hl-viewer .Content-Body p:last-child{margin-bottom:0!important}#hl-viewer .js-slider-item.multirightrail .Content-Body.multirightrail{border-bottom:none}#hl-viewer .js-slider-item.multirightrail .content-modal{padding-top:0}#hl-viewer .js-slider-item.multirightrail{border-top:1px solid #B2B2B2;padding-top:50px}#hl-viewer .js-slider-item.multirightrail.first{padding-top:0;border-top:none}#hl-viewer .modal-aside.single{position:absolute;right:0;float:none;width:300px}#hl-viewer .modal-aside{display:inline-block;width:300px;position:relative;vertical-align:top;z-index:1}#hl-viewer .slideshow-carousel .main-col{padding-top:15px}#hl-viewer .index-count{color:#878c91;position:absolute;left:12px;bottom:7px;-webkit-font-smoothing:antialiased}#hl-viewer .js-lb-next,#hl-viewer .js-lb-prev{top:50%;z-index:3;width:28px;color:#878c91;font-size:30px;margin-top:-17px}#hl-viewer .js-lb-next:hover,#hl-viewer .js-lb-prev:hover{color:#fff}#hl-viewer .js-lb-next.Disabled,#hl-viewer .js-lb-prev.Disabled{display:none}#hl-viewer .js-lb-next:focus,#hl-viewer .js-lb-prev:focus{outline:0}#hl-viewer .slideshowv2 .slv2-carousel{padding-bottom:78%}#hl-viewer .slideshowv2 .slv2-carousel .main-col{padding-top:20px;padding-bottom:60px}#hl-viewer .slideshowv2 .slv2-carousel .js-thm-sl-icon{color:#fff;font-size:25px;top:12px;display:none}#hl-viewer .slideshowv2 .slv2-carousel .sl-thumb-wide .sl-slider-ct{background-color:rgba(33,33,35,.5);border-radius:10px}#hl-viewer .slideshowv2 .slv2-carousel .sl-thumb-wide .sl-car-mask{width:300px;opacity:1}#hl-viewer .slideshowv2 .slv2-carousel .sl-thumb-wide .js-thm-sl-icon{display:inline-block}#hl-viewer .slideshowv2 .slv2-carousel .sl-thumb-wide .sl-pic{width:52px;height:52px}#hl-viewer .slideshowv2 .js-lb-next,#hl-viewer .slideshowv2 .js-lb-prev{opacity:.8;background:#444;color:#fff;width:45px;height:45px;padding:0;border-radius:3px}#hl-viewer .slideshowv2 .js-lb-next:hover,#hl-viewer .slideshowv2 .js-lb-prev:hover{background:#333;opacity:1}#hl-viewer .slideshowv2 .index-count{bottom:inherit;left:inherit;color:#000;position:relative;font-size:19px;font-size:1.9rem;font-weight:500;margin-bottom:8px}#hl-viewer .slideshowv2 .slide-title{font-weight:700;margin-bottom:4px}#hl-viewer .slideshowv2 .lb-meta-txt-container a:hover,#hl-viewer .slideshowv2 .lb-meta-txt-container a:link,#hl-viewer .slideshowv2 .lb-meta-txt-container a:visited{color:#188fff}#hl-viewer .slideshowv2 .sl-thumbnails{bottom:10px}#hl-viewer .slideshowv2 .sl-slider-ct{width:350px}#hl-viewer .slideshowv2 .sl-car-mask{width:150px;opacity:.5;transition:height .2s}#hl-viewer .slideshowv2 .sl-carousel .Selected .sl-pic{border:2px solid #fff}#hl-viewer .slideshowv2 .sl-carousel .sl-pic{width:22px;height:22px;border:2px solid transparent}#hl-viewer .Headline{line-height:1.2;margin-bottom:12px;font-size:30px;font-size:3rem}#hl-viewer .Headline-Box{margin-bottom:10px}#hl-viewer .Off-Network{border:1px solid #188FFF;padding:8px 20px;color:#188FFF!important;font-weight:700;text-decoration:none}#hl-viewer .Off-Network:hover{color:#fff!important;background:#0078ff;border-color:#0078ff}#hl-viewer .Content-Body p,#hl-viewer .remaining-body p{margin-bottom:1.1em;margin-top:0}#hl-viewer .Content-Body p p,#hl-viewer .remaining-body p p{margin:0}#hl-viewer img.Mb-20+p{margin-top:0}#hl-viewer .Content-Body,#hl-viewer .remaining-body{font-weight:400;font-size:15px;font-size:1.55rem;line-height:1.5;font-family:"Helvetica Neue",Helvetica,Arial,sans-serif!important}#hl-viewer .Content-Body #photo_copyright,#hl-viewer .Content-Body .caption,#hl-viewer .Content-Body .credit,#hl-viewer .Content-Body .source,#hl-viewer .remaining-body #photo_copyright,#hl-viewer .remaining-body .caption,#hl-viewer .remaining-body .credit,#hl-viewer .remaining-body .source{font-size:11px;font-size:1.1rem;color:#b2b2b2}#hl-viewer h2,#hl-viewer h3{font-size:22px;font-size:2.2rem;margin-bottom:.6em}#hl-viewer h4,#hl-viewer h5,#hl-viewer h6{font-size:19px;font-size:1.9rem;margin-bottom:.6em}#hl-viewer .Embed-Img{margin-bottom:20px}#hl-viewer .image{margin-bottom:20px;display:block;line-height:1}#hl-viewer .image .Embed-Img{margin-bottom:0}#hl-viewer .sidekick.fp h4{font-size:13px;font-size:1.3rem;margin-bottom:0}#hl-viewer .twitter-tweet-rendered{margin:10px auto}#hl-viewer #photo_copyright{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif!important}#hl-viewer .alignright{float:right;margin:0 0 10px 10px}#hl-viewer .alignleft{float:left;margin:0 10px 10px 0}#hl-viewer .pmc-related-type{margin-right:10px}#hl-viewer blockquote{line-height:1.6}#hl-viewer p:empty{display:none}#hl-viewer .Ad-Viewer blockquote p,#hl-viewer .Hl-Viewer blockquote p{display:block}#hl-viewer .hl-ad-LREC{height:250px}#hl-viewer .source{font-size:14px;font-size:1.4rem}#hl-viewer .attribution{font-size:12px}#hl-viewer .first .Fixed-Header{top:-100px}#hl-viewer .Fixed-Header{transition:opacity .8s ease-in-out,top .6s ease-in-out;-webkit-transition:opacity .8s ease-in-out,top .6s ease-in-out;position:fixed;top:0;width:600px;padding:14px 0 0}#hl-viewer .Fixed-Header .source{max-width:540px}#hl-viewer .Fixed-Header .modal-actions{top:11px;left:540px;width:100px!important}#hl-viewer .Fixed-Header-Wrap{position:fixed;top:-100px;width:100%;z-index:4;height:48px;background-color:#fff;margin-left:-57px;border-bottom:1px solid #e5e5e5;transition:opacity .6s ease-in-out,top .6s ease-in-out;-webkit-transition:opacity .6s ease-in-out,top .6s ease-in-out;opacity:0;box-shadow:0 2px 4px -1px rgba(0,9,30,.1)}#hl-viewer .fadein{opacity:1}#hl-viewer .backfill-video-ads{width:300px;height:169px}#hl-viewer .backfill-video-ads h3{font-size:16px;font-size:1.6rem}#hl-viewer .backfill-video-ads h4{font-size:15px;font-size:1.5rem}#hl-viewer .backfill-video-ads .yvp-setting-btn{display:none}#hl-viewer .SidekickTV .list-view-item{display:inline-block;zoom:1;letter-spacing:normal;word-spacing:normal;text-rendering:auto;vertical-align:top}#hl-viewer .SidekickTV .SidekickTVArrow{border-bottom:40px solid transparent;border-left:40px solid}#hl-viewer .SidekickTV .ad-sponsored{color:#959595;font-size:11px;margin-right:0;padding-right:0}#hl-viewer .continue_reading .arraw,#hl-viewer .js-viewer-view-article .arraw{border-left:5px solid transparent;border-right:5px solid transparent;border-top:6px solid #000;vertical-align:middle}#hl-viewer .continue_reading:hover{color:#188FFF}#hl-viewer .continue_reading:hover .arraw{border-top-color:#188FFF}#hl-viewer .Mt-neg-40{margin-top:-40px}#hl-viewer .Mt-neg-30{margin-top:-30px}#hl-viewer .modal-sidekick-following .sidekick.fp{margin-top:-20px}#hl-viewer figure{margin:0}#hl-viewer iframe{border:none}#hl-viewer #viewer-end{height:1px;width:1px;display:block}#hl-viewer #viewer-end:focus{outline:0}.ShareBtns .Share-Btn{padding:5px;border:1px solid #abaeb7}.ShareBtns .Share-Btn:hover{opacity:1;filter:alpha(opacity=100)}.ShareBtns .Tumblr-Btn{color:#35506d}.ShareBtns .Tumblr-Btn:hover{color:#35506d;background-color:#3593d3}.ShareBtns .Facebook-Btn{color:#3b5998}.ShareBtns .Facebook-Btn:hover{color:#3b5998;background-color:#4e91f2}.ShareBtns .Twitter-Btn{color:#00aced}.ShareBtns .Twitter-Btn:hover{color:#00aced;background-color:#94e8ff}.ShareBtns .Mail-Btn{background-position:-1px 161px;color:#0a80e3}.ShareBtns .Mail-Btn:hover{color:#0a80e3;background-color:#94e8ff}.ShareBtns .StickyPholder{display:inline-block}.ShareBtns .Viewer-Close-Btn{background-position:0 185px;padding:5px;margin-left:40px}.ShareBtns .Viewer-Close-Btn.Sticky{margin-left:-19px}#ModalSticker{max-width:1180px}#ModalSticker.Sticky{display:block!important;opacity:1;filter:alpha(opacity=100)}#ModalSticker .content-modal{border-bottom-width:3px;border-color:#000}#ModalSticker .ShareBtns{position:relative;float:right;margin-top:5px}.Modal-Credit{max-width:200px}.video-stage{margin-right:240px}.video-side-stage{width:240px}.Nav-Start{display:none}.First-Item .Nav-Start{display:block}.First-Item .Nav-Next,.First-Item .Nav-Prev{display:none}.slide-nav-arrow{background-color:#009bfb}.slide-nav-arrow:hover{opacity:1;filter:alpha(opacity=100)}.modal-sidekick,.modal-sidekick-following{display:inline-block}.js-sidekick-container{border-top:1px solid #f1f1f5}.modal-tooltip:hover .modal-tooltip\:h_H\(a\){height:auto!important}.modal-tooltip:hover .modal-tooltip\:h_Op\(1\){opacity:1!important}.c-modal-red{color:#ff156f}.lrec-before-loading{width:300px;height:250px;border:1px solid #e0e4e9;-webkit-transition:height 1.5s;-moz-transition:height 1.5s;-ms-transition:height 1.5s;-o-transition:height 1.5s;transition:height 1.5s}.js-header-searchbox{display:none;position:absolute;height:30px;left:730px;top:9px;border-radius:2px}.js-header-searchbox .srch-input{width:268px;height:100%;line-height:inherit;vertical-align:top;outline:0;box-shadow:none;border:none;border:1px solid #b0b0b0;border-radius:2px 0 0 2px;padding:0 10px}.js-header-searchbox .srch-input:focus{border-color:#0179ff}.js-header-searchbox button{width:32px;height:100%;background-color:#0179ff;font-size:16px;font-weight:700;color:#fff;border-radius:0 2px 2px 0;margin-left:-4px}.Z-6{z-index:6}.Z-7{z-index:7}.Z-8{z-index:8}.Z-9{z-index:9}.fixZindex{z-index:5!important}.viewer-right .js-slider-item.multirightrail.first{margin-top:25px!important}.viewer-right .Viewer-Close-Btn{border-radius:20px!important;font-size:17px!important;height:37px!important;margin-left:-18px!important;top:87px!important;width:37px!important}.viewer-right .viewer-wrapper{border-radius:6px;margin:1px!important;padding:0 0 30px 40px!important}.viewer-right.stream-sparse .viewer-wrapper{top:-12px}@media screen and (min-width:1100px){.js-header-searchbox{display:inline-block}}.Pt-35{padding-top:35px}.Pt-7{padding-top:7px}.JsEnabled .ImageLoader-Delayed{background:#f5f5f5}</style>
 

<style>#applet_p_63794 .Bd-0{border:0}#applet_p_63794 .Bd-1{border-width:1px}#applet_p_63794 .Bdx-1{border-right-width:1px;border-left-width:1px}#applet_p_63794 .Bdy-1{border-top-width:1px;border-bottom-width:1px}#applet_p_63794 .Bd-t{border-top-width:1px}#applet_p_63794 .Bd-end{border-right-width:1px}#applet_p_63794 .Bd-b{border-bottom-width:1px}#applet_p_63794 .Bd-start{border-left-width:1px}#applet_p_63794 .Bdt-0{border-top:0}#applet_p_63794 .Bdend-0{border-right:0}#applet_p_63794 .Bdb-0{border-bottom:0}#applet_p_63794 .Bdstart-0{border-left:0}#applet_p_63794 .Bdrs-0{border-radius:0}#applet_p_63794 .Bdtrrs-0{border-top-right-radius:0}#applet_p_63794 .Bdtlrs-0{border-top-left-radius:0}#applet_p_63794 .Bdbrrs-0{border-bottom-right-radius:0}#applet_p_63794 .Bdblrs-0{border-bottom-left-radius:0}#applet_p_63794 .Bdrs-100{border-radius:100px}#applet_p_63794 .Bdtrrs-100{border-top-right-radius:100px}#applet_p_63794 .Bdtlrs-100{border-top-left-radius:100px}#applet_p_63794 .Bdbrrs-100{border-bottom-right-radius:100px}#applet_p_63794 .Bdblrs-100{border-bottom-left-radius:100px}#applet_p_63794 .Bdrs-300{border-radius:300px}#applet_p_63794 .Bdtrrs-300{border-top-right-radius:300px}#applet_p_63794 .Bdtlrs-300{border-top-left-radius:300px}#applet_p_63794 .Bdbrrs-300{border-bottom-right-radius:300px}#applet_p_63794 .Bdblrs-300{border-bottom-left-radius:300px}#applet_p_63794 .Bdrs{border-radius:3px}#applet_p_63794 .Bdtrrs{border-top-right-radius:3px}#applet_p_63794 .Bdtlrs{border-top-left-radius:3px}#applet_p_63794 .Bdbrrs{border-bottom-right-radius:3px}#applet_p_63794 .Bdblrs{border-bottom-left-radius:3px}#applet_p_63794 .Ff{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif}#applet_p_63794 .Ff-a{font-family:Georgia,"Times New Roman",serif}#applet_p_63794 .Ff-b{font-family:Helvetica,Arial,sans-serif}#applet_p_63794 .Ff-c{font-family:"Monotype Corsiva","Comic Sans MS",cursive}#applet_p_63794 .Ff-d{font-family:Capitals,Impact,fantasy}#applet_p_63794 .Ff-e{font-family:Monaco,"Courier New",monospace}#applet_p_63794 .Fz-0{font-size:0}#applet_p_63794 .Fz-3xs{font-size:7px}#applet_p_63794 .Fz-2xs{font-size:9px}#applet_p_63794 .Fz-xs{font-size:11px;}#applet_p_63794 .Fz-s{font-size:13px;}#applet_p_63794 .Fz-m{font-size:15px;}#applet_p_63794 .Bgc-t{background-color:transparent}#applet_p_63794 .Bgi-n{background-image:none}#applet_p_63794 .Bg-n{background:0 0}#applet_p_63794 .Bgcp-bb{background-clip:border-box}#applet_p_63794 .Bgcp-pb{background-clip:padding-box}#applet_p_63794 .Bgcp-cb{background-clip:content-box}#applet_p_63794 .Bgo-pb{background-origin:padding-box}#applet_p_63794 .Bgo-bb{background-origin:border-box}#applet_p_63794 .Bgo-cb{background-origin:content-box}#applet_p_63794 .Bgz-a{background-size:auto}#applet_p_63794 .Bgz-ct{background-size:contain}#applet_p_63794 .Bgz-cv{background-size:cover;background-position:50% 15%}#applet_p_63794 .Bdcl-c{border-collapse:collapse}#applet_p_63794 .Bdcl-s{border-collapse:separate}#applet_p_63794 .Bxz-cb{-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box}#applet_p_63794 .Bxz-bb{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}#applet_p_63794 .Bxsh-n{box-shadow:none}#applet_p_63794 .Cl-n{clear:none}#applet_p_63794 .Cl-b{clear:both}#applet_p_63794 .Cl-start{clear:left}#applet_p_63794 .Cl-end{clear:right}#applet_p_63794 .Cur-d{cursor:default}#applet_p_63794 .Cur-he{cursor:help}#applet_p_63794 .Cur-m{cursor:move}#applet_p_63794 .Cur-na{cursor:not-allowed}#applet_p_63794 .Cur-nsr{cursor:ns-resize}#applet_p_63794 .Cur-p{cursor:pointer}#applet_p_63794 .Cur-cr{cursor:col-resize}#applet_p_63794 .Cur-w{cursor:wait}#applet_p_63794 .Cur-a{cursor:auto!important}#applet_p_63794 .D-n{display:none}#applet_p_63794 .D-b{display:block}#applet_p_63794 .D-i{display:inline}#applet_p_63794 .D-ib{display:inline-block;*display:inline;zoom:1}#applet_p_63794 .D-tb{display:table}#applet_p_63794 .D-tbr{display:table-row}#applet_p_63794 .D-tbc{display:table-cell}#applet_p_63794 .D-li{display:list-item}#applet_p_63794 .D-ri{display:run-in}#applet_p_63794 .D-cp{display:compact}#applet_p_63794 .D-itb{display:inline-table}#applet_p_63794 .D-tbcl{display:table-column}#applet_p_63794 .D-tbclg{display:table-column-group}#applet_p_63794 .D-tbhg{display:table-header-group}#applet_p_63794 .D-tbfg{display:table-footer-group}#applet_p_63794 .D-tbrg{display:table-row-group}#applet_p_63794 .Fl-n{float:none}#applet_p_63794 .Fl-start{float:left;_display:inline}#applet_p_63794 .Fl-end{float:right;_display:inline}#applet_p_63794 .Fw-n{font-weight:400}#applet_p_63794 .Fw-b{font-weight:700;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}#applet_p_63794 .Fw-100{font-weight:100;*font-weight:normal}#applet_p_63794 .Fw-200{font-weight:200;*font-weight:normal}#applet_p_63794 .Fw-400{font-weight:400;*font-weight:normal}#applet_p_63794 .Fw-br{font-weight:bolder}#applet_p_63794 .Fw-lr{font-weight:lighter}#applet_p_63794 .Fs-n{font-style:normal}#applet_p_63794 .Fs-i{font-style:italic}#applet_p_63794 .Fv-sc{font-variant:small-caps}#applet_p_63794 .Fv-n{font-variant:normal}#applet_p_63794 .H-0{height:0}#applet_p_63794 .H-50{height:50%}#applet_p_63794 .H-100{height:100%}#applet_p_63794 .H-a{height:auto}#applet_p_63794 .H-n,#applet_p_63794 .h-n{-webkit-hyphens:none;-moz-hyphens:none;-ms-hyphens:none;hyphens:none}#applet_p_63794 .List-n{list-style-type:none}#applet_p_63794 .List-d{list-style-type:disc}#applet_p_63794 .List-c{list-style-type:circle}#applet_p_63794 .List-s{list-style-type:square}#applet_p_63794 .List-dc{list-style-type:decimal}#applet_p_63794 .List-dclz{list-style-type:decimal-leading-zero}#applet_p_63794 .List-lr{list-style-type:lower-roman}#applet_p_63794 .List-ur{list-style-type:upper-roman}#applet_p_63794 .Lisi-n{list-style-image:none}#applet_p_63794 .Lh-0{line-height:0}#applet_p_63794 .Lh-01{line-height:.1}#applet_p_63794 .Lh-02{line-height:.2}#applet_p_63794 .Lh-03{line-height:.3}#applet_p_63794 .Lh-04{line-height:.4}#applet_p_63794 .Lh-05{line-height:.5}#applet_p_63794 .Lh-06{line-height:.6}#applet_p_63794 .Lh-07{line-height:.7}#applet_p_63794 .Lh-08{line-height:.8}#applet_p_63794 .Lh-09{line-height:.9}#applet_p_63794 .Lh-1{line-height:1}#applet_p_63794 .Lh-11{line-height:1.1}#applet_p_63794 .Lh-12{line-height:1.2}#applet_p_63794 .Lh-125{line-height:1.25}#applet_p_63794 .Lh-13{line-height:1.3}#applet_p_63794 .Lh-14{line-height:1.4}#applet_p_63794 .Lh-15{line-height:1.5}#applet_p_63794 .Lh-16{line-height:1.6}#applet_p_63794 .Lh-17{line-height:1.7}#applet_p_63794 .Lh-18{line-height:1.8}#applet_p_63794 .Lh-19{line-height:1.9}#applet_p_63794 .Lh-2{line-height:2}#applet_p_63794 .Lh-21{line-height:2.1}#applet_p_63794 .Lh-22{line-height:2.2}#applet_p_63794 .Lh-23{line-height:2.3}#applet_p_63794 .Lh-24{line-height:2.4}#applet_p_63794 .Lh-25{line-height:2.5}#applet_p_63794 .Lh-3{line-height:3}#applet_p_63794 .Lh-reset{line-height:normal}#applet_p_63794 .M-0{margin:0}#applet_p_63794 .M-2{margin:2px}#applet_p_63794 .M-4{margin:4px}#applet_p_63794 .M-6{margin:6px}#applet_p_63794 .M-8{margin:8px}#applet_p_63794 .M-10{margin:10px}#applet_p_63794 .M-12{margin:12px}#applet_p_63794 .M-14{margin:14px}#applet_p_63794 .M-16{margin:16px}#applet_p_63794 .M-18{margin:18px}#applet_p_63794 .M-20{margin:20px}#applet_p_63794 .Mx-1{margin-right:1px;margin-left:1px}#applet_p_63794 .Mx-2{margin-right:2px;margin-left:2px}#applet_p_63794 .Mx-4{margin-right:4px;margin-left:4px}#applet_p_63794 .Mx-6{margin-right:6px;margin-left:6px}#applet_p_63794 .Mx-8{margin-right:8px;margin-left:8px}#applet_p_63794 .Mx-10{margin-right:10px;margin-left:10px}#applet_p_63794 .Mx-12{margin-right:12px;margin-left:12px}#applet_p_63794 .Mx-14{margin-right:14px;margin-left:14px}#applet_p_63794 .Mx-16{margin-right:16px;margin-left:16px}#applet_p_63794 .Mx-18{margin-right:18px;margin-left:18px}#applet_p_63794 .Mx-20{margin-right:20px;margin-left:20px}#applet_p_63794 .My-1{margin-top:1px;margin-bottom:1px}#applet_p_63794 .My-2{margin-top:2px;margin-bottom:2px}#applet_p_63794 .My-4{margin-top:4px;margin-bottom:4px}#applet_p_63794 .My-6{margin-top:6px;margin-bottom:6px}#applet_p_63794 .My-8{margin-top:8px;margin-bottom:8px}#applet_p_63794 .My-10{margin-top:10px;margin-bottom:10px}#applet_p_63794 .My-12{margin-top:12px;margin-bottom:12px}#applet_p_63794 .My-14{margin-top:14px;margin-bottom:14px}#applet_p_63794 .My-16{margin-top:16px;margin-bottom:16px}#applet_p_63794 .My-18{margin-top:18px;margin-bottom:18px}#applet_p_63794 .My-20{margin-top:20px;margin-bottom:20px}#applet_p_63794 .Mt-1{margin-top:1px}#applet_p_63794 .Mb-1{margin-bottom:1px}#applet_p_63794 .Mstart-1{margin-left:1px}#applet_p_63794 .Mend-1{margin-right:1px}#applet_p_63794 .Mt-2{margin-top:2px}#applet_p_63794 .Mb-2{margin-bottom:2px}#applet_p_63794 .Mstart-2{margin-left:2px}#applet_p_63794 .Mend-2{margin-right:2px}#applet_p_63794 .Mt-4{margin-top:4px}#applet_p_63794 .Mb-4{margin-bottom:4px}#applet_p_63794 .Mstart-4{margin-left:4px}#applet_p_63794 .Mend-4{margin-right:4px}#applet_p_63794 .Mt-6{margin-top:6px}#applet_p_63794 .Mb-6{margin-bottom:6px}#applet_p_63794 .Mstart-6{margin-left:6px}#applet_p_63794 .Mend-6{margin-right:6px}#applet_p_63794 .Mt-8{margin-top:8px}#applet_p_63794 .Mb-8{margin-bottom:8px}#applet_p_63794 .Mstart-8{margin-left:8px}#applet_p_63794 .Mend-8{margin-right:8px}#applet_p_63794 .Mt-10{margin-top:10px}#applet_p_63794 .Mb-10{margin-bottom:10px}#applet_p_63794 .Mstart-10{margin-left:10px}#applet_p_63794 .Mend-10{margin-right:10px}#applet_p_63794 .Mt-12{margin-top:12px}#applet_p_63794 .Mb-12{margin-bottom:12px}#applet_p_63794 .Mstart-12{margin-left:12px}#applet_p_63794 .Mend-12{margin-right:12px}#applet_p_63794 .Mt-14{margin-top:14px}#applet_p_63794 .Mb-14{margin-bottom:14px}#applet_p_63794 .Mstart-14{margin-left:14px}#applet_p_63794 .Mend-14{margin-right:14px}#applet_p_63794 .Mt-16{margin-top:16px}#applet_p_63794 .Mb-16{margin-bottom:16px}#applet_p_63794 .Mstart-16{margin-left:16px}#applet_p_63794 .Mend-16{margin-right:16px}#applet_p_63794 .Mt-18{margin-top:18px}#applet_p_63794 .Mb-18{margin-bottom:18px}#applet_p_63794 .Mstart-18{margin-left:18px}#applet_p_63794 .Mend-18{margin-right:18px}#applet_p_63794 .Mt-20{margin-top:20px}#applet_p_63794 .Mb-20{margin-bottom:20px}#applet_p_63794 .Mstart-20{margin-left:20px}#applet_p_63794 .Mend-20{margin-right:20px}#applet_p_63794 .Mt-30{margin-top:30px}#applet_p_63794 .Mb-30{margin-bottom:30px}#applet_p_63794 .Mstart-30{margin-left:30px}#applet_p_63794 .Mend-30{margin-right:30px}#applet_p_63794 .Mt-40{margin-top:40px}#applet_p_63794 .Mb-40{margin-bottom:40px}#applet_p_63794 .Mstart-40{margin-left:40px}#applet_p_63794 .Mend-40{margin-right:40px}#applet_p_63794 .Mt-50{margin-top:50px}#applet_p_63794 .Mb-50{margin-bottom:50px}#applet_p_63794 .Mstart-50{margin-left:50px}#applet_p_63794 .Mend-50{margin-right:50px}#applet_p_63794 .Mt-60{margin-top:60px}#applet_p_63794 .Mb-60{margin-bottom:60px}#applet_p_63794 .Mstart-60{margin-left:60px}#applet_p_63794 .Mend-60{margin-right:60px}#applet_p_63794 .Mt-70{margin-top:70px}#applet_p_63794 .Mb-70{margin-bottom:70px}#applet_p_63794 .Mstart-70{margin-left:70px}#applet_p_63794 .Mend-70{margin-right:70px}#applet_p_63794 .Mt-neg-1{margin-top:-1px}#applet_p_63794 .Mb-neg-1{margin-bottom:-1px}#applet_p_63794 .Mstart-neg-1{margin-left:-1px}#applet_p_63794 .Mend-neg-1{margin-right:-1px}#applet_p_63794 .Mt-neg-4{margin-top:-4px}#applet_p_63794 .Mb-neg-4{margin-bottom:-4px}#applet_p_63794 .Mstart-neg-4{margin-left:-4px}#applet_p_63794 .Mend-neg-4{margin-right:-4px}#applet_p_63794 .Mt-neg-6{margin-top:-6px}#applet_p_63794 .Mb-neg-6{margin-bottom:-6px}#applet_p_63794 .Mstart-neg-6{margin-left:-6px}#applet_p_63794 .Mend-neg-6{margin-right:-6px}#applet_p_63794 .Mt-neg-8{margin-top:-8px}#applet_p_63794 .Mb-neg-8{margin-bottom:-8px}#applet_p_63794 .Mstart-neg-8{margin-left:-8px}#applet_p_63794 .Mend-neg-8{margin-right:-8px}#applet_p_63794 .Mt-neg-10{margin-top:-10px}#applet_p_63794 .Mb-neg-10{margin-bottom:-10px}#applet_p_63794 .Mstart-neg-10{margin-left:-10px}#applet_p_63794 .Mend-neg-10{margin-right:-10px}#applet_p_63794 .Mt-neg-12{margin-top:-12px}#applet_p_63794 .Mb-neg-12{margin-bottom:-12px}#applet_p_63794 .Mstart-neg-12{margin-left:-12px}#applet_p_63794 .Mend-neg-12{margin-right:-12px}#applet_p_63794 .Mt-neg-14{margin-top:-14px}#applet_p_63794 .Mb-neg-14{margin-bottom:-14px}#applet_p_63794 .Mstart-neg-14{margin-left:-14px}#applet_p_63794 .Mend-neg-14{margin-right:-14px}#applet_p_63794 .Mt-neg-16{margin-top:-16px}#applet_p_63794 .Mb-neg-16{margin-bottom:-16px}#applet_p_63794 .Mstart-neg-16{margin-left:-16px}#applet_p_63794 .Mend-neg-16{margin-right:-16px}#applet_p_63794 .Mt-neg-18{margin-top:-18px}#applet_p_63794 .Mb-neg-18{margin-bottom:-18px}#applet_p_63794 .Mstart-neg-18{margin-left:-18px}#applet_p_63794 .Mend-neg-18{margin-right:-18px}#applet_p_63794 .Mt-neg-20{margin-top:-20px}#applet_p_63794 .Mb-neg-20{margin-bottom:-20px}#applet_p_63794 .Mstart-neg-20{margin-left:-20px}#applet_p_63794 .Mend-neg-20{margin-right:-20px}#applet_p_63794 .Mstart-50\%{margin-left:50%}#applet_p_63794 .M-a{margin:auto}#applet_p_63794 .Mx-a{margin-right:auto;margin-left:auto}#applet_p_63794 .\!Mx-a{margin-right:auto!important;margin-left:auto!important}#applet_p_63794 .Mstart-a{margin-left:auto}#applet_p_63794 .Mend-a{margin-right:auto}#applet_p_63794 .Mt-0,#applet_p_63794 .My-0{margin-top:0}#applet_p_63794 .Mb-0,#applet_p_63794 .My-0{margin-bottom:0}#applet_p_63794 .Mstart-0,#applet_p_63794 .Mx-0{margin-left:0}#applet_p_63794 .Mend-0,#applet_p_63794 .Mx-0{margin-right:0}#applet_p_63794 .Miw-0{min-width:0}#applet_p_63794 .Miw-10{min-width:10%}#applet_p_63794 .Miw-15{min-width:15%}#applet_p_63794 .Miw-20{min-width:20%}#applet_p_63794 .Miw-25{min-width:25%}#applet_p_63794 .Miw-30{min-width:30%}#applet_p_63794 .Miw-35{min-width:35%}#applet_p_63794 .Miw-40{min-width:40%}#applet_p_63794 .Miw-45{min-width:45%}#applet_p_63794 .Miw-50{min-width:50%}#applet_p_63794 .Miw-60{min-width:60%}#applet_p_63794 .Miw-70{min-width:70%}#applet_p_63794 .Miw-80{min-width:80%}#applet_p_63794 .Miw-90{min-width:90%}#applet_p_63794 .Miw-100{min-width:100%}#applet_p_63794 .Maw-n{max-width:none}#applet_p_63794 .Maw-10{max-width:10%}#applet_p_63794 .Maw-15{max-width:15%}#applet_p_63794 .Maw-20{max-width:20%}#applet_p_63794 .Maw-25{max-width:25%}#applet_p_63794 .Maw-30{max-width:30%}#applet_p_63794 .Maw-35{max-width:35%}#applet_p_63794 .Maw-40{max-width:40%}#applet_p_63794 .Maw-45{max-width:45%}#applet_p_63794 .Maw-50{max-width:50%}#applet_p_63794 .Maw-60{max-width:60%}#applet_p_63794 .Maw-70{max-width:70%}#applet_p_63794 .Maw-80{max-width:80%}#applet_p_63794 .Maw-90{max-width:90%}#applet_p_63794 .Maw-99{max-width:99%}#applet_p_63794 .Maw-100{max-width:100%}#applet_p_63794 .Mih-0{min-height:0}#applet_p_63794 .Mih-50{min-height:50%;_height:50%}#applet_p_63794 .Mih-60{min-height:60%;_height:60%}#applet_p_63794 .Mih-100{min-height:100%;_height:100%}#applet_p_63794 .Mah-n{max-height:none}#applet_p_63794 .Mah-0{max-height:0}#applet_p_63794 .Mah-50{max-height:50%}#applet_p_63794 .Mah-60{max-height:60%}#applet_p_63794 .Mah-100{max-height:100%}#applet_p_63794 .O-0{outline:0}#applet_p_63794 .T-0{top:0}#applet_p_63794 .B-0{bottom:0}#applet_p_63794 .Start-0{left:0}#applet_p_63794 .End-0{right:0}#applet_p_63794 .T-10{top:10%}#applet_p_63794 .B-10{bottom:10%}#applet_p_63794 .Start-10{left:10%}#applet_p_63794 .End-10{right:10%}#applet_p_63794 .T-25{top:25%}#applet_p_63794 .B-25{bottom:25%}#applet_p_63794 .Start-25{left:25%}#applet_p_63794 .End-25{right:25%}#applet_p_63794 .T-50{top:50%}#applet_p_63794 .B-50{bottom:50%}#applet_p_63794 .Start-50{left:50%}#applet_p_63794 .End-50{right:50%}#applet_p_63794 .T-75{top:75%}#applet_p_63794 .B-75{bottom:75%}#applet_p_63794 .Start-75{left:75%}#applet_p_63794 .End-75{right:75%}#applet_p_63794 .T-100{top:100%}#applet_p_63794 .B-100{bottom:100%}#applet_p_63794 .Start-100{left:100%}#applet_p_63794 .End-100{right:100%}#applet_p_63794 .T-a{top:auto!important}#applet_p_63794 .B-a{bottom:auto!important}#applet_p_63794 .Start-a{left:auto!important}#applet_p_63794 .End-a{right:auto!important}#applet_p_63794 .Op-0{opacity:0;filter:alpha(opacity=0)}#applet_p_63794 .Op-33{opacity:.33;filter:alpha(opacity=33)}#applet_p_63794 .Op-50{opacity:.5;filter:alpha(opacity=50)}#applet_p_63794 .Op-66{opacity:.66;filter:alpha(opacity=66)}#applet_p_63794 .Op-100{opacity:1;filter:alpha(opacity=100)}#applet_p_63794 .Ov-h{overflow:hidden;zoom:1}#applet_p_63794 .Ov-v{overflow:visible}#applet_p_63794 .Ov-s{overflow:scroll}#applet_p_63794 .Ov-a{overflow:auto}#applet_p_63794 .Ovs-t{-webkit-overflow-scrolling:touch}#applet_p_63794 .Ovx-v{overflow-x:visible}#applet_p_63794 .Ovx-h{overflow-x:hidden}#applet_p_63794 .Ovx-s{overflow-x:scroll}#applet_p_63794 .Ovx-a{overflow-x:auto}#applet_p_63794 .Ovy-v{overflow-y:visible}#applet_p_63794 .Ovy-h{overflow-y:hidden}#applet_p_63794 .Ovy-s{overflow-y:scroll}#applet_p_63794 .Ovy-a{overflow-y:auto}#applet_p_63794 .P-0{padding:0}#applet_p_63794 .P-1{padding:1px}#applet_p_63794 .P-2{padding:2px}#applet_p_63794 .P-4{padding:4px}#applet_p_63794 .P-6{padding:6px}#applet_p_63794 .P-8{padding:8px}#applet_p_63794 .P-10{padding:10px}#applet_p_63794 .P-12{padding:12px}#applet_p_63794 .P-14{padding:14px}#applet_p_63794 .P-16{padding:16px}#applet_p_63794 .P-18{padding:18px}#applet_p_63794 .P-20{padding:20px}#applet_p_63794 .P-30{padding:30px}#applet_p_63794 .Px-1{padding-right:1px;padding-left:1px}#applet_p_63794 .Px-2{padding-right:2px;padding-left:2px}#applet_p_63794 .Px-4{padding-right:4px;padding-left:4px}#applet_p_63794 .Px-6{padding-right:6px;padding-left:6px}#applet_p_63794 .Px-8{padding-right:8px;padding-left:8px}#applet_p_63794 .Px-10{padding-right:10px;padding-left:10px}#applet_p_63794 .Px-12{padding-right:12px;padding-left:12px}#applet_p_63794 .Px-14{padding-right:14px;padding-left:14px}#applet_p_63794 .Px-16{padding-right:16px;padding-left:16px}#applet_p_63794 .Px-18{padding-right:18px;padding-left:18px}#applet_p_63794 .Px-20{padding-right:20px;padding-left:20px}#applet_p_63794 .Px-30{padding-right:30px;padding-left:30px}#applet_p_63794 .Py-1{padding-top:1px;padding-bottom:1px}#applet_p_63794 .Py-2{padding-top:2px;padding-bottom:2px}#applet_p_63794 .Py-4{padding-top:4px;padding-bottom:4px}#applet_p_63794 .Py-6{padding-top:6px;padding-bottom:6px}#applet_p_63794 .Py-8{padding-top:8px;padding-bottom:8px}#applet_p_63794 .Py-10{padding-top:10px;padding-bottom:10px}#applet_p_63794 .Py-12{padding-top:12px;padding-bottom:12px}#applet_p_63794 .Py-14{padding-top:14px;padding-bottom:14px}#applet_p_63794 .Py-16{padding-top:16px;padding-bottom:16px}#applet_p_63794 .Py-18{padding-top:18px;padding-bottom:18px}#applet_p_63794 .Py-20{padding-top:20px;padding-bottom:20px}#applet_p_63794 .Py-30{padding-top:30px;padding-bottom:30px}#applet_p_63794 .Pt-1{padding-top:1px}#applet_p_63794 .Pb-1{padding-bottom:1px}#applet_p_63794 .Pstart-1{padding-left:1px}#applet_p_63794 .Pend-1{padding-right:1px}#applet_p_63794 .Pt-2{padding-top:2px}#applet_p_63794 .Pb-2{padding-bottom:2px}#applet_p_63794 .Pstart-2{padding-left:2px}#applet_p_63794 .Pend-2{padding-right:2px}#applet_p_63794 .Pt-4{padding-top:4px}#applet_p_63794 .Pb-4{padding-bottom:4px}#applet_p_63794 .Pstart-4{padding-left:4px}#applet_p_63794 .Pend-4{padding-right:4px}#applet_p_63794 .Pt-6{padding-top:6px}#applet_p_63794 .Pb-6{padding-bottom:6px}#applet_p_63794 .Pstart-6{padding-left:6px}#applet_p_63794 .Pend-6{padding-right:6px}#applet_p_63794 .Pt-8{padding-top:8px}#applet_p_63794 .Pb-8{padding-bottom:8px}#applet_p_63794 .Pstart-8{padding-left:8px}#applet_p_63794 .Pend-8{padding-right:8px}#applet_p_63794 .Pt-10{padding-top:10px}#applet_p_63794 .Pb-10{padding-bottom:10px}#applet_p_63794 .Pstart-10{padding-left:10px}#applet_p_63794 .Pend-10{padding-right:10px}#applet_p_63794 .Pt-12{padding-top:12px}#applet_p_63794 .Pb-12{padding-bottom:12px}#applet_p_63794 .Pstart-12{padding-left:12px}#applet_p_63794 .Pend-12{padding-right:12px}#applet_p_63794 .Pt-14{padding-top:14px}#applet_p_63794 .Pb-14{padding-bottom:14px}#applet_p_63794 .Pstart-14{padding-left:14px}#applet_p_63794 .Pend-14{padding-right:14px}#applet_p_63794 .Pt-16{padding-top:16px}#applet_p_63794 .Pb-16{padding-bottom:16px}#applet_p_63794 .Pstart-16{padding-left:16px}#applet_p_63794 .Pend-16{padding-right:16px}#applet_p_63794 .Pt-18{padding-top:18px}#applet_p_63794 .Pb-18{padding-bottom:18px}#applet_p_63794 .Pstart-18{padding-left:18px}#applet_p_63794 .Pend-18{padding-right:18px}#applet_p_63794 .Pt-20{padding-top:20px}#applet_p_63794 .Pend-20{padding-right:20px}#applet_p_63794 .Pb-20{padding-bottom:20px}#applet_p_63794 .Pstart-20{padding-left:20px}#applet_p_63794 .Pt-30{padding-top:30px}#applet_p_63794 .Pend-30{padding-right:30px}#applet_p_63794 .Pb-30{padding-bottom:30px}#applet_p_63794 .Pstart-30{padding-left:30px}#applet_p_63794 .Pt-0,#applet_p_63794 .Py-0{padding-top:0}#applet_p_63794 .Pb-0,#applet_p_63794 .Py-0{padding-bottom:0}#applet_p_63794 .Pstart-0,#applet_p_63794 .Px-0{padding-left:0}#applet_p_63794 .Pend-0,#applet_p_63794 .Px-0{padding-right:0}#applet_p_63794 .Pe-n{pointer-events:none}#applet_p_63794 .Pe-a{pointer-events:auto}#applet_p_63794 .Pos-s{position:static}#applet_p_63794 .Pos-a{position:absolute}#applet_p_63794 .Pos-r{position:relative}#applet_p_63794 .Pos-f{position:fixed}#applet_p_63794 .Ws-n::-webkit-scrollbar{-webkit-appearance:none}#applet_p_63794 .Tbl-f{table-layout:fixed}#applet_p_63794 .Tbl-a{table-layout:auto}#applet_p_63794 .Ta-c{text-align:center}#applet_p_63794 .Ta-j{text-align:justify}#applet_p_63794 .Ta-start{text-align:left}#applet_p_63794 .Ta-end{text-align:right}#applet_p_63794 .Td-n{text-decoration:none!important}#applet_p_63794 .Td-u,#applet_p_63794 .Td-u\:h:hover{text-decoration:underline}#applet_p_63794 .Tr-a{-webkit-text-rendering:auto;-moz-text-rendering:auto;-ms-text-rendering:auto;-o-text-rendering:auto;text-rendering:auto}#applet_p_63794 .Tr-os{-webkit-text-rendering:optimizeSpeed;-moz-text-rendering:optimizeSpeed;-ms-text-rendering:optimizeSpeed;-o-text-rendering:optimizeSpeed;text-rendering:optimizeSpeed}#applet_p_63794 .Tr-ol{-webkit-text-rendering:optimizeLegibility;-moz-text-rendering:optimizeLegibility;-ms-text-rendering:optimizeLegibility;-o-text-rendering:optimizeLegibility;text-rendering:optimizeLegibility}#applet_p_63794 .Tr-gp{-webkit-text-rendering:geometricPrecision;-moz-text-rendering:geometricPrecision;-ms-text-rendering:geometricPrecision;-o-text-rendering:geometricPrecision;text-rendering:geometricPrecision}#applet_p_63794 .Tr-i{-webkit-text-rendering:inherit;-moz-text-rendering:inherit;-ms-text-rendering:inherit;-o-text-rendering:inherit;text-rendering:inherit}#applet_p_63794 .Tt-n{text-transform:none}#applet_p_63794 .Tt-u{text-transform:uppercase}#applet_p_63794 .Tt-c{text-transform:capitalize}#applet_p_63794 .Tt-l{text-transform:lowercase}#applet_p_63794 .Tsh{text-shadow:0 1px 1px #000}#applet_p_63794 .Tsh-n{text-shadow:none}#applet_p_63794 .Us-n{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}#applet_p_63794 .Va-sup{vertical-align:super}#applet_p_63794 .Va-t{vertical-align:top}#applet_p_63794 .Va-tt{vertical-align:text-top}#applet_p_63794 .Va-m{vertical-align:middle}#applet_p_63794 .Va-bl{vertical-align:baseline}#applet_p_63794 .Va-b{vertical-align:bottom}#applet_p_63794 .Va-tb{vertical-align:text-bottom}#applet_p_63794 .Va-sub{vertical-align:sub}#applet_p_63794 .V-v{visibility:visible}#applet_p_63794 .V-h{visibility:hidden}#applet_p_63794 .V-c{visibility:collapse}#applet_p_63794 .Whs-nw{white-space:nowrap}#applet_p_63794 .Whs-n{white-space:normal}#applet_p_63794 .W-a{width:auto}#applet_p_63794 .W-0{width:0}#applet_p_63794 .W-1{width:1%}#applet_p_63794 .W-10{width:10%;*width:9.6%}#applet_p_63794 .W-15{width:15%;*width:14.8%}#applet_p_63794 .W-20{width:20%;*width:19.5%}#applet_p_63794 .W-25{width:25%;*width:24.5%}#applet_p_63794 .W-30{width:30%;*width:29.6%}#applet_p_63794 .W-33{width:33.33%;*width:33%}#applet_p_63794 .W-35{width:35%;*width:34.9%}#applet_p_63794 .W-40{width:40%;*width:39.5%}#applet_p_63794 .W-45{width:45%;*width:44.9%}#applet_p_63794 .W-50{width:50%;*width:49.5%}#applet_p_63794 .W-55{width:55%;*width:54.8%}#applet_p_63794 .W-60{width:60%}#applet_p_63794 .W-66{width:66.66%}#applet_p_63794 .W-70{width:70%}#applet_p_63794 .W-75{width:75%}#applet_p_63794 .W-80{width:80%}#applet_p_63794 .W-90{width:90%}#applet_p_63794 .W-100{width:100%}#applet_p_63794 .Wpx-1{width:1px}#applet_p_63794 .Wpx-2{width:2px}#applet_p_63794 .Wpx-4{width:4px}#applet_p_63794 .Wpx-6{width:6px}#applet_p_63794 .Wpx-8{width:8px}#applet_p_63794 .Wpx-10{width:10px}#applet_p_63794 .Wpx-12{width:12px}#applet_p_63794 .Wpx-14{width:14px}#applet_p_63794 .Wpx-16{width:16px}#applet_p_63794 .Wpx-18{width:18px}#applet_p_63794 .Wpx-20{width:20px}#applet_p_63794 .Wpx-24{width:24px}#applet_p_63794 .Wpx-26{width:26px}#applet_p_63794 .Wpx-28{width:28px}#applet_p_63794 .Wpx-30{width:30px}#applet_p_63794 .Wpx-32{width:32px}#applet_p_63794 .Wob-ba{word-break:break-all}#applet_p_63794 .Wob-n{word-break:normal!important}#applet_p_63794 .Wow-bw{word-wrap:break-word}#applet_p_63794 .Wow-n{word-wrap:normal!important}#applet_p_63794 .Z-0{z-index:0}#applet_p_63794 .Z-1{z-index:1}#applet_p_63794 .Z-3{z-index:3}#applet_p_63794 .Z-5{z-index:5}#applet_p_63794 .Z-7{z-index:7}#applet_p_63794 .Z-10{z-index:10}#applet_p_63794 .Z-a{z-index:auto!important}#applet_p_63794 .Zoom-1{zoom:1}

#applet_p_63794 .StencilRoot input {
    font-size: 13px;    
}

#applet_p_63794 .StencilRoot button {
    font-size: 14px;
    line-height: normal;
}
</style>

    
    
    </head>
    <body class="my3columns  l-out Pos-r https fp fp-v2 rc1 fp-default mini-uh-on viewer-right ltr    stream-dense" dir="ltr">
        
        
        
                    <div id="darla-assets-top">
            <script type='text/javascript' src='/sy/rq/darla/2-9-7/js/g-r-min.js'></script>
            <script>
    var resourceTimingAssets = null;
    if (window.performance && window.performance.mark && window.performance.getEntriesByName) {
        resourceTimingAssets = {'darlaJsLoaded' : 'https://www.yahoo.com/sy/rq/darla/2-9-7/js/g-r-min.js'};
        window.performance.mark('darlaJsLoaded');
    }
</script>
            </div>
                <script type="text/javascript">
        var rapidPageConfig = {
            rapidEarlyConfig : {},
            rapidConfig: {"compr_type":"deflate","tracked_mods":[],"spaceid":2023538075,"ywa":{"project_id":"10001806365479","host":"y.analytics.yahoo.com"},"click_timeout":300,"track_right_click":true,"apv":true,"apv_time":0,"yql_host":"","test_id":"201","client_only":0,"pageview_on_init":true,"perf_navigationtime":2,"addmodules_timeout":500,"keys":{"_rid":"4qbuvt9bdji05","mrkt":"us","pt":1,"ver":"megastrm","uh_vw":0,"colo":"slw50.fp.bf1.yahoo.com","navtype":"server","nob":1},"viewability":true},
            rapidSingleInstance: 0,
            ywaCF: {"mrkt":12,"pt":13,"test":14,"sec":18,"slk":19,"cpos":21,"pkgt":22,"ct":23,"bpos":24,"cat":25,"dcl":26,"f":40,"prfm1":41,"site":42,"prfm2":43,"prfm3":44,"prfm4":45,"pct":48,"ver":49,"ft":51,"sca":53,"elm":56,"elmt":57,"ad":58,"tsrc":69,"err":72,"bkpt":73,"bw":98,"bh":99,"olncust":100,"noct":101,"uh_vw":102,"rspns":107,"grpt":109,"itc":111,"enr":112,"tar":113,"sp":115,"pos":117,"cposx":118,"cw":119,"ch":120,"t1":121,"t2":122,"t3":123,"t4":124,"t5":125,"refcnt":135,"tar_qa":145,"navtype":146},
            ywaActionMap: {"swp":103,"click":12,"secvw":18,"hvr":115,"error":99},
            ywaOutcomeMap: {"fetch":30,"end":31,"dclent":101,"dclitm":102,"op":105,"cl":106,"nav":108,"svct":109,"unsvct":110,"rmsvct":117,"slct":121,"imprt":123,"lgn":125,"lgo":126,"flagitm":129,"unflagitm":130,"flatcat":131,"unflagcat":132,"slctfltr":133} 
        };
        if (rapidPageConfig.rapidEarlyConfig.keys) {
            rapidPageConfig.rapidEarlyConfig.keys.bw = document.body.offsetWidth;
            rapidPageConfig.rapidEarlyConfig.keys.bh = document.body.offsetHeight;
        }
        rapidPageConfig.rapidConfig.keys.bw = document.body.offsetWidth;
        rapidPageConfig.rapidConfig.keys.bh = document.body.offsetHeight;
        </script>
        
                            
                    
                    
                    
                    
                    
                    

    <div id="UH">
        <div id="UH-ColWrap">
            <div id="applet_p_30345894" class=" M-0 js-applet header Zoom-1  Mb-0 " data-applet-guid="p_30345894" data-applet-type="header" data-applet-params="_suid:30345894" data-i13n="auto:true;sec:hd" data-i13n-sec="hd" data-ylk="rspns:nav;t1:a1;t2:hd;itc:0;"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  <div id="mega-uh-wrapper" class="Bds(n) Bdc($headerBdr) Scrolling_Bdc($headerBdrScroll) has-scrolled_Bdc($headerBdrScroll) Bdbw(1px) ua-ie7_Bdbs(s)! ua-ie8_Bdbs(s)! Scrolling_Bxsh($headerShadow) has-scrolled_Bxsh($headerShadow) Bgc(#fff) T(0) Start(0) End(0) Pos(f) Z(3) Zoom">
    
    <div id="mega-topbar" class="Pos(r) H(22px) mini-header_Mt(-19px) Reader-open_Mt(-19px) Bg($topbarBgc) Bxsh($topbarShadow) Z(7)"   data-ylk="rspns:nav;t1:a1;t2:hd;t3:tb;sec:hd;itc:0;elm:itm;elmt:pty;">
    <ul class="Pos(r) Miw(1000px) Pstart(9px) Lh(1.7) Reader-open_Op(0) mini-header_Op(0)" role="navigation">
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://www.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:home;t5:home;cpos:1;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) Icon-Fp2 IconHome"></i>Home</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://mail.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:mail;t5:mail;cpos:2;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Mail</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://www.flickr.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:flickr;t5:flickr;cpos:3;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Flickr</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://www.tumblr.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:tumblr;t5:tumblr;cpos:4;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Tumblr</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://answers.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:answers;t5:answers;cpos:5;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Answers</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://groups.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:groups;t5:groups;cpos:6;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Groups</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://mobile.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:mobile;t5:mobile;cpos:7;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Mobile</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(10px) navigation-menu">
            <a href="https://everything.yahoo.com/" class="navigation-menu-title Pos(r) C(#fff) Pstart(12px) Pend(24px) Py(4px) Fz(13px) menu-open_C($topbarMenu) menu-open_Bgc(#fff) menu-open_Z(8) rapidnofollow"   data-ylk="rspns:op;t5:more;slk:more;itc:1;elmt:mu;cpos:8;" tabindex="1">More<i class="Pos(a) Pt(2px) Pstart(6px) Fw(b) Icon-Fp2 IconDownCaret"></i></a>
            <div class="Pos(a) Bgc(#fff) Bxsh($topbarMenuShadow) Z(7) V(h) Op(0) menu-open_V(v) menu-open_Op(1)">
                <ul class="Px(12px) Py(5px)">
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/politics/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:politics;t5:politics;cpos:9;" tabindex="1">Politics</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/celebrity/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:celebrity;t5:celebrity;cpos:10;" tabindex="1">Celebrity</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/movies/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:movies;t5:movies;cpos:11;" tabindex="1">Movies</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/music/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:music;t5:music;cpos:12;" tabindex="1">Music</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/tv/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:tv;t5:tv;cpos:13;" tabindex="1">TV</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/style/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:style;t5:style;cpos:14;" tabindex="1">Style</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/beauty/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:beauty;t5:beauty;cpos:15;" tabindex="1">Beauty</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/tech/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:tech;t5:tech;cpos:16;" tabindex="1">Tech</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://shopping.yahoo.com/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:shopping;t5:shopping;cpos:17;" tabindex="1">Shopping</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/autos/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:autos;t5:autos;cpos:18;" tabindex="1">Autos</a>
                    </li>
                
                </ul>
            </div>
        </li>
    
    
    
    </ul>
</div>

    
    <div id="mega-uh" class="M(a) Maw(1256px) Miw(1000px) Pos(r) Pt(22px) Pb(24px) Reader-open_Pt(13px) mini-header_Pt(13px) Reader-open_Pb(14px) mini-header_Pb(14px) Z(6)">
        <h1 class="Fz(0) Pos(a) Pstart(15px) Reader-open_Pt(0px) mini-header_Pt(0px) search-mini-header_Pstart(10px) ua-ie7_Mt(4px)">
    <a id="uh-logo" href="https://www.yahoo.com/" class="D(ib) H(47px) W($bigLogoWidth) Bgi($logoImage) Bgr(nr) Bgz($bigLogoWidth) Reader-open_Bgz($mediumLogoWidth) mini-header_Bgz($mediumLogoWidth) search-mini-header_Bgz($searchLogoWidth) Bgz($smallLogoWidth)!--sm1024 Bgp($bigLogoPos) Reader-open_Bgp($mediumLogoPos) mini-header_Bgp($mediumLogoPos) search-mini-header_Bgp($searchLogoPos) Bgp($searchLogoPos)!--sm1024 ua-ie7_Bgi($logoImageIe) ua-ie7_Mstart(-170px) ua-ie8_Bgi($logoImageIe) "   data-ylk="rspns:nav;t1:a1;t2:hd;sec:hd;itc:0;slk:logo;elm:img;elmt:logo;" tabindex="1">
        <b class="Hidden">Yahoo</b>
    </a>
</h1>

        <ul class="Pos(a) End(15px) List(n) Mt(3px) Reader-open_Mt(1px) mini-header_Mt(1px)" role="menubar">
            
    <li class="Pos(r) Fl(start) Mend(26px)">
        <a id="uh-signin" class="Bdc($signInBtn) Bdrs(5px) Bds(s) Bdw(2px) Bgc($signInBtn):h C($signInBtn) C(#fff):h D(ib) Ell Fz(14px) Fw(b) Py(2px) Mt(5px) Ta(c) Td(n):h Miw(78px) H(18px)" href="https://login.yahoo.com/config/login?.src=fpctx&amp;.intl=us&amp;.lang=en-US&amp;.done=https%3A%2F%2Fwww.yahoo.com" data-action-outcome="lgn"  data-ylk="t3:usr;elm:btn;elmt:lgn;" tabindex="4"><div class="Miw(78px) Ta(c) Pos(a) T(0) Lh($userNavTextLh)">Sign in</div></a>
    </li>


            
    <li id="uh-notifications" class="uh-menu uh-notifications Fl(start) D(ib) Mend(13px) Cur(p) ua-ie7_D(n) ua-ie8_Pb(10px) ua-ie9_Pb(10px)" role="menuitem">
        <button class="uh-menu-btn Pos(r) Bd(0) Px(8px) Pt(1px) Lh(1.3)" aria-label="Notifications" aria-haspopup="true" aria-expanded="true" tabindex="5" title="Notifications">
            <i class="Lh($userNotifIconLh) C($mailBtn) Fz(28px) Grid-U Icon-Fp2 IconBell uh-notifications-icon"></i>
            <span class="Bgc($mailBadge) Bdrs(11px) C(#fff) D(n) display-count_D(b) End(-4px) Fz(11px) Fw(b) Pos(a) Py(4px) Ta(c) T(-7px) uh-notification-count W(22px)"></span>
        </button>
        <div class="uh-notification-panel uh-menu-panel Pos(a) End(0) T(45px) W(280px) Bgc($menuBgc) Bd Bdc($menuBdr) Bdrs(4px) Bxsh($menuShadow) Ovy(a) Trs($menuTransition) Cur(d) V(h) Op(0) Mah(0) uh-notifications:h_V(v) uh-notifications:h_Op(1) uh-notifications:h_Mah($panelMah) panel-open_V(v) panel-open_Op(1) panel-open_Mah($panelMah)" aria-label="Notifications" role="menu" tabindex="5">
            <div class="uh-notifications-loading Py(16px) Px(24px) Ta(c)">
                <div class="W(30px) H(30px) Mx(a) My(12px) Bgz(30px) Bgi($animatedSpinner) ua-ie8_D(n)"></div>
                <span class="C($mailTstamp) D(n) ua-ie8_D(b)">Loading Updates</span>
            </div>
        </div>
    </li>


            
    <li id="uh-mail" class="uh-menu uh-mail D(ib) Mstart(14px) ua-ie8_Pb(10px) ua-ie9_Pb(10px)" role="menuitem">
        <a id="uh-mail-link" href="https://mail.yahoo.com/" class="Pos(r) D(ib) Ta(s) Td(n):h uh-menu-btn" aria-label="Mail" aria-haspopup="true" role="button" aria-expanded="true" tabindex="6"><i class="Lh($userNavIconLh) W(28px) Mend(8px) C($mailBtn) Fz(30px) Grid-U Icon-Fp2 IconMail uh-mail-icon"></i><span class="Lh($userNavTextLh) D(ib) C($mailBtn) Fz(14px) Fw(b) Va(t)">Mail</span><span class="Bgc($mailBadge) Bdrs(11px) C(#fff) D(n) display-count_D(b) Start(16px) Fz(11px) Fw(b) Pos(a) Py(4px) Ta(c) T(-7px) uh-mail-count W(22px)"></span>
        </a>
        
            <div class="uh-mail-preview uh-menu-panel Pos(a) End(0) T(45px) W(280px) Bgc($menuBgc) Bd Bdc($menuBdr) Bdrs(4px) Bxsh($menuShadow) Trs($menuTransition) V(h) Op(0) Mah(0) uh-mail:h_V(v) uh-mail:h_Op(1) uh-mail:h_Mah($panelMah) panel-open_V(v) panel-open_Op(1) panel-open_Mah($panelMah)" role="menu" aria-label="Mail" tabindex="6">
                
                    <div class="Px(24px) Py(20px) Ta(c)">
                        <a class="C($menuLink) Fw(b) Td(n)" href="https://login.yahoo.com/config/login?.src=fpctx&amp;.intl=us&amp;.lang=en-US&amp;.done=https%3A%2F%2Fwww.yahoo.com" data-action-outcome="lgn"  data-ylk="t3:usr;elm:btn;elmt:lgn;">Sign in</a>  to view your mail
                    </div>
                
            </div>
        
    </li>


        </ul>
        <div id="uh-search" class="Pos(r) Mend(375px) Mstart(206px) search-mini-header_Mstart(112px) Mstart(120px)--sm1024 Maw(595px) H(40px) Reader-open_H(40px) mini-header_H(40px) search-mini-header_Maw(685px) Maw(745px)--sm1024 Va(t)">
    <form name="input" class="glowing glow" action="https://search.yahoo.com/search;_ylc=X3oDMTFiN25laTRvBF9TAzIwMjM1MzgwNzUEaXRjAzEEc2VjA3NyY2hfcWEEc2xrA3NyY2h3ZWI-" method="get" data-assist-action="https://search.yahoo.com/search;_ylc=X3oDMTFiaHBhMnJmBF9TAzIwMjM1MzgwNzUEaXRjAzEEc2VjA3NyY2hfcWEEc2xrA3NyY2hhc3Q-">
        <table class="Bdcl(s)" role="presentation">
            <tbody>
                <tr>
                    <td class="Pos(r) P(0) W(100%) H(40px) Reader-open_H(40px) mini-header_H(40px) D(tb) ua-safari_D(tbc) Tbl(f)">
                        <input id="uh-search-box" type="text" name="p" class="Pos(a) T(0) Start(0) Bd Bdc($searchBdrFoc):f Bdc($searchBdr) glow_Bdc($searchBtnGlow) Bgc(t) Bxsh(n) Fz(18px) M(0) O(0) Px(10px) W(100%) H(inh) Z(2) Bxz(bb) Bdrs(2px) ua-ie7_H(36px) ua-ie7_Lh(36px) ua-ie8_Lh(36px) ua-ie7_W(92%)" autofocus autocomplete="off" autocapitalize="off" aria-label="Search" tabindex="2">
                        
                        
                            <input type="hidden" data-fr="yfp-t-201" name="fr" value="yfp-t-201" />
                        
                            <input type="hidden" data-fp="1" name="fp" value="1" />
                        
                            <input type="hidden" data-toggle="1" name="toggle" value="1" />
                        
                            <input type="hidden" data-cop="mss" name="cop" value="mss" />
                        
                            <input type="hidden" data-ei="UTF-8" name="ei" value="UTF-8" />
                        
                    </td>
                    <td class="Miw(5px)"></td>
                    <td>
                        <button id="uh-search-button" type="submit" class="rapid-noclick-resp C(#fff) Fz(16px) H(40px) W(140px) Reader-open_H(40px) mini-header_H(40px) Px(26px) Py(0) Whs(nw) Bdrs(3px) Bdw(0) Bgc($searchBtn) glow_Bgc($searchBtnGlow) Bxsh($searchBtnShadow) glow_Bxsh($searchBtnShadowGlow) Lh(1)"   data-ylk="rspns:nav;t1:a1;t2:srch;sec:srch;slk:srchweb;elm:btn;elmt:srch;tar:search.yahoo.com;tar_uri:/search;itc:0;" tabindex="3">
                        <span class="search-default-label Fw(500) ">Search Web</span>
                        <span class="search-short-label Fw(500) D(n) ">Search</span>
                        </button>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
    
    <ul id="Skip-links" class="Pos(a)">
        <li><a href="#Navigation" class="W(0) O(h) D(ib) Whs(nw) Pos(a) Bg(#0078ff) C(#fff) Op(0) W(a):f Op(1):f P(5px):f">Skip to Navigation</a></li>
        <li><a href="#Main" class="W(0) O(h) D(ib) Whs(nw) Pos(a) Bg(#0078ff) C(#fff) Op(0) W(a):f Op(1):f P(5px):f">Skip to Main content</a></li>
        <li><a href="#Aside" class="W(0) O(h) D(ib) Whs(nw) Pos(a) Bg(#0078ff) C(#fff) Op(0) W(a):f Op(1):f P(5px):f">Skip to Right rail</a></li>
    </ul>
    
</div>

    <script>
!function(){var e,t,a,n,c=window,o=document,r=o.getElementById("uh-search-box"),g=function(c){try{a=c.keyCode,n=c.target?c.target:c.srcElement,e=r.value.length,n===r&&0===e&&a>=32&&40>=a?r.blur():"EMBED"===n.tagName||"OBJECT"===n.tagName||"INPUT"===n.tagName||"TEXTAREA"===n.tagName||c.altKey||c.metaKey||c.ctrlKey||!(32>a||a>40)||9==a||13==a||16==a||27==a||(e&&(r.setSelectionRange?r.setSelectionRange(e,e):r.createTextRange&&(t=r.createTextRange(),t.moveStart("character",e),t.select())),r.focus())}catch(c){}};r&&(c.addEventListener?c.addEventListener("keydown",g,!1):c.attachEvent&&o.attachEvent("onkeydown",g),setTimeout(function(){r.focus()},500))}();
</script>



    </div>
</div>

 </div> </div> </div>            <!-- App close -->
            </div>
        </div>
    </div><!-- Header -->
    <div id="Stencil">
    <div id="Reader" class="js-viewer-viewerwrapper">
        <div class="ReaderWrap">
            <div class="InnerWrap">
                <div id="applet_p_50000101" class=" App_v2  M-0 js-applet viewer Zoom-1  App-Chromeless " data-applet-guid="p_50000101" data-applet-type="viewer" data-applet-params="_suid:50000101" data-i13n="auto:true;sec:app-view" data-i13n-sec="app-view" data-applet-init="now"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  

<div id="hl-viewer" class="js-slider Hl-Viewer Ov-h desktop" role="document" aria-labelledby="modal-header" aria-live="polite" aria-hidden="true" tabindex="-1">
    
    
        <button id="closebtn" class="Viewer-Close-Btn Bdr-0 D-b Fw(b) js-close-content-viewer rapid-noclick-resp Z-7">
            <i class="Icon-Fp2 IconActionCross"></i>
            <b class="Td(n) Hidden">Close this content, you can also use the Escape key at anytime</b>
        </button>
        <div class="viewer-wrapper Ov-h mega-modal">
            <div class="">
                
                    <div class="content-container"></div>
                
                
                
                
                <div class="footer-ads">
                    <div id="hl-ad-FSRVY-0" class="D-ib">
                        <div id="hl-ad-FSRVY-0-0-iframe" class="hl-display-ads"></div>
                    </div>
                    <div id="hl-ad-FOOT9-0" class="D-ib">
                        <div id="hl-ad-FOOT9-0-0-iframe" class="hl-display-ads"></div>
                    </div>
                    <div id="hl-ad-FOOT-0" class="D-ib">
                        <div id="hl-ad-FOOT-0-0-iframe" class="hl-display-ads"></div>
                    </div>
                </div>
                <span id="viewer-end" tabindex="0"></span>
            </div>
        </div>
    

    


</div>

 </div> </div> </div>            <!-- App close -->
            </div>
            </div>
        </div>
        <div id="Overlay"></div>
        <div class="Reader-bg"></div>
    </div>
</div>
    
    <div id="Masterwrap" class="slider js-viewer-pagewrapper">
        <div class="page">
            <div id="Billboard-ad">
                                                    <div id="my-adsMAST" class="D-n">
                    <div id="my-adsMAST-iframe">
                        
                        <!-- MAST has no ad contents. Other Ad position will take over this ad --><noscript>
<!-- SpaceID=2023538075 loc=MAST noad --><!-- fac-gd2-noad --><!-- gd2-status-2 --><!--QYZ CMS_NONE_SELECTED,,;;MAST;2023538075;2-->
<img width="1" height="1" alt="" src="https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&amp;bs=(136c1pdeu(gid%24Okzx8DcyLjPvrchjZGr0dBYfMjAwMVbZyAURcDWk,st%241457113093508360,si%244452051,sp%242023538075,pv%241,v%242.0))&amp;t=J_3-D_3&amp;al=(as%24125ki9vvf,aid%243O6QvWKLDzw-,cr%24-1,ct%2425,at%24H,eob%24gd1_match_id=-1:ypos=MAST)"></noscript>
                    </div>
                </div>
                                <div id="ad-north-base" class=""><div id="ad-north" class="BillboardAd"></div></div>
            </div>
            
            <div class="Col1" role="navigation" id="Navigation" tabindex="-1">
                <div class="Col1-stack" data-plugin="sticker" data-sticker-top="75px">
                                <div id="applet_p_50000195" class=" M-0 js-applet navrailv2 Zoom-1  Mb-20 " data-applet-guid="p_50000195" data-applet-type="navrailv2" data-applet-params="_suid:50000195" data-i13n="auto:true;sec:nav" data-i13n-sec="nav" data-ylk="rspns:nav;itc:0;t1:a2;t2:nav;elm:itm;"> <!-- App open -->
        <!-- src=mdbm type=popular mod=td-applet-navlinks-atomic ins=p_50000195 ts=1457112160.0824 --><div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  

    

    

    <ul class="navlinks-list D(tbc) W(134px) Mstart(6px)">
    
        <li class="navlink  Py(5px)">
            <a href=https://mail.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconMail C($MailNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Mail</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://news.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconNews C($NewsNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">News</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://sports.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconSports C($SportsNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Sports</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://finance.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconFinance C($FinanceNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Finance</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/autos/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconAutos C($AutosNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Autos</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/celebrity/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconCelebrity C($CelebrityNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Celebrity</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://shopping.yahoo.com class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconShopping C($ShoppingNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Shopping</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/movies/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconMovies C($MoviesNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Movies</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/politics/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconPolitics C($PoliticsNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Politics</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/beauty/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconBeauty C($BeautyNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Beauty</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh700 Py(5px)">
            <a href=https://www.yahoo.com/style/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconStyle C($StyleNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Style</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh700 Py(5px)">
            <a href=https://www.yahoo.com/tech/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconTech C($TechNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Tech</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh700 Py(5px)">
            <a href=https://www.yahoo.com/tv/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconTV C($TVNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">TV</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh700 Py(5px)">
            <a href=https://everything.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconMoreOnYahoo C($MoreOnYahooNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">More on Yahoo</span>
            </a>
        </li>
    
        <li class="D(b)--maxh700 Py(5px) js-navlinks-seemore Pos(r) D(n) O(0)"  role="button" tabindex="0" aria-haspopup="true" aria-label="More Navlinks">
            <i class="Icon-Fp2 IconStreamShare Va(m) Pend(6px) Fz(22px) D(tbc) Miw(29px) Ta(c) C($MoreNav)"></i>
            <span class="Va(m) D(tbc) Fz(14px) Fw(b) V(h)--sm1024 C($navlink) Col1-stack:h_V(v) C($navlinkHover):h Cur(p)">More</span>

            <div class="js-navlinks-seemore-menu D(n) js-navlinks-menu-open_D(ib) Va(b) Pos(a) B(-14px) Start(60px) Pstart(60px)">
                <ul class="Bxsh($menuShadow) Bd($submenuBdr) Bdrs(3px) Bgc(#fff) Cur(d) Py(13px) Px(20px) Miw(140px)">
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh700">
                        <a href=https://www.yahoo.com/style/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconStyle C($StyleNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Style</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh700">
                        <a href=https://www.yahoo.com/tech/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconTech C($TechNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Tech</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh700">
                        <a href=https://www.yahoo.com/tv/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconTV C($TVNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">TV</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh700">
                        <a href=https://everything.yahoo.com/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconMoreOnYahoo C($MoreOnYahooNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">More on Yahoo</span>
                        </a>
                    </li>
                
                </ul>
            </div>
        </li>
    </ul>


 </div> </div> </div> <!-- yrid: X8TZVgAAAAArWgAAqVxB_g.. | edgepipe: 0 | authed: 0 | ynet: 1 | ssl: 1 | spdy: 0 | ytee: 0 | bucket: 201 | colo: bf1 | device: desktop | bot: 0 | environment: prod | lang: en-US | partner: default | site: fp | region: US | intl: us | tz: America/Los_Angeles | mode: fallback -->
<!-- tdserver-my.fp.production.manhattan.bf1.yahoo.com (pprd3-node50007-lh1.manhattan.bf1.yahoo.com) | Served by ynodejs | Fri Mar 04 2016 17:22:40 GMT+0000 (UTC) -->            <!-- App close -->
            </div>                           <div id="my-adsTXTL" class="Mt-10 Mb-20 Pt-20 Pos-r ad-txtl D-n">
                    <div class="Mx-a" id="my-adsTXTL-iframe">
                        <noscript>
<!-- APT Vendor: Right Media, Format: Standard Graphical -->
<style>
	#fc_align{
		position: static !important;
		width: 120px !important;
		margin: auto !important;
	}
</style>
<!--QYZ 2275188051,4451009051,;;TXTL;2023538075;1-->
<img width="1" height="1" alt="" src="https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&amp;bs=(136c1pdeu(gid%24Okzx8DcyLjPvrchjZGr0dBYfMjAwMVbZyAURcDWk,st%241457113093508360,si%244452051,sp%242023538075,pv%241,v%242.0))&amp;t=J_3-D_3&amp;al=(as%2413a72rhof,aid%24UQSRvWKLDzw-,bi%242275188051,agp%243476502551,cr%244451009051,ct%2425,at%24H,eob%24gd1_match_id=-1:ypos=TXTL)"></noscript>
                    </div>
                </div>
                </div>
            </div>
            <div class="Col2">
                <div id="Main" class="Col2-stack" role="content" tabindex="-1">
                    <div id="Banner">
                        
                    </div><!-- Banner -->
                                <div id="applet_p_50000173" class=" M-0 js-applet streamv2 Zoom-1  Mb-20 " data-applet-guid="p_50000173" data-applet-type="streamv2" data-applet-params="_suid:50000173" data-i13n="auto:true;sec:strm;useViewability:true" data-i13n-sec="strm" data-ylk="t1:a3;t2:strm;t3:ct;cat:default;rspns:nav;itc:0;" data-applet-init="now"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  

<div data-uuid-list="p_50000173">
    

    
    <ul class=" js-stream-tmpl-items js-stream-dense  js-stream-hover-enable My(0) Mb(0) Wow(bw)" id="Stream">
    
        
    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Los_Angeles|WIKIID:Nicole_Brown_Simpson|WIKIID:TMZ|WIKIID:Buck_Knives|WIKIID:Police_officer|YCT:001000667|YCT:001000780|YCT:001000291"  data-uuid="1a07fb94-d700-311d-8e60-e66daa2309b5" data-cauuid="1a07fb94-d700-311d-8e60-e66daa2309b5" data-type="article" data-cluster="1a07fb94-d700-311d-8e60-e66daa2309b5"  data-hosted="HOSTED">
    <div class="js-stream-item-wrap Pos(r) Pt(12px) Pb(17px)"><div class="js-stream-roundup js-stream-roundup-filmstrip Pos(r) Wow(bw) Cf">
    <div class="strm-headline Pos(r)">
        <a href="http://news.yahoo.com/police-testing-knife-recovered-property-once-owned-o-160945717.html" class="Pos(r) D(ib) Z(1) js-stream-content-link js-content-title js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:1;cposy:1;bpos:1;pos:1;ss_cid:1a07fb94-d700-311d-8e60-e66daa2309b5;refcnt:4;subsec:1162;imgt:ss;g:1a07fb94-d700-311d-8e60-e66daa2309b5;aid:id-3596795;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;elm:img;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;" >
        
        <img src="/sy/uu/api/res/1.2/MQbBeNWOHuZFhqoAUy0.ZA--/Zmk9c3RyaW07aD0yNzQ7cHlvZmY9MDtxPTk1O3c9NzAyO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/030416/images/smush/simpson_ipad_1457109715.jpg.cf.jpg" class="W(100%) rounded-img" alt="">
        
        <div class="Pos(a) T(0) Start(0) W(100%) H(100%) Ov(h)" title="O.J. Simpson during his murder trial in 1995. (AP)" >
            <div class="Pos(a) B(0) M(15px) Mb(11px) Mend(120px) C(#fff) Z(1)">
                <h2 class="js-stream-item-title Fz(21px) Fw(b) Mb(3px) Lh(24px) Td(u):h">Knife found on former O.J. property turns up</h2>
                <span class="stream-summary Fz(13px) C(#d9d9db) Mend(5px)">Police are testing the Buck knife, which was reportedly handed over to an officer a number of years ago by a construction worker.</span><span class="Fw(b) D(ib) Va(b) Lh(18px) Td(u):h">Connected to murders? Â»</span></div>
            <div class="strm-img-gradient W(100%) H(100%) rounded-img"></div>
        </div>
        </a>
        <div class="Pos(a) End(13px) T(10px) W(30px) Mend(2px) Ta(end) Z(2)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions has-comments js-stream-dislike-disabled">
    
    <li class="ActionComments D(ib) O(n) Mstart(-1px) Pos(r) C(white)" data-cmntnum="1162"><span class="js-stream-comment-counter D(b) Fz(11px) Ta(c) Fw(b) Tsh($comment_shadow) js-stream-comment-hidden">1162</span><span class="js-stream-comment-counter-update D(b) Fz(11px) Ta(c) Fw(b) Tsh($comment_shadow)"></span><a href="javascript:void(0)" class="Pos(r) js-stream-comments-button-drawer Td(n) O(n) C(white) Px(14px) Pb(5px) Pt(25px)" role="button" tabindex="0"   data-ylk="cpos:1;cposy:1;bpos:1;pos:1;ss_cid:1a07fb94-d700-311d-8e60-e66daa2309b5;refcnt:4;subsec:1162;imgt:ss;g:1a07fb94-d700-311d-8e60-e66daa2309b5;aid:id-3596795;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;slk:cmmt;itc:1;"><b aria-live="polite" class="js-stream-comment-label ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) Lh(14px) Py(7px) End(-25px) C($signin_blue) js-stream-comments-button>Start(50%) js-stream-comments-button:h>Start(a)">Comments</b><i class="Icon-Fp2 IconComments Fz(21px) Tsh($comment_shadow) ActionComments:h_C($signin_blue)"></i></a>
    </li>
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C(white) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) Tsh($comment_shadow)"></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C(white) P(14px)" role="button" tabindex="0"   data-ylk="cpos:1;cposy:1;bpos:1;pos:1;ss_cid:1a07fb94-d700-311d-8e60-e66daa2309b5;refcnt:4;subsec:1162;imgt:ss;g:1a07fb94-d700-311d-8e60-e66daa2309b5;aid:id-3596795;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) Tsh($comment_shadow)"></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C(white) Tsh($comment_shadow) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
    </div>
    <ul class="P(0) Mt(7px) Mstart(-2px) Mend(7px) Fz(12px) Whs(nw) Lts(-.31em)">
        
        
        <li class="Pos(r) Va(t) D(ib) Bxz(bb) Whs(n) W(25%) Ov(h)">
            <a href="https://www.yahoo.com/politics/trump-at-a-disadvantage-in-a-brokered-convention-152052016.html" data-uuid="9f4e9844-7077-3f79-b95e-ff0d228b5fec" class="js-stream-content-link js-stream-item-title js-content-title js-content-viewer rapid-noclick-resp rapidnofollow O(n):f C($m_blue):f Td(n) Fz(13px) Fw(b) Lts(n) Whs(n) D(b) Px(2px)"    data-ylk="cpos:1;cposy:2;bpos:1;pos:2;ss_cid:1a07fb94-d700-311d-8e60-e66daa2309b5;refcnt:4;subsec:1162;imgt:ss;g:9f4e9844-7077-3f79-b95e-ff0d228b5fec;aid:id-3596792;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;elm:rhdln;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" >
                <img src="/sy/uu/api/res/1.2/4Nxo_2wuR5Jaof9evL_LYg--/Zmk9c3RyaW07aD0xNjA7cHlvZmY9MDtxPTgwO3c9MzQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/030416/images/smush/trump_brokered_convention_ipad_1457106737.jpg.cf.jpg" class="W(100%) rounded-img" title="Why a brokered convention is bad for Trump. (AP)" alt="">
                <h3 class="D(b) Mt(5px) Mend(10px) LineClamp(2,30px) Fz(12px) Fw(b) C($link) js-stream-content-link:h>C($m_blue) js-stream-content-link:f_C($m_blue)">Why a brokered convention is bad for Trump</h3>
            </a>
        </li>
        
        
        
        <li class="Pos(r) Va(t) D(ib) Bxz(bb) Whs(n) W(25%) Ov(h)">
            <a href="https://www.yahoo.com/movies/never-seen-hunger-games-deleted-scene-snow-142801996.html" data-uuid="b2f85e1d-b4ce-38b2-9be9-e4f4df692093" class="js-stream-content-link js-stream-item-title js-content-title js-content-viewer rapid-noclick-resp rapidnofollow O(n):f C($m_blue):f Td(n) Fz(13px) Fw(b) Lts(n) Whs(n) D(b) Px(2px)"    data-ylk="cpos:1;cposy:3;bpos:1;pos:3;ss_cid:1a07fb94-d700-311d-8e60-e66daa2309b5;refcnt:4;subsec:1162;imgt:ss;g:b2f85e1d-b4ce-38b2-9be9-e4f4df692093;aid:id-3596785;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;elm:rhdln;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" >
                <img src="/sy/uu/api/res/1.2/12pwpgvZf2pS1AEyRSsWlQ--/Zmk9c3RyaW07aD0xNjA7cHlvZmY9MDtxPTgwO3c9MzQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/030416/images/smush/hunger-games_ipad_1457105759.jpg.cf.jpg" class="W(100%) rounded-img" title="Never-before-seen &quot;Hunger Games&quot; scene. (Yahoo Movies)" alt="">
                <h3 class="D(b) Mt(5px) Mend(10px) LineClamp(2,30px) Fz(12px) Fw(b) C($link) js-stream-content-link:h>C($m_blue) js-stream-content-link:f_C($m_blue)">Never-before-seen 'Hunger Games' scene</h3>
            </a>
        </li>
        
        
        
        <li class="Pos(r) Va(t) D(ib) Bxz(bb) Whs(n) W(25%) Ov(h)">
            <a href="http://sports.yahoo.com/news/holly-holm-is-ready-to-stop-talking-about-ronda-rousey-and-defend-her-title-025445079.html" data-uuid="6c1ec14a-f6b6-35ba-a48e-83e3910db7c4" class="js-stream-content-link js-stream-item-title js-content-title js-content-viewer rapid-noclick-resp rapidnofollow O(n):f C($m_blue):f Td(n) Fz(13px) Fw(b) Lts(n) Whs(n) D(b) Px(2px)"    data-ylk="cpos:1;cposy:4;bpos:1;pos:4;ss_cid:1a07fb94-d700-311d-8e60-e66daa2309b5;refcnt:4;subsec:1162;imgt:ss;g:6c1ec14a-f6b6-35ba-a48e-83e3910db7c4;aid:id-3596773;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;elm:rhdln;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" >
                <img src="/sy/uu/api/res/1.2/dZnahGlRENK7ibTVak6klw--/Zmk9c3RyaW07aD0xNjA7cHlvZmY9MDtxPTgwO3c9MzQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/030416/images/smush/HollyHolm_ipad_1457098969.jpg.cf.jpg" class="W(100%) rounded-img" title="Holly Holm (Getty Images)" alt="">
                <h3 class="D(b) Mt(5px) Mend(10px) LineClamp(2,30px) Fz(12px) Fw(b) C($link) js-stream-content-link:h>C($m_blue) js-stream-content-link:f_C($m_blue)">Holm eager to defend bantamweight title</h3>
            </a>
        </li>
        
        
        
        <li class="Pos(r) Va(t) D(ib) Bxz(bb) Whs(n) W(25%) Ov(h)">
            <a href="https://www.yahoo.com/politics/in-sign-of-apocalypse-donald-trump-defends-the-025053649.html" data-uuid="0a782601-1471-337a-91c4-fabc6ca8d269" class="js-stream-content-link js-stream-item-title js-content-title js-content-viewer rapid-noclick-resp rapidnofollow O(n):f C($m_blue):f Td(n) Fz(13px) Fw(b) Lts(n) Whs(n) D(b) Px(2px)"    data-ylk="cpos:1;cposy:5;bpos:1;pos:5;ss_cid:1a07fb94-d700-311d-8e60-e66daa2309b5;refcnt:4;subsec:1162;imgt:ss;g:0a782601-1471-337a-91c4-fabc6ca8d269;aid:id-3596740;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;elm:rhdln;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" >
                <img src="/sy/uu/api/res/1.2/uldE7o_XwTfST2INerMEog--/Zmk9c3RyaW07aD0xNjA7cHlvZmY9MDtxPTgwO3c9MzQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/030416/images/smush/trumphandsEDIT_ipad_1457058237.jpg.cf.jpg" class="W(100%) rounded-img" title="Carlos Osorio/AP" alt="">
                <h3 class="D(b) Mt(5px) Mend(10px) LineClamp(2,30px) Fz(12px) Fw(b) C($link) js-stream-content-link:h>C($m_blue) js-stream-content-link:f_C($m_blue)">Donald Trump defends the size of his hands</h3>
            </a>
        </li>
        
        
    </ul>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="YCT:001000291|YCT:001000288|YCT:001000780" data-offnet="1" data-uuid="eac3913b-8677-3613-8368-cb4ff55706a4" data-type="article"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="Pos(r) Wow(bw) Cf Pend(30px)">

    
    <div class="strm-left Fl(start) Pos(r) Pend(18px)"><div class="Pos(r)"><a href="https://www.bostonglobe.com/metro/2016/03/03/plans-for-family-skiing-week-ended-with-tragedy/nXYtNJLc4m1zEHceDn6Y0K/story.html" class="Pos(r) D(ib) streamImage js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:2;cposy:6;bpos:1;pos:1;imgt:ss;g:eac3913b-8677-3613-8368-cb4ff55706a4;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:U.S.;elm:img;elmt:ct;r:4000024130S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/.hnnDYDwrDCL.GLZqF01Fg--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/dcea719a17c02bc441913c9ac39c703b" height="107" width="190" class="W(100%) BackgroundPic rounded-img" alt="">

</a>
        </div>
    </div>
    
    <div class="strm-right Fl(start) W(66%)"><div class="strm-headline">
            <div class="js-content-label C(c_us) Fw(b) Mb(6px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="us">U.S.</div>
            <h3 class="Mb(4px) LineClamp(3,57px) Lh(19px)">
                <a href="https://www.bostonglobe.com/metro/2016/03/03/plans-for-family-skiing-week-ended-with-tragedy/nXYtNJLc4m1zEHceDn6Y0K/story.html" class="Pos(r) O(n):f C($m_blue):f js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:2;cposy:6;bpos:1;pos:1;imgt:ss;g:eac3913b-8677-3613-8368-cb4ff55706a4;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:U.S.;elm:hdln;elmt:ct;r:4000024130S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><span>Plans for a family skiing week ended with a tragedy</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,33px) Lh(17px) C($gray) Bfv(h)">WINCHESTER - Everything was set for a family week of skiing in New Hampshire. Louanne and Skip Clavette were in North Conway, on Feb. 12 and their son was driving north to meet them. Their 35-year-old daughter, Caitlin, was to arrive the following day with her brotherâs fiancee, and her boyfriend. Matt Bradley had just bought Caitlin Clavette a diamond ring and planned to ask her father that weekend for permission to marry her. But then the unimaginable happened. Clavette was driving south on the Southeast Expressway about 7:50 a.m. Feb. 12 on her way to her job teaching art in Milton when a storm drain cover became airborne and slammed into the windshield of her car. The cover, which weighed</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Mt(5px) Tt(c)">BostonGlobe.com</span>
            </div>
        </div>
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mstart(12px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:2;cposy:6;bpos:1;pos:1;imgt:ss;g:eac3913b-8677-3613-8368-cb4ff55706a4;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:U.S.;r:4000024130S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Teacher|YCT:001000267|YCT:001000780|YCT:001000269" data-offnet="1" data-uuid="e1f2df90-016d-37b6-b703-32aa83bcee15" data-type="article"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="Pos(r) Wow(bw) Cf Pend(30px)">

    
    <div class="strm-left Fl(start) Pos(r) Pend(18px)"><div class="Pos(r)"><a href="http://www.charlotteobserver.com/news/nation-world/national/article63858492.html" class="Pos(r) D(ib) streamImage js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:3;cposy:7;bpos:1;pos:1;imgt:ss;g:e1f2df90-016d-37b6-b703-32aa83bcee15;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:U.S.;elm:img;elmt:ct;r:4000025790S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/YIbaj6Fhe4musWq7TKHejg--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/2b40e029b28f7749dcbdfa5e3a4e0049" height="107" width="190" class="W(100%) BackgroundPic rounded-img" alt="">

</a>
        </div>
    </div>
    
    <div class="strm-right Fl(start) W(66%)"><div class="strm-headline">
            <div class="js-content-label C(c_us) Fw(b) Mb(6px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="us">U.S.</div>
            <h3 class="Mb(4px) LineClamp(3,57px) Lh(19px)">
                <a href="http://www.charlotteobserver.com/news/nation-world/national/article63858492.html" class="Pos(r) O(n):f C($m_blue):f js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:3;cposy:7;bpos:1;pos:1;imgt:ss;g:e1f2df90-016d-37b6-b703-32aa83bcee15;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:U.S.;elm:hdln;elmt:ct;r:4000025790S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><span>Rehire teacher who resigned over nude photo, students demand</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,33px) Lh(17px) C($gray) Bfv(h)">A South Carolina high school teacher who says she was forced to resign after a student took her phone and circulated a nude picture of her has garnered the support of hundreds of students who signed a petition demanding she be reinstated. The teacher, Leigh Anne Arthur, who had taught for more than a decade in Union County, which is about 70 miles northwest of Columbia, said she had left her phone on her desk and was out of the classroom when a 16-year-old boy took her phone. The phone was passed among several classes of students, and the photo of Arthur was copied and shared. The Union County Schools superintendent, David Eubanks, said in an interview that Arthur shared responsibility for the episode because she had allowed students to use her phone in the past.</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Mt(5px) Tt(c)">The Charlotte Observer</span>
            </div>
        </div>
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mstart(12px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:3;cposy:7;bpos:1;pos:1;imgt:ss;g:e1f2df90-016d-37b6-b703-32aa83bcee15;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:U.S.;r:4000025790S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Donald_Trump|WIKIID:Winners_%26_Losers|YCT:001000671|YCT:001000661" data-offnet="1" data-uuid="f4f1d5f1-7e87-3f0b-a1e6-772897de19b7" data-type="article" data-cluster="f75e38cf-d1e2-416f-b063-ef98eac4511d"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="strm-default-clusters Pos(r) Wow(bw) Cf Pend(30px) ">

    
    <div class="strm-left Fl(start) Mend(18px) Pos(r) "><a href="http://thehill.com/homenews/campaign/271763-gop-debate-winners-and-losers" class="streamImage Pos(r) D(ib) js-stream-content-link W(100%)   js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:4;cposy:8;bpos:1;pos:1;ss_cid:f75e38cf-d1e2-416f-b063-ef98eac4511d;refcnt:2;imgt:ss;g:f4f1d5f1-7e87-3f0b-a1e6-772897de19b7;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:img;elmt:ct;r:4000025600S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/c1QP1lhJ8_BqqPQ0V2aNLg--/Zmk9c3RyaW07aD0xOTA7cHlvZmY9MDtxPTk1O3c9MTkwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/c01937ce59c6d043f20cba85285fdfd5" height="190" width="190" class="W(100%) H(a) BackgroundPic rounded-img" alt="">

</a></div>
    
    <div class="strm-right Pos(r) Mstart(29%)"><div class="strm-headline Pend(14px)">
            <div class="strm-headline-label js-content-label C(c_politics) Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="politics">Politics</div>
            <h3 class="Mb(4px) Mb(0px)--md1160 LineClamp(2,42px) Lh(21px)">
                <a href="http://thehill.com/homenews/campaign/271763-gop-debate-winners-and-losers" class="O(n):f C($m_blue):f D(b) Pos(r) js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:4;cposy:8;bpos:1;pos:1;ss_cid:f75e38cf-d1e2-416f-b063-ef98eac4511d;refcnt:2;imgt:ss;g:f4f1d5f1-7e87-3f0b-a1e6-772897de19b7;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:hdln;elmt:ct;r:4000025600S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><span>GOP debate winners and losers</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,34px) Lh(17px) C($gray) Bfv(h) D(n)--md1160">The Republican presidential debate on Thursday night will be remembered for its insults, showdowns, and one very personal declaration from GOP front-runner Donald TrumpDonald TrumpGOP debate winners and losers Rubio on insults: I wouldn&#39;t do anything &#39;Jesus wouldn&#39;t be proud of&#39; Rubio, Trump share awkward fist bump after rancorous debate MORE about his anatomy.Â  Hereâs a look at the winners and losers from a chaotic and memorable night in Detroit only two days before the next round of contests, as the challengers seek to shake Trump out of the top spot. WINNERSÂ  Fox News Moderator Megyn KellyÂ Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â Â  âMr. Trump. Hello. How are you?âÂ  Those words from Megyn Kelly kicked off the first</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Tt(c) Mt(2px)">The Hill</span>
            </div>
        </div>
        
        <div class="Mt(8px) Mt(7px)--md1160 Ov(h)">
            
            
                <a href="http://www.usatoday.com/picture-gallery/news/politics/elections/2016/03/03/photos-the-11th-republican-debate/81290160/" data-uuid="2db57017-a414-3d42-8491-9c860a97f3d7" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Mb(6px)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb) Pend(13px)"    data-ylk="cpos:4;cposy:9;bpos:1;pos:2;ss_cid:f75e38cf-d1e2-416f-b063-ef98eac4511d;refcnt:2;imgt:ss;g:2db57017-a414-3d42-8491-9c860a97f3d7;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:rhdln;elmt:ct;r:4000025600S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/fP7YxN3f7cS5nWK7jBIcKg--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/6a7e888321b94c1b1b2cc629b17b204f" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160" alt=""><div class="D(ib) W(100%)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Photos: The 11th Republican debate</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">USA Today</span></div></a>
            
            
            
                <a href="http://www.politico.com/tipsheets/the-2016-blast/2016/03/trump-handed-hardest-debate-213023" data-uuid="2a1f12fa-c0f0-373c-b194-c68bf21e3046" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb)"    data-ylk="cpos:4;cposy:10;bpos:1;pos:3;ss_cid:f75e38cf-d1e2-416f-b063-ef98eac4511d;refcnt:2;imgt:ss;g:2a1f12fa-c0f0-373c-b194-c68bf21e3046;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:rhdln;elmt:ct;r:4000025600S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/tBrsxiZlXWE1OkFO1RME8Q--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/9b95474237dd5dfd3ea65dd8c9d89e7d" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160" alt=""><div class="D(ib) W(100%)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Trump handed hardest debate</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">POLITICO</span></div></a>
            
            
        </div>
        
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:4;cposy:8;bpos:1;pos:1;ss_cid:f75e38cf-d1e2-416f-b063-ef98eac4511d;refcnt:2;imgt:ss;g:f4f1d5f1-7e87-3f0b-a1e6-772897de19b7;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;r:4000025600S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="YCT:001000031|YCT:001000780|YCT:001000803" data-offnet="1" data-uuid="d776207d-d02f-3080-b59b-b0f3c50ef0bd" data-type="article" data-cluster="6d6d97b3-33c9-4976-a44d-c08b127f1907"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="strm-default-clusters Pos(r) Wow(bw) Cf Pend(30px) ">

    
    <div class="strm-left Fl(start) Mend(18px) Pos(r) "><a href="http://madamenoire.com/617410/receipt-outrage/" class="streamImage Pos(r) D(ib) js-stream-content-link W(100%)   js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:5;cposy:11;bpos:1;pos:1;ss_cid:6d6d97b3-33c9-4976-a44d-c08b127f1907;refcnt:2;imgt:ss;g:d776207d-d02f-3080-b59b-b0f3c50ef0bd;ct:1;pkgt:cluster_first_img;grpt:storyCluster;cnt_tpc:U.S.;elm:img;elmt:ct;r:4000026510S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="beauty"><img src="/sy/uu/api/res/1.2/QR417fmOeKFMkITUjh8VzA--/Zmk9c3RyaW07aD0zODA7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/a244d3f2917609dac2c74dbbdfa7b11d" height="190" width="190" class="W(100%) H(a) BackgroundPic rounded-img" alt="">

</a></div>
    
    <div class="strm-right Pos(r) Mstart(29%)"><div class="strm-headline Pend(14px)">
            <div class="strm-headline-label js-content-label C(c_us) Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="us">U.S.</div>
            <h3 class="Mb(4px) Mb(0px)--md1160 LineClamp(2,42px) Lh(21px)">
                <a href="http://madamenoire.com/617410/receipt-outrage/" class="O(n):f C($m_blue):f D(b) Pos(r) js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:5;cposy:11;bpos:1;pos:1;ss_cid:6d6d97b3-33c9-4976-a44d-c08b127f1907;refcnt:2;imgt:ss;g:d776207d-d02f-3080-b59b-b0f3c50ef0bd;ct:1;pkgt:cluster_first_img;grpt:storyCluster;cnt_tpc:U.S.;elm:hdln;elmt:ct;r:4000026510S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="beauty"><span>Are You Not Black?! I&#39;m Over Non-Racist Receipt Outrage</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,34px) Lh(17px) C($gray) Bfv(h) D(n)--md1160">Rolman Sparkman and his girlfriend Arainia Brown are the reason people feel the need to constantly remindÂ Black folks everythingâs not racist. The Austin, TX, couple caused a waiter to lose his jobÂ because he identified them as âBlack pplâ on their receipt at IHOP. The kicker? Arainia and RolmanÂ areÂ Black - and so is the former waiter, Dwayne Williams. So what, pray tell, isÂ the problem? My guess is paranoia and faux outrage. Arainia is the one who noticed the labeling on the receipt and pointed it out to her boyfriend. When Fox news reached out to her about the incident, she asked, âWhat does this mean? Are you going to do something to my food or my card?â Huh? Rolmanâs dramatic response to</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Tt(c) Mt(2px)">madamenoire.com</span>
            </div>
        </div>
        
        <div class="Mt(8px) Mt(7px)--md1160 Ov(h)">
            
            
                <a href="http://www.eater.com/2016/3/2/11148584/ihop-receipt-black-ppl" data-uuid="8a18abc1-c916-34f8-8033-23dc80c2d22c" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Mb(6px)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb) Pend(13px)"    data-ylk="cpos:5;cposy:12;bpos:1;pos:2;ss_cid:6d6d97b3-33c9-4976-a44d-c08b127f1907;refcnt:2;imgt:ss;g:8a18abc1-c916-34f8-8033-23dc80c2d22c;ct:1;pkgt:cluster_first_img;grpt:storyCluster;cnt_tpc:U.S.;elm:rhdln;elmt:ct;r:4000026510S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="food"><img src="/sy/uu/api/res/1.2/vjTlegkTryf3ietp0VpL8g--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en-US/homerun/eater.vox.com/4f2696754d44b571170ac19bbd9a45b6" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160" alt=""><div class="D(ib) W(100%)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Server at IHOP Labels Couple&#39;s Receipt &#39;BLACK PPL&#39;</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Eater</span></div></a>
            
            
            
                <a href="http://news.yahoo.com/austin-couple-received-racist-receipt-152700968.html" data-uuid="5bd72e3d-7f8f-3f66-842e-c57cb63f16ae" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb)"    data-ylk="cpos:5;cposy:13;bpos:1;pos:3;ss_cid:6d6d97b3-33c9-4976-a44d-c08b127f1907;refcnt:2;imgt:ss;g:5bd72e3d-7f8f-3f66-842e-c57cb63f16ae;ct:1;pkgt:cluster_first_img;grpt:storyCluster;cnt_tpc:U.S.;elm:rhdln;elmt:ct;r:4000026510S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="beauty"><div class="D(ib)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Austin Couple Received a Racist Receipt From IHOP â and This Is How They Responded</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Mic</span></div></a>
            
            
        </div>
        
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:5;cposy:11;bpos:1;pos:1;ss_cid:6d6d97b3-33c9-4976-a44d-c08b127f1907;refcnt:2;imgt:ss;g:d776207d-d02f-3080-b59b-b0f3c50ef0bd;ct:1;pkgt:cluster_first_img;grpt:storyCluster;cnt_tpc:U.S.;r:4000026510S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Kate_Taylor|WIKIID:Hot_dog|WIKIID:Burger_King|YCT:001000560|YCT:001000611|YCT:001000031"  data-uuid="39c55862-d5d5-34fe-90c5-868405ef714d" data-type="article" data-cluster="807b4e84-8754-4758-9e2c-727066e1b89b"  data-hosted="HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="strm-default-clusters Pos(r) Wow(bw) Cf Pend(30px) ">

    
    <div class="strm-left Fl(start) Mend(18px) Pos(r) "><a href="http://finance.yahoo.com/news/tried-burger-kings-menu-item-135902788.html" class="streamImage Pos(r) D(ib) js-stream-content-link W(100%)   js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:6;cposy:14;bpos:1;pos:1;ss_cid:807b4e84-8754-4758-9e2c-727066e1b89b;refcnt:2;subsec:530;imgt:ss;g:39c55862-d5d5-34fe-90c5-868405ef714d;ct:1;pkgt:cluster_first_img;grpt:storyCluster;cnt_tpc:Lifestyle;elm:img;elmt:ct;r:4000020080S00001;ccode:mega_global_ranking_hlv2_up_based;" ><img src="/sy/uu/api/res/1.2/OptmO8bIjdcC_7W6cCIBxA--/Zmk9c3RyaW07aD0zODA7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://globalfinance.zenfs.com/en_us/Finance/US_AFTP_SILICONALLEY_H_LIVE/We_tried_Burger_Kings_menu-0ea0152765dfe89e1c1fd098edae63d1" height="190" width="190" class="W(100%) H(a) BackgroundPic rounded-img" alt="">

</a></div>
    
    <div class="strm-right Pos(r) Mstart(29%)"><div class="strm-headline Pend(14px)">
            <div class="strm-headline-label js-content-label C(c_lifestyle) Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="lifestyle">Lifestyle</div>
            <h3 class="Mb(4px) Mb(0px)--md1160 LineClamp(2,42px) Lh(21px)">
                <a href="http://finance.yahoo.com/news/tried-burger-kings-menu-item-135902788.html" class="O(n):f C($m_blue):f D(b) Pos(r) js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:6;cposy:14;bpos:1;pos:1;ss_cid:807b4e84-8754-4758-9e2c-727066e1b89b;refcnt:2;subsec:530;imgt:ss;g:39c55862-d5d5-34fe-90c5-868405ef714d;ct:1;pkgt:cluster_first_img;grpt:storyCluster;cnt_tpc:Lifestyle;elm:hdln;elmt:ct;r:4000020080S00001;ccode:mega_global_ranking_hlv2_up_based;" ><span>We tried Burger King&#39;s menu item that&#39;s being called a &#39;disgusting disgrace&#39; - here&#39;s what it&#39;s really like</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,34px) Lh(17px) C($gray) Bfv(h) D(n)--md1160">(Kate Taylor) 
When Burger King launched its grilled hot dog last month, it was touted as the biggest new product in decades.Â 
While theÂ hot dog has received mostly positive reviews, Steve Cuozzo at The New York Post slammed the menu item, calling it</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Tt(c) Mt(2px)">Business Insider</span>
            </div>
        </div>
        
        <div class="Mt(8px) Mt(7px)--md1160 Ov(h)">
            
            
                <a href="http://nypost.com/2016/03/02/burger-kings-hot-dogs-are-a-disgusting-disgrace/" data-uuid="1581f39e-8088-3505-88b7-91a8244f73e7" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Mb(6px)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb) Pend(13px)"    data-ylk="cpos:6;cposy:15;bpos:1;pos:2;ss_cid:807b4e84-8754-4758-9e2c-727066e1b89b;refcnt:2;subsec:530;imgt:ss;g:1581f39e-8088-3505-88b7-91a8244f73e7;ct:1;pkgt:cluster_first_img;grpt:storyCluster;cnt_tpc:Lifestyle;elm:rhdln;elmt:ct;r:4000020080S00001;ccode:mega_global_ranking_hlv2_up_based;t4:rel;"  data-attrcat="news"><img src="/sy/uu/api/res/1.2/vhmVyhcP1_5TmUxw85PXKw--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/290a5582173f0aa8dfca2b78e30d8193" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160" alt=""><div class="D(ib) W(100%)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Burger Kingâs hot dogs are a disgusting disgrace</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">New York Post</span></div></a>
            
            
            
                <a href="http://www.marketwatch.com/News/Story/Story.aspx?guid=140DC62C-E0A6-11E5-B9F0-6FCBBC2751E2&amp;siteid=yhoof2" data-uuid="e7f24a45-fe8e-36df-b37d-43665d4a0c39" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb)"    data-ylk="cpos:6;cposy:16;bpos:1;pos:3;ss_cid:807b4e84-8754-4758-9e2c-727066e1b89b;refcnt:2;subsec:530;imgt:ss;g:e7f24a45-fe8e-36df-b37d-43665d4a0c39;ct:1;pkgt:cluster_first_img;grpt:storyCluster;cnt_tpc:Lifestyle;elm:rhdln;elmt:ct;r:4000020080S00001;ccode:mega_global_ranking_hlv2_up_based;t4:rel;"  data-attrcat="finance"><div class="D(ib)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Why Burger King hot dogs are a âculinary calamityâ</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">MarketWatch</span></div></a>
            
            
        </div>
        
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions has-comments js-stream-dislike-disabled">
    
    <li class="ActionComments D(ib) O(n) Mstart(-1px) Pos(r) " data-cmntnum="530"><span class="js-stream-comment-counter D(b) Fz(11px) Ta(c) Fw(b) C(#96989f) js-stream-comment-hidden">530</span><span class="js-stream-comment-counter-update D(b) Fz(11px) Ta(c) Fw(b) C(#96989f)"></span><a href="javascript:void(0)" class="Pos(r) js-stream-comments-button-drawer Td(n) O(n) C($c_icon) Px(14px) Pb(5px) Pt(25px)" role="button" tabindex="0"   data-ylk="cpos:6;cposy:14;bpos:1;pos:1;ss_cid:807b4e84-8754-4758-9e2c-727066e1b89b;refcnt:2;subsec:530;imgt:ss;g:39c55862-d5d5-34fe-90c5-868405ef714d;ct:1;pkgt:cluster_first_img;grpt:storyCluster;cnt_tpc:Lifestyle;r:4000020080S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;slk:cmmt;itc:1;"><b aria-live="polite" class="js-stream-comment-label ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) Lh(14px) Py(7px) End(-25px) C($signin_blue) js-stream-comments-button>Start(50%) js-stream-comments-button:h>Start(a)">Comments</b><i class="Icon-Fp2 IconComments Fz(21px) "></i></a>
    </li>
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:6;cposy:14;bpos:1;pos:1;ss_cid:807b4e84-8754-4758-9e2c-727066e1b89b;refcnt:2;subsec:530;imgt:ss;g:39c55862-d5d5-34fe-90c5-868405ef714d;ct:1;pkgt:cluster_first_img;grpt:storyCluster;cnt_tpc:Lifestyle;r:4000020080S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Ted_Cruz|YCT:001000671" data-offnet="1" data-uuid="eef30880-8c04-3b04-8f46-f4fef5beb812" data-type="article" data-cluster="2fb9aafb-d0a5-404e-9599-864a622cb31d"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="strm-default-clusters Pos(r) Wow(bw) Cf Pend(30px) ">

    
    <div class="strm-left Fl(start) Mend(18px) Pos(r) "><a href="https://www.washingtonpost.com/blogs/right-turn/wp/2016/03/04/cruzs-problem-in-a-nutshell/" class="streamImage Pos(r) D(ib) js-stream-content-link W(100%)   js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:7;cposy:17;bpos:1;pos:1;ss_cid:2fb9aafb-d0a5-404e-9599-864a622cb31d;refcnt:2;imgt:ss;g:eef30880-8c04-3b04-8f46-f4fef5beb812;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:img;elmt:ct;r:4000018280S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="190" width="190" class="W(100%) H(a) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/fP3.mSwfMZVhS.ZzJ8jooQ--/Zmk9c3RyaW07aD0zODA7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/e2c69e4b3ec2a308120937e8ad155e74')" alt="">

</a></div>
    
    <div class="strm-right Pos(r) Mstart(29%)"><div class="strm-headline Pend(14px)">
            <div class="strm-headline-label js-content-label C(c_politics) Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="politics">Politics</div>
            <h3 class="Mb(4px) Mb(0px)--md1160 LineClamp(2,42px) Lh(21px)">
                <a href="https://www.washingtonpost.com/blogs/right-turn/wp/2016/03/04/cruzs-problem-in-a-nutshell/" class="O(n):f C($m_blue):f D(b) Pos(r) js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:7;cposy:17;bpos:1;pos:1;ss_cid:2fb9aafb-d0a5-404e-9599-864a622cb31d;refcnt:2;imgt:ss;g:eef30880-8c04-3b04-8f46-f4fef5beb812;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:hdln;elmt:ct;r:4000018280S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><span>Cruzâs problem in a nutshell</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,34px) Lh(17px) C($gray) Bfv(h) D(n)--md1160">Over the weekend there will be GOP presidential contests in Kansas, Kentucky, Louisiana, Maine and Puerto Rico. (All the states are holding caucuses except Louisiana.) The predicament of Sen. Ted Cruz (R-Tex.) will be on full display. Cruz trails Donald Trump in the most recent polling inÂ Louisiana by nearly 20 points. Polling is scant in Kansas, but Trump leads there as well. Understand that deep red states with a high percentage of evangelical voters were supposed to be Cruzâs bread and butter. It did not turn out that way on March 1 when he lost all the SEC states except his own (and came in third in Georgia), and that pattern is very likely to continue. As Dan McLaughlin points out: If you</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Tt(c) Mt(2px)">Washington Post</span>
            </div>
        </div>
        
        <div class="Mt(8px) Mt(7px)--md1160 Ov(h)">
            
            
                <a href="http://news.yahoo.com/trump-gop-nominee-just-got-real-congress-083905342--election.html" data-uuid="ed038393-b2cc-37f3-9a56-22c832b54346" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Mb(6px)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb) Pend(13px)"    data-ylk="cpos:7;cposy:18;bpos:1;pos:2;ss_cid:2fb9aafb-d0a5-404e-9599-864a622cb31d;refcnt:2;imgt:ss;g:ed038393-b2cc-37f3-9a56-22c832b54346;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:rhdln;elmt:ct;r:4000018280S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/TdRCwIbMIQXbMVE5UOF13w--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en_us/News/ap_webfeeds/6f726065686a120b920f6a706700ba7d.jpg.cf.jpg')" alt=""><div class="D(ib) W(100%)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Trump as GOP nominee: It just got real for Congress</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Associated Press</span></div></a>
            
            
            
                <a href="http://www.usatoday.com/story/news/politics/elections/2016/03/03/gop-michigan-debate/81256782/" data-uuid="95925914-ef66-332a-b589-29889d115437" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb)"    data-ylk="cpos:7;cposy:19;bpos:1;pos:3;ss_cid:2fb9aafb-d0a5-404e-9599-864a622cb31d;refcnt:2;imgt:ss;g:95925914-ef66-332a-b589-29889d115437;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:rhdln;elmt:ct;r:4000018280S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/8HoBQJ4Kjea6vrXzWUgxxQ--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/68510de9df9a3875590f2d73a8b12eab')" alt=""><div class="D(ib) W(100%)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Ahead of GOP debate, Trump holds 10-point lead in Mich.</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">USA Today</span></div></a>
            
            
        </div>
        
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:7;cposy:17;bpos:1;pos:1;ss_cid:2fb9aafb-d0a5-404e-9599-864a622cb31d;refcnt:2;imgt:ss;g:eef30880-8c04-3b04-8f46-f4fef5beb812;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;r:4000018280S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="YCT:001000667|YCT:001000780|YCT:001000681" data-offnet="1" data-uuid="ef105aa6-3160-371c-8e54-f83c346ca3dd" data-type="article" data-cluster="f2a01e38-09d9-4d32-99bd-1dff25acffbd"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="strm-default-clusters Pos(r) Wow(bw) Cf Pend(30px) ">

    
    <div class="strm-left Fl(start) Mend(18px) Pos(r) "><a href="http://www.chicagotribune.com/suburbs/franklin-park/crime/ct-fhj-florida-man-drug-arrest-tl-0310-20160303-story.html" class="streamImage Pos(r) D(ib) js-stream-content-link W(100%)   js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:8;cposy:20;bpos:1;pos:1;ss_cid:f2a01e38-09d9-4d32-99bd-1dff25acffbd;refcnt:2;imgt:ss;g:ef105aa6-3160-371c-8e54-f83c346ca3dd;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:U.S.;elm:img;elmt:ct;r:4000018970S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="190" width="190" class="W(100%) H(a) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/88Lx1HHi22QsgYbKpoz.8Q--/Zmk9c3RyaW07aD0zODA7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/8d641e31cebc3d3e8142fd6a4648fc3b.cf.jpg')" alt="">

</a></div>
    
    <div class="strm-right Pos(r) Mstart(29%)"><div class="strm-headline Pend(14px)">
            <div class="strm-headline-label js-content-label C(c_us) Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="us">U.S.</div>
            <h3 class="Mb(4px) Mb(0px)--md1160 LineClamp(2,42px) Lh(21px)">
                <a href="http://www.chicagotribune.com/suburbs/franklin-park/crime/ct-fhj-florida-man-drug-arrest-tl-0310-20160303-story.html" class="O(n):f C($m_blue):f D(b) Pos(r) js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:8;cposy:20;bpos:1;pos:1;ss_cid:f2a01e38-09d9-4d32-99bd-1dff25acffbd;refcnt:2;imgt:ss;g:ef105aa6-3160-371c-8e54-f83c346ca3dd;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:U.S.;elm:hdln;elmt:ct;r:4000018970S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><span>Sheriff: Florida man used custom boots to smuggle drugs cross-country</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,34px) Lh(17px) C($gray) Bfv(h) D(n)--md1160">A Florida man drove from New York to Chicago to deliver heroin he was smuggling in his car and in custom shoes, according to the Cook County Sheriff&#39;s Office. Authorities said William M. Fajardo, 26, was driving in unincorporated Leyden Township near Franklin Park on Feb. 27, when officers with the Cook County Sheriff&#39;s Street Crimes Suppression Unit stopped Fajardo for failing to use a turn signal in the 2400 block of Mannheim Road around 11:25 p.m. Authorities said that during the traffic stop, officers suspected Fajardo had heroin and drug paraphernalia in his car and took him into custody. Once back at sheriff&#39;s police headquarters, officers called a Des Plaines Police Department K-9 unit</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Tt(c) Mt(2px)">Chicago Tribune</span>
            </div>
        </div>
        
        <div class="Mt(8px) Mt(7px)--md1160 Ov(h)">
            
            
                <a href="http://wtvr.com/2016/03/03/keith-smith-charges/" data-uuid="93bb7fc4-3d3d-38ae-b063-1b00258859a0" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Mb(6px)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb) Pend(13px)"    data-ylk="cpos:8;cposy:21;bpos:1;pos:2;ss_cid:f2a01e38-09d9-4d32-99bd-1dff25acffbd;refcnt:2;imgt:ss;g:93bb7fc4-3d3d-38ae-b063-1b00258859a0;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:U.S.;elm:rhdln;elmt:ct;r:4000018970S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/SXnN1uyZzCmfCj7m7zhKKw--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/ca78d54078e62727a2e15bfdafcaebd6')" alt=""><div class="D(ib) W(100%)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Man charged with attempted capital murder after Richmond Police shooting</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">WTVR.com</span></div></a>
            
            
            
                <a href="http://www.cleveland.com/metro/index.ssf/2016/03/cleveland_man_charged_with_str.html" data-uuid="794b36fd-b725-3bbd-b195-7b9f336865d6" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb)"    data-ylk="cpos:8;cposy:22;bpos:1;pos:3;ss_cid:f2a01e38-09d9-4d32-99bd-1dff25acffbd;refcnt:2;imgt:ss;g:794b36fd-b725-3bbd-b195-7b9f336865d6;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:U.S.;elm:rhdln;elmt:ct;r:4000018970S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/pUvGp3op0lwGJ00faypmFA--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/97481dc8434434e2372afc387a6fc8ba')" alt=""><div class="D(ib) W(100%)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Cleveland man charged with strangling roommate</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">cleveland.com</span></div></a>
            
            
        </div>
        
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:8;cposy:20;bpos:1;pos:1;ss_cid:f2a01e38-09d9-4d32-99bd-1dff25acffbd;refcnt:2;imgt:ss;g:ef105aa6-3160-371c-8e54-f83c346ca3dd;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:U.S.;r:4000018970S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Hillary_Clinton|YCT:001000661|YCT:001000638|YCT:001000637|YCT:001000695|YCT:001000681|YCT:001000648|YCT:001000646" data-offnet="1" data-uuid="2edb99c4-23e0-3300-bda4-33d8f95d92cc" data-type="article" data-cluster="076238af-47cb-4bdc-8c28-1d7d96055a04"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="strm-default-clusters Pos(r) Wow(bw) Cf Pend(30px) ">

    
    <div class="strm-left Fl(start) Mend(18px) Pos(r) "><a href="http://www.ibtimes.com/political-capital/hillary-clinton-spotlighting-crisis-flint-michigan-voted-against-measure-prevent" class="streamImage Pos(r) D(ib) js-stream-content-link W(100%)   js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:9;cposy:23;bpos:1;pos:1;ss_cid:076238af-47cb-4bdc-8c28-1d7d96055a04;refcnt:2;imgt:ss;g:2edb99c4-23e0-3300-bda4-33d8f95d92cc;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:img;elmt:ct;r:4000020240S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="190" width="190" class="W(100%) H(a) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/.hR6HpfsIsTNDOpo0YsBMg--/Zmk9c3RyaW07aD0zODA7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/069ce729b423a25843d5b3b8960c94a4')" alt="">

</a></div>
    
    <div class="strm-right Pos(r) Mstart(29%)"><div class="strm-headline Pend(14px)">
            <div class="strm-headline-label js-content-label C(c_politics) Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="politics">Politics</div>
            <h3 class="Mb(4px) Mb(0px)--md1160 LineClamp(2,42px) Lh(21px)">
                <a href="http://www.ibtimes.com/political-capital/hillary-clinton-spotlighting-crisis-flint-michigan-voted-against-measure-prevent" class="O(n):f C($m_blue):f D(b) Pos(r) js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:9;cposy:23;bpos:1;pos:1;ss_cid:076238af-47cb-4bdc-8c28-1d7d96055a04;refcnt:2;imgt:ss;g:2edb99c4-23e0-3300-bda4-33d8f95d92cc;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:hdln;elmt:ct;r:4000020240S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><span>Hillary Clinton, Before Spotlighting Crisis In Flint, Michigan, Voted Against Measure To Prevent Groundwater Pollution</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary LineClamp(2,34px) Lh(17px) C($gray) Bfv(h) D(n)--md1160">When the Democratic presidential contenders meet on Sunday for their debate in Flint, Michigan - where thousands of residents have been poisoned by polluted water - the candidatesâ records on clean water policy are likely to be in the spotlight. Hillary Clinton seems eager for that discussion, recently telling NPR: âThe idea that you would have a community in the United States of America of nearly 100,000 people who were drinking and bathing in lead-contaminated water infuriates me.â But despite that rhetoric, the issue of clean water may be politically perilous for the leading Democratic candidate, thanks to her vote against banning a possible carcinogen at the center of one of the largest water</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Tt(c) Mt(2px)">International Business Times</span>
            </div>
        </div>
        
        <div class="Mt(8px) Mt(7px)--md1160 Ov(h)">
            
            
                <a href="http://www.mlive.com/news/flint/index.ssf/2016/03/welcome_to_flint_michigan_were.html" data-uuid="275b62e9-66ed-3622-979c-6efb39b97a10" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Mb(6px)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb) Pend(13px)"    data-ylk="cpos:9;cposy:24;bpos:1;pos:2;ss_cid:076238af-47cb-4bdc-8c28-1d7d96055a04;refcnt:2;imgt:ss;g:275b62e9-66ed-3622-979c-6efb39b97a10;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:rhdln;elmt:ct;r:4000020240S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/D0BnR6dBLU8YUsUT242KKw--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/fc0783ddb294cf6dd067cbc6f8994caa')" alt=""><div class="D(ib) W(100%)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Flint, Michigan: Welcome to America's forgotten city</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">MLive.com</span></div></a>
            
            
            
                <a href="http://thinkprogress.org/health/2016/03/03/3756559/marco-rubio-slams-democrats-for-politicizing-flint-water-crisis/" data-uuid="1055767a-2a47-3d64-8c60-e03d84caf88e" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb)"    data-ylk="cpos:9;cposy:25;bpos:1;pos:3;ss_cid:076238af-47cb-4bdc-8c28-1d7d96055a04;refcnt:2;imgt:ss;g:1055767a-2a47-3d64-8c60-e03d84caf88e;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:rhdln;elmt:ct;r:4000020240S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/X6HmlXI8Tl0JtqjNKubAaQ--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/4bd66afa19b382261830023f675bcc41')" alt=""><div class="D(ib) W(100%)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">At Fox News Debate In Michigan, Republican Candidates Spend Less Than 2 Minutes On Flint</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">ThinkProgress</span></div></a>
            
            
        </div>
        
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:9;cposy:23;bpos:1;pos:1;ss_cid:076238af-47cb-4bdc-8c28-1d7d96055a04;refcnt:2;imgt:ss;g:2edb99c4-23e0-3300-bda4-33d8f95d92cc;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;r:4000020240S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Jared_Fogle"  data-uuid="4766d312-9aca-3d00-91b7-b2ae2c6d5e2a" data-type="video"  data-hosted="HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="Pos(r) Wow(bw) Cf Pend(30px)">
    <div class="strm-left Fl(start) Pos(r) Pend(20px) js-video-content Op(.99)">
        <div class="Pos(r) js-stream-content-img rounded-img Bd Td(n) W(100%) Bxz(bb)"><span class="D(b) js-video-target js-video-image BackgroundPic ImageLoader" style="padding-bottom:56.25%;height:auto;width:100%background-image:url('https://s.yimg.com/uu/api/res/1.2/98dHC97D.3ZDXc7hrES8FA--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en-us/video/video.complex.com/6422a3a5a5adcb06cdec8a5c0eb5cec4');"></span>
    <span class="Pos(a) T(50%) Start(50%) Mstart(-33px) Mt(-33px) orb-sprite video-icon Bgp(-162px,-32px) W(66px) H(66px) Ta(c) Va(m)"></span>


</div>
    </div>
    <div class="strm-right Fl(start) W(66%)">
        <div class="js-content-label C(c_celebrity) Tt(c) Fw(b) Mb(6px) Lh(12px) Fz(13px)" data-cat-label="celebrity">Celebrity</div>
        <div class="strm-headline">
            <h3 class="Mb(6px) LineClamp(2,42px) Lh(21px)">
                <a href="https://www.yahoo.com/music/jared-fogle-eats-sh-t-022938157.html" class="Pos(r) O(n):f C($m_blue):f js-stream-content-link js-content-viewer rapid-noclick-resp rapidnofollow js-stream-item-title js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:10;cposy:26;bpos:1;pos:1;subsec:394;imgt:ss;g:4766d312-9aca-3d00-91b7-b2ae2c6d5e2a;ct:3;pkgt:video;grpt:storyCluster;cnt_tpc:Celebrity;elm:hdln;elmt:ct;r:4000015680S00002;ccode:mega_global_ranking_hlv2_up_based;" ><span>Jared Fogle Eats Sh*t in Prison, Quickly Gaining Back Pre-Subway Weight</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%)"></u></a>
            </h3>
            <div>
                <p class="Pos(r) stream-summary M(0) LineClamp(2,36px) Lh(17px) C($gray) Bfv(h)">Disgraced former Subway spokesperson Jared Fogle has reportedly already packed on 30 pounds behind bars.</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Mt(6px)">Complex</span>
            </div>
        </div>
    </div>
    <div class="Pos(a) End(0) T(0) W(30px) Mstart(12px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions has-comments js-stream-dislike-disabled">
    
    <li class="ActionComments D(ib) O(n) Mstart(-1px) Pos(r) " data-cmntnum="394"><span class="js-stream-comment-counter D(b) Fz(11px) Ta(c) Fw(b) C(#96989f) js-stream-comment-hidden">394</span><span class="js-stream-comment-counter-update D(b) Fz(11px) Ta(c) Fw(b) C(#96989f)"></span><a href="javascript:void(0)" class="Pos(r) js-stream-comments-button-drawer Td(n) O(n) C($c_icon) Px(14px) Pb(5px) Pt(25px)" role="button" tabindex="0"   data-ylk="cpos:10;cposy:26;bpos:1;pos:1;subsec:394;imgt:ss;g:4766d312-9aca-3d00-91b7-b2ae2c6d5e2a;ct:3;pkgt:video;grpt:storyCluster;cnt_tpc:Celebrity;r:4000015680S00002;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;slk:cmmt;itc:1;"><b aria-live="polite" class="js-stream-comment-label ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) Lh(14px) Py(7px) End(-25px) C($signin_blue) js-stream-comments-button>Start(50%) js-stream-comments-button:h>Start(a)">Comments</b><i class="Icon-Fp2 IconComments Fz(21px) "></i></a>
    </li>
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:10;cposy:26;bpos:1;pos:1;subsec:394;imgt:ss;g:4766d312-9aca-3d00-91b7-b2ae2c6d5e2a;ct:3;pkgt:video;grpt:storyCluster;cnt_tpc:Celebrity;r:4000015680S00002;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    


    
    </ul>
</div>

<img src="/sy/os/mit/ape/m/81f43c2/t.gif" width="16" height="16" class="D(b) Mx(a) My(12px) js-stream-load-more ImageLoader" style="background-image:url('https://s.yimg.com/zz/nn/lib/metro/g/my/anim_loading_sm.gif')" alt="">


 </div> </div> </div>  <div class="App-Ft Row"> </div>            <!-- App close -->
            </div>
                </div>
            </div>
            <div class="Col3" id="Aside" role="complementary" tabindex="-1">
                <div class="Col3-stack" data-sticker-top="75px" >
                                <div id="applet_p_32209491" class=" M-0 js-applet trending Zoom-1  Mb(30px) " data-applet-guid="p_32209491" data-applet-type="trending" data-applet-params="_suid:32209491" data-i13n="auto:true;sec:tc-ts" data-i13n-sec="tc-ts"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  <div id="mega-trending" class="slingstone-selected"><h2 class="trending-title Cur(p) D(ib) W(50%) Fz(15px) Ell Ov(v) gifts-selected_C($disabledHeading) Trs($trendTrs)" data-category="slingstone">Trending Now</h2><ul class="Pos(r) Mt(10px)">
        <li class="trending-list" data-category="slingstone">
            <ul class="M(0) Mstart(-8px) ua-ie8_W(100%) ua-ie7_W(100%) Op(1) gifts-selected_Op(0) gifts-selected_V(h) Trs($trendTrs)"><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Katy+Perry&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Katy Perry"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:1;bpos:1;ccode:trending_search;g:da71471b-3194-3f69-8890-3ee0051f4cb4;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">1.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Katy Perry</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Kris+Jenner&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Kris Jenner"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:2;bpos:1;ccode:trending_search;g:04ab3517-4c18-3cde-a396-44215bb1842c;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">2.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Kris Jenner</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Julianne+Hough&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Julianne Hough"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:3;bpos:1;ccode:trending_search;g:f438a429-16ea-3493-8dbe-8f4d128eefe4;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">3.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Julianne Hough</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Alicia+Keys&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Alicia Keys"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:4;bpos:1;ccode:trending_search;g:c5aa7f14-a473-34ec-9c2b-2385cf7a42c0;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">4.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Alicia Keys</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Amanda+Seyfried&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Amanda Seyfried"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:5;bpos:1;ccode:trending_search;g:bd444cd5-5dde-3403-b73c-38d8e1d0af8c;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">5.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Amanda Seyfried</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Prostate+cancer&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Prostate cancer"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:6;bpos:1;ccode:trending_search;g:f33defde-92b2-3936-b8d2-c54024e6f60f;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">6.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Prostate cancer</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Zoe+Saldana&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Zoe Saldana"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:7;bpos:1;ccode:trending_search;g:e049537a-e8fe-35b5-8efa-086adc7e0106;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">7.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Zoe Saldana</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=David+Duke&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="David Duke"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:8;bpos:1;ccode:trending_search;g:f4c34366-8cc1-3f5f-8736-49797569c727;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">8.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> David Duke</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Apple+iPhone&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Apple iPhone"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:9;bpos:1;ccode:trending_search;g:3d72001d-d82f-3e69-9cb3-ac64066ef067;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">9.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Apple iPhone</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Lady+Gaga&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Lady Gaga"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:10;bpos:1;ccode:trending_search;g:ae7ac82c-f70a-3da2-b696-2cdb9c3d0e3e;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">10.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Lady Gaga</span>
                    </a>
                </li></ul>
        </li></ul>
</div>
 </div> </div> </div>            <!-- App close -->
            </div>                            <div id="my-adsFPAD-base">
                    <div id="my-adsFPAD" class="D-n sda-DAPF">
                        <script>
                            rtAdStart = window.performance && window.performance.now && window.performance.now();
                        </script>
                        <div class="Mx-a" id="my-adsFPAD-iframe">
                            <!-- FPAD has no ad contents. Other Ad position will take over this ad --><noscript>
<!-- SpaceID=2023538075 loc=FPAD noad --><!-- fac-gd2-noad --><!-- gd2-status-2 --><!--QYZ CMS_NONE_SELECTED,,;;FPAD;2023538075;2-->
<img width="1" height="1" alt="" src="https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&amp;bs=(136c1pdeu(gid%24Okzx8DcyLjPvrchjZGr0dBYfMjAwMVbZyAURcDWk,st%241457113093508360,si%244452051,sp%242023538075,pv%241,v%242.0))&amp;t=J_3-D_3&amp;al=(as%24125ts4g1f,aid%248sOQvWKLDzw-,cr%24-1,ct%2425,at%24H,eob%24gd1_match_id=-1:ypos=FPAD)"></noscript>
                        </div>
                        <script>
                            rtAdDone = window.performance && window.performance.now && window.performance.now();
                        </script>
                    </div>
                </div>                           <div id="my-adsLREC-base" class="lrec-bgcolor">
                   <div id="my-adsLREC" class="Ta-c Pos-r Z-1 Ht-250 Mb-20">
                        <div id="my-adsLREC-iframe">
                            <noscript>
<!-- APT Vendor: Right Media, Format: Standard Graphical -->
<!--QYZ 2264184051,4467049051,;;LREC;2023538075;1-->
<img width="1" height="1" alt="" src="https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&amp;bs=(136c1pdeu(gid%24Okzx8DcyLjPvrchjZGr0dBYfMjAwMVbZyAURcDWk,st%241457113093508360,si%244452051,sp%242023538075,pv%241,v%242.0))&amp;t=J_3-D_3&amp;al=(as%2413akg5a88,aid%24Z9mQvWKLDzw-,bi%242264184051,agp%243459460551,cr%244467049051,ct%2425,at%24H,eob%24gd1_match_id=-1:ypos=LREC)"></noscript>
                        </div>
                    </div>
                </div>            <div id="applet_p_63794" class=" App_v2  M-0 js-applet weather Zoom-1  Mb(30px) " data-applet-guid="p_63794" data-applet-type="weather" data-applet-params="_suid:63794" data-i13n="auto:true;sec:app-wea" data-i13n-sec="app-wea"> <!-- App open -->
        <div class="App-Hd" data-region="header"> <div class="js-applet-view-container-header"> <div class="StencilRoot">
    
        <a href="https://weather.yahoo.com" class="C(#000) C($m_blue):h Td(n)">
            <h2 class="Fz(15px) Fw(b) Mb(18px) D(ib) Grid-U App-Title js-show-panel">
                <span class="js-city-name">
                    
                        
                            Yvrac, 33
                        
                    
                </span>
            </h2>
        </a>
    
    
    <div class="Grid-U">
        <div class="LocationPanel Pos(r) Fl(end) Z(3)"></div>
    </div>
    
</div>
 </div> </div>  <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  <div class="D(ib)">
    
    <ul class="P(0) Mt(0) Mend(0) Cl(b) Td(n) Mb(10px) Mstart(-2px) forecasts forecasts-12597132">
        
         <li class="Pos(r) Ta(c) D(tbc) Pend(30px)">
            <a href="https://weather.yahoo.com/fr/33/yvrac-12597132/" class="C(#000) Td(n)"   data-ylk="itc:0;elm:itm;elmt:ct;rspns:nav;">
                <h3 class="Fz(13px) Mb(7px)">Today</h3>
                <img class="D(b) M(a)" src="/sy/os/mit/ape/w/d8f6e02/dark/rain_day_night.png" width="43" height="43" alt="Showers" />
                <span class="Mstart(6px) D(tbc) Mt(2px)">
                    <b class="Fz(13px) Fw(b) D(tbc) Pend(3px)">50&#xb0;<b class="Hidden">f High</b></b>
                    <b class="Fz(13px) Fw(b) C($temp_low) D(tbc)">40&#xb0;<b class="Hidden">f Low</b></b>
                </span>
            </a>
        </li>
        
         <li class="Pos(r) Ta(c) D(tbc) Pend(30px)">
            <a href="https://weather.yahoo.com/fr/33/yvrac-12597132/" class="C(#000) Td(n)"   data-ylk="itc:0;elm:itm;elmt:ct;rspns:nav;">
                <h3 class="Fz(13px) Mb(7px)">Sat</h3>
                <img class="D(b) M(a)" src="/sy/os/mit/ape/w/d8f6e02/dark/scattered_showers_day_night.png" width="43" height="43" alt="Showers" />
                <span class="Mstart(6px) D(tbc) Mt(2px)">
                    <b class="Fz(13px) Fw(b) D(tbc) Pend(3px)">51&#xb0;<b class="Hidden">f High</b></b>
                    <b class="Fz(13px) Fw(b) C($temp_low) D(tbc)">36&#xb0;<b class="Hidden">f Low</b></b>
                </span>
            </a>
        </li>
        
         <li class="Pos(r) Ta(c) D(tbc) Pend(30px)">
            <a href="https://weather.yahoo.com/fr/33/yvrac-12597132/" class="C(#000) Td(n)"   data-ylk="itc:0;elm:itm;elmt:ct;rspns:nav;">
                <h3 class="Fz(13px) Mb(7px)">Sun</h3>
                <img class="D(b) M(a)" src="/sy/os/mit/ape/w/d8f6e02/dark/partly_cloudy_day.png" width="43" height="43" alt="Showers" />
                <span class="Mstart(6px) D(tbc) Mt(2px)">
                    <b class="Fz(13px) Fw(b) D(tbc) Pend(3px)">51&#xb0;<b class="Hidden">f High</b></b>
                    <b class="Fz(13px) Fw(b) C($temp_low) D(tbc)">39&#xb0;<b class="Hidden">f Low</b></b>
                </span>
            </a>
        </li>
        
         <li class="Pos(r) Ta(c) D(tbc) ">
            <a href="https://weather.yahoo.com/fr/33/yvrac-12597132/" class="C(#000) Td(n)"   data-ylk="itc:0;elm:itm;elmt:ct;rspns:nav;">
                <h3 class="Fz(13px) Mb(7px)">Mon</h3>
                <img class="D(b) M(a)" src="/sy/os/mit/ape/w/d8f6e02/dark/rain_day_night.png" width="43" height="43" alt="Showers" />
                <span class="Mstart(6px) D(tbc) Mt(2px)">
                    <b class="Fz(13px) Fw(b) D(tbc) Pend(3px)">47&#xb0;<b class="Hidden">f High</b></b>
                    <b class="Fz(13px) Fw(b) C($temp_low) D(tbc)">35&#xb0;<b class="Hidden">f Low</b></b>
                </span>
            </a>
        </li>
        
    </ul>
    
</div>
 </div> </div> </div>            <!-- App close -->
            </div>            <div id="applet_p_50000171" class=" M-0 js-applet activitylist Zoom-1  Mb-20 " data-applet-guid="p_50000171" data-applet-type="activitylist" data-applet-params="_suid:50000171" data-i13n="auto:true;sec:ltst" data-i13n-sec="ltst"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  
<ul class="Mb(0) P(0) js-activitylist-list">

<li class="js-activitylist-item My(20px)" data-id="140449816939">
    
    <a href="https://www.yahoo.com/politics/in-sign-of-apocalypse-donald-trump-defends-the-025053649.html" class="Td(n):h"   data-ylk="pkgt:2;ct:16;rspns:nav;t1:a4;t2:ltst;t3:post;elm:img;elmt:ct;itc:0;cat:false;g:-1;">
    <div class="Ta(c) Pos(r)">
        <img src="/sy/uu/api/res/1.2/oTAVNzkN0COLbNqxi8UnBw--/Zmk9ZmlsbDtoPTQxNDtweW9mZj0wO3E9ODA7dz00MTQ7c209MTthcHBpZD15dGFjaHlvbg--/http://l.yimg.com/dh/ap/default/160304/trump.jpg" width="207px" height="207px" class="Bdrs(115px)" alt="">
        
        <div class="orb-sprite C($description) Fz(11px) Pos(a) B(-5px) End(24px) D(tb) orb Bdrs(34px)">
            
        </div>
    </div><span class="D(b) Ta(c) Td(n) M(0) Pt(16px) Pb(23px) Px(50px)">
        <div class="C(#400090) Tt(c) Fw(b) Fz(12px) Mb(1px)">Yahoo Politics</div>
        <span class="C($title) C($m_blue):h Fw(b) Fz(16px) Lh(18px)">In sign of apocalypse, Donald Trump defends the size of hisâ¦ hands</span>
    </span>
    </a>
    
</li>

<li class="js-activitylist-item My(20px)" data-id="140449727824">
    
    <a href="https://esports.yahoo.com/overlook-overwatch-204407144.html" class="Td(n):h"   data-ylk="pkgt:2;ct:16;rspns:nav;t1:a4;t2:ltst;t3:post;elm:img;elmt:ct;itc:0;cat:false;g:-1;">
    <div class="Ta(c) Pos(r)">
        <img src="/sy/uu/api/res/1.2/2odp4qk6lAiFefWvf299RA--/Zmk9ZmlsbDtoPTQxNDtweW9mZj0wO3E9ODA7dz00MTQ7c209MTthcHBpZD15dGFjaHlvbg--/http://l.yimg.com/dh/ap/default/160304/overwatch.jpg" width="207px" height="207px" class="Bdrs(115px)" alt="">
        
        <div class="orb-sprite C($description) Fz(11px) Pos(a) B(-5px) End(24px) D(tb) orb Bdrs(34px)">
            
        </div>
    </div><span class="D(b) Ta(c) Td(n) M(0) Pt(16px) Pb(23px) Px(50px)">
        <div class="C(#400090) Tt(c) Fw(b) Fz(12px) Mb(1px)">Yahoo Esports</div>
        <span class="C($title) C($m_blue):h Fw(b) Fz(16px) Lh(18px)">An overview of Overwatch</span>
    </span>
    </a>
    
</li>

<li class="js-activitylist-item Mt(20px)" data-id="140449623279">
    
    <a href="https://www.yahoo.com/celebrity/meghan-trainor-on-teenage-insecurities-i-wore-073326786.html" class="Td(n):h"   data-ylk="pkgt:2;ct:16;rspns:nav;t1:a4;t2:ltst;t3:post;elm:img;elmt:ct;itc:0;cat:false;g:-1;">
    <div class="Ta(c) Pos(r)">
        <img src="/sy/uu/api/res/1.2/EkqUoj5wUCwLFHE6UQLaOA--/Zmk9ZmlsbDtoPTQxNDtweW9mZj0wO3E9ODA7dz00MTQ7c209MTthcHBpZD15dGFjaHlvbg--/http://l.yimg.com/dh/ap/default/160304/trainor.jpg" width="207px" height="207px" class="Bdrs(115px)" alt="">
        
        <div class="orb-sprite C($description) Fz(11px) Pos(a) B(-5px) End(24px) D(tb) orb Bdrs(34px)">
            
        </div>
    </div><span class="D(b) Ta(c) Td(n) M(0) Pt(16px) Pb(23px) Px(50px)">
        <div class="C(#400090) Tt(c) Fw(b) Fz(12px) Mb(1px)">Yahoo Celebrity</div>
        <span class="C($title) C($m_blue):h Fw(b) Fz(16px) Lh(18px)">Meghan Trainor on teenage insecurities</span>
    </span>
    </a>
    
</li>

</ul>

 </div> </div> </div>            <!-- App close -->
            </div>                           <div id="sticky-lrec2-footer" data-plugin="sticker" data-sticker-top="110px" data-sticker-forceposition="top">
               <div class="lrec-bgcolor">
               <div id="my-adsLREC2" class="Ta-c Mt-10 Mb-20 Pos-r Ht-250 darla" data-autorotate="1" data-autoeventrt="10000" data-config={"pos":"LREC2","id":"LREC2","clean":"my-adsLREC2","dest":"my-adsLREC2-iframe","metaSize":true,"w":300,"h":250,"fdb":true,"supports":{"exp-ovr":1}} data-lrec3replace="1" >
                   <div class='Mx-a Ta-c' id="my-adsLREC2-iframe">
                        
                   </div>
               </div>
                               <div id="my-adsLREC3" class="Ta-c Mt-10 Mb-20 Pos-r Ht-250 darla D-n" data-config={"pos":"LREC3","id":"LREC3","clean":"my-adsLREC3","dest":"my-adsLREC3-iframe","metaSize":true,"w":300,"h":250,"fdb":true,"supports":{"exp-ovr":1}}>
                    <div class='Mx-a Ta-c' id="my-adsLREC3-iframe">
                    </div>
               </div>
               <div id="my-adsLREC2-fallback" class="D-n">
                    <a href="https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=20&mc=click&pli=16563095&PluID=0&ord=${CACHEBUSTER}" style="background:url(https://www.yahoo.com/sy/nn/lib/metro/300_250_Human_Touch_mail.jpg) 0 0 no-repeat;height:250px;width:300px;display:block;margin:auto;"></a><img width="1" height="1" alt="" src="https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=19&mc=imp&pli=16563095&PluID=0&ord=${CACHEBUSTER}&rtu=-1" style="display:none">
               </div>
               </div>
                    <div id="Footer" class="Row Pstart-20 Pend-10 Pos-r" role="contentinfo">
                         <ul class="Bdrs(3px) Px(20px) Py(14px) Ta(c)" role="contentinfo" id="fp-footer"><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://info.yahoo.com/legal/us/yahoo/utos/terms/" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Terms</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://info.yahoo.com/privacy/us/yahoo/" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Privacy</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="https://advertising.yahoo.com/" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Advertise</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://info.yahoo.com/privacy/us/yahoo/relevantads.html" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">About our Ads</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://careers.yahoo.com/us" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Careers</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://help.yahoo.com/l/us/yahoo/helpcentral/" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Help</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="https://yahoo.uservoice.com/forums/341361-yahoo-home" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Feedback</a></li></ul></div>
                         
                    </div>
                </div>
            </div>
        </div>
    </div>





        
                        <div id="darla-assets-bottom">
                    <script type="text/x-safeframe" id="fc" _ver="2-9-7">{ "positions": [ { "html": "<!-- SpaceID=2023538075 loc=FPAD noad --><!-- fac-gd2-noad --><!-- gd2-status-2 --><!--QYZ CMS_NONE_SELECTED,,;;FPAD;2023538075;2-->", "id": "FPAD", "meta": { "y": { "cscHTML": "<scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_d==null)window.xzq_d=new Object();\nwindow.xzq_d['8sOQvWKLDzw-']='(as$125ts4g1f,aid$8sOQvWKLDzw-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=FPAD)';\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136c1pdeu(gid$Okzx8DcyLjPvrchjZGr0dBYfMjAwMVbZyAURcDWk,st$1457113093508360,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$125ts4g1f,aid$8sOQvWKLDzw-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=FPAD)\"></noscr"+"ipt>", "cscURI": "https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136c1pdeu(gid$Okzx8DcyLjPvrchjZGr0dBYfMjAwMVbZyAURcDWk,st$1457113093508360,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$125ts4g1f,aid$8sOQvWKLDzw-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=FPAD)", "impID": "", "supp_ugc": "0", "placementID": "-1", "creativeID": "-1", "serveTime": "1.4571130935084E+15", "behavior": "non_exp", "adID": "#2", "matchID": "#2", "err": "invalid_space", "facStatus": {}, "hasExternal": 0, "size": "", "bookID": "-1", "serveType": "-1", "slotID": "0", "fdb": "{ \"fdb_url\": \"http:\\/\\/beap-bc.yahoo.com\\/af?bv=1.0.0&bs=(15ir45r6b(gid$jmTVQDk4LjHHbFsHU5jMkgKkMTAuNwAAAACljpkK,st$1402537233026922,srv$1,si$13303551,adv$25941429036,ct$25,li$3239250051,exp$1402544433026922,cr$4154984551,pbid$25372728133,v$1.0))&al=(type${type},cmnt${cmnt},subo${subo})&r=10\", \"fdb_on\": \"1\", \"fdb_exp\": \"1402544433026\", \"fdb_intl\": \"en-us\" , \"d\" : \"1\" }", "slotData": {"pt":"0","bamt":"10000000000.000000","namt":"0.000000","isLiveAdPreview":"false","is_ad_feedback":"false","trusted_custom":"false","isCompAds":"false","pvid":"Okzx8DcyLjPvrchjZGr0dBYfMjAwMVbZyAURcDWk"}, "adc": "{\"label\":\"AdChoices\",\"url\":\"https:\\/\\/info.yahoo.com\\/privacy\\/us\\/yahoo\\/relevantads.html\",\"close\":\"Close\",\"closeAd\":\"Close Ad\",\"showAd\":\"Show ad\",\"collapse\":\"Collapse\",\"fdb\":\"I don't like this ad\",\"code\":\"en-us\"}", "is3rd": "0", "userProvidedData": {} } } },{ "html": "<!-- APT Vendor: Right Media, Format: Standard Graphical -->\n<SCR"+"IPT TYPE=\"text/javascr"+"ipt\" SRC=\"https://na.ads.yahoo.com/yax/banner?ve=1&tt=1&si=146750051&megamodal=${MEGAMODAL}&bucket=201&asz=300x250&u=https://www.yahoo.com&gdAdId=Z9mQvWKLDzw-&gdUuid=Okzx8DcyLjPvrchjZGr0dBYfMjAwMVbZyAURcDWk&gdSt=1457113093508360&publisher_blob=${RS}|Okzx8DcyLjPvrchjZGr0dBYfMjAwMVbZyAURcDWk|2023538075|LREC|1457113093.654780|2-9-7:ysd:1&pub_redirect=https://beap-bc.yahoo.com/yc/YnY9MS4wLjAmYnM9KDE3aTNqdGUzbihnaWQkT2t6eDhEY3lMalB2cmNoalpHcjBkQllmTWpBd01WYlp5QVVSY0RXayxzdCQxNDU3MTEzMDkzNTA4MzYwLHNpJDQ0NTIwNTEsc3AkMjAyMzUzODA3NSxjdCQyNSx5YngkZkl5NDg5b01EaENiT2lGell6VmhlQSxsbmckZW4tdXMsY3IkNDQ2NzA0OTA1MSx2JDIuMCxhaWQkWjltUXZXS0xEenctLGJpJDIyNjQxODQwNTEsbW1lJDk1NDk0MzI2NTI4NjAyMjMxMDksciQwLHlvbyQxLGFncCQzNDU5NDYwNTUxLGFwJExSRUMpKQ/2/*&K=1\"></SCR"+"IPT><scr"+"ipt>var url = \"\"; if(url && url.search(\"http\") != -1){document.write('<scr"+"ipt src=\"' + url + '\"><\\/scr"+"ipt>');}</scr"+"ipt><!--QYZ 2264184051,4467049051,;;LREC;2023538075;1-->", "id": "LREC", "meta": { "y": { "cscHTML": "<scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_d==null)window.xzq_d=new Object();\nwindow.xzq_d['Z9mQvWKLDzw-']='(as$13akg5a88,aid$Z9mQvWKLDzw-,bi$2264184051,agp$3459460551,cr$4467049051,ct$25,at$H,eob$gd1_match_id=-1:ypos=LREC)';\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136c1pdeu(gid$Okzx8DcyLjPvrchjZGr0dBYfMjAwMVbZyAURcDWk,st$1457113093508360,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$13akg5a88,aid$Z9mQvWKLDzw-,bi$2264184051,agp$3459460551,cr$4467049051,ct$25,at$H,eob$gd1_match_id=-1:ypos=LREC)\"></noscr"+"ipt>", "cscURI": "https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136c1pdeu(gid$Okzx8DcyLjPvrchjZGr0dBYfMjAwMVbZyAURcDWk,st$1457113093508360,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$13akg5a88,aid$Z9mQvWKLDzw-,bi$2264184051,agp$3459460551,cr$4467049051,ct$25,at$H,eob$gd1_match_id=-1:ypos=LREC)", "impID": "Z9mQvWKLDzw-", "supp_ugc": "0", "placementID": "3459460551", "creativeID": "4467049051", "serveTime": "1457113093508360", "behavior": "non_exp", "adID": "9549432652860223109", "matchID": "999999.999999.999999.999999", "err": "", "facStatus": {}, "hasExternal": 0, "size": "300x250", "bookID": "2264184051", "serveType": "-1", "slotID": "1", "fdb": "{ \"fdb_url\": \"https:\\\/\\\/beap-bc.yahoo.com\\\/af\\\/us?bv=1.0.0&bs=(1603u34ij(gid$Okzx8DcyLjPvrchjZGr0dBYfMjAwMVbZyAURcDWk,st$1457113093508360,srv$1,si$4452051,ct$25,exp$1457120293508360,adv$26513753608,li$3458215051,cr$4467049051,v$1.0,pbid$20459933223,seid$146750051))&al=(type${type},cmnt${cmnt},subo${subo})&r=10\", \"fdb_on\": \"1\", \"fdb_exp\": \"1457120293508\", \"fdb_intl\": \"en-US\" }", "slotData": {"pt":"8","bamt":"10000000000.000000","namt":"0.000000","isLiveAdPreview":"false","is_ad_feedback":"false","trusted_custom":"false","isCompAds":"false","adjf":"1.000000","alpha":"-1.000000","ffrac":"0.999965","pcpm":"-1.000000","fc":"false","sdate":"1442339887","edate":"1498881599","bimpr":399999991808.000000,"pimpr":0.000000,"spltp":0.000000,"frp":"false","pvid":"Okzx8DcyLjPvrchjZGr0dBYfMjAwMVbZyAURcDWk"}, "adc": "{\"label\":\"AdChoices\",\"url\":\"https:\\/\\/info.yahoo.com\\/privacy\\/us\\/yahoo\\/relevantads.html\",\"close\":\"Close\",\"closeAd\":\"Close Ad\",\"showAd\":\"Show ad\",\"collapse\":\"Collapse\",\"fdb\":\"I don't like this ad\",\"code\":\"en-us\"}", "is3rd": "1", "userProvidedData": {} } } },{ "html": "<!-- SpaceID=2023538075 loc=MAST noad --><!-- fac-gd2-noad --><!-- gd2-status-2 --><!--QYZ CMS_NONE_SELECTED,,;;MAST;2023538075;2-->", "id": "MAST", "meta": { "y": { "cscHTML": "<scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_d==null)window.xzq_d=new Object();\nwindow.xzq_d['3O6QvWKLDzw-']='(as$125ki9vvf,aid$3O6QvWKLDzw-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=MAST)';\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136c1pdeu(gid$Okzx8DcyLjPvrchjZGr0dBYfMjAwMVbZyAURcDWk,st$1457113093508360,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$125ki9vvf,aid$3O6QvWKLDzw-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=MAST)\"></noscr"+"ipt>", "cscURI": "https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136c1pdeu(gid$Okzx8DcyLjPvrchjZGr0dBYfMjAwMVbZyAURcDWk,st$1457113093508360,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$125ki9vvf,aid$3O6QvWKLDzw-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=MAST)", "impID": "", "supp_ugc": "0", "placementID": "-1", "creativeID": "-1", "serveTime": "1457113093508360", "behavior": "non_exp", "adID": "#2", "matchID": "#2", "err": "invalid_space", "facStatus": {}, "hasExternal": 0, "size": "", "bookID": "-1", "serveType": "-1", "slotID": "2", "fdb": "{ \"fdb_url\": \"http:\\/\\/beap-bc.yahoo.com\\/af?bv=1.0.0&bs=(15ir45r6b(gid$jmTVQDk4LjHHbFsHU5jMkgKkMTAuNwAAAACljpkK,st$1402537233026922,srv$1,si$13303551,adv$25941429036,ct$25,li$3239250051,exp$1402544433026922,cr$4154984551,pbid$25372728133,v$1.0))&al=(type${type},cmnt${cmnt},subo${subo})&r=10\", \"fdb_on\": \"1\", \"fdb_exp\": \"1402544433026\", \"fdb_intl\": \"en-us\" , \"d\" : \"1\" }", "slotData": {"pt":"0","bamt":"10000000000.000000","namt":"0.000000","isLiveAdPreview":"false","is_ad_feedback":"false","trusted_custom":"false","isCompAds":"false","pvid":"Okzx8DcyLjPvrchjZGr0dBYfMjAwMVbZyAURcDWk"}, "adc": "{\"label\":\"AdChoices\",\"url\":\"https:\\/\\/info.yahoo.com\\/privacy\\/us\\/yahoo\\/relevantads.html\",\"close\":\"Close\",\"closeAd\":\"Close Ad\",\"showAd\":\"Show ad\",\"collapse\":\"Collapse\",\"fdb\":\"I don't like this ad\",\"code\":\"en-us\"}", "is3rd": "0", "userProvidedData": {} } } },{ "html": "<!-- APT Vendor: Right Media, Format: Standard Graphical -->\n<style>\n	#fc_align{\n		position: static !important;\n		width: 120px !important;\n		margin: auto !important;\n	}\n</style>\n<SCR"+"IPT TYPE=\"text/javascr"+"ipt\" SRC=\"https://na.ads.yahoo.com/yax/banner?ve=1&tt=1&si=251157193&asz=120x60&u=https://www.yahoo.com&gdAdId=UQSRvWKLDzw-&gdUuid=Okzx8DcyLjPvrchjZGr0dBYfMjAwMVbZyAURcDWk&gdSt=1457113093508360&publisher_blob=${RS}|Okzx8DcyLjPvrchjZGr0dBYfMjAwMVbZyAURcDWk|2023538075|TXTL|1457113093.654919|2-9-7:ysd:1&pub_redirect=https://beap-bc.yahoo.com/yc/YnY9MS4wLjAmYnM9KDE3aXRmMTdqdChnaWQkT2t6eDhEY3lMalB2cmNoalpHcjBkQllmTWpBd01WYlp5QVVSY0RXayxzdCQxNDU3MTEzMDkzNTA4MzYwLHNpJDQ0NTIwNTEsc3AkMjAyMzUzODA3NSxjdCQyNSx5YngkZkl5NDg5b01EaENiT2lGell6VmhlQSxsbmckZW4tdXMsY3IkNDQ1MTAwOTA1MSx2JDIuMCxhaWQkVVFTUnZXS0xEenctLGJpJDIyNzUxODgwNTEsbW1lJDk1OTMzMjA3NzYxNzQzNDk0MDgsciQwLHlvbyQxLGFncCQzNDc2NTAyNTUxLGFwJFRYVEwpKQ/2/*&K=1\"></SCR"+"IPT><scr"+"ipt>var url = \"\"; if(url && url.search(\"http\") != -1){document.write('<scr"+"ipt src=\"' + url + '\"><\\/scr"+"ipt>');}</scr"+"ipt><!--QYZ 2275188051,4451009051,;;TXTL;2023538075;1-->", "id": "TXTL", "meta": { "y": { "cscHTML": "<scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_d==null)window.xzq_d=new Object();\nwindow.xzq_d['UQSRvWKLDzw-']='(as$13a72rhof,aid$UQSRvWKLDzw-,bi$2275188051,agp$3476502551,cr$4451009051,ct$25,at$H,eob$gd1_match_id=-1:ypos=TXTL)';\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136c1pdeu(gid$Okzx8DcyLjPvrchjZGr0dBYfMjAwMVbZyAURcDWk,st$1457113093508360,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$13a72rhof,aid$UQSRvWKLDzw-,bi$2275188051,agp$3476502551,cr$4451009051,ct$25,at$H,eob$gd1_match_id=-1:ypos=TXTL)\"></noscr"+"ipt>", "cscURI": "https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136c1pdeu(gid$Okzx8DcyLjPvrchjZGr0dBYfMjAwMVbZyAURcDWk,st$1457113093508360,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$13a72rhof,aid$UQSRvWKLDzw-,bi$2275188051,agp$3476502551,cr$4451009051,ct$25,at$H,eob$gd1_match_id=-1:ypos=TXTL)", "impID": "UQSRvWKLDzw-", "supp_ugc": "0", "placementID": "3476502551", "creativeID": "4451009051", "serveTime": "1457113093508360", "behavior": "non_exp", "adID": "9593320776174349408", "matchID": "999999.999999.999999.999999", "err": "", "facStatus": {}, "hasExternal": 0, "size": "120x45", "bookID": "2275188051", "serveType": "-1", "slotID": "3", "fdb": "{ \"fdb_url\": \"https:\\\/\\\/beap-bc.yahoo.com\\\/af\\\/us?bv=1.0.0&bs=(160nsit7l(gid$Okzx8DcyLjPvrchjZGr0dBYfMjAwMVbZyAURcDWk,st$1457113093508360,srv$1,si$4452051,ct$25,exp$1457120293508360,adv$26679945979,li$3474892551,cr$4451009051,v$1.0,pbid$20459933223,seid$146750051))&al=(type${type},cmnt${cmnt},subo${subo})&r=10\", \"fdb_on\": \"1\", \"fdb_exp\": \"1457120293508\", \"fdb_intl\": \"en-US\" }", "slotData": {"pt":"3","bamt":"10000000000.000000","namt":"0.000000","isLiveAdPreview":"false","is_ad_feedback":"false","trusted_custom":"false","isCompAds":"false","adjf":"1.000000","alpha":"1.000000","ffrac":"1.000000","pcpm":"-1.000000","fc":"false","ecpm":0.000000,"sdate":"1446744408","edate":"1514782799","bimpr":0.000000,"pimpr":-463598208.000000,"spltp":100.000000,"frp":"false","pvid":"Okzx8DcyLjPvrchjZGr0dBYfMjAwMVbZyAURcDWk"}, "adc": "{\"label\":\"AdChoices\",\"url\":\"https:\\/\\/info.yahoo.com\\/privacy\\/us\\/yahoo\\/relevantads.html\",\"close\":\"Close\",\"closeAd\":\"Close Ad\",\"showAd\":\"Show ad\",\"collapse\":\"Collapse\",\"fdb\":\"I don't like this ad\",\"code\":\"en-us\"}", "is3rd": "0", "userProvidedData": {} } } } ], "meta": { "y": { "pageEndHTML": "<scr"+"ipt language=javascr"+"ipt>\n(function(){window.xzq_p=function(R){M=R};window.xzq_svr=function(R){J=R};function F(S){var T=document;if(T.xzq_i==null){T.xzq_i=new Array();T.xzq_i.c=0}var R=T.xzq_i;R[++R.c]=new Image();R[R.c].src=S}window.xzq_sr=function(){var S=window;var Y=S.xzq_d;if(Y==null){return }if(J==null){return }var T=J+M;if(T.length>P){C();return }var X=\"\";var U=0;var W=Math.random();var V=(Y.hasOwnProperty!=null);var R;for(R in Y){if(typeof Y[R]==\"string\"){if(V&&!Y.hasOwnProperty(R)){continue}if(T.length+X.length+Y[R].length<=P){X+=Y[R]}else{if(T.length+Y[R].length>P){}else{U++;N(T,X,U,W);X=Y[R]}}}}if(U){U++}N(T,X,U,W);C()};function N(R,U,S,T){if(U.length>0){R+=\"&al=\"}F(R+U+\"&s=\"+S+\"&r=\"+T)}function C(){window.xzq_d=null;M=null;J=null}function K(R){xzq_sr()}function B(R){xzq_sr()}function L(U,V,W){if(W){var R=W.toString();var T=U;var Y=R.match(new RegExp(\"\\\\\\\\(([^\\\\\\\\)]*)\\\\\\\\)\"));Y=(Y[1].length>0?Y[1]:\"e\");T=T.replace(new RegExp(\"\\\\\\\\([^\\\\\\\\)]*\\\\\\\\)\",\"g\"),\"(\"+Y+\")\");if(R.indexOf(T)<0){var X=R.indexOf(\"{\");if(X>0){R=R.substring(X,R.length)}else{return W}R=R.replace(new RegExp(\"([^a-zA-Z0-9$_])this([^a-zA-Z0-9$_])\",\"g\"),\"$1xzq_this$2\");var Z=T+\";var rv = f( \"+Y+\",this);\";var S=\"{var a0 = '\"+Y+\"';var ofb = '\"+escape(R)+\"' ;var f = new Function( a0, 'xzq_this', unescape(ofb));\"+Z+\"return rv;}\";return new Function(Y,S)}else{return W}}return V}window.xzq_eh=function(){if(E||I){this.onload=L(\"xzq_onload(e)\",K,this.onload,0);if(E&&typeof (this.onbeforeunload)!=O){this.onbeforeunload=L(\"xzq_dobeforeunload(e)\",B,this.onbeforeunload,0)}}};window.xzq_s=function(){setTimeout(\"xzq_sr()\",1)};var J=null;var M=null;var Q=navigator.appName;var H=navigator.appVersion;var G=navigator.userAgent;var A=parseInt(H);var D=Q.indexOf(\"Microsoft\");var E=D!=-1&&A>=4;var I=(Q.indexOf(\"Netscape\")!=-1||Q.indexOf(\"Opera\")!=-1)&&A>=4;var O=\"undefined\";var P=2000})();\n</scr"+"ipt><scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_svr)xzq_svr('https://csc.beap.bc.yahoo.com/');\nif(window.xzq_p)xzq_p('yi?bv=1.0.0&bs=(136c1pdeu(gid$Okzx8DcyLjPvrchjZGr0dBYfMjAwMVbZyAURcDWk,st$1457113093508360,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3');\nif(window.xzq_s)xzq_s();\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136c1pdeu(gid$Okzx8DcyLjPvrchjZGr0dBYfMjAwMVbZyAURcDWk,st$1457113093508360,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3\"></noscr"+"ipt>", "pos_list": [ "FPAD","LREC","MAST","TXTL" ], "filtered": [  ], "spaceID": "2023538075", "host": "www.yahoo.com", "lookupTime": "232", "k2_uri": "", "fac_rt": "-1", "serveTime":"1457113093508360", "pvid": "Okzx8DcyLjPvrchjZGr0dBYfMjAwMVbZyAURcDWk", "tID": "darla_prefetch_1457113093507_855571425_1", "npv": "0", "ep": "{\"site-attribute\":\"fdn=1 Y-BUCKET=\\\"201\\\"\",\"secure\":true,\"lang\":\"en-US\",\"ref\":\"https:\\/\\/www.yahoo.com\",\"filter\":\"no_expandable;\",\"darlaID\":\"darla_instance_1457113093506_582793604_0\"}", "pe": "CWZ1bmN0aW9uIGRwZWQoKSB7IAoJaWYod2luZG93Lnh6cV9kPT1udWxsKXdpbmRvdy54enFfZD1uZXcgT2JqZWN0KCk7CndpbmRvdy54enFfZFsnOHNPUXZXS0xEenctJ109JyhhcyQxMjV0czRnMWYsYWlkJDhzT1F2V0tMRHp3LSxjciQtMSxjdCQyNSxhdCRILGVvYiRnZDFfbWF0Y2hfaWQ9LTE6eXBvcz1GUEFEKSc7CglpZih3aW5kb3cueHpxX2Q9PW51bGwpd2luZG93Lnh6cV9kPW5ldyBPYmplY3QoKTsKd2luZG93Lnh6cV9kWydaOW1RdldLTER6dy0nXT0nKGFzJDEzYWtnNWE4OCxhaWQkWjltUXZXS0xEenctLGJpJDIyNjQxODQwNTEsYWdwJDM0NTk0NjA1NTEsY3IkNDQ2NzA0OTA1MSxjdCQyNSxhdCRILGVvYiRnZDFfbWF0Y2hfaWQ9LTE6eXBvcz1MUkVDKSc7CglpZih3aW5kb3cueHpxX2Q9PW51bGwpd2luZG93Lnh6cV9kPW5ldyBPYmplY3QoKTsKd2luZG93Lnh6cV9kWyczTzZRdldLTER6dy0nXT0nKGFzJDEyNWtpOXZ2ZixhaWQkM082UXZXS0xEenctLGNyJC0xLGN0JDI1LGF0JEgsZW9iJGdkMV9tYXRjaF9pZD0tMTp5cG9zPU1BU1QpJztpZih3aW5kb3cueHpxX2Q9PW51bGwpd2luZG93Lnh6cV9kPW5ldyBPYmplY3QoKTsKd2luZG93Lnh6cV9kWydVUVNSdldLTER6dy0nXT0nKGFzJDEzYTcycmhvZixhaWQkVVFTUnZXS0xEenctLGJpJDIyNzUxODgwNTEsYWdwJDM0NzY1MDI1NTEsY3IkNDQ1MTAwOTA1MSxjdCQyNSxhdCRILGVvYiRnZDFfbWF0Y2hfaWQ9LTE6eXBvcz1UWFRMKSc7CgkJIH07CmRwZWQudHJhbnNJRCA9ICJkYXJsYV9wcmVmZXRjaF8xNDU3MTEzMDkzNTA3Xzg1NTU3MTQyNV8xIjsKCglmdW5jdGlvbiBkcGVyKCkgeyAKCQppZih3aW5kb3cueHpxX3N2cil4enFfc3ZyKCdodHRwczovL2NzYy5iZWFwLmJjLnlhaG9vLmNvbS8nKTsKaWYod2luZG93Lnh6cV9wKXh6cV9wKCd5aT9idj0xLjAuMCZicz0oMTM2YzFwZGV1KGdpZCRPa3p4OERjeUxqUHZyY2hqWkdyMGRCWWZNakF3TVZiWnlBVVJjRFdrLHN0JDE0NTcxMTMwOTM1MDgzNjAsc2kkNDQ1MjA1MSxzcCQyMDIzNTM4MDc1LHB2JDEsdiQyLjApKSZ0PUpfMy1EXzMnKTsKaWYod2luZG93Lnh6cV9zKXh6cV9zKCk7CgoKCQogfTsKZHBlci50cmFuc0lEID0iZGFybGFfcHJlZmV0Y2hfMTQ1NzExMzA5MzUwN184NTU1NzE0MjVfMSI7Cgo=", "pym": "" } } } </script>    <script type="text/javascript">
        var pageloadValidAds = ["TXTL","LREC"];
        var mastRotationEnabled = 0;
        var bucketSAEnabled = true;
        var w = window, D = w.DARLA,
            C = {"useYAC":0,"usePE":0,"servicePath":"https:\/\/www.yahoo.com\/sdarla\/php\/fc.php","xservicePath":"","beaconPath":"https:\/\/www.yahoo.com\/sdarla\/php\/b.php","renderPath":"","allowFiF":false,"srenderPath":"https:\/\/s.yimg.com\/rq\/darla\/2-9-7\/html\/r-sf.html","renderFile":"https:\/\/s.yimg.com\/rq\/darla\/2-9-7\/html\/r-sf.html","sfbrenderPath":"https:\/\/s.yimg.com\/rq\/darla\/2-9-7\/html\/r-sf.html","msgPath":"https:\/\/www.yahoo.com\/sdarla\/2-9-7\/html\/msg.html","cscPath":"https:\/\/s.yimg.com\/rq\/darla\/2-9-7\/html\/r-csc.html","root":"sdarla","edgeRoot":"http:\/\/l.yimg.com\/rq\/darla\/2-9-7","sedgeRoot":"https:\/\/s.yimg.com\/rq\/darla\/2-9-7","version":"2-9-7","tpbURI":"","hostFile":"https:\/\/s.yimg.com\/rq\/darla\/2-9-7\/js\/g-r-min.js","beaconsDisabled":true,"rotationTimingDisabled":true,"fdb_locale":"What don't you like about this ad?|It's offensive|Something else|Thank you for helping us improve your Yahoo experience|It's not relevant|It's distracting|I don't like this ad|Send|Done|Why do I see ads?|Learn more about your feedback.","positions":{"DEFAULT":{"supports":false},"FPAD":[],"LREC":{"w":300,"h":250},"MAST":[],"TXTL":{"w":120,"h":45}},"lang":"en-US"},
            _adPerfBeaconData,
            _pendingAds = [],
            _adLT = [];
        var safeframeOptinPositions = {"FPAD":true};
                window._navStart = function _navStart () {
            if (window.performance) {
                if (window.performance.clearMeasures) {
                    try {
                        window.performance.clearMeasures();
                    } catch (ex) {
                    }
                }
                if (window.performance.clearMarks) {
                    try {
                        window.performance.clearMarks();
                    } catch (ex) {
                    }
                }
                _perfMark('MODAL_OPEN');
            }
            window._pendingAds = [];
            window._perfBackfillAdBeacon = false;
            window._perfVideoInitBeacon = false;
            window._adPerfBeaconData = {positions:{}};
        };
        window._adCallStart = function _adCallStart () {
            window._perfMark('MODAL_AD_EVENT_START');
        };
        window._isModalOpen = function _isModalOpen () {
            return (document.getElementsByTagName('html')[0].className.match(/Reader-open/) && !document.getElementsByTagName('html')[0].className.match(/Reader-closed/));
        };
        window._perfMark = function _perfMark (name) {
            if (window.performance && window.performance.mark) {
                window.performance.mark(name);
            }
        };
        window._perfBackfillAd = function _perfBackfillAd() {
                for (var i in window._adPerfBeaconData.positions) {
                    if (window._adPerfBeaconData.positions.hasOwnProperty(i) && i.match(/LREC-/)) {
                        window._perfMark('PSTMSG_' + i);
                        window._adPerfBeaconData.positions[i].backfill = true;
                    } else {
                        window._adPerfBeaconData.positions[i].backfill = false;
                    }
                }
        };
        window._backfillVideoStart = function _backfillVideoStart() {
            window._perfMark('PSTMSG_VIDSTART');
        };
        window._perfModalFetchStart = function _perfModalFetchStart() {
            window._perfMark('MODAL_FETCH_START');
        };
        window._perfModalFetchEnd = function _perfModalFetchEnd() {
            window._perfMark('MODAL_FETCH_END');
        };
        window._perfModalVideoInit = function _perfModalVideoInit() {
            var modalStart,
                videoInit,
                rapidPerfData,
                rapidInstance;
            if (!window._perfVideoInitBeacon && window._isModalOpen() &&
                window.performance && window.performance.getEntriesByName) {

                window._perfVideoInitBeacon = true;
                window._perfMark('MODAL_VIDEO_INIT');
                modalStart = window.performance.getEntriesByName('MODAL_OPEN')[0].startTime;
                modalVideoInit = Math.round(window.performance.getEntriesByName('MODAL_VIDEO_INIT')[0].startTime - modalStart);

                rapidInstance = (YMedia.Af && YMedia.Af.rapid && YMedia.Af.rapid.tracker) ? YMedia.Af.rapid.tracker : null;
                rapidPerfData = {
                    perf_usertime: {
                        utm: [{
                            name: 'modal_video_playback_info',
                            site : window._adPerfBeaconData.property || 'fp',
                            lang: window.Af.context.lang,
                            region: window.Af.context.region,
                            device: window.Af.context.device,
                            rid: window.Af.context.rid,
                            bucket: '201',
                            modalVideoInit: modalVideoInit
                }]}};
                if (rapidInstance) {
                    rapidInstance.beaconPerformanceData(rapidPerfData);
                }
            }
        };
        window._backfillVideoPlaybackStart = function _backfillVideoPlaybackStart(isAd) {
            var modalStart,
                videoInit,
                rapidPerfData,
                rapidInstance;
            if (!window._perfBackfillAdBeacon && window._isModalOpen() &&
                window.performance && window.performance.getEntriesByName) {

                window._perfBackfillAdBeacon = true;
                window._perfMark('PSTMSG_PBSTART');
                modalStart = window.performance.getEntriesByName('MODAL_OPEN')[0].startTime;
                videoInit = Math.round(window.performance.getEntriesByName('PSTMSG_VIDSTART')[0].startTime - modalStart);
                playbackStart = Math.round(window.performance.getEntriesByName('PSTMSG_PBSTART')[0].startTime - modalStart);

                rapidInstance = (YMedia.Af && YMedia.Af.rapid && YMedia.Af.rapid.tracker) ? YMedia.Af.rapid.tracker : null;
                rapidPerfData = {
                    perf_usertime: {
                        utm: [{
                            name: 'brightroll_backfill_info',
                            site : window._adPerfBeaconData.property || 'fp',
                            lang: window.Af.context.lang,
                            region: window.Af.context.region,
                            device: window.Af.context.device,
                            rid: window.Af.context.rid,
                            bucket: '201',
                            videoInit: videoInit,
                            videoAd: isAd,
                            playbackStart: playbackStart
                }]}};
                if (rapidInstance) {
                    rapidInstance.beaconPerformanceData(rapidPerfData);
                }
            }
        };
        window._adRenderComplete = function _adRenderComplete() {
            if (window.performance && window.performance.getEntriesByName) {
                var modalStart = window.performance.getEntriesByName('MODAL_OPEN')[0];
                var modalFetchStart = window.performance.getEntriesByName('MODAL_FETCH_START');
                var modalFetchEnd = window.performance.getEntriesByName('MODAL_FETCH_END');
                var rapidCustomData = [];
                var darlaFetchPerf= {};
                var rapidInstance = (YMedia.Af && YMedia.Af.rapid && YMedia.Af.rapid.tracker) ? YMedia.Af.rapid.tracker : null;
                var modalStartTime = modalStart.startTime;

                darlaFetchPerf['name'] = 'darla_info';
                darlaFetchPerf['spaceid'] = window._adPerfBeaconData.spaceId;
                darlaFetchPerf['pvid'] = window._adPerfBeaconData.pvid;
                darlaFetchPerf['site'] = window._adPerfBeaconData.property || 'fp';
                darlaFetchPerf['lang'] = window.Af.context.lang;
                darlaFetchPerf['region'] =  window.Af.context.region;
                darlaFetchPerf['device'] =  window.Af.context.device;
                darlaFetchPerf['rid'] =  window.Af.context.rid;
                darlaFetchPerf['bucket'] = '201';

                if (modalFetchStart && modalFetchStart.length > 0) {
                    darlaFetchPerf['modalFetchStart'] =  Math.round(modalFetchStart[0].startTime -modalStartTime);
                    darlaFetchPerf['modalFetchEnd'] =  Math.round(modalFetchEnd[0].startTime -modalStartTime);
                }
                darlaFetchPerf['eventStart'] =  Math.round(window.performance.getEntriesByName('MODAL_AD_EVENT_START')[0].startTime -modalStartTime);
                darlaFetchPerf['wsStart'] =  Math.round(window.performance.getEntriesByName('MODAL_AD_WS_START')[0].startTime - modalStartTime);
                darlaFetchPerf['wsEnd'] =  Math.round(window.performance.getEntriesByName('MODAL_AD_WS_END')[0].startTime - modalStartTime);
                rapidCustomData.push(darlaFetchPerf);

                for (var i in window._adPerfBeaconData.positions) {
                    var darlaPositionPerf = {};
                    var posInfo = window._adPerfBeaconData.positions.hasOwnProperty(i) && window._adPerfBeaconData.positions[i];
                    if(posInfo) {
                        darlaPositionPerf['name'] = i + '_info';
                        darlaPositionPerf['valid'] = posInfo.valid ? 1 : 0;
                        if (posInfo.valid) {
                            darlaPositionPerf['renderStart'] =  Math.round(window.performance.getEntriesByName('ADSTART_' + i)[0].startTime - modalStartTime);
                            darlaPositionPerf['renderEnd'] =  Math.round(window.performance.getEntriesByName('ADEND_' + i)[0].startTime - modalStartTime);
                            darlaPositionPerf['bookId'] = posInfo.bookId;
                            darlaPositionPerf['creativeId'] = posInfo.creativeId;
                            darlaPositionPerf['placementId'] = posInfo.placementId;
                            darlaPositionPerf['size'] = posInfo.size;
                            darlaPositionPerf['backfill'] = posInfo.backfill ? 1 : 0;
                            if (posInfo.backfill) {
                                darlaPositionPerf['pstmsg'] = Math.round(window.performance.getEntriesByName('PSTMSG_' + i)[0].startTime - modalStartTime);
                            }
                        }
                        rapidCustomData.push(darlaPositionPerf);
                    }
                }

                if (rapidInstance) {
                    var customPerfData = {'utm': rapidCustomData};
                    var rapidPerfData = {perf_usertime: customPerfData};
                    rapidInstance.beaconPerformanceData(rapidPerfData);
                }
            }
        };
        if (D && C) {
            C.positions = {"FPAD":{"clean":"my-adsFPAD","dest":"my-adsFPAD-iframe","metaSize":1,"fdb":"true,","supports":{"resize-to":1,"exp-ovr":1,"exp-push":1,"lyr":1}},"LREC":{"pos":"LREC","clean":"my-adsLREC","dest":"my-adsLREC-iframe","metaSize":true,"w":300,"h":250,"fdb":true,"supports":{"exp-ovr":0,"lyr":0}},"MAST":{"pos":"MAST","clean":"my-adsMAST","dest":"my-adsMAST-iframe","fr":"expIfr_exp","rmxp":0,"metaSize":true,"w":970,"h":250,"supports":{"exp-ovr":1,"exp-push":1,"resize-to":1},"closeBtn":{"mode":2,"useShow":1,"adc":0}},"TXTL":{"pos":"TXTL","id":"TXTL","clean":"my-adsTXTL","dest":"my-adsTXTL-iframe","w":120,"h":170,"fr":"expIfr_exp","rmxp":0,"css":".ad-tl2b {overflow:hidden; text-align:left;} p {margin:0px;} a {color:#020e65;} a:hover {color:#0078ff;} .y-fp-pg-controls {margin-top:5px; margin-bottom:5px;} #tl1_slug {font-family:'Helvetica Neue',Helvetica,Arial,sans-serif; font-size:11px; color:#999;} #fc_align a {font-family:'Helvetica Neue',Helvetica,Arial,sans-serif; font-size:11px;} a:link {text-decoration:none;}"}};
            C.k2={"res":{"rate":100,"pos":["LREC","MAST","FPAD","LREC2","LREC3","LREC-0","LREC2-0","LREC3-0","MAST-0","LDRB-0","SPL2-0","SPL-0","MON-0"]}};
            C.k2E2ERate=100;
C.k2Rate=100;
C.so=1;

            C.events = {"DEFAULT":{"clw":{"LREC-0":{"blocked_by":"MON-0"},"MON-0":{"blocked_by":"LREC-0"},"MAST-0":{"blocked_by":"LDRB-0,SPL-0"},"SPL-0":{"blocked_by":"LDRB-0,MAST-0"},"LDRB-0":{"blocked_by":"MAST-0,SPL-0"}}}};
            
                    C.onStartRequest = function(action_event_name) {
            if (window._isModalOpen && window._isModalOpen()) {
                window._perfMark('MODAL_AD_WS_START');
            }
        };
                    C.onFinishRequest = function(action_event_name) {
            if (window._isModalOpen && window._isModalOpen()) {
                window._perfMark('MODAL_AD_WS_END');
            }
        };
                        C.onFinishParse = function(eventName, result) {
                var ps = result.ps(),
                    modalOpen = false,
                    position, posItem, curAd, curEvt,
                    validPositions = {},
                    isMONFetch = false;
                            if (window._isModalOpen && window._isModalOpen()) {
                window._perfMark('AD_PARSE');
                modalOpen = true;
                window._adPerfBeaconData.spaceId = result.spaceID;
                window._adPerfBeaconData.pvid = result.pvid;
                curEvt = DARLA.evtSettings(eventName);
                if (curEvt.property) {
                    window._adPerfBeaconData.property = curEvt.property;
                }
            }
                        function fireBeacon(position, size, pageMode) {
            if (position && size && pageMode) {
                try {
                    var img = new Image();
                    img.src = '/p.gif?beaconType=darla&pos=' + position + '&size=' + size + '&mode=' +pageMode + '&bucket=' + 201 + '&rid=' + "4qbuvt9bdji05";
                } catch (e) {
                    if (console) {
                        console.log('failed to send darla beacon :', e);
                    }
                }
            }
        }

                if (ps && ps.length) {
                            if (eventName === 'hlAdsAll' || eventName === 'hlAdsCustom') {
            for (i = 0, l = ps.length; i < l; i++) {
                position = ps[i];
                posItem = result.item(position);
                if (posItem.hasErr || posItem.size + '' === '1x1' || (posItem.meta && posItem.meta.y && posItem.meta.y.size && posItem.meta.y.size + ''=== '1x1')) {
                    validPositions[position] = false;
                    if (position.indexOf("MON") > -1) {
                        isMONFetch = false;
                    }
                } else {
                    if (position.indexOf("MON") > -1) {
                        isMONFetch = true;
                    }
                    validPositions[position] = true;
                }
            }
            if (YMedia && YMedia.Af && YMedia.Af.Event && YMedia.Af.Event.fire) {
                YMedia.Af.Event.fire('sidekickrefresh', validPositions);
            }
        }

                    for (i = 0, l = ps.length; i < l; i++) {
                        position = ps[i];
                        posItem = result.item(position);
                                    if (modalOpen) {
                if (posItem.err || posItem.hasErr) {
                    window._adPerfBeaconData.positions[position] = {error: true};
                } else if (posItem.size + '' === '1x1' || (posItem.meta && posItem.meta.y && posItem.meta.y.size && posItem.meta.y.size + ''=== '1x1')) {
                    window._adPerfBeaconData.positions[position] = {size: '1x1'};
                } else {
                    window._adPerfBeaconData.positions[position] = {valid: true};
                    if (posItem.meta && posItem.meta.y) {
                        window._adPerfBeaconData.positions[position].size = posItem.meta.y.size;
                        window._adPerfBeaconData.positions[position].bookId = posItem.meta.y.bookID;
                        window._adPerfBeaconData.positions[position].creativeId = posItem.meta.y.creativeID;
                        window._adPerfBeaconData.positions[position].placementId = posItem.meta.y.placementID;
                    }
                    window._pendingAds.push(position);
                }
            }
                        if (posItem && posItem.conf && posItem.conf.clean) {
                            curAd = document.getElementById(posItem.conf.clean);
                            if (curAd) {
                                            if (eventName === 'hlAdsAll' || eventName === 'hlAdsCustom') {
                var posName = position.split('-')[0];
                if ((posName === "LDRB" || posName === "MAST" || posName === "SPL" || posName === "SPL2") &&
                   (!posItem.hasErr && posItem.size + '' !== '1x1')) {
                    curAd.className = curAd.className.replace('D-n', 'D-ib');
                    if (posName !== "SPL" && posName !== "SPL2") {
                        curAd.className = curAd.className + " Bdb-Grey-1";
                    }
                }
            }
            if (eventName === 'hlAdsAll' || eventName === 'hlAdsCustom') {
                if (position.match(/^SPL/)) {
                    if (posItem.hasErr || posItem.size + '' === '1x1') {
                        if (YMedia && YMedia.one('#' + posItem.conf.clean + '-wrap')) {
                            YMedia.one(curAd).setStyle('padding-top', '0px');
                        }
                    } else if (YMedia && YMedia.one('#' + posItem.conf.clean + '-wrap')) {
                        var splashWrap = YMedia.one('#' + posItem.conf.clean + '-wrap');
                        splashWrap.setStyle('padding-top', (5/12*100)+'%');
                        splashWrap.setStyle('width', '100%');
                        splashWrap.setStyle('height', '0px');
                        splashWrap.addClass('Mt-20 Mb-50');
                    }
                }
            }
            if ((eventName === 'hlAdsAll' || eventName === 'hlAdsCustom') && position.indexOf("LREC") > -1) {
              if (posItem && posItem.conf && posItem.conf.clean) {
                var fallbackDiv = document.getElementById(posItem.conf.clean + "-fallback");
                }
                if ((posItem.hasErr || posItem.size + '' === '1x1' || (posItem.meta && posItem.meta.y && posItem.meta.y.size && posItem.meta.y.size+'' === '1x1'))  && (!isMONFetch || position !== 'LREC-0')) {
                    var lrecBackfillString = "<a href=\"https:\/\/bs.serving-sys.com\/BurstingPipe\/adServer.bs?cn=tf&c=20&mc=click&pli=16563095&PluID=0&ord=${CACHEBUSTER}\" style=\"background:url(https:\/\/www.yahoo.com\/sy\/nn\/lib\/metro\/300_250_Human_Touch_mail.jpg) 0 0 no-repeat;height:250px;width:300px;display:block;margin:auto;\"><\/a><img width=\"1\" height=\"1\" alt=\"\" src=\"https:\/\/bs.serving-sys.com\/BurstingPipe\/adServer.bs?cn=tf&c=19&mc=imp&pli=16563095&PluID=0&ord=${CACHEBUSTER}&rtu=-1\" style=\"display:none\">";
                    if (fallbackDiv) {
                       fallbackDiv.innerHTML = lrecBackfillString;
                       fallbackDiv.className = fallbackDiv.className.replace('D-n', '');
                    }
                    curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-before-loading/, '');
                    curAd.className = curAd.className.replace(/hl-ad-LREC/, '');
                } else {
                  if (fallbackDiv && fallbackDiv.className.indexOf("D-n") < 0) {
                      fallbackDiv.className += ' D-n';
                    }
                    if (isMONFetch && position === 'LREC-0') {
                        curAd.parentElement.style.height = '600px';
                        var curAd = document.getElementById(posItem.conf.clean);
                        if (curAd.className.indexOf("D-n") < 0  && curAd.className.indexOf("D-ib") >= 0) {
                          curAd.className = curAd.className.replace(/D-ib/, 'D-n');
                        }
                    }
                }
            }
            if (eventName === 'prefetch' && position.indexOf("LREC") > -1) {
                curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-bgcolor/, '');
            }
            if ((eventName === 'fetch_selective_ad_lrec2' && position === 'LREC2') || (eventName === 'fetch_selective_ad_lrec3' && position === 'LREC3')) {
                var fallbackDiv = document.getElementById("my-adsLREC2-fallback");
                if (posItem.hasErr || (posItem.meta && posItem.meta.y && posItem.meta.y.size && posItem.meta.y.size+'' === '1x1')) {
                    curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-bgcolor/, '');
                    curAd.className = curAd.className.replace('Ht-250', '');
                    // hide div if ad fetch failed or is 1x1
                    if (curAd.className.indexOf("D-n") < 0) {
                        curAd.className += ' D-n';
                    }
                    fallbackDiv.className = fallbackDiv.className.replace('D-n', '');
                } else {
                    if (fallbackDiv.className.indexOf("D-n") < 0) {
                        fallbackDiv.className += ' D-n';
                    }
                    curAd.className = curAd.className.replace('D-n', '');
                }

                if (position === 'LREC3') {
                    var lrec2Div = document.getElementById("my-adsLREC2");
                    if (lrec2Div && lrec2Div.className.indexOf("D-n") < 0) {
                        lrec2Div.className += ' D-n';
                    }
                }
            }


                            }
                        }
                    }
                }
                            if (modalOpen && window._pendingAds.length === 0) {
                window._adRenderComplete();
            }
            };

                        C.onStartPosRender = function(posItem) {
                if (window.performance  && window.performance.now) {
                    var ltime = window.performance.now(),
                        posId = posItem && posItem.pos;
                    _adLT.push(['ADSTART_'+posId, Math.round(ltime)]);
                }
                if (window._isModalOpen && window._isModalOpen()) {
                    window._perfMark('ADSTART_' + posId);
                }
            };

                        C.onBeforeStartPosRender = function(posItem) {
                if (posItem && safeframeOptinPositions && safeframeOptinPositions[posItem.pos]) {
                    if (posItem.html && posItem.html.match(/<!--[^>]*sfoptout[^>]*-->/)) {
                        return true;
                    }
                }
            };

                        C.onFinishPosRender = function(posId, reqList, posItem) {
                var curAd = document.getElementById("my-ads"+posId),
                    adIndex,
                    posSize = (posItem && posItem.meta && posItem.meta.value("size", "y"));

                // Get clean div for ad position in case defined
                if (posItem && posItem.conf && posItem.conf.clean) {
                    curAd = document.getElementById(posItem.conf.clean);
                }
                if (curAd) {
                    // Let ad take its original size, remove default height given to ad div
                    curAd.className = curAd.className.replace('Ht-250', '');
                    curAd.className = curAd.className.replace('Ht-MAST', '');

                    if(posSize && posSize != "1x1") {
                        curAd.className = curAd.className.replace('D-n', '');
                        curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-bgcolor/, '');
                        curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-before-loading/, '');
                    }
                }

                if (window.performance !== undefined && window.performance.now !== undefined) {
                    var whiteListedAds = {"LREC":"my-adsLREC-base","MAST":"my-adsMAST","TL1":"my-adsTL1","TXTL":"my-adsTXTL","LREC-0":"hl-ad-LREC-0","MON-0":"hl-ad-MON-0","MAST-0":"hl-ad-MAST-0","LDRB-0":"hl-ad-LDRB-0","SPL2-0":"hl-ad-SPL2-0","SPL-0":"hl-ad-SPL-0"},
                      ltime = window.performance.now();
                     _adLT.push(['ADEND_'+posId, Math.round(ltime)]);
                    setTimeout(function () {
                        if (window.YAFT !== undefined && window.YAFT.isInitialized() && whiteListedAds[posId]) {
                            // Trigger custom timing for LREC ad position
                            window.YAFT.triggerCustomTiming(whiteListedAds[posId], '', ltime);
                        }
                    },300);
                }
                if (window._isModalOpen && window._isModalOpen()) {
                    window._perfMark('ADEND_' + posId);
                    adIndex = window._pendingAds.indexOf(posId);
                    if (adIndex >= 0) {
                        window._pendingAds.splice(adIndex, 1);
                        if (window._pendingAds.length === 0) {
                            window._adRenderComplete();
                        }
                    }
                }
            };

            C.onBeforePosMsg = function(msg_name, position) {
        // Make these configurable for INTLS
        var maxWidth = 970,
            maxHeight = 600,
            newWidth,
            newHeight,
            origWidth,
            origHeight,
            pos;

        if('MAST' !== position) {
            return;
        }

        if (msg_name === 'resize-to') {
            newWidth = arguments[2];
            newHeight = arguments[3];
        }
        else if (msg_name === 'exp-push' || msg_name === 'exp-ovr') {
            pos = $sf.host.get('MAST'),
            origWidth = pos.conf.w;
            origHeight = pos.conf.h;
            //"exp-ovr" or "exp-push", position id, delta X, delta Y, push (true /false), top increase, left increase, right increase, bottom increase
            newWidth = origWidth + arguments[6] + arguments[7];
            newHeight = origHeight + arguments[5] + arguments[8];
        }
        if(newWidth > maxWidth || newHeight > maxHeight) {
            return true;
        }
    };
                        //call back when the ad is expanded or collapsed
            C.onPosMsg = function (msg_name, data, msg_data)  {
                var visible;
                if(msg_name == "collapse" && data == "MAST") {
                    var bodyTag = document.getElementsByTagName("body")[0];
                    bodyTag.className = bodyTag.className.replace('mastAdExpanded', '');
                    bodyTag.className += " " +  "mastAdCollapsed";
                }
                if(msg_name == "exp-push" && data == "MAST") {
                    var bodyTag = document.getElementsByTagName("body")[0];
                    bodyTag.className = bodyTag.className.replace('mastAdCollapsed', '');
                    bodyTag.className += " " +  "mastAdExpanded";
                }

                /* generic ad expansion logic */
                if(msg_name == "collapse") {
                    var bodyTag = document.getElementsByTagName("body")[0];
                    bodyTag.className = bodyTag.className.replace(data + "-ad-expanded", '');
                }

                if(msg_name == "exp-ovr") {
                    var bodyTag = document.getElementsByTagName("body")[0];
                    bodyTag.className += " " + data + "-ad-expanded";
                }

                if (msg_name === 'geom-update') {
                    visible = D.render.RenderMgr.get(data).viewedAt > 0;
                    // geom-update event will always be available when Y is available
                    if (YMedia && visible) {
                        YMedia.Global.fire('ads:beacon', {id: data});
                    }
                }
                                if (msg_name === 'cmsg') {
                    var splashConf = DARLA.posSettings(data)
                    if (YMedia && splashConf && splashConf.clean) {
                        var splashNode = document.getElementById(splashConf.clean + '-wrap');
                        if (splashNode) {
                            if (msg_data === 'splash-expand') {
                                YMedia.one(splashNode).setStyle('padding-top', (9/16*100)+'%');
                                if (typeof splashNode.scrollIntoView === 'function') {
                                    splashNode.scrollIntoView();
                                }
                            } else if (msg_data === 'splash-collapse') {
                                YMedia.one(splashNode).setStyle('padding-top', (5/12*100)+'%');
                            }
                        }
                    }
                }
            };

            

            


            if ("OK" == D.config(C)) {
                setTimeout(function() {
                    if (window.performance  && window.performance.now) {
                        var ltime = window.performance.now();
                        _adLT.push(['DARLA_RSTART', Math.round(ltime)]);
                    }
                    var w = window,
                        d = document,
                        e = d.documentElement,
                        g = d.getElementsByTagName('body')[0],
                        winWidth = w.innerWidth || e.clientWidth || g.clientWidth;
                    if (true && winWidth < 1024 && window.pageloadValidAds && (window.pageloadValidAds.indexOf('TXTL') >= 0)) {
                        var prefetched = D.prefetched();
                        var txtlindex = prefetched.indexOf('TXTL');
                        if (txtlindex >= 0) {
                            delete(prefetched[txtlindex]);
                            for (var i=0; i < prefetched.length; i++) {
                                if (prefetched.hasOwnProperty(i)) {
                                    D.render(prefetched[i]);
                                }
                            }
                        } else {
                            D.render();
                        }
                    } else {
                        D.render();
                    }
                    if (window.performance  && window.performance.now) {
                        var ltime = window.performance.now();
                        _adLT.push(['DARLA_REND', Math.round(ltime)]);
                    }
                }, 2);
            }
        }
    </script>
                </div>
        
        

        
        
        <input type="hidden" id="afhistorystate">
    <!-- bottom -->
    
                    <script type="text/javascript" src="/sy/zz/combo?yui:/3.18.0/yui/yui-min.js&/ss/rapid-3.29.1.js&/os/mit/td/aperollup-min-163d3e68_desktop_advance.js"></script>
                    <script type="text/javascript" src="/sy/zz/combo?&&/os/mit/td/td-applet-stream-atomic-2.0.457/r-min.js&/os/mit/td/td-applet-mega-header-1.0.196/r-min.js&/os/mit/td/td-applet-viewer-0.1.2128-b.hotfix.j-18/r-min.js&/os/mit/td/td-applet-navlinks-atomic-0.0.57/r-min.js" async defer></script>                                <script type="text/javascript">                YUI().use('node-base', function(Y){
                    Y.on("domready", function(e) {
                        YUI.Env.core.push.apply(YUI.Env.core,["loader-angus","loader-ape-af","loader-ape-applet","loader-ape-location","loader-ape-pipe","loader-ape-social","loader-applet-server","loader-assembler","loader-dust-helpers","loader-finance-streamer","loader-highlander-client","loader-live-event-data","loader-mjata","loader-stencil","loader-td-api","loader-td-dev-info","loader-td-lib-entertainment","loader-td-lib-hovercards","loader-td-lib-livevideo","loader-td-lib-social","loader-td-applet-stream-atomic","loader-td-applet-mega-header","loader-td-applet-viewer","loader-td-applet-navlinks-atomic","loader-td-applet-trending-atomic","loader-td-applet-sidekick","loader-td-applet-related-content","loader-td-applet-mega-comments","loader-td-applet-weather-atomic"]);
YUI.add("loader-ape-af",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-beacon":{group:"ape-af",requires:["json-stringify","querystring-stringify-simple","io-base"]},"af-bootstrap":{group:"ape-af",requires:["af-utils"]},"af-cache":{group:"ape-af",requires:["af-beacon","json","gallery-storage-lite"]},"af-compositeview":{group:"ape-af",requires:["parallel","af-utils"]},"af-comscore":{group:"ape-af",requires:["af-beacon","af-config","querystring-stringify-simple"]},"af-config":{group:"ape-af",requires:["af-utils","cookie"]},"af-content":{group:"ape-af",requires:["af-config","af-dom","af-utils","json-parse","mjata-rest-http","node-pluginhost","querystring-stringify"]},"af-cookie":{group:"ape-af",requires:["cookie","json"]},"af-dom":{group:"ape-af",requires:["node-base","node-core"]},"af-dwelltime":{group:"ape-af",requires:["af-rapid","json-stringify"]},"af-eu-tracking":{group:"ape-af",requires:["af-beacon","media-agof-tracking"]},"af-event":{group:"ape-af",requires:["event-custom"]},"af-history":{group:"ape-af",requires:["json"]},"af-message":{group:"ape-af",langBundles:["strings"],requires:["ape-af-templates-message","af-utils","node-base","selector-css3"]},"af-pageviz":{group:"ape-af",requires:["event-custom"]},"af-poll":{group:"ape-af",requires:["af-pageviz"]},"af-rapid":{group:"ape-af",requires:["af-bootstrap","af-utils","media-rapid-tracking","node-core"]},"af-sync":{group:"ape-af",requires:["af-transport","af-utils"]},"af-trans":{group:"ape-af",requires:["node-base","transition","af-utils"]},"af-transport":{group:"ape-af",requires:["af-beacon","af-config","af-utils","json-stringify","tdapi-remotestore"]},"af-utils":{group:"ape-af",requires:["cookie","intl","oop","querystring-parse-simple"]},"af-viewport-loader":{group:"ape-af",requires:["node","event-base"]},"ape-af-lang-strings":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_ar":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_ar-jo":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_de-de":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_el-gr":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-ae":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-au":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-ca":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-gb":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-ie":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-in":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-jo":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-my":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-nz":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-ph":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-sg":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-us":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-za":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-ar":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-cl":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-co":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-es":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-mx":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-pe":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-us":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-ve":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_fr-be":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_fr-ca":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_fr-fr":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_id-id":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_it-it":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_nl-be":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_nl-nl":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_pt-br":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_ro-ro":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_sv-se":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_vi-vn":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_zh-hant-hk":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_zh-hant-tw":{group:"ape-af",requires:["intl"]},"ape-af-templates-message":{group:"ape-af",requires:["template-base","dust"]},"gallery-storage-lite":{group:"ape-af",requires:["event-base","event-custom","event-custom-complex","json","node-base"]},"media-agof-tracking":{group:"ape-af"},"media-rapid-tracking":{group:"ape-af",requires:["event-custom","base","node"]},"media-rmp":{group:"ape-af",requires:["node","io-base","async-queue","get"]},"tdapi-remotestore":{group:"ape-af",requires:["mjata-store","mjata-rest-http","json"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-ape-applet",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-applet":{group:"ape-applet",requires:["af-applet-dom","af-applet-model","af-applet-headerview","af-bootstrap","af-compositeview","af-config","af-utils","array-extras","event-custom","node","stencil-tooltip"]},"af-applet-action":{group:"ape-applet",requires:["af-applet-dom","af-utils","node-event-delegate","node-base"]},"af-applet-dom":{group:"ape-applet",requires:["node-core","af-trans"]},"af-applet-editview":{group:"ape-applet",requires:["af-applet-view","ape-applet-templates-moreinfo","ape-applet-templates-remove"]},"af-applet-headerview":{group:"ape-applet",requires:["af-applet-view","stencil-selectbox"]},"af-applet-init":{group:"ape-applet",requires:["af-applet","af-beacon","af-bootstrap","af-config","af-utils","event-touch","oop"]},"af-applet-load":{group:"ape-applet",requires:["af-applet-dom","af-message","af-transport","ape-applet-templates-reload"]},"af-applet-mgr":{group:"ape-applet",requires:["af-applet-dom","af-beacon","af-content","af-transport","af-utils"]},"af-applet-model":{group:"ape-applet",langBundles:["strings"],requires:["af-bootstrap","af-config","af-sync","base-build","mjata-json","model"]},"af-applet-savesettings":{group:"ape-applet",requires:["af-dom","af-message"]},"af-applet-settingsview":{group:"ape-applet",requires:["af-applet-view","ape-applet-templates-settingswrap"]},"af-applet-view":{group:"ape-applet",requires:["af-bootstrap","af-utils","base-build","dust","view"]},"af-applet-viewmgr":{group:"ape-applet",requires:["af-applet-dom"]},"af-applets":{group:"ape-applet",requires:["af-applet-action","af-applet-init","af-applet-load","af-applet-mgr","af-applet-savesettings","af-applet-viewmgr","af-dom","af-utils"]},"ape-applet-lang-strings":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_ar":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_ar-jo":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_de-de":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_el-gr":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-ae":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-au":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-ca":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-gb":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-ie":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-in":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-jo":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-my":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-nz":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-ph":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-sg":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-us":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-za":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-ar":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-cl":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-co":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-es":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-mx":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-pe":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-us":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-ve":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_fr-be":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_fr-ca":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_fr-fr":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_id-id":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_it-it":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_nl-be":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_nl-nl":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_pt-br":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_ro-ro":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_sv-se":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_vi-vn":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_zh-hant-hk":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_zh-hant-tw":{group:"ape-applet",requires:["intl"]},"ape-applet-templates-moreinfo":{group:"ape-applet",requires:["template-base","dust"]},"ape-applet-templates-reload":{group:"ape-applet",requires:["template-base","dust"]},"ape-applet-templates-remove":{group:"ape-applet",requires:["template-base","dust"]},"ape-applet-templates-settingswrap":{group:"ape-applet",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-ape-location",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-location-detection":{group:"ape-location",langBundles:["strings"],requires:["view","mjata-model-store","af-config","af-message","af-locations","af-utils","ape-location-templates-location-detection"]},"af-location-panel":{group:"ape-location",langBundles:["strings"],requires:["view","mjata-model-store","af-config","af-message","af-locations","af-utils","ape-location-templates-location-list","ape-location-templates-location-panel","stencil-toggle","stencil-tooltip"]},"af-locations":{group:"ape-location",requires:["af-sync","af-utils","mjata-model-base","mjata-model","mjata-lazy-modellist","mjata-model-store"]},"ape-location-lang-strings":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_ar":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_ar-jo":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_de-de":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_el-gr":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-ae":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-au":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-ca":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-gb":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-ie":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-in":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-jo":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-my":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-nz":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-ph":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-sg":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-us":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-za":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-ar":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-cl":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-co":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-es":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-mx":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-pe":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-us":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-ve":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_fr-be":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_fr-ca":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_fr-fr":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_id-id":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_it-it":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_nl-be":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_nl-nl":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_pt-br":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_ro-ro":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_sv-se":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_vi-vn":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_zh-hant-hk":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_zh-hant-tw":{group:"ape-location",requires:["intl"]},"ape-location-templates-location-detection":{group:"ape-location",requires:["template-base","dust"]},"ape-location-templates-location-list":{group:"ape-location",requires:["template-base","dust"]},"ape-location-templates-location-panel":{group:"ape-location",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-ape-pipe",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-comet":{group:"ape-pipe",requires:["af-beacon","af-config","comet","event-custom-base","json-parse"]},"af-pipe":{group:"ape-pipe",requires:["af-beacon","af-comet","af-config","af-utils","event-custom-base"]},comet:{group:"ape-pipe"}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-ape-social",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-social":{group:"ape-social",requires:[]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-dust-helpers",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{dust:{group:"dust-helpers",requires:["stencil-imageloader","moment","intl-helper"]},"dust-helper-intl":{es:!0,group:"dust-helpers",requires:["intl-messageformat"]},"intl-helper":{group:"dust-helpers",requires:["dust-helper-intl"]},"intl-messageformat":{es:!0,group:"dust-helpers"},moment:{group:"dust-helpers"}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-mjata",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"mjata-bind-model2dom":{group:"mjata",requires:["mjata-model-store","mjata-template","mjata-util","node-base","node-core"]},"mjata-binder":{group:"mjata",requires:["mjata-bind-model2dom"]},"mjata-json":{group:"mjata"},"mjata-lazy-modellist":{group:"mjata",requires:["lazy-model-list","mjata-model-store"]},"mjata-model":{group:"mjata",requires:["model","mjata-json","mjata-model-store"]},"mjata-model-base":{group:"mjata",requires:["base","mjata-model","mjata-modellist","mjata-model-store"]},"mjata-model-store":{group:"mjata",requires:["event-custom","promise"]},"mjata-modellist":{group:"mjata",requires:["model-list","mjata-model-store"]},"mjata-queue":{group:"mjata"},"mjata-rest-http":{group:"mjata",requires:["json-stringify","io-base","io-xdr"]},"mjata-store":{group:"mjata",requires:["mjata-queue","mjata-util"]},"mjata-template":{group:"mjata",requires:["dust"]},"mjata-util":{group:"mjata"}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-stencil",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{hammer:{group:"stencil"},stencil:{group:"stencil"},"stencil-base":{group:"stencil",requires:["stencil","yui-base","event-base","event-delegate","event-mouseenter","json-parse","dom-base","node-base","node-pluginhost","selector"]},"stencil-bquery":{group:"stencil",requires:["stencil-base","yui-base","dom-base","dom-style","node-base","node-pluginhost"]},"stencil-carousel":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","node-style","node-pluginhost","event-hover"]},"stencil-fx":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","pluginhost","event-custom","selector-css3"]},"stencil-fx-collapse":{group:"stencil",requires:["stencil-fx","node-style"]},"stencil-fx-fade":{group:"stencil",requires:["stencil-toggle","stencil-fx"]},"stencil-fx-flip":{group:"stencil",requires:["stencil-toggle","stencil-fx"]},"stencil-gallery":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","node-style","node-pluginhost","transition"]},"stencil-imageloader":{group:"stencil",requires:["stencil-base","yui-base","dom-base","dom-screen","node-base","node-screen","node-style","node-pluginhost","array-extras"]},"stencil-lightbox":{group:"stencil",requires:["stencil-base","yui-base","dom-base","dom-screen","node-base","node-pluginhost","shim-plugin"]},"stencil-scrollview":{group:"stencil",requires:["node-base","node-style","hammer"]},"stencil-selectbox":{group:"stencil",requires:["stencil-base"]},"stencil-slider":{group:"stencil",requires:["stencil"]},"stencil-source":{group:"stencil",requires:["node","oop","pluginhost","mjata-util"]},"stencil-source-af":{group:"stencil",requires:["node","stencil-source","mjata-model-store","mjata-bind-model2dom","af-applets"]},"stencil-source-dom":{group:"stencil",requires:["stencil-source"]},"stencil-source-rmp":{group:"stencil",requires:["stencil-source","stencil-source-url","media-rmp"]},"stencil-source-simple":{group:"stencil",requires:["stencil-source"]},"stencil-source-url":{group:"stencil",requires:["stencil-source","io-base","io-xdr"]},"stencil-sticker":{group:"stencil",requires:["node-base","node-style","node-screen"]},"stencil-tabpanel":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","node-pluginhost"]},"stencil-toggle":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","node-style","node-pluginhost","event-focus","event-mouseenter","json-parse"]},"stencil-tooltip":{group:"stencil",requires:["stencil-base","yui-base","dom-base","dom-screen","node-base","node-pluginhost","node-style","stencil-source"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-mega-comments",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-mega-comments-mainview":{group:"td-applet-mega-comments",requires:["af-applet-view","af-transport","af-utils","af-rapid","stencil-tooltip","td-applet-mega-comments-templates-styles"]},"td-applet-mega-comments-templates-activities":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-activity-comments"]},"td-applet-mega-comments-templates-activity-comments":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-avatar","td-applet-mega-comments-templates-comments-body","td-applet-mega-comments-templates-activity-replies"]},"td-applet-mega-comments-templates-activity-replies":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-avatar","td-applet-mega-comments-templates-replies-body"]},"td-applet-mega-comments-templates-avatar":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-comments":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-avatar","td-applet-mega-comments-templates-comments-body"]},"td-applet-mega-comments-templates-comments-body":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-comments-editor-tools"]},"td-applet-mega-comments-templates-comments-editor-tools":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-form-abuse":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-form-comment":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-form-delete":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-form-reply":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-main":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-form-comment","td-applet-mega-comments-templates-form-reply","td-applet-mega-comments-templates-form-abuse","td-applet-mega-comments-templates-form-delete"]},"td-applet-mega-comments-templates-main-activities":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-activities"]},"td-applet-mega-comments-templates-replies":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-avatar","td-applet-mega-comments-templates-replies-body"]},"td-applet-mega-comments-templates-replies-body":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-styles":{group:"td-applet-mega-comments",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-mega-header",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"mega-header-plugin-account-switch":{group:"td-applet-mega-header",requires:["node","event-custom","event-move","event-mouseenter","td-applet-mega-header-constants","get","anim","json-parse","escape"]},"mega-header-plugin-autocomplete":{group:"td-applet-mega-header",requires:["node","autocomplete","autocomplete-highlighters","autocomplete-list","td-applet-mega-header-constants"]},"mega-header-plugin-avatar":{group:"td-applet-mega-header",requires:["jsonp","td-applet-mega-header-constants"]},"mega-header-plugin-instant":{group:"td-applet-mega-header",requires:["node","event","af-event","json","jsonp","querystring","td-applet-mega-header-constants"]},"mega-header-plugin-mailpreview":{group:"td-applet-mega-header",requires:["jsonp","af-event","td-applet-mega-header-constants"]},"mega-header-plugin-notifications":{group:"td-applet-mega-header",requires:["af-event","af-cache","td-applet-mega-header-constants"]},"mega-header-plugin-username":{group:"td-applet-mega-header",requires:["jsonp"]},"td-applet-mega-header-constants":{group:"td-applet-mega-header"},"td-applet-mega-header-mainview":{group:"td-applet-mega-header",requires:["af-applet-view","node","td-applet-mega-header-constants","mega-header-plugin-autocomplete","mega-header-plugin-avatar","mega-header-plugin-username","mega-header-plugin-mailpreview","mega-header-plugin-notifications","mega-header-plugin-instant","mega-header-plugin-account-switch"]},"td-applet-mega-header-templates-accountSwitchPanel":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-autofocus_script":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-firefoxPromo_script":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-instant_filters":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-logo":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-mail":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-mailpreview":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-main":{group:"td-applet-mega-header",requires:["template-base","dust","td-applet-mega-header-templates-topbar","td-applet-mega-header-templates-logo","td-applet-mega-header-templates-profile","td-applet-mega-header-templates-notifications","td-applet-mega-header-templates-mail","td-applet-mega-header-templates-search","td-applet-mega-header-templates-instant_filters"]},"td-applet-mega-header-templates-notificationpanel":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-notifications":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-profile":{group:"td-applet-mega-header",requires:["template-base","dust","td-applet-mega-header-templates-accountSwitchPanel","td-applet-mega-header-templates-profilePanel"]},"td-applet-mega-header-templates-profilePanel":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-search":{group:"td-applet-mega-header",requires:["template-base","dust","td-applet-mega-header-templates-autofocus_script"]},"td-applet-mega-header-templates-topbar":{group:"td-applet-mega-header",requires:["template-base","dust","td-applet-mega-header-templates-firefoxPromo_script"]},"td-mega-header-model":{group:"td-applet-mega-header",requires:["model","af-sync"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-navlinks-atomic",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-navlinks-atomic-templates-main":{group:"td-applet-navlinks-atomic",requires:["template-base","dust"]},"td-applet-navlinks-mainview":{group:"td-applet-navlinks-atomic",requires:["af-applet-view"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-related-content",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-related-content-model":{group:"td-applet-related-content",requires:["model","af-event"]},"td-applet-related-content-templates-main":{group:"td-applet-related-content",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-sidekick",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-sidekick-templates-item":{group:"td-applet-sidekick",requires:["template-base","dust"]},"td-applet-sidekick-templates-main":{group:"td-applet-sidekick",requires:["template-base","dust","td-applet-sidekick-templates-item"]},"td-sidekick-mainview":{group:"td-applet-sidekick",requires:["af-applet-view","af-config","af-event","stencil"]},"td-sidekick-model":{group:"td-applet-sidekick",requires:["af-sync","base-build","model"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-stream-atomic",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"stream-actiondrawer-v2":{group:"td-applet-stream-atomic",requires:["af-applet-view","af-cache","af-event","af-utils","event-outside","stencil-fx","stencil-fx-collapse"]},"stream-needtoknow-anim":{group:"td-applet-stream-atomic",requires:["af-applet-view","af-pageviz"]},"stream-onboard-v2":{group:"td-applet-stream-atomic",requires:["af-applet-view","stencil-fx","stencil-fx-collapse"]},"td-applet-comments-model-v2":{group:"td-applet-stream-atomic",requires:["mjata-model-base","af-sync"]},"td-applet-interest-model-v2":{group:"td-applet-stream-atomic",requires:["mjata-model-base","af-sync"]},"td-applet-stream-appletmodel-v2":{group:"td-applet-stream-atomic",requires:["mjata-model-base","af-cookie","af-event","af-utils","af-applet-model","af-beacon","af-poll","td-applet-stream-model-v2","td-applet-stream-items-model-v2"]},"td-applet-stream-atomic-templates-breaking_news":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-debug":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-drawer_action":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-drawer_desktop":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-drawer_flyout":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-drawer_share":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-errormsg":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-ad":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-ad_dislike"]},"td-applet-stream-atomic-templates-item-ad_dislike":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-ad_dislike_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-ad_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-ad_dislike_v2"]},"td-applet-stream-atomic-templates-item-default":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-followable"]},"td-applet-stream-atomic-templates-item-default_clusters":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-followable"]},"td-applet-stream-atomic-templates-item-default_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-followable","td-applet-stream-atomic-templates-item-storyline_images","td-applet-stream-atomic-templates-item-storyline"]},"td-applet-stream-atomic-templates-item-featured_ad":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-ad_dislike"]},"td-applet-stream-atomic-templates-item-featured_ad_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-ad_dislike_v2"]},"td-applet-stream-atomic-templates-item-featured_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-storyline"]},"td-applet-stream-atomic-templates-item-filmstrip":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-followable":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-gs_tile":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon"]},"td-applet-stream-atomic-templates-item-inline_video":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon"]},"td-applet-stream-atomic-templates-item-inline_video_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-storyline_images","td-applet-stream-atomic-templates-item-storyline"]},"td-applet-stream-atomic-templates-item-needtoknow_actions":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-play_icon":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-right_menu":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-right_menu_featured"]},"td-applet-stream-atomic-templates-item-right_menu_featured":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon"]},"td-applet-stream-atomic-templates-item-storyline":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-storyline_images":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-storyline_upsell":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-items":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-debug"]},"td-applet-stream-atomic-templates-main":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-breaking_news","td-applet-stream-atomic-templates-errormsg"]},"td-applet-stream-atomic-templates-related":{group:"td-applet-stream-atomic",requires
:["template-base","dust"]},"td-applet-stream-atomic-templates-removeditem":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-baseview-v2":{group:"td-applet-stream-atomic",requires:["af-applet-view","node-scroll-info","af-beacon"]},"td-applet-stream-headerview-v2":{group:"td-applet-stream-atomic",requires:["af-applet-view"]},"td-applet-stream-items-model-v2":{group:"td-applet-stream-atomic",requires:["model","af-sync"]},"td-applet-stream-mainview-v2":{group:"td-applet-stream-atomic",requires:["af-beacon","af-cookie","af-cache","af-event","af-pipe","stencil-imageloader","td-applet-stream-baseview-v2"]},"td-applet-stream-model-v2":{group:"td-applet-stream-atomic",requires:["model","af-sync"]},"td-applet-stream-onboarding-model-v2":{group:"td-applet-stream-atomic",requires:["model","af-sync"]},"td-applet-stream-payoff-model-v2":{group:"td-applet-stream-atomic",requires:["model","af-sync"]},"td-applet-stream-related-model-atomic":{group:"td-applet-stream-atomic",requires:["model","af-sync"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-trending-atomic",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-trending-atomic-mainview":{group:"td-applet-trending-atomic",requires:["af-applet-view","node"]},"td-applet-trending-atomic-templates-main":{group:"td-applet-trending-atomic",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-viewer",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-ads-model":{group:"td-applet-viewer",requires:["af-sync","base-build","model","af-beacon"]},"td-applet-viewer-templates-ad_fdb_thank_you":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-ads_story":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-cards":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-debug","td-applet-viewer-templates-fallback","td-applet-viewer-templates-footer"]},"td-applet-viewer-templates-carousel":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-content_body":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-mag_slideshow","td-applet-viewer-templates-ads_story"]},"td-applet-viewer-templates-content_body_mega":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-mag_slideshow"]},"td-applet-viewer-templates-credit":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-debug":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-drawer_feedback":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-fallback":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-footer":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-header":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-share_btns_mega"]},"td-applet-viewer-templates-header_ads":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-lightbox":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-carousel"]},"td-applet-viewer-templates-lightbox_mega":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-carousel","td-applet-viewer-templates-thumbnail_items"]},"td-applet-viewer-templates-mag_slideshow":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-main":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-cards","td-applet-viewer-templates-lightbox"]},"td-applet-viewer-templates-modal_aside_mega":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-modal_lrecs_mega"]},"td-applet-viewer-templates-modal_lrecs_mega":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-reblog":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-share_btns":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-share_btns_mega":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-sidekicktv":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-slideshow":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-header","td-applet-viewer-templates-lightbox_mega","td-applet-viewer-templates-content_body_mega","td-applet-viewer-templates-modal_aside_mega","td-applet-viewer-templates-story_cover","td-applet-viewer-templates-content_body"]},"td-applet-viewer-templates-story":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-header","td-applet-viewer-templates-content_body_mega","td-applet-viewer-templates-modal_aside_mega","td-applet-viewer-templates-story_cover","td-applet-viewer-templates-content_body"]},"td-applet-viewer-templates-story_cover":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-header_ads","td-applet-viewer-templates-video","td-applet-viewer-templates-header"]},"td-applet-viewer-templates-thumbnail_items":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-video":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-header","td-applet-viewer-templates-content_body_mega","td-applet-viewer-templates-modal_aside_mega","td-applet-viewer-templates-header_ads","td-applet-viewer-templates-content_body"]},"td-viewer-ads":{group:"td-applet-viewer",requires:["af-beacon","af-config","af-event"]},"td-viewer-mainview":{group:"td-applet-viewer",requires:["af-applet-view","af-beacon","af-cache","af-config","af-event","stencil","angus-slider","event-tap","td-viewer-ads","td-viewer-slideshow"]},"td-viewer-model":{group:"td-applet-viewer",requires:["af-cache","af-sync","base-build","model","af-beacon"]},"td-viewer-slideshow":{group:"td-applet-viewer",requires:["stencil","angus-slider","event-tap"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-weather-atomic",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-weather-atomic-appletmodel":{group:"td-applet-weather-atomic",requires:["mjata-model-base","af-applet-model","mjata-model-store"]},"td-applet-weather-atomic-headerview":{group:"td-applet-weather-atomic",requires:["af-applet-view","mjata-model-store"]},"td-applet-weather-atomic-liteview":{group:"td-applet-weather-atomic",requires:["td-applet-weather-atomic-mainview"]},"td-applet-weather-atomic-mainview":{group:"td-applet-weather-atomic",requires:["af-applet-view","af-message","stencil-fx","stencil-fx-collapse"]},"td-applet-weather-atomic-model":{group:"td-applet-weather-atomic",requires:["model","af-sync"]},"td-applet-weather-atomic-templates-header":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-header.chiclet":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-header.error.chiclet":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-list-lite":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.chiclet":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.error.chiclet":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.error.lite":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.hovercard":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.lite":{group:"td-applet-weather-atomic",requires:["template-base","dust","td-applet-weather-atomic-templates-list-lite"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-dev-info",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-dev-info":{group:"td-dev-info",requires:["overlay","node-core","td-dev-info-templates-perf"]},"td-dev-info-templates-init":{group:"td-dev-info",requires:["template-base","dust"]},"td-dev-info-templates-perf":{group:"td-dev-info",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-lib-social",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"social-sharing-lib":{group:"td-lib-social",requires:["base","node-base","event-base","event-custom","stencil-lightbox","stencil-tooltip"]},"td-lib-social-templates-mtf":{group:"td-lib-social",requires:["template-base","dust","td-lib-social-templates-mtf_styles"]},"td-lib-social-templates-mtf_styles":{group:"td-lib-social",requires:["template-base","dust"]},"td-social-email-autocomplete":{group:"td-lib-social",requires:["autocomplete","autocomplete-highlighters","io","json","lang","node-base","node-core","yql"]},"td-social-mtf-popup":{group:"td-lib-social",requires:["base","event-base","io","autosuggest-standalone-loader","autosuggest-compose-utils","td-social-email-autocomplete","node-base","gallery-node-tokeninput"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.applyConfig({"groups":{"ape-af":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-af-0.0.326/","root":"os/mit/td/ape-af-0.0.326/"}}});
YUI.applyConfig({"groups":{"ape-applet":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-applet-0.0.207/","root":"os/mit/td/ape-applet-0.0.207/"}}});
YUI.applyConfig({"groups":{"ape-location":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-location-1.0.9/","root":"os/mit/td/ape-location-1.0.9/"}}});
YUI.applyConfig({"groups":{"ape-pipe":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-pipe-0.0.64/","root":"os/mit/td/ape-pipe-0.0.64/"}}});
YUI.applyConfig({"groups":{"ape-social":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-social-0.0.5/","root":"os/mit/td/ape-social-0.0.5/"}}});
YUI.applyConfig({"groups":{"dust-helpers":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/dust-helpers-0.0.144/","root":"os/mit/td/dust-helpers-0.0.144/"}}});
YUI.applyConfig({"groups":{"mjata":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/mjata-0.4.35/","root":"os/mit/td/mjata-0.4.35/"}}});
YUI.applyConfig({"groups":{"stencil":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/stencil-3.1.0/","root":"os/mit/td/stencil-3.1.0/"}}});
YUI.applyConfig({"groups":{"td-dev-info":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/td-dev-info-0.0.30/","root":"os/mit/td/td-dev-info-0.0.30/"}}});
YUI.applyConfig({"groups":{"td-lib-social":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/td-lib-social-0.1.207/","root":"os/mit/td/td-lib-social-0.1.207/"}}});
YUI.applyConfig({"modules":{"IntlPolyfill":{"fullpath":"https:\u002F\u002Fs.yimg.com\u002Fzz\u002Fcombo?yui:platform\u002Fintl\u002F0.1.4\u002FIntl.min.js&yui:platform\u002Fintl\u002F0.1.4\u002Flocale-data\u002Fjsonp\u002F{lang}.js","condition":{"name":"IntlPolyfill","trigger":"intl-messageformat","test":function (Y) {
                        return !Y.config.global.Intl;
                    },"when":"before"},"configFn":function (mod) {
                    var lang = 'en-US';
                    if (window.YUI_config && window.YUI_config.lang && window.IntlAvailableLangs && window.IntlAvailableLangs[window.YUI_config.lang]) {
                        lang = window.YUI_config.lang;
                    }
                    mod.fullpath = mod.fullpath.replace('{lang}', lang);
                    return true;
                }}}});
                    });
                });                </script>

<script type="text/javascript">
                YUI().use('node-base', function(Y){
                    Y.on("domready", function(e) {
                                        
                        (function(root) {
            root.YUI_config = root.YUI_config || {};
            root.YUI_config.lang = 'en-US';
        }(this));
            var YMedia = YUI({
                
                bootstrap: true,
                lang: 'en-US',
                comboBase: 'https://s.yimg.com/zz/combo?',
                comboSep: '&',
                root: 'yui:' + YUI.version + '/',
                filter: 'min',
                combine: true,
                maxURLLength: 2000,
                groups: {
                    arcade : {
                        base: 'https://s.yimg.com/nn/',
                        combine: true,
                        comboSep: '&',
                        comboBase: 'https://s.yimg.com/zz/combo?',
                        root: '',
                        modules: {
                                                'type_appscontainer_smartphone': {
                        'requires': ['node','node-event-delegate','anim','transition','event-move','stencil','stencil-scrollview'],
                        'path': '/nn/lib/metro/g/appscontainer/appscontainer_smartphone_0.0.18.js'
                    },
                    'type_customizedbutton': {
                        'requires': ['node','event'],
                        'path': '/nn/lib/metro/g/customizedbutton/customizedbutton_0.0.9.js'
                    },
                    'type_events_js': {
                        'requires': ['af-applet-contentmodel','af-applet-contentsettingsview'],
                        'path': '/nn/lib/metro/g/events/events_0.0.3.js'
                    },
                    'type_geminiads': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/geminiads/geminiads_0.0.4.js'
                    },
                    'type_myy': {
                        'requires': ['stencil-toggle','stencil','stencil-sticker','stencil-lightbox','stencil-bquery','af-applets','app','af-rapid','af-eu-tracking'],
                        'path': '/nn/lib/metro/g/myy/myy_0.0.41.js'
                    },
                    'type_myy_stub_rapid': {
                        'requires': ['stencil-toggle','stencil','stencil-sticker','stencil-lightbox','stencil-bquery','af-applets','app'],
                        'path': '/nn/lib/metro/g/myy/myy_stub_rapid_0.0.4.js'
                    },
                    'type_myy_mobile': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/myy_mobile_0.0.9.js'
                    },
                    'type_myy_viewer': {
                        'requires': ['highlander-client'],
                        'path': '/nn/lib/metro/g/myy/myy_viewer_0.0.10.js'
                    },
                    'type_advance': {
                        'requires': ['node'],
                        'path': '/nn/lib/metro/g/myy/advance_0.0.4.js'
                    },
                    'type_video_manager': {
                        'requires': ['af-content','af-event','base','event-synthetic','node-core','node-scroll-info'],
                        'path': '/nn/lib/metro/g/myy/video_manager_0.0.128.js'
                    },
                    'type_video_stage': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/video_stage_0.0.8.js'
                    },
                    'type_yahoodotcom_client': {
                        'requires': ['node'],
                        'path': '/nn/lib/metro/g/myy/yahoodotcom_client_0.0.15.js'
                    },
                    'type_myy_scroller': {
                        'requires': ['node'],
                        'path': '/nn/lib/metro/g/myy/myy_scroller_0.0.8.js'
                    },
                    'type_rapidworker_1_1': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/rapidworker_1_1_0.0.4.js'
                    },
                    'type_rapidworker_1_2': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/rapidworker_1_2_0.0.3.js'
                    },
                    'type_featurecue': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/featurecue_0.0.14.js'
                    },
                    'type_fp': {
                        'requires': ['stencil-toggle','stencil','stencil-sticker','stencil-lightbox','stencil-bquery','af-applets','app','af-rapid'],
                        'path': '/nn/lib/metro/g/myy/myy_0.0.41.js'
                    },
                    'type_addtomy': {
                        'requires': ['node','event','io','panel','io-base','transition','json-stringify'],
                        'path': '/nn/lib/metro/g/myy/addtomy_0.0.21.js'
                    },
                    'af-applet-basemodel': {
                        'requires': ['af-config','af-sync','af-utils','model'],
                        'path': '/nn/lib/metro/g/myy/af_applet_basemodel_0.0.3.js'
                    },
                    'af-applet-contentmodel': {
                        'requires': ['af-applet-basemodel'],
                        'path': '/nn/lib/metro/g/myy/af_applet_contentmodel_0.0.3.js'
                    },
                    'af-applet-baseview': {
                        'requires': ['af-dom','view'],
                        'path': '/nn/lib/metro/g/myy/af_applet_baseview_0.0.3.js'
                    },
                    'af-applet-contentview': {
                        'requires': ['af-applet-baseview','af-trans'],
                        'path': '/nn/lib/metro/g/myy/af_applet_contentview_0.0.3.js'
                    },
                    'af-applet-contentsettingsview': {
                        'requires': ['af-applet-contentview','af-utils','ape-applet-templates-settingswrap'],
                        'path': '/nn/lib/metro/g/myy/af_applet_contentsettingsview_0.0.3.js'
                    },
                    'af-applet-dd': {
                        'requires': ['af-applet-dom','af-message','event-custom-base'],
                        'path': '/nn/lib/metro/g/myy/af_applet_dd_0.0.4.js'
                    },
                    'type_abu': {
                        'requires': ['af-event'],
                        'path': '/nn/lib/metro/g/myy/abu_0.0.16.js'
                    },
                    'type_abu_event': {
                        'requires': ['event-synthetic','node-scroll-info'],
                        'path': '/nn/lib/metro/g/myy/abu_event_0.0.2.js'
                    },
                    'type_abu_video': {
                        'requires': ['af-content','af-event','base','event-custom','type_abu_event','node-core'],
                        'path': '/nn/lib/metro/g/myy/abu_video_0.0.7.js'
                    },
                    'type_abu_video_manager': {
                        'requires': ['af-content','af-event','base','event-custom','type_abu_event','type_abu_video','node-core'],
                        'path': '/nn/lib/metro/g/myy/abu_video_manager_0.0.20.js'
                    },
                    'type_advance_desktop': {
                        'requires': ['stencil','stencil-base','stencil-sticker','af-applets','base','af-rapid'],
                        'path': '/nn/lib/metro/g/myy/advance_desktop_0.0.8.js'
                    },
                    'type_advance_desktop_viewer': {
                        'requires': ['stencil','stencil-base','stencil-sticker','af-applets','base','af-rapid','highlander-client'],
                        'path': '/nn/lib/metro/g/myy/advance_desktop-viewer_0.0.3.js'
                    },
                    'type_app_declarations': {
                        'requires': ['af-cookie'],
                        'path': '/nn/lib/metro/g/myy/app_declarations_0.0.6.js'
                    },
                    'type_partner_att_enus_foreseealive': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/foresee/en-us/foresee-alive_0.0.3.js'
                    },
                    'type_partner_att_enus_foreseetrigger': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/foresee/en-us/foresee-trigger_0.0.3.js'
                    },
                    'pure_client_darla': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/pure_client_darla_0.0.2.js'
                    },
                    'type_idletimer': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/idletimer_0.0.1.js'
                    },
                    'type_myycontentdb': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myycontentdb/myycontentdb_0.0.54.js'
                    },
                    'type_uh_init': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myyheader/uh_init_0.0.13.js'
                    },
                    'type_myymail_mainview': {
                        'requires': ['af-applet-contentsettingsview','stencil-selectbox','stencil-toggle'],
                        'path': '/nn/lib/metro/g/myymail/myymail-mainview_0.0.23.js'
                    },
                    'type_myymail_appletmodel': {
                        'requires': ['mjata-model-base','af-applet-contentmodel'],
                        'path': '/nn/lib/metro/g/myymail/myymail-appletmodel_0.0.16.js'
                    },
                    'type_myyrss_js': {
                        'requires': ['af-applet-contentmodel','af-applet-contentsettingsview'],
                        'path': '/nn/lib/metro/g/myyrss/myyrss_0.0.13.js'
                    },
                    'type_nux': {
                        'requires': ['node-base','event-base','panel','io-base','json-stringify','af-transport'],
                        'path': '/nn/lib/metro/g/nux/nux_0.0.44.js'
                    },
                    'type_optin': {
                        'requires': ['node','event','io','panel','io-base'],
                        'path': '/nn/lib/metro/g/optin/optin_0.0.33.js'
                    },
                    'type_changelayout': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/pagenav/changelayout_0.0.40.js'
                    },
                    'type_changetheme': {
                        'requires': ['af-transport'],
                        'path': '/nn/lib/metro/g/pagenav/changetheme_0.0.50.js'
                    },
                    'type_addmodule': {
                        'requires': ['node','event','io','json-parse','json-stringify'],
                        'path': '/nn/lib/metro/g/pagenav/addmodule_0.0.16.js'
                    },
                    'type_addpage': {
                        'requires': ['af-transport'],
                        'path': '/nn/lib/metro/g/pagenav/addpage_0.0.27.js'
                    },
                    'type_pagenav': {
                        'requires': ['af-transport'],
                        'path': '/nn/lib/metro/g/pagenav/pagenav_0.0.35.js'
                    },
                    'type_reco': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/reco/reco_0.0.10.js'
                    },
                    'type_sda': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/sda/sda_0.0.32.js'
                    },
                    'type_sdarotate': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/sda/sdarotate_0.0.19.js'
                    },
                    'type_signoutcta': {
                        'requires': ['node','event'],
                        'path': '/nn/lib/metro/g/signoutcta/signoutcta_0.0.9.js'
                    },
                    'type_windowshade_js': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/windowshade/windowshade_0.0.4.js'
                    }
                        }
                    }
                }
            });

            if (!YMedia.config.patches || !YMedia.config.patches.length) {
                YMedia.config.patches = [
                    function patchLangBundlesRequires(Y, loader) {
                        var getRequires = loader.getRequires;
                        loader.getRequires = function (mod) {
                            var i, j, m, name, mods, loadDefaultBundle,
                                locales = Y.config.lang || [],
                                r = getRequires.apply(this, arguments);
                            // expanding requirements with optional requires
                            if (mod.langBundles && !mod.langBundlesExpanded) {
                                mod.langBundlesExpanded = [];
                                locales = typeof locales === 'string' ? [locales] : locales.concat();
                                for (i = 0; i < mod.langBundles.length; i += 1) {
                                    mods = [];
                                    loadDefaultBundle = false;
                                    name = mod.group + '-lang-' + mod.langBundles[i];
                                    for (j = 0; j < locales.length; j += 1) {
                                        m = this.getModule(name + '_' + locales[j].toLowerCase());
                                        if (m) {
                                            mods.push(m);
                                        } else {
                                            // if one of the requested locales is missing,
                                            // the default lang should be fetched
                                            loadDefaultBundle = true;
                                        }
                                    }
                                    if (!mods.length || loadDefaultBundle) {
                                        // falling back to the default lang bundle when needed
                                        m = this.getModule(name);
                                        if (m) {
                                            mods.push(m);
                                        }
                                    }
                                    // adding requirements for each lang bundle
                                    // (duplications are not a problem since they will be deduped)
                                    for (j = 0; j < mods.length; j += 1) {
                                        mod.langBundlesExpanded = mod.langBundlesExpanded.concat(this.getRequires(mods[j]), [mods[j].name]);
                                    }
                                }
                            }
                            return mod.langBundlesExpanded && mod.langBundlesExpanded.length ?
                                    [].concat(mod.langBundlesExpanded, r) : r;
                        };
                    }
            ];
        }
        for (var i = 0; i < YMedia.config.patches.length; i += 1) {YMedia.config.patches[i](YMedia, YMedia.Env._loader);}

        
                        YUI().use('node-base', function(Y) {
                    Y.Global.fire('ymediaReady', {e: YMedia});
                });

                    });
                });        YUI().use('node-base', function(Y) {
        Y.Global.on('ymediaReady', function(data) {
            YMedia = data.e;
    YMedia.applyConfig({"groups":{"td-applet-mega-header":{"base":"/sy/os/mit/td/td-applet-mega-header-1.0.196/","root":"os/mit/td/td-applet-mega-header-1.0.196/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_30345894"] = {"applet_type":"td-applet-mega-header","views":{"main":{"yui_module":"td-applet-mega-header-mainview","yui_class":"TD.Applet.MegaHeaderMainView","config":{"alphatar":{"enabled":true,"urlPath":"https://s.yimg.com/rz/uh/alphatars/","imgType":".png"},"avatar":{"serverCall":false,"api":{"protocol":"https","host":"ucs.query.yahoo.com","path":"/v1/console/yql","query":{"format":"json","_maxage":900}}},"useAvatar":true,"bucket":"201","followable":{"enabled":true,"uri":"/_td_api"},"hasMailPreview":true,"hasMail":true,"hasNotifications":true,"hasProfile":true,"iSearch":{"isEnabled":false,"instantTrending":false,"frcode":"yfp-t-201","frcodeTrending":"fp-tts-201","spaceid":97162737,"cacheMaxAge":30000,"pageViewDelay":1000,"comscoreDelay":3000,"timeout":2500,"highConfidence":true,"autocomplete":{"additionalParams":{"appid":"is","nresults":4},"max_results":4},"api":{"protocol":"https://","host":"search.yahoo.com","path":"/search?","tmpl":"ISRCHA:A0124"},"filterUrls":{"web":"https://search.yahoo.com/search?p=","images":"https://images.search.yahoo.com/search/images?p=","video":"https://video.search.yahoo.com/search/video?p=","news":"https://news.search.yahoo.com/search?p=","local":"https://search.yahoo.com/local/s?p=","answers":"https://answers.search.yahoo.com/search/search_result?p=","shopping":"https://shopping.search.yahoo.com/search?p="}},"isFallback":false,"loginUrl":"https://login.yahoo.com/config/login?.src=fpctx&.intl=us&.lang=en-US&.done=https%3A%2F%2Fwww.yahoo.com","logoutUrl":"https://login.yahoo.com/config/login?logout=1&.direct=2&.src=fpctx&.intl=us&.lang=en-US&.done=https%3A%2F%2Fwww.yahoo.com","mail":{"compose":{"url":"https://mrd.mail.yahoo.com/compose"},"count":{"api":{"protocol":"https","host":"mg.mail.yahoo.com","path":"/mailservices/v1/newmailcount","query":{"appid":"UnivHeader"}},"refreshInterval":120,"maxCountDisplay":99},"preview":{"prefetch":true,"api":{"protocol":"https","host":"ucs.query.yahoo.com","path":"/v1/console/yql","query":{"q":"select messageInfo.receivedDate, messageInfo.mid, messageInfo.flags.isRead, messageInfo.from.name, messageInfo.subject from ymail.messages where numMid=\"3\" limit 6","format":"json"}},"urls":{"message":"https://mrd.mail.yahoo.com/msg?fid=Inbox&src=hp&mid="}},"url":"https://mail.yahoo.com/"},"miniheader":true,"miniheaderYPos":0,"notifications":{"inlineReader":true,"maxDisplay":0,"maxUpsellsDisplay":10,"count":{"api":{"uri":"/_td_api","requestType":"clustersUpdatesCount"},"refreshInterval":120,"maxCountDisplay":99},"list":{"api":{"uri":"/_td_api","requestType":"clustersList","image_tag":"pc:size=square,pc:size=small_fixed"}}},"search":{"action":"https://search.yahoo.com/search","assistYlc":";_ylc=X3oDMTFiaHBhMnJmBF9TAzIwMjM1MzgwNzUEaXRjAzEEc2VjA3NyY2hfcWEEc2xrA3NyY2hhc3Q-","ylc":";_ylc=X3oDMTFiN25laTRvBF9TAzIwMjM1MzgwNzUEaXRjAzEEc2VjA3NyY2hfcWEEc2xrA3NyY2h3ZWI-","queries":[{"name":"fr","value":"yfp-t-201"},{"name":"fp","value":"1"},{"name":"toggle","value":"1"},{"name":"cop","value":"mss"},{"name":"ei","value":"UTF-8"}],"autocomplete":{"ghostEnabled":false,"host":"https://search.yahoo.com/sugg/gossip/gossip-us-ura/","crumbKey":"gossip","theme":{"highlight":{"color":"black","background":"#c6d7ff"}},"plugin":{"minQueryLength":3,"resultHighlighter":"phraseMatch"}}},"searchFormGlow":true,"searchMiniHeader":false,"accountSwitchData":{"enabled":""}}}},"templates":{"main":{"yui_module":"td-applet-mega-header-templates-main","template_name":"td-applet-mega-header-templates-main"},"mailpreview":{"yui_module":"td-applet-mega-header-templates-mailpreview","template_name":"td-applet-mega-header-templates-mailpreview"},"accountSwitchPanel":{"yui_module":"td-applet-mega-header-templates-accountSwitchPanel","template_name":"td-applet-mega-header-templates-accountSwitchPanel"},"notificationpanel":{"yui_module":"td-applet-mega-header-templates-notificationpanel","template_name":"td-applet-mega-header-templates-notificationpanel"},"topbar":{"yui_module":"td-applet-mega-header-templates-topbar","template_name":"td-applet-mega-header-templates-topbar"}},"i18n":{"ACCOUNT_INFO":"Account Info","ACCOUNT_SWITCH_WELCOME_1":"Easily switch between multiple Yahoo accounts using the new","ACCOUNT_SWITCH_WELCOME_2":"Click \"Add account\" below to get started!","ACCOUNT_SWITCH_COOKIE_ERR":"It looks like you switched accounts. Refresh the browser to view your personalized page.","ACCOUNT_SWITCH_CRUMB_ERR":"Your account data may be out of sync.<br>Refresh the page to see your accounts.","ACCOUNT_SWITCH_ACCOUNT_MANAGER":"Account Manager","ADD_ACCOUNT":"Add account","ADD_MANAGE_ACCOUNT":"Add or manage accounts","ANSWERS":"Answers","CLEAR_SEARCH":"Clear search query","COMPOSE":"Compose","COMPOSE_CAPS":"COMPOSE","CORPMAIL":"Corp Mail","DEVELOPING_NOW":"Developing Now","ENTER_KEY":"Enter","FIREFOX_PROMO_TEXT":"Install the new Firefox","FOLLOW":"Follow","FOLLOWING":"Following","FROM":"From","GO_TO_MAIL":"Go To Mail","GO_TO_MAIL_CAPS":"GO TO MAIL","HAVE_NO_NEW_MESSAGES":"You have no new messages.","HAVE_NO_UPDATES":"Check back later for updates on stories you are following.","HOME":"Home","IMAGES":"Images","LOADING_MAIL":"Loading Mail Preview","LOADING_UPDATES":"Loading Updates","LOCAL":"Local","MAIL":"Mail","MAIL_CAPS":"MAIL","MENU":"Menu","NEW":"New","NEWS":"News","NEW_MESSAGE":"new message.","NEW_MESSAGES":"new messages.","NOT_FOLLOWING":"Follow a story and get updates here.","NOTIFICATIONS":"Notifications","PRESS":"Press ","PROFILE":"Profile","REL_DAYS":"{0}d","REL_DAYS_AGO":"{0} days ago","REL_HOURS":"{0}h","REL_HOURS_AGO":"{0} hours ago","REL_MINS":"{0}m","REL_MINS_AGO":"{0} minutes ago","REL_MONTHS":"{0}mo","REL_MONTHS_AGO":"{0} months ago","REL_SECS":"{0}s","REL_SECS_AGO":"{0} seconds ago","REL_WEEKS":"{0}wk","REL_WEEKS_AGO":"{0} weeks ago","REL_YEARS":"{0}yr","REL_YEARS_AGO":"{0} years ago","SEARCH":"Search","SEARCH_WEB":"Search Web","SETTINGS":"Settings","SETTINGS_CAPS":"SETTINGS","SHOPPING":"Shopping","SIGNIN":"Sign in","SIGNIN_CAPS":"SIGN IN","SIGNOUT":"Sign Out","SIGNOUT_ALL":"Sign out all","SIGNOUT_CAPS":"SIGN OUT","START_TYPING":"Start typing...","SUBJECT":"Subject","TO_SAVE_FOLLOWS":" to save and get updates.","TO_SEARCH":" to search.","TO_VIEW_NOTIF":" to view your notifications","TO_VIEW_MAIL":" to view your mail","UNABLE_TO_PREVIEW_MAIL":"We are unable to preview your mail.","UNABLE_TO_FETCH_UPDATES":"Check back later for updates on stories you are following.","UNFOLLOW":"Unfollow","VIDEO":"Video","WEB":"Web","WELCOME_BACK":"Welcome back","YOU_HAVE":"You have","SKIP_ASIDE":"Skip to Right rail","SKIP_MAIN":"Skip to Main content","SKIP_NAV":"Skip to Navigation"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-viewer":{"base":"/sy/os/mit/td/td-applet-viewer-0.1.2128-b.hotfix.j-18/","root":"os/mit/td/td-applet-viewer-0.1.2128-b.hotfix.j-18/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_50000101"] = {"applet_type":"td-applet-viewer","models":{"viewer":{"yui_module":"td-viewer-model","yui_class":"TD.Viewer.Model","config":{"dataFetch":{"NUM_PREFETCH_ITEMS":20,"NUM_RENDER_ITEMS":1,"NUM_BATCH_PHOTOS":25,"STALE_DATA_FETCH":1800},"cache":{"max-age":{"SLIDESHOW":300,"STORY":600,"VIDEO":3600},"stale-while-revalidate":{"SLIDESHOW":43200,"STORY":43200,"VIDEO":43200}},"uiConfig":{"adsMeta":true,"alphatar":{"enabled":true,"defaultProfile":"https://s.yimg.com/dh/ap/social/profile/profile_b48.png"},"branding":{"gma":{"src":"https://s.yimg.com/dh/ap/default/151203/GMA-banner.jpg","alt":"Good Morning America"}},"comments":{"enabled":true,"proxy":1,"applet":"td-applet-mega-comments","offnet":{"enabled":false},"config":{"ui.enableMyComments":true}},"comments_allow_supression":true,"comments_preview":true,"comments_writes_enabled":true,"disableSlideshowMON":true,"enableCategories":true,"enableCaptionScroll":false,"enableEntities":true,"enableContinueReading":"","enableModalBackfillViewportDetection":false,"enableModalBackfillVideoAds":true,"enableModalBackfillVideoAdsHtml5":true,"enableModalBackfillVideoAdsIframe":false,"enableModalContentPV":true,"enableModalCustomEvent":true,"enableModalLargeAds":true,"enableModalLargeAdsOffnetwork":true,"enableModalLRECBackfill":true,"enableModalOmnitureBeacon":true,"enableModalSingleAutoplay":true,"enableModalSponsoredAds":true,"enableSlideshowv2":true,"enableSlideshowv2Thumbnails":false,"enableSidekickAdditionalRendering":true,"enableSidekickCommentsRefresh":true,"enableSidekickDynamicHeight":true,"enableSidekickFederationCall":true,"enableLike":true,"enableLongCaption":true,"enableReblog":true,"enableReadMore":true,"enableShare":true,"fixedHeader":false,"forceRedisRead":true,"imageResize":true,"footer":true,"inlineView":true,"lazyLoad":true,"lead":{"minWidth":600,"resizedWidth":300},"licensedFullBody":false,"linkYlk":"itc:0,elm:context_link;","loadAppletDelay":"","loginUrl":"https://login.yahoo.com/config/login?.intl=us&.lang=en-US","nydcContent":true,"optimisticPrefetch":false,"readMoreMinScore":"0.7","renderAdsOnly":false,"resizeSlideshowEnabled":true,"searchBox":false,"share":{"tumblr":{"url":"//www.tumblr.com/widgets/share/tool/preview","post_url":"https://www.tumblr.com/widgets/share/tool?shareSource=legacy&canonicalUrl=&posttype=link&"}},"singleRightRail":false,"slots":{"sidekickdesktop":{"name":"td-applet-sidekick","condition":"all","config":{"disableAppClass":1,"site":"fp","ui.template":"megastream","estHeight":230,"countMargin":5,"ads.display.start":2,"ads.display.frequency":4,"enableAdsFeedback":1,"enableStreamViewCall":1,"mode":"","viewer":"1","condition":"all","maxBatchCount":"35","smartCrop":"1","maxTotalCount":"90","wideImageDesign":"1","uiTemplate":"megacards"}},"readMore":{"name":"td-applet-related-content","condition":"all","config":{"disableAppClass":1,"site":"fp","viewer_enabled":1,"smart_crop":1,"count":"4","image_size":"img:159x159|2|80","min_count":"3","request_count":"20"}},"sidekick":{"config":{"enableAdsFeedback":1},"condition":"none"}},"slotsOrder":[],"sticker":true,"stickerTarget":"#SearchBar-Wrapper-Mini","stream":{"enabled":false,"config":{"header":0,"ui.dislike":0,"ui.filters":0,"ui.hq_ad_template":"featured_ad","ui.like":0,"ui.save":0,"ui.templates.all.gap":1,"ui.templates.all.start":5,"ui.templates.featured.batch_max":3,"ui.templates.featured.gap":2,"ui.viewer_off_network":1,"ui.viewer":1}},"swipe":false,"pcsExclusions":false,"useCapSummary":true,"useUuidPrefix":true,"useSsYcts":true,"lcpBodyEnabled":false,"videoEventsWaitTime":300,"videoPlayer":{"version":"","env":""},"videoEnrichment":false,"yqlResizeEnabled":true,"estSideAdsHeight":610,"estSideNoAdsHeight":60,"mode":"mega-modal","sidekickBatchDelay":500,"sidekickFollowingDelay":3000,"sidekickMaxBatchCount":35,"sidekickMaxTotalCount":90,"slotsSide":["sidekickdesktop"],"experience":"","useContentSite":1,"enable":{"cluster":{"items":1}},"enableMyComments":1,"lrecBackfill":{"img":"https://s.yimg.com/os/mit/media/m/ads/images/backfill/sports-dailyfantasy-v2-a966da5.jpg","link":"https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=20&mc=click&pli=14816902&PluID=0&ord=${CACHEBUSTER}"},"sidekickFixCount":"5"},"enableCategories":true,"enableEntities":true,"useUuidPrefix":true,"optimisticPrefetch":false}},"ads":{"yui_module":"td-ads-model","yui_class":"TD.Ads.Model","config":{"sectionId":4250754,"positionThreshold":200,"enabled":false,"count":5,"lowerBound":2,"fromContentAPI":0,"fromCurveballAPI":1,"enableBeaconListenerOnly":true,"enableVisibilityRect":true,"adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html"}},"applet_model":{"models":["viewer","ads"]}},"views":{"main":{"yui_module":"td-viewer-mainview","yui_class":"TD.Viewer.MainView","config":{"ads":{"enableAdsFeedback":0,"enableVideoSpaceid":1,"curveball":{"sectionId":4250754,"positionThreshold":200,"enabled":false,"count":5,"lowerBound":2,"fromContentAPI":0,"fromCurveballAPI":1,"enableBeaconListenerOnly":true,"enableVisibilityRect":true,"adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html"},"displayAds":{"config":{"LDRP-9":{"w":"768","h":"1"},"LDRB-9":{"w":"728","h":"90","supports":{"exp-ovr":1,"exp-push":1},"fdb":true,"z":11},"LREC-9":{"w":"300","h":"250","fdb":true,"supports":{"exp-ovr":1,"exp-push":1},"z":11},"WPS-9":{"w":"320","h":"50"},"WP-9":{"w":"320","h":"50"},"sa":"LREC='300x250;1x1' LREC2='300x250;1x1' LREC3='300x250;1x1' MON='300x600;1x1' megamodal=true","LREC2-9":{"w":"300","h":"250","fdb":true,"supports":{"exp-ovr":1,"exp-push":1},"z":11},"LREC3-9":{"w":"300","h":"250","fdb":true,"supports":{"exp-ovr":1,"exp-push":1},"z":11},"MAST-9":{"supports":{"exp-ovr":1,"exp-push":1,"resize-to":1},"fdb":false,"closeBtn":{"mode":2,"useShow":1},"metaSize":true,"z":11},"MON-9":{"w":"300","h":"600","fdb":true,"supports":{"exp-ovr":1,"exp-push":1},"z":11},"SPL-9":{"flex":"both","supports":{"cmsg":1},"fdb":false,"meta":{"type":"stream"},"z":11,"css":".Mags-FontA {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 300;} .Mags-FontA.Size1 {font-size: 13px;}.Mags-FontA.Size2 {font-size: 16px;} .Mags-FontA.Size3 {font-size: 20px;}.Mags-FontA.Size4 {font-size: 22px;} .Mags-FontA.Size5 {font-size: 33px;}.Mags-FontA.Size6 {font-size: 35px;} .Mags-FontA.Size7 {font-size: 58px;}.Mags-FontA.Size8 {font-size: 70px;} .Mags-FontA.Size9 {font-size: 100px;} .Mags-FontB {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 400;} .Mags-FontB.Size1 {font-size: 18px;} .Mags-FontC {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 400;}.Mags-FontC.Size1 {font-size: 11px;}.Mags-FontC.Size2 {font-size: 14px;} .Mags-FontC.Size3 {font-size: 16px;}.Mags-FontC.Size4 {font-size: 20px;} .Mags-FontC.Size5 {font-size: 30px;}.Mags-FontC.Size6 {font-size: 32px;} .Mags-FontC.Size7 {font-size: 52px;}"},"SPL2-9":{"flex":"both","supports":{"cmsg":1},"fdb":false,"meta":{"type":"stream"},"z":11,"css":".Mags-FontA {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 300;} .Mags-FontA.Size1 {font-size: 13px;}.Mags-FontA.Size2 {font-size: 16px;} .Mags-FontA.Size3 {font-size: 20px;}.Mags-FontA.Size4 {font-size: 22px;} .Mags-FontA.Size5 {font-size: 33px;}.Mags-FontA.Size6 {font-size: 35px;} .Mags-FontA.Size7 {font-size: 58px;}.Mags-FontA.Size8 {font-size: 70px;} .Mags-FontA.Size9 {font-size: 100px;} .Mags-FontB {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 400;} .Mags-FontB.Size1 {font-size: 18px;} .Mags-FontC {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 400;}.Mags-FontC.Size1 {font-size: 11px;}.Mags-FontC.Size2 {font-size: 14px;} .Mags-FontC.Size3 {font-size: 16px;}.Mags-FontC.Size4 {font-size: 20px;} .Mags-FontC.Size5 {font-size: 30px;}.Mags-FontC.Size6 {font-size: 32px;} .Mags-FontC.Size7 {font-size: 52px;}"},"FSRVY-9":{"w":"1","h":"1","supports":{"exp-ovr":1,"exp-push":1,"bg":1,"lyr":1},"z":11},"FOOT9-9":{"w":"1","h":"1","supports":{"exp-ovr":1,"exp-push":1},"z":11},"FOOT-9":{"w":"1","h":"1","supports":{"exp-ovr":1,"exp-push":1},"z":11}},"enabled":1,"enabledAuto":false,"enabledDeferRenderAds":false,"forced_modalspaceid":"","enabledMultiAds":true,"positions":{"aboveFold":["LREC-9"],"belowFold":[],"custom":["MAST-9","LDRB-9","SPL-9","SPL2-9"]},"modalposition":"LREC-9,MAST-9,LDRB-9,SPL-9,FSRVY-9,FOOT9-9,MON-9","lrecTimeout":"","enableK2BeaconConf":true,"enableBucketSiteAttribute":true,"offnetSpaceid":"1197762606","offnetposition":"LREC-9","magazineposition":"LREC-9,MAST-9,LDRB-9,SPL-9,FOOT-9,MON-9"},"YVAP":{"accountId":"904","playContext":"default","spaceId":null},"displayAdsFP":{"enabled":0,"pos":"FPAD,LREC","config":[{"id":"FPAD","dest":"my-adsFPAD","pos":"FPAD","clean":"my-adsFPAD-base","h":"250","w":"300"},{"id":"LREC","dest":"my-adsLREC","pos":"LREC","clean":"my-adsLREC-base","h":"250","w":"300"}]},"modalYVAP":{"news":"145","finance":"193","sports":"82","gma":"145","default":"904"},"modalBackfillYVAP":{"news":"1115","finance":"1116","sports":"1118","default":"1115"},"modalPlayContext":{"news":"default","finance":"default","sports":"default","gma":"gma","autos":"bmprpreover","beauty":"bmprpreover","celebrity":"bmprpreover","food":"bmprpreover","health":"bmprpreover","makers":"bmprpreover","movies":"bmprpreover","music":"bmprpreover","parenting":"bmprpreover","politics":"bmprpreover","style":"bmprpreover","tech":"bmprpreover","travel":"bmprpreover","tv":"bmprpreover"},"modalBackfillExpName":"sidekickTVlrecbackfillHTML5","modalBackfillExpType":"right-rail-autoplay","modalBackfillAdTitlePrefix":"UP NEXT: ","modalBackfillYVAPHTML5":{"news":"1115","finance":"1116","sports":"1118","default":"1115"},"modalBackfillExpNameHTML5":"sidekickTVlrecbackfillHTML5"},"category":"","hlView":true,"js":{"twitter":"https://platform.twitter.com/widgets.js","videoplayer":"https://yep.video.yahoo.com/js/3/videoplayer-min.js?r=&ypv=","omniture":"https://s.yimg.com/os/mit/media/m/news/omniture-mega-min-78f5e30.js","embed":["click-capture-min","utils"]},"redirectNoContent":true,"rendered":false,"search":{"action":"https://search.yahoo.com/search","frcode":"yfp-t-201-m"},"spaceid":0,"ui":{"adsMeta":true,"alphatar":{"enabled":true,"defaultProfile":"https://s.yimg.com/dh/ap/social/profile/profile_b48.png"},"branding":{"gma":{"src":"https://s.yimg.com/dh/ap/default/151203/GMA-banner.jpg","alt":"Good Morning America"}},"comments":{"enabled":true,"proxy":1,"applet":"td-applet-mega-comments","offnet":{"enabled":false},"config":{"ui.enableMyComments":true}},"comments_allow_supression":true,"comments_preview":true,"comments_writes_enabled":true,"disableSlideshowMON":true,"enableCategories":true,"enableCaptionScroll":false,"enableEntities":true,"enableContinueReading":"","enableModalBackfillViewportDetection":false,"enableModalBackfillVideoAds":true,"enableModalBackfillVideoAdsHtml5":true,"enableModalBackfillVideoAdsIframe":false,"enableModalContentPV":true,"enableModalCustomEvent":true,"enableModalLargeAds":true,"enableModalLargeAdsOffnetwork":true,"enableModalLRECBackfill":true,"enableModalOmnitureBeacon":true,"enableModalSingleAutoplay":true,"enableModalSponsoredAds":true,"enableSlideshowv2":true,"enableSlideshowv2Thumbnails":false,"enableSidekickAdditionalRendering":true,"enableSidekickCommentsRefresh":true,"enableSidekickDynamicHeight":true,"enableSidekickFederationCall":true,"enableLike":true,"enableLongCaption":true,"enableReblog":true,"enableReadMore":true,"enableShare":true,"fixedHeader":false,"forceRedisRead":true,"imageResize":true,"footer":true,"inlineView":true,"lazyLoad":true,"lead":{"minWidth":600,"resizedWidth":300},"licensedFullBody":false,"linkYlk":"itc:0,elm:context_link;","loadAppletDelay":"","loginUrl":"https://login.yahoo.com/config/login?.intl=us&.lang=en-US","nydcContent":true,"optimisticPrefetch":false,"readMoreMinScore":"0.7","renderAdsOnly":false,"resizeSlideshowEnabled":true,"searchBox":false,"share":{"tumblr":{"url":"//www.tumblr.com/widgets/share/tool/preview","post_url":"https://www.tumblr.com/widgets/share/tool?shareSource=legacy&canonicalUrl=&posttype=link&"}},"singleRightRail":false,"slots":{"sidekickdesktop":{"name":"td-applet-sidekick","condition":"all","config":{"disableAppClass":1,"site":"fp","ui.template":"megastream","estHeight":230,"countMargin":5,"ads.display.start":2,"ads.display.frequency":4,"enableAdsFeedback":1,"enableStreamViewCall":1,"mode":"","viewer":"1","condition":"all","maxBatchCount":"35","smartCrop":"1","maxTotalCount":"90","wideImageDesign":"1","uiTemplate":"megacards"}},"readMore":{"name":"td-applet-related-content","condition":"all","config":{"disableAppClass":1,"site":"fp","viewer_enabled":1,"smart_crop":1,"count":"4","image_size":"img:159x159|2|80","min_count":"3","request_count":"20"}},"sidekick":{"config":{"enableAdsFeedback":1},"condition":"none"}},"slotsOrder":[],"sticker":true,"stickerTarget":"#SearchBar-Wrapper-Mini","stream":{"enabled":false,"config":{"header":0,"ui.dislike":0,"ui.filters":0,"ui.hq_ad_template":"featured_ad","ui.like":0,"ui.save":0,"ui.templates.all.gap":1,"ui.templates.all.start":5,"ui.templates.featured.batch_max":3,"ui.templates.featured.gap":2,"ui.viewer_off_network":1,"ui.viewer":1}},"swipe":false,"pcsExclusions":false,"useCapSummary":true,"useUuidPrefix":true,"useSsYcts":true,"lcpBodyEnabled":false,"videoEventsWaitTime":300,"videoPlayer":{"version":"","env":""},"videoEnrichment":false,"yqlResizeEnabled":true,"estSideAdsHeight":610,"estSideNoAdsHeight":60,"mode":"mega-modal","sidekickBatchDelay":500,"sidekickFollowingDelay":3000,"sidekickMaxBatchCount":35,"sidekickMaxTotalCount":90,"slotsSide":["sidekickdesktop"],"experience":"","useContentSite":1,"enable":{"cluster":{"items":1}},"enableMyComments":1,"lrecBackfill":{"img":"https://s.yimg.com/os/mit/media/m/ads/images/backfill/sports-dailyfantasy-v2-a966da5.jpg","link":"https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=20&mc=click&pli=14816902&PluID=0&ord=${CACHEBUSTER}"},"sidekickFixCount":"5"},"clusterEnabled":false}}},"templates":{"main":{"yui_module":"td-applet-viewer-templates-main","template_name":"td-applet-viewer-templates-main"},"cards":{"yui_module":"td-applet-viewer-templates-cards","template_name":"td-applet-viewer-templates-cards"},"carousel":{"yui_module":"td-applet-viewer-templates-carousel","template_name":"td-applet-viewer-templates-carousel"},"story":{"yui_module":"td-applet-viewer-templates-story","template_name":"td-applet-viewer-templates-story"},"reblog":{"yui_module":"td-applet-viewer-templates-reblog","template_name":"td-applet-viewer-templates-reblog"},"video":{"yui_module":"td-applet-viewer-templates-video","template_name":"td-applet-viewer-templates-video"},"slideshow":{"yui_module":"td-applet-viewer-templates-slideshow","template_name":"td-applet-viewer-templates-slideshow"},"header":{"yui_module":"td-applet-viewer-templates-header","template_name":"td-applet-viewer-templates-header"},"content_body":{"yui_module":"td-applet-viewer-templates-content_body","template_name":"td-applet-viewer-templates-content_body"},"content_body_mega":{"yui_module":"td-applet-viewer-templates-content_body_mega","template_name":"td-applet-viewer-templates-content_body_mega"},"story_cover":{"yui_module":"td-applet-viewer-templates-story_cover","template_name":"td-applet-viewer-templates-story_cover"},"lightbox":{"yui_module":"td-applet-viewer-templates-lightbox","template_name":"td-applet-viewer-templates-lightbox"},"lightbox_mega":{"yui_module":"td-applet-viewer-templates-lightbox_mega","template_name":"td-applet-viewer-templates-lightbox_mega"},"fallback":{"yui_module":"td-applet-viewer-templates-fallback","template_name":"td-applet-viewer-templates-fallback"},"ads_story":{"yui_module":"td-applet-viewer-templates-ads_story","template_name":"td-applet-viewer-templates-ads_story"},"header_ads":{"yui_module":"td-applet-viewer-templates-header_ads","template_name":"td-applet-viewer-templates-header_ads"},"footer":{"yui_module":"td-applet-viewer-templates-footer","template_name":"td-applet-viewer-templates-footer"},"share_btns":{"yui_module":"td-applet-viewer-templates-share_btns","template_name":"td-applet-viewer-templates-share_btns"},"share_btns_mega":{"yui_module":"td-applet-viewer-templates-share_btns_mega","template_name":"td-applet-viewer-templates-share_btns_mega"},"mag_slideshow":{"yui_module":"td-applet-viewer-templates-mag_slideshow","template_name":"td-applet-viewer-templates-mag_slideshow"},"drawer":{"yui_module":"td-applet-viewer-templates-drawer_feedback","template_name":"td-applet-viewer-templates-drawer_feedback"},"thank_you":{"yui_module":"td-applet-viewer-templates-ad_fdb_thank_you","template_name":"td-applet-viewer-templates-ad_fdb_thank_you"},"sidekicktv":{"yui_module":"td-applet-viewer-templates-sidekicktv","template_name":"td-applet-viewer-templates-sidekicktv"}},"i18n":{"PREVIOUS":"Previous","Next":"Next","MORE":"...","LESS":"less","NOCONTENT_FUNNY":"Feeding the engineers.  Will be back soon...","NOCONTENT_READMORE":"Read more on \"{0}\"","OFFNETWORK":"Read more on {0}","ELLIPSIS":"{0} ...","VIDEO_DURATION":"Duration {0}","SPONSORED":"Sponsored","SWIPE_FOR_NEXT":"Swipe for next article","INSTALL_NOW":"Install now","FULL_ARTICLE":"Read Full Article","ARTICLE_SUMMARY":"Read Summary","READ_MORE":"Read More","AD":"Advertisement","SHARE_EMAIL":"Share to Mail","CLOSE":"Close","RELATED_NEWS":"Related news","START_SLIDESHOW":"Start Slideshow","CONTINUE_READING":"Continue Reading","CLICK_FOR_VIDEOS":"Click here to watch the video","CLICK_FOR_PHOTOS":"Click here to view photos","AD_FDB1":"It's offensive to me","AD_FDB2":"I keep seeing this","AD_FDB3":"It's not relevant to me","AD_FDB4":"Something else","AD_FDB_HEADING":"Why don't you like this ad ?","AD_REVIEW":"We'll review and make changes needed.","AD_THANKYOU":"Thank you for your feedback","AD_SUBMIT":"Submit","AD_FDB_ERORR":"Please select option. To help us making your experience better.","AD_FDB_SOMETHING_ELSE":"Tell us, why you don't like this ad?","DONE":"Done","READ_ARTICLE":"Read more","LIKE":"Like","LIKE_THIS_TOPIC":"Like this topic","REBLOG":"Reblog","REBLOG_TUMBLR":"Reblog on Tumblr","UNDO":"Undo","SIGN_IN_TO_LIKE":"Sign in to like","LIKABLE_ARTICLE_SIGN_IN":"Sign in to see more stories you like.","SIGN_IN_2":"Sign in","READ_FULL_ARTICLE":"Read full article","COMMENTS":"Comments","FACEBOOK":"Share","TWITTER":"Tweet","EMAIL":"Email","CLOSE_VIEWER":"Close this content, you can also use the Escape key at anytime"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};window.ViewerClickCapture||(window.ViewerClickCapture=function(){function a(b,c,d){return b?b.tagName===c&&b.className&&b.className.indexOf(d)>=0?b:a(b.parentNode,c,d):!1}function b(b){var c=a(b.target,"A","js-content-viewer");c&&(d=b,b.preventDefault&&b.preventDefault())}function c(){!e&&f.removeEventListener&&f.removeEventListener("click",b),e=!0}var d,e,f=document.documentElement;return f.addEventListener&&(f.addEventListener("click",b),window.setTimeout(function(){c()},4e3)),{clear:function(){d=null},last:function(){return d},disable:c}}());window.ViewerUtils||!function(){function a(){function a(a){return a&&"object"==typeof a&&"number"==typeof a.length&&g.call(a)===h||!1}function b(a){return"string"==typeof a||a&&"object"==typeof a&&g.call(a)===i||!1}function c(a){return"undefined"==typeof a}function d(a){return null===a}function e(a){return c(a)||d(a)}var f=Object.prototype,g=f.toString,h="[object Array]",i="[object String]";return{isArray:Array.isArray||a,isNull:d,isString:b,isUndefined:c,isVoid:e}}function b(b,c,d){var e=a();if(!b)return d;if(!c)return b;!e.isArray(c)&&e.isString(c)&&(c=c.split("."));for(var f=0,g=c.length;b&&g>f;f++)b=b[c[f]];return e.isVoid(b)?d:b}window.ViewerUtils={getObjValue:b}}();
YMedia.applyConfig({"groups":{"td-applet-navlinks-atomic":{"base":"https://s.yimg.com/os/mit/td/td-applet-navlinks-atomic-0.0.57/","root":"os/mit/td/td-applet-navlinks-atomic-0.0.57/","combine":true,"filter":"min","comboBase":"https://s.yimg.com/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_50000195"] = {"applet_type":"td-applet-navlinks-atomic","views":{"main":{"yui_module":"td-applet-navlinks-mainview","yui_class":"TD.Applet.NavlinksMainView"}},"i18n":{"TITLE":"navlinks-atomic","TOPICS":"Topics"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-stream-atomic":{"base":"/sy/os/mit/td/td-applet-stream-atomic-2.0.457/","root":"os/mit/td/td-applet-stream-atomic-2.0.457/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_50000173"] = {"applet_type":"td-applet-stream-atomic","models":{"stream":{"yui_module":"td-applet-stream-model-v2","yui_class":"TD.Applet.StreamModel2","data":{"ccode":"mega_global_ranking_hlv2_up_based","more_items":[{"type":"unknown","id":"e41e1170-3f2e-3d0e-9950-71210b7b1111","instrument":"4000013670S00001","is_eligible":"true"},{"type":"unknown","id":"13a2aadf-ae49-3105-a4fa-75658fdcf32f","instrument":"4000013840S00001","is_eligible":"true"},{"type":"unknown","id":"3f2d86b7-b102-3c6d-8360-97e60665fd97","instrument":"4000017100S00008","is_eligible":"false"},{"type":"unknown","id":"98828be4-8074-3b82-9ced-fc0e4b4f8d89","instrument":"4000006900S00001","is_eligible":"true"},{"type":"unknown","id":"c6c862af-fd3c-33ca-bfe0-ae43af52e59e","instrument":"4000016070S00001","is_eligible":"true"},{"type":"unknown","id":"143bcbe4-b09a-33a5-bc3d-3de974a80dcb","instrument":"4000009480S00001","is_eligible":"true"},{"type":"unknown","id":"bf5ed6dd-391a-3cfc-b2e3-1814b7c9b4b8","instrument":"4000002610S00004","is_eligible":"true"},{"type":"unknown","id":"72bb919f-d087-3564-bba5-12b2e81cb929","instrument":"4000021990S00008","is_eligible":"false"},{"type":"unknown","id":"ce45e48e-f336-3eba-a1b0-17de409463f9","instrument":"4000021190S00001","is_eligible":"true"},{"type":"unknown","id":"f46b240a-ac3a-3d1e-b1f6-dab08a5cc535","instrument":"4000006830S00001","is_eligible":"true"},{"type":"unknown","id":"c427c25b-4e90-32c9-8360-3792b64260f2","instrument":"4000006530S00004","is_eligible":"true"},{"type":"unknown","id":"036700b3-f193-3876-a735-2ae1c4aa959a","instrument":"4000019400S00001","is_eligible":"true"},{"type":"unknown","id":"87dda414-affa-3c2a-ae2b-51ccb5f11226","instrument":"4000021080S00008","is_eligible":"false"},{"type":"unknown","id":"2cedbc63-b6fb-36cd-a079-8eb81a8d88e3","instrument":"4000007979S00001","is_eligible":"true"},{"type":"unknown","id":"cfca7988-b7f0-3e27-821d-4f42ad4e0d30","instrument":"4000013560S00001","is_eligible":"true"},{"type":"unknown","id":"748c86d7-0f22-3d12-9f99-2c49032eb78c","instrument":"4000012110S00001","is_eligible":"true"},{"type":"unknown","id":"27b0a553-cc01-3cfd-af03-e132880fcb63","instrument":"4000007560S00001","is_eligible":"true"},{"type":"unknown","id":"84396f5c-491e-3c59-96c1-4c8f743db743","instrument":"4000019220S00008","is_eligible":"false"},{"type":"unknown","id":"592baa46-7038-3a22-8b0a-4bb25ddd9044","instrument":"4000019260S00008","is_eligible":"false"},{"type":"unknown","id":"536da447-fdfb-3f20-a2aa-28f97b0cab18","instrument":"4000007870S00001","is_eligible":"true"},{"type":"unknown","id":"3aac5d7f-3cc3-3154-9f98-95bb539db971","instrument":"4000019600S00008","is_eligible":"false"},{"type":"unknown","id":"561c5eef-a341-3dc6-960d-e8183fe541ae","instrument":"4000016960S00008","is_eligible":"false"},{"type":"unknown","id":"5204fcdc-7fe1-35ee-9ee7-dfe950a6bdd0","instrument":"4000011990S00001","is_eligible":"true"},{"type":"unknown","id":"d7c0de59-f545-34cf-af19-6c04aee80884","instrument":"4000016400S00002","is_eligible":"true"},{"type":"unknown","id":"a12b0ca6-8899-36eb-9a13-5c881166414a","instrument":"4000007420S00001","is_eligible":"true"},{"type":"unknown","id":"d6ddb0e7-16d0-3aaa-b4bd-9fb0fd5b4e5e","instrument":"4000020270S00008","is_eligible":"false"},{"type":"unknown","id":"508f5f1c-14f9-36d0-b552-2046c8fffa1b","instrument":"4000014750S00008","is_eligible":"false"},{"type":"unknown","id":"447acbef-9ab9-36c4-86c3-567318b2a345","instrument":"4000015580S00001","is_eligible":"true"},{"type":"unknown","id":"568a13a2-278f-3998-8709-df9ae91c97f2","instrument":"4000000840A100S00004","is_eligible":"true"},{"type":"unknown","id":"c-b043f86b-96bb-4502-8cd6-d23e71d3fd7f","instrument":"4000003150S00001","is_eligible":"false"},{"type":"unknown","id":"b8aaeae7-49d5-3d27-8eac-a688a03af05b","instrument":"4000014520S00008","is_eligible":"false"},{"type":"unknown","id":"5499c6f3-bbcd-33bc-9fa1-c33b39e15f72","instrument":"4000008320S00001","is_eligible":"true"},{"type":"unknown","id":"c-85bb277b-9c49-4b37-9634-3a4802094528","instrument":"4000001110S00001","is_eligible":"false"},{"type":"unknown","id":"f45bbb62-dd64-3442-99f0-00d3ce0b07c0","instrument":"4000007730S00001","is_eligible":"true"},{"type":"unknown","id":"804349be-8508-3bbf-8e1e-d0723508a9ea","instrument":"4000008140S00001","is_eligible":"true"},{"type":"unknown","id":"63922844-c5c0-3a88-83e8-580ccbec1a4d","instrument":"4000013040S00008","is_eligible":"false"},{"type":"unknown","id":"2c33186a-422d-3e8d-b728-a726b5fa27a7","instrument":"4000010180S00001","is_eligible":"true"},{"type":"unknown","id":"9507be09-737c-32f4-80d7-2da0e94cfe30","instrument":"4000008690S00001","is_eligible":"true"},{"type":"unknown","id":"c-95b9956f-7700-47ff-ad07-72f03ae3ff35","instrument":"4000003020S00001","is_eligible":"false"},{"type":"unknown","id":"429117f3-058b-31a7-98c7-103fedaf7417","instrument":"4000011750S00001","is_eligible":"true"},{"type":"unknown","id":"380df5d0-c672-3415-bb4b-c82d74e57e43","instrument":"4000013570S00002","is_eligible":"true"},{"type":"unknown","id":"e8254a6d-2ad1-315e-a856-da7c77b6c03f","instrument":"4000008280S00001","is_eligible":"true"},{"type":"unknown","id":"24283831-1b1a-3ed4-8472-c2a3374000e3","instrument":"4000012960S00001","is_eligible":"true"},{"type":"unknown","id":"41c8a13d-e35f-3651-84f2-c3fcfa693a86","instrument":"4000001030S00004","is_eligible":"true"},{"type":"unknown","id":"0faf6b4d-3fcb-3b59-931d-c651da217c69","instrument":"4000013460S00008","is_eligible":"false"},{"type":"unknown","id":"c-cb0dd808-d2bb-4603-8bec-d9509e685833","instrument":"4000001220S00001","is_eligible":"false"},{"type":"unknown","id":"c4befaad-a732-30fa-a48e-b385687be96e","instrument":"4000007080S00001","is_eligible":"true"},{"type":"unknown","id":"7a83e3ed-362f-3ddc-8647-d27f07fff961","instrument":"4000023980S00008","is_eligible":"false"},{"type":"unknown","id":"9425b92e-cd68-3805-9060-39e5523db13d","instrument":"4000008540S00001","is_eligible":"true"},{"type":"unknown","id":"6147207e-b3c3-3e6a-99eb-b9d9be949f76","instrument":"4000014510S00008","is_eligible":"false"},{"type":"unknown","id":"8068c983-732b-3dbc-8da0-84cd1f852212","instrument":"4000007979S00001","is_eligible":"true"},{"type":"unknown","id":"947c9279-d56e-3d62-9e17-5193edc01ddc","instrument":"4000015660S00008","is_eligible":"false"},{"type":"unknown","id":"4cc54af5-f098-3962-a090-50b5d7f005e1","instrument":"4000020950S00008","is_eligible":"false"},{"type":"unknown","id":"63b0ae0f-9c39-3f5f-b9c5-60f24584280c","instrument":"4000014660S00008","is_eligible":"false"},{"type":"unknown","id":"34264d07-0b8f-3a47-88b3-cba2cc5f3439","instrument":"4000008310S00001","is_eligible":"true"},{"type":"unknown","id":"f6dee085-5256-3080-bca9-1b48551e2a29","instrument":"4000013940S00002","is_eligible":"true"},{"type":"unknown","id":"41b51f5a-aef5-3ce1-a6e2-70b158fbc300","instrument":"4000007480S00001","is_eligible":"true"},{"type":"unknown","id":"4cfa6b56-10f5-3686-ab80-6f2a187d0561","instrument":"4000009140S00001","is_eligible":"true"},{"type":"unknown","id":"771d6dbc-600f-3118-8363-737c91849b98","instrument":"4000007070S00001","is_eligible":"true"},{"type":"unknown","id":"44d456c4-6506-3544-ae21-b6b62d2027ff","instrument":"4000015640S00008","is_eligible":"false"},{"type":"unknown","id":"4bc1d454-8ea6-3690-bc8f-8f424525a42b","instrument":"4000023280S00008","is_eligible":"false"},{"type":"unknown","id":"58e25fdd-6d39-37a5-85fd-0b4fd9681135","instrument":"4000013480S00001","is_eligible":"true"},{"type":"unknown","id":"20635729-4669-351f-ac50-407a902329bf","instrument":"4000002950S00004","is_eligible":"true"},{"type":"unknown","id":"a259601b-8165-3ce1-9dbb-1792df8e8915","instrument":"4000006490S00002","is_eligible":"true"},{"type":"unknown","id":"f51eda56-4419-3d2c-baa3-6cfe4da388bd","instrument":"4000016250S00001","is_eligible":"true"},{"type":"unknown","id":"c-f0aa89ff-7d4f-4fec-ab2b-bdf85076bcc4","instrument":"4000005310S00001","is_eligible":"false"},{"type":"unknown","id":"49496929-9e6a-3191-ac2b-40ce6e2c5521","instrument":"4000019990S00008","is_eligible":"false"},{"type":"unknown","id":"1a3f250e-daa7-358f-aeff-8b2697e78e13","instrument":"4000015490S00008","is_eligible":"false"},{"type":"unknown","id":"d910f718-0c58-3fe1-a072-a9f0ff390357","instrument":"4000011130S00001","is_eligible":"true"},{"type":"unknown","id":"00b581b8-4955-3eec-b360-6a8da064ca10","instrument":"4000010530S00001","is_eligible":"true"},{"type":"unknown","id":"feb83b87-8762-3c43-8b53-6c5006729b61","instrument":"4000010280S00001","is_eligible":"true"},{"type":"unknown","id":"765b9920-bc10-3eb2-8f69-8ce7502e8f65","instrument":"4000009090S00001","is_eligible":"true"},{"type":"unknown","id":"c-5221196c-492f-4aa8-a10a-f32b8655211b","instrument":"4000002250S00001","is_eligible":"false"},{"type":"unknown","id":"c75d5869-0a47-36d9-aa7f-e25060e49b24","instrument":"4000012630S00008","is_eligible":"false"},{"type":"unknown","id":"712d68d0-a29e-3939-8993-be056415ec0e","instrument":"4000007910S00001","is_eligible":"true"},{"type":"unknown","id":"0d0fd6af-acd7-317c-9084-09e5be34d659","instrument":"4000000840A100S00004","is_eligible":"true"},{"type":"unknown","id":"3b2ff46a-9698-35ab-af25-fb1059ad2529","instrument":"4000013680S00008","is_eligible":"false"},{"type":"unknown","id":"3c2971fe-7fbb-32dd-a149-0d0ba54c2ec1","instrument":"4000009530S00001","is_eligible":"true"},{"type":"unknown","id":"0ab4a2b8-3be0-360c-aab6-6f8f0b95d2d1","instrument":"4000010950S00001","is_eligible":"true"},{"type":"unknown","id":"d6bfd1de-ca20-348c-a1c7-ea31eeda7ee8","instrument":"4000013720S00001","is_eligible":"true"},{"type":"unknown","id":"f96566b8-117c-3150-a3a6-701913cef5a9","instrument":"4000019500S00008","is_eligible":"false"},{"type":"unknown","id":"7baae775-b2dd-3b76-8838-fb90db244aff","instrument":"4000007840S00001","is_eligible":"true"},{"type":"unknown","id":"c-be5c4f58-ad8b-42bf-a315-d6c4f1938237","instrument":"4000001430S00001","is_eligible":"false"},{"type":"unknown","id":"61b3819a-57a6-3bb2-96db-b9aa0a87c91f","instrument":"4000015770S00001","is_eligible":"true"},{"type":"unknown","id":"07dbbde9-3c73-3b2c-ba98-cdbd70f25fa1","instrument":"4000011120S00001","is_eligible":"true"},{"type":"unknown","id":"4a29e9e4-e3bd-3476-a5e5-982caf036330","instrument":"4000016400S00008","is_eligible":"false"},{"type":"unknown","id":"faf13430-e803-37f0-8b6b-f4367bd2dfc1","instrument":"4000006610S00001","is_eligible":"true"},{"type":"unknown","id":"ed271d0d-571c-3973-9dc4-85ba314a7d0e","instrument":"4000007660S00001","is_eligible":"true"},{"type":"unknown","id":"f620e8c5-8368-33b0-9297-66aa42f45613","instrument":"4000010610S00002","is_eligible":"true"},{"type":"unknown","id":"68856e10-aca6-372c-9519-5d49f07939da","instrument":"4000006950S00001","is_eligible":"true"},{"type":"unknown","id":"68673508-698b-3399-8009-4e8ec33806db","instrument":"4000007160S00001","is_eligible":"true"},{"type":"unknown","id":"f8fb82ad-b3b4-3e07-8686-4648ebcecfdd","instrument":"4000012970S00001","is_eligible":"true"},{"type":"unknown","id":"c-0204aaf0-63b2-4dc9-b378-c5a34070a5e8","instrument":"4000005240S00001","is_eligible":"false"},{"type":"unknown","id":"4f808baf-6696-378e-a7af-8b7fe600d04a","instrument":"4000009200S00001","is_eligible":"true"},{"type":"unknown","id":"c-3249de15-9238-4aab-b285-b206487ae74e","instrument":"4000002200S00001","is_eligible":"false"},{"type":"unknown","id":"aa789e23-143f-3688-af8d-ae1ff5ada498","instrument":"4000006600S00001","is_eligible":"true"},{"type":"unknown","id":"80df8fe1-d2dd-383c-a7ba-f7305c74d44f","instrument":"4000008970S00002","is_eligible":"true"},{"type":"unknown","id":"9f875f8d-1a03-3af9-898f-9da6b5f4fa4a","instrument":"4000004290S00004","is_eligible":"true"},{"type":"unknown","id":"3118e2b4-d21e-3fd9-a398-5f9de132160d","instrument":"4000008550S00001","is_eligible":"true"},{"type":"unknown","id":"82f6c218-7681-3d2b-a8e5-6bd92394334b","instrument":"4000008130S00001","is_eligible":"true"},{"type":"unknown","id":"cda15482-e890-38f7-8fae-817071194971","instrument":"4000008320S00001","is_eligible":"true"},{"type":"unknown","id":"fc352e54-a36b-3bf3-bc33-cf6a34bf6411","instrument":"4000007780S00001","is_eligible":"true"},{"type":"unknown","id":"f75e19e9-0746-3dcd-955f-24eb717d6024","instrument":"4000009120S00001","is_eligible":"true"},{"type":"unknown","id":"013781fe-450d-36a7-9a60-410de03b5f5e","instrument":"4000008149S00001","is_eligible":"true"},{"type":"unknown","id":"057e5877-e612-3457-9513-e792b423ab09","instrument":"4000011810S00001","is_eligible":"true"},{"type":"unknown","id":"41224ce6-d0b3-3d1d-9eff-9f1234cd67cf","instrument":"4000007340S00001","is_eligible":"true"},{"type":"unknown","id":"4e15f152-a2cc-3977-9267-ad00b4056b15","instrument":"4000013270S00001","is_eligible":"true"},{"type":"unknown","id":"4395f710-8e85-3f0f-80b8-f27f52400e85","instrument":"4000008940S00002","is_eligible":"true"},{"type":"unknown","id":"f5a4133e-6197-3be2-b096-6a0916558a3c","instrument":"4000000930S00004","is_eligible":"true"},{"type":"unknown","id":"310f7aec-7728-3354-864c-19e884d97785","instrument":"4000015180S00001","is_eligible":"true"},{"type":"unknown","id":"c-e961bdad-0c9e-4ca7-b0e4-1f55ad6b5366","instrument":"4000003950S00001","is_eligible":"false"},{"type":"unknown","id":"ff0230bf-c53e-3d62-a6c4-97431ac8dfe1","instrument":"4000013040S00001","is_eligible":"true"},{"type":"unknown","id":"c55392f2-9be3-3727-b409-9e1f9fcdc085","instrument":"4000012280S00001","is_eligible":"true"},{"type":"unknown","id":"517a0089-e538-3ece-862c-302ef5381ded","instrument":"4000012080S00001","is_eligible":"true"},{"type":"unknown","id":"2a56aa53-2495-30c4-9595-8788941be393","instrument":"4000014260S00001","is_eligible":"true"},{"type":"unknown","id":"09b578cb-dc83-327c-9361-741d40ad186f","instrument":"4000016270S00001","is_eligible":"true"},{"type":"unknown","id":"c-9d0ccf9b-2bcd-4fc5-91ad-3b7c6bb6171e","instrument":"4000001630S00001","is_eligible":"false"},{"type":"unknown","id":"7438e6d2-50e8-3fac-a25d-2d5ae3a591a0","instrument":"4000010480S00001","is_eligible":"true"},{"type":"unknown","id":"ce6d2b79-5432-31f3-ab03-2dc2af372487","instrument":"4000015360S00001","is_eligible":"true"},{"type":"unknown","id":"8977bb23-61a4-3e73-8335-da925628b8f5","instrument":"4000010810S00001","is_eligible":"true"},{"type":"unknown","id":"fc0feecb-a96b-37cf-a75e-f898292403f8","instrument":"4000008890S00001","is_eligible":"true"},{"type":"unknown","id":"65cb5f1f-c888-3b3b-8b0a-7eac76d571fd","instrument":"4000012030S00001","is_eligible":"true"},{"type":"unknown","id":"8a1e8aa8-0d16-32e0-aab7-87ad6e500a1f","instrument":"4000012240S00002","is_eligible":"true"},{"type":"unknown","id":"54de7958-9b90-3c3e-aeda-62995cffcc1e","instrument":"4000010350S00001","is_eligible":"true"},{"type":"unknown","id":"82095e3e-9f1d-34fc-8e6c-9a5f6dc7f61f","instrument":"4000009030S00001","is_eligible":"true"},{"type":"unknown","id":"79d7d42c-2ffe-362c-bd05-00af85316573","instrument":"4000008800S00001","is_eligible":"true"},{"type":"unknown","id":"d7ebd391-edd6-343b-a304-6615456cfa45","instrument":"4000011500S00001","is_eligible":"true"},{"type":"unknown","id":"bc6177c0-6367-34a0-899f-dfe6cc2e7a3b","instrument":"4000003340S00004","is_eligible":"true"},{"type":"unknown","id":"cd25ac3f-093f-38dc-92e4-a126d79696e5","instrument":"4000008430S00001","is_eligible":"true"},{"type":"unknown","id":"7508e664-1a72-3d0a-9f3d-c84d58f3d1c2","instrument":"4000010780S00001","is_eligible":"true"},{"type":"unknown","id":"9622bc41-a2fb-35a8-a9a7-49ad9f433309","instrument":"4000008189S00001","is_eligible":"true"},{"type":"unknown","id":"2cce2a89-c484-3c83-8d23-1b85a082511e","instrument":"4000012370S00002","is_eligible":"true"},{"type":"unknown","id":"ff948260-4143-31d6-9654-0e628c210e3a","instrument":"4000012190S00002","is_eligible":"true"},{"type":"unknown","id":"77a81111-294f-32b7-833d-5179bc0ee31d","instrument":"4000008440S00002","is_eligible":"true"},{"type":"unknown","id":"1a7cc594-5bfc-3b46-9fb7-598a32698f63","instrument":"4000009020S00002","is_eligible":"true"},{"type":"unknown","id":"41fb7319-6916-39c4-ad8a-c6efbcd3cc85","instrument":"4000009540S00002","is_eligible":"true"},{"type":"unknown","id":"47dba10e-6001-303c-a73b-3c108f987615","instrument":"4000007860S00002","is_eligible":"true"},{"type":"unknown","id":"1cc87254-6c1e-3468-8481-55b66823919e","instrument":"4000007830S00002","is_eligible":"true"},{"type":"unknown","id":"90995bbd-fca4-3972-bc0e-aceb8c1c835c","instrument":"4000011150S00002","is_eligible":"true"},{"type":"unknown","id":"f9e4b123-842d-3535-a0be-8d377821e252","instrument":"4000011270S00002","is_eligible":"true"},{"type":"unknown","id":"e5226d9a-dc12-3e3c-a29c-a35f1d5cdfcd","instrument":"4000007930S00002","is_eligible":"true"},{"type":"unknown","id":"70bd5c2c-c42b-312d-be36-7bfd9e2c9244","instrument":"4000008670S00002","is_eligible":"true"},{"type":"unknown","id":"c9b4702a-fdc1-3d81-a5ef-066a69c29e45","instrument":"4000009530S00002","is_eligible":"true"},{"type":"unknown","id":"d643b89d-f701-3163-a66b-1494cebfcaac","instrument":"4000000840A100S00004","is_eligible":"true"},{"type":"unknown","id":"576dcdd8-d795-3f36-add4-bfe809c5925a","instrument":"4000006520S00002","is_eligible":"true"},{"type":"unknown","id":"231e936a-34e0-3847-8b3a-9beb50a0bd1a","instrument":"4000007570S00002","is_eligible":"true"},{"type":"unknown","id":"50c7e762-469a-3056-abb0-263cae761a4c","instrument":"4000010590S00002","is_eligible":"true"},{"type":"unknown","id":"13525824-9067-3c72-8dbd-24d1eccb936a","instrument":"4000010020S00002","is_eligible":"true"},{"type":"unknown","id":"8b469a85-cf73-355c-baf2-a36efbfd393d","instrument":"4000006700S00002","is_eligible":"true"},{"type":"unknown","id":"9a72ae3d-5cbf-3353-8154-7ebc3876bf89","instrument":"4000006980S00002","is_eligible":"true"},{"type":"unknown","id":"e9ca72a8-f6e8-3aba-adb4-5e547978413a","instrument":"4000009100S00002","is_eligible":"true"},{"type":"unknown","id":"f43a23f5-1888-3011-b174-67f4493a9261","instrument":"4000008050S00002","is_eligible":"true"},{"type":"unknown","id":"e52d41fd-caa5-3775-9a1c-d2640a5b0ba8","instrument":"4000007080S00002","is_eligible":"true"},{"type":"unknown","id":"884310a2-b79c-3ae7-a354-f5f221aa8181","instrument":"4000007570S00002","is_eligible":"true"},{"type":"unknown","id":"46652918-8e44-332c-8c77-96c6a940f97d","instrument":"4000006520S00002","is_eligible":"true"},{"type":"unknown","id":"f79c4a09-2e39-3a66-a130-bc8b0941b74c","instrument":"4000008610S00002","is_eligible":"true"},{"type":"unknown","id":"c0e9b570-2899-38f3-8b9e-bfb996b9e5a1","instrument":"4000008640S00002","is_eligible":"true"},{"type":"unknown","id":"1c0767ad-7ac7-3e2d-9431-eb069552e095","instrument":"4000000770A100S00004","is_eligible":"true"},{"type":"unknown","id":"7da7c1c0-0979-3b65-b415-e4f2bea54e26","instrument":"4000007810S00002","is_eligible":"true"},{"type":"unknown","id":"3ba3c751-f7d4-3ccf-9523-b9274c4ef1a3","instrument":"4000010410S00002","is_eligible":"true"},{"type":"unknown","id":"91678823-abfc-3aa9-90fe-f2564f006f23","instrument":"4000012330S00002","is_eligible":"true"}],"continuation":"","more":161,"enrichment_ids":[],"cposy":26,"stream_items":[{"id":"id-3596795","cauuid":"1a07fb94-d700-311d-8e60-e66daa2309b5","i13n":{"cpos":1,"cposy":1,"bpos":1,"pos":1,"ss_cid":"1a07fb94-d700-311d-8e60-e66daa2309b5","refcnt":4,"subsec":1162,"imgt":"ss","g":"1a07fb94-d700-311d-8e60-e66daa2309b5","aid":"id-3596795","ct":1,"pkgt":"need_to_know","grpt":"roundup","cnt_tpc":"Need To Know"},"link":"http://news.yahoo.com/police-testing-knife-recovered-property-once-owned-o-160945717.html","type":"article","viewer_eligible":true,"viewer_fetch":true,"storyline":[{"id":"id-3596792","cauuid":"9f4e9844-7077-3f79-b95e-ff0d228b5fec","link":"https://www.yahoo.com/politics/trump-at-a-disadvantage-in-a-brokered-convention-152052016.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"id-3596785","cauuid":"b2f85e1d-b4ce-38b2-9be9-e4f4df692093","link":"https://www.yahoo.com/movies/never-seen-hunger-games-deleted-scene-snow-142801996.html","type":"video","viewer_eligible":true,"viewer_fetch":true},{"id":"id-3596773","cauuid":"6c1ec14a-f6b6-35ba-a48e-83e3910db7c4","link":"http://sports.yahoo.com/news/holly-holm-is-ready-to-stop-talking-about-ronda-rousey-and-defend-her-title-025445079.html","type":"video","viewer_eligible":true,"viewer_fetch":true},{"id":"id-3596740","cauuid":"0a782601-1471-337a-91c4-fabc6ca8d269","link":"https://www.yahoo.com/politics/in-sign-of-apocalypse-donald-trump-defends-the-025053649.html","type":"article","viewer_eligible":true,"viewer_fetch":true}]},{"id":"eac3913b-8677-3613-8368-cb4ff55706a4","i13n":{"cpos":2,"cposy":6,"bpos":1,"pos":1,"imgt":"ss","g":"eac3913b-8677-3613-8368-cb4ff55706a4","ct":1,"pkgt":"orphan_img","grpt":"singlestory","cnt_tpc":"U.S."},"link":"https://www.bostonglobe.com/metro/2016/03/03/plans-for-family-skiing-week-ended-with-tragedy/nXYtNJLc4m1zEHceDn6Y0K/story.html","type":"article","viewer_eligible":true,"viewer_fetch":false},{"id":"e1f2df90-016d-37b6-b703-32aa83bcee15","i13n":{"cpos":3,"cposy":7,"bpos":1,"pos":1,"imgt":"ss","g":"e1f2df90-016d-37b6-b703-32aa83bcee15","ct":1,"pkgt":"orphan_img","grpt":"storyCluster","cnt_tpc":"U.S."},"link":"http://www.charlotteobserver.com/news/nation-world/national/article63858492.html","type":"article","viewer_eligible":true,"viewer_fetch":false},{"id":"f4f1d5f1-7e87-3f0b-a1e6-772897de19b7","i13n":{"cpos":4,"cposy":8,"bpos":1,"pos":1,"ss_cid":"f75e38cf-d1e2-416f-b063-ef98eac4511d","refcnt":2,"imgt":"ss","g":"f4f1d5f1-7e87-3f0b-a1e6-772897de19b7","ct":1,"pkgt":"cluster_all_img","grpt":"storyCluster","cnt_tpc":"Politics"},"link":"http://thehill.com/homenews/campaign/271763-gop-debate-winners-and-losers","type":"article","viewer_eligible":true,"viewer_fetch":false,"storyline":[{"id":"2db57017-a414-3d42-8491-9c860a97f3d7","link":"http://www.usatoday.com/picture-gallery/news/politics/elections/2016/03/03/photos-the-11th-republican-debate/81290160/","type":"unknown","viewer_eligible":true,"viewer_fetch":false},{"id":"2a1f12fa-c0f0-373c-b194-c68bf21e3046","link":"http://www.politico.com/tipsheets/the-2016-blast/2016/03/trump-handed-hardest-debate-213023","type":"unknown","viewer_eligible":true,"viewer_fetch":false}]},{"id":"d776207d-d02f-3080-b59b-b0f3c50ef0bd","i13n":{"cpos":5,"cposy":11,"bpos":1,"pos":1,"ss_cid":"6d6d97b3-33c9-4976-a44d-c08b127f1907","refcnt":2,"imgt":"ss","g":"d776207d-d02f-3080-b59b-b0f3c50ef0bd","ct":1,"pkgt":"cluster_first_img","grpt":"storyCluster","cnt_tpc":"U.S."},"link":"http://madamenoire.com/617410/receipt-outrage/","type":"article","viewer_eligible":true,"viewer_fetch":false,"storyline":[{"id":"8a18abc1-c916-34f8-8033-23dc80c2d22c","link":"http://www.eater.com/2016/3/2/11148584/ihop-receipt-black-ppl","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"5bd72e3d-7f8f-3f66-842e-c57cb63f16ae","link":"http://news.yahoo.com/austin-couple-received-racist-receipt-152700968.html","type":"article","viewer_eligible":true,"viewer_fetch":true}]},{"id":"39c55862-d5d5-34fe-90c5-868405ef714d","i13n":{"cpos":6,"cposy":14,"bpos":1,"pos":1,"ss_cid":"807b4e84-8754-4758-9e2c-727066e1b89b","refcnt":2,"subsec":530,"imgt":"ss","g":"39c55862-d5d5-34fe-90c5-868405ef714d","ct":1,"pkgt":"cluster_first_img","grpt":"storyCluster","cnt_tpc":"Lifestyle"},"link":"http://finance.yahoo.com/news/tried-burger-kings-menu-item-135902788.html","type":"article","viewer_eligible":true,"viewer_fetch":true,"storyline":[{"id":"1581f39e-8088-3505-88b7-91a8244f73e7","link":"http://nypost.com/2016/03/02/burger-kings-hot-dogs-are-a-disgusting-disgrace/","type":"unknown","viewer_eligible":true,"viewer_fetch":false},{"id":"e7f24a45-fe8e-36df-b37d-43665d4a0c39","link":"http://www.marketwatch.com/News/Story/Story.aspx?guid=140DC62C-E0A6-11E5-B9F0-6FCBBC2751E2&siteid=yhoof2","type":"article","viewer_eligible":true,"viewer_fetch":true}]},{"id":"eef30880-8c04-3b04-8f46-f4fef5beb812","i13n":{"cpos":7,"cposy":17,"bpos":1,"pos":1,"ss_cid":"2fb9aafb-d0a5-404e-9599-864a622cb31d","refcnt":2,"imgt":"ss","g":"eef30880-8c04-3b04-8f46-f4fef5beb812","ct":1,"pkgt":"cluster_all_img","grpt":"storyCluster","cnt_tpc":"Politics"},"link":"https://www.washingtonpost.com/blogs/right-turn/wp/2016/03/04/cruzs-problem-in-a-nutshell/","type":"article","viewer_eligible":true,"viewer_fetch":false,"storyline":[{"id":"ed038393-b2cc-37f3-9a56-22c832b54346","link":"http://news.yahoo.com/trump-gop-nominee-just-got-real-congress-083905342--election.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"95925914-ef66-332a-b589-29889d115437","link":"http://www.usatoday.com/story/news/politics/elections/2016/03/03/gop-michigan-debate/81256782/","type":"unknown","viewer_eligible":true,"viewer_fetch":false}]},{"id":"ef105aa6-3160-371c-8e54-f83c346ca3dd","i13n":{"cpos":8,"cposy":20,"bpos":1,"pos":1,"ss_cid":"f2a01e38-09d9-4d32-99bd-1dff25acffbd","refcnt":2,"imgt":"ss","g":"ef105aa6-3160-371c-8e54-f83c346ca3dd","ct":1,"pkgt":"cluster_all_img","grpt":"storyCluster","cnt_tpc":"U.S."},"link":"http://www.chicagotribune.com/suburbs/franklin-park/crime/ct-fhj-florida-man-drug-arrest-tl-0310-20160303-story.html","type":"article","viewer_eligible":true,"viewer_fetch":false,"storyline":[{"id":"93bb7fc4-3d3d-38ae-b063-1b00258859a0","link":"http://wtvr.com/2016/03/03/keith-smith-charges/","type":"unknown","viewer_eligible":true,"viewer_fetch":false},{"id":"794b36fd-b725-3bbd-b195-7b9f336865d6","link":"http://www.cleveland.com/metro/index.ssf/2016/03/cleveland_man_charged_with_str.html","type":"unknown","viewer_eligible":true,"viewer_fetch":false}]},{"id":"2edb99c4-23e0-3300-bda4-33d8f95d92cc","i13n":{"cpos":9,"cposy":23,"bpos":1,"pos":1,"ss_cid":"076238af-47cb-4bdc-8c28-1d7d96055a04","refcnt":2,"imgt":"ss","g":"2edb99c4-23e0-3300-bda4-33d8f95d92cc","ct":1,"pkgt":"cluster_all_img","grpt":"storyCluster","cnt_tpc":"Politics"},"link":"http://www.ibtimes.com/political-capital/hillary-clinton-spotlighting-crisis-flint-michigan-voted-against-measure-prevent","type":"article","viewer_eligible":true,"viewer_fetch":false,"storyline":[{"id":"275b62e9-66ed-3622-979c-6efb39b97a10","link":"http://www.mlive.com/news/flint/index.ssf/2016/03/welcome_to_flint_michigan_were.html","type":"unknown","viewer_eligible":true,"viewer_fetch":false},{"id":"1055767a-2a47-3d64-8c60-e03d84caf88e","link":"http://thinkprogress.org/health/2016/03/03/3756559/marco-rubio-slams-democrats-for-politicizing-flint-water-crisis/","type":"unknown","viewer_eligible":true,"viewer_fetch":false}]},{"id":"4766d312-9aca-3d00-91b7-b2ae2c6d5e2a","i13n":{"cpos":10,"cposy":26,"bpos":1,"pos":1,"subsec":394,"imgt":"ss","g":"4766d312-9aca-3d00-91b7-b2ae2c6d5e2a","ct":3,"pkgt":"video","grpt":"storyCluster","cnt_tpc":"Celebrity"},"link":"https://www.yahoo.com/music/jared-fogle-eats-sh-t-022938157.html","type":"video","viewer_eligible":true,"viewer_fetch":true}],"fetched":10,"interest_data":{"WIKIID:Los_Angeles":{"name":"Los Angeles"},"WIKIID:Nicole_Brown_Simpson":{"name":"Nicole Brown Simpson"},"WIKIID:TMZ":{"name":"TMZ"},"WIKIID:Buck_Knives":{"name":"Buck knife"},"WIKIID:Police_officer":{"name":"police officer"},"YCT:001000667":{"name":"Crime & Justice"},"YCT:001000780":{"name":"Society & Culture"},"YCT:001000291":{"name":"Death & Funeral"},"YCT:001000288":{"name":"Relationships"},"WIKIID:Teacher":{"name":"Teacher"},"YCT:001000267":{"name":"Education"},"YCT:001000269":{"name":"Schools"},"WIKIID:Donald_Trump":{"name":"Donald Trump"},"WIKIID:Winners_%26_Losers":{"name":"Winners & Losers"},"YCT:001000671":{"name":"Elections"},"YCT:001000661":{"name":"Politics"},"YCT:001000031":{"name":"Arts & Entertainment"},"YCT:001000803":{"name":"Racism"},"WIKIID:Kate_Taylor":{"name":"Kate Taylor"},"WIKIID:Hot_dog":{"name":"Hot dog"},"WIKIID:Burger_King":{"name":"Burger King"},"YCT:001000560":{"name":"Hobbies & Personal Activities"},"YCT:001000611":{"name":"Pets"},"WIKIID:Ted_Cruz":{"name":"Ted Cruz"},"YCT:001000681":{"name":"Government"},"WIKIID:Hillary_Clinton":{"name":"Hillary Clinton"},"YCT:001000638":{"name":"Environment"},"YCT:001000637":{"name":"Nature"},"YCT:001000695":{"name":"Legislative Branch"},"YCT:001000648":{"name":"Water Resources"},"YCT:001000646":{"name":"Natural Resources"},"WIKIID:Jared_Fogle":{"name":"Jared Fogle"}}}},"items":{"yui_module":"td-applet-stream-items-model-v2","yui_class":"TD.Applet.StreamItemsModel2"},"applet_model":{"yui_module":"td-applet-stream-appletmodel-v2","yui_class":"TD.Applet.StreamAppletModel2","config":{"ads":{"adchoices_text":true,"adchoices_url":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","ad_polices":true,"advt_text":false,"count":25,"contentType":"","fallback":0,"feedback":true,"frequency":4,"inline_video":true,"pu":"www.yahoo.com","se":4250754,"related_ct_se":"5454650","related_ct_single_ad":true,"related_dedupe_ads":true,"spaceid":"2023538075","sponsored_url":"http://help.yahoo.com/kb/index?page=content&y=PROD_FRONT&locale=en_US&id=SLN14553","start_index":1,"timeout":0,"type":"STRM,STRM_CONTENT,STRM_VIDEO","version":2,"related_start_index":3},"category":"","js":{"attribution_pos":"bottom","click_context":false,"content_events":true,"filters":false,"full_content_event":false,"pluck_item_fields":"id,cauuid,payoffId,events,i13n,link,type,videoUuid,viewer_eligible,viewer_fetch,video_ad_beacons,video_ad_assets","pluck_today_fields":"image,title,summary,publisher,more_link_text","poll_interval":60000,"related_collapse":true,"related_content":true,"related_count":4,"restore_filter":false,"restore_state":false,"restore_state_storage":"history","show_read":true,"sticker":false,"sticker_toptarget":"","summary_pos":"left","xhr_ss_timeout":500,"stories_like_this":0,"login_url":"https://login.yahoo.com/config/login?.intl=us&.lang=en-US&.src=fpctx&.done="},"linkSupplement":null,"onboarding":{"count":3,"debug":false,"pos":-1,"result":3,"topic":false},"pagination":true,"remoteConfig":{"endpoint":"/_td_remote","params":{"disableAppClass":true,"footer":false,"i13n":{"auto":false},"noAbbreviation":true,"rendermode":"hovercard"},"passLocdropCrumb":true,"type":"td-applet-weather"},"request_xhtrace":0,"signed_in":false,"ss_endpoint":"ga-hr.slingstone.yahoo.com@score/v9/homerun/en-US/unified/mega","ss_timeout":300,"template":{"main":"td-applet-stream-atomic:main","drawer":"td-applet-stream-atomic:drawer_desktop","drawer_action":"td-applet-stream-atomic:drawer_action","drawer_share":"td-applet-stream-atomic:drawer_share"},"timeout":800,"total":170,"ui":{"backfill_property_region":false,"big_click_target":true,"bleed":false,"bnb_enabled":true,"cat_label_enabled":true,"comments":true,"comments_allow_supression":true,"comments_inline":true,"comments_inline_ranking":"highestRated","comments_offnet":false,"comments_editorial_suppression":true,"comments_request_count":5,"comments_update":true,"comments_update_maxidle":300000,"comments_update_monitoring":true,"comments_update_throttle":1750,"comments_writes_enabled":true,"followable":true,"followable_signedout":true,"dislike":false,"dollar_sign":true,"editorial_edition":{"label":"EDITION_DEFAULT","top":true,"shouldUpdateLVtimeInCookie":true},"enable_featured_ad_video_gap":true,"enable_hovercolor":true,"enable_stream_notify":false,"enrich_tweet":0,"enrichment":{"types":"","unique":false},"entity_max":9,"exclude_types":"","exclusive_type":"","fauxdal":true,"featured_align":"left","featured_delayed_defer":false,"featured_percent_width":45,"featured_width":460,"featured_height":258,"featured_width_portrait":200,"featured_height_portrait":200,"first_featured_treatment":true,"fixed_layout":false,"fptoday_thumbs_sparse":false,"headline_wrapper":true,"hq_ad_template":"featured_ad","i13n_key_tar_qa":false,"image_quality_override":true,"incremental_count":0,"incremental_delay":300,"incremental_history":100,"inline_filters":false,"inline_filters_article_min":10,"inline_filters_max":6,"inline_video":true,"item_template":"items","like":true,"limit_summary_height":true,"link_to_finance":false,"sticker_top":"94px","mms":false,"needtoknow_actions":true,"needtoknow_template":"filmstrip","needtoknow_storyline_min":4,"off_network_tab":false,"open_in_tab":false,"defaultDrawerAction":{"title":"ACTION_DEFAULT","description":"Content preferences","url":"https://settings.yahoo.com/interests"},"payoffDrawerActions":{"local":{"title":"ACTION_LOCAL","description":"More local news","urlPrefix":"https://news.yahoo.com/local/"},"finance":{"title":"ACTION_FINANCE","description":"Manage portfolios","url":"https://finance.yahoo.com/portfolios"},"sports":{"actionsEnabled":false,"attributionEnabled":false,"title":"ACTION_SPORTS","description":"Edit my teams","url":"https://sports.yahoo.com/myteams"}},"payoffs":"","payoffExpTime":24,"payoffFinanceArticleCount":2,"payoffInterval":6,"payoffInvalidTime":0.5,"payoffSportsLocalTeams":false,"payoffLocalArticleCount":3,"payoffLocalHeadliner":true,"payoffStartIndex":1,"pcsExclusions":false,"preview_enabled":false,"preview_lcp":true,"pubtime_format":"ONE_UNIT_ABBREVIATED","pubtime_maxage":3600,"ribbon_headers":false,"sfl":false,"sfl_get_started_string":"SFL_LINK","scrollbuffer":900,"scroll_node_selector":"body","show_tooltips":true,"smart_crop":true,"stateful_subtle_hint":true,"storyline_cap_image":2,"storyline_cap_noimage":3,"storyline_count":2,"storyline_elapsed_time":true,"storyline_elapsed_time_threshold":720,"storyline_enabled":true,"storyline_items_to_show":2,"storyline_multi_image":true,"storyline_multi_image_roundup":true,"storyline_newsy_disabled":false,"storyline_upsell_count":0,"storyline_upsell_image":"img:190x107|2|80","storyline_upsell_index":3,"stream_actions":true,"stream_actions_minimal":true,"stream_actions_reblog":true,"stream_actions_share":false,"stream_actions_share_panel":true,"stream_actions_likable":true,"stream_actions_tumblr":true,"stream_debug":false,"stream_payoff_actions":false,"stream_item_maxwidth":"none","stream_item_image_fallback":"https://s.yimg.com/dh/ap/default/151015/stream-gradient-230x130.png","stream_item_large_image_fallback":"https://s.yimg.com/dh/ap/default/150902/stream-gradient.png","stream_single_item_image_fallback":"https://s.yimg.com/dh/ap/default/150903/stream-gradient-single-3.png","summary":true,"summary_length":160,"template_label":"js-stream-dense","template_suffix":"","today_count":5,"today_snippet_count":5,"fc_related_minCount":2,"today_featured_image_tag":"pc:size=medium","tap_for_summary":false,"templates":{"all":{"batch_max":20,"gap":0,"max":170,"start":1,"batch":2,"total":2,"last":9},"featured":{"batch_max":20,"gap":0,"max":170,"portrait":false,"start":0,"batch":0,"total":0},"featured_ad":{"batch_max":20,"gap":0,"max":170,"start":0,"batch":0,"total":0},"inline_video":{"batch_max":20,"gap":0,"max":170,"start":0,"batch":2,"total":2,"last":9}},"thumbnail":true,"thumbnail_align":"right","thumbnail_hover":true,"thumbnail_size":160,"thumbnail_specs":{"featured":"img:190x107|2|80","featured_square":"img:190x190|2|80","inline_video":"img:190x107|2|80","items":"img:190x107|2|80","items_large":"img:190x190|2|80","storyline_square":"img:70x70|2|90","related":"img:165x120|2|80","featured_roundup":"img:702x274|1|95","roundup_wide":"img:170x80|2|80"},"toJpg":true,"truncate_summary":false,"tweet_action":true,"uis_inferred_finance":false,"ult_count":10,"ult_host":"hsrd.yahoo.com","ult_links":false,"use_ss_roundup_slot":true,"view_article_button":false,"viewer":true,"viewer_include_all":true,"viewer_off_network":false,"viewer_reblog":false,"viewer_action":true,"visit_state_threshold":1800000,"ntk_bypassA3c":0,"preview":0,"related_host":"","stream_payoff_v2":1,"stream_payoff_vizify":0,"stream_rc4":0,"show_jump_to_historical_items":0,"item_templates":["items"]},"weather":false,"woeid":12597132,"tz":"Europe/Paris","xcc":"fr"},"settings":{"size":9,"woeid":12597132},"models":["stream","items","related"]},"interest":{"yui_module":"td-applet-interest-model-v2","yui_class":"TD.Applet.InterestModel2"},"comments":{"yui_module":"td-applet-comments-model-v2","yui_class":"TD.Applet.CommentsModel2"},"related":{"yui_module":"td-applet-stream-related-model-atomic","yui_class":"TD.Applet.StreamRelatedModelAtomic"}},"views":{"main":{"yui_module":"td-applet-stream-mainview-v2","yui_class":"TD.Applet.StreamMainView2"},"drawer":{"yui_module":"stream-actiondrawer-v2"}},"templates":{"main":{"yui_module":"td-applet-stream-atomic-templates-main","template_name":"td-applet-stream-atomic-templates-main"},"related":{"yui_module":"td-applet-stream-atomic-templates-related","template_name":"td-applet-stream-atomic-templates-related"},"drawer":{"yui_module":"td-applet-stream-atomic-templates-drawer_desktop","template_name":"td-applet-stream-atomic-templates-drawer_desktop"},"removedItem":{"yui_module":"td-applet-stream-atomic-templates-removeditem","template_name":"td-applet-stream-atomic-templates-removeditem"},"drawerAction":{"yui_module":"td-applet-stream-atomic-templates-drawer_action","template_name":"td-applet-stream-atomic-templates-drawer_action"},"drawerSharePanel":{"yui_module":"td-applet-stream-atomic-templates-drawer_share","template_name":"td-applet-stream-atomic-templates-drawer_share"},"breakingNews":{"yui_module":"td-applet-stream-atomic-templates-breaking_news","template_name":"td-applet-stream-atomic-templates-breaking_news"},"flyoutDrawer":{"yui_module":"td-applet-stream-atomic-templates-drawer_flyout","template_name":"td-applet-stream-atomic-templates-drawer_flyout"},"items":{"yui_module":"td-applet-stream-atomic-templates-items","template_name":"td-applet-stream-atomic-templates-items"},"item_default":{"yui_module":"td-applet-stream-atomic-templates-item-default","template_name":"td-applet-stream-atomic-templates-item-default"},"item_default_clusters":{"yui_module":"td-applet-stream-atomic-templates-item-default_clusters","template_name":"td-applet-stream-atomic-templates-item-default_clusters"},"item_ad":{"yui_module":"td-applet-stream-atomic-templates-item-ad","template_name":"td-applet-stream-atomic-templates-item-ad"},"item_ad_dislike":{"yui_module":"td-applet-stream-atomic-templates-item-ad_dislike","template_name":"td-applet-stream-atomic-templates-item-ad_dislike"},"item_featured_ad":{"yui_module":"td-applet-stream-atomic-templates-item-featured_ad","template_name":"td-applet-stream-atomic-templates-item-featured_ad"},"item_featured":{"yui_module":"td-applet-stream-atomic-templates-item-featured","template_name":"td-applet-stream-atomic-templates-item-featured"},"item_inline_video":{"yui_module":"td-applet-stream-atomic-templates-item-inline_video","template_name":"td-applet-stream-atomic-templates-item-inline_video"}},"i18n":{"AD_FDB_HEADING":"Why don't you like this ad?","AD_FDB_TELL_US":"Tell us why","AD_FDB_THANKYOU":"Thank you for your feedback. We will remove this and make the changes needed.","AD_FDB1":"It is offensive to me","AD_FDB2":"It is not relevant to me","AD_FDB3":"I keep seeing this","AD_FDB4":"Something else","ABCLOGO":"ABC","ACTION_MORE_LIKE_THIS":"Got it! Tell us more about what you like:","ACTION_FEWER_LIKE_THIS":"Got it! Tell us more about what you dislike:","ACTION_DEFAULT":"Content preferences Â»","ACTION_SPORTS":"Edit my teams Â»","ACTION_FINANCE":"Manage portfolios Â»","ACTION_LOCAL":"More local news Â»","ACTION_STORY_REMOVED":"Story removed","ACTION_SEE_LESS":"Okay. You'll see fewer like this.","ACTION_SEE_MORE":"Great! You'll see more like this.","ACTION_SEE_NO_MORE":"This item has been removed from your stream.","ACTION_SIGN_IN_TO_DISLIKE":"{linkstart}Sign-in{linkend} and we'll show you less like this in the future.","ACTION_SIGN_IN_TO_DISLIKE_TUMBLR":"{linkstart}Sign-in{linkend} to dislike","ACTION_SIGN_IN_TO_LIKE":"{linkstart}Sign-in{linkend} and we'll show you more like this in the future.","ACTION_SIGN_IN_TO_LIKE_SIMPLE":"Sign in to like","ACTION_SIGN_IN_TO_LIKE_TUMBLR":"{linkstart}Sign-in{linkend} to like","ACTION_SIGN_IN_TO_PERSONALIZE":"{linkstart}Sign-in{linkend} to personalize!","ACTION_SIGN_IN_TO_SAVE":"{linkstart}Sign-in{linkend} to save this story to read later.","ACTION_SIGN_IN_TO_SAVE_SHORT":"Sign in to save","ACTION_TELL_LIKE":"Tell us more about what you like:","ACTION_TELL_DISLIKE":"Tell us more about what you dislike:","ACTION_MORE_LESS":"Show more or less of","ACTION_OPTIONS":"Options","ACTION_REMOVE":"Remove from my stream","ACTION_STORIES_LIKE_THIS":"Stories like this","ACTION_OPEN":"Open","ACTION_CLOSE":"Close","ADCHOICES":"AdChoices","ADVERTISEMENT":"Advertisement","ALL_ARTICLES":"All Articles","ALL_CELEBRITY":"All Celebrity News","ALL_FINANCE":"All Finance","ALL_MOVIES":"All Movies News","ALL_MUSIC":"All Music News","ALL_NEWS":"All News","ALL_SPORTS":"All Sports","ALL_STORIES":"All Stories","ALL_TRAVEL":"All Travel Ideas","ALL_TV":"All TV News","AROUND_THE_WEB":"Around The Web","ALSO_LIKE":"You may also like","ASTRO_GO_TO":"Go to Horoscopes Â»","ASTRO_NAME_AQUARIUS":"Aquarius","ASTRO_NAME_ARIES":"Aries","ASTRO_NAME_CANCER":"Cancer","ASTRO_NAME_CAPRICORN":"Capricorn","ASTRO_NAME_GEMINI":"Gemini","ASTRO_NAME_LEO":"Leo","ASTRO_NAME_LIBRA":"Libra","ASTRO_NAME_PISCES":"Pisces","ASTRO_NAME_SAGITTARIUS":"Sagittarius","ASTRO_NAME_SCORPIO":"Scorpio","ASTRO_NAME_TAURUS":"Taurus","ASTRO_NAME_VIRGO":"Virgo","ASTRO_TITLE":"Today's overview","AUCTION":"Action","AUTHOR_AT_PUBLISHER":"{author} at {publisher}","BREAKING_NEWS":"BREAKING NEWS","CNBCLOGO":"","COMMENT_ARTICLE_SIGN_IN":"Sign in to perform this action.","COMMENTS":"Comments","COMMENTS_ZERO":"Start the conversation","CONTENT_PREF":"Content preferences","DISLIKE":"Dislike","DONE":"Done","DYNAMIC_FILTERS":"You Might Like","EDITION_DEFAULT":"Need To Know","FEATURED_MOVIE":"Featured Movie","FLAGGED_COMMENT":"Flagged as inappropriate. Thanks for your feedback.","FOLLOW":"Follow","FOLLOW_GAME":"Follow Game","FOLLOWING":"Following {name}","FOLLOWING_STORY":"Following","FOLLOW_UPDATES":"Follow to receive updates on","FULL_HOROSCOPE":"Full Horoscope","GAME_PERIOD:baseball":"Innings","GAME_PERIOD:football":"Quarters","GAME_PERIOD_1":"1st","GAME_PERIOD_2":"2nd","GAME_PERIOD_3":"3rd","GAME_PERIOD_4":"4th","GAME_COVERAGE":"See Full Coverage","GAME_FINAL":"Final","GAME_RECAP":"Game Recap","GAME_TITLE":"{away_team} vs. {home_team}","HALFTIME":"Halftime","HOROSCOPE":"Horoscope","IN_THEATERS_NOW":"In Theaters Now","IN_THEATERS_TODAY":"In Theaters Today","INSTALL_APP":"Install now","IN_WATCHLIST":"In watchlist","IS_IN_FAVORITES":"is in your favorites","JUMP_TO_HISTORICAL_ITEMS":"Go to where you left off","LATEST_NEWS":"Latest News","LESS":"Less","LIKABLE_ARTICLE_SIGN_IN":"Sign in to save your preferences.","LIKE":"Like","LIKE_THIS_TOPIC":"Like this topic","LIVE":"Live","LIVE_GAME":"Live","LOAD_MORE":"Load more stories","LOCAL":"Local","LOCAL_BLOWING_SNOW":"Blowing Snow","LOCAL_BLUSTERY":"Blustery","LOCAL_CELSIUS":"Â°C","LOCAL_CLEAR":"Clear","LOCAL_CLOUDY":"Cloudy","LOCAL_COLD":"Cold","LOCAL_DRIZZLE":"Drizzle","LOCAL_DUST":"Dust","LOCAL_FAIR":"Fair","LOCAL_FOGGY":"Foggy","LOCAL_FAHRENHEIT":"Â°F","LOCAL_FREEZING_DRIZZLE":"Freezing Drizzle","LOCAL_FREEZING_RAIN":"Freezing Rain","LOCAL_GO_TO":"Go to Weather Â»","LOCAL_HAIL":"Hail","LOCAL_HAZE":"Haze","LOCAL_HEAVY_SNOW":"Heavy Snow","LOCAL_HOT":"Hot","LOCAL_HURRICANE":"Hurricane","LOCAL_ISOLATED_THUNDERSHOWERS":"Isolated Thundershowers","LOCAL_ISOLATED_THUNDERSTORMS":"Isolated Thunderstorms","LOCAL_LIGHT_SNOW_SHOWERS":"Light Snow Showers","LOCAL_MIXED_RAIN_AND_HAIL":"Mixed Rain and Hail","LOCAL_MIXED_RAIN_AND_SNOW":"Mixed Rain and Snow","LOCAL_MIXED_SNOW_AND_SLEET":"Mixed Snow and Sleet","LOCAL_MOSTLY_CLOUDY":"Mostly Cloudy","LOCAL_NEWS":"Local news","LOCAL_NO_STORIES":"Sorry, there are no news articles related to this location. Try setting your location to the nearest large city for more stories.","LOCAL_PARTLY_CLOUDY":"Partly Cloudy","LOCAL_SCATTERED_SHOWERS":"Scattered Snow Showers","LOCAL_SCATTERED_SNOW_SHOWERS":"Scattered Snow Showers","LOCAL_SCATTERED_THUNDERSTORMS":"Scattered Thunderstorms","LOCAL_SEVERE_THUNDERSTORMS":"Severe Thunderstorms","LOCAL_SHOWERS":"Showers","LOCAL_SLEET":"Sleet","LOCAL_SMOKY":"Smoky","LOCAL_SNOW":"Snow","LOCAL_SNOW_FLURRIES":"Snow Flurries","LOCAL_SNOW_SHOWERS":"Snow Showers","LOCAL_SUNNY":"Sunny","LOCAL_THUNDERSHOWERS":"Thundershowers","LOCAL_THUNDERSTORMS":"Thunderstorms","LOCAL_TITLE":"{city} News","LOCAL_TORNADO":"Tornado","LOCAL_TROPICAL_STORM":"Tropical Storm","LOCAL_UNKNOWN":"Unknown Conditions","LOCAL_WINDY":"Windy","MIDFIELD":"midfield","MLB_BALLS":"Balls","MLB_ERRORS":"Errors","MLB_HITS":"Hits","MLB_OUTS":"Outs","MLB_RUNS":"Runs","MLB_STRIKES":"Strikes","MORE":"More","MORE_FROM_ROUNDUP":"More from this Roundup","MORE_FROM_TOPIC":"More from {topic} news","MYQUOTES_ADD_MORE":"Add more investments to your {0}Portfolio{1} to stay up to date whenever you visit Yahoo Finance.","MYQUOTES_LOGIN":"{0}Sign-in{1} to see the latest news from the investments in your portfolio.","MYTEAMS_ADD_TEAMS":"{0}Add your favorite teams{1} to start getting news about them today.","MYTEAMS_LOGIN":"{0}Sign-in{1} to get news for your favorite teams.","MYTEAMS_NO_CONTENT":"We can't find recent news for your teams. {0}Edit your teams{1} or visit the {2}All Sports news{3}","MYSAVES":"My Saves","NEW_SINCE_YOUR_LAST_VISIT":"New since your last visit","NEW_STORIES":"New for you","NEW_STORIES_COUNT":"View {new_item_count} new {new_item_count, plural, one {story} other {stories}}","NEXT":"Next","NFL_PROGRESS":"on {field_side} {yard_line}","NO_STORIES_HEADER":"We couldn't find any new stories for you.","NO_STORIES_BODY":"Please check back later or {0}try again{1}","OFF":"OFF","OFFNETWORK":"Read this on {0}","ONBOARDING_CONFIRMATION_INITIAL":"Got it! Almost there","ONBOARDING_CONFIRMATION_ONE":"Got it! One more time","ONBOARDING_CONFIRMATION_ZERO":"Got it! Youâre done","ONBOARDING_TITLE1_FINAL":"Here are some stories based on your preferences","ONBOARDING_TITLE1_INITIAL":"Which would you rather read?","ONBOARDING_TITLE1_ONE":"Now which do you prefer?","ONBOARDING_TITLE1_ZERO":"Awesome! What is your final pick?","OPENS_TODAY":"Opens Today","PORTFOLIO":"Go to your portfolio","PORTFOLIO_GAINERS":"Gainers","PORTFOLIO_LOSERS":"Losers","PORTFOLIO_MARKET_CLOSED":"Market close","PLAY":"Play","PLAY_VIDEO":"Play Video","PLAYING":"playing","PLAYING_NEAR_YOU":"Playing near you","PREVIEW_GAME":"Preview Game","PREVIOUS":"Previous","PREVIOUSLY_VIEWED":"From your last visit","PROGRESS":"{possession_team} {down} & {to_go} at {field_side} {yard_line}","READ_MORE":"Read More","REBLOG":"Reblog on Tumblr","REMOVE":"Remove","SAVE":"Save","SCORE":"Score!","SCORE_ATTRIBUTION":"Click here to track the {display_name} &rarr;","SEE_ALL_STORIES":"See all stories Â»","SEE_DETAILS":"See details Â»","SEE_MORE_STORIES":"See more stories Â»","SEE_FULL_BOXSCORE":"See full boxscore Â»","SFL_HEADER":"Hi {0}, you have no saves. Here's how to get started:","SFL_LINK":"Get started now on the {0}Yahoo Homepage{1}.","SFL_LINK_ATT":"Get started now on the {0}att.net Homepage{1}.","SFL_LINK_FRONTIER":"Get started now on the {0}Frontier Yahoo Homepage{1}.","SFL_LINK_ROGERS":"Get started now on the {0}Rogers Yahoo Homepage{1}.","SFL_LINK_VERIZON":"Get started now on the {0}Verizon Yahoo Homepage{1}.","SFL_STEP_ONE":"Step 1","SFL_STEP_TWO":"Step 2","SFL_TITLE_ONE":"Click on {0} in the stream and on articles across Yahoo to save stories for later.","SFL_TITLE_TWO":"Access your saves from the profile menu {0} on desktop & tablet or menu {1} on your smartphone.","SHARE_THIS":"SHARE THIS","SHOPPING":"Shopping","SIGN_IN":"Sign in!","SIGN_IN_2":"Sign in","SIGN_IN_FOR_MORE":"Looking for past stories?","SIGN_IN_TO_SEE_MORE_TEAMS":" to save your preferences. You'll see more of this team next time you visit.","SLIDESHOW_COUNT":"({0} photos)","SLIDESHOW_PREVIEW_COUNT":"1 of {0}","SPONSORED":"Sponsored","STATUS":"{time} {period}","STORE":"Store","STORIES":"Stories:","STORIES_ABOUT":"Stories about:","STORYLINE_NEXT":"Next","STORYLINE_PREVIOUS":"Previous","STORYLINE_UPSELL":"Follow these developing stories to get updates when you come back to Yahoo:","SYNOPSIS":"Synopsis","TIME_AGO":"ago","TIME_DAY":"day","TIME_H":"h","TIME_M":"m","TITLE":"Recommended for You","TITLE_LOCAL":"{city} News","TO":"to","TWITTER_REPLY":"Reply","TWITTER_RETWEET":"Retweet","TWITTER_FAVORITE":"Favorite","UNFOLLOW":"Unfollow","UNDO":"Undo","UPCOMING_GAME":"Today at {game_time}","UP_OVER":"Up over","DOWN_OVER":"Down over","VIEW":"View","VIEW_ALL":"View All","VIEW_SLIDESHOW":"View Slideshow","VIEW_FULL_ARTICLE":"VIEW FULL ARTICLE","VIEW_FULL_QUOTE":"View full quote Â»","YAHOO_MOVIES":"Yahoo Movies","YAHOO_ORIGINALS":"Yahoo Originals","YCT:001000931":"Technology","YCUSTOM:COMMERCE":"Commerce","YCUSTOM:MYQUOTES":"My Quotes","YCUSTOM:MYTEAMS":"My Teams","YLABEL:autos":"Autos","YLABEL:business":"Business","YLABEL:beauty":"Beauty","YLABEL:celebrity":"Celebrity","YLABEL:diy":"DIY","YLABEL:entertainment":"entertainment","YLABEL:finance":"Finance","YLABEL:food":"Food","YLABEL:games":"Games","YLABEL:gma":"News","YLABEL:health":"Health","YLABEL:homes":"Homes","YLABEL:ivy":"Screen","YLABEL:lifestyle":"Lifestyle","YLABEL:makers":"Projects","YLABEL:movies":"Movies","YLABEL:music":"Music","YLABEL:news":"News","YLABEL:omg":"Celebrity","YLABEL:politics":"Politics","YLABEL:science":"science","YLABEL:shopping":"Shopping","YLABEL:sports":"Sports","YLABEL:style":"Style","YLABEL:tech":"Tech","YLABEL:technology":"Technology","YLABEL:travel":"Travel","YLABEL:trending":"Trending","YLABEL:tv":"TV","YLABEL:us":"US","YLABEL:world":"World","YPROP:FINANCE":"Business","YPROP:TOPSTORIES":"All Stories","YPROP:LOCAL":"Local","YPROP:NEWS":"News","YPROP:OMG":"Entertainment","YPROP:SCIENCE":"Science","YPROP:SPORTS":"Sports","YPROV:ABCNEWS":"News","YPROV:ap.org":"AP","YPROV:CNBC":"CNBC","YPROV:NBCSPORT":"NBC Sports","YPROV:reuters.com":"Reuters","YPROV:ROLLINGSTONES":"Rolling Stone","YPROV:us.sports.yahoo.com":"Experts","YTYPE:VIDEO":"Video","YPROP:STYLE":"Style","YMAG:food":"Food","YMAG:tech":"Tech","YMAG:travel":"Travel"},"i13n":{"pv":2,"pp":{"ccode_st":"mega_global_ranking_hlv2_up_based"}},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-trending-atomic":{"base":"https://s.yimg.com/os/mit/td/td-applet-trending-atomic-0.0.47/","root":"os/mit/td/td-applet-trending-atomic-0.0.47/","combine":true,"filter":"min","comboBase":"https://s.yimg.com/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_32209491"] = {"applet_type":"td-applet-trending-atomic","views":{"main":{"yui_module":"td-applet-trending-atomic-mainview","yui_class":"TD.Applet.TrendingAtomicMainView","config":{"clickableTitles":true,"rotationEnabled":true,"rotationInterval":12000,"giftsEnabled":false}}},"templates":{"main":{"yui_module":"td-applet-trending-atomic-templates-main","template_name":"td-applet-trending-atomic-templates-main"}},"i18n":{"GIFTS_TITLE":"Valentine's Day","TITLE":"Trending","TRENDING_NEWS":"Trending News","TRENDING_NOW":"Trending Now"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-weather-atomic":{"base":"/sy/os/mit/td/td-applet-weather-atomic-0.0.23/","root":"os/mit/td/td-applet-weather-atomic-0.0.23/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_63794"] = {"applet_type":"td-applet-weather-atomic","models":{"applet_model":{"yui_module":"td-applet-weather-atomic-appletmodel","yui_class":"TD.Applet.WeatherAppletModel","models":["weather"],"data":{"i13n":{"sec":"app-wea","bucket":"201"},"current":12597132,"favorite":null,"useplus":false},"user_settings":{"unit":"f","curloc":"12597132"},"config":{"moreinfo":{"link":"http://www.weather.com/?par=yahoonews","img":"http://l.yimg.com/dh/ap/default/130915/wcl1.gif"},"enableFav":false,"enableWeatherYQLP":true,"baseLink":"https://weather.yahoo.com","showWidgetLink":true,"urlGeneratorRules":{"enabled":false,"url_format_local":"","url_format_international":"","url_format_local_ajax":"","local_country_code":[]},"daysLimit":4,"fixed_layout":false}},"weather":{"yui_module":"td-applet-weather-atomic-model","yui_class":"TD.Applet.WeatherModel","data":{"weatherDataList":[{"location":{"woeid":"12597132","city":"Yvrac","state":"33","country":"FR"},"link":{"href":"https://weather.yahoo.com/fr/33/yvrac-12597132/"},"timezone":"CET","flickr":{"attribution":null,"image_assets":[{"width":"2048","height":"2048","type":"main","url":"https://s1.yimg.com/nn/lib/metro/rain_d.jpg"}]},"current":{"sunrise":"07:33:37","sunset":"18:54:00","condition":{"description":"Showers","code":"11","images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/rain_day_night.png"}}},"temp":{"unit":"f","now":"48"},"atmosphere":{"humidity":"93","wind_speed":"9","wind_chill":"44"}},"forecast":{"day":[{"label":"Today","day_of_week":"5","condition":{"description":"Showers","code":"12","images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/rain_day_night.png"}}},"temp":{"high":"50","low":"40","unit":"f"}},{"label":"Sat","day_of_week":"6","condition":{"description":" Scattered Thunderstorms","code":"39","images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/scattered_showers_day_night.png"}}},"temp":{"high":"51","low":"36","unit":"f"}},{"label":"Sun","day_of_week":"0","condition":{"description":"Partly Cloudy","code":"30","images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/partly_cloudy_day.png"}}},"temp":{"high":"51","low":"39","unit":"f"}},{"label":"Mon","day_of_week":"1","condition":{"description":"Showers","code":"11","images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/rain_day_night.png"}}},"temp":{"high":"47","low":"35","unit":"f"}}]},"woeid":"12597132","accordianState":"e"}],"currentLoc":{"woeid":"12597132","city":"Yvrac","state":"33","country":"FR"}}}},"views":{"main":{"yui_module":"td-applet-weather-atomic-mainview","yui_class":"TD.Applet.WeatherMainView"},"header":{"yui_module":"td-applet-weather-atomic-headerview","yui_class":"TD.Applet.WeatherHeaderView","config":{}}},"templates":{"main":{"yui_module":"td-applet-weather-atomic-templates-main","template_name":"td-applet-weather-atomic-templates-main"}},"i18n":{"CURRENT_LOCATION":"Current Location","WEATHER":"Weather","HIGH":"High","LOW":"Low","TODAY":"Today","TOMORROW":"Tomorrow","SUNDAY":"Sunday","MONDAY":"Monday","TUESDAY":"Tuesday","WEDNESDAY":"Wednesday","THURSDAY":"Thursday","FRIDAY":"Friday","SATURDAY":"Saturday","ABBR_SUNDAY":"Sun","ABBR_MONDAY":"Mon","ABBR_TUESDAY":"Tue","ABBR_WEDNESDAY":"Wed","ABBR_THURSDAY":"Thu","ABBR_FRIDAY":"Fri","ABBR_SATURDAY":"Sat","BLOWING_SNOW":"Blowing Snow","BLUSTERY":"Blustery","CLEAR":"Clear","CLOUDY":"Cloudy","COLD":"Cold","DRIZZLE":"Drizzle","DUST":"Dust","FAIR":"Fair","FREEZING_DRIZZLE":"Freezing Drizzle","FREEZING_RAIN":"Freezing Rain","FOGGY":"Foggy","FORECAST":"Forecast","FULL_FORECAST":"Full forecast","HAIL":"Hail","HAZE":"Haze","HEAVY_SNOW":"Heavy Snow","HOT":"Hot","HURRICANE":"Hurricane","ISOLATED_THUNDERSHOWERS":"Isolated Thundershowers","ISOLATED_THUNDERSTORMS":"Isolated Thunderstorms","LIGHT_SNOW_SHOWERS":"Light Snow Showers","MIXED_RAIN_AND_HAIL":"Mixed Rain and Hail","MIXED_RAIN_AND_SLEET":"Mixed Rain and Sleet","MIXED_RAIN_AND_SNOW":"Mixed Rain and Snow","MIXED_SNOW_AND_SLEET":"Mixed Snow and Sleet","MOSTLY_CLOUDY":"Mostly Cloudy","PARTLY_CLOUDY":"Partly Cloudy","SCATTERED_SHOWERS":"Scattered Showers","SCATTERED_SNOW_SHOWERS":"Scattered Snow Showers","SCATTERED_THUNDERSTORMS":" Scattered Thunderstorms","SEARCH_CITY":"Kindly search your city","SEVERE_THUNDERSTORMS":"Severe Thunderstorms","SHOWERS":"Showers","SLEET":"Sleet","SMOKY":"Smoky","SNOW":"Snow","SNOW_FLURRIES":"Snow Flurries","SNOW_SHOWERS":"Snow Showers","SUNNY":"Sunny","THUNDERSHOWERS":"Thundershowers","THUNDERSTORMS":"Thunderstorms","TORNADO":"Tornado","TROPICAL_STORM":"Tropical Storm","WINDY":"Windy","WEATHER_CHICLET_ERROR":"Visit {linkstart}Yahoo Weather{linkend} for a detailed forecast.","GOTO_WEATHER":"See more Â»","NO_WEATHER_MESSAGE":"Visit {linkstart}Yahoo Weather{linkend} for the latest forecast or {detectstart}click here{detectend} to select your precise location.","TEN_DAY":"10 day"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-activitylist-atomic":{"base":"/sy/os/mit/td/td-applet-activitylist-atomic-0.0.60/","root":"os/mit/td/td-applet-activitylist-atomic-0.0.60/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_50000171"] = {"applet_type":"td-applet-activitylist-atomic","templates":{"list":{"yui_module":"td-applet-activitylist-atomic-templates-list.tumblr","template_name":"td-applet-activitylist-atomic-templates-list.tumblr"}},"i18n":{"ACTION_POST":"Posted","FAVORITE":"favorite","FIND_OUT_WHY":"Find out why Â»","LIVE":"Live","REASON:BREAKING":"Breaking","REASON:COMMENTS":"{count, number, integer} Comments","REASON:EVERGREEN":"","REASON:EXCLUSIVE":"Exclusive","REASON:NEW":"New","REASON:NEW_ON":"New on {site}","REASON:ONLYONYAHOO":"Only on Yahoo","REASON:POPULARONSEARCH":"Popular on Search","REASON:READINGNOW":"{count, number, integer} Reading Now","REASON:SEARCHVISITS":"{count, number, integer} Search Visits","REASON:SHARES":"{count, number, integer} Shares","REASON:SOCIALVISITS":"{count, number, integer} Social Visits","REASON:VIDEOPLAYS":"{count, number, integer} Video Plays","REASON:VIEWS":"{count, number, integer} Views","REASON:VISITS":"{count, number, integer} Visits","REASON:WATCHINGNOW":"{count, number, integer} Watching Now","REASON:WATCHLIVE":"Watch Live","REPLY":"reply","RETWEET":"retweet","TIME_FORMAT":"h:mm A","TIME_FORMAT_SAME_DAY":"[Today]","TIME_FORMAT_NEXT_DAY":"[Tomorrow]","TIME_FORMAT_NEXT_WEEK":"dddd","TIME_FORMAT_ELSE":"MMM D","TITLE":"Only from Yahoo","TWEET_NAVIGATE":"navigate to tweet","VIA_TWITTER":"via Twitter","YLABEL:autos":"Autos","YLABEL:beauty":"Beauty","YLABEL:diy":"DIY","YLABEL:finance":"Finance","YLABEL:food":"Food","YLABEL:games":"Games","YLABEL:gma":"News","YLABEL:health":"Health","YLABEL:homes":"Homes","YLABEL:ivy":"Screen","YLABEL:movies":"Movies","YLABEL:music":"Music","YLABEL:news":"News","YLABEL:omg":"Celebrity","YLABEL:parenting":"Parenting","YLABEL:shopping":"Shopping","YLABEL:sports":"Sports","YLABEL:style":"Style","YLABEL:tech":"Tech","YLABEL:travel":"Travel","YLABEL:tv":"TV","YPROP:autos":"Yahoo Autos","YPROP:beauty":"Yahoo Beauty","YPROP:diy":"Yahoo DIY","YPROP:finance":"Yahoo Finance","YPROP:food":"Yahoo Food","YPROP:games":"Yahoo Games","YPROP:gma":"Yahoo News","YPROP:health":"Yahoo Health","YPROP:homes":"Yahoo Homes","YPROP:ivy":"Yahoo Screen","YPROP:makers":"Yahoo Makers","YPROP:movies":"Yahoo Movies","YPROP:music":"Yahoo Music","YPROP:news":"Yahoo News","YPROP:omg":"Yahoo Celebrity","YPROP:parenting":"Yahoo Parenting","YPROP:politics":"Yahoo Politics","YPROP:shopping":"Yahoo Shopping","YPROP:sports":"Yahoo Sports","YPROP:style":"Yahoo Style","YPROP:tech":"Yahoo Tech","YPROP:travel":"Yahoo Travel","YPROP:tv":"Yahoo TV","YPROP:yahoo":"Yahoo"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
    window.Af = window.Af || {};
    window.Af.config = window.Af.config || {};
    window.Af.config.spaceid = "2023538075";
    window.Af.context= {
        crumb : 'n403FhZV7ab',
        guid : '',
        mcCrumb: 'joLL4S9x8TO',
        ucsCrumb: '32Ay23p3R/W',
        device: 'desktop',
        rid : '4qbuvt9bdji05',
        default_page : 'p1',
        _p : 'p1',
        site : 'fp',
        lang : 'en-US',
        region : 'US',
        authed: 0,
        enable_dd : '',
        default_appletinit : 'viewport',
        locdrop_crumb: '', woeid: '12597132',
        ssl: 1,
        
        bucket: '201'
        
        
    };

    window.Af.config.transport = {
        crumbForGET: true,
        xhr: '/fpjs',
        consolidate: true,
        timeout: 6000
    };

    window.Af.config.onepush = {
        subscribeTimeout : 5000,
        subscribeMaxTries : 1,
        trackInitComet : true,
        trackLatency : true,
        latencySampleSize : 50,
        publicCometHost: 'https://comet.yahoo.com/comet',
        shutdown: false,
        trackShutdown: false
    };

    window.Af.config.beacon = {
        beaconUncaughtErr: true,
        sampleSizeUncaughtErr: 1,
        maxUncaughtErrCount: 5,
        beaconPageLoadPerf: false,
        sampleSize : 1,
        pathPrefix : '/_td_api/beacon',
        batch: false,
        batchInterval: 3
    };

    window.Af.config.pipe = {
        msg_throttle: 2000,
        err_maxstreak: 2
    };

    window.Af.config.td = {
        remote: '/_td_remote',
        xhr: '/_td_api'
    };

    
    YUI.namespace('Env.My.settings').context = {
        uh_js_file : '',
                videoAsyncEnabled: 0,
        videoplayerScriptElementId: 'videoplayerJs',
        videoplayerUrl: 'https://www.yahoo.com/sy/rx/builds/6.155.0.1453355318/en-us/videoplayer-nextgen-flash-min.js',
        videoAutoplay: 1,
        videoLooping: 0,
        videoForcedError: 0,
        videoFullscreen: 1,
        videoHtml5: 0,
        videoMinControls: 0,
        videoCmsEnv: 'prod',
        videoMustWatch: 1,
        videoMWSticky: 0,
        videoMute: 1,
        videoQosRate: 1,
        videoBuffering: 0,
        videoRelated: 0,
        videoYwaRate: 0,
        videoMaxLoops: 3,
        videoModeEnabled: 0,
        videoTiltbackModeEnabled: 0,
        videoSSButton: 0,
        videoPausescreen: 0,
        videoSingleVideo: 0,
        videoMaxInstances: 12,
        videoInitEvent: 'domready',
        videoExpName: 'advance',
        videoComscoreC4: 'US fp',
        videoplayerScrollThrottle: '200',
        
        themeCssEnabled : false,
        themeBgImageEnabled: false,
        transparentPixelImage: 'https://s.yimg.com/os/mit/media/m/base/images/transparent-95031.png',
        pageMessage: {
            type: '',
            msg: ''
        },
        servingBeaconUrl: 'https://bs.serving-sys.com/BurstingPipe/ActivityServer.bs',
        enablePerfBeacon: '0',
        test_id: '201',
        customizedEnabled: false,
        trendingNowOffScreen: 0
    };

    YUI.namespace('Env.Af.Perf').secondYUIUseStart = (new Date()).getTime();

    var yuiPreloadModules = ["type_advance_desktop_viewer","type_video_manager","type_idletimer","type_sdarotate","type_sda"];

    yuiPreloadModules = yuiPreloadModules.concat((function (appletTypes) {
        if (!appletTypes || appletTypes.length === 0) {
            return [];
        }
        var yui_modules = [],
            types = YMedia.Array.hash(appletTypes);
        YMedia.Object.each(window.Af.bootstrap, function (bootstrap, guid) {
            if (!types[bootstrap.applet_type]) {
                 return;
            }
            YMedia.Array.each(['models', 'views', 'templates'], function (type) {
                YMedia.Object.each(bootstrap[type], function (config, name) {
                    if (config.yui_module) {
                         yui_modules.push(config.yui_module);
                    }
                });
            });
        });
        return yui_modules;
    })([]));

    YMedia.use(yuiPreloadModules, function (Y, NAME) {
        YUI.namespace('Env.Af.Perf').secondYUIUseStop = (new Date()).getTime();
        var pageMessage = YUI.Env.My.settings.context.pageMessage,
        enablePerfBeacon = YUI.Env.My.settings.context.enablePerfBeacon,
        test_id = YUI.Env.My.settings.context.test_id,
        pausePerfBeacon = false,
        perfBeacon = {}
                ,
        rapidConfig = rapidPageConfig.rapidConfig,
        YWA_CF_MAP = rapidPageConfig.ywaCF,
        YWA_ACTION_MAP = rapidPageConfig.ywaActionMap,
        YWA_OUTCOME_MAP = rapidPageConfig.ywaOutcomeMap;
        if(YAHOO.i13n) {
            YAHOO.i13n.WEBWORKER_FILE = '/lib/metro/g/myy/rapidworker_1_2_0.0.3.js';
            YAHOO.i13n.SPACEID = '2023538075';
            YAHOO.i13n.TEST_ID = '201';
        }
        if (pageMessage.msg != '') {
            Y.Af.Message.show('body', {level: pageMessage.type, content: pageMessage.msg});
        }

        if (1 === 1) {
            Y.Lang.later(45000, null, function() {
                var failedModules  = Y.all("#myColumns .js-applet .App-loading");
                failedModules.each(function (module) {
                    module.setContent('Sorry! We are temporarily unable to load the content. Please refresh or try again later.');
                    module.removeClass('App-loading');
                    module.addClass('App-failed');
                });
            });
        }
        
        
        
        

        YUI.namespace('Env.Af.Perf').YMyAppCreateStart = (new Date()).getTime();
        YMedia.My.App = new Y.My.App(
            {                i13nConfig: {
                    rapid: rapidConfig,
                    rapidInstance: rapidPageConfig.rapidSingleInstance && YAHOO && YAHOO.i13n && YAHOO.i13n.rapidInstance || null,
                    ywaMaps: {
                        YWA_OUTCOME_MAP: YWA_OUTCOME_MAP,
                        YWA_CF_MAP: YWA_CF_MAP,
                        YWA_ACTION_MAP: YWA_ACTION_MAP
                    }
                },
                inlineViewer: 0,
                summaryView: 0,
                stickerTarget: "#SearchBar-Wrapper-Mini",
                magazineViewerEnabled: 1,
                viewerBlacklistDisable: 1,
                viewer: {
                    viewerAnimation: "slide",
                    pageAnimation: "zoom",
                    scrollTopAmount: 0,
                    highlanderMode: "mega-modal"
                }}
        );
        

        var firePerfBeacon = function(e) {
            var key, value, beaconNode, queryString = [];

            if (Y.Object.size(perfBeacon) < 1 || pausePerfBeacon) {
                return;
            }
            pausePerfBeacon = true;
            for (key in perfBeacon) {
                if (perfBeacon.hasOwnProperty(key)) {
                    queryString.push(key+'='+perfBeacon[key]);
                }
            }
            queryString.push('test='+test_id);
            beaconNode = Y.Node.create('<img src="myperf.php?'+queryString.join('&')+'" style="width:1px;height:1px;position:absolute;left:-900px;">');
            Y.one('body').append(beaconNode);
        };

        if (enablePerfBeacon == '1') {
            Y.one('window').once('scroll', firePerfBeacon);
            Y.Lang.later(10000, null, firePerfBeacon);

            YMedia.My.App.after('appletsinit', function (e) {
                if (pausePerfBeacon == true) {
                    return;
                }
                perfBeacon[e.applet.type] = (new Date()) - myYahoostartTime;
            });
        }
        YMedia.on('appletsinit', function (e) {
            
        });
        if (Y.IdleTimer) {
    Y.IdleTimer.start(parseInt(5400000,10));
    Y.IdleTimer.on(['idle','hidden'], function(e){
        if (e && e.type) {
            var isModalOpen = Y.one('html').hasClass('Reader-open'),
                location = Y.config.win.location,
                type = e.type,
                paramMap = {
                    idle: 'vi',
                    hidden: 'vh'
                },
                url = location.protocol + '//' + location.host,
                expires = new Date();

            if (paramMap[type]) {
                expires.setMinutes(expires.getMinutes() + parseInt(1,10));
                Y.Cookie.set('autorf', paramMap[type], {expires: expires, domain: location.hostname});

                if (isModalOpen) {
                    Y.config.win.location = url
                } else {
                    Y.config.win.location.replace(url);
                }
            }
        }
    });
}
    });
            });
     });
</script>
    <!-- bottom -->

    <!-- Comscore -->
            <!-- Begin comScore Tag -->
		<script>
		  var _comscore = _comscore || [];
		  _comscore.push({
		    c1: "2",
		    c2: "7241469",
		    c5: "2023538075",
		    c7: "https://www.yahoo.com/"
		  });
		  (function() {
		    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
		    s.src = "/sy/lq/lib/3pm/cs_0.2.js";
		    el.parentNode.insertBefore(s, el);
		  })();
		</script>
		<noscript>
		  <img src="https://sb.scorecardresearch.com/p?c1=2&c2=7241469&c7=https%3A%2F%2Fwww.yahoo.com%2F&c5=2023538075&cv=2.0&cj=1" />
		</noscript>
		<!-- End comScore Tag -->

    <!-- Nielsen -->
    

    <!-- Lighthouse  -->
    

    <!-- yaft -->
            <script type="text/javascript" src="/sy/zz/combo?/os/yaft/yaft-0.3.3.min.js&/os/yaft/yaft-plugin-aftnoad-0.1.3.min.js&os/yaft/yaft-plugin-harbeacon-0.1.3.min.js"> </script>        
        <script type="text/javascript">
            if (window.YAFT !== undefined) {
                var __yaftConfig = {
                    modules: ["UH","mega-uh","mega-topbar","applet_p_","ad-north-base","fea-","my-adsFPAD-base","my-adsLREC-base","my-adsMAST","my-adsTXTL","content-modal-","hl-ad-LREC-","modal-sidekick-","hl-ad-LREC-0","hl-ad-MON-0","hl-ad-MAST-0","hl-ad-LDRB-0","hl-ad-SPL2-0","hl-ad-SPL-0"], 
                    modulesExclude: ["UH-Search","UH-ColWrap","mega-uh-wrapper"],
                    canShowVisualReport: false,
                    useNormalizeCoverage: true,
                    generateHAR: false,
                    includeOnlyAft2: true,
                    useNativeStartRender : true,
                    modulesAft2Container: ["hl-viewer"],
                    maxWaitTime: 6000
                };
                __yaftConfig.plugins = [];                __yaftConfig.plugins.push({
                     name: 'aftnoad',
                     isPre: true,
                     config: {
                         useNormalizeCoverage: true,
                         adModules:["ad-north-base","my-adsFPAD-base","my-adsLREC-base","my-adsTL1","my-adsMAST","hl-ad-LREC-0","hl-ad-MAST-0","hl-ad-LDRB-0","hl-ad-SPL2-0","my-adsTXTL","hl-ad-SPL-0"]
                     }
                });
                            __yaftConfig.plugins.push({
                name: 'har',
                config: {
                    sec: 'har',
                    rapid: function() { if(YMedia && YMedia.My && YMedia.My.App && YMedia.My.App.getRapidTracker) {return YMedia.My.App.getRapidTracker(); } else {return null;}},
                    aftThreshold: 15000,
                    onlySlowest: false
                }
            });
            __yaftConfig.generateHAR = true;
                        if (window.LH && window.LH.isInitialized) {
            window.LH.tag('b',{val: '201'});               
            window.LH.tag("l", {val: false});
        }
                        window.aft2CB = function(data, error) {
        if (!error) {
            var aft2 = Math.round(data.aft);
            var vic2 = data.visuallyComplete;
            var srt2 = Math.round(data.startRender);

            if (window.LH && window.LH.isInitialized) {
                var lhRecord = function lhRecord(name, type, startTime, duration) {
                    window.LH.record(name, {
                        name: name, type: type, startTime: startTime, duration: duration
                    });
                };

                lhRecord('AFT', 'mark', aft2, 0);
                lhRecord('AFT2', 'mark', aft2, 0);
                lhRecord('VIC2', 'mark', vic2, 0);
                lhRecord('STR2', 'mark', srt2, 0);
                window.LH.beacon({
                    clearMarks:false,
                    clearMeasures: false,
                    clearCustomEntries: true,
                    clearTags: false
                });
            }

            var rapidInstance = (YMedia.My && YMedia.My.App && YMedia.My.App.getRapidTracker) ? YMedia.My.App.getRapidTracker() : null;
            if (rapidInstance) {
                var afterPageLoad = {
                    AFT: aft2,
                    AFT2: aft2,
                    STR: srt2,
                    VIC: vic2
                };
                var perfData = {
                    perf_commontime: {afterPageLoad: afterPageLoad}
                };
                rapidInstance.beaconPerformanceData(perfData);
            }
        }
    };
                window.YAFT.init(__yaftConfig, function(data, error) {
                    if (!error) {
                        try {
                                    if (window.LH && window.LH.isInitialized) {
            var results = [0], module = '', index = '', lhRecord = '';
            for (module in data.modulesReport) {
                for (index in data.modulesReport[module].resources) {
                    results.push(Math.round(data.modulesReport[module].resources[index].durationFromNStart));
                }
            }
            lhRecord = function (ma, va, custom) {
                var res = '';                                
                if (undefined === custom) {
                    if (va && !isNaN(va)) {
                        res = Math.round(va);
                    } else {
                        res = 0;
                    }
                } else {
                    res = va;
                }
                    window.LH.record(ma, { name: ma, type: 'mark', startTime: res, duration: 0 });
            };
                            
            lhRecord('AFT', data.aft);
            lhRecord('AFT1', data.aft);
            if (data.aftNoAd) {
                lhRecord('AFTNOAD', data.aftNoAd);
            }
            lhRecord('X_FB1', 42);
            lhRecord('X_FBN', 70);
            lhRecord('PLT_CACHED_REQs', data.httpRequests.onloadCached);
            lhRecord('COSTLY_RESOURCE', Math.max.apply(null, results));
            lhRecord('StartRender', data.startRender);
            if (screen) {
                lhRecord('SCR_H', screen.height);
                lhRecord('SCR_W', screen.width);
            }
            if (window.FPAD_rendered) {
                lhRecord('xAFT', data.aft);
                lhRecord('xPLT', data.pageLoadTime);
                if (window.rtAdStart && window.rtAdDone) {
                    lhRecord('ADSTART_FPAD', Math.round(rtAdStart));
                    lhRecord('ADEND_FPAD', Math.round(rtAdDone));
                }
            }

            // Track ad metrics
            if ( window._adLT) {
                for (var i = 0; i< _adLT.length; i++) {
                    lhRecord(_adLT[i][0], _adLT[i][1]);
                }
            }
            window.LH.beacon({ clearMarks: false, clearMeasures: false, clearCustomEntries: true, clearTags: false });   
        }
                                    var rapidInstance = (YMedia.My && YMedia.My.App && YMedia.My.App.getRapidTracker) ? YMedia.My.App.getRapidTracker() : null;
        if(rapidInstance) {
            var initialPageLoad = {
                AFT: Math.round(data.aft),
                AFT1: Math.round(data.aft),
                STR: data.startRender,
                VIC: data.visuallyComplete,
                PLT: data.pageLoadTime,
                DOMC: data.domElementsCount,
                HTTPC: data.httpRequests.count,
                CP: data.totalCoveragePercentage,
                NCP: data.normTotalCoveragePercentage
            };

            if(data.aftNoAd) {
                initialPageLoad.AFTNOAD = Math.round(data.aftNoAd);
            }              

            var customPerfData = {},
                pagePerfData = {},
                results = [];
            
            var yaftResults = [0], yaftModule = '', yaftIndex = '';
            // Find costly resource time
            for (yaftModule in data.modulesReport) {
                for (yaftIndex in data.modulesReport[yaftModule].resources) {
                    yaftResults.push(Math.round(data.modulesReport[yaftModule].resources[yaftIndex].durationFromNStart));
                }
            }
            pagePerfData['COSTLY_RESOURCE'] = Math.max.apply(null, yaftResults);
            pagePerfData['X_FB1'] = 42;
            pagePerfData['X_FBN'] = 70; 
   
            // Log ad perf data to rapid perf metric
            if (window.FPAD_rendered) {
                pagePerfData['xAFT'] = data.aft;
                pagePerfData['xPLT'] = data.pageLoadTime;
                if (window.rtAdStart && window.rtAdDone) {
                    pagePerfData['ADSTART_FPAD'] = Math.round(rtAdStart);
                    pagePerfData['ADEND_FPAD'] = Math.round(rtAdDone);
                }
            }
            
            // Track ad metrics
            if ( window._adLT) {
                for (var i = 0; i< _adLT.length; i++) {
                    pagePerfData[_adLT[i][0]]  = _adLT[i][1];
                }
            }
                        if(resourceTimingAssets) {
                for(i in resourceTimingAssets) {
                    if (resourceTimingAssets.hasOwnProperty(i)) {
                        resourceName = window.performance.getEntriesByName(resourceTimingAssets[i]);
                        if(resourceName && resourceName.length) {
                            var resourceFinish = resourceName[0].responseEnd;
                            pagePerfData[i] = Math.round(resourceFinish);
                        }
                    }
                }
            }

            customPerfData['utm'] = pagePerfData;
            var perfData = {
                perf_commontime: {initialPageLoad: initialPageLoad},
                perf_usertime: customPerfData
            };
            rapidInstance.beaconPerformanceData(perfData);
        }
                        } catch (e) {}                                                        
                    }
               });
            }
      </script>

    <!-- Via https/1.1 ir13.fp.ir2.yahoo.com[D992B319] (YahooTrafficServer), http/1.1 usproxy3.fp.bf1.yahoo.com[448EE3A5] (YahooTrafficServer) -->
    <!-- sid=2023538075 -->
    

    </body>
</html>
<!-- myproperty:myservice-us:0:Success -->
<!-- slw50.fp.bf1.yahoo.com compressed/chunked Fri Mar  4 17:38:13 UTC 2016 -->
