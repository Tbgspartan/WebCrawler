



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>

                    
                    
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "295-8141972-0156431";
                var ue_id = "10D2FC734YC0ANGQ6G3D";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        
    
        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />
    
    <meta property='fb:app_id' content='115109575169727' />
    
    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="10D2FC734YC0ANGQ6G3D" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1e-c3-2xl-i-9646dd77.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-4179705684._CB314325221_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-485555676._CB318510215_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-247421025._CB318510254_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-3978168775._CB318510288_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-1541130848._CB318510255_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
(function(){var d=function(o){return Object.prototype.toString.call(o)==="[object Array]";},g=function(q,p){var o;for(o=0;o<q.length;o++){if(o in q){p.call(null,q[o],o);}}},h=[],k,b,l=false,n=false,f=function(){var o=[],p=[],q={};g(h,function(s){var r="";g(s.dartsite.split("/"),function(t){if(t!==""){if(t in q){}else{q[t]=o.length;o.push(t);}r+="/"+q[t];}});p.push(r);});return{iu_parts:o,enc_prev_ius:p};},c=function(){var o=[];g(h,function(q){var p=[];g(q.sizes,function(r){p.push(r.join("x"));});o.push(p.join("|"));});return o;},m=function(){var o=[];g(h,function(p){o.push(a(p.targeting));});return o.join("|");},a=function(r,o){var q,p,s=[];for(q in r){p=[];for(j=0;j<r[q].length;j++){p.push(encodeURIComponent(r[q][j]));}if(o){s.push(q+"="+encodeURIComponent(p.join(",")));}else{s.push(q+"="+p.join(","));}}return s.join("&");},e=function(){var o=+new Date();if(n){return;}if(!this.readyState||"loaded"===this.readyState){n=true;if(l){imdbads.cmd.push(function(){for(i=0;i<h.length;i++){generic.monitoring.record_metric(h[i].name+".fail",csm.duration(o));}});}}};window.tinygpt={define_slot:function(r,q,o,p){h.push({dartsite:r.replace(/\/$/,""),sizes:q,name:o,targeting:p});},set_targeting:function(o){k=o;},callback:function(q){var r,p={},t,o,s=+new Date();l=false;for(r=0;r<h.length;r++){t=h[r].dartsite;o=h[r].name;if(q[r][t]){p[o]=q[r][t];}else{window.console&&console.error&&console.error("Unable to correlate GPT response for "+o);}}imdbads.cmd.push(function(){for(r=0;r<h.length;r++){ad_utils.slot_events.trigger(h[r].name,"request",{timestamp:b});ad_utils.slot_events.trigger(h[r].name,"tagdeliver",{timestamp:s});}ad_utils.gpt.handle_response(p);});},send:function(){var r=[],q=function(s,t){if(d(t)){t=t.join(",");}if(t){r.push(s+"="+encodeURIComponent(""+t));}},o,p;if(h.length===0){tinygpt.callback({});return;}q("gdfp_req","1");q("correlator",Math.floor(4503599627370496*Math.random()));q("output","json_html");q("callback","tinygpt.callback");q("impl","fifs");q("json_a","1");result=f();q("iu_parts",result.iu_parts);q("enc_prev_ius",result.enc_prev_ius);q("prev_iu_szs",c());q("prev_scp",m());q("cust_params",a(k,true));o=document.createElement("script");p=document.getElementsByTagName("script")[0];o.async=true;o.type="text/javascript";o.src="http://pubads.g.doubleclick.net/gampad/ads?"+r.join("&");o.id="tinygpt";o.onload=o.onerror=o.onreadystatechange=e;l=true;p.parentNode.insertBefore(o,p);b=+new Date();}};})();</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['b'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['1'],
'u': ['138504532178'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-3480509068._CB314075722_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"d22e485d156ad60ddcb1944f34786c96d495cdb1",
"2015-08-18T23%3A49%3A59GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 25801;
generic.days_to_midnight = 0.2986226975917816;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-1582251138._CB317085808_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'b']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm === undefined || window.generic === undefined || window.consoleLog === undefined)) {
                    if (console !== undefined && console.log !== undefined) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=138504532178;ord=138504532178?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=138504532178?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=0;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=138504532178?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
onsubmit="(new Image()).src='/rg/SEARCH-BOX/HEADER/images/b.gif?link=/find';"
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/imdbpicks/monthly-tv-picks/?ref_=nv_tp_tvpicks_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/search/title?count=100&title_type=feature,tv_series,tv_movie&explore=title_type,genres,year,countries&ref_=nv_ch_mm_1"
>Popular Movies & TV</a></li>
                        <li><a href="/chart/?ref_=nv_ch_cht_2"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_3"
>Oscar Winners</a></li>
                        <li><a href="/chart/top?ref_=nv_ch_250_4"
>Top 250 Movies</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_5"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/imdbpicks/monthly-tv-picks/?ref_=nv_tvv_picks_1"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_2"
>On Tonight</a></li>
                            <li><a href="/chart/toptv?ref_=nv_tvv_250_3"
>Top 250 TV</a></li>
                        <li><a href="/list/ls074093523/?ref_=nv_tvv_wn_4"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_5"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/academymuseum/?ref_=nv_sf_am_1"
>Academy Museum</a></li>
                        <li><a href="/imdbpicks/?ref_=nv_sf_pks_2"
>IMDb Picks</a></li>
                        <li><a href="/best-of/?ref_=nv_sf_bo_3"
>Best of 2014</a></li>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_4"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/whattowatch/?ref_=nv_sf_wtw_5"
>What to Watch</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=08-18&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_2"
>Sundance</a></li>
                        <li><a href="/sxsw/?ref_=nv_ev_sxsw_3"
>SXSW Film Festival</a></li>
                        <li><a href="/tribeca/?ref_=nv_ev_tri_4"
>Tribeca</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_5"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_6"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_7"
>Road to the Emmys</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_8"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_9"
>Toronto Film Festival</a></li>
                        <li><a href="/awards-central/?ref_=nv_ev_all_10"
>More Popular Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni58912740/?ref_=nv_nw_tn_1"
> Rosie OâDonnellâs Teenage Daughter Reported Missing
</a><br />
                        <span class="time">6 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni58913743/?ref_=nv_nw_tn_2"
> Carl Sagan Movie in the Works at Warner Bros.
</a><br />
                        <span class="time">2 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni58911678/?ref_=nv_nw_tn_3"
> Movie News: 'Alien 5' Might Wait Until After 'Prometheus 2'; First Look at Elizabeth Olsen in 'I Saw the Light'
</a><br />
                        <span class="time">4 hours ago</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/games/guess?ref_=nv_cm_qz_3"
>Quiz Game</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_4"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5> 
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB306318084_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB306318127_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB306318332_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0119698/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMjE4MzQ2NjMwM15BMl5BanBnXkFtZTcwOTUwMzU0NQ@@._V1._SY335_CR50,10,410,315_BR15_CT10_.jpg",
            titleYears : "1997",
            rank : 70,
                    headline : "Princess Mononoke"
    },
    nameAd : {
            clickThru : "/name/nm0413168/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTM3Njk0NjEwM15BMl5BanBnXkFtZTcwNzgzOTUxOQ@@._V1._SX250_CR0,0,250,315_BR-20_CT5_.jpg",
            rank : 218,
            headline : "Hugh Jackman"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB306318138_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYhhcXl-7h6NNK-7iLuTvDHroB_1Pbr-oWtLZ-D7fgeLt0r1t9I7kbyqHVAvJqP8FEwPEtsPWmS%0D%0A17-I6ylrw3baGIvml2K9l7P0lFLhyTPdtEgaIrR11SF09WpokuetZ0qq3q_gJopHYna7RodgTxtw%0D%0ApCyqhRL_dFXmmsXHxghp8YD2TWw78zTA7Ct2eDf5jl8g3rkEy8btg4H05GLnXjz1fQ%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYuYTlEquu9N8wGvMRMZZNNQEumKcNj5bdGj7h5WmfxlXfgA7xS4XB8G0_LZCPexVZtZGwkfS_5%0D%0AT098pHmceyqu54JjPPInjZ0dbpd_qJipjaYK5RHSx5X0brrnaYuhvuRM_9E2bt0Dpxk1qtI4SLNo%0D%0AB6SNWqdniDzaHPzZRm4CcUSt7mBelhZiW_Oj7INRBpyV%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYj7DWpoDsLESGo0tgbtk-UpFsm1SWwDrJjHuGfFZclyhLT7Yaw4-nKdcda8B1FX-jMHCbvbD3N%0D%0Aejto6APwrvDWbuGfawio9I1ckpG_NPpqEEJHyt07BPLzGuox16PJJFjia76FnqVfZFfTA4GPDwKK%0D%0AyWjx-S7xVV9zLUGMzVJwdTWJpLFowUUzgh1w8V4LIMlZDDH-KoU6CsyxQytIdleKmQ%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=138504532178;ord=138504532178?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>
                    <div id="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=138504532178;ord=138504532178?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	

                    
                    

<div id="content-2-wide">
    <div id="main">
                    
        <div class="heroWidget">
        <span class="ab_widget"
        >
            <div class="ab_hero">
<div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3739136793?ref_=hm_hp_i_1" data-video="vi3739136793" data-source="bylist" data-id="ls002397163" data-rid="10D2FC734YC0ANGQ6G3D" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="Told from Igor's perspective, we see the troubled young assistant's dark origins, his redemptive friendship with the young medical student Viktor Von Frankenstein, and become eyewitnesses to the emergence of how Frankenstein became the man - and the legend - we know today." alt="Told from Igor's perspective, we see the troubled young assistant's dark origins, his redemptive friendship with the young medical student Viktor Von Frankenstein, and become eyewitnesses to the emergence of how Frankenstein became the man - and the legend - we know today." src="http://ia.media-imdb.com/images/M/MV5BMTc2Mjk0MTM0NF5BMl5BanBnXkFtZTgwNjgyOTg1NjE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc2Mjk0MTM0NF5BMl5BanBnXkFtZTgwNjgyOTg1NjE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Told from Igor's perspective, we see the troubled young assistant's dark origins, his redemptive friendship with the young medical student Viktor Von Frankenstein, and become eyewitnesses to the emergence of how Frankenstein became the man - and the legend - we know today." title="Told from Igor's perspective, we see the troubled young assistant's dark origins, his redemptive friendship with the young medical student Viktor Von Frankenstein, and become eyewitnesses to the emergence of how Frankenstein became the man - and the legend - we know today." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Told from Igor's perspective, we see the troubled young assistant's dark origins, his redemptive friendship with the young medical student Viktor Von Frankenstein, and become eyewitnesses to the emergence of how Frankenstein became the man - and the legend - we know today." title="Told from Igor's perspective, we see the troubled young assistant's dark origins, his redemptive friendship with the young medical student Viktor Von Frankenstein, and become eyewitnesses to the emergence of how Frankenstein became the man - and the legend - we know today." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt1976009/?ref_=hm_hp_cap_pri_1" > Victor Frankenstein </a> </div> </div> <div class="secondary ellipsis"> Trailer #1 </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2262545177?ref_=hm_hp_i_2" data-video="vi2262545177" data-source="bylist" data-id="ls074935490" data-rid="10D2FC734YC0ANGQ6G3D" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="The cast of &quot;Scream&quot; discusses the show's success, season 2 surprises, and which TV shows they'd like to do crossover episodes with." alt="The cast of &quot;Scream&quot; discusses the show's success, season 2 surprises, and which TV shows they'd like to do crossover episodes with." src="http://ia.media-imdb.com/images/M/MV5BMjQwODYzNDI2MF5BMl5BanBnXkFtZTgwMDY0Njg1NjE@._V1_SY298_CR29,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQwODYzNDI2MF5BMl5BanBnXkFtZTgwMDY0Njg1NjE@._V1_SY298_CR29,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="The cast of &quot;Scream&quot; discusses the show's success, season 2 surprises, and which TV shows they'd like to do crossover episodes with." title="The cast of &quot;Scream&quot; discusses the show's success, season 2 surprises, and which TV shows they'd like to do crossover episodes with." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="The cast of &quot;Scream&quot; discusses the show's success, season 2 surprises, and which TV shows they'd like to do crossover episodes with." title="The cast of &quot;Scream&quot; discusses the show's success, season 2 surprises, and which TV shows they'd like to do crossover episodes with." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt4940624/?ref_=hm_hp_cap_pri_2" > Teen Choice Awards </a> </div> </div> <div class="secondary ellipsis"> "Scream" Cast on Season 2 </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3034428185?ref_=hm_hp_i_3" data-video="vi3034428185" data-source="bylist" data-id="ls002908747" data-rid="10D2FC734YC0ANGQ6G3D" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="The story of matinee idol Tab Hunter from teenage stable boy to closeted Hollywood star of the 1950s." alt="The story of matinee idol Tab Hunter from teenage stable boy to closeted Hollywood star of the 1950s." src="http://ia.media-imdb.com/images/M/MV5BMTkyNTA2MzM4Ml5BMl5BanBnXkFtZTgwNjE3ODE5NDE@._V1_SX201_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTkyNTA2MzM4Ml5BMl5BanBnXkFtZTgwNjE3ODE5NDE@._V1_SX201_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="The story of matinee idol Tab Hunter from teenage stable boy to closeted Hollywood star of the 1950s." title="The story of matinee idol Tab Hunter from teenage stable boy to closeted Hollywood star of the 1950s." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="The story of matinee idol Tab Hunter from teenage stable boy to closeted Hollywood star of the 1950s." title="The story of matinee idol Tab Hunter from teenage stable boy to closeted Hollywood star of the 1950s." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt1533089/?ref_=hm_hp_cap_pri_3" > Tab Hunter Confidential </a> </div> </div> <div class="secondary ellipsis"> Official Trailer </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/trailers?ref_=hm_hp_sm" class="position_bottom supplemental" > Browse more trailers </a> </p>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="http://www.imdb.com/family-entertainment-guide/?ref_=hm_feg_hm_hd" > <h3>Family Entertainment Guide: Something for Everyone</h3> </a> </span> </span> <p class="blurb">Check out IMDb's comprehensive <a href="http://www.imdb.com/family-entertainment-guide/?ref_=hm_feg_hm_lk1">Family Entertainment Guide</a>, brought to you by Honda Pilot, where you'll get recommendations for movies and TV series for every age and every viewing platform. From the latest releases to childhood classics to streaming TV shows, there's definitely something for the entire family.</p> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/family-entertainment-guide/most-awesome-family-movies-to-stream?ref_=hm_feg_hm_i_1" > <img itemprop="image" class="pri_image" title="Harry Potter and the Half-Blood Prince (2009)" alt="Harry Potter and the Half-Blood Prince (2009)" src="http://ia.media-imdb.com/images/M/MV5BMTY0NTU3NzM1MV5BMl5BanBnXkFtZTcwNTIyODMyNw@@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY0NTU3NzM1MV5BMl5BanBnXkFtZTcwNTIyODMyNw@@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="http://www.imdb.com/family-entertainment-guide/most-awesome-family-movies-to-stream?ref_=hm_feg_hm_cap_pri_1" > Top 20 Most Awesome Streaming Family Movies </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/family-entertainment-guide/galleries/favorite-family-films-rm3398290432?ref_=hm_feg_hm_i_2" > <img itemprop="image" class="pri_image" title="E.T. - O Extraterrestre (1982)" alt="E.T. - O Extraterrestre (1982)" src="http://ia.media-imdb.com/images/M/MV5BNjU5MTg4MDUyNF5BMl5BanBnXkFtZTgwNTA3MTg5MTE@._V1_SY201_CR49,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjU5MTg4MDUyNF5BMl5BanBnXkFtZTgwNTA3MTg5MTE@._V1_SY201_CR49,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="http://www.imdb.com/family-entertainment-guide/galleries/favorite-family-films-rm3398290432?ref_=hm_feg_hm_cap_pri_2" > Photos We Love From Our Favorite Family Films </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="http://www.imdb.com/family-entertainment-guide/best-live-action-kids-movies/?ref_=hm_feg_hm_i_3" > <img itemprop="image" class="pri_image" title="The Goonies (1985)" alt="The Goonies (1985)" src="http://ia.media-imdb.com/images/M/MV5BMTI5Njg1NDIxMF5BMl5BanBnXkFtZTcwNzQ4NDIwNA@@._V1_SY201_CR48,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTI5Njg1NDIxMF5BMl5BanBnXkFtZTcwNzQ4NDIwNA@@._V1_SY201_CR48,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="http://www.imdb.com/family-entertainment-guide/best-live-action-kids-movies/?ref_=hm_feg_hm_cap_pri_3" > 15 Best Live Action Kids' Movies </a> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="http://www.imdb.com/family-entertainment-guide/?ref_=hm_feg_hm_sm" class="position_bottom supplemental" > Visit our Family Entertainment Guide </a> </p>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <h3>IMDb on the Scene: 2015 Teen Choice Awards</h3> </span> </span> <p class="blurb">We hit the blue carpet at the 2015 Teen Choice Awards to talk to the casts of <a href="http://www.imdb.com/title/tt3566726/?ref_=nv_sr_1">"Jane the Virgin,"</a> <a href="http://www.imdb.com/title/tt1578873/?ref_=nv_sr_1">"Pretty Little Liars,"</a> <a href="http://www.imdb.com/title/tt3107288/?ref_=nv_sr_1">"The Flash,"</a> and many more. The stars dropped juicy TV scoop and shared stories about their first IMDb credits and some hilarious secrets to the onscreen kiss. <a href="http://m.imdb.com/list/ls074935490/?ref_=nv_sr_1">Watch all our coverage here.</a></p> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2480648985?ref_=hm_acd_tc_hm_i_1" data-video="vi2480648985" data-source="bylist" data-id="ls074935490" data-rid="10D2FC734YC0ANGQ6G3D" data-type="playlist" class="video-colorbox" data-refsuffix="hm_acd_tc_hm" data-ref="hm_acd_tc_hm_i_1"> <img itemprop="image" class="pri_image" title="IMDb on the Scene (2015-)" alt="IMDb on the Scene (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMTU4MjAxMDc3Nl5BMl5BanBnXkFtZTgwNzU3Njc1NjE@._V1._CR410,0,700,1080_SY172_SX116_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU4MjAxMDc3Nl5BMl5BanBnXkFtZTgwNzU3Njc1NjE@._V1._CR410,0,700,1080_SY172_SX116_AL_UY344_UX232_AL_.jpg" /> <img alt="IMDb on the Scene (2015-)" title="IMDb on the Scene (2015-)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="IMDb on the Scene (2015-)" title="IMDb on the Scene (2015-)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/awards-central/video/teen-choice?ref_=hm_acd_tc_hm_cap_pri_1" > Victoria Justice </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2312876825?ref_=hm_acd_tc_hm_i_2" data-video="vi2312876825" data-source="bylist" data-id="ls074935490" data-rid="10D2FC734YC0ANGQ6G3D" data-type="playlist" class="video-colorbox" data-refsuffix="hm_acd_tc_hm" data-ref="hm_acd_tc_hm_i_2"> <img itemprop="image" class="pri_image" title="IMDb on the Scene (2015-)" alt="IMDb on the Scene (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMzQwMDc5MzQzN15BMl5BanBnXkFtZTgwMDQ4NTc1NjE@._V1._CR380,0,700,1080_SY172_SX116_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMzQwMDc5MzQzN15BMl5BanBnXkFtZTgwMDQ4NTc1NjE@._V1._CR380,0,700,1080_SY172_SX116_AL_UY344_UX232_AL_.jpg" /> <img alt="IMDb on the Scene (2015-)" title="IMDb on the Scene (2015-)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="IMDb on the Scene (2015-)" title="IMDb on the Scene (2015-)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/awards-central/video/teen-choice?ref_=hm_acd_tc_hm_cap_pri_2" > Candice Patton </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2145104665?ref_=hm_acd_tc_hm_i_3" data-video="vi2145104665" data-source="bylist" data-id="ls074935490" data-rid="10D2FC734YC0ANGQ6G3D" data-type="playlist" class="video-colorbox" data-refsuffix="hm_acd_tc_hm" data-ref="hm_acd_tc_hm_i_3"> <img itemprop="image" class="pri_image" title="IMDb on the Scene (2015-)" alt="IMDb on the Scene (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMTQ4MTk0NzAzMl5BMl5BanBnXkFtZTgwOTQ1NTc1NjE@._V1._CR380,0,700,1080_SY172_SX116_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ4MTk0NzAzMl5BMl5BanBnXkFtZTgwOTQ1NTc1NjE@._V1._CR380,0,700,1080_SY172_SX116_AL_UY344_UX232_AL_.jpg" /> <img alt="IMDb on the Scene (2015-)" title="IMDb on the Scene (2015-)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="IMDb on the Scene (2015-)" title="IMDb on the Scene (2015-)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/awards-central/video/teen-choice?ref_=hm_acd_tc_hm_cap_pri_3" > Justin Baldoni </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2329654041?ref_=hm_acd_tc_hm_i_4" data-video="vi2329654041" data-source="bylist" data-id="ls074935490" data-rid="10D2FC734YC0ANGQ6G3D" data-type="playlist" class="video-colorbox" data-refsuffix="hm_acd_tc_hm" data-ref="hm_acd_tc_hm_i_4"> <img itemprop="image" class="pri_image" title="IMDb on the Scene (2015-)" alt="IMDb on the Scene (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMjA0MjUyNjU3MF5BMl5BanBnXkFtZTgwMDQzNjc1NjE@._V1._CR400,0,700,1080_SY172_SX116_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA0MjUyNjU3MF5BMl5BanBnXkFtZTgwMDQzNjc1NjE@._V1._CR400,0,700,1080_SY172_SX116_AL_UY344_UX232_AL_.jpg" /> <img alt="IMDb on the Scene (2015-)" title="IMDb on the Scene (2015-)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="IMDb on the Scene (2015-)" title="IMDb on the Scene (2015-)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/awards-central/video/teen-choice?ref_=hm_acd_tc_hm_cap_pri_4" > Nominees on Favorite Movies </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi1725674265?ref_=hm_acd_tc_hm_i_5" data-video="vi1725674265" data-source="bylist" data-id="ls074935490" data-rid="10D2FC734YC0ANGQ6G3D" data-type="playlist" class="video-colorbox" data-refsuffix="hm_acd_tc_hm" data-ref="hm_acd_tc_hm_i_5"> <img itemprop="image" class="pri_image" title="IMDb on the Scene (2015-)" alt="IMDb on the Scene (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMjE5NTkxNTc0MV5BMl5BanBnXkFtZTgwOTMzNjc1NjE@._V1._CR380,0,700,1080_SY172_SX116_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE5NTkxNTc0MV5BMl5BanBnXkFtZTgwOTMzNjc1NjE@._V1._CR380,0,700,1080_SY172_SX116_AL_UY344_UX232_AL_.jpg" /> <img alt="IMDb on the Scene (2015-)" title="IMDb on the Scene (2015-)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="IMDb on the Scene (2015-)" title="IMDb on the Scene (2015-)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/awards-central/video/teen-choice?ref_=hm_acd_tc_hm_cap_pri_5" > Beau Mirchoff </a> </div> </div> </div> </div> </div> </div> </div>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58912740?ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BNTcwNzk5NjkwNF5BMl5BanBnXkFtZTcwNzQ2ODQwMg@@._V1_SY150_CR8,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58912740?ref_=hm_nw_tp1"
class="headlines" >Rosie OâDonnellâs Teenage Daughter Reported Missing</a>
    <div class="infobar">
            <span class="text-muted">6 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?ref_=hm_nw_tp1_src"
>The Wrap</a></span>
    </div>
                                </div>
<p>Chelsea OâDonnell, the 17-year-old daughter of comedian Rosie OâDonnell, has gone missing, according to OâDonnellâs website. The younger OâDonnell was reported missing in Nyack, New York. Police have been searching for her in the Rockland County area since Sunday. Chelsea (pictured above at right) ...                                        <span class="nobr"><a href="/news/ni58912740?ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58913743?ref_=hm_nw_tp2"
class="headlines" >Carl Sagan Movie in the Works at Warner Bros.</a>
    <div class="infobar">
            <span class="text-muted">2 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?ref_=hm_nw_tp2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58911678?ref_=hm_nw_tp3"
class="headlines" >Movie News: 'Alien 5' Might Wait Until After 'Prometheus 2'; First Look at Elizabeth Olsen in 'I Saw the Light'</a>
    <div class="infobar">
            <span class="text-muted">4 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0005146?ref_=hm_nw_tp3_src"
>Movies.com</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58912664?ref_=hm_nw_tp4"
class="headlines" >John Boyega Joins âThe Circleâ From James Ponsoldt</a>
    <div class="infobar">
            <span class="text-muted">6 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000151?ref_=hm_nw_tp4_src"
>Slash Film</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58913106?ref_=hm_nw_tp5"
class="headlines" >Anne Hathaway to Star in Limited Series âThe Ambassadorâs Wifeâ From eOne, Mark Gordon Co.</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?ref_=hm_nw_tp5_src"
>Variety - TV News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58911678?ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BODY2MTQ0MjY4MV5BMl5BanBnXkFtZTcwODcxNjE0OQ@@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58911678?ref_=hm_nw_mv1"
class="headlines" >Movie News: 'Alien 5' Might Wait Until After 'Prometheus 2'; First Look at Elizabeth Olsen in 'I Saw the Light'</a>
    <div class="infobar">
            <span class="text-muted">4 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0005146?ref_=hm_nw_mv1_src"
>Movies.com</a></span>
    </div>
                                </div>
<p><a href="/title/tt4462546?ref_=hm_nw_mv1_lk1">Alien 5</a>: We've been hearing about <a href="/name/nm0088955?ref_=hm_nw_mv1_lk2">Neill Blomkamp</a>'s <a href="/title/tt0078748?ref_=hm_nw_mv1_lk3">Alien</a> sequel for months, with a tentative release date set for 2017. Then came a report that <a href="/name/nm0000631?ref_=hm_nw_mv1_lk4">Ridley Scott</a> would begin filming a sequel to 2012's <a href="/title/tt1446714?ref_=hm_nw_mv1_lk5">Prometheus</a> early next year. Now yet another report suggests that <a href="/title/tt2316204?ref_=hm_nw_mv1_lk6">Prometheus 2</a> will hit theaters in 2017...                                        <span class="nobr"><a href="/news/ni58911678?ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58913119?ref_=hm_nw_mv2"
class="headlines" >Ving Rhames Joins Growing Comedy Cast Of âBastardsâ</a>
    <div class="infobar">
            <span class="text-muted">4 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?ref_=hm_nw_mv2_src"
>Deadline</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58913009?ref_=hm_nw_mv3"
class="headlines" >Suicide Squad Director David Ayer Teases Arkham Asylum</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0012032?ref_=hm_nw_mv3_src"
>Obsessed with Film</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58913113?ref_=hm_nw_mv4"
class="headlines" >Desplat, JÃ³hannsson, Zimmer Are Double Nominees for World Soundtrack Awards</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?ref_=hm_nw_mv4_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58913184?ref_=hm_nw_mv5"
class="headlines" >âAbout Timeâ Actress Lydia Wilson Joins âStar Trek Beyondâ (Exclusive)</a>
    <div class="infobar">
            <span class="text-muted">4 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?ref_=hm_nw_mv5_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58913106?ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BNjQ5MTAxMDc5OF5BMl5BanBnXkFtZTcwOTI0OTE4OA@@._V1_SY150_CR1,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58913106?ref_=hm_nw_tv1"
class="headlines" >Anne Hathaway to Star in Limited Series âThe Ambassadorâs Wifeâ From eOne, Mark Gordon Co.</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052792?ref_=hm_nw_tv1_src"
>Variety - TV News</a></span>
    </div>
                                </div>
<p><a href="/name/nm0004266?ref_=hm_nw_tv1_lk1">Anne Hathaway</a> has signed on to star in the limited series âThe Ambassadorâs Wife,â from <a href="/company/co0212327?ref_=hm_nw_tv1_lk2">Entertainment One</a> and the <a href="/name/nm0330428?ref_=hm_nw_tv1_lk3">Mark Gordon</a> Co.Project is based on the newly released novel by author Jennifer Steil about a British woman who is kidnapped in Yemen, setting off an ordeal for her and her ambassador ...                                        <span class="nobr"><a href="/news/ni58913106?ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58913114?ref_=hm_nw_tv2"
class="headlines" >Interview: Hannah Fidell Talks â6 Years,â Bringing âA Teacherâ To HBO, And More</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0011867?ref_=hm_nw_tv2_src"
>The Playlist</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58913098?ref_=hm_nw_tv3"
class="headlines" >Once Upon a Time Alum Patrick Fischler Finds Family on Fox's Grandfathered</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?ref_=hm_nw_tv3_src"
>TVLine.com</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58912948?ref_=hm_nw_tv4"
class="headlines" >Debut ABC Drama âThe Catchâ Will Get New Showrunner As Creative Heads Quit Over âDifferencesâ</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004913?ref_=hm_nw_tv4_src"
>Deadline TV</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58913116?ref_=hm_nw_tv5"
class="headlines" >The Good Wife Creators Break Silence on 'Kalicia'-gate, Insist 'There Was No Attempt to Dupe Viewers'</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?ref_=hm_nw_tv5_src"
>TVLine.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni58913843?ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTY0NDk0OTk5OF5BMl5BanBnXkFtZTgwOTg2MTMzNTE@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni58913843?ref_=hm_nw_cel1"
class="headlines" >Jared Fogle the Subway Guy Expected to Plead Guilty to Child Pornography Charges (Reports)</a>
    <div class="infobar">
            <span class="text-muted">1 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?ref_=hm_nw_cel1_src"
>The Wrap</a></span>
    </div>
                                </div>
<p>Former Subway spokesman <a href="/name/nm1815678?ref_=hm_nw_cel1_lk1">Jared Fogle</a> is expected to plead guilty to charges in connection with a child pornography investigation as part of a plea deal, according to multiple media reports. Indianapolisâ local Fox station reported Tuesday that Fogle will take a plea deal and the details will be ...                                        <span class="nobr"><a href="/news/ni58913843?ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58912740?ref_=hm_nw_cel2"
class="headlines" >Rosie OâDonnellâs Teenage Daughter Reported Missing</a>
    <div class="infobar">
            <span class="text-muted">6 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?ref_=hm_nw_cel2_src"
>The Wrap</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58913050?ref_=hm_nw_cel3"
class="headlines" >Cara Delevingne Quits Modeling, Says the Industry Made Her "Hate" Her Body</a>
    <div class="infobar">
            <span class="text-muted">5 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004699?ref_=hm_nw_cel3_src"
>TooFab</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni58914097?ref_=hm_nw_cel4"
class="headlines" >Amber Rose and Other Models Victimized in Reported Internet Prostitution ScamâGet the Details</a>
    <div class="infobar">
            <span class="text-muted">22 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?ref_=hm_nw_cel4_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni58914098?ref_=hm_nw_cel5"
class="headlines" >Here Is the Most Popular Emoji in Every State (Guess Who Loves the Eggplant?!)</a>
    <div class="infobar">
            <span class="text-muted">25 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?ref_=hm_nw_cel5_src"
>E! Online</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <h3>IMDb Snapshot</h3> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1744825856/rg4106918656?ref_=hm_snp_i_1" > <img itemprop="image" class="pri_image" title="Hannibal (2013-)" alt="Hannibal (2013-)" src="http://ia.media-imdb.com/images/M/MV5BMjQwMjk4MDc0OV5BMl5BanBnXkFtZTgwMzI4MDk1NjE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQwMjk4MDc0OV5BMl5BanBnXkFtZTgwMzI4MDk1NjE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm1744825856/rg4106918656?ref_=hm_snp_cap_pri_1" > "Hannibal" - The Final Episodes </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3690982912/rg1528338944?ref_=hm_snp_i_2" > <img itemprop="image" class="pri_image" title="Hitman: Agent 47 (2015)" alt="Hitman: Agent 47 (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTk4MjE1NDIyNV5BMl5BanBnXkFtZTgwNDIzMTk1NjE@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk4MjE1NDIyNV5BMl5BanBnXkFtZTgwNDIzMTk1NjE@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm3690982912/rg1528338944?ref_=hm_snp_cap_pri_2" > Latest Posters </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm4175490560/rg784964352?ref_=hm_snp_i_3" > <img itemprop="image" class="pri_image" title="Colony (2015-)" alt="Colony (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMTA3NzkwOTY2NTVeQTJeQWpwZ15BbWU4MDk4MTQ4NTYx._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTA3NzkwOTY2NTVeQTJeQWpwZ15BbWU4MDk4MTQ4NTYx._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm4175490560/rg784964352?ref_=hm_snp_cap_pri_3" > Latest Stills </a> </div> </div> </div> </div> </div> </div> </div>    </div>

        </span>
        </div>
                    
        <div class="article">
        <span class="ab_widget"
        >
                <div class="ab_borntoday">
<span class="ninja_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=8-18&ref_=hm_brn_hd" > <h3>Born Today</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0001570?ref_=hm_brn_i_1" > <img itemprop="image" class="pri_image" title="Edward Norton" alt="Edward Norton" src="http://ia.media-imdb.com/images/M/MV5BMTYwNjQ5MTI1NF5BMl5BanBnXkFtZTcwMzU5MTI2Mw@@._V1_SY172_CR9,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTYwNjQ5MTI1NF5BMl5BanBnXkFtZTcwMzU5MTI2Mw@@._V1_SY172_CR9,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0001570?ref_=hm_brn_cap_pri_lk1_1">Edward Norton</a> (46) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0000225?ref_=hm_brn_i_2" > <img itemprop="image" class="pri_image" title="Christian Slater" alt="Christian Slater" src="http://ia.media-imdb.com/images/M/MV5BMTI2MDc2Nzc0Ml5BMl5BanBnXkFtZTYwODIyODE2._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTI2MDc2Nzc0Ml5BMl5BanBnXkFtZTYwODIyODE2._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0000225?ref_=hm_brn_cap_pri_lk1_2">Christian Slater</a> (46) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0000664?ref_=hm_brn_i_3" > <img itemprop="image" class="pri_image" title="Patrick Swayze" alt="Patrick Swayze" src="http://ia.media-imdb.com/images/M/MV5BNDM2NjI0MjYyMV5BMl5BanBnXkFtZTYwMjY1ODMz._V1_SY172_CR2,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNDM2NjI0MjYyMV5BMl5BanBnXkFtZTYwMjY1ODMz._V1_SY172_CR2,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0000664?ref_=hm_brn_cap_pri_lk1_3">Patrick Swayze</a> (1952-2009) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0000602?ref_=hm_brn_i_4" > <img itemprop="image" class="pri_image" title="Robert Redford" alt="Robert Redford" src="http://ia.media-imdb.com/images/M/MV5BMTk1Nzc5MzQyMV5BMl5BanBnXkFtZTcwNjQ5OTA0Mg@@._V1_SY172_CR4,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTk1Nzc5MzQyMV5BMl5BanBnXkFtZTcwNjQ5OTA0Mg@@._V1_SY172_CR4,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0000602?ref_=hm_brn_cap_pri_lk1_4">Robert Redford</a> (79) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1676221?ref_=hm_brn_i_5" > <img itemprop="image" class="pri_image" title="Andy Samberg" alt="Andy Samberg" src="http://ia.media-imdb.com/images/M/MV5BMTM1ODAwMDc2NF5BMl5BanBnXkFtZTcwMjUzNzg3MQ@@._V1_SY172_CR8,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTM1ODAwMDc2NF5BMl5BanBnXkFtZTcwMjUzNzg3MQ@@._V1_SY172_CR8,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1676221?ref_=hm_brn_cap_pri_lk1_5">Andy Samberg</a> (37) </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/search/name?refine=birth_monthday&birth_monthday=8-18&ref_=hm_brn_sm" class="position_bottom supplemental" > See all birthdays </a> </p>        </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget"
        >
            <div class="ab_trivia">
<span class="ninja_header"> <span class="oneline"> <a href="/title/tt4016454/trivia?item=tr2492626&ref_=hm_trv_hd" > <h3>Did You Know?</h3> </a> </span> </span> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt4016454?ref_=hm_trv_i_1" > <img itemprop="image" class="pri_image" title="Supergirl (2015-)" alt="Supergirl (2015-)" src="http://ia.media-imdb.com/images/M/MV5BMTg5MDI3OTI3M15BMl5BanBnXkFtZTgwNzk0MDI1NjE@._V1_SX89_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg5MDI3OTI3M15BMl5BanBnXkFtZTgwNzk0MDI1NjE@._V1_SX89_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding"></div><div class="ninja_image" > <strong class="text-large"><a href="/title/tt4016454?ref_=hm_trv_lk1">Supergirl</a></strong> <p class="blurb"><a href="/name/nm2652716?ref_=hm_trv_lk1">Grant Gustin</a> who plays CW's <a href="/title/tt3107288?ref_=hm_trv_lk2">The Flash</a> (2014) guest starred on many episodes of Fox's <a href="/title/tt1327801?ref_=hm_trv_lk3">Glee</a> (2009) alongside <a href="/name/nm2552034?ref_=hm_trv_lk4">Melissa Benoist</a>.</p></div> </div> </div> <p class="seemore"> <a href="/title/tt4016454/trivia?item=tr2492626&ref_=hm_trv_sm" class="position_bottom supplemental" > See more trivia </a> </p>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
        <div class="article">
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;J_6IK_XGEbU&quot;}"
        >
            <div class="ab_poll poll">
<span class="ninja_header"> <span class="oneline"> <a href="/poll/J_6IK_XGEbU/?ref_=hm_poll_hd" > <h3>Poll: Best Movie to Have Been the IMDb Top 250 #1</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?ref_=hm_poll_mp">More Polls</a></h4> </span> </span> <p class="blurb">Only five movies have ever topped the IMDb Top 250. Which of them is the best? <a href="http://www.imdb.com/board/bd0000088/nest/230445462/?ref_=hm_poll_lk1">Discuss here</a> after voting.</p> <p class="seemore"> <a href="/poll/J_6IK_XGEbU/?ref_=hm_poll_sm" class="position_blurb supplemental" > Vote now </a> </p> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/J_6IK_XGEbU/?ref_=hm_poll_i_1" > <img itemprop="image" class="pri_image" title="The Shawshank Redemption (1994)" alt="The Shawshank Redemption (1994)" src="http://ia.media-imdb.com/images/M/MV5BODU4MjU4NjIwNl5BMl5BanBnXkFtZTgwMDU2MjEyMDE@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BODU4MjU4NjIwNl5BMl5BanBnXkFtZTgwMDU2MjEyMDE@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/J_6IK_XGEbU/?ref_=hm_poll_i_2" > <img itemprop="image" class="pri_image" title="The Godfather (1972)" alt="The Godfather (1972)" src="http://ia.media-imdb.com/images/M/MV5BMjEyMjcyNDI4MF5BMl5BanBnXkFtZTcwMDA5Mzg3OA@@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjEyMjcyNDI4MF5BMl5BanBnXkFtZTcwMDA5Mzg3OA@@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/J_6IK_XGEbU/?ref_=hm_poll_i_3" > <img itemprop="image" class="pri_image" title="Batman: O Cavaleiro das Trevas (2008)" alt="Batman: O Cavaleiro das Trevas (2008)" src="http://ia.media-imdb.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/J_6IK_XGEbU/?ref_=hm_poll_i_4" > <img itemprop="image" class="pri_image" title="Star Wars: Episode IV - A New Hope (1977)" alt="Star Wars: Episode IV - A New Hope (1977)" src="http://ia.media-imdb.com/images/M/MV5BMTU4NTczODkwM15BMl5BanBnXkFtZTcwMzEyMTIyMw@@._V1_SX140_CR0,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU4NTczODkwM15BMl5BanBnXkFtZTcwMzEyMTIyMw@@._V1_SX140_CR0,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:18.4%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/J_6IK_XGEbU/?ref_=hm_poll_i_5" > <img itemprop="image" class="pri_image" title="The Lord of the Rings: The Fellowship of the Ring (2001)" alt="The Lord of the Rings: The Fellowship of the Ring (2001)" src="http://ia.media-imdb.com/images/M/MV5BNTEyMjAwMDU1OV5BMl5BanBnXkFtZTcwNDQyNTkxMw@@._V1_SY207_CR1,0,140,207_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNTEyMjAwMDU1OV5BMl5BanBnXkFtZTcwNDQyNTkxMw@@._V1_SY207_CR1,0,140,207_AL_UY414_UX280_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
    </div>
    <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-2513524760._CB315223700_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=138504532178;ord=138504532178?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript><a href="http://ad.doubleclick.net/N4215/jump/imdb2.consumer.homepage/;tile=1;sz=300x250,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=138504532178?" target="_blank"><img src="http://ad.doubleclick.net/N4215/ad/imdb2.consumer.homepage/;tile=1;sz=300x250,11x1;p=tr;fv=1;ab=b;bpx=1;c=0;s=3075;s=32;ord=138504532178?" border="0" alt="advertisement" /></a></noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	
                    
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/movies-in-theaters/?ref_=hm_otw_hd" > <h3>Opening This Week</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2752772"></div> <div class="title"> <a href="/title/tt2752772?ref_=hm_otw_t0"> Sinister 2 </a> <span class="secondary-text"></span> </div> <div class="action"> <a href="/showtimes/title/tt2752772?ref_=hm_otw_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3316948"></div> <div class="title"> <a href="/title/tt3316948?ref_=hm_otw_t1"> American Ultra </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2679042"></div> <div class="title"> <a href="/title/tt2679042?ref_=hm_otw_t2"> Hitman: Agent 47 </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4270516"></div> <div class="title"> <a href="/title/tt4270516?ref_=hm_otw_t3"> Grandma </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3799372"></div> <div class="title"> <a href="/title/tt3799372?ref_=hm_otw_t4"> 6 Years </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3704416"></div> <div class="title"> <a href="/title/tt3704416?ref_=hm_otw_t5"> Digging for Fire </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1767372"></div> <div class="title"> <a href="/title/tt1767372?ref_=hm_otw_t6"> Broadway Therapy </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3062976"></div> <div class="title"> <a href="/title/tt3062976?ref_=hm_otw_t7"> Learning to Drive </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <div><a href="/movies-in-theaters/?ref_=hm_otw_sm" ><p class="seemore position_bottom">See more opening this week</p></a></div>
        </span>
        </div>
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
            <div class="ab_ninja showtimesMessagingWidget">
<p class="blurb"><a href="/showtimes?ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p>    </div>

        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/chart/?ref_=hm_cht_hd" > <h3>Now Playing (Box Office)</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1398426"></div> <div class="title"> <a href="/title/tt1398426?ref_=hm_cht_t0"> Straight Outta Compton </a> <span class="secondary-text">$60.2M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2381249"></div> <div class="title"> <a href="/title/tt2381249?ref_=hm_cht_t1"> Mission: Impossible - Rogue Nation </a> <span class="secondary-text">$17.2M</span> </div> <div class="action"> <a href="/showtimes/title/tt2381249?ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1638355"></div> <div class="title"> <a href="/title/tt1638355?ref_=hm_cht_t2"> The Man from U.N.C.L.E. </a> <span class="secondary-text">$13.4M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1502712"></div> <div class="title"> <a href="/title/tt1502712?ref_=hm_cht_t3"> Fantastic Four </a> <span class="secondary-text">$8.2M</span> </div> <div class="action"> <a href="/showtimes/title/tt1502712?ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4178092"></div> <div class="title"> <a href="/title/tt4178092?ref_=hm_cht_t4"> The Gift </a> <span class="secondary-text">$6.5M</span> </div> </div> </div> </div> </div> <div><a href="/chart/?ref_=hm_cht_sm" ><p class="seemore position_bottom">See more box office results</p></a></div>
        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
        <span class="widget_header"> <a href="/movies-coming-soon/?ref_=hm_cs_hd" > <h3>Coming Soon</h3> </a> </span> <div class="widget_content"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3742378"></div> <div class="title"> <a href="/title/tt3742378?ref_=hm_cs_t0"> Une seconde mÃ¨re </a> <span class="secondary-text"></span> </div> <div class="action"> <a href="/showtimes/title/tt3742378?ref_=hm_cs_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3787590"></div> <div class="title"> <a href="/title/tt3787590?ref_=hm_cs_t1"> We Are Your Friends </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1781922"></div> <div class="title"> <a href="/title/tt1781922?ref_=hm_cs_t2"> No Escape </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1598642"></div> <div class="title"> <a href="/title/tt1598642?ref_=hm_cs_t3"> Z for Zachariah </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3832914"></div> <div class="title"> <a href="/title/tt3832914?ref_=hm_cs_t4"> War Room </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <div><a href="/movies-coming-soon/?ref_=hm_cs_sm" ><p class="seemore position_bottom">See more coming soon</p></a></div>
        </span>
        </div>
                    
                    
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
            <div class="ab_twitter">
<span class="widget_header"> <h3>Follow Us On Twitter</h3> </span> <div class="widget_content"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3776329187._CB305760180_.html#{"screen-name":"IMDb","height":"500px","width":"300px"}'></iframe> </div> </div>    </div>

        </span>
        </div>
                    
                    
        <div class="aux-content-widget-2">
        <span class="ab_widget"
        >
            <div class="ab_facebook">
<span class="widget_header"> <h3>Find Us On Facebook</h3> </span> <div class="widget_content"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

        </span>
        </div>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
        <div class="aux-content-widget-2 sticky-widget">
        <span class="ab_widget"
        >
            <div class="ab_ninja ">
<span class="ninja_header"> <span class="oneline"> <a href="/imdbpicks/?ref_=hm_pks_hd" > <h3>IMDb Picks: August</h3> </a> </span> </span> <p class="blurb">Visit our IMDb Picks section to see our recommendations of movies and TV shows coming out in August.</p> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/?ref_=hm_pks_i_1" > <img itemprop="image" class="pri_image" title="Fear the Walking Dead (2015-)" alt="Fear the Walking Dead (2015-)" src="http://ia.media-imdb.com/images/M/MV5BNzkxNTY4MzUyM15BMl5BanBnXkFtZTgwODQwNzUzNjE@._V1_SY525_CR0,0,700,525_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNzkxNTY4MzUyM15BMl5BanBnXkFtZTgwODQwNzUzNjE@._V1_SY525_CR0,0,700,525_AL_UY1050_UX1400_AL_.jpg" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/imdbpicks/?ref_=hm_pks_cap_pri_1" > Fear the Walking Dead </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"> <a href="/imdbpicks/?ref_=hm_pks_sm" class="position_bottom supplemental" > Visit the IMDb Picks section </a> </p>    </div>

        </span>
        </div>
    </div>
</div>

                    
                    


                   <br class="clear" />
                </div>





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYjY3O6urXu218LWJLVmTsAGDJ001-rjOCHeI9pfdTZA7sooO7D29bBjwd8l9HsjzHkzgUmLvvJ%0D%0AA4nzbjwm5WYrAzCJBXUld4XsCdzz586lwzTq6fUNq1NXN_lYITcenR4RWeTDv-zB2RictAxl-beQ%0D%0AZ7d6jdNKTFskI644BF5rpgg8-sdFn5ro0JKc0E3_S6SpQQjfwuqrgpb1ksLILuJ5AQ99gKnL4cwJ%0D%0A3ZvT_kf_lEg%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYtHuZmjzdRYZfOMrg5FvLC0-5ibJbOwgTyIwqilAuYKJoE-mbcWytgyLAgS2NGFvyfALzs-jK8%0D%0AR8ekpvKqzilmwn1NK_5LI449ihuZxHrKqJIOCAux8IW4B3pauklMen4ECsq7skyBrAi4MqUuKRJq%0D%0A0uLRg-dNi5wWl3__6fgc51VlUwAQxrZKFKHleA7uhLi9FGy1H2xusEP_hA7W-55_2A%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYp3qAuJ2eD1A24CQ14HJGQjh9rAwNh4vX91-SXpSrC5OajYpSusmuCon-zIwAKyPa20iOL8LYw%0D%0AHLOSkWlhxeYIVlZhcV_KOndzNoOlTJ8ZOukIb8udSG3WKfHSRP6pOLqnFO9ncVrEeTAgTwudnFrv%0D%0ANWPCrbvTlCVWNabmmS1NH8owBV-Vbb3rP_yckG3lXSHb8N2_FdZVVP5O5xHv2_dulzdgzMxp4pdx%0D%0Ar9JWmFcUYxp9SAGISPuF8rZZRQXd_ld6tUBngCLYDx_Fl8EdsAcg6iuLbEMdaNhlyhvsJEzZWysH%0D%0APqa57VZXTvEHNXI_fsoZeV7Z4-oeCkzKVeFh9CmB7yjx0T4Ugsq28oeYq12phpsx-mKlec4PDQIU%0D%0AHofBNYOAr4ZxUNh1MtoRhKbRX4e5yw%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYrH6DlHSftUVybera9WLKuDofFS4-i5kR7ufBdEN0YPqxeC98o97fL4PVxPu-ufByP73zJZ-z9%0D%0ABgCZ0KcoETJdgxKHJnECezMHAdGkdkEteO1H9yL5_q-fLkWqvzIRTnL7NmPVJyNUgc6KgBG5AHSw%0D%0AKJhAHBlKBiuYSMl5VZcgZTInCbubxbwb9Bv7fyTweU1PAPrMVbquI7R2lNJ81eUpLQ%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYvMOCvSoLkVi55ss-hG5XbJ9b--hCydBFQAdhA5oSUd7OEjs1EJt7XcGL8n7MlDL3olYOq9raX%0D%0AU8rouWz-BWgEerVL9TPcGRxc-Kw_iUsaX2J-kiM-0tTFJVLALUmpDg2W9-6ZCSUOvd1ZJ9aDosoZ%0D%0A9tepZPv--1PRR9-IYCUNdCLeV05PaItD8XecdgJtysD1%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYtkrnFEYh3rTuu01z9x30M8zxdiWESXFtjHPF9755VEt9L0UziAsvy6Fhg1Q3eNrLYtJTZUZxt%0D%0AGGW9bnMGmGghKdg03RstCJyL3PvWhxyecUxxxNg3rjMPhzPyVzeH4s4S8wvMHcbnJrxdjMCoAHI_%0D%0AjFyndsQA-uqogRN6hbCKisOyIOU6CkvwI6tFH81HwwC75jRAd_0ftgI6Hz3o_yAn3Q%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top 250</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Top Movies</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li>    <a href="/offsite/?page-action=ft_ad&token=BCYjLVvy3iIjOrCucVbsdf5e3M3FvWmBX3jHYDeD02VDJp5TDWgbFBfMXCOLd9-DqBZN8HOEBoTz%0D%0AgSx6N88riXkiS0ArsqhVr_OhkigFPGzP3A-bUlGtSp6xryRwtwLrapyDJp_4mrHHyL9I15WDSn0B%0D%0AfN9IlT3TV3BwI7sWJELZFfidfKN4XXZhJlGLLjfrfdhhS9Ej7pLZkxPeINakoHSqoRj-fsLzVrcb%0D%0AtwTOk2MRAoosoVSujcAX6uKYoBLXQPACMp3i0UHd21h8zp0EzPOo1A%0D%0A&ref_=ft_ad"
itemprop='url'>Advertising</a>
</li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYhZrJ731KFYPxjWz9bYs29Msd76C99twwvw7LDBuzNkdUPcU7TTCWHwmL4mnpiaq2bNU-Q57-e%0D%0A0zmS-eTcZQeGvuERAOq6QccdshoW3CMl9fBhCyaZLznzamYzNFBEAm_9VWiTlTkVBgfLEK9QiRa8%0D%0Ac0ofPFwHAsIdvzjDt_aV239VUI2yfgMzAR_6viAPWfY9NvFfHKdJbijw7B7WspT0ow%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYgJrziC8bj9QgMCmLxjcQxDYgAGvQIifm5z-Q1wqPeIavq88pF9Ki-8MgK-0ya0v6ZaW5PPHhd%0D%0AHGJJUnDUhjCd5glOOJ25k67BPsGaKWwPSUGiIT9HDEpk2oMThFhVGBzH-y7Fn_8pSZYKw_II4789%0D%0AhulaLa5zq16tp9j69lFI8TQUc4Tv8MtCSKAvqF1fFORm%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYuxPIoqSZrxIURSVCRUHH58pGU26Gqajz9Lutxw-mVad42xN741aOKbE5-kddjIbztp69xLLX7%0D%0ADAAReosqyLevNUiYSKG0tI2LySO-nUYN_Dcb8Co9OSGF9O3q05bWYM7KG6JnP-_IkQdA-it-cj9v%0D%0AJeaQLcXzqUKGKvjYeDVT2SjwoW1K3N6_7Iz2FxDbBg0qM5iILnafXw9HxjzUcuaJBmKbTjVhEGfw%0D%0A8k2v8ljcICyFeOpjHBUzBnWVOYX7A1lA%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/show_article?conditions';"
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2015
                <a
onclick="(new Image()).src='/rg/help/footer/images/b.gif?link=/help/';"
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYsQGjx51INoTt7LEIK1HFwM3boBqDq45BGMOAyc3pJ-i2-vooM9nCFhYEke6OOIGLcwDgmuCLN%0D%0AJg0eJ7dz9v76mA6KKUcnqBgt2qnP0YCnWuI8Zv4d_FviXlLp3iWbeDb3KwoFmYnm_fOK7F0xBCP1%0D%0A4N6TfNnvIzfs38AnQQdU9JpMky7ZFUezpe3S5v8WzYsV6mHf5bRCOy9b00Jzik4IGLLHw_6KCHAG%0D%0AWImDTp-jJuo%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Instant Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYjcrNvxC6kKG_K7lKxYB-i4RevguooZc39_oKRKULcfg5jZFrfeLk-zGdgEo80abZ887KBIstH%0D%0Aq0KAyDB37RkSdUIb3oJfXswRStr_FXOJrKOdGD80_iAW8sGVTG3yj_gVAo-7Nh8Wu4KDZPTrVysX%0D%0AnAM-46UscjbXEskQnlPtrHy6yR19uFQwOdo_Pw0LFejhrmtvNkRz5Cp9T5ygSNvC_WOFj1WXlA4O%0D%0AxSdAILjA14s%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Instant Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYh-xchu35UBRNPV5RD1hoEz1yb15r9CwZD20J2vO4n29I8bxmiChJmJcTyyn-Ww__3-JAuYJeO%0D%0AfJJ7BBMHPJFGcraWSIxKEDkMCnlc6RuPkwa8SpDmKHJQvlT4Rm6ylI7GjxTimiCd9rR8T9Afw-aK%0D%0AAIl2LrMYiiz3R4rK5feAFS03DBlhPIgt1wc9BkYC5p_1pzJ6ghNaEX6NDJ9DgsKC6HInp2fS_dsc%0D%0AvCrgEomhMbo%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYmUBR8zsi8UOO2MKzV8euhYC8x_l7aQ-a9AdQsJUBGWz1GXBieNzgR_BurOAxkGQIvIUqnN733%0D%0AT7Fc2FX2fTWwjqOWlPgqBL4RmZ-UA77-GKTXsQZQOi7SnvOHdN8MzghGOWWFaLqHWQkWhEuzRAa1%0D%0ASDNHF45ZEyrYiZPXAooJluf6Lqh59HX0089X3m1S2Wo8Ia-JBBrpp3ddGmO17oKkYCw-0nGQCZar%0D%0AlzMXrcNveIk%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYihHJuXwmVuAKh9sVu6ibmDENNP6l47WWEKNYj5YNnq-GV7D2J4wCBC1NZkkDlZTL7moxVndsc%0D%0APS_D70y6o1pJamXXZmKjdqLFPb3uPRLlCVk8mXDBicOjzESSi2LV7-y9b6Z-AGFzJh1K5xs5qta-%0D%0AWCWpPVYAma3L_xJ-2JMfGppwkLwMaYizKl-vLhVz8QUw4zM9giD4Y1EOA7usDyiQomrHharYf9RV%0D%0AfifHpBK-tUc%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYhyXFckKk6z07DAGq1Qn84Y1kXTWDPFXtQd48en-6a-DsrQUpGfVtxeJ3iJ_Mt7KZFBdCJJkWD%0D%0A9DxA1YkWvp3cLy_M0HIKIwLCxFBfJBiz5rWhtYGcG0kv_tuLDuBXeU7740dwg4e1JjKc7L0Pmp9-%0D%0AJYngcuW9phirK-ipJ_Y2IM5pFvBhUbdczVYLgRd8KptNOkQEswFSdXloQq_9YrDStMSoCxeuhcX8%0D%0A_Fch-JEKIdZpokuo8hT-RQJQB7F0tvzC%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYsHBdRlkDvv4R3qMzBytemOGIqpEHgP0xwcfiC8CourPNZJiTYAq4hu9rUC2TSTdiuBfh8QRU5%0D%0AJQAhJZzg77vLnePVzpwKgoMlQB49D70RjqyFRt3vEOFkE8O1MSDoB0LfjpKp8Psmf9evMhj7f1Wt%0D%0AIC_np1dzIG19akgD4XG-sfw%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYqqQSTfWdVD5iApNGZaLO-9WasrDo_pPRojM2JDAjzdYJ6uR9pnjU2Y0q8sbViqqWPsHc3JikK%0D%0A5BaZyamYgv6QRo4RAbG07aq3A2hfO7FGarkJ7pz0g3bfyaLbwxrB-xx-lQAEp4kg-Y2iEPJNmviu%0D%0A3xBaSOV-J-i0-SU0WnB9Ax0%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-2808948949._CB312481908_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-1753959650._CB315300322_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-3627981178._CB318528484_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-2430112694._CB318528507_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=0101f6f8199b9328476327e4f16ed4b2b67f17c128bbc79d68e09480346b4f38441c",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=138504532178"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-232398347._CB349580400_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=138504532178&ord=138504532178";
    },"unable to request AAN pixel");
</script>

<div id="flashContent" style="width: 0px; height: 0px; overflow:hidden;">
    <script type="text/javascript">
        if (generic && generic.monitoring.record_metric && flashAdUtils.canPlayFlash) {
            var flashLoaded = 0;
            function swfLoaded() {
                flashLoaded = 1;
                generic.monitoring.record_metric('ads_flash_did_play', 1, true);
            }
            window.onload = function() {
                // Flash was not loaded
                if (flashLoaded === 0) {
                    // Flash could be loaded but wasn't
                    if (flashAdUtils.canPlayFlash()) {
                        generic.monitoring.record_metric('ads_flash_can_but_did_not_play', 0, true);
                    } else {
                        generic.monitoring.record_metric('ads_flash_cannot_play', 0, true);
                    }
                }
            }
            /**
             * There is a rare possibility of window.onload getting called before swfLoaded.
             * Firing this metric to keep track of such calls.
             */
             generic.monitoring.record_metric('ads_flash_page_loaded', 1, true);
         }
    </script>
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="1x1" align="middle">
        <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
        <param name="quality" value="high" />
        <param name="play" value="true" />
        <param name="loop" value="true" />
        <param name="wmode" value="transparent" />
        <param name="scale" value="showall" />
        <param name="menu" value="true" />
        <param name="devicefont" value="false" />
        <param name="salign" value="" />
        <param name="allowScriptAccess" value="always" />

        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" width="1" height="1">
            <param name="movie" value="http://ia.media-imdb.com/images/G/01/imdbads/1x1-1154388164._CB316088672_.swf" />
            <param name="quality" value="high" />
            <param name="play" value="true" />
            <param name="loop" value="true" />
            <param name="wmode" value="transparent" />
            <param name="scale" value="showall" />
            <param name="menu" value="true" />
            <param name="devicefont" value="false" />
            <param name="salign" value="" />
            <param name="allowScriptAccess" value="always" />
        </object>
        <!--<![endif]-->
    </object>
</div>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="205"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
