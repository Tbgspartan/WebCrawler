<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trending  stories on Indian Lifestyle, Culture, Relationships, Food, Travel, Entertainment, News & New Technology News - Indiatimes.com</title>

<meta name="keywords" content="" />
<meta name="description" content="Indiatimes.com brings you the news, articles, stories and videos on entertainment, latest lifestyle, culture & new technologies emerging worldwide." />



		

<link rel="image_src" href="http://media.indiatimes.in/resources/images/fbimage.png" />
<meta property="og:title" content="" />    
<meta property="og:url" content="http://www.indiatimes.com" />
<meta property="og:image" content="http://media.indiatimes.in/resources/images/fbimage.png" />

<meta property="og:site_name" content="indiatimes.com" />
<meta property="fb:app_id" content="117787264903013" />
<meta property="og:description" content="" />
<meta name="google-site-verification" content="ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw" />
<meta name="msvalidate.01" content="BAB71AAED7CCC01DB106A58E99625EB4" />
<link href="https://plus.google.com/+indiatimes/posts" rel="publisher" />

<!--<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />-->

<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">    <link rel="shortcut icon" href="http://media.indiatimes.in/resources/images/favicon.ico?v=25" type="image/x-icon"><link rel="stylesheet" href="http://media.indiatimes.in/resources/css/site.css?v=100.8" media="screen"><link rel="stylesheet" href="http://www.indiatimes.com/fonts/fonts.min.css?v=100.8" media="screen"><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery-1.11.1.min.js?v=100.8"></script><script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/handlebars-v1.3.0.js?v=100.8"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.inview.min.js?v=100.8"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.preload.min.js?v=100.8"></script><script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script><script>(function() {var _fbq = window._fbq || (window._fbq = []);if (!_fbq.loaded) {var fbds = document.createElement('script');fbds.async = true;fbds.src = '//connect.facebook.net/en_US/fbds.js';var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(fbds, s);_fbq.loaded = true;}_fbq.push(['addPixelId', '624029644406124']);})();window._fbq = window._fbq || [];window._fbq.push(['track', 'PixelInitialized', {}]);</script><noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=624029644406124&amp;ev=PixelInitialized" /></noscript>           <!--  Analytics Code Begin -->    <!-- Begin comScore Tag -->    <script>        var _comscore = _comscore || [];        _comscore.push({ c1: "2", c2: "6036484" });        (function() {            var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";            el.parentNode.insertBefore(s, el);        })();    </script>    <noscript>    <img src="http://b.scorecardresearch.com/p?c1=2&c2=6036484&cv=2.0&cj=1" />    </noscript>    <!-- End comScore Tag -->    <!-- Begin Google Analytics Tag -->    <script>        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');        ga('create', 'UA-198011-6', 'indiatimes.com');            ga('require', 'displayfeatures');        ga('send', 'pageview');    </script>    <!-- End Google Analytics Tag -->    <!--  Analytics Code Begin --><!--adcode common script--><script type='text/javascript' src="http://ad.crwdcntrl.net/5/c=2800/pe=y/var=_ccaud"></script><script type="text/javascript">    var googletag = googletag || {};    googletag.cmd = googletag.cmd || [];    (function() {        var gads = document.createElement("script");        gads.async = true;        gads.type = "text/javascript";        var useSSL = "https:" == document.location.protocol;        gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";        var node =document.getElementsByTagName("script")[0];        node.parentNode.insertBefore(gads, node);    })();</script> <script type="text/javascript">        var nid='';        var cid='';        var sid='';        var brand_name='';                                                        </script>            <script type="text/javascript" src="http://media.indiatimes.in/resources/js/banner/home.js?v=100.8"></script>
            
<!--[if lt IE 9]>
  <script type="text/javascript" src="http://media.indiatimes.in/js/html5.js?v1.1"></script>
  <![endif]-->
<script>
var isDetailPage = 0</script>
</head>
<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];   
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=117787264903013&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script> 	
	
<div id="wrap" class="cf"><!--wrap start-->
    <div class='darkDisabledBg' id='disabledBg'></div>
<header>
    <div class="header-inner">
        <div class="comm-header ">
            <nav>
                <div class='forTop'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>

                    <div class="lnks">
                                                        <a href="http://www.indiatimes.com/news/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','News');">News</a> 
                                                               <a href="http://www.indiatimes.com/lifestyle/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Lifestyle');">Lifestyle</a> 
                                                               <a href="http://www.indiatimes.com/entertainment/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Entertainment');">Entertainment</a> 
                                                               <a href="http://www.indiatimes.com/videocafe/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Videos');">Videos</a> 
                                                               <a href="http://www.indiatimes.com/health/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Health');">Health</a> 
                                                                                          <a href='http://www.indiatimes.com/trending'  target="_blank" class="#fffff" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Trending');">Trending</a> 
                                                            <a href='http://www.indiatimes.com/specials/freedom-blogs/'  target="_blank" class="best" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','#FreedomBlogs');">#FreedomBlogs</a> 
                                                </div>
                </div>
                <div class='onScrolled'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>
                    <a href="javascript:;" class="menu rc sprite" id="headerMenu">Menu</a>
                    <div class="has-tag">
                                                       <a href='http://www.indiatimes.com/specials/freedom-blogs/'  target="_blank" class="best" >#FreedomBlogs</a> 
                                                </div>
                </div>

                                
            </nav>
			                 
	    <div id="sticker" style="display:none;"><span class="readTitle"></span></div>
                    <div class="socls fr share">
            </div>
                        
            
			 <div class="social fr">
                            <a title="facebook" target="_blank"  class="sprite fb" href="https://www.facebook.com/indiatimes">facebook</a>
                            <a title="twitter" target="_blank"  class="sprite twt" href="https://twitter.com/indiatimes">twitter</a>
                            <a title="search"  class="sprite serach" onclick="$('body').showSearch();" href="javascript:void(0);">search</a>
                        </div>
        </div>
    </div>
</header>
<div class="clr"></div>
<div id="overlay" class="animated bounceOut"><!--overlay start-->
    <div class="search_conts"><!--search-cont start-->
        <form onsubmit="return false;" class="serach-form">
            <input type="text" onkeyup="search_data();" class="input" onfocus="if(this.value=='Search'){this.value=''}" onblur="if(this.value==''){this.value='Search'};" value="Search" name="q" id="q" autocomplete="off">
            <input type="button" class="sprite src-btn" onclick="search_data();">
            <a class="sprite sclose" id="close" href="javascript:void(0);">X</a>
        </form>
        <div class="scroll-panes" id="scroll-panes">
            <div class="clr"></div>
            <div class="res-msg" id="res-msg"></div>
            <div class="search-list cf" id="results"></div>
            <div class="clr"></div>
            <div class="loadmore" id="progressBar" style="display: none;">Loading......</div>
            <a class="loadmore" style="display:none;" onclick="load_search_data(); " id="load_more_button" href="javascript:void(0);">Load more</a>
        </div>
    </div>
</div>
<div class="blk">
    <div class="inner">
        <dl id="leftMenu" class="leftMenu accordion">
            <dt data-color="red-bg" class=""><a href="http://www.indiatimes.com">Home</a></dt>
            <dd> </dd>
                            <dt  data-color="blue-bg" ><a href="http://www.indiatimes.com/news/">News</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/news/india/'>India</a>
                                     
                            <a href='http://www.indiatimes.com/news/world/'>World</a>
                                     
                            <a href='http://www.indiatimes.com/news/sports/'>Sports</a>
                                     
                            <a href='http://www.indiatimes.com/news/weird/'>Weird</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="purple-bg" ><a href="http://www.indiatimes.com/lifestyle/">Lifestyle</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/lifestyle/self/'>Self</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/style/'>Style</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                     
                            <a href='http://www.indiatimes.com/culture/who-we-are/'>Who we are</a>
                                     
                            <a href='http://www.indiatimes.com/culture/travel/'>Travel</a>
                                     
                            <a href='http://www.indiatimes.com/culture/food/'>Food</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="red-bg" ><a href="http://www.indiatimes.com/entertainment/">Entertainment</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/entertainment/bollywood/'>Bollywood</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/celebs/'>Celebscoop</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/hollywood/'>Hollywood</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="orange-bg" ><a href="http://www.indiatimes.com/videocafe/">Videos</a>
                                </dt>
                                <dt  data-color="green-bg" ><a href="http://www.indiatimes.com/health/">Health</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                     
                            <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                     
                            <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                     
                            <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                     
                            <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                            </dd>
                    <div class="clr"></div>
                            <dt class="pink-bg">
            <div class="follow">Follow indiatimes </div>
            <div class="follow_cont"> 
                <a href="https://www.facebook.com/indiatimes" class="fbleft sprite"> facebook</a> 
                <a href="https://twitter.com/indiatimes" class="twtleft sprite"> twitter</a> 
            </div>
            </dt>

            <dd>&nbsp;</dd>
            <div class="clr"></div>
        </dl>
    </div>
</div>


<div class="clr"></div>
<div class="dummy-cont">&nbsp;</div>
<div class="clr"></div>


<div id="pushdown">
    <div class="pull-ad"><!--pull-ad start-->   
	
                <script type="text/javascript"> 
                    showHeaderCode();
                </script>
	    </div><!--pull-ad end-->
</div>
    <!--testing 15-08-19 06:00:03-->    <section class="topslide-cont cf"><!--container start-->

         
 <div class="top-slide cf" id="top-slide" style="display:none;"><!--top-slide start-->
        <div class="top-slide-left pink-bg"><span class="yellow">TRENDING TODAY</span></div>
        <div class="top-slide-right skyblue-bg"><!--top-slide-right start-->
            <div class="flexslider">
                <div class="slides marqueeSlides" style=" max-width:800px;margin:auto; overflow:hidden">
				      
                    <span style="padding-right:50px;"><a title="This Video By The Indian Army Is So Good, It Just Gave Me Goosebumps!" href="http://www.indiatimes.com/videocafe/this-video-by-the-indian-army-is-so-good-it-just-gave-me-goosebumps-244302.html">This Video By The Indian Army Is So Good, It Just Gave Me Goosebumps!</a></span>
					 
                    <span style="padding-right:50px;"><a title="Dashrath Manjhi Aka Mountain Man's Story Is The Most Inspiring Thing You'll Read Today" href="http://www.indiatimes.com/entertainment/dashrath-manjhi-aka-mountain-mans-story-is-the-most-inspiring-thing-youll-read-today-243475.html">Dashrath Manjhi Aka Mountain Man's Story Is The Most Inspiring Thing You'll Read Today</a></span>
					 
                    <span style="padding-right:50px;"><a title="21 Anger Management Techniques That Will Stop You From Losing Your Shit. Ever" href="http://www.indiatimes.com/lifestyle/self/21-anger-management-techniques-that-will-stop-you-from-losing-your-shit-ever-244123.html">21 Anger Management Techniques That Will Stop You From Losing Your Shit. Ever</a></span>
					 
                    <span style="padding-right:50px;"><a title="9 Quick Bhindi Recipe Ideas For A Healthy Tiffin" href="http://www.indiatimes.com/health/recipes/9-quick-bhindi-recipes-ideas-for-a-healthy-tiffin-244157.html">9 Quick Bhindi Recipe Ideas For A Healthy Tiffin</a></span>
					 
                    <span style="padding-right:50px;"><a title="This Cartoon Strip Is The Perfect Tribute To Dr A.P.J.Abdul Kalam" href="http://www.indiatimes.com/culture/who-we-are/this-cartoon-strip-is-the-perfect-tribute-to-dr-apjabdul-kalam-244279.html">This Cartoon Strip Is The Perfect Tribute To Dr A.P.J.Abdul Kalam</a></span>
					 
                    <span style="padding-right:50px;"><a title="A Woman Ate Only Bananas For 12 Days And Look What It Did To Her" href="http://www.indiatimes.com/health/healthyliving/a-woman-ate-only-bananas-for-12-days-and-look-what-it-did-to-her-244237.html">A Woman Ate Only Bananas For 12 Days And Look What It Did To Her</a></span>
					 
                    <span style="padding-right:50px;"><a title="While The Internet Celebrated The London Marathon's Free-Bleeding Runner, It Forgot Its Champion" href="http://www.indiatimes.com/lifestyle/self/while-the-internet-celebrated-the-london-marathons-freebleeding-runner-it-forgot-its-champion-244304.html">While The Internet Celebrated The London Marathon's Free-Bleeding Runner, It Forgot Its Champion</a></span>
					 
                    <span style="padding-right:50px;"><a title="'My Relationship Status Is Ambiguous. Iâm An Enigma!' Priyanka Chopra Gets Candid On Love And Much More!" href="http://www.indiatimes.com/entertainment/celebs/my-relationship-status-is-ambiguous-iâm-an-enigma-priyanka-chopra-gets-candid-on-love-and-much-more-244278.html">'My Relationship Status Is Ambiguous. Iâm An Enigma!' Priyanka Chopra Gets Candid On Love And Much More!</a></span>
					 
                    <span style="padding-right:50px;"><a title="Millions Of Farmers In India Are Living Their Lives In Fear. You'll Be Shocked To Know Why" href="http://www.indiatimes.com/videocafe/millions-of-farmers-in-india-are-living-their-lives-in-fear-youll-be-shocked-to-know-why-244258.html">Millions Of Farmers In India Are Living Their Lives In Fear. You'll Be Shocked To Know Why</a></span>
					 
                    <span style="padding-right:50px;"><a title="Radhe Maa Drama Continues, Sonu Nigam Lands In Trouble For Defending The Self-Styled Godwoman" href="http://www.indiatimes.com/entertainment/radhe-maa-drama-continues-sonu-nigam-lands-in-trouble-for-defending-the-selfstyled-godwoman-244269.html">Radhe Maa Drama Continues, Sonu Nigam Lands In Trouble For Defending The Self-Styled Godwoman</a></span>
					 
                    <span style="padding-right:50px;"><a title="11 Face Mashups Of Popular Hollywood Celebrities That Will Add A Lot Of Heat To Your Life" href="http://www.indiatimes.com/entertainment/hollywood/11-face-mashups-of-popular-hollywood-celebrities-that-will-add-a-lot-of-heat-to-your-life-244270.html">11 Face Mashups Of Popular Hollywood Celebrities That Will Add A Lot Of Heat To Your Life</a></span>
					 
                    <span style="padding-right:50px;"><a title="Bored Of The Same Old Safety Demo In Flights? Check Out This Flight Attendant Performing The Same Act In A Hilarious Manner!" href="http://www.indiatimes.com/videocafe/bored-of-the-same-old-safety-demo-in-flights-check-out-this-flight-attendant-performing-the-same-act-in-a-hilarious-manner-244294.html">Bored Of The Same Old Safety Demo In Flights? Check Out This Flight Attendant Performing The Act In A Hilarious Manner!</a></span>
					 
                    <span style="padding-right:50px;"><a title="This Picture Of Joey & Chandler Shows How Time Flies. Yet Our Love For 'Friends' Remain Unfazed!" href="http://www.indiatimes.com/entertainment/hollywood/this-picture-of-joey-chandler-shows-how-time-flies-yet-our-love-for-friends-remain-unfazed-244263.html">This Picture Of Joey & Chandler Shows How Time Flies. Yet Our Love For 'Friends' Remain Unfazed!</a></span>
					 
                    <span style="padding-right:50px;"><a title="Gangster Ravi Pujari's Funny Barter With Arijit Singh. Sing Songs For Me Or Pay Rs 5 Crore" href="http://www.indiatimes.com/entertainment/bollywood/gangster-ravi-pujaris-funny-barter-with-arijit-singh-sing-songs-for-me-or-pay-rs-5-crore-244242.html">Gangster Ravi Pujari's Funny Barter With Arijit Singh. Sing Songs For Me Or Pay Rs 5 Crore</a></span>
					 
                    <span style="padding-right:50px;"><a title="How To Spot The Thin Line Between Confidence And Overconfidence" href="http://www.indiatimes.com/culture/who-we-are/how-to-spot-the-thin-line-between-confidence-and-overconfidence-244101.html">How To Spot The Thin Line Between Confidence And Overconfidence</a></span>
					 
                    <span style="padding-right:50px;"><a title="4 Healthy Recipes That Will Make You A Fan Of Parsi Food" href="http://www.indiatimes.com/health/recipes/4-healthy-recipes-that-will-make-you-a-fan-of-parsi-food-243986.html">4 Healthy Recipes That Will Make You A Fan Of Parsi Food</a></span>
					 
                    <span style="padding-right:50px;"><a title="Do Your Dreams Really Represent Your Deepest, Darkest Desires? Or Something Else Entirely?" href="http://www.indiatimes.com/health/healthyliving/do-your-dreams-really-represent-your-deepest-darkest-desires-or-something-else-entirely-244031.html">Do Your Dreams Really Represent Your Deepest, Darkest Desires? Or Something Else Entirely?</a></span>
					 
                    <span style="padding-right:50px;"><a title="10 Diplomatic Feuds That Were Brought About In The Name Of Food" href="http://www.indiatimes.com/culture/food/10-diplomatic-feuds-that-were-brought-about-in-the-name-of-food-244068.html">10 Diplomatic Feuds That Were Brought About In The Name Of Food</a></span>
					 
                    <span style="padding-right:50px;"><a title="NASA Just Tested The Rocket That Will Take Man To Mars. Visuals Obviously Are Stunning" href="http://www.indiatimes.com/lifestyle/technology/nasa-just-tested-the-rocket-that-will-take-man-to-mars-visuals-obviously-are-stunning-244230.html">NASA Just Tested The Rocket That Will Take Man To Mars. Visuals Obviously Are Stunning</a></span>
					 
                    <span style="padding-right:50px;"><a title="Ladies, Are You Wearing Your Bra Correctly?" href="http://www.indiatimes.com/health/tips-tricks/ladies-are-you-wearing-your-bra-correctly-244257.html">Ladies, Are You Wearing Your Bra Correctly?</a></span>
					 
                    <span style="padding-right:50px;"><a title="Oscar-Winning Actor Morgan Freeman's Step-Granddaughter Mercilessly Stabbed To Death Outside Her New-York Appartment" href="http://www.indiatimes.com/entertainment/hollywood/oscarwinning-actor-morgan-freemans-stepgranddaughter-mercilessly-stabbed-to-death-outside-her-newyork-appartment-244241.html">Oscar-Winning Actor Morgan Freeman's Step-Granddaughter Mercilessly Stabbed To Death Outside Her New-York Appartment</a></span>
					 
                    <span style="padding-right:50px;"><a title="Jeff Bezos Writes A Memo To Amazon Staff, Calls NYT Article 'Biased' & 'Full Of Half Truths'!" href="http://www.indiatimes.com/lifestyle/technology/jeff-bezos-writes-a-memo-to-amazon-staff-calls-nyt-article-biased-full-of-half-truths-244249.html">Jeff Bezos Writes A Memo To Amazon Staff, Calls NYT Article 'Biased' & 'Full Of Half Truths'!</a></span>
					 
                    <span style="padding-right:50px;"><a title="9 Indian TV Shows You Must Check Out In The Second Half Of 2015!" href="http://www.indiatimes.com/entertainment/bollywood/9-indian-tv-shows-you-must-check-out-in-the-second-half-of-2015_-244233.html">9 Indian TV Shows You Must Check Out In The Second Half Of 2015!</a></span>
					 
                    <span style="padding-right:50px;"><a title="Salman's 'Bajrangi Bhaijaan' Still Going Strong. Sets Rs 600 Crore Record!" href="http://www.indiatimes.com/entertainment/bollywood/bhai-does-it-again-salmans-bajrangi-bhaijaan-is-crushing-all-boxoffice-records-235019.html">Salman's 'Bajrangi Bhaijaan' Still Going Strong. Sets Rs 600 Crore Record!</a></span>
					 
                    <span style="padding-right:50px;"><a title="12 Surya Namaskar Steps You Should Practice Every Morning To Kill That Fat" href="http://www.indiatimes.com/health/healthyliving/12-surya-namaskar-steps-you-should-practice-every-morning-to-kill-that-fat-244107.html">12 Surya Namaskar Steps You Should Practice Every Morning To Kill That Fat</a></span>
					                    
                </div>
            </div>
            <a title="close" class="sprite close" href="javascript:void(0);">close</a>
        </div><!--top-slide-right end-->
    </div><!--top-slide end-->
    </section>

    <section id="hp_block_1" class="container cf" style="border: 0px solid red;"><!--container start-->
                

        <div class="news-panel cf"><!--news-panel start-->
            <h2>Big news</h2>

            <div class="feature-list cf"><!--feature-list start-->	
                                    <figure>
                        <div class="hash-tag">
                             18 hours ago                         </div>
						 						          
                        <a href="http://www.indiatimes.com/news/india/girl-drags-coca-cola-to-court-for-failing-to-keep-promise-of-a-date-with-hrithik-roshan-244272.html" class=" tint" title="Coke Gets Court Notice For Failing To Arrange A Girl's Date With Hritik Roshan 15 Years Ago!">
                            <img  class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Aug/coke-collage-502_1439876513_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/coke-collage-502_1439876513_236x111.jpg"  border="0" alt="Coke Gets Court Notice For Failing To Arrange A Girl's Date With Hritik Roshan 15 Years Ago!"/></a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/girl-drags-coca-cola-to-court-for-failing-to-keep-promise-of-a-date-with-hrithik-roshan-244272.html" title="Coke Gets Court Notice For Failing To Arrange A Girl's Date With Hritik Roshan 15 Years Ago!">
                            Coke Gets Court Notice For Failing To Arrange A Girl's Date With Hritik Roshan 15 Years Ago!                        </a>
                    </figcaption>
                            </div><!--feature-list end-->

                            <div class="news-panel-list cf" id="column1_0"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            11 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/india/squash-star-dipika-pallikal-breaks-hearts-marries-cricketer-dinesh-karthik-244312.html" title="Squash Star Dipika Pallikal Breaks Hearts, Marries Cricketer Dinesh Karthik" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Aug/dk-pallikalcorecommunique_1439903450_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/dk-pallikalcorecommunique_1439903450_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/squash-star-dipika-pallikal-breaks-hearts-marries-cricketer-dinesh-karthik-244312.html" title="Squash Star Dipika Pallikal Breaks Hearts, Marries Cricketer Dinesh Karthik">
                            Squash Star Dipika Pallikal Breaks Hearts, Marries Cricketer Dinesh Karthik                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_1"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            11 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/india/landslide-hits-manikaran-sahib-gurudwara-in-himachal-10-dead-several-still-trapped-inside-244311.html" title="Landslide Hits Manikaran Sahib Gurudwara In Himachal, 10 Dead, Several Still Trapped Inside" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Aug/manikaran-502-col_1439903608_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/manikaran-502-col_1439903608_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/landslide-hits-manikaran-sahib-gurudwara-in-himachal-10-dead-several-still-trapped-inside-244311.html" title="Landslide Hits Manikaran Sahib Gurudwara In Himachal, 10 Dead, Several Still Trapped Inside">
                            Landslide Hits Manikaran Sahib Gurudwara In Himachal, 10 Dead, Several Still Trapped Inside                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_2"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/world/this-woman-publicly-shed-her-clothes-for-all-to-see-and-the-reason-behind-it-is-heartwarming-244308.html" title="This Woman Publicly Shed Her Clothes For All To See, And The Reason Behind It Is Heartwarming" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Aug/502_1439898438_1439898443_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/502_1439898438_1439898443_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/this-woman-publicly-shed-her-clothes-for-all-to-see-and-the-reason-behind-it-is-heartwarming-244308.html" title="This Woman Publicly Shed Her Clothes For All To See, And The Reason Behind It Is Heartwarming">
                            This Woman Publicly Shed Her Clothes For All To See, And The Reason Behind It Is Heartwarming                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_3"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/world/what-the-world-looks-like-without-cars-is-all-you-need-to-see-to-give-up-one-of-yours-244297.html" title="What The World Looks Like Without Cars Is All You Need To See To Give Up One Of Yours" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Aug/cars502_1439892428_1439892432_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/cars502_1439892428_1439892432_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/what-the-world-looks-like-without-cars-is-all-you-need-to-see-to-give-up-one-of-yours-244297.html" title="What The World Looks Like Without Cars Is All You Need To See To Give Up One Of Yours">
                            What The World Looks Like Without Cars Is All You Need To See To Give Up One Of Yours                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                    </div><!--news-panel end-->

        <div class="life-panel cf" id="middleBlock1"><!--life-panel start-->
            <h2>lifestyle</h2>

            

                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                   
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/lifestyle/self/21-anger-management-techniques-that-will-stop-you-from-losing-your-shit-ever-244123.html" class="tint" title="21 Anger Management Techniques That Will Stop You From Losing Your Shit. Ever" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/self/21-anger-management-techniques-that-will-stop-you-from-losing-your-shit-ever-244123.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Aug/anger-card_1439455468_502x234.gif" data-original23="http://media.indiatimes.in/media/content/2015/Aug/anger-card_1439455468_502x234.gif"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/21-anger-management-techniques-that-will-stop-you-from-losing-your-shit-ever-244123.html" title="21 Anger Management Techniques That Will Stop You From Losing Your Shit. Ever" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/self/21-anger-management-techniques-that-will-stop-you-from-losing-your-shit-ever-244123.html');">
                            21 Anger Management Techniques That Will Stop You From Losing Your Shit. Ever                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                   
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/lifestyle/self/while-the-internet-celebrated-the-london-marathons-freebleeding-runner-it-forgot-its-champion-244304.html" class="tint" title="While The Internet Celebrated The London Marathon's Free-Bleeding Runner, It Forgot Its Champion" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/self/while-the-internet-celebrated-the-london-marathons-freebleeding-runner-it-forgot-its-champion-244304.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Aug/download_1439893967_1439893978_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/download_1439893967_1439893978_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/while-the-internet-celebrated-the-london-marathons-freebleeding-runner-it-forgot-its-champion-244304.html" title="While The Internet Celebrated The London Marathon's Free-Bleeding Runner, It Forgot Its Champion" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/self/while-the-internet-celebrated-the-london-marathons-freebleeding-runner-it-forgot-its-champion-244304.html');">
                            While The Internet Celebrated The London Marathon's Free-Bleeding Runner, It Forgot Its Champion                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                   
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/celebs/my-relationship-status-is-ambiguous-iâm-an-enigma-priyanka-chopra-gets-candid-on-love-and-much-more-244278.html" class="tint" title="'My Relationship Status Is Ambiguous. Iâm An Enigma!' Priyanka Chopra Gets Candid On Love And Much More!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/entertainment/celebs/my-relationship-status-is-ambiguous-iâm-an-enigma-priyanka-chopra-gets-candid-on-love-and-much-more-244278.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Aug/card_1439883743_1439883761_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/card_1439883743_1439883761_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/my-relationship-status-is-ambiguous-iâm-an-enigma-priyanka-chopra-gets-candid-on-love-and-much-more-244278.html" title="'My Relationship Status Is Ambiguous. Iâm An Enigma!' Priyanka Chopra Gets Candid On Love And Much More!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/entertainment/celebs/my-relationship-status-is-ambiguous-iâm-an-enigma-priyanka-chopra-gets-candid-on-love-and-much-more-244278.html');">
                            'My Relationship Status Is Ambiguous. Iâm An Enigma!' Priyanka Chopra Gets Candid On Love And Much More!                        </a>
                    </figcaption>
                </div>
                        
        </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_0">
                    <span class="strip skyblue-bg"></span>                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-video-by-the-indian-army-is-so-good-it-just-gave-me-goosebumps-244302.html'>video</a>                        <a href="http://www.indiatimes.com/videocafe/this-video-by-the-indian-army-is-so-good-it-just-gave-me-goosebumps-244302.html" class="tint" title="This Video By The Indian Army Is So Good, It Just Gave Me Goosebumps!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/videocafe/2015/Aug/army_1439893765_1439893781_218x102.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Aug/army_1439893765_1439893781_218x102.jpg" border="0" alt="This Video By The Indian Army Is So Good, It Just Gave Me Goosebumps!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/this-video-by-the-indian-army-is-so-good-it-just-gave-me-goosebumps-244302.html" title="This Video By The Indian Army Is So Good, It Just Gave Me Goosebumps!">
                            This Video By The Indian Army Is So Good, It Just Gave Me Goosebumps!                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_1">
                                        <figure>

                                                <a href="http://www.indiatimes.com/entertainment/dashrath-manjhi-aka-mountain-mans-story-is-the-most-inspiring-thing-youll-read-today-243475.html" class="tint" title="Dashrath Manjhi Aka Mountain Man's Story Is The Most Inspiring Thing You'll Read Today">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Jul/picmonkey-collage_1437987155_1437987160_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Jul/picmonkey-collage_1437987155_1437987160_218x102.jpg" border="0" alt="Dashrath Manjhi Aka Mountain Man's Story Is The Most Inspiring Thing You'll Read Today"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/dashrath-manjhi-aka-mountain-mans-story-is-the-most-inspiring-thing-youll-read-today-243475.html" title="Dashrath Manjhi Aka Mountain Man's Story Is The Most Inspiring Thing You'll Read Today">
                            Dashrath Manjhi Aka Mountain Man's Story Is The Most Inspiring Thing You'll Read Today                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_2">
                                        <figure>

                                                <a href="http://www.indiatimes.com/lifestyle/self/21-anger-management-techniques-that-will-stop-you-from-losing-your-shit-ever-244123.html" class="tint" title="21 Anger Management Techniques That Will Stop You From Losing Your Shit. Ever">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Aug/anger-card_1439455468_218x102.gif" data-original23="http://media.indiatimes.in/media/content/2015/Aug/anger-card_1439455468_218x102.gif" border="0" alt="21 Anger Management Techniques That Will Stop You From Losing Your Shit. Ever"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/21-anger-management-techniques-that-will-stop-you-from-losing-your-shit-ever-244123.html" title="21 Anger Management Techniques That Will Stop You From Losing Your Shit. Ever">
                            21 Anger Management Techniques That Will Stop You From Losing Your Shit. Ever                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_3">
                                        <figure>

                                                <a href="http://www.indiatimes.com/health/recipes/9-quick-bhindi-recipes-ideas-for-a-healthy-tiffin-244157.html" class="tint" title="9 Quick Bhindi Recipe Ideas For A Healthy Tiffin">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Aug/bhindi-card_1439536960_218x102.gif" data-original23="http://media.indiatimes.in/media/content/2015/Aug/bhindi-card_1439536960_218x102.gif" border="0" alt="9 Quick Bhindi Recipe Ideas For A Healthy Tiffin"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/recipes/9-quick-bhindi-recipes-ideas-for-a-healthy-tiffin-244157.html" title="9 Quick Bhindi Recipe Ideas For A Healthy Tiffin">
                            9 Quick Bhindi Recipe Ideas For A Healthy Tiffin                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_4">
                                        <figure>

                                                <a href="http://www.indiatimes.com/culture/who-we-are/this-cartoon-strip-is-the-perfect-tribute-to-dr-apjabdul-kalam-244279.html" class="tint" title="This Cartoon Strip Is The Perfect Tribute To Dr A.P.J.Abdul Kalam">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Aug/aa_1439897542_1439897553_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Aug/aa_1439897542_1439897553_218x102.jpg" border="0" alt="This Cartoon Strip Is The Perfect Tribute To Dr A.P.J.Abdul Kalam"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/this-cartoon-strip-is-the-perfect-tribute-to-dr-apjabdul-kalam-244279.html" title="This Cartoon Strip Is The Perfect Tribute To Dr A.P.J.Abdul Kalam">
                            This Cartoon Strip Is The Perfect Tribute To Dr A.P.J.Abdul Kalam                        </a>
                    </figcaption>
                </div>
                        </div><!--trending-panel end-->
    </section><!--container end-->

<!-- -------------------    end Block 1  ------------------------------------- -->
<section class="big-ads" id="ad1"> 
<div id="bigAd1_slot"></div>
    <script>        
        showBigAD1('bigAd1_slot');
    </script> 
</section>
<!-- -------------------    start Block 2  ------------------------------------- -->

    <section id="hp_block_2" class="container cf" style="border: 0px solid red;"><!--container start-->
        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
                            <div class="news-panel-list cf" id="column1_4"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            13 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/finally-malayalam-finds-fans-beyond-gulf-finds-new-base-and-admirers-in-germany-244305.html" title="Finally Malayalam Makes A Move Beyond The Gulf, Finds New Base And Admirers In Germany" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1439895944_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/finally-malayalam-finds-fans-beyond-gulf-finds-new-base-and-admirers-in-germany-244305.html" title="Finally Malayalam Makes A Move Beyond The Gulf, Finds New Base And Admirers In Germany">
                            Finally Malayalam Makes A Move Beyond The Gulf, Finds New Base And Admirers In Germany                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_5"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            14 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/world/taiwans-prettiest-waitress-in-the-world-plus-other-everyday-girls-who-went-batshit-viral-244291.html" title="Taiwan's 'Prettiest Waitress In The World' Plus Other Everyday Girls Who Went Batshit Viral!" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/wei5_1439888783_1439888788_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/world/taiwans-prettiest-waitress-in-the-world-plus-other-everyday-girls-who-went-batshit-viral-244291.html" title="Taiwan's 'Prettiest Waitress In The World' Plus Other Everyday Girls Who Went Batshit Viral!">
                            Taiwan's 'Prettiest Waitress In The World' Plus Other Everyday Girls Who Went Batshit Viral!                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_6"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            14 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/this-parrot-was-arrested-in-maharashtra-for-talking-dirty-to-an-85yearold_-244300.html" title="Parrot Arrested For Saying Obscene Things To An 85-Year-Old Woman. True Story" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/angry-parrot-502_1439893015_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/this-parrot-was-arrested-in-maharashtra-for-talking-dirty-to-an-85yearold_-244300.html" title="Parrot Arrested For Saying Obscene Things To An 85-Year-Old Woman. True Story">
                            Parrot Arrested For Saying Obscene Things To An 85-Year-Old Woman. True Story                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_7"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            14 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/cop-gets-drunk-streaks-across-a-railway-station-in-his-chaddies-yes-it-happens-only-in-up-244264.html" title="Cop Gets Drunk, Streaks Across A Railway Station In His Chaddies. Yes, It Happens Only In UP!" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/cop-undie0400_1439890456_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/cop-gets-drunk-streaks-across-a-railway-station-in-his-chaddies-yes-it-happens-only-in-up-244264.html" title="Cop Gets Drunk, Streaks Across A Railway Station In His Chaddies. Yes, It Happens Only In UP!">
                            Cop Gets Drunk, Streaks Across A Railway Station In His Chaddies. Yes, It Happens Only In UP!                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_8"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            15 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/did-you-know-osama-once-invoked-gandhi-to-his-followers-asked-them-to-follow-his-principles-against-america-244293.html" title="Did You Know Osama Once Invoked Gandhi To His Followers, Asked Them To Follow His Principles Against America?" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/osama-gandhi502_1439889966_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/did-you-know-osama-once-invoked-gandhi-to-his-followers-asked-them-to-follow-his-principles-against-america-244293.html" title="Did You Know Osama Once Invoked Gandhi To His Followers, Asked Them To Follow His Principles Against America?">
                            Did You Know Osama Once Invoked Gandhi To His Followers, Asked Them To Follow His Principles Against America?                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-video-by-the-indian-army-is-so-good-it-just-gave-me-goosebumps-244302.html'>video</a>                          
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/videocafe/this-video-by-the-indian-army-is-so-good-it-just-gave-me-goosebumps-244302.html" class="tint" title="This Video By The Indian Army Is So Good, It Just Gave Me Goosebumps!">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/army_1439893765_1439893781_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/this-video-by-the-indian-army-is-so-good-it-just-gave-me-goosebumps-244302.html" title="This Video By The Indian Army Is So Good, It Just Gave Me Goosebumps!">
                            This Video By The Indian Army Is So Good, It Just Gave Me Goosebumps!                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/bored-of-the-same-old-safety-demo-in-flights-check-out-this-flight-attendant-performing-the-same-act-in-a-hilarious-manner-244294.html'>video</a>                          
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/videocafe/bored-of-the-same-old-safety-demo-in-flights-check-out-this-flight-attendant-performing-the-same-act-in-a-hilarious-manner-244294.html" class="tint" title="Bored Of The Same Old Safety Demo In Flights? Check Out This Flight Attendant Performing The Act In A Hilarious Manner!">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/flight_attendant_card_1439890992_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/bored-of-the-same-old-safety-demo-in-flights-check-out-this-flight-attendant-performing-the-same-act-in-a-hilarious-manner-244294.html" title="Bored Of The Same Old Safety Demo In Flights? Check Out This Flight Attendant Performing The Act In A Hilarious Manner!">
                            Bored Of The Same Old Safety Demo In Flights? Check Out This Flight Attendant Performing The Act In A Hilarious Manner!                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/hollywood/vin-diesel-the-fast-and-furious-crew-pay-tribute-to-paul-walker-at-2015-teen-choice-awards-turn-a-confetticovered-show-into-a-heartwarming-tribute-244303.html" class="tint" title="The Fast And Furious Crew's Tribute To Paul Walker Turned A Confetti-Covered Award Show To A Heart-Warming Moment">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/paul-walker1_1439893976_1439893984_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/hollywood/vin-diesel-the-fast-and-furious-crew-pay-tribute-to-paul-walker-at-2015-teen-choice-awards-turn-a-confetticovered-show-into-a-heartwarming-tribute-244303.html" title="The Fast And Furious Crew's Tribute To Paul Walker Turned A Confetti-Covered Award Show To A Heart-Warming Moment">
                            The Fast And Furious Crew's Tribute To Paul Walker Turned A Confetti-Covered Award Show To A Heart-Warming Moment                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_5"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/health/healthyliving/a-woman-ate-only-bananas-for-12-days-and-look-what-it-did-to-her-244237.html" class="tint" title="A Woman Ate Only Bananas For 12 Days And Look What It Did To Her">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/fb_1439817513_218x102.jpg" border="0" alt="A Woman Ate Only Bananas For 12 Days And Look What It Did To Her"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/a-woman-ate-only-bananas-for-12-days-and-look-what-it-did-to-her-244237.html" title="A Woman Ate Only Bananas For 12 Days And Look What It Did To Her">
                            A Woman Ate Only Bananas For 12 Days And Look What It Did To Her                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_6"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/lifestyle/self/while-the-internet-celebrated-the-london-marathons-freebleeding-runner-it-forgot-its-champion-244304.html" class="tint" title="While The Internet Celebrated The London Marathon's Free-Bleeding Runner, It Forgot Its Champion">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/download_1439893967_1439893978_218x102.jpg" border="0" alt="While The Internet Celebrated The London Marathon's Free-Bleeding Runner, It Forgot Its Champion"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/while-the-internet-celebrated-the-london-marathons-freebleeding-runner-it-forgot-its-champion-244304.html" title="While The Internet Celebrated The London Marathon's Free-Bleeding Runner, It Forgot Its Champion">
                            While The Internet Celebrated The London Marathon's Free-Bleeding Runner, It Forgot Its Champion                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_7"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/celebs/my-relationship-status-is-ambiguous-iâm-an-enigma-priyanka-chopra-gets-candid-on-love-and-much-more-244278.html" class="tint" title="'My Relationship Status Is Ambiguous. Iâm An Enigma!' Priyanka Chopra Gets Candid On Love And Much More!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1439883743_1439883761_218x102.jpg" border="0" alt="'My Relationship Status Is Ambiguous. Iâm An Enigma!' Priyanka Chopra Gets Candid On Love And Much More!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/my-relationship-status-is-ambiguous-iâm-an-enigma-priyanka-chopra-gets-candid-on-love-and-much-more-244278.html" title="'My Relationship Status Is Ambiguous. Iâm An Enigma!' Priyanka Chopra Gets Candid On Love And Much More!">
                            'My Relationship Status Is Ambiguous. Iâm An Enigma!' Priyanka Chopra Gets Candid On Love And Much More!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_8"><!--trending-panel-list start-->
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/millions-of-farmers-in-india-are-living-their-lives-in-fear-youll-be-shocked-to-know-why-244258.html'>video</a>
                        <a href="http://www.indiatimes.com/videocafe/millions-of-farmers-in-india-are-living-their-lives-in-fear-youll-be-shocked-to-know-why-244258.html" class="tint" title="Millions Of Farmers In India Are Living Their Lives In Fear. You'll Be Shocked To Know Why">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/farmers_card_1439812603_218x102.jpg" border="0" alt="Millions Of Farmers In India Are Living Their Lives In Fear. You'll Be Shocked To Know Why"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/millions-of-farmers-in-india-are-living-their-lives-in-fear-youll-be-shocked-to-know-why-244258.html" title="Millions Of Farmers In India Are Living Their Lives In Fear. You'll Be Shocked To Know Why">
                            Millions Of Farmers In India Are Living Their Lives In Fear. You'll Be Shocked To Know Why                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_9"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/radhe-maa-drama-continues-sonu-nigam-lands-in-trouble-for-defending-the-selfstyled-godwoman-244269.html" class="tint" title="Radhe Maa Drama Continues, Sonu Nigam Lands In Trouble For Defending The Self-Styled Godwoman">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/sonu-nigam-news_1439876435_1439876451_218x102.jpg" border="0" alt="Radhe Maa Drama Continues, Sonu Nigam Lands In Trouble For Defending The Self-Styled Godwoman"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/radhe-maa-drama-continues-sonu-nigam-lands-in-trouble-for-defending-the-selfstyled-godwoman-244269.html" title="Radhe Maa Drama Continues, Sonu Nigam Lands In Trouble For Defending The Self-Styled Godwoman">
                            Radhe Maa Drama Continues, Sonu Nigam Lands In Trouble For Defending The Self-Styled Godwoman                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div>

    </section><!--container end-->

<!--------------------------- end Block2 ------------------------------------------>
<section class="big-ads" id="ad2"> 
<div id="bigAd2_slot"></div>
</section>
<!--------------------------- start Block3 ------------------------------------------>

    <section id="hp_block_3" class="container cf"><!--container start-->

        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
          
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_9">
                    <figure>
                        <div class="hash-tag">
                            16 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/sports/5-questions-that-the-indian-team-needs-to-answer-before-the-start-of-2nd-test-vs-sri-lanka-244289.html" title="5 Questions That The Indian Team Needs To Answer Before The Start Of 2nd Test Vs Sri Lanka" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/kohlishastricard_1439885063_236x111.jpg" border="0" alt="5 Questions That The Indian Team Needs To Answer Before The Start Of 2nd Test Vs Sri Lanka"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/5-questions-that-the-indian-team-needs-to-answer-before-the-start-of-2nd-test-vs-sri-lanka-244289.html" title="5 Questions That The Indian Team Needs To Answer Before The Start Of 2nd Test Vs Sri Lanka">
                            5 Questions That The Indian Team Needs To Answer Before The Start Of 2nd Test Vs Sri Lanka                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_10">
                    <figure>
                        <div class="hash-tag">
                            16 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/soon-you-may-be-able-to-whiz-past-toll-plazas-across-india-by-paying-an-annual-fee-244287.html" title="Soon You May Be Able To Whiz Past Toll Plazas Across India By Paying An Annual Fee" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/img-20150818-wa0026-502_1439885750_236x111.jpg" border="0" alt="Soon You May Be Able To Whiz Past Toll Plazas Across India By Paying An Annual Fee"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/soon-you-may-be-able-to-whiz-past-toll-plazas-across-india-by-paying-an-annual-fee-244287.html" title="Soon You May Be Able To Whiz Past Toll Plazas Across India By Paying An Annual Fee">
                            Soon You May Be Able To Whiz Past Toll Plazas Across India By Paying An Annual Fee                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_11">
                    <figure>
                        <div class="hash-tag">
                            16 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/world/watching-too-much-porn-may-leave-you-uninterested-and-unexcited-during-real-sex-244286.html" title="Here's What Happens In Your Brain When You Watch Too Much Porn!" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1439887276_236x111.jpg" border="0" alt="Here's What Happens In Your Brain When You Watch Too Much Porn!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/watching-too-much-porn-may-leave-you-uninterested-and-unexcited-during-real-sex-244286.html" title="Here's What Happens In Your Brain When You Watch Too Much Porn!">
                            Here's What Happens In Your Brain When You Watch Too Much Porn!                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_12">
                    <figure>
                        <div class="hash-tag">
                            16 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/this-awesome-hyperlapse-shot-over-a-year-shows-you-just-beautiful-delhi-is-without-even-trying-244275.html" title="This Awesome Hyperlapse Shot Over A Year Shows You Just Beautiful Delhi Is Without Even Trying!" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/green_delhi_card_1439882195_236x111.jpg" border="0" alt="This Awesome Hyperlapse Shot Over A Year Shows You Just Beautiful Delhi Is Without Even Trying!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/this-awesome-hyperlapse-shot-over-a-year-shows-you-just-beautiful-delhi-is-without-even-trying-244275.html" title="This Awesome Hyperlapse Shot Over A Year Shows You Just Beautiful Delhi Is Without Even Trying!">
                            This Awesome Hyperlapse Shot Over A Year Shows You Just Beautiful Delhi Is Without Even Trying!                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_13">
                    <figure>
                        <div class="hash-tag">
                            17 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/1971-war-veteran-uttam-patkar-fights-off-a-knifewielding-local-goon-to-save-employees-life-244282.html" title="1971 War Veteran Uttam Patkar Fights Off A Knife-Wielding Local Goon To Save Employee's Life!" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/uttam-collage-502_1439882073_236x111.jpg" border="0" alt="1971 War Veteran Uttam Patkar Fights Off A Knife-Wielding Local Goon To Save Employee's Life!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/1971-war-veteran-uttam-patkar-fights-off-a-knifewielding-local-goon-to-save-employees-life-244282.html" title="1971 War Veteran Uttam Patkar Fights Off A Knife-Wielding Local Goon To Save Employee's Life!">
                            1971 War Veteran Uttam Patkar Fights Off A Knife-Wielding Local Goon To Save Employee's Life!                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/healthyliving/a-woman-ate-only-bananas-for-12-days-and-look-what-it-did-to-her-244237.html" class="tint" title="A Woman Ate Only Bananas For 12 Days And Look What It Did To Her">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/fb_1439817513_502x234.jpg" border="0" alt="http://www.indiatimes.com/health/healthyliving/a-woman-ate-only-bananas-for-12-days-and-look-what-it-did-to-her-244237.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/a-woman-ate-only-bananas-for-12-days-and-look-what-it-did-to-her-244237.html" title="A Woman Ate Only Bananas For 12 Days And Look What It Did To Her">
                            A Woman Ate Only Bananas For 12 Days And Look What It Did To Her                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/videocafe/are-these-real-or-just-illusions-well-these-pictures-will-twist-your-mind-244288.html" class="tint" title="8 Pictures + 1 Video That Are Trippier Than Anything You Will See This Month. God Promise.">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/ferrari_card_1439886766_502x234.jpg" border="0" alt="http://www.indiatimes.com/videocafe/are-these-real-or-just-illusions-well-these-pictures-will-twist-your-mind-244288.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/are-these-real-or-just-illusions-well-these-pictures-will-twist-your-mind-244288.html" title="8 Pictures + 1 Video That Are Trippier Than Anything You Will See This Month. God Promise.">
                            8 Pictures + 1 Video That Are Trippier Than Anything You Will See This Month. God Promise.                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/celebs/neil-nitin-mukesh-teams-up-with-west-indies-cricketer-dwayne-bravo-for-an-international-single-quite-a-unique-combo-244277.html" class="tint" title="Neil Nitin Mukesh Teams Up With West Indies Cricketer Dwayne Bravo For An International Single. Quite A Unique Combo!">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage_1439881156_1439881161_502x234.jpg" border="0" alt="http://www.indiatimes.com/entertainment/celebs/neil-nitin-mukesh-teams-up-with-west-indies-cricketer-dwayne-bravo-for-an-international-single-quite-a-unique-combo-244277.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/neil-nitin-mukesh-teams-up-with-west-indies-cricketer-dwayne-bravo-for-an-international-single-quite-a-unique-combo-244277.html" title="Neil Nitin Mukesh Teams Up With West Indies Cricketer Dwayne Bravo For An International Single. Quite A Unique Combo!">
                            Neil Nitin Mukesh Teams Up With West Indies Cricketer Dwayne Bravo For An International Single. Quite A Unique Combo!                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_10"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/hollywood/11-face-mashups-of-popular-hollywood-celebrities-that-will-add-a-lot-of-heat-to-your-life-244270.html" class="tint" title="11 Face Mashups Of Popular Hollywood Celebrities That Will Add A Lot Of Heat To Your Life">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1439881307_218x102.jpg" border="0" alt="11 Face Mashups Of Popular Hollywood Celebrities That Will Add A Lot Of Heat To Your Life"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/hollywood/11-face-mashups-of-popular-hollywood-celebrities-that-will-add-a-lot-of-heat-to-your-life-244270.html" title="11 Face Mashups Of Popular Hollywood Celebrities That Will Add A Lot Of Heat To Your Life">
                            11 Face Mashups Of Popular Hollywood Celebrities That Will Add A Lot Of Heat To Your Life                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_11"><!--trending-panel-list start-->
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/bored-of-the-same-old-safety-demo-in-flights-check-out-this-flight-attendant-performing-the-same-act-in-a-hilarious-manner-244294.html'>video</a>		

                        <a href="http://www.indiatimes.com/videocafe/bored-of-the-same-old-safety-demo-in-flights-check-out-this-flight-attendant-performing-the-same-act-in-a-hilarious-manner-244294.html" class="tint" title="Bored Of The Same Old Safety Demo In Flights? Check Out This Flight Attendant Performing The Act In A Hilarious Manner!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/flight_attendant_card_1439890992_218x102.jpg" border="0" alt="Bored Of The Same Old Safety Demo In Flights? Check Out This Flight Attendant Performing The Act In A Hilarious Manner!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/bored-of-the-same-old-safety-demo-in-flights-check-out-this-flight-attendant-performing-the-same-act-in-a-hilarious-manner-244294.html" title="Bored Of The Same Old Safety Demo In Flights? Check Out This Flight Attendant Performing The Act In A Hilarious Manner!">
                            Bored Of The Same Old Safety Demo In Flights? Check Out This Flight Attendant Performing The Act In A Hilarious Manner!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_12"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/hollywood/this-picture-of-joey-chandler-shows-how-time-flies-yet-our-love-for-friends-remain-unfazed-244263.html" class="tint" title="This Picture Of Joey & Chandler Shows How Time Flies. Yet Our Love For 'Friends' Remain Unfazed!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collagehh_1439817964_1439817973_218x102.jpg" border="0" alt="This Picture Of Joey & Chandler Shows How Time Flies. Yet Our Love For 'Friends' Remain Unfazed!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/hollywood/this-picture-of-joey-chandler-shows-how-time-flies-yet-our-love-for-friends-remain-unfazed-244263.html" title="This Picture Of Joey & Chandler Shows How Time Flies. Yet Our Love For 'Friends' Remain Unfazed!">
                            This Picture Of Joey & Chandler Shows How Time Flies. Yet Our Love For 'Friends' Remain Unfazed!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_13"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/bollywood/gangster-ravi-pujaris-funny-barter-with-arijit-singh-sing-songs-for-me-or-pay-rs-5-crore-244242.html" class="tint" title="Gangster Ravi Pujari's Funny Barter With Arijit Singh. Sing Songs For Me Or Pay Rs 5 Crore">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/arijit-singh_1439804378_1439804390_218x102.jpg" border="0" alt="Gangster Ravi Pujari's Funny Barter With Arijit Singh. Sing Songs For Me Or Pay Rs 5 Crore"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/gangster-ravi-pujaris-funny-barter-with-arijit-singh-sing-songs-for-me-or-pay-rs-5-crore-244242.html" title="Gangster Ravi Pujari's Funny Barter With Arijit Singh. Sing Songs For Me Or Pay Rs 5 Crore">
                            Gangster Ravi Pujari's Funny Barter With Arijit Singh. Sing Songs For Me Or Pay Rs 5 Crore                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_14"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/culture/who-we-are/how-to-spot-the-thin-line-between-confidence-and-overconfidence-244101.html" class="tint" title="How To Spot The Thin Line Between Confidence And Overconfidence">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/over_1439388446_1439388457_218x102.jpg" border="0" alt="How To Spot The Thin Line Between Confidence And Overconfidence"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/how-to-spot-the-thin-line-between-confidence-and-overconfidence-244101.html" title="How To Spot The Thin Line Between Confidence And Overconfidence">
                            How To Spot The Thin Line Between Confidence And Overconfidence                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div><!--trending-panel end-->

    </section><!--container end-->

<!------------------------------- end Block 3 ---------------------------------------->
<section class="big-ads">
<div id="bigAd3_slot"></div>
</section>

<!------------------------------- start Block4  -------------------------------------->

    <section class="container cf" id="container4"><!--container start-->
        <div class="news-panel cf ">
            <h2>news</h2>

                            <div class="news-panel-list cf" id="column1_14">
                    <figure>
                        <div class="hash-tag">
                            17 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/nasa-finds-huge-cache-of-neon-on-the-moon-heres-to-colonisation-244276.html" title="NASA Finds Huge Cache Of Neon On The Moon! Here's To Colonisation!" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/moon502_1439880018_236x111.jpg" border="0" alt="NASA Finds Huge Cache Of Neon On The Moon! Here's To Colonisation!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/nasa-finds-huge-cache-of-neon-on-the-moon-heres-to-colonisation-244276.html" title="NASA Finds Huge Cache Of Neon On The Moon! Here's To Colonisation!">
                            NASA Finds Huge Cache Of Neon On The Moon! Here's To Colonisation!                        </a>
                    </figcaption> 
                </div>
                <div class='container1'>                <div class="news-panel-list cf" id="column1_15">
                    <figure>
                        <div class="hash-tag">
                            17 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/sports/delhi-girl-aditi-chauhan-becomes-first-indian-to-represent-premier-league-club-in-england-244268.html" title="Delhi Girl Aditi Chauhan Becomes First Indian To Play For A Premier League Side In England" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/aditichauhanfootball_1439834892_236x111.jpg" border="0" alt="Delhi Girl Aditi Chauhan Becomes First Indian To Play For A Premier League Side In England"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/delhi-girl-aditi-chauhan-becomes-first-indian-to-represent-premier-league-club-in-england-244268.html" title="Delhi Girl Aditi Chauhan Becomes First Indian To Play For A Premier League Side In England">
                            Delhi Girl Aditi Chauhan Becomes First Indian To Play For A Premier League Side In England                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_16">
                    <figure>
                        <div class="hash-tag">
                            18 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/parsi-man-declared-brain-dead-wife-donates-organs-to-five-patients-on-parsi-new-years-eve-244273.html" title="Parsi Man Declared Brain Dead, Wife Donates Organs To Five Patients On Parsi New Year's Eve" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/organ-transplant-502_1439878357_236x111.jpg" border="0" alt="Parsi Man Declared Brain Dead, Wife Donates Organs To Five Patients On Parsi New Year's Eve"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/parsi-man-declared-brain-dead-wife-donates-organs-to-five-patients-on-parsi-new-years-eve-244273.html" title="Parsi Man Declared Brain Dead, Wife Donates Organs To Five Patients On Parsi New Year's Eve">
                            Parsi Man Declared Brain Dead, Wife Donates Organs To Five Patients On Parsi New Year's Eve                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_17">
                    <figure>
                        <div class="hash-tag">
                            19 hours ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/desihoppers-dance-their-way-into-our-hearts-and-win-the-world-of-dance-championship-in-la-244271.html" title="'Desihoppers' Dance Their Way Into Our Hearts And Win The 'World of Dance' Championship In LA" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/desi502_1439876741_1439876746_236x111.jpg" border="0" alt="'Desihoppers' Dance Their Way Into Our Hearts And Win The 'World of Dance' Championship In LA"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/desihoppers-dance-their-way-into-our-hearts-and-win-the-world-of-dance-championship-in-la-244271.html" title="'Desihoppers' Dance Their Way Into Our Hearts And Win The 'World of Dance' Championship In LA">
                            'Desihoppers' Dance Their Way Into Our Hearts And Win The 'World of Dance' Championship In LA                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_18">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/the-burj-al-arab-lit-up-in-tiranga-colours-might-have-been-fake-but-check-out-the-pictures-of-empire-state-building-244267.html" title="The Burj Al Arab Lit Up In Tiranga Colours Might Have Been Fake, But Check Out The Pictures Of Empire State Building!" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/empire502_1439832259_236x111.jpg" border="0" alt="The Burj Al Arab Lit Up In Tiranga Colours Might Have Been Fake, But Check Out The Pictures Of Empire State Building!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/the-burj-al-arab-lit-up-in-tiranga-colours-might-have-been-fake-but-check-out-the-pictures-of-empire-state-building-244267.html" title="The Burj Al Arab Lit Up In Tiranga Colours Might Have Been Fake, But Check Out The Pictures Of Empire State Building!">
                            The Burj Al Arab Lit Up In Tiranga Colours Might Have Been Fake, But Check Out The Pictures Of Empire State Building!                        </a>
                    </figcaption> 
                </div>
                 
        </div>
    </div><!--news-panel end-->

    <div class="life-panel cf container2" id="b4c2"><!--life-panel start-->
        <h2>lifestyle</h2>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/playing-their-favourite-music-helps-patients-recover-faster-from-surgery-244169.html" class="tint" title="Playing Their Favourite Music Helps Patients Recover Faster From Surgery">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/musiccover_1439544258_502x234.jpg" border="0" alt="Playing Their Favourite Music Helps Patients Recover Faster From Surgery" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/playing-their-favourite-music-helps-patients-recover-faster-from-surgery-244169.html" title="Playing Their Favourite Music Helps Patients Recover Faster From Surgery">
                        Playing Their Favourite Music Helps Patients Recover Faster From Surgery                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/this-cartoon-strip-is-the-perfect-tribute-to-dr-apjabdul-kalam-244279.html" class="tint" title="This Cartoon Strip Is The Perfect Tribute To Dr A.P.J.Abdul Kalam">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/aa_1439897542_1439897553_502x234.jpg" border="0" alt="This Cartoon Strip Is The Perfect Tribute To Dr A.P.J.Abdul Kalam" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/this-cartoon-strip-is-the-perfect-tribute-to-dr-apjabdul-kalam-244279.html" title="This Cartoon Strip Is The Perfect Tribute To Dr A.P.J.Abdul Kalam">
                        This Cartoon Strip Is The Perfect Tribute To Dr A.P.J.Abdul Kalam                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/hollywood/11-face-mashups-of-popular-hollywood-celebrities-that-will-add-a-lot-of-heat-to-your-life-244270.html" class="tint" title="11 Face Mashups Of Popular Hollywood Celebrities That Will Add A Lot Of Heat To Your Life">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1439881307_502x234.jpg" border="0" alt="11 Face Mashups Of Popular Hollywood Celebrities That Will Add A Lot Of Heat To Your Life" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/hollywood/11-face-mashups-of-popular-hollywood-celebrities-that-will-add-a-lot-of-heat-to-your-life-244270.html" title="11 Face Mashups Of Popular Hollywood Celebrities That Will Add A Lot Of Heat To Your Life">
                        11 Face Mashups Of Popular Hollywood Celebrities That Will Add A Lot Of Heat To Your Life                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/radhe-maa-drama-continues-sonu-nigam-lands-in-trouble-for-defending-the-selfstyled-godwoman-244269.html" class="tint" title="Radhe Maa Drama Continues, Sonu Nigam Lands In Trouble For Defending The Self-Styled Godwoman">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/sonu-nigam-news_1439876435_1439876451_502x234.jpg" border="0" alt="Radhe Maa Drama Continues, Sonu Nigam Lands In Trouble For Defending The Self-Styled Godwoman" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/radhe-maa-drama-continues-sonu-nigam-lands-in-trouble-for-defending-the-selfstyled-godwoman-244269.html" title="Radhe Maa Drama Continues, Sonu Nigam Lands In Trouble For Defending The Self-Styled Godwoman">
                        Radhe Maa Drama Continues, Sonu Nigam Lands In Trouble For Defending The Self-Styled Godwoman                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/recipes/9-quick-bhindi-recipes-ideas-for-a-healthy-tiffin-244157.html" class="tint" title="9 Quick Bhindi Recipe Ideas For A Healthy Tiffin">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/bhindi-card_1439536960_502x234.gif" border="0" alt="9 Quick Bhindi Recipe Ideas For A Healthy Tiffin" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/recipes/9-quick-bhindi-recipes-ideas-for-a-healthy-tiffin-244157.html" title="9 Quick Bhindi Recipe Ideas For A Healthy Tiffin">
                        9 Quick Bhindi Recipe Ideas For A Healthy Tiffin                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/specials/transgender-project/once-a-train-beggar-she-won-the-mayoral-elections-in-rs-80000-now-indias-first-transgender-mayor-speaks-to-india-and-shes-so-humble-244156.html" class="tint" title="A Transgender Beggar Won The Mayoral Elections, And She's Still Humble">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/cardimg1_1439550691_502x234.jpg" border="0" alt="A Transgender Beggar Won The Mayoral Elections, And She's Still Humble" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/specials/transgender-project/once-a-train-beggar-she-won-the-mayoral-elections-in-rs-80000-now-indias-first-transgender-mayor-speaks-to-india-and-shes-so-humble-244156.html" title="A Transgender Beggar Won The Mayoral Elections, And She's Still Humble">
                        A Transgender Beggar Won The Mayoral Elections, And She's Still Humble                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/dashrath-manjhi-aka-mountain-mans-story-is-the-most-inspiring-thing-youll-read-today-243475.html" class="tint" title="Dashrath Manjhi Aka Mountain Man's Story Is The Most Inspiring Thing You'll Read Today">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Jul/picmonkey-collage_1437987155_1437987160_502x234.jpg" border="0" alt="Dashrath Manjhi Aka Mountain Man's Story Is The Most Inspiring Thing You'll Read Today" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/dashrath-manjhi-aka-mountain-mans-story-is-the-most-inspiring-thing-youll-read-today-243475.html" title="Dashrath Manjhi Aka Mountain Man's Story Is The Most Inspiring Thing You'll Read Today">
                        Dashrath Manjhi Aka Mountain Man's Story Is The Most Inspiring Thing You'll Read Today                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/do-your-dreams-really-represent-your-deepest-darkest-desires-or-something-else-entirely-244031.html" class="tint" title="Do Your Dreams Really Represent Your Deepest, Darkest Desires? Or Something Else Entirely?">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/dream_1439284061_502x234.jpg" border="0" alt="Do Your Dreams Really Represent Your Deepest, Darkest Desires? Or Something Else Entirely?" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/do-your-dreams-really-represent-your-deepest-darkest-desires-or-something-else-entirely-244031.html" title="Do Your Dreams Really Represent Your Deepest, Darkest Desires? Or Something Else Entirely?">
                        Do Your Dreams Really Represent Your Deepest, Darkest Desires? Or Something Else Entirely?                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/hollywood/this-picture-of-joey-chandler-shows-how-time-flies-yet-our-love-for-friends-remain-unfazed-244263.html" class="tint" title="This Picture Of Joey & Chandler Shows How Time Flies. Yet Our Love For 'Friends' Remain Unfazed!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collagehh_1439817964_1439817973_502x234.jpg" border="0" alt="This Picture Of Joey & Chandler Shows How Time Flies. Yet Our Love For 'Friends' Remain Unfazed!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/hollywood/this-picture-of-joey-chandler-shows-how-time-flies-yet-our-love-for-friends-remain-unfazed-244263.html" title="This Picture Of Joey & Chandler Shows How Time Flies. Yet Our Love For 'Friends' Remain Unfazed!">
                        This Picture Of Joey & Chandler Shows How Time Flies. Yet Our Love For 'Friends' Remain Unfazed!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/millions-of-farmers-in-india-are-living-their-lives-in-fear-youll-be-shocked-to-know-why-244258.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/millions-of-farmers-in-india-are-living-their-lives-in-fear-youll-be-shocked-to-know-why-244258.html" class="tint" title="Millions Of Farmers In India Are Living Their Lives In Fear. You'll Be Shocked To Know Why">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/farmers_card_1439812603_502x234.jpg" border="0" alt="Millions Of Farmers In India Are Living Their Lives In Fear. You'll Be Shocked To Know Why" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/millions-of-farmers-in-india-are-living-their-lives-in-fear-youll-be-shocked-to-know-why-244258.html" title="Millions Of Farmers In India Are Living Their Lives In Fear. You'll Be Shocked To Know Why">
                        Millions Of Farmers In India Are Living Their Lives In Fear. You'll Be Shocked To Know Why                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/rofl-comedian-saloni-turns-into-ekta-kapoor-and-hits-out-at-her-father-brother-teacher-and-tv-stars-244251.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/rofl-comedian-saloni-turns-into-ekta-kapoor-and-hits-out-at-her-father-brother-teacher-and-tv-stars-244251.html" class="tint" title="ROFL! Comedian Saloni Turns Into Ekta Kapoor And Hits Out At Her Father, Brother, Teacher And TV Stars!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/ektakapoor_card_1439809058_502x234.jpg" border="0" alt="ROFL! Comedian Saloni Turns Into Ekta Kapoor And Hits Out At Her Father, Brother, Teacher And TV Stars!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/rofl-comedian-saloni-turns-into-ekta-kapoor-and-hits-out-at-her-father-brother-teacher-and-tv-stars-244251.html" title="ROFL! Comedian Saloni Turns Into Ekta Kapoor And Hits Out At Her Father, Brother, Teacher And TV Stars!">
                        ROFL! Comedian Saloni Turns Into Ekta Kapoor And Hits Out At Her Father, Brother, Teacher And TV Stars!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/gangster-ravi-pujaris-funny-barter-with-arijit-singh-sing-songs-for-me-or-pay-rs-5-crore-244242.html" class="tint" title="Gangster Ravi Pujari's Funny Barter With Arijit Singh. Sing Songs For Me Or Pay Rs 5 Crore">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/arijit-singh_1439804378_1439804390_502x234.jpg" border="0" alt="Gangster Ravi Pujari's Funny Barter With Arijit Singh. Sing Songs For Me Or Pay Rs 5 Crore" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/gangster-ravi-pujaris-funny-barter-with-arijit-singh-sing-songs-for-me-or-pay-rs-5-crore-244242.html" title="Gangster Ravi Pujari's Funny Barter With Arijit Singh. Sing Songs For Me Or Pay Rs 5 Crore">
                        Gangster Ravi Pujari's Funny Barter With Arijit Singh. Sing Songs For Me Or Pay Rs 5 Crore                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/health/tips-tricks/ladies-are-you-wearing-your-bra-correctly-244257.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/ladies-are-you-wearing-your-bra-correctly-244257.html" class="tint" title="Ladies, Are You Wearing Your Bra Correctly?">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/bra_1439811340_502x234.jpg" border="0" alt="Ladies, Are You Wearing Your Bra Correctly?" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/ladies-are-you-wearing-your-bra-correctly-244257.html" title="Ladies, Are You Wearing Your Bra Correctly?">
                        Ladies, Are You Wearing Your Bra Correctly?                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/these-24-news-bloopers-are-so-hilarious-that-they-will-make-you-laugh-so-hard-youll-cry-244250.html" class="tint" title="These 24 News Bloopers Are So Hilarious That They Will Make You Laugh So Hard You'll Cry!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/newsbloopers_card_1439807938_502x234.jpg" border="0" alt="These 24 News Bloopers Are So Hilarious That They Will Make You Laugh So Hard You'll Cry!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/these-24-news-bloopers-are-so-hilarious-that-they-will-make-you-laugh-so-hard-youll-cry-244250.html" title="These 24 News Bloopers Are So Hilarious That They Will Make You Laugh So Hard You'll Cry!">
                        These 24 News Bloopers Are So Hilarious That They Will Make You Laugh So Hard You'll Cry!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/recipes/4-healthy-recipes-that-will-make-you-a-fan-of-parsi-food-243986.html" class="tint" title="4 Healthy Recipes That Will Make You A Fan Of Parsi Food">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/untitled-1_1439810061_502x234.gif" border="0" alt="4 Healthy Recipes That Will Make You A Fan Of Parsi Food" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/recipes/4-healthy-recipes-that-will-make-you-a-fan-of-parsi-food-243986.html" title="4 Healthy Recipes That Will Make You A Fan Of Parsi Food">
                        4 Healthy Recipes That Will Make You A Fan Of Parsi Food                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/aashiqui-actress-anu-aggarwals-biography-is-an-anusual-memoir-on-her-neardeath-experience-more-244239.html" class="tint" title="Aashiqui Actress Anu Aggarwal's Biography Is An 'Anusual' Memoir On Her Near-Death Experience & More">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/anu_1439798466_1439798494_502x234.jpg" border="0" alt="Aashiqui Actress Anu Aggarwal's Biography Is An 'Anusual' Memoir On Her Near-Death Experience & More" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/aashiqui-actress-anu-aggarwals-biography-is-an-anusual-memoir-on-her-neardeath-experience-more-244239.html" title="Aashiqui Actress Anu Aggarwal's Biography Is An 'Anusual' Memoir On Her Near-Death Experience & More">
                        Aashiqui Actress Anu Aggarwal's Biography Is An 'Anusual' Memoir On Her Near-Death Experience & More                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/jeff-bezos-writes-a-memo-to-amazon-staff-calls-nyt-article-biased-full-of-half-truths-244249.html" class="tint" title="Jeff Bezos Writes A Memo To Amazon Staff, Calls NYT Article 'Biased' & 'Full Of Half Truths'!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/jeff502_1439806464_502x234.jpg" border="0" alt="Jeff Bezos Writes A Memo To Amazon Staff, Calls NYT Article 'Biased' & 'Full Of Half Truths'!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/jeff-bezos-writes-a-memo-to-amazon-staff-calls-nyt-article-biased-full-of-half-truths-244249.html" title="Jeff Bezos Writes A Memo To Amazon Staff, Calls NYT Article 'Biased' & 'Full Of Half Truths'!">
                        Jeff Bezos Writes A Memo To Amazon Staff, Calls NYT Article 'Biased' & 'Full Of Half Truths'!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/9-indian-tv-shows-you-must-check-out-in-the-second-half-of-2015_-244233.html" class="tint" title="9 Indian TV Shows You Must Check Out In The Second Half Of 2015!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage_1439794209_1439794219_502x234.jpg" border="0" alt="9 Indian TV Shows You Must Check Out In The Second Half Of 2015!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/9-indian-tv-shows-you-must-check-out-in-the-second-half-of-2015_-244233.html" title="9 Indian TV Shows You Must Check Out In The Second Half Of 2015!">
                        9 Indian TV Shows You Must Check Out In The Second Half Of 2015!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/hollywood/oscarwinning-actor-morgan-freemans-stepgranddaughter-mercilessly-stabbed-to-death-outside-her-newyork-appartment-244241.html" class="tint" title="Oscar-Winning Actor Morgan Freeman's Step-Granddaughter Mercilessly Stabbed To Death Outside Her New-York Appartment">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/ss_1439803121_1439803135_502x234.jpg" border="0" alt="Oscar-Winning Actor Morgan Freeman's Step-Granddaughter Mercilessly Stabbed To Death Outside Her New-York Appartment" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/hollywood/oscarwinning-actor-morgan-freemans-stepgranddaughter-mercilessly-stabbed-to-death-outside-her-newyork-appartment-244241.html" title="Oscar-Winning Actor Morgan Freeman's Step-Granddaughter Mercilessly Stabbed To Death Outside Her New-York Appartment">
                        Oscar-Winning Actor Morgan Freeman's Step-Granddaughter Mercilessly Stabbed To Death Outside Her New-York Appartment                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/ladies-you-have-unknowingly-been-paying-makeup-tax-244130.html" class="tint" title="Ladies, You Have Unknowingly Been Paying Makeup Tax!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/makeupcover_1439459949_502x234.jpg" border="0" alt="Ladies, You Have Unknowingly Been Paying Makeup Tax!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/ladies-you-have-unknowingly-been-paying-makeup-tax-244130.html" title="Ladies, You Have Unknowingly Been Paying Makeup Tax!">
                        Ladies, You Have Unknowingly Been Paying Makeup Tax!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/an-inspiring-short-film-that-shows-every-child-deserves-an-education-244231.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/an-inspiring-short-film-that-shows-every-child-deserves-an-education-244231.html" class="tint" title="An Inspiring Short Film That Shows Every Child Deserves An Education!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/tshirt_card_1439794498_502x234.jpg" border="0" alt="An Inspiring Short Film That Shows Every Child Deserves An Education!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/an-inspiring-short-film-that-shows-every-child-deserves-an-education-244231.html" title="An Inspiring Short Film That Shows Every Child Deserves An Education!">
                        An Inspiring Short Film That Shows Every Child Deserves An Education!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/patriotism-backfires-for-shahid-kapoor-aditi-rao-hydari-as-they-go-horribly-wrong-with-saluteselfies-244232.html" class="tint" title="Patriotism Backfires For Shahid Kapoor, Aditi Rao Hydari As They Go Horribly Wrong With #SaluteSelfies!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card_1439794590_1439794596_502x234.jpg" border="0" alt="Patriotism Backfires For Shahid Kapoor, Aditi Rao Hydari As They Go Horribly Wrong With #SaluteSelfies!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/patriotism-backfires-for-shahid-kapoor-aditi-rao-hydari-as-they-go-horribly-wrong-with-saluteselfies-244232.html" title="Patriotism Backfires For Shahid Kapoor, Aditi Rao Hydari As They Go Horribly Wrong With #SaluteSelfies!">
                        Patriotism Backfires For Shahid Kapoor, Aditi Rao Hydari As They Go Horribly Wrong With #SaluteSelfies!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/lifestyle/technology/nasa-just-tested-the-rocket-that-will-take-man-to-mars-visuals-obviously-are-stunning-244230.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/nasa-just-tested-the-rocket-that-will-take-man-to-mars-visuals-obviously-are-stunning-244230.html" class="tint" title="NASA Just Tested The Rocket That Will Take Man To Mars. Visuals Obviously Are Stunning">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/picmonkey-collage502_1439794234_1439794243_502x234.jpg" border="0" alt="NASA Just Tested The Rocket That Will Take Man To Mars. Visuals Obviously Are Stunning" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/nasa-just-tested-the-rocket-that-will-take-man-to-mars-visuals-obviously-are-stunning-244230.html" title="NASA Just Tested The Rocket That Will Take Man To Mars. Visuals Obviously Are Stunning">
                        NASA Just Tested The Rocket That Will Take Man To Mars. Visuals Obviously Are Stunning                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/sonu-nigams-rant-on-radhe-maa-is-the-most-sensible-thing-youll-read-on-the-web-today-244228.html" class="tint" title="Sonu Nigam's Rant On Radhe Maa Is The Most LOL-Worthy Thing You'll Read Today">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/sonu-nigam-card_1439789712_1439789743_502x234.jpg" border="0" alt="Sonu Nigam's Rant On Radhe Maa Is The Most LOL-Worthy Thing You'll Read Today" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/sonu-nigams-rant-on-radhe-maa-is-the-most-sensible-thing-youll-read-on-the-web-today-244228.html" title="Sonu Nigam's Rant On Radhe Maa Is The Most LOL-Worthy Thing You'll Read Today">
                        Sonu Nigam's Rant On Radhe Maa Is The Most LOL-Worthy Thing You'll Read Today                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/food/10-diplomatic-feuds-that-were-brought-about-in-the-name-of-food-244068.html" class="tint" title="10 Diplomatic Feuds That Were Brought About In The Name Of Food">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/cp_1439384099_502x234.jpg" border="0" alt="10 Diplomatic Feuds That Were Brought About In The Name Of Food" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/food/10-diplomatic-feuds-that-were-brought-about-in-the-name-of-food-244068.html" title="10 Diplomatic Feuds That Were Brought About In The Name Of Food">
                        10 Diplomatic Feuds That Were Brought About In The Name Of Food                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/these-19-foods-will-help-you-put-on-weight-the-healthy-way-244090.html" class="tint" title="These 19 Foods Will Help You Put On Weight The Healthy Way">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/weight-gain-card_1439809701_502x234.gif" border="0" alt="These 19 Foods Will Help You Put On Weight The Healthy Way" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/these-19-foods-will-help-you-put-on-weight-the-healthy-way-244090.html" title="These 19 Foods Will Help You Put On Weight The Healthy Way">
                        These 19 Foods Will Help You Put On Weight The Healthy Way                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                                                <a href="http://www.indiatimes.com/specials/transgender-project/she-was-humiliated-barred-from-cwg-and-branded-a-man-heres-how-indian-sprinter-bounced-back-244166.html" class="awesome sticker">&nbsp;</a>
                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/specials/transgender-project/she-was-humiliated-barred-from-cwg-and-branded-a-man-heres-how-indian-sprinter-bounced-back-244166.html" class="tint" title="She Was Humiliated, Barred From CWG And Branded 'A Man'. Here's How Indian Sprinter Bounced Back!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/duteechandtransgender_1439546277_502x234.jpg" border="0" alt="She Was Humiliated, Barred From CWG And Branded 'A Man'. Here's How Indian Sprinter Bounced Back!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/specials/transgender-project/she-was-humiliated-barred-from-cwg-and-branded-a-man-heres-how-indian-sprinter-bounced-back-244166.html" title="She Was Humiliated, Barred From CWG And Branded 'A Man'. Here's How Indian Sprinter Bounced Back!">
                        She Was Humiliated, Barred From CWG And Branded 'A Man'. Here's How Indian Sprinter Bounced Back!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/how-to-spot-the-thin-line-between-confidence-and-overconfidence-244101.html" class="tint" title="How To Spot The Thin Line Between Confidence And Overconfidence">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/over_1439388446_1439388457_502x234.jpg" border="0" alt="How To Spot The Thin Line Between Confidence And Overconfidence" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/how-to-spot-the-thin-line-between-confidence-and-overconfidence-244101.html" title="How To Spot The Thin Line Between Confidence And Overconfidence">
                        How To Spot The Thin Line Between Confidence And Overconfidence                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/studies-show-that-sleeping-on-your-side-could-reduce-the-risk-of-alzheimers-243990.html" class="tint" title="Studies Show That Sleeping On Your Side Could Reduce The Risk Of Alzheimer's">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/shutterstock_156320471_1439191671_1439191676_502x234.jpg" border="0" alt="Studies Show That Sleeping On Your Side Could Reduce The Risk Of Alzheimer's" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/studies-show-that-sleeping-on-your-side-could-reduce-the-risk-of-alzheimers-243990.html" title="Studies Show That Sleeping On Your Side Could Reduce The Risk Of Alzheimer's">
                        Studies Show That Sleeping On Your Side Could Reduce The Risk Of Alzheimer's                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/27-things-that-perfectly-reflect-our-daily-experiences-244193.html" class="tint" title="27 Things That Perfectly Reflect Our Daily Experiences">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/cp_1439617995_502x234.jpg" border="0" alt="27 Things That Perfectly Reflect Our Daily Experiences" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/27-things-that-perfectly-reflect-our-daily-experiences-244193.html" title="27 Things That Perfectly Reflect Our Daily Experiences">
                        27 Things That Perfectly Reflect Our Daily Experiences                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/plastic-is-killing-the-earth-one-species-at-a-time-244221.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/plastic-is-killing-the-earth-one-species-at-a-time-244221.html" class="tint" title="Plastic Is Killing The Earth, One Species At A Time">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/turle_card_1439723067_502x234.jpg" border="0" alt="Plastic Is Killing The Earth, One Species At A Time" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/plastic-is-killing-the-earth-one-species-at-a-time-244221.html" title="Plastic Is Killing The Earth, One Species At A Time">
                        Plastic Is Killing The Earth, One Species At A Time                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/food/7-apple-recipes-that-are-guaranteed-to-impress-at-your-next-dinner-party-244081.html" class="tint" title="7 Apple Recipes That Are Guaranteed To Impress At Your Next Dinner Party">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/shutterstock_190676684_1439370336_1439370342_502x234.jpg" border="0" alt="7 Apple Recipes That Are Guaranteed To Impress At Your Next Dinner Party" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/food/7-apple-recipes-that-are-guaranteed-to-impress-at-your-next-dinner-party-244081.html" title="7 Apple Recipes That Are Guaranteed To Impress At Your Next Dinner Party">
                        7 Apple Recipes That Are Guaranteed To Impress At Your Next Dinner Party                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-version-of-mere-samne-wali-khidki-me-shows-similarities-between-people-of-india-and-pakistan-244215.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-version-of-mere-samne-wali-khidki-me-shows-similarities-between-people-of-india-and-pakistan-244215.html" class="tint" title="This Version Of 'Mere Samne Wali Khidki Me' Shows Similarities Between People Of India And Pakistan">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/indopak_card_1439726817_502x234.jpg" border="0" alt="This Version Of 'Mere Samne Wali Khidki Me' Shows Similarities Between People Of India And Pakistan" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-version-of-mere-samne-wali-khidki-me-shows-similarities-between-people-of-india-and-pakistan-244215.html" title="This Version Of 'Mere Samne Wali Khidki Me' Shows Similarities Between People Of India And Pakistan">
                        This Version Of 'Mere Samne Wali Khidki Me' Shows Similarities Between People Of India And Pakistan                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/nawazuddin-siddiqui-gives-a-beautiful-message-to-the-nation-this-independence-day-244217.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/nawazuddin-siddiqui-gives-a-beautiful-message-to-the-nation-this-independence-day-244217.html" class="tint" title="Nawazuddin Siddiqui Gives A Beautiful Message To The Nation This Independence Day">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/manjhi_card_1439717617_502x234.jpg" border="0" alt="Nawazuddin Siddiqui Gives A Beautiful Message To The Nation This Independence Day" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/nawazuddin-siddiqui-gives-a-beautiful-message-to-the-nation-this-independence-day-244217.html" title="Nawazuddin Siddiqui Gives A Beautiful Message To The Nation This Independence Day">
                        Nawazuddin Siddiqui Gives A Beautiful Message To The Nation This Independence Day                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/i-forgot-my-phone-in-a-cab-what-the-drivers-did-next-shocked-me-244072.html" class="tint" title="I Forgot My Phone In A Cab, What The Drivers Did Next Shocked Me">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/taxi-card_1439365091_1439365099_502x234.gif" border="0" alt="I Forgot My Phone In A Cab, What The Drivers Did Next Shocked Me" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/i-forgot-my-phone-in-a-cab-what-the-drivers-did-next-shocked-me-244072.html" title="I Forgot My Phone In A Cab, What The Drivers Did Next Shocked Me">
                        I Forgot My Phone In A Cab, What The Drivers Did Next Shocked Me                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/10-exciting-things-you-can-do-to-not-get-bored-on-a-lazy-day-244140.html" class="tint" title="10 Exciting Things You Can Do To Not Get Bored On A Lazy Day">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/cp_1439468522_502x234.jpg" border="0" alt="10 Exciting Things You Can Do To Not Get Bored On A Lazy Day" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/10-exciting-things-you-can-do-to-not-get-bored-on-a-lazy-day-244140.html" title="10 Exciting Things You Can Do To Not Get Bored On A Lazy Day">
                        10 Exciting Things You Can Do To Not Get Bored On A Lazy Day                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/10-ways-sugar-sweetly-but-surely-hurts-your-health-244124.html" class="tint" title="10 Ways Sugar Sweetly But Surely Hurts Your Health">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/candy_1439455787_502x234.jpg" border="0" alt="10 Ways Sugar Sweetly But Surely Hurts Your Health" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/10-ways-sugar-sweetly-but-surely-hurts-your-health-244124.html" title="10 Ways Sugar Sweetly But Surely Hurts Your Health">
                        10 Ways Sugar Sweetly But Surely Hurts Your Health                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/this-open-letter-to-india-from-a-24-year-old-french-man-is-the-most-sensible-thing-youll-read-today-244213.html" class="tint" title="This Open Letter To India From A  24 Year Old French Man Is The Most Sensible Thing You'll Read Today">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/3_1439709497_1439709500_502x234.jpg" border="0" alt="This Open Letter To India From A  24 Year Old French Man Is The Most Sensible Thing You'll Read Today" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/this-open-letter-to-india-from-a-24-year-old-french-man-is-the-most-sensible-thing-youll-read-today-244213.html" title="This Open Letter To India From A  24 Year Old French Man Is The Most Sensible Thing You'll Read Today">
                        This Open Letter To India From A  24 Year Old French Man Is The Most Sensible Thing You'll Read Today                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/bhai-does-it-again-salmans-bajrangi-bhaijaan-is-crushing-all-boxoffice-records-235019.html" class="tint" title="Salman's 'Bajrangi Bhaijaan' Still Going Strong. Sets Rs 600 Crore Record!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/bb1_1439707090_1439707097_502x234.jpg" border="0" alt="Salman's 'Bajrangi Bhaijaan' Still Going Strong. Sets Rs 600 Crore Record!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/bhai-does-it-again-salmans-bajrangi-bhaijaan-is-crushing-all-boxoffice-records-235019.html" title="Salman's 'Bajrangi Bhaijaan' Still Going Strong. Sets Rs 600 Crore Record!">
                        Salman's 'Bajrangi Bhaijaan' Still Going Strong. Sets Rs 600 Crore Record!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/selfie-emerges-as-the-way-of-showing-patriotism-this-independence-day-244209.html" class="tint" title="Selfie Emerges As The Way Of Showing Patriotism This Independence Day">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage1_1439704786_1439704800_502x234.jpg" border="0" alt="Selfie Emerges As The Way Of Showing Patriotism This Independence Day" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/selfie-emerges-as-the-way-of-showing-patriotism-this-independence-day-244209.html" title="Selfie Emerges As The Way Of Showing Patriotism This Independence Day">
                        Selfie Emerges As The Way Of Showing Patriotism This Independence Day                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/king-of-punjabi-folk-music-gurdas-maan-debuts-on-mtv-coke-studio-244211.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/king-of-punjabi-folk-music-gurdas-maan-debuts-on-mtv-coke-studio-244211.html" class="tint" title="This Song Is Precisely The Reason Gurdas Maan Is Called The Daddy Of Punjabi Folk Music!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/gm1_1439708071_1439708075_502x234.jpg" border="0" alt="This Song Is Precisely The Reason Gurdas Maan Is Called The Daddy Of Punjabi Folk Music!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/king-of-punjabi-folk-music-gurdas-maan-debuts-on-mtv-coke-studio-244211.html" title="This Song Is Precisely The Reason Gurdas Maan Is Called The Daddy Of Punjabi Folk Music!">
                        This Song Is Precisely The Reason Gurdas Maan Is Called The Daddy Of Punjabi Folk Music!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/what-happens-when-a-homeless-man-offers-money-to-the-passersby-watch-this-video-to-find-out-some-of-the-most-shocking-reactions-243972.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/what-happens-when-a-homeless-man-offers-money-to-the-passersby-watch-this-video-to-find-out-some-of-the-most-shocking-reactions-243972.html" class="tint" title="What Happens When A Homeless Man Offers Money To The Passersby? This Video Shows Some Really Shocking Reactions!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/homeless_card_1439112213_502x234.jpg" border="0" alt="What Happens When A Homeless Man Offers Money To The Passersby? This Video Shows Some Really Shocking Reactions!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/what-happens-when-a-homeless-man-offers-money-to-the-passersby-watch-this-video-to-find-out-some-of-the-most-shocking-reactions-243972.html" title="What Happens When A Homeless Man Offers Money To The Passersby? This Video Shows Some Really Shocking Reactions!">
                        What Happens When A Homeless Man Offers Money To The Passersby? This Video Shows Some Really Shocking Reactions!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/food/11-foods-that-can-figuratively-last-a-lifetime-244114.html" class="tint" title="11 Foods That Can Figuratively Last A Lifetime">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/15723388333_fcde11168d_z_1439463303_502x234.jpg" border="0" alt="11 Foods That Can Figuratively Last A Lifetime" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/food/11-foods-that-can-figuratively-last-a-lifetime-244114.html" title="11 Foods That Can Figuratively Last A Lifetime">
                        11 Foods That Can Figuratively Last A Lifetime                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/11-roles-which-proved-to-be-the-turning-point-in-these-actors-bollywood-careers-244112.html" class="tint" title="11 Roles Which Proved To Be The Turning Point In These Actors' Bollywood Careers">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/saif-ali_1439449030_1439449038_502x234.jpg" border="0" alt="11 Roles Which Proved To Be The Turning Point In These Actors' Bollywood Careers" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/11-roles-which-proved-to-be-the-turning-point-in-these-actors-bollywood-careers-244112.html" title="11 Roles Which Proved To Be The Turning Point In These Actors' Bollywood Careers">
                        11 Roles Which Proved To Be The Turning Point In These Actors' Bollywood Careers                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/12-surya-namaskar-steps-you-should-practice-every-morning-to-kill-that-fat-244107.html" class="tint" title="12 Surya Namaskar Steps You Should Practice Every Morning To Kill That Fat">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/shutterstock_502_1439447259_502x234.jpg" border="0" alt="12 Surya Namaskar Steps You Should Practice Every Morning To Kill That Fat" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/12-surya-namaskar-steps-you-should-practice-every-morning-to-kill-that-fat-244107.html" title="12 Surya Namaskar Steps You Should Practice Every Morning To Kill That Fat">
                        12 Surya Namaskar Steps You Should Practice Every Morning To Kill That Fat                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/mary-roach-takes-you-through-10-things-you-didnât-know-about-the-orgasm-243747.html" class="tint" title="Mary Roach Takes You Through 10 Things You Didnât Know About The Orgasm">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/shutterstock_253521151_1438589688_1438589697_502x234.jpg" border="0" alt="Mary Roach Takes You Through 10 Things You Didnât Know About The Orgasm" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/mary-roach-takes-you-through-10-things-you-didnât-know-about-the-orgasm-243747.html" title="Mary Roach Takes You Through 10 Things You Didnât Know About The Orgasm">
                        Mary Roach Takes You Through 10 Things You Didnât Know About The Orgasm                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/11-things-india-needs-to-introspect-about-this-independence-day-244185.html" class="tint" title="11 Things India Needs To Introspect About This Independence Day">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/india_1439562369_1439562398_502x234.jpg" border="0" alt="11 Things India Needs To Introspect About This Independence Day" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/11-things-india-needs-to-introspect-about-this-independence-day-244185.html" title="11 Things India Needs To Introspect About This Independence Day">
                        11 Things India Needs To Introspect About This Independence Day                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-parody-of-yeh-jo-des-hai-tera-depicts-exactly-whats-wrong-in-india-today-244188.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-parody-of-yeh-jo-des-hai-tera-depicts-exactly-whats-wrong-in-india-today-244188.html" class="tint" title="This Parody Of 'Yeh Jo Des Hai Tera' Depicts Exactly What's Wrong In India Today">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/capture_1439643426_1439643433_502x234.jpg" border="0" alt="This Parody Of 'Yeh Jo Des Hai Tera' Depicts Exactly What's Wrong In India Today" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-parody-of-yeh-jo-des-hai-tera-depicts-exactly-whats-wrong-in-india-today-244188.html" title="This Parody Of 'Yeh Jo Des Hai Tera' Depicts Exactly What's Wrong In India Today">
                        This Parody Of 'Yeh Jo Des Hai Tera' Depicts Exactly What's Wrong In India Today                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/15-facts-about-indian-independence-every-indian-need-to-know-244118.html" class="tint" title="16 Facts About Indian Independence Every Indian Needs To Know">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/card2_1439464622_502x234.jpg" border="0" alt="16 Facts About Indian Independence Every Indian Needs To Know" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/15-facts-about-indian-independence-every-indian-need-to-know-244118.html" title="16 Facts About Indian Independence Every Indian Needs To Know">
                        16 Facts About Indian Independence Every Indian Needs To Know                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/food/8-food-labelling-facts-we-can-all-learn-to-be-healthier-people-244182.html" class="tint" title="8 Secrets Food Labels Are Keeping From You - Deciphered!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/cp_1439557550_502x234.jpg" border="0" alt="8 Secrets Food Labels Are Keeping From You - Deciphered!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/food/8-food-labelling-facts-we-can-all-learn-to-be-healthier-people-244182.html" title="8 Secrets Food Labels Are Keeping From You - Deciphered!">
                        8 Secrets Food Labels Are Keeping From You - Deciphered!                    </a>
                </figcaption>
            </div>
        
        <script id="HpMiddleBlock_tpl" type="text/x-handlebars-template">
            {{#if MoreData}}
            {{#each MoreData}} 
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>


                    <a  class='video-btn sprite' href="{{guid}}" style="display:{{card_id}}"> </a>

    				{{#if label_name.length}}
                    <a href="{{guid}}" class="{{label_name}} sticker">&nbsp;</a>
    						 {{/if}} 
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="{{guid}}" title="{{carousal_headline}}" class="tint">
                        <img class="greyBg lazy" data-original="{{thumbnail}}" border="0" alt="{{carousal_headline}}" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="{{guid}}" title="{{carousal_headline}}">
                        {{carousal_headline}}
                    </a>
                </figcaption>
            </div>  
            {{/each}} 
            {{/if}}

            </script>

        </div><!--life-panel end-->

        <div class="trending-panel cf "><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_15"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/health/recipes/4-healthy-recipes-that-will-make-you-a-fan-of-parsi-food-243986.html" class="tint" title="4 Healthy Recipes That Will Make You A Fan Of Parsi Food">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/untitled-1_1439810061_218x102.gif" border="0" alt="4 Healthy Recipes That Will Make You A Fan Of Parsi Food"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/recipes/4-healthy-recipes-that-will-make-you-a-fan-of-parsi-food-243986.html" title="4 Healthy Recipes That Will Make You A Fan Of Parsi Food">
                            4 Healthy Recipes That Will Make You A Fan Of Parsi Food                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                <div class='container3'>                <div class="trending-panel-list cf" id="column3_16"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/health/healthyliving/do-your-dreams-really-represent-your-deepest-darkest-desires-or-something-else-entirely-244031.html" class="tint" title="Do Your Dreams Really Represent Your Deepest, Darkest Desires? Or Something Else Entirely?">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/dream_1439284061_218x102.jpg" border="0" alt="Do Your Dreams Really Represent Your Deepest, Darkest Desires? Or Something Else Entirely?"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/do-your-dreams-really-represent-your-deepest-darkest-desires-or-something-else-entirely-244031.html" title="Do Your Dreams Really Represent Your Deepest, Darkest Desires? Or Something Else Entirely?">
                            Do Your Dreams Really Represent Your Deepest, Darkest Desires? Or Something Else Entirely?                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_17"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/culture/food/10-diplomatic-feuds-that-were-brought-about-in-the-name-of-food-244068.html" class="tint" title="10 Diplomatic Feuds That Were Brought About In The Name Of Food">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Aug/cp_1439384099_218x102.jpg" border="0" alt="10 Diplomatic Feuds That Were Brought About In The Name Of Food"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/food/10-diplomatic-feuds-that-were-brought-about-in-the-name-of-food-244068.html" title="10 Diplomatic Feuds That Were Brought About In The Name Of Food">
                            10 Diplomatic Feuds That Were Brought About In The Name Of Food                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_18"><!--trending-panel-list start-->
                                        <figure>
                                
                        <a class='video-btn sprite' href='http://www.indiatimes.com/lifestyle/technology/nasa-just-tested-the-rocket-that-will-take-man-to-mars-visuals-obviously-are-stunning-244230.html'>video</a>					
                        <a href="http://www.indiatimes.com/lifestyle/technology/nasa-just-tested-the-rocket-that-will-take-man-to-mars-visuals-obviously-are-stunning-244230.html" class="tint" title="NASA Just Tested The Rocket That Will Take Man To Mars. Visuals Obviously Are Stunning">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/picmonkey-collage502_1439794234_1439794243_218x102.jpg" border="0" alt="NASA Just Tested The Rocket That Will Take Man To Mars. Visuals Obviously Are Stunning"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/technology/nasa-just-tested-the-rocket-that-will-take-man-to-mars-visuals-obviously-are-stunning-244230.html" title="NASA Just Tested The Rocket That Will Take Man To Mars. Visuals Obviously Are Stunning">
                            NASA Just Tested The Rocket That Will Take Man To Mars. Visuals Obviously Are Stunning                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_19"><!--trending-panel-list start-->
                                        <figure>
                                
                        <a class='video-btn sprite' href='http://www.indiatimes.com/health/tips-tricks/ladies-are-you-wearing-your-bra-correctly-244257.html'>video</a>					
                        <a href="http://www.indiatimes.com/health/tips-tricks/ladies-are-you-wearing-your-bra-correctly-244257.html" class="tint" title="Ladies, Are You Wearing Your Bra Correctly?">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Aug/bra_1439811340_218x102.jpg" border="0" alt="Ladies, Are You Wearing Your Bra Correctly?"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/tips-tricks/ladies-are-you-wearing-your-bra-correctly-244257.html" title="Ladies, Are You Wearing Your Bra Correctly?">
                            Ladies, Are You Wearing Your Bra Correctly?                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                
        </div>

    </div><!--trending-panel end-->

    </section>
    <section class="container cf" id="loader" style="display: none;border: 0px solid red;">
    <div class="news-panel cf ">&nbsp;</div>
    <div class="life-panel cf" style="text-align: center;"><img src="http://media.indiatimes.in/resources/images/loading.gif" border="0" /></div>
    <div class="trending-panel cf ">&nbsp;</div>
</section>
<section class="big-ads remove-fixed-home" id="adfooter">
<div id="badRos_slot"></div> 
</section>   

<!------------------------ end Block 4------------------------------------------------>
<!--container end-->

<script type="text/javascript">
   
    $('#hp_block_2').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible     
       showBigAD2('bigAd2_slot');
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#hp_block_3').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    if (visiblePartY == 'top') {
      // top part of element is visible

    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible  
      showBigAD3('bigAd3_slot');
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#container4').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible

       BADros('badRos_slot');
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});

$(document).ready(function(){
        var trigger_depth={b2:99, b3:499, b4:1199};
        var is_trigger_active={b2:true, b3:true, b4:true};
        var call_on_scroll=true;
        
            $(window).on("scroll",function(){
            if((!(is_trigger_active.b2)&&!(is_trigger_active.b3)&&!(is_trigger_active.b4))==false)
            {
                callPreload(is_trigger_active,trigger_depth);
                
            }
    });
});

function callPreload(active,depth)
{
    var scroll_top = $(window).scrollTop();
    if((active.b2)&&(scroll_top > depth.b2))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Aug/card_1439895944_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/wei5_1439888783_1439888788_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/angry-parrot-502_1439893015_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/cop-undie0400_1439890456_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/osama-gandhi502_1439889966_236x111.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/army_1439893765_1439893781_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/flight_attendant_card_1439890992_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/paul-walker1_1439893976_1439893984_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/fb_1439817513_218x102.jpg","http://media.indiatimes.in/media/content/2015/Aug/download_1439893967_1439893978_218x102.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1439883743_1439883761_218x102.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/farmers_card_1439812603_218x102.jpg","http://media.indiatimes.in/media/content/2015/Aug/sonu-nigam-news_1439876435_1439876451_218x102.jpg");
        active.b2=false;
    }
    if((active.b3)&&(scroll_top > depth.b3))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Aug/kohlishastricard_1439885063_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/img-20150818-wa0026-502_1439885750_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1439887276_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/green_delhi_card_1439882195_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/uttam-collage-502_1439882073_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/fb_1439817513_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/ferrari_card_1439886766_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage_1439881156_1439881161_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1439881307_218x102.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/flight_attendant_card_1439890992_218x102.jpg","http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collagehh_1439817964_1439817973_218x102.jpg","http://media.indiatimes.in/media/content/2015/Aug/arijit-singh_1439804378_1439804390_218x102.jpg","http://media.indiatimes.in/media/content/2015/Aug/over_1439388446_1439388457_218x102.jpg");
        active.b3=false;
    }
    
    if((active.b4)&&(scroll_top > depth.b4))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Aug/moon502_1439880018_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/aditichauhanfootball_1439834892_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/organ-transplant-502_1439878357_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/desi502_1439876741_1439876746_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/empire502_1439832259_236x111.jpg","http://media.indiatimes.in/media/content/2015/Aug/musiccover_1439544258_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/aa_1439897542_1439897553_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1439881307_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/sonu-nigam-news_1439876435_1439876451_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/bhindi-card_1439536960_502x234.gif","http://media.indiatimes.in/media/content/2015/Aug/cardimg1_1439550691_502x234.jpg","http://media.indiatimes.in/media/content/2015/Jul/picmonkey-collage_1437987155_1437987160_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/dream_1439284061_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collagehh_1439817964_1439817973_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/farmers_card_1439812603_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/ektakapoor_card_1439809058_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/arijit-singh_1439804378_1439804390_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/bra_1439811340_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/newsbloopers_card_1439807938_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/untitled-1_1439810061_502x234.gif","http://media.indiatimes.in/media/content/2015/Aug/anu_1439798466_1439798494_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/jeff502_1439806464_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage_1439794209_1439794219_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/ss_1439803121_1439803135_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/makeupcover_1439459949_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/tshirt_card_1439794498_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card_1439794590_1439794596_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/picmonkey-collage502_1439794234_1439794243_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/sonu-nigam-card_1439789712_1439789743_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/cp_1439384099_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/weight-gain-card_1439809701_502x234.gif","http://media.indiatimes.in/media/content/2015/Aug/duteechandtransgender_1439546277_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/over_1439388446_1439388457_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/shutterstock_156320471_1439191671_1439191676_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/cp_1439617995_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/turle_card_1439723067_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/shutterstock_190676684_1439370336_1439370342_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/indopak_card_1439726817_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/manjhi_card_1439717617_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/taxi-card_1439365091_1439365099_502x234.gif","http://media.indiatimes.in/media/content/2015/Aug/cp_1439468522_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/candy_1439455787_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/3_1439709497_1439709500_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/bb1_1439707090_1439707097_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/picmonkey-collage1_1439704786_1439704800_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/gm1_1439708071_1439708075_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/homeless_card_1439112213_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/15723388333_fcde11168d_z_1439463303_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/saif-ali_1439449030_1439449038_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/shutterstock_502_1439447259_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/shutterstock_253521151_1438589688_1438589697_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/india_1439562369_1439562398_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/capture_1439643426_1439643433_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/card2_1439464622_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/cp_1439557550_502x234.jpg","http://media.indiatimes.in/media/content/2015/Aug/untitled-1_1439810061_218x102.gif","http://media.indiatimes.in/media/content/2015/Aug/dream_1439284061_218x102.jpg","http://media.indiatimes.in/media/content/2015/Aug/cp_1439384099_218x102.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/picmonkey-collage502_1439794234_1439794243_218x102.jpg","http://media.indiatimes.in/media/videocafe/2015/Aug/bra_1439811340_218x102.jpg");
        active.b4=false;
    }
}
</script>





    <div class="clr"></div>
      
	
                 <script> 
				                    showFooterCode();
					            </script>
	           <style>
.bottom-strip{background:#ebebeb;display:block;padding:15px 0;overflow:hidden;}.bottom-strip-box{margin:0 auto;display:table;max-width:410px;width:100%;}.square-logo{width:66px;height:64px;float:left;margin-right:10px;}.bottom-strip-content{float:left;margin-top:14px;}.bottom-strip-content span{display:block;color:#737272;font:12px/1 "RobotoRegular", Arial, Helvetica, sans-serif;margin-bottom:2px;}.bottom-strip-content a{color:#1b85dd;font:24px/1 "RobotoRegular", Arial, Helvetica, sans-serif;text-decoration:none;}
</style>
<div class="bottom-strip">
<div class="bottom-strip-box">
<div class="square-logo"><a href="http://www.indiatimes.com?utm_source=footerStripHP&utm_medium=web" title=""><img src="http://media.indiatimes.in/resources/images/indiatimes-squre-logo.png" border="0" alt=""/></a></div>
<div class="bottom-strip-content"><span>Want More</span><a href="http://www.indiatimes.com?utm_source=footerStripHP&utm_medium=web" title="">Keep Looking...</a></div>
</div>
</div>   
<div class="last-container">
    <!--<br>--> 
    <!--social-panel start-->
    <div class="social-panel red-bg">
        <div class="soc-inner"><!--soc-inner start-->
            <p class="soc-title">Be a Part of The New & Next</p>
            <div class="soc-count"><!--soc-count start-->
                <a href="https://www.facebook.com/indiatimes" target="_blank" title="facebook" class="sprite soc-fb"></a>
                <p>
                    3,467,708<span class="black"> FRIENDS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://twitter.com/indiatimes" target="_blank" title="twitter" class="sprite soc-twt"></a>
                <p>10298  <span class="black">FOLLOWERS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://plus.google.com/+indiatimes" target="_blank" title="gplus" class="sprite soc-gplus"></a>
                <p>104,163 <span class="black">MEMBERS</span></p>
            </div>
            <div class="soc-count"><!--soc-count start-->
                <a href="javascript:void(0);" title="E-mail" class="sprite soc-mail"></a>
                <p>48,388 <span class="black">SUBSCRIBERS</span></p>
            </div>
            <!--soc-count end-->
        </div>
        <!--soc-inner end-->
    </div>
    <!--social-panel end-->
    <div class="clr"></div>

    <footer>
        <!-- social container start-->
        <div class="socical">
            <div class="containers">
                <div class="fl left_cont"> Get your weekly dose of virality sent to your inbox! </div>
                <div class="fr right_cont">

                    <input type="text" value="Please Enter Email Address" class="Email" name="UserEmail" id="UserEmail" defaultValue="Please Enter Email Address"/>
                    <input type="submit" id="btn-submit" value="SIGN UP" name="btn-submit" class="Subscribe">
                    <span id="subsEmal" style='color: red; padding-left: 66px; text-align: left;'></span>
                </div>

            </div>
        </div><!-- social containers end-->
        <div class="footer"><!-- footer grey start-->
            <div class="sub_container">
                <!-- container start-->
                <div class="cont_new">
                    <h2>Our Channels</h2>
                                                <a href='http://www.indiatimes.com/news/' class="blue size">News</a> 
                                                    <a href='http://www.indiatimes.com/lifestyle/' class="purple size">Lifestyle</a> 
                                                    <a href='http://www.indiatimes.com/entertainment/' class="red size">Entertainment</a> 
                                                    <a href='http://www.indiatimes.com/videocafe/' class="orange size">Videos</a> 
                                                    <a href='http://www.indiatimes.com/health/' class="green size">Health</a> 
                        
                </div><!-- container closed-->

                <div class="cont"> <!-- container start-->
                    <h2>Sections</h2>
                                            <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/news/india/'>india</a>
                                             
                                    <a href='http://www.indiatimes.com/news/world/'>world</a>
                                             
                                    <a href='http://www.indiatimes.com/news/sports/'>sports</a>
                                             
                                    <a href='http://www.indiatimes.com/news/weird/'>weird</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/lifestyle/self/'>self</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/style/'>style</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/who-we-are/'>who we are</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/travel/'>travel</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/food/'>food</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/entertainment/bollywood/'>bollywood</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/celebs/'>celebscoop</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/hollywood/'>hollywood</a>
                                
                             </div>                        <div class="sub_link">
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                             
                                    <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                             
                                    <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                             
                                    <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                             
                                    <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                
                             </div>                    <div class="sub_link"><a href="http://www.indiatimes.com/photogallery">Photogallery</a></div>
                </div><!-- container closed-->
                <div class="cont_new"> <!-- container start-->
                    <h2>Indiatimes Lifestyle Network</h2>

                    <div class="sub_link">                             <a href="http://www.mensxp.com"   target="_blank" >MENS XP</a>
                                                    <a href="http://www.idiva.com"   target="_blank" >iDiva</a>
                                                    <a href="http://luxpresso.com"   target="_blank" >Luxury</a>
                                                    <a href="http://in.askmen.com/"   target="_blank" >Ask Men</a>
                                                    <a href="http://www.pursuitist.in/"   target="_blank" >Pursuitist</a>
                        </div>
                </div><!-- container closed-->
                <div class="cont"> <!-- container start-->
                    <h2>Other Sites</h2>
                    <div class="sub_link"> 
                        
                            <a href="http://www.cricbuzz.com"  target="_blank">CricBuzz</a> 
                        
                            <a href="http://www.happytrips.com/"  target="_blank">Happy Trips</a> 
                        
                            <a href="http://www.businessinsider.in/"  target="_blank">Business Insider</a> 
                        
                            <a href="http://www.zoomtv.com"  target="_blank">ZoomTv</a> 
                        
                            <a href="http://www.gizmodo.in/"  target="_blank">Gizmodo</a> 
                        
                            <a href="http://www.lifehacker.co.in/"  target="_blank">Lifehacker</a> 
                        
                            <a href="http://in.ign.com/"  target="_blank">IGN</a> 
                        
                            <a href="http://boxtv.com"  target="_blank">BoxTV</a> 
                        
                            <a href="http://www.gaana.com"  target="_blank">Gaana</a> 
                        
                            <a href="http://shopping.indiatimes.com"  target="_blank">Online Shopping</a> 
                        
                            <a href="http://www.in.techradar.com/"  target="_blank">Techradar</a> 
                        
                            <a href="http://timesdeal.com"  target="_blank">Daily Deals</a> 
                        
                            <a href="http://www.gitanjalishop.com/"  target="_blank">Gitanjali Shop</a> 
                        
                            <a href="http://www.satvikshop.com/"  target="_blank">Satvik Shop</a> 
                        
                            <a href="http://whatshot.in"  target="_blank">WHAT'S HOT</a> 
                        
                            <a href="http://zigwheels.com"  target="_blank">ZigWheels</a> 
                        
                            <a href="http://filmipop.com"  target="_blank">FILMIPOP</a> 
                        
                            <a href="http://www.filmfare.com/"  target="_blank">Filmfare</a> 
                        
                            <a href="http://www.femina.in"  target="_blank">Femina</a> 
                        
                            <a href="http://mobile.indiatimes.com"  target="_blank">Mobile</a> 
                        
                            <a href="http://www.greetzap.com"  target="_blank">Greetings</a> 
                        
                            <a href="http://www.follo.co.in"  target="_blank">Follo</a> 
                        
                            <a href="http://timesjobs.com"  target="_blank">Jobs</a> 
                        
                            <a href="http://magicbricks.com"  target="_blank">Property</a> 
                        
                            <a href="http://www.astrospeak.com"  target="_blank">Astrology</a> 
                        
                            <a href="http://speakingtree.in"  target="_blank">Speaking Tree</a> 
                        
                            <a href="http://simplymarry.com"  target="_blank">Matrimonial</a> 
                        
                            <a href="http://itimes.com"  target="_blank">iTimes</a> 
                        
                            <a href="http://www.ads2book.com/"  target="_blank">Ads2book</a> 
                        
                            <a href="http://www.tcnext.com/"  target="_blank">TcNext</a> 
                                            </div>
                </div><!-- container closed-->
                <div class="cont border_none"><!-- container start-->
                    <h2>About us</h2>
                    <div class="sub_link">
                                                    <a href='http://www.indiatimes.com/privacypolicy' >Privacy Policy</a> 
                                                    <a href='http://www.indiatimes.com/sitemap.html' >sitemap</a> 
                                                    <a href='http://www.indiatimes.com/termsandcondition' >Terms & CONDITIONS</a> 
                                                    <a href='http://www.indiatimes.com/seoarchive' >ARCHIVES</a> 
                                                    <a href='http://www.indiatimes.com/contactus' >Contact us</a> 
                                                    <a href='http://www.indiatimes.com/aboutus' >ABOUT INDIATIMES</a> 
                                            </div>
                </div><!-- container closed-->
            </div><!-- footer grey closed-->
        </div><!-- footer black closed-->

        <div class="black_footer"><!-- footer black start-->
            <div class="containers">
                <p><a href="javascript:void(0)" class="copyrt">Copyright Â© 2015 Times Internet Limited. Powered by Indiatimes Lifestyle Network. All rights reserved</a></p>
                <span><a href="http://www.indiatimes.com/seoarchive/">Archive</a> | <a href="http://www.indiatimes.com/sitemap.html">Sitemap</a> | <a href="http://www.indiatimes.com/aboutus/">About Us</a> | <a href="http://www.indiatimes.com/contactus/">Contact Us</a> | <a href="http://www.indiatimes.com/privacypolicy/">Privacy Policy</a> | <a target="_blank" href="http://m.indiatimes.com">Indiatimes Mobile</a> | <a href="http://www.indiatimes.com/termsandcondition/">Terms of Use and Grievance Redressal Policy </a> </span> 
            </div>
        </div><!-- footer black end-->
    </footer>
</div>

    <script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.lazyload.min.js?v=100.8" charset="utf-8"></script>
<script defer type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.easing.1.3.min.js?v=100.8"></script>

<input type="hidden" name="currentId" id="currentId" value="">
    <div class="animate_cont dawn floating delay5">
        <span class="animate_arrow sprite_image"></span>
    </div>
<script defer src="http://media.indiatimes.in/resources/js/jquery.flexslider-min.js?v=1421222060"></script>



<script type="text/javascript" src="http://media.indiatimes.in/resources/js/main.js?v=100.8"></script>    
<script type="text/javascript" src="http://media.indiatimes.in/resources/js/common.js?v=100.8"></script>
<script  defer type="text/javascript" src="http://media.indiatimes.in/resources/js/jquery-scrolltofixed.js?v=100.8"></script>

<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://cdn.mouseflow.com/projects/1e238441-c30e-4d1b-8edd-c0f14d8fa6b7.js' type='text/javascript'%3E%3C/script%3E"));</script>
<!-- mouse flow code ends-->
<script  src="http://tags.crwdcntrl.net/c/2818/cc.js?ns=_cc2818" id="LOTCC_2818"></script> 

<script> 
    _cc2818.bcp(); 
</script>

	 <p id="back-top" style="display:none;"><a href="#top"><span></span></a> </p>
</div><!--wrap end-->


</body>
</html>