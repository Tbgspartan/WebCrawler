



<!DOCTYPE html>
<html
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

    
    
    

    
    
    

    <meta name="apple-itunes-app" content="app-id=342792525, app-argument=imdb:///?src=mdot">
        
    
            <script type="text/javascript">var ue_t0=window.ue_t0||+new Date();</script>
            <script type="text/javascript">
                var ue_mid = "A1EVAM02EL8SFB"; 
                var ue_sn = "www.imdb.com";  
                var ue_furl = "fls-na.amazon.com";
                var ue_sid = "205-3786787-7288382";
                var ue_id = "11PBAWT29DWDX12AGDK6";
                (function(e){var c=e;var a=c.ue||{};a.main_scope="mainscopecsm";a.q=[];a.t0=c.ue_t0||+new Date();a.d=g;function g(h){return +new Date()-(h?0:a.t0)}function d(h){return function(){a.q.push({n:h,a:arguments,t:a.d()})}}function b(m,l,h,j,i){var k={m:m,f:l,l:h,c:""+j,err:i,fromOnError:1,args:arguments};c.ueLogError(k);return false}b.skipTrace=1;e.onerror=b;function f(){c.uex("ld")}if(e.addEventListener){e.addEventListener("load",f,false)}else{if(e.attachEvent){e.attachEvent("onload",f)}}a.tag=d("tag");a.log=d("log");a.reset=d("rst");c.ue_csm=c;c.ue=a;c.ueLogError=d("err");c.ues=d("ues");c.uet=d("uet");c.uex=d("uex");c.uet("ue")})(window);(function(e,d){var a=e.ue||{};function c(g){if(!g){return}var f=d.head||d.getElementsByTagName("head")[0]||d.documentElement,h=d.createElement("script");h.async="async";h.src=g;f.insertBefore(h,f.firstChild)}function b(){var k=e.ue_cdn||"z-ecx.images-amazon.com",g=e.ue_cdns||"images-na.ssl-images-amazon.com",j="/images/G/01/csminstrumentation/",h=e.ue_file||"ue-full-051542d0cfa6f645f8266601739de597._V1_.js",f,i;if(h.indexOf("NSTRUMENTATION_FIL")>=0){return}if("ue_https" in e){f=e.ue_https}else{f=e.location&&e.location.protocol=="https:"?1:0}i=f?"https://":"http://";i+=f?g:k;i+=j;i+=h;c(i)}if(!e.ue_inline){if(a.loadUEFull){a.loadUEFull()}else{b()}}a.uels=c;e.ue=a})(window,document);
                if (!('CS' in window)) { window.CS = {}; }
                    window.CS.loginSecureHost = "https://secure.imdb.com";
            </script>
 

        
        <script type="text/javascript">var IMDbTimer={starttime: new Date().getTime(),pt:'java'};</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_title"] = new Date().getTime(); })(IMDbTimer);</script>
        <title>IMDb - Movies, TV and Celebrities - IMDb</title>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_title"] = new Date().getTime(); })(IMDbTimer);</script>
        
            <link rel="canonical" href="http://www.imdb.com/" />
            <meta property="og:url" content="http://www.imdb.com/" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/safari-favicon-517611381._CB303901978_.svg" mask rel="icon" sizes="any">
        <link rel="icon" type="image/ico" href="http://ia.media-imdb.com/images/G/01/imdb/images/favicon-2165806970._CB379387995_.ico" />
        <meta name="theme-color" content="#000000" />
        <link rel="shortcut icon" type="image/x-icon" href="http://ia.media-imdb.com/images/G/01/imdb/images/desktop-favicon-2165806970._CB379390718_.ico" />
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-4151659188._CB361295786_.png" rel="apple-touch-icon"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-76x76-53536248._CB361295462_.png" rel="apple-touch-icon" sizes="76x76"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-120x120-2442878471._CB361295428_.png" rel="apple-touch-icon" sizes="120x120"> 
        <link href="http://ia.media-imdb.com/images/G/01/imdb/images/mobile/apple-touch-icon-web-152x152-1475823641._CB361295368_.png" rel="apple-touch-icon" sizes="152x152">            
        <link rel="search" type="application/opensearchdescription+xml" href="http://ia.media-imdb.com/images/G/01/imdb/images/imdbsearch-3349468880._CB379388505_.xml" title="IMDb" />
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_icon"] = new Date().getTime(); })(IMDbTimer);</script>
        
        <meta property="pageType" content="home" />
        <meta property="subpageType" content="main" />


        <link rel='image_src' href="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png">
        <meta property='og:image' content="http://ia.media-imdb.com/images/G/01/imdb/images/logos/imdb_fb_logo-1730868325._CB306318125_.png" />

    <meta property='fb:app_id' content='115109575169727' />

    <meta property='og:title' content="IMDb - Movies, TV and Celebrities" />
    <meta property='og:site_name' content='IMDb' />
    <meta name="title" content="IMDb - Movies, TV and Celebrities - IMDb" />
        <meta name="description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta property="og:description" content="IMDb, the world's most popular and authoritative source for movie, TV and celebrity content." />
        <meta name="keywords" content="movies,films,movie database,actors,actresses,directors,hollywood,stars,quotes" />
        <meta name="request_id" content="11PBAWT29DWDX12AGDK6" />
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_css"] = new Date().getTime(); })(IMDbTimer);</script>
<!-- h=ics-1e-c3-2xl-i-17506a8d.us-east-1 -->

            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/pagelayout-flat-903233829._CB298432192_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-mega-3796876714._CB298540748_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/starbarwidget-2454701167._CB293329573_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/watchlistButton-2508181169._CB288096406_.css" />
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/collections/recommendations-3988610918._CB289934424_.css" />
        <noscript>
            <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/wheel/nojs-2827156349._CB318528195_.css">
        </noscript>
  <script>(function(t){ (t.events = t.events || {})["csm_head_post_css"] = new Date().getTime(); })(IMDbTimer);</script>
        
  <script>(function(t){ (t.events = t.events || {})["csm_head_pre_ads"] = new Date().getTime(); })(IMDbTimer);</script>
        <script>
window.ads_js_start = new Date().getTime();
var imdbads = imdbads || {}; imdbads.cmd = imdbads.cmd || [];
</script>
<!-- begin SRA -->
<script>
!function a(b,c,d){function e(g,h){if(!c[g]){if(!b[g]){var i="function"==typeof require&&require;if(!h&&i)return i(g,!0);if(f)return f(g,!0);var j=new Error("Cannot find module '"+g+"'");throw j.code="MODULE_NOT_FOUND",j}var k=c[g]={exports:{}};b[g][0].call(k.exports,function(a){var c=b[g][1][a];return e(c?c:a)},k,k.exports,a,b,c,d)}return c[g].exports}for(var f="function"==typeof require&&require,g=0;g<d.length;g++)e(d[g]);return e}({1:[function(a,b,c){"use strict";a(2)},{2:2}],2:[function(a,b,c){"use strict";!function(){var a,b,c=function(a){return"[object Array]"===Object.prototype.toString.call(a)},d=function(a,b){for(var c=0;c<a.length;c++)c in a&&b.call(null,a[c],c)},e=[],f=!1,g=!1,h=function(){var a=[],b=[],c={};return d(e,function(e){var f="";d(e.dartsite.split("/"),function(b){""!==b&&(b in c||(c[b]=a.length,a.push(b)),f+="/"+c[b])}),b.push(f)}),{iu_parts:a,enc_prev_ius:b}},i=function(){var a=[];return d(e,function(b){var c=[];d(b.sizes,function(a){c.push(a.join("x"))}),a.push(c.join("|"))}),a},j=function(){var a=[];return d(e,function(b){a.push(k(b.targeting))}),a.join("|")},k=function(a,b){var c,d=[];for(var e in a){c=[];for(var f=0;f<a[e].length;f++)c.push(encodeURIComponent(a[e][f]));b?d.push(e+"="+encodeURIComponent(c.join(","))):d.push(e+"="+c.join(","))}return d.join("&")},l=function(){var a=+new Date;g||document.readyState&&"loaded"!==document.readyState||(g=!0,f&&imdbads.cmd.push(function(){for(var b=0;b<e.length;b++)generic.monitoring.record_metric(e[b].name+".fail",csm.duration(a))}))};window.tinygpt={define_slot:function(a,b,c,d){e.push({dartsite:a.replace(/\/$/,""),sizes:b,name:c,targeting:d})},set_targeting:function(b){a=b},callback:function(a){for(var c,d,f={},g=+new Date,h=0;h<e.length;h++)c=e[h].dartsite,d=e[h].name,a[h][c]?f[d]=a[h][c]:window.console&&console.error&&console.error("Unable to correlate GPT response for "+d);imdbads.cmd.push(function(){for(var a=0;a<e.length;a++)ad_utils.slot_events.trigger(e[a].name,"request",{timestamp:b}),ad_utils.slot_events.trigger(e[a].name,"tagdeliver",{timestamp:g});ad_utils.gpt.handle_response(f)})},send:function(){var d,g,m=[],n=function(a,b){c(b)&&(b=b.join(",")),b&&m.push(a+"="+encodeURIComponent(""+b))};if(0===e.length)return void tinygpt.callback({});n("gdfp_req","1"),n("correlator",Math.floor(4503599627370496*Math.random())),n("output","json_html"),n("callback","tinygpt.callback"),n("impl","fifs"),n("json_a","1");var o=h();n("iu_parts",o.iu_parts),n("enc_prev_ius",o.enc_prev_ius),n("prev_iu_szs",i()),n("prev_scp",j()),n("cust_params",k(a,!0)),d=document.createElement("script"),g=document.getElementsByTagName("script")[0],d.async=!0,d.type="text/javascript",d.src="http://pubads.g.doubleclick.net/gampad/ads?"+m.join("&"),d.id="tinygpt",d.onload=d.onerror=d.onreadystatechange=l,f=!0,g.parentNode.insertBefore(d,g),b=+new Date}}}()},{}]},{},[1]);</script>
<script>
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[1008,150],[1008,200],[1008,30],[970,250],[9,1]],
'top_ad',
{
'p': ['top','t']
});
tinygpt.define_slot('/4215/imdb2.consumer.homepage/',
[[300,250],[300,600],[11,1]],
'top_rhs',
{
'p': ['tr']
});
tinygpt.set_targeting({
'fv' : ['1'],
'ab' : ['a'],
'c' : ['0'],
's' : ['3075','32'],
'bpx' : ['2'],
'u': ['024101001491'],
'oe': ['utf-8']
});
tinygpt.send();
</script>
<!-- begin ads header -->
<script src="http://ia.media-imdb.com/images/G/01/imdbads/js/collections/ads-tarnhelm-4261578659._CB296130991_.js"></script>
<script>
doWithAds = function(){};
</script>
<script>
doWithAds = function(inside, failureMessage){
if ('consoleLog' in window &&
'generic' in window &&
'ad_utils' in window &&
'custom' in window &&
'monitoring' in generic &&
'document_is_ready' in generic) {
try{
inside.call(this);
}catch(e) {
if ( window.ueLogError ) {
if(typeof failureMessage !== 'undefined'){
e.message = failureMessage;
}
e.attribution = "Advertising";
e.logLevel = "ERROR";
ueLogError(e);
}
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
} else {
if( (document.location.hash.match('debug=1')) &&
(typeof failureMessage !== 'undefined') ){
console.error(failureMessage);
}
}
};
</script><script>
doWithAds(function(){
generic.monitoring.record_metric("ads_js_request_to_done", (new Date().getTime()) - window.ads_js_start);
generic.monitoring.set_forester_info("homepage");
generic.monitoring.set_twilight_info(
"homepage",
"FR",
"c6de73654585800abf511962f88191c46dce1265",
"2016-04-10T17%3A03%3A30GMT",
"http://s.media-imdb.com/twilight/?",
"consumer");
generic.send_csm_head_metrics && generic.send_csm_head_metrics();
generic.monitoring.start_timing("page_load");
generic.seconds_to_midnight = 50190;
generic.days_to_midnight = 0.5809027552604675;
ad_utils.set_slots_on_page({ 'injected_billboard':1, 'injected_navstrip':1, 'top_ad':1, 'top_rhs':1, 'rhs_cornerstone':1 });
custom.full_page.data_url = "http://ia.media-imdb.com/images/G/01/imdbads/js/graffiti_data-3230827532._CB299577821_.js";
consoleLog('advertising initialized','ads');
},"ads js missing, skipping ads setup.");
var _gaq = _gaq || [];
_gaq.push(['_setCustomVar', 4, 'ads_abtest_treatment', 'a']);
</script>
<script>doWithAds(function() { ad_utils.ads_header.done(); });</script>
<!-- end ads header -->
        <script  type="text/javascript">
            // ensures js doesn't die if ads service fails.  
            // Note that we need to define the js here, since ad js is being rendered inline after this.
            (function(f) {
                // Fallback javascript, when the ad Service call fails.  
                
                if((window.csm == null || window.generic == null || window.consoleLog == null)) {
                    if (window.console && console.log) {
                        console.log("one or more of window.csm, window.generic or window.consoleLog has been stubbed...");
                    }
                }
                
                window.csm = window.csm || { measure:f, record:f, duration:f, listen:f, metrics:{} };
                window.generic = window.generic || { monitoring: { start_timing: f, stop_timing: f } };
                window.consoleLog = window.consoleLog || f;
            })(function() {});
        </script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_head_delivery_finished');
    }
  </script>
        </head>
    <body id="styleguide-v2" class="fixed">
<script>
    if (typeof uet == 'function') {
      uet("bb");
    }
</script>
  <script>
    if ('csm' in window) {
      csm.measure('csm_body_delivery_started');
    }
  </script>
        <div id="wrapper">
            <div id="root" class="redesign">
<script>
    if (typeof uet == 'function') {
      uet("ns");
    }
</script>
<div id="nb20" class="navbarSprite">
<div id="supertab">	
	<!-- begin TOP_AD -->
<div id="top_ad_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-3292747090._CB299577773_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_ad');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=1008x150,1008x200,1008x30,970x250,9x1;p=top;p=t;fv=1;ab=a;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=024101001491;ord=024101001491?" id="top_ad" name="top_ad" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript> <a href="http://pubads.g.doubleclick.net/gampad/jump?&iu=4215/imdb2.consumer.homepage/&sz=1008x150|1008x200|1008x30|970x250|9x1&t=p%3Dtop%26p%3Dt%26fv%3D1%26ab%3Da%26bpx%3D2%26c%3D0%26s%3D3075%26s%3D32&tile=0&c=024101001491" target="_blank"> <img src="http://pubads.g.doubleclick.net/gampad/ad?&iu=4215/imdb2.consumer.homepage/&sz=1008x150|1008x200|1008x30|970x250|9x1&t=p%3Dtop%26p%3Dt%26fv%3D1%26ab%3Da%26bpx%3D2%26c%3D0%26s%3D3075%26s%3D32&tile=0&c=024101001491" border="0" alt="advertisement" /> </a> </noscript>
</div>
<div id="top_ad_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_ad');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_AD -->
	
</div>
  <div id="navbar" class="navbarSprite">
<noscript>
  <link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-no-js-3652782989._CB318528046_.css" />
</noscript>
<!--[if IE]><link rel="stylesheet" type="text/css" href="http://ia.media-imdb.com/images/G/01/imdb/css/site/consumer-navbar-ie-2133976414._CB318528041_.css"><![endif]-->
<span id="home_img_holder">
<a href="/?ref_=nv_home"
title="Home" class="navbarSprite" id="home_img" ></a>  <span class="alt_logo">
    <a href="/?ref_=nv_home"
title="Home" >IMDb</a>
  </span>
</span>
<form
 method="get"
 action="/find"
 class="nav-searchbar-inner"
 id="navbar-form"

>
  <div id="nb_search">
    <noscript><div id="more_if_no_javascript"><a href="/search/">More</a></div></noscript>
    <button id="navbar-submit-button" class="primary btn" type="submit"><div class="magnifyingglass navbarSprite"></div></button>
    <input type="hidden" name="ref_" value="nv_sr_fn" />
    <input type="text" autocomplete="off" value="" name="q" id="navbar-query" placeholder="Find Movies, TV shows, Celebrities and more...">
    <div class="quicksearch_dropdown_wrapper">
      <select name="s" id="quicksearch" class="quicksearch_dropdown navbarSprite"
              onchange="jumpMenu(this); suggestionsearch_dropdown_choice(this);">
        <option value="all" >All</option>
        <option value="tt" >Titles</option>
        <option value="ep" >TV Episodes</option>
        <option value="nm" >Names</option>
        <option value="co" >Companies</option>
        <option value="kw" >Keywords</option>
        <option value="ch" >Characters</option>
        <option value="qu" >Quotes</option>
        <option value="bi" >Bios</option>
        <option value="pl" >Plots</option>
      </select>
    </div>
    <div id="navbar-suggestionsearch"></div>
  </div>
</form>
<div id="megaMenu">
    <ul id="consumer_main_nav" class="main_nav">
        <li class="spacer"></li>
        <li class="css_nav_item" id="navTitleMenu">
            <p class="navCategory">
                <a href="/movies-in-theaters/?ref_=nv_tp_inth_1"
>Movies</a>,
                <a href="/chart/toptv/?ref_=nv_tp_tv250_2"
>TV</a><br />
                & <a href="/showtimes/?ref_=nv_tp_sh_3"
>Showtimes</a></p>
            <span class="downArrow"></span>
            <div id="navMenu1" class="sub_nav">
                <div id="titleAd"></div>
                <div class="subNavListContainer">
                    <h5>MOVIES</h5>
                    <ul>
                        <li><a href="/movies-in-theaters/?ref_=nv_mv_inth_1"
>In Theaters</a></li>
                        <li><a href="/showtimes/?ref_=nv_mv_sh_2"
>Showtimes & Tickets</a></li>
                        <li><a href="/trailers/?ref_=nv_mv_tr_3"
>Latest Trailers</a></li>
                        <li><a href="/movies-coming-soon/?ref_=nv_mv_cs_4"
>Coming Soon</a></li>
                        <li><a href="/calendar/?ref_=nv_mv_cal_5"
>Release Calendar</a></li>
                        <li><a href="/chart/top?ref_=nv_mv_250_6"
>Top Rated Movies</a></li>
                        <li><a href="/chart/moviemeter?ref_=nv_mv_mpm_7"
>Most Popular Movies</a></li>
                    </ul>
                    <h5>CHARTS & TRENDS</h5>
                    <ul>
                        <li><a href="/chart/?ref_=nv_ch_cht_1"
>Box Office</a></li>
                        <li><a href="/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_2"
>Oscar Winners</a></li>
                        <li><a href="/genre/?ref_=nv_ch_gr_3"
>Most Popular by Genre</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>TV & VIDEO</h5>
                    <ul>
                        <li><a href="/imdbpicks/tv-releases/?ref_=nv_tvv_picks_1"
>TV Picks</a></li>
                        <li><a href="/tvgrid/?ref_=nv_tvv_ls_2"
>On Tonight</a></li>
                        <li><a href="/chart/toptv/?ref_=nv_tvv_250_3"
>Top Rated TV Shows</a></li>
                        <li><a href="/chart/tvmeter?ref_=nv_tvv_mptv_4"
>Most Popular TV Shows</a></li>
                        <li><a href="/feature/watch-now-on-amazon/?ref_=nv_tvv_wn_5"
>Watch Now on Amazon</a></li>
                        <li><a href="/sections/dvd/?ref_=nv_tvv_dvd_6"
>DVD & Blu-Ray</a></li>
                    </ul>
                    <h5>SPECIAL FEATURES</h5>
                    <ul>
                        <li><a href="/x-ray/?ref_=nv_sf_xray_2"
>X-Ray for Movies & TV</a></li>
                        <li><a href="/imdbpicks/?ref_=nv_sf_picks_3"
>IMDb Picks</a></li>
                        <li><a href="/best-of/?ref_=nv_sf_bstof_4"
>Best of 2015</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNameMenu">
            <p class="navCategory">
                <a href="/search/name?gender=male,female&ref_=nv_tp_cel_1"
>Celebs</a>,
                <a href="/awards-central/?ref_=nv_tp_awrd_2"
>Events</a><br />
                & <a href="/gallery/rg784964352?ref_=nv_tp_ph_3"
>Photos</a></p>
            <span class="downArrow"></span>
            <div id="navMenu2" class="sub_nav">
                <div id="nameAd"></div>
                <div class="subNavListContainer">
                    <h5>CELEBS</h5>
                    <ul>
                            <li><a href="/search/name?birth_monthday=04-10&refine=birth_monthday&ref_=nv_cel_brn_1"
>Born Today</a></li>
                        <li><a href="/news/celebrity?ref_=nv_cel_nw_2"
>Celebrity News</a></li>
                        <li><a href="/search/name?gender=male,female&ref_=nv_cel_m_3"
>Most Popular Celebs</a></li>
                    </ul>
                    <h5>PHOTOS</h5>
                    <ul>
                        <li><a href="/gallery/rg784964352?ref_=nv_ph_ls_1"
>Latest Stills</a></li>
                        <li><a href="/gallery/rg1528338944?ref_=nv_ph_lp_2"
>Latest Posters</a></li>
                        <li><a href="/gallery/rg2465176320?ref_=nv_ph_lv_3"
>Photos We Love</a></li>
                    </ul>
                </div>
                <div class="subNavListContainer">
                    <h5>EVENTS</h5>
                    <ul>
                        <li><a href="/awards-central/?ref_=nv_ev_awrd_1"
>Awards Central</a></li>
                        <li><a href="/sundance/?ref_=nv_ev_sun_3"
>Sundance</a></li>
                        <li><a href="/cannes/?ref_=nv_ev_can_6"
>Cannes</a></li>
                        <li><a href="/comic-con/?ref_=nv_ev_comic_7"
>Comic-Con</a></li>
                        <li><a href="/emmys/?ref_=nv_ev_rte_8"
>Emmy Awards</a></li>
                        <li><a href="/venice/?ref_=nv_ev_venice_9"
>Venice Film Festival</a></li>
                        <li><a href="/toronto/?ref_=nv_ev_tff_10"
>Toronto Film Festival</a></li>
                        <li><a href="/event/all/?ref_=nv_ev_all_11"
>All Events</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navNewsMenu">
            <p class="navCategory">
                <a href="/news/top?ref_=nv_tp_nw_1"
>News</a> &<br />
                <a href="/boards/?ref_=nv_tp_bd_2"
>Community</a></p>
            <span class="downArrow"></span>
            <div id="navMenu3" class="sub_nav">
                <div id="latestHeadlines">
                    <div class="subNavListContainer">
                        <h5>LATEST HEADLINES</h5>
    <ul>
                <li itemprop="headline">
<a href="/news/ni59702721/?ref_=nv_nw_tn_1"
> Melissa McCarthy's 'The Boss' Narrowly Tops 'Batman v Superman' for Weekend #1
</a><br />
                        <span class="time">2 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59701878/?ref_=nv_nw_tn_2"
> âThe Huntsman: Winterâs Warâ Waging $21.3M Weekend In Offshore Bow â Intl Box Office
</a><br />
                        <span class="time">22 hours ago</span>
                </li>
                <li itemprop="headline">
<a href="/news/ni59701187/?ref_=nv_nw_tn_3"
> Melissa McCarthy, Ben Falcone Boarding âLife of the Partyâ
</a><br />
                        <span class="time">9 April 2016 1:55 AM, UTC</span>
                </li>
    </ul>
                    </div>
                </div>
                <div class="subNavListContainer">
                    <h5>NEWS</h5>
                    <ul>
                        <li><a href="/news/top?ref_=nv_nw_tp_1"
>Top News</a></li>
                        <li><a href="/news/movie?ref_=nv_nw_mv_2"
>Movie News</a></li>
                        <li><a href="/news/tv?ref_=nv_nw_tv_3"
>TV News</a></li>
                        <li><a href="/news/celebrity?ref_=nv_nw_cel_4"
>Celebrity News</a></li>
                        <li><a href="/news/indie?ref_=nv_nw_ind_5"
>Indie News</a></li>
                    </ul>
                    <h5>COMMUNITY</h5>
                    <ul>
                        <li><a href="/boards/?ref_=nv_cm_bd_1"
>Message Boards</a></li>
                        <li><a href="/czone/?ref_=nv_cm_cz_2"
>Contributor Zone</a></li>
                        <li><a href="/poll/?ref_=nv_cm_pl_3"
>Polls</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="spacer"></li>
        <li class="css_nav_item" id="navWatchlistMenu">
<p class="navCategory singleLine watchlist">
    <a href="/list/watchlist?ref_=nv_wl_all_0"
>Watchlist</a>
</p>
<span class="downArrow"></span>
<div id="navMenu4" class="sub_nav">
    <h5>
            YOUR WATCHLIST
    </h5>
    <ul id="navWatchlist">
    </ul>
    <script>
    if (!('imdb' in window)) { window.imdb = {}; }
    window.imdb.watchlistTeaserData = [
                {
                        href : "/list/watchlist",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot1_logged_out-1670046337._CB276458993_.jpg"
                },
                {
                    href : "/search/title?count=100&title_type=feature,tv_series",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot2_popular-4090757197._CB276458997_.jpg"
                },
                {
                    href : "/chart/top",
                src : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/watchlist_slot3_top250-575799966._CB276458995_.jpg"
                }
    ];
    </script>
</div>
        </li>
        <li class="spacer"></li>
    </ul>
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.navbarAdSlots = {
    titleAd : {
            clickThru : "/title/tt0034583/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BMTk3MTMyMjc3Nl5BMl5BanBnXkFtZTcwODg4NDYwOA@@._V1._SY400_CR30,30,410,315_.jpg",
            titleYears : "1942",
            rank : 33,
                    headline : "Casablanca"
    },
    nameAd : {
            clickThru : "/name/nm0005109/",
            imageUrl : "http://ia.media-imdb.com/images/M/MV5BNDc5MTIwMzg1MF5BMl5BanBnXkFtZTcwMzg0MjQ5NQ@@._V1._SX270_CR15,0,250,315_.jpg",
            rank : 183,
            headline : "Mila Kunis"
    }
}
</script>
</div>
<div id="nb_extra">
    <ul id="nb_extra_nav" class="main_nav">
        <li class="css_nav_item" id="navProMenu">
            <p class="navCategory">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
> <img alt="IMDbPro Menu" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_nb-720143162._CB306318304_.png" />
</a>            </p>
            <span class="downArrow"></span>
            <div id="navMenuPro" class="sub_nav">
<a href="http://pro.imdb.com/signup/index.html?rf=cons_nb_hm&ref_=cons_nb_hm"
id="proLink" > <div id="proAd">
<script>
if (!('imdb' in window)) { window.imdb = {}; }
window.imdb.proMenuTeaser = {
imageUrl : "http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_menu_user-2082544740._CB276459002_.jpg"
};
</script>
</div>
<div class="subNavListContainer">
<img alt="Go to IMDbPro" title="Go to IMDbPro" src="http://ia.media-imdb.com/images/G/01/imdb/images/navbar/imdbpro_logo_menu-2185879182._CB306318245_.png" />
<h5>GET INFORMED</h5>
<p>Industry information at your fingertips</p>
<h5>GET CONNECTED</h5>
<p>Over 200,000 Hollywood insiders</p>
<h5>GET DISCOVERED</h5>
<p>Enhance your IMDb Page</p>
<p><strong>Go to IMDbPro &raquo;</strong></p>
</div>
</a>            </div>
        </li>
        <li class="spacer"><span class="ghost">|</span></li>
        <li>
            <a href="/help/?ref_=nb_hlp"
>Help</a>
        </li>
        <li class="social">

    <a href="/offsite/?page-action=fol_fb&token=BCYm5l0ucUC2WWeSjcNRQd03WwmigREpw1bsZc_I9Qll9Te5GtiI3zs8p20EonAg3Dgz4fEDkuKP%0D%0APDavBGI8I7pHSt7lMmQ-3Sw-AQVAmcUbb-W78pnJbNDLAj68GPIhbPr4KqOz3j41hzavGOLBjQoB%0D%0AcO0O3yBIgcqpXlnyzXgEj8PB-3Ho-0hvB15mR1RjU5YkcdDBYif2ixqjBsps84vj_Q%0D%0A&ref_=hm_nv_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYm7KB7XsvpEBrIWKZb6q3MzxL38P6Im-jerreM3AKJzShZlu2OpjN9Zd8hC-NM4vqQWTQBepEm%0D%0AIRSuHICRmjzq85TK7xOl61BPdfeDlzdA-yQ_0Q7lLw76XcCD0AYsr_PTU_vzJbRzAWa9aXM_XxYw%0D%0AY19BdDSBgxcKb9PbDq_c1UodVz8itoIirBmC1xgTaTyS%0D%0A&ref_=hm_nv_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYn9xfMclGoSUKwkAaWoU7jZY0aYmcEynwnSlYY0pTxwP7h-nnV22CaXZUhUddnz2-3NZ1161gl%0D%0Aio7bONWQRPYPEdDvbEnsTdG42t0MRa38dgcbzEgzV9IIeu2cgURpos0d_87MU7OMTUSJmtIl1hzH%0D%0ALavSVe8AsfB9C82f1ueek9sC9UwEc-IqqPjXXX0V9drR%0D%0A&ref_=hm_nv_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
        </li>
    </ul>
</div>
<div id="nb_personal">
    <ul id="consumer_user_nav" class="main_nav">

        <li class="css_nav_menu" id="navUserMenu">
            <p class="navCategory singleLine">
                <a href="/register/login?ref_=nv_usr_lgin_1"
rel="login" id="nblogin" >Login</a>
            </p>
            <span class="downArrow"></span>
            <div class="sub_nav">
                <div class="subNavListContainer">
                    <br />
                    <ul>
                        <li>
                            <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=nv_usr_reg_2"
>Register</a>
                        </li>
                        <li>
                            <a href="/register/login?ref_=nv_usr_lgin_3"
rel="login" id="nblogin" >Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
  </div>
</div>
	
	<!-- no content received for slot: navstrip -->
	
	
	<!-- begin injectable INJECTED_NAVSTRIP -->
<div id="injected_navstrip_wrapper" class="injected_slot">
<iframe id="injected_navstrip" name="injected_navstrip" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=024101001491;ord=024101001491?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_navstrip');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_navstrip_reflow_helper"></div>
<!-- end injectable INJECTED_NAVSTRIP -->
	
<script>
    if (typeof uet == 'function') {
      uet("ne");
    }
</script>

    <div id="pagecontent">
    <div class="pagecontent">
	
	<!-- begin injectable INJECTED_BILLBOARD -->
<div id="injected_billboard_wrapper" class="injected_slot">
<iframe id="injected_billboard" name="injected_billboard" class="yesScript" width="0" height="0" data-dart-params="#imdb2.consumer.homepage/;oe=utf-8;u=024101001491;ord=024101001491?" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe> </div>
<script>
doWithAds(function(){
ad_utils.inject_ad.register('injected_billboard');
}, "ad_utils not defined, unable to render injected ad.");
</script>
<div id="injected_billboard_reflow_helper"></div>
<!-- end injectable INJECTED_BILLBOARD -->
	

    
    
    

    
    
    

    
    
    
    </div>
    <div id="top-slot-wrapper" class="pagecontent">

    
    
    

    
    
    
    </div>
    <div class="pagecontent">
        <div id="content-2-wide">
            <div id="main">

    
    
        <a name="slot_hero"></a>
        <div class="heroWidget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','HeroWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_hero">
<div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi3713971481?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2462697862&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_1" data-video="vi3713971481" data-source="bylist" data-id="ls002309697" data-rid="11PBAWT29DWDX12AGDK6" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_1"> <img itemprop="image" class="pri_image" title="Rebels set out on a mission to steal the plans for the Death Star." alt="Rebels set out on a mission to steal the plans for the Death Star." src="http://ia.media-imdb.com/images/M/MV5BMTQ5Nzg4ODczN15BMl5BanBnXkFtZTgwODA1MDY1ODE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ5Nzg4ODczN15BMl5BanBnXkFtZTgwODA1MDY1ODE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Rebels set out on a mission to steal the plans for the Death Star." title="Rebels set out on a mission to steal the plans for the Death Star." class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Rebels set out on a mission to steal the plans for the Death Star." title="Rebels set out on a mission to steal the plans for the Death Star." class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt3748528/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2462697862&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_1" > Rogue One: A Star Wars Story </a> </div> </div> <div class="secondary ellipsis"> Official Teaser Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi174044441?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2462697862&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_2" data-video="vi174044441" data-source="bylist" data-id="ls053181649" data-rid="11PBAWT29DWDX12AGDK6" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_2"> <img itemprop="image" class="pri_image" title="Watch the latest trailer for Captain America: Civil War!" alt="Watch the latest trailer for Captain America: Civil War!" src="http://ia.media-imdb.com/images/M/MV5BMjQ0MTgyNjAxMV5BMl5BanBnXkFtZTgwNjUzMDkyODE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjQ0MTgyNjAxMV5BMl5BanBnXkFtZTgwNjUzMDkyODE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="Watch the latest trailer for Captain America: Civil War!" title="Watch the latest trailer for Captain America: Civil War!" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Watch the latest trailer for Captain America: Civil War!" title="Watch the latest trailer for Captain America: Civil War!" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt3498820/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2462697862&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_2" > Captain America: Civil War </a> </div> </div> <div class="secondary ellipsis"> New Trailer </div> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2522330393?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2462697862&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_i_3" data-video="vi2522330393" data-source="bylist" data-id="ls002653141" data-rid="11PBAWT29DWDX12AGDK6" data-type="playlist" class="video-colorbox" data-refsuffix="hm_hp" data-ref="hm_hp_i_3"> <img itemprop="image" class="pri_image" title="See the latest footage from 'X-Men: Apocalypse.'" alt="See the latest footage from 'X-Men: Apocalypse.'" src="http://ia.media-imdb.com/images/M/MV5BMTY0MDY0NjExN15BMl5BanBnXkFtZTgwOTU3OTYyODE@._V1_SY298_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTY0MDY0NjExN15BMl5BanBnXkFtZTgwOTU3OTYyODE@._V1_SY298_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> <img alt="See the latest footage from 'X-Men: Apocalypse.'" title="See the latest footage from 'X-Men: Apocalypse.'" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="See the latest footage from 'X-Men: Apocalypse.'" title="See the latest footage from 'X-Men: Apocalypse.'" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/title/tt3385516/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2462697862&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_cap_pri_3" > X-Men: Apocalypse </a> </div> </div> <div class="secondary ellipsis"> "The Four Horsemen" </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/trailers?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2462697862&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=hero&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_hp_sm" class="position_bottom supplemental" >Browse more trailers</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','HeroWidget',{wb:1});}
                if(typeof uet === 'function'){uet("cf");}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-1"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/imdbpicks/10-biggest-questions-fear-the-walking-dead-things-season-2/ls036692458?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2464233182&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_tv_fwd_hd" > <h3>10 Biggest "Fear the Walking Dead" Season 2 Questions</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:624px;height:auto;" > <div style="width:624px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/10-biggest-questions-fear-the-walking-dead-things-season-2/ls036692458?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2464233182&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_tv_fwd_i_1" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTc2MzU0MjQwOV5BMl5BanBnXkFtZTgwNzYxNjY0ODE@._UX1300_CR0,320,1248,702_SY351_SX624_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc2MzU0MjQwOV5BMl5BanBnXkFtZTgwNzYxNjY0ODE@._UX1300_CR0,320,1248,702_SY351_SX624_AL_UY702_UX1248_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">The six-episode first season of "<a href="/title/tt3743822/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2464233182&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_tv_fwd_lk1">Fear the Walking Dead</a>" left fans with plenty of questions. Here are 10 things we'd most like to see covered when the show returns this Sunday.</p> <p class="seemore"><a href="/imdbpicks/10-biggest-questions-fear-the-walking-dead-things-season-2/ls036692458?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2464233182&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-1&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_tv_fwd_sm" class="position_bottom supplemental" >Discover the 10 biggest questions for Season 2</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
                if(typeof uet === 'function'){uet("af");}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-2"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/video/imdb/vi2187310361/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2464219182&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_vd_nsp_mk_hd" > <h3>"No Small Parts" IMDb Exclusive: Domhnall Gleeson</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:170px;height:auto;" > <div style="width:170px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt2488496/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2464219182&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_vd_nsp_mk_i_1" > <img itemprop="image" class="pri_image" title="Harrison Ford, Anthony Daniels, Carrie Fisher, Peter Mayhew, Oscar Isaac, Lupita Nyong'o, Adam Driver, Gwendoline Christie, John Boyega and Daisy Ridley in Star Wars: Episode VII - The Force Awakens (2015)" alt="Harrison Ford, Anthony Daniels, Carrie Fisher, Peter Mayhew, Oscar Isaac, Lupita Nyong'o, Adam Driver, Gwendoline Christie, John Boyega and Daisy Ridley in Star Wars: Episode VII - The Force Awakens (2015)" src="http://ia.media-imdb.com/images/M/MV5BOTAzODEzNDAzMl5BMl5BanBnXkFtZTgwMDU1MTgzNzE@._V1_SX170_CR0,0,170,250_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BOTAzODEzNDAzMl5BMl5BanBnXkFtZTgwMDU1MTgzNzE@._V1_SX170_CR0,0,170,250_AL_UY500_UX340_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:444px;height:auto;" > <div style="width:444px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/video/imdb/vi2187310361?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2464219182&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_vd_nsp_mk_i_2" data-video="vi2187310361" data-source="bylist" data-id="ls036992962" data-rid="11PBAWT29DWDX12AGDK6" data-type="playlist" class="video-colorbox" data-refsuffix="hm_vd_nsp_mk" data-ref="hm_vd_nsp_mk_i_2"> <img itemprop="image" class="pri_image" title="Star Wars: Episode VII - The Force Awakens (2015)" alt="Star Wars: Episode VII - The Force Awakens (2015)" src="http://ia.media-imdb.com/images/M/MV5BMTU1MDkzMzQxM15BMl5BanBnXkFtZTgwODUyNzg0NzE@._UX888_CR0,0,888,500_SY250_SX444_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTU1MDkzMzQxM15BMl5BanBnXkFtZTgwODUyNzg0NzE@._UX888_CR0,0,888,500_SY250_SX444_AL_UY500_UX888_AL_.jpg" /> <img alt="Star Wars: Episode VII - The Force Awakens (2015)" title="Star Wars: Episode VII - The Force Awakens (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button._CB318667375_.png" /> <img alt="Star Wars: Episode VII - The Force Awakens (2015)" title="Star Wars: Episode VII - The Force Awakens (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/play-button-hover._CB318667374_.png" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Irish actor <a href="/name/nm1727304/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2464219182&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_vd_nsp_mk_lk1">Domhnall Gleeson</a> made a name for himself this past year by appearing in four films that were nominated for Academy Awards. Find out where this versatile star of <i><a href="/title/tt2488496/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2464219182&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_vd_nsp_mk_lk2">The Force Awakens</a></i> got his start.</p> <p class="seemore"><a href="/video/imdb/vi2187310361/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2464219182&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-2&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_vd_nsp_mk_sm" class="position_bottom supplemental" >Learn more about Domhnall Gleeson's on-screen career</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-3"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NewsDeskWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="newsdesk" id="newsdesk">
    <h2 class="float-left">Latest News</h2>
    <div class="newsTabs">
<a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp_tb"
class="active" id="tnw" ><strong>Top</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv_tb"
id="mnw" ><strong>Movies</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv_tb"
id="tvw" ><strong>TV</strong></a>        &nbsp;<span class="ghost">|</span>&nbsp;
<a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel_tb"
id="cnw" ><strong>Celebs</strong></a>        
    </div>
        <div class="channel " id="tnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59702721?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjQzNTUwNDU0OV5BMl5BanBnXkFtZTgwNzU5NTAwODE@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59702721?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1"
class="headlines" >Melissa McCarthy's 'The Boss' Narrowly Tops 'Batman v Superman' for Weekend #1</a>
    <div class="infobar">
            <span class="text-muted">2 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000159?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_src"
>Box Office Mojo</a></span>
    </div>
                                </div>
<p><a href="/name/nm0565250?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk1">Melissa McCarthy</a>'s <a href="/title/tt2702724?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk2">The Boss</a> and <a href="/title/tt2975590?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk3">Batman v Superman: Dawn of Justice</a> are in a tight race for the weekend number one with a mere $45,000 giving the edge to <a href="/title/tt2702724?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk4">The Boss</a> based on estimates. Also opening this weekend, Stx's <a href="/title/tt3072482?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk5">Hardcore Henry</a> fell short of expectations while Fox Searchlight's <a href="/title/tt1172049?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_lk6">Demolition</a> was ...                                        <span class="nobr"><a href="/news/ni59702721?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59701878?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2"
class="headlines" >âThe Huntsman: Winterâs Warâ Waging $21.3M Weekend In Offshore Bow â Intl Box Office</a>
    <div class="infobar">
            <span class="text-muted">22 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp2_src"
>Deadline</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59701187?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3"
class="headlines" >Melissa McCarthy, Ben Falcone Boarding âLife of the Partyâ</a>
    <div class="infobar">
            <span class="text-muted">9 April 2016 2:55 AM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp3_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59702086?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4"
class="headlines" >âCaptain America: Civil Warâ First Reactions: âBest Comic Book Movie Fight Everâ</a>
    <div class="infobar">
            <span class="text-muted">18 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp4_src"
>The Wrap</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59702163?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5"
class="headlines" >Gabrielle Carteris Elected President of SAG-AFTRA</a>
    <div class="infobar">
            <span class="text-muted">16 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004912?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tp5_src"
>Deadline</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/top?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Top News</a></span>
        </div>
        <div class="channel hidden" id="mnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59702721?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMjQzNTUwNDU0OV5BMl5BanBnXkFtZTgwNzU5NTAwODE@._V1_SX101_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59702721?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1"
class="headlines" >Melissa McCarthy's 'The Boss' Narrowly Tops 'Batman v Superman' for Weekend #1</a>
    <div class="infobar">
            <span class="text-muted">2 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000159?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_src"
>Box Office Mojo</a></span>
    </div>
                                </div>
<p><a href="/name/nm0565250?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk1">Melissa McCarthy</a>'s <a href="/title/tt2702724?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk2">The Boss</a> and <a href="/title/tt2975590?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk3">Batman v Superman: Dawn of Justice</a> are in a tight race for the weekend number one with a mere $45,000 giving the edge to <a href="/title/tt2702724?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk4">The Boss</a> based on estimates. Also opening this weekend, Stx's <a href="/title/tt3072482?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk5">Hardcore Henry</a> fell short of expectations while Fox Searchlight's <a href="/title/tt1172049?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_lk6">Demolition</a> was ...                                        <span class="nobr"><a href="/news/ni59702721?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59701187?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2"
class="headlines" >Melissa McCarthy, Ben Falcone Boarding âLife of the Partyâ</a>
    <div class="infobar">
            <span class="text-muted">9 April 2016 2:55 AM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv2_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59701100?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3"
class="headlines" >âNarcosâ Star Pedro Pascal Joining âKingsmanâ Sequel</a>
    <div class="infobar">
            <span class="text-muted">9 April 2016 1:36 AM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0052791?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv3_src"
>Variety - Film News</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59701139?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4"
class="headlines" >Analyst: 'Batman v Superman' will be less profitable than 'Man of Steel'</a>
    <div class="infobar">
            <span class="text-muted">9 April 2016 2:15 AM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000173?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv4_src"
>Hitfix</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59701138?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5"
class="headlines" >The R-Rated Batman V Superman Cut May Be Coming To Theaters</a>
    <div class="infobar">
            <span class="text-muted">9 April 2016 1:40 AM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000098?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_mv5_src"
>cinemablend.com</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/movie?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Movies News</a></span>
        </div>
        <div class="channel hidden" id="tvw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59702095?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTk4NDUyMjk5Ml5BMl5BanBnXkFtZTgwODYxNjY0ODE@._V1_SY150_CR49,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59702095?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1"
class="headlines" >Is âFear the Walking Deadâ Star Colman Domingo the Showâs Negan?</a>
    <div class="infobar">
            <span class="text-muted">18 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000236?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_src"
>The Wrap</a></span>
    </div>
                                </div>
<p>As â<a href="/title/tt3743822?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk1">Fear the Walking Dead</a>â Season 1 came to an end, the blended families of Travis (<a href="/name/nm0193295?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk2">Cliff Curtis</a>), Madison (<a href="/name/nm0225332?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk3">Kim Dickens</a>) and Daniel (<a href="/name/nm0001952?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk4">Ruben Blades</a>) took refuge with the mysterious Victor Strand and planned to embark for safety on his yacht, The Abigail. Strand (<a href="/name/nm0231458?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_lk5">Colman Domingo</a>) remains mostly a ...                                        <span class="nobr"><a href="/news/ni59702095?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59701697?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2"
class="headlines" >Ratings: Sleepy Hollow Ticks Up With Finale, Hits 5-Week High in Audience</a>
    <div class="infobar">
            <span class="text-muted">9 April 2016 4:26 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv2_src"
>TVLine.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59701160?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3"
class="headlines" >Sebastian Maniscalco Sets Third Stand-Up Comedy Special At Showtime</a>
    <div class="infobar">
            <span class="text-muted">9 April 2016 2:04 AM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004913?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv3_src"
>Deadline TV</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59701623?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4"
class="headlines" >Performer of the Week: Robin Wright</a>
    <div class="infobar">
            <span class="text-muted">9 April 2016 3:32 PM, +01:00</span>
<span class="ghost">|</span>            <span><a href="/news/ns0013977?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv4_src"
>TVLine.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59702748?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5"
class="headlines" >âSaturday Night Liveâ Ratings Flat With Host Russell Crowe</a>
    <div class="infobar">
            <span class="text-muted">40 minutes ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0004913?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_tv5_src"
>Deadline TV</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/tv?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more TV News</a></span>
        </div>
        <div class="channel hidden" id="cnw">
            <div class="channel-content">
                                             <div class="primary-news">
                                <div class="primary-image">
                                    <div class="image">
<a href="/news/ni59702424?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_i"
> <img height="150"
width="101"
src="http://ia.media-imdb.com/images/M/MV5BMTk2MDExNzQwN15BMl5BanBnXkFtZTgwMzM2NzQwNjE@._V1_SY150_CR7,0,101,150_AL_.jpg" />
</a>                                     </div> 
                                </div>
                            <div class="primary-content">
                                <div class="news-header">
<a href="/news/ni59702424?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1"
class="headlines" >'Star Wars' Anakin Skywalker -- Schizophrenia Triggers Move From Jail To Hospital</a>
    <div class="infobar">
            <span class="text-muted">9 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000358?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_src"
>TMZ</a></span>
    </div>
                                </div>
<p><a href="/name/nm0005157?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk1">Jake Lloyd</a>, who played Anakin Skywalker in '<a href="/title/tt0120915?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_lk2">Star Wars: The Phantom Menace</a>,'Â has been moved from a jail cell to a psychiatric facility.Â  Jake's been behind bars since June for taking cops on a chase that exceeded speeds of 100 mph. The actor's mom, Lisa, tells us authorities reached the conclusion ...                                        <span class="nobr"><a href="/news/ni59702424?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel1_sm"
>See more</a> &#187;</span>
                                </p>
                            </div>
                        </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59701979?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2"
class="headlines" >Lindsey Vonn Spotted Leaving Dinner with Hunger Games Star Alexander Ludwig</a>
    <div class="infobar">
            <span class="text-muted">21 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000018?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel2_src"
>PEOPLE.com</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59702050?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3"
class="headlines" >Zach Braff's Birthday Weekend With Scrubs Co-Star Donald Faison Is Just What the Doctor Ordered</a>
    <div class="infobar">
            <span class="text-muted">19 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel3_src"
>E! Online</a></span>
    </div>
                            </div>
                            </div>
                                                <div class="summary-news">
                            <div class="news-header">
<a href="/news/ni59701850?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4"
class="headlines" >Kaley Cuoco Shows PDA With Karl Cook Again: "That's What Happy Looks Like"</a>
    <div class="infobar">
            <span class="text-muted">22 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel4_src"
>E! Online</a></span>
    </div>
                            </div>
                                                <div class="news-header">
<a href="/news/ni59702049?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5"
class="headlines" >Hugh Hefner Celebrates His 90th Birthday and Wife Crystal Hefner Shares a Sweet Photo</a>
    <div class="infobar">
            <span class="text-muted">19 hours ago</span>
<span class="ghost">|</span>            <span><a href="/news/ns0000141?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_cel5_src"
>E! Online</a></span>
    </div>
                            </div>
                            </div>
            </div>
        <span class="see-all"><a href="/news/celebrity?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418398782&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_nw_sm"
>See more Celebs News</a></span>
        </div>
    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NewsDeskWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-4"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <h3>IMDb Snapshot</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1398268160/rg3469646592?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465936182&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_1" > <img itemprop="image" class="pri_image" title="Daisy Ridley at event of Star Wars: Episode VII - The Force Awakens (2015)" alt="Daisy Ridley at event of Star Wars: Episode VII - The Force Awakens (2015)" src="http://ia.media-imdb.com/images/M/MV5BNjYxOTQ4ODg5Ml5BMl5BanBnXkFtZTgwNjYwNDM1NzE@._V1_SY201_CR56,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjYxOTQ4ODg5Ml5BMl5BanBnXkFtZTgwNjYwNDM1NzE@._V1_SY201_CR56,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/media/rm1398268160/rg3469646592?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465936182&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_1" > Happy Birthday to Daisy Ridley </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/photos-we-love-0408/rg1791204096?imageid=rm1735334912&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465936182&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_2" > <img itemprop="image" class="pri_image" title="Still of Elle Fanning in The Neon Demon (2016)" alt="Still of Elle Fanning in The Neon Demon (2016)" src="http://ia.media-imdb.com/images/M/MV5BOTk2NTE4NzE5Ml5BMl5BanBnXkFtZTgwNzQ2Mjg1ODE@._V1_SY201_CR42,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BOTk2NTE4NzE5Ml5BMl5BanBnXkFtZTgwNzQ2Mjg1ODE@._V1_SY201_CR42,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/imdbpicks/photos-we-love-0408/rg1791204096?imageid=rm1735334912&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465936182&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_2" > This Week's Photos We Love </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/outlander-cast-in-out-costume/rg834902784?imageid=rm201716736&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465936182&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_i_3" > <img itemprop="image" class="pri_image" title="Still of Finn Den Hertog in Outlander (2014)" alt="Still of Finn Den Hertog in Outlander (2014)" src="http://ia.media-imdb.com/images/M/MV5BMTg1Mzk0NzUxNl5BMl5BanBnXkFtZTgwODExODI4NTE@._V1_SY201_CR50,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg1Mzk0NzUxNl5BMl5BanBnXkFtZTgwODExODI4NTE@._V1_SY201_CR50,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/imdbpicks/outlander-cast-in-out-costume/rg834902784?imageid=rm201716736&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465936182&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_snp_cap_pri_3" > "Outlander" Stars in and out of Costume </a> </div> </div> </div> </div> </div> </div> </div> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-5"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','BornTodayWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_borntoday">
<span class="widget_header"> <span class="oneline"> <a href="/search/name?refine=birth_monthday&birth_monthday=4-10&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_hd" > <h3>Born Today</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm5397459?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_1" > <img itemprop="image" class="pri_image" title="Daisy Ridley" alt="Daisy Ridley" src="http://ia.media-imdb.com/images/M/MV5BMTgzMDk3MjI4OF5BMl5BanBnXkFtZTgwMzQxMDY5NjE@._V1_SY172_CR11,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTgzMDk3MjI4OF5BMl5BanBnXkFtZTgwMzQxMDY5NjE@._V1_SY172_CR11,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm5397459?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_1">Daisy Ridley</a> (24) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0402271?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_2" > <img itemprop="image" class="pri_image" title="Charlie Hunnam" alt="Charlie Hunnam" src="http://ia.media-imdb.com/images/M/MV5BMjE5NjE5Mzk2MV5BMl5BanBnXkFtZTcwODI5MDE1Ng@@._V1_SY172_CR3,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjE5NjE5Mzk2MV5BMl5BanBnXkFtZTcwODI5MDE1Ng@@._V1_SY172_CR3,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0402271?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_2">Charlie Hunnam</a> (36) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0500200?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_3" > <img itemprop="image" class="pri_image" title="Chyler Leigh" alt="Chyler Leigh" src="http://ia.media-imdb.com/images/M/MV5BMTc5NjUwMzEwMl5BMl5BanBnXkFtZTcwOTc4Mzk5MQ@@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc5NjUwMzEwMl5BMl5BanBnXkFtZTcwOTc4Mzk5MQ@@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0500200?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_3">Chyler Leigh</a> (34) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm1641117?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_4" > <img itemprop="image" class="pri_image" title="Alex Pettyfer" alt="Alex Pettyfer" src="http://ia.media-imdb.com/images/M/MV5BMTcwNDIxNTY0M15BMl5BanBnXkFtZTgwODM0MDUzODE@._V1_SY172_CR10,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTcwNDIxNTY0M15BMl5BanBnXkFtZTgwODM0MDUzODE@._V1_SY172_CR10,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm1641117?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_4">Alex Pettyfer</a> (26) </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/name/nm0005286?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_i_5" > <img itemprop="image" class="pri_image" title="Haley Joel Osment" alt="Haley Joel Osment" src="http://ia.media-imdb.com/images/M/MV5BNzI1NDQyMDQ4Ml5BMl5BanBnXkFtZTYwNDIxMjMz._V1_SY172_CR10,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNzI1NDQyMDQ4Ml5BMl5BanBnXkFtZTYwNDIxMjMz._V1_SY172_CR10,0,116,172_AL_UY344_UX232_AL_.jpg" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/name/nm0005286?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_cap_pri_lk1_5">Haley Joel Osment</a> (28) </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/search/name?refine=birth_monthday&birth_monthday=4-10&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2418400662&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-5&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_brn_sm" class="position_bottom supplemental" >See all birthdays</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','BornTodayWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-7"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/awards-central/mtv-movie-awards-2016-red-carpet/rg700685056?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465941942&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_mtv_ph_hd" > <h3>2016 MTV Movie Awards: Red Carpet Photos</h3> </a> </span> </span> <p class="blurb">Check out our photos from the <a href="/awards-central/mtv-movie-awards/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465941942&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_mtv_ph_lk1">2016 MTV Movie Awards</a> red carpet.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/mtv-movie-awards-2016-red-carpet/rg700685056?imageid=rm1466113024&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465941942&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_mtv_ph_i_1" > <img itemprop="image" class="pri_image" title="Kendall Jenner" alt="Kendall Jenner" src="http://ia.media-imdb.com/images/M/MV5BNDkxNjUxNDUxM15BMl5BanBnXkFtZTgwMzIxNDg1ODE@._V1_SX201_CR0,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNDkxNjUxNDUxM15BMl5BanBnXkFtZTgwMzIxNDg1ODE@._V1_SX201_CR0,0,201,298_AL_UY596_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/mtv-movie-awards-2016-red-carpet/rg700685056?imageid=rm2774735872&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465941942&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_mtv_ph_i_2" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMjI2OTYzMzUzNl5BMl5BanBnXkFtZTgwMzQxNDg1ODE@._UY596_CR380,0,402,596_SY298_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjI2OTYzMzUzNl5BMl5BanBnXkFtZTgwMzQxNDg1ODE@._UY596_CR380,0,402,596_SY298_SX201_AL_UY596_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/awards-central/mtv-movie-awards-2016-red-carpet/rg700685056?imageid=rm1315118080&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465941942&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_mtv_ph_i_3" > <img itemprop="image" class="pri_image" title="Chris Pratt" alt="Chris Pratt" src="http://ia.media-imdb.com/images/M/MV5BMjA4MDY5NzQ0N15BMl5BanBnXkFtZTgwNjExNDg1ODE@._V1_SY298_CR123,0,201,298_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjA4MDY5NzQ0N15BMl5BanBnXkFtZTgwNjExNDg1ODE@._V1_SY298_CR123,0,201,298_AL_UY596_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/awards-central/mtv-movie-awards-2016-red-carpet/rg700685056?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465941942&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-7&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ac_mtv_ph_sm" class="position_bottom supplemental" >See all the photos</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-8"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/gallery/rg2076351232?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2462646682&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_sib_hd" > <h3>Star Siblings: Famous Brothers and Sisters</h3> </a> </span> </span> <p class="blurb">Celebrate National Sibling Day with a look at some famous Hollywood families.</p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3421234944/rg2076351232?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2462646682&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_sib_i_1" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTQ2MjE2Nzk0OV5BMl5BanBnXkFtZTgwNTE4MDg2NzE@._UX600_CR80,50,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ2MjE2Nzk0OV5BMl5BanBnXkFtZTgwNTE4MDg2NzE@._UX600_CR80,50,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm1391768320/rg2076351232?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2462646682&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_sib_i_2" > <img itemprop="image" class="pri_image" src="http://ia.media-imdb.com/images/M/MV5BMTQ4NzM5ODIxOV5BMl5BanBnXkFtZTcwMTI4MzUwNw@@._UX402_CR0,50,402,402_SY201_SX201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ4NzM5ODIxOV5BMl5BanBnXkFtZTcwMTI4MzUwNw@@._UX402_CR0,50,402,402_SY201_SX201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:201px;height:auto;" > <div style="width:201px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3763975168/rg2076351232?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2462646682&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_sib_i_3" > <img itemprop="image" class="pri_image" title="Rosanna Arquette at event of Girl in Progress (2012)" alt="Rosanna Arquette at event of Girl in Progress (2012)" src="http://ia.media-imdb.com/images/M/MV5BMTQxNjI4NDc4Ml5BMl5BanBnXkFtZTcwNTcxNzU3Nw@@._V1_SX201_CR0,0,201,201_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQxNjI4NDc4Ml5BMl5BanBnXkFtZTcwNTcxNzU3Nw@@._V1_SX201_CR0,0,201,201_AL_UY402_UX402_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/gallery/rg2076351232?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2462646682&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_sib_sm" class="position_bottom supplemental" >See the full gallery</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
        <a name="slot_center-9"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/gallery/rg3234568960?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2462601582&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-9&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_tv_ouat_hd" > <h3>TV Spotlight: Season 5 Photos From "Once Upon a Time"</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm2027625472/rg3234568960?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2462601582&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-9&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_tv_ouat_i_1" > <img itemprop="image" class="pri_image" title="Still of Rebecca Mader in Once Upon a Time (2011)" alt="Still of Rebecca Mader in Once Upon a Time (2011)" src="http://ia.media-imdb.com/images/M/MV5BMTQ2OTgzODc2M15BMl5BanBnXkFtZTgwNjI1MTU1ODE@._V1_SY230_CR19,0,307,230_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTQ2OTgzODc2M15BMl5BanBnXkFtZTgwNjI1MTU1ODE@._V1_SY230_CR19,0,307,230_AL_UY460_UX614_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:307px;height:auto;" > <div style="width:307px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/media/rm3314818816/rg3234568960?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2462601582&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-9&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_tv_ouat_i_2" > <img itemprop="image" class="pri_image" title="Still of Jennifer Morrison in Once Upon a Time (2011)" alt="Still of Jennifer Morrison in Once Upon a Time (2011)" src="http://ia.media-imdb.com/images/M/MV5BNjI2NzQ1OTAzNV5BMl5BanBnXkFtZTgwNjQwNjg0ODE@._V1_SX307_CR0,0,307,230_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjI2NzQ1OTAzNV5BMl5BanBnXkFtZTgwNjQwNjg0ODE@._V1_SX307_CR0,0,307,230_AL_UY460_UX614_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="seemore"><a href="/gallery/rg3234568960?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2462601582&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-9&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_ph_tv_ouat_sm" class="position_bottom supplemental" >See more photos from "Once Upon a Time"</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-11"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/feature/watch-now-on-amazon/?tag=imdb-aiv-tv-20&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465986502&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_aiv_hd" > <h3>Now Trending on Amazon Video</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465986502&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYkdoxGfYagePSmqCXuist3_a_43KN5nJEdTft8PoNF_8LLxvCthNPAdGFikCgTvCWozUAevM3F%0D%0AdK0OcokAknVnL2TteefxlFS9dtlje6LwIysi1SGMlZ4_71sT_VBii8S7Gc0W_MqnUJjao_JR2XKe%0D%0ARRRX5L-LNCEAIICluwNkhQq4G6u-5UVssjJ3OxyFgQ9DzAa2zPiczAZ2zy7H2-KWAmuxSsU1aZCO%0D%0AVoyMkcrL2jVhI85jsXsV7G15Fulakb6o9uV5exzyI-NNf1HvoxakOA%0D%0A&ref_=hm_aiv_i_1" > <img itemprop="image" class="pri_image" title="Harrison Ford, Anthony Daniels, Carrie Fisher, Peter Mayhew, Oscar Isaac, Lupita Nyong'o, Adam Driver, Gwendoline Christie, John Boyega and Daisy Ridley in Star Wars: Episode VII - The Force Awakens (2015)" alt="Harrison Ford, Anthony Daniels, Carrie Fisher, Peter Mayhew, Oscar Isaac, Lupita Nyong'o, Adam Driver, Gwendoline Christie, John Boyega and Daisy Ridley in Star Wars: Episode VII - The Force Awakens (2015)" src="http://ia.media-imdb.com/images/M/MV5BOTAzODEzNDAzMl5BMl5BanBnXkFtZTgwMDU1MTgzNzE@._V1_SY172_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BOTAzODEzNDAzMl5BMl5BanBnXkFtZTgwMDU1MTgzNzE@._V1_SY172_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> <img alt="Harrison Ford, Anthony Daniels, Carrie Fisher, Peter Mayhew, Oscar Isaac, Lupita Nyong'o, Adam Driver, Gwendoline Christie, John Boyega and Daisy Ridley in Star Wars: Episode VII - The Force Awakens (2015)" title="Harrison Ford, Anthony Daniels, Carrie Fisher, Peter Mayhew, Oscar Isaac, Lupita Nyong'o, Adam Driver, Gwendoline Christie, John Boyega and Daisy Ridley in Star Wars: Episode VII - The Force Awakens (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" /> <img alt="Harrison Ford, Anthony Daniels, Carrie Fisher, Peter Mayhew, Oscar Isaac, Lupita Nyong'o, Adam Driver, Gwendoline Christie, John Boyega and Daisy Ridley in Star Wars: Episode VII - The Force Awakens (2015)" title="Harrison Ford, Anthony Daniels, Carrie Fisher, Peter Mayhew, Oscar Isaac, Lupita Nyong'o, Adam Driver, Gwendoline Christie, John Boyega and Daisy Ridley in Star Wars: Episode VII - The Force Awakens (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465986502&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYjPyKXr1LtYOFOjh0olEKhOtcSKU2EgQ9Hahxvfj9gjtMXAltP06ySdfH4IzO2T-K2ns391O5q%0D%0A5PPQS6bDNkxT6owA1BtGOnQmV4th9sCC8I4HZEgomiGSeWKhrMycUqeXUDVa9MNmjKH9epFhVqgt%0D%0AOEaLmxf7byoOL2lUcM58Hh5LsQxqJFKSGg6j7C4qBLuzdDmlYYSBwSvlbIM3l3Zwk2rdEpH6V_-Y%0D%0AAi26FtXg_K2kGuBzwVLbrOuji0E6KTO6zL13uWWfr9F8efxoxzwj_g%0D%0A&ref_=hm_aiv_cap_pri_1" > <i>Star Wars: The Force Awakens</i> </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465986502&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYjnde-o5dWXd1t8lyTJdaSE1qr0dRjNRQN7lp90pRk6uREz6EcFMFT8xkuyPfnudTIzK79072U%0D%0AvRs4KI321KbogjLjFc0Kj_qTa7e5kEOWnh9ya4nmv_LjqK-4vTwYvD1UyQllFAb8LBp0NHvQsJRf%0D%0ASQueai3CF4CnpXwS7vs7UDkBpgIL6Rc11zHl9x_QS9Dp069ysfRnuUQ5JuM4hIwldYol9U1YESGH%0D%0A_V9JZLgq0DVFjWaABbbU1bIwZ7knebRJrBHRizm4vuEU6CRdlpNadQ%0D%0A&ref_=hm_aiv_i_2" > <img itemprop="image" class="pri_image" title="Jennifer Lawrence in The Hunger Games: Mockingjay - Part 2 (2015)" alt="Jennifer Lawrence in The Hunger Games: Mockingjay - Part 2 (2015)" src="http://ia.media-imdb.com/images/M/MV5BNjQzNDI2NTU1Ml5BMl5BanBnXkFtZTgwNTAyMDQ5NjE@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNjQzNDI2NTU1Ml5BMl5BanBnXkFtZTgwNTAyMDQ5NjE@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> <img alt="Jennifer Lawrence in The Hunger Games: Mockingjay - Part 2 (2015)" title="Jennifer Lawrence in The Hunger Games: Mockingjay - Part 2 (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" /> <img alt="Jennifer Lawrence in The Hunger Games: Mockingjay - Part 2 (2015)" title="Jennifer Lawrence in The Hunger Games: Mockingjay - Part 2 (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465986502&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYviRdcIsn8So_W6iz1roydfuWgYITp5Jh0GKryQIC6RAmJ6chz0h1h9UZzYKYukE7h5pqOFNkI%0D%0Ay78FFki81pwY8ZytBRT1m-E77uQGaKSmLFvjzNvfcBfXxHd1xgF579awc6fEjALpsEQZNDveYKoV%0D%0ALkOnItC3L2lSzQ6OjKYfp1OZOQV_EXEsJaiqlYAaSUeyciVEONdgG2DPp5JG_yMTh4UUsObtjl1T%0D%0Ai_GrSffTDcSFFZYXmjHWrJKJuK-TQhTNfOOkKBRq6pUkSJYND8AUaQ%0D%0A&ref_=hm_aiv_cap_pri_2" > <i>The Hunger Games: Mockingjay Part 2</i> </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465986502&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYj8h0NEt3qiAH48WWKu5fmXSYakEaBrNQketWPhGas-QWCiIGYSWSIiYee13u_Hz2gU2zWqVmz%0D%0ASptfMrHXy1QNQQA71WUaQi_U9hH35xNc44d1Vcizot-Z9rSegzdARX_OtX1USEUEtKK6Xns8nyQJ%0D%0A_rhin5jkC1Me5tuIvdw8PRxfi_3MLYft0trioGDON2W4GcaZdfxjRfiNqDNs5y3Ivz7XSb-uJ716%0D%0Al53pSJ6EkNi36_Fc9CZqK_iyTvku8awOkIg-rqHYn2vzlD-YvTaMrQ%0D%0A&ref_=hm_aiv_i_3" > <img itemprop="image" class="pri_image" title="Keri Russell and Matthew Rhys in The Americans (2013)" alt="Keri Russell and Matthew Rhys in The Americans (2013)" src="http://ia.media-imdb.com/images/M/MV5BMTg3Njk0MTAwMF5BMl5BanBnXkFtZTgwMTUyNDM3NzE@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTg3Njk0MTAwMF5BMl5BanBnXkFtZTgwMTUyNDM3NzE@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> <img alt="Keri Russell and Matthew Rhys in The Americans (2013)" title="Keri Russell and Matthew Rhys in The Americans (2013)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" /> <img alt="Keri Russell and Matthew Rhys in The Americans (2013)" title="Keri Russell and Matthew Rhys in The Americans (2013)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465986502&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYj3ZRpVE-v11rFdZGskgUr18dqKGcKo1dwuG-j3cYqzu9w0OZxfLmtm6ynruDeeFd5iB02jP7j%0D%0AyFHIwrT_uY5v3AwPv-uNYFw_lQ6xCxM_Or9grFWzs3NQHljGpjmF6Rtep-sBfmzH5_698S9FVxFm%0D%0A5LgSlXAbEdQH26EBi2pTs75E57sHVLq4Ywh2Tkh-baRZHweYPiDqFXeFk_QT57UFCUOdXFOlNJJg%0D%0Aw1H3dcoxY8NtNX8d61wvyz85gDOKzKdRFaZbVTJ2a53e5g1ZFNefKw%0D%0A&ref_=hm_aiv_cap_pri_3" > "The Americans - Season 4" </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465986502&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYtpVcaBCMOGxoAvlS5s4RTT1o003b91kg1KgmJwnYZKq79v523RsCBbJ1hdMU_M4Di4SZu2wQm%0D%0AlHzU789bytrxE-hUTwojJBrbUTlG6eCnNgFygl1nMSWtnb25vlr0FIUKGxP53xLcXZ_Eu8jydX1q%0D%0AcPDfAHVq72O6iDsJAA-oRjIL-NTH9qFreMqoam4jQbWCVv3p_Cz6-hFBCFxIJmgGccV3_3kCE43R%0D%0A0qKDegRy_8HveVEUXb05kQ6uv074IIgrrrVZfffM2XwE508mK9xFKg%0D%0A&ref_=hm_aiv_i_4" > <img itemprop="image" class="pri_image" title="Cuba Gooding Jr. in American Crime Story (2016)" alt="Cuba Gooding Jr. in American Crime Story (2016)" src="http://ia.media-imdb.com/images/M/MV5BMTc4Mzg0MzMwMF5BMl5BanBnXkFtZTgwNzQ3NTY3NzE@._V1_SX116_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMTc4Mzg0MzMwMF5BMl5BanBnXkFtZTgwNzQ3NTY3NzE@._V1_SX116_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> <img alt="Cuba Gooding Jr. in American Crime Story (2016)" title="Cuba Gooding Jr. in American Crime Story (2016)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" /> <img alt="Cuba Gooding Jr. in American Crime Story (2016)" title="Cuba Gooding Jr. in American Crime Story (2016)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465986502&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYoLRozMZglcWVfvkPVgtm_3dP6ArLjXk0ljcDR0eeraFhEsywO2VT8NPqPFktbLlinmjtIaLK0%0D%0AHex1-nI5T61Km6iIIiVxYMHJvFmBCX2SfGe2GXWTS70zXmGnmCf0e4khpEeo42kDAZLNOrS6u72V%0D%0AIROZuDlV0fdvakf7pb825lfHuoX7QRaMB63-vOY9xfOj2iADNQJlswbKpDLvXY-Zc3AY_jySOwlX%0D%0Argxxu4pvKddeCDbjMR7fJb2SiA0s4bEXKUESHGBPqdTXHXdrNwgtrg%0D%0A&ref_=hm_aiv_cap_pri_4" > "The People v. O.J. Simpson: American Crime Story" </a> </div> </div> </div> </div> </div><div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="ninja_image last_image" style="width:116px;height:auto;" > <div style="width:116px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465986502&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYpH9VRQ9gAtgYJKpLAmoN1v_ZKKz4jK-p88pOzNESWnkQG74TtKj_WC_Fe9pdG8deCvA3xrozh%0D%0AbfOP6esbhLQ3XXMi_xTI_vezJh5QZ8KV7EdNVlIndCM5N4lhQCdcz_qhqUh7TWF6XCZYOfMwWIUO%0D%0AOuv7jqC6FSD_Fu2WeDCOtcdhKghs5RkH33hFHWeeFRD-oSXxf5F8x-jpMJJKRaTDrtiYlwZNYCd5%0D%0AkLhcjj-fXlsTgwsabTFqBAKAUbUlZqXhU3Uucw3WYIiWDdtNLkO9uQ%0D%0A&ref_=hm_aiv_i_5" > <img itemprop="image" class="pri_image" title="Leonardo DiCaprio in The Revenant (2015)" alt="Leonardo DiCaprio in The Revenant (2015)" src="http://ia.media-imdb.com/images/M/MV5BMjU4NDExNDM1NF5BMl5BanBnXkFtZTgwMDIyMTgxNzE@._V1_SY172_CR0,0,116,172_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjU4NDExNDM1NF5BMl5BanBnXkFtZTgwMDIyMTgxNzE@._V1_SY172_CR0,0,116,172_AL_UY344_UX232_AL_.jpg" /> <img alt="Leonardo DiCaprio in The Revenant (2015)" title="Leonardo DiCaprio in The Revenant (2015)" class="image_overlay overlay_mouseout" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button._CB304896345_.png" /> <img alt="Leonardo DiCaprio in The Revenant (2015)" title="Leonardo DiCaprio in The Revenant (2015)" class="image_overlay overlay_mouseover" src="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" data-src-x2="http://ia.media-imdb.com/images/G/01/IMDb/icon/external-button-hover._CB304896345_.png" /> </a> </div> <div class="widget_caption caption_below"> <div class="primary"> <a href="/offsite/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465986502&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&page-action=offsite-amazon&token=BCYkoJOHfi8clzWoI7USdEhrU0bgzplf232rwsk1Q0YG9MzQ7GW3lEKT5BATHTBLfS6qRU8Otn3K%0D%0Ag-TbXGHUWxt78219A-KOu9aAMgiP6Js8wobZMFhEzEmhiVPViP2GvCNmeJ6_qG93IiIE092SzdfC%0D%0AbGoKh76-YMeWyhsJVJQ6TTSvA7dtDEjGui3Eg_QAYL6DvFerHmZ3jAJuYw7aXhwdCPyEtfrHbvUd%0D%0AW1V37KsQt22dWQeQNerUZJAkq5eaRVCpNNTDzQuzTGrjsYlE12uT0Q%0D%0A&ref_=hm_aiv_cap_pri_5" > <i>The Revenant</i> </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">While <i>Star Wars: Episode VII - The Force Awakens</i> continues to top the charts on Amazon Video, "The Americans - Season 4" and "The People v. O.J. Simpson: American Crime Story" are also trending.</p> <p class="seemore"><a href="/feature/watch-now-on-amazon/?tag=imdb-aiv-tv-20&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465986502&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-11&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_aiv_sm" class="position_bottom supplemental" >See which other movies and shows are trending on Amazon Video</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_center-13"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','RecsWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="homepage">
        <div id="lateload-recs-widget"></div>    
    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','RecsWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-19"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','TriviaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_trivia">
<span class="widget_header"> <span class="oneline"> <a href="/title/tt2638144/trivia?item=tr2645541&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_hd" > <h3>Did You Know?</h3> </a> </span> </span> <div class="widget_content inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image last_image" style="width:89px;height:auto;" > <div style="width:89px;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/title/tt2638144?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_i_1" > <img itemprop="image" class="pri_image" title="Ben-Hur (2016)" alt="Ben-Hur (2016)" src="http://ia.media-imdb.com/images/M/MV5BNzU5ODQzOTUwMF5BMl5BanBnXkFtZTgwMzMwMjUzODE@._V1_SX89_CR0,0,89,132_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNzU5ODQzOTUwMF5BMl5BanBnXkFtZTgwMzMwMjUzODE@._V1_SX89_CR0,0,89,132_AL_UY264_UX178_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> <div class="ninja_image ninja_image_fixed_padding widget_padding"></div><div class="widget_inline_blurb"><strong class="text-large"><a href="/title/tt2638144?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">Ben-Hur</a></strong> <p class="blurb">Director Timur Bekmambetov explained the film's adaptation in a interview with Collider: "When we say "original Ben-Hur," we have to be very concrete about which original version we are talking about. The are two big screen versions made, in 1925 and 1959. These are the two most famous ones. There was also a Broadway stage version at the beginning of the [20th] century. There have been a lot of television versions. The Ben-Hur story reminds me of "Romeo and Juliet," "Hamlet," and any story written by Chekhov. It is timeless, so every new generation wants to go back to it in order to adapt it for the new world. The screen version made in 1959 runs for four hours, and there is only a small number of people who can actually stay through the whole movie. It is about people different from us. And it's normal, because people used to be different. The audience was different, too, as well as the cinema language the film was made in. The 1959 movie was about revenge, not about forgiveness. For me that was the main problem, as I think that the novel is mainly about forgiveness, about the fact that a human being learned how to forgive. I got so excited about the project when I read <a href="/name/nm0725983?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_lk1">John Ridley</a>'s script. I understood that John's vision of the story has so much light to it, and that he shares the same thoughts about a certain morals as I do. We talked with him about our modern world, which actually reminds me very much of a huge Roman Empire. In the Roman Empire, the most important important values were pride, rivalry, power, strength, the dictatorship of power and self-love. This kind of world does not have any prospects today. Humanity has to learn how to love and forgive. This would be our only solution."</p> <p class="seemore"><a href="/title/tt2638144/trivia?item=tr2645541&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2016840082&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-19&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_trv_sm" class="position_blurb supplemental" >See more trivia</a></p> </div> </div>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','TriviaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_center-26"></a>
        <div class="article">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','PollWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget" data-json="{&quot;allowvoting&quot;:false,&quot;pollid&quot;:&quot;Zq6egvxpGxM&quot;}">
        
    

    <div class="ab_poll poll">
<span class="widget_header"> <span class="oneline"> <a href="/poll/Zq6egvxpGxM/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465971042&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_hd" > <h3>Poll: Face-Off: Iron Man vs. Armored Batman</h3> </a> <span>&nbsp;|&nbsp;</span> <h4><a href="http://www.imdb.com/poll/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465971042&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_mp">More Polls</a></h4> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_left"> <div class="ninja_image first_image" style="width:49%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/Zq6egvxpGxM/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465971042&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_1" > <img itemprop="image" class="pri_image" title="Still of Robert Downey Jr. in The Avengers (2012)" alt="Still of Robert Downey Jr. in The Avengers (2012)" src="http://ia.media-imdb.com/images/M/MV5BMjMwMzM2MTg1M15BMl5BanBnXkFtZTcwNjM4ODY3Nw@@._V1_SY462_CR255,0,311,462_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BMjMwMzM2MTg1M15BMl5BanBnXkFtZTcwNjM4ODY3Nw@@._V1_SY462_CR255,0,311,462_AL_UY924_UX622_AL_.jpg" /> </a> </div> </div> </div> </div><div class="ninja_image ninja_image_relative_padding widget_padding" style="width:2%;"></div><div class="ninja_image last_image" style="width:49%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/poll/Zq6egvxpGxM/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465971042&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_i_2" > <img itemprop="image" class="pri_image" title="Still of Ben Affleck in Batman v Superman: Dawn of Justice (2016)" alt="Still of Ben Affleck in Batman v Superman: Dawn of Justice (2016)" src="http://ia.media-imdb.com/images/M/MV5BZjZmNmIyOTMtYmFlMy00Y2QzLTk5NWMtM2EwN2Q2ODk4YmYxXkEyXkFqcGdeQXVyNjUwNzk3NDc@._V1_SY462_CR213,0,311,462_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BZjZmNmIyOTMtYmFlMy00Y2QzLTk5NWMtM2EwN2Q2ODk4YmYxXkEyXkFqcGdeQXVyNjUwNzk3NDc@._V1_SY462_CR213,0,311,462_AL_UY924_UX622_AL_.jpg" /> </a> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Two suave billionaires strapped in high-tech metal gear. Who do you think would win in a fight?<br /><br />Discuss <a href="http://www.imdb.com/board/bd0000088/nest/254815361?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465971042&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_lk1">here</a> after voting.</p> <p class="seemore"><a href="/poll/Zq6egvxpGxM/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2465971042&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=center-26&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_poll_sm" class="position_bottom supplemental" >Vote now</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','PollWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    
            </div>
            <div id="sidebar">
	
	<!-- begin TOP_RHS -->
<div id="top_rhs_wrapper" class="dfp_slot">
<script>
window.generic = window.generic || {};
generic.alphalfaComponents = {
"alphalfa-container.html" : "http://ia.media-imdb.com/images/G/01/imdbads/alphalfa-container-253150330._CB315223742_.html",
"js/collections/alphalfa.js" : "http://ia.media-imdb.com/images/G/01/imdbads/js/collections/alphalfa-3292747090._CB299577773_.js"
};
</script>
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('top_rhs');
});
</script>
<iframe data-dart-params="#imdb2.consumer.homepage/;!TILE!;sz=300x250,300x600,11x1;p=tr;fv=1;ab=a;bpx=2;c=0;s=3075;s=32;oe=utf-8;[CLIENT_SIDE_KEYVALUES];u=024101001491;ord=024101001491?" id="top_rhs" name="top_rhs" class="yesScript" width="0" height="0" data-original-width="0" data-original-height="0" data-config-width="0" data-config-height="0" data-cookie-width="null" data-cookie-height="null" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });"></iframe>
<noscript> <a href="http://pubads.g.doubleclick.net/gampad/jump?&iu=4215/imdb2.consumer.homepage/&sz=300x250|300x600|11x1&t=p%3Dtr%26fv%3D1%26ab%3Da%26bpx%3D2%26c%3D0%26s%3D3075%26s%3D32&tile=1&c=024101001491" target="_blank"> <img src="http://pubads.g.doubleclick.net/gampad/ad?&iu=4215/imdb2.consumer.homepage/&sz=300x250|300x600|11x1&t=p%3Dtr%26fv%3D1%26ab%3Da%26bpx%3D2%26c%3D0%26s%3D3075%26s%3D32&tile=1&c=024101001491" border="0" alt="advertisement" /> </a> </noscript>
</div>
<div id="top_rhs_reflow_helper"></div>
<div id="top_rhs_after" class="after_ad" style="display:none;">
<a class="yesScript" href="#" onclick="ad_utils.show_ad_feedback('top_rhs');return false;" id="ad_feedback_top_rhs">ad feedback</a>
</div>
<script>
doWithAds(function(){
ad_utils.gpt.render_ad('top_rhs');
}, "ad_utils not defined, unable to render client-side GPT ad.");
</script>
<!-- End TOP_RHS -->
	

    
    
    

    
    
    

    
    
        <a name="slot_right-3"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_hd" > <h3>Opening This Week</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt1172049"></div> <div class="title"> <a href="/title/tt1172049?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t0"> Demolition </a> <span class="secondary-text"></span> </div> <div class="action"> <a href="/showtimes/title/tt1172049?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3072482"></div> <div class="title"> <a href="/title/tt3072482?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t1"> Hardcore Henry </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2702724"></div> <div class="title"> <a href="/title/tt2702724?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t2"> The Boss </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2400463"></div> <div class="title"> <a href="/title/tt2400463?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t3"> The Invitation </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2217859"></div> <div class="title"> <a href="/title/tt2217859?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t4"> Back Home </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4794512"></div> <div class="title"> <a href="/title/tt4794512?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t5"> Wedding Doll </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2091935"></div> <div class="title"> <a href="/title/tt2091935?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t6"> Mr. Right </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt5217256"></div> <div class="title"> <a href="/title/tt5217256?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t7"> The Dying of the Light </a> <span class="secondary-text"></span> </div> <div class="action"> Limited </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4630158"></div> <div class="title"> <a href="/title/tt4630158?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_t8"> A Space Program </a> <span class="secondary-text"></span> </div> <div class="action"> Los Angeles </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-in-theaters/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2446567422&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-3&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_otw_sm" class="position_bottom supplemental" >See more opening this week</a></p>
                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','WatchableTitlesListWidget',{wb:1});}
            </script>
        




        </div>
    
	
	<!-- begin RHS_CORNERSTONE -->
<div id="rhs_cornerstone_wrapper" class="cornerstone_slot">
<script type="text/javascript">
doWithAds(function(){
ad_utils.register_ad('rhs_cornerstone');
});
</script>
<iframe id="rhs_cornerstone" name="rhs_cornerstone" class="yesScript" width="300" height="125" data-original-width="300" data-original-height="125" data-blank-serverside marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true" onload="doWithAds.call(this, function(){ ad_utils.on_ad_load(this); });" allowfullscreen="true"></iframe>
</div>
<div id="rhs_cornerstone_reflow_helper"></div>
<script>
doWithAds(function(){
ad_utils.inject_serverside_ad('rhs_cornerstone', '');
},"unable to inject serverside ad");
</script>
	

    
    
        <a name="slot_right-4"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="showtimesMessagingWidget"> <p class="blurb"><a href="/showtimes?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2006955922&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_sh_lk1">Get Showtimes &raquo;</a></p> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> </div> </div> </span>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-6"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','BoxOfficeListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/chart/boxoffice?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_hd" > <h3>Now Playing (Box Office)</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2702724"></div> <div class="title"> <a href="/title/tt2702724?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t0"> The Boss </a> <span class="secondary-text">Weekend: $23.5M</span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2975590"></div> <div class="title"> <a href="/title/tt2975590?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t1"> Batman v Superman: Dawn of Justice </a> <span class="secondary-text">Weekend: $23.4M</span> </div> <div class="action"> <a href="/showtimes/title/tt2975590?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt2948356"></div> <div class="title"> <a href="/title/tt2948356?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t2"> Zootopia </a> <span class="secondary-text">Weekend: $14.4M</span> </div> <div class="action"> <a href="/showtimes/title/tt2948356?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3760922"></div> <div class="title"> <a href="/title/tt3760922?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t3"> My Big Fat Greek Wedding 2 </a> <span class="secondary-text">Weekend: $6.4M</span> </div> <div class="action"> <a href="/showtimes/title/tt3760922?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3072482"></div> <div class="title"> <a href="/title/tt3072482?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_t4"> Hardcore Henry </a> <span class="secondary-text">Weekend: $5.1M</span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/chart/boxoffice?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2417984122&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-6&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cht_sm" class="position_bottom supplemental" >See more box office results</a></p>
                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','BoxOfficeListWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-8"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','WatchableTitlesListWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        <span class="widget_header"> <span class="oneline"> <a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_hd" > <h3>Coming Soon</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="rhs-body"> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3040964"></div> <div class="title"> <a href="/title/tt3040964?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t0"> The Jungle Book </a> <span class="secondary-text"></span> </div> <div class="action"> <a href="/showtimes/title/tt3040964?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_gs"> Showtimes </a> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3628584"></div> <div class="title"> <a href="/title/tt3628584?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t1"> Barbershop: The Next Cut </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3014866"></div> <div class="title"> <a href="/title/tt3014866?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t2"> Criminal </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt4062536"></div> <div class="title"> <a href="/title/tt4062536?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t3"> Green Room </a> <span class="secondary-text"></span> </div> </div> <div class="rhs-row"> <div class="ribbonize" data-tconst="tt3495026"></div> <div class="title"> <a href="/title/tt3495026?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_t4"> Fan </a> <span class="secondary-text"></span> </div> </div> </div> </div> </div> <p class="seemore"><a href="/movies-coming-soon/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=1971069222&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-8&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_cs_sm" class="position_bottom supplemental" >See more coming soon</a></p>
                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','WatchableTitlesListWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-12"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','TwitterWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_twitter">
<span class="widget_header"> <span class="oneline"> <h3>Follow Us On Twitter</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="twitter-frame" scrolling="no" seamless src="http://ia.media-imdb.com/images/G/01/imdb/html/twitter-3626940295._CB298540565_.html?config=%7B%22width%22%3A%22300px%22%2C%22height%22%3A%22500px%22%2C%22href%22%3A%22https%3A%2F%2Ftwitter.com%2FIMDb%22%2C%22heading%22%3A%22Follow%20Us%20On%20Twitter%22%2C%22screen-name%22%3A%22IMDb%22%2C%22list-name%22%3Anull%2C%22widget-id%22%3A%22354387022028357633%22%7D"></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','TwitterWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
        <a name="slot_right-14"></a>
        <div class="aux-content-widget-2">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','FacebookWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
            <div class="ab_facebook">
<span class="widget_header"> <span class="oneline"> <h3>Find Us On Facebook</h3> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <iframe class="facebook-frame" width="285" height="214" scrolling="no" seamless src='http://ia.media-imdb.com/images/G/01/imdb/html/facebook-869486875._CB313935095_.html'></iframe> </div> </div>    </div>

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','FacebookWidget',{wb:1});}
            </script>
        




        </div>
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
    

    
    
        <a name="slot_right-30"></a>
        <div class="aux-content-widget-2 sticky-widget">
        
    
        
                                

    
            <script type="text/javascript">if(typeof uet === 'function'){uet('bb','NinjaWidget',{wb:1});}</script>
                                

                    
    
        <span class="ab_widget">
        
    

    <div class="ab_ninja">
<span class="widget_header"> <span class="oneline"> <a href="/imdbpicks/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2461184062&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_hd" > <h3>IMDb Picks: April</h3> </a> </span> </span> <div class="widget_content no_inline_blurb"> <div class="widget_nested"> <div class="ninja_image_pack"> <div class="ninja_center"> <div class="ninja_image first_image last_image" style="width:100%;height:auto;" > <div style="width:100%;height:auto;margin:0 auto;"> <div class="widget_image"> <div class="image"> <a href="/imdbpicks/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2461184062&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_i_1" > <img itemprop="image" class="pri_image" title="Still of Emilia Clarke in Game of Thrones (2011)" alt="Still of Emilia Clarke in Game of Thrones (2011)" src="http://ia.media-imdb.com/images/M/MV5BNTE4NzY3MjY2NV5BMl5BanBnXkFtZTgwODY5Njk5NzE@._V1_SY525_CR44,0,700,525_AL_.jpg" data-src-x2="http://ia.media-imdb.com/images/M/MV5BNTE4NzY3MjY2NV5BMl5BanBnXkFtZTgwODY5Njk5NzE@._V1_SY525_CR44,0,700,525_AL_UY1050_UX1400_AL_.jpg" /> </a> <div class="widget_caption caption_overlay"> <div class="primary"> <div class="onoverflow"> <a href="/imdbpicks/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2461184062&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_cap_pri_1" > "Game of Thrones" </a> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <p class="blurb">Of course "<a href="/title/tt0944947/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2461184062&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_lk1">Game of Thrones</a>" is on our radar this month. See which other movies and TV shows we're excited about in <a href="/imdbpicks/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2461184062&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_lk2">IMDb Picks</a>.</p> <p class="seemore"><a href="/imdbpicks/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2461184062&pf_rd_r=11PBAWT29DWDX12AGDK6&pf_rd_s=right-30&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_pks_sm" class="position_bottom supplemental" >Visit IMDb Picks</a></p>    </div>
    

                        
        </span>



            <script type="text/javascript">
                if(typeof uex === 'function'){uex('ld','NinjaWidget',{wb:1});}
            </script>
        




        </div>
    
            </div>
            <br class="clear" />
        </div>
        

    
    
    

    
    
    
        
    
        <br class="clear" />
    </div>
</div>
                





    <div id="footer" class="ft">

            <div id="rvi-div">
                <div class="recently-viewed">&nbsp;</div>
                <br class="clear">
            </div>

	
	<!-- no content received for slot: bottom_ad -->
	

        <div class="container footer-grid-wrapper">
            <div class="row footer-row">
                <div class="col outside">
    <h3>IMDb Everywhere</h3>
    <div class="app-links">
    <a href="/offsite/?page-action=ft_app_apple&token=BCYglbUdLiHVwyrDQ1Efd1MmgNwLmS_ZijiIRjM5u4HTUg0h9ZHKR_KrY7ID_SCU4-YUV_iDO7vg%0D%0Axxhq-KRZ7ou_OghlWNY_CJQeLTEaZmlp09xGidbY4zORBOcguTnc_7rk5a9ykbXXhqDGodfpwYKb%0D%0AkZxFHr8OT7dn-X3KGldW1o2p7hvtmEZFS_ViLKZgXxpuZVT6ulmwB2LpR_oNFZ2zJCAdlcdb9FxS%0D%0ADGqz_HTu8is%0D%0A&ref_=ft_app_apple"
title="Get the IMDb App on the Apple App Store" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-apple" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_google&token=BCYqlVRxZr9lemqPOTtIWesGuI2Vj0uJFZt6xSHwyIKdIGN5V60JZ5CDJvSLBZ3snL63lDvZqSpJ%0D%0AnLEst_hskyrmLt3uO5GMPNfZjvS2MTM9qp9UYJ0o34eqvLomQErv4wOcgTXOqvqSlhj5PTWOmk5U%0D%0AYj1o83gAEDe9XaD2rvnjsWCPCdShVhxAny1ziZ7vLxtmzglDDIQ7i4SBIi-5luD4Wg%0D%0A&ref_=ft_app_google"
title="Get the IMDb app on Google Play" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-google" ></span>
</a>
    <a href="/offsite/?page-action=ft_app_amazon&token=BCYheS7CryFV0rCFEAeZCxwJHQDGbe19arvMH4J3vV4M4vyNmFIZ4tRBI2B5mx0DpREwEZ1vrSf6%0D%0AwG54J_rAyilNbp5Zk0pnas_rO7p4X6rMFjn4af4-6dRlTrgkBevYcN42dHLUrUaoR-5Bvwe1YGrg%0D%0AruHKWFgb_QTV-0YcSNcoHxozeWdvQwbIoVNLwAs_FnyivFo-_IWahBE1AOWJKx4Bf_ssO2VOKvkQ%0D%0A8yHHYpKsde1UwbKXvLFlfJJwUDsPbGkv_DekrvYZhw6rFTAOxszUrLpMAzmmwcuepv7bH8q9WmW9%0D%0AXmFl43mZvI3HGwkby6zDkmXlIrLxE9k5g8bih1GXOo8NgXNN7vXOoPq5ssB2t-wxxTA4rnAhOfiw%0D%0AO66zYG5YCqrwGQYNrVY8iA8q2EZ6EE1q3mg8-WebKR0TQZ-PS8o%0D%0A&ref_=ft_app_amazon"
title="Get the IMDb app on Amazon Appstore for Android" target="_blank" itemprop='url'> <span class="desktop-sprite appstore-amazon" ></span>
</a>
    </div>

    <p>Find showtimes, watch trailers, browse photos, track your Watchlist and rate your favorite movies and TV shows on your phone or tablet!</p>

    <a href="http://m.imdb.com?ref_=ft_mdot"
class="touchlink" >IMDb Mobile site</a>
                </div>
                <div class="col center">
  <div class="link-bar icon-link-bar">
    <h3>
      Follow IMDb on
      <div>

    <a href="/offsite/?page-action=fol_fb&token=BCYoNnlRhlj_bPVfQGPPX-br21uJ5WJ9KvdMtU6hqfsGoJVcrrzzHe70lCTXb5sofdudjBCYcsrt%0D%0A0qWnAqCnBALMCqsj6bR3wo3Fffg41LYGDkOhfhag6Frm3kTwcwytAVjf3DiVfnAJEtb1dzK06shY%0D%0A_4Uxb481DWHtZ5yYS1FDDFTKOfQqRz4km57LnloWxd0A2XC3XRwips9UX4YIb7VpjA%0D%0A&ref_=hm_ft_fol_fb"
title="Follow IMDb on Facebook" target="_blank" itemprop='url'> <span class="desktop-sprite follow-facebook" ></span>
</a>
    <a href="/offsite/?page-action=fol_tw&token=BCYln6nQM4ZDqNgCOLPzgd_5jn4ElqMP-R_HtwbRYmbTWRh6pfpUL5jn4LVGR1JldyAQrl28D9XA%0D%0AycA063es4Q2UwZwXMMQPf2iC9WzBO00Iu_wDxgHdFUApGSDNn3vuOrT8P79m8C4l6R2jC-I8Dihs%0D%0AQAD7JwIyzhsxtTNEjyrYSrcmDf3a8vPxos0iI3n6kEH3%0D%0A&ref_=hm_ft_fol_tw"
title="Follow IMDb on Twitter" target="_blank" itemprop='url'> <span class="desktop-sprite follow-twitter" ></span>
</a>
    <a href="/offsite/?page-action=fol_inst&token=BCYgjFJ_loab35do86Btzs_4APPf676i_VsOH0VB3HvGpy8jZOPhzE7lpPD6rdaNFomQvYuUMEI9%0D%0AkW4HmULU0F06FBe4Qga8U1Q3V3fLZJBWmaS463XHvgA0DyQwfsCaz-YfHv7BcIc_ZKgx-qnwXcG3%0D%0AKEKyaJD-pWi31GL0XAIuga7JB68zoc_1MsNdc8y5UGHs%0D%0A&ref_=hm_ft_fol_inst"
title="Follow IMDb on Instagram" target="_blank" itemprop='url'> <span class="desktop-sprite follow-instagram" ></span>
</a>
      </div>
    </h3>
  </div>
                </div>
                <div class="col outside">
    <div class="row">
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/?ref_=ft_hm"
>Home</a></li>
                <li><a href="/chart/top?ref_=ft_250"
>Top Rated Movies</a></li>
                <li><a href="/chart/?ref_=ft_cht"
>Box Office</a></li>
                <li><a href="/sections/tv/?ref_=ft_tv"
>TV</a></li>
                <li><a href="/movies-coming-soon/?ref_=ft_cs"
>Coming Soon</a></li>
                <li><a href="/a2z?ref_=ft_si"
>Site Index</a></li>
                <li><a href="/search?ref_=ft_sr"
>Search</a></li>
                <li><a href="/movies-in-theaters/?ref_=ft_inth"
>In Theaters</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="/helpdesk/contact?ref_=ft_con"
>Contact Us</a></li>
                <li><a href="/boards/?ref_=ft_mb"
>Message Boards</a></li>
                <li>        <a href="https://secure.imdb.com/register-imdb/form-v2?ref_=ft_reg"
>Register</a>
</li>
                <li><a href="/news/?ref_=ft_nw"
>News</a></li>
                <li class="spacer"></li>
                <li><a href="/pressroom/?ref_=ft_pr"
>Press Room</a></li>
                <li><a href="/advertising/?ref_=ft_ad"
>Advertising</a></li>
                <li><a href="/jobs?ref_=ft_jb"
>Jobs</a></li>
            </ul>
        </div>
        <div class="col col-4">
            <ul class="unstyled">
                <li><a href="http://pro.imdb.com/signup/index.html?rf=cons_ft_hm&ref_=cons_ft_hm"
>IMDbPro</a></li>
                <li>    <a href="/offsite/?page-action=ft-mojo&token=BCYtEh9BjJbXmYHlHOQZmnxryBjDcpkQ1upy4Xo2Nwv7cL2o2e_VVjH_xwJ37A8tFcs_K2GIMBkb%0D%0AxRRVcwAjoAqh_VEY91-cMujYVlr4u1TigbtwRqXCgSGp9gVx7FbfPCtVmMIgPJlCrYPdzipEpbo-%0D%0AsiypVXApVTEro96MBlnIFqshehbv6KIE0IYSoCb84RKI0ijg5_5EA7T2k6DQ_74i3Q%0D%0A&ref_=ft_bom"
itemprop='url'>Box Office Mojo</a>
</li>
                <li>    <a href="/offsite/?page-action=ft-wab&token=BCYu5r30F7TWESRXYneN9jL7ZTnPElqWsKJ5uiSAV8pVVHkCFkMa0fW8TBEF-L6SS5zBdUoNdG6k%0D%0AhCX1otvz3wX_rnSQpi4pqFyEDoVYti6qSJ3pUKi4vJyVpfO1zXpYvEtch--3qrRhM8jGWApYMIOB%0D%0A2XAf8r6C2GD857bpq3uKtcRTCYciao2mqYstkkC0Zwiz%0D%0A&ref_=ft_wab"
itemprop='url'>Withoutabox</a>
</li>
                <li class="spacer"></li>
                <li><a href="/help/show_article?conditions&ref_=ft_cou"
>Conditions of Use</a></li>
                <li><a href="/privacy?ref_=ft_pvc"
>Privacy Policy</a></li>
                <li>    <a href="/offsite/?page-action=ft-iba&token=BCYoE7khPazT8Y_G7xmItIHyTCB2aNWgOO0Wz_JInQQLqdR4HxPQj_RQ_BTavCwX-fyrBqI7J4zV%0D%0Aoj-tKx9Tk0PuyLE1NYcDMEcbUNdC7PC4cqtHlrEaQ5E1MILtwuIjOq4-wvseEgo75uuwW8vByxdU%0D%0AUbxhCkj8xRE1YVr5UCR5WT0j1FnA_9cpln2XOeFAY0EUdBf2bczfK4nL7ebBlucog0u84zaAv8ZX%0D%0Az5CmMXvdTOuWM1x_1qurm23bbHxsU024%0D%0A&ref_=ft_iba"
itemprop='url'>Interest-Based Ads</a>
</li>
                <li class="spacer"></li>
            </ul>
        </div>
    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="ft-copy float-right">
                <a
href="/help/show_article?conditions"
>Copyright &copy;</a> 1990-2016
                <a
href="/help/"
>IMDb.com, Inc.</a>
            </div>
            <div>
                An <span id="amazon_logo" class="footer_logo" align="middle">Amazon.com</span> company.
            </div>
        </div>



    <table class="footer" id="amazon-affiliates">
        <tr>
            <td colspan="8">
                Amazon Affiliates
            </td>
        </tr>
        <tr>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-aiv&token=BCYnZWuYUpEU8PCfGSESG8WXwkZy2_sBNpF-RkI5zKS8AGYMB-q_hKKhjQULObKv9KIZgtUJvjPu%0D%0A-R4XZcCBIDCIUtm8TmEq4CzKffHDMt7t6-BqsNsRsCE1G3KcgCHMGDDPhw_I7AFsHVXWvzV1HUBC%0D%0ARWNNzjPBm86gK1tClXazisBsJtjxm8m8dVLy1vhA-Cx0F607R_YRzoGK-GM_BXjT5LCrshcnEwGL%0D%0AY_P-gtuJ96E%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Video</span><br>
                <span class="amazon-affiliate-site-desc">Watch Movies &<br>TV Online</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-piv&token=BCYk5t_f9V20vxRDQbXIoLILHXL00P92XtA3g109KMDTuPY8ml8mLyZqxWggq5ppukRJGXCOO2N8%0D%0AWtQQ38EpK8UbwecPKQ5jxEoKdvjFmxXTOB63X5yag_vOUZthF6aCSuD7X49dSwHBlJbz8-lFEBpu%0D%0A4bJf8RP4mvCoYsGQbqRKPE7vruenvC8r9g05qjujKTnTr1DElnjP3DKgTvh-tn9zF8orfK4oIDII%0D%0AvRXO3SI9Kig%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Prime Video</span><br>
                <span class="amazon-affiliate-site-desc">Unlimited Streaming<br>of Movies & TV</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-de&token=BCYspm9jNH0_pNns9CDcyzv-Cr_bihsa4Nbs33ORKaNmvsl5qBIzgPeWzIJwMwh4UuqK4xLiEc2L%0D%0A7i2uSfJEu0533pJZz9jkoDMFIaQ80WP5f-mcvY1Ogul_bufFzxUHmXG0Nq4sSgR1m_NA_BewtCbd%0D%0A1FwChfit0_-EJlCRzKe9zQ-94Bit4wOug8lr7V4XTLxXdbZc-8NVu9ulbNnLICoV0y29c_ug9DkZ%0D%0AXmFuNEHuIpM%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Germany</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-it&token=BCYlpVA1mS35E4LdNBQTbux4oF0QbFgm0qIlhv3fSLrXATJo9xzSbWFCctnbilttQr_LzBmFeshS%0D%0A8uBkc-yuqGCT0DFF1bZ05eFITsoKIjbjF9pyWim4ePTECFk1XE3UC4Xb8SA9Zl0X5msZxfqgzasE%0D%0A4naNEoyyR8bOb6iL8Tw8BDFvbxP_Mkr5JHIDAoxJwPj6Gc3nJ5Z4DFE57AmVTbbQgSYW_uRFl5sO%0D%0AIL905W-5fbg%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon Italy</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-fr&token=BCYq_-5PxrtT-_ETZHmtGR-VxixXbcoWO3if5o_paeHINZndhne8BLHwq37fmHvI_DCSRFQ9DC83%0D%0AFS2LeNSXsK3Ct46X5zrx8dEH-kbvvVvO6jiLCfdqSvFmaHWdt7HlPr8hIHz06H1qTmaqCd6rT-cm%0D%0AyD7mHfSK4WSeCH_ie5L7SkV7jmyATmsOkWVVf7TcgofDE34RlTzsDzLrs4bvf247KPHYE7AJmaiz%0D%0AbmSfmBFEBNo%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon France</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movies on<br>DVD & Blu-ray</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-in&token=BCYiy42qBWz2j1Fx-VrVlzsFxCyc8s7zv80cysqPYLINwhbdlmXwhZ8rvnoLTUiFZxXHlL4gDC8r%0D%0AwZdIhD9kJpvn4ggbjZmnHc6l-rWAcydVABRme-ri2UbHu7C1S6Jf6QEs2Qskz5KrWFXQo_9J27qW%0D%0Ajd_5p2dLykEZ06tacpC-6bWoFAtxTJTUXP0N_tP73-SNitJ19cUMcTDD_lrPNgoO1PpI6TsDxH4T%0D%0ADapVqWHodYlfVdzwF2S3o9F8p6NjuOPY%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Amazon India</span><br>
                <span class="amazon-affiliate-site-desc">Buy Movie and<br>TV Show DVDs</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-dpr&token=BCYiob-zfs-Shrvrwlx2srWf50YCGQx1ki9vYoNYpFFa9a5EufCIieWAzqg605Jj6fYl4UuNjqm2%0D%0AZ8eMcB4BjuXqrHKjtk6UHlllyol6Uej0VgSzu8-uJPcdNYL3-epRYQzR9dFDGflNpevINRQsRNsk%0D%0AK8GOzHQjhLCjXZv0G_990Sw%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">DPReview</span><br>
                <span class="amazon-affiliate-site-desc">Digital<br>Photography</span>
            </a>
        </div>
    </td>

    <td>
        <div>
            <a href="/offsite/?page-action=ft-amzn-aud&token=BCYrp5mPyr3MKtUDVufCKvSYa-P10J5I6B6_mmpJJmau5_1teqcy4jRjTMQhEWaDNtzSj147eywn%0D%0Am-8DUApMRLkRi1nXZfXcfm3tVA4TaXV2OFq9NIIpVeuNGO9igCiB_x3kB5M8ZDAhjyEdYVs0kGsg%0D%0AdEvTsSJIP650ybTaYW4RnwU%0D%0A" class="amazon-affiliate-site-link">
                <span class="amazon-affiliate-site-name">Audible</span><br>
                <span class="amazon-affiliate-site-desc">Download<br>Audio Books</span>
            </a>
        </div>
    </td>
        </tr>
    </table>
      </div>
            </div>
        </div>

<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/common-2997851314._CB293837866_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/pagelayout-3327433850._CB276437338_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/starbarwidget-335260898._CB289148898_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/watchlistButton-194820129._CB286493173_.js"></script>
<script type="text/javascript" src="http://ia.media-imdb.com/images/G/01/imdb/js/collections/recommendations-2126106747._CB289323193_.js"></script>

<script type="text/javascript" id="login">
(function(){
    var readyTimeout = setInterval(function() {
        if (window.jQuery && window.imdb && window.imdb.login_lightbox) {
            clearTimeout(readyTimeout);
            window.imdb.login_lightbox("https://secure.imdb.com", "http://www.imdb.com/");
        }
    }, 100);
})();
</script>

        <script type="text/javascript">
        function jQueryOnReady(remaining_count) {
            if (window.jQuery) {
                jQuery(
                         function() {
        $(".newsdesk .newsTabs a").on("click", function(event) {
            event.preventDefault();
            var channelId = $(this).attr("id");
            // clear all other tabs with active status
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            // only show the clicked channel's content
            $(this).parent().siblings(".channel").each(function() {
                if ($(this).attr("id") === channelId) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    }

                );
                jQuery(
                           function() {
         "use strict";
         var MIN_ITEMS = 6,
             data = {
                caller_name : "persrecs-homepage",
                start : 0,
                count: 24,
                noIncludes: 1, // Dont need to include recs CSS + JS collection from Mayhem
                standards: "personal",
                ref_marker: "hm_rec_lm"};
          $('#lateload-recs-widget').load(
              "/widget/recommendations/recs",
              data,
              function( response, status, xhr ) {
                  if (status === "error") {
                      if ($('.rec_slide .rec_item').length < MIN_ITEMS) {
                          $('.rec_heading_wrapper').parents('.article').hide();
                      }
                  }
                  if ('csm' in window) {
                      csm.measure('csm_recs_delivered');
                  }
                  if ($('#lateload-recs-widget').text().length === 0) {
                      $('#lateload-recs-widget').closest('.ab_widget').parent().filter('.article').detach();
                  }
                  
                  // Filter out any unwelcome CSS links - https://tt.amazon.com/0048281389
                  $('#lateload-recs-widget').find('link').remove();
              }
          );
      }

                );
                jQuery(
                     function() { $('#content-2-wide').watchlistRibbon('.ribbonize'); }
                );
            } else if (remaining_count > 0) {
                setTimeout(function() { jQueryOnReady(remaining_count-1) }, 100);
            }
        }
        jQueryOnReady(50);
        </script>

<!-- begin ads footer -->

<!-- Begin SIS code --> 
<iframe id="sis_pixel_sitewide" width="1" height="1" frameborder="0" marginwidth="0" marginheight="0" style="display: none;"></iframe>
<script>
    setTimeout(function(){
        try{
            //sis3.0 pixel
            var url_sis3 = 'http://s.amazon-adsystem.com/iu3?',
                params_sis3 = [
                    "d=imdb.com",
                    "a1=",
                    "a2=010136762e08ae1004a3d0f7606a44b3926014d88e41fb6e0cdcc2f62f910483d4c0",
                    "pId=",
                    "r=1",
                    "rP=http%3A%2F%2Fwww.imdb.com%2F",
                    "encoding=server",
                    "cb=024101001491"  
                ];
        
            if (document.getElementById('sis_pixel_sitewide')) {
                (document.getElementById('sis_pixel_sitewide')).src = url_sis3 + params_sis3.join('&');
            }
        }
        catch(e){
            if ('consoleLog' in window){
                consoleLog('Pixel failure ' + e.toString(),'sis');
            }
            if (window.ueLogError) { 
                window.ueLogError(e);
            }
        }
    }, 20);
</script>
<!-- End SIS code -->

<!-- begin comscore beacon -->
<script type="text/javascript" src='http://ia.media-imdb.com/images/G/01/imdbads/js/beacon-458185442._CB294884543_.js'></script>
<script type="text/javascript">
if(window.COMSCORE){
COMSCORE.beacon({
c1: 2,
c2:"6034961",
c3:"",
c4:"http://www.imdb.com/",
c5:"",
c6:"",
c15:""
});
}
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6034961&c3=&c4=http%3A%2F%2Fwww.imdb.com%2F&c5=c6=&15=&cj=1"/>
</noscript>
<!-- end comscore beacon -->

<script>
    doWithAds(function(){
        (new Image()).src = "http://www.amazon.com/aan/2009-05-01/imdb/default?slot=sitewide-iframe&u=024101001491&ord=024101001491";
    },"unable to request AAN pixel");
</script>

<script>
(function(){
    var readyTimeout = setInterval(function(){
        doWithAds(pageLoaded, "No monitoring or document_is_ready object in generic");
    }, 50);

    // Wait until jQuery is loaded before firing final events.
    var pageLoaded = function() {
        if (window.jQuery) {
            clearTimeout(readyTimeout);
            jQuery(function(){ generic.document_is_ready(); });
            generic.monitoring.stop_timing('page_load','',true);
            generic.monitoring.all_events_started();
        }
    }
})();
</script>
<!-- end ads footer -->

<div id="servertime" time="577"/>



<script>
    if (typeof uet == 'function') {
      uet("be");
    }
</script>

    </body>
</html>
