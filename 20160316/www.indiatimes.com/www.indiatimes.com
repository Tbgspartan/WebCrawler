<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trending  stories on Indian Lifestyle, Culture, Relationships, Food, Travel, Entertainment, News & New Technology News - Indiatimes.com</title>

<meta name="keywords" content="" />
<meta name="description" content="Indiatimes.com brings you the news, articles, stories and videos on entertainment, latest lifestyle, culture & new technologies emerging worldwide." />



		

<link rel="image_src" href="http://media.indiatimes.in/resources/images/fbimage.png" />
<meta property="og:title" content="" />    
<meta property="og:url" content="http://www.indiatimes.com" />
<meta property="og:image" content="http://media.indiatimes.in/resources/images/fbimage.png" />

<meta property="og:site_name" content="indiatimes.com" />
<meta property="fb:app_id" content="117787264903013" />
<meta property="og:description" content="" />
<meta name="google-site-verification" content="ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw" />
<meta name="msvalidate.01" content="BAB71AAED7CCC01DB106A58E99625EB4" />
<link href="https://plus.google.com/+indiatimes/posts" rel="publisher" />

<meta property="og:type" content="website" />
    
<!--<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />-->

<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">    <link rel="shortcut icon" href="http://media.indiatimes.in/resources/images/favicon.ico?v=25" type="image/x-icon"><link rel="stylesheet" href="http://www.indiatimes.com/fonts/fonts.min.css?v=115.6" media="screen"><link rel="stylesheet" href="http://media.indiatimes.in/resources/css/site.css?v=115.6" media="screen"><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery-1.11.1.min.js?v=115.6"></script><script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/handlebars-v1.3.0.js?v=115.6"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.inview.min.js?v=115.6"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.preload.min.js?v=115.6"></script><script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script><!-- Facebook Pixel Code --><script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');fbq('init', '853339751421540');fbq('track', 'PageView');fbq("track","ViewContent");</script><noscript><img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=853339751421540&ev=PageView&noscript=1"/></noscript><!-- End Facebook Pixel Code -->           <!--  Analytics Code Begin -->    <!-- Begin comScore Tag -->    <script>        var _comscore = _comscore || [];        _comscore.push({ c1: "2", c2: "6036484" });        (function() {            var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";            el.parentNode.insertBefore(s, el);        })();    </script>    <noscript>    <img src="http://b.scorecardresearch.com/p?c1=2&c2=6036484&cv=2.0&cj=1" />    </noscript>    <!-- End comScore Tag -->    <!-- Begin Google Analytics Tag -->    <script>        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');        ga('create', 'UA-198011-6', 'indiatimes.com');            ga('require', 'displayfeatures');        ga('send', 'pageview');    </script>    <!-- End Google Analytics Tag -->    <!--  Analytics Code Begin --><!--adcode common script--><script type='text/javascript' src="http://ad.crwdcntrl.net/5/c=2800/pe=y/var=_ccaud"></script><script type="text/javascript">    var googletag = googletag || {};    googletag.cmd = googletag.cmd || [];    (function() {        var gads = document.createElement("script");        gads.async = true;        gads.type = "text/javascript";        var useSSL = "https:" == document.location.protocol;        gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";        var node =document.getElementsByTagName("script")[0];        node.parentNode.insertBefore(gads, node);    })();</script> <script type="text/javascript">        var nid='';        var cid='';        var sid='';        var brand_name='';                                                             </script>    <script>
    var contType = "";
    </script>
        <script type="text/javascript" src="http://media.indiatimes.in/resources/js/banner/home.js?v=115.6"></script>
            
<!--[if lt IE 9]>
  <script type="text/javascript" src="http://media.indiatimes.in/js/html5.js?v1.1"></script>
  <![endif]-->
<script>
var isDetailPage = 0;
var isNewYear = 1;
</script>
</head>
<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];   
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=117787264903013&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script> 	
	
<div id="wrap" class="cf"><!--wrap start-->
    <div class='darkDisabledBg' id='disabledBg'></div>
<header>
    <div class="header-inner">
        <div class="comm-header ">
            <nav>
                <div class='forTop'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>

                    <div class="lnks">
                                <a href="http://www.indiatimes.com/news/"  target="_blank" onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'News');">News</a> 
                                                               <a href="http://www.indiatimes.com/lifestyle/"  target="_blank" onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'Lifestyle');">Lifestyle</a> 
                                                               <a href="http://www.indiatimes.com/entertainment/"  target="_blank" onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'Entertainment');">Entertainment</a> 
                                                               <a href="http://www.indiatimes.com/health/"  target="_blank" onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'Health');">Health</a> 
                                                               <a href="http://www.indiatimes.com/videocafe/"  target="_blank" onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'Videos');">Videos</a> 
                                                                                          <a href='http://www.indiatimes.com/trending'  target="_blank" class="#fffff" onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'Trending');">Trending</a> 
                                                </div>
                </div>
                    <div class='onScrolled'> 
                        <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>
                        <a href="javascript:;" class="menu rc sprite" id="headerMenu">Menu</a>
                        <div class="has-tag">
                                                    </div>
                    </div>

            
            </nav>
            
            <div id="sticker" style="display:none;"><span class="readTitle">
                                    </span></div>
                <div class="socls fr share">
                </div>


            <div class="social fr">
                <a title="facebook" target="_blank"  class="sprite fb" href="https://www.facebook.com/indiatimes">facebook</a>
                <a title="twitter" target="_blank"  class="sprite twt" href="https://twitter.com/indiatimes">twitter</a>
                <a title="search"  class="sprite serach" onclick="$('body').showSearch();" href="javascript:void(0);">search</a>
            </div>
        </div>
    </div>
</header>
<div class="clr"></div>
<div id="overlay" class="animated bounceOut"><!--overlay start-->
    <div class="search_conts"><!--search-cont start-->
        <form onsubmit="return false;" class="serach-form">
            <input type="text" onkeyup="search_data();" class="input" onfocus="if (this.value == 'Search') {
                        this.value = ''
                    }" onblur="if (this.value == '') {
                                this.value = 'Search'
                            }
                            ;" value="Search" name="q" id="q" autocomplete="off">
            <input type="button" class="sprite src-btn" onclick="search_data();">
            <a class="sprite sclose" id="close" href="javascript:void(0);">X</a>
        </form>
        <div class="scroll-panes" id="scroll-panes">
            <div class="clr"></div>
            <div class="res-msg" id="res-msg"></div>
            <div class="search-list cf" id="results"></div>
            <div class="clr"></div>
            <div class="loadmore" id="progressBar" style="display: none;">Loading......</div>
            <a class="loadmore" style="display:none;" onclick="load_search_data();" id="load_more_button" href="javascript:void(0);">Load more</a>
        </div>
    </div>
</div>
<div class="blk">
    <div  id="leftMenu" class="leftMenu accordion inner">
        <dl >
            <dt data-color="red-bg" class=""><a href="http://www.indiatimes.com">Home</a></dt>
            <dd> </dd>
                            <dt  data-color="blue-bg" ><a href="http://www.indiatimes.com/news/">News</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                        <dd >
                     
                            <a href='http://www.indiatimes.com/news/india/'>India</a>
                     
                            <a href='http://www.indiatimes.com/news/world/'>World</a>
                     
                            <a href='http://www.indiatimes.com/news/sports/'>Sports</a>
                     
                            <a href='http://www.indiatimes.com/news/weird/'>Weird</a>
                            </dd>
                    <div class="clr"></div>
                    <dt  data-color="purple-bg" ><a href="http://www.indiatimes.com/lifestyle/">Lifestyle</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                        <dd >
                     
                            <a href='http://www.indiatimes.com/lifestyle/self/'>Self</a>
                     
                            <a href='http://www.indiatimes.com/lifestyle/style/'>Style</a>
                     
                            <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                     
                            <a href='http://www.indiatimes.com/culture/who-we-are/'>Who we are</a>
                     
                            <a href='http://www.indiatimes.com/culture/travel/'>Travel</a>
                     
                            <a href='http://www.indiatimes.com/culture/food/'>Food</a>
                            </dd>
                    <div class="clr"></div>
                    <dt  data-color="red-bg" ><a href="http://www.indiatimes.com/entertainment/">Entertainment</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                        <dd >
                     
                            <a href='http://www.indiatimes.com/entertainment/bollywood/'>Bollywood</a>
                     
                            <a href='http://www.indiatimes.com/entertainment/celebs/'>Celebscoop</a>
                     
                            <a href='http://www.indiatimes.com/entertainment/hollywood/'>Hollywood</a>
                            </dd>
                    <div class="clr"></div>
                    <dt  data-color="green-bg" ><a href="http://www.indiatimes.com/health/">Health</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                        <dd >
                     
                            <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                     
                            <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                     
                            <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                     
                            <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                     
                            <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                            </dd>
                    <div class="clr"></div>
                    <dt  data-color="orange-bg" ><a href="http://www.indiatimes.com/videocafe/">Videos</a>
                                </dt>
                                <dt data-color="" class=""><a href="http://www.indiatimes.com/trending/">Trending</a>
            </dt>
            <dt class="pink-bg">
                <div class="follow">Follow indiatimes </div>
                <div class="follow_cont"> 
                    <a href="https://www.facebook.com/indiatimes" class="fbleft sprite"> facebook</a> 
                    <a href="https://twitter.com/indiatimes" class="twtleft sprite"> twitter</a> 
                </div>
            </dt>

            <dd>&nbsp;</dd>
            <div class="clr"></div>
        </dl>
    </div>
</div>


<div class="clr"></div>
<div class="dummy-cont">&nbsp;</div>
<div class="clr"></div>


<div id="pushdown">
    <div class="pull-ad"><!--pull-ad start-->   
                            <div id="OOP_Inter" style="display: none;">
                        <script type='text/javascript'>
                            googletag.cmd.push(function () {
                                googletag.display('OOP_Inter'); });
                        </script>
                    </div>
                    <div id="PPD" style="display: none;">
                        <script type='text/javascript'>
                            googletag.cmd.push(function () {
                                googletag.display('PPD');
                            });
                        </script>
                    </div>
            
    </div><!--pull-ad end-->
</div>
    <!--testing 16-03-17 00:00:04-->

<section id="hp_block_1" class="container cf" style="border: 0px solid red;"><!--container start-->
    
<div class="big-image">
        	<div class="gradient-b"></div>
		<a href="http://www.indiatimes.com/news/india/all-the-things-that-india-can-achieve-if-only-vijay-mallya-were-to-return-the-money-252099.html" class="gradient-patten"></a>
            <div class="featureds"><span class="border-left">&nbsp;</span> Featured <span class="border-right">&nbsp;</span></div>
            <div class="bid-card-txt">
                                <a href="http://www.indiatimes.com/news/india/all-the-things-that-india-can-achieve-if-only-vijay-mallya-were-to-return-the-money-252099.html" class="big-card-small">
                    All The Things That India Can Achieve, If Only Vijay Mallya Were To Return The Money                </a>
              <span class="card-line"></span>  
            </div>
            <a href="http://www.indiatimes.com/news/india/all-the-things-that-india-can-achieve-if-only-vijay-mallya-were-to-return-the-money-252099.html" class="tint"><img src="http://media.indiatimes.in/media/content/2016/Mar/big-image_1458124536_1024x477.jpg"/></a>
</div>
    
        

    <div class="news-panel cf"><!--news-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/news/">news</a></h2>

        <div class="feature-list cf"><!--feature-list start-->	
                            <figure>

                        <a href="http://www.indiatimes.com/culture/who-we-are/did-you-know-india-had-2-currencies-in-operation-up-until-1959_-252060.html" class=" tint" title="Did You Know India Had 2 Currencies In Operation Up Until 1959?">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458048289_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/card_1458048289_236x111.jpg"  border="0" alt="Did You Know India Had 2 Currencies In Operation Up Until 1959?"/></a>
                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/did-you-know-india-had-2-currencies-in-operation-up-until-1959_-252060.html" title="Did You Know India Had 2 Currencies In Operation Up Until 1959?">
    Did You Know India Had 2 Currencies In Operation Up Until 1959?                    </a>
                </figcaption>
                </div><!--feature-list end-->

                    <div class="news-panel-list cf" id="column1_0"><!--news-panel-list start-->
                <figure>
                        
                        <a href="http://www.indiatimes.com/news/world/18-images-from-the-forgotten-past-of-soviet-union-that-are-hauntingly-beautiful-252107.html" title="18 Images From The Forgotten Past Of Soviet Union That Are Hauntingly Beautiful" class=" tint">                       
                        <img src="http://media.indiatimes.in/media/content/2016/Mar/600_1458128435_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/600_1458128435_236x111.jpg" border="0" alt=""/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/18-images-from-the-forgotten-past-of-soviet-union-that-are-hauntingly-beautiful-252107.html" title="18 Images From The Forgotten Past Of Soviet Union That Are Hauntingly Beautiful">
    18 Images From The Forgotten Past Of Soviet Union That Are Hauntingly Beautiful                    </a>
                </figcaption> 
            </div><!--news-panel-list end-->
                    <div class="news-panel-list cf" id="column1_1"><!--news-panel-list start-->
                <figure>
                        
                        <a href="http://www.indiatimes.com/news/india/this-report-reveals-the-happiest-countries-in-the-world-and-pakistan-beat-india-252124.html" title="This Report Reveals The Happiest Countries In The World, And Pakistan Beat India!" class=" tint">                       
                        <img src="http://media.indiatimes.in/media/content/2016/Mar/novaflickr8_1458133019_1458133023_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/novaflickr8_1458133019_1458133023_236x111.jpg" border="0" alt=""/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/this-report-reveals-the-happiest-countries-in-the-world-and-pakistan-beat-india-252124.html" title="This Report Reveals The Happiest Countries In The World, And Pakistan Beat India!">
    This Report Reveals The Happiest Countries In The World, And Pakistan Beat India!                    </a>
                </figcaption> 
            </div><!--news-panel-list end-->
                    <div class="news-panel-list cf" id="column1_2"><!--news-panel-list start-->
                <figure>
                        
                        <a href="http://www.indiatimes.com/news/india/hyderabad-based-tv-anchor-commits-suicide-while-talking-to-her-friend-on-a-skype-call-252116.html" title="Hyderabad Based TV Anchor Commits Suicide While Talking To Her Friend On A Skype Call!" class=" tint">                       
                        <img src="http://media.indiatimes.in/media/content/2016/Mar/nirosha_640x480_51458114729_1458131353_1458131359_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/nirosha_640x480_51458114729_1458131353_1458131359_236x111.jpg" border="0" alt=""/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/hyderabad-based-tv-anchor-commits-suicide-while-talking-to-her-friend-on-a-skype-call-252116.html" title="Hyderabad Based TV Anchor Commits Suicide While Talking To Her Friend On A Skype Call!">
    Hyderabad Based TV Anchor Commits Suicide While Talking To Her Friend On A Skype Call!                    </a>
                </figcaption> 
            </div><!--news-panel-list end-->
            </div><!--news-panel end-->

    <div class="life-panel cf" id="middleBlock1"><!--life-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/lifestyle/">lifestyle</a></h2>

                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-indian-version-of-adele-s-hello-is-all-you-need-after-a-tiring-day-at-work-252127.html'>video</a>                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-indian-version-of-adele-s-hello-is-all-you-need-after-a-tiring-day-at-work-252127.html" class="tint" title="This Indian Version Of Adele's Hello Is All You Need After A Tiring Day At Work">
                        <img  src="http://media.indiatimes.in/media/videocafe/2016/Mar/hello-fin_1458137928_1458137935_502x234.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2016/Mar/hello-fin_1458137928_1458137935_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/videocafe/this-indian-version-of-adele-s-hello-is-all-you-need-after-a-tiring-day-at-work-252127.html" title="This Indian Version Of Adele's Hello Is All You Need After A Tiring Day At Work" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories', 'http://www.indiatimes.com/videocafe/this-indian-version-of-adele-s-hello-is-all-you-need-after-a-tiring-day-at-work-252127.html');">

    This Indian Version Of Adele's Hello Is All You Need After A Tiring Day At Work                        </a>
                    </div>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/eating-with-hands-has-many-health-benefits-including-weight-loss-252095.html" class="tint" title="Eating With Hands Has Many Health Benefits, Including Weight Loss!">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/2hinduismworld org-card_1458120023_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/2hinduismworld org-card_1458120023_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/health/healthyliving/eating-with-hands-has-many-health-benefits-including-weight-loss-252095.html" title="Eating With Hands Has Many Health Benefits, Including Weight Loss!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories', 'http://www.indiatimes.com/health/healthyliving/eating-with-hands-has-many-health-benefits-including-weight-loss-252095.html');">

    Eating With Hands Has Many Health Benefits, Including Weight Loss!                        </a>
                    </div>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/action-scenes-of-rajinikanth-and-akshay-kumar-starrer-2-0-will-be-shot-at-delhi-s-jln-stadium-252120.html" class="tint" title="Action Scenes Of Rajinikanth And Akshay Kumar Starrer '2.0' Will Be Shot At Delhi's JLN Stadium">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/robot-card_1458132359_1458132365_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/robot-card_1458132359_1458132365_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/action-scenes-of-rajinikanth-and-akshay-kumar-starrer-2-0-will-be-shot-at-delhi-s-jln-stadium-252120.html" title="Action Scenes Of Rajinikanth And Akshay Kumar Starrer '2.0' Will Be Shot At Delhi's JLN Stadium" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories', 'http://www.indiatimes.com/entertainment/bollywood/action-scenes-of-rajinikanth-and-akshay-kumar-starrer-2-0-will-be-shot-at-delhi-s-jln-stadium-252120.html');">

    Action Scenes Of Rajinikanth And Akshay Kumar Starrer '2.0' Will Be Shot At Delhi's JLN Stadium                        </a>
                    </div>
                </figcaption>
            </div>
                    
    </div><!--life-panel end-->

    <div class="trending-panel cf"><!--trending-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/trending/">trending</a></h2>
        
                    
                        <div class="trending-panel-list cf" id="column3_0">
    <span class="strip skyblue-bg"></span>                <figure>
                        <a href="http://www.indiatimes.com/lifestyle/self/8-frugal-billionaires-who-prove-you-re-never-too-rich-to-value-money-251993.html" class="tint" title="8 Frugal Billionaires Who Prove You're Never Too Rich To Value Money">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/azim_1457962800_1457962814_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/azim_1457962800_1457962814_236x111.jpg" border="0" alt="8 Frugal Billionaires Who Prove You're Never Too Rich To Value Money"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/self/8-frugal-billionaires-who-prove-you-re-never-too-rich-to-value-money-251993.html" title="8 Frugal Billionaires Who Prove You're Never Too Rich To Value Money " onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','');">
    8 Frugal Billionaires Who Prove You're Never Too Rich To Value Money                    </a>
                   </div>     
                </figcaption>
            </div>
                <div class="trending-panel-list cf" id="column3_1">
                    <figure>
                        <a href="http://www.indiatimes.com/culture/travel/17-pictures-of-mountain-goats-that-prove-gravity-doesn-t-exist-for-them-251982.html" class="tint" title="17 Pictures Of Mountain Goats That Prove Gravity Doesn't Exist For Them">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage2_1457955260_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage2_1457955260_236x111.jpg" border="0" alt="17 Pictures Of Mountain Goats That Prove Gravity Doesn't Exist For Them"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/travel/17-pictures-of-mountain-goats-that-prove-gravity-doesn-t-exist-for-them-251982.html" title="17 Pictures Of Mountain Goats That Prove Gravity Doesn't Exist For Them " onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','');">
    17 Pictures Of Mountain Goats That Prove Gravity Doesn't Exist For Them                    </a>
                   </div>     
                </figcaption>
            </div>
                <div class="trending-panel-list cf" id="column3_2">
                    <figure>
                        <a href="http://www.indiatimes.com/entertainment/celebs/kangana-ranaut-hrithik-roshan-s-legal-battle-turns-into-a-circus-as-allegations-get-nastier-252071.html" class="tint" title="Kangana Ranaut & Hrithik Roshan's Legal Battle Turns Into A Circus As Allegations Get Nastier!">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/kangana-and-hrithik_1458106303_1458106311_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/kangana-and-hrithik_1458106303_1458106311_236x111.jpg" border="0" alt="Kangana Ranaut & Hrithik Roshan's Legal Battle Turns Into A Circus As Allegations Get Nastier!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/kangana-ranaut-hrithik-roshan-s-legal-battle-turns-into-a-circus-as-allegations-get-nastier-252071.html" title="Kangana Ranaut & Hrithik Roshan's Legal Battle Turns Into A Circus As Allegations Get Nastier! " onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','');">
    Kangana Ranaut & Hrithik Roshan's Legal Battle Turns Into A Circus As Allegations Get Nastier!                    </a>
                   </div>     
                </figcaption>
            </div>
                <div class="trending-panel-list cf" id="column3_3">
                    <figure>
                        <a href="http://www.indiatimes.com/culture/travel/this-village-in-the-netherlands-has-no-traffic-problems-because-there-are-no-roads-251876.html" class="tint" title="This Village In The Netherlands Has No Traffic Problems Because There Are No Roads!">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1457701799_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Mar/card_1457701799_236x111.jpg" border="0" alt="This Village In The Netherlands Has No Traffic Problems Because There Are No Roads!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/travel/this-village-in-the-netherlands-has-no-traffic-problems-because-there-are-no-roads-251876.html" title="This Village In The Netherlands Has No Traffic Problems Because There Are No Roads! " onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','');">
    This Village In The Netherlands Has No Traffic Problems Because There Are No Roads!                    </a>
                   </div>     
                </figcaption>
            </div>
        </div><!--trending-panel end-->
</section><!--container end-->

<!-- -------------------    end Block 1  ------------------------------------- -->
<section class="big-ads" id="ad1"> 
    <div id="bigAd1_slot"></div>
    <script>
        showBigAD1('bigAd1_slot');
    </script> 
</section>
<!-- -------------------    start Block 2  ------------------------------------- -->

<section id="hp_block_2" class="container cf" style="border: 0px solid red;"><!--container start-->
    <div class="news-panel cf"><!--news-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/news/">news</a></h2>
                    <div class="news-panel-list cf " ><!--news-panel-list start-->
                <figure>

                                            <a href="http://www.indiatimes.com/news/sports/india-pakistan-profilesforpeace-is-back-for-t20-cricket-and-it-s-got-mark-zuckerberg-s-attention-252110.html" title="India Pakistan #ProfilesForPeace Is Back For T20 Cricket And It's Got Mark Zuckerberg's Attention!" class=" tint">


                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/cp_1458129552_1458129563_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cp_1458129552_1458129563_236x111.jpg" border="0" alt=""/>
                    </a>
                </figure>
                <figcaption>

                    <a class="bold-txt" href="http://www.indiatimes.com/news/sports/india-pakistan-profilesforpeace-is-back-for-t20-cricket-and-it-s-got-mark-zuckerberg-s-attention-252110.html" title="India Pakistan #ProfilesForPeace Is Back For T20 Cricket And It's Got Mark Zuckerberg's Attention!">
    India Pakistan #ProfilesForPeace Is Back For T20 Cricket And It's Got Mark Zuckerberg's Attention!                    </a>
                </figcaption> 
            </div>
                        <div class="news-panel-list cf colombia" id="div-clmb-ctn-129061-4"  data-slot="129061" data-position="4" data-section="0" data-cb="adwidgetNew"><!--news-panel-list start-->
                <figure>

                                            <a href="http://www.indiatimes.com/news/india/40-years-ago-a-man-stole-some-lemons-the-police-has-just-arrested-him-252119.html" title="40 Years Ago, A Man Stole Some Lemons. The Police Has Just Arrested Him!" class=" tint">


                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/5451762105_7fb9e7c11f_b-600_1458131814_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/5451762105_7fb9e7c11f_b-600_1458131814_236x111.jpg" border="0" alt=""/>
                    </a>
                </figure>
                <figcaption>

                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/40-years-ago-a-man-stole-some-lemons-the-police-has-just-arrested-him-252119.html" title="40 Years Ago, A Man Stole Some Lemons. The Police Has Just Arrested Him!">
    40 Years Ago, A Man Stole Some Lemons. The Police Has Just Arrested Him!                    </a>
                </figcaption> 
            </div>
                        <div class="news-panel-list cf " ><!--news-panel-list start-->
                <figure>

                                            <a href="http://www.indiatimes.com/news/world/instead-of-one-wedding-this-couple-had-wedding-ceremonies-around-the-world-252114.html" title="Instead Of One Wedding, This Couple Had Wedding Ceremonies Around The World!" class=" tint">


                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/couple64_1458131271_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/couple64_1458131271_236x111.jpg" border="0" alt=""/>
                    </a>
                </figure>
                <figcaption>

                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/instead-of-one-wedding-this-couple-had-wedding-ceremonies-around-the-world-252114.html" title="Instead Of One Wedding, This Couple Had Wedding Ceremonies Around The World!">
    Instead Of One Wedding, This Couple Had Wedding Ceremonies Around The World!                    </a>
                </figcaption> 
            </div>
                        <div class="news-panel-list cf " ><!--news-panel-list start-->
                <figure>

                                            <a href="http://www.indiatimes.com/news/world/trump-becomes-the-latest-name-on-anonymous-s-hit-list-here-are-some-others-who-faced-it-in-the-past-252121.html" title="Trump Becomes The Latest Name On Anonymous's Hit-List. Here Are Some Others Who Faced It In The Past" class=" tint">


                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458132386_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1458132386_236x111.jpg" border="0" alt=""/>
                    </a>
                </figure>
                <figcaption>

                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/trump-becomes-the-latest-name-on-anonymous-s-hit-list-here-are-some-others-who-faced-it-in-the-past-252121.html" title="Trump Becomes The Latest Name On Anonymous's Hit-List. Here Are Some Others Who Faced It In The Past">
    Trump Becomes The Latest Name On Anonymous's Hit-List. Here Are Some Others Who Faced It In The Past                    </a>
                </figcaption> 
            </div>
                </div><!--news-panel end-->

    <div class="life-panel cf"><!--life-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/lifestyle/">lifestyle</a></h2>
                        
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/anupam-kher-says-bharat-mata-ki-jai-should-be-the-only-definition-of-nationalism-252122.html" class="tint" title="Anupam Kher Says 'Bharat Mata Ki Jai' Should Be The Only Definition Of Nationalism">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/anupam-kher-in-conversation-during-the-press-conference-of-kucch-bhi-ho-sakta-hai_1458132435_1458132439_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/anupam-kher-in-conversation-during-the-press-conference-of-kucch-bhi-ho-sakta-hai_1458132435_1458132439_502x234.jpg" border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/anupam-kher-says-bharat-mata-ki-jai-should-be-the-only-definition-of-nationalism-252122.html" title="Anupam Kher Says 'Bharat Mata Ki Jai' Should Be The Only Definition Of Nationalism">
    Anupam Kher Says 'Bharat Mata Ki Jai' Should Be The Only Definition Of Nationalism                        </a>
                    </div>
                </figcaption>
            </div>
                    
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/travel/21-things-you-need-to-remember-when-you-re-driving-on-an-indian-license-around-the-world-252045.html" class="tint" title="21 Things You Need To Remember When You're Driving On An Indian License Around The World">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/cp_1458108300_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cp_1458108300_502x234.jpg" border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/culture/travel/21-things-you-need-to-remember-when-you-re-driving-on-an-indian-license-around-the-world-252045.html" title="21 Things You Need To Remember When You're Driving On An Indian License Around The World">
    21 Things You Need To Remember When You're Driving On An Indian License Around The World                        </a>
                    </div>
                </figcaption>
            </div>
                    
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/nana-patekar-helps-widows-of-farmers-who-committed-suicide-proves-he-s-a-real-life-superhero-252103.html" class="tint" title="Nana Patekar Helps Widows Of Farmers Who Committed Suicide, Proves He's A Real Life Superhero!">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/10583797_1779099635651482_7845419075436565407_n_1458125471_1458125476_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/10583797_1779099635651482_7845419075436565407_n_1458125471_1458125476_502x234.jpg" border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/nana-patekar-helps-widows-of-farmers-who-committed-suicide-proves-he-s-a-real-life-superhero-252103.html" title="Nana Patekar Helps Widows Of Farmers Who Committed Suicide, Proves He's A Real Life Superhero!">
    Nana Patekar Helps Widows Of Farmers Who Committed Suicide, Proves He's A Real Life Superhero!                        </a>
                    </div>
                </figcaption>
            </div>
                </div><!--life-panel end-->

    <div class="trending-panel cf"><!--trending-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/trending/">trending</a></h2>
        
            <div class="trending-panel-list cf "  ><!--trending-panel-list start-->
                                        <figure>

    <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/this-short-film-is-the-story-of-every-love-struck-couple-with-just-one-massive-difference-252029.html'>video</a>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/this-short-film-is-the-story-of-every-love-struck-couple-with-just-one-massive-difference-252029.html" class="tint" title="This Short Film Is The Story Of Every Love-Struck Couple, With Just One Massive Difference!">

                        <img  src="http://media.indiatimes.in/media/videocafe/2016/Mar/card2_1458034268_1458034274_236x111.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Mar/card2_1458034268_1458034274_236x111.jpg" border="0" alt="This Short Film Is The Story Of Every Love-Struck Couple, With Just One Massive Difference!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/this-short-film-is-the-story-of-every-love-struck-couple-with-just-one-massive-difference-252029.html" title="This Short Film Is The Story Of Every Love-Struck Couple, With Just One Massive Difference!">
            This Short Film Is The Story Of Every Love-Struck Couple, With Just One Massive Difference!                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
    
            <div class="trending-panel-list cf colombia"  id="div-clmb-ctn-129061-2"  data-slot="129061" data-position="2" data-section="0" data-cb="adwidgetNew"><!--trending-panel-list start-->
                                        <figure>

    
                    <a href="http://www.indiatimes.com/entertainment/celebs/meet-isha-pant-the-brave-cop-from-mp-who-inspired-priyanka-chopra-s-role-in-jai-gangaajal-252030.html" class="tint" title="Meet Isha Pant, The Brave Cop From MP Who Inspired Priyanka Chopra's Role In Jai Gangaajal!">

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/card1_1458035292_1458035299_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card1_1458035292_1458035299_236x111.jpg" border="0" alt="Meet Isha Pant, The Brave Cop From MP Who Inspired Priyanka Chopra's Role In Jai Gangaajal!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/meet-isha-pant-the-brave-cop-from-mp-who-inspired-priyanka-chopra-s-role-in-jai-gangaajal-252030.html" title="Meet Isha Pant, The Brave Cop From MP Who Inspired Priyanka Chopra's Role In Jai Gangaajal!">
            Meet Isha Pant, The Brave Cop From MP Who Inspired Priyanka Chopra's Role In Jai Gangaajal!                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
    
            <div class="trending-panel-list cf "  ><!--trending-panel-list start-->
                                        <figure>

    
                    <a href="http://www.indiatimes.com/culture/food/someone-decided-to-pour-molten-copper-on-a-maharaja-mac-and-it-does-not-look-pretty-252028.html" class="tint" title="Someone Decided To Pour Molten Copper On A Maharaja Mac And It Does Not Look Pretty">

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/cp_1458033655_1458033659_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cp_1458033655_1458033659_236x111.jpg" border="0" alt="Someone Decided To Pour Molten Copper On A Maharaja Mac And It Does Not Look Pretty"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/food/someone-decided-to-pour-molten-copper-on-a-maharaja-mac-and-it-does-not-look-pretty-252028.html" title="Someone Decided To Pour Molten Copper On A Maharaja Mac And It Does Not Look Pretty">
            Someone Decided To Pour Molten Copper On A Maharaja Mac And It Does Not Look Pretty                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
    
            <div class="trending-panel-list cf "  ><!--trending-panel-list start-->
                                        <figure>

    <a class='video-btn sprite' href='http://www.indiatimes.com/health/tips-tricks/here-s-how-to-easily-de-seed-a-pomegranate-in-minutes-251721.html'>video</a>
                    <a href="http://www.indiatimes.com/health/tips-tricks/here-s-how-to-easily-de-seed-a-pomegranate-in-minutes-251721.html" class="tint" title="Here's How To Easily De-Seed A Pomegranate In Minutes!">

                        <img  src="http://media.indiatimes.in/media/videocafe/2016/Mar/cover_1457441286_236x111.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Mar/cover_1457441286_236x111.jpg" border="0" alt="Here's How To Easily De-Seed A Pomegranate In Minutes!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/tips-tricks/here-s-how-to-easily-de-seed-a-pomegranate-in-minutes-251721.html" title="Here's How To Easily De-Seed A Pomegranate In Minutes!">
            Here's How To Easily De-Seed A Pomegranate In Minutes!                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
        </div>

</section><!--container end-->

<!--------------------------- end Block2 ------------------------------------------>
<section class="big-ads" id="ad2"> 
    <div id="bigAd2_slot"></div>
</section>
<!--------------------------- start Block3 ------------------------------------------>

<section id="hp_block_3" class="container cf"><!--container start-->

    <div class="news-panel cf"><!--news-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/news/">news</a></h2>

                    <div class="news-panel-list cf " >

                <figure>

                    <a href="http://www.indiatimes.com/news/sports/lewis-hamilton-took-a-selfie-while-riding-a-bike-in-new-zealand-now-the-police-are-after-him-252098.html" title="Formula 1 Star Lewis Hamilton Took A Selfie While Riding A Bike, Now The Police Are After Him!" class=" tint">
            

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/hamilton640_1458122685_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/hamilton640_1458122685_236x111.jpg" border="0" alt="Formula 1 Star Lewis Hamilton Took A Selfie While Riding A Bike, Now The Police Are After Him!"/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/sports/lewis-hamilton-took-a-selfie-while-riding-a-bike-in-new-zealand-now-the-police-are-after-him-252098.html" title="Formula 1 Star Lewis Hamilton Took A Selfie While Riding A Bike, Now The Police Are After Him!">
            Formula 1 Star Lewis Hamilton Took A Selfie While Riding A Bike, Now The Police Are After Him!                    </a>
                </figcaption> 
            </div>
                <div class="news-panel-list cf colombia" id="div-clmb-ctn-129061-8"  data-slot="129061" data-position="8" data-section="0" data-cb="adwidgetNew">

                <figure>

                    <a href="http://www.indiatimes.com/news/world/scientists-just-created-the-world-s-first-invisibility-cloak-252112.html" title="Scientists Just Created The World's First 'Invisibility Cloak'" class=" tint">
            

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/invisible_original-640_1458129877_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/invisible_original-640_1458129877_236x111.jpg" border="0" alt="Scientists Just Created The World's First 'Invisibility Cloak'"/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/scientists-just-created-the-world-s-first-invisibility-cloak-252112.html" title="Scientists Just Created The World's First 'Invisibility Cloak'">
            Scientists Just Created The World's First 'Invisibility Cloak'                    </a>
                </figcaption> 
            </div>
                <div class="news-panel-list cf " >

                <figure>

                    <a href="http://www.indiatimes.com/news/sports/national-level-kabaddi-player-shot-dead-in-rohtak-murder-caught-on-camera-252109.html" title="National Level Kabaddi Player Shot Dead in Rohtak, No One Comes To His Rescue As He Lies Bleeding!" class=" tint">
            

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/murder640_1458129204_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/murder640_1458129204_236x111.jpg" border="0" alt="National Level Kabaddi Player Shot Dead in Rohtak, No One Comes To His Rescue As He Lies Bleeding!"/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/sports/national-level-kabaddi-player-shot-dead-in-rohtak-murder-caught-on-camera-252109.html" title="National Level Kabaddi Player Shot Dead in Rohtak, No One Comes To His Rescue As He Lies Bleeding!">
            National Level Kabaddi Player Shot Dead in Rohtak, No One Comes To His Rescue As He Lies Bleeding!                    </a>
                </figcaption> 
            </div>
                <div class="news-panel-list cf " >

                <figure>

                    <a href="http://www.indiatimes.com/news/india/the-return-of-radhemaa-police-to-file-an-fir-against-her-for-carrying-a-trishul-on-flight-252104.html" title="The Return Of #RadheMaa! Police To File An FIR Against Her For Carrying A 'Trishul' On Flight" class=" tint">
            

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/dsds64_1458126812_1458126815_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/dsds64_1458126812_1458126815_236x111.jpg" border="0" alt="The Return Of #RadheMaa! Police To File An FIR Against Her For Carrying A 'Trishul' On Flight"/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/the-return-of-radhemaa-police-to-file-an-fir-against-her-for-carrying-a-trishul-on-flight-252104.html" title="The Return Of #RadheMaa! Police To File An FIR Against Her For Carrying A 'Trishul' On Flight">
            The Return Of #RadheMaa! Police To File An FIR Against Her For Carrying A 'Trishul' On Flight                    </a>
                </figcaption> 
            </div>
        </div><!--news-panel end-->

    <div class="life-panel cf"><!--life-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/lifestyle/">lifestyle</a></h2>
         
            <div class="life-panel-list cf">
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/a-mother-slams-hrithik-roshan-for-his-insensitive-comments-about-kangana-ranaut-s-mental-health-252106.html" class="tint" title="A Mother Slams Hrithik Roshan For His Insensitive Comments About Kangana Ranaut's Mental Health">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458128538_1458128542_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1458128538_1458128542_502x234.jpg" border="0" alt="http://www.indiatimes.com/entertainment/bollywood/a-mother-slams-hrithik-roshan-for-his-insensitive-comments-about-kangana-ranaut-s-mental-health-252106.html" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/a-mother-slams-hrithik-roshan-for-his-insensitive-comments-about-kangana-ranaut-s-mental-health-252106.html" title="A Mother Slams Hrithik Roshan For His Insensitive Comments About Kangana Ranaut's Mental Health">
            A Mother Slams Hrithik Roshan For His Insensitive Comments About Kangana Ranaut's Mental Health                        </a>
                    </div>
                </figcaption>
            </div>
                     
            <div class="life-panel-list cf">
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/food/taking-pictures-of-food-and-uploading-them-on-instagram-might-actually-make-it-taste-better-252100.html" class="tint" title="Taking Pictures Of Food And Uploading Them On Instagram Might Actually Make It Taste Better!">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/foodphotog_1458124128_1458124132_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/foodphotog_1458124128_1458124132_502x234.jpg" border="0" alt="http://www.indiatimes.com/culture/food/taking-pictures-of-food-and-uploading-them-on-instagram-might-actually-make-it-taste-better-252100.html" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/culture/food/taking-pictures-of-food-and-uploading-them-on-instagram-might-actually-make-it-taste-better-252100.html" title="Taking Pictures Of Food And Uploading Them On Instagram Might Actually Make It Taste Better!">
            Taking Pictures Of Food And Uploading Them On Instagram Might Actually Make It Taste Better!                        </a>
                    </div>
                </figcaption>
            </div>
                     
            <div class="life-panel-list cf">
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/this-former-fashion-model-talks-about-being-raped-by-a-photographer-and-it-will-leave-you-furious-252085.html" class="tint" title="This Former Fashion Model Talks About Being Raped By A Photographer And It Will Leave You Furious">
                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/nikki_1458113555_1458113563_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/nikki_1458113555_1458113563_502x234.jpg" border="0" alt="http://www.indiatimes.com/culture/who-we-are/this-former-fashion-model-talks-about-being-raped-by-a-photographer-and-it-will-leave-you-furious-252085.html" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/this-former-fashion-model-talks-about-being-raped-by-a-photographer-and-it-will-leave-you-furious-252085.html" title="This Former Fashion Model Talks About Being Raped By A Photographer And It Will Leave You Furious">
            This Former Fashion Model Talks About Being Raped By A Photographer And It Will Leave You Furious                        </a>
                    </div>
                </figcaption>
            </div>
                </div><!--life-panel end-->

    <div class="trending-panel cf"><!--trending-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/trending/">trending</a></h2>
                    <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                            <figure>

            		

                    <a href="http://www.indiatimes.com/entertainment/hollywood/whoa-tv-host-ellen-degeneres-s-wife-portia-might-have-left-her-for-a-man-252056.html" class="tint" title="Whoa! TV Host Ellen DeGeneres's Wife Portia Might Have Left Her For A Man!">

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/ellen-degeneres-portia-de-rossi-getty1_1458047201_1458047204_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/ellen-degeneres-portia-de-rossi-getty1_1458047201_1458047204_236x111.jpg" border="0" alt="Whoa! TV Host Ellen DeGeneres's Wife Portia Might Have Left Her For A Man!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/hollywood/whoa-tv-host-ellen-degeneres-s-wife-portia-might-have-left-her-for-a-man-252056.html" title="Whoa! TV Host Ellen DeGeneres's Wife Portia Might Have Left Her For A Man!">
    Whoa! TV Host Ellen DeGeneres's Wife Portia Might Have Left Her For A Man!                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
                <div class="trending-panel-list cf colombia" id="div-clmb-ctn-129061-6"  data-slot="129061" data-position="6" data-section="0" data-cb="adwidgetNew"><!--trending-panel-list start-->
                            <figure>

            		

                    <a href="http://www.indiatimes.com/entertainment/celebs/shah-rukh-khan-shared-a-new-still-from-his-upcoming-film-fan-it-ll-kill-you-with-excitement-252019.html" class="tint" title="Shah Rukh Khan Shared A New Still From His Upcoming Film 'Fan' & It'll Kill You With Excitement!">

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/fan-still_1458027424_1458027429_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/fan-still_1458027424_1458027429_236x111.jpg" border="0" alt="Shah Rukh Khan Shared A New Still From His Upcoming Film 'Fan' & It'll Kill You With Excitement!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/shah-rukh-khan-shared-a-new-still-from-his-upcoming-film-fan-it-ll-kill-you-with-excitement-252019.html" title="Shah Rukh Khan Shared A New Still From His Upcoming Film 'Fan' & It'll Kill You With Excitement!">
    Shah Rukh Khan Shared A New Still From His Upcoming Film 'Fan' & It'll Kill You With Excitement!                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
                <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                            <figure>

            		

                    <a href="http://www.indiatimes.com/entertainment/celebs/honey-singh-settles-rumours-finally-opens-up-about-his-struggles-with-alcoholism-bipolarity-252000.html" class="tint" title="Honey Singh Settles Rumours, Finally Opens Up About His Struggles With Alcoholism & Bipolarity!">

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/honey-singh-3_1458019602_1458019606_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/honey-singh-3_1458019602_1458019606_236x111.jpg" border="0" alt="Honey Singh Settles Rumours, Finally Opens Up About His Struggles With Alcoholism & Bipolarity!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/honey-singh-settles-rumours-finally-opens-up-about-his-struggles-with-alcoholism-bipolarity-252000.html" title="Honey Singh Settles Rumours, Finally Opens Up About His Struggles With Alcoholism & Bipolarity!">
    Honey Singh Settles Rumours, Finally Opens Up About His Struggles With Alcoholism & Bipolarity!                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
                <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                            <figure>

            		

                    <a href="http://www.indiatimes.com/entertainment/celebs/hrithik-roshan-kangana-ranaut-war-turns-uglier-as-the-two-slap-each-other-with-legal-notices-252005.html" class="tint" title="Hrithik Roshan-Kangana Ranaut War Turns Uglier As The Two Slap Each Other With Legal Notices!">

                        <img  src="http://media.indiatimes.in/media/content/2016/Mar/659243_1458021985_1458021991_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/659243_1458021985_1458021991_236x111.jpg" border="0" alt="Hrithik Roshan-Kangana Ranaut War Turns Uglier As The Two Slap Each Other With Legal Notices!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/hrithik-roshan-kangana-ranaut-war-turns-uglier-as-the-two-slap-each-other-with-legal-notices-252005.html" title="Hrithik Roshan-Kangana Ranaut War Turns Uglier As The Two Slap Each Other With Legal Notices!">
    Hrithik Roshan-Kangana Ranaut War Turns Uglier As The Two Slap Each Other With Legal Notices!                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
        </div><!--trending-panel end-->

</section><!--container end-->

<!------------------------------- end Block 3 ---------------------------------------->
<section class="big-ads">
    <div id="bigAd3_slot"></div>
</section>

<!------------------------------- start Block4  -------------------------------------->

<section class="container cf" id="container4"><!--container start-->
    <div class="news-panel cf ">
        <h2><a target="_blank" href="http://www.indiatimes.com/news/">news</a></h2>

                    <div class="news-panel-list cf " >
                <figure>                      
                    <a href="http://www.indiatimes.com/news/india/meet-mansukhbhai-the-high-school-dropout-who-made-a-clay-fridge-that-runs-without-electricity-252096.html" title="Meet Mansukhbhai, The High School Dropout Who Made A Clay Fridge That Runs Without Electricity" class=" tint">
                                                <img  src="http://media.indiatimes.in/media/content/2016/Mar/600_1458121217_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/600_1458121217_236x111.jpg" border="0" alt="Meet Mansukhbhai, The High School Dropout Who Made A Clay Fridge That Runs Without Electricity"/>
                    </a>
                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/meet-mansukhbhai-the-high-school-dropout-who-made-a-clay-fridge-that-runs-without-electricity-252096.html" title="Meet Mansukhbhai, The High School Dropout Who Made A Clay Fridge That Runs Without Electricity">
        Meet Mansukhbhai, The High School Dropout Who Made A Clay Fridge That Runs Without Electricity                    </a>
                </figcaption> 
            </div>
                <div class='container1'>            <div class="news-panel-list cf colombia" id="div-clmb-ctn-129061-12"  data-slot="129061" data-position="12" data-section="0" data-cb="adwidgetNew">
                <figure>                      
                    <a href="http://www.indiatimes.com/news/india/meet-arun-suvarna-the-couple-that-s-made-it-their-mission-to-look-after-dogs-on-the-streets-252094.html" title="Meet Arun & Suvarna - The Couple That's Made It Their Mission To Look After Dogs On The Streets" class=" tint">
                                                <img  src="http://media.indiatimes.in/media/content/2016/Mar/cp_1458122865_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cp_1458122865_236x111.jpg" border="0" alt="Meet Arun & Suvarna - The Couple That's Made It Their Mission To Look After Dogs On The Streets"/>
                    </a>
                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/meet-arun-suvarna-the-couple-that-s-made-it-their-mission-to-look-after-dogs-on-the-streets-252094.html" title="Meet Arun & Suvarna - The Couple That's Made It Their Mission To Look After Dogs On The Streets">
        Meet Arun & Suvarna - The Couple That's Made It Their Mission To Look After Dogs On The Streets                    </a>
                </figcaption> 
            </div>
                            <div class="news-panel-list cf " >
                <figure>                      
                    <a href="http://www.indiatimes.com/news/sports/shahid-afridi-still-loves-india-slams-49-off-19-balls-in-kolkata-to-demolish-bangladesh-252105.html" title="Shahid Afridi Still Loves India, Slams 49 Off 19 Balls In Kolkata To Demolish Bangladesh" class=" tint">
                                                <img  src="http://media.indiatimes.in/media/content/2016/Mar/afridiban_1458128441_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/afridiban_1458128441_236x111.jpg" border="0" alt="Shahid Afridi Still Loves India, Slams 49 Off 19 Balls In Kolkata To Demolish Bangladesh"/>
                    </a>
                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/sports/shahid-afridi-still-loves-india-slams-49-off-19-balls-in-kolkata-to-demolish-bangladesh-252105.html" title="Shahid Afridi Still Loves India, Slams 49 Off 19 Balls In Kolkata To Demolish Bangladesh">
        Shahid Afridi Still Loves India, Slams 49 Off 19 Balls In Kolkata To Demolish Bangladesh                    </a>
                </figcaption> 
            </div>
                            <div class="news-panel-list cf " >
                <figure>                      
                    <a href="http://www.indiatimes.com/news/world/in-a-first-pakistan-declares-national-holidays-for-holi-diwali-and-easter-252097.html" title="In A First, Pakistan Declares National Holidays For Holi, Diwali And Easter" class=" tint">
                                                <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458122589_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1458122589_236x111.jpg" border="0" alt="In A First, Pakistan Declares National Holidays For Holi, Diwali And Easter"/>
                    </a>
                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/in-a-first-pakistan-declares-national-holidays-for-holi-diwali-and-easter-252097.html" title="In A First, Pakistan Declares National Holidays For Holi, Diwali And Easter">
        In A First, Pakistan Declares National Holidays For Holi, Diwali And Easter                    </a>
                </figcaption> 
            </div>
                 
    </div>
</div><!--news-panel end-->

<div class="life-panel cf container2" id="b4c2"><!--life-panel start-->
    <h2><a target="_blank" href="http://www.indiatimes.com/lifestyle/">lifestyle</a></h2>
        
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/after-terrorist-jim-sarbh-in-neerja-meet-sudheer-babu-the-evil-villain-of-baaghi-252087.html" class="tint" title="After 'Terrorist' Jim Sarbh In Neerja, Meet Sudheer Babu, The Evil Villain Of Baaghi">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage_1458113622_1458113629_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage_1458113622_1458113629_502x234.jpg" border="0" alt="After 'Terrorist' Jim Sarbh In Neerja, Meet Sudheer Babu, The Evil Villain Of Baaghi" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/after-terrorist-jim-sarbh-in-neerja-meet-sudheer-babu-the-evil-villain-of-baaghi-252087.html" title="After 'Terrorist' Jim Sarbh In Neerja, Meet Sudheer Babu, The Evil Villain Of Baaghi">
    After 'Terrorist' Jim Sarbh In Neerja, Meet Sudheer Babu, The Evil Villain Of Baaghi                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/buzz/another-reason-to-hang-out-with-your-buddies-research-shows-bromances-are-good-for-health-252057.html" class="tint" title="Another Reason To Hang Out With Your Buddies, Research Shows Bromances Are Good For Health">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/cover_1458047505_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cover_1458047505_502x234.jpg" border="0" alt="Another Reason To Hang Out With Your Buddies, Research Shows Bromances Are Good For Health" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/buzz/another-reason-to-hang-out-with-your-buddies-research-shows-bromances-are-good-for-health-252057.html" title="Another Reason To Hang Out With Your Buddies, Research Shows Bromances Are Good For Health">
    Another Reason To Hang Out With Your Buddies, Research Shows Bromances Are Good For Health                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/after-quitting-bhabhiji-ghar-pe-hain-shilpa-shinde-gets-a-legal-notice-from-the-makers-252093.html" class="tint" title="After Quitting 'Bhabhiji Ghar Pe Hain', Shilpa Shinde Gets A Legal Notice From The Makers!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/shilpa_1458118120_1458118128_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/shilpa_1458118120_1458118128_502x234.jpg" border="0" alt="After Quitting 'Bhabhiji Ghar Pe Hain', Shilpa Shinde Gets A Legal Notice From The Makers!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/after-quitting-bhabhiji-ghar-pe-hain-shilpa-shinde-gets-a-legal-notice-from-the-makers-252093.html" title="After Quitting 'Bhabhiji Ghar Pe Hain', Shilpa Shinde Gets A Legal Notice From The Makers!">
    After Quitting 'Bhabhiji Ghar Pe Hain', Shilpa Shinde Gets A Legal Notice From The Makers!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/travel/17-pictures-of-mountain-goats-that-prove-gravity-doesn-t-exist-for-them-251982.html" class="tint" title="17 Pictures Of Mountain Goats That Prove Gravity Doesn't Exist For Them">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage2_1457955260_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage2_1457955260_502x234.jpg" border="0" alt="17 Pictures Of Mountain Goats That Prove Gravity Doesn't Exist For Them" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/travel/17-pictures-of-mountain-goats-that-prove-gravity-doesn-t-exist-for-them-251982.html" title="17 Pictures Of Mountain Goats That Prove Gravity Doesn't Exist For Them">
    17 Pictures Of Mountain Goats That Prove Gravity Doesn't Exist For Them                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/6-things-alia-said-about-sidharth-that-prove-they-are-the-cutest-rumoured-couple-in-bollywood-252078.html" class="tint" title="6 Things Alia Said About Sidharth That Prove They Are The Cutest 'Rumoured' Couple In Bollywood">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/alia-card_1458109228_1458109232_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/alia-card_1458109228_1458109232_502x234.jpg" border="0" alt="6 Things Alia Said About Sidharth That Prove They Are The Cutest 'Rumoured' Couple In Bollywood" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/6-things-alia-said-about-sidharth-that-prove-they-are-the-cutest-rumoured-couple-in-bollywood-252078.html" title="6 Things Alia Said About Sidharth That Prove They Are The Cutest 'Rumoured' Couple In Bollywood">
    6 Things Alia Said About Sidharth That Prove They Are The Cutest 'Rumoured' Couple In Bollywood                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/healthyliving/10-healthy-combos-that-will-fix-your-aversion-to-fruits-252053.html" class="tint" title="10 Healthy Combos That Will Fix Your Aversion To Fruits">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458044971_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1458044971_502x234.jpg" border="0" alt="10 Healthy Combos That Will Fix Your Aversion To Fruits" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/healthyliving/10-healthy-combos-that-will-fix-your-aversion-to-fruits-252053.html" title="10 Healthy Combos That Will Fix Your Aversion To Fruits">
    10 Healthy Combos That Will Fix Your Aversion To Fruits                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/hollywood/warrior-princess-xena-is-returning-to-tv-p-s-she-will-come-out-of-the-closet-in-this-one-252075.html" class="tint" title="Warrior Princess Xena Is Returning To TV! P.S. She Will Come Out Of The Closet In This One!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/072015-xena-warrior-princess_1458108113_1458108121_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/072015-xena-warrior-princess_1458108113_1458108121_502x234.jpg" border="0" alt="Warrior Princess Xena Is Returning To TV! P.S. She Will Come Out Of The Closet In This One!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/hollywood/warrior-princess-xena-is-returning-to-tv-p-s-she-will-come-out-of-the-closet-in-this-one-252075.html" title="Warrior Princess Xena Is Returning To TV! P.S. She Will Come Out Of The Closet In This One!">
    Warrior Princess Xena Is Returning To TV! P.S. She Will Come Out Of The Closet In This One!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/technology/forget-cash-card-or-wallets-amazon-will-soon-let-you-make-payments-via-selfies-252079.html" class="tint" title="Forget Cash, Card Or Wallets, Amazon Will Soon Let You Make Payments Via Selfies!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/simbioscflickr8_1458109903_1458109907_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/simbioscflickr8_1458109903_1458109907_502x234.jpg" border="0" alt="Forget Cash, Card Or Wallets, Amazon Will Soon Let You Make Payments Via Selfies!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/technology/forget-cash-card-or-wallets-amazon-will-soon-let-you-make-payments-via-selfies-252079.html" title="Forget Cash, Card Or Wallets, Amazon Will Soon Let You Make Payments Via Selfies!">
    Forget Cash, Card Or Wallets, Amazon Will Soon Let You Make Payments Via Selfies!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/self/8-frugal-billionaires-who-prove-you-re-never-too-rich-to-value-money-251993.html" class="tint" title="8 Frugal Billionaires Who Prove You're Never Too Rich To Value Money">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/azim_1457962800_1457962814_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/azim_1457962800_1457962814_502x234.jpg" border="0" alt="8 Frugal Billionaires Who Prove You're Never Too Rich To Value Money" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/self/8-frugal-billionaires-who-prove-you-re-never-too-rich-to-value-money-251993.html" title="8 Frugal Billionaires Who Prove You're Never Too Rich To Value Money">
    8 Frugal Billionaires Who Prove You're Never Too Rich To Value Money                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/kangana-ranaut-hrithik-roshan-s-legal-battle-turns-into-a-circus-as-allegations-get-nastier-252071.html" class="tint" title="Kangana Ranaut & Hrithik Roshan's Legal Battle Turns Into A Circus As Allegations Get Nastier!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/kangana-and-hrithik_1458106303_1458106311_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/kangana-and-hrithik_1458106303_1458106311_502x234.jpg" border="0" alt="Kangana Ranaut & Hrithik Roshan's Legal Battle Turns Into A Circus As Allegations Get Nastier!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/kangana-ranaut-hrithik-roshan-s-legal-battle-turns-into-a-circus-as-allegations-get-nastier-252071.html" title="Kangana Ranaut & Hrithik Roshan's Legal Battle Turns Into A Circus As Allegations Get Nastier!">
    Kangana Ranaut & Hrithik Roshan's Legal Battle Turns Into A Circus As Allegations Get Nastier!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
        <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/the-teaser-of-ms-dhoni-s-biopic-is-out-and-it-shows-a-glimpse-of-the-cricketer-s-early-life-252073.html'>video</a>                <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/the-teaser-of-ms-dhoni-s-biopic-is-out-and-it-shows-a-glimpse-of-the-cricketer-s-early-life-252073.html" class="tint" title="The Teaser Of MS Dhoni's Biopic Is Out And It Shows A Glimpse Of The Cricketer's Early Life">
                    <img  src="http://media.indiatimes.in/media/videocafe/2016/Mar/msdhoni_card_1458107258_502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Mar/msdhoni_card_1458107258_502x234.jpg" border="0" alt="The Teaser Of MS Dhoni's Biopic Is Out And It Shows A Glimpse Of The Cricketer's Early Life" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/the-teaser-of-ms-dhoni-s-biopic-is-out-and-it-shows-a-glimpse-of-the-cricketer-s-early-life-252073.html" title="The Teaser Of MS Dhoni's Biopic Is Out And It Shows A Glimpse Of The Cricketer's Early Life">
    The Teaser Of MS Dhoni's Biopic Is Out And It Shows A Glimpse Of The Cricketer's Early Life                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
        <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/get-rid-of-that-belly-fat-with-this-walk-based-workout-no-crunches-required-252063.html'>video</a>                <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/healthyliving/get-rid-of-that-belly-fat-with-this-walk-based-workout-no-crunches-required-252063.html" class="tint" title="Get Rid Of That Belly Fat With This Walk-Based Workout â No Crunches Required!">
                    <img  src="http://media.indiatimes.in/media/videocafe/2016/Mar/cover1_1458113430_502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Mar/cover1_1458113430_502x234.jpg" border="0" alt="Get Rid Of That Belly Fat With This Walk-Based Workout â No Crunches Required!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/healthyliving/get-rid-of-that-belly-fat-with-this-walk-based-workout-no-crunches-required-252063.html" title="Get Rid Of That Belly Fat With This Walk-Based Workout â No Crunches Required!">
    Get Rid Of That Belly Fat With This Walk-Based Workout â No Crunches Required!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/buzz/not-sleeping-enough-is-like-smoking-marijuana-because-it-makes-you-eat-300-extra-calories-251981.html" class="tint" title="Not Sleeping Enough Is Like Smoking Marijuana, Because It Makes You Eat 300 Extra Calories!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/cover_1457954984_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cover_1457954984_502x234.jpg" border="0" alt="Not Sleeping Enough Is Like Smoking Marijuana, Because It Makes You Eat 300 Extra Calories!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/buzz/not-sleeping-enough-is-like-smoking-marijuana-because-it-makes-you-eat-300-extra-calories-251981.html" title="Not Sleeping Enough Is Like Smoking Marijuana, Because It Makes You Eat 300 Extra Calories!">
    Not Sleeping Enough Is Like Smoking Marijuana, Because It Makes You Eat 300 Extra Calories!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/hollywood/whoa-tv-host-ellen-degeneres-s-wife-portia-might-have-left-her-for-a-man-252056.html" class="tint" title="Whoa! TV Host Ellen DeGeneres's Wife Portia Might Have Left Her For A Man!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/ellen-degeneres-portia-de-rossi-getty1_1458047201_1458047204_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/ellen-degeneres-portia-de-rossi-getty1_1458047201_1458047204_502x234.jpg" border="0" alt="Whoa! TV Host Ellen DeGeneres's Wife Portia Might Have Left Her For A Man!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/hollywood/whoa-tv-host-ellen-degeneres-s-wife-portia-might-have-left-her-for-a-man-252056.html" title="Whoa! TV Host Ellen DeGeneres's Wife Portia Might Have Left Her For A Man!">
    Whoa! TV Host Ellen DeGeneres's Wife Portia Might Have Left Her For A Man!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/sonam-kapoor-sunny-leone-slam-bjp-mla-for-beating-up-a-horse-during-the-protests-252055.html" class="tint" title="Sonam Kapoor & Sunny Leone Slam BJP MLA For Beating Up A Horse During The Protests">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage_1458045643_1458045647_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage_1458045643_1458045647_502x234.jpg" border="0" alt="Sonam Kapoor & Sunny Leone Slam BJP MLA For Beating Up A Horse During The Protests" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/sonam-kapoor-sunny-leone-slam-bjp-mla-for-beating-up-a-horse-during-the-protests-252055.html" title="Sonam Kapoor & Sunny Leone Slam BJP MLA For Beating Up A Horse During The Protests">
    Sonam Kapoor & Sunny Leone Slam BJP MLA For Beating Up A Horse During The Protests                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/alia-bhatt-calls-krk-creep-speaks-up-on-his-nasty-tweet-and-sidharth-s-defence-of-her-252051.html" class="tint" title="Alia Bhatt Calls KRK A Creep, Speaks Up On His Nasty Tweet And Sidharth's Defence Of Her">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/krk-alia-card_1458044751_1458044755_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/krk-alia-card_1458044751_1458044755_502x234.jpg" border="0" alt="Alia Bhatt Calls KRK A Creep, Speaks Up On His Nasty Tweet And Sidharth's Defence Of Her" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/alia-bhatt-calls-krk-creep-speaks-up-on-his-nasty-tweet-and-sidharth-s-defence-of-her-252051.html" title="Alia Bhatt Calls KRK A Creep, Speaks Up On His Nasty Tweet And Sidharth's Defence Of Her">
    Alia Bhatt Calls KRK A Creep, Speaks Up On His Nasty Tweet And Sidharth's Defence Of Her                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/hollywood/robert-downey-jr-disappoints-fans-says-there-probably-won-t-be-another-iron-man-film-252047.html" class="tint" title="Robert Downey Jr. Disappoints Fans, Says There Probably Won't Be Another Iron Man Film!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/negumopoopuikj_1_3_1458041952_1458041961_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/negumopoopuikj_1_3_1458041952_1458041961_502x234.jpg" border="0" alt="Robert Downey Jr. Disappoints Fans, Says There Probably Won't Be Another Iron Man Film!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/hollywood/robert-downey-jr-disappoints-fans-says-there-probably-won-t-be-another-iron-man-film-252047.html" title="Robert Downey Jr. Disappoints Fans, Says There Probably Won't Be Another Iron Man Film!">
    Robert Downey Jr. Disappoints Fans, Says There Probably Won't Be Another Iron Man Film!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/technology/this-bionic-eye-lens-can-give-you-superhuman-vision-with-an-8-minute-surgery-252023.html" class="tint" title="This Bionic Eye Lens Can Give You Superhuman Vision With An 8-Minute Surgery!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458030037_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1458030037_502x234.jpg" border="0" alt="This Bionic Eye Lens Can Give You Superhuman Vision With An 8-Minute Surgery!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/technology/this-bionic-eye-lens-can-give-you-superhuman-vision-with-an-8-minute-surgery-252023.html" title="This Bionic Eye Lens Can Give You Superhuman Vision With An 8-Minute Surgery!">
    This Bionic Eye Lens Can Give You Superhuman Vision With An 8-Minute Surgery!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/pakistani-actor-fawad-khan-supports-shahid-afridi-says-india-has-always-been-very-welcoming-252048.html" class="tint" title="Pakistani Actor Fawad Khan Supports Shahid Afridi, Says India Has Always Been Very Welcoming">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/card1_1458042406_1458042412_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card1_1458042406_1458042412_502x234.jpg" border="0" alt="Pakistani Actor Fawad Khan Supports Shahid Afridi, Says India Has Always Been Very Welcoming" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/pakistani-actor-fawad-khan-supports-shahid-afridi-says-india-has-always-been-very-welcoming-252048.html" title="Pakistani Actor Fawad Khan Supports Shahid Afridi, Says India Has Always Been Very Welcoming">
    Pakistani Actor Fawad Khan Supports Shahid Afridi, Says India Has Always Been Very Welcoming                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/travel/baby-olive-ridley-turtles-safely-make-it-to-the-sea-courtesy-volunteers-who-chose-humanity-over-selfies-252018.html" class="tint" title="Baby Olive Ridley Turtles Safely Make It To The Sea, Courtesy Volunteers Who Chose Humanity Over Selfies">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458026464_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1458026464_502x234.jpg" border="0" alt="Baby Olive Ridley Turtles Safely Make It To The Sea, Courtesy Volunteers Who Chose Humanity Over Selfies" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/travel/baby-olive-ridley-turtles-safely-make-it-to-the-sea-courtesy-volunteers-who-chose-humanity-over-selfies-252018.html" title="Baby Olive Ridley Turtles Safely Make It To The Sea, Courtesy Volunteers Who Chose Humanity Over Selfies">
    Baby Olive Ridley Turtles Safely Make It To The Sea, Courtesy Volunteers Who Chose Humanity Over Selfies                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/alia-bhatt-celebrates-her-birthday-with-kapoor-sons-says-its-success-will-be-the-biggest-gift-252041.html" class="tint" title="Alia Bhatt Has A Working Birthday, Says The Success Of 'Kapoor & Sons' Will Be Her Biggest Gift">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/a-card_1458048569_1458048572_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/a-card_1458048569_1458048572_502x234.jpg" border="0" alt="Alia Bhatt Has A Working Birthday, Says The Success Of 'Kapoor & Sons' Will Be Her Biggest Gift" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/alia-bhatt-celebrates-her-birthday-with-kapoor-sons-says-its-success-will-be-the-biggest-gift-252041.html" title="Alia Bhatt Has A Working Birthday, Says The Success Of 'Kapoor & Sons' Will Be Her Biggest Gift">
    Alia Bhatt Has A Working Birthday, Says The Success Of 'Kapoor & Sons' Will Be Her Biggest Gift                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/meet-isha-pant-the-brave-cop-from-mp-who-inspired-priyanka-chopra-s-role-in-jai-gangaajal-252030.html" class="tint" title="Meet Isha Pant, The Brave Cop From MP Who Inspired Priyanka Chopra's Role In Jai Gangaajal!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/card1_1458035292_1458035299_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card1_1458035292_1458035299_502x234.jpg" border="0" alt="Meet Isha Pant, The Brave Cop From MP Who Inspired Priyanka Chopra's Role In Jai Gangaajal!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/meet-isha-pant-the-brave-cop-from-mp-who-inspired-priyanka-chopra-s-role-in-jai-gangaajal-252030.html" title="Meet Isha Pant, The Brave Cop From MP Who Inspired Priyanka Chopra's Role In Jai Gangaajal!">
    Meet Isha Pant, The Brave Cop From MP Who Inspired Priyanka Chopra's Role In Jai Gangaajal!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/sanjay-dutt-biopic-will-hit-the-silver-screen-on-christmas-2017-6-things-you-need-to-know-about-it-252021.html" class="tint" title="Sanjay Dutt Biopic Will Hit The Silver Screen On Christmas 2017 + 6 Things You Need To Know About It!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage_1458029700_1458029704_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-collage_1458029700_1458029704_502x234.jpg" border="0" alt="Sanjay Dutt Biopic Will Hit The Silver Screen On Christmas 2017 + 6 Things You Need To Know About It!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/sanjay-dutt-biopic-will-hit-the-silver-screen-on-christmas-2017-6-things-you-need-to-know-about-it-252021.html" title="Sanjay Dutt Biopic Will Hit The Silver Screen On Christmas 2017 + 6 Things You Need To Know About It!">
    Sanjay Dutt Biopic Will Hit The Silver Screen On Christmas 2017 + 6 Things You Need To Know About It!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
        <a class='video-btn sprite' href='http://www.indiatimes.com/health/tips-tricks/here-s-how-to-easily-de-seed-a-pomegranate-in-minutes-251721.html'>video</a>                <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/tips-tricks/here-s-how-to-easily-de-seed-a-pomegranate-in-minutes-251721.html" class="tint" title="Here's How To Easily De-Seed A Pomegranate In Minutes!">
                    <img  src="http://media.indiatimes.in/media/videocafe/2016/Mar/cover_1457441286_502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Mar/cover_1457441286_502x234.jpg" border="0" alt="Here's How To Easily De-Seed A Pomegranate In Minutes!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/tips-tricks/here-s-how-to-easily-de-seed-a-pomegranate-in-minutes-251721.html" title="Here's How To Easily De-Seed A Pomegranate In Minutes!">
    Here's How To Easily De-Seed A Pomegranate In Minutes!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/travel/an-architect-s-idea-for-this-hotel-in-the-alps-is-a-whole-new-level-of-creativity-252035.html" class="tint" title="An Architect's Idea For This Hotel In The Alps Is A Whole New Level Of Creativity!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/600_1458036463_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/600_1458036463_502x234.jpg" border="0" alt="An Architect's Idea For This Hotel In The Alps Is A Whole New Level Of Creativity!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/travel/an-architect-s-idea-for-this-hotel-in-the-alps-is-a-whole-new-level-of-creativity-252035.html" title="An Architect's Idea For This Hotel In The Alps Is A Whole New Level Of Creativity!">
    An Architect's Idea For This Hotel In The Alps Is A Whole New Level Of Creativity!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
        <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/this-short-film-is-the-story-of-every-love-struck-couple-with-just-one-massive-difference-252029.html'>video</a>                <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/this-short-film-is-the-story-of-every-love-struck-couple-with-just-one-massive-difference-252029.html" class="tint" title="This Short Film Is The Story Of Every Love-Struck Couple, With Just One Massive Difference!">
                    <img  src="http://media.indiatimes.in/media/videocafe/2016/Mar/card2_1458034268_1458034274_502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Mar/card2_1458034268_1458034274_502x234.jpg" border="0" alt="This Short Film Is The Story Of Every Love-Struck Couple, With Just One Massive Difference!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/this-short-film-is-the-story-of-every-love-struck-couple-with-just-one-massive-difference-252029.html" title="This Short Film Is The Story Of Every Love-Struck Couple, With Just One Massive Difference!">
    This Short Film Is The Story Of Every Love-Struck Couple, With Just One Massive Difference!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/food/someone-decided-to-pour-molten-copper-on-a-maharaja-mac-and-it-does-not-look-pretty-252028.html" class="tint" title="Someone Decided To Pour Molten Copper On A Maharaja Mac And It Does Not Look Pretty">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/cp_1458033655_1458033659_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cp_1458033655_1458033659_502x234.jpg" border="0" alt="Someone Decided To Pour Molten Copper On A Maharaja Mac And It Does Not Look Pretty" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/food/someone-decided-to-pour-molten-copper-on-a-maharaja-mac-and-it-does-not-look-pretty-252028.html" title="Someone Decided To Pour Molten Copper On A Maharaja Mac And It Does Not Look Pretty">
    Someone Decided To Pour Molten Copper On A Maharaja Mac And It Does Not Look Pretty                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/shah-rukh-khan-shared-a-new-still-from-his-upcoming-film-fan-it-ll-kill-you-with-excitement-252019.html" class="tint" title="Shah Rukh Khan Shared A New Still From His Upcoming Film 'Fan' & It'll Kill You With Excitement!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/fan-still_1458027424_1458027429_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/fan-still_1458027424_1458027429_502x234.jpg" border="0" alt="Shah Rukh Khan Shared A New Still From His Upcoming Film 'Fan' & It'll Kill You With Excitement!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/shah-rukh-khan-shared-a-new-still-from-his-upcoming-film-fan-it-ll-kill-you-with-excitement-252019.html" title="Shah Rukh Khan Shared A New Still From His Upcoming Film 'Fan' & It'll Kill You With Excitement!">
    Shah Rukh Khan Shared A New Still From His Upcoming Film 'Fan' & It'll Kill You With Excitement!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/travel/this-village-in-the-netherlands-has-no-traffic-problems-because-there-are-no-roads-251876.html" class="tint" title="This Village In The Netherlands Has No Traffic Problems Because There Are No Roads!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1457701799_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1457701799_502x234.jpg" border="0" alt="This Village In The Netherlands Has No Traffic Problems Because There Are No Roads!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/travel/this-village-in-the-netherlands-has-no-traffic-problems-because-there-are-no-roads-251876.html" title="This Village In The Netherlands Has No Traffic Problems Because There Are No Roads!">
    This Village In The Netherlands Has No Traffic Problems Because There Are No Roads!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/healthyliving/7-things-you-didn-t-know-are-making-you-to-feel-lethargic-all-the-time-252006.html" class="tint" title="7 Things You Didn't Know Are Making You To Feel Lethargic All The Time">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458022098_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1458022098_502x234.jpg" border="0" alt="7 Things You Didn't Know Are Making You To Feel Lethargic All The Time" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/healthyliving/7-things-you-didn-t-know-are-making-you-to-feel-lethargic-all-the-time-252006.html" title="7 Things You Didn't Know Are Making You To Feel Lethargic All The Time">
    7 Things You Didn't Know Are Making You To Feel Lethargic All The Time                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/who-we-are/creepy-doctor-sends-28-outrageous-texts-to-a-woman-after-she-cancels-their-blind-date-251987.html" class="tint" title="Creepy Doctor Sends 28 Outrageous Texts To A Woman After She Cancels Their Blind Date!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458025252_1458025256_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1458025252_1458025256_502x234.jpg" border="0" alt="Creepy Doctor Sends 28 Outrageous Texts To A Woman After She Cancels Their Blind Date!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/creepy-doctor-sends-28-outrageous-texts-to-a-woman-after-she-cancels-their-blind-date-251987.html" title="Creepy Doctor Sends 28 Outrageous Texts To A Woman After She Cancels Their Blind Date!">
    Creepy Doctor Sends 28 Outrageous Texts To A Woman After She Cancels Their Blind Date!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/aamir-khan-s-family-home-in-varanasi-has-turned-into-a-gambler-s-den-252014.html" class="tint" title="Aamir Khan's Family Home In Varanasi Has Turned Into A Gambler's Den!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/aamir_khan_berlin_film_festival_2011_1458024199_1458024204_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/aamir_khan_berlin_film_festival_2011_1458024199_1458024204_502x234.jpg" border="0" alt="Aamir Khan's Family Home In Varanasi Has Turned Into A Gambler's Den!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/aamir-khan-s-family-home-in-varanasi-has-turned-into-a-gambler-s-den-252014.html" title="Aamir Khan's Family Home In Varanasi Has Turned Into A Gambler's Den!">
    Aamir Khan's Family Home In Varanasi Has Turned Into A Gambler's Den!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/game-of-thrones-cast-join-hands-in-an-advertisement-to-support-refugee-crisis-campaign-252011.html" class="tint" title="'Game Of Thrones' Cast Join Hands In An Advertisement To Support Refugee Crisis Campaign">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458023864_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1458023864_502x234.jpg" border="0" alt="'Game Of Thrones' Cast Join Hands In An Advertisement To Support Refugee Crisis Campaign" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/game-of-thrones-cast-join-hands-in-an-advertisement-to-support-refugee-crisis-campaign-252011.html" title="'Game Of Thrones' Cast Join Hands In An Advertisement To Support Refugee Crisis Campaign">
    'Game Of Thrones' Cast Join Hands In An Advertisement To Support Refugee Crisis Campaign                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/hrithik-roshan-kangana-ranaut-war-turns-uglier-as-the-two-slap-each-other-with-legal-notices-252005.html" class="tint" title="Hrithik Roshan-Kangana Ranaut War Turns Uglier As The Two Slap Each Other With Legal Notices!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/659243_1458021985_1458021991_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/659243_1458021985_1458021991_502x234.jpg" border="0" alt="Hrithik Roshan-Kangana Ranaut War Turns Uglier As The Two Slap Each Other With Legal Notices!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/hrithik-roshan-kangana-ranaut-war-turns-uglier-as-the-two-slap-each-other-with-legal-notices-252005.html" title="Hrithik Roshan-Kangana Ranaut War Turns Uglier As The Two Slap Each Other With Legal Notices!">
    Hrithik Roshan-Kangana Ranaut War Turns Uglier As The Two Slap Each Other With Legal Notices!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/a-94-million-year-old-mystery-of-the-marine-dino-ichthyosaur-has-finally-been-solved-here-s-why-it-s-important-251759.html" class="tint" title="A 94-Million-Year-Old Mystery Of The Marine Dino Ichthyosaur Has Finally Been Solved & Here's Why It's Important">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1457517791_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1457517791_502x234.jpg" border="0" alt="A 94-Million-Year-Old Mystery Of The Marine Dino Ichthyosaur Has Finally Been Solved & Here's Why It's Important" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/a-94-million-year-old-mystery-of-the-marine-dino-ichthyosaur-has-finally-been-solved-here-s-why-it-s-important-251759.html" title="A 94-Million-Year-Old Mystery Of The Marine Dino Ichthyosaur Has Finally Been Solved & Here's Why It's Important">
    A 94-Million-Year-Old Mystery Of The Marine Dino Ichthyosaur Has Finally Been Solved & Here's Why It's Important                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/honey-singh-settles-rumours-finally-opens-up-about-his-struggles-with-alcoholism-bipolarity-252000.html" class="tint" title="Honey Singh Settles Rumours, Finally Opens Up About His Struggles With Alcoholism & Bipolarity!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/honey-singh-3_1458019602_1458019606_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/honey-singh-3_1458019602_1458019606_502x234.jpg" border="0" alt="Honey Singh Settles Rumours, Finally Opens Up About His Struggles With Alcoholism & Bipolarity!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/honey-singh-settles-rumours-finally-opens-up-about-his-struggles-with-alcoholism-bipolarity-252000.html" title="Honey Singh Settles Rumours, Finally Opens Up About His Struggles With Alcoholism & Bipolarity!">
    Honey Singh Settles Rumours, Finally Opens Up About His Struggles With Alcoholism & Bipolarity!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
        <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/this-8-year-old-fitness-trainer-takes-exercise-very-seriously-and-it-s-adorable-251879.html'>video</a>                <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/healthyliving/this-8-year-old-fitness-trainer-takes-exercise-very-seriously-and-it-s-adorable-251879.html" class="tint" title="This 8-Year-Old Fitness Trainer Takes Exercise Very Seriously And Itâs Adorable!">
                    <img  src="http://media.indiatimes.in/media/videocafe/2016/Mar/cover_1457703929_502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Mar/cover_1457703929_502x234.jpg" border="0" alt="This 8-Year-Old Fitness Trainer Takes Exercise Very Seriously And Itâs Adorable!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/healthyliving/this-8-year-old-fitness-trainer-takes-exercise-very-seriously-and-it-s-adorable-251879.html" title="This 8-Year-Old Fitness Trainer Takes Exercise Very Seriously And Itâs Adorable!">
    This 8-Year-Old Fitness Trainer Takes Exercise Very Seriously And Itâs Adorable!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/news/world/a-new-camera-and-casing-design-here-re-the-first-images-of-the-alleged-new-iphone-7_-251994.html" class="tint" title="A New Camera And Casing Design, Here're The First Images Of The Alleged New iPhone 7">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/64_1457964435_1457964441_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/64_1457964435_1457964441_502x234.jpg" border="0" alt="A New Camera And Casing Design, Here're The First Images Of The Alleged New iPhone 7" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/a-new-camera-and-casing-design-here-re-the-first-images-of-the-alleged-new-iphone-7_-251994.html" title="A New Camera And Casing Design, Here're The First Images Of The Alleged New iPhone 7">
    A New Camera And Casing Design, Here're The First Images Of The Alleged New iPhone 7                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/healthyliving/these-11-healthy-foods-can-be-dangerous-if-you-eat-them-at-the-wrong-time-251985.html" class="tint" title="These 11 Healthy Foods Can Be Dangerous If You Eat Them At The Wrong Time!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458047199_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1458047199_502x234.jpg" border="0" alt="These 11 Healthy Foods Can Be Dangerous If You Eat Them At The Wrong Time!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/healthyliving/these-11-healthy-foods-can-be-dangerous-if-you-eat-them-at-the-wrong-time-251985.html" title="These 11 Healthy Foods Can Be Dangerous If You Eat Them At The Wrong Time!">
    These 11 Healthy Foods Can Be Dangerous If You Eat Them At The Wrong Time!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
        <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/shraddha-kapoor-tiger-shroff-show-their-rebellious-streak-in-this-action-packed-trailer-of-baaghi-251989.html'>video</a>                <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/shraddha-kapoor-tiger-shroff-show-their-rebellious-streak-in-this-action-packed-trailer-of-baaghi-251989.html" class="tint" title="Shraddha Kapoor & Tiger Shroff Reveal Their 'Rebellious Streak' In This Action-Packed Trailer Of 'Baaghi'!">
                    <img  src="http://media.indiatimes.in/media/videocafe/2016/Mar/baaghi1_1457959789_1457959794_502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Mar/baaghi1_1457959789_1457959794_502x234.jpg" border="0" alt="Shraddha Kapoor & Tiger Shroff Reveal Their 'Rebellious Streak' In This Action-Packed Trailer Of 'Baaghi'!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/shraddha-kapoor-tiger-shroff-show-their-rebellious-streak-in-this-action-packed-trailer-of-baaghi-251989.html" title="Shraddha Kapoor & Tiger Shroff Reveal Their 'Rebellious Streak' In This Action-Packed Trailer Of 'Baaghi'!">
    Shraddha Kapoor & Tiger Shroff Reveal Their 'Rebellious Streak' In This Action-Packed Trailer Of 'Baaghi'!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/travel/11-of-the-oldest-cities-in-the-world-that-were-the-first-to-be-inhabited-251792.html" class="tint" title="11 Of The Oldest Cities In The World That Were The First To Be Inhabited">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/varansi2_1457589394_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/varansi2_1457589394_502x234.jpg" border="0" alt="11 Of The Oldest Cities In The World That Were The First To Be Inhabited" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/travel/11-of-the-oldest-cities-in-the-world-that-were-the-first-to-be-inhabited-251792.html" title="11 Of The Oldest Cities In The World That Were The First To Be Inhabited">
    11 Of The Oldest Cities In The World That Were The First To Be Inhabited                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/a-uk-couple-danced-on-london-thumakda-on-their-wedding-it-was-way-better-than-the-original-251977.html" class="tint" title="A UK Couple Danced On 'London Thumakda' On Their Wedding & It Was Way Better Than The Original!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-card_1457953321_1457953328_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-card_1457953321_1457953328_502x234.jpg" border="0" alt="A UK Couple Danced On 'London Thumakda' On Their Wedding & It Was Way Better Than The Original!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/a-uk-couple-danced-on-london-thumakda-on-their-wedding-it-was-way-better-than-the-original-251977.html" title="A UK Couple Danced On 'London Thumakda' On Their Wedding & It Was Way Better Than The Original!">
    A UK Couple Danced On 'London Thumakda' On Their Wedding & It Was Way Better Than The Original!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/21-things-you-probably-didn-t-know-about-bollywood-s-mr-perfectionist-aamir-khan-251953.html" class="tint" title="21 Things You Probably Didn't Know About Bollywood's 'Mr. Perfectionist' Aamir Khan">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/aamir-card_1457937305_1457937311_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/aamir-card_1457937305_1457937311_502x234.jpg" border="0" alt="21 Things You Probably Didn't Know About Bollywood's 'Mr. Perfectionist' Aamir Khan" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/21-things-you-probably-didn-t-know-about-bollywood-s-mr-perfectionist-aamir-khan-251953.html" title="21 Things You Probably Didn't Know About Bollywood's 'Mr. Perfectionist' Aamir Khan">
    21 Things You Probably Didn't Know About Bollywood's 'Mr. Perfectionist' Aamir Khan                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/these-pics-of-b-town-stars-partying-with-salma-hayek-matthew-mcconaughey-will-leave-you-awestruck-251973.html" class="tint" title="These Pics Of B-Town Stars Partying With Salma Hayek & Matthew McConaughey Will Leave You Awestruck!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/akki-card_1457952382_1457952387_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/akki-card_1457952382_1457952387_502x234.jpg" border="0" alt="These Pics Of B-Town Stars Partying With Salma Hayek & Matthew McConaughey Will Leave You Awestruck!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/these-pics-of-b-town-stars-partying-with-salma-hayek-matthew-mcconaughey-will-leave-you-awestruck-251973.html" title="These Pics Of B-Town Stars Partying With Salma Hayek & Matthew McConaughey Will Leave You Awestruck!">
    These Pics Of B-Town Stars Partying With Salma Hayek & Matthew McConaughey Will Leave You Awestruck!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/9-note-worthy-things-aamir-khan-said-at-his-51st-birthday-celebration-with-the-press-251965.html" class="tint" title="9 Note-Worthy Things Aamir Khan Said At His 51st Birthday Celebration With The Press!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/aamir_1457945982_1457945992_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/aamir_1457945982_1457945992_502x234.jpg" border="0" alt="9 Note-Worthy Things Aamir Khan Said At His 51st Birthday Celebration With The Press!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/9-note-worthy-things-aamir-khan-said-at-his-51st-birthday-celebration-with-the-press-251965.html" title="9 Note-Worthy Things Aamir Khan Said At His 51st Birthday Celebration With The Press!">
    9 Note-Worthy Things Aamir Khan Said At His 51st Birthday Celebration With The Press!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/buzz/chihuahua-or-muffin-puppy-or-bagel-look-carefully-at-these-pictures-they-ll-confuse-you-251974.html" class="tint" title="Chihuahua Or Muffin? Puppy Or Bagel? Look Carefully At These Pictures, Theyâll Confuse You!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/cover_1457952241_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/cover_1457952241_502x234.jpg" border="0" alt="Chihuahua Or Muffin? Puppy Or Bagel? Look Carefully At These Pictures, Theyâll Confuse You!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/buzz/chihuahua-or-muffin-puppy-or-bagel-look-carefully-at-these-pictures-they-ll-confuse-you-251974.html" title="Chihuahua Or Muffin? Puppy Or Bagel? Look Carefully At These Pictures, Theyâll Confuse You!">
    Chihuahua Or Muffin? Puppy Or Bagel? Look Carefully At These Pictures, Theyâll Confuse You!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-funny-video-shows-a-scared-pakistan-pushing-new-zealand-to-play-first-against-india-in-the-world-t20-lol-251976.html'>video</a>                <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/videocafe/this-funny-video-shows-a-scared-pakistan-pushing-new-zealand-to-play-first-against-india-in-the-world-t20-lol-251976.html" class="tint" title="This Funny Video Shows A Scared Pakistan Pushing New Zealand To Play First Against India In The World T20 #LOL">
                    <img  src="http://media.indiatimes.in/media/videocafe/2016/Mar/indiaworldt20_card_1457953069_502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Mar/indiaworldt20_card_1457953069_502x234.jpg" border="0" alt="This Funny Video Shows A Scared Pakistan Pushing New Zealand To Play First Against India In The World T20 #LOL" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/videocafe/this-funny-video-shows-a-scared-pakistan-pushing-new-zealand-to-play-first-against-india-in-the-world-t20-lol-251976.html" title="This Funny Video Shows A Scared Pakistan Pushing New Zealand To Play First Against India In The World T20 #LOL">
    This Funny Video Shows A Scared Pakistan Pushing New Zealand To Play First Against India In The World T20 #LOL                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/food/15-delicious-recipes-from-the-internet-vs-how-they-actually-turned-out-251870.html" class="tint" title="15 Delicious Recipes From The Internet Vs. How They Actually Turned Out">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/pizza_1457695225_1457695236_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/pizza_1457695225_1457695236_502x234.jpg" border="0" alt="15 Delicious Recipes From The Internet Vs. How They Actually Turned Out" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/food/15-delicious-recipes-from-the-internet-vs-how-they-actually-turned-out-251870.html" title="15 Delicious Recipes From The Internet Vs. How They Actually Turned Out">
    15 Delicious Recipes From The Internet Vs. How They Actually Turned Out                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/we-don-t-know-what-s-more-pathetic-mallya-s-9000-cr-debt-or-rgv-s-nasty-solution-for-it-251959.html" class="tint" title="We Don't Know What's More Pathetic - Mallya's 9000 Cr Debt Or RGV's Nasty Solution For It!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/card2_1457944158_1457944170_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card2_1457944158_1457944170_502x234.jpg" border="0" alt="We Don't Know What's More Pathetic - Mallya's 9000 Cr Debt Or RGV's Nasty Solution For It!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/we-don-t-know-what-s-more-pathetic-mallya-s-9000-cr-debt-or-rgv-s-nasty-solution-for-it-251959.html" title="We Don't Know What's More Pathetic - Mallya's 9000 Cr Debt Or RGV's Nasty Solution For It!">
    We Don't Know What's More Pathetic - Mallya's 9000 Cr Debt Or RGV's Nasty Solution For It!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/everything-you-need-to-know-about-sunil-grover-aka-gutthi-251968.html" class="tint" title="Everything You Need To Know About Sunil Grover Aka Gutthi">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/gutthi-card_1457946957_1457946961_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/gutthi-card_1457946957_1457946961_502x234.jpg" border="0" alt="Everything You Need To Know About Sunil Grover Aka Gutthi" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/everything-you-need-to-know-about-sunil-grover-aka-gutthi-251968.html" title="Everything You Need To Know About Sunil Grover Aka Gutthi">
    Everything You Need To Know About Sunil Grover Aka Gutthi                    </a>
                </div>    
            </figcaption>
        </div>
    
</div><!--life-panel end-->

<div class="trending-panel cf "><!--trending-panel start-->
    <h2><a target="_blank" href="http://www.indiatimes.com/trending/">trending</a></h2>
                    <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                    <figure>
      
    					
                <a href="http://www.indiatimes.com/entertainment/bollywood/a-uk-couple-danced-on-london-thumakda-on-their-wedding-it-was-way-better-than-the-original-251977.html" class="tint" title="A UK Couple Danced On 'London Thumakda' On Their Wedding & It Was Way Better Than The Original!">

                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-card_1457953321_1457953328_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/picmonkey-card_1457953321_1457953328_236x111.jpg" border="0" alt="A UK Couple Danced On 'London Thumakda' On Their Wedding & It Was Way Better Than The Original!"/>
                </a>
            </figure>
            <figcaption>
            <div class="white-container">
                <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/a-uk-couple-danced-on-london-thumakda-on-their-wedding-it-was-way-better-than-the-original-251977.html" title="A UK Couple Danced On 'London Thumakda' On Their Wedding & It Was Way Better Than The Original!">
    A UK Couple Danced On 'London Thumakda' On Their Wedding & It Was Way Better Than The Original!                </a>
            </div>
            </figcaption>
        </div><!--trending-panel-list end-->
    <div class='container3'>        <div class="trending-panel-list cf colombia" id="div-clmb-ctn-129061-10"  data-slot="129061" data-position="10" data-section="0" data-cb="adwidgetNew"><!--trending-panel-list start-->
                    <figure>
      
    					
                <a href="http://www.indiatimes.com/entertainment/bollywood/alia-bhatt-calls-krk-creep-speaks-up-on-his-nasty-tweet-and-sidharth-s-defence-of-her-252051.html" class="tint" title="Alia Bhatt Calls KRK A Creep, Speaks Up On His Nasty Tweet And Sidharth's Defence Of Her">

                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/krk-alia-card_1458044751_1458044755_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/krk-alia-card_1458044751_1458044755_236x111.jpg" border="0" alt="Alia Bhatt Calls KRK A Creep, Speaks Up On His Nasty Tweet And Sidharth's Defence Of Her"/>
                </a>
            </figure>
            <figcaption>
            <div class="white-container">
                <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/alia-bhatt-calls-krk-creep-speaks-up-on-his-nasty-tweet-and-sidharth-s-defence-of-her-252051.html" title="Alia Bhatt Calls KRK A Creep, Speaks Up On His Nasty Tweet And Sidharth's Defence Of Her">
    Alia Bhatt Calls KRK A Creep, Speaks Up On His Nasty Tweet And Sidharth's Defence Of Her                </a>
            </div>
            </figcaption>
        </div><!--trending-panel-list end-->
            <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                    <figure>
      
    					
                <a href="http://www.indiatimes.com/health/healthyliving/these-11-healthy-foods-can-be-dangerous-if-you-eat-them-at-the-wrong-time-251985.html" class="tint" title="These 11 Healthy Foods Can Be Dangerous If You Eat Them At The Wrong Time!">

                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/card_1458047199_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card_1458047199_236x111.jpg" border="0" alt="These 11 Healthy Foods Can Be Dangerous If You Eat Them At The Wrong Time!"/>
                </a>
            </figure>
            <figcaption>
            <div class="white-container">
                <a class="bold-txt" href="http://www.indiatimes.com/health/healthyliving/these-11-healthy-foods-can-be-dangerous-if-you-eat-them-at-the-wrong-time-251985.html" title="These 11 Healthy Foods Can Be Dangerous If You Eat Them At The Wrong Time!">
    These 11 Healthy Foods Can Be Dangerous If You Eat Them At The Wrong Time!                </a>
            </div>
            </figcaption>
        </div><!--trending-panel-list end-->
            <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                    <figure>
      
    					
                <a href="http://www.indiatimes.com/culture/travel/11-of-the-oldest-cities-in-the-world-that-were-the-first-to-be-inhabited-251792.html" class="tint" title="11 Of The Oldest Cities In The World That Were The First To Be Inhabited">

                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/varansi2_1457589394_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/varansi2_1457589394_236x111.jpg" border="0" alt="11 Of The Oldest Cities In The World That Were The First To Be Inhabited"/>
                </a>
            </figure>
            <figcaption>
            <div class="white-container">
                <a class="bold-txt" href="http://www.indiatimes.com/culture/travel/11-of-the-oldest-cities-in-the-world-that-were-the-first-to-be-inhabited-251792.html" title="11 Of The Oldest Cities In The World That Were The First To Be Inhabited">
    11 Of The Oldest Cities In The World That Were The First To Be Inhabited                </a>
            </div>
            </figcaption>
        </div><!--trending-panel-list end-->
    
</div>

</div><!--trending-panel end-->

</section>
<section class="container cf" id="loader" style="display: none;border: 0px solid red;">
    <div class="news-panel cf ">&nbsp;</div>
    <div class="life-panel cf" style="text-align: center;"><img src="http://media.indiatimes.in/resources/images/loading.gif" border="0" /></div>
    <div class="trending-panel cf ">&nbsp;</div>
</section>
<div class="remove-fixed-home">&nbsp;</div>
<section class="big-ads" id="adfooter">
    <div id="badRos_slot"></div> 
</section>   

<!------------------------ end Block 4------------------------------------------------>
<!--container end-->

<script type="text/javascript">

    $('#bigAd1_slot').bind('inview', function (event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            // element is now visible in the viewport
            showBigAD2('bigAd2_slot');
            if (visiblePartY == 'top') {
                // top part of element is visible
            } else if (visiblePartY == 'bottom') {
                // bottom part of element is visible     

            } else {
                // whole part of element is visible
            }
        } else {
            // element has gone out of viewport
        }
    });
    $('#bigAd2_slot').bind('inview', function (event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            // element is now visible in the viewport
            showBigAD3('bigAd3_slot');
            if (visiblePartY == 'top') {
                // top part of element is visible

            } else if (visiblePartY == 'bottom') {
                // bottom part of element is visible  

            } else {
                // whole part of element is visible
            }
        } else {
            // element has gone out of viewport
        }
    });
    $('#bigAd3_slot').bind('inview', function (event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            // element is now visible in the viewport
            BADros('badRos_slot');
            if (visiblePartY == 'top') {
                // top part of element is visible
            } else if (visiblePartY == 'bottom') {
                // bottom part of element is visible
            } else {
                // whole part of element is visible
            }
        } else {
            // element has gone out of viewport
        }
    });

    $(document).ready(function () {
        /* spotlight onload tracking homepage */
        //console.log("homepage");
    });

</script>    <div class="clr"></div>

	
    <script>
                showFooterCode();
        </script>
<style>
    .bottom-strip{background:#ebebeb;display:block;padding:15px 0;overflow:hidden;}.bottom-strip-box{margin:0 auto;display:table;max-width:410px;width:100%;}.square-logo{width:66px;height:64px;float:left;margin-right:10px;}.bottom-strip-content{float:left;margin-top:14px;}.bottom-strip-content span{display:block;color:#737272;font:12px/1 "RobotoRegular", Arial, Helvetica, sans-serif;margin-bottom:2px;}.bottom-strip-content a{color:#1b85dd;font:24px/1 "RobotoRegular", Arial, Helvetica, sans-serif;text-decoration:none;}
</style>
<div class="last-container">
<!--159<br>Health--> 
    <!--social-panel start-->
    <div class="social-panel red-bg">
        <div class="soc-inner"><!--soc-inner start-->
            <p class="soc-title">Be a Part of The New & Next</p>
            <div class="soc-count"><!--soc-count start-->
                <a href="https://www.facebook.com/indiatimes" target="_blank" title="facebook" class="sprite soc-fb"></a>
                <p>
                    4,654,881<span class="black"> FRIENDS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://twitter.com/indiatimes" target="_blank" title="twitter" class="sprite soc-twt"></a>
                <p>10343  <span class="black">FOLLOWERS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://plus.google.com/+indiatimes" target="_blank" title="gplus" class="sprite soc-gplus"></a>
                <p>104,163 <span class="black">MEMBERS</span></p>
            </div>
            <div class="soc-count"><!--soc-count start-->
                <a href="javascript:void(0);" title="E-mail" class="sprite soc-mail" id="subscribers_id"></a>
                <p>60,183 <span class="black">SUBSCRIBERS</span></p>
            </div>
            <!--soc-count end-->
        </div>
        <!--soc-inner end-->
    </div>
    <!--social-panel end-->
    <div class="clr"></div>

    <footer>

        <div class="footer"><!-- footer grey start-->
            <div class="sub_container">
                <!-- container start-->
                <div class="cont_new">
                    <h2>Our Channels</h2>
                                                <a href='http://www.indiatimes.com/news/' class="blue size">News</a> 
                                <a href='http://www.indiatimes.com/lifestyle/' class="purple size">Lifestyle</a> 
                                <a href='http://www.indiatimes.com/entertainment/' class="red size">Entertainment</a> 
                                <a href='http://www.indiatimes.com/health/' class="green size">Health</a> 
                                <a href='http://www.indiatimes.com/videocafe/' class="orange size">Videos</a> 
                                <a href='http://www.indiatimes.com/play/' class="red size">Slog Overs</a> 
                        <a href='http://www.indiatimes.com/trending' class="size" style="color:white">Trending</a> 
                </div><!-- container closed-->

                <div class="cont"> <!-- container start-->
                    <h2>Sections</h2>
                                                <div class="sub_link">
                         
                                    <a href='http://www.indiatimes.com/news/india/'>india</a>
                                             
                                    <a href='http://www.indiatimes.com/news/world/'>world</a>
                                             
                                    <a href='http://www.indiatimes.com/news/sports/'>sports</a>
                                             
                                    <a href='http://www.indiatimes.com/news/weird/'>weird</a>
                                
                             </div>                        <div class="sub_link">
                         
                                    <a href='http://www.indiatimes.com/lifestyle/self/'>self</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/style/'>style</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/who-we-are/'>who we are</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/travel/'>travel</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/food/'>food</a>
                                
                             </div>                        <div class="sub_link">
                         
                                    <a href='http://www.indiatimes.com/entertainment/bollywood/'>bollywood</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/celebs/'>celebscoop</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/hollywood/'>hollywood</a>
                                
                             </div>                        <div class="sub_link">
                         
                                    <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                             
                                    <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                             
                                    <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                             
                                    <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                             
                                    <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                
                             </div>                        <div class="sub_link">
     </div>                    <div class="sub_link"><a href="http://www.indiatimes.com/photogallery">Photogallery</a></div>
                </div><!-- container closed-->
                <div class="cont_new"> <!-- container start-->
                    <h2>Indiatimes Lifestyle Network</h2>

                    <div class="sub_link">                             <a href="http://www.mensxp.com"   target="_blank" >MENS XP</a>
                                                    <a href="http://www.idiva.com"   target="_blank" >iDiva</a>
                                                    <a href="http://luxpresso.com"   target="_blank" >Luxury</a>
                                                    <a href="http://in.askmen.com/"   target="_blank" >Ask Men</a>
                                                    <a href="http://www.pursuitist.in/"   target="_blank" >Pursuitist</a>
                        </div>
                </div><!-- container closed-->
                <div class="cont"> <!-- container start-->
                    <h2>Other Sites</h2>
                    <div class="sub_link"> 
                        
                            <a href="http://timesofindia.indiatimes.com/budgetspecial.cms"  target="_blank">Budget 2016</a> 
                        
                            <a href="http://timesofindia.indiatimes.com/budget-2016/rail-budget-2016/indiabudget/50867848.cms"  target="_blank">Rail Budget 2016</a> 
                        
                            <a href="http://www.cricbuzz.com"  target="_blank">CricBuzz</a> 
                        
                            <a href="http://www.happytrips.com/"  target="_blank">Happy Trips</a> 
                        
                            <a href="http://www.businessinsider.in/"  target="_blank">Business Insider</a> 
                        
                            <a href="http://www.zoomtv.com"  target="_blank">ZoomTv</a> 
                        
                            <a href="http://www.gizmodo.in/"  target="_blank">Gizmodo</a> 
                        
                            <a href="http://www.lifehacker.co.in/"  target="_blank">Lifehacker</a> 
                        
                            <a href="http://in.ign.com/"  target="_blank">IGN</a> 
                        
                            <a href="http://boxtv.com"  target="_blank">BoxTV</a> 
                        
                            <a href="http://www.gaana.com"  target="_blank">Gaana</a> 
                        
                            <a href="http://shopping.indiatimes.com"  target="_blank">Online Shopping</a> 
                        
                            <a href="http://www.in.techradar.com/"  target="_blank">Techradar</a> 
                        
                            <a href="http://timesdeal.com"  target="_blank">Daily Deals</a> 
                        
                            <a href="http://www.gitanjalishop.com/"  target="_blank">Gitanjali Shop</a> 
                        
                            <a href="http://www.satvikshop.com/"  target="_blank">Satvik Shop</a> 
                        
                            <a href="http://whatshot.in"  target="_blank">WHAT'S HOT</a> 
                        
                            <a href="http://zigwheels.com"  target="_blank">ZigWheels</a> 
                        
                            <a href="http://filmipop.com"  target="_blank">FILMIPOP</a> 
                        
                            <a href="http://www.filmfare.com/"  target="_blank">Filmfare</a> 
                        
                            <a href="http://www.femina.in"  target="_blank">Femina</a> 
                        
                            <a href="http://mobile.indiatimes.com"  target="_blank">Mobile</a> 
                        
                            <a href="http://www.greetzap.com"  target="_blank">Greetings</a> 
                        
                            <a href="http://www.follo.co.in"  target="_blank">Follo</a> 
                        
                            <a href="http://timesjobs.com"  target="_blank">Jobs</a> 
                        
                            <a href="http://magicbricks.com"  target="_blank">Property</a> 
                        
                            <a href="http://www.astrospeak.com"  target="_blank">Astrology</a> 
                        
                            <a href="http://speakingtree.in"  target="_blank">Speaking Tree</a> 
                        
                            <a href="http://simplymarry.com"  target="_blank">Matrimonial</a> 
                        
                            <a href="http://itimes.com"  target="_blank">iTimes</a> 
                        
                            <a href="http://www.ads2book.com/"  target="_blank">Ads2book</a> 
                        
                            <a href="http://www.tcnext.com/"  target="_blank">TcNext</a> 
                        
                            <a href="http://www.romedynow.com"  target="_blank">Romedy Now</a> 
                                            </div>
                </div><!-- container closed-->
                <div class="cont border_none"><!-- container start-->
                    <h2>About us</h2>
                    <div class="sub_link">
                            <a href='http://www.indiatimes.com/privacypolicy' >Privacy Policy</a> 
                            <a href='http://www.indiatimes.com/sitemap.html' >sitemap</a> 
                            <a href='http://www.indiatimes.com/termsandcondition' >Terms & CONDITIONS</a> 
                            <a href='http://www.indiatimes.com/seoarchive' >ARCHIVES</a> 
                            <a href='http://www.indiatimes.com/contactus' >Contact us</a> 
                            <a href='http://www.indiatimes.com/aboutus' >ABOUT INDIATIMES</a> 
                    </div>
                </div><!-- container closed-->
            </div><!-- footer grey closed-->
        </div><!-- footer black closed-->

        <div class="black_footer"><!-- footer black start-->
            <div class="containers">
                <p><a href="javascript:void(0)" class="copyrt">Copyright Â© 2016 Times Internet Limited. Powered by Indiatimes Lifestyle Network. All rights reserved</a></p>
                <span><a href="http://www.indiatimes.com/seoarchive/">Archive</a> | <a href="http://www.indiatimes.com/sitemap.html">Sitemap</a> | <a href="http://www.indiatimes.com/aboutus/">About Us</a> | <a href="http://www.indiatimes.com/contactus/">Contact Us</a> | <a href="http://www.indiatimes.com/privacypolicy/">Privacy Policy</a> | <a target="_blank" href="http://m.indiatimes.com">Indiatimes Mobile</a> | <a href="http://www.indiatimes.com/termsandcondition/">Terms of Use and Grievance Redressal Policy </a> </span> 
            </div>
        </div><!-- footer black end-->
    </footer>
</div>
<script>
    $(document).ready(function () {
        $("#subscribers_id").click(function () {
            $("#UserEmail").focus();
        });
    });
    function socialRHSHide() {}
</script>

    <script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.lazyload.min.js?v=115.6" charset="utf-8"></script>
<script defer type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.easing.1.3.min.js?v=115.6"></script>

<input type="hidden" name="currentId" id="currentId" value="">
    <div class="animate_cont dawn floating delay5">
        <span class="animate_arrow sprite_image"></span>
    </div>




<script type="text/javascript" src="http://media.indiatimes.in/resources/js/main.js?v=115.6"></script>    
<script type="text/javascript" src="http://media.indiatimes.in/resources/js/common.js?v=115.6"></script>
<script  defer type="text/javascript" src="http://media.indiatimes.in/resources/js/jquery-scrolltofixed.js?v=115.6"></script>

<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://cdn.mouseflow.com/projects/1e238441-c30e-4d1b-8edd-c0f14d8fa6b7.js' type='text/javascript'%3E%3C/script%3E"));</script>
<!-- mouse flow code ends-->
<script  src="http://tags.crwdcntrl.net/c/2818/cc.js?ns=_cc2818" id="LOTCC_2818"></script> 

<script> 
    _cc2818.bcp(); 
</script>

	 <p id="back-top" style="display:none;"><a href="#top"><span></span></a> </p>
</div><!--wrap end-->


</body>
</html>