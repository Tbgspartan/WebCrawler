<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="auto">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-261c451.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-261c451.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-261c451.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-261c451.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kickass.to/sc-261c451.js');

        sc('setHost', 'a.kickass.to');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: '261c451',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-261c451.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<span  data-sc-slot="_60318cd4e8d28f6fb76fe34e9bd9c498"></span>
<span  data-sc-slot="_39ecb76dd457e5ac33776fdf11500d56"></span>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka ka-settings"></i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka ka-search"></i></button></div>
			</form>
		</div>
        <span  data-sc-slot="_277923e5f9d753c5b0630c28e641790c"></span>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/007%20spectre/" class="tag3">007 spectre</a>
	<a href="/search/1080p/" class="tag5">1080p</a>
	<a href="/search/2014/" class="tag2">2014</a>
	<a href="/search/2015/" class="tag10">2015</a>
	<a href="/search/3d/" class="tag3">3d</a>
	<a href="/search/adele/" class="tag3">adele</a>
	<a href="/search/adele%2025/" class="tag2">adele 25</a>
	<a href="/search/android/" class="tag2">android</a>
	<a href="/search/blindspot/" class="tag3">blindspot</a>
	<a href="/search/blindspot%20s01e10/" class="tag2">blindspot s01e10</a>
	<a href="/search/christmas/" class="tag3">christmas</a>
	<a href="/search/discography/" class="tag2">discography</a>
	<a href="/search/doctor%20who/" class="tag2">doctor who</a>
	<a href="/search/dual%20audio%20hindi/" class="tag2">dual audio hindi</a>
	<a href="/search/fallout%204/" class="tag2">fallout 4</a>
	<a href="/search/fargo/" class="tag3">fargo</a>
	<a href="/search/fargo%20s02e07/" class="tag2">fargo s02e07</a>
	<a href="/search/french/" class="tag3">french</a>
	<a href="/search/gotham/" class="tag3">gotham</a>
	<a href="/search/gotham%20s02e10/" class="tag2">gotham s02e10</a>
	<a href="/search/hindi/" class="tag8">hindi</a>
	<a href="/search/hindi%202015/" class="tag6">hindi 2015</a>
	<a href="/search/hotel%20transylvania%202/" class="tag2">hotel transylvania 2</a>
	<a href="/search/is%20safe%201/" class="tag4">is safe 1</a>
	<a href="/search/ita/" class="tag2">ita</a>
	<a href="/search/jessica%20jones/" class="tag5">jessica jones</a>
	<a href="/search/limitless/" class="tag2">limitless</a>
	<a href="/search/malayalam/" class="tag3">malayalam</a>
	<a href="/search/nezu/" class="tag10">nezu</a>
	<a href="/search/prem%20ratan%20dhan%20payo/" class="tag2">prem ratan dhan payo</a>
	<a href="/search/quantico/" class="tag3">quantico</a>
	<a href="/search/ripsalot/" class="tag4">ripsalot</a>
	<a href="/search/scorpion/" class="tag2">scorpion</a>
	<a href="/search/spectre/" class="tag2">spectre</a>
	<a href="/search/star%20wars/" class="tag2">star wars</a>
	<a href="/search/supergirl/" class="tag4">supergirl</a>
	<a href="/search/tamil/" class="tag4">tamil</a>
	<a href="/search/telugu/" class="tag4">telugu</a>
	<a href="/search/telugu%202015/" class="tag2">telugu 2015</a>
	<a href="/search/the%20man%20in%20the%20high%20castle/" class="tag3">the man in the high castle</a>
	<a href="/search/the%20martian/" class="tag2">the martian</a>
	<a href="/search/the%20walking%20dead/" class="tag7">the walking dead</a>
	<a href="/search/the%20walking%20dead%20s06e07/" class="tag2">the walking dead s06e07</a>
	<a href="/search/walking%20dead/" class="tag3">walking dead</a>
	<a href="/search/wwe/" class="tag2">wwe</a>
	<a href="/search/yify/" class="tag10">yify</a>
	<a href="/search/yify%201080p/" class="tag4">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag4">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11637936,0" class="icommentjs kaButton smallButton rightButton" href="/007-spectre-2015-real-720p-hdts-1gb-mkvcage-t11637936.html#comment">78 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/007-spectre-2015-real-720p-hdts-1gb-mkvcage-t11637936.html" class="cellMainLink">007: Spectre (2015) REAL 720P HDTS 1GB - MkvCage</a></div>
			</td>
			<td class="nobr center" data-sort="1148281818">1.07 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T03:51:21+00:00">23 Nov 2015, 03:51:21</span></td>
			<td class="green center">8618</td>
			<td class="red lasttd center">5886</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11646264,0" class="icommentjs kaButton smallButton rightButton" href="/heist-2015-1080p-web-dl-x264-ac3-jyk-t11646264.html#comment">21 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/heist-2015-1080p-web-dl-x264-ac3-jyk-t11646264.html" class="cellMainLink">Heist 2015 1080p WEB-DL x264 AC3-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="2718760935">2.53 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T15:32:47+00:00">24 Nov 2015, 15:32:47</span></td>
			<td class="green center">7097</td>
			<td class="red lasttd center">7900</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11648412,0" class="icommentjs kaButton smallButton rightButton" href="/the-hunger-games-mockingjay-part-2-2015-hd-ts-xvid-ac3-hq-hive-cm8-t11648412.html#comment">44 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-hunger-games-mockingjay-part-2-2015-hd-ts-xvid-ac3-hq-hive-cm8-t11648412.html" class="cellMainLink">The Hunger Games-Mockingjay Part 2 2015 HD-TS XVID AC3 HQ Hive-CM8</a></div>
			</td>
			<td class="nobr center" data-sort="1862550440">1.73 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T20:58:06+00:00">24 Nov 2015, 20:58:06</span></td>
			<td class="green center">4235</td>
			<td class="red lasttd center">7676</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11639377,0" class="icommentjs kaButton smallButton rightButton" href="/infini-2015-1080p-bluray-x264-dts-jyk-t11639377.html#comment">31 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/infini-2015-1080p-bluray-x264-dts-jyk-t11639377.html" class="cellMainLink">Infini 2015 1080p BluRay x264 DTS-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="2993055589">2.79 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T10:50:08+00:00">23 Nov 2015, 10:50:08</span></td>
			<td class="green center">5158</td>
			<td class="red lasttd center">3691</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11631898,0" class="icommentjs kaButton smallButton rightButton" href="/maze-runner-the-scorch-trials-2015-1080p-bluray-x264-dts-jyk-t11631898.html#comment">58 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/maze-runner-the-scorch-trials-2015-1080p-bluray-x264-dts-jyk-t11631898.html" class="cellMainLink">Maze Runner The Scorch Trials 2015 1080p BluRay x264 DTS-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="3563344341">3.32 <span>GB</span></td>
			<td class="center">8</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T23:30:18+00:00">21 Nov 2015, 23:30:18</span></td>
			<td class="green center">4324</td>
			<td class="red lasttd center">3006</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11645939,0" class="icommentjs kaButton smallButton rightButton" href="/santas-little-helper-2015-hdrip-xvid-ac3-evo-t11645939.html#comment">15 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/santas-little-helper-2015-hdrip-xvid-ac3-evo-t11645939.html" class="cellMainLink">Santas Little Helper 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1504298166">1.4 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T13:52:08+00:00">24 Nov 2015, 13:52:08</span></td>
			<td class="green center">2684</td>
			<td class="red lasttd center">3463</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11638135,0" class="icommentjs kaButton smallButton rightButton" href="/love-2015-v2-hdrip-xvid-ac3-evo-t11638135.html#comment">23 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/love-2015-v2-hdrip-xvid-ac3-evo-t11638135.html" class="cellMainLink">Love 2015 V2 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1543262348">1.44 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T04:44:46+00:00">23 Nov 2015, 04:44:46</span></td>
			<td class="green center">2714</td>
			<td class="red lasttd center">3292</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11635529,0" class="icommentjs kaButton smallButton rightButton" href="/christmas-trade-2015-hdrip-xvid-ac3-evo-t11635529.html#comment">21 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/christmas-trade-2015-hdrip-xvid-ac3-evo-t11635529.html" class="cellMainLink">Christmas Trade 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1505478358">1.4 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T15:42:21+00:00">22 Nov 2015, 15:42:21</span></td>
			<td class="green center">2978</td>
			<td class="red lasttd center">2531</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11639924,0" class="icommentjs kaButton smallButton rightButton" href="/the-lion-guard-return-of-the-roar-2015-720p-hdtv-350mb-mkvcage-t11639924.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-lion-guard-return-of-the-roar-2015-720p-hdtv-350mb-mkvcage-t11639924.html" class="cellMainLink">The Lion Guard: Return of the Roar (2015) 720p HDTV 350MB - MkvCage</a></div>
			</td>
			<td class="nobr center" data-sort="368930661">351.84 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T13:48:40+00:00">23 Nov 2015, 13:48:40</span></td>
			<td class="green center">3087</td>
			<td class="red lasttd center">1113</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11639378,0" class="icommentjs kaButton smallButton rightButton" href="/the-mummy-resurrected-2014-1080p-bluray-x264-dts-jyk-t11639378.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-mummy-resurrected-2014-1080p-bluray-x264-dts-jyk-t11639378.html" class="cellMainLink">The Mummy Resurrected 2014 1080p BluRay x264 DTS-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="2173854040">2.02 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T10:50:11+00:00">23 Nov 2015, 10:50:11</span></td>
			<td class="green center">2613</td>
			<td class="red lasttd center">1803</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11638149,0" class="icommentjs kaButton smallButton rightButton" href="/the-perfect-husband-2014-uncut-brrip-xvid-ac3-evo-t11638149.html#comment">22 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-perfect-husband-2014-uncut-brrip-xvid-ac3-evo-t11638149.html" class="cellMainLink">The Perfect Husband 2014 UNCUT BRRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1512382884">1.41 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T04:48:21+00:00">23 Nov 2015, 04:48:21</span></td>
			<td class="green center">2363</td>
			<td class="red lasttd center">2153</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11649234,0" class="icommentjs kaButton smallButton rightButton" href="/goodbye-mr-loser-2015-hd720p-x264-aac-mandarin-chs-eng-mp4ba-t11649234.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/goodbye-mr-loser-2015-hd720p-x264-aac-mandarin-chs-eng-mp4ba-t11649234.html" class="cellMainLink">Goodbye Mr Loser 2015 HD720P X264 AAC Mandarin CHS-ENG Mp4Ba</a></div>
			</td>
			<td class="nobr center" data-sort="1525153914">1.42 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T02:59:40+00:00">25 Nov 2015, 02:59:40</span></td>
			<td class="green center">1036</td>
			<td class="red lasttd center">3890</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11636952,0" class="icommentjs kaButton smallButton rightButton" href="/unforgiven-2013-french-bdrip-x264-avitech-mkv-t11636952.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/unforgiven-2013-french-bdrip-x264-avitech-mkv-t11636952.html" class="cellMainLink">Unforgiven 2013 FRENCH BDRiP x264-AViTECH mkv</a></div>
			</td>
			<td class="nobr center" data-sort="729356551">695.57 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T22:28:55+00:00">22 Nov 2015, 22:28:55</span></td>
			<td class="green center">1945</td>
			<td class="red lasttd center">409</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11635538,0" class="icommentjs kaButton smallButton rightButton" href="/dark-moon-rising-2015-brrip-xvid-ac3-evo-t11635538.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dark-moon-rising-2015-brrip-xvid-ac3-evo-t11635538.html" class="cellMainLink">Dark Moon Rising 2015 BRRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1476435492">1.38 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T15:45:02+00:00">22 Nov 2015, 15:45:02</span></td>
			<td class="green center">1073</td>
			<td class="red lasttd center">1257</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11637322,0" class="icommentjs kaButton smallButton rightButton" href="/slow-west-2015-french-bdrip-xvid-avitech-avi-t11637322.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/slow-west-2015-french-bdrip-xvid-avitech-avi-t11637322.html" class="cellMainLink">Slow West 2015 FRENCH BDRiP XViD-AViTECH avi</a></div>
			</td>
			<td class="nobr center" data-sort="733661184">699.67 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T00:23:06+00:00">23 Nov 2015, 00:23:06</span></td>
			<td class="green center">1392</td>
			<td class="red lasttd center">295</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11643465,0" class="icommentjs kaButton smallButton rightButton" href="/gotham-s02e10-hdtv-x264-lol-ettv-t11643465.html#comment">145 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/gotham-s02e10-hdtv-x264-lol-ettv-t11643465.html" class="cellMainLink">Gotham S02E10 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="231998941">221.25 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T02:01:18+00:00">24 Nov 2015, 02:01:18</span></td>
			<td class="green center">15028</td>
			<td class="red lasttd center">2444</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11648928,0" class="icommentjs kaButton smallButton rightButton" href="/limitless-s01e10-hdtv-x264-lol-rartv-t11648928.html#comment">29 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/limitless-s01e10-hdtv-x264-lol-rartv-t11648928.html" class="cellMainLink">Limitless S01E10 HDTV x264-LOL[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="257294263">245.37 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T00:59:02+00:00">25 Nov 2015, 00:59:02</span></td>
			<td class="green center">13051</td>
			<td class="red lasttd center">4459</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11643455,0" class="icommentjs kaButton smallButton rightButton" href="/supergirl-s01e04-hdtv-x264-lol-ettv-t11643455.html#comment">138 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/supergirl-s01e04-hdtv-x264-lol-ettv-t11643455.html" class="cellMainLink">Supergirl S01E04 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="358426975">341.82 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T01:59:23+00:00">24 Nov 2015, 01:59:23</span></td>
			<td class="green center">11750</td>
			<td class="red lasttd center">2720</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11643622,0" class="icommentjs kaButton smallButton rightButton" href="/blindspot-s01e10-hdtv-x264-lol-ettv-t11643622.html#comment">119 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/blindspot-s01e10-hdtv-x264-lol-ettv-t11643622.html" class="cellMainLink">Blindspot S01E10 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="301103349">287.15 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T03:02:19+00:00">24 Nov 2015, 03:02:19</span></td>
			<td class="green center">10957</td>
			<td class="red lasttd center">2060</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11647639,0" class="icommentjs kaButton smallButton rightButton" href="/the-walking-dead-s06e07-fastsub-vostfr-hdtv-xvid-ark01-avi-t11647639.html#comment">17 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-walking-dead-s06e07-fastsub-vostfr-hdtv-xvid-ark01-avi-t11647639.html" class="cellMainLink">The Walking Dead S06E07 FASTSUB VOSTFR HDTV XviD-ARK01 avi</a></div>
			</td>
			<td class="nobr center" data-sort="367269888">350.26 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T17:45:38+00:00">24 Nov 2015, 17:45:38</span></td>
			<td class="green center">8063</td>
			<td class="red lasttd center">848</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11638011,0" class="icommentjs kaButton smallButton rightButton" href="/homeland-s05e08-internal-hdtv-x264-killers-ettv-t11638011.html#comment">84 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/homeland-s05e08-internal-hdtv-x264-killers-ettv-t11638011.html" class="cellMainLink">Homeland S05E08 INTERNAL HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="289743725">276.32 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T04:14:22+00:00">23 Nov 2015, 04:14:22</span></td>
			<td class="green center">7925</td>
			<td class="red lasttd center">709</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11643603,0" class="icommentjs kaButton smallButton rightButton" href="/scorpion-s02e10-hdtv-x264-lol-ettv-t11643603.html#comment">70 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/scorpion-s02e10-hdtv-x264-lol-ettv-t11643603.html" class="cellMainLink">Scorpion S02E10 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="308872348">294.56 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T02:59:18+00:00">24 Nov 2015, 02:59:18</span></td>
			<td class="green center">6708</td>
			<td class="red lasttd center">1302</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11637587,0" class="icommentjs kaButton smallButton rightButton" href="/the-man-in-the-high-castle-s01-webrip-xvid-fum-ettv-t11637587.html#comment">45 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-man-in-the-high-castle-s01-webrip-xvid-fum-ettv-t11637587.html" class="cellMainLink">The Man In The High Castle S01 WEBRip XviD-FUM[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="4600247046">4.28 <span>GB</span></td>
			<td class="center">11</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T02:09:55+00:00">23 Nov 2015, 02:09:55</span></td>
			<td class="green center">5011</td>
			<td class="red lasttd center">4318</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11643904,0" class="icommentjs kaButton smallButton rightButton" href="/fargo-s02e07-hdtv-x264-killers-ettv-t11643904.html#comment">63 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fargo-s02e07-hdtv-x264-killers-ettv-t11643904.html" class="cellMainLink">Fargo S02E07 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="450636574">429.76 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T04:35:19+00:00">24 Nov 2015, 04:35:19</span></td>
			<td class="green center">6239</td>
			<td class="red lasttd center">975</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11639107,0" class="icommentjs kaButton smallButton rightButton" href="/the-expanse-s01e01-hdtv-x264-batv-ettv-t11639107.html#comment">67 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-expanse-s01e01-hdtv-x264-batv-ettv-t11639107.html" class="cellMainLink">The Expanse S01E01 HDTV x264-BATV[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="335296297">319.76 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T09:04:26+00:00">23 Nov 2015, 09:04:26</span></td>
			<td class="green center">5549</td>
			<td class="red lasttd center">829</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11639475,0" class="icommentjs kaButton smallButton rightButton" href="/the-43rd-annual-american-music-awards-2015-hdtv-x264-alterego-ettv-t11639475.html#comment">45 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-43rd-annual-american-music-awards-2015-hdtv-x264-alterego-ettv-t11639475.html" class="cellMainLink">The 43rd Annual American Music Awards 2015 HDTV x264-ALTEREGO[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="1595297926">1.49 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T11:29:24+00:00">23 Nov 2015, 11:29:24</span></td>
			<td class="green center">4278</td>
			<td class="red lasttd center">2278</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11643610,0" class="icommentjs kaButton smallButton rightButton" href="/castle-2009-s08e08-hdtv-x264-lol-ettv-t11643610.html#comment">39 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/castle-2009-s08e08-hdtv-x264-lol-ettv-t11643610.html" class="cellMainLink">Castle 2009 S08E08 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="301466780">287.5 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T03:00:18+00:00">24 Nov 2015, 03:00:18</span></td>
			<td class="green center">4503</td>
			<td class="red lasttd center">769</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11649048,0" class="icommentjs kaButton smallButton rightButton" href="/ncis-s13e10-hdtv-x264-lol-rartv-t11649048.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ncis-s13e10-hdtv-x264-lol-rartv-t11649048.html" class="cellMainLink">NCIS S13E10 HDTV x264-LOL[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="215199613">205.23 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T01:55:43+00:00">25 Nov 2015, 01:55:43</span></td>
			<td class="green center">4230</td>
			<td class="red lasttd center">1028</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11637823,0" class="icommentjs kaButton smallButton rightButton" href="/brooklyn-nine-nine-s03e08-proper-hdtv-x264-killers-ettv-t11637823.html#comment">46 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/brooklyn-nine-nine-s03e08-proper-hdtv-x264-killers-ettv-t11637823.html" class="cellMainLink">Brooklyn Nine-Nine S03E08 PROPER HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="203144979">193.73 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T03:19:20+00:00">23 Nov 2015, 03:19:20</span></td>
			<td class="green center">4269</td>
			<td class="red lasttd center">374</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11649409,0" class="icommentjs kaButton smallButton rightButton" href="/scream-queens-2015-s01e10-hdtv-x264-killers-ettv-t11649409.html#comment">15 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/scream-queens-2015-s01e10-hdtv-x264-killers-ettv-t11649409.html" class="cellMainLink">Scream Queens 2015 S01E10 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="305809898">291.64 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T04:26:37+00:00">25 Nov 2015, 04:26:37</span></td>
			<td class="green center">3463</td>
			<td class="red lasttd center">1579</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11631543,0" class="icommentjs kaButton smallButton rightButton" href="/justin-bieber-purpose-2015-deluxe-edition-mp3-320kbps-t11631543.html#comment">21 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/justin-bieber-purpose-2015-deluxe-edition-mp3-320kbps-t11631543.html" class="cellMainLink">Justin Bieber - Purpose [2015] [Deluxe Edition] [Mp3-320Kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="201402861">192.07 <span>MB</span></td>
			<td class="center">27</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T21:17:31+00:00">21 Nov 2015, 21:17:31</span></td>
			<td class="green center">1167</td>
			<td class="red lasttd center">416</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11634761,0" class="icommentjs kaButton smallButton rightButton" href="/adele-special-discography-2008-15-channel-neo-t11634761.html#comment">16 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/adele-special-discography-2008-15-channel-neo-t11634761.html" class="cellMainLink">ADELE - SPECIAL DISCOGRAPHY (2008-15) [CHANNEL NEO]</a></div>
			</td>
			<td class="nobr center" data-sort="941733755">898.11 <span>MB</span></td>
			<td class="center">100</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T12:15:02+00:00">22 Nov 2015, 12:15:02</span></td>
			<td class="green center">494</td>
			<td class="red lasttd center">575</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11640512,0" class="icommentjs kaButton smallButton rightButton" href="/foo-fighters-saint-cecilia-ep-rock-2015-t11640512.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/foo-fighters-saint-cecilia-ep-rock-2015-t11640512.html" class="cellMainLink">Foo Fighters - Saint Cecilia (EP) [Rock] [2015]</a></div>
			</td>
			<td class="nobr center" data-sort="46107932">43.97 <span>MB</span></td>
			<td class="center">8</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T16:43:20+00:00">23 Nov 2015, 16:43:20</span></td>
			<td class="green center">635</td>
			<td class="red lasttd center">92</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11634620,0" class="icommentjs kaButton smallButton rightButton" href="/va-music-compilation-november-2015-2015-mp3-320-kbps-t11634620.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-music-compilation-november-2015-2015-mp3-320-kbps-t11634620.html" class="cellMainLink">VA - Music compilation November 2015 (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="368649503">351.57 <span>MB</span></td>
			<td class="center">39</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T11:42:20+00:00">22 Nov 2015, 11:42:20</span></td>
			<td class="green center">493</td>
			<td class="red lasttd center">186</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11641493,0" class="icommentjs kaButton smallButton rightButton" href="/va-natural-deep-house-2015-mp3-320-kbps-t11641493.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-natural-deep-house-2015-mp3-320-kbps-t11641493.html" class="cellMainLink">VA - Natural Deep House (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="1383174503">1.29 <span>GB</span></td>
			<td class="center">103</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T18:40:25+00:00">23 Nov 2015, 18:40:25</span></td>
			<td class="green center">409</td>
			<td class="red lasttd center">282</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-autumn-festival-100-club-house-2015-mp3-320-kbps-t11641417.html" class="cellMainLink">VA - Autumn Festival: 100 Club House (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="1232357030">1.15 <span>GB</span></td>
			<td class="center">102</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T18:20:17+00:00">23 Nov 2015, 18:20:17</span></td>
			<td class="green center">352</td>
			<td class="red lasttd center">238</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11634593,0" class="icommentjs kaButton smallButton rightButton" href="/va-disco-dance-club-vol-145-2015-mp3-320-kbps-t11634593.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-disco-dance-club-vol-145-2015-mp3-320-kbps-t11634593.html" class="cellMainLink">VA - Disco Dance Club Vol. 145 (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="1598082851">1.49 <span>GB</span></td>
			<td class="center">135</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T11:36:45+00:00">22 Nov 2015, 11:36:45</span></td>
			<td class="green center">341</td>
			<td class="red lasttd center">218</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-classical-voices-the-musicals-2015-320kpbs-freak37-t11645111.html" class="cellMainLink">VA - Classical Voices The Musicals - (2015) [320kpbs ] - Freak37</a></div>
			</td>
			<td class="nobr center" data-sort="388639182">370.64 <span>MB</span></td>
			<td class="center">48</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T10:30:36+00:00">24 Nov 2015, 10:30:36</span></td>
			<td class="green center">358</td>
			<td class="red lasttd center">115</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11637220,0" class="icommentjs kaButton smallButton rightButton" href="/eric-clapton-slowhand-at-70-live-at-the-royal-albert-hall-2015-freak37-t11637220.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/eric-clapton-slowhand-at-70-live-at-the-royal-albert-hall-2015-freak37-t11637220.html" class="cellMainLink">Eric Clapton âSlowhand At 70 Live At The Royal Albert Hall (2015)...Freak37</a></div>
			</td>
			<td class="nobr center" data-sort="287085317">273.79 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T23:44:19+00:00">22 Nov 2015, 23:44:19</span></td>
			<td class="green center">369</td>
			<td class="red lasttd center">69</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11632750,0" class="icommentjs kaButton smallButton rightButton" href="/various-artists-hip-hop-txl-vol-73-224kbps-2015-mixjoint-t11632750.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/various-artists-hip-hop-txl-vol-73-224kbps-2015-mixjoint-t11632750.html" class="cellMainLink">Various Artists - Hip Hop TXL Vol 73 [224Kbps] [2015] (MixJoint)</a></div>
			</td>
			<td class="nobr center" data-sort="354770457">338.34 <span>MB</span></td>
			<td class="center">63</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T04:25:36+00:00">22 Nov 2015, 04:25:36</span></td>
			<td class="green center">282</td>
			<td class="red lasttd center">75</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11634640,0" class="icommentjs kaButton smallButton rightButton" href="/va-chillout-vol-32-compiled-by-zebyte-2015-mp3-320-kbps-t11634640.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-chillout-vol-32-compiled-by-zebyte-2015-mp3-320-kbps-t11634640.html" class="cellMainLink">VA - Chillout Vol.32 [Compiled by Zebyte] (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="340109644">324.35 <span>MB</span></td>
			<td class="center">32</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T11:47:37+00:00">22 Nov 2015, 11:47:37</span></td>
			<td class="green center">220</td>
			<td class="red lasttd center">75</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11631966,0" class="icommentjs kaButton smallButton rightButton" href="/the-christmas-beauty-lounge-mp3-divxtotal-t11631966.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-christmas-beauty-lounge-mp3-divxtotal-t11631966.html" class="cellMainLink">The Christmas Beauty Lounge MP3-DiVXTOTAL</a></div>
			</td>
			<td class="nobr center" data-sort="361234204">344.5 <span>MB</span></td>
			<td class="center">31</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T00:03:06+00:00">22 Nov 2015, 00:03:06</span></td>
			<td class="green center">223</td>
			<td class="red lasttd center">65</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11644661,0" class="icommentjs kaButton smallButton rightButton" href="/status-quo-accept-no-substitute-the-definitive-hits-2015-freak37-t11644661.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/status-quo-accept-no-substitute-the-definitive-hits-2015-freak37-t11644661.html" class="cellMainLink">Status Quo â Accept No Substitute The Definitive Hits (2015) ...Freak37</a></div>
			</td>
			<td class="nobr center" data-sort="536830331">511.96 <span>MB</span></td>
			<td class="center">57</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T08:21:34+00:00">24 Nov 2015, 08:21:34</span></td>
			<td class="green center">160</td>
			<td class="red lasttd center">71</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11643664,0" class="icommentjs kaButton smallButton rightButton" href="/epica-consign-to-oblivion-expanded-edition-2015-320ak-t11643664.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/epica-consign-to-oblivion-expanded-edition-2015-320ak-t11643664.html" class="cellMainLink">Epica - Consign To Oblivion (Expanded Edition) 2015 320ak</a></div>
			</td>
			<td class="nobr center" data-sort="323331498">308.35 <span>MB</span></td>
			<td class="center">28</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T03:16:08+00:00">24 Nov 2015, 03:16:08</span></td>
			<td class="green center">155</td>
			<td class="red lasttd center">42</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11631961,0" class="icommentjs kaButton smallButton rightButton" href="/this-is-chillout-mp3-divxtotal-t11631961.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/this-is-chillout-mp3-divxtotal-t11631961.html" class="cellMainLink">This Is Chillout MP3-DiVXTOTAL</a></div>
			</td>
			<td class="nobr center" data-sort="232838582">222.05 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T00:01:06+00:00">22 Nov 2015, 00:01:06</span></td>
			<td class="green center">135</td>
			<td class="red lasttd center">19</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11644353,0" class="icommentjs kaButton smallButton rightButton" href="/starcraft-2-legacy-of-the-void-2015-battle-rip-reloaded-t11644353.html#comment">23 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/starcraft-2-legacy-of-the-void-2015-battle-rip-reloaded-t11644353.html" class="cellMainLink">StarCraft 2: Legacy of the Void [2015] (Battle-Rip)- RELOADED</a></div>
			</td>
			<td class="nobr center" data-sort="25488672600">23.74 <span>GB</span></td>
			<td class="center">13</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T06:51:31+00:00">24 Nov 2015, 06:51:31</span></td>
			<td class="green center">985</td>
			<td class="red lasttd center">635</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11646297,0" class="icommentjs kaButton smallButton rightButton" href="/fallout-4-update-2-2015-pc-repack-by-r-g-freedom-t11646297.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/fallout-4-update-2-2015-pc-repack-by-r-g-freedom-t11646297.html" class="cellMainLink">Fallout 4 [Update 2] (2015) PC | RePack by R.G. Freedom</a></div>
			</td>
			<td class="nobr center" data-sort="20392777657">18.99 <span>GB</span></td>
			<td class="center">12</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T15:40:43+00:00">24 Nov 2015, 15:40:43</span></td>
			<td class="green center">677</td>
			<td class="red lasttd center">857</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11639271,0" class="icommentjs kaButton smallButton rightButton" href="/call-of-duty-black-ops-3-update-3-rus-2015-pc-repack-Ð¾f-xatab-t11639271.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/call-of-duty-black-ops-3-update-3-rus-2015-pc-repack-Ð¾f-xatab-t11639271.html" class="cellMainLink">Call of Duty: Black Ops 3 [Update 3][RUS] (2015) PC | Repack Ð¾f Xatab</a></div>
			</td>
			<td class="nobr center" data-sort="42312330667">39.41 <span>GB</span></td>
			<td class="center">14</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T10:11:03+00:00">23 Nov 2015, 10:11:03</span></td>
			<td class="green center">572</td>
			<td class="red lasttd center">762</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11637577,0" class="icommentjs kaButton smallButton rightButton" href="/game-of-thrones-a-telltale-game-series-complete-season-1-eps-1-6-fitgirl-repack-t11637577.html#comment">46 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/game-of-thrones-a-telltale-game-series-complete-season-1-eps-1-6-fitgirl-repack-t11637577.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/game-of-thrones-a-telltale-game-series-complete-season-1-eps-1-6-fitgirl-repack-t11637577.html" class="cellMainLink">Game of Thrones: A Telltale Game Series - Complete Season 1 (Eps. 1-6) [FitGirl Repack]</a></div>
			</td>
			<td class="nobr center" data-sort="5262897925">4.9 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T02:04:11+00:00">23 Nov 2015, 02:04:11</span></td>
			<td class="green center">212</td>
			<td class="red lasttd center">361</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11642157,0" class="icommentjs kaButton smallButton rightButton" href="/starcraft-ii-legacy-of-the-void-reloaded-t11642157.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/starcraft-ii-legacy-of-the-void-reloaded-t11642157.html" class="cellMainLink">StarCraft II Legacy of the Void-RELOADED</a></div>
			</td>
			<td class="nobr center" data-sort="20951470667">19.51 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T21:22:10+00:00">23 Nov 2015, 21:22:10</span></td>
			<td class="green center">114</td>
			<td class="red lasttd center">528</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11642216,0" class="icommentjs kaButton smallButton rightButton" href="/assassin-s-creed-syndicate-gold-edition-update-1-multi16-repack-r-g-mechanics-t11642216.html#comment">35 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/assassin-s-creed-syndicate-gold-edition-update-1-multi16-repack-r-g-mechanics-t11642216.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/assassin-s-creed-syndicate-gold-edition-update-1-multi16-repack-r-g-mechanics-t11642216.html" class="cellMainLink">Assassin&#039;s Creed Syndicate [Gold Edition] [Update 1] [multi16] Repack-R.G.Mechanics</a></div>
			</td>
			<td class="nobr center" data-sort="22364385521">20.83 <span>GB</span></td>
			<td class="center">20</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T21:36:56+00:00">23 Nov 2015, 21:36:56</span></td>
			<td class="green center">79</td>
			<td class="red lasttd center">323</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11647721,0" class="icommentjs kaButton smallButton rightButton" href="/minecraft-story-mode-episode-3-codex-t11647721.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/minecraft-story-mode-episode-3-codex-t11647721.html" class="cellMainLink">Minecraft Story Mode Episode 3-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="2091325165">1.95 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T18:08:17+00:00">24 Nov 2015, 18:08:17</span></td>
			<td class="green center">102</td>
			<td class="red lasttd center">130</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11636290,0" class="icommentjs kaButton smallButton rightButton" href="/12-is-better-than-6-t11636290.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/12-is-better-than-6-t11636290.html" class="cellMainLink">12 is Better Than 6</a></div>
			</td>
			<td class="nobr center" data-sort="179879815">171.55 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T19:00:34+00:00">22 Nov 2015, 19:00:34</span></td>
			<td class="green center">134</td>
			<td class="red lasttd center">28</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11631544,0" class="icommentjs kaButton smallButton rightButton" href="/switch-galaxy-ultra-skidrow-t11631544.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/switch-galaxy-ultra-skidrow-t11631544.html" class="cellMainLink">Switch.Galaxy.Ultra-SKIDROW</a></div>
			</td>
			<td class="nobr center" data-sort="1091056043">1.02 <span>GB</span></td>
			<td class="center">24</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T21:17:54+00:00">21 Nov 2015, 21:17:54</span></td>
			<td class="green center">90</td>
			<td class="red lasttd center">34</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11649759,0" class="icommentjs kaButton smallButton rightButton" href="/might-and-magic-heroes-vii-deluxe-edition-v-1-50-2015-pc-repack-by-r-g-catalyst-t11649759.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/might-and-magic-heroes-vii-deluxe-edition-v-1-50-2015-pc-repack-by-r-g-catalyst-t11649759.html" class="cellMainLink">Might and Magic Heroes VII: Deluxe Edition [v 1.50] (2015) PC | RePack by R.G. Catalyst</a></div>
			</td>
			<td class="nobr center" data-sort="9604326806">8.94 <span>GB</span></td>
			<td class="center">31</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T06:04:10+00:00">25 Nov 2015, 06:04:10</span></td>
			<td class="green center">65</td>
			<td class="red lasttd center">83</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11648354,0" class="icommentjs kaButton smallButton rightButton" href="/stronghold-crusader-2-incl-4-dlc-gog-t11648354.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/stronghold-crusader-2-incl-4-dlc-gog-t11648354.html" class="cellMainLink">Stronghold Crusader 2 (Incl 4 DLC) (GOG)</a></div>
			</td>
			<td class="nobr center" data-sort="3970691676">3.7 <span>GB</span></td>
			<td class="center">12</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T20:39:45+00:00">24 Nov 2015, 20:39:45</span></td>
			<td class="green center">39</td>
			<td class="red lasttd center">108</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11636278,0" class="icommentjs kaButton smallButton rightButton" href="/subnautica-build-2838-t11636278.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/subnautica-build-2838-t11636278.html" class="cellMainLink">Subnautica (Build 2838)</a></div>
			</td>
			<td class="nobr center" data-sort="3100247079">2.89 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T18:58:45+00:00">22 Nov 2015, 18:58:45</span></td>
			<td class="green center">75</td>
			<td class="red lasttd center">33</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11641869,0" class="icommentjs kaButton smallButton rightButton" href="/mordheim-city-of-the-damned-x86-x64-multi7-fitgirl-repack-selective-download-2-4-2-5-gb-t11641869.html#comment">20 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/mordheim-city-of-the-damned-x86-x64-multi7-fitgirl-repack-selective-download-2-4-2-5-gb-t11641869.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/mordheim-city-of-the-damned-x86-x64-multi7-fitgirl-repack-selective-download-2-4-2-5-gb-t11641869.html" class="cellMainLink">Mordheim: City of the Damned (x86/x64, MULTI7) [FitGirl Repack, Selective Download - 2.4/2.5 GB]</a></div>
			</td>
			<td class="nobr center" data-sort="2705461315">2.52 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T20:12:53+00:00">23 Nov 2015, 20:12:53</span></td>
			<td class="green center">37</td>
			<td class="red lasttd center">32</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11636283,0" class="icommentjs kaButton smallButton rightButton" href="/life-is-feudal-your-own-v1-0-0-6-t11636283.html#comment">15 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/life-is-feudal-your-own-v1-0-0-6-t11636283.html" class="cellMainLink">Life is Feudal: Your Own v1.0.0.6</a></div>
			</td>
			<td class="nobr center" data-sort="2370033418">2.21 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T18:59:56+00:00">22 Nov 2015, 18:59:56</span></td>
			<td class="green center">41</td>
			<td class="red lasttd center">20</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11631829,0" class="icommentjs kaButton smallButton rightButton" href="/assassin-creed-syndicate-v-1-12-update-1-24dlc-repack-by-freedom-t11631829.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/assassin-creed-syndicate-v-1-12-update-1-24dlc-repack-by-freedom-t11631829.html" class="cellMainLink">Assassin Creed Syndicate v.1.12 (Update 1)+24DLC RePack by Freedom</a></div>
			</td>
			<td class="nobr center" data-sort="25336090464">23.6 <span>GB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T23:02:01+00:00">21 Nov 2015, 23:02:01</span></td>
			<td class="green center">5</td>
			<td class="red lasttd center">84</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11637430,0" class="icommentjs kaButton smallButton rightButton" href="/teracopy-pro-3-0-alpha-5-key-4realtorrentz-t11637430.html#comment">30 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/teracopy-pro-3-0-alpha-5-key-4realtorrentz-t11637430.html" class="cellMainLink">TeraCopy Pro 3.0 alpha 5 + Key [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="4603309">4.39 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T01:10:00+00:00">23 Nov 2015, 01:10:00</span></td>
			<td class="green center">483</td>
			<td class="red lasttd center">36</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11645829,0" class="icommentjs kaButton smallButton rightButton" href="/winrar-5-30-final-incl-crack-techtools-t11645829.html#comment">24 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/winrar-5-30-final-incl-crack-techtools-t11645829.html" class="cellMainLink">WinRAR 5.30 FINAL Incl. Crack [TechTools]</a></div>
			</td>
			<td class="nobr center" data-sort="5577060">5.32 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T13:21:03+00:00">24 Nov 2015, 13:21:03</span></td>
			<td class="green center">466</td>
			<td class="red lasttd center">53</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11637390,0" class="icommentjs kaButton smallButton rightButton" href="/utorrent-pro-3-4-5-build-41372-stable-portable-4realtorrentz-t11637390.html#comment">16 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/utorrent-pro-3-4-5-build-41372-stable-portable-4realtorrentz-t11637390.html" class="cellMainLink">uTorrent Pro 3.4.5 Build 41372 Stable + Portable [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="6898540">6.58 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T00:41:59+00:00">23 Nov 2015, 00:41:59</span></td>
			<td class="green center">229</td>
			<td class="red lasttd center">28</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11634893,0" class="icommentjs kaButton smallButton rightButton" href="/windows-7-ultimate-sp1-x86-x64-en-us-oem-esd-nov2015-pre-activated-team-os-t11634893.html#comment">22 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-7-ultimate-sp1-x86-x64-en-us-oem-esd-nov2015-pre-activated-team-os-t11634893.html" class="cellMainLink">Windows 7 Ultimate Sp1 x86-x64 En-Us OEM ESD Nov2015 Pre-Activated=-{TEAM OS}=</a></div>
			</td>
			<td class="nobr center" data-sort="4186503357">3.9 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T12:50:41+00:00">22 Nov 2015, 12:50:41</span></td>
			<td class="green center">127</td>
			<td class="red lasttd center">205</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11637518,0" class="icommentjs kaButton smallButton rightButton" href="/foxit-phantompdf-business-7-2-5-x32-x64-multi-patch-t11637518.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/foxit-phantompdf-business-7-2-5-x32-x64-multi-patch-t11637518.html" class="cellMainLink">Foxit PhantomPDF Business 7.2.5 (x32/x64)[Multi][Patch]</a></div>
			</td>
			<td class="nobr center" data-sort="358233663">341.64 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T01:45:09+00:00">23 Nov 2015, 01:45:09</span></td>
			<td class="green center">184</td>
			<td class="red lasttd center">55</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11645977,0" class="icommentjs kaButton smallButton rightButton" href="/ccleaner-5-12-5431-2015-pc-portable-t11645977.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/ccleaner-5-12-5431-2015-pc-portable-t11645977.html" class="cellMainLink">CCleaner 5.12.5431 (2015) PC | + Portable</a></div>
			</td>
			<td class="nobr center" data-sort="24978001">23.82 <span>MB</span></td>
			<td class="center">60</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T14:11:03+00:00">24 Nov 2015, 14:11:03</span></td>
			<td class="green center">186</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11634121,0" class="icommentjs kaButton smallButton rightButton" href="/latest-dfx-audio-enhancer-12-013-crack-s0ft4pc-t11634121.html#comment">17 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/latest-dfx-audio-enhancer-12-013-crack-s0ft4pc-t11634121.html" class="cellMainLink">Latest DFX Audio Enhancer 12.013 + Crack [S0ft4PC]</a></div>
			</td>
			<td class="nobr center" data-sort="5034123">4.8 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T09:48:00+00:00">22 Nov 2015, 09:48:00</span></td>
			<td class="green center">170</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11648941,0" class="icommentjs kaButton smallButton rightButton" href="/iobit-advanced-systemcare-pro-9-0-3-1078-key-4realtorrentz-t11648941.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/iobit-advanced-systemcare-pro-9-0-3-1078-key-4realtorrentz-t11648941.html" class="cellMainLink">IObit advanced SystemCare Pro 9.0.3.1078 + Key [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="39547105">37.72 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T01:06:59+00:00">25 Nov 2015, 01:06:59</span></td>
			<td class="green center">138</td>
			<td class="red lasttd center">36</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11637414,0" class="icommentjs kaButton smallButton rightButton" href="/bittorrent-pro-7-9-5-build-41373-stable-portable-4realtorrentz-t11637414.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/bittorrent-pro-7-9-5-build-41373-stable-portable-4realtorrentz-t11637414.html" class="cellMainLink">BitTorrent Pro 7.9.5 build 41373 Stable + Portable [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="6751151">6.44 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T00:57:19+00:00">23 Nov 2015, 00:57:19</span></td>
			<td class="green center">145</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11649367,0" class="icommentjs kaButton smallButton rightButton" href="/daz3d-poser-selina-bundle-t11649367.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/daz3d-poser-selina-bundle-t11649367.html" class="cellMainLink">DAZ3D - Poser Selina Bundle</a></div>
			</td>
			<td class="nobr center" data-sort="379822311">362.23 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T04:05:18+00:00">25 Nov 2015, 04:05:18</span></td>
			<td class="green center">138</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11639412,0" class="icommentjs kaButton smallButton rightButton" href="/adwcleaner-5-022-2015-pc-portable-t11639412.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/adwcleaner-5-022-2015-pc-portable-t11639412.html" class="cellMainLink">AdwCleaner 5.022 (2015) PC | Portable</a></div>
			</td>
			<td class="nobr center" data-sort="1733632">1.65 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T10:57:10+00:00">23 Nov 2015, 10:57:10</span></td>
			<td class="green center">78</td>
			<td class="red lasttd center">2</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/winrar-v5-30-final-x86x64-incl-key-4realtorrentz-t11649994.html" class="cellMainLink">WinRAR.v5.30 Final (x86x64) Incl Key [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="3534552">3.37 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T06:57:35+00:00">25 Nov 2015, 06:57:35</span></td>
			<td class="green center">66</td>
			<td class="red lasttd center">20</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11648387,0" class="icommentjs kaButton smallButton rightButton" href="/daz3d-the-hero-and-the-damsel-poses-g2m-g2f-t11648387.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/daz3d-the-hero-and-the-damsel-poses-g2m-g2f-t11648387.html" class="cellMainLink">Daz3D - The Hero and the Damsel Poses (G2M, G2F)</a></div>
			</td>
			<td class="nobr center" data-sort="2088843">1.99 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T20:47:39+00:00">24 Nov 2015, 20:47:39</span></td>
			<td class="green center">72</td>
			<td class="red lasttd center">3</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11646205,0" class="icommentjs kaButton smallButton rightButton" href="/poser-daz3d-city-unseelie-for-g2f-19063-t11646205.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/poser-daz3d-city-unseelie-for-g2f-19063-t11646205.html" class="cellMainLink">Poser - Daz3D - City Unseelie for G2F 19063</a></div>
			</td>
			<td class="nobr center" data-sort="133955525">127.75 <span>MB</span></td>
			<td class="center">12</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T15:16:10+00:00">24 Nov 2015, 15:16:10</span></td>
			<td class="green center">51</td>
			<td class="red lasttd center">4</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11645041,0" class="icommentjs kaButton smallButton rightButton" href="/drive-snapshot-1-43-0-17827-portable-key-4realtorrentz-t11645041.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/drive-snapshot-1-43-0-17827-portable-key-4realtorrentz-t11645041.html" class="cellMainLink">Drive SnapShot 1.43.0.17827 + Portable + Key [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="4859089">4.63 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T10:19:11+00:00">24 Nov 2015, 10:19:11</span></td>
			<td class="green center">44</td>
			<td class="red lasttd center">5</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11636490,0" class="icommentjs kaButton smallButton rightButton" href="/animerg-one-punch-man-08-720p-mkv-t11636490.html#comment">24 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-one-punch-man-08-720p-mkv-t11636490.html" class="cellMainLink">[AnimeRG] One Punch Man - 08 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="347575535">331.47 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T20:01:30+00:00">22 Nov 2015, 20:01:30</span></td>
			<td class="green center">1540</td>
			<td class="red lasttd center">194</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11634818,0" class="icommentjs kaButton smallButton rightButton" href="/arrg-dragon-ball-super-20-english-subbed-720p-sehjada-t11634818.html#comment">23 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/arrg-dragon-ball-super-20-english-subbed-720p-sehjada-t11634818.html" class="cellMainLink">[ARRG]Dragon Ball Super - 20 English Subbed [720P] (Sehjada)</a></div>
			</td>
			<td class="nobr center" data-sort="135587643">129.31 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T12:29:30+00:00">22 Nov 2015, 12:29:30</span></td>
			<td class="green center">688</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/kaerizaki-fansub-one-piece-719-vostfr-hd-1280x720-mp4-t11635048.html" class="cellMainLink">[Kaerizaki-Fansub] One Piece 719 [VOSTFR][HD 1280x720].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="294342292">280.71 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T13:34:40+00:00">22 Nov 2015, 13:34:40</span></td>
			<td class="green center">646</td>
			<td class="red lasttd center">52</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-gintama-299-720p-mkv-t11651058.html" class="cellMainLink">[HorribleSubs] Gintama - 299 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="342313394">326.46 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T11:40:02+00:00">25 Nov 2015, 11:40:02</span></td>
			<td class="green center">352</td>
			<td class="red lasttd center">202</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11636201,0" class="icommentjs kaButton smallButton rightButton" href="/dragon-ball-super-episode-020-english-subbed-720p-arizone-t11636201.html#comment">13 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dragon-ball-super-episode-020-english-subbed-720p-arizone-t11636201.html" class="cellMainLink">DRAGON BALL SUPER Episode - 020 [ENGLISH SUBBED] 720p ~ARIZONE</a></div>
			</td>
			<td class="nobr center" data-sort="137456355">131.09 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T18:34:03+00:00">22 Nov 2015, 18:34:03</span></td>
			<td class="green center">194</td>
			<td class="red lasttd center">33</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11638037,0" class="icommentjs kaButton smallButton rightButton" href="/one-punch-man-08-480p-engsub-iorchid-mkv-t11638037.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/one-punch-man-08-480p-engsub-iorchid-mkv-t11638037.html" class="cellMainLink">One-Punch Man - 08 [480p][EngSub][iORcHiD].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="102450760">97.7 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T04:23:13+00:00">23 Nov 2015, 04:23:13</span></td>
			<td class="green center">93</td>
			<td class="red lasttd center">30</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-jk-meshi-08-raw-mx-1280x720-x264-aac-mp4-t11640527.html" class="cellMainLink">[Leopard-Raws] JK Meshi! - 08 RAW (MX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="34248251">32.66 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T16:45:02+00:00">23 Nov 2015, 16:45:02</span></td>
			<td class="green center">83</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11632812,0" class="icommentjs kaButton smallButton rightButton" href="/one-piece-719-480p-engsub-iorchid-mkv-t11632812.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/one-piece-719-480p-engsub-iorchid-mkv-t11632812.html" class="cellMainLink">One Piece - 719 [480p][EngSub][iORcHiD].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="99323234">94.72 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T04:54:04+00:00">22 Nov 2015, 04:54:04</span></td>
			<td class="green center">71</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11645490,0" class="icommentjs kaButton smallButton rightButton" href="/shepardtds-k-project-dual-audio-720p-8-bit-x265-hevc-1-13-complete-t11645490.html#comment">4 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/shepardtds-k-project-dual-audio-720p-8-bit-x265-hevc-1-13-complete-t11645490.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/shepardtds-k-project-dual-audio-720p-8-bit-x265-hevc-1-13-complete-t11645490.html" class="cellMainLink">[ShepardTDS] K-Project Dual Audio [720p 8-bit x265 HEVC] 1-13 Complete</a></div>
			</td>
			<td class="nobr center" data-sort="3424230629">3.19 <span>GB</span></td>
			<td class="center">16</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T12:02:46+00:00">24 Nov 2015, 12:02:46</span></td>
			<td class="green center">37</td>
			<td class="red lasttd center">31</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11646486,0" class="icommentjs kaButton smallButton rightButton" href="/ipunisher-monster-musume-no-iru-nichijou-vol-3-bd-1280x720-h264-10bit-aac-uncensored-t11646486.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ipunisher-monster-musume-no-iru-nichijou-vol-3-bd-1280x720-h264-10bit-aac-uncensored-t11646486.html" class="cellMainLink">[iPUNISHER] Monster Musume no Iru Nichijou Vol.3 (BD 1280x720 h264 10bit AAC UNCENSORED)</a></div>
			</td>
			<td class="nobr center" data-sort="615898532">587.37 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T16:29:15+00:00">24 Nov 2015, 16:29:15</span></td>
			<td class="green center">42</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11636166,0" class="icommentjs kaButton smallButton rightButton" href="/ohys-raws-one-punch-man-08-tx-1280x720-x264-aac-mp4-t11636166.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ohys-raws-one-punch-man-08-tx-1280x720-x264-aac-mp4-t11636166.html" class="cellMainLink">[Ohys-Raws] One-Punch Man - 08 (TX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="525585043">501.24 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T18:25:17+00:00">22 Nov 2015, 18:25:17</span></td>
			<td class="green center">47</td>
			<td class="red lasttd center">2</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11636051,0" class="icommentjs kaButton smallButton rightButton" href="/arrg-one-punch-man-08-english-subbed-720p-sehjada-t11636051.html#comment">13 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/arrg-one-punch-man-08-english-subbed-720p-sehjada-t11636051.html" class="cellMainLink">[ARRG] One Punch Man - 08 English Subbed 720P (Sehjada)</a></div>
			</td>
			<td class="nobr center" data-sort="164193379">156.59 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T17:49:40+00:00">22 Nov 2015, 17:49:40</span></td>
			<td class="green center">46</td>
			<td class="red lasttd center">2</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11643280,0" class="icommentjs kaButton smallButton rightButton" href="/deadfish-dragon-ball-super-20-720p-aac-mp4-t11643280.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/deadfish-dragon-ball-super-20-720p-aac-mp4-t11643280.html" class="cellMainLink">[DeadFish] Dragon Ball Super - 20 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="379923873">362.32 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T00:48:11+00:00">24 Nov 2015, 00:48:11</span></td>
			<td class="green center">41</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11635037,0" class="icommentjs kaButton smallButton rightButton" href="/j-x-wiki-the-next-generation-patlabor-tokyo-war-2015-1080p-bluray-x264-10bit-mkv-t11635037.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/j-x-wiki-the-next-generation-patlabor-tokyo-war-2015-1080p-bluray-x264-10bit-mkv-t11635037.html" class="cellMainLink">[J X&amp;WiKi]The Next Generation Patlabor Tokyo War 2015 1080p BluRay x264 10bit mkv</a></div>
			</td>
			<td class="nobr center" data-sort="2197872005">2.05 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T13:31:31+00:00">22 Nov 2015, 13:31:31</span></td>
			<td class="green center">30</td>
			<td class="red lasttd center">28</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ohys-raws-shingeki-kyojin-chuugakkou-08-mbs-1280x720-x264-aac-mp4-t11634750.html" class="cellMainLink">[Ohys-Raws] Shingeki! Kyojin Chuugakkou - 08 (MBS 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="296071822">282.36 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T12:13:19+00:00">22 Nov 2015, 12:13:19</span></td>
			<td class="green center">39</td>
			<td class="red lasttd center">1</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11631483,0" class="icommentjs kaButton smallButton rightButton" href="/dslr-photography-for-beginners-take-10-times-better-pictures-in-48-hours-or-less-best-way-to-learn-digital-photography-master-your-dslr-camera-improve-your-digital-slr-photography-skills-epub-mobi-ertb-t11631483.html#comment">13 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/dslr-photography-for-beginners-take-10-times-better-pictures-in-48-hours-or-less-best-way-to-learn-digital-photography-master-your-dslr-camera-improve-your-digital-slr-photography-skills-epub-mobi-ertb-t11631483.html" class="cellMainLink">DSLR Photography for Beginners - Take 10 Times Better Pictures in 48 Hours or Less! Best Way to Learn Digital Photography, Master Your DSLR Camera &amp; Improve Your Digital SLR Photography Skills [ePUB + MOBI] {{ERTB}}</a></div>
			</td>
			<td class="nobr center" data-sort="14906938">14.22 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T20:58:43+00:00">21 Nov 2015, 20:58:43</span></td>
			<td class="green center">708</td>
			<td class="red lasttd center">81</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11633173,0" class="icommentjs kaButton smallButton rightButton" href="/assorted-magazines-bundle-november-22-2015-true-pdf-t11633173.html#comment">24 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/assorted-magazines-bundle-november-22-2015-true-pdf-t11633173.html" class="cellMainLink">Assorted Magazines Bundle - November 22 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="575774361">549.1 <span>MB</span></td>
			<td class="center">50</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T06:07:13+00:00">22 Nov 2015, 06:07:13</span></td>
			<td class="green center">533</td>
			<td class="red lasttd center">313</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11631546,0" class="icommentjs kaButton smallButton rightButton" href="/new-york-times-bestsellers-december-06-2015-fiction-non-fiction-ertb-t11631546.html#comment">59 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/new-york-times-bestsellers-december-06-2015-fiction-non-fiction-ertb-t11631546.html" class="cellMainLink">New York Times Bestsellers (December 06, 2015) (Fiction + Non Fiction) {{ERTB}}</a></div>
			</td>
			<td class="nobr center" data-sort="270985182">258.43 <span>MB</span></td>
			<td class="center">31</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T21:19:03+00:00">21 Nov 2015, 21:19:03</span></td>
			<td class="green center">446</td>
			<td class="red lasttd center">114</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11636033,0" class="icommentjs kaButton smallButton rightButton" href="/english-sentence-structure-intensive-course-in-english-by-robert-krohn-pdf-zeke23-t11636033.html#comment">20 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/english-sentence-structure-intensive-course-in-english-by-robert-krohn-pdf-zeke23-t11636033.html" class="cellMainLink">English Sentence Structure (Intensive Course in English) by Robert Krohn - pdf - zeke23</a></div>
			</td>
			<td class="nobr center" data-sort="25837136">24.64 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T17:45:32+00:00">22 Nov 2015, 17:45:32</span></td>
			<td class="green center">394</td>
			<td class="red lasttd center">27</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11641281,0" class="icommentjs kaButton smallButton rightButton" href="/the-telecommunications-handbook-engineering-guidelines-for-fixed-mobile-and-satellite-systems-1st-edition-2015-pdf-gooner-t11641281.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-telecommunications-handbook-engineering-guidelines-for-fixed-mobile-and-satellite-systems-1st-edition-2015-pdf-gooner-t11641281.html" class="cellMainLink">The Telecommunications Handbook - Engineering Guidelines for Fixed, Mobile and Satellite Systems - 1st Edition (2015).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="14366502">13.7 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T18:02:41+00:00">23 Nov 2015, 18:02:41</span></td>
			<td class="green center">344</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11631131,0" class="icommentjs kaButton smallButton rightButton" href="/the-real-life-mba-your-no-bs-guide-to-winning-the-game-building-a-team-and-growing-your-career-2015-epub-gooner-t11631131.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-real-life-mba-your-no-bs-guide-to-winning-the-game-building-a-team-and-growing-your-career-2015-epub-gooner-t11631131.html" class="cellMainLink">The Real-Life MBA - Your No-BS Guide to Winning the Game, Building a Team and Growing Your Career (2015).epub Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="131017281">124.95 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T19:05:37+00:00">21 Nov 2015, 19:05:37</span></td>
			<td class="green center">279</td>
			<td class="red lasttd center">30</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11651567,0" class="icommentjs kaButton smallButton rightButton" href="/dark-knight-iii-the-master-race-001-2016-digital-zone-empire-cbr-nem-t11651567.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/dark-knight-iii-the-master-race-001-2016-digital-zone-empire-cbr-nem-t11651567.html" class="cellMainLink">Dark Knight III - The Master Race 001 (2016) (Digital) (Zone-Empire).cbr (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="64731515">61.73 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T14:13:53+00:00">25 Nov 2015, 14:13:53</span></td>
			<td class="green center">203</td>
			<td class="red lasttd center">151</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11632423,0" class="icommentjs kaButton smallButton rightButton" href="/ultimate-marvel-universe-2000-2015-digital-empire-minutemen-nem-t11632423.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/ultimate-marvel-universe-2000-2015-digital-empire-minutemen-nem-t11632423.html" class="cellMainLink">Ultimate Marvel Universe (2000-2015) (digital) (Empire+Minutemen) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="36118614213">33.64 <span>GB</span></td>
			<td class="center">1095</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T02:53:29+00:00">22 Nov 2015, 02:53:29</span></td>
			<td class="green center">51</td>
			<td class="red lasttd center">444</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11636871,0" class="icommentjs kaButton smallButton rightButton" href="/womens-magazines-bundle-november-23-2015-true-pdf-t11636871.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/womens-magazines-bundle-november-23-2015-true-pdf-t11636871.html" class="cellMainLink">Womens Magazines Bundle - November 23 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="453807715">432.78 <span>MB</span></td>
			<td class="center">27</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T21:54:18+00:00">22 Nov 2015, 21:54:18</span></td>
			<td class="green center">176</td>
			<td class="red lasttd center">107</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11633371,0" class="icommentjs kaButton smallButton rightButton" href="/motorcycle-magazines-november-22-2015-true-pdf-t11633371.html#comment">12 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/motorcycle-magazines-november-22-2015-true-pdf-t11633371.html" class="cellMainLink">Motorcycle Magazines - November 22 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="94599713">90.22 <span>MB</span></td>
			<td class="center">8</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T06:50:47+00:00">22 Nov 2015, 06:50:47</span></td>
			<td class="green center">207</td>
			<td class="red lasttd center">42</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11645165,0" class="icommentjs kaButton smallButton rightButton" href="/it-s-not-about-the-shark-how-to-solve-unsolvable-problems-pdf-mobi-t11645165.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/it-s-not-about-the-shark-how-to-solve-unsolvable-problems-pdf-mobi-t11645165.html" class="cellMainLink">It&#039;s Not About the Shark How to Solve Unsolvable Problems (PDF, MOBI)</a></div>
			</td>
			<td class="nobr center" data-sort="1298234">1.24 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T10:41:16+00:00">24 Nov 2015, 10:41:16</span></td>
			<td class="green center">164</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11649810,0" class="icommentjs kaButton smallButton rightButton" href="/0-day-week-of-2015-11-18-t11649810.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/0-day-week-of-2015-11-18-t11649810.html" class="cellMainLink">0-Day Week of 2015.11.18</a></div>
			</td>
			<td class="nobr center" data-sort="7224598430">6.73 <span>GB</span></td>
			<td class="center">149</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T06:15:12+00:00">25 Nov 2015, 06:15:12</span></td>
			<td class="green center">77</td>
			<td class="red lasttd center">194</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11650472,0" class="icommentjs kaButton smallButton rightButton" href="/superman-wonder-woman-023-2016-2-covers-digital-cypher-2-0-empire-cbr-nem-t11650472.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/superman-wonder-woman-023-2016-2-covers-digital-cypher-2-0-empire-cbr-nem-t11650472.html" class="cellMainLink">Superman-Wonder Woman 023 (2016) (2 covers) (Digital) (Cypher 2.0-Empire).cbr (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="53073745">50.62 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T09:03:28+00:00">25 Nov 2015, 09:03:28</span></td>
			<td class="green center">157</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11647889,0" class="icommentjs kaButton smallButton rightButton" href="/earth-and-space-photographs-from-the-archives-of-nasa-pdf-ertb-t11647889.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/earth-and-space-photographs-from-the-archives-of-nasa-pdf-ertb-t11647889.html" class="cellMainLink">Earth and Space - Photographs from the Archives of NASA [PDF] {{ERTB}}</a></div>
			</td>
			<td class="nobr center" data-sort="371602764">354.39 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T18:45:27+00:00">24 Nov 2015, 18:45:27</span></td>
			<td class="green center">131</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11651472,0" class="icommentjs kaButton smallButton rightButton" href="/robin-son-of-batman-006-2016-digital-oroboros-dcp-cbr-nem-t11651472.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/robin-son-of-batman-006-2016-digital-oroboros-dcp-cbr-nem-t11651472.html" class="cellMainLink">Robin - Son of Batman 006 (2016) (digital) (Oroboros-DCP).cbr (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="35886155">34.22 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-25T13:40:39+00:00">25 Nov 2015, 13:40:39</span></td>
			<td class="green center">108</td>
			<td class="red lasttd center">38</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11648781,0" class="icommentjs kaButton smallButton rightButton" href="/adele-25-2015-24-96-hd-flac-t11648781.html#comment">20 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/adele-25-2015-24-96-hd-flac-t11648781.html" class="cellMainLink">Adele - 25 (2015) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1011757897">964.89 <span>MB</span></td>
			<td class="center">12</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T23:49:43+00:00">24 Nov 2015, 23:49:43</span></td>
			<td class="green center">998</td>
			<td class="red lasttd center">794</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11631235,0" class="icommentjs kaButton smallButton rightButton" href="/va-the-greatest-easy-listening-2-cd-2004-flac-tfm-t11631235.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-the-greatest-easy-listening-2-cd-2004-flac-tfm-t11631235.html" class="cellMainLink">VA - The Greatest Easy Listening - 2-CD - (2004) - [FLAC] - [TFM]</a></div>
			</td>
			<td class="nobr center" data-sort="575486337">548.83 <span>MB</span></td>
			<td class="center">46</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T19:31:31+00:00">21 Nov 2015, 19:31:31</span></td>
			<td class="green center">274</td>
			<td class="red lasttd center">74</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11640435,0" class="icommentjs kaButton smallButton rightButton" href="/top-100-90-s-rock-albums-by-ultimateclassicrock-cd-s-26-50-flac-t11640435.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/top-100-90-s-rock-albums-by-ultimateclassicrock-cd-s-26-50-flac-t11640435.html" class="cellMainLink">Top 100 &#039;90&#039;s Rock Albums by UltimateClassicRock - CD&#039;s 26-50 [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="9829352483">9.15 <span>GB</span></td>
			<td class="center">669</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T16:15:18+00:00">23 Nov 2015, 16:15:18</span></td>
			<td class="green center">158</td>
			<td class="red lasttd center">296</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11632261,0" class="icommentjs kaButton smallButton rightButton" href="/enya-studio-discography-1987-2015-flac-pirate-shovon-t11632261.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/enya-studio-discography-1987-2015-flac-pirate-shovon-t11632261.html" class="cellMainLink">Enya - Studio Discography [1987 - 2015] [FLAC] [Pirate Shovon]</a></div>
			</td>
			<td class="nobr center" data-sort="2497053676">2.33 <span>GB</span></td>
			<td class="center">138</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T01:50:11+00:00">22 Nov 2015, 01:50:11</span></td>
			<td class="green center">220</td>
			<td class="red lasttd center">139</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11641500,0" class="icommentjs kaButton smallButton rightButton" href="/frank-sinatra-a-jolly-christmas-from-frank-sinatra-2015-24-96-hd-flac-t11641500.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/frank-sinatra-a-jolly-christmas-from-frank-sinatra-2015-24-96-hd-flac-t11641500.html" class="cellMainLink">Frank Sinatra - A Jolly Christmas From Frank Sinatra (2015) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="369570344">352.45 <span>MB</span></td>
			<td class="center">31</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T18:43:06+00:00">23 Nov 2015, 18:43:06</span></td>
			<td class="green center">258</td>
			<td class="red lasttd center">45</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11642139,0" class="icommentjs kaButton smallButton rightButton" href="/the-who-tommy-super-deluxe-2014-24-96-hd-flac-t11642139.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-who-tommy-super-deluxe-2014-24-96-hd-flac-t11642139.html" class="cellMainLink">The Who - Tommy (Super Deluxe 2014) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="5615599402">5.23 <span>GB</span></td>
			<td class="center">182</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-23T21:15:27+00:00">23 Nov 2015, 21:15:27</span></td>
			<td class="green center">150</td>
			<td class="red lasttd center">105</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11643509,0" class="icommentjs kaButton smallButton rightButton" href="/va-classical-voices-the-musicals-3cd-2015-flac-t11643509.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-classical-voices-the-musicals-3cd-2015-flac-t11643509.html" class="cellMainLink">VA - Classical Voices: The Musicals [3CD] (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="901603680">859.84 <span>MB</span></td>
			<td class="center">65</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T02:16:37+00:00">24 Nov 2015, 02:16:37</span></td>
			<td class="green center">119</td>
			<td class="red lasttd center">35</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11632963,0" class="icommentjs kaButton smallButton rightButton" href="/bob-dylan-the-cutting-edge-1965-1966-the-bootleg-series-vol-12-collector-s-edition-24bit-96khz-pirate-shovon-t11632963.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bob-dylan-the-cutting-edge-1965-1966-the-bootleg-series-vol-12-collector-s-edition-24bit-96khz-pirate-shovon-t11632963.html" class="cellMainLink">Bob Dylan - The Cutting Edge 1965-1966 The Bootleg Series Vol. 12 [Collector&#039;s Edition] [24Bit 96KHz] [Pirate Shovon]</a></div>
			</td>
			<td class="nobr center" data-sort="23395922545">21.79 <span>GB</span></td>
			<td class="center">434</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T05:37:25+00:00">22 Nov 2015, 05:37:25</span></td>
			<td class="green center">69</td>
			<td class="red lasttd center">129</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11634800,0" class="icommentjs kaButton smallButton rightButton" href="/queen-a-night-at-the-odeon-live-at-the-hammersmith-flac-96-0-khz-24-bit-pirate-shovon-t11634800.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/queen-a-night-at-the-odeon-live-at-the-hammersmith-flac-96-0-khz-24-bit-pirate-shovon-t11634800.html" class="cellMainLink">Queen - A Night at the Odeon [Live At The Hammersmith] [FLAC 96.0 KHz 24-bit] [Pirate Shovon]</a></div>
			</td>
			<td class="nobr center" data-sort="1595548812">1.49 <span>GB</span></td>
			<td class="center">20</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-22T12:25:00+00:00">22 Nov 2015, 12:25:00</span></td>
			<td class="green center">95</td>
			<td class="red lasttd center">39</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11643668,0" class="icommentjs kaButton smallButton rightButton" href="/anastacia-ultimate-collection-2015-flac-t11643668.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/anastacia-ultimate-collection-2015-flac-t11643668.html" class="cellMainLink">Anastacia - Ultimate Collection (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="547840058">522.46 <span>MB</span></td>
			<td class="center">26</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T03:16:50+00:00">24 Nov 2015, 03:16:50</span></td>
			<td class="green center">99</td>
			<td class="red lasttd center">31</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11631664,0" class="icommentjs kaButton smallButton rightButton" href="/various-artists-deuses-da-guitarra-flac-tnt-t11631664.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/various-artists-deuses-da-guitarra-flac-tnt-t11631664.html" class="cellMainLink">Various Artists - Deuses da Guitarra [Flac][TNT]</a></div>
			</td>
			<td class="nobr center" data-sort="375590284">358.19 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-21T22:02:40+00:00">21 Nov 2015, 22:02:40</span></td>
			<td class="green center">100</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11643334,0" class="icommentjs kaButton smallButton rightButton" href="/uriah-heep-totally-driven-2015-flac-t11643334.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/uriah-heep-totally-driven-2015-flac-t11643334.html" class="cellMainLink">Uriah Heep - Totally Driven (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="955168839">910.92 <span>MB</span></td>
			<td class="center">32</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T01:11:18+00:00">24 Nov 2015, 01:11:18</span></td>
			<td class="green center">86</td>
			<td class="red lasttd center">36</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11648059,0" class="icommentjs kaButton smallButton rightButton" href="/the-velvet-underground-the-complete-matrix-tapes-2015-flac-beolab1700-t11648059.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-velvet-underground-the-complete-matrix-tapes-2015-flac-beolab1700-t11648059.html" class="cellMainLink">The Velvet Underground - The Complete Matrix Tapes (2015) FLAC Beolab1700</a></div>
			</td>
			<td class="nobr center" data-sort="1562194208">1.45 <span>GB</span></td>
			<td class="center">45</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T19:14:38+00:00">24 Nov 2015, 19:14:38</span></td>
			<td class="green center">64</td>
			<td class="red lasttd center">42</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-who-live-in-hyde-park-2cd-2015-flac-t11647827.html" class="cellMainLink">The Who - Live in Hyde Park [2CD] (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="865994922">825.88 <span>MB</span></td>
			<td class="center">43</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T18:30:25+00:00">24 Nov 2015, 18:30:25</span></td>
			<td class="green center">70</td>
			<td class="red lasttd center">28</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bobby-vinton-a-very-merry-christmas-1995-cd-flac-urbin4hd-t11644060.html" class="cellMainLink">BOBBY VINTON A Very Merry Christmas 1995 CD-FLAC URBiN4HD</a></div>
			</td>
			<td class="nobr center" data-sort="188267808">179.55 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-11-24T05:20:23+00:00">24 Nov 2015, 05:20:23</span></td>
			<td class="green center">69</td>
			<td class="red lasttd center">22</td>
        </tr>
			</table>


	<script type="text/javascript">
		if ($.cookie('kat_settings[fpw_column]') != null && $.cookie('kat_settings[fpw_order]')) {
			var colIndex = $.cookie('kat_settings[fpw_column]')
			var ascend = $.cookie('kat_settings[fpw_order]');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			sortTables(colIndex, ascend);
		}
		$('.frontPageWidget .firstr th .fpw-sort').click(function() {
			var colIndex = $(this).parent().index();
			var ascend = !$(this).hasClass('sortedAsc')?1:0;
			$('.fpw-sort').removeClass('sortedAsc sortedDesc');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			updateFPWsettings(colIndex, ascend);
			sortTables(colIndex, ascend);
		});
		function sortTables(colIndex, ascend) {	
			$('.frontPageWidget').each(function() {
				var data = [];
				$('tr:not(.firstr)', $(this)).each(function() {
					var name = $('td:first .cellMainLink', $(this)).text();
					var id = $('td:first .cellMainLink', $(this)).attr('href').match(/.*-t(\d+)\.html/)[1];
					var fileSize = $('td:eq(1)', $(this)).attr('data-sort');
					data.push({
						'col0':name, 'col1':padNum(fileSize), 'col2':padNum($('td:eq(2)',$(this)).text()), 'col3':padNum(id), 'col4':padNum($('td:eq(4)',$(this)).text()), 'col5':padNum($('td:eq(5)',$(this)).text()), 'html':$(this).html()
					});
				});
				data.sort(function(a, b) {
					var x = a['col'+colIndex].toLowerCase(); var y = b['col'+colIndex].toLowerCase();
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
				if (!ascend||ascend=='0') data.reverse();
				$(this).find('tr:not(.firstr)').remove();
				for (var i=0;i<data.length;i++) {
					$(this).append('<tr class="'+((i+1)%2==0?'even':'odd')+'">'+data[i].html+'</tr>');
				}
			});
		}
		function padNum(val) {
			val = '0000000000000000'+val;
			return val.substring(val.length-15);
		}
		function updateFPWsettings(t, o) {
			$.cookie('kat_settings[fpw_column]', t, { expires: 365, path: '/' });
			$.cookie('kat_settings[fpw_order]', o, { expires: 365, path: '/' });
		}
	</script>
		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
    
            
        <span  data-sc-slot="_119b0a17fab5493361a252d04bf527db"></span>
    
                
    	    <div class="spareBlock">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <span  data-sc-slot="_7063408f1c01d50e0dc2d833186ce962" data-sc-params="{ 'searchQuery': '' }"></span>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/official-software-request-thread/?unread=17137854">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Official Software Request Thread
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/ceyon/">ceyon</a></span></span> <time class="timeago" datetime="2015-11-25T17:55:50+00:00">25 Nov 2015, 17:55</time></span>
	</li>
		<li>
		<a href="/community/show/hello-it-true/?unread=17137851">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Hello, is it true that....
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_verified"><a class="plain" href="/user/XpoZ.Torrent/">XpoZ.Torrent</a></span></span> <time class="timeago" datetime="2015-11-25T17:54:29+00:00">25 Nov 2015, 17:54</time></span>
	</li>
		<li>
		<a href="/community/show/seen-any-good-foreign-language-movies/?unread=17137847">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Seen Any Good Foreign Language Movies?
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/Maurice312/">Maurice312</a></span></span> <time class="timeago" datetime="2015-11-25T17:54:03+00:00">25 Nov 2015, 17:54</time></span>
	</li>
		<li>
		<a href="/community/show/karaoke-korner/?unread=17137839">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Karaoke Korner
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_eliteuploader"><a class="plain" href="/user/BJthe1DJ/">BJthe1DJ</a></span></span> <time class="timeago" datetime="2015-11-25T17:51:58+00:00">25 Nov 2015, 17:51</time></span>
	</li>
		<li>
		<a href="/community/show/what-are-you-listening-right-now-v5-thread-116026/?unread=17137829">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What are you listening to right now? V5
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/nobody_here/">nobody_here</a></span></span> <time class="timeago" datetime="2015-11-25T17:49:04+00:00">25 Nov 2015, 17:49</time></span>
	</li>
		<li>
		<a href="/community/show/need-help-ask-super-users-experienced-site-members-here-v7/?unread=17137825">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Need Help? Ask Super Users &amp; Experienced Site Members Here v7
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/Swegin8r/">Swegin8r</a></span></span> <time class="timeago" datetime="2015-11-25T17:46:26+00:00">25 Nov 2015, 17:46</time></span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/new-site-rules/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				New site Rules
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-15T14:18:43+00:00">15 Oct 2015, 14:18</time></span>
	</li>
	<li>
		<a href="/blog/post/look-mama-i-m-popular/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Look, mama, I&#039;m popular!
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-05T17:42:40+00:00">05 Oct 2015, 17:42</time></span>
	</li>
	<li>
		<a href="/blog/post/summer-updates-september-1/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Summer updates [September, 1]
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-09-01T16:13:36+00:00">01 Sep 2015, 16:13</time></span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/ElBrado./post/http-bodybuildingbullshitzudotcomdotcom-com/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> http://bodybuildingbullshitzudotcomdotcom.com/</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/ElBrado./">ElBrado.</a> <time class="timeago" datetime="2015-11-25T15:38:38+00:00">25 Nov 2015, 15:38</time></span></li>
	<li><a href="/blog/SushiKushi/post/sushikushi-update-new-projects-break-time/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> SushiKushi Update: New Projects &amp; Break Time</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/SushiKushi/">SushiKushi</a> <time class="timeago" datetime="2015-11-24T02:20:42+00:00">24 Nov 2015, 02:20</time></span></li>
	<li><a href="/blog/ElBrado./post/untitled-blog/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Untitled blog</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/ElBrado./">ElBrado.</a> <time class="timeago" datetime="2015-11-23T11:01:27+00:00">23 Nov 2015, 11:01</time></span></li>
	<li><a href="/blog/OptimusPr1me/post/one-second/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> One Second..</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/OptimusPr1me/">OptimusPr1me</a> <time class="timeago" datetime="2015-11-23T10:51:28+00:00">23 Nov 2015, 10:51</time></span></li>
	<li><a href="/blog/olderthangod/post/getting-ripped-off/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Getting ripped off..</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/olderthangod/">olderthangod</a> <time class="timeago" datetime="2015-11-22T23:16:46+00:00">22 Nov 2015, 23:16</time></span></li>
	<li><a href="/blog/TheDels/post/youtube-is-finally-taking-a-stand-against-bs-copyright-strikes/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Youtube is finally taking a stand against BS Copyright Strikes</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/TheDels/">TheDels</a> <time class="timeago" datetime="2015-11-22T12:55:54+00:00">22 Nov 2015, 12:55</time></span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/yui%20hatano%20uncensor/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				yui hatano uncensor
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/brain%20games%20s04e08/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				brain games s04e08
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/stupid%20porn/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				stupid porn
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/enemy/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				enemy
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/scarlett%20monroe/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Scarlett Monroe
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/ariella%20ferrera/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Ariella Ferrera
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/lafranceapoil/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				lafranceapoil
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/incest.net/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				incest.net
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/drf/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				drf
			</p>
		</a>
				<span class="explanation">1&nbsp;sec.&nbsp;ago</span>
	</li>

	<li>
		<a href="/search/korean%20drama/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				korean drama
			</p>
		</a>
				<span class="explanation">1&nbsp;sec.&nbsp;ago</span>
	</li>

	<li>
		<a href="/search/dev%20d%20720p/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				dev d 720p
			</p>
		</a>
				<span class="explanation">1&nbsp;sec.&nbsp;ago</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a href="#" onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a href="#" onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a href="#" onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a href="#" onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a href="#" onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a href="#" onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a href="#" onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a href="#" onclick="setLanguage('bsc', '.kat.cr');return false;" class="plain">Bosnian-Cyrillic</a></li>
                                <li><a href="#" onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a href="#" onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a href="#" onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a href="#" onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a href="#" onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a href="#" onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a href="#" onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a href="#" onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a href="#" onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a href="#" onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a href="#" onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a href="#" onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a href="#" onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a href="#" onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a href="#" onclick="setLanguage('he', '.kat.cr');return false;" class="plain">Hebrew</a></li>
                                <li><a href="#" onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a href="#" onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a href="#" onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a href="#" onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a href="#" onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a href="#" onclick="setLanguage('ko', '.kat.cr');return false;" class="plain">Korean</a></li>
                                <li><a href="#" onclick="setLanguage('lv', '.kat.cr');return false;" class="plain">Latvian</a></li>
                                <li><a href="#" onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a href="#" onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a href="#" onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a href="#" onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a href="#" onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a href="#" onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a href="#" onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a href="#" onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a href="#" onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a href="#" onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a href="#" onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a href="#" onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a href="#" onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a href="#" onclick="setLanguage('si', '.kat.cr');return false;" class="plain">Sinhala</a></li>
                                <li><a href="#" onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a href="#" onclick="setLanguage('sl', '.kat.cr');return false;" class="plain">Slovenian</a></li>
                                <li><a href="#" onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a href="#" onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a href="#" onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a href="#" onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a href="#" onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a href="#" onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a href="#" onclick="setLanguage('ur', '.kat.cr');return false;" class="plain">Urdu</a></li>
                                <li><a href="#" onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" data-nop href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
        		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
        <li><a href="https://kastatus.com">KAT status</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <span  data-sc-slot="_673e31f53f8166159b8e996c4124765b"></span>
        <span  data-sc-slot="_e7050fb15fd39b3e4e99a5be4a57b6ea"></span>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script>
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "Organization",
"name": "KickassTorrents",
"url": "https://kat.cr",
"logo": "https://kat.cr/content/images/kickasslogo.png",
"sameAs": [
    "https://www.facebook.com/Official.KAT.Fanclub",
    "https://twitter.com/kickasstorrents"
]
}
</script>
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "WebSite",
"url": "https://kat.cr",
"potentialAction": {
    "@type": "SearchAction",
    "target": "https://kat.cr/usearch/{q}/",
    "query-input": {
        "@type": "PropertyValueSpecification",
        "valueRequired": true,
        "valueName": "q"
    }
}
}
</script>
<script>
 sc('addGlobal', 'pagetype', 'front');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>

</body>
</html>
