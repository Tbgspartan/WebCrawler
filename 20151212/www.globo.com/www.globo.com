



<!DOCTYPE html>

<!--[if IE]><![endif]--><html><head><meta charset="utf-8" /><meta http-equiv="Content-Type" content="text/html;charset=UTF-8" /><title>globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento</title><meta name="google-site-verification" content="BKmmuVQac1JM6sKlj3IoXQvffyIRJvJfbicMouA2a88" /><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.5" /><meta property="og:title" content="globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento"/><meta property="og:type" content="website"/><meta property="og:url" content="http://www.globo.com/"/><meta property="og:image" content="http://s.glbimg.com/en/ho/static/globocom2012/img/fb_marca.png"/><meta property="og:site_name" content="globo.com"/><meta property="og:description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta property="fb:page_id" content="224969370851736" /><meta property='busca:title' content='Globo.com' /><meta property='busca:species' content='Home' /><meta property='busca:issued' content='12/12/2015 16:40:12' /><meta property='busca:modified' content='12/12/2015 16:40:12' /><meta property='busca:publisher' content='www.globo.com' /><meta name="description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta name="keywords" content="NotÃ­cias, Entretenimento, Esporte, Tecnologia, Portal, ConteÃºdo, Rede Globo, TV Globo, VÃ­deos, TelevisÃ£o"/><meta name="application-name" content="Globo.com"/><meta name="msapplication-TileColor" content="#0669DE"/><meta name="msapplication-TileImage" content="http://s.glbimg.com/en/ho/static/globocom2012/img/globo-win-tile.png"/><link rel="canonical" href="http://www.globo.com/" /><link rel="shortcut icon" href="http://s.glbimg.com/en/ho/static/globocom2012/img/favicon.png" type="image/ico" /><link rel="apple-touch-icon-precomposed" sizes="57x57" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone.png" /><link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-ipad.png" /><link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone-retina.png" /><link rel="stylesheet" href="http://s.glbimg.com/en/ho/static/CACHE/css/7eddc671c75c.css" type="text/css" media="screen" /><!--[if lt IE 10]><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/d783f8b1c48f.js"></script><![endif]--><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/87daf046b449.js"></script></head><!--[if lt IE 7 ]><body class="ie ie6"><![endif]--><!--[if IE 7 ]><body class="ie ie7"><![endif]--><!--[if IE 8 ]><body class="ie ie8"><![endif]--><!--[if IE 9 ]><body class="ie ie9gt ie9"><![endif]--><!--[if gt IE 9 ]><body class="ie ie9gt ie10gt"><![endif]--><!--[if !IE]><!--><body><!--<![endif]--><div id="home-push-menu"><div class="home-push-menu-container"><div class="home-push-menu-initial"><div class="home-push-menu-header">NAVEGUE</div><ul class="barra-itens" data-analytics-context="Link Produto"><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://g1.globo.com" accesskey="n" class="barra-item-g1 link-produto analytics-area analytics-id-T">g1</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://globoesporte.globo.com" accesskey="e" class="barra-item-globoesporte link-produto analytics-area analytics-id-T">globoesporte</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://gshow.globo.com" accesskey="t" class="barra-item-gshow link-produto analytics-area analytics-id-T">gshow</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://famosos.globo.com" accesskey="t" class="barra-item-famosos-etc link-produto analytics-area analytics-id-T">famosos &amp; etc</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://techtudo.com.br" accesskey="b" class="barra-item-tech link-produto analytics-area analytics-id-T">tecnologia</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://video.globo.com" accesskey="v" class="barra-item-videos link-produto analytics-area analytics-id-T">vÃ­deos</a></li><li><a href="#" id="home-push-menu-show-email">e-mail</a><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="2.423,0 0,2.399 5.153,7.5 0,12.602 2.423,15 10,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-right.png"></image></svg></span></li><li><a href="https://meuperfil.globo.com/">central globo.com</a></li></ul></div><div class="home-push-menu-email"><ul><li><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="7.577,0 10,2.399 4.847,7.5 10,12.602 7.577,15 0,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-left.png"></image></svg></span><a id="home-push-menu-hide-email" href="#">e-mail</a></li><li><a href="https://login.globo.com/login/1948">globomail free</a></li><li><a href="https://login.globo.com/login/1">globomail pro</a></li></ul></div></div></div><div id="home-pagecontent" class=" home-menu-rendered" style=""><script type="text/javascript">
function comScore(C){var A="comScore",j=document,y=j.location,B="",z="undefined",x=2048,D,v,i,w,c="characterSet",l="defaultCharset",u=(typeof encodeURIComponent!=z?encodeURIComponent:escape);if(j.cookie.indexOf(A+"=")!=-1){i=j.cookie.split(";");for(w=0,y=i.length;w<y;w++){var d=i[w].indexOf(A+"=");if(d!=-1){B="&"+unescape(i[w].substring(d+A.length+1))}}}C=C+"&ns__t="+(new Date().getTime());C=C+"&ns_c="+(j[c]?j[c]:(j[l]?j[l]:""))+"&c8="+u(j.title)+B+"&c7="+u(y&&y.href?y.href:j.URL)+"&c9="+u(j.referrer);if(C.length>x&&C.indexOf("&")>0){D=C.substr(0,x-8).lastIndexOf("&");C=(C.substring(0,D)+"&ns_cut="+u(C.substring(D+1))).substr(0,x)}if(j.images){v=new Image();if(typeof ns_p==z){ns_p=v}v.src=C}else{j.write('<p><img src="'+C+'" height="1" width="1" alt="*"/></p>')}}comScore("http"+(document.location.href.charAt(4)=="s"?"s://sb":"://b")+".scorecardresearch.com/p?c1=2&c2=6035227");</script><noscript><p><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6035227&amp;c4=http%3A%2F%2Fwww.globo.com%2Findex.html" height="1" width="1" alt="*"/></p></noscript><script>
libby.loadScript('http://b.scorecardresearch.com/c2/6035227/cs.js');</script><script type="text/javascript">
var utag_data = {"structure_tree": "[\"globocom\"]", "ad_site_page": "{\"adUnit\":\"tvg_Globo.com.Home\", \"adPositionsDesktop\": [\"banner_slim_topo\",\"banner_slb_meio\",\"banner_slb_fim\",\"banner_selo4\",\"banner_floating\"], \"adPositionsMobile\":[\"banner_mobile_topo\",\"banner_mobile_meio\",\"banner_mobile_fim\"]}", "page_name": "index"};</script><script type="text/javascript">
(function (a,b,c,d) {a = '//tags.globo.com/utag/globo/home/prod/utag.js';b=document;c = 'script';d=b.createElement(c);d.src=a;d.type = 'text/java' + c;d.async=true;a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);})();
</script><header><div id="base-container-width-element" class="container"><div class="header-mobile-top analytics-area analytics-id-H"><a id="open-menu" class="open-menu" href="#"><svg version="1.1" class="burger-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19px" height="13px" viewBox="0 0 19 13" enable-background="new 0 0 19 13" xml:space="preserve"><g><path fill="#1063E0" d="M17.493,5H1.485C0.665,5,0,5.665,0,6.485C0,7.305,0.665,7.97,1.485,7.97h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,5.665,18.313,5,17.493,5z M1.485,2.971h16.008c0.82,0,1.486-0.666,1.486-1.486C18.979,0.666,18.313,0,17.493,0H1.485C0.665,0,0,0.666,0,1.484C0,2.305,0.665,2.971,1.485,2.971z M17.493,10H1.485C0.665,10,0,10.666,0,11.485c0,0.82,0.665,1.485,1.485,1.485h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,10.666,18.313,10,17.493,10z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/menu-button.png"></image></svg></a><a class="logo-globo analytics-area analytics-id-T" href="http://globo.com" title="globo.com"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 98 22" enable-background="new 0 0 98 22" xml:space="preserve"><g><path fill="#1063E0" d="M92.982,5.033c-1.115,0-2.549,0.601-3.459,1.625c-0.865-1.069-2.026-1.625-3.392-1.625c-1.205,0-2.252,0.489-3.049,1.336c-0.114-0.757-0.661-1.203-1.502-1.203c-0.957,0-1.639,0.712-1.639,1.781v8.551c0,1.069,0.682,1.781,1.639,1.781s1.638-0.712,1.638-1.781V9.909c0-1.292,0.592-2.205,1.936-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.682,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.909c0-1.292,0.728-2.205,1.934-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.683,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.352C97.466,6.547,95.645,5.033,92.982,5.033z M10.036,5.166c-0.91,0-1.434,0.49-1.639,1.336c-0.614-0.98-1.889-1.469-3.004-1.469C2.093,5.033,0,8.039,0,11.268c0,3.184,1.616,6.146,5.052,6.146c1.502,0,2.617-0.535,3.299-1.515h0.045v0.757c0,1.625-1.183,2.538-2.776,2.538c-1.889,0-3.049-1.001-4.097-1.001c-0.637,0-1.319,0.8-1.319,1.403C0.205,21.354,4.028,22,5.371,22c3.732,0,6.304-1.938,6.304-5.791V6.947C11.674,5.878,10.991,5.166,10.036,5.166z M5.826,14.474c-1.707,0-2.549-1.559-2.549-3.273c0-1.492,0.842-3.229,2.549-3.229c1.797,0,2.571,1.781,2.571,3.339C8.397,12.871,7.578,14.474,5.826,14.474z M14.318,0c-0.956,0-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781s1.639-0.712,1.639-1.781V1.782C15.957,0.712,15.274,0,14.318,0z M22.479,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169c3.527,0,5.872-2.829,5.872-6.169C28.35,7.994,25.938,5.033,22.479,5.033z M22.479,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.797-3.273,2.594-3.273c1.798,0,2.594,1.737,2.594,3.273C25.073,12.782,24.254,14.474,22.479,14.474z M35.371,5.033c-1.138,0-2.253,0.378-3.118,1.113V1.782C32.253,0.712,31.57,0,30.614,0s-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781c0.911,0,1.434-0.49,1.639-1.336c0.614,0.979,1.889,1.471,3.004,1.471c3.3,0,5.393-3.008,5.393-6.036C40.65,8.261,38.897,5.033,35.371,5.033z M34.825,14.474c-1.798,0-2.572-1.781-2.572-3.34c0-1.559,0.819-3.162,2.572-3.162c1.707,0,2.548,1.759,2.548,3.273C37.373,12.737,36.531,14.474,34.825,14.474z M46.863,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169s5.872-2.829,5.872-6.169C52.735,7.994,50.322,5.033,46.863,5.033z M46.863,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273s2.594,1.737,2.594,3.273C49.458,12.782,48.638,14.474,46.863,14.474z M55.425,13.939c-0.978,0-1.774,0.779-1.774,1.736s0.796,1.738,1.774,1.738c0.979,0,1.775-0.781,1.775-1.738S56.404,13.939,55.425,13.939z M66.583,13.805c-0.386,0-1.616,0.669-2.436,0.669c-1.774,0-2.753-1.648-2.753-3.229c0-1.648,0.888-3.273,2.753-3.273c1.048,0,1.776,0.623,2.594,0.623c0.775,0,1.253-0.868,1.253-1.536c0-1.56-2.709-2.026-4.006-2.026c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.343,6.169,5.871,6.169c1.184,0,4.006-0.558,4.006-2.206C67.994,14.451,67.355,13.805,66.583,13.805z M73.446,5.033c-3.459,0-5.872,2.961-5.872,6.212c0,3.34,2.344,6.169,5.872,6.169c3.527,0,5.871-2.829,5.871-6.169C79.317,7.994,76.904,5.033,73.446,5.033z M73.446,14.474c-1.774,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273c1.797,0,2.594,1.737,2.594,3.273C76.04,12.782,75.221,14.474,73.446,14.474z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/logo.png"></image></svg></a><form id="search-form" action="http://www.globo.com/busca/" method="get"><div id="search-container" class="search-container"><span><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="12px" height="12px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><g><path fill="#999999" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/search-small.png"></image></svg></span><div class="search-internal-container"><input type="text" id="search-globo" name="q" placeholder="encontre na globo.com"></div><a id="close-search" class="close-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="11px" height="11px" viewBox="0 0 11 11" enable-background="new 0 0 11 11" xml:space="preserve"><g><path fill="#666766" d="M6.884,5.501c1.955-1.955,3.733-3.732,3.809-3.808c0.388-0.388,0.388-1.015,0-1.403 c-0.387-0.386-1.015-0.386-1.402,0C9.215,0.365,7.437,2.144,5.482,4.099c-1.947-1.947-3.714-3.714-3.79-3.79 c-0.387-0.388-1.015-0.388-1.402,0c-0.387,0.387-0.387,1.016,0,1.401c0.076,0.076,1.842,1.843,3.79,3.791 C2.135,7.446,0.372,9.209,0.297,9.285c-0.387,0.387-0.387,1.015,0,1.401c0.387,0.389,1.015,0.389,1.402,0 c0.075-0.075,1.838-1.838,3.784-3.783c1.953,1.952,3.727,3.727,3.802,3.802c0.387,0.387,1.015,0.387,1.402,0 c0.387-0.387,0.387-1.015,0-1.402C10.611,9.228,8.837,7.454,6.884,5.501z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/close.png"></image></svg></a></div><a id="open-search" class="open-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><path fill="#1063e0" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path><image src="http://s.glbimg.com/gl/ba/img/common/search.png"></image></svg></a></form></div><h1 class="analytics-area analytics-id-H"><a class="logo-topo analytics-area analytics-id-T" href="/" title="globo.com">
                globo.com
            </a></h1><div class="complementos analytics-area analytics-id-T"><div id="busca-padrao"><form action="http://www.globo.com/busca/" method="get" accept-charset="utf-8"><fieldset><legend>buscar</legend><label for="busca-campo" class="label-for-search">buscar</label><input id="busca-campo" type="text" name="q" autocomplete="off" accesskey="s" lang="pt-BR" x-webkit-speech speech /><button type="submit">buscar</button></fieldset></form></div><div id="libby-box-previsao-tempo" class="analytics-area analytics-id-L"></div></div><nav><div class="spacer"></div><header id="header-produto" class="header-navegacao header-home" data-analytics="T"><div id="glbbarrawidget"></div><div class="floating-bar"><div class="header-bar"><div class="grid-12 clearfix area-principal row"><div class="column"><div class="menu-area to-left" data-analytics="S"><div class="menu-button"><div class="burger"><b></b><b></b><b></b></div><span class="menu-label">MENU</span></div><span class="menu-produto"></span></div><a class="logo-area" href="http://www.globo.com/"><h1 class="logo icon-produto">Home</h1></a><div class="to-right"><div class="search-area"><form id="frmBuscaScroll" action="#"><input id="search-input-floating" type="text" name="q" placeholder="BUSCAR" class="search" /><label for="search-input-floating" class="glass-container"><div class="glass"><div class="iglass css3pie"></div></div></label></form></div></div></div></div></div></div><div class="cube-container"><div class="cube"><div class="face front"><div class="grid-12 clearfix area-principal row"><div class="column"><div class="menu-area to-left" data-analytics="S"><div class="menu-button"><div class="burger"><b></b><b></b><b></b></div><span class="menu-label">MENU</span></div><span class="menu-produto"></span></div><a class="logo-area" href="http://www.globo.com/"><h1 class="logo icon-produto">Home</h1></a><div class="to-right"><div class="search-area"><form id="frmBuscaScroll" action="#"><input id="search-input-top" type="text" name="q" placeholder="BUSCAR" class="search" /><label for="search-input-top" class="glass-container"><div class="glass"><div class="iglass css3pie"></div></div></label></form></div><div id="busca-padrao"></div></div></div></div></div></div></div></header><script>
(function(window,document) {try{new CustomEvent("test");} catch(e){var CustomEvent=function(event,params) {var evt;params=params || {bubbles: false,
cancelable: false,
detail: undefined};evt=document.createEvent("CustomEvent");evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);return evt;};
CustomEvent.prototype=window.Event.prototype;window.CustomEvent=CustomEvent;}
document.dispatchEvent(new CustomEvent('glb.headerDom.ready'));})(window,document);</script><style type="text/css">
    /*
       evita que o markup do menu seja exibido sem estilo enquanto
       ele nÃ£o foi carregado
    */
    #menu-container {
        display: none;
    }
    #menu-fonts {
        height: 1px;
        width: 1px;
        color: transparent;
        overflow: hidden;
        position: absolute;
    }
</style>

<nav id="menu-container" class="regua-navegacao-tab menu-no-animation">
    

<script id="menu-carousel-template" type="x-tmpl-mustache">

<div id="menu-carousel-header-sizes-infos" class="menu-carousel-header-sizes-infos">
</div>
<div id="menu-carousel-header" class="menu-carousel menu-carousel-header swiper-container">
    <ul class="menu-brands swiper-wrapper">
        {{#portals}}
        <li data-produto="{{name}}" class="menu-carousel-item menu-carousel-{{name}} {{#active}}menu-carousel-item-active{{/active}} swiper-slide">
            <a href="{{link}}" class="menu-carousel-link" data-index="{{index}}">
                <svg class="brand-icon">
                    <use class="menu-logo" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#eixo-icone-{{name}}"></use>
                </svg>
            </a>
            <div class="menu-carousel-link-shadow"></div>
        </li>
        {{/portals}}
    </ul>
</div>
<div id="menu-carousel-body" class="menu-carousel menu-carousel-body swiper-container">
    <ul class="menu-brands swiper-wrapper">
        {{#portals}}
        <li data-produto="{{name}}" class="menu-carousel-item menu-carousel-{{name}} {{#active}}menu-carousel-item-active{{/active}} swiper-slide">
            <div id="menu-rounder" class="menu-rounder">{{{menu}}}</div>
        </li>
        {{/portals}}
    </ul>
</div>

</script>

<script id="menu-rounder-template" type="x-tmpl-mustache">

  <div id="menu-cascade" class="menu-cascade" data-menu-index="{{index}}">
    <ul class="menu-root menu-level menu-level-0">{{#items}}{{>recursive_partial}}{{/items}}</ul>
  </div>
  <div id="menu-addon" class="menu-addon"></div>

</script>

<script id="menu-custom-template" type="x-tmpl-mustache">

    {{#items}}{{>recursive_partial}}{{/items}}

</script>


<script id="menu-dinamico-template" type="x-tmpl-mustache">

<li id="menu-{{deepness}}-{{tituloSlug}}" class="menu-item {{#separador}} menu-item-separator{{/separador}}{{#destacado}} menu-item-highlighted{{/destacado}}{{#agrupador}} menu-item-grouped{{/agrupador}}{{#hasChildren}} is-father{{/hasChildren}}{{#hasSubmenuBroken}} menu-item-submenu-broken{{/hasSubmenuBroken}}">

    {{#shouldBeLink}}<a href="{{link}}" class="menu-item-link">{{/shouldBeLink}}
    {{^shouldBeLink}}<span class="menu-item-link">{{/shouldBeLink}}
        <span class="menu-item-title">{{titulo}}<span class="menu-item-arrow"><svg class="regua-navegacao-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#regua-arrow"></use></svg></span></span>
    {{#shouldBeLink}}</a>{{/shouldBeLink}}
    {{^shouldBeLink}}</span>{{/shouldBeLink}}

    {{#hasChildren}}
        <div class="menu-level menu-submenu menu-submenu-level{{deepness}}{{#hasSubmenuBroken}} menu-item-submenu-broken{{/hasSubmenuBroken}} menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back"><span class="menu-item-arrow"><svg class="regua-navegacao-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#regua-arrow"></use></svg></span><span class="regua-navegacao-menu-item">{{titulo}}</span></a>
            <ul class="menu-submenu-vertical-scroll">
            {{#children}}
                {{>recursive_partial}}
            {{/children}}
            <ul>
        </div>
    {{/hasChildren}}
</li>

</script>


    <div id="menu-rounder" class="menu-rounder">
        <div id="menu-cascade" class="menu-cascade" data-menu-index="0">
            

<ul class="menu-root menu-level menu-level-0">
    
        




<li class="menu-item is-father " id="menu-1-famosos">

    <a class="menu-item-link">
        <span class="menu-item-title">Famosos</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Famosos</a>
            
                

                




<li class="menu-item " id="menu-2-ego">

    <a href="http://ego.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-paparazzo">

    <a href="http://ego.globo.com/paparazzo/index.html" class="menu-item-link">
        <span class="menu-item-title">Paparazzo</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-patricia-kogut">

    <a href="http://kogut.oglobo.globo.com/noticias-da-tv/index.html" class="menu-item-link">
        <span class="menu-item-title">Patricia Kogut</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-quem">

    <a href="http://revistaquem.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Quem</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-retratos-da-vida">

    <a href="http://extra.globo.com/famosos/" class="menu-item-link">
        <span class="menu-item-title">Retratos da Vida</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-moda-estilo">

    <a class="menu-item-link">
        <span class="menu-item-title">Moda &amp; Estilo</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Moda &amp; Estilo</a>
            
                

                




<li class="menu-item " id="menu-2-ela-no-o-globo">

    <a href="http://ela.oglobo.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Ela no O Globo</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-estilo-no-gshow">

    <a href="http://gshow.globo.com/Estilo/" class="menu-item-link">
        <span class="menu-item-title">Estilo no Gshow</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-moda-no-ego">

    <a href="http://ego.globo.com/moda/index.html" class="menu-item-link">
        <span class="menu-item-title">Moda no Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-moda-no-gnt">

    <a href="http://gnt.globo.com/moda/" class="menu-item-link">
        <span class="menu-item-title">Moda no GNT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-glamour">

    <a href="http://revistaglamour.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Glamour</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-gq-brasil">

    <a href="http://gq.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista GQ Brasil</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-marie-claire">

    <a href="http://revistamarieclaire.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Marie Claire</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-vogue">

    <a href="http://vogue.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Vogue</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-bem-estar-saude">

    <a class="menu-item-link">
        <span class="menu-item-title">Bem-Estar &amp; SaÃºde</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Bem-Estar &amp; SaÃºde</a>
            
                

                




<li class="menu-item " id="menu-2-beleza-no-ego">

    <a href="http://ego.globo.com/beleza/index.html" class="menu-item-link">
        <span class="menu-item-title">Beleza no Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-beleza-no-gnt">

    <a href="http://gnt.globo.com/beleza/" class="menu-item-link">
        <span class="menu-item-title">Beleza no GNT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-beleza-no-gshow">

    <a href="http://gshow.globo.com/Estilo/Beleza/" class="menu-item-link">
        <span class="menu-item-title">Beleza no Gshow</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-bem-estar-no-g1">

    <a href="http://g1.globo.com/bemestar/" class="menu-item-link">
        <span class="menu-item-title">Bem Estar no G1</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-bem-estar-no-gnt">

    <a href="http://gnt.com.br/bemestar" class="menu-item-link">
        <span class="menu-item-title">Bem Estar no GNT </span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-horoscopo-no-ego">

    <a href="http://horoscopo.ego.globo.com/" class="menu-item-link">
        <span class="menu-item-title">HorÃ³scopo no Ego</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-maes-no-gnt">

    <a href="http://gnt.globo.com/maes-e-filhos/" class="menu-item-link">
        <span class="menu-item-title">MÃ£es no GNT </span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-meus-5-minutos">

    <a href="http://meus5minutos.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Meus 5 Minutos</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-crescer">

    <a href="http://revistacrescer.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Crescer</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-casa-decoracao">

    <a class="menu-item-link">
        <span class="menu-item-title">Casa &amp; DecoraÃ§Ã£o</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Casa &amp; DecoraÃ§Ã£o</a>
            
                

                




<li class="menu-item " id="menu-2-casa-no-gnt">

    <a href="http://gnt.globo.com/casa-e-decoracao/" class="menu-item-link">
        <span class="menu-item-title">Casa no GNT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-casa-vogue">

    <a href="http://casavogue.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Casa Vogue</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-casa-e-jardim">

    <a href="http://revistacasaejardim.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Casa e Jardim</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-do-zap">

    <a href="http://revista.zapimoveis.com.br/?utm_source=globo.com-etc&amp;utm_medium=botao-casa-decoracao&amp;utm_campaign=revista-imoveis" class="menu-item-link">
        <span class="menu-item-title">Revista do Zap</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-gastronomia">

    <a class="menu-item-link">
        <span class="menu-item-title">Gastronomia</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Gastronomia</a>
            
                

                




<li class="menu-item " id="menu-2-casa-e-comida">

    <a href="http://revistacasaejardim.globo.com/Casa-e-Comida/" class="menu-item-link">
        <span class="menu-item-title">Casa e Comida</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-colheradas">

    <a href="http://meus5minutos.globo.com/blogs/Colheradas/" class="menu-item-link">
        <span class="menu-item-title">Colheradas</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-receitas-da-ana-maria">

    <a href="http://gshow.globo.com/programas/mais-voce/Receitas-da-Ana-Maria/" class="menu-item-link">
        <span class="menu-item-title">Receitas da Ana Maria</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-receitascom">

    <a href="http://gshow.globo.com/receitas/" class="menu-item-link">
        <span class="menu-item-title">Receitas.com</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-receitas-no-gnt">

    <a href="http://gnt.globo.com/receitas/" class="menu-item-link">
        <span class="menu-item-title">Receitas no GNT</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-musica">

    <a class="menu-item-link">
        <span class="menu-item-title">MÃºsica</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">MÃºsica</a>
            
                

                




<li class="menu-item " id="menu-2-altas-horas">

    <a href=" http://gshow.globo.com/programas/altas-horas/" class="menu-item-link">
        <span class="menu-item-title">Altas Horas </span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-g1-musica">

    <a href="http://g1.globo.com/musica/" class="menu-item-link">
        <span class="menu-item-title">G1 MÃºsica</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-gshow-musica">

    <a href="http://gshow.globo.com/Musica/" class="menu-item-link">
        <span class="menu-item-title">Gshow MÃºsica</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-musica-no-multishow">

    <a href="http://multishow.globo.com/musica/" class="menu-item-link">
        <span class="menu-item-title">MÃºsica no Multishow</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-musicacombr">

    <a href="http://musica.com.br/" class="menu-item-link">
        <span class="menu-item-title">MÃºsica.com.br</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-radiobeat">

    <a href="http://radiobeat.com.br/" class="menu-item-link">
        <span class="menu-item-title">RADIOBEAT</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-som-brasil">

    <a href="http://globotv.globo.com/rede-globo/som-brasil/" class="menu-item-link">
        <span class="menu-item-title">Som Brasil</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item is-father " id="menu-1-cinema-teatro">

    <a class="menu-item-link">
        <span class="menu-item-title">Cinema &amp; Teatro</span>
    </a>

    
        <ul class="menu-level menu-submenu menu-submenu-level1  menu-submenu-column-1">
            <a class="menu-submenu-title menu-item-back">Cinema &amp; Teatro</a>
            
                

                




<li class="menu-item " id="menu-2-canal-brasil">

    <a href="http://canalbrasil.globo.com/ " class="menu-item-link">
        <span class="menu-item-title">Canal Brasil</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-canal-universal">

    <a href="http://universal.globo.com" class="menu-item-link">
        <span class="menu-item-title">Canal Universal</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-filmes-na-globo">

    <a href="http://redeglobo.globo.com/filmes/index.html" class="menu-item-link">
        <span class="menu-item-title">Filmes na Globo</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-g1-cinema">

    <a href="http://g1.globo.com/pop-arte/cinema/" class="menu-item-link">
        <span class="menu-item-title">G1 Cinema</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-globo-filmes">

    <a href=" http://globofilmes.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Globo Filmes</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-megapix">

    <a href="http://megapix.globo.com/" class="menu-item-link">
        <span class="menu-item-title">MegaPix</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-revista-monet">

    <a href="http://revistamonet.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Revista Monet</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-teatro">

    <a href="http://www.agentesevenoteatro.com.br/" class="menu-item-link">
        <span class="menu-item-title">Teatro</span>
    </a>

    
</li>

            
                

                




<li class="menu-item " id="menu-2-telecine">

    <a href="http://telecine.globo.com/" class="menu-item-link">
        <span class="menu-item-title">Telecine</span>
    </a>

    
</li>

            
        </ul>
    
</li>

    
        




<li class="menu-item menu-item-highlighted " id="menu-1-ego">

    <a href="http://ego.globo.com/" class="menu-item-link">
        <span class="menu-item-title">ego</span>
    </a>

    
</li>

    
        




<li class="menu-item menu-item-highlighted " id="menu-1-quem">

    <a href="http://revistaquem.globo.com/" class="menu-item-link">
        <span class="menu-item-title">quem</span>
    </a>

    
</li>

    
        




<li class="menu-item menu-item-highlighted " id="menu-1-patricia-kogut">

    <a href="http://kogut.oglobo.globo.com/" class="menu-item-link">
        <span class="menu-item-title">patricia kogut</span>
    </a>

    
</li>

    
</ul>

        </div>
        <div id="menu-addon" class="menu-addon"></div>
    </div>

    
</nav>

<div id="menu-fonts">
    
    
    <span class="open-sans">BESbswy</span>
    <span class="open-sans-bold">BESbswy</span>
    <span class="open-sans-light">BESbswy</span>
    <span class="roboto-slab">BESbswy</span>
    <span class="proximanova-semibold">BESbswy</span>
</div>

<div id="menu-content-overlay"></div>
<style>
.regua-svg-container{height:0;width:0;position:absolute;visibility:hidden}</style><script>
(function (window) {window.REGUA_SETTINGS=window.REGUA_SETTINGS || {};window.REGUA_SETTINGS.portalName = "home";window.REGUA_SETTINGS.portalHome = "http://www.globo.com/";window.REGUA_SETTINGS.portalsList = ["home","g1","ge","gshow","famosos","techtudo","globotv","globoplay","globosatplay"];window.REGUA_SETTINGS.portalsLinkList = ["http://www.globo.com/","http://g1.globo.com/index.html","http://globoesporte.globo.com/","http://gshow.globo.com/","http://famosos.globo.com/","http://www.techtudo.com.br/","http://globotv.globo.com/","http://globoplay.globo.com/","http://globosatplay.globo.com/"];window.REGUA_SETTINGS.staticUrl = "http://s.glbimg.com/en/ho/static/";window.REGUA_SETTINGS.svgSpriteName = "sprite-e737468ca1.svg";window.REGUA_SETTINGS.suggestUrl = "";window.REGUA_SETTINGS.version = "1.3.7";})(window);
</script><script>(function(){var e,t,n,o;e=function(){function e(){this.name="Android",this.bridge=window.GloboBridge||{navigated:function(e){},menuAction:function(e){},reguaAction:function(e){}}}return e.prototype.navigateTo=function(e){this.bridge.navigated(JSON.stringify(e))},e.prototype.menuAction=function(e){this.bridge.menuAction(JSON.stringify(e))},e.prototype.reguaAction=function(e){this.bridge.reguaAction(JSON.stringify(e))},e}(),t=function(){function e(){this.name="iOS",this.connectWebViewJavascriptBridge(function(e){return e.init(function(e,t){return window.glb.masterApp.manageMessage(e)})})}return e.prototype.connectWebViewJavascriptBridge=function(e){window.WebViewJavascriptBridge?e(WebViewJavascriptBridge):document.addEventListener("WebViewJavascriptBridgeReady",function(){e(WebViewJavascriptBridge)},!1)},e.prototype.brigdeSendJson=function(e){return this.connectWebViewJavascriptBridge(function(t){t.send(JSON.stringify(e))})},e.prototype.navigateTo=function(e){this.brigdeSendJson(e)},e.prototype.menuAction=function(e){this.brigdeSendJson(e)},e.prototype.reguaAction=function(e){this.brigdeSendJson(e)},e}(),n=function(){function n(e){this.platformApp=e,this.initCalled=!1,null!=this.platformApp&&this.bind()}return n.prototype.init=function(){this.initCalled||(this.initCalled=!0,this.header&&(this.processHeaderInfo(),this.bindAfterInit()))},n.prototype.processHeaderInfo=function(){this.countHeaderTries=0,this.maxHeaderTries=100,this.headerCubeContainer=this.header.querySelector(".cube-container"),this.headerCubeContainer?this.processHeaderInfoAction():setTimeout(function(e){return function(){return e.processHeaderInfoAction()}}(this),1)},n.prototype.processHeaderInfoAction=function(){var e;if(this.countHeaderTries++,this.headerCubeContainer=this.header.querySelector(".cube-container"),this.headerCubeContainer)this.cacheVariables(),e=this.getHeaderData(),this.navigateTo(e);else{if(this.countHeaderTries>this.maxHeaderTries)return void console.log("Nao achou o markup interno do header");setTimeout(function(e){return function(){return e.processHeaderInfoAction()}}(this),1)}},n.prototype.bind=function(){var e;return this.bindAppEvents(),this.header=document.querySelector("#header-produto"),e=function(e){return function(){return e.header||(e.header=document.querySelector("#header-produto")),e.init()}}(this),this.header?this.init():(document.addEventListener("glb.headerDom.ready",e),document.addEventListener("DOMContentLoaded",e))},n.prototype.cacheVariables=function(){return this.elements={},this.elements.header=this.header,this.elements.headerCubeContainer=this.headerCubeContainer,this.elements.headerFront=this.elements.headerCubeContainer?this.elements.headerCubeContainer.querySelector(".front"):null,this.elements.headerLogoProduto=this.elements.headerCubeContainer?this.elements.headerCubeContainer.querySelector(".logo-produto-container"):null,this.elements.headerLogoProdutoLink=this.elements.headerLogoProduto?this.elements.headerLogoProduto.parentNode:null,this.elements.headerLogoArea=this.elements.headerCubeContainer?this.elements.headerCubeContainer.querySelector(".logo-area .logo"):null,this.elements.headerLogoAreaLink=this.elements.headerLogoArea?this.elements.headerLogoArea.parentNode:null,this.elements.headerSubeditoria=this.elements.headerFront?this.elements.headerFront.querySelector(".menu-subeditoria"):null,this.elements.headerSubeditoriaLink=this.elements.headerSubeditoria?this.elements.headerSubeditoria.querySelector("a"):null,!0},n.prototype.bindAppEvents=function(){document.addEventListener("glb.menu-carousel.ready",function(e){return function(){return e.platformApp.menuAction({action:"menuReady"})}}(this)),document.addEventListener("glb.menu-carousel.shown.before",function(e){return function(){return e.platformApp.menuAction({action:"menuShownBefore"})}}(this)),document.addEventListener("glb.menu-carousel.shown.after",function(e){return function(){return e.platformApp.menuAction({action:"menuShownAfter"})}}(this)),document.addEventListener("glb.menu-carousel.off.before",function(e){return function(){return e.platformApp.menuAction({action:"menuOffBefore"})}}(this)),document.addEventListener("glb.menu-carousel.off.after",function(e){return function(){return e.platformApp.menuAction({action:"menuOffAfter"})}}(this)),document.addEventListener("glb.regua.exists",function(e){return function(){return e.platformApp.reguaAction({action:"reguaExists"})}}(this))},n.prototype.bindAfterInit=function(){},n.prototype._getText=function(e){return e?(e.innerText||e.textContent).trim()||"":""},n.prototype._getHref=function(e){return e?e.getAttribute("href")||"":""},n.prototype._getCssProp=function(e,t){var n;return e?(n=window.getComputedStyle(e),n.getPropertyValue(t)||""):""},n.prototype._getBackgroundColor=function(e){return this._getCssProp(e,"background-color")},n.prototype.getHeaderData=function(){var e;return e={color:this._getBackgroundColor(this.elements.headerFront),editoria:"",editoriaHref:"",subeditoria:"",subeditoriaHref:"",logo:"",logoHref:""},this.elements.headerLogoProduto?(e.logo=this._getText(this.elements.headerLogoProduto),e.logoHref=this._getHref(this.elements.headerLogoProdutoLink),e.editoria=this._getText(this.elements.headerLogoArea),e.editoriaHref=this._getHref(this.elements.headerLogoArea),this.elements.headerSubeditoria&&(e.subeditoria=this._getText(this.elements.headerSubeditoria),e.subeditoriaHref=this._getHref(this.elements.headerSubeditoriaLink))):(e.logo=this._getText(this.elements.headerLogoArea),e.logoHref=this._getHref(this.elements.headerLogoAreaLink)),e},n.prototype.navigateTo=function(e){e.action="navigateTo",e.url=window.location.href,this.platformApp?this.platformApp.navigateTo(e):console.log("Sem Plataforma")},n.prototype.manageMessage=function(e){var t,n;return e&&(t=JSON.parse(e),t.action="regua-change")?(n=t.rulerItem||"home",null!=window.$?$(document).trigger("glb.regua.change",[n]):console.log("Sem jQuery")):void 0},n.prototype.getUserAgent=function(){return window.getUserAgent?window.getUserAgent():window.navigator.userAgent},n.prototype.getPlatformApp=function(){var n;return n=this.getUserAgent(),n.match(/(iPad|iPhone|iPod)/g)?new t:n.match(/(Android)/g)?new e:void 0},n}(),window.glb=window.glb||{},window.glb.MasterApp=n,window.glb.AndroidApp=e,window.glb.IosApp=t,window.initMasterApp=function(){var e;if(!window.glb.masterApp)return e=n.prototype.getPlatformApp(),window.glb.masterApp=new n(e)},o=null!=window.noAutoLoadReguaNavegacao&&window.noAutoLoadReguaNavegacao,o||window.initMasterApp()}).call(this);</script><script>(function(){var n,o,e,t;e=function(){return/MobApp_Android|MobApp_iOS|MobApp_WP/},o=function(){return window.getUserAgent?window.getUserAgent():window.navigator.userAgent},n=function(){var n,t,a,i,r;i=o(),r=i.split(" ");for(a in r)if(n=r[a],e().test(n))return t=n.split("/"),{name:t[0].replace("_"," "),version:t[1],frameworkVersion:t[2]};return null},window.addGlbOnAppClass=function(){var o;window.glb=window.glb||{},window.glb.extractAppFromUserAgent||(window.glb.extractAppFromUserAgent=n,window.glb.nativeAppInfo=n(),null!==window.glb.nativeAppInfo&&(window.glb.nativeAppInfo.frameworkVersion?(o=parseInt(window.glb.nativeAppInfo.frameworkVersion,10),o>=1&&(document.documentElement.className+=" glb-on-app")):document.documentElement.className+=" glb-on-app-comnio"))},t=null!=window.noAutoLoadReguaNavegacao&&window.noAutoLoadReguaNavegacao,t||window.addGlbOnAppClass()}).call(this);</script><script>(function(){var n,e,o,i,t,w,d,r,a,s,l,u,c;try{new CustomEvent("test")}catch(m){o=m,n=function(n,e){var o;return o=void 0,e=e||{bubbles:!1,cancelable:!1,detail:void 0},o=document.createEvent("CustomEvent"),o.initCustomEvent(n,e.bubbles,e.cancelable,e.detail),o},n.prototype=window.Event.prototype,window.CustomEvent=n}s=function(n,e){var o;return null==n||null==e?!1:(o=new RegExp("(?:^|\\s)"+e+"(?!\\S)"),!!n.className.match(o))},e=function(n,e){s(n,e)||(n.className+=" "+e)},u=function(n,e){var o;s(n,e)&&(o=new RegExp("(?:^|\\s)"+e+"(?!\\S)","g"),n.className=n.className.replace(o,""))},window.viewportSize={},window.viewportSize.getHeight=function(){return a("Height")},window.viewportSize.getWidth=function(){return a("Width")},a=function(n){var e,o,i,t,w,d;return d=void 0,w=n.toLowerCase(),i=window.document,t=i.documentElement,void 0===window["inner"+n]?d=t["client"+n]:window["inner"+n]!==t["client"+n]?(e=i.createElement("body"),e.id="vpw-test-b",e.style.cssText="overflow:scroll",o=i.createElement("div"),o.id="vpw-test-d",o.style.cssText="position:absolute;top:-1000px",o.innerHTML="<style>@media("+w+":"+t["client"+n]+"px){body#vpw-test-b div#vpw-test-d{"+w+":7px!important}}</style>",e.appendChild(o),t.insertBefore(e,i.head),d=7===o["offset"+n]?t["client"+n]:window["inner"+n],t.removeChild(e)):d=window["inner"+n],d},window.myInnerWidth=viewportSize.getWidth(),window.myInnerHeight=viewportSize.getHeight(),t=function(){return window.myInnerWidth||window.innerWidth},i=function(){return window.myInnerHeight||window.innerHeight},w=function(){var n;return null!=window.isAndroidBrowser?window.isAndroidBrowser:(n=navigator.userAgent,window.isAndroidBrowser=n.indexOf("Mozilla/5.0")>-1&&n.indexOf("Android ")>-1&&n.indexOf("AppleWebKit")>-1&&-1===n.indexOf("Chrome"),window.isAndroidBrowser)},d=function(){return null!=window.isPortrait?window.isPortrait:(window.isPortrait=t()<=i(),window.isPortrait)},r=function(){return null!=window.isTouchable?window.isTouchable:(window.isTouchable="ontouchstart"in window||navigator.msMaxTouchPoints||window.DocumentTouch&&document instanceof window.DocumentTouch,window.isTouchable)},l=function(){var n;return window.REGUAMAXWIDTHPORTRAIT=window.REGUAMAXWIDTHPORTRAIT||640,window.REGUAMAXWIDTHLANDSCAPE=window.REGUAMAXWIDTHLANDSCAPE||767,window.isPortrait=d(),window.isTouchable=r(),window.isAndroidBrowser=w(),n=t(),(window.isPortrait&&n<=window.REGUAMAXWIDTHPORTRAIT||!window.isPortrait&&n<=window.REGUAMAXWIDTHLANDSCAPE)&&!window.isAndroidBrowser&&document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image","1.1")&&(null==window.glb.nativeAppInfo||window.glb.nativeAppInfo.frameworkVersion)},window.glb=window.glb||{},window.glb.hasClass=s,window.glb.addClass=e,window.glb.removeClass=u,window.addHasReguaClass=function(){window.glb.reguaShouldStart||(window.glb.reguaShouldStart=l,window.glb.reguaShouldStart()?(window.glb.addClass(document.documentElement,"has-regua"),window.glb.addClass(document.documentElement,"svg-support"),window.glb.removeClass(document.documentElement,"has-not-regua"),document.dispatchEvent(new CustomEvent("glb.regua.exists"))):(window.glb.removeClass(document.documentElement,"has-regua"),window.glb.removeClass(document.documentElement,"svg-support"),window.glb.addClass(document.documentElement,"has-not-regua")))},c=null!=window.noAutoLoadReguaNavegacao&&window.noAutoLoadReguaNavegacao,c||window.addHasReguaClass()}).call(this);</script><nav id="regua-navegacao" class="regua-navegacao "><div id="regua-svg-container" class="regua-svg-container"><svg xmlns="http://www.w3.org/2000/svg"><symbol id="eixo-icone-busca" viewBox="0 0 24 24"><path d="M21.67 20.27l-5.72-5.73c.98-1.26 1.55-2.83 1.55-4.54 0-4.14-3.36-7.5-7.5-7.5-4.15 0-7.5 3.36-7.5 7.5 0 4.14 3.35 7.5 7.5 7.5 1.7 0 3.27-.57 4.53-1.54l5.73 5.73c.42.41 1.07.43 1.46.04.39-.39.37-1.04-.05-1.46zM10 15.5c-3.04 0-5.5-2.46-5.5-5.5S6.96 4.5 10 4.5c3.03 0 5.5 2.46 5.5 5.5s-2.47 5.5-5.5 5.5z"/></symbol><symbol id="eixo-icone-feed" viewBox="0 0 24 24"><path d="M17 14c0 .55-.45 1-1 1h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1zM19 10c0 .55-.45 1-1 1h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1z"/><path fill="#FFF" d="M17 14c0 .55-.45 1-1 1h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1zM19 10c0 .55-.45 1-1 1h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1z"/><path d="M19 10c0 .55-.45 1-1 1h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1zM17 14c0 .55-.45 1-1 1h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1z"/><path d="M4 3v4H0v11c0 1.66 1.34 3 3 3h19c1.1 0 2-.9 2-2V3H4zm0 15.09c0 .5-.45.91-1 .91s-1-.41-1-.91V9h2v9.09zM22 19H6V5h16v14z"/><path fill="#FFF" d="M4 9v9.09c0 .5-.45.91-1 .91s-1-.41-1-.91V9h2zM6 5v14h16V5H6zm10 10h-6c-.55 0-1-.45-1-1s.45-1 1-1h6c.55 0 1 .45 1 1s-.45 1-1 1zm2-4h-8c-.55 0-1-.45-1-1s.45-1 1-1h8c.55 0 1 .45 1 1s-.45 1-1 1z"/></symbol><symbol id="eixo-icone-menu" viewBox="0 0 24 24"><path d="M22 6c0 .552-.447 1-1 1H3c-.552 0-1-.448-1-1 0-.553.448-1 1-1h18c.553 0 1 .447 1 1zM22 12c0 .552-.447 1-1 1H3c-.552 0-1-.448-1-1 0-.553.448-1 1-1h18c.553 0 1 .447 1 1zM22 18c0 .552-.447 1-1 1H3c-.552 0-1-.448-1-1 0-.553.448-1 1-1h18c.553 0 1 .447 1 1z"/></symbol><symbol id="eixo-icone-usuario" viewBox="0 0 24 24"><path d="M12 2C8.4 2 5.5 4.9 5.5 8.5c0 3.61 2.9 6.5 6.5 6.5s6.5-2.89 6.5-6.5C18.5 4.9 15.6 2 12 2zm0 11.07c-2.49 0-4.5-2.02-4.5-4.5s2.01-4.5 4.5-4.5 4.5 2.02 4.5 4.5-2.01 4.5-4.5 4.5zM22.893 23c-.733 0-1.074-.455-1.074-.86C21.817 20.26 18.22 19 12 19s-9.818 1.136-9.818 3.14c0 .406-.41.86-1.092.86C.358 23 0 22.513 0 22.108 0 18.958 4.598 17 12 17s12 1.958 12 5.108c0 .405-.398.892-1.107.892z"/></symbol><symbol id="regua-arrow" viewBox="0 0 5 14"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.902 6.64L1.422.386C1.217.017.762-.11.402.104.05.317-.074.79.133 1.16l3.25 5.843L.1 12.847c-.208.368-.085.838.274 1.05.357.212.814.086 1.02-.28l3.412-6.074c.21-.243.26-.606.096-.904z"/></symbol></svg></div><div class="regua-navegacao-container"><ul class="regua-lista regua-lista-home"><li id="regua-navegacao-item-home" class="regua-navegacao-item active"><span data-href="http://www.globo.com/"
                                            data-target="regua-tab-home" class="home-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-feed"/></svg></span></li><li id="regua-navegacao-item-menu" class="regua-navegacao-item "><span data-target="regua-tab-menu" class="menu-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-menu"/></svg></span></li><li id="regua-navegacao-item-busca" class="regua-navegacao-item "><span data-target="regua-tab-busca" class="busca-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-busca"/></svg></span></li><li id="regua-navegacao-item-usuario" class="regua-navegacao-item "><span data-target="regua-tab-usuario" class="usuario-button regua-navegacao-link"><svg class="regua-navegacao-icon"><use xlink:href="#eixo-icone-usuario"/></svg></span></li></ul></div></nav><nav id="regua-user-container" class="regua-user-container regua-user-home regua-navegacao-tab"><div id="regua-user-controls" class="regua-user-controls"></div><div id="regua-user-tabs" class="regua-tabs regua-user-tab-2"><div id="tab-labels" class="tab-labels"><span class="tab-label tab-label-1" data-index="1">notificaÃ§Ãµes</span><span class="tab-label tab-label-2" data-index="2">minha conta</span></div><div class="regua-tab regua-tab-1" data-index="1"><div class="regua-content"><ul id="regua-user-profile-notifications" class="regua-user-row regua-user-profile-notifications"></ul></div></div><div class="regua-tab regua-tab-2" data-index="2"><div class="regua-content"><ul id="regua-user-profile-list" class="regua-user-row regua-user-profile-list"></ul><div id="regua-user-logout" class="regua-user-logout"></div></div></div></div></nav><div id="regua-tab-busca" class="regua-navegacao-tab regua-tab-busca regua-busca-home"><div class="regua-search-header"><div class="regua-search-box"><form class="regua-search-form" action="http://globo.com/busca/" method="GET"><div class="regua-search-form-container regua-table"><div class="regua-search-input-box regua-table-cell"><input class="regua-search-input" name="q" type="search" placeholder="o que vocÃª procura?" autocorrect="off" autocapitalize="off" autocomplete="off"/></div><div class="regua-search-buttons-container regua-table-cell"><div class="regua-search-clear-container"><a href="#" class="regua-search-clear-button">clear</a></div><div class="search-button-icon"><input type="submit" value="go" class="regua-search-submit"/><div class="regua-icon-block"><svg class="regua-navegacao-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#eixo-icone-busca"></use></svg></div><a href="#" class="regua-icon-block search-button-go"><svg class="regua-navegacao-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#eixo-icone-busca"></use></svg></a></div></div></div></form></div></div><div class="regua-container-search-body"><ul class="regua-pre-suggest"></ul><div class="regua-container-suggest"><ul class="regua-suggest"></ul></div><div class="regua-container-results"></div></div></div>



<ul id="home-menu" class="nested widget-menu">
    
        <li class="g1 analytics-product analytics-multi-product">
            <a href="http://g1.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">g1</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://g1.globo.com/" data-menu-id="g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">g1</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="jornais-revistas-e-radio">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">jornais, revistas e rÃ¡dio</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="servicos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">serviÃ§os</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-g1">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-g1" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/carros/">
                                                <span class="titulo">Carros</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/politica/">
                                                <span class="titulo">PolÃ­tica</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/">
                                                <span class="titulo">G1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/mundo/">
                                                <span class="titulo">Mundo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/economia/">
                                                <span class="titulo">Economia</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/">
                                                <span class="titulo">Jornal Extra</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/">
                                                <span class="titulo">Jornal O Globo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/concursos-e-emprego/">
                                                <span class="titulo">Concursos e Empregos</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/jornal-hoje/">
                                                <span class="titulo">Jornal Hoje</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/jornal-nacional/">
                                                <span class="titulo">Jornal Nacional</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="g1">
                                        <div class="submenu-title">g1</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Carros</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/concursos-e-emprego/">
                                                    <span class="titulo">Concursos e Empregos</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/">
                                                    <span class="titulo">Economia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/educacao/">
                                                    <span class="titulo">EducaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/imposto-de-renda/2015/index.html">
                                                    <span class="titulo">Imposto de Renda 2015</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/mundo/">
                                                    <span class="titulo">Mundo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/planeta-bizarro/">
                                                    <span class="titulo">Planeta Bizarro</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/politica/">
                                                    <span class="titulo">PolÃ­tica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/">
                                                    <span class="titulo">Pop &amp; Arte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/tecnologia/">
                                                    <span class="titulo">Tecnologia &amp; Games</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bom-dia-brasil/">
                                                    <span class="titulo">Bom dia Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://fantastico.globo.com/">
                                                    <span class="titulo">FantÃ¡stico</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/agronegocios/">
                                                    <span class="titulo">Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/globo-news/">
                                                    <span class="titulo">GloboNews</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/hora1">
                                                    <span class="titulo">Hora 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-da-globo/">
                                                    <span class="titulo">Jornal da Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-hoje/">
                                                    <span class="titulo">Jornal Hoje</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-nacional/">
                                                    <span class="titulo">Jornal Nacional</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="jornais-revistas-e-radio">
                                        <div class="submenu-title">jornais, revistas e rÃ¡dio</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/">
                                                    <span class="titulo">Jornal Extra</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/">
                                                    <span class="titulo">Jornal O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://cbn.globoradio.globo.com/home/HOME.htm">
                                                    <span class="titulo">RÃ¡dio CBN</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaautoesporte.globo.com/">
                                                    <span class="titulo">Revista Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaepoca.globo.com/">
                                                    <span class="titulo">Revista Ãpoca</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagalileu.globo.com/">
                                                    <span class="titulo">Revista Galileu</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagloborural.globo.com/">
                                                    <span class="titulo">Revista Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radioglobo.globoradio.globo.com/">
                                                    <span class="titulo">RÃ¡dio Globo</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="servicos">
                                        <div class="submenu-title">serviÃ§os</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/agenda.html">
                                                    <span class="titulo">Agenda de Shows</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://especiais.g1.globo.com/educacao/app-g1-enem/">
                                                    <span class="titulo">Aplicativo G1 Enem</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/">
                                                    <span class="titulo">Conversor de Moedas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">
                                                    <span class="titulo">CotaÃ§Ãµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/">
                                                    <span class="titulo">Enem e Vestibular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/loteria/">
                                                    <span class="titulo">Loterias</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/previsao-do-tempo.html">
                                                    <span class="titulo">PrevisÃ£o do tempo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/tabela-fipe/index.html">
                                                    <span class="titulo">Tabela FIPE</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/telecurso/">
                                                    <span class="titulo">Telecurso</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.techtudo.com.br/velocimetro.html">
                                                    <span class="titulo">VelocÃ­metro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-g1"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="globoesporte analytics-product analytics-multi-product">
            <a href="http://globoesporte.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">globoesporte</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-globoesporte">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/" data-menu-id="globoesportecom">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globoesporte.com</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://sportv.globo.com/" data-menu-id="sportv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sportv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/futebol/" data-menu-id="futebol">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">futebol</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="mais-esportes">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mais esportes</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-globoesporte">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-globoesporte" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                <span class="titulo">Futebol Internacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://sportv.globo.com/site/combate/">
                                                <span class="titulo">MMA</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/">
                                                <span class="titulo">Globo Esporte</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                <span class="titulo">FÃ³rmula 1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/">
                                                <span class="titulo">Futebol Nacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/volei/">
                                                <span class="titulo">VÃ´lei</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://sportv.globo.com/">
                                                <span class="titulo">SporTV</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://sportv.globo.com/site/eventos/combate/">
                                                <span class="titulo">Combate</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/basquete/">
                                                <span class="titulo">Basquete</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="globoesportecom">
                                        <div class="submenu-title">globoesporte.com</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://futpedia.globo.com/">
                                                    <span class="titulo">FutpÃ©dia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/cartola-fc/">
                                                    <span class="titulo">Cartola FC</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="sportv">
                                        <div class="submenu-title">sportv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/nba/">
                                                    <span class="titulo">NBA</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/eventos/combate/">
                                                    <span class="titulo">Combate</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/corujao-do-esporte/">
                                                    <span class="titulo">CorujÃ£o do Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/esporte-espetacular/">
                                                    <span class="titulo">Esporte Espetacular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/">
                                                    <span class="titulo">Globo Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://app.globoesporte.globo.com/tv/planeta-extremo/">
                                                    <span class="titulo">Planeta Extremo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sociopremiere.globo.com/">
                                                    <span class="titulo">Premiere</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/">
                                                    <span class="titulo">SporTV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/lutas/tuf-brasil/">
                                                    <span class="titulo">The Ultimate Fighter Brasil</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="futebol">
                                        <div class="submenu-title">futebol</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie B</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-c/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie C</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-do-brasil/">
                                                    <span class="titulo">Copa do Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-sul-americana/">
                                                    <span class="titulo">Copa Sul-Americana</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                    <span class="titulo">Futebol Internacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol Nacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">
                                                    <span class="titulo">Liga dos CampeÃµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/selecao-brasileira/">
                                                    <span class="titulo">SeleÃ§Ã£o Brasileira</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="mais-esportes">
                                        <div class="submenu-title">mais esportes</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/atletismo/">
                                                    <span class="titulo">Atletismo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/basquete/">
                                                    <span class="titulo">Basquete</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eu-atleta/">
                                                    <span class="titulo">Eu Atleta</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                    <span class="titulo">FÃ³rmula 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eventos/futsal/">
                                                    <span class="titulo">Futsal</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/combate/">
                                                    <span class="titulo">MMA</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/natacao/">
                                                    <span class="titulo">NataÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/stock-car/">
                                                    <span class="titulo">StockCar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/tenis/">
                                                    <span class="titulo">TÃªnis</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/volei/">
                                                    <span class="titulo">VÃ´lei</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-globoesporte"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="gshow analytics-product analytics-multi-product">
            <a href="http://gshow.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">gshow</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/" data-menu-id="gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gshow</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="programas">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">programas</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="series">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sÃ©ries</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/programas/webseries/" data-menu-id="series-originais">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sÃ©ries originais</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com/" data-menu-id="rede-globo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">rede globo</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-gshow">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-gshow" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/realities/the-voice-brasil/">
                                                <span class="titulo">The Voice</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/mais-voce/">
                                                <span class="titulo">Mais VocÃª</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/e-de-casa/">
                                                <span class="titulo">Ã de Casa</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://redeglobo.globo.com/programacao.html">
                                                <span class="titulo">ProgramaÃ§Ã£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/">
                                                <span class="titulo">Gshow</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/estrelas/index.html">
                                                <span class="titulo">Estrelas</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/programa-do-jo/">
                                                <span class="titulo">Programa do JÃ´</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://redeglobo.globo.com/">
                                                <span class="titulo">Rede globo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                <span class="titulo">CaldeirÃ£o do Huck</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="gshow">
                                        <div class="submenu-title">gshow</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Bastidores/">
                                                    <span class="titulo">Bastidores</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/como-fazer/">
                                                    <span class="titulo">Como Fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo">
                                                    <span class="titulo">Estilo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/especial-blog/gshow-troll/1.html">
                                                    <span class="titulo">Gshow Troll</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/participe/">
                                                    <span class="titulo">Participe</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas-da-Ana-Maria/">
                                                    <span class="titulo">Receitas da Ana Maria</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/tv/plantao/">
                                                    <span class="titulo">TV</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="programas">
                                        <div class="submenu-title">programas</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://gshow.globo.com/programas/altas-horas/">
                                                    <span class="titulo">Altas Horas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                    <span class="titulo">CaldeirÃ£o do Huck</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                    <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/e-de-casa/">
                                                    <span class="titulo">Ã de Casa</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                    <span class="titulo">Encontro com FÃ¡tima</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/estrelas/index.html">
                                                    <span class="titulo">Estrelas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/">
                                                    <span class="titulo">Mais VocÃª</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/programa-do-jo/">
                                                    <span class="titulo">Programa do JÃ´</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/realities/the-voice-brasil/">
                                                    <span class="titulo">The Voice</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                    <span class="titulo">VÃ­deo Show</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="series">
                                        <div class="submenu-title">sÃ©ries</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/amorteamo/">
                                                    <span class="titulo">Amorteamo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/chapa-quente/">
                                                    <span class="titulo">Chapa Quente</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/ligacoes-perigosas/">
                                                    <span class="titulo">LigaÃ§Ãµes Perigosas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/mister-brau/2015/">
                                                    <span class="titulo">Mister Brau</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/pe-na-cova/2015/">
                                                    <span class="titulo">PÃ© na Cova</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/especial-blog/ta-no-ar-a-tv-na-tv/1.html">
                                                    <span class="titulo">TÃ¡ no ar: a TV na TV </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/zorra/">
                                                    <span class="titulo">Zorra</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="series-originais">
                                        <div class="submenu-title">sÃ©ries originais</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/bichos/no-ar.html">
                                                    <span class="titulo">Bichos</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/como-fazer/no-ar.html">
                                                    <span class="titulo">Como fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/deborah-secco-apresenta/no-ar.html">
                                                    <span class="titulo">Deborah Secco apresenta</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/dulce-delight/">
                                                    <span class="titulo">Dulce Delight</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/laboratorio-do-som/no-ar.html">
                                                    <span class="titulo">LaboratÃ³rio do som</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/morri-na-tv/no-ar.html">
                                                    <span class="titulo">Morri na TV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/os-desatinados-malhacao-seu-lugar-no-mundo/no-ar.html">
                                                    <span class="titulo">Os desatinados</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/sebastiana-quebra-galho/no-ar.html">
                                                    <span class="titulo">Sebastiana Quebra-Galho</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/verdades-secretasdoc/no-ar.html">
                                                    <span class="titulo">Verdades Secretas.doc</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="rede-globo">
                                        <div class="submenu-title">rede globo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/programacao.html">
                                                    <span class="titulo">ProgramaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-gshow"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="famosos-etc analytics-product analytics-multi-product">
            <a href="http://famosos.globo.com" class="analytics-area analytics-id-T">
                <span class="titulo">famosos &amp; etc</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-famosos-etc">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://famosos.globo.com/" data-menu-id="famosos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">famosos</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="moda-estilo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">moda & estilo</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="bem-estar-saude">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">bem-estar & saÃºde</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="casa-decoracao">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">casa & decoraÃ§Ã£o</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="gastronomia">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gastronomia</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="musica-cinema-e-teatro">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mÃºsica, cinema e teatro</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="canais-de-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">canais de tv</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-famosos-etc">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-famosos-etc" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://ego.globo.com/">
                                                <span class="titulo">Ego</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://paparazzo.globo.com/">
                                                <span class="titulo">Paparazzo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://revistaquem.globo.com/">
                                                <span class="titulo">Quem</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/famosos/">
                                                <span class="titulo">Retratos da Vida</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/cultura/kogut/">
                                                <span class="titulo">Patricia Kogut</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://famosos.globo.com/">
                                                <span class="titulo">Famosos</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="famosos">
                                        <div class="submenu-title">famosos</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/">
                                                    <span class="titulo">Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://paparazzo.globo.com/">
                                                    <span class="titulo">Paparazzo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/cultura/kogut/">
                                                    <span class="titulo">Patricia Kogut</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaquem.globo.com/">
                                                    <span class="titulo">Quem</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/famosos/">
                                                    <span class="titulo">Retratos da Vida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="moda-estilo">
                                        <div class="submenu-title">moda &amp; estilo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/moda/index.html">
                                                    <span class="titulo">Moda no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ela.oglobo.globo.com/">
                                                    <span class="titulo">Ela no O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo/">
                                                    <span class="titulo">Estilo no Gshow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaglamour.globo.com/">
                                                    <span class="titulo">Revista Glamour</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gq.globo.com/">
                                                    <span class="titulo">Revista GQ Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistamarieclaire.globo.com/">
                                                    <span class="titulo">Revista Marie Claire </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/moda/">
                                                    <span class="titulo">Moda no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://vogue.globo.com/">
                                                    <span class="titulo">Revista Vogue</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="bem-estar-saude">
                                        <div class="submenu-title">bem-estar &amp; saÃºde</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/beleza/index.html">
                                                    <span class="titulo">Beleza no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/beleza/">
                                                    <span class="titulo">Beleza no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.com.br/bemestar">
                                                    <span class="titulo">Bem Estar no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar no G1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/maes-e-filhos/">
                                                    <span class="titulo">MÃ£es no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/">
                                                    <span class="titulo">Meus 5 Minutos </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://horoscopo.ego.globo.com/">
                                                    <span class="titulo">HorÃ³scopo no Ego</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="casa-decoracao">
                                        <div class="submenu-title">casa &amp; decoraÃ§Ã£o</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/">
                                                    <span class="titulo">Revista Casa e Jardim</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/casa-e-decoracao/">
                                                    <span class="titulo">Casa no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://casavogue.globo.com/">
                                                    <span class="titulo">Casa Vogue </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revista.zapimoveis.com.br/?utm_source=globo.com-etc&amp;utm_medium=botao-casa-decoracao&amp;utm_campaign=Revista-Home">
                                                    <span class="titulo">Revista do Zap</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="gastronomia">
                                        <div class="submenu-title">gastronomia</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/receitas/">
                                                    <span class="titulo">Receitas no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas/index.html">
                                                    <span class="titulo">Receitas no Mais VocÃª </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/blogs/Colheradas/">
                                                    <span class="titulo">Colheradas </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/Casa-e-Comida/">
                                                    <span class="titulo">Casa e Comida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="musica-cinema-e-teatro">
                                        <div class="submenu-title">mÃºsica, cinema e teatro</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/cinema/">
                                                    <span class="titulo">G1 Cinema</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/">
                                                    <span class="titulo">G1 MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">Gshow MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://musica.com.br/">
                                                    <span class="titulo">MÃºsica.com.br</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radiobeat.com.br/">
                                                    <span class="titulo">RADIOBEAT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.agentesevenoteatro.com.br/">
                                                    <span class="titulo">Teatro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="canais-de-tv">
                                        <div class="submenu-title">canais de tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/">
                                                    <span class="titulo">Rede Globo </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalbrasil.globo.com/">
                                                    <span class="titulo">Canal Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://mundogloob.globo.com/">
                                                    <span class="titulo">Gloob</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/">
                                                    <span class="titulo">GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://megapix.globo.com/">
                                                    <span class="titulo">Megapix</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://multishow.globo.com/">
                                                    <span class="titulo">Multishow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://telecine.globo.com/">
                                                    <span class="titulo">Telecine</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://uc.globo.com/">
                                                    <span class="titulo">Universal Channel</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalviva.globo.com/">
                                                    <span class="titulo">Viva</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://canaloff.globo.com">
                                                    <span class="titulo">Off</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-famosos-etc"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="tecnologia analytics-product analytics-multi-product">
            <a href="http://www.techtudo.com.br/" class="analytics-area analytics-id-T">
                <span class="titulo">tecnologia</span>

        
        
            </a>
        </li>
        
    
        <li class="videos analytics-product analytics-multi-product">
            <a href="http://globoplay.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">vÃ­deos</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                            
                                <li class="has-link ">
                                    <a href="http://globoplay.globo.com/" data-menu-id="globo-play">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globo play</span>
                                    </a>
                                </li>
                            
                                <li class="has-link ">
                                    <a href="http://globosatplay.globo.com/" data-menu-id="globosat-play">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globosat play</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-videos">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
</ul>
<!-- gerado em: 2015-12-1216:43:58Z --><div id="assinante-menu"></div></nav></div><div class="header-bottom"></div></header><script>
libby.loadScript('http://s.glbimg.com/en/ho/static/etc/busca/js/jquery.buscaPadrao.v2.compressed.js', function(){var searchArea=document.getElementById('busca-padrao'),
placeholderValue = 'encontre na globo.com';if(typeof searchWidget !== "undefined" && searchWidget){searchWidget(searchArea, {suggestionsEnabled: true,
baseSearchUrl: 'http://www.globo.com',
addClass:'home-search',
placeholder: placeholderValue,
buscaHome: true,
qtd_min_letras_pro_suggest: 2,
qtd_min_letras_pra_busca: 2,
addStyleSheet: ''});}
searchArea.style.display = "block";if(searchArea && searchArea.querySelectorAll){var inputElement=searchArea.querySelectorAll('input')[0];inputElement.onwebkitspeechchange=inputElement.onspeechchange=function (ev) {inputElement.value=inputElement.value.replace(placeholderValue, '');inputElement.form.submit();};}});</script><div id="x60" class="opec-area opec-mobile opec-x60 grid-12"><div id="banner_mobile_topo" class="tag-manager-publicidade-container"></div></div><div id="urgente" class="container urgente analytics-area analytics-id-U "><div class="grid-base super-wide"></div></div><div id="bloco-principal" class="container first-scroll-container clearfix"><div class="grid-base wide analytics-area analytics-id-A"><div class="grid-base narrow pull-left primeira-area hide-mobile"><div class="destaque-principal-vertical-foto-topo destaque destaque-primeiro-scroll principal glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/natureza/ao-vivo/2015/cop21-conferencia-da-onu-sobre-mudancas-climaticas.html" class=" " title="SIGA: acordo global do clima Ã© aprovado durante plenÃ¡ria"><div class="conteudo"><h2>SIGA: acordo global <br />do clima Ã© aprovado durante plenÃ¡ria</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Documento prevÃª fundo de US$ 100 bi; veja pontos" href="http://g1.globo.com/natureza/noticia/2015/12/proposta-final-de-acordo-do-clima-elimina-metas-de-longo-prazo.html">Documento prevÃª fundo de US$ 100 bi; veja pontos</a></div></li></ul></div><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/natureza/noticia/2015/12/ongs-mostram-nas-ruas-de-paris-desacordo-com-acordo-da-cop-21.html" class="foto " title="ONGs fazem ato pelo clima nas ruas de Paris (AP Photo/Thibault Camus)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/hRSKT3a981oLeQOSZvz9gJzbMfQ=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/H30AAbSStRgZLFHVEKL6GH3nV74=/951x892:4758x2735/155x75/s.glbimg.com/jo/g1/f/original/2015/12/12/france_climate_countd_fran_3.jpg" alt="ONGs fazem ato pelo clima nas ruas de Paris (AP Photo/Thibault Camus)" title="ONGs fazem ato pelo clima nas ruas de Paris (AP Photo/Thibault Camus)"
         data-original-image="s2.glbimg.com/H30AAbSStRgZLFHVEKL6GH3nV74=/951x892:4758x2735/155x75/s.glbimg.com/jo/g1/f/original/2015/12/12/france_climate_countd_fran_3.jpg" data-url-smart_horizontal="KjEq7amGBdwPYvedxX_hRTzslT4=/90x56/smart/filters:strip_icc()/" data-url-smart="KjEq7amGBdwPYvedxX_hRTzslT4=/90x56/smart/filters:strip_icc()/" data-url-feature="KjEq7amGBdwPYvedxX_hRTzslT4=/90x56/smart/filters:strip_icc()/" data-url-tablet="wt_ByCizScP2OdG6TIWD6rMLoGk=/160xorig/smart/filters:strip_icc()/" data-url-desktop="ewf83KA6QsUF3BjhCauoMlw7_0M=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>ONGs fazem ato pelo clima nas ruas de Paris</h2></div></a></div></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/minas-gerais/desastre-ambiental-em-mariana/noticia/2015/12/mp-apura-se-obras-foram-iniciadas-antes-de-licenciamento-em-fundao.html" class=" " title="MP investiga licenÃ§as em barragem"><div class="conteudo"><h2>MP investiga licenÃ§as em barragem</h2></div></a></div></div><div class="ultimo-destaque"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://g1.globo.com/sao-paulo/noticia/2015/12/chuva-forte-deixa-sao-paulo-em-estado-de-atencao-neste-sabado.html" class="foto " title="Temporal deixa SP em atenÃ§Ã£o e derruba Ã¡rvore (Andre Luis Lino/VC no G1)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/dULsnEjEuZHpZ7PTIUPtB_cMN3I=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/cBnYY3tUeN7YeqF2alEPZuabZws=/0x61:1700x938/155x80/s.glbimg.com/jo/g1/f/original/2015/12/12/arvore2.jpg" alt="Temporal deixa SP em atenÃ§Ã£o e derruba Ã¡rvore (Andre Luis Lino/VC no G1)" title="Temporal deixa SP em atenÃ§Ã£o e derruba Ã¡rvore (Andre Luis Lino/VC no G1)"
         data-original-image="s2.glbimg.com/cBnYY3tUeN7YeqF2alEPZuabZws=/0x61:1700x938/155x80/s.glbimg.com/jo/g1/f/original/2015/12/12/arvore2.jpg" data-url-smart_horizontal="kHPa1c47zCq1LN_Q1qBHhe6_Ui4=/90x56/smart/filters:strip_icc()/" data-url-smart="kHPa1c47zCq1LN_Q1qBHhe6_Ui4=/90x56/smart/filters:strip_icc()/" data-url-feature="kHPa1c47zCq1LN_Q1qBHhe6_Ui4=/90x56/smart/filters:strip_icc()/" data-url-tablet="Rcq58wI9WGc_3cV5Xl9LiRl9NcM=/160xorig/smart/filters:strip_icc()/" data-url-desktop="nuO4f5pNjmsnnPUwDrWnqxEs2DU=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Temporal deixa SP em atenÃ§Ã£o e derruba Ã¡rvore</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://extra.globo.com/casos-de-policia/crianca-de-dois-anos-morre-baleada-na-zona-norte-do-rio-18280799.html" class="foto " title="RJ: mÃ£e lamenta filho de 2 anos morto em favela (Guilherme Pinto / Extra)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/cUx2NzL6ylYiDRiI6av5Uaq7NUM=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/zn1-nNNA2nBFSZ8ursnyrKeTo1w=/50x31:640x336/155x80/s.glbimg.com/en/ho/f/original/2015/12/12/bebe_morto.jpg" alt="RJ: mÃ£e lamenta filho de 2 anos morto em favela (Guilherme Pinto / Extra)" title="RJ: mÃ£e lamenta filho de 2 anos morto em favela (Guilherme Pinto / Extra)"
         data-original-image="s2.glbimg.com/zn1-nNNA2nBFSZ8ursnyrKeTo1w=/50x31:640x336/155x80/s.glbimg.com/en/ho/f/original/2015/12/12/bebe_morto.jpg" data-url-smart_horizontal="52biukVV8qUiLYt89GxvS0b7Gxc=/90x56/smart/filters:strip_icc()/" data-url-smart="52biukVV8qUiLYt89GxvS0b7Gxc=/90x56/smart/filters:strip_icc()/" data-url-feature="52biukVV8qUiLYt89GxvS0b7Gxc=/90x56/smart/filters:strip_icc()/" data-url-tablet="VtanLh5VDi97CV5ZTZXb3tyw37U=/160xorig/smart/filters:strip_icc()/" data-url-desktop="Fxyx9bATTFosRvhb-sZMC_MhDJU=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>RJ: mÃ£e lamenta filho de 2 anos morto em favela</h2></div></a></div></div></div></div></div><div class="grid-base narrow ultimo hide-mobile"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/futebol-internacional/jogo/12-12-2015/barcelona-deportivo-la-coruna/" class="foto " title="Sem Neymar, BarÃ§a faz 2 a 0, mas sÃ³ empata (Juan Medina / Reuters)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/lN71cXHX3O6QAhXvEFbD17vYsUo=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/zAIqS6xH11pEq6LV1l0ehS2iBiw=/908x189:2028x1055/155x120/s.glbimg.com/es/ge/f/original/2015/12/12/2015-12-12t170242z_1053635940_gf10000263660_rtrmadp_3_soccer-spain.jpg" alt="Sem Neymar, BarÃ§a faz 2 a 0, mas sÃ³ empata (Juan Medina / Reuters)" title="Sem Neymar, BarÃ§a faz 2 a 0, mas sÃ³ empata (Juan Medina / Reuters)"
         data-original-image="s2.glbimg.com/zAIqS6xH11pEq6LV1l0ehS2iBiw=/908x189:2028x1055/155x120/s.glbimg.com/es/ge/f/original/2015/12/12/2015-12-12t170242z_1053635940_gf10000263660_rtrmadp_3_soccer-spain.jpg" data-url-smart_horizontal="x_FFJBqi1kNqV78y_s7YkV1yfUU=/90x56/smart/filters:strip_icc()/" data-url-smart="x_FFJBqi1kNqV78y_s7YkV1yfUU=/90x56/smart/filters:strip_icc()/" data-url-feature="x_FFJBqi1kNqV78y_s7YkV1yfUU=/90x56/smart/filters:strip_icc()/" data-url-tablet="s0WeSqB3piOSM8anM2us-UksVVY=/160xorig/smart/filters:strip_icc()/" data-url-desktop="iMZtX-JqtwWLRk0nS1Bk4hg0D1w=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Sem Neymar, BarÃ§a faz 2 a 0, mas sÃ³ empata</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/eurocopa/noticia/2015/12/franca-alemanha-e-portugal-escapam-de-pedreiras-em-sorteio-da-euro-2016.html" class="foto " title="Euro 2016: grupo da morte tem ItÃ¡lia e BÃ©lgica (Reuters)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/msFzHaCfQg0yEzlol5PvzYNWT-c=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/pUZ15kMaxlxbbf1EewQ74Hc97mI=/144x19:1827x1320/155x120/s.glbimg.com/es/ge/f/original/2015/12/12/2015-12-12t172026z_1684334683_lr1ebcc1c5sfm_rtrmadp_3_soccer-euro-draw_1.jpg" alt="Euro 2016: grupo da morte tem ItÃ¡lia e BÃ©lgica (Reuters)" title="Euro 2016: grupo da morte tem ItÃ¡lia e BÃ©lgica (Reuters)"
         data-original-image="s2.glbimg.com/pUZ15kMaxlxbbf1EewQ74Hc97mI=/144x19:1827x1320/155x120/s.glbimg.com/es/ge/f/original/2015/12/12/2015-12-12t172026z_1684334683_lr1ebcc1c5sfm_rtrmadp_3_soccer-euro-draw_1.jpg" data-url-smart_horizontal="3qNg75k6Z2oMSMy19Uh0DZ6PNDE=/90x56/smart/filters:strip_icc()/" data-url-smart="3qNg75k6Z2oMSMy19Uh0DZ6PNDE=/90x56/smart/filters:strip_icc()/" data-url-feature="3qNg75k6Z2oMSMy19Uh0DZ6PNDE=/90x56/smart/filters:strip_icc()/" data-url-tablet="NiaqhMj380Uj5NZwsN3ZVw0VmOA=/160xorig/smart/filters:strip_icc()/" data-url-desktop="XaxLhyrLsr4FUdWnHQyEfAvQNzk=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Euro 2016: grupo da morte tem ItÃ¡lia e BÃ©lgica</h2></div></a></div></div></div><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="mobile-grid-partial"><a href="http://sportv.globo.com/site/combate/blogs/especial-blog/ultimmato/post/aldo-e-mcgregor-trocam-provocacoes-antes-da-pesagem-veja-o-video.html" class="foto " title="McGregor grita com Aldo: &#39;Vai morrer&#39;; assista (sportv)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/OSbvtxRuEYy9ZNpI1naMRccU4RQ=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/zO8xBw5cCZlsOv_5vcqiPytUxC0=/0x0:168x75/155x70/s.glbimg.com/en/ho/f/original/2015/12/12/conor.jpg" alt="McGregor grita com Aldo: &#39;Vai morrer&#39;; assista (sportv)" title="McGregor grita com Aldo: &#39;Vai morrer&#39;; assista (sportv)"
         data-original-image="s2.glbimg.com/zO8xBw5cCZlsOv_5vcqiPytUxC0=/0x0:168x75/155x70/s.glbimg.com/en/ho/f/original/2015/12/12/conor.jpg" data-url-smart_horizontal="FUHAukQA6bK30VOePZhdDwxvSD0=/90x56/smart/filters:strip_icc()/" data-url-smart="FUHAukQA6bK30VOePZhdDwxvSD0=/90x56/smart/filters:strip_icc()/" data-url-feature="FUHAukQA6bK30VOePZhdDwxvSD0=/90x56/smart/filters:strip_icc()/" data-url-tablet="X04-bAY1H4UyTXyICIOCvIAycqc=/160xorig/smart/filters:strip_icc()/" data-url-desktop="yZ_D1PB_pzeeJt9d3sActhjHqlU=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>McGregor grita com Aldo: &#39;Vai morrer&#39;; assista</h2></div></a></div></div><div class="ultimo-destaque"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-entretenimento analytics-area  analytics-id-M
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/vem-por-ai/noticia/2015/12/romero-descobre-que-ze-maria-e-o-pai-da-filha-de-kiki.html" class="foto " title="&#39;Regra&#39;: Romero vÃª ZÃ© com Kiki (TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/TyYmTKlQfl2Xk1RtXeidoo3wCNA=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/oNlRPuFIfDKg1IvkcOctVAVPDhw=/0x0:720x278/155x60/s.glbimg.com/et/gs/f/original/2015/12/11/ze-maria-e-kiki.jpg" alt="&#39;Regra&#39;: Romero vÃª ZÃ© com Kiki (TV Globo)" title="&#39;Regra&#39;: Romero vÃª ZÃ© com Kiki (TV Globo)"
         data-original-image="s2.glbimg.com/oNlRPuFIfDKg1IvkcOctVAVPDhw=/0x0:720x278/155x60/s.glbimg.com/et/gs/f/original/2015/12/11/ze-maria-e-kiki.jpg" data-url-smart_horizontal="dzc-7bxEUSxKJXL8-fMvDV0GTX0=/90x56/smart/filters:strip_icc()/" data-url-smart="dzc-7bxEUSxKJXL8-fMvDV0GTX0=/90x56/smart/filters:strip_icc()/" data-url-feature="dzc-7bxEUSxKJXL8-fMvDV0GTX0=/90x56/smart/filters:strip_icc()/" data-url-tablet="rlx7rNCgAFgDEf8shZs5s88G-2I=/160xorig/smart/filters:strip_icc()/" data-url-desktop="-0_jMowZRIlPH7Kt4dkQJMLbtao=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>&#39;Regra&#39;: Romero vÃª ZÃ© com Kiki</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="TÃ³ia denunciarÃ¡ Atena<br />" href="http://kogut.oglobo.globo.com/noticias-da-tv/novelas/noticia/2015/12/toia-denunciara-atena-para-dante.html">TÃ³ia denunciarÃ¡ Atena
</a></div></li></ul></div><div class="direita"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/totalmente-demais/vem-por-ai/noticia/2015/12/teaser-comeca-1-eliminatoria-do-concurso-garota-td.html" class="foto " title="&#39;Demais&#39;: Eliza vence 1Âª etapa (Artur Meninea/Gshow)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/5xTpwvk14X2YZ_7jpRdCKS735Uw=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/2s2N43AVz8L4_9aYxej9zRGxky0=/104x60:1234x498/155x60/s.glbimg.com/et/gs/f/original/2015/12/09/carolina-eliza.jpg" alt="&#39;Demais&#39;: Eliza vence 1Âª etapa (Artur Meninea/Gshow)" title="&#39;Demais&#39;: Eliza vence 1Âª etapa (Artur Meninea/Gshow)"
         data-original-image="s2.glbimg.com/2s2N43AVz8L4_9aYxej9zRGxky0=/104x60:1234x498/155x60/s.glbimg.com/et/gs/f/original/2015/12/09/carolina-eliza.jpg" data-url-smart_horizontal="zm20Tn4s5bRLviTQjMUvJm_Hl4Q=/90x56/smart/filters:strip_icc()/" data-url-smart="zm20Tn4s5bRLviTQjMUvJm_Hl4Q=/90x56/smart/filters:strip_icc()/" data-url-feature="zm20Tn4s5bRLviTQjMUvJm_Hl4Q=/90x56/smart/filters:strip_icc()/" data-url-tablet="CI561NqnEtT2nO6tH_Krar5wJlw=/160xorig/smart/filters:strip_icc()/" data-url-desktop="DCALegWbw_Wk5jNgksKdVMBM5fs=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>&#39;Demais&#39;: Eliza vence 1Âª etapa</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Germano Ã© pai de Eliza" href="http://extra.globo.com/tv-e-lazer/telinha/totalmente-demais-germano-pai-de-eliza-18280537.html">Germano Ã© pai de Eliza</a></div></li></ul></div></div></div></div><div class="grid-base wide analytics-area analytics-id-A mobile-grid-base"><div class="destaque-principal-vertical-foto-topo destaque destaque-primeiro-scroll principal glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/natureza/ao-vivo/2015/cop21-conferencia-da-onu-sobre-mudancas-climaticas.html" class=" " title="SIGA: acordo global do clima Ã© aprovado durante plenÃ¡ria"><div class="conteudo"><h2>SIGA: acordo global <br />do clima Ã© aprovado durante plenÃ¡ria</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Documento prevÃª fundo de US$ 100 bi; veja pontos" href="http://g1.globo.com/natureza/noticia/2015/12/proposta-final-de-acordo-do-clima-elimina-metas-de-longo-prazo.html">Documento prevÃª fundo de US$ 100 bi; veja pontos</a></div></li></ul></div><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/natureza/noticia/2015/12/ongs-mostram-nas-ruas-de-paris-desacordo-com-acordo-da-cop-21.html" class="foto " title="ONGs fazem ato pelo clima nas ruas de Paris (AP Photo/Thibault Camus)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/hRSKT3a981oLeQOSZvz9gJzbMfQ=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/H30AAbSStRgZLFHVEKL6GH3nV74=/951x892:4758x2735/155x75/s.glbimg.com/jo/g1/f/original/2015/12/12/france_climate_countd_fran_3.jpg" alt="ONGs fazem ato pelo clima nas ruas de Paris (AP Photo/Thibault Camus)" title="ONGs fazem ato pelo clima nas ruas de Paris (AP Photo/Thibault Camus)"
         data-original-image="s2.glbimg.com/H30AAbSStRgZLFHVEKL6GH3nV74=/951x892:4758x2735/155x75/s.glbimg.com/jo/g1/f/original/2015/12/12/france_climate_countd_fran_3.jpg" data-url-smart_horizontal="KjEq7amGBdwPYvedxX_hRTzslT4=/90x56/smart/filters:strip_icc()/" data-url-smart="KjEq7amGBdwPYvedxX_hRTzslT4=/90x56/smart/filters:strip_icc()/" data-url-feature="KjEq7amGBdwPYvedxX_hRTzslT4=/90x56/smart/filters:strip_icc()/" data-url-tablet="wt_ByCizScP2OdG6TIWD6rMLoGk=/160xorig/smart/filters:strip_icc()/" data-url-desktop="ewf83KA6QsUF3BjhCauoMlw7_0M=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>ONGs fazem ato pelo clima nas ruas de Paris</h2></div></a></div></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/minas-gerais/desastre-ambiental-em-mariana/noticia/2015/12/mp-apura-se-obras-foram-iniciadas-antes-de-licenciamento-em-fundao.html" class=" " title="MP investiga licenÃ§as em barragem"><div class="conteudo"><h2>MP investiga licenÃ§as em barragem</h2></div></a></div></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://g1.globo.com/sao-paulo/noticia/2015/12/chuva-forte-deixa-sao-paulo-em-estado-de-atencao-neste-sabado.html" class="foto " title="Temporal deixa SP em atenÃ§Ã£o e derruba Ã¡rvore (Andre Luis Lino/VC no G1)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/dULsnEjEuZHpZ7PTIUPtB_cMN3I=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/cBnYY3tUeN7YeqF2alEPZuabZws=/0x61:1700x938/155x80/s.glbimg.com/jo/g1/f/original/2015/12/12/arvore2.jpg" alt="Temporal deixa SP em atenÃ§Ã£o e derruba Ã¡rvore (Andre Luis Lino/VC no G1)" title="Temporal deixa SP em atenÃ§Ã£o e derruba Ã¡rvore (Andre Luis Lino/VC no G1)"
         data-original-image="s2.glbimg.com/cBnYY3tUeN7YeqF2alEPZuabZws=/0x61:1700x938/155x80/s.glbimg.com/jo/g1/f/original/2015/12/12/arvore2.jpg" data-url-smart_horizontal="kHPa1c47zCq1LN_Q1qBHhe6_Ui4=/90x56/smart/filters:strip_icc()/" data-url-smart="kHPa1c47zCq1LN_Q1qBHhe6_Ui4=/90x56/smart/filters:strip_icc()/" data-url-feature="kHPa1c47zCq1LN_Q1qBHhe6_Ui4=/90x56/smart/filters:strip_icc()/" data-url-tablet="Rcq58wI9WGc_3cV5Xl9LiRl9NcM=/160xorig/smart/filters:strip_icc()/" data-url-desktop="nuO4f5pNjmsnnPUwDrWnqxEs2DU=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Temporal deixa SP em atenÃ§Ã£o e derruba Ã¡rvore</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://extra.globo.com/casos-de-policia/crianca-de-dois-anos-morre-baleada-na-zona-norte-do-rio-18280799.html" class="foto " title="RJ: mÃ£e lamenta filho de 2 anos morto em favela (Guilherme Pinto / Extra)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/cUx2NzL6ylYiDRiI6av5Uaq7NUM=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/zn1-nNNA2nBFSZ8ursnyrKeTo1w=/50x31:640x336/155x80/s.glbimg.com/en/ho/f/original/2015/12/12/bebe_morto.jpg" alt="RJ: mÃ£e lamenta filho de 2 anos morto em favela (Guilherme Pinto / Extra)" title="RJ: mÃ£e lamenta filho de 2 anos morto em favela (Guilherme Pinto / Extra)"
         data-original-image="s2.glbimg.com/zn1-nNNA2nBFSZ8ursnyrKeTo1w=/50x31:640x336/155x80/s.glbimg.com/en/ho/f/original/2015/12/12/bebe_morto.jpg" data-url-smart_horizontal="52biukVV8qUiLYt89GxvS0b7Gxc=/90x56/smart/filters:strip_icc()/" data-url-smart="52biukVV8qUiLYt89GxvS0b7Gxc=/90x56/smart/filters:strip_icc()/" data-url-feature="52biukVV8qUiLYt89GxvS0b7Gxc=/90x56/smart/filters:strip_icc()/" data-url-tablet="VtanLh5VDi97CV5ZTZXb3tyw37U=/160xorig/smart/filters:strip_icc()/" data-url-desktop="Fxyx9bATTFosRvhb-sZMC_MhDJU=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>RJ: mÃ£e lamenta filho de 2 anos morto em favela</h2></div></a></div></div></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/futebol-internacional/jogo/12-12-2015/barcelona-deportivo-la-coruna/" class="foto " title="Sem Neymar, BarÃ§a faz 2 a 0, mas sÃ³ empata (Juan Medina / Reuters)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/lN71cXHX3O6QAhXvEFbD17vYsUo=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/zAIqS6xH11pEq6LV1l0ehS2iBiw=/908x189:2028x1055/155x120/s.glbimg.com/es/ge/f/original/2015/12/12/2015-12-12t170242z_1053635940_gf10000263660_rtrmadp_3_soccer-spain.jpg" alt="Sem Neymar, BarÃ§a faz 2 a 0, mas sÃ³ empata (Juan Medina / Reuters)" title="Sem Neymar, BarÃ§a faz 2 a 0, mas sÃ³ empata (Juan Medina / Reuters)"
         data-original-image="s2.glbimg.com/zAIqS6xH11pEq6LV1l0ehS2iBiw=/908x189:2028x1055/155x120/s.glbimg.com/es/ge/f/original/2015/12/12/2015-12-12t170242z_1053635940_gf10000263660_rtrmadp_3_soccer-spain.jpg" data-url-smart_horizontal="x_FFJBqi1kNqV78y_s7YkV1yfUU=/90x56/smart/filters:strip_icc()/" data-url-smart="x_FFJBqi1kNqV78y_s7YkV1yfUU=/90x56/smart/filters:strip_icc()/" data-url-feature="x_FFJBqi1kNqV78y_s7YkV1yfUU=/90x56/smart/filters:strip_icc()/" data-url-tablet="s0WeSqB3piOSM8anM2us-UksVVY=/160xorig/smart/filters:strip_icc()/" data-url-desktop="iMZtX-JqtwWLRk0nS1Bk4hg0D1w=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Sem Neymar, BarÃ§a faz 2 a 0, mas sÃ³ empata</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/eurocopa/noticia/2015/12/franca-alemanha-e-portugal-escapam-de-pedreiras-em-sorteio-da-euro-2016.html" class="foto " title="Euro 2016: grupo da morte tem ItÃ¡lia e BÃ©lgica (Reuters)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/msFzHaCfQg0yEzlol5PvzYNWT-c=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/pUZ15kMaxlxbbf1EewQ74Hc97mI=/144x19:1827x1320/155x120/s.glbimg.com/es/ge/f/original/2015/12/12/2015-12-12t172026z_1684334683_lr1ebcc1c5sfm_rtrmadp_3_soccer-euro-draw_1.jpg" alt="Euro 2016: grupo da morte tem ItÃ¡lia e BÃ©lgica (Reuters)" title="Euro 2016: grupo da morte tem ItÃ¡lia e BÃ©lgica (Reuters)"
         data-original-image="s2.glbimg.com/pUZ15kMaxlxbbf1EewQ74Hc97mI=/144x19:1827x1320/155x120/s.glbimg.com/es/ge/f/original/2015/12/12/2015-12-12t172026z_1684334683_lr1ebcc1c5sfm_rtrmadp_3_soccer-euro-draw_1.jpg" data-url-smart_horizontal="3qNg75k6Z2oMSMy19Uh0DZ6PNDE=/90x56/smart/filters:strip_icc()/" data-url-smart="3qNg75k6Z2oMSMy19Uh0DZ6PNDE=/90x56/smart/filters:strip_icc()/" data-url-feature="3qNg75k6Z2oMSMy19Uh0DZ6PNDE=/90x56/smart/filters:strip_icc()/" data-url-tablet="NiaqhMj380Uj5NZwsN3ZVw0VmOA=/160xorig/smart/filters:strip_icc()/" data-url-desktop="XaxLhyrLsr4FUdWnHQyEfAvQNzk=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Euro 2016: grupo da morte tem ItÃ¡lia e BÃ©lgica</h2></div></a></div></div></div><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="mobile-grid-partial"><a href="http://sportv.globo.com/site/combate/blogs/especial-blog/ultimmato/post/aldo-e-mcgregor-trocam-provocacoes-antes-da-pesagem-veja-o-video.html" class="foto " title="McGregor grita com Aldo: &#39;Vai morrer&#39;; assista (sportv)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/OSbvtxRuEYy9ZNpI1naMRccU4RQ=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/zO8xBw5cCZlsOv_5vcqiPytUxC0=/0x0:168x75/155x70/s.glbimg.com/en/ho/f/original/2015/12/12/conor.jpg" alt="McGregor grita com Aldo: &#39;Vai morrer&#39;; assista (sportv)" title="McGregor grita com Aldo: &#39;Vai morrer&#39;; assista (sportv)"
         data-original-image="s2.glbimg.com/zO8xBw5cCZlsOv_5vcqiPytUxC0=/0x0:168x75/155x70/s.glbimg.com/en/ho/f/original/2015/12/12/conor.jpg" data-url-smart_horizontal="FUHAukQA6bK30VOePZhdDwxvSD0=/90x56/smart/filters:strip_icc()/" data-url-smart="FUHAukQA6bK30VOePZhdDwxvSD0=/90x56/smart/filters:strip_icc()/" data-url-feature="FUHAukQA6bK30VOePZhdDwxvSD0=/90x56/smart/filters:strip_icc()/" data-url-tablet="X04-bAY1H4UyTXyICIOCvIAycqc=/160xorig/smart/filters:strip_icc()/" data-url-desktop="yZ_D1PB_pzeeJt9d3sActhjHqlU=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>McGregor grita com Aldo: &#39;Vai morrer&#39;; assista</h2></div></a></div></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-entretenimento analytics-area  analytics-id-M
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/vem-por-ai/noticia/2015/12/romero-descobre-que-ze-maria-e-o-pai-da-filha-de-kiki.html" class="foto " title="&#39;Regra&#39;: Romero vÃª ZÃ© com Kiki (TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/TyYmTKlQfl2Xk1RtXeidoo3wCNA=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/oNlRPuFIfDKg1IvkcOctVAVPDhw=/0x0:720x278/155x60/s.glbimg.com/et/gs/f/original/2015/12/11/ze-maria-e-kiki.jpg" alt="&#39;Regra&#39;: Romero vÃª ZÃ© com Kiki (TV Globo)" title="&#39;Regra&#39;: Romero vÃª ZÃ© com Kiki (TV Globo)"
         data-original-image="s2.glbimg.com/oNlRPuFIfDKg1IvkcOctVAVPDhw=/0x0:720x278/155x60/s.glbimg.com/et/gs/f/original/2015/12/11/ze-maria-e-kiki.jpg" data-url-smart_horizontal="dzc-7bxEUSxKJXL8-fMvDV0GTX0=/90x56/smart/filters:strip_icc()/" data-url-smart="dzc-7bxEUSxKJXL8-fMvDV0GTX0=/90x56/smart/filters:strip_icc()/" data-url-feature="dzc-7bxEUSxKJXL8-fMvDV0GTX0=/90x56/smart/filters:strip_icc()/" data-url-tablet="rlx7rNCgAFgDEf8shZs5s88G-2I=/160xorig/smart/filters:strip_icc()/" data-url-desktop="-0_jMowZRIlPH7Kt4dkQJMLbtao=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>&#39;Regra&#39;: Romero vÃª ZÃ© com Kiki</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="TÃ³ia denunciarÃ¡ Atena<br />" href="http://kogut.oglobo.globo.com/noticias-da-tv/novelas/noticia/2015/12/toia-denunciara-atena-para-dante.html">TÃ³ia denunciarÃ¡ Atena
</a></div></li></ul></div><div class="direita"><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/totalmente-demais/vem-por-ai/noticia/2015/12/teaser-comeca-1-eliminatoria-do-concurso-garota-td.html" class="foto " title="&#39;Demais&#39;: Eliza vence 1Âª etapa (Artur Meninea/Gshow)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/5xTpwvk14X2YZ_7jpRdCKS735Uw=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/2s2N43AVz8L4_9aYxej9zRGxky0=/104x60:1234x498/155x60/s.glbimg.com/et/gs/f/original/2015/12/09/carolina-eliza.jpg" alt="&#39;Demais&#39;: Eliza vence 1Âª etapa (Artur Meninea/Gshow)" title="&#39;Demais&#39;: Eliza vence 1Âª etapa (Artur Meninea/Gshow)"
         data-original-image="s2.glbimg.com/2s2N43AVz8L4_9aYxej9zRGxky0=/104x60:1234x498/155x60/s.glbimg.com/et/gs/f/original/2015/12/09/carolina-eliza.jpg" data-url-smart_horizontal="zm20Tn4s5bRLviTQjMUvJm_Hl4Q=/90x56/smart/filters:strip_icc()/" data-url-smart="zm20Tn4s5bRLviTQjMUvJm_Hl4Q=/90x56/smart/filters:strip_icc()/" data-url-feature="zm20Tn4s5bRLviTQjMUvJm_Hl4Q=/90x56/smart/filters:strip_icc()/" data-url-tablet="CI561NqnEtT2nO6tH_Krar5wJlw=/160xorig/smart/filters:strip_icc()/" data-url-desktop="DCALegWbw_Wk5jNgksKdVMBM5fs=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>&#39;Demais&#39;: Eliza vence 1Âª etapa</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Germano Ã© pai de Eliza" href="http://extra.globo.com/tv-e-lazer/telinha/totalmente-demais-germano-pai-de-eliza-18280537.html">Germano Ã© pai de Eliza</a></div></li></ul></div></div></div><div class="grid-base wide pull-left wide-inferior"></div></div><div id="area-widgets-direita-topo" class="grid-base narrow area-widgets-direta-topo analytics-area analytics-id-V"><div id="ad-position-top1" class="opec"><div id="banner_slim_topo" class="tag-manager-publicidade-container"></div></div><div class="destaque destaque-agrupador-globoplay mobile-grid-full"><a href="http://globoplay.globo.com/" class="go-to analytics-area analytics-id-header">GloboPlay</a><div class="destaque-agrupador-globoplay-container"><div class="stage-container"><ul class="stage analytics-area analytics-id-1 analytics-id-I"><li class="showing"><a href="http://globoplay.globo.com/v/4671733/" title="TÃºlio Maravilha se arrisca e canta sucesso de Zeca" class=""><div class="foto"><div class="foto-overlay"><img src="http://s2.glbimg.com/E9YggkoTxSO11TvhgNMLEdHaKDQ=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/HFP7ACU-xVsVBb6nsbHO7VKpOKU=/335x188/filters:max_age(3600)/s02.video.glbimg.com/deo/vi/33/17/4671733" alt="TÃºlio Maravilha se arrisca e canta sucesso de Zeca" title="TÃºlio Maravilha se arrisca e canta sucesso de Zeca"
                 data-original-image="s2.glbimg.com/HFP7ACU-xVsVBb6nsbHO7VKpOKU=/335x188/filters:max_age(3600)/s02.video.glbimg.com/deo/vi/33/17/4671733" data-url-smart_horizontal="lSVxiwi517L860MkTaPchElctfs=/90x56/smart/filters:strip_icc()/" data-url-smart="lSVxiwi517L860MkTaPchElctfs=/90x56/smart/filters:strip_icc()/" data-url-feature="lSVxiwi517L860MkTaPchElctfs=/90x56/smart/filters:strip_icc()/" data-url-tablet="ZF4kQ0EY5EHm5DtQ-1jl49LSlwY=/160xorig/smart/filters:strip_icc()/" data-url-desktop="uk6Gp-2n0Yqb_t_FNUbgH8Dncu8=/335x188/smart/filters:strip_icc()/"
            /></div><p>7 min</p></div><div class="conteudo"><h3>GLOBO ESPORTE</h3><h2>TÃºlio Maravilha se arrisca e canta sucesso de Zeca</h2></div></a></li><li><a href="http://globoplay.globo.com/v/4671424/" title="Casas prÃ©-fabricadas no Brasil vÃ£o abrigar sÃ­rios " class="next"><div class="foto"><div class="foto-overlay"><img src="http://s2.glbimg.com/0K8gsQR5nBfNhhAD9wDVeToABTM=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/rOpS1cYKJ1GoQ7RDn1uObRIWKOU=/245x130/filters:max_age(3600)/s01.video.glbimg.com/deo/vi/24/14/4671424" alt="Casas prÃ©-fabricadas no Brasil vÃ£o abrigar sÃ­rios " title="Casas prÃ©-fabricadas no Brasil vÃ£o abrigar sÃ­rios "
                 data-original-image="s2.glbimg.com/rOpS1cYKJ1GoQ7RDn1uObRIWKOU=/245x130/filters:max_age(3600)/s01.video.glbimg.com/deo/vi/24/14/4671424" data-url-smart_horizontal="d5kwyAKYeI7orjp43Vomu3K0Il0=/90x56/smart/filters:strip_icc()/" data-url-smart="d5kwyAKYeI7orjp43Vomu3K0Il0=/90x56/smart/filters:strip_icc()/" data-url-feature="d5kwyAKYeI7orjp43Vomu3K0Il0=/90x56/smart/filters:strip_icc()/" data-url-tablet="Sg8y1wqFq2vIg7EEMAqA0YfyXgk=/160xorig/smart/filters:strip_icc()/" data-url-desktop="sw6oICeW3KGv_qmIGw9VfVi_DtQ=/335x188/smart/filters:strip_icc()/"
            /></div></div><div class="conteudo"><h3>Ã DE CASA</h3><h2>Casas prÃ©-fabricadas no Brasil vÃ£o abrigar sÃ­rios </h2></div></a></li><li><a href="http://globoplay.globo.com/v/4667750/" title="Nutricionista dÃ¡ dicas de boas escolhas para festas" class="hidden"><div class="foto"><div class="foto-overlay"><img src="http://s2.glbimg.com/wFUoRqu0tQ3lb0C01TkqqLZ-luM=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/cR_mHqTOo9u0lbXcxLBB02QaJyM=/245x130/filters:max_age(3600)/s03.video.glbimg.com/deo/vi/50/77/4667750" alt="Nutricionista dÃ¡ dicas de boas escolhas para festas" title="Nutricionista dÃ¡ dicas de boas escolhas para festas"
                 data-original-image="s2.glbimg.com/cR_mHqTOo9u0lbXcxLBB02QaJyM=/245x130/filters:max_age(3600)/s03.video.glbimg.com/deo/vi/50/77/4667750" data-url-smart_horizontal="hMW1hCTJyZQ8KSPAtAizF_DdQl8=/90x56/smart/filters:strip_icc()/" data-url-smart="hMW1hCTJyZQ8KSPAtAizF_DdQl8=/90x56/smart/filters:strip_icc()/" data-url-feature="hMW1hCTJyZQ8KSPAtAizF_DdQl8=/90x56/smart/filters:strip_icc()/" data-url-tablet="3jFV8BlhpdhJAhrPAhhr1ZhO9YE=/160xorig/smart/filters:strip_icc()/" data-url-desktop="myxO3wUfQInWeZ5KZQlkIYuduyI=/335x188/smart/filters:strip_icc()/"
            /></div></div><div class="conteudo"><h3>COMO SERÃ?</h3><h2>Nutricionista dÃ¡ dicas de boas escolhas para festas</h2></div></a></li></ul></div><div class="destaque-agrupador-globoplay-bottom-container"><div class="videos-list-container"><ul class="videos-list analytics-area analytics-id-2 analytics-id-I"><li><div class="destaque"><a href="http://globoplay.globo.com/v/4671424/" title="Casas prÃ©-fabricadas no Brasil vÃ£o abrigar sÃ­rios "><div class="foto"><div class="foto-overlay"><img src="http://s2.glbimg.com/0K8gsQR5nBfNhhAD9wDVeToABTM=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/rOpS1cYKJ1GoQ7RDn1uObRIWKOU=/245x130/filters:max_age(3600)/s01.video.glbimg.com/deo/vi/24/14/4671424" alt="Casas prÃ©-fabricadas no Brasil vÃ£o abrigar sÃ­rios " title="Casas prÃ©-fabricadas no Brasil vÃ£o abrigar sÃ­rios "
                         data-original-image="s2.glbimg.com/rOpS1cYKJ1GoQ7RDn1uObRIWKOU=/245x130/filters:max_age(3600)/s01.video.glbimg.com/deo/vi/24/14/4671424" data-url-smart_horizontal="d5kwyAKYeI7orjp43Vomu3K0Il0=/90x56/smart/filters:strip_icc()/" data-url-smart="d5kwyAKYeI7orjp43Vomu3K0Il0=/90x56/smart/filters:strip_icc()/" data-url-feature="d5kwyAKYeI7orjp43Vomu3K0Il0=/90x56/smart/filters:strip_icc()/" data-url-tablet="jRrLLrOqoVj7WGPd7yetE2gxCv4=/85x60/smart/filters:strip_icc()/" data-url-desktop="CvxalpUtoim4ioGfUWqR4qPDCLk=/138x78/smart/filters:strip_icc()/"
                    /></div></div><div class="conteudo"><h3>Ã DE CASA</h3><h2>Casas prÃ©-fabricadas no Brasil vÃ£o abrigar sÃ­rios </h2></div></a></div></li><li><div class="destaque"><a href="http://globoplay.globo.com/v/4667750/" title="Nutricionista dÃ¡ dicas de boas escolhas para festas"><div class="foto"><div class="foto-overlay"><img src="http://s2.glbimg.com/wFUoRqu0tQ3lb0C01TkqqLZ-luM=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/cR_mHqTOo9u0lbXcxLBB02QaJyM=/245x130/filters:max_age(3600)/s03.video.glbimg.com/deo/vi/50/77/4667750" alt="Nutricionista dÃ¡ dicas de boas escolhas para festas" title="Nutricionista dÃ¡ dicas de boas escolhas para festas"
                         data-original-image="s2.glbimg.com/cR_mHqTOo9u0lbXcxLBB02QaJyM=/245x130/filters:max_age(3600)/s03.video.glbimg.com/deo/vi/50/77/4667750" data-url-smart_horizontal="hMW1hCTJyZQ8KSPAtAizF_DdQl8=/90x56/smart/filters:strip_icc()/" data-url-smart="hMW1hCTJyZQ8KSPAtAizF_DdQl8=/90x56/smart/filters:strip_icc()/" data-url-feature="hMW1hCTJyZQ8KSPAtAizF_DdQl8=/90x56/smart/filters:strip_icc()/" data-url-tablet="wf2dWKv3dfxvC3U0bVaEw1JH5Zk=/85x60/smart/filters:strip_icc()/" data-url-desktop="FTTM76jqNc_ixbJLKKH8dfeyGg8=/138x78/smart/filters:strip_icc()/"
                    /></div></div><div class="conteudo"><h3>COMO SERÃ?</h3><h2>Nutricionista dÃ¡ dicas de boas escolhas para festas</h2></div></a></div></li><li><div class="destaque"><a href="http://globoplay.globo.com/v/4671733/" title="TÃºlio Maravilha se arrisca e canta sucesso de Zeca"><div class="foto"><div class="foto-overlay"><img src="http://s2.glbimg.com/E9YggkoTxSO11TvhgNMLEdHaKDQ=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/HFP7ACU-xVsVBb6nsbHO7VKpOKU=/335x188/filters:max_age(3600)/s02.video.glbimg.com/deo/vi/33/17/4671733" alt="TÃºlio Maravilha se arrisca e canta sucesso de Zeca" title="TÃºlio Maravilha se arrisca e canta sucesso de Zeca"
                         data-original-image="s2.glbimg.com/HFP7ACU-xVsVBb6nsbHO7VKpOKU=/335x188/filters:max_age(3600)/s02.video.glbimg.com/deo/vi/33/17/4671733" data-url-smart_horizontal="lSVxiwi517L860MkTaPchElctfs=/90x56/smart/filters:strip_icc()/" data-url-smart="lSVxiwi517L860MkTaPchElctfs=/90x56/smart/filters:strip_icc()/" data-url-feature="lSVxiwi517L860MkTaPchElctfs=/90x56/smart/filters:strip_icc()/" data-url-tablet="3sOhWuw0famp03792kmkNYgSiuE=/85x60/smart/filters:strip_icc()/" data-url-desktop="DlbISmNxJPSVqhl0rEYGYCNu4QI=/138x78/smart/filters:strip_icc()/"
                    /><p>7 min</p></div></div><div class="conteudo"><h3>GLOBO ESPORTE</h3><h2>TÃºlio Maravilha se arrisca e canta sucesso de Zeca</h2></div></a></div></li></ul></div><a class="more" href="http://globoplay.globo.com/">
            MAIS VÃDEOS
              <svg width="9px" height="6px" viewBox="0 0 9 6" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M8.176,2.996 L4.242,0.14 L4.41,1.946 L0.658,1.946 L0.658,4.046 L4.41,4.046 L4.242,5.866 L8.176,2.996 Z"></path></svg></a></div></div><div class="destaque-agrupador-globoplay-container-responsive"><div class="slider"><ul><li class="analytics-area analytics-id-1 analytics-id-I"><div class="destaque"><a href="http://globoplay.globo.com/v/4671733/" title="TÃºlio Maravilha se arrisca e canta sucesso de Zeca"><div class="foto"><div class="foto-overlay"><img src="http://s2.glbimg.com/E9YggkoTxSO11TvhgNMLEdHaKDQ=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/HFP7ACU-xVsVBb6nsbHO7VKpOKU=/335x188/filters:max_age(3600)/s02.video.glbimg.com/deo/vi/33/17/4671733" alt="TÃºlio Maravilha se arrisca e canta sucesso de Zeca" title="TÃºlio Maravilha se arrisca e canta sucesso de Zeca"
                         data-original-image="s2.glbimg.com/HFP7ACU-xVsVBb6nsbHO7VKpOKU=/335x188/filters:max_age(3600)/s02.video.glbimg.com/deo/vi/33/17/4671733" data-url-smart_horizontal="lSVxiwi517L860MkTaPchElctfs=/90x56/smart/filters:strip_icc()/" data-url-smart="lSVxiwi517L860MkTaPchElctfs=/90x56/smart/filters:strip_icc()/" data-url-feature="lSVxiwi517L860MkTaPchElctfs=/90x56/smart/filters:strip_icc()/" data-url-tablet="3sOhWuw0famp03792kmkNYgSiuE=/85x60/smart/filters:strip_icc()/" data-url-desktop="DlbISmNxJPSVqhl0rEYGYCNu4QI=/138x78/smart/filters:strip_icc()/"
                    /><p>7 min</p></div></div><div class="conteudo"><h3>GLOBO ESPORTE</h3><h2>TÃºlio Maravilha se arrisca e canta sucesso de Zeca</h2></div></a></div></li><li class="analytics-area analytics-id-2 analytics-id-I"><div class="destaque"><a href="http://globoplay.globo.com/v/4671424/" title="Casas prÃ©-fabricadas no Brasil vÃ£o abrigar sÃ­rios "><div class="foto"><div class="foto-overlay"><img src="http://s2.glbimg.com/0K8gsQR5nBfNhhAD9wDVeToABTM=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/rOpS1cYKJ1GoQ7RDn1uObRIWKOU=/245x130/filters:max_age(3600)/s01.video.glbimg.com/deo/vi/24/14/4671424" alt="Casas prÃ©-fabricadas no Brasil vÃ£o abrigar sÃ­rios " title="Casas prÃ©-fabricadas no Brasil vÃ£o abrigar sÃ­rios "
                         data-original-image="s2.glbimg.com/rOpS1cYKJ1GoQ7RDn1uObRIWKOU=/245x130/filters:max_age(3600)/s01.video.glbimg.com/deo/vi/24/14/4671424" data-url-smart_horizontal="d5kwyAKYeI7orjp43Vomu3K0Il0=/90x56/smart/filters:strip_icc()/" data-url-smart="d5kwyAKYeI7orjp43Vomu3K0Il0=/90x56/smart/filters:strip_icc()/" data-url-feature="d5kwyAKYeI7orjp43Vomu3K0Il0=/90x56/smart/filters:strip_icc()/" data-url-tablet="jRrLLrOqoVj7WGPd7yetE2gxCv4=/85x60/smart/filters:strip_icc()/" data-url-desktop="CvxalpUtoim4ioGfUWqR4qPDCLk=/138x78/smart/filters:strip_icc()/"
                    /></div></div><div class="conteudo"><h3>Ã DE CASA</h3><h2>Casas prÃ©-fabricadas no Brasil vÃ£o abrigar sÃ­rios </h2></div></a></div></li><li class="analytics-area analytics-id-3 analytics-id-I"><div class="destaque"><a href="http://globoplay.globo.com/v/4667750/" title="Nutricionista dÃ¡ dicas de boas escolhas para festas"><div class="foto"><div class="foto-overlay"><img src="http://s2.glbimg.com/wFUoRqu0tQ3lb0C01TkqqLZ-luM=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/cR_mHqTOo9u0lbXcxLBB02QaJyM=/245x130/filters:max_age(3600)/s03.video.glbimg.com/deo/vi/50/77/4667750" alt="Nutricionista dÃ¡ dicas de boas escolhas para festas" title="Nutricionista dÃ¡ dicas de boas escolhas para festas"
                         data-original-image="s2.glbimg.com/cR_mHqTOo9u0lbXcxLBB02QaJyM=/245x130/filters:max_age(3600)/s03.video.glbimg.com/deo/vi/50/77/4667750" data-url-smart_horizontal="hMW1hCTJyZQ8KSPAtAizF_DdQl8=/90x56/smart/filters:strip_icc()/" data-url-smart="hMW1hCTJyZQ8KSPAtAizF_DdQl8=/90x56/smart/filters:strip_icc()/" data-url-feature="hMW1hCTJyZQ8KSPAtAizF_DdQl8=/90x56/smart/filters:strip_icc()/" data-url-tablet="wf2dWKv3dfxvC3U0bVaEw1JH5Zk=/85x60/smart/filters:strip_icc()/" data-url-desktop="FTTM76jqNc_ixbJLKKH8dfeyGg8=/138x78/smart/filters:strip_icc()/"
                    /></div></div><div class="conteudo"><h3>COMO SERÃ?</h3><h2>Nutricionista dÃ¡ dicas de boas escolhas para festas</h2></div></a></div></li></ul></div><div class="page-marker"><li class="active"><div></div></li><li><div></div></li><li><div></div></li></div></div></div></div></div><div class="separator-first-scroll-container"><section><div><div id="opec-banner-middle-container" class="opec-banner-middle-container"><div id="ad-position-middle" class="opec"><div id="banner_slb_meio" class="tag-manager-publicidade-container"></div></div></div></div></section></div><div id="x62" class="opec-area opec-mobile opec-x62 grid-12"><div id="banner_mobile_meio" class="tag-manager-publicidade-container"></div></div><div id="container-columns" class="container columns clearfix glb-area-colunas"><section class="area news-column   last-has-photo
            
        
        "><div class="destaques-container closed analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/sao-paulo/sorocaba-jundiai/noticia/2015/12/motociclista-rodopia-no-ar-apos-ser-atingido-por-carro-guiado-por-menor.html" class="foto" title="Carro guiado por menor faz manobra proibida e atinge motociclista; assista (ReproduÃ§Ã£o)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/g6mbQNTl2gWhOqJ8eVULBCSwCoU=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/S5H_tpa4N0zq3A2s8-oteuuEnHs=/439x0:1141x377/335x180/s.glbimg.com/en/ho/f/original/2015/12/12/acidente.jpg" alt="Carro guiado por menor faz manobra proibida e atinge motociclista; assista (ReproduÃ§Ã£o)" title="Carro guiado por menor faz manobra proibida e atinge motociclista; assista (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/S5H_tpa4N0zq3A2s8-oteuuEnHs=/439x0:1141x377/335x180/s.glbimg.com/en/ho/f/original/2015/12/12/acidente.jpg" data-url-smart_horizontal="ckI0MyqYLkymiAVWLnNuYfk-fcc=/90x0/smart/filters:strip_icc()/" data-url-smart="ckI0MyqYLkymiAVWLnNuYfk-fcc=/90x0/smart/filters:strip_icc()/" data-url-feature="ckI0MyqYLkymiAVWLnNuYfk-fcc=/90x0/smart/filters:strip_icc()/" data-url-tablet="udBufHkrxqpiWN6ImFS4G2F7PjM=/220x125/smart/filters:strip_icc()/" data-url-desktop="1duv-g2M9aD-XRBf8gm-nTTsg5A=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Carro guiado por menor faz manobra proibida e atinge motociclista; assista</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-12 15:25:22" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/sao-paulo/noticia/2015/12/atriz-nydia-licia-morre-aos-89-anos-em-sao-paulo.html" class="foto" title="Morre em SP, aos 89 anos, a atriz, diretora e produtora Nydia Licia; ela teve cÃ¢ncer (reproduÃ§Ã£o / tv globo)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/aNu_a4BoKsVRkoHCToeX6CxZIEw=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/K6hO5Cktcn4bmjnD-SIXuyNMqmY=/0x0:120x80/120x80/s.glbimg.com/en/ho/f/original/2015/12/12/nydia-licia.jpg" alt="Morre em SP, aos 89 anos, a atriz, diretora e produtora Nydia Licia; ela teve cÃ¢ncer (reproduÃ§Ã£o / tv globo)" title="Morre em SP, aos 89 anos, a atriz, diretora e produtora Nydia Licia; ela teve cÃ¢ncer (reproduÃ§Ã£o / tv globo)"
                data-original-image="s2.glbimg.com/K6hO5Cktcn4bmjnD-SIXuyNMqmY=/0x0:120x80/120x80/s.glbimg.com/en/ho/f/original/2015/12/12/nydia-licia.jpg" data-url-smart_horizontal="u0MTJp4cpO0zSAwtZMs6wSF7Znw=/90x0/smart/filters:strip_icc()/" data-url-smart="u0MTJp4cpO0zSAwtZMs6wSF7Znw=/90x0/smart/filters:strip_icc()/" data-url-feature="u0MTJp4cpO0zSAwtZMs6wSF7Znw=/90x0/smart/filters:strip_icc()/" data-url-tablet="z0nGAWE26JRRDQzSzXbTWtfCnAo=/70x50/smart/filters:strip_icc()/" data-url-desktop="60_G0jjJBZhFwQLDTMWTvCT0Log=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Morre em SP, aos 89 anos, a atriz, diretora e produtora Nydia Licia; ela teve cÃ¢ncer</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-12 16:27:23" class="chamada chamada-principal mobile-grid-full"><a href="http://especiais.g1.globo.com/educacao/2015/voce-e-bom-em-matematica/" class="foto" title="Nem os universitÃ¡rios dominam matemÃ¡tica bÃ¡sica: clique e faÃ§a o teste (Editoria de Arte / G1)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/tSD-B8l84c7Ze1jyTYHChVnZvPU=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/GudA5jvgWIiNuvLiWh-UjoXPcHQ=/331x97:1466x853/120x80/s.glbimg.com/jo/g1/f/original/2015/12/12/matematica.jpg" alt="Nem os universitÃ¡rios dominam matemÃ¡tica bÃ¡sica: clique e faÃ§a o teste (Editoria de Arte / G1)" title="Nem os universitÃ¡rios dominam matemÃ¡tica bÃ¡sica: clique e faÃ§a o teste (Editoria de Arte / G1)"
                data-original-image="s2.glbimg.com/GudA5jvgWIiNuvLiWh-UjoXPcHQ=/331x97:1466x853/120x80/s.glbimg.com/jo/g1/f/original/2015/12/12/matematica.jpg" data-url-smart_horizontal="pnswzAbNhOTdDYCqp3XbhOpYjNY=/90x0/smart/filters:strip_icc()/" data-url-smart="pnswzAbNhOTdDYCqp3XbhOpYjNY=/90x0/smart/filters:strip_icc()/" data-url-feature="pnswzAbNhOTdDYCqp3XbhOpYjNY=/90x0/smart/filters:strip_icc()/" data-url-tablet="X8gqofUjsKt8Q6gY1V7-3UzuLNQ=/70x50/smart/filters:strip_icc()/" data-url-desktop="zHe4Cc38s6s6mPgFi12nHKSjakc=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Nem os universitÃ¡rios dominam matemÃ¡tica bÃ¡sica: clique e faÃ§a o teste</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-12 15:45:08" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/bemestar/noticia/2015/12/conheca-cinco-focos-de-reproducao-do-mosquito-aedes-aegypti-frequentemente-ignorados.html" class="" title="Veja cinco focos de reproduÃ§Ã£o do mosquito Aedes aegypti frequentemente ignorados (reproduÃ§Ã£o / globonews)" rel="bookmark"><span class="conteudo"><p>Veja cinco focos de reproduÃ§Ã£o do mosquito Aedes aegypti frequentemente ignorados</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-12 15:06:32" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/distrito-federal/noticia/2015/12/noivo-que-comprava-maconha-antes-de-casamento-e-preso-e-perde-festa.html" class="foto" title="Noivo Ã© preso quando comprava droga e perde festa de casamento no DF (PolÃ­cia Militar/DivulgaÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/QNzINk4uHjv1RuikEYVvA5vISl4=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/aIylMFSIyney5IbtS9mVll_1hyg=/130x44:1649x1056/120x80/s.glbimg.com/jo/g1/f/original/2015/12/11/droga.jpg" alt="Noivo Ã© preso quando comprava droga e perde festa de casamento no DF (PolÃ­cia Militar/DivulgaÃ§Ã£o)" title="Noivo Ã© preso quando comprava droga e perde festa de casamento no DF (PolÃ­cia Militar/DivulgaÃ§Ã£o)"
                data-original-image="s2.glbimg.com/aIylMFSIyney5IbtS9mVll_1hyg=/130x44:1649x1056/120x80/s.glbimg.com/jo/g1/f/original/2015/12/11/droga.jpg" data-url-smart_horizontal="bUQQskeBZncnO902_41cvpJCtyg=/90x0/smart/filters:strip_icc()/" data-url-smart="bUQQskeBZncnO902_41cvpJCtyg=/90x0/smart/filters:strip_icc()/" data-url-feature="bUQQskeBZncnO902_41cvpJCtyg=/90x0/smart/filters:strip_icc()/" data-url-tablet="EVE4MibXPZn44dQxkWvOObbwc2M=/70x50/smart/filters:strip_icc()/" data-url-desktop="g7FVlv4l-Vzj6hCyYJ2B9Wb_HP4=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Noivo Ã© preso quando comprava droga e perde festa de casamento no DF</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-12 15:31:30" class="chamada chamada-principal mobile-grid-full"><a href="http://www.techtudo.com.br/review/moto-x-play.html" class="foto" title="Moto X ganha versÃ£o econÃ´mica e vira &#39;febre&#39;; veja se vale a pena (Lucas Mendes/TechTudo)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/fgHcHw51L6IpEkF-omYYqJZmDO8=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/ULwIYuoOM19gbEJrgowpuPyJpV0=/0x0:2589x1728/120x80/s.glbimg.com/po/tt2/f/original/2015/11/30/moto-x-play-4.jpg" alt="Moto X ganha versÃ£o econÃ´mica e vira &#39;febre&#39;; veja se vale a pena (Lucas Mendes/TechTudo)" title="Moto X ganha versÃ£o econÃ´mica e vira &#39;febre&#39;; veja se vale a pena (Lucas Mendes/TechTudo)"
                data-original-image="s2.glbimg.com/ULwIYuoOM19gbEJrgowpuPyJpV0=/0x0:2589x1728/120x80/s.glbimg.com/po/tt2/f/original/2015/11/30/moto-x-play-4.jpg" data-url-smart_horizontal="m9nRQi72XRB8W7-KqCGV0TYCQpg=/90x0/smart/filters:strip_icc()/" data-url-smart="m9nRQi72XRB8W7-KqCGV0TYCQpg=/90x0/smart/filters:strip_icc()/" data-url-feature="m9nRQi72XRB8W7-KqCGV0TYCQpg=/90x0/smart/filters:strip_icc()/" data-url-tablet="RE-mn0ebeUEvOQqexjDqdxi2hT8=/70x50/smart/filters:strip_icc()/" data-url-desktop="4y3apyxc6y8QrGOPWKQcs20vprY=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Moto X ganha versÃ£o econÃ´mica e vira &#39;febre&#39;; veja se vale a pena</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-12 13:19:20" class="chamada chamada-principal mobile-grid-full"><a href="http://www.techtudo.com.br/tudo-sobre/clean-ram-memory-and-task-manage.html?" class="" title="App &#39;limpaâ arquivos desnecessÃ¡rios do seu Android e deixa o smartphone voando; baixe" rel="bookmark"><span class="conteudo"><p>App &#39;limpaâ arquivos desnecessÃ¡rios do seu Android e deixa o smartphone voando; baixe</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-12 15:56:01" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/12/fernanda-montenegro-inconcebivel-que-nao-se-tenha-mais-marilia-pera.html" class="foto" title="Fernanda Montenegro se emociona na missa de 7Âº dia de MarÃ­lia PÃªra (Marcos Serra Lima/ EGO)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/mbYrbddS76GjIQ0dnqE3HWQ93dw=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/ZakFHkAnRQorDd0qy9Hhk3sqEwU=/1189x411:1866x863/120x80/s.glbimg.com/jo/eg/f/original/2015/12/12/t0a7404.jpg" alt="Fernanda Montenegro se emociona na missa de 7Âº dia de MarÃ­lia PÃªra (Marcos Serra Lima/ EGO)" title="Fernanda Montenegro se emociona na missa de 7Âº dia de MarÃ­lia PÃªra (Marcos Serra Lima/ EGO)"
                data-original-image="s2.glbimg.com/ZakFHkAnRQorDd0qy9Hhk3sqEwU=/1189x411:1866x863/120x80/s.glbimg.com/jo/eg/f/original/2015/12/12/t0a7404.jpg" data-url-smart_horizontal="SWxirsWKVXchHDj0xaLj6tMmZPQ=/90x0/smart/filters:strip_icc()/" data-url-smart="SWxirsWKVXchHDj0xaLj6tMmZPQ=/90x0/smart/filters:strip_icc()/" data-url-feature="SWxirsWKVXchHDj0xaLj6tMmZPQ=/90x0/smart/filters:strip_icc()/" data-url-tablet="zF89NdkBA-eFAdbYazC7ejsFASE=/70x50/smart/filters:strip_icc()/" data-url-desktop="HwlKK7o79Nsi7osJsAqoifblqy0=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Fernanda Montenegro se emociona na missa de 7Âº dia de MarÃ­lia PÃªra</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-12 08:01:56" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/bemestar/VC-no-Bem-Estar/noticia/2015/12/mineiro-emagrece-36-kg-em-5-meses-com-dieta-da-substituicao.html" class="foto" title="Mineiro monta &#39;dieta da substituiÃ§Ã£o&#39; e perde 36kg: &#39;De sedentÃ¡rio a proativo&#39; (Breno Fagundes/Arquivo pessoal)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/yznzubAh40NsO20naqyl2Q7fnU0=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/UUd0s4HN9QCpnc0o86_vACvQKbs=/0x0:120x80/120x80/s.glbimg.com/en/ho/f/original/2015/12/12/emagrece_V2I25AM.jpg" alt="Mineiro monta &#39;dieta da substituiÃ§Ã£o&#39; e perde 36kg: &#39;De sedentÃ¡rio a proativo&#39; (Breno Fagundes/Arquivo pessoal)" title="Mineiro monta &#39;dieta da substituiÃ§Ã£o&#39; e perde 36kg: &#39;De sedentÃ¡rio a proativo&#39; (Breno Fagundes/Arquivo pessoal)"
                data-original-image="s2.glbimg.com/UUd0s4HN9QCpnc0o86_vACvQKbs=/0x0:120x80/120x80/s.glbimg.com/en/ho/f/original/2015/12/12/emagrece_V2I25AM.jpg" data-url-smart_horizontal="mL--WUzbcOlceHopZ22W1ib5tPI=/90x0/smart/filters:strip_icc()/" data-url-smart="mL--WUzbcOlceHopZ22W1ib5tPI=/90x0/smart/filters:strip_icc()/" data-url-feature="mL--WUzbcOlceHopZ22W1ib5tPI=/90x0/smart/filters:strip_icc()/" data-url-tablet="vnRYQYaGKW1UinUfVW4zCsreeiE=/70x50/smart/filters:strip_icc()/" data-url-desktop="OgQ3d7PBaVvu4f_Y-6VftUXVRkg=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Mineiro monta &#39;dieta da substituiÃ§Ã£o&#39; e perde 36kg: &#39;De sedentÃ¡rio a proativo&#39;</p></span></a></div></div><div class="show-more"><span class="setinha-show-more"></span></div></div><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-noticias
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "mogi-e-suzano": {"ordering": 1, "url": "http://g1.globo.com/sp/mogi-das-cruzes-suzano", "name": "Mogi e Suzano"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "mogi-e-suzano": {"ordering": 1, "url": "http://g1.globo.com/sp/mogi-das-cruzes-suzano", "name": "Mogi e Suzano"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}});}
</script><div id="libby-home-widget-economia" class="libby-home-widgetBase libby-home-widget-economia analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais notÃ­cias" href="http://g1.globo.com/"><span class="text">mais notÃ­cias</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area sports-column   last-has-photo
            
        
        "><div class="destaques-container closed analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/palmeiras/noticia/2015/12/marcos-eterniza-busto-no-palestra-e-brinca-beleza-nunca-foi-meu-forte.html" class="foto" title="12/12: &#39;SÃ£o Marcos&#39; inaugura busto no Paletras e brinca sobre a sua beleza (Tossiro Neto)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/Y75LjPlHbPwGRIVg3CWmleHiHmU=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/JSOIktjTkA3XsQt9tI102-qVngQ=/0x652:1628x1528/335x180/s.glbimg.com/es/ge/f/original/2015/12/12/img_20151212_121949_1.jpg" alt="12/12: &#39;SÃ£o Marcos&#39; inaugura busto no Paletras e brinca sobre a sua beleza (Tossiro Neto)" title="12/12: &#39;SÃ£o Marcos&#39; inaugura busto no Paletras e brinca sobre a sua beleza (Tossiro Neto)"
                data-original-image="s2.glbimg.com/JSOIktjTkA3XsQt9tI102-qVngQ=/0x652:1628x1528/335x180/s.glbimg.com/es/ge/f/original/2015/12/12/img_20151212_121949_1.jpg" data-url-smart_horizontal="-_6n_fMPoinJLCcQSE6Nmgl-OwY=/90x0/smart/filters:strip_icc()/" data-url-smart="-_6n_fMPoinJLCcQSE6Nmgl-OwY=/90x0/smart/filters:strip_icc()/" data-url-feature="-_6n_fMPoinJLCcQSE6Nmgl-OwY=/90x0/smart/filters:strip_icc()/" data-url-tablet="HHTWu04qmYXqT-NRh_XfPneG5Gw=/220x125/smart/filters:strip_icc()/" data-url-desktop="DHiaXJlmx23XOUMIzCCXhAirDuU=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>12/12: &#39;SÃ£o Marcos&#39; inaugura busto no Paletras e brinca sobre a sua beleza</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-12 15:57:48" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/rs/futebol/noticia/2015/12/fernando-bob-assina-contrato-de-tres-anos-e-e-primeiro-reforco-do-inter.html" class="foto" title="Volante Fernando Bob assina contrato de trÃªs anos e Ã© 1Âº reforÃ§o do Inter (Arquivo Pessoal)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/BR4_TOjQGr-sXvjZbPtC01cezWY=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/F-VJrk0q89hjcSQttKcBtLDDIKQ=/223x58:521x256/120x80/s.glbimg.com/es/ge/f/original/2015/12/12/bob.jpg" alt="Volante Fernando Bob assina contrato de trÃªs anos e Ã© 1Âº reforÃ§o do Inter (Arquivo Pessoal)" title="Volante Fernando Bob assina contrato de trÃªs anos e Ã© 1Âº reforÃ§o do Inter (Arquivo Pessoal)"
                data-original-image="s2.glbimg.com/F-VJrk0q89hjcSQttKcBtLDDIKQ=/223x58:521x256/120x80/s.glbimg.com/es/ge/f/original/2015/12/12/bob.jpg" data-url-smart_horizontal="qh2P1mbMPobT-v_DTrJJYE1-p5k=/90x0/smart/filters:strip_icc()/" data-url-smart="qh2P1mbMPobT-v_DTrJJYE1-p5k=/90x0/smart/filters:strip_icc()/" data-url-feature="qh2P1mbMPobT-v_DTrJJYE1-p5k=/90x0/smart/filters:strip_icc()/" data-url-tablet="BrcVk8jCtNxMvnDwDD1lb12A0ms=/70x50/smart/filters:strip_icc()/" data-url-desktop="TLSUK-RPwqWob13obWQ3gSCEQVY=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Volante Fernando Bob assina contrato de trÃªs anos e Ã© 1Âº reforÃ§o do Inter</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-12 10:19:36" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/blogs/especial-blog/brasil-mundial-fc/post/time-ideal-de-ibra-tem-zidane-messi-e-ronaldos-nao-preciso-de-zagueiros.html" class="foto" title="Time ideal de Ibra tem Zidane, Messi e Ronaldos: &#39;NÃ£o preciso de zagueiros&#39; (AFP)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/dTAOYWO-53YcDcdds5VSDlGZOzI=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/HvQjDItvLCbsjqp07HoTVVQHco8=/0x0:1991x1327/120x80/s.glbimg.com/es/ge/f/original/2015/12/01/000_arp4354147.jpg" alt="Time ideal de Ibra tem Zidane, Messi e Ronaldos: &#39;NÃ£o preciso de zagueiros&#39; (AFP)" title="Time ideal de Ibra tem Zidane, Messi e Ronaldos: &#39;NÃ£o preciso de zagueiros&#39; (AFP)"
                data-original-image="s2.glbimg.com/HvQjDItvLCbsjqp07HoTVVQHco8=/0x0:1991x1327/120x80/s.glbimg.com/es/ge/f/original/2015/12/01/000_arp4354147.jpg" data-url-smart_horizontal="34XJbIZP1_C28OETJTTLdUhJSPc=/90x0/smart/filters:strip_icc()/" data-url-smart="34XJbIZP1_C28OETJTTLdUhJSPc=/90x0/smart/filters:strip_icc()/" data-url-feature="34XJbIZP1_C28OETJTTLdUhJSPc=/90x0/smart/filters:strip_icc()/" data-url-tablet="SRIZ78siNMS21eThXQHjvFTdu3g=/70x50/smart/filters:strip_icc()/" data-url-desktop="1pYzTg9iLaa2ZAmB3w-Xikc2LLM=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Time ideal de Ibra tem Zidane, Messi e Ronaldos: &#39;NÃ£o preciso de zagueiros&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/corinthians/noticia/2015/12/corinthians-aguarda-documento-por-jadson-e-pode-faturar-r-6-milhoes.html" class="" title="Corinthians aguarda documento por Jadson e pode faturar R$ 6 milhÃµes em negociaÃ§Ã£o" rel="bookmark"><span class="conteudo"><p>Corinthians aguarda documento por Jadson e pode faturar R$ 6 milhÃµes em negociaÃ§Ã£o</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-12 15:35:55" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/rj/futebol/noticia/2015/12/agente-de-pikachu-descarta-negocio-com-fla-nao-vai-rolar-ida-para-o-rio.html" class="foto" title="Agente descarta ida de Pikachu para Fla e Vasco: &#39;NÃ£o vai agora pro Rio&#39; (Fernando Torres/Ascom Paysandu)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/HOD1dobn4_aqOxFVMEWYu8iaSIw=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/d80qm-vgp7J0vy0fp14Y7d_uwTw=/220x94:815x491/120x80/s.glbimg.com/es/ge/f/original/2015/11/26/pikachuuuuu.jpg" alt="Agente descarta ida de Pikachu para Fla e Vasco: &#39;NÃ£o vai agora pro Rio&#39; (Fernando Torres/Ascom Paysandu)" title="Agente descarta ida de Pikachu para Fla e Vasco: &#39;NÃ£o vai agora pro Rio&#39; (Fernando Torres/Ascom Paysandu)"
                data-original-image="s2.glbimg.com/d80qm-vgp7J0vy0fp14Y7d_uwTw=/220x94:815x491/120x80/s.glbimg.com/es/ge/f/original/2015/11/26/pikachuuuuu.jpg" data-url-smart_horizontal="TATGJprjb1fONfhZBaw-VZi4S2c=/90x0/smart/filters:strip_icc()/" data-url-smart="TATGJprjb1fONfhZBaw-VZi4S2c=/90x0/smart/filters:strip_icc()/" data-url-feature="TATGJprjb1fONfhZBaw-VZi4S2c=/90x0/smart/filters:strip_icc()/" data-url-tablet="FWpWMKAZpsS2TA2dV5J9Rkpc5w4=/70x50/smart/filters:strip_icc()/" data-url-desktop="lzNe53351LsorSk1OExl-u6KOoQ=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Agente descarta ida de Pikachu para Fla e Vasco: &#39;NÃ£o vai agora pro Rio&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-12 05:38:19" class="chamada chamada-principal mobile-grid-full"><a href="http://sportv.globo.com/site/combate/noticia/2015/12/ronda-rousey-cumpre-promessa-e-vai-baile-da-marinha-com-fuzileiro-naval.html" class="foto" title="Ronda Rousey cumpre promessa e vai a baile da Marinha com fuzileiro (ReproduÃ§Ã£o/Instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/LAns4hfSYZVuDG1O6jkg7l-XpbE=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/L-CwDE97aPburP5YFwva4Ia_PPg=/7x2:1167x776/120x80/s.glbimg.com/es/ge/f/original/2015/12/12/screen_shot_2015-12-11_at_10.50.52_pm.png" alt="Ronda Rousey cumpre promessa e vai a baile da Marinha com fuzileiro (ReproduÃ§Ã£o/Instagram)" title="Ronda Rousey cumpre promessa e vai a baile da Marinha com fuzileiro (ReproduÃ§Ã£o/Instagram)"
                data-original-image="s2.glbimg.com/L-CwDE97aPburP5YFwva4Ia_PPg=/7x2:1167x776/120x80/s.glbimg.com/es/ge/f/original/2015/12/12/screen_shot_2015-12-11_at_10.50.52_pm.png" data-url-smart_horizontal="gZ9pLbhCb-zgvT4uJ2XbIawkh4I=/90x0/smart/filters:strip_icc()/" data-url-smart="gZ9pLbhCb-zgvT4uJ2XbIawkh4I=/90x0/smart/filters:strip_icc()/" data-url-feature="gZ9pLbhCb-zgvT4uJ2XbIawkh4I=/90x0/smart/filters:strip_icc()/" data-url-tablet="JN0kPTcIT7f68oNbqbKg5u9dbhY=/70x50/smart/filters:strip_icc()/" data-url-desktop="w7OWJJBC0i9ioKErgq4ifwjb5g4=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Ronda Rousey cumpre promessa e vai a baile da Marinha com fuzileiro</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/flamengo/noticia/2015/12/fla-manifesta-interesse-faz-consulta-por-lavezzi-e-monitora-situacao.html" class="" title="Interessado, Flamengo faz consulta e monitora atacante argentino Lavezzi, do PSG" rel="bookmark"><span class="conteudo"><p>Interessado, Flamengo faz consulta e monitora atacante argentino Lavezzi, do PSG</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-12 13:30:01" class="chamada chamada-principal mobile-grid-full"><a href="http://app.globoesporte.globo.com/futebol/times/sao-paulo/todos-os-gols-de-rogerio-ceni/" class="foto" title="Goleiro artilheiro! Veja estatÃ­sticas e todos gols da carreira de RogÃ©rio Ceni (Globoesporte.com)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/uLNkD8-BUiLPRCSTWSOyoNEVZjA=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/mxfF-QmfdXhieb8mwdvzn73FWeo=/0x28:524x377/120x80/s.glbimg.com/es/ge/f/original/2015/12/11/carrossel_ceni1_524x567.jpg" alt="Goleiro artilheiro! Veja estatÃ­sticas e todos gols da carreira de RogÃ©rio Ceni (Globoesporte.com)" title="Goleiro artilheiro! Veja estatÃ­sticas e todos gols da carreira de RogÃ©rio Ceni (Globoesporte.com)"
                data-original-image="s2.glbimg.com/mxfF-QmfdXhieb8mwdvzn73FWeo=/0x28:524x377/120x80/s.glbimg.com/es/ge/f/original/2015/12/11/carrossel_ceni1_524x567.jpg" data-url-smart_horizontal="G6f7Zow0_gP_2KY3UkJvAZT8Ouw=/90x0/smart/filters:strip_icc()/" data-url-smart="G6f7Zow0_gP_2KY3UkJvAZT8Ouw=/90x0/smart/filters:strip_icc()/" data-url-feature="G6f7Zow0_gP_2KY3UkJvAZT8Ouw=/90x0/smart/filters:strip_icc()/" data-url-tablet="FW9FPKguv-jMLjRh3cJ8idUAQqQ=/70x50/smart/filters:strip_icc()/" data-url-desktop="X9vCN075okj-HwXZpn2-zr6Z1HU=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Goleiro artilheiro! Veja estatÃ­sticas e todos gols da carreira de RogÃ©rio Ceni</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-12 15:37:01" class="chamada chamada-principal mobile-grid-full"><a href="http://www.techtudo.com.br/dicas-e-tutoriais/noticia/2015/12/saiba-como-baixar-e-jogar-pes-2016-gratis-pela-playstation-store.html" class="foto" title="PES 2016 liberado! Saiba como baixar e instalar a versÃ£o gratuita do game (DivulgaÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/E5IIFY_duAaR24n9E-60UbUfC44=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/F-_MVPQsJ8HydIvY7UuNhsGxeSA=/32x7:386x243/120x80/s.glbimg.com/po/tt2/f/original/2015/11/26/128_1.jpg" alt="PES 2016 liberado! Saiba como baixar e instalar a versÃ£o gratuita do game (DivulgaÃ§Ã£o)" title="PES 2016 liberado! Saiba como baixar e instalar a versÃ£o gratuita do game (DivulgaÃ§Ã£o)"
                data-original-image="s2.glbimg.com/F-_MVPQsJ8HydIvY7UuNhsGxeSA=/32x7:386x243/120x80/s.glbimg.com/po/tt2/f/original/2015/11/26/128_1.jpg" data-url-smart_horizontal="FNXEhl3tEVuV1MY0rTDPpmboiVc=/90x0/smart/filters:strip_icc()/" data-url-smart="FNXEhl3tEVuV1MY0rTDPpmboiVc=/90x0/smart/filters:strip_icc()/" data-url-feature="FNXEhl3tEVuV1MY0rTDPpmboiVc=/90x0/smart/filters:strip_icc()/" data-url-tablet="3RpibLY1FrFsdinGSIL7Fbaik5A=/70x50/smart/filters:strip_icc()/" data-url-desktop="Ppg8Avy79_sjIMcKIXCFJiuErVo=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>PES 2016 liberado! Saiba como baixar e instalar a versÃ£o gratuita do game</p></span></a></div></div><div class="show-more"><span class="setinha-show-more"></span></div></div><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-times soccer-teams
"></div><div id="libby-home-widget-brasileirao" class="libby-home-widgetBase libby-home-widget-brasileirao analytics-area analytics-id-W"><input type="hidden" name="rodada_selecionada" value="current"></div><a class="link-see-more analytics-area analytics-id-L" title="mais esportes" href="http://globoesporte.globo.com/"><span class="text">mais esportes</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area etc-column analytics-area   last-has-photo
            
        
        "><div class="destaques-container closed analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/12/thaila-ayala-faz-carao-em-foto-de-biquini-magali-em-acao.html" class="foto" title="De biquÃ­ni, Thaila Ayala curte visual paradisÃ­aco no CearÃ¡: &#39;Magali em aÃ§Ã£o&#39;
 (ReproduÃ§Ã£o/Instagram)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/chNVJImDJxfAF735-4zMuPyFoEM=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/aA7iaqsMnpvd0zNLPFOuNZT3W4c=/0x186:1080x766/335x180/s.glbimg.com/jo/eg/f/original/2015/12/12/thaila_ayala.jpg" alt="De biquÃ­ni, Thaila Ayala curte visual paradisÃ­aco no CearÃ¡: &#39;Magali em aÃ§Ã£o&#39;
 (ReproduÃ§Ã£o/Instagram)" title="De biquÃ­ni, Thaila Ayala curte visual paradisÃ­aco no CearÃ¡: &#39;Magali em aÃ§Ã£o&#39;
 (ReproduÃ§Ã£o/Instagram)"
                data-original-image="s2.glbimg.com/aA7iaqsMnpvd0zNLPFOuNZT3W4c=/0x186:1080x766/335x180/s.glbimg.com/jo/eg/f/original/2015/12/12/thaila_ayala.jpg" data-url-smart_horizontal="PM5B2rwdMOCtYQ2d9tOeLf3uNPQ=/90x0/smart/filters:strip_icc()/" data-url-smart="PM5B2rwdMOCtYQ2d9tOeLf3uNPQ=/90x0/smart/filters:strip_icc()/" data-url-feature="PM5B2rwdMOCtYQ2d9tOeLf3uNPQ=/90x0/smart/filters:strip_icc()/" data-url-tablet="aniMwVP6P6X0MnmspOCiA8juzuU=/220x125/smart/filters:strip_icc()/" data-url-desktop="xBjDqRTatQifn99pUFym9GydBvQ=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>De biquÃ­ni, Thaila Ayala curte visual paradisÃ­aco no CearÃ¡: &#39;Magali em aÃ§Ã£o&#39;<br /></p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/12/mulher-de-alexandre-nero-aparece-com-barrigao-em-ensaio.html" class="foto" title="Mulher de Alexandre Nero, atriz e consultora de estilo posa grÃ¡vida em ensaio (ReproduÃ§Ã£o/Instagram/Sergio Baia)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/Pd1D_q-_BezRrDB1A02D6EJohF4=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/ZoXJtnZLIQrba02dS6mh6NM-DPw=/0x61:750x464/335x180/s.glbimg.com/jo/eg/f/original/2015/12/12/karen_brusttolin.jpg" alt="Mulher de Alexandre Nero, atriz e consultora de estilo posa grÃ¡vida em ensaio (ReproduÃ§Ã£o/Instagram/Sergio Baia)" title="Mulher de Alexandre Nero, atriz e consultora de estilo posa grÃ¡vida em ensaio (ReproduÃ§Ã£o/Instagram/Sergio Baia)"
                data-original-image="s2.glbimg.com/ZoXJtnZLIQrba02dS6mh6NM-DPw=/0x61:750x464/335x180/s.glbimg.com/jo/eg/f/original/2015/12/12/karen_brusttolin.jpg" data-url-smart_horizontal="YgJ6a47Fms_CP47TA7HF_RIyUUE=/90x0/smart/filters:strip_icc()/" data-url-smart="YgJ6a47Fms_CP47TA7HF_RIyUUE=/90x0/smart/filters:strip_icc()/" data-url-feature="YgJ6a47Fms_CP47TA7HF_RIyUUE=/90x0/smart/filters:strip_icc()/" data-url-tablet="k1Iu7o3j_SRf0oJXZGlaxG31INc=/70x50/smart/filters:strip_icc()/" data-url-desktop="opbxsOXdw9mtqso3ayJds9dQRJQ=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Mulher de Alexandre Nero, atriz e consultora de estilo posa grÃ¡vida em ensaio</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/12/gabriela-pugliesi-exibe-o-corpao-de-biquini-em-cenario-paradisiaco.html" class="foto" title="Gabriela Pugliesi curte litoral do CearÃ¡ e esbanja boa forma com namorado (ReproduÃ§Ã£o/Instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/uyq22o31QraxLoR1bm2VN06cMfs=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/AyRViOHEDJZpTJXPpnKTrYngq8M=/165x208:437x390/120x80/s.glbimg.com/jo/eg/f/original/2015/12/12/pugliesi_2.jpg" alt="Gabriela Pugliesi curte litoral do CearÃ¡ e esbanja boa forma com namorado (ReproduÃ§Ã£o/Instagram)" title="Gabriela Pugliesi curte litoral do CearÃ¡ e esbanja boa forma com namorado (ReproduÃ§Ã£o/Instagram)"
                data-original-image="s2.glbimg.com/AyRViOHEDJZpTJXPpnKTrYngq8M=/165x208:437x390/120x80/s.glbimg.com/jo/eg/f/original/2015/12/12/pugliesi_2.jpg" data-url-smart_horizontal="vz9Fo-y6mJsE3hjYPYobvl0ePf0=/90x0/smart/filters:strip_icc()/" data-url-smart="vz9Fo-y6mJsE3hjYPYobvl0ePf0=/90x0/smart/filters:strip_icc()/" data-url-feature="vz9Fo-y6mJsE3hjYPYobvl0ePf0=/90x0/smart/filters:strip_icc()/" data-url-tablet="bXQUb0qqwBxZmIjmXqWqxEXsBPk=/70x50/smart/filters:strip_icc()/" data-url-desktop="cSShCxgr8JYWVA73CWqfjCnUvbQ=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Gabriela Pugliesi curte litoral do CearÃ¡ e esbanja boa forma com namorado</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://kogut.oglobo.globo.com/noticias-da-tv/noticia/2015/12/ex-bambulua-louise-peres-fala-da-vida-em-nova-york-e-sente-saudades-dos-tempos-na-tv-voltaria-atuar-sim.html" class="" title="Hoje em NY, ex-atriz mirim que fez programa com AngÃ©lica sente falta de trabalhar na TV (reproduÃ§Ã£o)" rel="bookmark"><span class="conteudo"><p>Hoje em NY, ex-atriz mirim que fez programa com AngÃ©lica sente falta de trabalhar na TV</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-12 16:04:05" class="chamada chamada-principal mobile-grid-full"><a href="http://revistaquem.globo.com/Entrevista/noticia/2015/12/julianne-trevisol-fala-do-namorado-12-anos-mais-novo-idade-nao-fez-diferenca.html" class="foto" title="Julianne Trevisol posa no RJ e fala do namorado 12 anos mais jovem; fotos (ZÃ´ GuimarÃ£es)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/eXfR5CKiws7InlDUQsBymHIoqFo=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/RPESfM74jba_MSnFUZ6_A_ygwGU=/293x108:618x325/120x80/e.glbimg.com/og/ed/f/original/2015/12/09/juliann2.jpg" alt="Julianne Trevisol posa no RJ e fala do namorado 12 anos mais jovem; fotos (ZÃ´ GuimarÃ£es)" title="Julianne Trevisol posa no RJ e fala do namorado 12 anos mais jovem; fotos (ZÃ´ GuimarÃ£es)"
                data-original-image="s2.glbimg.com/RPESfM74jba_MSnFUZ6_A_ygwGU=/293x108:618x325/120x80/e.glbimg.com/og/ed/f/original/2015/12/09/juliann2.jpg" data-url-smart_horizontal="cC7fAn-DfIuRQ6op3dgFD8CpSeA=/90x0/smart/filters:strip_icc()/" data-url-smart="cC7fAn-DfIuRQ6op3dgFD8CpSeA=/90x0/smart/filters:strip_icc()/" data-url-feature="cC7fAn-DfIuRQ6op3dgFD8CpSeA=/90x0/smart/filters:strip_icc()/" data-url-tablet="rtT1d2pk_e8XM-FbnKhbyW75WT8=/70x50/smart/filters:strip_icc()/" data-url-desktop="RbmzCwnSUQ41NKEfDjIlTNWHKco=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Julianne Trevisol posa no RJ e fala do namorado 12 anos mais jovem; fotos</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-12 15:20:41" class="chamada chamada-principal mobile-grid-full"><a href="http://epoca.globo.com/colunas-e-blogs/bruno-astuto/noticia/2015/12/solteiro-caua-diz-que-quer-arrumar-tempo-para-namorar.html" class="foto" title="CauÃ£: &#39;Depois da novela, vou encontrar tempo para conhecer alguÃ©m bacana&#39; (Cau Mar)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/hXeCTzmqefQiEEBspOQyseYaxSo=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/P3SKjP9QA-Q7UgIAjtEUwcVuicE=/154x15:376x163/120x80/s.glbimg.com/en/ho/f/original/2015/12/12/caua.jpg" alt="CauÃ£: &#39;Depois da novela, vou encontrar tempo para conhecer alguÃ©m bacana&#39; (Cau Mar)" title="CauÃ£: &#39;Depois da novela, vou encontrar tempo para conhecer alguÃ©m bacana&#39; (Cau Mar)"
                data-original-image="s2.glbimg.com/P3SKjP9QA-Q7UgIAjtEUwcVuicE=/154x15:376x163/120x80/s.glbimg.com/en/ho/f/original/2015/12/12/caua.jpg" data-url-smart_horizontal="8JF3Z-MHZ_lBkVtRpIBeu-66HlA=/90x0/smart/filters:strip_icc()/" data-url-smart="8JF3Z-MHZ_lBkVtRpIBeu-66HlA=/90x0/smart/filters:strip_icc()/" data-url-feature="8JF3Z-MHZ_lBkVtRpIBeu-66HlA=/90x0/smart/filters:strip_icc()/" data-url-tablet="ppd0QTEi02xqmGsgffXX1P9iNmM=/70x50/smart/filters:strip_icc()/" data-url-desktop="_6y0HmSsXbhlIy4yOF2oE-2lJfY=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>CauÃ£: &#39;Depois da novela, vou encontrar tempo para conhecer alguÃ©m bacana&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-12 15:37:01" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/famosos/paulo-gustavo-nao-convida-sua-equipe-para-festa-de-casamento-com-thales-bretas-18279659.html" class="" title="&#39;Extra&#39;: equipe de Paulo Gustavo nÃ£o foi convidada para o casamento do ator (Evan Agostini / Evan Agostini/Invision/AP)" rel="bookmark"><span class="conteudo"><p>&#39;Extra&#39;: equipe de Paulo Gustavo nÃ£o foi convidada para o casamento do ator</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-12 07:33:19" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/12/ronaldo-beija-namorada-celina-locks-no-leblon-no-rio.html" class="foto" title="Em jantar no Rio, Ronaldo troca beijos e carinhos com a namorada, Celina Locks  (Ag. News)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/PF0xDBJygZL_wwA22dOqjtsoZqk=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/WsUFTs1ztijTjdUCOtNTgEYEhG4=/532x264:1066x620/120x80/s.glbimg.com/jo/eg/f/original/2015/12/12/gui4075.jpg" alt="Em jantar no Rio, Ronaldo troca beijos e carinhos com a namorada, Celina Locks  (Ag. News)" title="Em jantar no Rio, Ronaldo troca beijos e carinhos com a namorada, Celina Locks  (Ag. News)"
                data-original-image="s2.glbimg.com/WsUFTs1ztijTjdUCOtNTgEYEhG4=/532x264:1066x620/120x80/s.glbimg.com/jo/eg/f/original/2015/12/12/gui4075.jpg" data-url-smart_horizontal="NesuOW5NYxhhTFzRC9vmkTgVM44=/90x0/smart/filters:strip_icc()/" data-url-smart="NesuOW5NYxhhTFzRC9vmkTgVM44=/90x0/smart/filters:strip_icc()/" data-url-feature="NesuOW5NYxhhTFzRC9vmkTgVM44=/90x0/smart/filters:strip_icc()/" data-url-tablet="Xy2ITeXAwNpBdA8umyrh2FKKOL4=/70x50/smart/filters:strip_icc()/" data-url-desktop="kCWsluvLOccxSOY5_9jPXGWz9qU=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Em jantar no Rio, Ronaldo troca beijos e carinhos com a namorada, Celina Locks </p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-12-12 11:07:17" class="chamada chamada-principal mobile-grid-full"><a href="http://gnt.globo.com/receitas/receitas/como-fazer-cheesecake-de-chocolate-com-manteiga-de-amendoim.htm" class="foto" title="Prepare cheesecake de chocolate com manteiga de amendoim; anote (ReproduÃ§Ã£o/TV)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/oY80HPTvP2pdc90gY_evWH-IGKw=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/jpEWBzhaC2hlrMo80LBiBw1SfTI=/8x0:585x385/120x80/g.glbimg.com/og/gs/gsat5/f/original/2015/12/11/cheesecake-manteiga-amendoim-620.jpg" alt="Prepare cheesecake de chocolate com manteiga de amendoim; anote (ReproduÃ§Ã£o/TV)" title="Prepare cheesecake de chocolate com manteiga de amendoim; anote (ReproduÃ§Ã£o/TV)"
                data-original-image="s2.glbimg.com/jpEWBzhaC2hlrMo80LBiBw1SfTI=/8x0:585x385/120x80/g.glbimg.com/og/gs/gsat5/f/original/2015/12/11/cheesecake-manteiga-amendoim-620.jpg" data-url-smart_horizontal="3IeVuQgShyypCYpC8L_d8kLepw8=/90x0/smart/filters:strip_icc()/" data-url-smart="3IeVuQgShyypCYpC8L_d8kLepw8=/90x0/smart/filters:strip_icc()/" data-url-feature="3IeVuQgShyypCYpC8L_d8kLepw8=/90x0/smart/filters:strip_icc()/" data-url-tablet="hInmcMjBC6Zle9EffVHXqlwzE0I=/70x50/smart/filters:strip_icc()/" data-url-desktop="dvMEP5q9yurGpaUdijDNCSUoYI0=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Prepare cheesecake de chocolate com manteiga de amendoim; anote</p></span></a></div></div><div class="show-more"><span class="setinha-show-more"></span></div></div><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-novelas
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2015/", "logo": "http://s2.glbimg.com/tjsQl1SMvddbD-BGiSoC2Nb6kqg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-36x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2015/rss/", "logo_tv": "http://s2.glbimg.com/y8thjGY4IQuSvc4VfunyXi0d8C4=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-45x30.jpg"}, {"ordering": 34, "default": false, "cor": "#FF7F00", "name": "\u00cata Mundo Bom! ", "url": "http://gshow.globo.com/novelas/eta-mundo-bom/", "logo": "http://s2.glbimg.com/03_kBb44AvSMInRXaxaDxbqybTY=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/12/11/eta_36x20.jpg", "slug": "eta-mundo-bom", "url_feed": "http://gshow.globo.com/novelas/eta-mundo-bom/rss/", "logo_tv": "http://s2.glbimg.com/4yiquDhiaRXleK9H_vmDHGDDiLM=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/12/11/eta_45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/FRxoFZaNszGUmyoDZALOiIC_SOY=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/10/22/alem_do_tempo_segunda_fase-36x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/3Z-Sw1ITHzu4-EEfXG-zRbCs_dY=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/10/22/alem_g_45x30_1.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "Totalmente Demais", "url": "http://gshow.globo.com/novelas/totalmente-demais/", "logo": "http://s2.glbimg.com/R8T_N-bSmq23FCuZwf27cTTmjzg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/11/06/logo_36x20.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/totalmente-demais/rss/", "logo_tv": "http://s2.glbimg.com/DOKGjWF1k80-ujyw_Q-OT1IX65M=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/11/06/logo_45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "A Regra do Jogo", "url": "http://gshow.globo.com/novelas/a-regra-do-jogo/", "logo": "http://s2.glbimg.com/K-Nvvvm7zp2F7zS8TcY6TkIcxoE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-36x20.jpg", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/a-regra-do-jogo/rss/", "logo_tv": "http://s2.glbimg.com/4B7Jk0R6K1HlAAhuZIrDOJp8hbs=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-45x30.jpg"}]);}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2015/", "logo": "http://s2.glbimg.com/tjsQl1SMvddbD-BGiSoC2Nb6kqg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-36x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2015/rss/", "logo_tv": "http://s2.glbimg.com/y8thjGY4IQuSvc4VfunyXi0d8C4=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-45x30.jpg"}, {"ordering": 34, "default": false, "cor": "#FF7F00", "name": "\u00cata Mundo Bom! ", "url": "http://gshow.globo.com/novelas/eta-mundo-bom/", "logo": "http://s2.glbimg.com/03_kBb44AvSMInRXaxaDxbqybTY=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/12/11/eta_36x20.jpg", "slug": "eta-mundo-bom", "url_feed": "http://gshow.globo.com/novelas/eta-mundo-bom/rss/", "logo_tv": "http://s2.glbimg.com/4yiquDhiaRXleK9H_vmDHGDDiLM=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/12/11/eta_45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/FRxoFZaNszGUmyoDZALOiIC_SOY=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/10/22/alem_do_tempo_segunda_fase-36x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/3Z-Sw1ITHzu4-EEfXG-zRbCs_dY=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/10/22/alem_g_45x30_1.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "Totalmente Demais", "url": "http://gshow.globo.com/novelas/totalmente-demais/", "logo": "http://s2.glbimg.com/R8T_N-bSmq23FCuZwf27cTTmjzg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/11/06/logo_36x20.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/totalmente-demais/rss/", "logo_tv": "http://s2.glbimg.com/DOKGjWF1k80-ujyw_Q-OT1IX65M=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/11/06/logo_45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "A Regra do Jogo", "url": "http://gshow.globo.com/novelas/a-regra-do-jogo/", "logo": "http://s2.glbimg.com/K-Nvvvm7zp2F7zS8TcY6TkIcxoE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-36x20.jpg", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/a-regra-do-jogo/rss/", "logo_tv": "http://s2.glbimg.com/4B7Jk0R6K1HlAAhuZIrDOJp8hbs=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-45x30.jpg"}]);}});}
</script><div id="libby-home-widget-horoscopo" class="libby-home-widgetBase libby-home-widget-horoscopo analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais entretenimento" href="http://variedades.globo.com/"><span class="text">mais variedades</span><span class="arrow">&rsaquo;</span></a></div></section></div><div id="ad-position-middle3" class="opec"><div id="banner_slb_fim" class="tag-manager-publicidade-container"></div></div><div class="container columns clearfix glb-area-colunas second-area"><section class="area news-column"></section><section class="area sports-column"></section><section class="area etc-column analytics-area"></section></div><section class="area central container analytics-area analytics-id-O"><div class="separator"></div><section class="agrupador-quadruplo-tecnologia analytics-area analytics-id-T clearfix franja-inferior tecnologia"><div class="cabecalho"><h2><a href="http://www.techtudo.com.br/" title="TECNOLOGIA &amp; GAMES"><span class="word word-0">TECNOLOGIA</span><span class="word word-1">&</span><span class="word word-2">GAMES</span></a></h2><div id="ad-position-x34" class="opec"><div id="banner_selo4" class="tag-manager-publicidade-container"></div></div><aside class="links analytics-area analytics-id-T"><ul><li><a href="http://www.techtudo.com.br/jogos/" title="jogos">jogos</a></li><li><a href="http://www.techtudo.com.br/downloads/" title="downloads">downloads</a></li><li><a href="http://forum.techtudo.com.br/" title="fÃ³rum">fÃ³rum</a></li><li><a href="http://www.techtudo.com.br/mobile/" title="celulares">celulares</a></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaque"><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/dicas-e-tutoriais/noticia/2015/12/seu-whatsapp-web-nao-fica-online-saiba-o-que-pode-estar-acontecendo.html" title="Seu WhatsApp Web nÃ£o fica online? Aprenda como resolver"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/q4sa8Rady_IwIU7hCu_4zs2si54=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/jnpZ-PWBy0RyXxXbCqK0UNx0PMs=/0x0:694x369/245x130/s.glbimg.com/po/tt2/f/original/2015/12/10/whatsappweb1.jpg" alt="Seu WhatsApp Web nÃ£o fica online? Aprenda como resolver (Veja como resolver problemas no Whatsapp Web (Foto: TechTudo/Lucas Mendes))" title="Seu WhatsApp Web nÃ£o fica online? Aprenda como resolver (Veja como resolver problemas no Whatsapp Web (Foto: TechTudo/Lucas Mendes))"
             data-original-image="s2.glbimg.com/jnpZ-PWBy0RyXxXbCqK0UNx0PMs=/0x0:694x369/245x130/s.glbimg.com/po/tt2/f/original/2015/12/10/whatsappweb1.jpg" data-url-smart_horizontal="-GW1iYxQqYT3wDiagpGdbgPoqPs=/90x56/smart/filters:strip_icc()/" data-url-smart="-GW1iYxQqYT3wDiagpGdbgPoqPs=/90x56/smart/filters:strip_icc()/" data-url-feature="-GW1iYxQqYT3wDiagpGdbgPoqPs=/90x56/smart/filters:strip_icc()/" data-url-tablet="UuI-dyVzmvhRJCN_FEvJW4T6bsY=/160x96/smart/filters:strip_icc()/" data-url-desktop="TtUKiKAWcnJisjWKyv3II_W9dF4=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Seu WhatsApp Web nÃ£o fica online? Aprenda como resolver</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/noticias/noticia/2015/12/iphone-6s-plus-ou-lg-g4-comparativo.html" title="Celular mais em conta promete &#39;derrubar&#39; iPhone; veja disputa"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/oXrtUkAcbtLi--vTSmJvH4ngQac=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/WZP_t76t8gzuQhQwttDr_cfsNG0=/0x53:696x422/245x130/s.glbimg.com/po/tt2/f/original/2015/12/08/iphone_6s_plus_vs_lg_g4.png" alt="Celular mais em conta promete &#39;derrubar&#39; iPhone; veja disputa (Arte/TechTudo)" title="Celular mais em conta promete &#39;derrubar&#39; iPhone; veja disputa (Arte/TechTudo)"
             data-original-image="s2.glbimg.com/WZP_t76t8gzuQhQwttDr_cfsNG0=/0x53:696x422/245x130/s.glbimg.com/po/tt2/f/original/2015/12/08/iphone_6s_plus_vs_lg_g4.png" data-url-smart_horizontal="tVNyA6vd8aIkkrZlbX2U2yGdwVc=/90x56/smart/filters:strip_icc()/" data-url-smart="tVNyA6vd8aIkkrZlbX2U2yGdwVc=/90x56/smart/filters:strip_icc()/" data-url-feature="tVNyA6vd8aIkkrZlbX2U2yGdwVc=/90x56/smart/filters:strip_icc()/" data-url-tablet="vFYbVzyX93QCkYCNxaY1cOnQFJ4=/160x96/smart/filters:strip_icc()/" data-url-desktop="MZFTiMU0kLK92-yA1fzukFwNvyQ=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Celular mais em conta promete &#39;derrubar&#39; iPhone; veja disputa</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/dicas-e-tutoriais/noticia/2015/12/como-fazer-ligacoes-baratas-com-o-aplicativo-ringo.html" title="App gratuito faz ligaÃ§Ãµes baratas com ou sem internet; conheÃ§a"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/5l2mMbnebUY0VIvvbifNg5FD_UU=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/aJheipzOcPrXyZpuWiBFT6Wb7wY=/0x266:5472x3172/245x130/s.glbimg.com/po/tt2/f/original/2015/08/18/img_3993.jpg" alt="App gratuito faz ligaÃ§Ãµes baratas com ou sem internet; conheÃ§a (Luana Marfim/TechTudo)" title="App gratuito faz ligaÃ§Ãµes baratas com ou sem internet; conheÃ§a (Luana Marfim/TechTudo)"
             data-original-image="s2.glbimg.com/aJheipzOcPrXyZpuWiBFT6Wb7wY=/0x266:5472x3172/245x130/s.glbimg.com/po/tt2/f/original/2015/08/18/img_3993.jpg" data-url-smart_horizontal="k8KaU759zx7qcA3qZ4n0qNXvww0=/90x56/smart/filters:strip_icc()/" data-url-smart="k8KaU759zx7qcA3qZ4n0qNXvww0=/90x56/smart/filters:strip_icc()/" data-url-feature="k8KaU759zx7qcA3qZ4n0qNXvww0=/90x56/smart/filters:strip_icc()/" data-url-tablet="ex6fbq1_LpHSASJ8Gu2B-t3oSCk=/160x96/smart/filters:strip_icc()/" data-url-desktop="47eE9HY8_y5XynmuzCimt42b4XQ=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>App gratuito faz ligaÃ§Ãµes baratas com ou sem internet; conheÃ§a</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/tudo-sobre/whatsmileys-smileys-bate-papo.html" title="WhatsApp de cara nova: app grÃ¡tis adiciona emojis inÃ©ditos"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/oUYf2zhknAT_-NAz0zm-tVbC7aA=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/-NNV1VD0w4ZT7NX3pWpcWvZQ9_8=/0x0:4032x2141/245x130/s.glbimg.com/po/tt2/f/original/2015/12/03/novos_emojis-2.jpg" alt="WhatsApp de cara nova: app grÃ¡tis adiciona emojis inÃ©ditos (Luana Marfim/TechTudo)" title="WhatsApp de cara nova: app grÃ¡tis adiciona emojis inÃ©ditos (Luana Marfim/TechTudo)"
             data-original-image="s2.glbimg.com/-NNV1VD0w4ZT7NX3pWpcWvZQ9_8=/0x0:4032x2141/245x130/s.glbimg.com/po/tt2/f/original/2015/12/03/novos_emojis-2.jpg" data-url-smart_horizontal="1GOHaU4Zp0Ha0D9IKg0-P8CCIBc=/90x56/smart/filters:strip_icc()/" data-url-smart="1GOHaU4Zp0Ha0D9IKg0-P8CCIBc=/90x56/smart/filters:strip_icc()/" data-url-feature="1GOHaU4Zp0Ha0D9IKg0-P8CCIBc=/90x56/smart/filters:strip_icc()/" data-url-tablet="Qq-5rDMN6bSDGWq0gsSkdH5Bc04=/160x96/smart/filters:strip_icc()/" data-url-desktop="evbZqL8bB1e1xTK2Cv-Chtl2oS4=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>WhatsApp de cara nova: app grÃ¡tis adiciona emojis inÃ©ditos</p></span></a></li></ul></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-tecnologia']);</script><div class="separator"></div><section class="agrupador-sextuplo analytics-area analytics-id-S"><section class="analytics-area analytics-id-E esquerda tvg"><div class="cabecalho"><h2><span class="word word-0">moda</span><span class="word word-1">&amp;</span><span class="word word-2">beleza</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://revistamarieclaire.globo.com/Beleza/noticia/2015/12/sem-criatividade-makeup-artist-ensina-3-makes-perfeitos-para-o-fim-de-semana.html" alt="Aprenda trÃªs tipos de maquiagem perfeitas para o fim de semana"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/ru60C0_Thz4cjBOhxLg5K_om4-A=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/13jsw0Ni6y7F57AK-bVX95qFQRg=/102x10:543x295/155x100/e.glbimg.com/og/ed/f/original/2015/12/12/classico.jpg" alt="Aprenda trÃªs tipos de maquiagem perfeitas para o fim de semana (ReproduÃ§Ã£o/YouTube)" title="Aprenda trÃªs tipos de maquiagem perfeitas para o fim de semana (ReproduÃ§Ã£o/YouTube)"
            data-original-image="s2.glbimg.com/13jsw0Ni6y7F57AK-bVX95qFQRg=/102x10:543x295/155x100/e.glbimg.com/og/ed/f/original/2015/12/12/classico.jpg" data-url-smart_horizontal="csF13oQR0feEuRk7Fz45UuZ00Gc=/90x56/smart/filters:strip_icc()/" data-url-smart="csF13oQR0feEuRk7Fz45UuZ00Gc=/90x56/smart/filters:strip_icc()/" data-url-feature="csF13oQR0feEuRk7Fz45UuZ00Gc=/90x56/smart/filters:strip_icc()/" data-url-tablet="WTa_8tWDmA2qJsLq4fME9C7pOX0=/122x75/smart/filters:strip_icc()/" data-url-desktop="uVlypJIo292Hs0TkQ31sNqyb8N8=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>anda sem criatividade?</h3><p>Aprenda trÃªs tipos de maquiagem perfeitas para o fim de semana</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://vogue.globo.com/moda/guia-de-estilo/noticia/2015/12/pilares-do-poder-aposte-nos-sapatos-com-saltos-grossos-decorados.html" alt="Saltos grossos altÃ­ssimos celebram a volta da extravagÃ¢ncia no visual"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/oC6s1JHjcM0T1aO9ojcoPYLSKuU=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/Q1rXawU5X8hV9EOqGDcVvRNwPIs=/0x95:620x495/155x100/e.glbimg.com/og/ed/f/original/2015/12/12/salto01_1.jpg" alt="Saltos grossos altÃ­ssimos celebram a volta da extravagÃ¢ncia no visual (sdalt)" title="Saltos grossos altÃ­ssimos celebram a volta da extravagÃ¢ncia no visual (sdalt)"
            data-original-image="s2.glbimg.com/Q1rXawU5X8hV9EOqGDcVvRNwPIs=/0x95:620x495/155x100/e.glbimg.com/og/ed/f/original/2015/12/12/salto01_1.jpg" data-url-smart_horizontal="LFqkC9K5ceUVM4SFTXGz-OxMwY8=/90x56/smart/filters:strip_icc()/" data-url-smart="LFqkC9K5ceUVM4SFTXGz-OxMwY8=/90x56/smart/filters:strip_icc()/" data-url-feature="LFqkC9K5ceUVM4SFTXGz-OxMwY8=/90x56/smart/filters:strip_icc()/" data-url-tablet="fZ-BWbuyCWA-m0pkI9lnhsakC4s=/122x75/smart/filters:strip_icc()/" data-url-desktop="WJ6xWz2diZy31jrbycTeXp5qYLA=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>vai encarar?</h3><p>Saltos grossos altÃ­ssimos  celebram a volta da extravagÃ¢ncia no visual</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revistaglamour.globo.com/Moda/noticia/2015/12/moda-praia-pra-todos-os-corpos-sem-erro.html" alt="Aprenda dicas de moda praia para todos os corpos, sem erro"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/8MFizLRRYFD4ZtZsDm0d6SxwvYE=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/EIq0IF2rBLQCpY_ZlJkBWHFJH48=/0x26:512x357/155x100/s.glbimg.com/en/ho/f/original/2015/12/12/biquini-cintura-alta.jpg" alt="Aprenda dicas de moda praia para todos os corpos, sem erro (DivulgaÃ§Ã£o)" title="Aprenda dicas de moda praia para todos os corpos, sem erro (DivulgaÃ§Ã£o)"
            data-original-image="s2.glbimg.com/EIq0IF2rBLQCpY_ZlJkBWHFJH48=/0x26:512x357/155x100/s.glbimg.com/en/ho/f/original/2015/12/12/biquini-cintura-alta.jpg" data-url-smart_horizontal="Ttn4FH4KoOJTKckVRlmMQhbaJNs=/90x56/smart/filters:strip_icc()/" data-url-smart="Ttn4FH4KoOJTKckVRlmMQhbaJNs=/90x56/smart/filters:strip_icc()/" data-url-feature="Ttn4FH4KoOJTKckVRlmMQhbaJNs=/90x56/smart/filters:strip_icc()/" data-url-tablet="MhlPAO0QovsdrUpGaAL4z0AkWkE=/122x75/smart/filters:strip_icc()/" data-url-desktop="9OVHBM8gp802FFZeRCnS3iKvdEk=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>quer disfarÃ§ar algo?</h3><p>Aprenda dicas de moda praia para todos os corpos, sem erro</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://gnt.globo.com/" title="GNT">GNT</a></h3></li><li><h3><a href="http://vogue.globo.com/" title="vogue">vogue</a></h3></li><li></li></ul></aside></section><div class="separator"></div><section class="analytics-area analytics-id-D direita tvg"><div class="cabecalho"><h2><span class="word word-0">CASA</span><span class="word word-1">&amp;</span><span class="word word-2">DECORAÃÃO</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://oglobo.globo.com/ela/decoracao/decoracao-leva-azul-do-mar-o-bege-da-areia-para-cobertura-18271836" alt="DecoraÃ§Ã£o leva o azul do mar e o bege da areia para cobertura"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/U5YAPfhtS5a6xBfcZtbJRjP6MQI=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/3i3GSa92KkPDnlvh8PZgdJb_hZE=/47x0:699x420/155x100/s.glbimg.com/en/ho/f/original/2015/12/12/2015-863708669-150211_paula-neder04.jpg_20151104.jpg" alt="DecoraÃ§Ã£o leva o azul do mar e o bege da areia para cobertura (DivulgaÃ§Ã£o)" title="DecoraÃ§Ã£o leva o azul do mar e o bege da areia para cobertura (DivulgaÃ§Ã£o)"
            data-original-image="s2.glbimg.com/3i3GSa92KkPDnlvh8PZgdJb_hZE=/47x0:699x420/155x100/s.glbimg.com/en/ho/f/original/2015/12/12/2015-863708669-150211_paula-neder04.jpg_20151104.jpg" data-url-smart_horizontal="mAHAUxr8Fdx8t5fm8XPMojZt8fA=/90x56/smart/filters:strip_icc()/" data-url-smart="mAHAUxr8Fdx8t5fm8XPMojZt8fA=/90x56/smart/filters:strip_icc()/" data-url-feature="mAHAUxr8Fdx8t5fm8XPMojZt8fA=/90x56/smart/filters:strip_icc()/" data-url-tablet="hlLExV64GYctkoy1M-mKP86Od-k=/122x75/smart/filters:strip_icc()/" data-url-desktop="6vNuUBgq93AaUJtf-YMSTuTh3hs=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>clima de praia</h3><p>DecoraÃ§Ã£o leva o azul do mar e o bege da areia para cobertura</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://gnt.globo.com/casa-e-decoracao/fotos/como-decorar-sua-casa-em-clima-de-natal-veja-fotos-e-inspire-se.htm#5118=1" alt="Aprenda como decorar sua casa e criar um clima de Natal"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/0Z_dUZk9DAItc__9YKw5wf7WRPw=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/hU1MP9M5sbCOUz1w8SCzAiz8KSs=/0x57:300x250/155x100/g.glbimg.com/og/gs/gsat5/f/original/2015/12/11/decoracao-natal-385.jpg" alt="Aprenda como decorar sua casa e criar um clima de Natal (ReproduÃ§Ã£o)" title="Aprenda como decorar sua casa e criar um clima de Natal (ReproduÃ§Ã£o)"
            data-original-image="s2.glbimg.com/hU1MP9M5sbCOUz1w8SCzAiz8KSs=/0x57:300x250/155x100/g.glbimg.com/og/gs/gsat5/f/original/2015/12/11/decoracao-natal-385.jpg" data-url-smart_horizontal="Zndo2kFbvC1kxm6vMk5QFNUlUQc=/90x56/smart/filters:strip_icc()/" data-url-smart="Zndo2kFbvC1kxm6vMk5QFNUlUQc=/90x56/smart/filters:strip_icc()/" data-url-feature="Zndo2kFbvC1kxm6vMk5QFNUlUQc=/90x56/smart/filters:strip_icc()/" data-url-tablet="FkRZ0mOxGOJmJdd_EgXCbw9IoPs=/122x75/smart/filters:strip_icc()/" data-url-desktop="lejzLItCQF6LwIjgkkEj3xJKg28=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>lar aconchegante!</h3><p>Aprenda como decorar sua casa e criar um clima de Natal</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://gq.globo.com/Blogs/Da-redacao/noticia/2015/12/soltar-borboletas-em-casamento-vira-tendencia-apesar-de-ser-crime-ambiental.html" alt="Soltar borboletas em casamento vira tendÃªncia e causa polÃªmica"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/1NKed8VodozszdEIAKSt4IhbKYY=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/OYyy4usk6ybRko3IqiET98Lm7V8=/21x44:553x388/155x100/s.glbimg.com/en/ho/f/original/2015/12/12/borboleta-casamento.jpg" alt="Soltar borboletas em casamento vira tendÃªncia e causa polÃªmica (ReproduÃ§Ã£o)" title="Soltar borboletas em casamento vira tendÃªncia e causa polÃªmica (ReproduÃ§Ã£o)"
            data-original-image="s2.glbimg.com/OYyy4usk6ybRko3IqiET98Lm7V8=/21x44:553x388/155x100/s.glbimg.com/en/ho/f/original/2015/12/12/borboleta-casamento.jpg" data-url-smart_horizontal="m9NeinATHOOYRD713rvvETVNMe8=/90x56/smart/filters:strip_icc()/" data-url-smart="m9NeinATHOOYRD713rvvETVNMe8=/90x56/smart/filters:strip_icc()/" data-url-feature="m9NeinATHOOYRD713rvvETVNMe8=/90x56/smart/filters:strip_icc()/" data-url-tablet="vVQpRlYv8QF8g4QYrtvxpynjOt4=/122x75/smart/filters:strip_icc()/" data-url-desktop="jE8Kmkf22cPWv9EYWYOdU0OUMbg=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>crime ambiental</h3><p>Soltar borboletas em casamento vira tendÃªncia e causa polÃªmica</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://revistacasaejardim.globo.com/" title="CASA E JARDIM">CASA E JARDIM</a></h3></li><li><h3><a href="http://casavogue.globo.com/" title="CASA VOGUE">CASA VOGUE</a></h3></li><li></li></ul></aside></section></section><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior famosos-style"><div class="cabecalho"><h2><a title="famosos"><span class='logo'>famosos</span><span class='texto'>famosos</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://ego.globo.com/">ego</a></h3></li><li class=""><h3><a href="http://revistaquem.globo.com/">quem</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/Bastidores/noticia/2015/12/giulia-costa-e-brenno-leone-contam-como-e-gravar-cenas-quentes-em-malhacao.html" title="Giulia Costa conta como Ã© gravar cenas sobre virgindade"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/dIyl6lkV9OnEMFn9sseyMHOd5hs=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/krS55dgbXIr5TwP04OZ8asjYIyU=/0x51:720x434/245x130/s.glbimg.com/et/gs/f/original/2015/12/11/giulia-costa-brenno-leone.jpg" alt="Giulia Costa conta como Ã© gravar cenas sobre virgindade (Felipe Monteiro/Gshow)" title="Giulia Costa conta como Ã© gravar cenas sobre virgindade (Felipe Monteiro/Gshow)"
                    data-original-image="s2.glbimg.com/krS55dgbXIr5TwP04OZ8asjYIyU=/0x51:720x434/245x130/s.glbimg.com/et/gs/f/original/2015/12/11/giulia-costa-brenno-leone.jpg" data-url-smart_horizontal="wi9HKjpgCMtLCQVB_Mh5q_PPJLQ=/90x56/smart/filters:strip_icc()/" data-url-smart="wi9HKjpgCMtLCQVB_Mh5q_PPJLQ=/90x56/smart/filters:strip_icc()/" data-url-feature="wi9HKjpgCMtLCQVB_Mh5q_PPJLQ=/90x56/smart/filters:strip_icc()/" data-url-tablet="cROsg_e84_HdCZBJmVgQWfQgfhc=/160x95/smart/filters:strip_icc()/" data-url-desktop="F08z6WSMKqcNTubKTO4-TOP837Y=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Giulia Costa conta como Ã© gravar cenas sobre virgindade</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://revistaquem.globo.com/Entrevista/noticia/2015/12/minha-vida-comecou-aos-40-revela-reynaldo-gianecchini.html" title="&#39;Minha vida comeÃ§ou aos 40&#39;, afirma Reynaldo Gianecchini
"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/qllSqB2_j5p5FqaMP2aB32fLIjg=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/niR1w3M0U1HGyyHTRd64bqN4rx4=/0x103:620x432/245x130/s.glbimg.com/en/ho/f/original/2015/12/01/gianechinni.jpg" alt="&#39;Minha vida comeÃ§ou aos 40&#39;, afirma Reynaldo Gianecchini
 (Marcelo Tabach / ed. globo)" title="&#39;Minha vida comeÃ§ou aos 40&#39;, afirma Reynaldo Gianecchini
 (Marcelo Tabach / ed. globo)"
                    data-original-image="s2.glbimg.com/niR1w3M0U1HGyyHTRd64bqN4rx4=/0x103:620x432/245x130/s.glbimg.com/en/ho/f/original/2015/12/01/gianechinni.jpg" data-url-smart_horizontal="SSAQJbeK5jq8LFeV2QM3LZm7WWM=/90x56/smart/filters:strip_icc()/" data-url-smart="SSAQJbeK5jq8LFeV2QM3LZm7WWM=/90x56/smart/filters:strip_icc()/" data-url-feature="SSAQJbeK5jq8LFeV2QM3LZm7WWM=/90x56/smart/filters:strip_icc()/" data-url-tablet="AaYJHBV5QDvmoN4VzainYt7ny-A=/160x95/smart/filters:strip_icc()/" data-url-desktop="WLOuQ5qFCunT5KQy3SucS3MgjhE=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>&#39;Minha vida comeÃ§ou aos 40&#39;, afirma Reynaldo Gianecchini<br /></p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://revistamonet.globo.com/Celebridades/noticia/2015/12/lady-gaga-fala-do-abuso-sexual-que-sofreu-aos-19-eu-me-senti-culpada-e-me-calei-por-7-anos.html" title="Lady Gaga lembra abuso sexual aos 19 anos: &#39;Me senti culpada&#39;"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/Q1lGECEz_EAGgELNY9-7caRdPCo=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/qsBeKAJz6VJOob_RAJwaVRDqrSw=/92x72:600x341/245x130/s.glbimg.com/en/ho/f/original/2015/12/12/lady_gaga.jpg" alt="Lady Gaga lembra abuso sexual aos 19 anos: &#39;Me senti culpada&#39; (getty images)" title="Lady Gaga lembra abuso sexual aos 19 anos: &#39;Me senti culpada&#39; (getty images)"
                    data-original-image="s2.glbimg.com/qsBeKAJz6VJOob_RAJwaVRDqrSw=/92x72:600x341/245x130/s.glbimg.com/en/ho/f/original/2015/12/12/lady_gaga.jpg" data-url-smart_horizontal="Dd_TpDx_xd5bWs9C6idurmdI8G8=/90x56/smart/filters:strip_icc()/" data-url-smart="Dd_TpDx_xd5bWs9C6idurmdI8G8=/90x56/smart/filters:strip_icc()/" data-url-feature="Dd_TpDx_xd5bWs9C6idurmdI8G8=/90x56/smart/filters:strip_icc()/" data-url-tablet="bcxumTPROWEWsdoFHnGe9Jj-5qY=/160x95/smart/filters:strip_icc()/" data-url-desktop="SuNKPQjzzN9tGuWEa959V_PoPDU=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Lady Gaga lembra abuso sexual aos 19 anos: &#39;Me senti culpada&#39;</p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://ego.globo.com/noite/noticia/2015/12/claudia-raia-baba-pelos-filhos-enzo-e-sophia-orgulhosa.html" title="Claudia Raia posta look de formatura de Enzo: &#39;Orgulhosa&#39;"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/u5dVS9iZDkda_OwQQZj-O5fsJBs=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/TinEauTfdokxm9edOnEEvV36GWQ=/0x0:640x339/245x130/s.glbimg.com/jo/eg/f/original/2015/12/12/12356446_426762084197754_387435530_n.jpg" alt="Claudia Raia posta look de formatura de Enzo: &#39;Orgulhosa&#39; (Instagram/ ReproduÃ§Ã£o)" title="Claudia Raia posta look de formatura de Enzo: &#39;Orgulhosa&#39; (Instagram/ ReproduÃ§Ã£o)"
                    data-original-image="s2.glbimg.com/TinEauTfdokxm9edOnEEvV36GWQ=/0x0:640x339/245x130/s.glbimg.com/jo/eg/f/original/2015/12/12/12356446_426762084197754_387435530_n.jpg" data-url-smart_horizontal="Gd3hnZM4qX-1tDZQtq-RJcnLcwE=/90x56/smart/filters:strip_icc()/" data-url-smart="Gd3hnZM4qX-1tDZQtq-RJcnLcwE=/90x56/smart/filters:strip_icc()/" data-url-feature="Gd3hnZM4qX-1tDZQtq-RJcnLcwE=/90x56/smart/filters:strip_icc()/" data-url-tablet="LpFlPeRGUVKeau9oz5SSYlk_xwY=/160x95/smart/filters:strip_icc()/" data-url-desktop="g-NEPOklwtd_YwPOtTHKEuyoPZA=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Claudia Raia posta look de formatura de Enzo: &#39;Orgulhosa&#39;</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_famosos-style']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior gshow"><div class="cabecalho"><h2><a href="http://gshow.globo.com/" title="novelas, sÃ©ries, programas e muito mais"><span class='logo'>novelas, sÃ©ries, programas e muito mais</span><span class='texto'>novelas, sÃ©ries, programas e muito mais</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://gshow.globo.com/estilo-tv/">ESTILO TV</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/">A REGRA DO JOGO</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/totalmente-demais/">TOTALMENTE DEMAIS</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/alem-do-tempo/">ALÃM DO TEMPO</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/malhacao/2015/">MALHAÃÃO</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/alem-do-tempo/vem-por-ai/noticia/2015/12/emilia-faz-ligacao-misteriosa-apos-revelacao-de-bernardo.html" title="EmÃ­lia faz ligaÃ§Ã£o estranha apÃ³s revelaÃ§Ã£o de Bernardo em &#39;AlÃ©m&#39;"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/ekQLnNT-Klbr6_ZVgno7m5WmR-I=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/wx-VpTDvyyvh-hr4gJwQ2N7fcjc=/0x45:720x427/245x130/s.glbimg.com/et/gs/f/original/2015/12/08/ana-beatriz-nogueira2.jpg" alt="EmÃ­lia faz ligaÃ§Ã£o estranha apÃ³s revelaÃ§Ã£o de Bernardo em &#39;AlÃ©m&#39; (TV Globo)" title="EmÃ­lia faz ligaÃ§Ã£o estranha apÃ³s revelaÃ§Ã£o de Bernardo em &#39;AlÃ©m&#39; (TV Globo)"
                    data-original-image="s2.glbimg.com/wx-VpTDvyyvh-hr4gJwQ2N7fcjc=/0x45:720x427/245x130/s.glbimg.com/et/gs/f/original/2015/12/08/ana-beatriz-nogueira2.jpg" data-url-smart_horizontal="vzlvyqPjKZfyQtaTrfGgEx2qbaA=/90x56/smart/filters:strip_icc()/" data-url-smart="vzlvyqPjKZfyQtaTrfGgEx2qbaA=/90x56/smart/filters:strip_icc()/" data-url-feature="vzlvyqPjKZfyQtaTrfGgEx2qbaA=/90x56/smart/filters:strip_icc()/" data-url-tablet="Z2irUCZkuSzm_TduHAKlFA3hPyw=/160x95/smart/filters:strip_icc()/" data-url-desktop="6TsBhdDUSvsMmchA2E8a8PqXKAc=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>EmÃ­lia faz ligaÃ§Ã£o estranha apÃ³s revelaÃ§Ã£o de Bernardo em &#39;AlÃ©m&#39;</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/malhacao/2015/Vem-por-ai/noticia/2015/12/taina-perdoa-camila.html" title="ApÃ³s conversa sincera em &#39;MalhaÃ§Ã£o&#39;, TainÃ¡ perdoa Camila"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/sXRVDQUWkB4K31_WbLDfkeY2yac=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/3wPnAb9ThdHGWjCfcTcXlmeHLWk=/0x84:720x466/245x130/s.glbimg.com/et/gs/f/original/2015/12/11/taina-camila.jpg" alt="ApÃ³s conversa sincera em &#39;MalhaÃ§Ã£o&#39;, TainÃ¡ perdoa Camila (TV Globo)" title="ApÃ³s conversa sincera em &#39;MalhaÃ§Ã£o&#39;, TainÃ¡ perdoa Camila (TV Globo)"
                    data-original-image="s2.glbimg.com/3wPnAb9ThdHGWjCfcTcXlmeHLWk=/0x84:720x466/245x130/s.glbimg.com/et/gs/f/original/2015/12/11/taina-camila.jpg" data-url-smart_horizontal="urCvYnGkA6Bel1X3_kYXZMtlzy0=/90x56/smart/filters:strip_icc()/" data-url-smart="urCvYnGkA6Bel1X3_kYXZMtlzy0=/90x56/smart/filters:strip_icc()/" data-url-feature="urCvYnGkA6Bel1X3_kYXZMtlzy0=/90x56/smart/filters:strip_icc()/" data-url-tablet="7WyiK6H-G-8mOR2nd-LBLTjnmG8=/160x95/smart/filters:strip_icc()/" data-url-desktop="q1qwV8AEKkVfZ_1d_iV3SjaWtcI=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>ApÃ³s conversa sincera em &#39;MalhaÃ§Ã£o&#39;, TainÃ¡ perdoa Camila</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/Bastidores/noticia/2015/12/melhores-do-ano-faz-20-anos-veja-como-era-o-mundo-em-1995.html" title="PrÃªmio &#39;Melhores do Ano&#39; faz 20 anos; veja como foi o ano de 95"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/bSoqIVu2fuAKnutJOIdvkF3OUxs=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/a-gCqAXCMIfwU0oeCN8krL568yM=/0x17:690x384/245x130/s.glbimg.com/et/gs/f/original/2015/12/10/1995ok.jpg" alt="PrÃªmio &#39;Melhores do Ano&#39; faz 20 anos; veja como foi o ano de 95 (CEDOC/TV Globo)" title="PrÃªmio &#39;Melhores do Ano&#39; faz 20 anos; veja como foi o ano de 95 (CEDOC/TV Globo)"
                    data-original-image="s2.glbimg.com/a-gCqAXCMIfwU0oeCN8krL568yM=/0x17:690x384/245x130/s.glbimg.com/et/gs/f/original/2015/12/10/1995ok.jpg" data-url-smart_horizontal="rtwplq1_zMQXiHiE4oB3qqrCPSE=/90x56/smart/filters:strip_icc()/" data-url-smart="rtwplq1_zMQXiHiE4oB3qqrCPSE=/90x56/smart/filters:strip_icc()/" data-url-feature="rtwplq1_zMQXiHiE4oB3qqrCPSE=/90x56/smart/filters:strip_icc()/" data-url-tablet="pGvaGuzeZ0vnqWenlqmA2ruFQ5A=/160x95/smart/filters:strip_icc()/" data-url-desktop="fBWp1kKBbxoJyZsBpeYBZYlVPTA=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>PrÃªmio &#39;Melhores do Ano&#39; faz 20 anos; veja como foi o ano de 95</p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://gshow.globo.com/Bastidores/noticia/2015/12/ex-verdades-secretas-flavio-tolezani-radicaliza-visual-para-nova-novela-das-6-estou-limpinho.html" title="Ator de &#39;Verdades Secretas&#39; muda visual para nova novela"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/ffGX-l_UCtIort8ambqhtFI3dO8=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/eK0F5McfnGNSnE-EknfCB5M7AB8=/0x17:690x384/245x130/s.glbimg.com/et/gs/f/original/2015/12/10/flavio-tolezani3_1.jpg" alt="Ator de &#39;Verdades Secretas&#39; muda visual para nova novela (Ellen Soares/Gshow)" title="Ator de &#39;Verdades Secretas&#39; muda visual para nova novela (Ellen Soares/Gshow)"
                    data-original-image="s2.glbimg.com/eK0F5McfnGNSnE-EknfCB5M7AB8=/0x17:690x384/245x130/s.glbimg.com/et/gs/f/original/2015/12/10/flavio-tolezani3_1.jpg" data-url-smart_horizontal="PvYx3-FAEZNhk6cmy2qmeRVnTVU=/90x56/smart/filters:strip_icc()/" data-url-smart="PvYx3-FAEZNhk6cmy2qmeRVnTVU=/90x56/smart/filters:strip_icc()/" data-url-feature="PvYx3-FAEZNhk6cmy2qmeRVnTVU=/90x56/smart/filters:strip_icc()/" data-url-tablet="2xB5oKKYfbjLKAk_ZIMjMONVcM4=/160x95/smart/filters:strip_icc()/" data-url-desktop="5amUf_xf1hWTxKaVY3r3qfcVkEE=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Ator de &#39;Verdades Secretas&#39; muda visual para nova novela</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_gshow']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior globoplay"><div class="cabecalho"><h2><a href="http://globoplay.globo.com/" title="globoplay"><span class='logo'>globoplay</span><span class='texto'>globoplay</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://globoplay.globo.com/a-regra-do-jogo/p/8887/">A REGRA DO JOGO</a></h3></li><li class=""><h3><a href="http://globoplay.globo.com/jornal-nacional/p/819/">JORNAL NACIONAL</a></h3></li><li class=""><h3><a href="http://globoplay.globo.com/totalmente-demais/p/8943/">TOTALMENTE DEMAIS</a></h3></li><li class=""><h3><a href="http://globoplay.globo.com/alem-do-tempo/p/8811/">ALÃM DO TEMPO</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://globoplay.globo.com/v/4671530/" title="Garoto Ã© baleado enquanto dormia com a mÃ£e em casa"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/2VAMPdejvs-wC-DaQWAleU71IqM=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/K5uRrFbixe1AeFyqzWB-RwY7SOY=/245x130/filters:max_age(3600)/s03.video.glbimg.com/deo/vi/30/15/4671530" alt="Garoto Ã© baleado enquanto dormia com a mÃ£e em casa" title="Garoto Ã© baleado enquanto dormia com a mÃ£e em casa"
                    data-original-image="s2.glbimg.com/K5uRrFbixe1AeFyqzWB-RwY7SOY=/245x130/filters:max_age(3600)/s03.video.glbimg.com/deo/vi/30/15/4671530" data-url-smart_horizontal="NOrJH_xuZXu4o6skfzAXOwwqrn8=/90x56/smart/filters:strip_icc()/" data-url-smart="NOrJH_xuZXu4o6skfzAXOwwqrn8=/90x56/smart/filters:strip_icc()/" data-url-feature="NOrJH_xuZXu4o6skfzAXOwwqrn8=/90x56/smart/filters:strip_icc()/" data-url-tablet="kCSMwGrrOcYzMJsPbh3aGXPC2CY=/160x95/smart/filters:strip_icc()/" data-url-desktop="bHLDMCCN_WfSY73pHWNzwq4qw7c=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><h3>rjTV</h3><p>Garoto Ã© baleado enquanto dormia com a mÃ£e em casa</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://globoplay.globo.com/v/4671331/" title="RogÃ©rio Ceni se despede dos campos ao lado de 60 mil fÃ£s"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/r0aDUwujl27shUGAnRgrHLXJC5A=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/HG1NJk972bu7Sm9iOZRVNRTcnjY=/245x130/filters:max_age(3600)/s04.video.glbimg.com/deo/vi/31/13/4671331" alt="RogÃ©rio Ceni se despede dos campos ao lado de 60 mil fÃ£s" title="RogÃ©rio Ceni se despede dos campos ao lado de 60 mil fÃ£s"
                    data-original-image="s2.glbimg.com/HG1NJk972bu7Sm9iOZRVNRTcnjY=/245x130/filters:max_age(3600)/s04.video.glbimg.com/deo/vi/31/13/4671331" data-url-smart_horizontal="pbCcGiaPcg8zw_KX4loYO4O3yfw=/90x56/smart/filters:strip_icc()/" data-url-smart="pbCcGiaPcg8zw_KX4loYO4O3yfw=/90x56/smart/filters:strip_icc()/" data-url-feature="pbCcGiaPcg8zw_KX4loYO4O3yfw=/90x56/smart/filters:strip_icc()/" data-url-tablet="Z4HxxIwFKx-H-v0cuTQ-yk6k7IE=/160x95/smart/filters:strip_icc()/" data-url-desktop="cn0Gj517muBXjYIhp5t7VRgGvYE=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><h3>jornal da globo</h3><p>RogÃ©rio Ceni se despede dos campos ao lado de 60 mil fÃ£s</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://globotv.globo.com/rede-globo/rede-globo/v/totalmente-arthur-fica-furioso-com-romance-de-jonatas-e-eliza/4666775/" title="Arthur fica furioso com romance de Jonatas e Eliza"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/kfAvHPVgjUF6PKz-tdW6X5GcwYs=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/q1MPK4jw_zW1gs3rW8OXjCuUrc8=/335x188/filters:max_age(3600)/s04.video.glbimg.com/deo/vi/75/67/4666775" alt="Arthur fica furioso com romance de Jonatas e Eliza" title="Arthur fica furioso com romance de Jonatas e Eliza"
                    data-original-image="s2.glbimg.com/q1MPK4jw_zW1gs3rW8OXjCuUrc8=/335x188/filters:max_age(3600)/s04.video.glbimg.com/deo/vi/75/67/4666775" data-url-smart_horizontal="WLPtJmt_HVZc0aSc4C03el0P5so=/90x56/smart/filters:strip_icc()/" data-url-smart="WLPtJmt_HVZc0aSc4C03el0P5so=/90x56/smart/filters:strip_icc()/" data-url-feature="WLPtJmt_HVZc0aSc4C03el0P5so=/90x56/smart/filters:strip_icc()/" data-url-tablet="dl_6Ca51aDLta-0PWMKd5a_mn1E=/160x95/smart/filters:strip_icc()/" data-url-desktop="bBtxcNtStBtk-EifDvdeJmCrc4g=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><h3>TOTALMENTE DEMAIS</h3><p>Arthur fica furioso com <br />romance de Jonatas e Eliza</p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://globoplay.globo.com/v/4670657/" title="Pedro estranha que EmÃ­lia ainda queria se vingar de VitÃ³ria"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/47RYHB3eHhEWr612RHn9AWGeHxU=/filters:quality(1):strip_icc():blur(1)/s2.glbimg.com/GorxDiQMYdO-m_SZvKeKl7xxoeQ=/245x130/filters:max_age(3600)/s02.video.glbimg.com/deo/vi/57/06/4670657" alt="Pedro estranha que EmÃ­lia ainda queria se vingar de VitÃ³ria" title="Pedro estranha que EmÃ­lia ainda queria se vingar de VitÃ³ria"
                    data-original-image="s2.glbimg.com/GorxDiQMYdO-m_SZvKeKl7xxoeQ=/245x130/filters:max_age(3600)/s02.video.glbimg.com/deo/vi/57/06/4670657" data-url-smart_horizontal="koV9vZJituri7cvbj6reLrlirSs=/90x56/smart/filters:strip_icc()/" data-url-smart="koV9vZJituri7cvbj6reLrlirSs=/90x56/smart/filters:strip_icc()/" data-url-feature="koV9vZJituri7cvbj6reLrlirSs=/90x56/smart/filters:strip_icc()/" data-url-tablet="f5wnnYLGV0SDKym15nVTuOTQnuw=/160x95/smart/filters:strip_icc()/" data-url-desktop="o6upWT_nTNxJewY06aBI7YRNIfo=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><h3>alÃ©m do tempo</h3><p>Pedro estranha que EmÃ­lia ainda queria se vingar de VitÃ³ria</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_globoplay']);</script></section><section class="area topglobocom"><div class="container area"><div class="separator"></div><div id="topglobocom3" class="clearfix destaque topglobocom3"><div class="topo"><h3 class="titulo">
            top <strong>globo</strong><span class="subtitulo">tÃ¡ todo mundo clicando...</span></h3></div><div id="mais-vistas" class="clearfix mais-vistas tab-content analytics-area analytics-id-R"><div class="slider"><ol class="noticias analytics-area analytics-id-J"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://extra.globo.com/noticias/mundo/brasileira-que-havia-sumido-na-espanha-esta-sedada-teve-pertences-roubados-18276184.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Brasileira que havia sumido na Espanha estÃ¡ sedada e teve pertences roubados</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://oglobo.globo.com/brasil/parecer-de-renan-ao-stf-diz-que-senado-pode-arquivar-impeachment-18274660" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Parecer de Renan ao STF diz que Senado pode arquivar impeachment<br /></span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/noticia/2015/12/nao-pude-evitar-diz-caminhoneiro-sobre-acidente-com-cinco-mortes.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">&#39;NÃ£o pude evitar&#39;, diz caminhoneiro sobre acidente com cinco mortes<br /></span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/bahia/noticia/2015/12/me-sinto-acabado-diz-tio-de-crianca-morta-durante-festa-em-escola-de-pe.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">&#39;Me sinto acabado&#39;, diz tio de crianÃ§a morta durante festa em escola de Pernambuco</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/politica/operacao-lava-jato/noticia/2015/12/ministro-do-stf-autoriza-transferencia-de-delcidio-para-quartel-da-pm-no-df.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Ministro do STF autoriza transferÃªncia de DelcÃ­dio para quartel da PM no DF<br /></span></span></a></div></div></li></ol><ol class="esportes analytics-area analytics-id-E"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://sportv.globo.com/site/combate/noticia/2015/12/aldo-imita-pose-de-conor-mcgregor-em-pesagem-do-ufc-194.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Apesar de territÃ³rio hostil, JosÃ© Aldo tira McGregor do sÃ©rio em pesagem</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/handebol/noticia/2015/12/dara-faz-do-meio-da-quadra-na-ultima-bola-brasil-bate-franca-e-avanca-em-1.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Dara faz do meio da quadra na Ãºltima bola, Brasil vence e pega a RomÃªnia</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://sportv.globo.com/site/combate/noticia/2015/12/aldo-diz-que-encarada-mexeu-com-mcgregor-ficou-todo-sem-graca.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Aldo diz que encarada mexeu com McGregor: &#39;Ficou todo sem graÃ§a&#39;<br /></span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/futebol/times/sao-paulo/noticia/2015/12/goleiro-artilheiro-e-cantor-rogerio-ceni-se-despede-em-festa-no-morumbi.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Goleiro, artilheiro e cantor: RogÃ©rio Ceni se despede em festa no Morumbi<br /></span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://sportv.globo.com/site/combate/noticia/2015/12/ronda-rousey-cumpre-promessa-e-vai-baile-da-marinha-com-fuzileiro-naval.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Ronda Rousey cumpre promessa e vai a baile da Marinha com fuzileiro naval<br /></span></span></a></div></div></li></ol><ol class="entretenimento analytics-area analytics-id-M"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://extra.globo.com/tv-e-lazer/telinha/totalmente-demais-germano-pai-de-eliza-18280537.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">&#39;Totalmente demais&#39;: Eliza Ã© filha do rico empresÃ¡rio Germano</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://gshow.globo.com/novelas/totalmente-demais/vem-por-ai/noticia/2015/12/eliza-ganha-primeira-etapa-do-concurso-garota-totalmente-d.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Mais &#39;Totalmente Demais&#39;: Eliza ganha a primeira etapa do concurso</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/vem-por-ai/noticia/2015/12/kiki-explode-e-coloca-romero-sob-mira-de-seu-revolver.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">&#39;A Regra&#39;: Kiki explode e coloca Romero sob a mira de seu revÃ³lver<br /></span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/vem-por-ai/noticia/2015/12/romero-descobre-que-ze-maria-e-o-pai-da-filha-de-kiki.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Mais &#39;Regra&#39;: Romero descobre que ZÃ© Maria Ã© o pai da filha de Kiki<br /></span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/noite/noticia/2015/12/claudia-raia-baba-pelos-filhos-enzo-e-sophia-orgulhosa.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Claudia Raia posta look de formatura de Enzo e baba pelos filhos: &#39;Orgulhosa&#39;</span></span></a></div></div></li></ol></div></div><div class="controls-area"><ul class="step-marker"><li class=" glb-hl-style-noticias"><div class=""><a href="#" data-related=""><div class="ball active"></div></a></div></li><li class=" glb-hl-style-esporte"><div class=""><a href="#" data-related=""><div class="ball"></div></a></div></li><li class=" glb-hl-style-entretenimento"><div class=""><a href="#" data-related=""><div class="ball "></div></a></div></li></ul></div></div></div></section><section class="area area-pre-servicos analytics-area analytics-id-O"><div class="container area"><section class="franja-globosatplay franja-inferior analytics-area analytics-id-A"><div style="height: 0; width: 0; position: absolute; visibility: hidden"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><path id="a" d="M324.327 4.747c-13.334 0-24.22 10.88-24.22 24.216s10.886 24.215 24.22 24.215c13.333 0 24.213-10.88 24.213-24.215S337.66 4.747 324.327 4.747zm9.425 26.764l-13.615 7.875c-.694.403-1.355.594-1.94.597h-.033c-1.438-.008-2.43-1.16-2.43-3.142V21.088c0-1.977.99-3.132 2.425-3.144h.046c.583.005 1.24.198 1.932.6l13.615 7.874c2.425 1.4 2.425 3.692 0 5.093z"/></defs><symbol id="globosatplay__bis_play" viewBox="0 0 295.891 167.449"><path fill="#25B5E9" d="M136.175 18.26h24.027v78.755h-24.027zM111.892 48.27c1.396-2.798 2.19-5.947 2.19-9.274 0-11.324-9.066-20.522-20.336-20.735H68.48v78.755l29.61-.025c14.408-.19 26.07-11.97 26.07-26.42.002-9.177-4.64-17.483-12.268-22.3M200.714 26.49c-10.937 10.22-11.853 20.946-11.853 30.126l-.002.798c0 6.963-.676 8.996-4.01 12.1l-.098.1c-1.638 1.562-4.397 2.6-12.53 2.6v24.8c10.222 0 20.89-1.144 29.592-9.4 10.928-10.227 11.845-20.953 11.845-30.126l.003-.796c0-6.968 2.073-10.346 4.03-12.114 1.948-1.76 7.288-1.5 9.64-1.5V18.26c-9.91.245-19.553 1.526-26.616 8.23M87.782 149.156c10.597 0 19.245-8.65 19.245-19.248 0-10.6-8.648-19.246-19.245-19.246-10.6 0-19.25 8.647-19.25 19.246 0 10.598 8.652 19.248 19.25 19.248m-6.83-25.506c0-1.57.786-2.49 1.927-2.498h.036c.464.004.985.157 1.535.475l10.823 6.26c1.928 1.114 1.928 2.935 0 4.048l-10.822 6.257c-.55.322-1.075.473-1.54.476h-.027c-1.143-.008-1.932-.922-1.932-2.5V123.65z"/><path fill="#99B1C8" d="M115.258 149.124V110.63h14.46c1.85 0 3.497.298 4.944.894 1.446.597 2.662 1.414 3.645 2.452.982 1.04 1.734 2.242 2.256 3.61.52 1.364.78 2.797.78 4.3 0 1.5-.26 2.933-.78 4.297-.52 1.367-1.283 2.568-2.285 3.606-1.004 1.04-2.22 1.857-3.645 2.453-1.43.597-3.068.895-4.92.895h-11.166v15.986h-3.29zm14.082-18.988c1.27 0 2.424-.202 3.463-.606 1.04-.405 1.933-.972 2.684-1.702.75-.73 1.326-1.598 1.73-2.597.405-1 .605-2.116.605-3.346 0-1.193-.2-2.3-.605-3.32-.404-1.02-.98-1.894-1.73-2.625-.75-.732-1.645-1.3-2.684-1.704-1.04-.403-2.193-.605-3.463-.605h-10.793v16.507h10.793zM185.072 110.63h-4.105l-14.55 35.553h-18.474V110.63h-3.295v38.558h24.354l3.815-9.48h20.405l3.814 9.48h3.814l-15.778-38.558zm-11.156 26.07l9.075-22.544 9.133 22.545h-18.207zM208.795 149.183v-16.475l-15.32-22.084h3.99l12.978 18.96 12.98-18.96h3.987l-15.32 22.084v16.475"/></symbol><symbol id="globosatplay__brasil_play" viewBox="0 0 295.889 167.449"><path fill="#2C3488" d="M182.486.168h-39.88c1.502.426 2.98 1.017 4.396 1.835l34.226 19.762-16.834 30.95-4.677-10.166c6.427-2.052 8.255-5.49 8.808-9.033 1.442-9.142-4.873-12.247-12.63-12.247h-14.798l-5.762 36.57h11.083l2.327-14.794 6.04 14.795h17.791l3.822-7.368h10.14l1.33 7.37h11.583l-6.788-29.475 4.113 2.375c4.496 2.587 7.53 6.73 8.842 11.355V23.3C205.62 10.527 195.262.17 182.487.17m-25.374 33.515c-.446 3.1-2.66 3.768-4.986 3.768h-2.496l1.33-8.37h2.442c2.437.002 4.154 1.722 3.71 4.603m22.168 10.86l5.044-10.472 1.22 10.472h-6.264zM105.473 35.645l-3.498 22.198h16.954c9.09 0 14.02-3.602 14.963-9.698.83-5.098-2.107-8.424-5.6-9.586 3.492-.832 6.098-2.826 6.762-7.148 1.22-7.76-5.21-10.14-11.47-10.14h-9.812l7.078-12.26c2.59-4.5 6.734-7.533 11.363-8.843H113.41c-12.774 0-23.137 10.356-23.137 23.13V63.2c.424-1.51 1.018-2.994 1.838-4.413l13.363-23.14zm12.516-6.948h2.436c1.94 0 3.437 1.664 3.05 4.1-.39 2.605-2.274 3.602-4.602 3.602h-2.104l1.22-7.703zm-2.107 13.3h2.493c2.77 0 4.433 1.825 3.99 4.597-.444 2.77-2.882 3.768-5.263 3.768h-2.552l1.333-8.366zM122.128 98.228c.312.006.62.014.948.014 9.752 0 14.904-4.212 16.127-10.638 1.107-5.544-2.607-8.593-6.54-10.75-1.995-1.108-4.045-1.995-5.54-2.88-1.44-.833-2.553-1.775-2.33-2.94.167-1.33 2.162-2.437 4.214-2.437 4.156 0 6.76 1.33 9.64 3.27l4.877-8.645c-3.714-2.05-6.87-3.435-14.02-3.435-6.703 0-15.35 2.38-16.623 10.527-.886 5.594 2.938 8.365 6.985 10.417 1.992 1 4.043 1.83 5.54 2.718 1.495.886 2.546 1.937 2.325 3.323-.277 1.608-2.937 2.383-4.71 2.383-3.49 0-6.318-1.33-9.863-4.1l-3.464 5.993-10.576-6.107c-4.5-2.595-7.535-6.745-8.844-11.38v18.822c0 12.774 10.36 23.128 23.137 23.128h39.863c-1.498-.422-2.97-1.014-4.382-1.83L122.13 98.23z"/><path fill="#2C3488" d="M185.002 89.43h-13.834l4.547-28.756H164.63l-5.76 36.57h21.62l-5.445 9.427c-2.588 4.497-6.73 7.53-11.354 8.84h18.796c12.775 0 23.133-10.354 23.133-23.128V52.518c-.426 1.497-1.015 2.97-1.827 4.38l-18.79 32.532zM158.924 60.674H147.84l-5.762 36.57h11.082M87.756 167.247c10.597 0 19.243-8.65 19.243-19.246 0-10.598-8.647-19.247-19.244-19.247-10.6 0-19.25 8.65-19.25 19.248s8.65 19.247 19.25 19.247m-6.832-25.505c0-1.572.79-2.49 1.928-2.498h.038c.464.004.985.158 1.535.475l10.82 6.26c1.93 1.113 1.93 2.934 0 4.046l-10.82 6.258c-.552.32-1.077.473-1.542.475h-.027c-1.143-.007-1.932-.923-1.932-2.498v-12.52z"/><path fill="#99B1C8" d="M115.23 167.215V128.72h14.46c1.85 0 3.498.298 4.944.894 1.447.597 2.662 1.415 3.645 2.453.98 1.04 1.733 2.242 2.255 3.61.52 1.364.78 2.798.78 4.298s-.26 2.934-.78 4.3c-.52 1.366-1.283 2.567-2.285 3.605-1.003 1.038-2.22 1.858-3.645 2.454-1.43.596-3.067.895-4.918.895H118.52v15.985h-3.29zm14.082-18.988c1.27 0 2.424-.2 3.463-.605 1.038-.404 1.934-.972 2.684-1.702.75-.73 1.327-1.597 1.73-2.598.403-1 .604-2.116.604-3.347 0-1.193-.2-2.3-.605-3.32-.403-1.018-.98-1.892-1.73-2.625-.75-.73-1.647-1.298-2.685-1.702-1.04-.404-2.193-.605-3.463-.605H118.52v16.505h10.792zM185.045 128.722h-4.104l-14.55 35.552h-18.475v-35.552h-3.295v38.56h24.354l3.816-9.482h20.404l3.814 9.48h3.814l-15.777-38.558zm-11.156 26.072l9.074-22.545 9.132 22.544H173.89zM208.77 167.274V150.8l-15.32-22.085h3.988l12.98 18.962 12.977-18.962h3.988l-15.32 22.085v16.474"/></symbol><symbol id="globosatplay__combate_play" viewBox="0 0 295.89 167.449"><path fill="#CF2028" d="M68.502 61.48l-2.438 9.318c-.38 1.386-1.807 2.562-3.273 2.562H43.644c-1.975 0-3.19-1.43-2.686-3.403l6.424-25.02c.335-1.387 1.808-2.563 3.274-2.563h19.148c1.973 0 3.19 1.552 2.686 3.444l-2.18 8.438H62.83l.925-3.566c.168-.63-.21-1.136-.84-1.136h-8.018c-.676 0-1.387.632-1.555 1.26l-3.61 14.234c-.17.632.206 1.092.837 1.092h7.978c.67 0 1.387-.545 1.553-1.175l.886-3.483h7.516zM94.53 70.797c-.335 1.386-1.806 2.562-3.272 2.562h-19.23c-1.975 0-3.11-1.43-2.604-3.404l6.383-25.02c.334-1.387 1.846-2.563 3.314-2.563h19.147c1.975 0 3.148 1.552 2.69 3.443l-6.426 24.98zm-16.415-5.754c-.168.633.293 1.093.922 1.093h7.98c.675 0 1.34-.545 1.513-1.175l3.695-14.272c.166-.63-.213-1.138-.84-1.138h-8.02c-.715 0-1.39.632-1.557 1.26l-3.693 14.233zM128.12 42.372h12.26c1.978 0 3.15 1.47 2.647 3.444l-7.055 27.543h-7.475l5.795-22.673c.17-.675-.252-1.136-.88-1.136h-5.542c-.672 0-1.387.59-1.555 1.26l-5.752 22.55h-7.516l5.793-22.672c.17-.675-.21-1.136-.838-1.136H111.2l-6.09 23.81h-7.474l7.895-30.988h15.495c1.594 0 2.727.502 3.398 1.3.97-.8 2.186-1.302 3.698-1.302M168.936 60.132l-2.564 10.246c-.46 1.888-2.1 2.98-4.28 2.98H139.96l7.855-30.987h21.035c2.938 0 4.197 1.682 3.486 4.37l-1.514 5.667c-.336 1.218-1.05 2.1-2.057 2.645l-1.637.88c1.722.885 2.224 2.44 1.808 4.2m-18.352 6.004h7.98c.67 0 1.34-.505 1.51-1.175l1.09-4.616c.17-.714-.253-1.133-1.048-.714l-9.785 5.162c-.713.38-.627 1.344.254 1.344m4.494-16.586c-.672 0-1.387.507-1.557 1.178l-1.173 4.703c-.168.67.21.965.838.63l9.826-5.166c.632-.336.757-.46.8-.755.04-.295-.21-.59-.547-.59h-8.185zM176.488 73.36h-7.516l7.307-28.426c.337-1.386 1.85-2.562 3.276-2.562h19.23c1.89 0 3.106 1.47 2.604 3.444l-7.012 27.543h-7.56l2.964-11.29-10.465-.03-2.83 11.32zm4.74-18.417l10.33-.017 1.06-4.238c.164-.63-.213-1.138-.8-1.138h-8.022c-.672 0-1.385.632-1.553 1.26 0 0-1.016 4.093-1.016 4.133M202.814 49.55l1.85-7.178h24.73l-1.847 7.178h-8.65l-6.088 23.81h-7.472l6.086-23.81M248.244 54.253L246.4 61.48h-12.22l-.927 3.564c-.164.632.213 1.092.844 1.092h14.863l-1.848 7.223h-19.988c-1.97 0-3.104-1.43-2.602-3.404l6.34-25.02c.336-1.387 1.893-2.563 3.36-2.563h20.823L253.2 49.55h-14.74c-.67 0-1.384.633-1.554 1.26l-.88 3.444h12.218zM87.752 125.044c10.598 0 19.244-8.65 19.244-19.246 0-10.6-8.646-19.247-19.244-19.247S68.504 95.2 68.504 105.8c0 10.597 8.65 19.246 19.248 19.246m-6.83-25.505c0-1.573.787-2.49 1.928-2.5h.037c.463.005.984.158 1.535.476l10.822 6.26c1.928 1.114 1.928 2.935 0 4.05l-10.822 6.255c-.553.322-1.078.474-1.54.476h-.03c-1.143-.005-1.93-.922-1.93-2.497V99.54z"/><path fill="#99B1C8" d="M115.227 125.012V86.52h14.46c1.85 0 3.5.297 4.946.892 1.445.597 2.66 1.415 3.643 2.454.984 1.04 1.736 2.24 2.258 3.607.52 1.367.78 2.798.78 4.3 0 1.5-.26 2.935-.78 4.3-.52 1.366-1.285 2.567-2.287 3.607-1.002 1.038-2.22 1.856-3.645 2.453-1.428.598-3.066.896-4.918.896h-11.166v15.984h-3.29zm14.082-18.988c1.27 0 2.423-.202 3.462-.604 1.04-.405 1.934-.972 2.686-1.703.748-.73 1.326-1.597 1.73-2.597.405-1 .606-2.116.606-3.348 0-1.192-.2-2.3-.607-3.318-.402-1.02-.98-1.894-1.73-2.626-.75-.73-1.645-1.298-2.685-1.703-1.04-.403-2.193-.604-3.463-.604h-10.792v16.506h10.79zM185.043 86.52h-4.105l-14.55 35.55h-18.476V86.52h-3.295v38.558h24.355l3.814-9.48h20.404l3.814 9.48h3.816l-15.777-38.56zm-11.156 26.07l9.074-22.545 9.132 22.545h-18.205zM208.766 125.072v-16.476l-15.32-22.083h3.99l12.978 18.96 12.977-18.96h3.99l-15.32 22.083v16.476"/></symbol><symbol id="globosatplay__globonews_play" viewBox="0 0 295.889 167.449"><path fill="#EC1C24" d="M235.632 53.06c-6.222-1.726-12.504-2.404-12.752-5.545.005-1.905 1.71-2.287 3.144-2.287 1.052 0 2.55.31 3.432.896.915.61 1.565 1.482 1.587 2.725v-.002h15.11c-.31-10.797-11.337-15.436-21.76-13.81h-15.767l-11.443 21.018V35.04h-14.385l-13.312 24.424V35.04h-50.29v19.27l-10.482-19.27h-16.25v45.65h15.13V60.62l11.27 20.07h50.762l13.185-23.317V80.69h14.754l13.56-24.124c2.805 2.973 7.504 4.17 11.813 5.39 7.29 2.08 9.128 2.67 9.128 4.864 0 2.146-2.86 3.265-4.317 3.265-1.66 0-2.97-.998-3.616-2.74-.167-.434-.312-1.056-.312-1.997h-16.875c.304 13.797 15.784 15.562 20.744 15.562 9.793 0 20.27-3.698 20.27-15.158 0-8.068-6.106-10.97-12.328-12.692m-79.557 15.12h-22.14V63.3h17.147V52.43h-17.146v-4.883h22.14V68.18z"/><path fill-rule="evenodd" clip-rule="evenodd" fill="#B1B3B5" d="M91.036 57.867c0 3.104-.016 6.17-.314 9.137-.164 1.617-.973 1.988-2.475 2.148-5.2.544-10.955.67-16.344.653-5.388.018-11.145-.11-16.344-.653-1.507-.16-2.312-.53-2.48-2.148-.295-2.968-.312-6.033-.312-9.137 0-3.106.018-6.173.313-9.14.168-1.615.973-1.986 2.48-2.147 5.198-.543 10.955-.67 16.343-.655 5.39-.014 11.145.112 16.344.655 1.502.16 2.31.532 2.475 2.148.3 2.966.314 6.032.314 9.14m4.84 0c0 13.167-10.74 23.91-23.973 23.91-13.232 0-23.975-10.743-23.975-23.91 0-13.173 10.742-23.914 23.975-23.914 13.232 0 23.973 10.74 23.973 23.913m-33.715 0c0-5.33 4.348-9.678 9.743-9.678 5.394 0 9.742 4.346 9.742 9.677 0 5.33-4.35 9.677-9.742 9.677-5.395 0-9.742-4.348-9.742-9.677"/><path fill="#EC1C24" d="M87.753 133.464c10.598 0 19.245-8.65 19.245-19.248 0-10.6-8.647-19.246-19.245-19.246s-19.248 8.647-19.248 19.246c0 10.598 8.65 19.248 19.248 19.248m-6.83-25.506c0-1.57.787-2.49 1.928-2.498h.038c.464.004.986.157 1.535.475l10.822 6.26c1.928 1.114 1.928 2.935 0 4.048L84.423 122.5c-.552.322-1.077.473-1.54.476h-.03c-1.143-.008-1.93-.922-1.93-2.5V107.96z"/><path fill="#99B1C8" d="M115.23 133.43V94.94h14.458c1.85 0 3.5.298 4.945.894 1.446.597 2.66 1.414 3.645 2.452.982 1.04 1.734 2.242 2.256 3.61.52 1.364.78 2.797.78 4.3 0 1.5-.26 2.933-.78 4.297-.52 1.368-1.283 2.57-2.286 3.607-1.003 1.04-2.218 1.858-3.644 2.454-1.43.598-3.068.896-4.92.896h-11.166v15.986h-3.29zm14.08-18.987c1.27 0 2.426-.202 3.465-.606 1.038-.405 1.933-.972 2.684-1.702.75-.73 1.325-1.598 1.73-2.597.403-1 .604-2.117.604-3.347 0-1.192-.2-2.3-.605-3.32-.405-1.018-.98-1.893-1.73-2.624-.752-.73-1.647-1.298-2.685-1.703-1.04-.403-2.194-.605-3.464-.605h-10.79v16.506h10.79zM185.044 94.938h-4.105l-14.55 35.553h-18.475V94.94h-3.295v38.558h24.354l3.815-9.48h20.403l3.814 9.48h3.815l-15.778-38.558zm-11.156 26.07l9.074-22.544 9.132 22.545h-18.206zM208.767 133.49v-16.474l-15.32-22.085h3.99l12.978 18.963 12.978-18.962h3.99l-15.32 22.086v16.475"/></symbol><symbol id="globosatplay__globosat_play" viewBox="0 0 295.889 167.449"><path fill="#4576A1" d="M72.195 51.673l2.387-5.126-2.867-7.704-5.62 11.38M41.076 53.863l5.092 6.374 3.543.98 2.335-4.477M79.79 54.882l-3.915 8.346-14.404-3.836-7.087 14.202-7.865-2.624 4.87 5.28 7.717 2.682 6.258-13.048 13.33 3.825 3.504-7.972"/><path fill="#4576A1" d="M61.016 58.384l14.386 3.83 3.76-8.012-7.12-1.686v.003l-7.192-1.712 6.222-12.6-7.64-1.42-6.362 12.118-12.332-2.93-3.894 6.94L53.3 56.184l-2.743 5.267.007.002-4.445 8.48 7.827 2.613"/><path fill-rule="evenodd" clip-rule="evenodd" fill="#4576A1" d="M149.352 44.935h8.602c5.793 0 7.998 3.66 8.014 6.914.012 2.36-.64 3.944-2.365 5.22 2.195 1.144 3.326 3.445 3.326 5.805 0 4.464-2.935 7.912-8.17 7.912h-9.407V44.935zm8.462 5.658l-1.857-.003v4.367h1.83c1.25 0 2.152-.937 2.148-2.258-.003-1.37-1.152-2.107-2.12-2.107m.218 9.316h-2.076v5.162h2.23c1.417 0 2.505-1.262 2.505-2.56 0-1.41-1.092-2.603-2.66-2.603m-23.553-8.964c3.804 0 6.91 3.11 6.91 6.917s-3.106 6.915-6.91 6.915c-3.813 0-6.92-3.107-6.92-6.915s3.106-6.917 6.92-6.917m0-6.726c7.51 0 13.638 6.13 13.638 13.644 0 7.508-6.13 13.637-13.64 13.637-7.513 0-13.64-6.128-13.64-13.636 0-7.514 6.127-13.644 13.64-13.644m46.797 6.726c3.81 0 6.918 3.11 6.918 6.917s-3.11 6.915-6.918 6.915-6.916-3.107-6.916-6.915 3.107-6.917 6.915-6.917m0-6.726c7.512 0 13.64 6.13 13.64 13.644 0 7.508-6.128 13.637-13.64 13.637-7.508 0-13.64-6.128-13.64-13.636 0-7.514 6.133-13.644 13.64-13.644m-64.048.715l-6.57 10.92h-9.43v5.653h6.428c-1.225 1.82-2.89 3.12-5.902 3.1-3.482-.02-6.803-2.312-6.803-7.13 0-4.092 3.525-6.837 6.662-6.833 2.396 0 5.32.718 7.3 3.998l3.526-5.84c-2.938-3.11-6.19-4.575-11.21-4.575-7.265.157-12.962 6.37-12.98 13.073 0 8.202 6.57 13.486 13.188 13.486h19.375V64.97h-7.897l12.084-20.035h-7.77z"/><path fill="#4576A1" d="M205.102 71.498c-5.293 0-9.484-2.736-9.484-8.345v-.855h6.68c0 1.706.78 3.588 2.732 3.588 1.385 0 2.555-1.103 2.555-2.52 0-1.707-1.42-2.274-2.803-2.88-.78-.355-1.562-.673-2.346-.99-3.41-1.426-6.428-3.41-6.428-7.532 0-4.9 4.727-7.744 9.2-7.744 2.556 0 5.433.96 7.1 2.985 1.353 1.67 1.673 3.125 1.74 5.184h-6.64c-.213-1.454-.71-2.56-2.414-2.56-1.172 0-2.31.816-2.31 2.062 0 .39.036.782.25 1.1.638 1.065 4.083 2.417 5.187 2.915 3.478 1.598 6.146 3.405 6.146 7.53 0 5.504-3.98 8.06-9.164 8.06M249.898 50.615v20.17h-6.712v-20.17h-5.184v-5.68h17.045v5.68M227.77 44.946l-15.587 25.84h7.768l2.157-3.576h7.94v3.572h6.72V44.946h-8.996zm2.277 17.35h-4.973l4.973-8.238v8.237zM87.74 130.626c10.598 0 19.244-8.648 19.244-19.247 0-10.6-8.646-19.247-19.244-19.247s-19.25 8.648-19.25 19.246c0 10.598 8.653 19.246 19.25 19.246m-6.83-25.506c0-1.57.787-2.49 1.926-2.498h.037c.465.005.984.157 1.535.475l10.822 6.26c1.928 1.114 1.928 2.937 0 4.05l-10.822 6.256c-.55.32-1.076.473-1.54.475h-.028c-1.143-.006-1.93-.922-1.93-2.497v-12.52z"/><path fill="#99B1C8" d="M115.215 130.594V92.102h14.46c1.85 0 3.5.297 4.946.893 1.446.598 2.66 1.414 3.644 2.453.982 1.038 1.734 2.242 2.256 3.606.52 1.366.78 2.8.78 4.3 0 1.502-.26 2.935-.78 4.3-.52 1.365-1.283 2.566-2.285 3.606-1.002 1.038-2.22 1.857-3.645 2.453-1.43.598-3.068.896-4.92.896h-11.166v15.984h-3.29zm14.082-18.988c1.27 0 2.424-.202 3.463-.604 1.04-.406 1.934-.972 2.684-1.703s1.326-1.6 1.73-2.6.605-2.116.605-3.347c0-1.192-.202-2.3-.606-3.318-.404-1.02-.98-1.894-1.73-2.626-.75-.73-1.645-1.3-2.684-1.705-1.04-.403-2.193-.604-3.463-.604h-10.793v16.507h10.793zM185.03 92.102h-4.105l-14.55 35.55H147.9v-35.55h-3.295v38.56h24.355l3.814-9.483h20.404l3.814 9.48h3.814L185.03 92.103zm-11.157 26.07l9.076-22.545 9.13 22.546h-18.207zM208.752 130.653V114.18l-15.318-22.085h3.988l12.978 18.96 12.98-18.96h3.987L212.05 114.18v16.473"/></symbol><symbol id="globosatplay__globosatplay" viewBox="0 0 500 58"><path fill-rule="evenodd" clip-rule="evenodd" fill="#98B0C7" d="M108.42 6.036h15.26c10.282 0 14.192 6.485 14.224 12.26.022 4.193-1.14 7.007-4.202 9.262 3.9 2.035 5.91 6.114 5.91 10.305 0 7.92-5.208 14.037-14.503 14.037h-16.69V6.036zm15.017 10.032l-3.3-.003v7.746h3.253c2.216 0 3.812-1.666 3.81-4.006-.005-2.423-2.043-3.735-3.763-3.736m.392 16.54h-3.692v9.155h3.958c2.52 0 4.443-2.236 4.443-4.545 0-2.505-1.936-4.61-4.71-4.61M82.026 16.695c6.758 0 12.27 5.515 12.27 12.272 0 6.757-5.512 12.273-12.27 12.273-6.76 0-12.272-5.517-12.272-12.273 0-6.757 5.513-12.272 12.272-12.272m0-11.928c13.326 0 24.2 10.874 24.2 24.2s-10.874 24.204-24.2 24.204c-13.328 0-24.205-10.875-24.205-24.202s10.877-24.2 24.205-24.2m83.038 11.927c6.757 0 12.27 5.515 12.27 12.272 0 6.757-5.514 12.273-12.27 12.273-6.758 0-12.274-5.517-12.274-12.273 0-6.757 5.518-12.272 12.275-12.272m0-11.928c13.325 0 24.2 10.874 24.2 24.2s-10.875 24.204-24.2 24.204c-13.327 0-24.206-10.875-24.206-24.202s10.878-24.2 24.205-24.2M51.417 6.035L39.764 25.4H23.027v10.035h11.408c-2.175 3.23-5.125 5.537-10.474 5.505-6.18-.035-12.07-4.104-12.07-12.655 0-7.258 6.262-12.128 11.824-12.125 4.25.004 9.437 1.27 12.948 7.096l6.26-10.362c-5.21-5.52-10.983-8.116-19.895-8.116C10.143 5.05.032 16.08 0 27.972 0 42.526 11.66 51.9 23.398 51.9h34.38V41.577H43.764l21.44-35.54H51.418z"/><path fill="#98B0C7" d="M207.336 53.162c-9.39 0-16.824-4.853-16.824-14.81V36.84h11.848c0 3.023 1.385 6.365 4.852 6.365 2.458 0 4.538-1.954 4.538-4.475 0-3.025-2.52-4.032-4.98-5.104-1.386-.63-2.77-1.197-4.158-1.766-6.048-2.52-11.405-6.048-11.405-13.36 0-8.693 8.38-13.734 16.32-13.734 4.538 0 9.644 1.7 12.604 5.292 2.397 2.96 2.963 5.546 3.09 9.2h-11.785c-.38-2.583-1.26-4.537-4.286-4.537-2.08 0-4.096 1.45-4.096 3.655 0 .694.063 1.388.44 1.955 1.134 1.89 7.248 4.285 9.2 5.167 6.177 2.834 10.902 6.05 10.902 13.36 0 9.768-7.06 14.305-16.26 14.305M286.824 16.108v35.79h-11.907V16.11h-9.203V6.036h30.25v10.072M247.565 6.053l-27.66 45.85h13.784l3.824-6.346h14.083v6.337h11.926V6.054h-15.958zm4.033 30.783h-8.816l8.816-14.615v14.616zM358.895 53.18V4.747h18.193c2.328 0 4.4.374 6.222 1.124 1.82.75 3.35 1.78 4.585 3.086 1.236 1.308 2.183 2.82 2.838 4.54s.983 3.52.983 5.408c0 1.888-.328 3.69-.983 5.41-.655 1.718-1.614 3.23-2.875 4.536-1.262 1.308-2.792 2.338-4.586 3.087-1.797.75-3.86 1.126-6.188 1.126h-14.05v20.112h-4.14zm17.718-23.89c1.597 0 3.05-.255 4.355-.763 1.307-.51 2.434-1.223 3.377-2.143.944-.92 1.67-2.01 2.18-3.268.507-1.258.76-2.662.76-4.21 0-1.5-.253-2.894-.76-4.177-.51-1.283-1.235-2.384-2.18-3.305-.943-.92-2.07-1.634-3.377-2.143-1.307-.508-2.76-.76-4.355-.76h-13.578v20.765h13.578zM446.732 4.748h-5.163l-18.307 44.73h-23.245V4.748h-4.146V53.26h30.642l4.8-11.928h25.672l4.8 11.928h4.8L446.732 4.748zM432.697 37.55l11.418-28.365 11.488 28.365h-22.906zM476.58 53.254v-20.73L457.307 4.74h5.018l16.33 23.857L494.98 4.74H500l-19.273 27.785v20.73"/><clipPath id="b"><use xlink:href="#a" overflow="visible"/></clipPath><linearGradient id="c" gradientUnits="userSpaceOnUse" x1="-281.142" y1="327.715" x2="-280.55" y2="327.715" gradientTransform="scale(81.6953) rotate(-45 257.577 -507.484)"><stop offset="0" stop-color="#7B61A6"/><stop offset=".061" stop-color="#8E5599"/><stop offset=".198" stop-color="#B53C80"/><stop offset=".324" stop-color="#D12B6E"/><stop offset=".435" stop-color="#E22063"/><stop offset=".518" stop-color="#E81C5F"/><stop offset=".587" stop-color="#E92859"/><stop offset=".714" stop-color="#ED484A"/><stop offset=".882" stop-color="#F37C31"/><stop offset="1" stop-color="#F7A51E"/></linearGradient><path clip-path="url(#b)" fill="url(#c)" d="M275.894 28.963l48.43-48.432 48.43 48.433-48.43 48.432"/></symbol><symbol id="globosatplay__gloob_play" viewBox="0 0 295.891 167.449"><path fill="#A3CC39" d="M165.266 39.822c-14.754 0-26.715 11.96-26.715 26.715 0 14.755 11.962 26.712 26.716 26.712 14.752 0 26.713-11.958 26.713-26.713 0-14.754-11.962-26.715-26.714-26.715m0 31.61c-2.71 0-4.898-2.19-4.898-4.895 0-2.703 2.19-4.9 4.898-4.9 2.703 0 4.896 2.197 4.896 4.9 0 2.704-2.193 4.894-4.896 4.894"/><path fill="#8375B5" d="M198.432 28.245c-7.867 0-14.246 6.38-14.246 14.247 0 7.87 6.38 14.247 14.246 14.247 7.87 0 14.248-6.378 14.248-14.248 0-7.868-6.377-14.247-14.248-14.247m.004 17.362c-1.723 0-3.12-1.393-3.12-3.115 0-1.72 1.397-3.117 3.12-3.117 1.72 0 3.113 1.397 3.113 3.117 0 1.723-1.396 3.115-3.114 3.115"/><path fill="#FFCA07" d="M124.137 22.9l21.046 3.71-11.755 66.644-21.045-3.712z"/><path fill="#EE4065" d="M238.043 54.372l-4.803-2.246 5.27-11.297-19.367-9.032-22.58 48.423 23.973 11.18c.006 0 .01.006.014.006l.184.085.004-.003c10.21 4.632 22.268.19 27.016-9.992 4.746-10.187.396-22.278-9.71-27.123M229.39 72.93c-.624 1.338-2.214 1.915-3.55 1.293-.03-.016-.057-.03-.084-.047l-.004.006-2.328-1.085 2.258-4.847 2.332 1.09-.004.004c.03.014.055.022.086.037 1.338.624 1.914 2.213 1.295 3.55"/><path fill="#25B2E7" d="M109.906 35.702l-31.45 14.666 6.772 14.525 11.64-5.427c-.618 5.004-3.716 9.596-8.626 11.882-7.578 3.535-16.586.26-20.12-7.32-3.53-7.575-.255-16.585 7.325-20.118 4.05-1.89 8.514-1.828 12.324-.204l7.827-18.933c-8.932-3.732-19.344-3.838-28.807.576-17.832 8.312-25.543 29.505-17.23 47.335 8.313 17.827 29.505 25.54 47.335 17.225 17.828-8.312 25.54-29.504 17.227-47.33-1.163-2.502-2.59-4.792-4.216-6.878M87.756 145.41c10.596 0 19.242-8.646 19.242-19.245s-8.646-19.246-19.242-19.246c-10.6 0-19.25 8.646-19.25 19.245s8.65 19.245 19.25 19.245m-6.832-25.504c0-1.572.787-2.49 1.928-2.498h.037c.464.004.983.156 1.534.474l10.822 6.26c1.928 1.114 1.928 2.936 0 4.05l-10.822 6.257c-.553.32-1.076.47-1.54.474h-.028c-1.143-.008-1.932-.922-1.932-2.497v-12.52z"/><path fill="#99B1C8" d="M115.23 145.38v-38.493h14.46c1.85 0 3.5.297 4.944.893 1.447.598 2.662 1.414 3.645 2.453s1.733 2.24 2.255 3.608c.52 1.366.78 2.8.78 4.3s-.26 2.933-.78 4.3c-.52 1.364-1.285 2.566-2.285 3.605-1.004 1.038-2.22 1.857-3.645 2.454-1.43.596-3.068.894-4.918.894H118.52v15.986h-3.29zm14.082-18.988c1.268 0 2.424-.2 3.463-.605 1.037-.403 1.932-.97 2.684-1.703.75-.73 1.325-1.597 1.73-2.596.403-1 .604-2.117.604-3.348 0-1.192-.2-2.3-.605-3.32s-.98-1.894-1.73-2.626c-.753-.73-1.647-1.298-2.685-1.7-1.04-.405-2.195-.606-3.463-.606H118.52v16.505h10.792zM185.043 106.887h-4.104l-14.55 35.552h-18.475v-35.553h-3.295v38.558h24.354l3.814-9.48h20.406l3.814 9.48h3.814l-15.78-38.558zm-11.154 26.07l9.073-22.545 9.133 22.545H173.89zM208.768 145.44v-16.476l-15.32-22.084h3.99l12.978 18.962 12.98-18.962h3.987l-15.32 22.084v16.475"/></symbol><symbol id="globosatplay__gnt_play" viewBox="0 0 295.89 167.449"><path fill-rule="evenodd" clip-rule="evenodd" fill="#6B4894" d="M60.37 41.958c-12.593 0-34.388 3.143-34.388 25.488 0 22.342 24.965 25.507 34.39 25.702 7.757.15 13.917-.502 20.405-2.636 0 0 .15 4.52-.1 8.215-.424 6.172-1.56 10.05-4.258 13.426-5.208 6.504-15.85 6.457-22.312 6.54-3.02-.003-8.455-.505-8.455-.505l.015 4.837c25.136 2.344 48.06.826 51.542-20.86.51-3.136.566-6.45.576-12.136.02-4.083 0-48.072 0-48.072H60.37zm20.407 42.488c0 .427-7.857 3.79-17.635 2.742-11.37-1.215-18.895-8.914-18.895-19.753 0-11.458 7.33-18.604 16.337-19.987 3.458-.53 8.563-.777 12.552-.62 4.072.152 7.64.616 7.64.616v37.002zM157.954 66.478c0-14.182-6.327-19.558-22.784-19.558-4.518 0-12.164.585-12.235.585V91.9H105.77V41.96h33.434c12.59 0 35.92-1.047 35.92 27.06V91.9h-17.17V66.478zM204.188 86.27c-6.637-4.058-6.014-12.093-6.01-20.095.016-1.473 0-6.177 0-16.838h19.528v-5.393h-19.528c.016-6.61-.004-15.295 0-22.087h-16.666v46.21c0 17.42 9.27 25.803 31.027 25.803 2.04 0 3.78-.068 6.077-.216v-5.418c-3.886.082-10.706.308-14.43-1.966"/><path fill="#6B4894" d="M130.28 145.56c10.597 0 19.244-8.65 19.244-19.247 0-10.6-8.647-19.247-19.245-19.247s-19.25 8.648-19.25 19.247c0 10.597 8.652 19.246 19.25 19.246m-6.832-25.506c0-1.572.788-2.49 1.928-2.498h.037c.464.004.985.157 1.535.475l10.82 6.26c1.93 1.115 1.93 2.936 0 4.05l-10.82 6.256c-.552.32-1.077.473-1.54.475h-.03c-1.142-.004-1.93-.92-1.93-2.496v-12.52z"/><path fill="#99B1C8" d="M157.755 145.527v-38.494h14.46c1.85 0 3.497.298 4.944.893 1.445.597 2.66 1.415 3.644 2.454.982 1.04 1.734 2.24 2.256 3.607.52 1.367.78 2.798.78 4.3 0 1.5-.26 2.935-.78 4.3-.52 1.366-1.283 2.567-2.285 3.607-1.004 1.038-2.22 1.856-3.645 2.453-1.43.598-3.068.896-4.92.896h-11.166v15.985h-3.29zm14.082-18.987c1.27 0 2.424-.202 3.463-.604 1.04-.405 1.933-.972 2.684-1.703.75-.73 1.326-1.597 1.73-2.597.405-1 .605-2.116.605-3.348 0-1.192-.2-2.3-.606-3.318-.404-1.02-.98-1.894-1.73-2.626-.75-.73-1.645-1.298-2.684-1.703-1.04-.402-2.193-.603-3.463-.603h-10.793v16.505h10.793zM227.57 107.034h-4.106l-14.55 35.552H190.44v-35.552h-3.295v38.56H211.5l3.814-9.482h20.405l3.813 9.48h3.814l-15.778-38.558zm-11.157 26.07l9.075-22.544 9.132 22.545h-18.207zM251.292 145.587V129.11l-15.32-22.082h3.99l12.978 18.96 12.98-18.96h3.987l-15.32 22.083v16.477"/></symbol><symbol id="globosatplay__megapix_play" viewBox="0 0 295.889 167.449"><path fill="#5DDB24" d="M82.494 126.062c7.5 7.5 19.7 7.5 27.2 0s7.5-19.7 0-27.1c-7.5-7.5-19.7-7.5-27.1 0-7.6 7.4-7.6 19.6-.1 27.1"/><path fill="#FFF" d="M91.994 103.762c-1.1 0-1.9.9-1.9 2.5v12.5c0 1.6.8 2.5 1.9 2.5.5 0 1-.2 1.5-.5l10.8-6.2c1.9-1.1 1.9-2.9 0-4l-10.8-6.2c-.5-.5-1-.6-1.5-.6z"/><path fill="#99B1C8" d="M123.395 131.662v-38.4h14.4c1.8 0 3.5.3 4.9.9s2.7 1.4 3.6 2.4c1 1 1.7 2.2 2.2 3.6s.8 2.8.8 4.3-.3 2.9-.8 4.3-1.3 2.6-2.3 3.6-2.2 1.9-3.6 2.4c-1.4.6-3.102.9-4.9.9h-11.1v15.9h-3.2v.1zm14.1-18.9c1.3 0 2.4-.2 3.5-.6 1-.4 1.9-1 2.7-1.7.7-.7 1.3-1.6 1.7-2.6.398-1 .6-2.1.6-3.3 0-1.2-.2-2.3-.6-3.3-.4-1-1-1.9-1.7-2.6s-1.6-1.3-2.7-1.7c-1-.4-2.2-.6-3.5-.6h-10.8v16.5l10.8-.1zM153.294 131.662v-38.4h3.3v35.4h18.5v3"/><path fill="#99B1C8" d="M205.494 131.662l-3.8-9.4h-20.3l-3.8 9.4h-3.8l15.7-38.4h4.1l15.7 38.4h-3.8zm-14-34.9l-9 22.4h18.1l-9.1-22.4zM217.694 131.662v-16.4l-15.3-22h4l12.9 18.9 12.9-18.9h4l-15.3 22v16.4"/><g><path fill="#453767" d="M92.494 78.162v-23.9l-9.3 23.9h-3.7l-9.2-24v24h-8.5v-34.5h12.1l7.4 19.5 7.6-19.5h12.1v34.5M108.194 78.162v-34.5h25.1v7.4h-16.6v6h16.2v7.3h-16.2v6.5h16.6v7.3M156.395 78.762c-2.6 0-5-.4-7.3-1.2-2.3-.8-4.2-2-5.9-3.6-1.7-1.5-3-3.4-4-5.6s-1.5-4.7-1.5-7.4.5-5.2 1.5-7.4 2.3-4.1 4-5.6 3.7-2.7 5.9-3.6c2.2-.8 4.7-1.2 7.3-1.2 1.9 0 3.7.2 5.2.7s2.9 1.1 4.1 1.9c1.2.8 2.3 1.7 3.1 2.7.8.9 1.5 1.9 2.1 2.9l-7.1 3.7c-.7-1.102-1.6-2.102-2.9-3-1.3-.9-2.9-1.4-4.6-1.4-1.5 0-2.8.3-4 .8-1.2.5-2.3 1.3-3.2 2.2-.9.9-1.6 2-2.1 3.3s-.7 2.6-.7 4.1.2 2.8.7 4.1 1.2 2.4 2.1 3.3c.9.9 2 1.7 3.2 2.2 1.2.5 2.6.8 4 .8 1.3 0 2.6-.2 3.8-.7 1.2-.4 2.2-.9 2.8-1.5l.1-.1v-3.2h-7.8v-7.4h16.4v13.6c-1.8 2-4 3.7-6.5 4.9-2.6 1.1-5.5 1.7-8.7 1.7M200.694 78.162l-1.7-5h-14.2l-1.7 5h-9.7l13-34.5h11l13 34.5h-9.7zm-13.6-12.4h9.7l-4.8-14.3-4.9 14.3zM213.694 78.162v-34.5h17.3c1.9 0 3.6.3 5.1.9 1.5.6 2.7 1.4 3.7 2.5 1 1 1.8 2.3 2.3 3.6.5 1.4.8 2.9.8 4.4s-.3 3-.8 4.4c-.5 1.4-1.3 2.6-2.3 3.6s-2.3 1.9-3.7 2.5c-1.5.6-3.2.9-5.1.9h-8.8v11.8l-8.5-.1zm8.5-19.1h7.6c1.3 0 2.4-.3 3.2-1 .9-.7 1.3-1.7 1.3-3s-.4-2.3-1.3-3c-.8-.7-1.9-1-3.2-1h-7.6v8zM247.095 43.662h8.5v34.5h-8.5zM284.694 78.162l-7.8-12-7.7 12h-10.1l12.1-17.7-11.3-16.8h10l7 11.2 6.9-11.2h10.1l-11.2 16.8 12 17.7"/><path fill="#97DE32" d="M51.494 60.962l-25.1-25.2v25.2"/><path fill="#BDF950" d="M26.395 35.762l-25.2 25.2h25.2"/><path fill="#5DDB24" d="M1.194 60.962l25.2 25.1v-25.1"/><path fill="#38BB27" d="M26.395 60.962v25.1l25.1-25.1"/></g></symbol><symbol id="globosatplay__multishow_play" viewBox="0 0 295.89 167.449"><path fill="#E7525A" d="M155.005 17.89h11.906v26.237h17.466V54.46h-29.373M215.34 17.89h11.905v36.57H215.34zM189.14 28.222h-10.323V17.886h32.55v10.336h-10.323V54.46H189.14M81.302 54.062H69.24v-36.25h12.728l7.94 12.72 7.94-12.72h12.288v36.25H98.66V36.974L89.96 49.69l-8.656-12.716M138.087 17.812v19.724c0 4.707-2.053 6.588-5.295 6.588-3.238 0-5.295-2.05-5.295-6.87V17.813h-12.95v20.126c0 12.246 7.265 17.314 18.284 17.314 11 0 18.5-4.938 18.5-17.49v-19.95h-13.243zM128.083 59.665v12.96h-12.06v-12.96H104.2l-.008 37.42h11.83V84.12h12.06v12.966h11.476v-37.42M170.524 78.417v-.102c0-4.336-2.957-8.263-7.635-8.263-4.624 0-7.532 3.875-7.532 8.16v.103c0 4.34 2.96 8.264 7.635 8.264 4.627-.002 7.53-3.87 7.53-8.163m-27.428 0v-.102c0-10.616 8.678-19.08 19.898-19.08 11.223 0 19.795 8.36 19.795 18.978v.102c0 10.614-8.678 19.085-19.898 19.085-11.223 0-19.795-8.366-19.795-18.983M215.175 59.665h12.36v37.42H214.54l-7.94-12.72-7.937 12.72H186.34v-37.42h11.77V77.52l8.553-12.726 8.512 12.727M89.67 73.864c-.968-.232-2.923-.672-2.923-.672-3.926-.796-4.562-1.448-4.562-2.462v-.102c0-.905.807-1.81 2.664-1.81 2.642 0 5.31.93 8.183 2.394l8.02-7.23c-4.208-3.165-9.712-4.748-16.606-4.748-10.227 0-16.09 5.484-16.09 12.565v.103c0 6.354 5.066 9.13 11.27 10.87.97.23 3.256.668 3.256.668 3.922.794 4.893 1.446 4.893 2.457v.106c0 .905-1.037 1.812-2.895 1.812-2.642 0-5.43-.934-8.3-2.396l-8.08 7.228c4.207 3.166 9.486 4.75 16.38 4.75 10.228 0 15.84-5.486 15.84-12.563v-.105c0-6.36-4.85-9.133-11.05-10.866M87.604 149.604c10.598 0 19.246-8.65 19.246-19.247s-8.648-19.247-19.246-19.247-19.248 8.65-19.248 19.247 8.65 19.247 19.248 19.247m-6.83-25.506c0-1.57.787-2.49 1.928-2.5h.037c.462.006.983.16 1.534.478l10.82 6.26c1.93 1.113 1.93 2.934 0 4.047l-10.82 6.256c-.553.32-1.078.473-1.54.475h-.03c-1.143-.006-1.93-.922-1.93-2.497v-12.52z"/><path fill="#99B1C8" d="M115.08 149.57V111.08h14.46c1.85 0 3.498.3 4.945.893 1.445.6 2.66 1.416 3.645 2.454.982 1.04 1.734 2.242 2.256 3.607.52 1.368.78 2.8.78 4.3 0 1.502-.26 2.936-.78 4.3-.52 1.367-1.283 2.568-2.287 3.608-1.003 1.038-2.22 1.857-3.644 2.45-1.43.6-3.068.897-4.92.897H118.37v15.985h-3.29zm14.083-18.986c1.27 0 2.424-.202 3.46-.606 1.04-.405 1.935-.97 2.685-1.702.752-.73 1.328-1.597 1.732-2.598.404-1 .605-2.115.605-3.347 0-1.192-.2-2.297-.605-3.318-.404-1.02-.98-1.894-1.732-2.625-.75-.73-1.645-1.3-2.684-1.703-1.037-.403-2.19-.604-3.46-.604H118.37v16.505h10.793zM184.896 111.078h-4.105l-14.548 35.552h-18.477v-35.552h-3.295v38.56h24.355l3.814-9.483h20.405l3.814 9.482h3.813l-15.777-38.56zm-11.157 26.07l9.073-22.543 9.133 22.544H173.74zM208.618 149.63v-16.474l-15.32-22.083h3.988l12.98 18.96 12.98-18.96h3.987l-15.32 22.083v16.475"/></symbol><symbol id="globosatplay__off_play" viewBox="0 0 295.891 167.449"><path fill="#010101" d="M91.217 30.085c-.89 1.124-2.383 2.06-4.496 2.06-3 0-5.05-1.95-5.05-4.816 0-3.883 2.986-6.97 6.72-6.97 1.65 0 3.146.696 3.78 1.79l-1.033.68c-.348-.728-1.303-1.488-2.78-1.488-3.224 0-5.48 2.882-5.48 5.876 0 2.186 1.382 3.943 3.91 3.943 1.35 0 2.62-.522 3.59-1.678l.84.603zM92.678 31.86h-1.303l7.244-11.216h1.11l2.336 11.215h-1.19l-.605-2.947h-5.75l-1.842 2.946zm2.494-3.993h4.924l-1.176-5.955-3.748 5.955zM111.898 30.26h.032l1.7-9.616h1.128L112.77 31.86h-1.413l-4.605-9.742h-.033l-1.716 9.74h-1.127l1.984-11.214h1.415M115.63 31.86h-1.304l7.246-11.216h1.11l2.337 11.215h-1.194l-.602-2.947h-5.752l-1.84 2.946zm2.493-3.993h4.926l-1.177-5.955-3.75 5.955zM128.1 30.86h5.257l-.158 1h-6.388l1.987-11.216h1.126"/><path fill="#010101" d="M206.432 19.693c2.826-.432 6.475.43 8.252 3.34l13.168-8.007c-4.816-7.874-14.143-12.007-23.768-10.53-9.895 1.52-17.598 8.74-20.098 18.84-.998 4.03-1.834 8.14-2.598 12.24h-28.615c.596-2.902 1.22-5.775 1.91-8.554 1.385-5.59 5.277-6.992 7.473-7.33 2.826-.43 6.475.43 8.252 3.34l13.168-8.006c-4.816-7.874-14.143-12.007-23.768-10.53-9.895 1.52-17.596 8.74-20.098 18.842-.996 4.023-1.86 8.14-2.662 12.24H94.634c-14.664 0-26.596 11.895-26.596 26.516 0 14.62 11.932 26.516 26.596 26.516s26.594-11.896 26.594-26.516c0-4.56-1.16-8.854-3.2-12.606h16.368c-5.484 27.887-11.51 45.205-35.625 46.643l-3.27 15.474c19.36-.02 33.115-7.25 42.045-22.095 6.824-11.34 9.87-25.775 12.562-40.022h28.83c-2.2 12.528-4.607 24.155-9.76 32.625-4.86 7.988-11.79 12.354-21.988 13.68l-3.323 15.712c17.672-.9 30.29-7.928 38.496-21.417 6.973-11.464 9.734-26.182 12.23-40.6h16.868l2.942-13.91h-17.314c.57-2.906 1.184-5.777 1.87-8.555 1.384-5.592 5.276-6.994 7.474-7.33M94.635 74.698c-6.97 0-12.643-5.654-12.643-12.604 0-6.952 5.672-12.606 12.643-12.606s12.643 5.654 12.643 12.606c0 6.95-5.673 12.604-12.643 12.604M87.754 163.215c10.598 0 19.246-8.65 19.246-19.247s-8.648-19.247-19.246-19.247c-10.596 0-19.248 8.65-19.248 19.248s8.652 19.247 19.248 19.247m-6.83-25.506c0-1.572.787-2.49 1.928-2.5h.037c.464.005.983.158 1.534.477l10.822 6.26c1.926 1.113 1.926 2.934 0 4.047l-10.822 6.256c-.553.322-1.076.474-1.54.476h-.028c-1.145-.006-1.932-.92-1.932-2.497v-12.52z"/><path fill="#99B1C8" d="M115.23 163.183V124.69h14.46c1.85 0 3.5.3 4.944.893 1.447.598 2.662 1.415 3.645 2.453.98 1.04 1.733 2.242 2.255 3.607.52 1.367.78 2.8.78 4.3s-.26 2.935-.78 4.3c-.52 1.366-1.283 2.567-2.287 3.607-1.002 1.038-2.217 1.857-3.643 2.45-1.43.6-3.068.897-4.92.897H118.52v15.985h-3.29zm14.082-18.987c1.268 0 2.424-.202 3.46-.606 1.04-.405 1.937-.97 2.685-1.702.75-.73 1.328-1.597 1.732-2.598.403-1 .604-2.115.604-3.347 0-1.193-.2-2.298-.605-3.32-.405-1.02-.983-1.893-1.733-2.624-.748-.73-1.645-1.3-2.684-1.704-1.037-.403-2.193-.604-3.46-.604H118.52v16.505h10.792zM185.045 124.69h-4.105l-14.55 35.552h-18.474V124.69h-3.295v38.56h24.355l3.816-9.483h20.405l3.814 9.482h3.813l-15.778-38.56zm-11.156 26.07l9.073-22.543 9.133 22.544H173.89zM208.77 163.243v-16.475l-15.32-22.083h3.988l12.978 18.96 12.98-18.96h3.987l-15.32 22.083v16.475"/></symbol><symbol id="globosatplay__premiere_play" viewBox="0 0 295.889 167.449"><path fill="#2E8F44" d="M98.773 56.455v-3.06c0-.416-.342-.76-.76-.76h-13.42c-.42 0-.758.344-.758.76v3.878c.245-.04.5-.06.76-.06l13.42.004c.42-.005.758-.342.758-.762M214.036 56.455v-3.06c0-.416-.342-.76-.76-.76h-13.42c-.42 0-.76.344-.76.76v3.878c.248-.04.503-.06.76-.06l13.42.004c.42-.005.76-.342.76-.762M70.845 52.625H57.297c-.416 0-.76.34-.76.755v3.86c.25-.04.503-.06.76-.06h13.548c.42 0 .756-.34.758-.754V53.38c-.002-.415-.34-.755-.758-.755"/><path fill="#2E8F44" d="M251.837 40.888H44.05c-1.77 0-3.207 1.434-3.207 3.205v27.544c0 1.773 1.436 3.207 3.207 3.207h207.787c1.775 0 3.21-1.434 3.21-3.207V44.093c0-1.77-1.435-3.205-3.21-3.205M75.407 56.426c-.003 2.62-2.127 4.748-4.75 4.748h-13.36c-.42 0-.76.337-.76.75v5.178H52.55v-1.59h-.003V53.38c.003-2.622 2.124-4.75 4.75-4.75h13.36c2.625 0 4.747 2.126 4.75 4.75v3.046zm22.378 10.682l-5.89-5.882-7.3-.003c-.41.003-.745.318-.76.723V67.104h-4.013V53.397c.002-2.634 2.133-4.768 4.77-4.77h13.42c2.638.002 4.77 2.136 4.77 4.77v3.06c0 2.64-2.136 4.768-4.77 4.77h-.445l5.92 5.882h-5.702zm30.294-14.63h-15.745c-.504 0-.77.306-.77.663v2.462l16.514.02v3.877l-16.514.016v2.946c0 .41.396.647.754.647h15.76v3.972h-15.823c-3.267 0-5.05-2.344-5.05-4.653v-8.983c0-1.992 1.575-4.79 4.743-4.79h16.13v3.82zm28.552 14.637h-4.176v-13.15l-5.943 10.93h-3.605l-6.237-10.978v13.197h-4.174V48.64h5.108l6.958 12.134 7.097-12.15h4.973v18.49zm8.75.05h-4.334V48.57h4.334v18.594zm25.29-14.687h-15.744c-.502 0-.77.306-.77.663v2.462l16.513.02v3.877l-16.512.016v2.946c0 .41.395.647.754.647h15.76v3.972h-15.82c-3.27 0-5.054-2.344-5.054-4.653v-8.983c0-1.992 1.574-4.79 4.745-4.79h16.128v3.82zm22.376 14.63l-5.89-5.882-7.3-.003c-.412.003-.743.318-.762.723V67.104h-4.01V53.397c.002-2.634 2.133-4.768 4.77-4.77h13.42c2.636.002 4.768 2.136 4.77 4.77v3.06c-.002 2.64-2.136 4.768-4.77 4.77h-.446l5.918 5.882h-5.7zm30.293-14.63H227.6c-.505 0-.77.306-.77.663v2.462l16.513.02v3.877l-16.513.016v2.946c0 .41.396.647.756.647h15.758v3.972h-15.82c-3.267 0-5.053-2.344-5.053-4.653v-8.983c0-1.992 1.577-4.79 4.747-4.79h16.127v3.82zM87.76 126.53c10.597 0 19.245-8.65 19.245-19.248 0-10.6-8.648-19.246-19.244-19.246-10.597 0-19.25 8.647-19.25 19.246 0 10.597 8.653 19.247 19.25 19.247m-6.83-25.507c0-1.57.786-2.49 1.927-2.498h.037c.465.004.986.156 1.535.475l10.82 6.26c1.93 1.114 1.93 2.935 0 4.048l-10.82 6.257c-.553.322-1.077.473-1.54.476h-.03c-1.143-.007-1.93-.92-1.93-2.497v-12.52z"/><path fill="#99B1C8" d="M115.235 126.497V88.002h14.46c1.85 0 3.5.298 4.946.895 1.446.597 2.66 1.413 3.644 2.45.982 1.042 1.734 2.243 2.257 3.61.522 1.365.78 2.798.78 4.3 0 1.5-.26 2.934-.78 4.298-.52 1.366-1.283 2.568-2.285 3.606-1.004 1.04-2.22 1.858-3.645 2.455-1.43.597-3.067.895-4.92.895h-11.164v15.986h-3.29zm14.082-18.99c1.27 0 2.424-.2 3.462-.604 1.04-.405 1.934-.972 2.684-1.702.75-.73 1.328-1.597 1.732-2.596.402-1 .604-2.117.604-3.347 0-1.193-.2-2.3-.604-3.32-.404-1.02-.982-1.895-1.732-2.626s-1.645-1.297-2.685-1.702c-1.04-.402-2.193-.605-3.463-.605h-10.79v16.506h10.79zM185.05 88.002h-4.104l-14.55 35.554h-18.475V88.002h-3.294v38.56h24.354l3.816-9.48H193.2l3.814 9.48h3.814L185.05 88zm-11.155 26.072l9.074-22.545 9.13 22.544h-18.205zM208.774 126.556V110.08l-15.32-22.083h3.988l12.98 18.96 12.977-18.96h3.988l-15.32 22.084v16.476"/></symbol><symbol id="globosatplay__sportv_play" viewBox="0 0 295.89 167.449"><path fill="#010101" d="M172.26 57.55v-3.12c0-.423-.346-.77-.773-.77h-13.664c-.43 0-.775.347-.775.77v3.947c.252-.04.51-.058.775-.058h13.664c.427 0 .773-.346.773-.77M138.236 53.667h-13.71c-.413 0-.75.34-.75.758v9.124c0 .41.337.75.75.75h13.71c.414 0 .752-.34.752-.75v-9.125c0-.418-.34-.758-.752-.758M106.328 53.662H92.664c-.428 0-.773.346-.773.77v3.946c.253-.04.513-.058.774-.058h13.664c.428 0 .77-.348.775-.77v-3.12c-.003-.423-.35-.768-.775-.768"/><path fill="#205EAB" d="M86.965 129.52c10.596 0 19.244-8.648 19.244-19.245 0-10.6-8.65-19.247-19.245-19.247-10.598 0-19.25 8.648-19.25 19.247 0 10.597 8.652 19.246 19.25 19.246m-6.83-25.505c0-1.572.785-2.49 1.926-2.498h.038c.465.004.986.157 1.535.475l10.822 6.26c1.928 1.114 1.928 2.935 0 4.05l-10.822 6.255c-.55.32-1.076.473-1.54.475h-.028c-1.143-.005-1.93-.922-1.93-2.497v-12.52z"/><path fill="#99B1C8" d="M114.44 129.49V90.994h14.46c1.85 0 3.498.298 4.945.893 1.445.597 2.66 1.415 3.643 2.454.982 1.04 1.734 2.24 2.258 3.607.52 1.366.78 2.797.78 4.3 0 1.5-.26 2.934-.78 4.298-.523 1.367-1.285 2.568-2.287 3.608-1.005 1.038-2.22 1.856-3.646 2.453-1.428.597-3.066.895-4.918.895H117.73v15.985h-3.29zm14.08-18.99c1.27 0 2.425-.2 3.464-.603 1.04-.405 1.934-.972 2.684-1.703.75-.73 1.328-1.597 1.732-2.597.402-1 .604-2.116.604-3.348 0-1.193-.2-2.3-.604-3.32-.404-1.02-.982-1.893-1.732-2.625-.75-.73-1.645-1.298-2.684-1.703-1.04-.403-2.193-.604-3.463-.604h-10.79v16.505h10.79zM184.254 90.995h-4.104l-14.55 35.552h-18.476V90.995h-3.295v38.56h24.353l3.816-9.482h20.403l3.814 9.48h3.814l-15.776-38.558zm-11.154 26.07l9.074-22.544 9.13 22.546H173.1zM207.98 129.548v-16.476L192.657 90.99h3.988l12.98 18.96 12.978-18.96h3.988l-15.32 22.082v16.476"/><path fill="#205EAB" d="M255.355 41.225c0-1.84-1.566-3.326-3.514-3.326H44.046c-1.938 0-3.512 1.484-3.512 3.325v31.782c0 1.84 1.574 3.33 3.512 3.33h207.797c1.947 0 3.514-1.488 3.514-3.33V41.225z"/><path fill="#C12D3F" d="M251.85 37.895h-57.307v38.44h57.307c1.938 0 3.506-1.485 3.506-3.33v-31.78c0-1.845-1.57-3.33-3.506-3.33"/><path fill="#FFF" d="M107.783 57.527c-.002.396-.346.73-.775.73H93.3c-.26 0-.52.016-.772.054v-3.75c0-.402.346-.732.773-.732h13.708c.43 0 .773.33.775.732v2.967zm-.775-7.585H93.3c-2.69.006-4.866 2.07-4.868 4.617v11.796h.002v1.484h4.094v-4.972c0-.402.342-.734.773-.734h13.708c2.69 0 4.865-2.063 4.87-4.608l.003-2.967c-.007-2.546-2.18-4.61-4.872-4.618M61.383 51.164H75.13c.43 0 .772.334.776.737h4.105c0-2.553-2.187-4.626-4.88-4.626H61.383c-2.695 0-4.88 2.073-4.88 4.627v2.97c0 2.555 2.185 4.626 4.88 4.626H75.13c.43 0 .772.33.776.74v2.962c-.004.408-.348.74-.775.74H61.384c-.43 0-.773-.332-.773-.74h-4.107c0 2.558 2.186 4.627 4.88 4.627h13.75c2.692 0 4.88-2.07 4.88-4.627v-2.962c0-2.564-2.19-4.628-4.88-4.628h-13.75c-.43 0-.773-.333-.773-.738l-.004-2.968c.003-.404.347-.738.777-.738M158.65 58.257c-.268 0-.527.016-.777.054v-3.75c0-.402.348-.73.777-.73h13.7c.433 0 .774.328.774.73v2.967c0 .4-.342.73-.773.73h-13.7zm13.702 3.878c2.69 0 4.87-2.063 4.875-4.608V54.56c-.004-2.546-2.178-4.61-4.875-4.61l-13.7-.007c-2.7.006-4.874 2.07-4.874 4.617v13.272h4.096V62.84c.016-.396.357-.707.777-.707h7.458l6.01 5.697h5.824l-6.045-5.697h.455zM139.764 63.223c0 .394-.342.717-.762.717H125.26c-.418 0-.756-.323-.756-.717v-8.67c0-.396.338-.716.756-.716h13.742c.416 0 .762.32.762.716v8.67zm-.67-13.28h-13.93c-2.65.005-4.795 2.038-4.8 4.55v8.787c.005 2.512 2.15 4.552 4.8 4.552h13.93c2.652 0 4.797-2.04 4.803-4.552v-8.787c-.006-2.512-2.145-4.544-4.803-4.55M237.844 47.29l-9.817 14.825-9.806-14.826h-5.064l13.098 20.542h3.564l13.16-20.543M182.666 51.155h9.625v16.677h4.093V51.155h9.627v-3.872h-23.344"/></symbol><symbol id="globosatplay__syfy_play" viewBox="0 0 295.891 167.449"><path fill="#42145F" d="M87.756 151.34c10.598 0 19.244-8.65 19.244-19.248s-8.646-19.246-19.244-19.246-19.25 8.647-19.25 19.246 8.652 19.248 19.25 19.248m-6.83-25.507c0-1.57.787-2.49 1.928-2.498h.037c.464.004.985.156 1.536.475l10.822 6.26c1.928 1.114 1.928 2.935 0 4.048l-10.822 6.256c-.553.32-1.078.473-1.54.476h-.03c-1.14-.008-1.93-.923-1.93-2.5v-12.517z"/><g fill="#99B1C8"><path d="M115.232 151.308v-38.494h14.46c1.85 0 3.497.298 4.944.894 1.447.598 2.662 1.414 3.645 2.452.983 1.04 1.735 2.242 2.255 3.608.52 1.367.783 2.8.783 4.3s-.262 2.934-.783 4.3c-.52 1.365-1.28 2.567-2.283 3.605-1.004 1.04-2.22 1.857-3.646 2.453-1.428.6-3.066.895-4.918.895h-11.166v15.988h-3.29zm14.082-18.99c1.27 0 2.424-.2 3.463-.604 1.037-.405 1.932-.97 2.684-1.702.75-.73 1.327-1.597 1.73-2.597.405-1 .606-2.116.606-3.348 0-1.192-.2-2.298-.605-3.32-.403-1.018-.98-1.894-1.73-2.625-.75-.73-1.645-1.298-2.683-1.703-1.04-.404-2.193-.605-3.463-.605H118.52v16.505h10.794zM185.047 112.813h-4.105l-14.55 35.553H147.92v-35.553h-3.295v38.56h24.354l3.814-9.482h20.407l3.814 9.483h3.815l-15.778-38.56zm-11.154 26.072l9.072-22.545 9.133 22.545h-18.205zM208.77 151.367v-16.475l-15.32-22.083h3.99l12.978 18.96 12.978-18.96h3.99l-15.32 22.082v16.475"/></g><g fill="#3D2661"><path d="M185.246 38.926H171.87V37.46c-.005-.886.032-1.842.19-2.706.11-.64.285-1.2.49-1.625.336-.647.57-.968 1.3-1.413.733-.426 2.23-.998 5.21-1.002h9.125v-14.64h-9.125c-4.336 0-8.145.77-11.377 2.35-2.412 1.176-4.445 2.82-5.953 4.645-2.28 2.753-3.373 5.76-3.918 8.288-.543 2.537-.578 4.712-.58 6.103l.002 39.56h14.64l-.005-24.248h13.376V38.926zM207.684 99.98c12.875-.01 19.676-9.424 19.7-18.274v-42.78h-13.757v21.302c0 .973-.123 2.302-1.174 3.452-1.027 1.13-2.365 1.994-4.566 1.983-2.293.015-3.922-.836-4.748-1.998-.83-1.152-.99-2.474-.993-3.438v-21.3h-13.96V60.53c.015 9.007 6.677 18.257 17.872 18.28 2.183 0 4.35-.45 6.458-1.288l1.018-.404-.004 1.094-.004 3.136c0 .655-.014 1.566-.707 2.78-.68 1.225-2.19 2.42-4.728 2.4-2.29.01-3.764-1.002-4.543-2.04-.796-1.033-.974-2.043-1-2.185l-14.118-.057.285 2.25c1.633 7.725 8.67 15.5 18.967 15.483M134.562 99.98c12.877-.01 19.678-9.424 19.7-18.274v-42.78h-13.755v21.302c0 .973-.066 2.27-1.176 3.452-1.046 1.12-2.366 1.994-4.565 1.983-2.295.015-3.922-.836-4.748-1.998-.832-1.152-.988-2.474-.992-3.438v-21.3h-13.96V60.53c.015 9.007 6.677 18.257 17.87 18.28 2.184 0 4.348-.45 6.46-1.288l1.017-.404v1.094l-.006 3.136c0 .645-.012 1.566-.71 2.78-.68 1.225-2.19 2.42-4.728 2.4-2.29.01-3.764-1.002-4.543-2.04-.79-1.033-.973-2.043-1-2.185l-14.115-.057.28 2.25c1.64 7.725 8.674 15.5 18.972 15.483M71.736 59.12s.01-1.202.057-1.91l13.826.036-.042.846c-.014 2.472 1.49 4.827 2.812 5.796 1.316.977 3.207 1.293 4.357 1.293 2.96-.002 5.66-1.808 5.695-4.587l-.006-.255c-.094-2.168-1.467-3.355-3.432-4.274-1.762-.826-4.78-2.105-8.05-3.726-3.274-1.623-6.81-3.592-9.6-5.794-4.026-3.19-5.84-7.3-5.62-13.015.188-4.87 2.307-9.06 6.26-12.532 3.957-3.474 8.832-4.86 14-4.858 6.566 0 12.053 2.648 15.682 6.964 3.676 4.522 4.385 8.806 4.54 11.96l.028.604H97.9c-.146-1.476-.844-3.243-1.682-4.203-.986-1.14-2.506-1.88-4.408-1.876-1.098.007-2.383.33-3.465 1.016-1.068.672-1.953 1.784-1.945 3.25-.018 1.582.723 2.817 2.756 3.85 1.998 1.036 8.023 3.7 8.87 4.027 8.597 3.35 14.03 8.77 14.647 16.955.014.178.016.368.016.57.01 2.767-.82 7.555-3.99 11.732-3.18 4.18-8.68 7.83-15.888 7.83-14.25-.05-21.042-10.4-21.076-19.7"/></g></symbol><symbol id="globosatplay__telecine_play" viewBox="0 0 295.891 167.449"><path fill="#E53A33" d="M234.4 109.892H61.49V51.95H234.4v57.942zm-139-22.92c-1 .696-2.27 1.052-3.84 1.052-2.162 0-3.88-.63-5.143-1.88-1.3-1.242-1.967-2.936-1.967-5.085 0-2.147.6-3.884 1.816-5.175 1.164-1.297 2.867-1.944 5.145-1.944 1.806 0 3.073.688 3.83 1.17.858.504 1.478 1.307 1.81 1.658l9.722-6.68c-1.076-1.566-2.902-3.624-4.55-4.846-2.82-2.176-6.173-3.278-10.73-3.304-5.902.072-10.553 1.9-13.975 5.51-3.48 3.614-5.246 8.125-5.287 13.558.04 5.715 1.807 10.258 5.334 13.648 3.44 3.444 8.025 5.18 13.738 5.272 4.475-.092 8.174-1.3 11.156-3.674 1.662-1.312 3.04-2.758 4.197-4.33l-9.553-6.77c-.34.362-.666 1.118-1.702 1.82m37.002-24.09h-11.846c-.066.03-.094.185-.094.445V98.28c0 .243.027.383.094.435.043.026.176.03.434.03h11.412c.297 0 .46-.03.508-.092.033-.04.033-.15.033-.354V63.36c.055-.358-.14-.514-.54-.48m48.398.2c-.026-.062-.194-.092-.5-.092h-9.843c-.242 0-.38.014-.42.062-.033.056-.045.25-.045.568v13.648c-1.63-1.177-8.943-6.196-21.965-15.073-.205-.17-.346-.23-.453-.15-.082.01-.117.173-.117.447-.146 12.188-.146 23.683 0 35.848-.04.303.15.474.57.53h9.727c.27 0 .426-.057.453-.122.033-.03.062-.145.062-.357V84.665c1.45 1.126 8.767 6.132 21.923 15 .305.15.494.204.566.16.048-.073.087-.317.087-.704.156-13.8.156-25.19 0-35.504 0-.248-.013-.416-.046-.54m42.182 26.93c-.05-.035-.225-.057-.54-.057h-14.808v-4.367h14.068c.46.03.684-.2.656-.733v-7.938c0-.348-.02-.548-.068-.668-.074-.08-.256-.123-.588-.123h-14.068V71.56h14.332c.44.06.65-.223.635-.76v-7.03c0-.353-.024-.592-.065-.7-.062-.017-.264-.048-.695-.048h-25.818c-.17 0-.34.03-.36.048-.08.04-.1.2-.1.5v34.466c0 .306.03.513.14.62.088.105.26.16.502.16h26.238c.256 0 .432-.055.54-.16.032-.107.046-.314.046-.62V90.69c0-.382-.014-.594-.046-.68"/><path fill="#010101" d="M141.854 32.34h-14.72v-4.422h13.882c.453.048.676-.212.637-.712v-7.834c0-.312-.027-.537-.04-.638-.08-.09-.278-.135-.597-.135h-13.88v-4.527H141.4c.426.04.63-.21.588-.728V6.228c0-.33 0-.578-.04-.683-.075-.02-.25-.05-.548-.05h-25.818c-.29 0-.45.028-.488.05-.072.035-.092.205-.092.48V40.47c0 .295.05.507.148.6.072.103.242.164.48.164h26.223c.258 0 .42-.062.527-.165.05-.092.055-.304.055-.6V33.06c0-.37-.006-.59-.055-.673-.04-.022-.21-.047-.526-.047M222.182 32.34H207.48v-4.422h13.88c.445.048.674-.212.64-.712v-7.834c0-.312-.024-.537-.06-.638-.074-.09-.264-.135-.58-.135h-13.88v-4.527h14.27c.426.04.643-.21.61-.728V6.228c0-.33-.028-.578-.058-.683-.08-.02-.254-.05-.553-.05h-25.827c-.277 0-.438.028-.486.05-.06.035-.102.205-.102.48V40.47c0 .295.06.507.15.6.086.103.264.164.494.164h26.2c.27 0 .45-.062.54-.165.034-.092.063-.304.063-.6V33.06c0-.37-.03-.59-.062-.673-.023-.022-.206-.047-.538-.047M103.717 5.43H72.99c-.305 0-.44.168-.398.52v8.967c-.04.303.08.43.354.398h9.44v25.492c0 .242.03.386.097.42.04.035.176.053.418.053h11.26c.255 0 .425-.044.446-.108.033-.035.06-.16.06-.364V15.315h9.12c.242.03.357-.096.357-.398V5.95c0-.352-.135-.52-.426-.52M181.436 31.432h-12.682V5.958c.066-.36-.115-.512-.512-.48h-11.666c-.06.038-.1.193-.1.46v34.826c0 .233.04.382.1.408.03.044.178.06.428.06h24.492c.27 0 .436-.03.467-.103.043-.033.06-.154.06-.366v-8.78c0-.363-.207-.552-.587-.552"/><path fill="#99B1C8" d="M115.23 161.956v-38.493h14.46c1.85 0 3.497.298 4.944.894 1.445.598 2.66 1.414 3.645 2.452.98 1.04 1.733 2.24 2.255 3.607.52 1.365.78 2.8.78 4.3s-.26 2.934-.78 4.298c-.52 1.367-1.283 2.568-2.285 3.606-1.004 1.04-2.22 1.86-3.646 2.455-1.428.598-3.066.896-4.916.896H118.52v15.986h-3.29zm14.082-18.987c1.27 0 2.424-.203 3.463-.607 1.037-.404 1.932-.972 2.684-1.7.75-.732 1.325-1.6 1.73-2.6s.604-2.116.604-3.346c0-1.193-.2-2.3-.605-3.32-.405-1.02-.98-1.894-1.73-2.625-.753-.73-1.647-1.298-2.685-1.703-1.04-.404-2.193-.606-3.463-.606H118.52v16.506h10.792zM185.043 123.463h-4.104l-14.55 35.553h-18.475v-35.553h-3.295v38.558h24.354l3.816-9.48h20.404l3.812 9.48h3.816l-15.78-38.557zm-11.154 26.07l9.073-22.545 9.133 22.546H173.89zM208.768 162.016V145.54l-15.32-22.084h3.99l12.978 18.962 12.98-18.962h3.985l-15.318 22.085v16.476"/><path fill="#010101" d="M141.854 32.34h-14.72v-4.422h13.882c.453.048.676-.212.637-.712v-7.834c0-.312-.027-.537-.04-.638-.08-.09-.278-.135-.597-.135h-13.88v-4.527H141.4c.426.04.63-.21.588-.728V6.228c0-.33 0-.578-.04-.683-.075-.02-.25-.05-.548-.05h-25.818c-.29 0-.45.028-.488.05-.072.035-.092.205-.092.48V40.47c0 .295.05.507.148.6.072.103.242.164.48.164h26.223c.258 0 .42-.062.527-.165.05-.092.055-.304.055-.6V33.06c0-.37-.006-.59-.055-.673-.04-.022-.21-.047-.526-.047M222.182 32.34H207.48v-4.422h13.88c.445.048.674-.212.64-.712v-7.834c0-.312-.024-.537-.06-.638-.074-.09-.264-.135-.58-.135h-13.88v-4.527h14.27c.426.04.643-.21.61-.728V6.228c0-.33-.028-.578-.058-.683-.08-.02-.254-.05-.553-.05h-25.827c-.277 0-.438.028-.486.05-.06.035-.102.205-.102.48V40.47c0 .295.06.507.15.6.086.103.264.164.494.164h26.2c.27 0 .45-.062.54-.165.034-.092.063-.304.063-.6V33.06c0-.37-.03-.59-.062-.673-.023-.022-.206-.047-.538-.047M103.717 5.43H72.99c-.305 0-.44.168-.398.52v8.967c-.04.303.08.43.354.398h9.44v25.492c0 .242.03.386.097.42.04.035.176.053.418.053h11.26c.255 0 .425-.044.446-.108.033-.035.06-.16.06-.364V15.315h9.12c.242.03.357-.096.357-.398V5.95c0-.352-.135-.52-.426-.52M181.436 31.432h-12.682V5.958c.066-.36-.115-.512-.512-.48h-11.666c-.06.038-.1.193-.1.46v34.826c0 .233.04.382.1.408.03.044.178.06.428.06h24.492c.27 0 .436-.03.467-.103.043-.033.06-.154.06-.366v-8.78c0-.363-.207-.552-.587-.552"/><path fill="#99B1C8" d="M115.23 161.956v-38.493h14.46c1.85 0 3.497.298 4.944.894 1.445.598 2.66 1.414 3.645 2.452.98 1.04 1.733 2.24 2.255 3.607.52 1.365.78 2.8.78 4.3s-.26 2.934-.78 4.298c-.52 1.367-1.283 2.568-2.285 3.606-1.004 1.04-2.22 1.86-3.646 2.455-1.428.598-3.066.896-4.916.896H118.52v15.986h-3.29zm14.082-18.987c1.27 0 2.424-.203 3.463-.607 1.037-.404 1.932-.972 2.684-1.7.75-.732 1.325-1.6 1.73-2.6s.604-2.116.604-3.346c0-1.193-.2-2.3-.605-3.32-.405-1.02-.98-1.894-1.73-2.625-.753-.73-1.647-1.298-2.685-1.703-1.04-.404-2.193-.606-3.463-.606H118.52v16.506h10.792zM185.043 123.463h-4.104l-14.55 35.553h-18.475v-35.553h-3.295v38.558h24.354l3.816-9.48h20.404l3.812 9.48h3.816l-15.78-38.557zm-11.154 26.07l9.073-22.545 9.133 22.546H173.89zM208.768 162.016V145.54l-15.32-22.084h3.99l12.978 18.962 12.98-18.962h3.985l-15.318 22.085v16.476"/><path fill="#E53A33" d="M87.756 161.99c10.596 0 19.242-8.65 19.242-19.25 0-10.598-8.646-19.245-19.242-19.245-10.6 0-19.25 8.647-19.25 19.246 0 10.6 8.65 19.25 19.25 19.25m-6.832-25.508c0-1.57.787-2.488 1.928-2.498h.037c.462.004.985.157 1.534.475l10.822 6.258c1.928 1.115 1.928 2.936 0 4.05l-10.822 6.257c-.553.322-1.076.473-1.54.476h-.028c-1.143-.008-1.932-.922-1.932-2.498v-12.52z"/></symbol><symbol id="globosatplay__universal_play" viewBox="0 0 295.89 167.449"><path fill="#531C63" d="M98.925 43.127h-8.43C96.92 18.26 119.353 0 146.427 0c26.846 0 49.416 18.313 55.902 43.127h-8.405c-6.254-20.296-25.154-35.04-47.498-35.04-22.346 0-41.242 14.744-47.502 35.04m47.502 64.353c-23.297 0-42.852-16.034-48.23-37.667h-8.296c1.964 9.278 6.155 17.74 11.99 24.784l7.95-.56-.84 7.77c10.082 8.576 23.148 13.757 37.426 13.757 27.787 0 50.988-19.61 56.525-45.75h-8.298c-5.378 21.632-24.926 37.666-48.225 37.666"/><path fill="#531C63" d="M106.415 75.304v-.03c0-3 2.262-5.46 5.504-5.46 1.987 0 3.185.66 4.16 1.63l-1.474 1.702c-.816-.736-1.645-1.19-2.703-1.19-1.777 0-3.062 1.48-3.062 3.29v.028c0 1.81 1.255 3.32 3.063 3.32 1.21 0 1.947-.483 2.777-1.236l1.48 1.492c-1.09 1.16-2.297 1.883-4.334 1.883-3.104 0-5.41-2.395-5.41-5.43M118.565 69.994h2.32v4.185h4.282v-4.186h2.326v10.56h-2.326v-4.235h-4.283v4.233h-2.32M134.118 69.92h2.145l4.52 10.633h-2.424l-.966-2.37h-4.463l-.964 2.37H129.6l4.518-10.632zm2.444 6.214l-1.4-3.423-1.404 3.424h2.804zM142.655 69.994h2.143l4.947 6.502v-6.502h2.29v10.56h-1.98l-5.105-6.71v6.71h-2.295M155.288 69.994h2.14l4.948 6.502v-6.502h2.295v10.56h-1.976l-5.113-6.71v6.71h-2.292M167.917 69.994h7.96v2.068h-5.653v2.14h4.976v2.066h-4.976v2.216h5.732v2.07h-8.04M178.987 69.994h2.32v8.45h5.26v2.11h-7.58M79.49 57.725c0 .68-.09 1.268-.25 1.77-.507 1.56-1.735 2.302-3.42 2.302-1.067 0-1.954-.32-2.587-.975-.672-.696-1.06-1.767-1.06-3.236v-10.99H66.63v11.103c0 1.803.308 3.328.874 4.576 1.283 2.833 3.922 4.267 7.488 4.443h1.543c4.936-.242 8.17-2.845 8.47-8.33.01-.27.022-.548.022-.835v-10.96h-5.54v11.13zM100.55 56.18v.87l-.613-.8-7.526-9.655h-5.114v19.779h5.426V55.523l1.19 1.52 7.284 9.33h4.776V46.596h-5.422M108.46 55.423v10.95h5.48V46.596h-5.48M127.552 54.222l-1.768 4.89-1.7-4.748-2.79-7.77h-6.104l3.243 8.08 4.754 11.836h5.087l5.018-12.46 3.002-7.455h-5.986M143.187 58.576h9.49V54.25h-9.49v-3.023h10.478v-4.632H137.76v19.778h16.05V61.71h-10.623M173.304 55.044c.117-.515.178-1.074.178-1.673v-.05c0-1.95-.594-3.45-1.754-4.61-1.322-1.324-3.416-2.115-6.44-2.115h-9.35v19.78h5.48V60.38h2.43l3.983 5.994h6.302l-4.72-6.9c1.978-.836 3.405-2.302 3.892-4.43m-8.24 1.042h-3.647v-4.774h3.62c1.806 0 2.966.79 2.966 2.372v.06c0 .347-.066.665-.19.945-.388.874-1.36 1.396-2.748 1.396M191.292 60.213c0-1.615-.537-2.8-1.516-3.71-1.27-1.18-3.283-1.895-5.834-2.45-2.77-.622-3.447-1.017-3.447-1.92v-.056c0-.74.652-1.272 1.98-1.272 1.753 0 3.73.65 5.536 1.946l2.745-3.867c-2.15-1.723-4.77-2.63-8.14-2.63-4.747 0-7.657 2.66-7.657 6.326v.06c0 1.017.205 1.85.576 2.545 1.113 2.076 3.73 2.925 6.883 3.645 2.717.646 3.363 1.07 3.363 1.92v.057c0 .848-.795 1.36-2.29 1.36-2.286 0-4.43-.823-6.413-2.377l-3.053 3.644c2.455 2.18 5.766 3.277 9.27 3.277 4.803 0 7.998-2.403 7.998-6.44v-.057zM204.41 46.455h-5.284l-4.45 10.558-3.942 9.36h5.732l1.41-3.563h7.658l1.416 3.564h5.877l-3.066-7.258-5.35-12.66zm-4.917 12.09l.35-.877 1.88-4.714 2.07 5.263.136.327h-4.437zM223.866 61.568h-4.185V46.595h-5.484v19.779h15.06v-4.806M87.608 167.417c10.615 0 19.283-8.668 19.283-19.286 0-10.618-8.667-19.285-19.282-19.285-10.62 0-19.29 8.667-19.29 19.286 0 10.62 8.67 19.287 19.29 19.287m-6.843-25.56c0-1.576.787-2.493 1.932-2.5h.035c.467.003.99.153 1.54.474l10.843 6.273c1.932 1.116 1.932 2.938 0 4.056l-10.844 6.267c-.552.324-1.08.477-1.544.48H82.7c-1.146-.008-1.935-.93-1.935-2.506V141.86z"/><path fill="#99B1C8" d="M115.138 167.382v-38.57h14.486c1.855 0 3.508.3 4.96.897 1.444.596 2.663 1.415 3.647 2.456.985 1.043 1.74 2.247 2.26 3.615.524 1.366.784 2.806.784 4.31 0 1.5-.26 2.94-.783 4.306-.52 1.368-1.286 2.573-2.286 3.615-1.006 1.042-2.227 1.858-3.656 2.456-1.43.6-3.072.9-4.926.9h-11.19v16.016h-3.294zm14.11-19.025c1.27 0 2.427-.2 3.466-.61 1.043-.4 1.94-.97 2.69-1.703.75-.73 1.333-1.6 1.735-2.6.403-1.002.604-2.123.604-3.355 0-1.198-.2-2.304-.605-3.328-.403-1.02-.985-1.898-1.735-2.628-.752-.73-1.648-1.304-2.69-1.706-1.04-.406-2.198-.607-3.468-.607h-10.814v16.538h10.814zM185.09 128.812h-4.112l-14.582 35.625h-18.512v-35.625h-3.297v38.636h24.4l3.824-9.5h20.444l3.82 9.5h3.824l-15.81-38.636zm-11.18 26.12l9.095-22.584 9.146 22.585h-18.24zM208.858 167.44v-16.504l-15.347-22.13h3.993l13.004 19 13.006-19h3.998l-15.35 22.13v16.505"/></symbol><symbol id="globosatplay__viva_play" viewBox="0 0 295.891 167.449"><path fill="#F79522" d="M147.947.19C116.093.19 90.27 26.008 90.27 57.86s25.823 57.672 57.677 57.672c31.85 0 57.668-25.82 57.668-57.673 0-31.852-25.818-57.67-57.668-57.67M116.86 74.61h-6.51c-1.267-3.695-5.185-14.42-7.597-20.987-1.08-2.938-3.26-9.54-6.42-12.67 6.828.53 9.19 2.208 11.578 7.77 1.526 3.544 3.864 10.196 5.753 15.535l7.387-23.395h6.924L116.86 74.61zm21.97 0h-6.65V40.863h6.65V74.61zm16.457 0l-12.22-33.747h6.827l8.465 23.928 7.593-23.927h6.75L161.756 74.61h-6.47zm23.547-10.277c.4-.002 5.822-1.483 10.686.857l-6.236-15.244c-3.12 9.912-7.58 24.664-7.58 24.664h-6.695l10.753-33.747h6.508l13.286 33.747h-7.11c-2.766-5.237-7.38-8.826-13.612-10.277M87.756 167.227c10.598 0 19.244-8.65 19.244-19.248 0-10.6-8.646-19.247-19.244-19.247s-19.25 8.648-19.25 19.246c0 10.598 8.652 19.247 19.25 19.247m-6.832-25.507c0-1.57.79-2.49 1.928-2.498h.04c.462.004.983.157 1.534.476l10.82 6.26c1.928 1.113 1.928 2.934 0 4.048l-10.82 6.255c-.553.323-1.078.475-1.543.478h-.027c-1.143-.007-1.932-.923-1.932-2.498v-12.52z"/><path fill="#99B1C8" d="M115.23 167.194V128.7h14.46c1.85 0 3.5.298 4.944.894 1.447.598 2.662 1.414 3.645 2.452.983 1.04 1.733 2.242 2.255 3.61.52 1.364.78 2.797.78 4.298 0 1.5-.26 2.934-.78 4.3-.52 1.366-1.283 2.567-2.285 3.606-1.002 1.04-2.22 1.858-3.645 2.453-1.428.6-3.068.896-4.918.896H118.52v15.985h-3.29zm14.082-18.988c1.27 0 2.424-.202 3.463-.605 1.04-.403 1.934-.97 2.684-1.7s1.327-1.598 1.73-2.598c.405-1 .606-2.116.606-3.348 0-1.19-.2-2.3-.607-3.318-.403-1.02-.98-1.896-1.73-2.627-.75-.73-1.646-1.3-2.685-1.704-1.04-.403-2.193-.606-3.463-.606H118.52v16.506h10.792zM185.045 128.7h-4.104l-14.548 35.552h-18.477V128.7h-3.295v38.56h24.354l3.816-9.482h20.404l3.814 9.48h3.814L185.045 128.7zm-11.154 26.07l9.075-22.543 9.133 22.544H173.89zM208.77 167.254v-16.476l-15.32-22.083h3.988l12.98 18.96 12.977-18.96h3.99l-15.32 22.083v16.476"/></symbol></svg></div><header class="cabecalho"><h2 class="globosatplay__title"><a href="http://globosatplay.globo.com/" class="globosatplay__title-link"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="500" height="58" viewBox="0 0 500 58"><path fill-rule="evenodd" clip-rule="evenodd" fill="#98B0C7" d="M108.42 6.036h15.26c10.282 0 14.192 6.485 14.224 12.26.022 4.193-1.14 7.007-4.202 9.262 3.9 2.035 5.91 6.114 5.91 10.305 0 7.92-5.208 14.037-14.503 14.037h-16.69V6.036zm15.017 10.032l-3.3-.003v7.746h3.253c2.216 0 3.812-1.666 3.81-4.006-.005-2.423-2.043-3.735-3.763-3.736m.392 16.54h-3.692v9.155h3.958c2.52 0 4.443-2.236 4.443-4.545 0-2.505-1.936-4.61-4.71-4.61M82.026 16.695c6.758 0 12.27 5.515 12.27 12.272 0 6.757-5.512 12.273-12.27 12.273-6.76 0-12.272-5.517-12.272-12.273 0-6.757 5.513-12.272 12.272-12.272m0-11.928c13.326 0 24.2 10.874 24.2 24.2s-10.874 24.204-24.2 24.204c-13.328 0-24.205-10.875-24.205-24.202s10.877-24.2 24.205-24.2m83.038 11.927c6.757 0 12.27 5.515 12.27 12.272 0 6.757-5.514 12.273-12.27 12.273-6.758 0-12.274-5.517-12.274-12.273 0-6.757 5.518-12.272 12.275-12.272m0-11.928c13.325 0 24.2 10.874 24.2 24.2s-10.875 24.204-24.2 24.204c-13.327 0-24.206-10.875-24.206-24.202s10.878-24.2 24.205-24.2M51.417 6.035L39.764 25.4H23.027v10.035h11.408c-2.175 3.23-5.125 5.537-10.474 5.505-6.18-.035-12.07-4.104-12.07-12.655 0-7.258 6.262-12.128 11.824-12.125 4.25.004 9.437 1.27 12.948 7.096l6.26-10.362c-5.21-5.52-10.983-8.116-19.895-8.116C10.143 5.05.032 16.08 0 27.972 0 42.526 11.66 51.9 23.398 51.9h34.38V41.577H43.764l21.44-35.54H51.418z"/><path fill="#98B0C7" d="M207.336 53.162c-9.39 0-16.824-4.853-16.824-14.81V36.84h11.848c0 3.023 1.385 6.365 4.852 6.365 2.458 0 4.538-1.954 4.538-4.475 0-3.025-2.52-4.032-4.98-5.104-1.386-.63-2.77-1.197-4.158-1.766-6.048-2.52-11.405-6.048-11.405-13.36 0-8.693 8.38-13.734 16.32-13.734 4.538 0 9.644 1.7 12.604 5.292 2.397 2.96 2.963 5.546 3.09 9.2h-11.785c-.38-2.583-1.26-4.537-4.286-4.537-2.08 0-4.096 1.45-4.096 3.655 0 .694.063 1.388.44 1.955 1.134 1.89 7.248 4.285 9.2 5.167 6.177 2.834 10.902 6.05 10.902 13.36 0 9.768-7.06 14.305-16.26 14.305M286.824 16.108v35.79h-11.907V16.11h-9.203V6.036h30.25v10.072M247.565 6.053l-27.66 45.85h13.784l3.824-6.346h14.083v6.337h11.926V6.054h-15.958zm4.033 30.783h-8.816l8.816-14.615v14.616zM358.895 53.18V4.747h18.193c2.328 0 4.4.374 6.222 1.124 1.82.75 3.35 1.78 4.585 3.086 1.236 1.308 2.183 2.82 2.838 4.54s.983 3.52.983 5.408c0 1.888-.328 3.69-.983 5.41-.655 1.718-1.614 3.23-2.875 4.536-1.262 1.308-2.792 2.338-4.586 3.087-1.797.75-3.86 1.126-6.188 1.126h-14.05v20.112h-4.14zm17.718-23.89c1.597 0 3.05-.255 4.355-.763 1.307-.51 2.434-1.223 3.377-2.143.944-.92 1.67-2.01 2.18-3.268.507-1.258.76-2.662.76-4.21 0-1.5-.253-2.894-.76-4.177-.51-1.283-1.235-2.384-2.18-3.305-.943-.92-2.07-1.634-3.377-2.143-1.307-.508-2.76-.76-4.355-.76h-13.578v20.765h13.578zM446.732 4.748h-5.163l-18.307 44.73h-23.245V4.748h-4.146V53.26h30.642l4.8-11.928h25.672l4.8 11.928h4.8L446.732 4.748zM432.697 37.55l11.418-28.365 11.488 28.365h-22.906zM476.58 53.254v-20.73L457.307 4.74h5.018l16.33 23.857L494.98 4.74H500l-19.273 27.785v20.73"/><defs><path id="a" d="M324.327 4.747c-13.334 0-24.22 10.88-24.22 24.216s10.886 24.215 24.22 24.215c13.333 0 24.213-10.88 24.213-24.215S337.66 4.747 324.327 4.747zm9.425 26.764l-13.615 7.875c-.694.403-1.355.594-1.94.597h-.033c-1.438-.008-2.43-1.16-2.43-3.142V21.088c0-1.977.99-3.132 2.425-3.144h.046c.583.005 1.24.198 1.932.6l13.615 7.874c2.425 1.4 2.425 3.692 0 5.093z"/></defs><clipPath id="b"><use xlink:href="#a" overflow="visible"/></clipPath><linearGradient id="c" gradientUnits="userSpaceOnUse" x1="-281.142" y1="327.715" x2="-280.55" y2="327.715" gradientTransform="scale(81.6953) rotate(-45 257.577 -507.484)"><stop offset="0" stop-color="#7B61A6"/><stop offset=".061" stop-color="#8E5599"/><stop offset=".198" stop-color="#B53C80"/><stop offset=".324" stop-color="#D12B6E"/><stop offset=".435" stop-color="#E22063"/><stop offset=".518" stop-color="#E81C5F"/><stop offset=".587" stop-color="#E92859"/><stop offset=".714" stop-color="#ED484A"/><stop offset=".882" stop-color="#F37C31"/><stop offset="1" stop-color="#F7A51E"/></linearGradient><path clip-path="url(#b)" fill="url(#c)" d="M275.894 28.963l48.43-48.432 48.43 48.433-48.43 48.432"/></svg></a></h2><aside class="action analytics-area analytics-id-T"><a class="globosatplay__side-link" href="http://globosatplay.globo.com/conheca/">
        ConheÃ§a o GlobosatPlay
        <span class="arrow">âº</span></a></aside></header><div class="borda"><div class="rodape"></div></div><div class="globosatplay__logo-list-wrapper"><ul class="globosatplay__logo-list analytics-link-list"><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/gnt/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__gnt_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/sportv/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__sportv_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/multishow/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__multishow_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/globosat/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__globosat_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/bis/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__bis_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/viva/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__viva_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/canal-off/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__off_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/gloob/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__gloob_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/telecine/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__telecine_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/universal/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__universal_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/syfy/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__syfy_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/globonews/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__globonews_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/canal-brasil/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__brasil_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/megapix/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__megapix_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/combate/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__combate_play'></use></svg></a></li><li class="globosatplay__logo-list-item"><a href="http://globosatplay.globo.com/premierefc/ao-vivo/" class="globosatplay__logo-link"><svg class="globosatplay__logo"><use xlink:href='#globosatplay__premiere_play'></use></svg></a></li></ul></div></section><div class="separator "></div></div></section><section class="area globo-servicos container analytics-area analytics-id-C"><h2 class="titulo">serviÃ§os</h2><div class="borda"><span class="rodape"></span></div><div class="content"><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-loterias
" 
    data-lotteries-url="http://api.globo.com/lotteries/megasena.jsonp"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-fipe
"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-zap
" 
    data-zap-url="http://api.globo.com/zap/search/"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"></div></div></section><section class="area widgets_inferior franja-inferior container analytics-area analytics-id-C"><div class="box-links-servicos"><div class="links"><p>MAIS SERVIÃOS</p><a href="http://revistapegn.globo.com/Franquias/compare/">Abra uma franquia</a><a href="http://globoesporte.globo.com/eu-atleta/calculo-imc.html">AvaliaÃ§Ã£o fÃ­sica</a><a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">Conversor de moedas</a><a href="http://www.techtudo.com.br/softwares/">Downloads</a><a href="http://g1.globo.com/previsao-do-tempo/">PrevisÃ£o do tempo</a><a href="http://g1.globo.com/radar-g1/platb/">TrÃ¢nsito</a></div></div><div id="glb-shopping-widget" class="area globoshopping container" data-url="http://vitrines.globo.com/vitrine/vitrine.min.js" data-sitepage="globocom/home" data-responsive="0" data-branding-color="0669DE"></div></section><section class="area diretorio"><div class="container analytics-area analytics-id-Z"><div class="diretorio-responsive"><a href="/todos-os-sites.html">veja todos os sites da <strong>globo.com</strong> âº</a></div><div id="glb-diretorio"><div class="glb-conteudo"><div class="glb-bloco"><div class="glb-grid-12"><ul class="diretorio-indice"><li class="diretorio-linha diretorio-first "><ul><li class="diretorio-first-level analytics-product glb-menu-g1 diretorio-quebra "><a title="g1" href="http://g1.globo.com/">g1</a><ul><li class="diretorio-second-level"><a title="Acre" href="http://g1.globo.com/ac/acre/">Acre</a></li><li class="diretorio-second-level"><a title="Alagoas" href="http://g1.globo.com/al/alagoas/">Alagoas</a></li><li class="diretorio-second-level"><a title="Amazonas" href="http://g1.globo.com/am/amazonas/">Amazonas</a></li><li class="diretorio-second-level"><a title="Bahia" href="http://g1.globo.com/bahia/?noAudience=true">Bahia</a></li><li class="diretorio-second-level"><a title="Bem Estar" href="http://g1.globo.com/bemestar/">Bem Estar</a></li><li class="diretorio-second-level"><a title="Bom Dia Brasil" href="http://g1.globo.com/bom-dia-brasil/">Bom Dia Brasil</a></li><li class="diretorio-second-level"><a title="Brasil" href="http://g1.globo.com/brasil/">Brasil</a></li><li class="diretorio-second-level"><a title="Carnaval" href="http://g1.globo.com/carnaval/2013/">Carnaval</a></li><li class="diretorio-second-level"><a title="Carros" href="http://g1.globo.com/carros/">Carros</a></li><li class="diretorio-second-level"><a title="CearÃ¡" href="http://g1.globo.com/ceara/">CearÃ¡</a></li><li class="diretorio-second-level"><a title="CiÃªncia e SaÃºde" href="http://g1.globo.com/ciencia-e-saude/">CiÃªncia e SaÃºde</a></li><li class="diretorio-second-level"><a title="Como SerÃ¡?" href="http://redeglobo.globo.com/como-sera/index.html">Como SerÃ¡?</a></li><li class="diretorio-second-level"><a title="Concursos e Emprego" href="http://g1.globo.com/concursos-e-emprego/">Concursos e Emprego</a></li><li class="diretorio-second-level"><a title="Distrito Federal" href="http://g1.globo.com/distrito-federal/">Distrito Federal</a></li><li class="diretorio-second-level"><a title="Economia" href="http://g1.globo.com/economia/">Economia</a></li><li class="diretorio-second-level"><a title="EducaÃ§Ã£o" href="http://www.educacao.globo.com/">EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="EspÃ­rito Santo" href="http://g1.globo.com/espirito-santo/">EspÃ­rito Santo</a></li><li class="diretorio-second-level"><a title="FantÃ¡stico" href="http://fantastico.globo.com/">FantÃ¡stico</a></li><li class="diretorio-second-level"><a title="G1 EducaÃ§Ã£o" href="http://g1.globo.com/educacao/">G1 EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Globo Natureza" href="http://g1.globo.com/natureza/">Globo Natureza</a></li><li class="diretorio-second-level"><a title="Globo News" href="http://globonews.globo.com/">Globo News</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://g1.globo.com/economia/agronegocios/">Globo Rural</a></li><li class="diretorio-second-level"><a title="Globo RepÃ³rter" href="http://g1.globo.com/globo-reporter/">Globo RepÃ³rter</a></li><li class="diretorio-second-level"><a title="GoiÃ¡s" href="http://g1.globo.com/goias/">GoiÃ¡s</a></li><li class="diretorio-second-level"><a title="Jornal da Globo" href="http://g1.globo.com/jornal-da-globo/">Jornal da Globo</a></li><li class="diretorio-second-level"><a title="Jornal Hoje" href="http://g1.globo.com/jornal-hoje/">Jornal Hoje</a></li><li class="diretorio-second-level"><a title="Jornal Nacional" href="http://g1.globo.com/jornal-nacional/">Jornal Nacional</a></li><li class="diretorio-second-level"><a title="MaranhÃ£o" href="http://g1.globo.com/ma/maranhao/">MaranhÃ£o</a></li><li class="diretorio-second-level"><a title="Mato Grosso" href="http://g1.globo.com/mato-grosso/">Mato Grosso</a></li><li class="diretorio-second-level"><a title="Mato Grosso do Sul" href="http://g1.globo.com/mato-grosso-do-sul/">Mato Grosso do Sul</a></li><li class="diretorio-second-level"><a title="Minas Gerais" href="http://g1.globo.com/minas-gerais/">Minas Gerais</a></li><li class="diretorio-second-level"><a title="Mundo" href="http://g1.globo.com/mundo/">Mundo</a></li><li class="diretorio-second-level"><a title="ParÃ¡" href="http://g1.globo.com/pa/para/">ParÃ¡</a></li><li class="diretorio-second-level"><a title="ParaÃ­ba" href="http://g1.globo.com/paraiba/">ParaÃ­ba</a></li><li class="diretorio-second-level"><a title="ParanÃ¡" href="http://g1.globo.com/parana/">ParanÃ¡</a></li><li class="diretorio-second-level"><a title="Pernambuco" href="http://g1.globo.com/pernambuco/">Pernambuco</a></li><li class="diretorio-second-level"><a title="PiauÃ­" href="http://g1.globo.com/pi/piaui/">PiauÃ­</a></li><li class="diretorio-second-level"><a title="Planeta Bizarro" href="http://g1.globo.com/planeta-bizarro/">Planeta Bizarro</a></li><li class="diretorio-second-level"><a title="PolÃ­tica" href="http://g1.globo.com/politica/">PolÃ­tica</a></li><li class="diretorio-second-level"><a title="Pop &amp; Arte" href="http://g1.globo.com/pop-arte/">Pop &amp; Arte</a></li><li class="diretorio-second-level"><a title="PrevisÃ£o do Tempo" href="http://g1.globo.com/previsao-do-tempo.html">PrevisÃ£o do Tempo</a></li><li class="diretorio-second-level"><a title="ProfissÃ£o RepÃ³rter" href="http://g1.globo.com/profissao-reporter/">ProfissÃ£o RepÃ³rter</a></li><li class="diretorio-second-level"><a title="Rio de Janeiro" href="http://g1.globo.com/rio-de-janeiro/">Rio de Janeiro</a></li><li class="diretorio-second-level"><a title="Rio Grande do Norte" href="http://g1.globo.com/rn/rio-grande-do-norte/">Rio Grande do Norte</a></li><li class="diretorio-second-level"><a title="Rio Grande do Sul" href="http://g1.globo.com/rs/rio-grande-do-sul/">Rio Grande do Sul</a></li><li class="diretorio-second-level"><a title="RondÃ´nia" href="http://g1.globo.com/ro/rondonia/">RondÃ´nia</a></li><li class="diretorio-second-level"><a title="Santa Catarina" href="http://g1.globo.com/sc/santa-catarina/">Santa Catarina</a></li><li class="diretorio-second-level"><a title="SÃ£o Paulo" href="http://g1.globo.com/sao-paulo/">SÃ£o Paulo</a></li><li class="diretorio-second-level"><a title="Sergipe" href="http://g1.globo.com/se/sergipe/">Sergipe</a></li><li class="diretorio-second-level"><a title="Serra, Lagos e Norte" href="http://g1.globo.com/rj/serra-lagos-norte/">Serra, Lagos e Norte</a></li><li class="diretorio-second-level"><a title="Tecnologia e Games" href="http://g1.globo.com/tecnologia/">Tecnologia e Games</a></li><li class="diretorio-second-level"><a title="Turismo e Viagem" href="http://g1.globo.com/turismo-e-viagem/">Turismo e Viagem</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-globoesporte diretorio-quebra "><a title="globoesporte" href="http://globoesporte.globo.com/">globoesporte</a><ul><li class="diretorio-second-level"><a title="Atletismo" href="http://globoesporte.globo.com/atletismo/">Atletismo</a></li><li class="diretorio-second-level"><a title="Basquete" href="http://globoesporte.globo.com/basquete/">Basquete</a></li><li class="diretorio-second-level"><a title="Basquete/NBB" href="http://globoesporte.globo.com/basquete/nbb/">Basquete/NBB</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie A" href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">BrasileirÃ£o SÃ©rie A</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie B" href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">BrasileirÃ£o SÃ©rie B</a></li><li class="diretorio-second-level"><a title="Campeonato Carioca" href="http://globoesporte.globo.com/rj/futebol/campeonato-carioca/index.html">Campeonato Carioca</a></li><li class="diretorio-second-level"><a title="Campeonato GaÃºcho" href="http://globoesporte.globo.com/rs/futebol/campeonato-gaucho/index.html">Campeonato GaÃºcho</a></li><li class="diretorio-second-level"><a title="Campeonato Mineiro" href="http://globoesporte.globo.com/mg/futebol/campeonato-mineiro/index.html">Campeonato Mineiro</a></li><li class="diretorio-second-level"><a title="Campeonato Paulista" href="http://globoesporte.globo.com/sp/futebol/campeonato-paulista/index.html">Campeonato Paulista</a></li><li class="diretorio-second-level"><a title="Cartola FC" href="http://globoesporte.globo.com/cartola-fc/">Cartola FC</a></li><li class="diretorio-second-level"><a title="Copa do Mundo da Fifa" href="http://globoesporte.globo.com/futebol/copa-do-mundo/index.html">Copa do Mundo da Fifa</a></li><li class="diretorio-second-level"><a title="Copa AmÃ©rica" href="http://globoesporte.globo.com/futebol/copa-america/">Copa AmÃ©rica</a></li><li class="diretorio-second-level"><a title="Copa Sul-Americana" href="http://globoesporte.globo.com/futebol/copa-sul-americana/index.html">Copa Sul-Americana</a></li><li class="diretorio-second-level"><a title="Desafio de Futsal" href="http://globoesporte.globo.com/eventos/desafio-de-futsal/">Desafio de Futsal</a></li><li class="diretorio-second-level"><a title="Esporte Espetacular" href="http://globoesporte.globo.com/programas/esporte-espetacular/">Esporte Espetacular</a></li><li class="diretorio-second-level"><a title="Esportes AquÃ¡ticos" href="http://globoesporte.globo.com/aquaticos/">Esportes AquÃ¡ticos</a></li><li class="diretorio-second-level"><a title="Esportes Radicais" href="http://globoesporte.globo.com/radicais/">Esportes Radicais</a></li><li class="diretorio-second-level"><a title="Eu Atleta" href="http://globoesporte.globo.com/eu-atleta/">Eu Atleta</a></li><li class="diretorio-second-level"><a title="FÃ³rmula 1" href="http://globoesporte.globo.com/motor/formula-1/">FÃ³rmula 1</a></li><li class="diretorio-second-level"><a title="Futebol" href="http://globoesporte.globo.com/futebol/">Futebol</a></li><li class="diretorio-second-level"><a title="Futebol Internacional" href="http://globoesporte.globo.com/futebol/futebol-internacional/">Futebol Internacional</a></li><li class="diretorio-second-level"><a title="FutpÃ©dia" href="http://futpedia.globo.com/">FutpÃ©dia</a></li><li class="diretorio-second-level"><a title="Game Futebol" href="http://gamefutebol.globoesporte.globo.com/">Game Futebol</a></li><li class="diretorio-second-level"><a title="Liga dos CampeÃµes" href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">Liga dos CampeÃµes</a></li><li class="diretorio-second-level"><a title="OlimpÃ­adas" href="http://globoesporte.globo.com/olimpiadas/">OlimpÃ­adas</a></li><li class="diretorio-second-level"><a title="SeleÃ§Ã£o Brasileira" href="http://globoesporte.globo.com/futebol/selecao-brasileira/">SeleÃ§Ã£o Brasileira</a></li><li class="diretorio-second-level"><a title="StockCar" href="http://globoesporte.globo.com/motor/stock-car/">StockCar</a></li><li class="diretorio-second-level"><a title="Surfe" href="http://globoesporte.globo.com/radicais/surfe/">Surfe</a></li><li class="diretorio-second-level"><a title="TÃªnis" href="http://globoesporte.globo.com/tenis/">TÃªnis</a></li><li class="diretorio-second-level"><a title="Vai-e-vem do Mercado" href="http://globoesporte.globo.com/futebol/vai-e-vem-do-mercado/">Vai-e-vem do Mercado</a></li><li class="diretorio-second-level"><a title="VerÃ£o Espetacular" href="http://globoesporte.globo.com/programas/verao-espetacular/">VerÃ£o Espetacular</a></li><li class="diretorio-second-level"><a title="VÃ´lei" href="http://globoesporte.globo.com/volei/">VÃ´lei</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-gshow diretorio-quebra "><a title="gshow" href="http://gshow.globo.com/">gshow</a><ul><li class="diretorio-second-level"><a title="A Regra do Jogo" href="http://gshow.globo.com/novelas/a-regra-do-jogo/">A Regra do Jogo</a></li><li class="diretorio-second-level"><a title="AlÃ©m do Tempo" href="http://gshow.globo.com/novelas/alem-do-tempo/">AlÃ©m do Tempo</a></li><li class="diretorio-second-level"><a title="Altas Horas" href="http://altashoras.globo.com/">Altas Horas</a></li><li class="diretorio-second-level"><a title="Amor &amp; Sexo" href="http://amoresexo.globo.com/">Amor &amp; Sexo</a></li><li class="diretorio-second-level"><a title="Bastidores" href="http://gshow.globo.com/Bastidores/">Bastidores</a></li><li class="diretorio-second-level"><a title="BBB" href="http://bbb.globo.com/">BBB</a></li><li class="diretorio-second-level"><a title="CaldeirÃ£o do Huck" href="http://caldeiraodohuck.globo.com/">CaldeirÃ£o do Huck</a></li><li class="diretorio-second-level"><a title="Caminho das Ãndias" href="http://gshow.globo.com/novelas/caminho-das-indias/">Caminho das Ãndias</a></li><li class="diretorio-second-level"><a title="Como Fazer" href="http://gshow.globo.com/como-fazer/">Como Fazer</a></li><li class="diretorio-second-level"><a title="DomingÃ£o do FaustÃ£o" href="http://domingaodofaustao.globo.com/">DomingÃ£o do FaustÃ£o</a></li><li class="diretorio-second-level"><a title="Ã de Casa" href="http://gshow.globo.com/programas/e-de-casa/">Ã de Casa</a></li><li class="diretorio-second-level"><a title="Encontro com FÃ¡tima Bernardes" href="http://tvg.globo.com/programas/encontro-com-fatima-bernardes/">Encontro com FÃ¡tima Bernardes</a></li><li class="diretorio-second-level"><a title="Escolinha do Professor Raimundo" href="http://gshow.globo.com/programas/escolinha-do-professor-raimundo/">Escolinha do Professor Raimundo</a></li><li class="diretorio-second-level"><a title="Esquenta!" href="http://tvg.globo.com/programas/esquenta/">Esquenta!</a></li><li class="diretorio-second-level"><a title="Estilo" href="http://gshow.globo.com/Estilo/">Estilo</a></li><li class="diretorio-second-level"><a title="Estrelas" href="http://estrelas.globo.com/">Estrelas</a></li><li class="diretorio-second-level"><a title="Ãta Mundo Bom!" href="http://gshow.globo.com/novelas/eta-mundo-bom/">Ãta Mundo Bom!</a></li><li class="diretorio-second-level"><a title="LigaÃ§Ãµes Perigosas" href="http://gshow.globo.com/series/ligacoes-perigosas/">LigaÃ§Ãµes Perigosas</a></li><li class="diretorio-second-level"><a title="Mais VocÃª" href="http://maisvoce.globo.com/">Mais VocÃª</a></li><li class="diretorio-second-level"><a title="MalhaÃ§Ã£o" href="http://malhacao.globo.com/">MalhaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Mister Brau" href="http://gshow.globo.com/series/mister-brau/2015/">Mister Brau</a></li><li class="diretorio-second-level"><a title="MÃºsica" href="http://gshow.globo.com/Musica/">MÃºsica</a></li><li class="diretorio-second-level"><a title="PÃ© na Cova" href="http://gshow.globo.com/series/pe-na-cova/2015/">PÃ© na Cova</a></li><li class="diretorio-second-level"><a title="Programa do JÃ´" href="http://programadojo.globo.com/">Programa do JÃ´</a></li><li class="diretorio-second-level"><a title="Receitas.com" href="http://www.receitas.com/">Receitas.com</a></li><li class="diretorio-second-level"><a title="Receitas da Ana Maria" href="http://gshow.globo.com/programas/mais-voce/Receitas-da-Ana-Maria/">Receitas da Ana Maria</a></li><li class="diretorio-second-level"><a title="SÃ©ries Originais" href="http://gshow.globo.com/webseries/">SÃ©ries Originais</a></li><li class="diretorio-second-level"><a title="SessÃ£o ComÃ©dia" href="http://gshow.globo.com/sessao-comedia/videos/">SessÃ£o ComÃ©dia</a></li><li class="diretorio-second-level"><a title="TÃ¡ no Ar" href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/index.html">TÃ¡ no Ar</a></li><li class="diretorio-second-level"><a title="The Voice Brasil" href="http://gshow.globo.com/realities/the-voice-brasil/">The Voice Brasil</a></li><li class="diretorio-second-level"><a title="The Voice Kids" href="http://gshow.globo.com/programas/the-voice-kids/">The Voice Kids</a></li><li class="diretorio-second-level"><a title="Totalmente Demais" href="http://gshow.globo.com/novelas/totalmente-demais/">Totalmente Demais</a></li><li class="diretorio-second-level"><a title="Tomara que Caia" href="http://gshow.globo.com/programas/tomara-que-caia/">Tomara que Caia</a></li><li class="diretorio-second-level"><a title="Video Show" href="http://videoshow.globo.com/">Video Show</a></li><li class="diretorio-second-level"><a title="Zorra" href="http://gshow.globo.com/programas/zorra/">Zorra</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-famosos-etc diretorio-quebra "><a title="famosos &amp; etc" href="http://famosos.globo.com/">famosos &amp; etc</a><ul><li class="diretorio-second-level"><a title="EGO" href="http://ego.globo.com/">EGO</a></li><li class="diretorio-second-level"><a title="G1 Cinema" href="http://g1.globo.com/pop-arte/cinema/">G1 Cinema</a></li><li class="diretorio-second-level"><a title="G1 MÃºsica" href="http://g1.globo.com/pop-arte/musica/">G1 MÃºsica</a></li><li class="diretorio-second-level"><a title="Globo Jogos" href="http://jogos.globo.com/">Globo Jogos</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="HorÃ³scopo" href="http://horoscopo.ego.globo.com/ne/home.html">HorÃ³scopo</a></li><li class="diretorio-second-level"><a title="Meus 5 minutos" href="http://www.meus5minutos.com.br/">Meus 5 minutos</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="Namoro na Web" href="http://tracking.parperfeito.com.br/ppbanner/bannerTracker?originId=13&amp;identifierId=41763&amp;actionId=1">Namoro na Web</a></li><li class="diretorio-second-level"><a title="Paparazzo" href="http://paparazzo.globo.com/">Paparazzo</a></li><li class="diretorio-second-level"><a title="Patricia Kogut" href="http://oglobo.globo.com/cultura/kogut/">Patricia Kogut</a></li><li class="diretorio-second-level"><a title="Retratos da Vida" href="http://extra.globo.com/lazer/retratosdavida/">Retratos da Vida</a></li><li class="diretorio-second-level"><a title="Senninha" href="http://senninha.globo.com/">Senninha</a></li><li class="diretorio-second-level"><a title="Teatro" href="http://www.agentesevenoteatro.com.br/">Teatro</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-globo-play diretorio-sem-quebra "><a title="globo play" href="http://globoplay.globo.com/">globo play</a></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-grupo-globo diretorio-quebra "><a title="Grupo Globo" href="http://grupoglobo.globo.com/">Grupo Globo</a><ul><li class="diretorio-second-level"><a title="princÃ­pios editoriais" href="http://g1.globo.com/principios-editoriais-do-grupo-globo.html">princÃ­pios editoriais</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-editora-globo diretorio-sem-quebra "><a title="editora globo" href="http://editoraglobo.globo.com/">editora globo</a><ul><li class="diretorio-second-level"><a title="Auto Esporte" href="http://revistaautoesporte.globo.com/">Auto Esporte</a></li><li class="diretorio-second-level"><a title="Casa e Comida" href="http://revistacasaejardim.globo.com/Casa-e-Comida/">Casa e Comida</a></li><li class="diretorio-second-level"><a title="Casa e Jardim" href="http://revistacasaejardim.globo.com/">Casa e Jardim</a></li><li class="diretorio-second-level"><a title="Casa Vogue" href="http://casavogue.globo.com/">Casa Vogue</a></li><li class="diretorio-second-level"><a title="Crescer" href="http://revistacrescer.globo.com/">Crescer</a></li><li class="diretorio-second-level"><a title="Ãpoca" href="http://revistaepoca.globo.com/">Ãpoca</a></li><li class="diretorio-second-level"><a title="Ãpoca NegÃ³cios" href="http://epocanegocios.globo.com/">Ãpoca NegÃ³cios</a></li><li class="diretorio-second-level"><a title="Galileu" href="http://revistagalileu.globo.com/">Galileu</a></li><li class="diretorio-second-level"><a title="Glamour" href="http://revistaglamour.globo.com/">Glamour</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://revistagloborural.globo.com/">Globo Rural</a></li><li class="diretorio-second-level"><a title="GQ Brasil" href="http://gq.globo.com/">GQ Brasil</a></li><li class="diretorio-second-level"><a title="Marie Claire" href="http://revistamarieclaire.globo.com/">Marie Claire</a></li><li class="diretorio-second-level"><a title="Meus 5 Minutos" href="http://meus5minutos.globo.com/">Meus 5 Minutos</a></li><li class="diretorio-second-level"><a title="Monet" href="http://revistamonet.globo.com/">Monet</a></li><li class="diretorio-second-level"><a title="Mundo do SÃ­tio" href="http://mundodositio.globo.com/">Mundo do SÃ­tio</a></li><li class="diretorio-second-level"><a title="PEGN" href="http://revistapegn.globo.com/">PEGN</a></li><li class="diretorio-second-level"><a title="Quem" href="http://revistaquem.globo.com/">Quem</a></li><li class="diretorio-second-level"><a title="Vogue" href="http://vogue.globo.com/">Vogue</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-infoglobo diretorio-sem-quebra "><a title="infoglobo" href="http://infoglobo.com.br/Anuncie/Home.aspx">infoglobo</a><ul><li class="diretorio-second-level"><a title="Ela Digital" href="http://ela.oglobo.globo.com/">Ela Digital</a></li><li class="diretorio-second-level"><a title="Extra" href="http://extra.globo.com/">Extra</a></li><li class="diretorio-second-level"><a title="O Globo" href="http://oglobo.globo.com/">O Globo</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-last "><ul><li class="diretorio-first-level analytics-product glb-menu-globosat diretorio-quebra "><a title="globosat" href="http://canaisglobosat.globo.com/">globosat</a><ul><li class="diretorio-second-level"><a title="Globosat Play" href="http://globosatplay.globo.com/">Globosat Play</a></li><li class="diretorio-second-level"><a title="+Globosat" href="http://maisglobosat.globo.com/">+Globosat</a></li><li class="diretorio-second-level"><a title="BIS" href="http://canalbis.globo.com/">BIS</a></li><li class="diretorio-second-level"><a title="Canal Brasil" href="http://canalbrasil.globo.com/">Canal Brasil</a></li><li class="diretorio-second-level"><a title="Canal Futura" href="http://www.futura.org.br/">Canal Futura</a></li><li class="diretorio-second-level"><a title="Combate" href="http://combate.globo.com/">Combate</a></li><li class="diretorio-second-level"><a title="GloboNews" href="http://globonews.globo.com/">GloboNews</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="Megapix" href="http://megapix.globo.com/">Megapix</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="Off" href="http://canaloff.globo.com/">Off</a></li><li class="diretorio-second-level"><a title="Premiere" href="http://sociopremiere.globo.com/">Premiere</a></li><li class="diretorio-second-level"><a title="SporTV" href="http://sportv.globo.com/">SporTV</a></li><li class="diretorio-second-level"><a title="Studio Universal" href="http://studiouniversal.globo.com/">Studio Universal</a></li><li class="diretorio-second-level"><a title="Syfy" href="http://syfy.globo.com/">Syfy</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Universal Channel" href="http://uc.globo.com/">Universal Channel</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-sistema-globo-de-radio diretorio-sem-quebra "><a title="sistema globo de rÃ¡dio" href="http://globoradio.globo.com/home/HOME.htm">sistema globo de rÃ¡dio</a><ul><li class="diretorio-second-level"><a title="BHFM" href="http://bhfm.globoradio.globo.com/home/HOME.htm">BHFM</a></li><li class="diretorio-second-level"><a title="CBN" href="http://cbn.globoradio.globo.com/home/HOME.htm">CBN</a></li><li class="diretorio-second-level"><a title="RADIOBEAT" href="http://radiobeat.com.br/">RADIOBEAT</a></li><li class="diretorio-second-level"><a title="RÃ¡dio Globo" href="http://radioglobo.globoradio.globo.com/home/HOME.htm">RÃ¡dio Globo</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-globo-filmes diretorio-sem-quebra "><a title="globo filmes" href="http://globofilmes.globo.com/">globo filmes</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-som-livre diretorio-sem-quebra "><a title="som livre" href="http://www.somlivre.com/">som livre</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-blog-globocom diretorio-sem-quebra "><a title="blog globo.com" href="http://blog.globo.com/1.html">blog globo.com</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-zap-imoveis diretorio-sem-quebra "><a title="zap imÃ³veis" href="http://www.zapimoveis.com.br/">zap imÃ³veis</a></li></ul></li></ul></div></div></div></div></div></section><div id="x61" class="opec-area opec-mobile opec-x61 grid-12"><div id="banner_mobile_fim" class="tag-manager-publicidade-container"></div></div><footer><div class="container analytics-area analytics-id-B"><a href="http://www.globo.com/" title="Globo.com">Globo.com</a><span class="copyright">&copy; Copyright 2000-2015 Globo ComunicaÃ§Ã£o e ParticipaÃ§Ãµes S.A.</span><span class="copyright-480 hidden"><p>&copy; 2000-2015.</p><p>Todos os direitos reservados.</p></span><ul class="footer-menu"><li><a href="/privacidade.html">polÃ­tica de privacidade e seguranÃ§a</a></li><li><a href="/todos-os-sites.html">todos os sites</a></li><li><a href="http://anuncie.globo.com/">anuncie conosco</a></li><li><a href="http://meuperfil.globo.com">central globo.com</a></li></ul></div></footer></div><!--[if lt IE 9]><script src="http://s.glbimg.com/en/ho/static/libby/css3pie/js/PIE.js" type="text/javascript"></script><![endif]--><script>
responsiveHub.updateImages();</script><script>
(function(){var scr=document.createElement('script');scr.type = 'text/javascript';scr.async=true;scr.src = 'http://s.glbimg.com/en/ho/static/CACHE/js/991f8930202c.js';document.getElementsByTagName('head')[0].appendChild(scr);}());</script><div id="opec-frame1"><div id="banner_floating" class="tag-manager-publicidade-container"></div></div><script>if(document.body && document.body.offsetWidth <= 711){}else{setTimeout(function(){var bannerMiddleTop = $("#ad-position-middle"),
bannerMiddleBottom = $('#ad-position-middle3');if(bannerMiddleTop.height() < 15){bannerMiddleTop.addClass('without-opec');}if(bannerMiddleBottom.height() < 15){bannerMiddleBottom.addClass('without-opec');}}, 6000);}
</script><script>
window.glb=window.glb || {};window.glb.analyticsConfig=window.glb.analyticsConfig  || {};window.glb.analyticsConfig = {scrollEventChance: 0};</script><script>
document.cookie = 'globoapi.geo.geolocation=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.estado=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.novela=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.time=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';setTimeout(function(){window.glb.barra=window.glb.barra || {};window.glb.barra.component=window.glb.barra.component || {};window.glb.barra.component.barraDiv = {className: ''};}, 1000);</script><style>
#assinante-menu .barra-notifications-area .float-box.config-active .save-area{bottom:0;position:absolute;width:100%}#assinante-menu .barra-notifications-area .float-box.config-active .config-box{padding-bottom:44px}</style><style>
header .container{height:150px}.box-destaque-uber .destaques-primario .titulo{font-family:"ProximaNovaA-Bold"}@media (min-width:1600px){.conteudo-uber .box-destaque-uber{position:static;max-width:1600px;margin:0 auto}}</style><style>
.agrupador-quadruplo-tecnologia .destaque-inferior ul{display:none;// css de folder}</style><link type="text/css" rel="stylesheet" href="http://c.api.globo.com/soccer_teams/b_45x45.css"></body></html>
<!-- PÃ¡gina gerada em 12/12/2015 16:40:14 -->
